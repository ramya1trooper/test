import { Renderer2, ViewContainerRef } from "@angular/core";
import { MatPaginator } from "@angular/material";
export declare class StylePaginatorDirective {
    private readonly matPag;
    private vr;
    private ren;
    private _currentPage;
    private _pageGapTxt;
    private _rangeStart;
    private _rangeEnd;
    private _buttons;
    showTotalPages: number;
    private _showTotalPages;
    constructor(matPag: MatPaginator, vr: ViewContainerRef, ren: Renderer2);
    private buildPageNumbers;
    private createButton;
    private initPageRange;
    private switchPage;
    ngAfterViewInit(): void;
}
