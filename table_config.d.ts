export declare const manageAvTableConfig: ({
    value: string;
    name: string;
    isactive: boolean;
    onSelect?: undefined;
} | {
    value: string;
    name: string;
    isactive: boolean;
    onSelect: boolean;
})[];
export declare const manageAvDetailTableConfig: ({
    value: string;
    name: string;
    isactive: boolean;
    onSelect?: undefined;
} | {
    value: string;
    name: string;
    isactive: boolean;
    onSelect: boolean;
})[];
export declare const manageAvbyLedgerUserTableConfig: {
    value: string;
    name: string;
    isactive: boolean;
}[];
export declare const manageAvbyVendorUserTableConfig: {
    value: string;
    name: string;
    isactive: boolean;
}[];
export declare const manageAvbyInvoiceUserTableConfig: {
    value: string;
    name: string;
    isactive: boolean;
}[];
