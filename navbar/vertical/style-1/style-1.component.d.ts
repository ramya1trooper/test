import { OnDestroy, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FuseConfigService } from "../../../@fuse/services/config.service";
import { FuseNavigationService } from "../../../@fuse/components/navigation/navigation.service";
import { FusePerfectScrollbarDirective } from "../../../@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";
import { FuseSidebarService } from "../../../@fuse/components/sidebar/sidebar.service";
export declare class NavbarVerticalStyle1Component implements OnInit, OnDestroy {
    private _fuseConfigService;
    private _fuseNavigationService;
    private _fuseSidebarService;
    private _router;
    english: any;
    fuseConfig: any;
    navigation: any;
    toggleOpen: boolean;
    private _fusePerfectScrollbar;
    private _unsubscribeAll;
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     */
    constructor(_fuseConfigService: FuseConfigService, _fuseNavigationService: FuseNavigationService, _fuseSidebarService: FuseSidebarService, _router: Router, english: any);
    directive: FusePerfectScrollbarDirective;
    /**
     * On init
     */
    ngOnInit(): void;
    /**
     * On destroy
     */
    ngOnDestroy(): void;
    /**
     * Toggle sidebar opened status
     */
    toggleSidebarOpened(): void;
    /**
     * Toggle sidebar folded status
     */
    toggleSidebarFolded(): void;
}
