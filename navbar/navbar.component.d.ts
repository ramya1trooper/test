import { ElementRef, Renderer2 } from '@angular/core';
export declare class NavbarComponent {
    private _elementRef;
    private _renderer;
    _variant: string;
    /**
     * Constructor
     *
     * @param {ElementRef} _elementRef
     * @param {Renderer2} _renderer
     */
    constructor(_elementRef: ElementRef, _renderer: Renderer2);
    /**
     * Variant
     */
    variant: string;
}
