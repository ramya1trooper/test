import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { ContentComponent } from "./content.component";
import { TableLayoutModule } from "../table-layout/table-layout.module";
import { ButtonLayoutModule } from "../button-layout/button-layout.module";
import { MaterialModule } from "../material.module";
import { FormLayoutModule } from "../form-layout/form-layout.module";
import { NewTableLayoutModule } from "../new-table-layout/new-table-layout.module";
//from
import { CardLayoutModule } from "../card-layout/card-layout.module";
import { TranslateModule } from "@ngx-translate/core";
// import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClientModule } from "@angular/common/http";
import { ChartLayoutModule } from "../chart-layout/chart-layout.module";
import { GridListLayoutModule } from "../grid-list-layout/grid-list-layout.module";
import { ReportManagementModule } from '../avm/report-management.module';
//to this
var ContentModule = /** @class */ (function () {
    function ContentModule() {
    }
    ContentModule.forRoot = function (metaData, environment, english) {
        return {
            ngModule: ContentModule,
            providers: [
                {
                    provide: "metaData",
                    useValue: metaData
                },
                {
                    provide: "environment",
                    useValue: environment
                },
                {
                    provide: "english",
                    useValue: english
                }
            ]
        };
    };
    ContentModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ContentComponent],
                    imports: [
                        RouterModule,
                        FuseSharedModule,
                        TableLayoutModule,
                        ButtonLayoutModule,
                        FormLayoutModule,
                        MaterialModule,
                        ReportManagementModule,
                        NewTableLayoutModule,
                        //from
                        CardLayoutModule,
                        GridListLayoutModule,
                        ChartLayoutModule,
                        HttpClientModule,
                        TranslateModule.forRoot()
                        //to this
                    ],
                    exports: [
                        ContentComponent,
                        MaterialModule,
                        //from
                        TranslateModule
                        //to this
                    ]
                },] }
    ];
    return ContentModule;
}());
export { ContentModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiY29udGVudC9jb250ZW50Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFMUQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDeEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBRXJFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ25GLE1BQU07QUFDTixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZUFBZSxFQUFtQixNQUFNLHFCQUFxQixDQUFDO0FBQ3ZFLGtFQUFrRTtBQUNsRSxPQUFPLEVBQWMsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUdwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNuRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztBQUN2RSxTQUFTO0FBRVQ7SUFBQTtJQStDQSxDQUFDO0lBbkJRLHFCQUFPLEdBQWQsVUFBZSxRQUFRLEVBQUUsV0FBVyxFQUFFLE9BQU87UUFDM0MsT0FBTztZQUNMLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsVUFBVTtvQkFDbkIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxhQUFhO29CQUN0QixRQUFRLEVBQUUsV0FBVztpQkFDdEI7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLFNBQVM7b0JBQ2xCLFFBQVEsRUFBRSxPQUFPO2lCQUNsQjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7O2dCQTlDRixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7b0JBQ2hDLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLGdCQUFnQjt3QkFDaEIsaUJBQWlCO3dCQUNqQixrQkFBa0I7d0JBQ2xCLGdCQUFnQjt3QkFDaEIsY0FBYzt3QkFDZCxzQkFBc0I7d0JBQ3RCLG9CQUFvQjt3QkFDcEIsTUFBTTt3QkFDTixnQkFBZ0I7d0JBQ2hCLG9CQUFvQjt3QkFDcEIsaUJBQWlCO3dCQUNqQixnQkFBZ0I7d0JBQ2hCLGVBQWUsQ0FBQyxPQUFPLEVBQUU7d0JBQ3pCLFNBQVM7cUJBQ1Y7b0JBQ0QsT0FBTyxFQUFFO3dCQUNQLGdCQUFnQjt3QkFDaEIsY0FBYzt3QkFDZCxNQUFNO3dCQUNOLGVBQWU7d0JBQ2YsU0FBUztxQkFDVjtpQkFDRjs7SUFxQkQsb0JBQUM7Q0FBQSxBQS9DRCxJQStDQztTQXBCWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IENvbnRlbnRDb21wb25lbnQgfSBmcm9tIFwiLi9jb250ZW50LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBUYWJsZUxheW91dE1vZHVsZSB9IGZyb20gXCIuLi90YWJsZS1sYXlvdXQvdGFibGUtbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBCdXR0b25MYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vYnV0dG9uLWxheW91dC9idXR0b24tbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gXCIuLi9tYXRlcmlhbC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgRm9ybUxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9mb3JtLWxheW91dC9mb3JtLWxheW91dC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IE5ld1RhYmxlTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL25ldy10YWJsZS1sYXlvdXQvbmV3LXRhYmxlLWxheW91dC5tb2R1bGVcIjtcclxuLy9mcm9tXHJcbmltcG9ydCB7IENhcmRMYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vY2FyZC1sYXlvdXQvY2FyZC1sYXlvdXQubW9kdWxlXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSwgVHJhbnNsYXRlTG9hZGVyIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuLy8gaW1wb3J0IHtUcmFuc2xhdGVIdHRwTG9hZGVyfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9odHRwLWxvYWRlcic7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcclxuaW1wb3J0IHsgTmd4Q2hhcnRzTW9kdWxlIH0gZnJvbSBcIkBzd2ltbGFuZS9uZ3gtY2hhcnRzXCI7XHJcbmltcG9ydCB7IENoYXJ0c01vZHVsZSB9IGZyb20gXCJuZzItY2hhcnRzXCI7XHJcbmltcG9ydCB7IENoYXJ0TGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2NoYXJ0LWxheW91dC9jaGFydC1sYXlvdXQubW9kdWxlXCI7XHJcbmltcG9ydCB7IEdyaWRMaXN0TGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2dyaWQtbGlzdC1sYXlvdXQvZ3JpZC1saXN0LWxheW91dC5tb2R1bGVcIjtcclxuaW1wb3J0IHtSZXBvcnRNYW5hZ2VtZW50TW9kdWxlfSBmcm9tICcuLi9hdm0vcmVwb3J0LW1hbmFnZW1lbnQubW9kdWxlJztcclxuLy90byB0aGlzXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0NvbnRlbnRDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIEZ1c2VTaGFyZWRNb2R1bGUsXHJcbiAgICBUYWJsZUxheW91dE1vZHVsZSxcclxuICAgIEJ1dHRvbkxheW91dE1vZHVsZSxcclxuICAgIEZvcm1MYXlvdXRNb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIFJlcG9ydE1hbmFnZW1lbnRNb2R1bGUsXHJcbiAgICBOZXdUYWJsZUxheW91dE1vZHVsZSxcclxuICAgIC8vZnJvbVxyXG4gICAgQ2FyZExheW91dE1vZHVsZSxcclxuICAgIEdyaWRMaXN0TGF5b3V0TW9kdWxlLFxyXG4gICAgQ2hhcnRMYXlvdXRNb2R1bGUsXHJcbiAgICBIdHRwQ2xpZW50TW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLmZvclJvb3QoKVxyXG4gICAgLy90byB0aGlzXHJcbiAgXSxcclxuICBleHBvcnRzOiBbXHJcbiAgICBDb250ZW50Q29tcG9uZW50LFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAvL2Zyb21cclxuICAgIFRyYW5zbGF0ZU1vZHVsZVxyXG4gICAgLy90byB0aGlzXHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGVudE1vZHVsZSB7XHJcbiAgc3RhdGljIGZvclJvb3QobWV0YURhdGEsIGVudmlyb25tZW50LCBlbmdsaXNoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBuZ01vZHVsZTogQ29udGVudE1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvdmlkZTogXCJtZXRhRGF0YVwiLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IG1ldGFEYXRhXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBcImVudmlyb25tZW50XCIsXHJcbiAgICAgICAgICB1c2VWYWx1ZTogZW52aXJvbm1lbnRcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IFwiZW5nbGlzaFwiLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IGVuZ2xpc2hcclxuICAgICAgICB9XHJcbiAgICAgIF1cclxuICAgIH07XHJcbiAgfVxyXG59XHJcbiJdfQ==