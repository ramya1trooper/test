import * as tslib_1 from "tslib";
import { Component, ViewEncapsulation, Inject } from "@angular/core";
import { MessageService } from "../_services/message.service";
import { HttpClient } from "@angular/common/http";
import { Compiler } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ContentService } from "./content.service";
import * as _ from "lodash";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from '../loader.service';
// import {REPORT_MANAGEMENT_ROUTE} from '../avm/report-management-routing.module';
// import { navigation } from "./../navigation/navigation";
var ContentComponent = /** @class */ (function () {
    function ContentComponent(compiler, router, messageService, contentService, httpClient, route, loaderService, _metaData) {
        var _this = this;
        this.compiler = compiler;
        this.router = router;
        this.messageService = messageService;
        this.contentService = contentService;
        this.httpClient = httpClient;
        this.route = route;
        this.loaderService = loaderService;
        this._metaData = _metaData;
        this.enableHeading = false;
        this.enableAVM = false;
        this.enableAVMDetails = false;
        this.enableAVMUser = false;
        this.enableConfigTracker = false;
        this.enableConfigSetup = false;
        this.enableLibSetup = false;
        this.enableRouting = false;
        this.unsubscribe = new Subject();
        this.unsubscribeRouting = new Subject();
        console.log(">>>>>>>", this._metaData);
        this.messageService
            .getRouting()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(function (data) {
            localStorage.removeItem("selectedTableHeaders");
            console.log(data, ".....data");
            data = data.data;
            _this.enableHeading = false;
            if (data) {
                _this.currentRouteID = data.id;
                _this.data = data;
                _this.loadRouting(_this.data);
            }
        });
        this.messageService.getRoutingMessage()
            .pipe(takeUntil(this.unsubscribeRouting))
            .subscribe(function (data) {
            console.log(data, ">>> DATA");
            data = data.pageData;
            _this.enableHeading = false;
            var hideView = data.hideView;
            if (hideView.indexOf('newTableLayout') >= 0) {
                _this.currentData.enableNewTableLayout = false;
            }
            if (hideView.indexOf('formLayout') >= 0) {
                _this.currentData.enableFormLayout = false;
            }
            _this.loadPageRedirection();
        });
    }
    ContentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (x) {
            if (_.isEmpty(x)) {
                setTimeout(function () {
                    _this.loadRouting(_this.getMetaData()[0]);
                }, 1500);
            }
        });
    };
    ContentComponent.prototype.getMetaData = function () {
        console.log(this._metaData);
        var navigationList = [];
        _.forEach(this._metaData, function (item) {
            var childrenList = [];
            if (item.subMenu && item.subMenu.length) {
                _.forEach(item.subMenu, function (menuItem) {
                    var tempItem = {
                        id: menuItem.menuName,
                        title: menuItem.title,
                        type: menuItem.subMenu && menuItem.subMenu.length
                            ? "collapsable"
                            : "item",
                        url: menuItem.routingUI,
                        configFilePath: menuItem.configFilePath
                    };
                    childrenList.push(tempItem);
                });
            }
            var temp = {
                id: item.menuName,
                title: item.title,
                type: item.subMenu && item.subMenu.length ? "collapsable" : "item",
                translate: item.translate,
                icon: item.icon,
                children: childrenList,
                configFilePath: item.configFilePath
            };
            navigationList.push(temp);
        });
        return navigationList;
    };
    ContentComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
        this.unsubscribeRouting.next();
    };
    ContentComponent.prototype.receiveClick = function (event) {
        console.log(event, "....event");
        this.messageService.sendButtonEnableMessage("data");
    };
    ContentComponent.prototype.loadData = function (data) {
        var _this = this;
        console.log(data, "......DATADATAA");
        this.enableRouting = false;
        this.httpClient.get(data.configFilePath).subscribe(function (fileResponse) {
            if (fileResponse)
                localStorage.setItem("currentConfigData", JSON.stringify(fileResponse));
            _this.currentConfigData = fileResponse;
            _this.currentData = fileResponse ? fileResponse["listView"] : {};
            console.log(_this.currentData, ".....currentData");
            if (_this.currentData && _this.currentData.productName) {
                localStorage.setItem("productName", _this.currentData.productName);
            }
            setTimeout(function () {
                _this.enableHeading = true;
            }, 1000);
        });
    };
    ContentComponent.prototype.loadRouting = function (data) {
        this.enableAVM = false;
        this.enableAVMDetails = false;
        this.enableAVMUser = false;
        this.enableConfigTracker = false;
        this.enableConfigSetup = false;
        this.enableLibSetup = false;
        console.log(this.router, ">>>>router");
        if (data.id == "manageAvm") {
            this.enableAVM = true;
        }
        else if (data.id == 'manageAvDetail') {
            this.enableAVMDetails = true;
        }
        else if (data.id == 'manageAvUser') {
            this.enableAVMUser = true;
        }
        else if (data.id == 'manageConfigTracker') {
            this.enableConfigTracker = true;
        }
        else if (data.id == 'manageConfigTrackerSetup') {
            this.enableConfigSetup = true;
        }
        else {
            this.enableLibSetup = true;
            var template = "";
            var tmpCmp = Component({ template: template })(/** @class */ (function () {
                function class_1() {
                }
                return class_1;
            }()));
            var appRoutes = tslib_1.__spread(this.router.config);
            var url = data.url ? data.url : "/" + data.id;
            var route = {
                path: url.slice(1),
                component: tmpCmp
            };
            appRoutes.push(route);
            this.router.resetConfig(appRoutes);
            this.router.navigateByUrl(data.url);
            this.loadData(data);
        }
    };
    ContentComponent.prototype.loadPageRedirection = function () {
        var _this = this;
        console.log(this, ">> THIS");
        // this.enableHeading = true;
        var currentPageLoad = this.currentConfigData.pageRoutingView;
        this.enableRouting = true;
        this.currentData = currentPageLoad;
        // this.currentData.enableButtonLayout = true;
        setTimeout(function () {
            _this.enableHeading = true;
        }, 200);
    };
    ContentComponent.decorators = [
        { type: Component, args: [{
                    selector: "content",
                    template: "<div class=\"page-layout blank\" style=\"padding: 0px !important;\" fusePerfectScrollbar *ngIf=\"enableLibSetup\">\r\n  <mat-drawer-container class=\"example-container sen-bg-container\" autosize fxFlex [hasBackdrop]=\"false\">\r\n    <div>\r\n      <ng-container *ngIf=\"enableHeading\">\r\n        <button-layout [fromRouting]=\"enableRouting\"></button-layout>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"currentData && currentData.enableFormLayout\">\r\n        <ng-container *ngFor=\"let formField of currentData.formData\">\r\n          <form-layout [formValues]=\"formField\" [fromRouting]=\"enableRouting\"></form-layout>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- <div [hidden]=\"!currentData || !currentData.enableTableLayout\">\r\n        <ng-container *ngFor=\"let tableItem of currentData.tableData\">\r\n          <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n            [viewFrom]=\"'list'\"></table-layout>\r\n        </ng-container>\r\n      </div> -->\r\n      <div *ngIf=\"currentData && currentData.enableNewTableLayout\">\r\n        <ng-container *ngFor=\"let tableItem of currentData.tableData\">\r\n          <new-table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n            [viewFrom]=\"'list'\"></new-table-layout>\r\n        </ng-container>\r\n      </div>\r\n      <div *ngIf=\"currentData && currentData.tableData && currentData.enableTableLayout\">\r\n        <ng-container *ngFor=\"let tableItem of currentData.tableData\">\r\n          <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n            [viewFrom]=\"'list'\"></table-layout>\r\n        </ng-container>\r\n      </div>\r\n      <ng-container *ngIf=\"currentData && currentData.enableChartAndCard\">\r\n        <card-layout>\r\n        </card-layout>\r\n        <chart-layout></chart-layout>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"currentData && currentData.enableGridList\">\r\n        <grid-list-layout></grid-list-layout>\r\n      </ng-container>\r\n    </div>\r\n  </mat-drawer-container>\r\n</div>\r\n<ng-container *ngIf=\"enableAVM\">\r\n  <app-manage-av></app-manage-av>\r\n</ng-container>\r\n<ng-container *ngIf=\"enableAVMDetails\">\r\n  <app-manage-av-detail></app-manage-av-detail>\r\n</ng-container>\r\n<ng-container *ngIf=\"enableAVMUser\">\r\n  <app-manage-av-byuser></app-manage-av-byuser>\r\n</ng-container>\r\n<ng-container *ngIf=\"enableConfigTracker\">\r\n  <app-config-tracker></app-config-tracker>\r\n</ng-container>\r\n<ng-container *ngIf=\"enableConfigSetup\">\r\n  <app-config-tracker-setup></app-config-tracker-setup>\r\n</ng-container>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: ["content{position:relative;display:-webkit-box;display:flex;z-index:1;-webkit-box-flex:1;flex:1 0 auto}content>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto;width:100%;min-width:100%}.example-container{width:100%;height:100%}.card-directive{background:#fff;border:1px solid #d3d3d3;margin:5px!important}chart-layout{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;-webkit-box-flex:1!important;flex:auto!important}"]
                }] }
    ];
    /** @nocollapse */
    ContentComponent.ctorParameters = function () { return [
        { type: Compiler },
        { type: Router },
        { type: MessageService },
        { type: ContentService },
        { type: HttpClient },
        { type: ActivatedRoute },
        { type: LoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["metaData",] }] }
    ]; };
    return ContentComponent;
}());
export { ContentComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiY29udGVudC9jb250ZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBYSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDaEYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBRTlELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVsRCxPQUFPLEVBQVksUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBZ0IsTUFBTSxFQUFFLGNBQWMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNuRCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsbUZBQW1GO0FBQ25GLDJEQUEyRDtBQUUzRDtJQStCRSwwQkFDVSxRQUFrQixFQUNsQixNQUFjLEVBQ2QsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsVUFBc0IsRUFDdEIsS0FBcUIsRUFDckIsYUFBNEIsRUFDUixTQUFTO1FBUnZDLGlCQXdDQztRQXZDUyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDckIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDUixjQUFTLEdBQVQsU0FBUyxDQUFBO1FBOUJ2QyxrQkFBYSxHQUFZLEtBQUssQ0FBQztRQU0vQixjQUFTLEdBQWEsS0FBSyxDQUFDO1FBQzVCLHFCQUFnQixHQUFZLEtBQUssQ0FBQztRQUNsQyxrQkFBYSxHQUFhLEtBQUssQ0FBQztRQUNoQyx3QkFBbUIsR0FBYSxLQUFLLENBQUM7UUFDdEMsc0JBQWlCLEdBQWEsS0FBSyxDQUFDO1FBQ3BDLG1CQUFjLEdBQWEsS0FBSyxDQUFDO1FBR2pDLGtCQUFhLEdBQWEsS0FBSyxDQUFDO1FBRXhCLGdCQUFXLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUNsQyx1QkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBZS9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsY0FBYzthQUNoQixVQUFVLEVBQUU7YUFDWixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNqQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQ2IsWUFBWSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQy9CLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ2pCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzNCLElBQUksSUFBSSxFQUFFO2dCQUNSLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztnQkFDOUIsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzdCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFO2FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDeEMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFVBQVUsQ0FBQyxDQUFBO1lBQzVCLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1lBQzNCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDN0IsSUFBRyxRQUFRLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLElBQUcsQ0FBQyxFQUFDO2dCQUN4QyxLQUFJLENBQUMsV0FBVyxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQzthQUMvQztZQUNELElBQUcsUUFBUSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsSUFBRyxDQUFDLEVBQUM7Z0JBQ3BDLEtBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO2FBQzNDO1lBQ0QsS0FBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDN0IsQ0FBQyxDQUFDLENBQUE7SUFDSixDQUFDO0lBQ0QsbUNBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLFVBQUEsQ0FBQztZQUMzQixJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2hCLFVBQVUsQ0FBQztvQkFDVCxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUMxQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDVjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELHNDQUFXLEdBQVg7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1QixJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFFeEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVMsSUFBSTtZQUNyQyxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUN2QyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBUyxRQUFRO29CQUN2QyxJQUFJLFFBQVEsR0FBRzt3QkFDYixFQUFFLEVBQUUsUUFBUSxDQUFDLFFBQVE7d0JBQ3JCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSzt3QkFDckIsSUFBSSxFQUNGLFFBQVEsQ0FBQyxPQUFPLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzRCQUN6QyxDQUFDLENBQUMsYUFBYTs0QkFDZixDQUFDLENBQUMsTUFBTTt3QkFDWixHQUFHLEVBQUUsUUFBUSxDQUFDLFNBQVM7d0JBRXZCLGNBQWMsRUFBRSxRQUFRLENBQUMsY0FBYztxQkFDeEMsQ0FBQztvQkFDRixZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQzthQUNKO1lBRUQsSUFBSSxJQUFJLEdBQUc7Z0JBQ1QsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ2xFLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dCQUNmLFFBQVEsRUFBRSxZQUFZO2dCQUN0QixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7YUFDcEMsQ0FBQztZQUNGLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLGNBQWMsQ0FBQztJQUN4QixDQUFDO0lBQ0Qsc0NBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFDRCx1Q0FBWSxHQUFaLFVBQWEsS0FBSztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztRQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFDRCxtQ0FBUSxHQUFSLFVBQVMsSUFBSTtRQUFiLGlCQWdCQztRQWZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFlBQVk7WUFDN0QsSUFBSSxZQUFZO2dCQUNkLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO1lBQzFFLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUM7WUFDdEMsS0FBSSxDQUFDLFdBQVcsR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQ2xELElBQUcsS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBQztnQkFDbEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNuRTtZQUNELFVBQVUsQ0FBQztnQkFDVCxLQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQTtZQUMzQixDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDWCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxzQ0FBVyxHQUFYLFVBQVksSUFBSTtRQUNkLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDOUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztRQUNqQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxZQUFZLENBQUMsQ0FBQTtRQUNyQyxJQUFHLElBQUksQ0FBQyxFQUFFLElBQUksV0FBVyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO2FBQUssSUFBRyxJQUFJLENBQUMsRUFBRSxJQUFJLGdCQUFnQixFQUFFO1lBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDOUI7YUFBSyxJQUFHLElBQUksQ0FBQyxFQUFFLElBQUksY0FBYyxFQUFDO1lBQy9CLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQzdCO2FBQUssSUFBRyxJQUFJLENBQUMsRUFBRSxJQUFJLHFCQUFxQixFQUFFO1lBQ3pDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUM7U0FDakM7YUFBSyxJQUFHLElBQUksQ0FBQyxFQUFFLElBQUksMEJBQTBCLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztTQUMvQjthQUFJO1lBQ0gsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IsSUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLElBQU0sTUFBTSxHQUFHLFNBQVMsQ0FBQyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FBQztnQkFBQztnQkFBTyxDQUFDO2dCQUFELGNBQUM7WUFBRCxDQUFDLEFBQVIsSUFBUyxDQUFDO1lBRTNELElBQU0sU0FBUyxvQkFBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzFDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQzlDLElBQU0sS0FBSyxHQUFHO2dCQUNaLElBQUksRUFBRSxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDbEIsU0FBUyxFQUFFLE1BQU07YUFDbEIsQ0FBQztZQUNGLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDckI7SUFDSCxDQUFDO0lBRUQsOENBQW1CLEdBQW5CO1FBQUEsaUJBVUM7UUFUQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxTQUFTLENBQUMsQ0FBQTtRQUMzQiw2QkFBNkI7UUFDN0IsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGVBQWUsQ0FBQztRQUM3RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLGVBQWUsQ0FBQztRQUNuQyw4Q0FBOEM7UUFDOUMsVUFBVSxDQUFDO1lBQ1QsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUE7UUFDM0IsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ1YsQ0FBQzs7Z0JBOUxGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsU0FBUztvQkFDbkIsbXRGQUF1QztvQkFFdkMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN0Qzs7OztnQkFoQmtCLFFBQVE7Z0JBQ0osTUFBTTtnQkFMcEIsY0FBYztnQkFPZCxjQUFjO2dCQUxkLFVBQVU7Z0JBR1ksY0FBYztnQkFNcEMsYUFBYTtnREEyQ2pCLE1BQU0sU0FBQyxVQUFVOztJQXdKdEIsdUJBQUM7Q0FBQSxBQS9MRCxJQStMQztTQXpMWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uLCBWaWV3Q2hpbGQsIEluamVjdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWxcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5cclxuaW1wb3J0IHsgTmdNb2R1bGUsIENvbXBpbGVyIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlLCBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBDb250ZW50TW9kdWxlIH0gZnJvbSBcIi4vY29udGVudC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tIFwiLi9jb250ZW50LnNlcnZpY2VcIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqcy9TdWJqZWN0XCI7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyLnNlcnZpY2UnO1xyXG4vLyBpbXBvcnQge1JFUE9SVF9NQU5BR0VNRU5UX1JPVVRFfSBmcm9tICcuLi9hdm0vcmVwb3J0LW1hbmFnZW1lbnQtcm91dGluZy5tb2R1bGUnO1xyXG4vLyBpbXBvcnQgeyBuYXZpZ2F0aW9uIH0gZnJvbSBcIi4vLi4vbmF2aWdhdGlvbi9uYXZpZ2F0aW9uXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJjb250ZW50XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9jb250ZW50LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2NvbnRlbnQuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGVudENvbXBvbmVudCB7XHJcbiAgY3VycmVudFJvdXRlSUQ6IGFueTtcclxuXHJcbiAgZW5hYmxlSGVhZGluZzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHRhYmxlSWQ6IGFueTtcclxuICBjdXJyZW50RGF0YTogYW55O1xyXG5cclxuICBlbmFibGVSdW5SZXBvcnRMYXlvdXQ6IGJvb2xlYW47XHJcbiAgZW5hYmxlUnVuUmVwb3J0OiBib29sZWFuO1xyXG4gIGVuYWJsZUFWTSA6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBlbmFibGVBVk1EZXRhaWxzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZW5hYmxlQVZNVXNlciA6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBlbmFibGVDb25maWdUcmFja2VyIDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGVuYWJsZUNvbmZpZ1NldHVwIDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGVuYWJsZUxpYlNldHVwIDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGdyaWRMaXN0RGF0YTogYW55O1xyXG4gIGN1cnJlbnRDb25maWdEYXRhIDogYW55O1xyXG4gIGVuYWJsZVJvdXRpbmcgOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gIHByaXZhdGUgdW5zdWJzY3JpYmUgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xyXG4gIHByaXZhdGUgdW5zdWJzY3JpYmVSb3V0aW5nID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqL1xyXG4gIGRhdGE6IGFueTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgY29tcGlsZXI6IENvbXBpbGVyLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXHJcbiAgICBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIEBJbmplY3QoXCJtZXRhRGF0YVwiKSBwcml2YXRlIF9tZXRhRGF0YVxyXG4gICkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+XCIsIHRoaXMuX21ldGFEYXRhKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2VcclxuICAgICAgLmdldFJvdXRpbmcoKVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZSkpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIi4uLi4uZGF0YVwiKTtcclxuICAgICAgICBkYXRhID0gZGF0YS5kYXRhO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlSGVhZGluZyA9IGZhbHNlO1xyXG4gICAgICAgIGlmIChkYXRhKSB7XHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRSb3V0ZUlEID0gZGF0YS5pZDtcclxuICAgICAgICAgIHRoaXMuZGF0YSA9IGRhdGE7XHJcbiAgICAgICAgICB0aGlzLmxvYWRSb3V0aW5nKHRoaXMuZGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2UuZ2V0Um91dGluZ01lc3NhZ2UoKVxyXG4gICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmVSb3V0aW5nKSlcclxuICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGRhdGEsXCI+Pj4gREFUQVwiKVxyXG4gICAgICBkYXRhID0gZGF0YS5wYWdlRGF0YTtcclxuICAgICAgdGhpcy5lbmFibGVIZWFkaW5nID0gZmFsc2U7XHJcbiAgICAgIGxldCBoaWRlVmlldyA9IGRhdGEuaGlkZVZpZXc7XHJcbiAgICAgIGlmKGhpZGVWaWV3LmluZGV4T2YoJ25ld1RhYmxlTGF5b3V0JykgPj0wKXtcclxuICAgICAgICB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgaWYoaGlkZVZpZXcuaW5kZXhPZignZm9ybUxheW91dCcpID49MCl7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50RGF0YS5lbmFibGVGb3JtTGF5b3V0ID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5sb2FkUGFnZVJlZGlyZWN0aW9uKCk7XHJcbiAgICB9KVxyXG4gIH1cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMucm91dGUucGFyYW1zLnN1YnNjcmliZSh4ID0+IHtcclxuICAgICAgaWYgKF8uaXNFbXB0eSh4KSkge1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkUm91dGluZyh0aGlzLmdldE1ldGFEYXRhKClbMF0pO1xyXG4gICAgICAgIH0sIDE1MDApO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgZ2V0TWV0YURhdGEoKSB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLl9tZXRhRGF0YSk7XHJcbiAgICBsZXQgbmF2aWdhdGlvbkxpc3QgPSBbXTtcclxuXHJcbiAgICBfLmZvckVhY2godGhpcy5fbWV0YURhdGEsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgdmFyIGNoaWxkcmVuTGlzdCA9IFtdO1xyXG4gICAgICBpZiAoaXRlbS5zdWJNZW51ICYmIGl0ZW0uc3ViTWVudS5sZW5ndGgpIHtcclxuICAgICAgICBfLmZvckVhY2goaXRlbS5zdWJNZW51LCBmdW5jdGlvbihtZW51SXRlbSkge1xyXG4gICAgICAgICAgbGV0IHRlbXBJdGVtID0ge1xyXG4gICAgICAgICAgICBpZDogbWVudUl0ZW0ubWVudU5hbWUsXHJcbiAgICAgICAgICAgIHRpdGxlOiBtZW51SXRlbS50aXRsZSxcclxuICAgICAgICAgICAgdHlwZTpcclxuICAgICAgICAgICAgICBtZW51SXRlbS5zdWJNZW51ICYmIG1lbnVJdGVtLnN1Yk1lbnUubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICA/IFwiY29sbGFwc2FibGVcIlxyXG4gICAgICAgICAgICAgICAgOiBcIml0ZW1cIixcclxuICAgICAgICAgICAgdXJsOiBtZW51SXRlbS5yb3V0aW5nVUksXHJcblxyXG4gICAgICAgICAgICBjb25maWdGaWxlUGF0aDogbWVudUl0ZW0uY29uZmlnRmlsZVBhdGhcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICBjaGlsZHJlbkxpc3QucHVzaCh0ZW1wSXRlbSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGxldCB0ZW1wID0ge1xyXG4gICAgICAgIGlkOiBpdGVtLm1lbnVOYW1lLFxyXG4gICAgICAgIHRpdGxlOiBpdGVtLnRpdGxlLFxyXG4gICAgICAgIHR5cGU6IGl0ZW0uc3ViTWVudSAmJiBpdGVtLnN1Yk1lbnUubGVuZ3RoID8gXCJjb2xsYXBzYWJsZVwiIDogXCJpdGVtXCIsXHJcbiAgICAgICAgdHJhbnNsYXRlOiBpdGVtLnRyYW5zbGF0ZSxcclxuICAgICAgICBpY29uOiBpdGVtLmljb24sXHJcbiAgICAgICAgY2hpbGRyZW46IGNoaWxkcmVuTGlzdCxcclxuICAgICAgICBjb25maWdGaWxlUGF0aDogaXRlbS5jb25maWdGaWxlUGF0aFxyXG4gICAgICB9O1xyXG4gICAgICBuYXZpZ2F0aW9uTGlzdC5wdXNoKHRlbXApO1xyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIG5hdmlnYXRpb25MaXN0O1xyXG4gIH1cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMudW5zdWJzY3JpYmUubmV4dCgpO1xyXG4gICAgdGhpcy51bnN1YnNjcmliZVJvdXRpbmcubmV4dCgpO1xyXG4gIH1cclxuICByZWNlaXZlQ2xpY2soZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcIi4uLi5ldmVudFwiKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZEJ1dHRvbkVuYWJsZU1lc3NhZ2UoXCJkYXRhXCIpO1xyXG4gIH1cclxuICBsb2FkRGF0YShkYXRhKSB7XHJcbiAgICBjb25zb2xlLmxvZyhkYXRhLCBcIi4uLi4uLkRBVEFEQVRBQVwiKTtcclxuICAgIHRoaXMuZW5hYmxlUm91dGluZyA9IGZhbHNlO1xyXG4gICAgdGhpcy5odHRwQ2xpZW50LmdldChkYXRhLmNvbmZpZ0ZpbGVQYXRoKS5zdWJzY3JpYmUoZmlsZVJlc3BvbnNlID0+IHtcclxuICAgICAgaWYgKGZpbGVSZXNwb25zZSlcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIsIEpTT04uc3RyaW5naWZ5KGZpbGVSZXNwb25zZSkpO1xyXG4gICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gZmlsZVJlc3BvbnNlO1xyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhID0gZmlsZVJlc3BvbnNlID8gZmlsZVJlc3BvbnNlW1wibGlzdFZpZXdcIl0gOiB7fTtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50RGF0YSwgXCIuLi4uLmN1cnJlbnREYXRhXCIpO1xyXG4gICAgICBpZih0aGlzLmN1cnJlbnREYXRhICYmIHRoaXMuY3VycmVudERhdGEucHJvZHVjdE5hbWUpe1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwicHJvZHVjdE5hbWVcIiwgdGhpcy5jdXJyZW50RGF0YS5wcm9kdWN0TmFtZSk7XHJcbiAgICAgIH1cclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5lbmFibGVIZWFkaW5nID0gdHJ1ZVxyXG4gICAgICB9LCAxMDAwKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbG9hZFJvdXRpbmcoZGF0YSkge1xyXG4gICAgdGhpcy5lbmFibGVBVk0gPSBmYWxzZTtcclxuICAgIHRoaXMuZW5hYmxlQVZNRGV0YWlscyA9IGZhbHNlO1xyXG4gICAgdGhpcy5lbmFibGVBVk1Vc2VyID0gZmFsc2U7XHJcbiAgICB0aGlzLmVuYWJsZUNvbmZpZ1RyYWNrZXIgPSBmYWxzZTtcclxuICAgIHRoaXMuZW5hYmxlQ29uZmlnU2V0dXAgPSBmYWxzZTtcclxuICAgIHRoaXMuZW5hYmxlTGliU2V0dXAgPSBmYWxzZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMucm91dGVyLFwiPj4+PnJvdXRlclwiKVxyXG4gICAgaWYoZGF0YS5pZCA9PSBcIm1hbmFnZUF2bVwiKSB7XHJcbiAgICAgIHRoaXMuZW5hYmxlQVZNID0gdHJ1ZTtcclxuICAgIH1lbHNlIGlmKGRhdGEuaWQgPT0gJ21hbmFnZUF2RGV0YWlsJykge1xyXG4gICAgICB0aGlzLmVuYWJsZUFWTURldGFpbHMgPSB0cnVlO1xyXG4gICAgfWVsc2UgaWYoZGF0YS5pZCA9PSAnbWFuYWdlQXZVc2VyJyl7XHJcbiAgICAgICAgdGhpcy5lbmFibGVBVk1Vc2VyID0gdHJ1ZTtcclxuICAgIH1lbHNlIGlmKGRhdGEuaWQgPT0gJ21hbmFnZUNvbmZpZ1RyYWNrZXInKSB7XHJcbiAgICAgIHRoaXMuZW5hYmxlQ29uZmlnVHJhY2tlciA9IHRydWU7XHJcbiAgICB9ZWxzZSBpZihkYXRhLmlkID09ICdtYW5hZ2VDb25maWdUcmFja2VyU2V0dXAnICl7XHJcbiAgICAgIHRoaXMuZW5hYmxlQ29uZmlnU2V0dXAgPSB0cnVlO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIHRoaXMuZW5hYmxlTGliU2V0dXAgPSB0cnVlO1xyXG4gICAgICBjb25zdCB0ZW1wbGF0ZSA9IFwiXCI7XHJcbiAgICAgIGNvbnN0IHRtcENtcCA9IENvbXBvbmVudCh7IHRlbXBsYXRlOiB0ZW1wbGF0ZSB9KShjbGFzcyB7fSk7XHJcbiAgXHJcbiAgICAgIGNvbnN0IGFwcFJvdXRlcyA9IFsuLi50aGlzLnJvdXRlci5jb25maWddO1xyXG4gICAgICBsZXQgdXJsID0gZGF0YS51cmwgPyBkYXRhLnVybCA6IFwiL1wiICsgZGF0YS5pZDtcclxuICAgICAgY29uc3Qgcm91dGUgPSB7XHJcbiAgICAgICAgcGF0aDogdXJsLnNsaWNlKDEpLFxyXG4gICAgICAgIGNvbXBvbmVudDogdG1wQ21wXHJcbiAgICAgIH07XHJcbiAgICAgIGFwcFJvdXRlcy5wdXNoKHJvdXRlKTtcclxuICAgICAgdGhpcy5yb3V0ZXIucmVzZXRDb25maWcoYXBwUm91dGVzKTtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybChkYXRhLnVybCk7XHJcbiAgICAgIHRoaXMubG9hZERhdGEoZGF0YSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBsb2FkUGFnZVJlZGlyZWN0aW9uKCkge1xyXG4gICAgY29uc29sZS5sb2codGhpcyxcIj4+IFRISVNcIilcclxuICAgIC8vIHRoaXMuZW5hYmxlSGVhZGluZyA9IHRydWU7XHJcbiAgICBsZXQgY3VycmVudFBhZ2VMb2FkID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5wYWdlUm91dGluZ1ZpZXc7XHJcbiAgICB0aGlzLmVuYWJsZVJvdXRpbmcgPSB0cnVlO1xyXG4gICAgdGhpcy5jdXJyZW50RGF0YSA9IGN1cnJlbnRQYWdlTG9hZDtcclxuICAgIC8vIHRoaXMuY3VycmVudERhdGEuZW5hYmxlQnV0dG9uTGF5b3V0ID0gdHJ1ZTtcclxuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICB0aGlzLmVuYWJsZUhlYWRpbmcgPSB0cnVlXHJcbiAgICB9LCAyMDApO1xyXG4gIH1cclxufVxyXG4iXX0=