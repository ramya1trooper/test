import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import "rxjs/Rx";
import { map } from "rxjs/operators";
import { LoaderService } from '../loader.service';
import { MessageService } from '../_services';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../loader.service";
import * as i3 from "../_services/message.service";
// import { MessageService } from "../_services/index";
var ContentService = /** @class */ (function () {
    function ContentService(httpClient, loaderService, messageService, environment) {
        this.httpClient = httpClient;
        this.loaderService = loaderService;
        this.messageService = messageService;
        this.environment = environment;
        this.baseURL = this.environment.baseUrl;
        console.log(">>>>>>>>>>>>environment", this.environment);
    }
    ContentService.prototype.getAllReponse = function (query, apiUrl) {
        var url = this.baseURL + apiUrl;
        // this.messageService.sendTriggerNotification({data : "trigger"})
        return this.httpClient.get(url, { params: query });
    };
    ContentService.prototype.getResponse = function (query, apiUrl) {
        var url = this.baseURL + apiUrl;
        // this.messageService.sendTriggerNotification({data : "trigger"})
        return this.httpClient.get(url + query);
    };
    ContentService.prototype.getExportResponse = function (query, apiUrl) {
        var url = this.baseURL + apiUrl;
        return this.httpClient.get(url, {
            params: query,
            responseType: "text"
        });
    };
    ContentService.prototype.getS3Response = function (query, apiUrl) {
        var url = this.baseURL + apiUrl;
        // return this.httpClient.get<any>(url, {
        //   params: query,
        //   responseType: "text" as "json"
        // });
        return this.httpClient.get(url, { observe: 'response', responseType: 'blob' });
    };
    ContentService.prototype.createRequest = function (data, apiUrl) {
        var _this = this;
        var url = this.baseURL + apiUrl;
        this.loaderService.startLoader();
        return this.httpClient
            .post(url, data, { observe: "response" })
            .pipe(map(function (resp) {
            _this.loaderService.stopLoader();
            _this.messageService.sendTriggerNotification({ data: "trigger" });
            return resp;
        }));
    };
    ContentService.prototype.updateRequest = function (data, apiUrl, id) {
        var _this = this;
        var url = this.baseURL + apiUrl + id;
        this.loaderService.startLoader();
        return this.httpClient
            .put(url, data, { observe: "response" })
            .pipe(map(function (resp) {
            _this.loaderService.stopLoader();
            _this.messageService.sendTriggerNotification({ data: "trigger" });
            return resp;
        }));
    };
    ContentService.prototype.avmImport = function (data, file, apiUrl) {
        var _this = this;
        var formData = new FormData();
        Object.keys(data).map(function (key) {
            formData.append(key, data[key]);
        });
        formData.append('file', file);
        console.log('file >>', file);
        return this.httpClient.post("" + this.baseURL + apiUrl, formData, { observe: 'response' })
            .pipe(map(function (resp) {
            _this.messageService.sendTriggerNotification({ data: "trigger" });
            return resp;
        }));
    };
    ContentService.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    ContentService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: LoaderService },
        { type: MessageService },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
    ]; };
    ContentService.ngInjectableDef = i0.defineInjectable({ factory: function ContentService_Factory() { return new ContentService(i0.inject(i1.HttpClient), i0.inject(i2.LoaderService), i0.inject(i3.MessageService), i0.inject("environment")); }, token: ContentService, providedIn: "root" });
    return ContentService;
}());
export { ContentService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNvbnRlbnQvY29udGVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLFNBQVMsQ0FBQztBQUNqQixPQUFPLEVBQUUsR0FBRyxFQUFjLE1BQU0sZ0JBQWdCLENBQUM7QUFDakQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxjQUFjLENBQUE7Ozs7O0FBQzdDLHVEQUF1RDtBQUV2RDtJQU1FLHdCQUNVLFVBQXNCLEVBQ3RCLGFBQTJCLEVBQzNCLGNBQStCLEVBQ1IsV0FBVztRQUhsQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGtCQUFhLEdBQWIsYUFBYSxDQUFjO1FBQzNCLG1CQUFjLEdBQWQsY0FBYyxDQUFpQjtRQUNSLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBRTFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxLQUFVLEVBQUUsTUFBVztRQUNuQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUNoQyxrRUFBa0U7UUFDbEUsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBTSxHQUFHLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsb0NBQVcsR0FBWCxVQUFZLEtBQVUsRUFBRSxNQUFXO1FBQ2pDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ2hDLGtFQUFrRTtRQUNsRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFNLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBQ0QsMENBQWlCLEdBQWpCLFVBQWtCLEtBQVUsRUFBRSxNQUFXO1FBQ3ZDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQU0sR0FBRyxFQUFFO1lBQ25DLE1BQU0sRUFBRSxLQUFLO1lBQ2IsWUFBWSxFQUFFLE1BQWdCO1NBQy9CLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxzQ0FBYSxHQUFiLFVBQWMsS0FBVSxFQUFFLE1BQVc7UUFDbkMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDaEMseUNBQXlDO1FBQ3pDLG1CQUFtQjtRQUNuQixtQ0FBbUM7UUFDbkMsTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsTUFBZ0IsRUFBRSxDQUFDLENBQUM7SUFFM0YsQ0FBQztJQUVELHNDQUFhLEdBQWIsVUFBYyxJQUFTLEVBQUUsTUFBVztRQUFwQyxpQkFhQztRQVpDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsVUFBVTthQUNuQixJQUFJLENBQU0sR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQzthQUM3QyxJQUFJLENBQ0gsR0FBRyxDQUFDLFVBQUEsSUFBSTtZQUNOLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDaEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFDLElBQUksRUFBRyxTQUFTLEVBQUMsQ0FBQyxDQUFBO1lBQy9ELE9BQU8sSUFBSSxDQUFDO1FBRWQsQ0FBQyxDQUFDLENBQ0gsQ0FBQztJQUNOLENBQUM7SUFFRCxzQ0FBYSxHQUFiLFVBQWMsSUFBUyxFQUFFLE1BQVcsRUFBRSxFQUFFO1FBQXhDLGlCQVlDO1FBWEMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsVUFBVTthQUNuQixHQUFHLENBQU0sR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQzthQUM1QyxJQUFJLENBQ0gsR0FBRyxDQUFDLFVBQUEsSUFBSTtZQUNOLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDaEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFDLElBQUksRUFBRyxTQUFTLEVBQUMsQ0FBQyxDQUFBO1lBQy9ELE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxDQUFDLENBQ0gsQ0FBQTtJQUNMLENBQUM7SUFDRCxrQ0FBUyxHQUFULFVBQVUsSUFBUyxFQUFFLElBQVMsRUFBRSxNQUFZO1FBQTVDLGlCQWNBO1FBYkEsSUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7WUFDekIsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDakMsQ0FBQyxDQUFDLENBQUM7UUFDSCxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBQyxJQUFJLENBQUMsQ0FBQTtRQUMzQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFNLEtBQUcsSUFBSSxDQUFDLE9BQVMsR0FBRyxNQUFNLEVBQUUsUUFBUSxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDO2FBQzdGLElBQUksQ0FDSixHQUFHLENBQUMsVUFBQSxJQUFJO1lBQ0YsS0FBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFDLElBQUksRUFBRyxTQUFTLEVBQUMsQ0FBQyxDQUFBO1lBQ3BFLE9BQU8sSUFBSSxDQUFDO1FBQ2IsQ0FBQyxDQUFDLENBQ0YsQ0FBQTtJQUNILENBQUM7O2dCQXRGRCxVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQVRRLFVBQVU7Z0JBR1YsYUFBYTtnQkFDYixjQUFjO2dEQWFsQixNQUFNLFNBQUMsYUFBYTs7O3lCQW5CekI7Q0FnR0MsQUF2RkQsSUF1RkM7U0FwRlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gXCJyeGpzL1wiO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCBcInJ4anMvUnhcIjtcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSAnLi4vX3NlcnZpY2VzJ1xyXG4vLyBpbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLi9fc2VydmljZXMvaW5kZXhcIjtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiBcInJvb3RcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGVudFNlcnZpY2Uge1xyXG4gIGJhc2VVUkw6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6TG9hZGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2UgOiBNZXNzYWdlU2VydmljZSxcclxuICAgIEBJbmplY3QoXCJlbnZpcm9ubWVudFwiKSBwcml2YXRlIGVudmlyb25tZW50XHJcbiAgKSB7XHJcbiAgICB0aGlzLmJhc2VVUkwgPSB0aGlzLmVudmlyb25tZW50LmJhc2VVcmw7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+PmVudmlyb25tZW50XCIsIHRoaXMuZW52aXJvbm1lbnQpO1xyXG4gIH1cclxuXHJcbiAgZ2V0QWxsUmVwb25zZShxdWVyeTogYW55LCBhcGlVcmw6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBsZXQgdXJsID0gdGhpcy5iYXNlVVJMICsgYXBpVXJsO1xyXG4gICAgLy8gdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kVHJpZ2dlck5vdGlmaWNhdGlvbih7ZGF0YSA6IFwidHJpZ2dlclwifSlcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PGFueT4odXJsLCB7IHBhcmFtczogcXVlcnkgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRSZXNwb25zZShxdWVyeTogYW55LCBhcGlVcmw6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBsZXQgdXJsID0gdGhpcy5iYXNlVVJMICsgYXBpVXJsO1xyXG4gICAgLy8gdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kVHJpZ2dlck5vdGlmaWNhdGlvbih7ZGF0YSA6IFwidHJpZ2dlclwifSlcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PGFueT4odXJsICsgcXVlcnkpO1xyXG4gIH1cclxuICBnZXRFeHBvcnRSZXNwb25zZShxdWVyeTogYW55LCBhcGlVcmw6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBsZXQgdXJsID0gdGhpcy5iYXNlVVJMICsgYXBpVXJsO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQ8YW55Pih1cmwsIHtcclxuICAgICAgcGFyYW1zOiBxdWVyeSxcclxuICAgICAgcmVzcG9uc2VUeXBlOiBcInRleHRcIiBhcyBcImpzb25cIlxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIGdldFMzUmVzcG9uc2UocXVlcnk6IGFueSwgYXBpVXJsOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMuYmFzZVVSTCArIGFwaVVybDtcclxuICAgIC8vIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PGFueT4odXJsLCB7XHJcbiAgICAvLyAgIHBhcmFtczogcXVlcnksXHJcbiAgICAvLyAgIHJlc3BvbnNlVHlwZTogXCJ0ZXh0XCIgYXMgXCJqc29uXCJcclxuICAgIC8vIH0pO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudC5nZXQodXJsLCB7IG9ic2VydmU6ICdyZXNwb25zZScsIHJlc3BvbnNlVHlwZTogJ2Jsb2InIGFzICdqc29uJyB9KTtcclxuXHJcbiAgfVxyXG5cclxuICBjcmVhdGVSZXF1ZXN0KGRhdGE6IGFueSwgYXBpVXJsOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMuYmFzZVVSTCArIGFwaVVybDtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudFxyXG4gICAgICAucG9zdDxhbnk+KHVybCwgZGF0YSwgeyBvYnNlcnZlOiBcInJlc3BvbnNlXCIgfSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgbWFwKHJlc3AgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTsgICBcclxuICAgICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRyaWdnZXJOb3RpZmljYXRpb24oe2RhdGEgOiBcInRyaWdnZXJcIn0pXHJcbiAgICAgICAgICByZXR1cm4gcmVzcDtcclxuICAgICAgICAgICBcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlUmVxdWVzdChkYXRhOiBhbnksIGFwaVVybDogYW55LCBpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICBsZXQgdXJsID0gdGhpcy5iYXNlVVJMICsgYXBpVXJsICsgaWQ7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnRcclxuICAgICAgLnB1dDxhbnk+KHVybCwgZGF0YSwgeyBvYnNlcnZlOiBcInJlc3BvbnNlXCIgfSlcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgbWFwKHJlc3AgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTsgICBcclxuICAgICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRyaWdnZXJOb3RpZmljYXRpb24oe2RhdGEgOiBcInRyaWdnZXJcIn0pXHJcbiAgICAgICAgICByZXR1cm4gcmVzcDtcclxuICAgICAgICB9KVxyXG4gICAgICApXHJcbiAgfVxyXG4gIGF2bUltcG9ydChkYXRhOiBhbnksIGZpbGU6IGFueSwgYXBpVXJsIDogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHZhciBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG5cdFx0T2JqZWN0LmtleXMoZGF0YSkubWFwKChrZXkpID0+IHtcclxuXHRcdFx0Zm9ybURhdGEuYXBwZW5kKGtleSwgZGF0YVtrZXldKTtcclxuXHRcdH0pO1xyXG5cdFx0Zm9ybURhdGEuYXBwZW5kKCdmaWxlJywgZmlsZSk7XHJcblx0XHRjb25zb2xlLmxvZygnZmlsZSA+PicsZmlsZSlcclxuXHRcdHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxhbnk+KGAke3RoaXMuYmFzZVVSTH1gICsgYXBpVXJsLCBmb3JtRGF0YSwgeyBvYnNlcnZlOiAncmVzcG9uc2UnIH0pXHJcblx0XHRcdC5waXBlKFxyXG5cdFx0XHRcdG1hcChyZXNwID0+IHtcclxuICAgICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRyaWdnZXJOb3RpZmljYXRpb24oe2RhdGEgOiBcInRyaWdnZXJcIn0pXHJcblx0XHRcdFx0XHRyZXR1cm4gcmVzcDtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHQpXHJcblx0fVxyXG59XHJcbiJdfQ==