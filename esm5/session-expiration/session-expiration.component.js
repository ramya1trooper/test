import { Component, HostListener } from '@angular/core';
import { SessionService } from './session-expiration.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, timer } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AuthModelDialog } from './session-expiration-alert.component';
var SessionComponent = /** @class */ (function () {
    function SessionComponent(authService, dialog, oauthService) {
        var _this = this;
        this.authService = authService;
        this.dialog = dialog;
        this.oauthService = oauthService;
        this.minutesDisplay = 0;
        this.secondsDisplay = 0;
        this.endTime = 20; // 20 - 20Mins;
        this.unsubscribe$ = new Subject();
        this.appParentMessage = 'This message is from parent';
        this.subscription = this.authService
            .userActionOccured()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(function () {
            if (_this.timerSubscription) {
                _this.timerSubscription.unsubscribe();
            }
            _this.resetTimer();
        });
    }
    SessionComponent.prototype.resetTimerfn = function () {
        this.authService.notifyUserAction();
    };
    SessionComponent.prototype.ngOnInit = function () {
        this.resetTimer();
    };
    SessionComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(AuthModelDialog, {
            width: '40%',
            data: {
                minutesDisplay: this.minutesDisplay,
                secondsDisplay: this.secondsDisplay
            }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            console.log("Dialog result:");
            _this.authService.notifyUserAction();
        });
    };
    SessionComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    };
    SessionComponent.prototype.resetTimer = function (endTime) {
        var _this = this;
        if (endTime === void 0) { endTime = this.endTime; }
        // console.log('######');
        var interval = 1000; //1000
        var duration = endTime * 60;
        this.timerSubscription = timer(0, interval)
            .pipe(take(duration))
            .subscribe(function (value) { return _this.render((duration - +value) * interval); }, function (err) { }, function () {
            _this.authService.logOutUser();
        });
    };
    SessionComponent.prototype.render = function (count) {
        this.secondsDisplay = this.getSeconds(count);
        this.minutesDisplay = this.getMinutes(count);
        if (this.minutesDisplay == 1 && this.secondsDisplay == 59) {
            this.openDialog();
        }
    };
    SessionComponent.prototype.getSeconds = function (ticks) {
        var seconds = ((ticks % 60000) / 1000).toFixed(0);
        return this.pad(seconds);
    };
    SessionComponent.prototype.getMinutes = function (ticks) {
        var minutes = Math.floor(ticks / 60000);
        return this.pad(minutes);
    };
    SessionComponent.prototype.pad = function (digit) {
        return digit <= 9 ? '0' + digit : digit;
    };
    SessionComponent.decorators = [
        { type: Component, args: [{
                    selector: 'session-expiration',
                    template: "\n    <h1></h1>\n  ",
                    providers: [SessionService]
                }] }
    ];
    /** @nocollapse */
    SessionComponent.ctorParameters = function () { return [
        { type: SessionService },
        { type: MatDialog },
        { type: OAuthService }
    ]; };
    SessionComponent.propDecorators = {
        resetTimerfn: [{ type: HostListener, args: ['document:keyup', [],] }, { type: HostListener, args: ['document:click', [],] }, { type: HostListener, args: ['document:wheel', [],] }]
    };
    return SessionComponent;
}());
export { SessionComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQU9ULFlBQVksRUFDYixNQUFNLGVBQWUsQ0FBQztBQUl2QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUNMLFlBQVksRUFHYixNQUFNLHFCQUFxQixDQUFDO0FBQzdCLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWpELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVFyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFFdkU7SUFrQkUsMEJBQ1UsV0FBMkIsRUFDNUIsTUFBaUIsRUFDaEIsWUFBMEI7UUFIcEMsaUJBY0M7UUFiUyxnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDNUIsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNoQixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQVhwQyxtQkFBYyxHQUFHLENBQUMsQ0FBQztRQUNuQixtQkFBYyxHQUFHLENBQUMsQ0FBQztRQUNuQixZQUFPLEdBQUcsRUFBRSxDQUFDLENBQUMsZUFBZTtRQUU3QixpQkFBWSxHQUFrQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBRTVDLHFCQUFnQixHQUFHLDZCQUE2QixDQUFDO1FBTy9DLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVc7YUFDakMsaUJBQWlCLEVBQUU7YUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDbEMsU0FBUyxDQUFDO1lBQ1QsSUFBSSxLQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQzFCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUN0QztZQUNELEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFJRCx1Q0FBWSxHQUhaO1FBSUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFDRCxtQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxxQ0FBVSxHQUFWO1FBQUEsaUJBYUM7UUFaQyxJQUFNLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDbEQsS0FBSyxFQUFFLEtBQUs7WUFDWixJQUFJLEVBQUU7Z0JBQ0osY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO2dCQUNuQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7YUFDcEM7U0FDRixDQUFDLENBQUM7UUFFSCxTQUFTLENBQUMsV0FBVyxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsTUFBTTtZQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHNDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELHFDQUFVLEdBQVYsVUFBVyxPQUE4QjtRQUF6QyxpQkFjQztRQWRVLHdCQUFBLEVBQUEsVUFBa0IsSUFBSSxDQUFDLE9BQU87UUFDdkMseUJBQXlCO1FBQ3pCLElBQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLE1BQU07UUFFN0IsSUFBTSxRQUFRLEdBQUcsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUM7YUFDeEMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNwQixTQUFTLENBQ1IsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsUUFBUSxDQUFDLEVBQTNDLENBQTJDLEVBQ3BELFVBQUEsR0FBRyxJQUFLLENBQUMsRUFDVDtZQUNFLEtBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDaEMsQ0FBQyxDQUNGLENBQUM7SUFDTixDQUFDO0lBRU8saUNBQU0sR0FBZCxVQUFlLEtBQWM7UUFDM0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QyxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksRUFBRSxFQUFFO1lBQ3pELElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUNuQjtJQUNILENBQUM7SUFFTyxxQ0FBVSxHQUFsQixVQUFtQixLQUFhO1FBQzlCLElBQU0sT0FBTyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU8scUNBQVUsR0FBbEIsVUFBbUIsS0FBYTtRQUM5QixJQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQztRQUMxQyxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0IsQ0FBQztJQUVPLDhCQUFHLEdBQVgsVUFBWSxLQUFVO1FBQ3BCLE9BQU8sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzFDLENBQUM7O2dCQW5HRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsUUFBUSxFQUFFLHFCQUVUO29CQUVELFNBQVMsRUFBRSxDQUFDLGNBQWMsQ0FBQztpQkFDNUI7Ozs7Z0JBMUJRLGNBQWM7Z0JBU2QsU0FBUztnQkFQaEIsWUFBWTs7OytCQWtEWCxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxjQUNqQyxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxjQUNqQyxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRTs7SUFpRXBDLHVCQUFDO0NBQUEsQUFwR0QsSUFvR0M7U0EzRlksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25EZXN0cm95LFxyXG4gIE9uSW5pdCxcclxuICBJbmplY3QsXHJcbiAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgVmlld0NoaWxkLFxyXG4gIEFmdGVyVmlld0luaXQsXHJcbiAgSG9zdExpc3RlbmVyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBTZXNzaW9uU2VydmljZSB9IGZyb20gJy4vc2Vzc2lvbi1leHBpcmF0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQge1xyXG4gIE9BdXRoU2VydmljZSxcclxuICBBdXRoQ29uZmlnLFxyXG4gIE51bGxWYWxpZGF0aW9uSGFuZGxlclxyXG59IGZyb20gJ2FuZ3VsYXItb2F1dGgyLW9pZGMnO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCB0aW1lciwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCwgdGFrZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7XHJcbiAgTWF0RGlhbG9nUmVmLFxyXG4gIE1hdENoaXBJbnB1dEV2ZW50LFxyXG4gIE1hdEF1dG9jb21wbGV0ZSxcclxuICBNQVRfRElBTE9HX0RBVEEsXHJcbiAgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudFxyXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQXV0aE1vZGVsRGlhbG9nIH0gZnJvbSAnLi9zZXNzaW9uLWV4cGlyYXRpb24tYWxlcnQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnc2Vzc2lvbi1leHBpcmF0aW9uJyxcclxuICB0ZW1wbGF0ZTogYFxyXG4gICAgPGgxPjwvaDE+XHJcbiAgYCxcclxuICBzdHlsZXM6IFtdLFxyXG4gIHByb3ZpZGVyczogW1Nlc3Npb25TZXJ2aWNlXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNlc3Npb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3ksIE9uSW5pdCB7XHJcbiAgbWludXRlc0Rpc3BsYXkgPSAwO1xyXG4gIHNlY29uZHNEaXNwbGF5ID0gMDtcclxuICBlbmRUaW1lID0gMjA7IC8vIDIwIC0gMjBNaW5zO1xyXG5cclxuICB1bnN1YnNjcmliZSQ6IFN1YmplY3Q8dm9pZD4gPSBuZXcgU3ViamVjdCgpO1xyXG4gIHRpbWVyU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgYXBwUGFyZW50TWVzc2FnZSA9ICdUaGlzIG1lc3NhZ2UgaXMgZnJvbSBwYXJlbnQnO1xyXG4gIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBhdXRoU2VydmljZTogU2Vzc2lvblNlcnZpY2UsXHJcbiAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIG9hdXRoU2VydmljZTogT0F1dGhTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMuYXV0aFNlcnZpY2VcclxuICAgICAgLnVzZXJBY3Rpb25PY2N1cmVkKClcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmUkKSlcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMudGltZXJTdWJzY3JpcHRpb24pIHtcclxuICAgICAgICAgIHRoaXMudGltZXJTdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZXNldFRpbWVyKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDprZXl1cCcsIFtdKVxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmNsaWNrJywgW10pXHJcbiAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6d2hlZWwnLCBbXSlcclxuICByZXNldFRpbWVyZm4oKSB7XHJcbiAgICB0aGlzLmF1dGhTZXJ2aWNlLm5vdGlmeVVzZXJBY3Rpb24oKTtcclxuICB9XHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnJlc2V0VGltZXIoKTtcclxuICB9XHJcblxyXG4gIG9wZW5EaWFsb2coKSB7XHJcbiAgICBjb25zdCBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKEF1dGhNb2RlbERpYWxvZywge1xyXG4gICAgICB3aWR0aDogJzQwJScsXHJcbiAgICAgIGRhdGE6IHtcclxuICAgICAgICBtaW51dGVzRGlzcGxheTogdGhpcy5taW51dGVzRGlzcGxheSxcclxuICAgICAgICBzZWNvbmRzRGlzcGxheTogdGhpcy5zZWNvbmRzRGlzcGxheVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICBkaWFsb2dSZWYuYWZ0ZXJDbG9zZWQoKS5zdWJzY3JpYmUocmVzdWx0ID0+IHtcclxuICAgICAgY29uc29sZS5sb2coYERpYWxvZyByZXN1bHQ6YCk7XHJcbiAgICAgIHRoaXMuYXV0aFNlcnZpY2Uubm90aWZ5VXNlckFjdGlvbigpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMudW5zdWJzY3JpYmUkLm5leHQoKTtcclxuICAgIHRoaXMudW5zdWJzY3JpYmUkLmNvbXBsZXRlKCk7XHJcbiAgfVxyXG5cclxuICByZXNldFRpbWVyKGVuZFRpbWU6IG51bWJlciA9IHRoaXMuZW5kVGltZSkge1xyXG4gICAgLy8gY29uc29sZS5sb2coJyMjIyMjIycpO1xyXG4gICAgY29uc3QgaW50ZXJ2YWwgPSAxMDAwOyAvLzEwMDBcclxuXHJcbiAgICBjb25zdCBkdXJhdGlvbiA9IGVuZFRpbWUgKiA2MDtcclxuICAgIHRoaXMudGltZXJTdWJzY3JpcHRpb24gPSB0aW1lcigwLCBpbnRlcnZhbClcclxuICAgICAgLnBpcGUodGFrZShkdXJhdGlvbikpXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgdmFsdWUgPT4gdGhpcy5yZW5kZXIoKGR1cmF0aW9uIC0gK3ZhbHVlKSAqIGludGVydmFsKSxcclxuICAgICAgICBlcnIgPT4ge30sXHJcbiAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5sb2dPdXRVc2VyKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSByZW5kZXIoY291bnQgOiBudW1iZXIpIHtcclxuICAgIHRoaXMuc2Vjb25kc0Rpc3BsYXkgPSB0aGlzLmdldFNlY29uZHMoY291bnQpO1xyXG4gICAgdGhpcy5taW51dGVzRGlzcGxheSA9IHRoaXMuZ2V0TWludXRlcyhjb3VudCk7XHJcbiAgICBpZiAodGhpcy5taW51dGVzRGlzcGxheSA9PSAxICYmIHRoaXMuc2Vjb25kc0Rpc3BsYXkgPT0gNTkpIHtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIGdldFNlY29uZHModGlja3M6IG51bWJlcikge1xyXG4gICAgY29uc3Qgc2Vjb25kcyA9ICgodGlja3MgJSA2MDAwMCkgLyAxMDAwKS50b0ZpeGVkKDApO1xyXG4gICAgcmV0dXJuIHRoaXMucGFkKHNlY29uZHMpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRNaW51dGVzKHRpY2tzOiBudW1iZXIpIHtcclxuICAgIGNvbnN0IG1pbnV0ZXMgPSBNYXRoLmZsb29yKHRpY2tzIC8gNjAwMDApO1xyXG4gICAgcmV0dXJuIHRoaXMucGFkKG1pbnV0ZXMpO1xyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBwYWQoZGlnaXQ6IGFueSkge1xyXG4gICAgcmV0dXJuIGRpZ2l0IDw9IDkgPyAnMCcgKyBkaWdpdCA6IGRpZ2l0O1xyXG4gIH1cclxufVxyXG4iXX0=