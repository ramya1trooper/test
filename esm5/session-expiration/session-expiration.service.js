import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { environment } from '../environments/environment';
import * as i0 from "@angular/core";
import * as i1 from "angular-oauth2-oidc";
import * as i2 from "@angular/common/http";
var SessionService = /** @class */ (function () {
    function SessionService(oauthService, httpClient) {
        this.oauthService = oauthService;
        this.httpClient = httpClient;
        this.baseURL = environment.baseUrl;
        this._userActionOccured = new Subject();
    }
    SessionService.prototype.userActionOccured = function () {
        return this._userActionOccured.asObservable();
    };
    SessionService.prototype.notifyUserAction = function () {
        this._userActionOccured.next();
    };
    SessionService.prototype.loginUser = function () {
        console.log('user login');
    };
    SessionService.prototype.logOutUser = function () {
        console.log('logout...');
        localStorage.clear();
        sessionStorage.clear();
        this.oauthService.logOut(false);
    };
    SessionService.prototype.continueSession = function () {
        console.log("I issue an API request to server.");
    };
    SessionService.prototype.stopSession = function () {
        console.log("I logout.");
    };
    SessionService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SessionService.ctorParameters = function () { return [
        { type: OAuthService },
        { type: HttpClient }
    ]; };
    SessionService.ngInjectableDef = i0.defineInjectable({ factory: function SessionService_Factory() { return new SessionService(i0.inject(i1.OAuthService), i0.inject(i2.HttpClient)); }, token: SessionService, providedIn: "root" });
    return SessionService;
}());
export { SessionService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsic2Vzc2lvbi1leHBpcmF0aW9uL3Nlc3Npb24tZXhwaXJhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxPQUFPLEVBQStCLE1BQU0sTUFBTSxDQUFDO0FBQzVELE9BQU8sRUFBRSxVQUFVLEVBQWdCLE1BQU0sc0JBQXNCLENBQUM7QUFFaEUsT0FBTyxTQUFTLENBQUM7QUFFakIsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDZCQUE2QixDQUFDOzs7O0FBRTFEO0lBVUUsd0JBQ1UsWUFBMEIsRUFDMUIsVUFBc0I7UUFEdEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQU5oQyxZQUFPLEdBQVcsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUV0Qyx1QkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO0lBS3JDLENBQUM7SUFDSiwwQ0FBaUIsR0FBakI7UUFDRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNoRCxDQUFDO0lBQ0QseUNBQWdCLEdBQWhCO1FBQ0UsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRCxrQ0FBUyxHQUFUO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsbUNBQVUsR0FBVjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0NBQWUsR0FBZjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ0Qsb0NBQVcsR0FBWDtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7Z0JBckNGLFVBQVUsU0FDVDtvQkFDQSxVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBWFEsWUFBWTtnQkFFWixVQUFVOzs7eUJBSG5CO0NBK0NDLEFBdENELElBc0NDO1NBakNZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9BdXRoU2VydmljZSB9IGZyb20gJ2FuZ3VsYXItb2F1dGgyLW9pZGMnO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCBPYnNlcnZhYmxlLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCwgSHR0cFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCAncnhqcy9SeCc7XHJcblxyXG5pbXBvcnQgeyBlbnZpcm9ubWVudCB9IGZyb20gJy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudCc7XHJcblxyXG5ASW5qZWN0YWJsZShcclxuICB7XHJcbiAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn1cclxuKVxyXG5leHBvcnQgY2xhc3MgU2Vzc2lvblNlcnZpY2Uge1xyXG4gIGJhc2VVUkw6IHN0cmluZyA9IGVudmlyb25tZW50LmJhc2VVcmw7XHJcblxyXG4gIF91c2VyQWN0aW9uT2NjdXJlZCA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIG9hdXRoU2VydmljZTogT0F1dGhTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50XHJcbiAgKSB7fVxyXG4gIHVzZXJBY3Rpb25PY2N1cmVkKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fdXNlckFjdGlvbk9jY3VyZWQuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG4gIG5vdGlmeVVzZXJBY3Rpb24oKSB7XHJcbiAgICB0aGlzLl91c2VyQWN0aW9uT2NjdXJlZC5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICBsb2dpblVzZXIoKSB7XHJcbiAgICBjb25zb2xlLmxvZygndXNlciBsb2dpbicpO1xyXG4gIH1cclxuXHJcbiAgbG9nT3V0VXNlcigpIHtcclxuICAgIGNvbnNvbGUubG9nKCdsb2dvdXQuLi4nKTtcclxuICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgc2Vzc2lvblN0b3JhZ2UuY2xlYXIoKTtcclxuICAgIHRoaXMub2F1dGhTZXJ2aWNlLmxvZ091dChmYWxzZSk7XHJcbiAgfVxyXG5cclxuICBjb250aW51ZVNlc3Npb24oKSB7XHJcbiAgICBjb25zb2xlLmxvZyhgSSBpc3N1ZSBhbiBBUEkgcmVxdWVzdCB0byBzZXJ2ZXIuYCk7XHJcbiAgfVxyXG4gIHN0b3BTZXNzaW9uKCkge1xyXG4gICAgY29uc29sZS5sb2coYEkgbG9nb3V0LmApO1xyXG4gIH1cclxufVxyXG4iXX0=