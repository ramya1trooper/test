import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { SessionComponent } from './session-expiration.component';
import { SessionService } from './session-expiration.service';
import { AuthModelDialog } from './session-expiration-alert.component';
import { FuseSharedModule } from '../@fuse/shared.module';
var SessionModule = /** @class */ (function () {
    function SessionModule() {
    }
    SessionModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [SessionComponent, AuthModelDialog
                    ],
                    imports: [
                        MatButtonModule,
                        MatCheckboxModule,
                        MatFormFieldModule,
                        MatIconModule,
                        MatInputModule, FuseSharedModule
                    ],
                    exports: [SessionComponent, AuthModelDialog],
                    providers: [SessionService],
                    schemas: [CUSTOM_ELEMENTS_SCHEMA]
                },] }
    ];
    return SessionModule;
}());
export { SessionModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpFLE9BQU8sRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsa0JBQWtCLEVBQUUsYUFBYSxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBR3pILE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFHMUQ7SUFBQTtJQWdCQSxDQUFDOztnQkFoQkEsUUFBUSxTQUFDO29CQUNULFlBQVksRUFBRSxDQUFDLGdCQUFnQixFQUFDLGVBQWU7cUJBRTlDO29CQUNELE9BQU8sRUFBRTt3QkFDUixlQUFlO3dCQUNmLGlCQUFpQjt3QkFDakIsa0JBQWtCO3dCQUNsQixhQUFhO3dCQUNiLGNBQWMsRUFBQyxnQkFBZ0I7cUJBQy9CO29CQUNELE9BQU8sRUFBRSxDQUFDLGdCQUFnQixFQUFDLGVBQWUsQ0FBQztvQkFDM0MsU0FBUyxFQUFFLENBQUMsY0FBYyxDQUFDO29CQUMzQixPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztpQkFDakM7O0lBRUQsb0JBQUM7Q0FBQSxBQWhCRCxJQWdCQztTQURZLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBNYXRCdXR0b25Nb2R1bGUsIE1hdENoZWNrYm94TW9kdWxlLCBNYXRGb3JtRmllbGRNb2R1bGUsIE1hdEljb25Nb2R1bGUsIE1hdElucHV0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuXHJcbiBpbXBvcnQgeyBTZXNzaW9uQ29tcG9uZW50IH0gZnJvbSAnLi9zZXNzaW9uLWV4cGlyYXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2Vzc2lvblNlcnZpY2UgfSBmcm9tICcuL3Nlc3Npb24tZXhwaXJhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aE1vZGVsRGlhbG9nIH0gZnJvbSAnLi9zZXNzaW9uLWV4cGlyYXRpb24tYWxlcnQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gJy4uL0BmdXNlL3NoYXJlZC5tb2R1bGUnO1xyXG5cclxuXHJcbkBOZ01vZHVsZSh7XHJcblx0ZGVjbGFyYXRpb25zOiBbU2Vzc2lvbkNvbXBvbmVudCxBdXRoTW9kZWxEaWFsb2dcclxuXHRcdFxyXG5cdF0sXHJcblx0aW1wb3J0czogW1xyXG5cdFx0TWF0QnV0dG9uTW9kdWxlLFxyXG5cdFx0TWF0Q2hlY2tib3hNb2R1bGUsXHJcblx0XHRNYXRGb3JtRmllbGRNb2R1bGUsXHJcblx0XHRNYXRJY29uTW9kdWxlLFxyXG5cdFx0TWF0SW5wdXRNb2R1bGUsRnVzZVNoYXJlZE1vZHVsZVxyXG5cdF0sXHJcblx0ZXhwb3J0czogW1Nlc3Npb25Db21wb25lbnQsQXV0aE1vZGVsRGlhbG9nXSxcclxuXHRwcm92aWRlcnM6IFtTZXNzaW9uU2VydmljZV0sXHJcblx0c2NoZW1hczogW0NVU1RPTV9FTEVNRU5UU19TQ0hFTUFdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZXNzaW9uTW9kdWxlIHtcclxufVxyXG4iXX0=