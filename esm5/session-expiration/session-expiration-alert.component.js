import { Component, Inject, Input } from '@angular/core';
import { SessionService } from './session-expiration.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
var AuthModelDialog = /** @class */ (function () {
    function AuthModelDialog(authService, dialog, matDialogRef, data, oauthService) {
        this.authService = authService;
        this.dialog = dialog;
        this.matDialogRef = matDialogRef;
        this.data = data;
        this.oauthService = oauthService;
        this.unsubscribe$ = new Subject();
    }
    AuthModelDialog.prototype.ngOnInit = function () {
        var _this = this;
        this.minutesDisplay = this.data.minutesDisplay;
        this.secondsDisplay = this.data.secondsDisplay;
        var interval = 1000; //1000
        var duration = 2 * 60;
        this.timerSubscription = timer(0, interval).pipe(take(duration)).subscribe(function (value) {
            return _this.render((duration - +value) * interval);
        }, function (err) { }, function () {
            _this.authService.logOutUser();
        });
    };
    AuthModelDialog.prototype.logout = function () {
        this.oauthService.logOut(false);
        this.matDialogRef.close();
    };
    AuthModelDialog.prototype.onSubmit = function () {
        this.authService.notifyUserAction();
        this.matDialogRef.close();
        this.oauthService.silentRefresh();
    };
    AuthModelDialog.prototype.render = function (count) {
        this.secondsDisplay = this.getSeconds(count);
        this.minutesDisplay = this.getMinutes(count);
    };
    AuthModelDialog.prototype.getSeconds = function (ticks) {
        var seconds = ((ticks % 60000) / 1000).toFixed(0);
        return this.pad(seconds);
    };
    AuthModelDialog.prototype.getMinutes = function (ticks) {
        var minutes = Math.floor(ticks / 60000);
        return this.pad(minutes);
    };
    AuthModelDialog.prototype.pad = function (digit) {
        return digit <= 9 ? '0' + digit : digit;
    };
    AuthModelDialog.decorators = [
        { type: Component, args: [{
                    selector: 'auth.model',
                    template: "<scrumboard-board-card-dialog>\r\n\t<div class=\"dialog-content-wrapper\">\r\n\t\t<div class=\"header-top accent ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"m-0 font-weight-900\">Session Timeout</h2>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<button mat-icon-button (click)=\"matDialogRef.close()\" aria-label=\"Close Dialog\" style=\"float:right\">\r\n\t\t\t\t\t<mat-icon>close</mat-icon>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n        </div>\r\n        <div mat-dialog-content class=\"pb-0 m-0 msg_content\" fusePerfectScrollbar style=\"padding: 20px;\">\r\n            <p>You're being timed out due to inactivity.Please choose to stay signed in or to logout.</p>\r\n            <p>Otherwise,you will logged off automatically in <strong>{{ (minutesDisplay) }}:{{ (secondsDisplay) && (secondsDisplay <=59) ? secondsDisplay : '00' }}</strong></p>\r\n        </div>\r\n        <div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n        fxlayoutalign=\"space-between center\" style=\"border-top: 1px solid grey;\">\r\n        <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n            <h2 class=\"m-0\"></h2>\r\n        </div>\r\n        <div class=\"toolbar\"  fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n            <button mat-raised-button class=\"mr-4\" color=\"warn\" style=\"float:right;border-radius: 25px !important;\" (click)=\"onSubmit()\">Stay Logged In\t\t\t\r\n            </button>\r\n            <button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\" ng-reflect-type=\"button\"\r\n                style=\"background-color: grey !important;color: white !important;border-radius: 25px !important;margin-right: 8px !important;\"\r\n                (click)=\"logout()\">\r\n                <span class=\"mat-button-wrapper\">Log Off</span>\r\n                <div class=\"mat-button-ripple mat-ripple\" matripple=\"\" ng-reflect-centered=\"false\"\r\n                    ng-reflect-disabled=\"false\"></div>\r\n                <div class=\"mat-button-focus-overlay\"></div>\r\n            </button>\r\n        </div>\r\n    </div>\r\n    </div>\r\n</scrumboard-board-card-dialog>        ",
                    providers: [SessionService]
                }] }
    ];
    /** @nocollapse */
    AuthModelDialog.ctorParameters = function () { return [
        { type: SessionService },
        { type: MatDialog },
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: OAuthService }
    ]; };
    AuthModelDialog.propDecorators = {
        appChildMessage: [{ type: Input }]
    };
    return AuthModelDialog;
}());
export { AuthModelDialog };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLWFsZXJ0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLWFsZXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixNQUFNLEVBQThDLEtBQUssRUFBRyxNQUFNLGVBQWUsQ0FBQztBQUt6SCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFlBQVksRUFBcUMsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFDcEQsT0FBTyxFQUFhLElBQUksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWpELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFzQyxlQUFlLEVBQWdDLE1BQU0sbUJBQW1CLENBQUM7QUFHcEk7SUFlSSx5QkFBb0IsV0FBMkIsRUFDcEMsTUFBaUIsRUFDakIsWUFBMkMsRUFDakIsSUFBUyxFQUNsQyxZQUEwQjtRQUpsQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDcEMsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixpQkFBWSxHQUFaLFlBQVksQ0FBK0I7UUFDakIsU0FBSSxHQUFKLElBQUksQ0FBSztRQUNsQyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQVB0QyxpQkFBWSxHQUFrQixJQUFJLE9BQU8sRUFBRSxDQUFDO0lBVTVDLENBQUM7SUFFRCxrQ0FBUSxHQUFSO1FBQUEsaUJBZUM7UUFkRyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQy9DLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDL0MsSUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsTUFBTTtRQUU3QixJQUFNLFFBQVEsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FDaEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUNiLENBQUMsU0FBUyxDQUFDLFVBQUEsS0FBSztZQUNqQixPQUFBLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxRQUFRLENBQUM7UUFBM0MsQ0FBMkMsRUFDM0MsVUFBQSxHQUFHLElBQU0sQ0FBQyxFQUNWO1lBQ0ksS0FBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNsQyxDQUFDLENBQ0EsQ0FBQTtJQUNMLENBQUM7SUFFRCxnQ0FBTSxHQUFOO1FBQ0ksSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUM5QixDQUFDO0lBRUQsa0NBQVEsR0FBUjtRQUNJLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdEMsQ0FBQztJQUVPLGdDQUFNLEdBQWQsVUFBZSxLQUFLO1FBQ2hCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVPLG9DQUFVLEdBQWxCLFVBQW1CLEtBQWE7UUFDaEMsSUFBTSxPQUFPLEdBQUcsQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDcEQsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFTyxvQ0FBVSxHQUFsQixVQUFtQixLQUFhO1FBQ2hDLElBQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzFDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRU8sNkJBQUcsR0FBWCxVQUFZLEtBQVU7UUFDdEIsT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDeEMsQ0FBQzs7Z0JBckVKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsWUFBWTtvQkFDdEIsaTdFQUFrRDtvQkFDbEQsU0FBUyxFQUFFLENBQUMsY0FBYyxDQUFDO2lCQUM5Qjs7OztnQkFiUSxjQUFjO2dCQUtkLFNBQVM7Z0JBQ1QsWUFBWTtnREFxQlosTUFBTSxTQUFDLGVBQWU7Z0JBMUJ0QixZQUFZOzs7a0NBbUJoQixLQUFLOztJQTJEVixzQkFBQztDQUFBLEFBdEVELElBc0VDO1NBL0RZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSwgT25Jbml0LCBJbmplY3QsIFZpZXdFbmNhcHN1bGF0aW9uLCBWaWV3Q2hpbGQsIEFmdGVyVmlld0luaXQsSW5wdXQgIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtR3JvdXAsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5cclxuaW1wb3J0IHsgU2Vzc2lvblNlcnZpY2UgfSBmcm9tICcuL3Nlc3Npb24tZXhwaXJhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT0F1dGhTZXJ2aWNlLCBBdXRoQ29uZmlnLCBOdWxsVmFsaWRhdGlvbkhhbmRsZXIgfSBmcm9tICdhbmd1bGFyLW9hdXRoMi1vaWRjJztcclxuaW1wb3J0IHsgU3ViamVjdCwgdGltZXIsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwsIHRha2UgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1hdENoaXBJbnB1dEV2ZW50LCBNYXRBdXRvY29tcGxldGUsIE1BVF9ESUFMT0dfREFUQSwgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYXV0aC5tb2RlbCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2Vzc2lvbi1leHBpcmF0aW9uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHByb3ZpZGVyczogW1Nlc3Npb25TZXJ2aWNlXVxyXG59KVxyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBBdXRoTW9kZWxEaWFsb2cge1xyXG5cclxuICAgIG1pbnV0ZXNEaXNwbGF5O1xyXG4gICAgc2Vjb25kc0Rpc3BsYXk7XHJcbiAgICBASW5wdXQoKSBhcHBDaGlsZE1lc3NhZ2U6IHN0cmluZztcclxuICAgIHVuc3Vic2NyaWJlJDogU3ViamVjdDx2b2lkPiA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB0aW1lclN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gICAgXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGF1dGhTZXJ2aWNlOiBTZXNzaW9uU2VydmljZSxcclxuICAgICAgICBwdWJsaWMgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHVibGljIG1hdERpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEF1dGhNb2RlbERpYWxvZz4sXHJcbiAgICAgICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHByaXZhdGUgZGF0YTogYW55LFxyXG4gICAgICAgIHByaXZhdGUgb2F1dGhTZXJ2aWNlOiBPQXV0aFNlcnZpY2VcclxuICAgICkge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLm1pbnV0ZXNEaXNwbGF5ID0gdGhpcy5kYXRhLm1pbnV0ZXNEaXNwbGF5O1xyXG4gICAgICAgIHRoaXMuc2Vjb25kc0Rpc3BsYXkgPSB0aGlzLmRhdGEuc2Vjb25kc0Rpc3BsYXk7XHJcbiAgICAgICAgY29uc3QgaW50ZXJ2YWwgPSAxMDAwOyAvLzEwMDBcclxuXHJcbiAgICAgICAgY29uc3QgZHVyYXRpb24gPSAyICogNjA7XHJcbiAgICAgICAgdGhpcy50aW1lclN1YnNjcmlwdGlvbiA9IHRpbWVyKDAsIGludGVydmFsKS5waXBlKFxyXG4gICAgICAgIHRha2UoZHVyYXRpb24pXHJcbiAgICAgICAgKS5zdWJzY3JpYmUodmFsdWUgPT5cclxuICAgICAgICB0aGlzLnJlbmRlcigoZHVyYXRpb24gLSArdmFsdWUpICogaW50ZXJ2YWwpLFxyXG4gICAgICAgIGVyciA9PiB7IH0sXHJcbiAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLmxvZ091dFVzZXIoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgKVxyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLm9hdXRoU2VydmljZS5sb2dPdXQoZmFsc2UpO1xyXG4gICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TdWJtaXQoKSB7XHJcbiAgICAgICAgdGhpcy5hdXRoU2VydmljZS5ub3RpZnlVc2VyQWN0aW9uKCk7XHJcbiAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICB0aGlzLm9hdXRoU2VydmljZS5zaWxlbnRSZWZyZXNoKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSByZW5kZXIoY291bnQpIHtcclxuICAgICAgICB0aGlzLnNlY29uZHNEaXNwbGF5ID0gdGhpcy5nZXRTZWNvbmRzKGNvdW50KTtcclxuICAgICAgICB0aGlzLm1pbnV0ZXNEaXNwbGF5ID0gdGhpcy5nZXRNaW51dGVzKGNvdW50KTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgcHJpdmF0ZSBnZXRTZWNvbmRzKHRpY2tzOiBudW1iZXIpIHtcclxuICAgIGNvbnN0IHNlY29uZHMgPSAoKHRpY2tzICUgNjAwMDApIC8gMTAwMCkudG9GaXhlZCgwKTtcclxuICAgIHJldHVybiB0aGlzLnBhZChzZWNvbmRzKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE1pbnV0ZXModGlja3M6IG51bWJlcikge1xyXG4gICAgY29uc3QgbWludXRlcyA9IE1hdGguZmxvb3IodGlja3MgLyA2MDAwMCk7XHJcbiAgICByZXR1cm4gdGhpcy5wYWQobWludXRlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwYWQoZGlnaXQ6IGFueSkge1xyXG4gICAgcmV0dXJuIGRpZ2l0IDw9IDkgPyAnMCcgKyBkaWdpdCA6IGRpZ2l0O1xyXG4gICAgfVxyXG59Il19