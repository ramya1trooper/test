import * as tslib_1 from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter, Inject, ChangeDetectorRef } from "@angular/core";
import { MatPaginator, MatTableDataSource, MatDialog, MatCheckbox, MatTable } from "@angular/material";
import { SnackBarService } from "./../shared/snackbar.service";
import * as FileSaver from "file-saver";
import { FormControl } from "@angular/forms";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import * as _ from "lodash";
// require('jquery')
// require('kendoGrid')
// /<reference path="../jquery/index" />
// import $ from "../JQuery";
import { ContentService } from "../content/content.service";
import { MessageService } from "../_services/index";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from '../loader.service';
import { process } from '@progress/kendo-data-query';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
// declare  $: any;
// declare var $ : JQueryStatic;
var NewTableLayoutComponent = /** @class */ (function () {
    function NewTableLayoutComponent(_fuseTranslationLoaderService, contentService, messageService, _matDialog, snackBarService, changeDetectorRef, environment, english, loaderService) {
        var _this = this;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.contentService = contentService;
        this.messageService = messageService;
        this._matDialog = _matDialog;
        this.snackBarService = snackBarService;
        this.changeDetectorRef = changeDetectorRef;
        this.environment = environment;
        this.english = english;
        this.loaderService = loaderService;
        this.disablePagination = false;
        this.checkClickEventMessage = new EventEmitter();
        this.actionClickEvent = new EventEmitter();
        this.enableSelectOption = false;
        this.enableSearch = false;
        this.tableData = [];
        this.enableAction = false;
        this.limit = 10;
        this.offset = 0;
        this.selectedData = [];
        this.inputData = {};
        this.unsubscribe = new Subject();
        this.unsubscribeMsg = new Subject();
        this.unsubscribeModel = new Subject();
        this.currentFilteredValue = {};
        this.enableOptionFilter = false;
        this.enableUIfilter = false;
        this.enableFilter = false;
        this.groupField = [];
        this.selectAllOption = "none";
        this.enableDelete = false;
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.notifyId = {};
        this.enableGrouping = false;
        this.selectAllItem = "unchecked";
        // selectOption: any[] = [{ Key: 'All', Name: 'All', Value: 'All', Type: 'String'},
        // { Key: 'Read', Name: 'Read', Value: 'read', Type: 'String'},
        // { Key: 'Unread', Name: 'Unread', Value: 'unread', Type: 'String'},
        // { Key: 'None', Name: 'None', Value: 'None', Type: 'String'}  ];
        this.selectOption = [{ "value": "all", "name": "All" },
            { "value": true, "name": "Read" },
            { "value": false, "name": "Unread" },
            { "value": "none", "name": "None" }];
        this.redirectUri = this.environment.redirectUri;
        this._fuseTranslationLoaderService.loadTranslations(english);
        this.messageService.modelCloseMessage
            .pipe(takeUntil(this.unsubscribeModel))
            .subscribe(function (data) {
            console.log(data, ">>>>data");
            _this.selectedData = [];
            if (data != 0) {
                _this.inputData["selectedData"] = [];
                _this.inputData["selectedIdList"] = [];
                _this.inputData["selectAll"] = false;
            }
            _this.messageService.sendTriggerNotification({ data: "trigger" });
            _this.messageService.sendDatasource("null");
            console.log(_this.tableData);
            console.log(_this.dataSource);
            // _.forEach(this.tableData, function(item) {
            //   item.checked = false;
            // });
            // this.dataSource.data.map(obj => {
            //   obj.checked = false;
            // });
            // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
            // // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            // this.checkClickEventMessage.emit("clicked");
            _this.data = data;
            // if(this.data === 'listView'){
            if (_this.data) {
                _this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
                _this.ngOnInit();
            }
            // }
        });
        this.messageService.getMessage().pipe(takeUntil(this.unsubscribeMsg)).subscribe(function (message) {
            console.log(message, ">>message");
            _this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            console.log(_this, ">>THIS");
            if (_this.currentConfigData &&
                _this.currentConfigData.listView &&
                _this.currentConfigData.listView.enableNewTableLayout) {
                _this.ngOnInit();
            }
        });
        this.messageService
            .getTableHeaderUpdate()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(function (data) {
            console.log(data, ">>>>>data");
            if (data.data == 'update') {
                _this.updateTableView();
            }
            else {
                console.log("FILTERRRR");
                _this.enableFilter = !_this.enableFilter;
            }
        });
    }
    NewTableLayoutComponent.prototype.ngOnInit = function () {
        // this.changeDetectorRef.detectChanges();
        this.inputData["selectAll"] = false;
        this.selectAllValue = "";
        this.enableDelete = false;
        this.selectAllItem = 'unchecked';
        console.log(this, "......table this");
        this.defaultDatasource = localStorage.getItem("datasource");
        this.realm = localStorage.getItem("realm");
        this.queryParams = {
            offset: 0,
            limit: 10,
            datasource: this.defaultDatasource,
            realm: this.realm
        };
        this.currentTenant = {
            realm: this.realm,
            userId: localStorage.getItem("userId")
        };
        //  this.paginator["pageIndex"] = 0;
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        console.log(">>> this.data ", this.data);
        if (this.data == 'listView' || this.data == 'refreshPage' || this.data === 0) {
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.selectedData = [];
        }
        this.inputData["datasourceId"] = this.defaultDatasource;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        // this.changeDetectorRef.detectChanges();
        this.fromFieldValue = new FormControl([""]);
        this.toFieldValue = new FormControl([""]);
        this.onLoadData(null);
    };
    NewTableLayoutComponent.prototype.ngAfterViewInit = function () {
        console.log("ngAfterViewInit ");
        console.log(this.selectedPaginator, "selectedPag>>>>");
        if (this.dataSource && this.selectedPaginator) {
            this.dataSource.paginator = this.selectedPaginator;
        }
    };
    NewTableLayoutComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
        this.unsubscribeMsg.next();
        this.unsubscribeModel.next();
    };
    NewTableLayoutComponent.prototype.groupChange = function (event) {
        var _this = this;
        console.log(event, ">>>>> GROUP EVENT");
        this.groupField = event;
        console.log(this, ">>>> THIS");
        var apiFunctionDetails = this.currentTableData.onGroupingFunction;
        var self = this;
        _.forEach(apiFunctionDetails.requestData, function (requestItem) {
            if (requestItem.fromCurrentData) {
                self.queryParams[requestItem.name] = self.currentData[requestItem.value];
            }
            else if (requestItem.directAssign) {
                self.queryParams[requestItem.name] = requestItem.convertToString
                    ? JSON.stringify(requestItem.value)
                    : requestItem.value;
            }
            else {
                self.queryParams[requestItem.name] = requestItem.convertToString
                    ? JSON.stringify(self.inputData[requestItem.value])
                    : self.inputData[requestItem.value];
            }
        });
        _.forEach(event, function (eventItem) {
            self.queryParams[eventItem.field] = true;
        });
        this.contentService
            .getAllReponse(this.queryParams, apiFunctionDetails.apiUrl)
            .subscribe(function (data) {
            // if ( currentTableValue.onLoadFunction.enableLoader) {
            _this.loaderService.stopLoader();
            //}
            _this.enableSearch = _this.currentData.enableGlobalSearch;
            var selectedTableHeaders = localStorage.getItem("selectedTableHeaders"); // after global setting change
            _this.columns = !_.isEmpty(selectedTableHeaders) ? JSON.parse(selectedTableHeaders) : _this.columns;
            _this.displayedColumns = _this.columns
                .filter(function (val) {
                return val.isActive;
            });
            var tempArray = [];
            var self = _this;
            console.log(self, ">>>>>>>>>>THISSSS");
            _.forEach(data.response[apiFunctionDetails.response], function (item, index) {
                if (typeof item !== "object") {
                    var tempObj = {};
                    // tempObj[keyToSet] = item;
                    tempArray.push(tempObj);
                }
                else {
                    var tempObj_1 = {};
                    _.forEach(self.currentTableData.dataViewFormat, function (viewItem) {
                        if (viewItem.subkey) {
                            if (viewItem.assignFirstIndexval) {
                                var resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                tempObj_1[viewItem.name] = item[viewItem.value]
                                    ? resultValue
                                    : "";
                            }
                            else {
                                tempObj_1[viewItem.name] = item[viewItem.value]
                                    ? item[viewItem.value][viewItem.subkey]
                                    : "";
                            }
                        }
                        else if (viewItem.isCondition) {
                            tempObj_1[viewItem.name] = eval(viewItem.condition);
                        }
                        else if (viewItem.isAddDefaultValue) {
                            tempObj_1[viewItem.name] = viewItem.value;
                        }
                        else {
                            tempObj_1[viewItem.name] = item[viewItem.value];
                        }
                        if (self.currentTableData.loadLabelFromConfig) {
                            tempObj_1[viewItem.name] =
                                self.currentTableData.headerList[item[viewItem.value]];
                        }
                    });
                    var filterObj = {};
                    filterObj[self.currentTableData.filterKey] = item[self.currentTableData.dataFilterKey];
                    var selectedValue = _.find(self.inputData[self.currentTableData.selectedValuesKey], filterObj);
                    console.log(selectedValue, ">>>>>selectedValue");
                    if (selectedValue) {
                        item.checked = true;
                    }
                    tempObj_1 = tslib_1.__assign({}, item, tempObj_1);
                    tempArray.push(tempObj_1);
                }
            });
            if (tempArray && tempArray.length) {
                _this.tableData = tempArray;
            }
            else {
                _this.tableData =
                    data && data.response && data.response[apiFunctionDetails.response]
                        ? (data.response[apiFunctionDetails.response])
                        : [];
            }
            if (data && data.response) {
                _this.length = data.response.total;
            }
            _this.constructGroupedData(data.response, apiFunctionDetails.response);
            // let concatData = this.tableData[0].data.concat(this.tableData[1].data)
            _this.dataSource = new MatTableDataSource(_this.tableData);
            _this.dataSource.paginator = _this.selectedPaginator; // newly added 
            localStorage.setItem("currentInput", JSON.stringify(_this.inputData));
            _this.inputData["selectAll"] = false;
            var filteredKey = (_this.currentFilteredValue.searchKey) ? _this.currentFilteredValue.searchKey : '';
            _.forEach(self.columns, function (x) {
                if (filteredKey == x.value) {
                    self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                }
                else {
                    self.inputData[x.keyToSave] = null;
                }
            });
            // this.loadKendoGrid();
            // this.selectAllCheck({ checked: false });
            // this.selectAll = false;
            // console.log(this.selectAll, "SelectAll>>>>>>>");
            // this.inputData["selectedData"] = [];
            // this.inputData["selectedIdList"] = [];
            // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            // console.log(this.selectAll, "SelectAll>>>>>>>");
            // this.changeDetectorRef.detectChanges();
            _this.checkClickEventMessage.emit("clicked");
        }, function (err) {
            _this.loaderService.stopLoader();
            console.log(" Error ", err);
        });
    };
    NewTableLayoutComponent.prototype.constructGroupedData = function (response, responseName) {
        console.log(response, ">>response");
        console.log(">>>> curenttable Data ", this.currentTableData);
        this.tableData = [];
        var self = this;
        _.forEach(response[responseName], function (item) {
            _.forEach(item.data, function (dataItem, index) {
                var obj = dataItem;
                obj["controlName"] = response.accessControls[dataItem.accessControl].accessControlName;
                obj["riskRanking"] = response.accessControls[dataItem.accessControl].riskRanking;
                obj["controlType"] = response.accessControls[dataItem.accessControl].controlType;
                obj["controlDescription"] = response.accessControls[dataItem.accessControl].accessControlDescription;
                obj["entitlementName"] = response.accessGroups[dataItem.accessGroup].accessGroupName;
                obj["rulesetName"] = response.rulesets[response.accessControls[dataItem.accessControl].rulesetId].name;
                obj["datasourceName"] = response.datasources[dataItem.datasource].name;
                obj["statusClass"] = {
                    "Open": "label bg-intiated",
                    "Closed": "label bg-failed",
                    "Remediated": "label bg-failed",
                    "Authorized": "label bg-success",
                    "Approved": "label bg-success"
                };
                self.tableData.push(obj);
                if (item.data.length == 10 && index == '9') {
                    console.log("LOAD MORE");
                    console.log(self.tableData.length, ">>>>self.tableData.length");
                    console.log(index, ">>>> INDEX");
                    var tempobj_1 = {};
                    tempobj_1["entitlementName"] = response.accessGroups[dataItem.accessGroup].accessGroupName;
                    tempobj_1["userName"] = dataItem.userName;
                    tempobj_1["roleName"] = dataItem.roleName;
                    tempobj_1['accessGroup'] = dataItem.accessGroup;
                    tempobj_1['accessControl'] = dataItem.accessControl;
                    tempobj_1["controlName"] = response.accessControls[dataItem.accessControl].accessControlName;
                    tempobj_1["riskRanking"] = response.accessControls[dataItem.accessControl].riskRanking;
                    tempobj_1["controlType"] = response.accessControls[dataItem.accessControl].controlType;
                    tempobj_1["controlDescription"] = response.accessControls[dataItem.accessControl].accessControlDescription;
                    tempobj_1["_id"] = response.accessControls[dataItem.accessControl]._id;
                    tempobj_1["enableLoad"] = true;
                    tempobj_1["statusClass"] = {
                        "Open": "label bg-intiated",
                        "Closed": "label bg-failed",
                        "Remediated": "label bg-failed",
                        "Authorized": "label bg-success",
                        "Approved": "label bg-success"
                    };
                    _.forEach(self.currentTableData.productKeys, function (viewItem) {
                        tempobj_1[viewItem.name] = dataItem[viewItem.value];
                    });
                    console.log(">>> tempobj ", tempobj_1);
                    self.tableData.push(tempobj_1);
                }
            });
        });
        this.newTableData = process(this.tableData, { group: this.groupField });
        this.newTableData.total = response.total;
        // this.notifycount = this.newTableData.total;
        // _.forEach(this.newTableData.data,function(itemVal, index){
        //   itemVal.collapseGroup(index.toString())
        // } )
        //   for (let m = 0; m < 5; m = m + 1) {
        //     this.newTableData.collapseGroup(m.toString());
        // }
        //   this.newTableData.find(".k-grouping-row").each(function () {
        //     this.newTableData.collapseGroup(this);
        // });
        console.log(this, ">>> THIS");
        console.log(this.newTableData, ">>> TABLE DAT NEW");
    };
    NewTableLayoutComponent.prototype.onRoutingClick = function (selectedRow) {
        if (this.currentData && this.currentData.enableRouting) {
            this.inputData = tslib_1.__assign({}, this.inputData, selectedRow);
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            this.messageService.sendRoutingMessage({
                selectedRow: selectedRow,
                hideView: this.currentData.hideViewLayouts
            });
        }
    };
    NewTableLayoutComponent.prototype.collapseGroups = function (grid) {
        this.newTableData.data.forEach(function (gr, idx) { return grid.collapseGroup(idx.toString()); });
    };
    NewTableLayoutComponent.prototype.loadMoreClick = function (row) {
        var _this = this;
        console.log(row, ">>>>>>> ROW");
        var apiFunctionDetails = this.currentTableData.onGroupingFunction;
        console.log(this.queryParams, ">>> QUERY PARAMS");
        console.log(this, ">>> THIS");
        var indexVal = -1;
        var queryObj = this.queryParams;
        var groupingField = this.currentTableData.groupingDetail;
        if (this.groupField && this.groupField.length == 1) {
            var self_1 = this;
            _.forEach(groupingField, function (groupItem) {
                if (self_1.groupField && _.findIndex(self_1.groupField, { field: groupItem.name }) >= 0) {
                    if (groupItem.isId) {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.value];
                    }
                    else {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.name];
                    }
                    indexVal = _.findIndex(self_1.newTableData.data, { value: row[groupItem.name] });
                }
            });
            queryObj.loadMoreOffset = (this.newTableData.data[indexVal].items.length > 10) ? this.newTableData.data[indexVal].items.length - 1 : 10;
            this.contentService
                .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
                .subscribe(function (data) {
                console.log(data, ">>>data");
                var responseData = data.response;
                var controlItemValue = _this.newTableData.data[indexVal].items;
                controlItemValue.pop();
                if (responseData[apiFunctionDetails.response]) {
                    _.forEach(responseData[apiFunctionDetails.response][0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        var obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                        obj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        obj["statusClass"] = {
                            "Open": "label bg-intiated",
                            "Closed": "label bg-failed",
                            "Remediated": "label bg-failed",
                            "Authorized": "label bg-success",
                            "Approved": "label bg-success"
                        };
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            var tempobj_2 = {};
                            tempobj_2['accessGroup'] = dataItem.accessGroup;
                            tempobj_2['accessControl'] = dataItem.accessControl;
                            tempobj_2["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj_2["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj_2["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                            tempobj_2["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                            tempobj_2["roleName"] = dataItem.roleName;
                            tempobj_2["entitlementName"] = dataItem.entitlementName;
                            tempobj_2["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj_2["enableLoad"] = true;
                            tempobj_2["statusClass"] = {
                                "Open": "label bg-intiated",
                                "Closed": "label bg-failed",
                                "Remediated": "label bg-failed",
                                "Authorized": "label bg-success",
                                "Approved": "label bg-success"
                            };
                            _.forEach(self_1.currentTableData.productKeys, function (viewItem) {
                                tempobj_2[viewItem.name] = dataItem[viewItem.value];
                            });
                            controlItemValue.push(tempobj_2);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                _this.newTableData.data[indexVal].items = controlItemValue;
            }, function (err) {
                _this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
        else if (this.groupField && this.groupField.length == 2) {
            var self_2 = this;
            var firstIndexVal_1 = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self_2.groupField && _.findIndex(self_2.groupField, { field: groupItem.name }) >= 0) {
                    if (groupItem.isId) {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.value];
                    }
                    else {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.name];
                    }
                }
            });
            _.forEach(groupingField, function (groupItem) {
                if (self_2.groupField && _.findIndex(self_2.groupField, { field: groupItem.name }) == 0) {
                    firstIndexVal_1 = _.findIndex(self_2.newTableData.data, { value: row[groupItem.name] });
                }
            });
            var secondIndexVal_1 = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self_2.groupField && _.findIndex(self_2.groupField, { field: groupItem.name }) == 1) {
                    secondIndexVal_1 = _.findIndex(self_2.newTableData.data[firstIndexVal_1].items, { value: row[groupItem.name] });
                }
            });
            console.log(firstIndexVal_1, ">>> firstIndexVal");
            console.log(secondIndexVal_1, ">>>>> secondIndexVal");
            queryObj.loadMoreOffset = (this.newTableData.data[firstIndexVal_1].items[secondIndexVal_1].items.length > 10) ? this.newTableData.data[firstIndexVal_1].items[secondIndexVal_1].items.length - 1 : 10;
            this.contentService
                .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
                .subscribe(function (data) {
                console.log(data, ">>>data");
                var responseData = data.response;
                var controlItemValue = _this.newTableData.data[firstIndexVal_1].items[secondIndexVal_1].items;
                controlItemValue.pop();
                if (responseData[apiFunctionDetails.response]) {
                    _.forEach(responseData[apiFunctionDetails.response][0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        var obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                        obj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        obj["statusClass"] = {
                            "Open": "label bg-intiated",
                            "Closed": "label bg-failed",
                            "Remediated": "label bg-failed",
                            "Authorized": "label bg-success",
                            "Approved": "label bg-success"
                        };
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            var tempobj_3 = {};
                            tempobj_3['accessGroup'] = dataItem.accessGroup;
                            tempobj_3['accessControl'] = dataItem.accessControl;
                            tempobj_3["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj_3["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj_3["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                            tempobj_3["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                            tempobj_3["roleName"] = dataItem.roleName;
                            tempobj_3["userName"] = dataItem.userName;
                            tempobj_3["entitlementName"] = dataItem.entitlementName;
                            tempobj_3["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj_3["enableLoad"] = true;
                            _.forEach(self_2.currentTableData.productKeys, function (viewItem) {
                                tempobj_3[viewItem.name] = dataItem[viewItem.value];
                            });
                            tempobj_3["statusClass"] = {
                                "Open": "label bg-intiated",
                                "Closed": "label bg-failed",
                                "Remediated": "label bg-failed",
                                "Authorized": "label bg-success",
                                "Approved": "label bg-success"
                            };
                            controlItemValue.push(tempobj_3);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                _this.newTableData.data[firstIndexVal_1].items[secondIndexVal_1].items = controlItemValue;
            }, function (err) {
                _this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
        else if (this.groupField && this.groupField.length == 3) {
            var self_3 = this;
            var firstIndexVal_2 = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self_3.groupField && _.findIndex(self_3.groupField, { field: groupItem.name }) >= 0) {
                    if (groupItem.isId) {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.value];
                    }
                    else {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.name];
                    }
                }
            });
            _.forEach(groupingField, function (groupItem) {
                if (self_3.groupField && _.findIndex(self_3.groupField, { field: groupItem.name }) == 0) {
                    firstIndexVal_2 = _.findIndex(self_3.newTableData.data, { value: row[groupItem.name] });
                }
            });
            var secondIndexVal_2 = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self_3.groupField && _.findIndex(self_3.groupField, { field: groupItem.name }) == 1) {
                    secondIndexVal_2 = _.findIndex(self_3.newTableData.data[firstIndexVal_2].items, { value: row[groupItem.name] });
                }
            });
            var thirdIndexVal_1 = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self_3.groupField && _.findIndex(self_3.groupField, { field: groupItem.name }) == 2) {
                    thirdIndexVal_1 = _.findIndex(self_3.newTableData.data[firstIndexVal_2].items[secondIndexVal_2].items, { value: row[groupItem.name] });
                }
            });
            console.log(firstIndexVal_2, ">>> firstIndexVal");
            console.log(secondIndexVal_2, ">>>>> secondIndexVal");
            console.log(thirdIndexVal_1, ">>>>> thirdIndexVal");
            queryObj.loadMoreOffset = (this.newTableData.data[firstIndexVal_2].items[secondIndexVal_2].items[thirdIndexVal_1].items.length > 10) ? this.newTableData.data[firstIndexVal_2].items[secondIndexVal_2].items[thirdIndexVal_1].items.length - 1 : 10;
            this.contentService
                .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
                .subscribe(function (data) {
                console.log(data, ">>>data");
                var responseData = data.response;
                var controlItemValue = _this.newTableData.data[firstIndexVal_2].items[secondIndexVal_2].items[thirdIndexVal_1].items;
                controlItemValue.pop();
                if (responseData[apiFunctionDetails.response]) {
                    _.forEach(responseData[apiFunctionDetails.response][0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        var obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                        obj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        obj["statusClass"] = {
                            "Open": "label bg-intiated",
                            "Closed": "label bg-failed",
                            "Remediated": "label bg-failed",
                            "Authorized": "label bg-success",
                            "Approved": "label bg-success"
                        };
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            var tempobj_4 = {};
                            tempobj_4['accessGroup'] = dataItem.accessGroup;
                            tempobj_4['accessControl'] = dataItem.accessControl;
                            tempobj_4["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj_4["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj_4["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                            tempobj_4["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                            tempobj_4["roleName"] = dataItem.roleName;
                            tempobj_4["userName"] = dataItem.userName;
                            tempobj_4["entitlementName"] = dataItem.entitlementName;
                            tempobj_4["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj_4["enableLoad"] = true;
                            _.forEach(self_3.currentTableData.productKeys, function (viewItem) {
                                tempobj_4[viewItem.name] = dataItem[viewItem.value];
                            });
                            tempobj_4["statusClass"] = {
                                "Open": "label bg-intiated",
                                "Closed": "label bg-failed",
                                "Remediated": "label bg-failed",
                                "Authorized": "label bg-success",
                                "Approved": "label bg-success"
                            };
                            controlItemValue.push(tempobj_4);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                _this.newTableData.data[firstIndexVal_2].items[secondIndexVal_2].items[thirdIndexVal_1].items = controlItemValue;
            }, function (err) {
                _this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
    };
    NewTableLayoutComponent.prototype.loadMoreClick_backup = function (row) {
        var _this = this;
        console.log(row, ">>>>>>> ROW");
        var apiFunctionDetails = this.currentTableData.onGroupingFunction;
        console.log(this.queryParams, ">>> QUERY PARAMS");
        console.log(this, ">>> THIS");
        var indexVal = -1;
        var queryObj = this.queryParams;
        if (this.groupField && _.findIndex(this.groupField, { field: "controlName" }) >= 0) {
            queryObj.loadMoreControl = row.controlName;
            indexVal = _.findIndex(this.newTableData.data, { value: row.controlName });
        }
        if (this.groupField && _.findIndex(this.groupField, { field: "userName" }) >= 0) {
            queryObj.loadMoreUserName = row.userName;
            indexVal = _.findIndex(this.newTableData.data, { value: row.userName });
        }
        if (this.groupField && _.findIndex(this.groupField, { field: "entitlementName" }) >= 0) {
            queryObj.loadMoreGroupName = row.accessGroup;
            indexVal = _.findIndex(this.newTableData.data, { value: row.entitlementName });
        }
        if (this.groupField && _.findIndex(this.groupField, { field: "roleName" }) >= 0) {
            queryObj.loadMoreRespName = row.roleName;
            indexVal = _.findIndex(this.newTableData.data, { value: row.roleName });
        }
        console.log(indexVal, ">>>>> indexVal");
        queryObj.loadMoreOffset = (this.newTableData.data[indexVal].items.length > 10) ? this.newTableData.data[indexVal].items.length - 1 : 10;
        console.log(queryObj, ">>>> queery Obj");
        this.contentService
            .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
            .subscribe(function (data) {
            console.log(data, ">>>data");
            var responseData = data.response;
            // if(_.findIndex(this.groupField,{field : 'userName'}) >= 0){
            // }else{
            // }
            if (_this.groupField && _this.groupField.length == 1) {
                var controlItemValue_1 = _this.newTableData.data[indexVal].items;
                controlItemValue_1.pop();
                if (responseData.userConflicts) {
                    _.forEach(responseData.userConflicts[0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        var obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        controlItemValue_1.push(obj);
                        console.log(controlItemValue_1.length, ">>>> controlItemValue.length");
                        if (controlItemValue_1.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            var tempobj = {};
                            tempobj['accessGroup'] = dataItem.accessGroup;
                            tempobj['accessControl'] = dataItem.accessControl;
                            tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj["roleName"] = dataItem.roleName;
                            tempobj["entitlementName"] = dataItem.entitlementName;
                            tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj["enableLoad"] = true;
                            controlItemValue_1.push(tempobj);
                        }
                    });
                }
                console.log(controlItemValue_1, ">>controlItemValue");
                _this.newTableData.data[indexVal].items = controlItemValue_1;
            }
            else if (_this.groupField && _this.groupField.length == 2) {
                console.log(_this, ">>>> THIS");
                console.log(responseData, ">>>>responseData");
                var controlItemValue = _this.newTableData.data[indexVal].items;
                var secondValueIndex = _.findIndex(controlItemValue, { value: row.userName });
                var secondGroupVal = controlItemValue[secondValueIndex].items;
                // console.log(secondGroupVal,">>> GROUP VAL")
                secondGroupVal.pop();
                // if(responseData.userConflicts){
                //   _.forEach(responseData.userConflicts[0].data, function(dataItem, index){
                //     console.log(index,">>>index")
                //     let obj = dataItem;
                //       obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                //       obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                //       obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                //       obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                //       controlItemValue.push(obj);
                //       console.log(controlItemValue.length,">>>> controlItemValue.length")
                //       if(controlItemValue.length % 10 == 0 && index == '9'){
                //         console.log("LOAD MORE")
                //         let tempobj={};
                //         tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                //         tempobj["roleName"] = dataItem.roleName;
                //         tempobj["entitlementName"] = dataItem.entitlementName;
                //         tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                //         tempobj["enableLoad"] = true;
                //         controlItemValue.push(tempobj);
                //   }
                //   });
                // }
                // console.log(controlItemValue,">>controlItemValue");
                // this.newTableData.data[indexVal].items = controlItemValue;
            }
            //   // constructGroupedDa/ta(data, )
        }, function (err) {
            _this.loaderService.stopLoader();
            console.log(" Error ", err);
        });
    };
    NewTableLayoutComponent.prototype.groupCollapse = function (event) {
        console.log(event, ">>> EVNT VAL");
    };
    NewTableLayoutComponent.prototype.updateTableView = function () {
        if (this.currentTableData && this.currentTableData.tableHeader) {
            var currentTableHeader = JSON.parse(localStorage.getItem("selectedTableHeaders"));
            var tableHeaderLength = this.currentTableData.tableHeader.length;
            // this.columns = currentTableHeader;
            this.displayedColumns = [];
            if (_.findIndex(this.currentTableData.tableHeader, { value: "select" }) > -1) {
                this.displayedColumns.push(this.currentTableData.tableHeader[0]);
            }
            var filterArray = currentTableHeader
                .filter(function (val) {
                return val.isActive;
            });
            this.displayedColumns = _.concat(this.displayedColumns, filterArray);
            if (_.findIndex(this.currentTableData.tableHeader, { value: "action" }) > -1) {
                this.displayedColumns.push(this.currentTableData.tableHeader[tableHeaderLength - 1]);
            }
        }
    };
    NewTableLayoutComponent.prototype.onLoadData = function (enableNext) {
        if (this.data == 'refreshPage') {
            this.queryParams["offset"] = 0;
            this.queryParams["limit"] = 10;
            this.offset = 0;
        }
        this.currentFilteredValue = {};
        this.columns = [];
        this.displayedColumns = [];
        var tableArray = this.onLoad
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        var responseKey = this.onLoad && this.onLoad.responseKey ? this.onLoad.responseKey : null;
        var index = _.findIndex(tableArray, { tableId: this.tableId });
        if (index > -1)
            this.currentTableData = tableArray[index];
        console.log(tableArray, ".....tableArray");
        console.log(this.currentTableData, ".....currentTableData");
        var checkOptionFilter = (this.currentTableData && this.currentTableData.enableOptionsFilter) ? true : false;
        if (this.currentTableData && this.currentTableData.enableMultiSelect) {
            this.enableOptionFilter = true;
        }
        if (checkOptionFilter) {
            var self = this;
            this.enableOptionFilter = true;
            var Optiondata = this.currentTableData.filterOptionLoadFunction;
            this.optionFilterFields = this.currentTableData.optionFields;
            var apiUrl = Optiondata.apiUrl;
            var responseName = Optiondata.response;
            var self = this;
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(function (res) {
                var tempArray = (res.response[responseName]) ? res.response[responseName] : [];
                self.optionFilterdata = tempArray;
            });
        }
        if (this.currentTableData && this.currentTableData.enableUifilter) {
            var filterKey = this.currentTableData.uiFilterKey;
            this.enableUIfilter = true;
            // this.dataSource.filterPredicate = (data: Element, filter: string) => {
            //   return data[filterKey] == filter;
            //  };
        }
        if (this.onLoad && this.onLoad.response && !enableNext) {
            this.getLoadData(this.currentTableData, this.onLoad.response, this.onLoad.total, responseKey);
        }
        else {
            if (this.currentTableData)
                this.getData(this.currentTableData, null);
        }
        if (!enableNext) {
            // this.paginator.firstPage();
        }
    };
    NewTableLayoutComponent.prototype.getData = function (currentTableValue, enableTableSearch) {
        var _this = this;
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.columns = currentTableValue.tableHeader;
        if (currentTableValue.onLoadFunction) {
            var apiUrl = currentTableValue.onLoadFunction.apiUrl;
            var responseName = currentTableValue.onLoadFunction.response;
            var keyToSet_1 = currentTableValue.onLoadFunction.keyForStringResponse;
            this.currentData = this.currentConfigData["listView"];
            var includeRequest = currentTableValue.onLoadFunction.includerequestData ? true : false;
            //if ( currentTableValue.onLoadFunction.enableLoader) {
            this.loaderService.startLoader();
            //}
            var dynamicLoad = currentTableValue.onLoadFunction.dynamicReportLoad; // condition checked when implement additional report view
            var requestedQuery = currentTableValue.onLoadFunction.requestData;
            var self = this;
            var requestQueryParam = {};
            _.forEach(this.groupField, function (eventItem) {
                self.queryParams[eventItem.field] = true;
            });
            if (dynamicLoad) {
                var tempObj = localStorage.getItem("CurrentReportData");
                var reportITem_1 = JSON.parse(tempObj);
                _.forEach(requestedQuery, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else {
                        requestQueryParam[requestItem.name] = (reportITem_1[requestItem.value]) ? reportITem_1[requestItem.value] : requestItem.value;
                    }
                });
            }
            else {
                if (!enableTableSearch) {
                    _.forEach(requestedQuery, function (requestItem) {
                        //  requestData[item.name] = item.subKey
                        //  ? details[item.value][item.subKey]
                        //  : details[item.value];
                        if (requestItem.fromCurrentData) {
                            requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                        }
                        else if (requestItem.directAssign) {
                            self.queryParams[requestItem.name] = requestItem.convertToString
                                ? JSON.stringify(requestItem.value)
                                : requestItem.value;
                        }
                        else {
                            var existCheck = _.has(self.inputData, requestItem.value);
                            //  tempObj[viewItem.name] = item[viewItem.value] ? item[viewItem.value][viewItem.subkey] : "";
                            if (existCheck) {
                                var tempData = requestItem.subKey
                                    ? self.inputData[requestItem.value][requestItem.subKey]
                                    : self.inputData[requestItem.value];
                                self.queryParams[requestItem.name] = requestItem.convertToString
                                    ? JSON.stringify(tempData)
                                    : tempData;
                            }
                        }
                    });
                }
            }
            this.queryParams["disableGroup"] = (this.groupField && this.groupField.length) ? false : true;
            this.queryParams = tslib_1.__assign({}, this.queryParams, requestQueryParam);
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(function (data) {
                // if ( currentTableValue.onLoadFunction.enableLoader) {
                _this.loaderService.stopLoader();
                //}
                var selectedTableHeaders = localStorage.getItem("selectedTableHeaders"); // after global setting change
                _this.columns = !_.isEmpty(selectedTableHeaders) ? JSON.parse(selectedTableHeaders) : _this.columns;
                _this.enableSearch = _this.currentData.enableGlobalSearch;
                _this.displayedColumns = _this.columns
                    .filter(function (val) {
                    return val.isActive;
                });
                console.log(">>  this.displayedColumn ", _this.displayedColumns);
                var tempArray = [];
                var self = _this;
                console.log(self, ">>>>>>>>>>THISSSS");
                console.log("Data Response *****", data.response);
                console.log("Data Response Name *****", responseName);
                console.log(currentTableValue, "Curr Table data >>>>.");
                _.forEach(data.response[responseName], function (item, index) {
                    console.log("Item Data >>>>>>", item);
                    if (typeof item !== "object") {
                        var tempObj = {};
                        tempObj[keyToSet_1] = item;
                        tempArray.push(tempObj);
                    }
                    else {
                        // if (self.tableId == "notifications") {
                        //   item.feature = self._fuseTranslationLoaderService.instant(
                        //     "NOTIFICATION_FORMATS.FEATURE." + item.messageObj.feature);
                        //   let operationStatus = item.messageObj.action + "  " + item.status;
                        //   console.log("Operation Data", operationStatus);
                        //   item.status_action = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION_STATUS." + operationStatus);
                        //   item.msg = item.feature + "   " + item.status_action;
                        //   console.log("Entry of Condition item");
                        // }
                        var tempObj_2 = {};
                        console.log("********** Item Data **************", item);
                        _.forEach(currentTableValue.dataViewFormat, function (viewItem) {
                            console.log("View Item of Dataview Format >>>>", viewItem);
                            if (viewItem.subkey) {
                                if (viewItem.assignFirstIndexval) {
                                    var resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                    tempObj_2[viewItem.name] = item[viewItem.value]
                                        ? resultValue
                                        : "";
                                }
                                else {
                                    tempObj_2[viewItem.name] = item[viewItem.value]
                                        ? item[viewItem.value][viewItem.subkey]
                                        : "";
                                }
                            }
                            else if (viewItem.isCondition) {
                                tempObj_2[viewItem.name] = eval(viewItem.condition);
                            }
                            else if (viewItem.isAddDefaultValue) {
                                tempObj_2[viewItem.name] = viewItem.value;
                            }
                            else {
                                tempObj_2[viewItem.name] = item[viewItem.value];
                            }
                            if (viewItem.isNotify) {
                                // tempObj[viewItem.name] = eval(viewItem.condition);
                                var feat = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.FEATURE." + (item[viewItem.value].feature));
                                var operationStatus = item[viewItem.value].action + "_" + item[viewItem.value].status;
                                // tslint:disable-next-line:max-line-length
                                var status_action = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION_STATUS." + operationStatus);
                                var message = feat + " " + status_action;
                                tempObj_2[viewItem.name] = message;
                                tempObj_2["featureName"] = feat;
                                tempObj_2["operationName"] = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION." + item[viewItem.value].action);
                                tempObj_2["typeName"] = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.TYPE." + item[viewItem.value].type);
                            }
                            if (currentTableValue.loadLabelFromConfig) {
                                tempObj_2[viewItem.name] =
                                    currentTableValue.headerList[item[viewItem.value]];
                            }
                        });
                        var filterObj = {};
                        filterObj[currentTableValue.filterKey] = item[currentTableValue.dataFilterKey];
                        var selectedValue = _.find(self.inputData[currentTableValue.selectedValuesKey], filterObj);
                        console.log(selectedValue, ">>>>>selectedValue");
                        if (selectedValue) {
                            item.checked = true;
                        }
                        tempObj_2 = tslib_1.__assign({}, item, tempObj_2);
                        tempArray.push(tempObj_2);
                    }
                });
                console.log("", tempArray);
                if (tempArray && tempArray.length) {
                    _this.tableData = tempArray;
                }
                else {
                    _this.tableData =
                        data && data.response && data.response[responseName]
                            ? (data.response[responseName])
                            : [];
                }
                if (_this.groupField && _this.groupField.length) {
                    _this.constructGroupedData(data.response, responseName);
                }
                if (data && data.response) {
                    _this.length = data.response.total;
                }
                console.log(">>>(this.tableData ", _this.tableData);
                _this.inputData["selectedData"] = (_this.inputData["selectedData"] && _this.inputData["selectedData"].length > 0) ? (_this.inputData["selectedData"]) : [];
                _this.inputData["selectedIdList"] = (_this.inputData["selectedIdList"] && _this.inputData["selectedIdList"].length > 0) ? (_this.inputData["selectedIdList"]) : [];
                _this.selectedData = _this.inputData["selectedIdList"];
                _this.inputData["selectAll"] = (_this.selectedData.length >= _this.limit) ? true : false;
                _this.newTableData = process(_this.tableData, { group: _this.groupField });
                _this.newTableData.total = _this.length;
                _this.dataSource = new MatTableDataSource(_this.tableData);
                _this.dataSource.paginator = _this.selectedPaginator; // newly added 
                localStorage.setItem("currentInput", JSON.stringify(_this.inputData));
                // this.inputData["selectAll"] = false;
                var filteredKey = (_this.currentFilteredValue.searchKey) ? _this.currentFilteredValue.searchKey : '';
                _.forEach(self.columns, function (x) {
                    if (filteredKey == x.value) {
                        self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                    }
                    else {
                        self.inputData[x.keyToSave] = null;
                    }
                });
                // this.loadKendoGrid();
                // this.selectAllCheck({ checked: false });
                // this.selectAll = false;
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.inputData["selectedData"] = [];
                // this.inputData["selectedIdList"] = [];
                // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.changeDetectorRef.detectChanges();
                _this.checkClickEventMessage.emit("clicked");
            }, function (err) {
                _this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
    };
    //   loadKendoGrid(){
    //     $(document).ready(function() {
    //       (<any>$("#grid")).kendoGrid({
    //           dataSource: {
    //               pageSize: 10,
    //               transport: {
    //                   read: {
    //                       url: "https://demos.telerik.com/kendo-ui/service/Products",
    //                       dataType: "jsonp"
    //                   }
    //               },
    //               schema: {
    //                   model: {
    //                       id: "ProductID"
    //                   }
    //               }
    //           },
    //           pageable: true,
    //           scrollable: false,
    //           persistSelection: true,
    //           sortable: true,
    //           // change: onChange,
    //           columns: [{
    //                   selectable: true,
    //                   width: "50px"
    //               },
    //               {
    //                   field: "ProductName",
    //                   title: "Product Name"
    //               },
    //               {
    //                   field: "UnitPrice",
    //                   title: "Unit Price",
    //                   format: "{0:c}"
    //               },
    //               {
    //                   field: "UnitsInStock",
    //                   title: "Units In Stock"
    //               },
    //               {
    //                   field: "Discontinued"
    //               }
    //           ]
    //       });
    //       var grid = $("#grid").data("kendoGrid");
    //       // grid.thead.on("click", ".k-checkbox", onClick);
    //   });
    //       // //bind click event to the checkbox
    //       // grid.table.on("click", ".k-checkbox" , selectRow);
    //       // $('#header-chb').change(function(ev){
    //       //   var checked = ev.target.checked;
    //       //   $('.k-checkbox').each(function(idx, item){
    //       //     if(checked){
    //       //       if(!($(item).closest('tr').is('.k-state-selected'))){
    //       //         $(item).click();
    //       //       }
    //       //     } else {
    //       //       if($(item).closest('tr').is('.k-state-selected')){
    //       //         $(item).click();
    //       //       }
    //       //     }
    //       //   });
    //       // });
    //       // $("#showSelection").bind("click", function () {
    //       //   var checked = [];
    //       //   for(var i in checkedIds){
    //       //     if(checkedIds[i]){
    //       //       checked.push(i);
    //       //     }
    //       //   }
    //         // alert(checked);
    //       // });
    //     // });
    // }
    NewTableLayoutComponent.prototype.getLoadData = function (currentValue, responseValue, totalLength, responseKey) {
        console.log(currentValue, "....currentValue");
        var tempArray = [];
        // if (responseValue && responseValue[responseKey]) {
        responseValue = responseKey ? responseValue[responseKey] : responseValue;
        if (responseValue && responseValue.length) {
            _.forEach(responseValue, function (item, index) {
                if (typeof item !== "object") {
                    var tempObj = {};
                    tempObj[currentValue.keyToSet] = item;
                    tempArray.push(tempObj);
                }
                else {
                    var tempObj_3 = {};
                    _.forEach(currentValue.dataViewFormat, function (viewItem) {
                        if (viewItem.subkey) {
                            if (viewItem.assignFirstIndexval) {
                                var resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                tempObj_3[viewItem.name] = item[viewItem.value]
                                    ? resultValue
                                    : "";
                            }
                            else {
                                tempObj_3[viewItem.name] = item[viewItem.value]
                                    ? item[viewItem.value][viewItem.subkey]
                                    : "";
                            }
                        }
                        else {
                            tempObj_3[viewItem.name] = item[viewItem.value];
                        }
                    });
                    tempObj_3 = tslib_1.__assign({}, item, tempObj_3);
                    tempArray.push(tempObj_3);
                }
            });
        }
        this.columns = currentValue.tableHeader;
        responseValue = tempArray.length ? tempArray : responseValue;
        console.log(this.inputData, "...INPUTDATA");
        if (currentValue && currentValue.mapDataFunction) {
            var self_4 = this;
            _.forEach(currentValue.mapDataFunction, function (mapItem) {
                _.forEach(mapItem.requestData, function (requestItem) {
                    self_4.queryParams[requestItem.name] = requestItem.convertToString
                        ? JSON.stringify(self_4.inputData[requestItem.value])
                        : self_4.inputData[requestItem.value];
                });
                self_4.contentService
                    .getAllReponse(self_4.queryParams, mapItem.apiUrl)
                    .subscribe(function (data) {
                    console.log(mapItem, ">>>>>>> MAP ITEM");
                    console.log(data, ".....DATAAAA");
                    var tempObj = _.keyBy(data.response[mapItem.response], "_id");
                    console.log(tempObj, "....tempObj");
                    _.forEach(responseValue, function (responseItem) {
                        if (data.response &&
                            data.response.keyForCheck == mapItem.keyForCheck &&
                            responseItem[mapItem.keyForCheck]) {
                            responseItem[mapItem.showKey] =
                                tempObj && responseItem[mapItem.keyForCheck]
                                    ? tempObj[responseItem[mapItem.keyForCheck]].objectTypes
                                    : [];
                            responseItem[mapItem.modelKey] = responseItem[mapItem.showKey];
                        }
                        else
                            console.log(responseItem, ".....responseItem");
                    });
                    console.log(responseValue, ">>>>>responseValue");
                });
            });
        }
        else {
            this.dataSource = new MatTableDataSource(responseValue);
            this.length = totalLength ? totalLength : responseValue.length;
        }
        this.tableData = tempArray;
        this.dataSource = new MatTableDataSource(responseValue);
        this.length = totalLength ? totalLength : responseValue.length;
        this.newTableData = process(this.tableData, { group: this.groupField });
        this.newTableData.total = this.length;
        console.log(this.columns, ">>>>>COLUMNSS");
        // this.displayedColumns = _.map(this.columns, "value");
        var selectedTableHeaders = localStorage.getItem("selectedTableHeaders"); // after global setting change
        this.columns = !_.isEmpty(selectedTableHeaders) ? JSON.parse(selectedTableHeaders) : this.columns;
        this.displayedColumns = this.columns
            .filter(function (val) {
            return val.isActive;
        });
        this.dataSource.paginator = this.selectedPaginator;
        console.log(this.selectedPaginator, ".................... selectedPaginator");
        console.log(this.dataSource, ".........DATASOURCEEEE");
        // this.dataSource = responseKey
        //   ? new MatTableDataSource(responseValue[responseKey])
        //   : new MatTableDataSource(responseValue);
    };
    NewTableLayoutComponent.prototype.updateCheckClick = function (selected, event) {
        // selected.type = this.currentTableData.tableType
        //   ? this.currentTableData.tableType
        //   : "";
        if (this.currentTableData && this.currentTableData.tableType) {
            selected.type = this.currentTableData.tableType;
        }
        // selected.typeId = this.currentTableData.typeId
        //   ? this.currentTableData.typeId
        //   : "";
        var securityTypeLength = this.currentTableData.securityTypeLength ? Number(this.currentTableData.securityTypeLength) : 3;
        console.log(">>> securityTypeLength ", securityTypeLength);
        // if (selected && selected.typeId) {
        //   if (selected.typeId == "2") {
        //     selected["security_type"] = [4];
        //     selected["level"] = [4];
        //   } else {
        //     selected["security_type"] = [1, 2, 3];
        //     selected["level"] = [1, 2, 3];
        //    }
        var tempArray = [];
        var predefinedArray = this.currentTableData.securityArrayValue ? JSON.parse("[" + this.currentTableData.securityArrayValue + "]") : tempArray;
        console.log(">>> predefinedArray ", predefinedArray);
        if (securityTypeLength && securityTypeLength > 0) {
            if (predefinedArray && predefinedArray.length) {
                tempArray = predefinedArray;
            }
            else {
                for (var i = 1; i <= securityTypeLength; i++) {
                    tempArray.push(i);
                }
            }
            selected["security_type"] = tempArray;
        }
        else {
            if (securityTypeLength == 0) {
                selected["security_type"] = tempArray;
            }
            else {
                selected["security_type"] = [1, 2, 3];
            }
        }
        //}
        console.log(">>>  selected security_type ", selected["security_type"]);
        if (this.currentTableData && this.currentTableData.dataFormatToSave) {
            _.forEach(this.currentTableData.dataFormatToSave, function (item) {
                selected[item.name] = selected[item.value];
            });
        }
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        var savedSelectedData = this.inputData.selectedData && this.inputData.selectedData.length
            ? this.inputData.selectedData
            : [];
        this.selectedData = _.merge(savedSelectedData, this.selectedData);
        if (event.checked) {
            this.selectedData.push(selected);
        }
        else {
            var index = this.selectedData.findIndex(function (obj) { return obj.object_name === selected.object_name; });
            this.selectedData.splice(index, 1);
        }
        this.inputData["selectedData"] = this.selectedData;
        if (this.currentTableData && this.currentTableData.saveInArray) {
            var arrayObj = this.currentTableData.saveInArray;
            if (this.currentTableData.tableId === arrayObj.key) {
                var tempArray_1 = this.selectedData || this.selectedData.length
                    ? _.map(this.selectedData, arrayObj.valueToMap)
                    : [];
                tempArray_1 = tempArray_1.filter(function (element) {
                    return element != null;
                });
                this.inputData[arrayObj.key] = tempArray_1;
            }
        }
        this.inputData["selectedIdList"] = _.map(this.selectedData, "_id");
        console.log("Selected Id Data List >>>>>>>", this.inputData["selectedIdList"]);
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    };
    NewTableLayoutComponent.prototype.onSelectedKeysChange = function (e) {
        console.log(">>>> onSelectedKeysChange ", e);
        var len = this.selectedData.length;
        this.selectCount = this.selectedData.length;
        console.log(">>> len ", len, "total records ", this.newTableData.data.total, " this.length ", this.length, " this.limit ", this.limit, " >>> selectCount ", this.selectCount);
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        var selectedIds = e;
        if (len === 0) {
            this.selectAllItem = "unchecked";
            this.enableDelete = false;
            this.inputData["selectAll"] = false;
        }
        else if (len > 0 && len < this.length) {
            this.selectAllItem = "indeterminate";
            this.enableDelete = true;
            // this.inputData["selectAll"] = (this.newTableData.data.length === this.limit)?true:false;
            this.inputData["selectAll"] = (len >= this.limit) ? true : false;
        }
        else {
            this.selectAllItem = "checked";
            this.enableDelete = true;
            this.inputData["selectAll"] = true;
        }
        if (this.selectedData.length > 0) {
            // current selection
            var tempData_1 = [];
            var self = this;
            _.forEach(selectedIds, function (tableItem) {
                var indexs = self.newTableData.data.filter(function (obj) { return obj["_id"] == tableItem; });
                console.log(">>>> ndexs.length ", indexs.length);
                if (indexs.length > 0) {
                    tempData_1.push(indexs[0]);
                }
            });
            this.selectCount = tempData_1.length;
            this.inputData["selectedData"] = _.concat(this.inputData["selectedData"], tempData_1);
            this.inputData["selectedData"] = _.uniqBy(this.inputData["selectedData"], "_id");
            selectedIds = _.concat(this.selectedData, selectedIds); // total selected ids
        }
        this.selectedData = _.uniq(selectedIds);
        this.inputData["selectedIdList"] = this.selectedData;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    };
    NewTableLayoutComponent.prototype.onSelectAllChange = function (checkedState) {
        console.log("selectAll -> ", checkedState);
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        if (checkedState === 'checked') {
            this.newTableData.data.map(function (obj) {
                obj.checked = true;
            });
            this.inputData["selectedData"] = this.newTableData.data;
            this.inputData["selectedIdList"] = _.map(this.newTableData.data, "_id");
            this.inputData["selectAll"] = true;
            this.selectAllItem = 'checked';
            this.enableDelete = true;
        }
        else {
            this.selectedData = [];
            this.newTableData.data.map(function (obj) {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.inputData["selectAll"] = false;
            this.selectAllItem = 'unchecked';
            this.enableDelete = false;
        }
        var selectedIds = this.inputData["selectedIdList"];
        if (this.selectedData.length > 0) {
            selectedIds = _.concat(this.selectedData, selectedIds);
        }
        this.selectedData = _.uniq(selectedIds);
        this.selectCount = this.selectedData.length;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    };
    NewTableLayoutComponent.prototype.selectAllCheck = function (event) {
        console.log(event);
        if (event.checked === true) {
            this.dataSource.data.map(function (obj) {
                obj.checked = true;
            });
            this.inputData["selectedData"] = this.dataSource.data;
            this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
            console.log("Check Event All Id List >>>>>", this.inputData["selectedIdList"]);
            this.enableDelete = true;
            this.selectCount = this.inputData["selectedIdList"].length;
            this.selectedData.push(this.inputData["selectedIdList"]);
        }
        else {
            this.dataSource.data.map(function (obj) {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.selectedData = [];
            this.enableDelete = false;
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    };
    NewTableLayoutComponent.prototype.addFieldClick = function (item, objectList) {
        console.log(item, "..........ITEM");
        console.log(this.inputData, ".......INPUT DAta");
        console.log(objectList, "......ObjectList");
    };
    NewTableLayoutComponent.prototype.selectAllData = function () {
        var _this = this;
        console.log("Select all Data >>>>>");
        this.selectAllItem = 'checked';
        var requestDetails = this.currentData.selectAllRequest;
        var query = {};
        var self = this;
        _.forEach(requestDetails.requestData, function (item) {
            query[item.name] = self.inputData[item.value];
        });
        this.contentService
            .getAllReponse(query, requestDetails.apiUrl)
            .subscribe(function (data) {
            _this.inputData["selectedIdList"] =
                data.response[requestDetails.responseName];
            _this.selectedData = _this.inputData["selectedIdList"];
            console.log("Selected Id list @@@@@@@@@", _this.selectedData);
            localStorage.setItem("currentInput", JSON.stringify(_this.inputData));
        });
    };
    NewTableLayoutComponent.prototype.clearSelectAllData = function () {
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.inputData["selectedIdList"] = [];
        this.inputData["selectedData"] = [];
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.enableDelete = false;
        this.ngOnInit();
    };
    NewTableLayoutComponent.prototype.applyFilter = function (event) {
        console.log(">>>>>>>>>> event ", event);
        if (event && event.filters) {
            var filterValue = event.filters ? event.filters[0] : {};
            this.currentFilteredValue = {
                searchKey: filterValue.field,
                searchValue: filterValue.value
            };
            this.inputData[filterValue.field] = filterValue.value;
            console.log(this.inputData, ">>>>> INPUT DATA");
            var tableArray = this.onLoad && this.onLoad.tableData
                ? this.onLoad.tableData
                : this.currentConfigData["listView"].tableData;
            var index = _.findIndex(tableArray, { tableId: this.tableId });
            var searchRequest = tableArray[index].onTableSearch;
            var request = searchRequest.requestData;
            var isUiserach = (searchRequest.uiSearch) ? true : false;
            if (isUiserach) {
                this.dataSource.filter = event;
            }
            else {
                this.currentTableData = tableArray[index];
                var query = {
                    offset: 0,
                    limit: 10,
                    datasource: this.defaultDatasource
                };
                if (searchRequest && searchRequest.isSinglerequest) {
                    query[searchRequest.singleRequestKey] = event;
                }
                else {
                    var self = this;
                    _.forEach(request, function (item) {
                        var tempData = item.subKey
                            ? self.inputData[item.value][item.subKey]
                            : self.inputData[item.value];
                        if (item.directAssign || item.isDefault) {
                            self.queryParams[item.name] = item.convertToString
                                ? JSON.stringify(item.value)
                                : item.value;
                        }
                        else if (tempData) {
                            self.queryParams[item.name] = item.convertToString
                                ? JSON.stringify(tempData)
                                : tempData;
                        }
                    });
                }
                // this.queryParams = query;
                this.getData(this.currentTableData, "tableSearch");
            }
        }
    };
    NewTableLayoutComponent.prototype.filterReset = function (field) {
        delete this.queryParams[field.requestKey];
        this.ngOnInit();
    };
    NewTableLayoutComponent.prototype.onMultiSelect = function (event, rowVal) {
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        var selectedData = this.inputData.selectedData;
        var index = _.findIndex(selectedData, { obj_name: rowVal.obj_name });
        if (index > -1 && selectedData && selectedData.length) {
            this.inputData.selectedData[index].security_type = event.value;
            this.inputData.selectedData[index].level = event.value;
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    };
    NewTableLayoutComponent.prototype.changePage = function (event) {
        console.log(event, ">>> EVENT");
        // if (event.pageSize != this.limit) {
        //   this.queryParams["limit"] = event.pageSize;
        //   this.queryParams["offset"] = 0;
        // } else {
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
        // }
        this.onLoadData("next");
    };
    NewTableLayoutComponent.prototype.viewData = function (element, col) {
        console.log(col, "viewData >>>>>>>>> isRedirect ", col.isRedirect, this.environment);
        if (col.isRedirect) {
            var testStr = "/control-management/access-control";
            window.location.href = "" + this.redirectUri + testStr;
            console.log("Redirected ********************");
            // this.route.navigateByUrl("/control-management/access-control");
        }
        else {
            this.actionClick(element, "view");
        }
    };
    NewTableLayoutComponent.prototype.addButtonClick = function (element, action) {
        console.log("addButtonClick ");
        console.log(" element ", element);
        console.log(" action ", action);
    };
    NewTableLayoutComponent.prototype.actionClick = function (element, action) {
        var _this = this;
        console.log(element, ">>>>element");
        if (action == "edit" || action == "view" || action == "visibility" || action == "info") {
            action = (action == 'visibility' || action == "info") ? 'view' : action;
            this.openDialog(element, action);
        }
        else if (action == "delete") {
            console.log(this, ">>>>>>>this");
            if (this.onLoad && this.onLoad.inputKey) {
                var inputKey = this.onLoad.inputKey;
                if (this.inputData && this.inputData[inputKey]) {
                    var selectedData = this.inputData[inputKey];
                    var index = selectedData.findIndex(function (obj) { return obj == element; });
                    selectedData.splice(index, 1);
                    this.inputData[inputKey] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    // this.actionClickEvent.emit("clicked");
                }
            }
            else {
                if (this.inputData && this.inputData.selectedData) {
                    var selectedData = this.inputData.selectedData;
                    var index = selectedData.findIndex(function (obj) { return obj.object_name == element.object_name; });
                    selectedData.splice(index, 1);
                    this.inputData["selectedData"] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    // this.actionClickEvent.emit("clicked");
                }
            }
        }
        else if (action == "remove_red_eye") {
            this.openDialog(element, "view");
        }
        else if (action == "restore_page") {
            this.openDialog(element, action);
        }
        else if (action == 'askdefaultbutton') {
            var dsid_1 = element._id;
            var dsname = element.name;
            console.log(" dsid ", dsid_1, " dsname ", dsname);
            var self = this;
            var requestDetails = this.currentTableData;
            var queryObj = {
                defaultDatasource: true
            };
            var toastMessageDetails_1 = requestDetails.setDefault.toastMessage;
            self.contentService
                .updateRequest(queryObj, requestDetails.setDefault.apiUrl, dsid_1)
                .subscribe(function (res) {
                _this.messageService.sendDatasource(dsid_1);
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_1.success));
                _this.ngOnInit();
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_1.error));
            });
            // this.messageService.sendDatasource(dsid);
            // this.snackBarService.add(
            //   this._fuseTranslationLoaderService.instant(
            //     "Datasource Default Updated Successfully!"
            //   )
            // );
            this.inputData.datasourceId = dsid_1;
        }
        else if (action == "save_alt") {
            var requestDetails = this.currentData.downloadRequest;
            var query = {
                reportId: element._id
            };
            this.contentService
                .getAllReponse(query, requestDetails.apiUrl)
                .subscribe(function (data) {
                var ab = new ArrayBuffer(data.data.length);
                var view = new Uint8Array(ab);
                for (var i = 0; i < data.data.length; i++) {
                    view[i] = data.data[i];
                }
                var downloadType = 'application/zip';
                var file = new Blob([ab], { type: downloadType });
                FileSaver.saveAs(file, element.fileName + '.zip');
            });
        }
        else if (action == "speaker_notes" || action == "speaker_notes_off") {
            var requestDetails = this.currentData.notificationRead;
            var idsList = [];
            idsList.push(element._id);
            // if (action == "notdelete") {
            // queryObj["delete"] = true;
            //  let queryObj = {
            //     isread: true,
            //     feature: "delete",
            //     idList: idsList
            //   }
            // }
            var queryObj = {
                isread: (action == "speaker_notes") ? true : false,
                feature: "isread",
                idList: idsList
            };
            var self = this;
            var toastMessageDetails_2 = requestDetails.toastMessage;
            this.contentService.createRequest(queryObj, requestDetails.apiUrl)
                .subscribe(function (res) {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.success));
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.error));
            });
            this.ngOnInit();
        }
        else if (action == "notdelete") {
            var requestDetails = this.currentData.notificationRead;
            var idsList = [];
            idsList.push(element._id);
            var queryObj = {
                feature: "delete",
                idList: idsList,
                isread: true
            };
            var self = this;
            var toastMessageDetails_3 = requestDetails.toastMessage;
            this.contentService.createRequest(queryObj, requestDetails.apiUrl)
                .subscribe(function (res) {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_3.success));
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_3.error));
            });
            this.ngOnInit();
        }
    };
    NewTableLayoutComponent.prototype.updateStatus = function (actionName, selectedId) {
        var _this = this;
        var query = {};
        if (this.currentTableData && this.currentTableData.statusUpdate) {
            var currentRequestDetails = this.currentTableData.statusUpdate;
            var toastMessageDetails_4 = currentRequestDetails.toastMessage;
            query["updateId"] = selectedId._id;
            query["status"] = actionName;
            this.contentService
                .updateRequest(query, currentRequestDetails.apiUrl, selectedId._id)
                .subscribe(function (res) {
                _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails_4.success));
                _this.ngOnInit();
            }, function (error) {
                _this.snackBarService.warning(_this._fuseTranslationLoaderService.instant(toastMessageDetails_4.error));
            });
        }
    };
    NewTableLayoutComponent.prototype.actionRedirect = function (element, columnData) {
        var _this = this;
        console.log("actionRedirect >>>>>> ", element);
        var self = this;
        var requestDetails = this.currentTableData;
        var queryObj = {};
        var toastMessageDetails = requestDetails.onTableUpdate.toastMessage;
        console.log("requestDetails >>>>>> ", requestDetails);
        if (columnData.rulesetCheck) {
            _.forEach(requestDetails.onTableUpdate.requestData, function (item) {
                queryObj[item.name] = item.subkey
                    ? element[item.value][item.subkey]
                    : element[item.value];
            });
        }
        else {
            queryObj = {
                defaultDatasource: true
            };
            console.log("data source >>>>");
        }
        self.contentService
            .updateRequest(queryObj, requestDetails.onTableUpdate.apiUrl, element._id)
            .subscribe(function (res) {
            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            if (!columnData.rulesetCheck) {
                _this.messageService.sendDatasource(element._id);
            }
            _this.ngOnInit();
        }, function (error) {
            self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
        });
        console.log('actionRedirect >>>>>>>>>>>>>>> queryObj ', queryObj);
        console.log("Query Obj >>>", queryObj);
        console.log("Update URL >>>", requestDetails.onTableUpdate.apiUrl);
    };
    NewTableLayoutComponent.prototype.onToggleChange = function (event, element) {
        console.log(">>>>>>>>>>>> event ", event, " :element ", element);
        element.status = event.checked ? "ACTIVE" : "INACTIVE";
        var requestDetails = this.currentTableData
            ? this.currentTableData.onToggleChange
            : "";
        if (requestDetails) {
            var toastMessageDetails_5 = requestDetails.toastMessage;
            var self = this;
            var queryObj_1 = {};
            _.forEach(requestDetails.requestData, function (item) {
                // queryObj[item.name] = item.subkey
                //   ? element[item.value][item.subkey]
                //   : element[item.value];
                if (item.subkey) {
                    queryObj_1[item.name] = element[item.value][item.subkey];
                }
                else if (item.fromTenantData) {
                    queryObj_1[item.name] = self.currentTenant[item.value];
                }
                else {
                    queryObj_1[item.name] = element[item.value];
                }
            });
            console.log('Toggle URL >>>', requestDetails.apiUrl);
            self.contentService
                .updateRequest(queryObj_1, requestDetails.apiUrl, element._id)
                .subscribe(function (res) {
                if (element.status == 'ACTIVE' && toastMessageDetails_5.enable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_5.enable));
                }
                else if (element.status == 'INACTIVE' && toastMessageDetails_5.disable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_5.disable));
                }
                else {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_5.success));
                }
                if (requestDetails.isPageRefresh) {
                    setTimeout(function () {
                        self.onLoadData(null);
                    }, 100);
                }
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_5.error));
            });
        }
        else if (this.currentTableData && this.currentTableData.saveselectedToggle) {
            this.inputData[this.currentTableData["keyTosave"]] = this.dataSource.data;
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    };
    NewTableLayoutComponent.prototype.checkConfirm = function (element, action, column) {
        this.openConfirmDialog(element, action, column);
    };
    NewTableLayoutComponent.prototype.openConfirmDialog = function (element, action, column) {
        var _this = this;
        console.log(" openConfirmDialog Dialog ********* element: ", element, " **** action ", action, ">>> column ", column);
        var modelWidth = this.currentConfigData[action].modelData.size;
        var modelData = this.currentConfigData[action].modelData;
        console.log(">>> modelWidth ", modelWidth);
        this.dialogRef = this._matDialog
            .open(ConfirmDialogComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                savedData: element,
                modelData: modelData
            }
        })
            .afterClosed()
            .subscribe(function (response) {
            console.log(">>>> Confirm modal response ", response);
            if (response && column.redirectAction) {
                _this.openDialog(element, column.redirectAction);
            }
            else {
                localStorage.removeItem("currentInput");
                _this.messageService.sendModelCloseEvent("listView");
            }
        });
    };
    NewTableLayoutComponent.prototype.openDialog = function (element, action) {
        var _this = this;
        console.log(" Open Dialog ********* element: ", element);
        var modelWidth = this.currentConfigData[action].modelData.size;
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                savedData: element
            }
        })
            .afterClosed()
            .subscribe(function (response) {
            localStorage.removeItem("currentInput");
            _this.messageService.sendModelCloseEvent("listView");
        });
    };
    NewTableLayoutComponent.prototype.onSelectOptionbackup = function (item) {
        var self = this;
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        if (item.value == 'none') {
            _.forEach(self.newTableData.data, function (tableItem) {
                tableItem.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.inputData["selectAll"] = false;
            this.enableDelete = false;
        }
        else {
            if (item.value != 'all') {
                self.queryParams[item.name] = item.value;
                console.log("Select Value >>>>>>>>>", item);
                if (item.keyToShow == "Read") {
                    self.currentData.isRead = true;
                }
                else {
                    self.currentData.isRead = false;
                }
            }
            this.inputData["selectAll"] = true;
            this.enableDelete = true;
            this.getData(this.currentTableData, null);
        }
        console.log(">>> this.inputData ", this.inputData);
    };
    NewTableLayoutComponent.prototype.onSelectOption = function (item) {
        console.log("Select Value >>>>>>>>>", item);
        var existCheck = _.has(this.queryParams, item.name);
        if (item.alternativeKey == 'unread') {
            delete this.queryParams["unread"];
            // _.omit(this.queryParams, this.queryParams["unread"]);
        }
        else {
            delete this.queryParams["isread"];
            // _.omit(this.queryParams, this.queryParams["isread"]);
        }
        console.log("Query Params >>", this.queryParams);
        if (this.queryParams["operation"] != item.name) {
            this.selectedData = [];
            this.newTableData.data.map(function (obj) {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.inputData["selectAll"] = false;
            this.selectAllItem = 'unchecked';
            this.enableDelete = false;
        }
        this.queryParams["operation"] = item.name;
        this.queryParams[item.name] = item.value;
        this.enableDelete = false;
        this.getData(this.currentTableData, null);
    };
    NewTableLayoutComponent.prototype.DeleteOption = function () {
        var _this = this;
        var self = this;
        var requestDetails = this.currentData.notificationRead;
        var toastMessageDetails = requestDetails.toastMessage;
        // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        var input = this.inputData["selectedIdList"];
        console.log("Input data >>>>", input);
        var queryObj = {
            feature: "delete",
            idList: input
        };
        if (input.length > 0) {
            this.contentService.createRequest(queryObj, requestDetails.apiUrl)
                .subscribe(function (res) {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                _this.ngOnInit();
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                _this.ngOnInit();
            });
        }
    };
    NewTableLayoutComponent.prototype.Read = function () {
        var _this = this;
        var self = this;
        // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        var requestDetails = this.currentData.notificationRead;
        var input = this.inputData["selectedIdList"];
        console.log("Input data >>>>", input);
        var queryObj = {
            isread: true,
            feature: "isread",
            idList: input
        };
        var toastMessageDetails = requestDetails.toastMessage;
        this.contentService.createRequest(queryObj, requestDetails.apiUrl)
            .subscribe(function (res) {
            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            _this.ngOnInit();
        }, function (error) {
            self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            _this.ngOnInit();
        });
    };
    NewTableLayoutComponent.prototype.Unread = function () {
        var _this = this;
        var self = this;
        // this.inputData["selectedIdList"]
        // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        var requestDetails = this.currentData.notificationRead;
        var input = this.inputData["selectedIdList"];
        console.log("Selected ID List of Input >>>", input);
        var toastMessageDetails = requestDetails.toastMessage;
        var queryObj = {
            isread: false,
            feature: "isread",
            idList: input
        };
        this.contentService.createRequest(queryObj, requestDetails.apiUrl)
            .subscribe(function (res) {
            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            _this.ngOnInit();
        }, function (error) {
            self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            _this.ngOnInit();
        });
    };
    NewTableLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "new-table-layout",
                    template: "<div class=\"mat-elevation-z8 sen-margin-10\" [ngClass]=\"currentConfigData.listView.inTable\">\r\n  <!-- <button class=\"k-button\" (click)=\"collapseGroups(grid)\">Collapse All</button>\r\n      <br /><br /> -->\r\n  <!-- <ng-container *ngIf=\"currentData.enableMultiSelect\">\r\n      <div class=\"col-12 sent-lib-border-btm\">\r\n        <div class=\"row\">\r\n          <div class=\"col-4\">\r\n            <div class=\"btn-group\" dropdown >\r\n            <mat-checkbox class=\"sen-select-box\" (change)=\"selectAllCheck($event)\" >\r\n          </mat-checkbox>\r\n            <mat-form-field  class=\"btn btn-default\">  \r\n                <mat-select (selectionChange)=\"onSelectOption($event)\"\r\n                [(ngModel)]=\"selectAllValue\">\r\n                <mat-option *ngFor=\"let select of selectOption;\" [value]=\"select.value\">{{select.name}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <ng-container *ngIf=\"enableDelete == true\">\r\n            <span class=\"material-icons sen-lib-delete\" (click)=\"DeleteOption()\" matTooltip=\"Delete\"> delete </span>\r\n            <mat-icon class=\"material-icons sen-lib-delete\" (click)=\"Read()\" matTooltip=\"Mark as Read\">mark_chat_read\r\n            </mat-icon>\r\n            <mat-icon class=\"material-icons sen-lib-delete\" (click)=\"Unread()\" matTooltip=\"Mark as Unread\">\r\n              mark_chat_unread\r\n            </mat-icon>\r\n\r\n          </ng-container>\r\n\r\n        </div>\r\n        <div class=\"col-6\" *ngIf=\"enableDelete == true\">\r\n          <span class=\"sen-lib-iconslist-content\">\r\n            All {{selectCount}} notifications on this page are selected.\r\n          </span>\r\n          <span class=\"sen-conversation sen-lib-iconslist-content\" (change)=\"selectAllCheck($event)\"> Select all\r\n            {{length}} notifications</span>\r\n\r\n        </div>\r\n\r\n      </div>\r\n  </ng-container> \r\n\r\n\r\n-->\r\n  <!-- Started on 12-11-2020 optionfilter-->\r\n  <ng-container *ngIf=\"enableOptionFilter\">\r\n    <div class=\"col-12 sent-lib-border-btm\">\r\n      <div class=\"row\">\r\n        <div class=\"col-12\">\r\n          <div class=\"text-right\" style=\"margin-top:-5px;\">\r\n            <mat-form-field appearance=\"outline\" style=\"width: 18%;margin-top:12px;\">\r\n              <mat-label>Select Option</mat-label>\r\n              <mat-select [(ngModel)]=\"selectAllValue\">\r\n                <mat-option *ngFor=\"let item of currentData.selectOption\" (click)=\"onSelectOption(item)\"\r\n                  value=\"{{item.value | translate}}\">\r\n                  {{item.keyToShow | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <ng-container *ngIf=\"enableDelete == true\">\r\n              <!-- <span class=\"material-icons sen-lib-delete sen-lib-cursor\" (click)=\"DeleteOption()\" matTooltip=\"Delete\">\r\n                delete </span>\r\n              <span class=\"material-icons  sen-lib-chat-read sen-lib-cursor\" (click)=\"Read()\" matTooltip=\"Mark as Read\">\r\n                speaker_notes\r\n              </span>\r\n              <span class=\"material-icons  sen-lib-chat-unread sen-lib-cursor\" (click)=\"Unread()\"\r\n                matTooltip=\"Mark as Unread\">\r\n                speaker_notes_off </span> -->\r\n              <div class=\"btn-group sen-lib-btn-bg-group\" role=\"group\" aria-label=\"Basic example\">\r\n                <button type=\"button\" class=\"btn btn-border-right\" (click)=\"DeleteOption()\">\r\n                  <span class=\"material-icons sen-lib-delete sen-lib-cursor\">\r\n                    delete </span> <span class=\"sen-lib-grp-btn-font\">Delete</span>\r\n                </button>\r\n                <!-- *ngIf=\"(currentData.isRead == false)\" -->\r\n                <button type=\"button\" class=\"btn  \" (click)=\"Read()\">\r\n                  <span class=\"material-icons  sen-lib-chat-read sen-lib-cursor\">\r\n                    speaker_notes\r\n                  </span>\r\n                  <span class=\"sen-lib-grp-btn-font\"> Mark as Read</span>\r\n                </button>\r\n                <button type=\"button\" class=\"btn \" (click)=\"Unread()\">\r\n                  <span class=\"material-icons  sen-lib-chat-unread sen-lib-cursor\">\r\n                    speaker_notes_off </span> <span class=\"sen-lib-grp-btn-font\">Mark as Unread</span>\r\n                </button>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n  <!-- <p>Text: {{currentData.selectAllTex}} - selectAll: {{inputData.selectAll}} - length: {{length}} -idlistLength : {{inputData.selectedIdList.length}} - selectedData len :{{selectedData.length}}- {{selectCount}}</p>  -->\r\n  <ng-container\r\n    *ngIf=\"currentData && currentData.selectAllText && inputData.selectAll && selectedData.length > 9 && length > 10\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 sent-lib-border-btm\" style=\"text-align: center;\">\r\n        <ng-container>\r\n          <div *ngIf=\"selectedData.length != length\">\r\n            <span class=\"m-0\">{{selectedData.length}} {{currentData.afterSelectAllText | translate}}\r\n              <button mat-button color=\"accent\" (click)=\"selectAllData()\">Select All {{length}}\r\n                {{currentData.selectFileText}}</button>\r\n            </span>\r\n          </div>\r\n          <div *ngIf=\"selectedData.length == length\" class=\"sen-conversation\">\r\n            <span class=\"m-0\">\r\n              All {{length}} {{currentData.afterSelectAllText}}\r\n              <button mat-button color=\"accent\" (click)=\"clearSelectAllData()\">Clear Selection</button></span>\r\n          </div>\r\n        </ng-container>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <!-- Ended option filter -->\r\n  <kendo-grid #grid [data]=\"newTableData\" [pageSize]=\"limit\" [skip]=\"offset\" [pageable]=\"true\" [pageable]=\"{\r\n    buttonCount: 10,\r\n    info: info,\r\n    type: type,\r\n    pageSizes: pageSizes,\r\n    previousNext: previousNext\r\n  }\" [filterable]=\"enableFilter\" [sortable]=\"true\" [resizable]=\"true\" [group]=\"groupField\"\r\n    [selectable]=\"{  checkboxOnly: true }\" [kendoGridSelectBy]=\"'_id'\" [selectedKeys]=\"selectedData\"\r\n    (selectedKeysChange)=\"onSelectedKeysChange($event)\"\r\n    [groupable]=\"(currentTableData && currentTableData.enableGrouping) ? true : false\"\r\n    (filterChange)=\"applyFilter($event)\" (pageChange)=\"changePage($event)\" (groupChange)=\"groupChange($event)\"\r\n    (groupCollapse)=\"groupCollapse($event)\">\r\n    <kendo-grid-checkbox-column *ngIf=\"currentData && currentData.selectAllText\"\r\n      [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\" [width]=\"40\" title=\"Select\">\r\n      <ng-template kendoGridHeaderTemplate>\r\n        <span class=\"text-center sen-lib-bx\"> <input class=\" k-checkbox \" id=\"selectAllCheckboxId\"\r\n            kendoGridSelectAllCheckbox [state]=\"selectAllItem\" (selectAllChange)=\"onSelectAllChange($event)\" /></span>\r\n        <label class=\"k-checkbox-label\" for=\"selectAllCheckboxId\"></label>\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem let-rowIndex=\"rowIndex\" >\r\n<div class=\"{{dataItem.status === 'INACTIVE' ? 'k-disabled' : ''}}\">\r\n  <input class=\"ui-lib-checkbox-align\" [kendoGridSelectionCheckbox]=\"rowIndex\" />\r\n</div>\r\n</ng-template>\r\n      <!-- <ng-template kendoGridCellTemplate let-dataItem>\r\n        {{dataItem | json}}\r\n      </ng-template> -->\r\n    </kendo-grid-checkbox-column>\r\n\r\n    <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n      <!-- <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'select' && enableOptionFilter\" title=\"{{column.name | translate}}\" [width]=\"column.width ? column.width:30\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <div class=\"text-center\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"updateCheckClick(dataItem, $event)\"\r\n              [checked]=\"dataItem.checked\">\r\n            </mat-checkbox>\r\n          </div>\r\n        </ng-template>  \r\n      </kendo-grid-column>  -->\r\n      <!-- <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'select'\" title=\"{{column.name | translate}}\" width=\"30\">\r\n      <span *ngIf=\"column.value == 'select'\">\r\n        <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"updateCheckClick(row, $event)\"\r\n          [checked]=\"row.checked\">\r\n        </mat-checkbox>\r\n      </span>\r\n      </kendo-grid-column> -->\r\n\r\n      <!-- <span class=\"view-mode\" (click)=\"viewData(dataItem,column)\">{{dataItem[column.value]}}</span> -->\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.type == 'date'\" field=\"{{column.value}}\" title=\"{{column.name | translate}}\" width=\"130\"\r\n        filter=\"date\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          {{dataItem[column.value] | date: column.dateFormat}}\r\n          <!-- {{dataItem[column.value]}} -->\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.mapFromConfig\" title=\"{{column.name | translate}}\" width=\"100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span>\r\n            {{column.mapData[dataItem[column.value]] | translate}}</span>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'action' || column.value == 'viewInfo'\" title=\"{{column.name | translate}}\" width=\"50\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <ng-container *ngFor=\"let button of column.buttons\">\r\n            <span style=\"cursor: pointer;\" (click)=\"actionClick(dataItem, button)\">\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button=='save_alt' && (dataItem.status=='Completed')\">{{button}}\r\n              </span>\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button=='speaker_notes_off' && (dataItem.isread == true)\">{{button}}\r\n              </span>\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button=='speaker_notes' && (dataItem.isread  ==  false)\">{{button}}\r\n              </span>\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button!='save_alt' && button !='speaker_notes_off' && button !='speaker_notes'\">{{button}}\r\n              </span>\r\n            </span>\r\n          </ng-container>\r\n\r\n          <!-- condition based button start 10-12-20 --->\r\n          <ng-container *ngIf=\"column.isCondition\">\r\n            <button style=\"width: 45% !important;\" *ngFor=\"let button of column.buttonsData\" mat-icon-button\r\n              color=\"accent\" (click)=\"actionClick(dataItem, button.value)\"\r\n              [disabled]=\"button.disableCheck && dataItem[button.disableName]\">\r\n              <span class=\"material-icons sen-lib-icon-size\">{{button.value | translate}}</span>\r\n            </button>\r\n          </ng-container>\r\n          <!-- condition based button end --->\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n\r\n\r\n      <!-- <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n          *ngIf=\"column.value == 'action'\" title=\"{{column.name | translate}}\" width=\"50\">\r\n          <ng-template kendoGridCellTemplate let-dataItem>\r\n            <button style=\"width: 45% !important;\" *ngFor=\"let button of column.buttons\" mat-icon-button color=\"accent\"\r\n              (click)=\"actionClick(dataItem, button)\">\r\n              <mat-icon *ngIf=\"dataItem.isread=='true'\">{{button}}\r\n              </mat-icon>\r\n              <mat-icon *ngIf=\"!dataItem.isread\">{{button}}</mat-icon>\r\n            </button>\r\n          </ng-template>\r\n        </kendo-grid-column> -->\r\n\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'reimport'\" title=\"{{column.name | translate}}\" width=\"100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span class=\"label bg-success ng-star-inserted reimport\"\r\n            *ngIf=\"!column.isCondition ||(column.isCondition && dataItem[column.value]) \"\r\n            (click)=\"actionClick(dataItem,'edit' )\">\r\n            Re-import\r\n          </span>\r\n\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'confirmDialog'\" title=\"{{column.name | translate}}\" width=\"100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span class=\"label bg-success ng-star-inserted reimport\"\r\n            (click)=\"checkConfirm(dataItem,'confirmView',column )\">\r\n            {{column.keyToShow | translate}}\r\n          </span>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value != 'action' && !column.type && column.value != 'select' && column.value != 'reimport' &&  column.value != 'confirmDialog' && !column.mapFromConfig && !column.mapFromConfig && column.value != 'viewInfo'\"\r\n        field=\"{{column.value}}\" title=\"{{column.name | translate}}\" [width]=\"column.width ? column.width:100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span\r\n            *ngIf=\"!dataItem.enableLoad && !column.showTooltip && !column.showStatusToggle && !column.isArryObj && !column.hyperLink\"\r\n            [ngClass]=\"(dataItem.statusClass && dataItem.statusClass[dataItem[column.value]] )? dataItem.statusClass[dataItem[column.value]] : (dataItem.isread == false) ? 'sen-lib-bold' : ''\">\r\n            {{dataItem[column.value]}}\r\n            <button *ngIf=\"column.isViewInfoIcon && dataItem.controlName == 'Multiple Controls'\"\r\n              style=\"width: 0% !important;\" mat-icon-button color=\"accent\" (click)=\"actionClick(dataItem, 'info')\">\r\n              <mat-icon> info\r\n              </mat-icon>\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.isButtonArray\">\r\n            <button *ngFor=\"let button of column.buttonArray\" [ngClass]=\"button.class ? button.class : ''\"\r\n              style=\"margin : 3px;\" (click)=\"updateStatus(button.name, dataItem)\">\r\n              {{button.name}}\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.showTooltip\">\r\n            <button style=\"width: 45% !important;\" [matTooltip]=\"dataItem[column.value]\" mat-icon-button color=\"accent\">\r\n              <mat-icon>{{column.iconName}} </mat-icon>\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.hyperLink\">\r\n            <a style=\"color: #039be5; cursor: pointer;\" (click)=\"onRoutingClick(dataItem)\">\r\n              {{column.hyperLinkLable |translate}}</a>\r\n          </span>\r\n          <!-- <span *ngIf=\"!dataItem.enableLoad && !column.showStatusToggle && !column.isArryObj\"\r\n          [ngClass]=\"(dataItem.statusClass && dataItem.statusClass[dataItem[column.value]] )? dataItem.statusClass[dataItem[column.value]] : ''\">\r\n          {{dataItem[column.value]}} -->\r\n          <!-- </span> -->\r\n          <span *ngIf=\"column.isArryObj\">\r\n            <ng-container *ngFor=\"let data of dataItem[column.keyToShow]; let i=index\">\r\n              {{data[column.subKey] | translate }}\r\n              <span *ngIf=\"dataItem[column.keyToShow].length-1>i\">\r\n                {{column[dataItem[column.keyToCheck]] || \",\"}}\r\n              </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"dataItem.enableLoad && dataItem[column.value] && column.enableLoad\">\r\n            <button mat-raised-button color=\"primary\" (click)=\"loadMoreClick(dataItem)\">Load More</button>\r\n          </span>\r\n          <span *ngIf=\"column.showStatusToggle\">\r\n            <!-- <mat-slide-toggle fxFlex class=\"mat-accent\" aria-label=\"Cloud\" (change)=\"onToggleChange($event, dataItem)\"\r\n              [checked]=\"(dataItem[column.value] == 'ACTIVE' || dataItem[column.value] === true)? true : false\">\r\n            </mat-slide-toggle> -->\r\n            <mat-slide-toggle fxFlex class=\"mat-accent\" aria-label=\"Cloud\" (change)=\"onToggleChange($event, dataItem)\"\r\n              [disabled]=\"column.disableCheck && dataItem[column.disableName]\"\r\n              [checked]=\"(dataItem[column.value] == 'ACTIVE' || dataItem[column.value] === true)? true : false\"\r\n              *ngIf=\"!column.isCondition || (column.isCondition && dataItem[column.conditionName])\">\r\n            </mat-slide-toggle>\r\n          </span>\r\n          <!-- Default Data Source API Update rewrite- 11-12-2020 -->\r\n          <ng-container\r\n            *ngIf=\"column.value == 'status'&& dataItem.type !='AVM' && (column.isCondition && dataItem[column.setasdefaultConName])\">\r\n            &nbsp;&nbsp; <span class=\"label bg-intiated ng-star-inserted reimport \"\r\n              (click)=\"actionRedirect(dataItem,column)\">\r\n              Set as Default\r\n            </span>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"column.value == 'status' && column.isCondition && dataItem[column.defaultConName]\">\r\n            &nbsp;&nbsp; <span class=\"label bg-success ng-star-inserted reimport \">\r\n              Default\r\n            </span>\r\n          </ng-container>\r\n          <!-- Default Data source API  End -->\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n    </ng-container>\r\n  </kendo-grid>\r\n</div>\r\n",
                    styles: [".custom-mat-form{display:inline!important}.tabheade{padding-top:3px}.heading-label{font-size:14px;padding-top:10px}tr.example-detail-row{height:0}tr.example-element-row:not(.example-expanded-row):hover{background:#f5f5f5}tr.example-element-row:not(.example-expanded-row):active{background:#efefef}.res-table{height:100%;overflow-x:auto}.mat-table-sticky{background:#59abfd;opacity:1}.mat-cell,.mat-footer-cell,.mat-header-cell{min-width:80px;box-sizing:border-box}.mat-footer-row,.mat-header-row,.mat-row{min-width:1920px}table{width:100%;padding-top:10px}.mat-form-field-wrapper{padding-bottom:10px}.mat-header-cell{font-weight:700!important;color:#000!important}.selectall-export{text-align:center;margin:0 1%;padding:4px;font-size:14px;border-radius:4px;background:#ececeaab}.multi-select-dropdown{min-width:0;width:180px!important}.selectBoxStyle{font-size:50px!important}.view-mode{color:#039be5;font-size:14px;font-weight:600;cursor:pointer}.selectTH{position:absolute;top:31px}.noRecords{padding:25px 0;color:gray}.reimport{cursor:pointer}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.check_message{display:block;background-color:#ffc;color:#222;padding:3px 8px;font-size:90%;text-align:center;font-family:arial,sans-serif}.sen-select-box{margin-top:22px}.sen-conversation{font-style:initial;color:#039be5;font-weight:700}.sen-lib-delete{font-size:22px;position:relative;color:#fff}.sent-lib-border-btm{box-shadow:0 1px 2px rgba(0,0,0,.1);margin-bottom:10px}.sen-lib-iconslist-content{position:relative;top:22px}.sen-lib-bold{font-weight:700!important}.sen-in-table{margin-top:15px;margin-left:28px!important;margin-right:28px!important}.sen-lib-icon-size{font-size:21px}.sen-lib-chat-read,.sen-lib-chat-unread{font-size:22px;position:relative;color:#fff!important}.sen-lib-cursor{cursor:pointer!important}.sen-lib-grp-btn-font{position:relative;bottom:4px;font-size:13px;color:#fff}.btn-border-right{border-right:2px solid #fff!important}.sen-lib-btn-bg-group{border-radius:29px;margin-left:10px;box-shadow:3px 3px 5px 0 #b3b3b3;background:linear-gradient(135deg,#a2b6df 0,#33569b 100%)}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}"]
                }] }
    ];
    /** @nocollapse */
    NewTableLayoutComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: ContentService },
        { type: MessageService },
        { type: MatDialog },
        { type: SnackBarService },
        { type: ChangeDetectorRef },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] },
        { type: LoaderService }
    ]; };
    NewTableLayoutComponent.propDecorators = {
        viewFrom: [{ type: Input }],
        onLoad: [{ type: Input }],
        tableId: [{ type: Input }],
        disablePagination: [{ type: Input }],
        selectAllBox: [{ type: ViewChild, args: ["selectAllBox",] }],
        table: [{ type: ViewChild, args: ["MatTable",] }],
        checkClickEventMessage: [{ type: Output }],
        actionClickEvent: [{ type: Output }],
        paginator: [{ type: ViewChild, args: ["paginator",] }],
        selectedPaginator: [{ type: ViewChild, args: ["selectedPaginator",] }]
    };
    return NewTableLayoutComponent;
}());
export { NewTableLayoutComponent };
export function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64); // Comment this if not using base64
    var bytes = new Uint8Array(binaryString.length);
    return bytes.map(function (byte, i) { return binaryString.charCodeAt(i); });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3LXRhYmxlLWxheW91dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibmV3LXRhYmxlLWxheW91dC9uZXctdGFibGUtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxTQUFTLEVBQ1QsS0FBSyxFQUNMLE1BQU0sRUFDTixZQUFZLEVBQ1osTUFBTSxFQUVOLGlCQUFpQixFQUNsQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQ0wsWUFBWSxFQUNaLGtCQUFrQixFQUNsQixTQUFTLEVBQ1QsV0FBVyxFQUNYLFFBQVEsRUFFVCxNQUFNLG1CQUFtQixDQUFDO0FBQzNCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUUvRCxPQUFPLEtBQUssU0FBUyxNQUFNLFlBQVksQ0FBQztBQUN4QyxPQUFPLEVBRUwsV0FBVyxFQUlaLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEIsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDNUYsa0RBQWtEO0FBQ2xELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLG9CQUFvQjtBQUNwQix1QkFBdUI7QUFFdkIsd0NBQXdDO0FBRXhDLDZCQUE2QjtBQUU3QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDdkMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVsRCxPQUFPLEVBQStCLE9BQU8sRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBSWxGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBR3BGLG1CQUFtQjtBQUduQixnQ0FBZ0M7QUFDaEM7SUF5RUUsaUNBQ1UsNkJBQTJELEVBQzNELGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLFVBQXFCLEVBQ3JCLGVBQWdDLEVBQ2hDLGlCQUFvQyxFQUNiLFdBQVcsRUFFZixPQUFPLEVBQzFCLGFBQTRCO1FBVnRDLGlCQTRFQztRQTNFUyxrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQzNELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNiLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBRWYsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQTFFN0Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBR2xDLDJCQUFzQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDcEQscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUN4RCx1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsY0FBUyxHQUFVLEVBQUUsQ0FBQztRQUV0QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQVM5QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFFbkIsaUJBQVksR0FBUSxFQUFFLENBQUM7UUFDdkIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQU1aLGdCQUFXLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUNsQyxtQkFBYyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFDckMscUJBQWdCLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUkvQyx5QkFBb0IsR0FBUSxFQUFFLENBQUM7UUFFL0IsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBRXBDLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRXZCLGVBQVUsR0FBc0IsRUFBRSxDQUFDO1FBQzFDLG9CQUFlLEdBQVEsTUFBTSxDQUFDO1FBRzlCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBSWQsU0FBSSxHQUFHLElBQUksQ0FBQztRQUNaLFNBQUksR0FBd0IsU0FBUyxDQUFDO1FBQ3RDLGNBQVMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNuSCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUMzQixhQUFRLEdBQVEsRUFBRSxDQUFDO1FBQ25CLG1CQUFjLEdBQWEsS0FBSyxDQUFDO1FBQzFCLGtCQUFhLEdBQTJCLFdBQVcsQ0FBQztRQUMzRCxtRkFBbUY7UUFDbkYsK0RBQStEO1FBQy9ELHFFQUFxRTtRQUNyRSxrRUFBa0U7UUFDbEUsaUJBQVksR0FBRyxDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO1lBQ2pELEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO1lBQ2pDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFO1lBQ3BDLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztRQWNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDO1FBQ2hELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQjthQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3RDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQTtZQUM3QixLQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztZQUN2QixJQUFJLElBQUksSUFBSSxDQUFDLEVBQUU7Z0JBQ2IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3BDLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQ3JDO1lBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFDLElBQUksRUFBRyxTQUFTLEVBQUMsQ0FBQyxDQUFBO1lBQy9ELEtBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdCLDZDQUE2QztZQUM3QywwQkFBMEI7WUFDMUIsTUFBTTtZQUVOLG9DQUFvQztZQUNwQyx5QkFBeUI7WUFDekIsTUFBTTtZQUNOLHlFQUF5RTtZQUN6RSwyRUFBMkU7WUFDM0UsK0NBQStDO1lBQy9DLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLGdDQUFnQztZQUNoQyxJQUFJLEtBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2IsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztnQkFDRixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7WUFDRCxJQUFJO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsT0FBTztZQUNyRixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBQyxXQUFXLENBQUMsQ0FBQTtZQUNoQyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLEVBQUMsUUFBUSxDQUFDLENBQUE7WUFDMUIsSUFDRSxLQUFJLENBQUMsaUJBQWlCO2dCQUN0QixLQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUTtnQkFDL0IsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxvQkFBb0IsRUFDcEQ7Z0JBQ0EsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsY0FBYzthQUNoQixvQkFBb0IsRUFBRTthQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNqQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUE7WUFDOUIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLFFBQVEsRUFBRTtnQkFDekIsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3pCLEtBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDO2FBQ3hDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0QsMENBQVEsR0FBUjtRQUNFLDBDQUEwQztRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUNwQyxJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQztRQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxLQUFLLEdBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsV0FBVyxHQUFHO1lBQ2pCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLEVBQUU7WUFDVCxVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtZQUNsQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDbEIsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLEdBQUM7WUFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLE1BQU0sRUFBRSxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztTQUN2QyxDQUFBO1FBQ0Qsb0NBQW9DO1FBQ3BDLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEMsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLFVBQVUsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLGFBQWEsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFHLENBQUMsRUFBRTtZQUMxRSxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxZQUFZLEdBQUMsRUFBRSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDeEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRSwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFdBQVcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUV4QixDQUFDO0lBQ0QsaURBQWUsR0FBZjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQ3ZELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDN0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1NBQ3BEO0lBRUgsQ0FBQztJQUNELDZDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCw2Q0FBVyxHQUFYLFVBQVksS0FBSztRQUFqQixpQkEySEM7UUExSEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMvQixJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQztRQUNsRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO1lBQzdELElBQUksV0FBVyxDQUFDLGVBQWUsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7YUFDekU7aUJBQU0sSUFBSSxXQUFXLENBQUMsWUFBWSxFQUFFO2dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTtvQkFDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztvQkFDbkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGVBQWU7b0JBQzlELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsU0FBUztZQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsY0FBYzthQUNoQixhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7YUFDMUQsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUNiLHdEQUF3RDtZQUN4RCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLEdBQUc7WUFDSCxLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUM7WUFDeEQsSUFBSSxvQkFBb0IsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBRSw4QkFBOEI7WUFDeEcsS0FBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2xHLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsT0FBTztpQkFDakMsTUFBTSxDQUFDLFVBQVUsR0FBRztnQkFDbkIsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLElBQUksSUFBSSxHQUFHLEtBQUksQ0FBQztZQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxLQUFLO2dCQUN6RSxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtvQkFDNUIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNqQiw0QkFBNEI7b0JBQzVCLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pCO3FCQUFNO29CQUNMLElBQUksU0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLFVBQVUsUUFBUTt3QkFDaEUsSUFBSSxRQUFRLENBQUMsTUFBTSxFQUFFOzRCQUNuQixJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQ0FDaEMsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dDQUNuSixTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29DQUMzQyxDQUFDLENBQUMsV0FBVztvQ0FDYixDQUFDLENBQUMsRUFBRSxDQUFDOzZCQUNSO2lDQUFNO2dDQUNMLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7b0NBQ3ZDLENBQUMsQ0FBQyxFQUFFLENBQUM7NkJBQ1I7eUJBQ0Y7NkJBQU0sSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFOzRCQUMvQixTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQ25EOzZCQUFNLElBQUksUUFBUSxDQUFDLGlCQUFpQixFQUFFOzRCQUNyQyxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7eUJBQ3pDOzZCQUFNOzRCQUNMLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDL0M7d0JBRUQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUU7NEJBQzdDLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2dDQUNwQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt5QkFDMUQ7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO29CQUNuQixTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ3ZGLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztvQkFDL0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQTtvQkFDaEQsSUFBSSxhQUFhLEVBQUU7d0JBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO3FCQUNyQjtvQkFDRCxTQUFPLHdCQUFRLElBQUksRUFBSyxTQUFPLENBQUUsQ0FBQztvQkFDbEMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFPLENBQUMsQ0FBQztpQkFDekI7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2FBQzVCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxTQUFTO29CQUNaLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDO3dCQUNqRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUM5QyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ1Y7WUFDRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUN6QixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2FBQ25DO1lBQ0QsS0FBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDckUseUVBQXlFO1lBRXpFLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsZUFBZTtZQUNuRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksV0FBVyxHQUFHLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDbkcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQztnQkFDakMsSUFBSSxXQUFXLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQztpQkFDckU7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsd0JBQXdCO1lBQ3hCLDJDQUEyQztZQUMzQywwQkFBMEI7WUFDMUIsbURBQW1EO1lBQ25ELHVDQUF1QztZQUN2Qyx5Q0FBeUM7WUFDekMsd0VBQXdFO1lBQ3hFLG1EQUFtRDtZQUNuRCwwQ0FBMEM7WUFFMUMsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQ0MsVUFBQSxHQUFHO1lBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFFRCxzREFBb0IsR0FBcEIsVUFBcUIsUUFBUSxFQUFFLFlBQVk7UUFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUUsVUFBVSxJQUFJO1lBQzlDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLFFBQVEsRUFBRSxLQUFLO2dCQUM1QyxJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUM7Z0JBQ25CLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDdkYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQztnQkFDakYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQztnQkFDakYsR0FBRyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7Z0JBQ3JHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGVBQWUsQ0FBQztnQkFDckYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2RyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZFLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRztvQkFDbkIsTUFBTSxFQUFFLG1CQUFtQjtvQkFDM0IsUUFBUSxFQUFFLGlCQUFpQjtvQkFDM0IsWUFBWSxFQUFHLGlCQUFpQjtvQkFDaEMsWUFBWSxFQUFFLGtCQUFrQjtvQkFDaEMsVUFBVSxFQUFFLGtCQUFrQjtpQkFDakMsQ0FBQztnQkFDQSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekIsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxFQUFFLElBQUksS0FBSyxJQUFJLEdBQUcsRUFBRTtvQkFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO29CQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQztvQkFDakMsSUFBSSxTQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNqQixTQUFPLENBQUMsaUJBQWlCLENBQUMsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxlQUFlLENBQUM7b0JBQ3pGLFNBQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO29CQUN4QyxTQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztvQkFDeEMsU0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7b0JBQzlDLFNBQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDO29CQUNsRCxTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsaUJBQWlCLENBQUM7b0JBQzNGLFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUM7b0JBQ3JGLFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUM7b0JBQ3JGLFNBQU8sQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLHdCQUF3QixDQUFDO29CQUN6RyxTQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDO29CQUNyRSxTQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUM3QixTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUc7d0JBQ3ZCLE1BQU0sRUFBRSxtQkFBbUI7d0JBQzNCLFFBQVEsRUFBRSxpQkFBaUI7d0JBQzNCLFlBQVksRUFBRyxpQkFBaUI7d0JBQ2hDLFlBQVksRUFBRSxrQkFBa0I7d0JBQ2hDLFVBQVUsRUFBRSxrQkFBa0I7cUJBQ2pDLENBQUM7b0JBQ0EsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFVBQVUsUUFBUTt3QkFDN0QsU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNwRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxTQUFPLENBQUMsQ0FBQztvQkFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBTyxDQUFDLENBQUM7aUJBRTlCO1lBQ0gsQ0FBQyxDQUFDLENBQUE7UUFFSixDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztRQUN6Qyw4Q0FBOEM7UUFDOUMsNkRBQTZEO1FBQzdELDRDQUE0QztRQUM1QyxNQUFNO1FBQ04sd0NBQXdDO1FBQ3hDLHFEQUFxRDtRQUNyRCxJQUFJO1FBQ0osaUVBQWlFO1FBQ2pFLDZDQUE2QztRQUM3QyxNQUFNO1FBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUE7UUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLG1CQUFtQixDQUFDLENBQUE7SUFDckQsQ0FBQztJQUVELGdEQUFjLEdBQWQsVUFBZSxXQUFXO1FBQ3hCLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRTtZQUN0RCxJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLFdBQVcsQ0FBRSxDQUFDO1lBQ3ZELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQztnQkFDckMsV0FBVyxFQUFFLFdBQVc7Z0JBQ3hCLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWU7YUFDM0MsQ0FBQyxDQUFBO1NBQ0g7SUFDSCxDQUFDO0lBRUQsZ0RBQWMsR0FBZCxVQUFlLElBQUk7UUFDakIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUMsRUFBRSxFQUFFLEdBQUcsSUFBSyxPQUFBLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLEVBQWxDLENBQWtDLENBQUMsQ0FBQztJQUNsRixDQUFDO0lBRUQsK0NBQWEsR0FBYixVQUFjLEdBQUc7UUFBakIsaUJBK1FDO1FBOVFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLGFBQWEsQ0FBQyxDQUFBO1FBQy9CLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO1FBQ2xFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFBO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQzdCLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2xCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFDaEMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQztRQUV6RCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO1lBQ2xELElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxVQUFVLFNBQVM7Z0JBQzFDLElBQUksTUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNuRixJQUFJLFNBQVMsQ0FBQyxJQUFJLEVBQUU7d0JBQ2xCLFFBQVEsQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQzlEO3lCQUFNO3dCQUNMLFFBQVEsQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQzdEO29CQUNELFFBQVEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNoRjtZQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0YsUUFBUSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDeEksSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsTUFBTSxDQUFDO2lCQUNsRCxTQUFTLENBQUMsVUFBQSxJQUFJO2dCQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUM3QixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUNqQyxJQUFJLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDOUQsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksWUFBWSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM3QyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsVUFBVSxRQUFRLEVBQUUsS0FBSzt3QkFDcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUE7d0JBQzlCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQzt3QkFDbkIsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO3dCQUMzRixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO3dCQUNyRixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO3dCQUNyRixHQUFHLENBQUMsb0JBQW9CLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQzt3QkFDekcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsZUFBZSxDQUFDO3dCQUN6RixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQy9HLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDM0UsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHOzRCQUNuQixNQUFNLEVBQUUsbUJBQW1COzRCQUMzQixRQUFRLEVBQUUsaUJBQWlCOzRCQUMzQixZQUFZLEVBQUcsaUJBQWlCOzRCQUNoQyxZQUFZLEVBQUUsa0JBQWtCOzRCQUNoQyxVQUFVLEVBQUUsa0JBQWtCO3lCQUNqQyxDQUFDO3dCQUNBLGdCQUFnQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsOEJBQThCLENBQUMsQ0FBQTt3QkFDcEUsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksR0FBRyxFQUFFOzRCQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBOzRCQUN4QixJQUFJLFNBQU8sR0FBRyxFQUFFLENBQUM7NEJBQ2pCLFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDOzRCQUM5QyxTQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQzs0QkFDbEQsU0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDOzRCQUMvRixTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixTQUFPLENBQUMsb0JBQW9CLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQzs0QkFDN0csU0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7NEJBQ3hDLFNBQU8sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxlQUFlLENBQUM7NEJBQ3RELFNBQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLENBQUM7NEJBQ3pFLFNBQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUM7NEJBQzdCLFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRztnQ0FDdkIsTUFBTSxFQUFFLG1CQUFtQjtnQ0FDM0IsUUFBUSxFQUFFLGlCQUFpQjtnQ0FDM0IsWUFBWSxFQUFHLGlCQUFpQjtnQ0FDaEMsWUFBWSxFQUFFLGtCQUFrQjtnQ0FDaEMsVUFBVSxFQUFFLGtCQUFrQjs2QkFDakMsQ0FBQzs0QkFDQSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsVUFBVSxRQUFRO2dDQUM3RCxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3BELENBQUMsQ0FBQyxDQUFDOzRCQUNILGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFPLENBQUMsQ0FBQzt5QkFDaEM7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7Z0JBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO2dCQUNwRCxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLEdBQUcsZ0JBQWdCLENBQUM7WUFDNUQsQ0FBQyxFQUNDLFVBQUEsR0FBRztnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNSO2FBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUN6RCxJQUFJLE1BQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxlQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLE1BQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO3dCQUNsQixRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM5RDt5QkFBTTt3QkFDTCxRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM3RDtpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLE1BQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsZUFBYSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3JGO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGdCQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLE1BQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsZ0JBQWMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGVBQWEsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDM0c7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBYSxFQUFFLG1CQUFtQixDQUFDLENBQUM7WUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBYyxFQUFFLHNCQUFzQixDQUFDLENBQUM7WUFDcEQsUUFBUSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGVBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQkFBYyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsZUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLGdCQUFjLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQzlMLElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLE1BQU0sQ0FBQztpQkFDbEQsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDakMsSUFBSSxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsZ0JBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDekYsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksWUFBWSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM3QyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsVUFBVSxRQUFRLEVBQUUsS0FBSzt3QkFDcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUE7d0JBQzlCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQzt3QkFDbkIsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO3dCQUMzRixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO3dCQUNyRixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO3dCQUNyRixHQUFHLENBQUMsb0JBQW9CLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQzt3QkFDekcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsZUFBZSxDQUFDO3dCQUN6RixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQy9HLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDM0UsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHOzRCQUNuQixNQUFNLEVBQUUsbUJBQW1COzRCQUMzQixRQUFRLEVBQUUsaUJBQWlCOzRCQUMzQixZQUFZLEVBQUcsaUJBQWlCOzRCQUNoQyxZQUFZLEVBQUUsa0JBQWtCOzRCQUNoQyxVQUFVLEVBQUUsa0JBQWtCO3lCQUNqQyxDQUFDO3dCQUNBLGdCQUFnQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsOEJBQThCLENBQUMsQ0FBQTt3QkFDcEUsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksR0FBRyxFQUFFOzRCQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBOzRCQUN4QixJQUFJLFNBQU8sR0FBRyxFQUFFLENBQUM7NEJBQ2pCLFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDOzRCQUM5QyxTQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQzs0QkFDbEQsU0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDOzRCQUMvRixTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixTQUFPLENBQUMsb0JBQW9CLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQzs0QkFDN0csU0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7NEJBQ3hDLFNBQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QyxTQUFPLENBQUMsaUJBQWlCLENBQUMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDOzRCQUN0RCxTQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDOzRCQUN6RSxTQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDOzRCQUM3QixDQUFDLENBQUMsT0FBTyxDQUFDLE1BQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsVUFBVSxRQUFRO2dDQUM3RCxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3BELENBQUMsQ0FBQyxDQUFDOzRCQUNILFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRztnQ0FDdkIsTUFBTSxFQUFFLG1CQUFtQjtnQ0FDM0IsUUFBUSxFQUFFLGlCQUFpQjtnQ0FDM0IsWUFBWSxFQUFHLGlCQUFpQjtnQ0FDaEMsWUFBWSxFQUFFLGtCQUFrQjtnQ0FDaEMsVUFBVSxFQUFFLGtCQUFrQjs2QkFDakMsQ0FBQzs0QkFDQSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBTyxDQUFDLENBQUM7eUJBQ2hDO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDcEQsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsZUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLGdCQUFjLENBQUMsQ0FBQyxLQUFLLEdBQUcsZ0JBQWdCLENBQUM7WUFDdkYsQ0FBQyxFQUNDLFVBQUEsR0FBRztnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNSO2FBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUN6RCxJQUFJLE1BQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxlQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLE1BQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO3dCQUNsQixRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM5RDt5QkFBTTt3QkFDTCxRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM3RDtpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLE1BQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsZUFBYSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3JGO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGdCQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLE1BQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsZ0JBQWMsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGVBQWEsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDM0c7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksZUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsU0FBUztnQkFDMUMsSUFBSSxNQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ25GLGVBQWEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLE1BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGVBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQkFBYyxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNoSTtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFhLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztZQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFjLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWEsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2xELFFBQVEsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsZ0JBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsZ0JBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3hPLElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLE1BQU0sQ0FBQztpQkFDbEQsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDakMsSUFBSSxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsZ0JBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxlQUFhLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzlHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN2QixJQUFJLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDN0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLFVBQVUsUUFBUSxFQUFFLEtBQUs7d0JBQ3BGLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFBO3dCQUM5QixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUM7d0JBQ25CLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDM0YsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7d0JBRXpHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGVBQWUsQ0FBQzt3QkFDekYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUMvRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQzNFLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRzs0QkFDbkIsTUFBTSxFQUFFLG1CQUFtQjs0QkFDM0IsUUFBUSxFQUFFLGlCQUFpQjs0QkFDM0IsWUFBWSxFQUFHLGlCQUFpQjs0QkFDaEMsWUFBWSxFQUFFLGtCQUFrQjs0QkFDaEMsVUFBVSxFQUFFLGtCQUFrQjt5QkFDakMsQ0FBQzt3QkFDQSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLDhCQUE4QixDQUFDLENBQUE7d0JBQ3BFLElBQUksZ0JBQWdCLENBQUMsTUFBTSxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsRUFBRTs0QkFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs0QkFDeEIsSUFBSSxTQUFPLEdBQUcsRUFBRSxDQUFDOzRCQUNqQixTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQzs0QkFDOUMsU0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUM7NEJBQ2xELFNBQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzs0QkFDL0YsU0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDekYsU0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDekYsU0FBTyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7NEJBQzdHLFNBQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QyxTQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQzs0QkFDeEMsU0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQzs0QkFDdEQsU0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsQ0FBQzs0QkFDekUsU0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQzs0QkFDN0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFVBQVUsUUFBUTtnQ0FDN0QsU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUNwRCxDQUFDLENBQUMsQ0FBQzs0QkFDSCxTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUc7Z0NBQ3ZCLE1BQU0sRUFBRSxtQkFBbUI7Z0NBQzNCLFFBQVEsRUFBRSxpQkFBaUI7Z0NBQzNCLFlBQVksRUFBRyxpQkFBaUI7Z0NBQ2hDLFlBQVksRUFBRSxrQkFBa0I7Z0NBQ2hDLFVBQVUsRUFBRSxrQkFBa0I7NkJBQ2pDLENBQUM7NEJBQ0EsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQU8sQ0FBQyxDQUFDO3lCQUNoQztvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLG9CQUFvQixDQUFDLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGVBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxnQkFBYyxDQUFDLENBQUMsS0FBSyxDQUFDLGVBQWEsQ0FBQyxDQUFDLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQztZQUM1RyxDQUFDLEVBQ0MsVUFBQSxHQUFHO2dCQUNELEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLENBQUMsQ0FBQyxDQUFDO1NBQ1I7SUFDSCxDQUFDO0lBRUQsc0RBQW9CLEdBQXBCLFVBQXFCLEdBQUc7UUFBeEIsaUJBZ0hDO1FBL0dDLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLGFBQWEsQ0FBQyxDQUFBO1FBQy9CLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDO1FBQ2xFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFBO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQzdCLElBQUksUUFBUSxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2xCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFFaEMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxhQUFhLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNsRixRQUFRLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUM7WUFDM0MsUUFBUSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7U0FDNUU7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9FLFFBQVEsQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO1lBQ3pDLFFBQVEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3pFO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxpQkFBaUIsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3RGLFFBQVEsQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUMsV0FBVyxDQUFDO1lBQzdDLFFBQVEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO1NBQ2hGO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMvRSxRQUFRLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQztZQUN6QyxRQUFRLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztTQUN6RTtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLGdCQUFnQixDQUFDLENBQUE7UUFFdkMsUUFBUSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFFeEksT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLENBQUMsQ0FBQTtRQUN4QyxJQUFJLENBQUMsY0FBYzthQUNoQixhQUFhLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLE1BQU0sQ0FBQzthQUNsRCxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7WUFDN0IsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNqQyw4REFBOEQ7WUFFOUQsU0FBUztZQUVULElBQUk7WUFDSixJQUFJLEtBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUNsRCxJQUFJLGtCQUFnQixHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDOUQsa0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksWUFBWSxDQUFDLGFBQWEsRUFBRTtvQkFDOUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBRSxVQUFVLFFBQVEsRUFBRSxLQUFLO3dCQUNyRSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQTt3QkFDOUIsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDO3dCQUNuQixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsaUJBQWlCLENBQUM7d0JBQzNGLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUM7d0JBQ3JGLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGVBQWUsQ0FBQzt3QkFDekYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUMvRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQzNFLGtCQUFnQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsOEJBQThCLENBQUMsQ0FBQTt3QkFDcEUsSUFBSSxrQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksR0FBRyxFQUFFOzRCQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBOzRCQUN4QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7NEJBQ2pCLE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDOzRCQUM5QyxPQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQzs0QkFDbEQsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDOzRCQUMvRixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQzs0QkFDeEMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQzs0QkFDdEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsQ0FBQzs0QkFDekUsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQzs0QkFDN0Isa0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUNoQztvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFnQixFQUFFLG9CQUFvQixDQUFDLENBQUM7Z0JBQ3BELEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssR0FBRyxrQkFBZ0IsQ0FBQzthQUMzRDtpQkFBTSxJQUFJLEtBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFFO2dCQUN6RCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztnQkFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsa0JBQWtCLENBQUMsQ0FBQTtnQkFDN0MsSUFBSSxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzlELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQTtnQkFDN0UsSUFBSSxjQUFjLEdBQUcsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzlELDhDQUE4QztnQkFDOUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNyQixrQ0FBa0M7Z0JBQ2xDLDZFQUE2RTtnQkFDN0Usb0NBQW9DO2dCQUNwQywwQkFBMEI7Z0JBQzFCLG9HQUFvRztnQkFDcEcsa0dBQWtHO2dCQUNsRyx3SEFBd0g7Z0JBQ3hILG9GQUFvRjtnQkFDcEYsb0NBQW9DO2dCQUNwQyw0RUFBNEU7Z0JBQzVFLCtEQUErRDtnQkFDL0QsbUNBQW1DO2dCQUNuQywwQkFBMEI7Z0JBQzFCLDBHQUEwRztnQkFDMUcsbURBQW1EO2dCQUNuRCxpRUFBaUU7Z0JBQ2pFLG9GQUFvRjtnQkFDcEYsd0NBQXdDO2dCQUN4QywwQ0FBMEM7Z0JBQzFDLE1BQU07Z0JBQ04sUUFBUTtnQkFDUixJQUFJO2dCQUVKLHNEQUFzRDtnQkFDdEQsNkRBQTZEO2FBQzlEO1lBRUQscUNBQXFDO1FBQ3ZDLENBQUMsRUFDQyxVQUFBLEdBQUc7WUFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUNELCtDQUFhLEdBQWIsVUFBYyxLQUFLO1FBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFDRCxpREFBZSxHQUFmO1FBQ0UsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRTtZQUM5RCxJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FDN0MsQ0FBQztZQUNGLElBQUksaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFDakUscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDNUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbEU7WUFFRCxJQUFJLFdBQVcsR0FBRyxrQkFBa0I7aUJBQ2pDLE1BQU0sQ0FBQyxVQUFVLEdBQUc7Z0JBQ25CLE9BQU8sR0FBRyxDQUFDLFFBQVEsQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztZQUNMLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUM1RSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUN0RjtTQUNGO0lBRUgsQ0FBQztJQUNELDRDQUFVLEdBQVYsVUFBVyxVQUFVO1FBRW5CLElBQUcsSUFBSSxDQUFDLElBQUksSUFBRSxhQUFhLEVBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDakI7UUFDRCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsRUFBRSxDQUFDO1FBQy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU07WUFDMUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztZQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUNqRCxJQUFJLFdBQVcsR0FDYixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzFFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQztZQUNaLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO1FBQzVELElBQUksaUJBQWlCLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFBO1FBQzNHLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsRUFBRTtZQUNwRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1NBQ2hDO1FBQ0QsSUFBSSxpQkFBaUIsRUFBRTtZQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUM7WUFDaEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUM7WUFDN0QsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUMvQixJQUFJLFlBQVksR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3ZDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN2QyxTQUFTLENBQUMsVUFBQSxHQUFHO2dCQUNaLElBQUksU0FBUyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBRS9FLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUU7WUFDakUsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztZQUNsRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQix5RUFBeUU7WUFDekUsc0NBQXNDO1lBQ3RDLE1BQU07U0FDUDtRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN0RCxJQUFJLENBQUMsV0FBVyxDQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUNqQixXQUFXLENBQ1osQ0FBQztTQUNIO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxnQkFBZ0I7Z0JBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsOEJBQThCO1NBQy9CO0lBQ0gsQ0FBQztJQUNELHlDQUFPLEdBQVAsVUFBUSxpQkFBaUIsRUFBRSxpQkFBaUI7UUFBNUMsaUJBbU5DO1FBbE5DLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLFdBQVcsQ0FBQztRQUM3QyxJQUFJLGlCQUFpQixDQUFDLGNBQWMsRUFBRTtZQUNwQyxJQUFJLE1BQU0sR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1lBQ3JELElBQUksWUFBWSxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7WUFDN0QsSUFBSSxVQUFRLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLG9CQUFvQixDQUFDO1lBQ3JFLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RELElBQUksY0FBYyxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDeEYsdURBQXVEO1lBQ3ZELElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDakMsR0FBRztZQUNILElBQUksV0FBVyxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFFLDBEQUEwRDtZQUNqSSxJQUFJLGNBQWMsR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO1lBQ2xFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztZQUMzQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsVUFBVSxTQUFTO2dCQUM1QyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUE7WUFDRixJQUFJLFdBQVcsRUFBRTtnQkFDZixJQUFJLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUM7Z0JBQ3hELElBQUksWUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3JDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLFVBQVUsV0FBVztvQkFDN0MsSUFBSSxXQUFXLENBQUMsZUFBZSxFQUFFO3dCQUMvQixpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7cUJBQzFFO3lCQUFNO3dCQUNMLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBVSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztxQkFDM0g7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxJQUFHLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3JCLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLFVBQVUsV0FBVzt3QkFDN0Msd0NBQXdDO3dCQUN4QyxzQ0FBc0M7d0JBQ3RDLDBCQUEwQjt3QkFDMUIsSUFBSSxXQUFXLENBQUMsZUFBZSxFQUFFOzRCQUMvQixpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7eUJBQzFFOzZCQUFNLElBQUksV0FBVyxDQUFDLFlBQVksRUFBRTs0QkFDbkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGVBQWU7Z0NBQzlELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7Z0NBQ25DLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO3lCQUN2Qjs2QkFBSzs0QkFDSixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUM1RCwrRkFBK0Y7NEJBQzdGLElBQUcsVUFBVSxFQUFDO2dDQUNiLElBQUssUUFBUSxHQUFHLFdBQVcsQ0FBQyxNQUFNO29DQUNqQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztvQ0FDdkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dDQUV0QyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTtvQ0FDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO29DQUMxQixDQUFDLENBQUMsUUFBUSxDQUFDOzZCQUNaO3lCQUNGO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2FBRUY7WUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUM5RixJQUFJLENBQUMsV0FBVyx3QkFBUSxJQUFJLENBQUMsV0FBVyxFQUFLLGlCQUFpQixDQUFFLENBQUM7WUFDakUsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQztpQkFDdkMsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDYix3REFBd0Q7Z0JBQ3hELEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ2hDLEdBQUc7Z0JBQ0gsSUFBSSxvQkFBb0IsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBRSw4QkFBOEI7Z0JBQ3hHLEtBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQztnQkFDbEcsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDO2dCQUN4RCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLE9BQU87cUJBQ2pDLE1BQU0sQ0FBQyxVQUFVLEdBQUc7b0JBQ25CLE9BQU8sR0FBRyxDQUFDLFFBQVEsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsRUFBRSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDaEUsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUNuQixJQUFJLElBQUksR0FBRyxLQUFJLENBQUM7Z0JBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLG1CQUFtQixDQUFDLENBQUM7Z0JBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLDBCQUEwQixFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLHVCQUF1QixDQUFDLENBQUM7Z0JBR3hELENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxLQUFLO29CQUMxRCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxDQUFDO29CQUV0QyxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTt3QkFDNUIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO3dCQUNqQixPQUFPLENBQUMsVUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO3dCQUN6QixTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUN6Qjt5QkFBTTt3QkFDTCx5Q0FBeUM7d0JBQ3pDLCtEQUErRDt3QkFDL0Qsa0VBQWtFO3dCQUNsRSx1RUFBdUU7d0JBQ3ZFLG9EQUFvRDt3QkFFcEQsOEhBQThIO3dCQUM5SCwwREFBMEQ7d0JBQzFELDRDQUE0Qzt3QkFDNUMsSUFBSTt3QkFDSixJQUFJLFNBQU8sR0FBRyxFQUFFLENBQUM7d0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUNBQXFDLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBQ3pELENBQUMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsY0FBYyxFQUFFLFVBQVUsUUFBUTs0QkFDNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsRUFBRSxRQUFRLENBQUMsQ0FBQzs0QkFFM0QsSUFBSSxRQUFRLENBQUMsTUFBTSxFQUFFO2dDQUNuQixJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtvQ0FDaEMsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO29DQUNuSixTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO3dDQUMzQyxDQUFDLENBQUMsV0FBVzt3Q0FDYixDQUFDLENBQUMsRUFBRSxDQUFDO2lDQUNSO3FDQUNJO29DQUNILFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7d0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7d0NBQ3ZDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUNBQ1I7NkJBQ0Y7aUNBQU0sSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFO2dDQUMvQixTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7NkJBQ25EO2lDQUNJLElBQUksUUFBUSxDQUFDLGlCQUFpQixFQUFFO2dDQUNuQyxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7NkJBQ3pDO2lDQUFNO2dDQUNMLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDL0M7NEJBQ0QsSUFBSSxRQUFRLENBQUMsUUFBUSxFQUFFO2dDQUNyQixxREFBcUQ7Z0NBRXJELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ25ELCtCQUErQixHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO2dDQUVwRSxJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUM7Z0NBQ3RGLDJDQUEyQztnQ0FDM0MsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyxxQ0FBcUMsR0FBRyxlQUFlLENBQUMsQ0FBQztnQ0FFeEgsSUFBSSxPQUFPLEdBQUcsSUFBSSxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUM7Z0NBQ3pDLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDO2dDQUNqQyxTQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDO2dDQUM5QixTQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUNwSSxTQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyw0QkFBNEIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUU1SDs0QkFDRCxJQUFJLGlCQUFpQixDQUFDLG1CQUFtQixFQUFFO2dDQUN6QyxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztvQ0FDcEIsaUJBQWlCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzs2QkFDdEQ7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7d0JBQ0gsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO3dCQUNuQixTQUFTLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUMvRSxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQzt3QkFDM0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQTt3QkFDaEQsSUFBSSxhQUFhLEVBQUU7NEJBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO3lCQUNyQjt3QkFDRCxTQUFPLHdCQUFRLElBQUksRUFBSyxTQUFPLENBQUUsQ0FBQzt3QkFDbEMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFPLENBQUMsQ0FBQztxQkFDekI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBRTNCLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2lCQUM1QjtxQkFBTTtvQkFDTCxLQUFJLENBQUMsU0FBUzt3QkFDWixJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQzs0QkFDbEQsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQzs0QkFDL0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDVjtnQkFDRCxJQUFJLEtBQUksQ0FBQyxVQUFVLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUU7b0JBQzdDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFBO2lCQUN2RDtnQkFDRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUN6QixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2lCQUNuQztnQkFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDbEQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ3ZKLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQy9KLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNyRCxLQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLElBQUUsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUEsQ0FBQyxDQUFBLEtBQUssQ0FBQztnQkFDaEYsS0FBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsS0FBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztnQkFDeEUsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQztnQkFDdEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDekQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsZUFBZTtnQkFDbkUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDdEUsdUNBQXVDO2dCQUN0QyxJQUFJLFdBQVcsR0FBRyxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNuRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDO29CQUNqQyxJQUFJLFdBQVcsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO3dCQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDO3FCQUNyRTt5QkFBTTt3QkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ3BDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILHdCQUF3QjtnQkFDeEIsMkNBQTJDO2dCQUMzQywwQkFBMEI7Z0JBQzFCLG1EQUFtRDtnQkFDbkQsdUNBQXVDO2dCQUN2Qyx5Q0FBeUM7Z0JBQ3pDLHdFQUF3RTtnQkFDeEUsbURBQW1EO2dCQUNuRCwwQ0FBMEM7Z0JBRTFDLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDOUMsQ0FBQyxFQUNDLFVBQUEsR0FBRztnQkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNSO0lBQ0gsQ0FBQztJQUNELHFCQUFxQjtJQUNyQixxQ0FBcUM7SUFDckMsc0NBQXNDO0lBQ3RDLDBCQUEwQjtJQUMxQiw4QkFBOEI7SUFDOUIsNkJBQTZCO0lBQzdCLDRCQUE0QjtJQUM1QixvRkFBb0Y7SUFDcEYsMENBQTBDO0lBQzFDLHNCQUFzQjtJQUN0QixtQkFBbUI7SUFDbkIsMEJBQTBCO0lBQzFCLDZCQUE2QjtJQUM3Qix3Q0FBd0M7SUFDeEMsc0JBQXNCO0lBQ3RCLGtCQUFrQjtJQUNsQixlQUFlO0lBQ2YsNEJBQTRCO0lBQzVCLCtCQUErQjtJQUMvQixvQ0FBb0M7SUFDcEMsNEJBQTRCO0lBQzVCLGlDQUFpQztJQUNqQyx3QkFBd0I7SUFDeEIsc0NBQXNDO0lBQ3RDLGtDQUFrQztJQUNsQyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLDBDQUEwQztJQUMxQywwQ0FBMEM7SUFDMUMsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQix3Q0FBd0M7SUFDeEMseUNBQXlDO0lBQ3pDLG9DQUFvQztJQUNwQyxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLDJDQUEyQztJQUMzQyw0Q0FBNEM7SUFDNUMsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQiwwQ0FBMEM7SUFDMUMsa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxZQUFZO0lBRVosaURBQWlEO0lBRWpELDJEQUEyRDtJQUMzRCxRQUFRO0lBRVIsOENBQThDO0lBQzlDLDhEQUE4RDtJQUM5RCxpREFBaUQ7SUFDakQsOENBQThDO0lBQzlDLHdEQUF3RDtJQUN4RCw0QkFBNEI7SUFDNUIsdUVBQXVFO0lBQ3ZFLG9DQUFvQztJQUNwQyxtQkFBbUI7SUFDbkIsd0JBQXdCO0lBQ3hCLG9FQUFvRTtJQUNwRSxvQ0FBb0M7SUFDcEMsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQixpQkFBaUI7SUFFakIsZUFBZTtJQUVmLDJEQUEyRDtJQUMzRCwrQkFBK0I7SUFDL0IsdUNBQXVDO0lBQ3ZDLGtDQUFrQztJQUNsQyxrQ0FBa0M7SUFDbEMsaUJBQWlCO0lBQ2pCLGVBQWU7SUFFZiw2QkFBNkI7SUFDN0IsZUFBZTtJQUNmLGFBQWE7SUFDYixJQUFJO0lBQ0osNkNBQVcsR0FBWCxVQUFZLFlBQVksRUFBRSxhQUFhLEVBQUUsV0FBVyxFQUFFLFdBQVc7UUFDL0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUM5QyxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDbkIscURBQXFEO1FBQ3JELGFBQWEsR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQ3pFLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDekMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxJQUFJLEVBQUUsS0FBSztnQkFDNUMsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7b0JBQzVCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsT0FBTyxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7b0JBQ3RDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pCO3FCQUFNO29CQUNMLElBQUksU0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsY0FBYyxFQUFFLFVBQVUsUUFBUTt3QkFDdkQsSUFBSSxRQUFRLENBQUMsTUFBTSxFQUFFOzRCQUNuQixJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQ0FDaEMsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dDQUNuSixTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29DQUMzQyxDQUFDLENBQUMsV0FBVztvQ0FDYixDQUFDLENBQUMsRUFBRSxDQUFDOzZCQUNSO2lDQUFNO2dDQUNMLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7b0NBQ3ZDLENBQUMsQ0FBQyxFQUFFLENBQUM7NkJBQ1I7eUJBQ0Y7NkJBQU07NEJBQ0wsU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUMvQztvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxTQUFPLHdCQUFRLElBQUksRUFBSyxTQUFPLENBQUUsQ0FBQztvQkFDbEMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFPLENBQUMsQ0FBQztpQkFDekI7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDO1FBQ3hDLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDNUMsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLGVBQWUsRUFBRTtZQUNoRCxJQUFJLE1BQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsZUFBZSxFQUFFLFVBQVUsT0FBTztnQkFDdkQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFVBQVUsV0FBVztvQkFDbEQsTUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGVBQWU7d0JBQzlELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNuRCxDQUFDLENBQUMsTUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3hDLENBQUMsQ0FBQyxDQUFDO2dCQUNILE1BQUksQ0FBQyxjQUFjO3FCQUNoQixhQUFhLENBQUMsTUFBSSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDO3FCQUMvQyxTQUFTLENBQUMsVUFBQSxJQUFJO29CQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGtCQUFrQixDQUFDLENBQUM7b0JBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO29CQUNsQyxJQUFJLE9BQU8sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM5RCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQztvQkFDcEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxZQUFZO3dCQUM3QyxJQUNFLElBQUksQ0FBQyxRQUFROzRCQUNiLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxJQUFJLE9BQU8sQ0FBQyxXQUFXOzRCQUNoRCxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUNqQzs0QkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztnQ0FDM0IsT0FBTyxJQUFJLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDO29DQUMxQyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXO29DQUN4RCxDQUFDLENBQUMsRUFBRSxDQUFDOzRCQUNULFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDaEU7OzRCQUNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLG1CQUFtQixDQUFDLENBQUM7b0JBQ25ELENBQUMsQ0FBQyxDQUFDO29CQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLG9CQUFvQixDQUFDLENBQUM7Z0JBQ25ELENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3hELElBQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7U0FDaEU7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztRQUMvRCxJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQzNDLHdEQUF3RDtRQUN4RCxJQUFJLG9CQUFvQixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFFLDhCQUE4QjtRQUN4RyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDbEcsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO2FBQ2pDLE1BQU0sQ0FBQyxVQUFVLEdBQUc7WUFDbkIsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLHdDQUF3QyxDQUFDLENBQUM7UUFDOUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLHdCQUF3QixDQUFDLENBQUM7UUFDdkQsZ0NBQWdDO1FBQ2hDLHlEQUF5RDtRQUN6RCw2Q0FBNkM7SUFDL0MsQ0FBQztJQUNELGtEQUFnQixHQUFoQixVQUFpQixRQUFRLEVBQUUsS0FBSztRQUM5QixrREFBa0Q7UUFDbEQsc0NBQXNDO1FBQ3RDLFVBQVU7UUFDVixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFO1lBQzVELFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztTQUNqRDtRQUVELGlEQUFpRDtRQUNqRCxtQ0FBbUM7UUFDbkMsVUFBVTtRQUNWLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6SCxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDM0QscUNBQXFDO1FBQ3JDLGtDQUFrQztRQUNsQyx1Q0FBdUM7UUFDdkMsK0JBQStCO1FBQy9CLGFBQWE7UUFDYiw2Q0FBNkM7UUFDN0MscUNBQXFDO1FBQ3JDLE9BQU87UUFDUCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUM5SSxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ3JELElBQUksa0JBQWtCLElBQUksa0JBQWtCLEdBQUcsQ0FBQyxFQUFFO1lBQ2hELElBQUksZUFBZSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEVBQUU7Z0JBQzdDLFNBQVMsR0FBRyxlQUFlLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0wsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLGtCQUFrQixFQUFFLENBQUMsRUFBRSxFQUFFO29CQUM1QyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNuQjthQUNGO1lBQ0QsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLFNBQVMsQ0FBQztTQUN2QzthQUFNO1lBQ0wsSUFBSSxrQkFBa0IsSUFBSSxDQUFDLEVBQUU7Z0JBQzNCLFFBQVEsQ0FBQyxlQUFlLENBQUMsR0FBRyxTQUFTLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0wsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUd2QztTQUNGO1FBQ0QsR0FBRztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLEVBQUUsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO1lBQ25FLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFLFVBQVUsSUFBSTtnQkFDOUQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxpQkFBaUIsR0FDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTTtZQUMvRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO1lBQzdCLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDVCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2xFLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBQ0wsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQ3JDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsV0FBVyxFQUF4QyxDQUF3QyxDQUNoRCxDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ25ELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7WUFDOUQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztZQUNqRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLEdBQUcsRUFBRTtnQkFDbEQsSUFBSSxXQUFTLEdBQ1gsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU07b0JBQzNDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLFVBQVUsQ0FBQztvQkFDL0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDVCxXQUFTLEdBQUcsV0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLE9BQU87b0JBQzVDLE9BQU8sT0FBTyxJQUFJLElBQUksQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsV0FBUyxDQUFDO2FBQzFDO1NBQ0Y7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ25FLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUE7UUFFOUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxzREFBb0IsR0FBcEIsVUFBcUIsQ0FBQztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO1FBQ25DLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7UUFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUMsR0FBRyxFQUFFLGdCQUFnQixFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBQyxlQUFlLEVBQUMsSUFBSSxDQUFDLE1BQU0sRUFBQyxjQUFjLEVBQUMsSUFBSSxDQUFDLEtBQUssRUFBQyxtQkFBbUIsRUFBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDdEssSUFBSSxJQUFJLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFELElBQUksV0FBVyxHQUFFLENBQUMsQ0FBQztRQUNuQixJQUFJLEdBQUcsS0FBSyxDQUFDLEVBQUU7WUFDYixJQUFJLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQztZQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQztTQUNyQzthQUFNLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN2QyxJQUFJLENBQUMsYUFBYSxHQUFHLGVBQWUsQ0FBQztZQUNyQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUMxQiwyRkFBMkY7WUFDMUYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDO1NBQzVEO2FBQU07WUFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztZQUMvQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQztTQUNwQztRQUVELElBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEdBQUMsQ0FBQyxFQUM3QjtZQUNFLG9CQUFvQjtZQUNwQixJQUFJLFVBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFVBQVUsU0FBUztnQkFDeEMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLFNBQVMsRUFBdkIsQ0FBdUIsQ0FBQyxDQUFDO2dCQUMzRSxPQUFPLENBQUMsR0FBRyxDQUFDLG9CQUFvQixFQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDaEQsSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDckIsVUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUI7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNELElBQUksQ0FBQyxXQUFXLEdBQUMsVUFBUSxDQUFDLE1BQU0sQ0FBQztZQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBQyxVQUFRLENBQUMsQ0FBQztZQUNqRixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsRUFBQyxLQUFLLENBQUMsQ0FBQztZQUMvRSxXQUFXLEdBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMscUJBQXFCO1NBQzVFO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ25ELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsbURBQWlCLEdBQWpCLFVBQWtCLFlBQW9DO1FBQ3BELE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQzNDLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLFlBQVksS0FBSyxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsR0FBRztnQkFDNUIsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1lBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1lBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQzVCO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFDLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHO2dCQUM1QixHQUFHLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxLQUFLLENBQUM7WUFDcEMsSUFBSSxDQUFDLGFBQWEsR0FBRyxXQUFXLENBQUM7WUFDakMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7U0FDM0I7UUFDRCxJQUFJLFdBQVcsR0FBRSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDbEQsSUFBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBQyxDQUFDLEVBQzdCO1lBQ0UsV0FBVyxHQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBQyxXQUFXLENBQUMsQ0FBQztTQUNyRDtRQUNELElBQUksQ0FBQyxZQUFZLEdBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDO1FBQzVDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUNELGdEQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkIsSUFBSSxLQUFLLENBQUMsT0FBTyxLQUFLLElBQUksRUFBRTtZQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHO2dCQUMxQixHQUFHLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztZQUNyQixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDdEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFDM0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7U0FFMUQ7YUFBTTtZQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFBLEdBQUc7Z0JBQzFCLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0QyxJQUFJLENBQUMsWUFBWSxHQUFDLEVBQUUsQ0FBQTtZQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUMzQjtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQ0QsK0NBQWEsR0FBYixVQUFjLElBQUksRUFBRSxVQUFVO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLG1CQUFtQixDQUFDLENBQUE7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsK0NBQWEsR0FBYjtRQUFBLGlCQW1CQztRQWxCQyxPQUFPLENBQUMsR0FBRyxDQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7UUFDL0IsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN2RCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDZixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtZQUNsRCxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLGNBQWM7YUFDaEIsYUFBYSxDQUFDLEtBQUssRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDO2FBQzNDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDYixLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDO2dCQUNoQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUMzQyxLQUFJLENBQUMsWUFBWSxHQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixFQUFFLEtBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUU3RCxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELG9EQUFrQixHQUFsQjtRQUNFLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUMsRUFBRSxDQUFDO1FBQ2xDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCw2Q0FBVyxHQUFYLFVBQVksS0FBSztRQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDeEMsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUMxQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDeEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHO2dCQUMxQixTQUFTLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzVCLFdBQVcsRUFBRSxXQUFXLENBQUMsS0FBSzthQUMvQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUMvQyxJQUFJLFVBQVUsR0FDWixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztnQkFDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztnQkFDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDbkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7WUFDL0QsSUFBSSxhQUFhLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUNwRCxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO1lBQ3hDLElBQUksVUFBVSxHQUFHLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN6RCxJQUFJLFVBQVUsRUFBRTtnQkFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7YUFDaEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxLQUFLLEdBQUc7b0JBQ1YsTUFBTSxFQUFFLENBQUM7b0JBQ1QsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsVUFBVSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7aUJBQ25DLENBQUM7Z0JBQ0YsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLGVBQWUsRUFBRTtvQkFDbEQsS0FBSyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEtBQUssQ0FBQztpQkFDL0M7cUJBQU07b0JBQ0wsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUk7d0JBQy9CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNOzRCQUN4QixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzs0QkFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUMvQixJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTs0QkFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7Z0NBQ2hELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0NBQzVCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO3lCQUNoQjs2QkFBTSxJQUFJLFFBQVEsRUFBRTs0QkFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7Z0NBQ2hELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztnQ0FDMUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQzt5QkFDZDtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCw0QkFBNEI7Z0JBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2FBQ3BEO1NBQ0Y7SUFHSCxDQUFDO0lBQ0QsNkNBQVcsR0FBWCxVQUFZLEtBQUs7UUFDZixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBQ0QsK0NBQWEsR0FBYixVQUFjLEtBQUssRUFBRSxNQUFNO1FBQ3pCLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztRQUMvQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNyRSxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUMvRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztTQUN4RDtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELDRDQUFVLEdBQVYsVUFBVyxLQUFLO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFDL0Isc0NBQXNDO1FBQ3RDLGdEQUFnRDtRQUNoRCxvQ0FBb0M7UUFDcEMsV0FBVztRQUNYLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUk7UUFDSixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFDRCwwQ0FBUSxHQUFSLFVBQVMsT0FBTyxFQUFFLEdBQUc7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsZ0NBQWdDLEVBQUUsR0FBRyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDckYsSUFBSSxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQ2xCLElBQUksT0FBTyxHQUFHLG9DQUFvQyxDQUFBO1lBQ2xELE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLEtBQUcsSUFBSSxDQUFDLFdBQWEsR0FBRyxPQUFPLENBQUM7WUFFdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFBO1lBQzlDLGtFQUFrRTtTQUVuRTthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDbkM7SUFDSCxDQUFDO0lBQ0QsZ0RBQWMsR0FBZCxVQUFlLE9BQU8sRUFBRSxNQUFNO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBQ0QsNkNBQVcsR0FBWCxVQUFZLE9BQU8sRUFBRSxNQUFNO1FBQTNCLGlCQW1LQztRQWxLQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQTtRQUNuQyxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksTUFBTSxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksWUFBWSxJQUFJLE1BQU0sSUFBSSxNQUFNLEVBQUU7WUFDdEYsTUFBTSxHQUFHLENBQUMsTUFBTSxJQUFJLFlBQVksSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO1lBQ3hFLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxNQUFNLElBQUksUUFBUSxFQUFFO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQ2pDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFDdkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7Z0JBQ3BDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM5QyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUM1QyxJQUFJLEtBQUssR0FBRyxZQUFZLENBQUMsU0FBUyxDQUNoQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsSUFBSSxPQUFPLEVBQWQsQ0FBYyxDQUN0QixDQUFDO29CQUNGLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFlBQVksQ0FBQztvQkFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUseUNBQXlDO2lCQUMxQzthQUNGO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRTtvQkFDakQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUM7b0JBQy9DLElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQ2hDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVyxFQUF0QyxDQUFzQyxDQUM5QyxDQUFDO29CQUNGLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLFlBQVksQ0FBQztvQkFDOUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUseUNBQXlDO2lCQUMxQzthQUNGO1NBQ0Y7YUFBTSxJQUFJLE1BQU0sSUFBSSxnQkFBZ0IsRUFBRTtZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLGNBQWMsRUFBRTtZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLGtCQUFrQixFQUFFO1lBQ3ZDLElBQUksTUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDdkIsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDM0MsSUFBSSxRQUFRLEdBQUc7Z0JBQ2IsaUJBQWlCLEVBQUcsSUFBSTthQUN6QixDQUFDO1lBQ0YsSUFBSSxxQkFBbUIsR0FBRyxjQUFjLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztZQUNqRSxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxNQUFJLENBQUM7aUJBQy9ELFNBQVMsQ0FDUixVQUFBLEdBQUc7Z0JBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsTUFBSSxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztnQkFDRixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDbEIsQ0FBQyxFQUNELFVBQUEsS0FBSztnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztZQUVKLDRDQUE0QztZQUM1Qyw0QkFBNEI7WUFDNUIsZ0RBQWdEO1lBQ2hELGlEQUFpRDtZQUNqRCxNQUFNO1lBQ04sS0FBSztZQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxHQUFHLE1BQUksQ0FBQztTQUNwQzthQUFNLElBQUksTUFBTSxJQUFJLFVBQVUsRUFBRTtZQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQztZQUN0RCxJQUFJLEtBQUssR0FBRztnQkFDVixRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUc7YUFDdEIsQ0FBQTtZQUNELElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7aUJBQzNDLFNBQVMsQ0FBQyxVQUFBLElBQUk7Z0JBQ2IsSUFBTSxFQUFFLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDN0MsSUFBTSxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDekMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hCO2dCQUNELElBQUksWUFBWSxHQUFHLGlCQUFpQixDQUFDO2dCQUNyQyxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUM7Z0JBQ3BELFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDcEQsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNLElBQUksTUFBTSxJQUFJLGVBQWUsSUFBSSxNQUFNLElBQUksbUJBQW1CLEVBQUU7WUFDckUsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztZQUN6RCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDakIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDMUIsK0JBQStCO1lBQzdCLDZCQUE2QjtZQUMvQixvQkFBb0I7WUFDcEIsb0JBQW9CO1lBQ3BCLHlCQUF5QjtZQUN6QixzQkFBc0I7WUFDdEIsTUFBTTtZQUNOLElBQUk7WUFDSixJQUFJLFFBQVEsR0FBRztnQkFDYixNQUFNLEVBQUUsQ0FBQyxNQUFNLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSztnQkFDbEQsT0FBTyxFQUFFLFFBQVE7Z0JBQ2pCLE1BQU0sRUFBRSxPQUFPO2FBQ2hCLENBQUE7WUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxxQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ3RELElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDO2lCQUMvRCxTQUFTLENBQ1IsVUFBQSxHQUFHO2dCQUNELElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztZQUNKLENBQUMsRUFDRCxVQUFBLEtBQUs7Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO1lBQ0osQ0FBQyxDQUNGLENBQUM7WUFDSixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7YUFBTSxJQUFJLE1BQU0sSUFBSSxXQUFXLEVBQUU7WUFDaEMsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztZQUN6RCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDakIsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDMUIsSUFBSSxRQUFRLEdBQUc7Z0JBQ2IsT0FBTyxFQUFFLFFBQVE7Z0JBQ2pCLE1BQU0sRUFBRSxPQUFPO2dCQUNmLE1BQU0sRUFBRSxJQUFJO2FBQ2IsQ0FBQTtZQUNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLHFCQUFtQixHQUFHLGNBQWMsQ0FBQyxZQUFZLENBQUM7WUFDdEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7aUJBQy9ELFNBQVMsQ0FDUixVQUFBLEdBQUc7Z0JBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO1lBQ0osQ0FBQyxFQUNELFVBQUEsS0FBSztnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztZQUNKLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtJQUNILENBQUM7SUFFRCw4Q0FBWSxHQUFaLFVBQWEsVUFBVSxFQUFFLFVBQVU7UUFBbkMsaUJBMkJDO1FBMUJDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNmLElBQUcsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUM7WUFDN0QsSUFBSSxxQkFBcUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDO1lBQy9ELElBQUkscUJBQW1CLEdBQUcscUJBQXFCLENBQUMsWUFBWSxDQUFDO1lBQzdELEtBQUssQ0FBQyxVQUFVLENBQUMsR0FBRyxVQUFVLENBQUMsR0FBRyxDQUFDO1lBQ25DLEtBQUssQ0FBQyxRQUFRLENBQUMsR0FBRyxVQUFVLENBQUM7WUFDN0IsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxLQUFLLEVBQUUscUJBQXFCLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUM7aUJBQ2xFLFNBQVMsQ0FDUixVQUFBLEdBQUc7Z0JBQ0QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNsQixDQUFDLEVBQ0QsVUFBQSxLQUFLO2dCQUNILEtBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixLQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FDRixDQUFDO1NBQ0w7SUFDSCxDQUFDO0lBRUQsZ0RBQWMsR0FBZCxVQUFlLE9BQU8sRUFBRSxVQUFVO1FBQWxDLGlCQWdEQztRQS9DQyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQy9DLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUE7UUFDMUMsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksbUJBQW1CLEdBQUcsY0FBYyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFFcEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUN0RCxJQUFJLFVBQVUsQ0FBQyxZQUFZLEVBQUU7WUFDM0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUk7Z0JBQ2hFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU07b0JBQy9CLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLFFBQVEsR0FBRztnQkFDVCxpQkFBaUIsRUFBRyxJQUFJO2FBQ3pCLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDakM7UUFFRCxJQUFJLENBQUMsY0FBYzthQUNsQixhQUFhLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUM7YUFDekUsU0FBUyxDQUNSLFVBQUEsR0FBRztZQUNELElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztZQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFO2dCQUM1QixLQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDakQ7WUFDRCxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztRQUNKLENBQUMsQ0FDRixDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUNqRSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGNBQWMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFHckUsQ0FBQztJQUNELGdEQUFjLEdBQWQsVUFBZSxLQUFLLEVBQUUsT0FBTztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakUsT0FBTyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCO1lBQ3hDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYztZQUN0QyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1AsSUFBSSxjQUFjLEVBQUU7WUFDbEIsSUFBSSxxQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLFVBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsb0NBQW9DO2dCQUNwQyx1Q0FBdUM7Z0JBQ3ZDLDJCQUEyQjtnQkFDekIsSUFBRyxJQUFJLENBQUMsTUFBTSxFQUNkO29CQUNFLFVBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3REO3FCQUFLLElBQUcsSUFBSSxDQUFDLGNBQWMsRUFBQztvQkFDM0IsVUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDcEQ7cUJBQ0c7b0JBQ0YsVUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN6QztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFckQsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxVQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDO2lCQUMzRCxTQUFTLENBQ1IsVUFBQSxHQUFHO2dCQUNELElBQUcsT0FBTyxDQUFDLE1BQU0sSUFBRSxRQUFRLElBQUsscUJBQW1CLENBQUMsTUFBTSxFQUMxRDtvQkFDQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsTUFBTSxDQUMzQixDQUNGLENBQUM7aUJBQ0Y7cUJBQUssSUFBRyxPQUFPLENBQUMsTUFBTSxJQUFFLFVBQVUsSUFBSyxxQkFBbUIsQ0FBQyxPQUFPLEVBQ25FO29CQUNDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztpQkFDRjtxQkFDRDtvQkFDQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7aUJBQ0Y7Z0JBQ0EsSUFBRyxjQUFjLENBQUMsYUFBYSxFQUFDO29CQUMvQixVQUFVLENBQUM7d0JBQ1QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUVSO1lBQ0osQ0FBQyxFQUNELFVBQUEsS0FBSztnQkFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztTQUNMO2FBQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFFO1lBQzVFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDMUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztTQUN0RTtJQUNILENBQUM7SUFDSCw4Q0FBWSxHQUFaLFVBQWEsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNO1FBQzlCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFDRixtREFBaUIsR0FBakIsVUFBa0IsT0FBTyxFQUFDLE1BQU0sRUFBQyxNQUFNO1FBQXZDLGlCQTBCQztRQXpCQyxPQUFPLENBQUMsR0FBRyxDQUFDLCtDQUErQyxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUMsTUFBTSxFQUFDLGFBQWEsRUFBQyxNQUFNLENBQUMsQ0FBQztRQUNuSCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUMvRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDO1FBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUMsVUFBVSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTthQUMvQixJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDNUIsWUFBWSxFQUFFLElBQUk7WUFDbEIsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLHFCQUFxQjtZQUNqQyxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLE1BQU07Z0JBQ2QsU0FBUyxFQUFFLE9BQU87Z0JBQ2xCLFNBQVMsRUFBRSxTQUFTO2FBQ3JCO1NBQ0YsQ0FBQzthQUNELFdBQVcsRUFBRTthQUNiLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsRUFBQyxRQUFRLENBQUMsQ0FBQztZQUNyRCxJQUFHLFFBQVEsSUFBSSxNQUFNLENBQUMsY0FBYyxFQUFDO2dCQUNuQyxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsY0FBYyxDQUFDLENBQUE7YUFDL0M7aUJBQU07Z0JBQ04sWUFBWSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDeEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNwRDtRQUNKLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNDLDRDQUFVLEdBQVYsVUFBVyxPQUFPLEVBQUUsTUFBTTtRQUExQixpQkFrQkM7UUFqQkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUMvRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVO2FBQzdCLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUMxQixZQUFZLEVBQUUsSUFBSTtZQUNsQixLQUFLLEVBQUUsVUFBVTtZQUNqQixVQUFVLEVBQUUscUJBQXFCO1lBQ2pDLElBQUksRUFBRTtnQkFDSixNQUFNLEVBQUUsTUFBTTtnQkFDZCxTQUFTLEVBQUUsT0FBTzthQUNuQjtTQUNGLENBQUM7YUFDRCxXQUFXLEVBQUU7YUFDYixTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2pCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDeEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxzREFBb0IsR0FBcEIsVUFBcUIsSUFBSTtRQUN2QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxJQUFJLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFELElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxNQUFNLEVBQUU7WUFDeEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxVQUFVLFNBQVM7Z0JBQ25ELFNBQVMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQzVCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNwQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUMzQjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLEtBQUssRUFBRTtnQkFDdkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLE1BQU0sRUFBRTtvQkFDNUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2lCQUNoQztxQkFBTTtvQkFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7aUJBQ2pDO2FBQ0Y7WUFFRCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQztZQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUMzQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3JELENBQUM7SUFDRCxnREFBYyxHQUFkLFVBQWUsSUFBSTtRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVDLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUMsY0FBYyxJQUFJLFFBQVEsRUFBRTtZQUNuQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEMsd0RBQXdEO1NBQ3pEO2FBQU07WUFDTCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDbEMsd0RBQXdEO1NBQ3pEO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDakQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDaEQsSUFBSSxDQUFDLFlBQVksR0FBQyxFQUFFLENBQUM7WUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsR0FBRztnQkFDNUIsR0FBRyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxhQUFhLEdBQUcsV0FBVyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQzFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDekMsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELDhDQUFZLEdBQVo7UUFBQSxpQkFrQ0M7UUFqQ0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDdkQsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1FBQ3RELHlFQUF5RTtRQUN6RSxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUV0QyxJQUFJLFFBQVEsR0FBRztZQUNiLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLE1BQU0sRUFBRSxLQUFLO1NBQ2QsQ0FBQTtRQUNELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7aUJBQ2pFLFNBQVMsQ0FDUixVQUFBLEdBQUc7Z0JBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNsQixDQUFDLEVBQ0QsVUFBQSxLQUFLO2dCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztnQkFDRixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDbEIsQ0FBQyxDQUNGLENBQUM7U0FDSDtJQUVILENBQUM7SUFDRCxzQ0FBSSxHQUFKO1FBQUEsaUJBZ0NDO1FBL0JDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQix5RUFBeUU7UUFDekUsSUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6RCxJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN0QyxJQUFJLFFBQVEsR0FBRztZQUNiLE1BQU0sRUFBRSxJQUFJO1lBQ1osT0FBTyxFQUFFLFFBQVE7WUFDakIsTUFBTSxFQUFFLEtBQUs7U0FDZCxDQUFBO1FBQ0QsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1FBRXRELElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDO2FBQy9ELFNBQVMsQ0FDUixVQUFBLEdBQUc7WUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7WUFDRixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxFQUNELFVBQUEsS0FBSztZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNGLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNsQixDQUFDLENBQ0YsQ0FBQztJQUNOLENBQUM7SUFFRCx3Q0FBTSxHQUFOO1FBQUEsaUJBa0NDO1FBakNDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixtQ0FBbUM7UUFDbkMseUVBQXlFO1FBQ3pFLElBQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDekQsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFcEQsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1FBQ3RELElBQUksUUFBUSxHQUFHO1lBQ2IsTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUUsUUFBUTtZQUNqQixNQUFNLEVBQUUsS0FBSztTQUNkLENBQUE7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQzthQUMvRCxTQUFTLENBQ1IsVUFBQSxHQUFHO1lBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO1lBQ0YsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsRUFDRCxVQUFBLEtBQUs7WUFDSCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDRixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxDQUNGLENBQUM7SUFDTixDQUFDOztnQkEza0VGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1Qix1MmtCQUFnRDs7aUJBRWpEOzs7O2dCQWxDUSw0QkFBNEI7Z0JBVzVCLGNBQWM7Z0JBQ2QsY0FBYztnQkE1QnJCLFNBQVM7Z0JBS0YsZUFBZTtnQkFWdEIsaUJBQWlCO2dEQW1JZCxNQUFNLFNBQUMsYUFBYTtnREFFcEIsTUFBTSxTQUFDLFNBQVM7Z0JBL0ZaLGFBQWE7OzsyQkFtQm5CLEtBQUs7eUJBQ0wsS0FBSzswQkFDTCxLQUFLO29DQUNMLEtBQUs7K0JBQ0wsU0FBUyxTQUFDLGNBQWM7d0JBQ3hCLFNBQVMsU0FBQyxVQUFVO3lDQUNwQixNQUFNO21DQUNOLE1BQU07NEJBeUlOLFNBQVMsU0FBQyxXQUFXO29DQUNyQixTQUFTLFNBQUMsbUJBQW1COztJQXU3RGhDLDhCQUFDO0NBQUEsQUE5a0VELElBOGtFQztTQXprRVksdUJBQXVCO0FBMmtFcEMsTUFBTSxVQUFVLG1CQUFtQixDQUFDLE1BQWM7SUFDaEQsSUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLG1DQUFtQztJQUM3RSxJQUFNLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEQsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLFVBQUMsSUFBSSxFQUFFLENBQUMsSUFBSyxPQUFBLFlBQVksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEVBQTFCLENBQTBCLENBQUMsQ0FBQztBQUM1RCxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgVmlld0NoaWxkLFxyXG4gIElucHV0LFxyXG4gIE91dHB1dCxcclxuICBFdmVudEVtaXR0ZXIsXHJcbiAgSW5qZWN0LFxyXG4gIE9uSW5pdCxcclxuICBDaGFuZ2VEZXRlY3RvclJlZlxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7XHJcbiAgTWF0UGFnaW5hdG9yLFxyXG4gIE1hdFRhYmxlRGF0YVNvdXJjZSxcclxuICBNYXREaWFsb2csXHJcbiAgTWF0Q2hlY2tib3gsXHJcbiAgTWF0VGFibGUsXHJcbiAgTWF0VG9vbHRpcE1vZHVsZVxyXG59IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbFwiO1xyXG5pbXBvcnQgeyBTbmFja0JhclNlcnZpY2UgfSBmcm9tIFwiLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcbmltcG9ydCB7XHJcbiAgRm9ybUJ1aWxkZXIsXHJcbiAgRm9ybUNvbnRyb2wsXHJcbiAgRm9ybUdyb3VwLFxyXG4gIFZhbGlkYXRvcnMsXHJcbiAgRm9ybUFycmF5XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlXCI7XHJcbi8vIGltcG9ydCB7IGxvY2FsZSBhcyBlbmdsaXNoIH0gZnJvbSBcIi4uL2kxOG4vZW5cIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG4vLyByZXF1aXJlKCdqcXVlcnknKVxyXG4vLyByZXF1aXJlKCdrZW5kb0dyaWQnKVxyXG5cclxuLy8gLzxyZWZlcmVuY2UgcGF0aD1cIi4uL2pxdWVyeS9pbmRleFwiIC8+XHJcblxyXG4vLyBpbXBvcnQgJCBmcm9tIFwiLi4vSlF1ZXJ5XCI7XHJcblxyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gXCIuLi9jb250ZW50L2NvbnRlbnQuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLi9fc2VydmljZXMvaW5kZXhcIjtcclxuaW1wb3J0IHsgTW9kZWxMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kZWwtbGF5b3V0L21vZGVsLWxheW91dC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSBcIkBhbmd1bGFyL2Nkay9jb2xsZWN0aW9uc1wiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBHcmlkRGF0YVJlc3VsdCwgUGFnZUNoYW5nZUV2ZW50LFNlbGVjdEFsbENoZWNrYm94U3RhdGUgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tYW5ndWxhci1ncmlkJztcclxuaW1wb3J0IHsgR3JvdXBEZXNjcmlwdG9yLCBEYXRhUmVzdWx0LCBwcm9jZXNzIH0gZnJvbSAnQHByb2dyZXNzL2tlbmRvLWRhdGEtcXVlcnknO1xyXG5pbXBvcnQgeyBldmVudE5hbWVzIH0gZnJvbSAncHJvY2Vzcyc7XHJcbmltcG9ydCB7IGRhdGEgfSBmcm9tICdqcXVlcnknO1xyXG5pbXBvcnQgeyBlbGVtZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29yZS9zcmMvcmVuZGVyMyc7XHJcbmltcG9ydCB7IENvbmZpcm1EaWFsb2dDb21wb25lbnQgfSBmcm9tICcuLi9jb25maXJtLWRpYWxvZy9jb25maXJtLWRpYWxvZy5jb21wb25lbnQnO1xyXG5cclxuXHJcbi8vIGRlY2xhcmUgICQ6IGFueTtcclxuXHJcblxyXG4vLyBkZWNsYXJlIHZhciAkIDogSlF1ZXJ5U3RhdGljO1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJuZXctdGFibGUtbGF5b3V0XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9uZXctdGFibGUtbGF5b3V0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL25ldy10YWJsZS1sYXlvdXQuY29tcG9uZW50LnNjc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIE5ld1RhYmxlTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSB2aWV3RnJvbTogYW55O1xyXG4gIEBJbnB1dCgpIG9uTG9hZDogYW55O1xyXG4gIEBJbnB1dCgpIHRhYmxlSWQ6IGFueTtcclxuICBASW5wdXQoKSBkaXNhYmxlUGFnaW5hdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIEBWaWV3Q2hpbGQoXCJzZWxlY3RBbGxCb3hcIikgc2VsZWN0QWxsQm94OiBNYXRDaGVja2JveDtcclxuICBAVmlld0NoaWxkKFwiTWF0VGFibGVcIikgdGFibGU6IE1hdFRhYmxlPGFueT47XHJcbiAgQE91dHB1dCgpIGNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuICBAT3V0cHV0KCkgYWN0aW9uQ2xpY2tFdmVudCA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gIGVuYWJsZVNlbGVjdE9wdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGVuYWJsZVNlYXJjaDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHRhYmxlRGF0YTogYW55W10gPSBbXTtcclxuICBuZXdUYWJsZURhdGE6IGFueTtcclxuICBlbmFibGVBY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBkaXNwbGF5ZWRDb2x1bW5zOiBhbnk7XHJcbiAgY29sdW1uczogYW55O1xyXG4gIHRhYmxlTGlzdDogYW55O1xyXG4gIGN1cnJlbnRDb25maWdEYXRhOiBhbnk7XHJcbiAgY3VycmVudERhdGE6IGFueTtcclxuICBleHBhbmRlZEVsZW1lbnQ6IGFueTtcclxuICAvLyBAVmlld0NoaWxkKCd2aWV3TWUnLCB7IHN0YXRpYzogZmFsc2UgfSlcclxuICBkYXRhU291cmNlOiBhbnk7XHJcbiAgbGltaXQ6IG51bWJlciA9IDEwO1xyXG4gIG9mZnNldDogbnVtYmVyID0gMDtcclxuICBsZW5ndGg6IG51bWJlcjtcclxuICBzZWxlY3RlZERhdGE6IGFueSA9IFtdO1xyXG4gIGlucHV0RGF0YTogYW55ID0ge307XHJcbiAgcXVlcnlQYXJhbXM6IGFueTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIGRpYWxvZ1JlZjogYW55O1xyXG4gIGN1cnJlbnRUYWJsZURhdGE6IGFueTtcclxuICBkYXRhOiBhbnk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZU1zZyA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZU1vZGVsID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBpbnB1dFZhbHVlOiBhbnk7XHJcbiAgZnJvbUZpZWxkVmFsdWU6IGFueTtcclxuICB0b0ZpZWxkVmFsdWU6IGFueTtcclxuICBjdXJyZW50RmlsdGVyZWRWYWx1ZTogYW55ID0ge307XHJcbiAgb3B0aW9uRmlsdGVyZGF0YTogYW55O1xyXG4gIGVuYWJsZU9wdGlvbkZpbHRlcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIG9wdGlvbkZpbHRlckZpZWxkczogYW55O1xyXG4gIGVuYWJsZVVJZmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZW5hYmxlRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcmVkaXJlY3RVcmk6IHN0cmluZztcclxuICBwdWJsaWMgZ3JvdXBGaWVsZDogR3JvdXBEZXNjcmlwdG9yW10gPSBbXTtcclxuICBzZWxlY3RBbGxPcHRpb246IGFueSA9IFwibm9uZVwiO1xyXG4gIG5vdGlmeWNvdW50OiBudW1iZXI7XHJcbiAgc2VsZWN0QWxsVmFsdWU6IGFueTtcclxuICBlbmFibGVEZWxldGUgPSBmYWxzZTtcclxuICBzZWxlY3RDb3VudDogbnVtYmVyO1xyXG4gIHJlYWxtOiBhbnk7XHJcbiAgY3VycmVudFRlbmFudDogYW55O1xyXG4gIHB1YmxpYyBpbmZvID0gdHJ1ZTtcclxuICBwdWJsaWMgdHlwZTogXCJudW1lcmljXCIgfCBcImlucHV0XCIgPSBcIm51bWVyaWNcIjtcclxuICBwdWJsaWMgcGFnZVNpemVzID0gW3sgdGV4dDogMTAsIHZhbHVlOiAxMCB9LCB7IHRleHQ6IDI1LCB2YWx1ZTogMjUgfSwgeyB0ZXh0OiA1MCwgdmFsdWU6IDUwIH0sIHsgdGV4dDogMTAwLCB2YWx1ZTogMTAwIH1dO1xyXG4gIHB1YmxpYyBwcmV2aW91c05leHQgPSB0cnVlO1xyXG4gIG5vdGlmeUlkOiBhbnkgPSB7fTtcclxuICBlbmFibGVHcm91cGluZyA6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgc2VsZWN0QWxsSXRlbTogU2VsZWN0QWxsQ2hlY2tib3hTdGF0ZSA9IFwidW5jaGVja2VkXCI7XHJcbiAgLy8gc2VsZWN0T3B0aW9uOiBhbnlbXSA9IFt7IEtleTogJ0FsbCcsIE5hbWU6ICdBbGwnLCBWYWx1ZTogJ0FsbCcsIFR5cGU6ICdTdHJpbmcnfSxcclxuICAvLyB7IEtleTogJ1JlYWQnLCBOYW1lOiAnUmVhZCcsIFZhbHVlOiAncmVhZCcsIFR5cGU6ICdTdHJpbmcnfSxcclxuICAvLyB7IEtleTogJ1VucmVhZCcsIE5hbWU6ICdVbnJlYWQnLCBWYWx1ZTogJ3VucmVhZCcsIFR5cGU6ICdTdHJpbmcnfSxcclxuICAvLyB7IEtleTogJ05vbmUnLCBOYW1lOiAnTm9uZScsIFZhbHVlOiAnTm9uZScsIFR5cGU6ICdTdHJpbmcnfSAgXTtcclxuICBzZWxlY3RPcHRpb24gPSBbeyBcInZhbHVlXCI6IFwiYWxsXCIsIFwibmFtZVwiOiBcIkFsbFwiIH0sXHJcbiAgeyBcInZhbHVlXCI6IHRydWUsIFwibmFtZVwiOiBcIlJlYWRcIiB9LFxyXG4gIHsgXCJ2YWx1ZVwiOiBmYWxzZSwgXCJuYW1lXCI6IFwiVW5yZWFkXCIgfSxcclxuICB7IFwidmFsdWVcIjogXCJub25lXCIsIFwibmFtZVwiOiBcIk5vbmVcIiB9XTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfbWF0RGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIHNuYWNrQmFyU2VydmljZTogU25hY2tCYXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBASW5qZWN0KFwiZW52aXJvbm1lbnRcIikgcHJpdmF0ZSBlbnZpcm9ubWVudCxcclxuXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2gsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2VcclxuICApIHtcclxuXHJcbiAgICB0aGlzLnJlZGlyZWN0VXJpID0gdGhpcy5lbnZpcm9ubWVudC5yZWRpcmVjdFVyaTtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2UubW9kZWxDbG9zZU1lc3NhZ2VcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmVNb2RlbCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj4+ZGF0YVwiKVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhID0gW107XHJcbiAgICAgICAgaWYgKGRhdGEgIT0gMCkge1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRyaWdnZXJOb3RpZmljYXRpb24oe2RhdGEgOiBcInRyaWdnZXJcIn0pXHJcbiAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kRGF0YXNvdXJjZShcIm51bGxcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy50YWJsZURhdGEpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0YVNvdXJjZSk7XHJcbiAgICAgICAgLy8gXy5mb3JFYWNoKHRoaXMudGFibGVEYXRhLCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgLy8gICBpdGVtLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAvLyB9KTtcclxuXHJcbiAgICAgICAgLy8gdGhpcy5kYXRhU291cmNlLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgICAgLy8gICBvYmouY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIH0pO1xyXG4gICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLmRhdGFTb3VyY2UuZGF0YSwgXCJfaWRcIik7XHJcbiAgICAgICAgLy8gLy8gbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAvLyB0aGlzLmNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcclxuICAgICAgICAvLyBpZih0aGlzLmRhdGEgPT09ICdsaXN0Vmlldycpe1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcclxuICAgICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5nZXRNZXNzYWdlKCkucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZU1zZykpLnN1YnNjcmliZShtZXNzYWdlID0+IHtcclxuICAgICAgY29uc29sZS5sb2cobWVzc2FnZSxcIj4+bWVzc2FnZVwiKVxyXG4gICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICAgICk7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMsXCI+PlRISVNcIilcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgJiZcclxuICAgICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhLmxpc3RWaWV3ICYmXHJcbiAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5saXN0Vmlldy5lbmFibGVOZXdUYWJsZUxheW91dFxyXG4gICAgICApIHtcclxuICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2VcclxuICAgICAgLmdldFRhYmxlSGVhZGVyVXBkYXRlKClcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmUpKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiPj4+Pj5kYXRhXCIpXHJcbiAgICAgICAgaWYgKGRhdGEuZGF0YSA9PSAndXBkYXRlJykge1xyXG4gICAgICAgICAgdGhpcy51cGRhdGVUYWJsZVZpZXcoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJGSUxURVJSUlJcIik7XHJcbiAgICAgICAgICB0aGlzLmVuYWJsZUZpbHRlciA9ICF0aGlzLmVuYWJsZUZpbHRlcjtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuICBAVmlld0NoaWxkKFwicGFnaW5hdG9yXCIpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIEBWaWV3Q2hpbGQoXCJzZWxlY3RlZFBhZ2luYXRvclwiKSBzZWxlY3RlZFBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy8gdGhpcy5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgdGhpcy5zZWxlY3RBbGxWYWx1ZSA9IFwiXCI7XHJcbiAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5zZWxlY3RBbGxJdGVtID0gJ3VuY2hlY2tlZCc7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLCBcIi4uLi4uLnRhYmxlIHRoaXNcIik7XHJcbiAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJkYXRhc291cmNlXCIpO1xyXG4gICAgdGhpcy5yZWFsbT1sb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInJlYWxtXCIpO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtcyA9IHtcclxuICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgICBsaW1pdDogMTAsXHJcbiAgICAgIGRhdGFzb3VyY2U6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2UsXHJcbiAgICAgIHJlYWxtOiB0aGlzLnJlYWxtXHJcbiAgICB9O1xyXG4gICAgdGhpcy5jdXJyZW50VGVuYW50PXtcclxuICAgICAgcmVhbG06IHRoaXMucmVhbG0sXHJcbiAgICAgIHVzZXJJZDogbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ1c2VySWRcIilcclxuICAgIH1cclxuICAgIC8vICB0aGlzLnBhZ2luYXRvcltcInBhZ2VJbmRleFwiXSA9IDA7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICk7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiB0aGlzLmRhdGEgXCIsdGhpcy5kYXRhKTtcclxuICAgIGlmICh0aGlzLmRhdGEgPT0gJ2xpc3RWaWV3JyB8fCB0aGlzLmRhdGEgPT0gJ3JlZnJlc2hQYWdlJyB8fCB0aGlzLmRhdGE9PT0wKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGE9W107XHJcbiAgICB9XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImRhdGFzb3VyY2VJZFwiXSA9IHRoaXMuZGVmYXVsdERhdGFzb3VyY2U7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgLy8gdGhpcy5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICB0aGlzLmZyb21GaWVsZFZhbHVlID0gbmV3IEZvcm1Db250cm9sKFtcIlwiXSk7XHJcbiAgICB0aGlzLnRvRmllbGRWYWx1ZSA9IG5ldyBGb3JtQ29udHJvbChbXCJcIl0pO1xyXG4gICAgdGhpcy5vbkxvYWREYXRhKG51bGwpO1xyXG5cclxuICB9XHJcbiAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgY29uc29sZS5sb2coXCJuZ0FmdGVyVmlld0luaXQgXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZFBhZ2luYXRvciwgXCJzZWxlY3RlZFBhZz4+Pj5cIik7XHJcbiAgICBpZiAodGhpcy5kYXRhU291cmNlICYmIHRoaXMuc2VsZWN0ZWRQYWdpbmF0b3IpIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLnBhZ2luYXRvciA9IHRoaXMuc2VsZWN0ZWRQYWdpbmF0b3I7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuICBuZ09uRGVzdHJveSgpIHtcclxuICAgIHRoaXMudW5zdWJzY3JpYmUubmV4dCgpO1xyXG4gICAgdGhpcy51bnN1YnNjcmliZU1zZy5uZXh0KCk7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlTW9kZWwubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgZ3JvdXBDaGFuZ2UoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcIj4+Pj4+IEdST1VQIEVWRU5UXCIpO1xyXG4gICAgdGhpcy5ncm91cEZpZWxkID0gZXZlbnQ7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLCBcIj4+Pj4gVEhJU1wiKTtcclxuICAgIGxldCBhcGlGdW5jdGlvbkRldGFpbHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEub25Hcm91cGluZ0Z1bmN0aW9uO1xyXG4gICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgXy5mb3JFYWNoKGFwaUZ1bmN0aW9uRGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgIGlmIChyZXF1ZXN0SXRlbS5mcm9tQ3VycmVudERhdGEpIHtcclxuICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5jdXJyZW50RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0SXRlbS5kaXJlY3RBc3NpZ24pIHtcclxuICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHJlcXVlc3RJdGVtLnZhbHVlKVxyXG4gICAgICAgICAgOiByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSlcclxuICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIF8uZm9yRWFjaChldmVudCwgZnVuY3Rpb24gKGV2ZW50SXRlbSkge1xyXG4gICAgICBzZWxmLnF1ZXJ5UGFyYW1zW2V2ZW50SXRlbS5maWVsZF0gPSB0cnVlO1xyXG4gICAgfSlcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgLmdldEFsbFJlcG9uc2UodGhpcy5xdWVyeVBhcmFtcywgYXBpRnVuY3Rpb25EZXRhaWxzLmFwaVVybClcclxuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAvLyBpZiAoIGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmVuYWJsZUxvYWRlcikge1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgLy99XHJcbiAgICAgICAgdGhpcy5lbmFibGVTZWFyY2ggPSB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZUdsb2JhbFNlYXJjaDtcclxuICAgICAgICBsZXQgc2VsZWN0ZWRUYWJsZUhlYWRlcnMgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInNlbGVjdGVkVGFibGVIZWFkZXJzXCIpOyAgLy8gYWZ0ZXIgZ2xvYmFsIHNldHRpbmcgY2hhbmdlXHJcbiAgICAgICAgdGhpcy5jb2x1bW5zID0gIV8uaXNFbXB0eShzZWxlY3RlZFRhYmxlSGVhZGVycykgPyBKU09OLnBhcnNlKHNlbGVjdGVkVGFibGVIZWFkZXJzKSA6IHRoaXMuY29sdW1ucztcclxuICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmNvbHVtbnNcclxuICAgICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKHZhbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdmFsLmlzQWN0aXZlO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgbGV0IHRlbXBBcnJheSA9IFtdO1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxmLCBcIj4+Pj4+Pj4+Pj5USElTU1NTXCIpO1xyXG4gICAgICAgIF8uZm9yRWFjaChkYXRhLnJlc3BvbnNlW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV0sIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xyXG4gICAgICAgICAgaWYgKHR5cGVvZiBpdGVtICE9PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgIC8vIHRlbXBPYmpba2V5VG9TZXRdID0gaXRlbTtcclxuICAgICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgICBfLmZvckVhY2goc2VsZi5jdXJyZW50VGFibGVEYXRhLmRhdGFWaWV3Rm9ybWF0LCBmdW5jdGlvbiAodmlld0l0ZW0pIHtcclxuICAgICAgICAgICAgICBpZiAodmlld0l0ZW0uc3Via2V5KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodmlld0l0ZW0uYXNzaWduRmlyc3RJbmRleHZhbCkge1xyXG4gICAgICAgICAgICAgICAgICBsZXQgcmVzdWx0VmFsdWUgPSAoIV8uaXNFbXB0eShpdGVtW3ZpZXdJdGVtLnZhbHVlXSkgJiYgaXRlbVt2aWV3SXRlbS52YWx1ZV1bMF1bdmlld0l0ZW0uc3Via2V5XSkgPyAoaXRlbVt2aWV3SXRlbS52YWx1ZV1bMF1bdmlld0l0ZW0uc3Via2V5XSkgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgICA/IHJlc3VsdFZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgICAgPyBpdGVtW3ZpZXdJdGVtLnZhbHVlXVt2aWV3SXRlbS5zdWJrZXldXHJcbiAgICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAodmlld0l0ZW0uaXNDb25kaXRpb24pIHtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBldmFsKHZpZXdJdGVtLmNvbmRpdGlvbik7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmICh2aWV3SXRlbS5pc0FkZERlZmF1bHRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IHZpZXdJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICBpZiAoc2VsZi5jdXJyZW50VGFibGVEYXRhLmxvYWRMYWJlbEZyb21Db25maWcpIHtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPVxyXG4gICAgICAgICAgICAgICAgICBzZWxmLmN1cnJlbnRUYWJsZURhdGEuaGVhZGVyTGlzdFtpdGVtW3ZpZXdJdGVtLnZhbHVlXV07XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgbGV0IGZpbHRlck9iaiA9IHt9O1xyXG4gICAgICAgICAgICBmaWx0ZXJPYmpbc2VsZi5jdXJyZW50VGFibGVEYXRhLmZpbHRlcktleV0gPSBpdGVtW3NlbGYuY3VycmVudFRhYmxlRGF0YS5kYXRhRmlsdGVyS2V5XTtcclxuICAgICAgICAgICAgbGV0IHNlbGVjdGVkVmFsdWUgPSBfLmZpbmQoc2VsZi5pbnB1dERhdGFbc2VsZi5jdXJyZW50VGFibGVEYXRhLnNlbGVjdGVkVmFsdWVzS2V5XSwgZmlsdGVyT2JqKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRWYWx1ZSwgXCI+Pj4+PnNlbGVjdGVkVmFsdWVcIilcclxuICAgICAgICAgICAgaWYgKHNlbGVjdGVkVmFsdWUpIHtcclxuICAgICAgICAgICAgICBpdGVtLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRlbXBPYmogPSB7IC4uLml0ZW0sIC4uLnRlbXBPYmogfTtcclxuICAgICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgaWYgKHRlbXBBcnJheSAmJiB0ZW1wQXJyYXkubGVuZ3RoKSB7XHJcbiAgICAgICAgICB0aGlzLnRhYmxlRGF0YSA9IHRlbXBBcnJheTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy50YWJsZURhdGEgPVxyXG4gICAgICAgICAgICBkYXRhICYmIGRhdGEucmVzcG9uc2UgJiYgZGF0YS5yZXNwb25zZVthcGlGdW5jdGlvbkRldGFpbHMucmVzcG9uc2VdXHJcbiAgICAgICAgICAgICAgPyAoZGF0YS5yZXNwb25zZVthcGlGdW5jdGlvbkRldGFpbHMucmVzcG9uc2VdKVxyXG4gICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChkYXRhICYmIGRhdGEucmVzcG9uc2UpIHtcclxuICAgICAgICAgIHRoaXMubGVuZ3RoID0gZGF0YS5yZXNwb25zZS50b3RhbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5jb25zdHJ1Y3RHcm91cGVkRGF0YShkYXRhLnJlc3BvbnNlLCBhcGlGdW5jdGlvbkRldGFpbHMucmVzcG9uc2UpXHJcbiAgICAgICAgLy8gbGV0IGNvbmNhdERhdGEgPSB0aGlzLnRhYmxlRGF0YVswXS5kYXRhLmNvbmNhdCh0aGlzLnRhYmxlRGF0YVsxXS5kYXRhKVxyXG5cclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMudGFibGVEYXRhKTtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5zZWxlY3RlZFBhZ2luYXRvcjsgLy8gbmV3bHkgYWRkZWQgXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgICAgIHZhciBmaWx0ZXJlZEtleSA9ICh0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSkgPyB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSA6ICcnO1xyXG4gICAgICAgIF8uZm9yRWFjaChzZWxmLmNvbHVtbnMsIGZ1bmN0aW9uICh4KSB7XHJcbiAgICAgICAgICBpZiAoZmlsdGVyZWRLZXkgPT0geC52YWx1ZSkge1xyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YVt4LmtleVRvU2F2ZV0gPSBzZWxmLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbeC5rZXlUb1NhdmVdID0gbnVsbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICAvLyB0aGlzLmxvYWRLZW5kb0dyaWQoKTtcclxuICAgICAgICAvLyB0aGlzLnNlbGVjdEFsbENoZWNrKHsgY2hlY2tlZDogZmFsc2UgfSk7XHJcbiAgICAgICAgLy8gdGhpcy5zZWxlY3RBbGwgPSBmYWxzZTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdEFsbCwgXCJTZWxlY3RBbGw+Pj4+Pj4+XCIpO1xyXG4gICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICAgIC8vIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5zZWxlY3RBbGwsIFwiU2VsZWN0QWxsPj4+Pj4+PlwiKTtcclxuICAgICAgICAvLyB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuXHJcbiAgICAgICAgdGhpcy5jaGVja0NsaWNrRXZlbnRNZXNzYWdlLmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gICAgICB9LFxyXG4gICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjb25zdHJ1Y3RHcm91cGVkRGF0YShyZXNwb25zZSwgcmVzcG9uc2VOYW1lKSB7XHJcbiAgICBjb25zb2xlLmxvZyhyZXNwb25zZSwgXCI+PnJlc3BvbnNlXCIpO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+IGN1cmVudHRhYmxlIERhdGEgXCIsIHRoaXMuY3VycmVudFRhYmxlRGF0YSk7XHJcbiAgICB0aGlzLnRhYmxlRGF0YSA9IFtdO1xyXG4gICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgXy5mb3JFYWNoKHJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0sIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIF8uZm9yRWFjaChpdGVtLmRhdGEsIGZ1bmN0aW9uIChkYXRhSXRlbSwgaW5kZXgpIHtcclxuICAgICAgICBsZXQgb2JqID0gZGF0YUl0ZW07XHJcbiAgICAgICAgb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgIG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uY29udHJvbFR5cGU7XHJcbiAgICAgICAgb2JqW1wiY29udHJvbERlc2NyaXB0aW9uXCJdID0gcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbERlc2NyaXB0aW9uO1xyXG4gICAgICAgIG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IHJlc3BvbnNlLmFjY2Vzc0dyb3Vwc1tkYXRhSXRlbS5hY2Nlc3NHcm91cF0uYWNjZXNzR3JvdXBOYW1lO1xyXG4gICAgICAgIG9ialtcInJ1bGVzZXROYW1lXCJdID0gcmVzcG9uc2UucnVsZXNldHNbcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucnVsZXNldElkXS5uYW1lO1xyXG4gICAgICAgIG9ialtcImRhdGFzb3VyY2VOYW1lXCJdID0gcmVzcG9uc2UuZGF0YXNvdXJjZXNbZGF0YUl0ZW0uZGF0YXNvdXJjZV0ubmFtZTtcclxuICAgICAgICBvYmpbXCJzdGF0dXNDbGFzc1wiXSA9IHtcclxuICAgICAgICAgIFwiT3BlblwiOiBcImxhYmVsIGJnLWludGlhdGVkXCIsXHJcbiAgICAgICAgICBcIkNsb3NlZFwiOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgXCJSZW1lZGlhdGVkXCIgOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgXCJBdXRob3JpemVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiLFxyXG4gICAgICAgICAgXCJBcHByb3ZlZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIlxyXG4gICAgICB9O1xyXG4gICAgICAgIHNlbGYudGFibGVEYXRhLnB1c2gob2JqKTtcclxuICAgICAgICBpZiAoaXRlbS5kYXRhLmxlbmd0aCA9PSAxMCAmJiBpbmRleCA9PSAnOScpIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiTE9BRCBNT1JFXCIpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coc2VsZi50YWJsZURhdGEubGVuZ3RoLCBcIj4+Pj5zZWxmLnRhYmxlRGF0YS5sZW5ndGhcIik7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhpbmRleCwgXCI+Pj4+IElOREVYXCIpO1xyXG4gICAgICAgICAgbGV0IHRlbXBvYmogPSB7fTtcclxuICAgICAgICAgIHRlbXBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSByZXNwb25zZS5hY2Nlc3NHcm91cHNbZGF0YUl0ZW0uYWNjZXNzR3JvdXBdLmFjY2Vzc0dyb3VwTmFtZTtcclxuICAgICAgICAgIHRlbXBvYmpbXCJ1c2VyTmFtZVwiXSA9IGRhdGFJdGVtLnVzZXJOYW1lO1xyXG4gICAgICAgICAgdGVtcG9ialtcInJvbGVOYW1lXCJdID0gZGF0YUl0ZW0ucm9sZU5hbWU7XHJcbiAgICAgICAgICB0ZW1wb2JqWydhY2Nlc3NHcm91cCddID0gZGF0YUl0ZW0uYWNjZXNzR3JvdXA7XHJcbiAgICAgICAgICB0ZW1wb2JqWydhY2Nlc3NDb250cm9sJ10gPSBkYXRhSXRlbS5hY2Nlc3NDb250cm9sO1xyXG4gICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICB0ZW1wb2JqW1wicmlza1JhbmtpbmdcIl0gPSByZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5yaXNrUmFua2luZztcclxuICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sVHlwZVwiXSA9IHJlc3BvbnNlLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmNvbnRyb2xUeXBlO1xyXG4gICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xEZXNjcmlwdGlvblwiXSA9IHJlc3BvbnNlLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xEZXNjcmlwdGlvbjtcclxuICAgICAgICAgIHRlbXBvYmpbXCJfaWRcIl0gPSByZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5faWQ7XHJcbiAgICAgICAgICB0ZW1wb2JqW1wiZW5hYmxlTG9hZFwiXSA9IHRydWU7XHJcbiAgICAgICAgICB0ZW1wb2JqW1wic3RhdHVzQ2xhc3NcIl0gPSB7XHJcbiAgICAgICAgICAgIFwiT3BlblwiOiBcImxhYmVsIGJnLWludGlhdGVkXCIsXHJcbiAgICAgICAgICAgIFwiQ2xvc2VkXCI6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgIFwiUmVtZWRpYXRlZFwiIDogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgXCJBdXRob3JpemVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiLFxyXG4gICAgICAgICAgICBcIkFwcHJvdmVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiXHJcbiAgICAgICAgfTtcclxuICAgICAgICAgIF8uZm9yRWFjaChzZWxmLmN1cnJlbnRUYWJsZURhdGEucHJvZHVjdEtleXMsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICB0ZW1wb2JqW3ZpZXdJdGVtLm5hbWVdID0gZGF0YUl0ZW1bdmlld0l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+PiB0ZW1wb2JqIFwiLCB0ZW1wb2JqKTtcclxuICAgICAgICAgIHNlbGYudGFibGVEYXRhLnB1c2godGVtcG9iaik7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuXHJcbiAgICB9KVxyXG4gICAgdGhpcy5uZXdUYWJsZURhdGEgPSBwcm9jZXNzKHRoaXMudGFibGVEYXRhLCB7IGdyb3VwOiB0aGlzLmdyb3VwRmllbGQgfSk7XHJcbiAgICB0aGlzLm5ld1RhYmxlRGF0YS50b3RhbCA9IHJlc3BvbnNlLnRvdGFsO1xyXG4gICAgLy8gdGhpcy5ub3RpZnljb3VudCA9IHRoaXMubmV3VGFibGVEYXRhLnRvdGFsO1xyXG4gICAgLy8gXy5mb3JFYWNoKHRoaXMubmV3VGFibGVEYXRhLmRhdGEsZnVuY3Rpb24oaXRlbVZhbCwgaW5kZXgpe1xyXG4gICAgLy8gICBpdGVtVmFsLmNvbGxhcHNlR3JvdXAoaW5kZXgudG9TdHJpbmcoKSlcclxuICAgIC8vIH0gKVxyXG4gICAgLy8gICBmb3IgKGxldCBtID0gMDsgbSA8IDU7IG0gPSBtICsgMSkge1xyXG4gICAgLy8gICAgIHRoaXMubmV3VGFibGVEYXRhLmNvbGxhcHNlR3JvdXAobS50b1N0cmluZygpKTtcclxuICAgIC8vIH1cclxuICAgIC8vICAgdGhpcy5uZXdUYWJsZURhdGEuZmluZChcIi5rLWdyb3VwaW5nLXJvd1wiKS5lYWNoKGZ1bmN0aW9uICgpIHtcclxuICAgIC8vICAgICB0aGlzLm5ld1RhYmxlRGF0YS5jb2xsYXBzZUdyb3VwKHRoaXMpO1xyXG4gICAgLy8gfSk7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLCBcIj4+PiBUSElTXCIpXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLm5ld1RhYmxlRGF0YSwgXCI+Pj4gVEFCTEUgREFUIE5FV1wiKVxyXG4gIH1cclxuXHJcbiAgb25Sb3V0aW5nQ2xpY2soc2VsZWN0ZWRSb3cpIHtcclxuICAgIGlmICh0aGlzLmN1cnJlbnREYXRhICYmIHRoaXMuY3VycmVudERhdGEuZW5hYmxlUm91dGluZykge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLnNlbGVjdGVkUm93IH07XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFJvdXRpbmdNZXNzYWdlKHtcclxuICAgICAgICBzZWxlY3RlZFJvdzogc2VsZWN0ZWRSb3csXHJcbiAgICAgICAgaGlkZVZpZXc6IHRoaXMuY3VycmVudERhdGEuaGlkZVZpZXdMYXlvdXRzXHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBjb2xsYXBzZUdyb3VwcyhncmlkKSB7XHJcbiAgICB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLmZvckVhY2goKGdyLCBpZHgpID0+IGdyaWQuY29sbGFwc2VHcm91cChpZHgudG9TdHJpbmcoKSkpO1xyXG4gIH1cclxuXHJcbiAgbG9hZE1vcmVDbGljayhyb3cpIHtcclxuICAgIGNvbnNvbGUubG9nKHJvdywgXCI+Pj4+Pj4+IFJPV1wiKVxyXG4gICAgbGV0IGFwaUZ1bmN0aW9uRGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5vbkdyb3VwaW5nRnVuY3Rpb247XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnF1ZXJ5UGFyYW1zLCBcIj4+PiBRVUVSWSBQQVJBTVNcIilcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiPj4+IFRISVNcIilcclxuICAgIGxldCBpbmRleFZhbCA9IC0xO1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0gdGhpcy5xdWVyeVBhcmFtcztcclxuICAgIGxldCBncm91cGluZ0ZpZWxkID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLmdyb3VwaW5nRGV0YWlsO1xyXG5cclxuICAgIGlmICh0aGlzLmdyb3VwRmllbGQgJiYgdGhpcy5ncm91cEZpZWxkLmxlbmd0aCA9PSAxKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKGdyb3VwaW5nRmllbGQsIGZ1bmN0aW9uIChncm91cEl0ZW0pIHtcclxuICAgICAgICBpZiAoc2VsZi5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHNlbGYuZ3JvdXBGaWVsZCwgeyBmaWVsZDogZ3JvdXBJdGVtLm5hbWUgfSkgPj0gMCkge1xyXG4gICAgICAgICAgaWYgKGdyb3VwSXRlbS5pc0lkKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5T2JqWydsb2FkTW9yZScgKyBncm91cEl0ZW0ubmFtZV0gPSByb3dbZ3JvdXBJdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5T2JqWydsb2FkTW9yZScgKyBncm91cEl0ZW0ubmFtZV0gPSByb3dbZ3JvdXBJdGVtLm5hbWVdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaW5kZXhWYWwgPSBfLmZpbmRJbmRleChzZWxmLm5ld1RhYmxlRGF0YS5kYXRhLCB7IHZhbHVlOiByb3dbZ3JvdXBJdGVtLm5hbWVdIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgcXVlcnlPYmoubG9hZE1vcmVPZmZzZXQgPSAodGhpcy5uZXdUYWJsZURhdGEuZGF0YVtpbmRleFZhbF0uaXRlbXMubGVuZ3RoID4gMTApID8gdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtpbmRleFZhbF0uaXRlbXMubGVuZ3RoIC0gMSA6IDEwO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnlPYmosIGFwaUZ1bmN0aW9uRGV0YWlscy5hcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiPj4+ZGF0YVwiKTtcclxuICAgICAgICAgIGxldCByZXNwb25zZURhdGEgPSBkYXRhLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IGNvbnRyb2xJdGVtVmFsdWUgPSB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcztcclxuICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucG9wKCk7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2VEYXRhW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV0pIHtcclxuICAgICAgICAgICAgXy5mb3JFYWNoKHJlc3BvbnNlRGF0YVthcGlGdW5jdGlvbkRldGFpbHMucmVzcG9uc2VdWzBdLmRhdGEsIGZ1bmN0aW9uIChkYXRhSXRlbSwgaW5kZXgpIHtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhpbmRleCwgXCI+Pj5pbmRleFwiKVxyXG4gICAgICAgICAgICAgIGxldCBvYmogPSBkYXRhSXRlbTtcclxuICAgICAgICAgICAgICBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5yaXNrUmFua2luZztcclxuICAgICAgICAgICAgICBvYmpbXCJjb250cm9sVHlwZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5jb250cm9sVHlwZTtcclxuICAgICAgICAgICAgICBvYmpbXCJjb250cm9sRGVzY3JpcHRpb25cIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbERlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICAgIG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NHcm91cHNbZGF0YUl0ZW0uYWNjZXNzR3JvdXBdLmFjY2Vzc0dyb3VwTmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJydWxlc2V0TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5ydWxlc2V0c1tyZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucnVsZXNldElkXS5uYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcImRhdGFzb3VyY2VOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmRhdGFzb3VyY2VzW2RhdGFJdGVtLmRhdGFzb3VyY2VdLm5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wic3RhdHVzQ2xhc3NcIl0gPSB7XHJcbiAgICAgICAgICAgICAgICBcIk9wZW5cIjogXCJsYWJlbCBiZy1pbnRpYXRlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJDbG9zZWRcIjogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgIFwiUmVtZWRpYXRlZFwiIDogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgIFwiQXV0aG9yaXplZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgIFwiQXBwcm92ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCJcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2gob2JqKTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLmxlbmd0aCwgXCI+Pj4+IGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoXCIpXHJcbiAgICAgICAgICAgICAgaWYgKGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoICUgMTAgPT0gMCAmJiBpbmRleCA9PSAnOScpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTE9BRCBNT1JFXCIpXHJcbiAgICAgICAgICAgICAgICBsZXQgdGVtcG9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialsnYWNjZXNzR3JvdXAnXSA9IGRhdGFJdGVtLmFjY2Vzc0dyb3VwO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialsnYWNjZXNzQ29udHJvbCddID0gZGF0YUl0ZW0uYWNjZXNzQ29udHJvbDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5yaXNrUmFua2luZztcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sVHlwZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5jb250cm9sVHlwZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sRGVzY3JpcHRpb25cIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbERlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInJvbGVOYW1lXCJdID0gZGF0YUl0ZW0ucm9sZU5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gZGF0YUl0ZW0uZW50aXRsZW1lbnROYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcIl9pZFwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5faWQ7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiZW5hYmxlTG9hZFwiXSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wic3RhdHVzQ2xhc3NcIl0gPSB7XHJcbiAgICAgICAgICAgICAgICAgIFwiT3BlblwiOiBcImxhYmVsIGJnLWludGlhdGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiQ2xvc2VkXCI6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiUmVtZWRpYXRlZFwiIDogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgICAgXCJBdXRob3JpemVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiLFxyXG4gICAgICAgICAgICAgICAgICBcIkFwcHJvdmVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiXHJcbiAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIF8uZm9yRWFjaChzZWxmLmN1cnJlbnRUYWJsZURhdGEucHJvZHVjdEtleXMsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wb2JqW3ZpZXdJdGVtLm5hbWVdID0gZGF0YUl0ZW1bdmlld0l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2godGVtcG9iaik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLCBcIj4+Y29udHJvbEl0ZW1WYWx1ZVwiKTtcclxuICAgICAgICAgIHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zID0gY29udHJvbEl0ZW1WYWx1ZTtcclxuICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgICB9KTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5ncm91cEZpZWxkICYmIHRoaXMuZ3JvdXBGaWVsZC5sZW5ndGggPT0gMikge1xyXG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCBmaXJzdEluZGV4VmFsID0gLTE7XHJcbiAgICAgIF8uZm9yRWFjaChncm91cGluZ0ZpZWxkLCBmdW5jdGlvbiAoZ3JvdXBJdGVtKSB7XHJcbiAgICAgICAgaWYgKHNlbGYuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleChzZWxmLmdyb3VwRmllbGQsIHsgZmllbGQ6IGdyb3VwSXRlbS5uYW1lIH0pID49IDApIHtcclxuICAgICAgICAgIGlmIChncm91cEl0ZW0uaXNJZCkge1xyXG4gICAgICAgICAgICBxdWVyeU9ialsnbG9hZE1vcmUnICsgZ3JvdXBJdGVtLm5hbWVdID0gcm93W2dyb3VwSXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBxdWVyeU9ialsnbG9hZE1vcmUnICsgZ3JvdXBJdGVtLm5hbWVdID0gcm93W2dyb3VwSXRlbS5uYW1lXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBfLmZvckVhY2goZ3JvdXBpbmdGaWVsZCwgZnVuY3Rpb24gKGdyb3VwSXRlbSkge1xyXG4gICAgICAgIGlmIChzZWxmLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgoc2VsZi5ncm91cEZpZWxkLCB7IGZpZWxkOiBncm91cEl0ZW0ubmFtZSB9KSA9PSAwKSB7XHJcbiAgICAgICAgICBmaXJzdEluZGV4VmFsID0gXy5maW5kSW5kZXgoc2VsZi5uZXdUYWJsZURhdGEuZGF0YSwgeyB2YWx1ZTogcm93W2dyb3VwSXRlbS5uYW1lXSB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBsZXQgc2Vjb25kSW5kZXhWYWwgPSAtMTtcclxuICAgICAgXy5mb3JFYWNoKGdyb3VwaW5nRmllbGQsIGZ1bmN0aW9uIChncm91cEl0ZW0pIHtcclxuICAgICAgICBpZiAoc2VsZi5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHNlbGYuZ3JvdXBGaWVsZCwgeyBmaWVsZDogZ3JvdXBJdGVtLm5hbWUgfSkgPT0gMSkge1xyXG4gICAgICAgICAgc2Vjb25kSW5kZXhWYWwgPSBfLmZpbmRJbmRleChzZWxmLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zLCB7IHZhbHVlOiByb3dbZ3JvdXBJdGVtLm5hbWVdIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKGZpcnN0SW5kZXhWYWwsIFwiPj4+IGZpcnN0SW5kZXhWYWxcIik7XHJcbiAgICAgIGNvbnNvbGUubG9nKHNlY29uZEluZGV4VmFsLCBcIj4+Pj4+IHNlY29uZEluZGV4VmFsXCIpO1xyXG4gICAgICBxdWVyeU9iai5sb2FkTW9yZU9mZnNldCA9ICh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtcy5sZW5ndGggPiAxMCkgPyB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtcy5sZW5ndGggLSAxIDogMTA7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0QWxsUmVwb25zZShxdWVyeU9iaiwgYXBpRnVuY3Rpb25EZXRhaWxzLmFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj5kYXRhXCIpO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlRGF0YSA9IGRhdGEucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgY29udHJvbEl0ZW1WYWx1ZSA9IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXNbc2Vjb25kSW5kZXhWYWxdLml0ZW1zO1xyXG4gICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wb3AoKTtcclxuICAgICAgICAgIGlmIChyZXNwb25zZURhdGFbYXBpRnVuY3Rpb25EZXRhaWxzLnJlc3BvbnNlXSkge1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVzcG9uc2VEYXRhW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV1bMF0uZGF0YSwgZnVuY3Rpb24gKGRhdGFJdGVtLCBpbmRleCkge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+PmluZGV4XCIpXHJcbiAgICAgICAgICAgICAgbGV0IG9iaiA9IGRhdGFJdGVtO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmNvbnRyb2xUeXBlO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xEZXNjcmlwdGlvblwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sRGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgICAgb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0dyb3Vwc1tkYXRhSXRlbS5hY2Nlc3NHcm91cF0uYWNjZXNzR3JvdXBOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJ1bGVzZXROYW1lXCJdID0gcmVzcG9uc2VEYXRhLnJ1bGVzZXRzW3Jlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5ydWxlc2V0SWRdLm5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wiZGF0YXNvdXJjZU5hbWVcIl0gPSByZXNwb25zZURhdGEuZGF0YXNvdXJjZXNbZGF0YUl0ZW0uZGF0YXNvdXJjZV0ubmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJzdGF0dXNDbGFzc1wiXSA9IHtcclxuICAgICAgICAgICAgICAgIFwiT3BlblwiOiBcImxhYmVsIGJnLWludGlhdGVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIkNsb3NlZFwiOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJSZW1lZGlhdGVkXCIgOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJBdXRob3JpemVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiLFxyXG4gICAgICAgICAgICAgICAgXCJBcHByb3ZlZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaChvYmopO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoLCBcIj4+Pj4gY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGhcIilcclxuICAgICAgICAgICAgICBpZiAoY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGggJSAxMCA9PSAwICYmIGluZGV4ID09ICc5Jykge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJMT0FEIE1PUkVcIilcclxuICAgICAgICAgICAgICAgIGxldCB0ZW1wb2JqID0ge307XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqWydhY2Nlc3NHcm91cCddID0gZGF0YUl0ZW0uYWNjZXNzR3JvdXA7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqWydhY2Nlc3NDb250cm9sJ10gPSBkYXRhSXRlbS5hY2Nlc3NDb250cm9sO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmNvbnRyb2xUeXBlO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xEZXNjcmlwdGlvblwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sRGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wicm9sZU5hbWVcIl0gPSBkYXRhSXRlbS5yb2xlTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJ1c2VyTmFtZVwiXSA9IGRhdGFJdGVtLnVzZXJOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IGRhdGFJdGVtLmVudGl0bGVtZW50TmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJfaWRcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uX2lkO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImVuYWJsZUxvYWRcIl0gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgXy5mb3JFYWNoKHNlbGYuY3VycmVudFRhYmxlRGF0YS5wcm9kdWN0S2V5cywgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBvYmpbdmlld0l0ZW0ubmFtZV0gPSBkYXRhSXRlbVt2aWV3SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJzdGF0dXNDbGFzc1wiXSA9IHtcclxuICAgICAgICAgICAgICAgICAgXCJPcGVuXCI6IFwibGFiZWwgYmctaW50aWF0ZWRcIixcclxuICAgICAgICAgICAgICAgICAgXCJDbG9zZWRcIjogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgICAgXCJSZW1lZGlhdGVkXCIgOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgICBcIkF1dGhvcml6ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiQXBwcm92ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCJcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wdXNoKHRlbXBvYmopO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZSwgXCI+PmNvbnRyb2xJdGVtVmFsdWVcIik7XHJcbiAgICAgICAgICB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtcyA9IGNvbnRyb2xJdGVtVmFsdWU7XHJcbiAgICAgICAgfSxcclxuICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIEVycm9yIFwiLCBlcnIpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiB0aGlzLmdyb3VwRmllbGQubGVuZ3RoID09IDMpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICBsZXQgZmlyc3RJbmRleFZhbCA9IC0xO1xyXG4gICAgICBfLmZvckVhY2goZ3JvdXBpbmdGaWVsZCwgZnVuY3Rpb24gKGdyb3VwSXRlbSkge1xyXG4gICAgICAgIGlmIChzZWxmLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgoc2VsZi5ncm91cEZpZWxkLCB7IGZpZWxkOiBncm91cEl0ZW0ubmFtZSB9KSA+PSAwKSB7XHJcbiAgICAgICAgICBpZiAoZ3JvdXBJdGVtLmlzSWQpIHtcclxuICAgICAgICAgICAgcXVlcnlPYmpbJ2xvYWRNb3JlJyArIGdyb3VwSXRlbS5uYW1lXSA9IHJvd1tncm91cEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcXVlcnlPYmpbJ2xvYWRNb3JlJyArIGdyb3VwSXRlbS5uYW1lXSA9IHJvd1tncm91cEl0ZW0ubmFtZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgXy5mb3JFYWNoKGdyb3VwaW5nRmllbGQsIGZ1bmN0aW9uIChncm91cEl0ZW0pIHtcclxuICAgICAgICBpZiAoc2VsZi5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHNlbGYuZ3JvdXBGaWVsZCwgeyBmaWVsZDogZ3JvdXBJdGVtLm5hbWUgfSkgPT0gMCkge1xyXG4gICAgICAgICAgZmlyc3RJbmRleFZhbCA9IF8uZmluZEluZGV4KHNlbGYubmV3VGFibGVEYXRhLmRhdGEsIHsgdmFsdWU6IHJvd1tncm91cEl0ZW0ubmFtZV0gfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgbGV0IHNlY29uZEluZGV4VmFsID0gLTE7XHJcbiAgICAgIF8uZm9yRWFjaChncm91cGluZ0ZpZWxkLCBmdW5jdGlvbiAoZ3JvdXBJdGVtKSB7XHJcbiAgICAgICAgaWYgKHNlbGYuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleChzZWxmLmdyb3VwRmllbGQsIHsgZmllbGQ6IGdyb3VwSXRlbS5uYW1lIH0pID09IDEpIHtcclxuICAgICAgICAgIHNlY29uZEluZGV4VmFsID0gXy5maW5kSW5kZXgoc2VsZi5uZXdUYWJsZURhdGEuZGF0YVtmaXJzdEluZGV4VmFsXS5pdGVtcywgeyB2YWx1ZTogcm93W2dyb3VwSXRlbS5uYW1lXSB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBsZXQgdGhpcmRJbmRleFZhbCA9IC0xO1xyXG4gICAgICBfLmZvckVhY2goZ3JvdXBpbmdGaWVsZCwgZnVuY3Rpb24gKGdyb3VwSXRlbSkge1xyXG4gICAgICAgIGlmIChzZWxmLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgoc2VsZi5ncm91cEZpZWxkLCB7IGZpZWxkOiBncm91cEl0ZW0ubmFtZSB9KSA9PSAyKSB7XHJcbiAgICAgICAgICB0aGlyZEluZGV4VmFsID0gXy5maW5kSW5kZXgoc2VsZi5uZXdUYWJsZURhdGEuZGF0YVtmaXJzdEluZGV4VmFsXS5pdGVtc1tzZWNvbmRJbmRleFZhbF0uaXRlbXMsIHsgdmFsdWU6IHJvd1tncm91cEl0ZW0ubmFtZV0gfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2coZmlyc3RJbmRleFZhbCwgXCI+Pj4gZmlyc3RJbmRleFZhbFwiKTtcclxuICAgICAgY29uc29sZS5sb2coc2Vjb25kSW5kZXhWYWwsIFwiPj4+Pj4gc2Vjb25kSW5kZXhWYWxcIik7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXJkSW5kZXhWYWwsIFwiPj4+Pj4gdGhpcmRJbmRleFZhbFwiKTtcclxuICAgICAgcXVlcnlPYmoubG9hZE1vcmVPZmZzZXQgPSAodGhpcy5uZXdUYWJsZURhdGEuZGF0YVtmaXJzdEluZGV4VmFsXS5pdGVtc1tzZWNvbmRJbmRleFZhbF0uaXRlbXNbdGhpcmRJbmRleFZhbF0uaXRlbXMubGVuZ3RoID4gMTApID8gdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtmaXJzdEluZGV4VmFsXS5pdGVtc1tzZWNvbmRJbmRleFZhbF0uaXRlbXNbdGhpcmRJbmRleFZhbF0uaXRlbXMubGVuZ3RoIC0gMSA6IDEwO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnlPYmosIGFwaUZ1bmN0aW9uRGV0YWlscy5hcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiPj4+ZGF0YVwiKTtcclxuICAgICAgICAgIGxldCByZXNwb25zZURhdGEgPSBkYXRhLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IGNvbnRyb2xJdGVtVmFsdWUgPSB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtc1t0aGlyZEluZGV4VmFsXS5pdGVtcztcclxuICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucG9wKCk7XHJcbiAgICAgICAgICBpZiAocmVzcG9uc2VEYXRhW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV0pIHtcclxuICAgICAgICAgICAgXy5mb3JFYWNoKHJlc3BvbnNlRGF0YVthcGlGdW5jdGlvbkRldGFpbHMucmVzcG9uc2VdWzBdLmRhdGEsIGZ1bmN0aW9uIChkYXRhSXRlbSwgaW5kZXgpIHtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhpbmRleCwgXCI+Pj5pbmRleFwiKVxyXG4gICAgICAgICAgICAgIGxldCBvYmogPSBkYXRhSXRlbTtcclxuICAgICAgICAgICAgICBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5yaXNrUmFua2luZztcclxuICAgICAgICAgICAgICBvYmpbXCJjb250cm9sVHlwZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5jb250cm9sVHlwZTtcclxuICAgICAgICAgICAgICBvYmpbXCJjb250cm9sRGVzY3JpcHRpb25cIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbERlc2NyaXB0aW9uO1xyXG5cclxuICAgICAgICAgICAgICBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzR3JvdXBzW2RhdGFJdGVtLmFjY2Vzc0dyb3VwXS5hY2Nlc3NHcm91cE5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wicnVsZXNldE5hbWVcIl0gPSByZXNwb25zZURhdGEucnVsZXNldHNbcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJ1bGVzZXRJZF0ubmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJkYXRhc291cmNlTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5kYXRhc291cmNlc1tkYXRhSXRlbS5kYXRhc291cmNlXS5uYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInN0YXR1c0NsYXNzXCJdID0ge1xyXG4gICAgICAgICAgICAgICAgXCJPcGVuXCI6IFwibGFiZWwgYmctaW50aWF0ZWRcIixcclxuICAgICAgICAgICAgICAgIFwiQ2xvc2VkXCI6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIlJlbWVkaWF0ZWRcIiA6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIkF1dGhvcml6ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCIsXHJcbiAgICAgICAgICAgICAgICBcIkFwcHJvdmVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wdXNoKG9iaik7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGgsIFwiPj4+PiBjb250cm9sSXRlbVZhbHVlLmxlbmd0aFwiKVxyXG4gICAgICAgICAgICAgIGlmIChjb250cm9sSXRlbVZhbHVlLmxlbmd0aCAlIDEwID09IDAgJiYgaW5kZXggPT0gJzknKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkxPQUQgTU9SRVwiKVxyXG4gICAgICAgICAgICAgICAgbGV0IHRlbXBvYmogPSB7fTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0dyb3VwJ10gPSBkYXRhSXRlbS5hY2Nlc3NHcm91cDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0NvbnRyb2wnXSA9IGRhdGFJdGVtLmFjY2Vzc0NvbnRyb2w7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wicmlza1JhbmtpbmdcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucmlza1Jhbmtpbmc7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbFR5cGVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uY29udHJvbFR5cGU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbERlc2NyaXB0aW9uXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xEZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJyb2xlTmFtZVwiXSA9IGRhdGFJdGVtLnJvbGVOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInVzZXJOYW1lXCJdID0gZGF0YUl0ZW0udXNlck5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gZGF0YUl0ZW0uZW50aXRsZW1lbnROYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcIl9pZFwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5faWQ7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiZW5hYmxlTG9hZFwiXSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goc2VsZi5jdXJyZW50VGFibGVEYXRhLnByb2R1Y3RLZXlzLCBmdW5jdGlvbiAodmlld0l0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcG9ialt2aWV3SXRlbS5uYW1lXSA9IGRhdGFJdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInN0YXR1c0NsYXNzXCJdID0ge1xyXG4gICAgICAgICAgICAgICAgICBcIk9wZW5cIjogXCJsYWJlbCBiZy1pbnRpYXRlZFwiLFxyXG4gICAgICAgICAgICAgICAgICBcIkNsb3NlZFwiOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgICBcIlJlbWVkaWF0ZWRcIiA6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiQXV0aG9yaXplZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgICAgXCJBcHByb3ZlZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2godGVtcG9iaik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLCBcIj4+Y29udHJvbEl0ZW1WYWx1ZVwiKTtcclxuICAgICAgICAgIHRoaXMubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXNbc2Vjb25kSW5kZXhWYWxdLml0ZW1zW3RoaXJkSW5kZXhWYWxdLml0ZW1zID0gY29udHJvbEl0ZW1WYWx1ZTtcclxuICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGxvYWRNb3JlQ2xpY2tfYmFja3VwKHJvdykge1xyXG4gICAgY29uc29sZS5sb2cocm93LCBcIj4+Pj4+Pj4gUk9XXCIpXHJcbiAgICBsZXQgYXBpRnVuY3Rpb25EZXRhaWxzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLm9uR3JvdXBpbmdGdW5jdGlvbjtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMucXVlcnlQYXJhbXMsIFwiPj4+IFFVRVJZIFBBUkFNU1wiKVxyXG4gICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4gVEhJU1wiKVxyXG4gICAgbGV0IGluZGV4VmFsID0gLTE7XHJcbiAgICBsZXQgcXVlcnlPYmogPSB0aGlzLnF1ZXJ5UGFyYW1zO1xyXG5cclxuICAgIGlmICh0aGlzLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgodGhpcy5ncm91cEZpZWxkLCB7IGZpZWxkOiBcImNvbnRyb2xOYW1lXCIgfSkgPj0gMCkge1xyXG4gICAgICBxdWVyeU9iai5sb2FkTW9yZUNvbnRyb2wgPSByb3cuY29udHJvbE5hbWU7XHJcbiAgICAgIGluZGV4VmFsID0gXy5maW5kSW5kZXgodGhpcy5uZXdUYWJsZURhdGEuZGF0YSwgeyB2YWx1ZTogcm93LmNvbnRyb2xOYW1lIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleCh0aGlzLmdyb3VwRmllbGQsIHsgZmllbGQ6IFwidXNlck5hbWVcIiB9KSA+PSAwKSB7XHJcbiAgICAgIHF1ZXJ5T2JqLmxvYWRNb3JlVXNlck5hbWUgPSByb3cudXNlck5hbWU7XHJcbiAgICAgIGluZGV4VmFsID0gXy5maW5kSW5kZXgodGhpcy5uZXdUYWJsZURhdGEuZGF0YSwgeyB2YWx1ZTogcm93LnVzZXJOYW1lIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleCh0aGlzLmdyb3VwRmllbGQsIHsgZmllbGQ6IFwiZW50aXRsZW1lbnROYW1lXCIgfSkgPj0gMCkge1xyXG4gICAgICBxdWVyeU9iai5sb2FkTW9yZUdyb3VwTmFtZSA9IHJvdy5hY2Nlc3NHcm91cDtcclxuICAgICAgaW5kZXhWYWwgPSBfLmZpbmRJbmRleCh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLCB7IHZhbHVlOiByb3cuZW50aXRsZW1lbnROYW1lIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleCh0aGlzLmdyb3VwRmllbGQsIHsgZmllbGQ6IFwicm9sZU5hbWVcIiB9KSA+PSAwKSB7XHJcbiAgICAgIHF1ZXJ5T2JqLmxvYWRNb3JlUmVzcE5hbWUgPSByb3cucm9sZU5hbWU7XHJcbiAgICAgIGluZGV4VmFsID0gXy5maW5kSW5kZXgodGhpcy5uZXdUYWJsZURhdGEuZGF0YSwgeyB2YWx1ZTogcm93LnJvbGVOYW1lIH0pO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coaW5kZXhWYWwsIFwiPj4+Pj4gaW5kZXhWYWxcIilcclxuXHJcbiAgICBxdWVyeU9iai5sb2FkTW9yZU9mZnNldCA9ICh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcy5sZW5ndGggPiAxMCkgPyB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcy5sZW5ndGggLSAxIDogMTA7XHJcblxyXG4gICAgY29uc29sZS5sb2cocXVlcnlPYmosIFwiPj4+PiBxdWVlcnkgT2JqXCIpXHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5T2JqLCBhcGlGdW5jdGlvbkRldGFpbHMuYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiPj4+ZGF0YVwiKTtcclxuICAgICAgICBsZXQgcmVzcG9uc2VEYXRhID0gZGF0YS5yZXNwb25zZTtcclxuICAgICAgICAvLyBpZihfLmZpbmRJbmRleCh0aGlzLmdyb3VwRmllbGQse2ZpZWxkIDogJ3VzZXJOYW1lJ30pID49IDApe1xyXG5cclxuICAgICAgICAvLyB9ZWxzZXtcclxuXHJcbiAgICAgICAgLy8gfVxyXG4gICAgICAgIGlmICh0aGlzLmdyb3VwRmllbGQgJiYgdGhpcy5ncm91cEZpZWxkLmxlbmd0aCA9PSAxKSB7XHJcbiAgICAgICAgICBsZXQgY29udHJvbEl0ZW1WYWx1ZSA9IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zO1xyXG4gICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wb3AoKTtcclxuICAgICAgICAgIGlmIChyZXNwb25zZURhdGEudXNlckNvbmZsaWN0cykge1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVzcG9uc2VEYXRhLnVzZXJDb25mbGljdHNbMF0uZGF0YSwgZnVuY3Rpb24gKGRhdGFJdGVtLCBpbmRleCkge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+PmluZGV4XCIpXHJcbiAgICAgICAgICAgICAgbGV0IG9iaiA9IGRhdGFJdGVtO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgICAgIG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NHcm91cHNbZGF0YUl0ZW0uYWNjZXNzR3JvdXBdLmFjY2Vzc0dyb3VwTmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJydWxlc2V0TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5ydWxlc2V0c1tyZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucnVsZXNldElkXS5uYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcImRhdGFzb3VyY2VOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmRhdGFzb3VyY2VzW2RhdGFJdGVtLmRhdGFzb3VyY2VdLm5hbWU7XHJcbiAgICAgICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wdXNoKG9iaik7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGgsIFwiPj4+PiBjb250cm9sSXRlbVZhbHVlLmxlbmd0aFwiKVxyXG4gICAgICAgICAgICAgIGlmIChjb250cm9sSXRlbVZhbHVlLmxlbmd0aCAlIDEwID09IDAgJiYgaW5kZXggPT0gJzknKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkxPQUQgTU9SRVwiKVxyXG4gICAgICAgICAgICAgICAgbGV0IHRlbXBvYmogPSB7fTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0dyb3VwJ10gPSBkYXRhSXRlbS5hY2Nlc3NHcm91cDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0NvbnRyb2wnXSA9IGRhdGFJdGVtLmFjY2Vzc0NvbnRyb2w7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wicmlza1JhbmtpbmdcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucmlza1Jhbmtpbmc7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wicm9sZU5hbWVcIl0gPSBkYXRhSXRlbS5yb2xlTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSBkYXRhSXRlbS5lbnRpdGxlbWVudE5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiX2lkXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLl9pZDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJlbmFibGVMb2FkXCJdID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaCh0ZW1wb2JqKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGNvbnNvbGUubG9nKGNvbnRyb2xJdGVtVmFsdWUsIFwiPj5jb250cm9sSXRlbVZhbHVlXCIpO1xyXG4gICAgICAgICAgdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtpbmRleFZhbF0uaXRlbXMgPSBjb250cm9sSXRlbVZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5ncm91cEZpZWxkICYmIHRoaXMuZ3JvdXBGaWVsZC5sZW5ndGggPT0gMikge1xyXG4gICAgICAgICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4+IFRISVNcIik7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZURhdGEsIFwiPj4+PnJlc3BvbnNlRGF0YVwiKVxyXG4gICAgICAgICAgbGV0IGNvbnRyb2xJdGVtVmFsdWUgPSB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcztcclxuICAgICAgICAgIGxldCBzZWNvbmRWYWx1ZUluZGV4ID0gXy5maW5kSW5kZXgoY29udHJvbEl0ZW1WYWx1ZSwgeyB2YWx1ZTogcm93LnVzZXJOYW1lIH0pXHJcbiAgICAgICAgICBsZXQgc2Vjb25kR3JvdXBWYWwgPSBjb250cm9sSXRlbVZhbHVlW3NlY29uZFZhbHVlSW5kZXhdLml0ZW1zO1xyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2coc2Vjb25kR3JvdXBWYWwsXCI+Pj4gR1JPVVAgVkFMXCIpXHJcbiAgICAgICAgICBzZWNvbmRHcm91cFZhbC5wb3AoKTtcclxuICAgICAgICAgIC8vIGlmKHJlc3BvbnNlRGF0YS51c2VyQ29uZmxpY3RzKXtcclxuICAgICAgICAgIC8vICAgXy5mb3JFYWNoKHJlc3BvbnNlRGF0YS51c2VyQ29uZmxpY3RzWzBdLmRhdGEsIGZ1bmN0aW9uKGRhdGFJdGVtLCBpbmRleCl7XHJcbiAgICAgICAgICAvLyAgICAgY29uc29sZS5sb2coaW5kZXgsXCI+Pj5pbmRleFwiKVxyXG4gICAgICAgICAgLy8gICAgIGxldCBvYmogPSBkYXRhSXRlbTtcclxuICAgICAgICAgIC8vICAgICAgIG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgLy8gICAgICAgb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0dyb3Vwc1tkYXRhSXRlbS5hY2Nlc3NHcm91cF0uYWNjZXNzR3JvdXBOYW1lO1xyXG4gICAgICAgICAgLy8gICAgICAgb2JqW1wicnVsZXNldE5hbWVcIl0gPSByZXNwb25zZURhdGEucnVsZXNldHNbcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJ1bGVzZXRJZF0ubmFtZTtcclxuICAgICAgICAgIC8vICAgICAgIG9ialtcImRhdGFzb3VyY2VOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmRhdGFzb3VyY2VzW2RhdGFJdGVtLmRhdGFzb3VyY2VdLm5hbWU7XHJcbiAgICAgICAgICAvLyAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2gob2JqKTtcclxuICAgICAgICAgIC8vICAgICAgIGNvbnNvbGUubG9nKGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoLFwiPj4+PiBjb250cm9sSXRlbVZhbHVlLmxlbmd0aFwiKVxyXG4gICAgICAgICAgLy8gICAgICAgaWYoY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGggJSAxMCA9PSAwICYmIGluZGV4ID09ICc5Jyl7XHJcbiAgICAgICAgICAvLyAgICAgICAgIGNvbnNvbGUubG9nKFwiTE9BRCBNT1JFXCIpXHJcbiAgICAgICAgICAvLyAgICAgICAgIGxldCB0ZW1wb2JqPXt9O1xyXG4gICAgICAgICAgLy8gICAgICAgICB0ZW1wb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAvLyAgICAgICAgIHRlbXBvYmpbXCJyb2xlTmFtZVwiXSA9IGRhdGFJdGVtLnJvbGVOYW1lO1xyXG4gICAgICAgICAgLy8gICAgICAgICB0ZW1wb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gZGF0YUl0ZW0uZW50aXRsZW1lbnROYW1lO1xyXG4gICAgICAgICAgLy8gICAgICAgICB0ZW1wb2JqW1wiX2lkXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLl9pZDtcclxuICAgICAgICAgIC8vICAgICAgICAgdGVtcG9ialtcImVuYWJsZUxvYWRcIl0gPSB0cnVlO1xyXG4gICAgICAgICAgLy8gICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2godGVtcG9iaik7XHJcbiAgICAgICAgICAvLyAgIH1cclxuICAgICAgICAgIC8vICAgfSk7XHJcbiAgICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZSxcIj4+Y29udHJvbEl0ZW1WYWx1ZVwiKTtcclxuICAgICAgICAgIC8vIHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zID0gY29udHJvbEl0ZW1WYWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vICAgLy8gY29uc3RydWN0R3JvdXBlZERhL3RhKGRhdGEsIClcclxuICAgICAgfSxcclxuICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiIEVycm9yIFwiLCBlcnIpO1xyXG4gICAgICAgIH0pO1xyXG4gIH1cclxuICBncm91cENvbGxhcHNlKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCI+Pj4gRVZOVCBWQUxcIik7XHJcbiAgfVxyXG4gIHVwZGF0ZVRhYmxlVmlldygpIHtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlSGVhZGVyKSB7XHJcbiAgICAgIGxldCBjdXJyZW50VGFibGVIZWFkZXIgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwic2VsZWN0ZWRUYWJsZUhlYWRlcnNcIilcclxuICAgICAgKTtcclxuICAgICAgbGV0IHRhYmxlSGVhZGVyTGVuZ3RoID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlSGVhZGVyLmxlbmd0aDtcclxuICAgICAgLy8gdGhpcy5jb2x1bW5zID0gY3VycmVudFRhYmxlSGVhZGVyO1xyXG4gICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSBbXTtcclxuICAgICAgaWYgKF8uZmluZEluZGV4KHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZUhlYWRlciwgeyB2YWx1ZTogXCJzZWxlY3RcIiB9KSA+IC0xKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2godGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlSGVhZGVyWzBdKTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IGZpbHRlckFycmF5ID0gY3VycmVudFRhYmxlSGVhZGVyXHJcbiAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICByZXR1cm4gdmFsLmlzQWN0aXZlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSBfLmNvbmNhdCh0aGlzLmRpc3BsYXllZENvbHVtbnMsIGZpbHRlckFycmF5KTtcclxuICAgICAgaWYgKF8uZmluZEluZGV4KHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZUhlYWRlciwgeyB2YWx1ZTogXCJhY3Rpb25cIiB9KSA+IC0xKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2godGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlSGVhZGVyW3RhYmxlSGVhZGVyTGVuZ3RoIC0gMV0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gIH1cclxuICBvbkxvYWREYXRhKGVuYWJsZU5leHQpIHtcclxuXHJcbiAgICBpZih0aGlzLmRhdGE9PSdyZWZyZXNoUGFnZScpe1xyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wib2Zmc2V0XCJdID0gMDtcclxuICAgICAgdGhpcy5xdWVyeVBhcmFtc1tcImxpbWl0XCJdID0gMTA7XHJcbiAgICAgIHRoaXMub2Zmc2V0ID0gMDtcclxuICAgIH1cclxuICAgIHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUgPSB7fTtcclxuICAgIHRoaXMuY29sdW1ucyA9IFtdO1xyXG4gICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gW107XHJcbiAgICBsZXQgdGFibGVBcnJheSA9IHRoaXMub25Mb2FkXHJcbiAgICAgID8gdGhpcy5vbkxvYWQudGFibGVEYXRhXHJcbiAgICAgIDogdGhpcy5jdXJyZW50Q29uZmlnRGF0YVtcImxpc3RWaWV3XCJdLnRhYmxlRGF0YTtcclxuICAgIGxldCByZXNwb25zZUtleSA9XHJcbiAgICAgIHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLnJlc3BvbnNlS2V5ID8gdGhpcy5vbkxvYWQucmVzcG9uc2VLZXkgOiBudWxsO1xyXG4gICAgbGV0IGluZGV4ID0gXy5maW5kSW5kZXgodGFibGVBcnJheSwgeyB0YWJsZUlkOiB0aGlzLnRhYmxlSWQgfSk7XHJcbiAgICBpZiAoaW5kZXggPiAtMSlcclxuICAgICAgdGhpcy5jdXJyZW50VGFibGVEYXRhID0gdGFibGVBcnJheVtpbmRleF07XHJcbiAgICBjb25zb2xlLmxvZyh0YWJsZUFycmF5LCBcIi4uLi4udGFibGVBcnJheVwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY3VycmVudFRhYmxlRGF0YSwgXCIuLi4uLmN1cnJlbnRUYWJsZURhdGFcIik7XHJcbiAgICBsZXQgY2hlY2tPcHRpb25GaWx0ZXIgPSAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5lbmFibGVPcHRpb25zRmlsdGVyKSA/IHRydWUgOiBmYWxzZVxyXG4gICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEuZW5hYmxlTXVsdGlTZWxlY3QpIHtcclxuICAgICAgdGhpcy5lbmFibGVPcHRpb25GaWx0ZXIgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKGNoZWNrT3B0aW9uRmlsdGVyKSB7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgdGhpcy5lbmFibGVPcHRpb25GaWx0ZXIgPSB0cnVlO1xyXG4gICAgICBsZXQgT3B0aW9uZGF0YSA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5maWx0ZXJPcHRpb25Mb2FkRnVuY3Rpb247XHJcbiAgICAgIHRoaXMub3B0aW9uRmlsdGVyRmllbGRzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLm9wdGlvbkZpZWxkcztcclxuICAgICAgdmFyIGFwaVVybCA9IE9wdGlvbmRhdGEuYXBpVXJsO1xyXG4gICAgICB2YXIgcmVzcG9uc2VOYW1lID0gT3B0aW9uZGF0YS5yZXNwb25zZTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UodGhpcy5xdWVyeVBhcmFtcywgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgIGxldCB0ZW1wQXJyYXkgPSAocmVzLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0pID8gcmVzLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gOiBbXTtcclxuXHJcbiAgICAgICAgICBzZWxmLm9wdGlvbkZpbHRlcmRhdGEgPSB0ZW1wQXJyYXk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5lbmFibGVVaWZpbHRlcikge1xyXG4gICAgICBsZXQgZmlsdGVyS2V5ID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnVpRmlsdGVyS2V5O1xyXG4gICAgICB0aGlzLmVuYWJsZVVJZmlsdGVyID0gdHJ1ZTtcclxuICAgICAgLy8gdGhpcy5kYXRhU291cmNlLmZpbHRlclByZWRpY2F0ZSA9IChkYXRhOiBFbGVtZW50LCBmaWx0ZXI6IHN0cmluZykgPT4ge1xyXG4gICAgICAvLyAgIHJldHVybiBkYXRhW2ZpbHRlcktleV0gPT0gZmlsdGVyO1xyXG4gICAgICAvLyAgfTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLm9uTG9hZCAmJiB0aGlzLm9uTG9hZC5yZXNwb25zZSAmJiAhZW5hYmxlTmV4dCkge1xyXG4gICAgICB0aGlzLmdldExvYWREYXRhKFxyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlRGF0YSxcclxuICAgICAgICB0aGlzLm9uTG9hZC5yZXNwb25zZSxcclxuICAgICAgICB0aGlzLm9uTG9hZC50b3RhbCxcclxuICAgICAgICByZXNwb25zZUtleVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YSkgdGhpcy5nZXREYXRhKHRoaXMuY3VycmVudFRhYmxlRGF0YSwgbnVsbCk7XHJcbiAgICB9XHJcbiAgICBpZiAoIWVuYWJsZU5leHQpIHtcclxuICAgICAgLy8gdGhpcy5wYWdpbmF0b3IuZmlyc3RQYWdlKCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldERhdGEoY3VycmVudFRhYmxlVmFsdWUsIGVuYWJsZVRhYmxlU2VhcmNoKSB7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgdGhpcy5jb2x1bW5zID0gY3VycmVudFRhYmxlVmFsdWUudGFibGVIZWFkZXI7XHJcbiAgICBpZiAoY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgdmFyIGFwaVVybCA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgdmFyIHJlc3BvbnNlTmFtZSA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICBsZXQga2V5VG9TZXQgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5rZXlGb3JTdHJpbmdSZXNwb25zZTtcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXTtcclxuICAgICAgdmFyIGluY2x1ZGVSZXF1ZXN0ID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uaW5jbHVkZXJlcXVlc3REYXRhID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAvL2lmICggY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uZW5hYmxlTG9hZGVyKSB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAvL31cclxuICAgICAgbGV0IGR5bmFtaWNMb2FkID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uZHluYW1pY1JlcG9ydExvYWQ7ICAvLyBjb25kaXRpb24gY2hlY2tlZCB3aGVuIGltcGxlbWVudCBhZGRpdGlvbmFsIHJlcG9ydCB2aWV3XHJcbiAgICAgIGxldCByZXF1ZXN0ZWRRdWVyeSA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHZhciByZXF1ZXN0UXVlcnlQYXJhbSA9IHt9O1xyXG4gICAgICBfLmZvckVhY2godGhpcy5ncm91cEZpZWxkLCBmdW5jdGlvbiAoZXZlbnRJdGVtKSB7XHJcbiAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tldmVudEl0ZW0uZmllbGRdID0gdHJ1ZTtcclxuICAgICAgfSlcclxuICAgICAgaWYgKGR5bmFtaWNMb2FkKSB7XHJcbiAgICAgICAgbGV0IHRlbXBPYmogPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIkN1cnJlbnRSZXBvcnREYXRhXCIpO1xyXG4gICAgICAgIGxldCByZXBvcnRJVGVtID0gSlNPTi5wYXJzZSh0ZW1wT2JqKTtcclxuICAgICAgICBfLmZvckVhY2gocmVxdWVzdGVkUXVlcnksIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmZyb21DdXJyZW50RGF0YSkge1xyXG4gICAgICAgICAgICByZXF1ZXN0UXVlcnlQYXJhbVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuY3VycmVudERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXF1ZXN0UXVlcnlQYXJhbVtyZXF1ZXN0SXRlbS5uYW1lXSA9IChyZXBvcnRJVGVtW3JlcXVlc3RJdGVtLnZhbHVlXSkgPyByZXBvcnRJVGVtW3JlcXVlc3RJdGVtLnZhbHVlXSA6IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2UgeyBcclxuICAgICAgICBpZighZW5hYmxlVGFibGVTZWFyY2gpIHtcclxuICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0ZWRRdWVyeSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICAgIC8vICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5zdWJLZXlcclxuICAgICAgICAgICAgLy8gID8gZGV0YWlsc1tpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgICAgLy8gIDogZGV0YWlsc1tpdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmZyb21DdXJyZW50RGF0YSkge1xyXG4gICAgICAgICAgICAgIHJlcXVlc3RRdWVyeVBhcmFtW3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5jdXJyZW50RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0SXRlbS5kaXJlY3RBc3NpZ24pIHtcclxuICAgICAgICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHJlcXVlc3RJdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICAgICAgOiByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNleyBcclxuICAgICAgICAgICAgICBsZXQgZXhpc3RDaGVjayA9IF8uaGFzKHNlbGYuaW5wdXREYXRhLCByZXF1ZXN0SXRlbS52YWx1ZSk7XHJcbiAgICAgICAgICAgIC8vICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV0gPyBpdGVtW3ZpZXdJdGVtLnZhbHVlXVt2aWV3SXRlbS5zdWJrZXldIDogXCJcIjtcclxuICAgICAgICAgICAgICBpZihleGlzdENoZWNrKXtcclxuICAgICAgICAgICAgICAgbGV0ICB0ZW1wRGF0YSA9IHJlcXVlc3RJdGVtLnN1YktleVxyXG4gICAgICAgICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcblxyXG4gICAgICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkodGVtcERhdGEpXHJcbiAgICAgICAgICAgICAgICA6IHRlbXBEYXRhO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgfVxyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wiZGlzYWJsZUdyb3VwXCJdID0gKHRoaXMuZ3JvdXBGaWVsZCAmJiB0aGlzLmdyb3VwRmllbGQubGVuZ3RoKSA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IHsgLi4udGhpcy5xdWVyeVBhcmFtcywgLi4ucmVxdWVzdFF1ZXJ5UGFyYW0gfTtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRBbGxSZXBvbnNlKHRoaXMucXVlcnlQYXJhbXMsIGFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgLy8gaWYgKCBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5lbmFibGVMb2FkZXIpIHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAvL31cclxuICAgICAgICAgIGxldCBzZWxlY3RlZFRhYmxlSGVhZGVycyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwic2VsZWN0ZWRUYWJsZUhlYWRlcnNcIik7ICAvLyBhZnRlciBnbG9iYWwgc2V0dGluZyBjaGFuZ2VcclxuICAgICAgICAgIHRoaXMuY29sdW1ucyA9ICFfLmlzRW1wdHkoc2VsZWN0ZWRUYWJsZUhlYWRlcnMpID8gSlNPTi5wYXJzZShzZWxlY3RlZFRhYmxlSGVhZGVycykgOiB0aGlzLmNvbHVtbnM7XHJcbiAgICAgICAgICB0aGlzLmVuYWJsZVNlYXJjaCA9IHRoaXMuY3VycmVudERhdGEuZW5hYmxlR2xvYmFsU2VhcmNoO1xyXG4gICAgICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gdGhpcy5jb2x1bW5zXHJcbiAgICAgICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKHZhbCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiB2YWwuaXNBY3RpdmU7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCI+PiAgdGhpcy5kaXNwbGF5ZWRDb2x1bW4gXCIsIHRoaXMuZGlzcGxheWVkQ29sdW1ucyk7XHJcbiAgICAgICAgICBsZXQgdGVtcEFycmF5ID0gW107XHJcbiAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhzZWxmLCBcIj4+Pj4+Pj4+Pj5USElTU1NTXCIpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJEYXRhIFJlc3BvbnNlICoqKioqXCIsIGRhdGEucmVzcG9uc2UpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJEYXRhIFJlc3BvbnNlIE5hbWUgKioqKipcIiwgcmVzcG9uc2VOYW1lKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGN1cnJlbnRUYWJsZVZhbHVlLCBcIkN1cnIgVGFibGUgZGF0YSA+Pj4+LlwiKTtcclxuXHJcblxyXG4gICAgICAgICAgXy5mb3JFYWNoKGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSwgZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiSXRlbSBEYXRhID4+Pj4+PlwiLCBpdGVtKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0eXBlb2YgaXRlbSAhPT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgICAgICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgdGVtcE9ialtrZXlUb1NldF0gPSBpdGVtO1xyXG4gICAgICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIC8vIGlmIChzZWxmLnRhYmxlSWQgPT0gXCJub3RpZmljYXRpb25zXCIpIHtcclxuICAgICAgICAgICAgICAvLyAgIGl0ZW0uZmVhdHVyZSA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAvLyAgICAgXCJOT1RJRklDQVRJT05fRk9STUFUUy5GRUFUVVJFLlwiICsgaXRlbS5tZXNzYWdlT2JqLmZlYXR1cmUpO1xyXG4gICAgICAgICAgICAgIC8vICAgbGV0IG9wZXJhdGlvblN0YXR1cyA9IGl0ZW0ubWVzc2FnZU9iai5hY3Rpb24gKyBcIiAgXCIgKyBpdGVtLnN0YXR1cztcclxuICAgICAgICAgICAgICAvLyAgIGNvbnNvbGUubG9nKFwiT3BlcmF0aW9uIERhdGFcIiwgb3BlcmF0aW9uU3RhdHVzKTtcclxuXHJcbiAgICAgICAgICAgICAgLy8gICBpdGVtLnN0YXR1c19hY3Rpb24gPSBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXCJOT1RJRklDQVRJT05fRk9STUFUUy5BQ1RJT05fU1RBVFVTLlwiICsgb3BlcmF0aW9uU3RhdHVzKTtcclxuICAgICAgICAgICAgICAvLyAgIGl0ZW0ubXNnID0gaXRlbS5mZWF0dXJlICsgXCIgICBcIiArIGl0ZW0uc3RhdHVzX2FjdGlvbjtcclxuICAgICAgICAgICAgICAvLyAgIGNvbnNvbGUubG9nKFwiRW50cnkgb2YgQ29uZGl0aW9uIGl0ZW1cIik7XHJcbiAgICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCIqKioqKioqKioqIEl0ZW0gRGF0YSAqKioqKioqKioqKioqKlwiLCBpdGVtKTtcclxuICAgICAgICAgICAgICBfLmZvckVhY2goY3VycmVudFRhYmxlVmFsdWUuZGF0YVZpZXdGb3JtYXQsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJWaWV3IEl0ZW0gb2YgRGF0YXZpZXcgRm9ybWF0ID4+Pj5cIiwgdmlld0l0ZW0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICh2aWV3SXRlbS5zdWJrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgaWYgKHZpZXdJdGVtLmFzc2lnbkZpcnN0SW5kZXh2YWwpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzdWx0VmFsdWUgPSAoIV8uaXNFbXB0eShpdGVtW3ZpZXdJdGVtLnZhbHVlXSkgJiYgaXRlbVt2aWV3SXRlbS52YWx1ZV1bMF1bdmlld0l0ZW0uc3Via2V5XSkgPyAoaXRlbVt2aWV3SXRlbS52YWx1ZV1bMF1bdmlld0l0ZW0uc3Via2V5XSkgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICAgICAgPyByZXN1bHRWYWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICAgICAgPyBpdGVtW3ZpZXdJdGVtLnZhbHVlXVt2aWV3SXRlbS5zdWJrZXldXHJcbiAgICAgICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodmlld0l0ZW0uaXNDb25kaXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGV2YWwodmlld0l0ZW0uY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHZpZXdJdGVtLmlzQWRkRGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSB2aWV3SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICh2aWV3SXRlbS5pc05vdGlmeSkge1xyXG4gICAgICAgICAgICAgICAgICAvLyB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gZXZhbCh2aWV3SXRlbS5jb25kaXRpb24pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgbGV0IGZlYXQgPSBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgXCJOT1RJRklDQVRJT05fRk9STUFUUy5GRUFUVVJFLlwiICsgKGl0ZW1bdmlld0l0ZW0udmFsdWVdLmZlYXR1cmUpKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgIGxldCBvcGVyYXRpb25TdGF0dXMgPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXS5hY3Rpb24gKyBcIl9cIiArIGl0ZW1bdmlld0l0ZW0udmFsdWVdLnN0YXR1cztcclxuICAgICAgICAgICAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOm1heC1saW5lLWxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICBsZXQgc3RhdHVzX2FjdGlvbiA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcIk5PVElGSUNBVElPTl9GT1JNQVRTLkFDVElPTl9TVEFUVVMuXCIgKyBvcGVyYXRpb25TdGF0dXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgbGV0IG1lc3NhZ2UgPSBmZWF0ICsgXCIgXCIgKyBzdGF0dXNfYWN0aW9uO1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gbWVzc2FnZTtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialtcImZlYXR1cmVOYW1lXCJdID0gZmVhdDtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialtcIm9wZXJhdGlvbk5hbWVcIl0gPSBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXCJOT1RJRklDQVRJT05fRk9STUFUUy5BQ1RJT04uXCIgKyBpdGVtW3ZpZXdJdGVtLnZhbHVlXS5hY3Rpb24pO1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW1widHlwZU5hbWVcIl0gPSBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXCJOT1RJRklDQVRJT05fRk9STUFUUy5UWVBFLlwiICsgaXRlbVt2aWV3SXRlbS52YWx1ZV0udHlwZSk7XHJcbiAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJlbnRUYWJsZVZhbHVlLmxvYWRMYWJlbEZyb21Db25maWcpIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9XHJcbiAgICAgICAgICAgICAgICAgICAgY3VycmVudFRhYmxlVmFsdWUuaGVhZGVyTGlzdFtpdGVtW3ZpZXdJdGVtLnZhbHVlXV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgbGV0IGZpbHRlck9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgIGZpbHRlck9ialtjdXJyZW50VGFibGVWYWx1ZS5maWx0ZXJLZXldID0gaXRlbVtjdXJyZW50VGFibGVWYWx1ZS5kYXRhRmlsdGVyS2V5XTtcclxuICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRWYWx1ZSA9IF8uZmluZChzZWxmLmlucHV0RGF0YVtjdXJyZW50VGFibGVWYWx1ZS5zZWxlY3RlZFZhbHVlc0tleV0sIGZpbHRlck9iaik7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRWYWx1ZSwgXCI+Pj4+PnNlbGVjdGVkVmFsdWVcIilcclxuICAgICAgICAgICAgICBpZiAoc2VsZWN0ZWRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgaXRlbS5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgdGVtcE9iaiA9IHsgLi4uaXRlbSwgLi4udGVtcE9iaiB9O1xyXG4gICAgICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiXCIsIHRlbXBBcnJheSk7XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIGlmICh0ZW1wQXJyYXkgJiYgdGVtcEFycmF5Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhYmxlRGF0YSA9IHRlbXBBcnJheTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFibGVEYXRhID1cclxuICAgICAgICAgICAgICBkYXRhICYmIGRhdGEucmVzcG9uc2UgJiYgZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdXHJcbiAgICAgICAgICAgICAgICA/IChkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0pXHJcbiAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiB0aGlzLmdyb3VwRmllbGQubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29uc3RydWN0R3JvdXBlZERhdGEoZGF0YS5yZXNwb25zZSwgcmVzcG9uc2VOYW1lKVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5yZXNwb25zZSkge1xyXG4gICAgICAgICAgICB0aGlzLmxlbmd0aCA9IGRhdGEucmVzcG9uc2UudG90YWw7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pih0aGlzLnRhYmxlRGF0YSBcIix0aGlzLnRhYmxlRGF0YSk7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9ICh0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSAmJiB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXS5sZW5ndGggPiAwKSA/ICh0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSkgOiBbXTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSAodGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSAmJiB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdLmxlbmd0aCA+IDApID8gKHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0pIDogW107XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRGF0YT0gIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl07XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9ICh0aGlzLnNlbGVjdGVkRGF0YS5sZW5ndGg+PXRoaXMubGltaXQpP3RydWU6ZmFsc2U7ICBcclxuICAgICAgICAgIHRoaXMubmV3VGFibGVEYXRhID0gcHJvY2Vzcyh0aGlzLnRhYmxlRGF0YSwgeyBncm91cDogdGhpcy5ncm91cEZpZWxkIH0pO1xyXG4gICAgICAgICAgdGhpcy5uZXdUYWJsZURhdGEudG90YWwgPSB0aGlzLmxlbmd0aDtcclxuICAgICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy50YWJsZURhdGEpO1xyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlLnBhZ2luYXRvciA9IHRoaXMuc2VsZWN0ZWRQYWdpbmF0b3I7IC8vIG5ld2x5IGFkZGVkIFxyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuICAgICAgICAgIHZhciBmaWx0ZXJlZEtleSA9ICh0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSkgPyB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSA6ICcnO1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHNlbGYuY29sdW1ucywgZnVuY3Rpb24gKHgpIHtcclxuICAgICAgICAgICAgaWYgKGZpbHRlcmVkS2V5ID09IHgudmFsdWUpIHtcclxuICAgICAgICAgICAgICBzZWxmLmlucHV0RGF0YVt4LmtleVRvU2F2ZV0gPSBzZWxmLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3gua2V5VG9TYXZlXSA9IG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgLy8gdGhpcy5sb2FkS2VuZG9HcmlkKCk7XHJcbiAgICAgICAgICAvLyB0aGlzLnNlbGVjdEFsbENoZWNrKHsgY2hlY2tlZDogZmFsc2UgfSk7XHJcbiAgICAgICAgICAvLyB0aGlzLnNlbGVjdEFsbCA9IGZhbHNlO1xyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5zZWxlY3RBbGwsIFwiU2VsZWN0QWxsPj4+Pj4+PlwiKTtcclxuICAgICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgICAgICAvLyB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gW107XHJcbiAgICAgICAgICAvLyBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgLy8gY29uc29sZS5sb2codGhpcy5zZWxlY3RBbGwsIFwiU2VsZWN0QWxsPj4+Pj4+PlwiKTtcclxuICAgICAgICAgIC8vIHRoaXMuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xyXG5cclxuICAgICAgICAgIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbiAgLy8gICBsb2FkS2VuZG9HcmlkKCl7XHJcbiAgLy8gICAgICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG4gIC8vICAgICAgICg8YW55PiQoXCIjZ3JpZFwiKSkua2VuZG9HcmlkKHtcclxuICAvLyAgICAgICAgICAgZGF0YVNvdXJjZToge1xyXG4gIC8vICAgICAgICAgICAgICAgcGFnZVNpemU6IDEwLFxyXG4gIC8vICAgICAgICAgICAgICAgdHJhbnNwb3J0OiB7XHJcbiAgLy8gICAgICAgICAgICAgICAgICAgcmVhZDoge1xyXG4gIC8vICAgICAgICAgICAgICAgICAgICAgICB1cmw6IFwiaHR0cHM6Ly9kZW1vcy50ZWxlcmlrLmNvbS9rZW5kby11aS9zZXJ2aWNlL1Byb2R1Y3RzXCIsXHJcbiAgLy8gICAgICAgICAgICAgICAgICAgICAgIGRhdGFUeXBlOiBcImpzb25wXCJcclxuICAvLyAgICAgICAgICAgICAgICAgICB9XHJcbiAgLy8gICAgICAgICAgICAgICB9LFxyXG4gIC8vICAgICAgICAgICAgICAgc2NoZW1hOiB7XHJcbiAgLy8gICAgICAgICAgICAgICAgICAgbW9kZWw6IHtcclxuICAvLyAgICAgICAgICAgICAgICAgICAgICAgaWQ6IFwiUHJvZHVjdElEXCJcclxuICAvLyAgICAgICAgICAgICAgICAgICB9XHJcbiAgLy8gICAgICAgICAgICAgICB9XHJcbiAgLy8gICAgICAgICAgIH0sXHJcbiAgLy8gICAgICAgICAgIHBhZ2VhYmxlOiB0cnVlLFxyXG4gIC8vICAgICAgICAgICBzY3JvbGxhYmxlOiBmYWxzZSxcclxuICAvLyAgICAgICAgICAgcGVyc2lzdFNlbGVjdGlvbjogdHJ1ZSxcclxuICAvLyAgICAgICAgICAgc29ydGFibGU6IHRydWUsXHJcbiAgLy8gICAgICAgICAgIC8vIGNoYW5nZTogb25DaGFuZ2UsXHJcbiAgLy8gICAgICAgICAgIGNvbHVtbnM6IFt7XHJcbiAgLy8gICAgICAgICAgICAgICAgICAgc2VsZWN0YWJsZTogdHJ1ZSxcclxuICAvLyAgICAgICAgICAgICAgICAgICB3aWR0aDogXCI1MHB4XCJcclxuICAvLyAgICAgICAgICAgICAgIH0sXHJcbiAgLy8gICAgICAgICAgICAgICB7XHJcbiAgLy8gICAgICAgICAgICAgICAgICAgZmllbGQ6IFwiUHJvZHVjdE5hbWVcIixcclxuICAvLyAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJQcm9kdWN0IE5hbWVcIlxyXG4gIC8vICAgICAgICAgICAgICAgfSxcclxuICAvLyAgICAgICAgICAgICAgIHtcclxuICAvLyAgICAgICAgICAgICAgICAgICBmaWVsZDogXCJVbml0UHJpY2VcIixcclxuICAvLyAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJVbml0IFByaWNlXCIsXHJcbiAgLy8gICAgICAgICAgICAgICAgICAgZm9ybWF0OiBcInswOmN9XCJcclxuICAvLyAgICAgICAgICAgICAgIH0sXHJcbiAgLy8gICAgICAgICAgICAgICB7XHJcbiAgLy8gICAgICAgICAgICAgICAgICAgZmllbGQ6IFwiVW5pdHNJblN0b2NrXCIsXHJcbiAgLy8gICAgICAgICAgICAgICAgICAgdGl0bGU6IFwiVW5pdHMgSW4gU3RvY2tcIlxyXG4gIC8vICAgICAgICAgICAgICAgfSxcclxuICAvLyAgICAgICAgICAgICAgIHtcclxuICAvLyAgICAgICAgICAgICAgICAgICBmaWVsZDogXCJEaXNjb250aW51ZWRcIlxyXG4gIC8vICAgICAgICAgICAgICAgfVxyXG4gIC8vICAgICAgICAgICBdXHJcbiAgLy8gICAgICAgfSk7XHJcblxyXG4gIC8vICAgICAgIHZhciBncmlkID0gJChcIiNncmlkXCIpLmRhdGEoXCJrZW5kb0dyaWRcIik7XHJcblxyXG4gIC8vICAgICAgIC8vIGdyaWQudGhlYWQub24oXCJjbGlja1wiLCBcIi5rLWNoZWNrYm94XCIsIG9uQ2xpY2spO1xyXG4gIC8vICAgfSk7XHJcblxyXG4gIC8vICAgICAgIC8vIC8vYmluZCBjbGljayBldmVudCB0byB0aGUgY2hlY2tib3hcclxuICAvLyAgICAgICAvLyBncmlkLnRhYmxlLm9uKFwiY2xpY2tcIiwgXCIuay1jaGVja2JveFwiICwgc2VsZWN0Um93KTtcclxuICAvLyAgICAgICAvLyAkKCcjaGVhZGVyLWNoYicpLmNoYW5nZShmdW5jdGlvbihldil7XHJcbiAgLy8gICAgICAgLy8gICB2YXIgY2hlY2tlZCA9IGV2LnRhcmdldC5jaGVja2VkO1xyXG4gIC8vICAgICAgIC8vICAgJCgnLmstY2hlY2tib3gnKS5lYWNoKGZ1bmN0aW9uKGlkeCwgaXRlbSl7XHJcbiAgLy8gICAgICAgLy8gICAgIGlmKGNoZWNrZWQpe1xyXG4gIC8vICAgICAgIC8vICAgICAgIGlmKCEoJChpdGVtKS5jbG9zZXN0KCd0cicpLmlzKCcuay1zdGF0ZS1zZWxlY3RlZCcpKSl7XHJcbiAgLy8gICAgICAgLy8gICAgICAgICAkKGl0ZW0pLmNsaWNrKCk7XHJcbiAgLy8gICAgICAgLy8gICAgICAgfVxyXG4gIC8vICAgICAgIC8vICAgICB9IGVsc2Uge1xyXG4gIC8vICAgICAgIC8vICAgICAgIGlmKCQoaXRlbSkuY2xvc2VzdCgndHInKS5pcygnLmstc3RhdGUtc2VsZWN0ZWQnKSl7XHJcbiAgLy8gICAgICAgLy8gICAgICAgICAkKGl0ZW0pLmNsaWNrKCk7XHJcbiAgLy8gICAgICAgLy8gICAgICAgfVxyXG4gIC8vICAgICAgIC8vICAgICB9XHJcbiAgLy8gICAgICAgLy8gICB9KTtcclxuXHJcbiAgLy8gICAgICAgLy8gfSk7XHJcblxyXG4gIC8vICAgICAgIC8vICQoXCIjc2hvd1NlbGVjdGlvblwiKS5iaW5kKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG4gIC8vICAgICAgIC8vICAgdmFyIGNoZWNrZWQgPSBbXTtcclxuICAvLyAgICAgICAvLyAgIGZvcih2YXIgaSBpbiBjaGVja2VkSWRzKXtcclxuICAvLyAgICAgICAvLyAgICAgaWYoY2hlY2tlZElkc1tpXSl7XHJcbiAgLy8gICAgICAgLy8gICAgICAgY2hlY2tlZC5wdXNoKGkpO1xyXG4gIC8vICAgICAgIC8vICAgICB9XHJcbiAgLy8gICAgICAgLy8gICB9XHJcblxyXG4gIC8vICAgICAgICAgLy8gYWxlcnQoY2hlY2tlZCk7XHJcbiAgLy8gICAgICAgLy8gfSk7XHJcbiAgLy8gICAgIC8vIH0pO1xyXG4gIC8vIH1cclxuICBnZXRMb2FkRGF0YShjdXJyZW50VmFsdWUsIHJlc3BvbnNlVmFsdWUsIHRvdGFsTGVuZ3RoLCByZXNwb25zZUtleSkge1xyXG4gICAgY29uc29sZS5sb2coY3VycmVudFZhbHVlLCBcIi4uLi5jdXJyZW50VmFsdWVcIik7XHJcbiAgICBsZXQgdGVtcEFycmF5ID0gW107XHJcbiAgICAvLyBpZiAocmVzcG9uc2VWYWx1ZSAmJiByZXNwb25zZVZhbHVlW3Jlc3BvbnNlS2V5XSkge1xyXG4gICAgcmVzcG9uc2VWYWx1ZSA9IHJlc3BvbnNlS2V5ID8gcmVzcG9uc2VWYWx1ZVtyZXNwb25zZUtleV0gOiByZXNwb25zZVZhbHVlO1xyXG4gICAgaWYgKHJlc3BvbnNlVmFsdWUgJiYgcmVzcG9uc2VWYWx1ZS5sZW5ndGgpIHtcclxuICAgICAgXy5mb3JFYWNoKHJlc3BvbnNlVmFsdWUsIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXRlbSAhPT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgIHRlbXBPYmpbY3VycmVudFZhbHVlLmtleVRvU2V0XSA9IGl0ZW07XHJcbiAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50VmFsdWUuZGF0YVZpZXdGb3JtYXQsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICBpZiAodmlld0l0ZW0uc3Via2V5KSB7XHJcbiAgICAgICAgICAgICAgaWYgKHZpZXdJdGVtLmFzc2lnbkZpcnN0SW5kZXh2YWwpIHtcclxuICAgICAgICAgICAgICAgIGxldCByZXN1bHRWYWx1ZSA9ICghXy5pc0VtcHR5KGl0ZW1bdmlld0l0ZW0udmFsdWVdKSAmJiBpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA/IChpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgPyByZXN1bHRWYWx1ZVxyXG4gICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICA/IGl0ZW1bdmlld0l0ZW0udmFsdWVdW3ZpZXdJdGVtLnN1YmtleV1cclxuICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGVtcE9iaiA9IHsgLi4uaXRlbSwgLi4udGVtcE9iaiB9O1xyXG4gICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHRoaXMuY29sdW1ucyA9IGN1cnJlbnRWYWx1ZS50YWJsZUhlYWRlcjtcclxuICAgIHJlc3BvbnNlVmFsdWUgPSB0ZW1wQXJyYXkubGVuZ3RoID8gdGVtcEFycmF5IDogcmVzcG9uc2VWYWx1ZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIi4uLklOUFVUREFUQVwiKTtcclxuICAgIGlmIChjdXJyZW50VmFsdWUgJiYgY3VycmVudFZhbHVlLm1hcERhdGFGdW5jdGlvbikge1xyXG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICAgIF8uZm9yRWFjaChjdXJyZW50VmFsdWUubWFwRGF0YUZ1bmN0aW9uLCBmdW5jdGlvbiAobWFwSXRlbSkge1xyXG4gICAgICAgIF8uZm9yRWFjaChtYXBJdGVtLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0pXHJcbiAgICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgIC5nZXRBbGxSZXBvbnNlKHNlbGYucXVlcnlQYXJhbXMsIG1hcEl0ZW0uYXBpVXJsKVxyXG4gICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cobWFwSXRlbSwgXCI+Pj4+Pj4+IE1BUCBJVEVNXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIi4uLi4uREFUQUFBQVwiKTtcclxuICAgICAgICAgICAgbGV0IHRlbXBPYmogPSBfLmtleUJ5KGRhdGEucmVzcG9uc2VbbWFwSXRlbS5yZXNwb25zZV0sIFwiX2lkXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0ZW1wT2JqLCBcIi4uLi50ZW1wT2JqXCIpO1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVzcG9uc2VWYWx1ZSwgZnVuY3Rpb24gKHJlc3BvbnNlSXRlbSkge1xyXG4gICAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIGRhdGEucmVzcG9uc2UgJiZcclxuICAgICAgICAgICAgICAgIGRhdGEucmVzcG9uc2Uua2V5Rm9yQ2hlY2sgPT0gbWFwSXRlbS5rZXlGb3JDaGVjayAmJlxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2VJdGVtW21hcEl0ZW0ua2V5Rm9yQ2hlY2tdXHJcbiAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZUl0ZW1bbWFwSXRlbS5zaG93S2V5XSA9XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmogJiYgcmVzcG9uc2VJdGVtW21hcEl0ZW0ua2V5Rm9yQ2hlY2tdXHJcbiAgICAgICAgICAgICAgICAgICAgPyB0ZW1wT2JqW3Jlc3BvbnNlSXRlbVttYXBJdGVtLmtleUZvckNoZWNrXV0ub2JqZWN0VHlwZXNcclxuICAgICAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2VJdGVtW21hcEl0ZW0ubW9kZWxLZXldID0gcmVzcG9uc2VJdGVtW21hcEl0ZW0uc2hvd0tleV07XHJcbiAgICAgICAgICAgICAgfSBlbHNlXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUl0ZW0sIFwiLi4uLi5yZXNwb25zZUl0ZW1cIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZVZhbHVlLCBcIj4+Pj4+cmVzcG9uc2VWYWx1ZVwiKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZSk7XHJcbiAgICAgIHRoaXMubGVuZ3RoID0gdG90YWxMZW5ndGggPyB0b3RhbExlbmd0aCA6IHJlc3BvbnNlVmFsdWUubGVuZ3RoO1xyXG4gICAgfVxyXG4gICAgdGhpcy50YWJsZURhdGEgPSB0ZW1wQXJyYXk7XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlVmFsdWUpO1xyXG4gICAgdGhpcy5sZW5ndGggPSB0b3RhbExlbmd0aCA/IHRvdGFsTGVuZ3RoIDogcmVzcG9uc2VWYWx1ZS5sZW5ndGg7XHJcbiAgICB0aGlzLm5ld1RhYmxlRGF0YSA9IHByb2Nlc3ModGhpcy50YWJsZURhdGEsIHsgZ3JvdXA6IHRoaXMuZ3JvdXBGaWVsZCB9KTtcclxuICAgIHRoaXMubmV3VGFibGVEYXRhLnRvdGFsID0gdGhpcy5sZW5ndGg7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNvbHVtbnMsIFwiPj4+Pj5DT0xVTU5TU1wiKTtcclxuICAgIC8vIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IF8ubWFwKHRoaXMuY29sdW1ucywgXCJ2YWx1ZVwiKTtcclxuICAgIGxldCBzZWxlY3RlZFRhYmxlSGVhZGVycyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwic2VsZWN0ZWRUYWJsZUhlYWRlcnNcIik7ICAvLyBhZnRlciBnbG9iYWwgc2V0dGluZyBjaGFuZ2VcclxuICAgIHRoaXMuY29sdW1ucyA9ICFfLmlzRW1wdHkoc2VsZWN0ZWRUYWJsZUhlYWRlcnMpID8gSlNPTi5wYXJzZShzZWxlY3RlZFRhYmxlSGVhZGVycykgOiB0aGlzLmNvbHVtbnM7XHJcbiAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmNvbHVtbnNcclxuICAgICAgLmZpbHRlcihmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbC5pc0FjdGl2ZTtcclxuICAgICAgfSk7XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5zZWxlY3RlZFBhZ2luYXRvcjtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRQYWdpbmF0b3IsIFwiLi4uLi4uLi4uLi4uLi4uLi4uLi4gc2VsZWN0ZWRQYWdpbmF0b3JcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmRhdGFTb3VyY2UsIFwiLi4uLi4uLi4uREFUQVNPVVJDRUVFRVwiKTtcclxuICAgIC8vIHRoaXMuZGF0YVNvdXJjZSA9IHJlc3BvbnNlS2V5XHJcbiAgICAvLyAgID8gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShyZXNwb25zZVZhbHVlW3Jlc3BvbnNlS2V5XSlcclxuICAgIC8vICAgOiBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlVmFsdWUpO1xyXG4gIH1cclxuICB1cGRhdGVDaGVja0NsaWNrKHNlbGVjdGVkLCBldmVudCkge1xyXG4gICAgLy8gc2VsZWN0ZWQudHlwZSA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZVR5cGVcclxuICAgIC8vICAgPyB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVUeXBlXHJcbiAgICAvLyAgIDogXCJcIjtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlVHlwZSkge1xyXG4gICAgICBzZWxlY3RlZC50eXBlID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlVHlwZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBzZWxlY3RlZC50eXBlSWQgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudHlwZUlkXHJcbiAgICAvLyAgID8gdGhpcy5jdXJyZW50VGFibGVEYXRhLnR5cGVJZFxyXG4gICAgLy8gICA6IFwiXCI7XHJcbiAgICBsZXQgc2VjdXJpdHlUeXBlTGVuZ3RoID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5VHlwZUxlbmd0aCA/IE51bWJlcih0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlUeXBlTGVuZ3RoKSA6IDM7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiBzZWN1cml0eVR5cGVMZW5ndGggXCIsIHNlY3VyaXR5VHlwZUxlbmd0aCk7XHJcbiAgICAvLyBpZiAoc2VsZWN0ZWQgJiYgc2VsZWN0ZWQudHlwZUlkKSB7XHJcbiAgICAvLyAgIGlmIChzZWxlY3RlZC50eXBlSWQgPT0gXCIyXCIpIHtcclxuICAgIC8vICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0gPSBbNF07XHJcbiAgICAvLyAgICAgc2VsZWN0ZWRbXCJsZXZlbFwiXSA9IFs0XTtcclxuICAgIC8vICAgfSBlbHNlIHtcclxuICAgIC8vICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0gPSBbMSwgMiwgM107XHJcbiAgICAvLyAgICAgc2VsZWN0ZWRbXCJsZXZlbFwiXSA9IFsxLCAyLCAzXTtcclxuICAgIC8vICAgIH1cclxuICAgIGxldCB0ZW1wQXJyYXkgPSBbXTtcclxuICAgIGxldCBwcmVkZWZpbmVkQXJyYXkgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlBcnJheVZhbHVlID8gSlNPTi5wYXJzZShcIltcIiArIHRoaXMuY3VycmVudFRhYmxlRGF0YS5zZWN1cml0eUFycmF5VmFsdWUgKyBcIl1cIikgOiB0ZW1wQXJyYXk7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiBwcmVkZWZpbmVkQXJyYXkgXCIsIHByZWRlZmluZWRBcnJheSk7XHJcbiAgICBpZiAoc2VjdXJpdHlUeXBlTGVuZ3RoICYmIHNlY3VyaXR5VHlwZUxlbmd0aCA+IDApIHtcclxuICAgICAgaWYgKHByZWRlZmluZWRBcnJheSAmJiBwcmVkZWZpbmVkQXJyYXkubGVuZ3RoKSB7XHJcbiAgICAgICAgdGVtcEFycmF5ID0gcHJlZGVmaW5lZEFycmF5O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGZvciAobGV0IGkgPSAxOyBpIDw9IHNlY3VyaXR5VHlwZUxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICB0ZW1wQXJyYXkucHVzaChpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgc2VsZWN0ZWRbXCJzZWN1cml0eV90eXBlXCJdID0gdGVtcEFycmF5O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHNlY3VyaXR5VHlwZUxlbmd0aCA9PSAwKSB7XHJcbiAgICAgICAgc2VsZWN0ZWRbXCJzZWN1cml0eV90eXBlXCJdID0gdGVtcEFycmF5O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9IFsxLCAyLCAzXTtcclxuXHJcblxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAvL31cclxuICAgIGNvbnNvbGUubG9nKFwiPj4+ICBzZWxlY3RlZCBzZWN1cml0eV90eXBlIFwiLCBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0pO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEuZGF0YUZvcm1hdFRvU2F2ZSkge1xyXG4gICAgICBfLmZvckVhY2godGhpcy5jdXJyZW50VGFibGVEYXRhLmRhdGFGb3JtYXRUb1NhdmUsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgc2VsZWN0ZWRbaXRlbS5uYW1lXSA9IHNlbGVjdGVkW2l0ZW0udmFsdWVdO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBsZXQgc2F2ZWRTZWxlY3RlZERhdGEgPVxyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEgJiYgdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhLmxlbmd0aFxyXG4gICAgICAgID8gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhXHJcbiAgICAgICAgOiBbXTtcclxuICAgIHRoaXMuc2VsZWN0ZWREYXRhID0gXy5tZXJnZShzYXZlZFNlbGVjdGVkRGF0YSwgdGhpcy5zZWxlY3RlZERhdGEpO1xyXG4gICAgaWYgKGV2ZW50LmNoZWNrZWQpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGEucHVzaChzZWxlY3RlZCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBsZXQgaW5kZXggPSB0aGlzLnNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgb2JqID0+IG9iai5vYmplY3RfbmFtZSA9PT0gc2VsZWN0ZWQub2JqZWN0X25hbWVcclxuICAgICAgKTtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gdGhpcy5zZWxlY3RlZERhdGE7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5zYXZlSW5BcnJheSkge1xyXG4gICAgICBsZXQgYXJyYXlPYmogPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2F2ZUluQXJyYXk7XHJcbiAgICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVJZCA9PT0gYXJyYXlPYmoua2V5KSB7XHJcbiAgICAgICAgbGV0IHRlbXBBcnJheSA9XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRGF0YSB8fCB0aGlzLnNlbGVjdGVkRGF0YS5sZW5ndGhcclxuICAgICAgICAgICAgPyBfLm1hcCh0aGlzLnNlbGVjdGVkRGF0YSwgYXJyYXlPYmoudmFsdWVUb01hcClcclxuICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICB0ZW1wQXJyYXkgPSB0ZW1wQXJyYXkuZmlsdGVyKGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gZWxlbWVudCAhPSBudWxsO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW2FycmF5T2JqLmtleV0gPSB0ZW1wQXJyYXk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLnNlbGVjdGVkRGF0YSwgXCJfaWRcIik7XHJcbiAgICBjb25zb2xlLmxvZyhcIlNlbGVjdGVkIElkIERhdGEgTGlzdCA+Pj4+Pj4+XCIsIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0pXHJcblxyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0ZWRLZXlzQ2hhbmdlKGUpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+PiBvblNlbGVjdGVkS2V5c0NoYW5nZSBcIiwgZSk7XHJcbiAgICBsZXQgbGVuID0gdGhpcy5zZWxlY3RlZERhdGEubGVuZ3RoO1xyXG4gICAgdGhpcy5zZWxlY3RDb3VudCA9IHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aDtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IGxlbiBcIixsZW4sIFwidG90YWwgcmVjb3JkcyBcIix0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLnRvdGFsLFwiIHRoaXMubGVuZ3RoIFwiLHRoaXMubGVuZ3RoLFwiIHRoaXMubGltaXQgXCIsdGhpcy5saW1pdCxcIiA+Pj4gc2VsZWN0Q291bnQgXCIsdGhpcy5zZWxlY3RDb3VudCk7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgbGV0IHNlbGVjdGVkSWRzPSBlO1xyXG4gICAgaWYgKGxlbiA9PT0gMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdEFsbEl0ZW0gPSBcInVuY2hlY2tlZFwiO1xyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgfSBlbHNlIGlmIChsZW4gPiAwICYmIGxlbiA8IHRoaXMubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0QWxsSXRlbSA9IFwiaW5kZXRlcm1pbmF0ZVwiO1xyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IHRydWU7XHJcbiAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSAodGhpcy5uZXdUYWJsZURhdGEuZGF0YS5sZW5ndGggPT09IHRoaXMubGltaXQpP3RydWU6ZmFsc2U7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gKGxlbj49dGhpcy5saW1pdCk/dHJ1ZTpmYWxzZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0QWxsSXRlbSA9IFwiY2hlY2tlZFwiO1xyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IHRydWU7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBpZih0aGlzLnNlbGVjdGVkRGF0YS5sZW5ndGg+MClcclxuICAgIHtcclxuICAgICAgLy8gY3VycmVudCBzZWxlY3Rpb25cclxuICAgICAgbGV0IHRlbXBEYXRhID0gW107XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHNlbGVjdGVkSWRzLCBmdW5jdGlvbiAodGFibGVJdGVtKSB7XHJcbiAgICAgICAgbGV0IGluZGV4cyA9IHNlbGYubmV3VGFibGVEYXRhLmRhdGEuZmlsdGVyKG9iaiA9PiBvYmpbXCJfaWRcIl0gPT0gdGFibGVJdGVtKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4gbmRleHMubGVuZ3RoIFwiLGluZGV4cy5sZW5ndGgpO1xyXG4gICAgICAgIGlmIChpbmRleHMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgdGVtcERhdGEucHVzaChpbmRleHNbMF0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgIHRoaXMuc2VsZWN0Q291bnQ9dGVtcERhdGEubGVuZ3RoO1xyXG4gICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl09Xy5jb25jYXQodGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0sdGVtcERhdGEpO1xyXG4gICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPV8udW5pcUJ5KHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdLFwiX2lkXCIpO1xyXG4gICAgICAgc2VsZWN0ZWRJZHM9Xy5jb25jYXQodGhpcy5zZWxlY3RlZERhdGEsc2VsZWN0ZWRJZHMpOyAvLyB0b3RhbCBzZWxlY3RlZCBpZHNcclxuICAgIH1cclxuICAgIHRoaXMuc2VsZWN0ZWREYXRhPV8udW5pcShzZWxlY3RlZElkcyk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdPXRoaXMuc2VsZWN0ZWREYXRhO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0QWxsQ2hhbmdlKGNoZWNrZWRTdGF0ZTogU2VsZWN0QWxsQ2hlY2tib3hTdGF0ZSkge1xyXG4gICAgY29uc29sZS5sb2coXCJzZWxlY3RBbGwgLT4gXCIsIGNoZWNrZWRTdGF0ZSk7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgaWYgKGNoZWNrZWRTdGF0ZSA9PT0gJ2NoZWNrZWQnKSB7XHJcbiAgICAgIHRoaXMubmV3VGFibGVEYXRhLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgICAgb2JqLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAodGhpcy5uZXdUYWJsZURhdGEuZGF0YSwgXCJfaWRcIik7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gdHJ1ZTtcclxuICAgICAgdGhpcy5zZWxlY3RBbGxJdGVtID0gJ2NoZWNrZWQnO1xyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IHRydWU7XHJcbiAgfSBlbHNlIHtcclxuICAgIHRoaXMuc2VsZWN0ZWREYXRhPVtdO1xyXG4gICAgdGhpcy5uZXdUYWJsZURhdGEuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgb2JqLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlbGVjdEFsbEl0ZW0gPSAndW5jaGVja2VkJztcclxuICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gZmFsc2U7XHJcbiAgfVxyXG4gIGxldCBzZWxlY3RlZElkcz0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXTtcclxuICBpZih0aGlzLnNlbGVjdGVkRGF0YS5sZW5ndGg+MClcclxuICB7XHJcbiAgICBzZWxlY3RlZElkcz1fLmNvbmNhdCh0aGlzLnNlbGVjdGVkRGF0YSxzZWxlY3RlZElkcyk7XHJcbiAgfVxyXG4gIHRoaXMuc2VsZWN0ZWREYXRhPV8udW5pcShzZWxlY3RlZElkcyk7XHJcbiAgdGhpcy5zZWxlY3RDb3VudCA9IHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aDtcclxuICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gIH1cclxuICBzZWxlY3RBbGxDaGVjayhldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQpO1xyXG4gICAgaWYgKGV2ZW50LmNoZWNrZWQgPT09IHRydWUpIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgICAgb2JqLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSB0aGlzLmRhdGFTb3VyY2UuZGF0YTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuZGF0YVNvdXJjZS5kYXRhLCBcIl9pZFwiKTtcclxuICAgICAgY29uc29sZS5sb2coXCJDaGVjayBFdmVudCBBbGwgSWQgTGlzdCA+Pj4+PlwiLCB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdKTtcclxuICAgICAgdGhpcy5lbmFibGVEZWxldGUgPSB0cnVlO1xyXG4gICAgICB0aGlzLnNlbGVjdENvdW50ID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXS5sZW5ndGg7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWREYXRhLnB1c2godGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSk7XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgICAgb2JqLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGE9W11cclxuICAgICAgdGhpcy5lbmFibGVEZWxldGUgPSBmYWxzZTtcclxuICAgIH1cclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB0aGlzLmNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgfVxyXG4gIGFkZEZpZWxkQ2xpY2soaXRlbSwgb2JqZWN0TGlzdCkge1xyXG4gICAgY29uc29sZS5sb2coaXRlbSwgXCIuLi4uLi4uLi4uSVRFTVwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIi4uLi4uLi5JTlBVVCBEQXRhXCIpXHJcbiAgICBjb25zb2xlLmxvZyhvYmplY3RMaXN0LCBcIi4uLi4uLk9iamVjdExpc3RcIik7XHJcbiAgfVxyXG5cclxuICBzZWxlY3RBbGxEYXRhKCkge1xyXG4gICAgY29uc29sZS5sb2coXCJTZWxlY3QgYWxsIERhdGEgPj4+Pj5cIik7XHJcbiAgICB0aGlzLnNlbGVjdEFsbEl0ZW0gPSAnY2hlY2tlZCc7XHJcbiAgICBsZXQgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnREYXRhLnNlbGVjdEFsbFJlcXVlc3Q7XHJcbiAgICBsZXQgcXVlcnkgPSB7fTtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgcXVlcnlbaXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9XHJcbiAgICAgICAgZGF0YS5yZXNwb25zZVtyZXF1ZXN0RGV0YWlscy5yZXNwb25zZU5hbWVdO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhPXRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl07XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJTZWxlY3RlZCBJZCBsaXN0IEBAQEBAQEBAQFwiLCB0aGlzLnNlbGVjdGVkRGF0YSk7XHJcbiAgICAgICAgXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjbGVhclNlbGVjdEFsbERhdGEoKSB7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXT1bXTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdPVtdO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gZmFsc2U7XHJcbiAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgfVxyXG5cclxuICBhcHBseUZpbHRlcihldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+IGV2ZW50IFwiLCBldmVudCk7XHJcbiAgICBpZiAoZXZlbnQgJiYgZXZlbnQuZmlsdGVycykge1xyXG4gICAgICBsZXQgZmlsdGVyVmFsdWUgPSBldmVudC5maWx0ZXJzID8gZXZlbnQuZmlsdGVyc1swXSA6IHt9O1xyXG4gICAgICB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlID0ge1xyXG4gICAgICAgIHNlYXJjaEtleTogZmlsdGVyVmFsdWUuZmllbGQsXHJcbiAgICAgICAgc2VhcmNoVmFsdWU6IGZpbHRlclZhbHVlLnZhbHVlXHJcbiAgICAgIH07XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW2ZpbHRlclZhbHVlLmZpZWxkXSA9IGZpbHRlclZhbHVlLnZhbHVlO1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmlucHV0RGF0YSxcIj4+Pj4+IElOUFVUIERBVEFcIik7XHJcbiAgICAgIGxldCB0YWJsZUFycmF5ID1cclxuICAgICAgICB0aGlzLm9uTG9hZCAmJiB0aGlzLm9uTG9hZC50YWJsZURhdGFcclxuICAgICAgICAgID8gdGhpcy5vbkxvYWQudGFibGVEYXRhXHJcbiAgICAgICAgICA6IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXS50YWJsZURhdGE7XHJcbiAgICAgIGxldCBpbmRleCA9IF8uZmluZEluZGV4KHRhYmxlQXJyYXksIHsgdGFibGVJZDogdGhpcy50YWJsZUlkIH0pO1xyXG4gICAgICBsZXQgc2VhcmNoUmVxdWVzdCA9IHRhYmxlQXJyYXlbaW5kZXhdLm9uVGFibGVTZWFyY2g7XHJcbiAgICAgIGxldCByZXF1ZXN0ID0gc2VhcmNoUmVxdWVzdC5yZXF1ZXN0RGF0YTtcclxuICAgICAgbGV0IGlzVWlzZXJhY2ggPSAoc2VhcmNoUmVxdWVzdC51aVNlYXJjaCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIGlmIChpc1Vpc2VyYWNoKSB7XHJcbiAgICAgICAgdGhpcy5kYXRhU291cmNlLmZpbHRlciA9IGV2ZW50O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlRGF0YSA9IHRhYmxlQXJyYXlbaW5kZXhdO1xyXG4gICAgICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgICAgIG9mZnNldDogMCxcclxuICAgICAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgICAgIGRhdGFzb3VyY2U6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2VcclxuICAgICAgICB9O1xyXG4gICAgICAgIGlmIChzZWFyY2hSZXF1ZXN0ICYmIHNlYXJjaFJlcXVlc3QuaXNTaW5nbGVyZXF1ZXN0KSB7XHJcbiAgICAgICAgICBxdWVyeVtzZWFyY2hSZXF1ZXN0LnNpbmdsZVJlcXVlc3RLZXldID0gZXZlbnQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0LCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICBsZXQgdGVtcERhdGEgPSBpdGVtLnN1YktleVxyXG4gICAgICAgICAgICAgID8gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgaWYgKGl0ZW0uZGlyZWN0QXNzaWduIHx8IGl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkoaXRlbS52YWx1ZSlcclxuICAgICAgICAgICAgICAgIDogaXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0ZW1wRGF0YSkge1xyXG4gICAgICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbaXRlbS5uYW1lXSA9IGl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHRlbXBEYXRhKVxyXG4gICAgICAgICAgICAgICAgOiB0ZW1wRGF0YTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHRoaXMucXVlcnlQYXJhbXMgPSBxdWVyeTtcclxuICAgICAgICB0aGlzLmdldERhdGEodGhpcy5jdXJyZW50VGFibGVEYXRhLCBcInRhYmxlU2VhcmNoXCIpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG5cclxuICB9XHJcbiAgZmlsdGVyUmVzZXQoZmllbGQpIHtcclxuICAgIGRlbGV0ZSB0aGlzLnF1ZXJ5UGFyYW1zW2ZpZWxkLnJlcXVlc3RLZXldO1xyXG4gICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gIH1cclxuICBvbk11bHRpU2VsZWN0KGV2ZW50LCByb3dWYWwpIHtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBsZXQgc2VsZWN0ZWREYXRhID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhO1xyXG4gICAgbGV0IGluZGV4ID0gXy5maW5kSW5kZXgoc2VsZWN0ZWREYXRhLCB7IG9ial9uYW1lOiByb3dWYWwub2JqX25hbWUgfSk7XHJcbiAgICBpZiAoaW5kZXggPiAtMSAmJiBzZWxlY3RlZERhdGEgJiYgc2VsZWN0ZWREYXRhLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFbaW5kZXhdLnNlY3VyaXR5X3R5cGUgPSBldmVudC52YWx1ZTtcclxuICAgICAgdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhW2luZGV4XS5sZXZlbCA9IGV2ZW50LnZhbHVlO1xyXG4gICAgfVxyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICB9XHJcblxyXG4gIGNoYW5nZVBhZ2UoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcIj4+PiBFVkVOVFwiKVxyXG4gICAgLy8gaWYgKGV2ZW50LnBhZ2VTaXplICE9IHRoaXMubGltaXQpIHtcclxuICAgIC8vICAgdGhpcy5xdWVyeVBhcmFtc1tcImxpbWl0XCJdID0gZXZlbnQucGFnZVNpemU7XHJcbiAgICAvLyAgIHRoaXMucXVlcnlQYXJhbXNbXCJvZmZzZXRcIl0gPSAwO1xyXG4gICAgLy8gfSBlbHNlIHtcclxuICAgIHRoaXMucXVlcnlQYXJhbXNbXCJvZmZzZXRcIl0gPSBldmVudC5za2lwO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtc1tcImxpbWl0XCJdID0gdGhpcy5saW1pdDtcclxuICAgIHRoaXMub2Zmc2V0ID0gZXZlbnQuc2tpcDtcclxuICAgIC8vIH1cclxuICAgIHRoaXMub25Mb2FkRGF0YShcIm5leHRcIik7XHJcbiAgfVxyXG4gIHZpZXdEYXRhKGVsZW1lbnQsIGNvbCkge1xyXG4gICAgY29uc29sZS5sb2coY29sLCBcInZpZXdEYXRhID4+Pj4+Pj4+PiBpc1JlZGlyZWN0IFwiLCBjb2wuaXNSZWRpcmVjdCwgdGhpcy5lbnZpcm9ubWVudCk7XHJcbiAgICBpZiAoY29sLmlzUmVkaXJlY3QpIHtcclxuICAgICAgbGV0IHRlc3RTdHIgPSBcIi9jb250cm9sLW1hbmFnZW1lbnQvYWNjZXNzLWNvbnRyb2xcIlxyXG4gICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGAke3RoaXMucmVkaXJlY3RVcml9YCArIHRlc3RTdHI7XHJcblxyXG4gICAgICBjb25zb2xlLmxvZyhcIlJlZGlyZWN0ZWQgKioqKioqKioqKioqKioqKioqKipcIilcclxuICAgICAgLy8gdGhpcy5yb3V0ZS5uYXZpZ2F0ZUJ5VXJsKFwiL2NvbnRyb2wtbWFuYWdlbWVudC9hY2Nlc3MtY29udHJvbFwiKTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmFjdGlvbkNsaWNrKGVsZW1lbnQsIFwidmlld1wiKTtcclxuICAgIH1cclxuICB9XHJcbiAgYWRkQnV0dG9uQ2xpY2soZWxlbWVudCwgYWN0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImFkZEJ1dHRvbkNsaWNrIFwiKTtcclxuICAgIGNvbnNvbGUubG9nKFwiIGVsZW1lbnQgXCIsIGVsZW1lbnQpO1xyXG4gICAgY29uc29sZS5sb2coXCIgYWN0aW9uIFwiLCBhY3Rpb24pO1xyXG4gIH1cclxuICBhY3Rpb25DbGljayhlbGVtZW50LCBhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKGVsZW1lbnQsIFwiPj4+PmVsZW1lbnRcIilcclxuICAgIGlmIChhY3Rpb24gPT0gXCJlZGl0XCIgfHwgYWN0aW9uID09IFwidmlld1wiIHx8IGFjdGlvbiA9PSBcInZpc2liaWxpdHlcIiB8fCBhY3Rpb24gPT0gXCJpbmZvXCIpIHtcclxuICAgICAgYWN0aW9uID0gKGFjdGlvbiA9PSAndmlzaWJpbGl0eScgfHwgYWN0aW9uID09IFwiaW5mb1wiKSA/ICd2aWV3JyA6IGFjdGlvbjtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGVsZW1lbnQsIGFjdGlvbik7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcImRlbGV0ZVwiKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiPj4+Pj4+PnRoaXNcIik7XHJcbiAgICAgIGlmICh0aGlzLm9uTG9hZCAmJiB0aGlzLm9uTG9hZC5pbnB1dEtleSkge1xyXG4gICAgICAgIGxldCBpbnB1dEtleSA9IHRoaXMub25Mb2FkLmlucHV0S2V5O1xyXG4gICAgICAgIGlmICh0aGlzLmlucHV0RGF0YSAmJiB0aGlzLmlucHV0RGF0YVtpbnB1dEtleV0pIHtcclxuICAgICAgICAgIGxldCBzZWxlY3RlZERhdGEgPSB0aGlzLmlucHV0RGF0YVtpbnB1dEtleV07XHJcbiAgICAgICAgICBsZXQgaW5kZXggPSBzZWxlY3RlZERhdGEuZmluZEluZGV4KFxyXG4gICAgICAgICAgICBvYmogPT4gb2JqID09IGVsZW1lbnRcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZWxlY3RlZERhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW2lucHV0S2V5XSA9IHNlbGVjdGVkRGF0YTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcChzZWxlY3RlZERhdGEsIFwiX2lkXCIpO1xyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShzZWxlY3RlZERhdGEpO1xyXG4gICAgICAgICAgdGhpcy5sZW5ndGggPSBzZWxlY3RlZERhdGEubGVuZ3RoO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgIC8vIHRoaXMuYWN0aW9uQ2xpY2tFdmVudC5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5wdXREYXRhICYmIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YSkge1xyXG4gICAgICAgICAgbGV0IHNlbGVjdGVkRGF0YSA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YTtcclxuICAgICAgICAgIGxldCBpbmRleCA9IHNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIG9iaiA9PiBvYmoub2JqZWN0X25hbWUgPT0gZWxlbWVudC5vYmplY3RfbmFtZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNlbGVjdGVkRGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBzZWxlY3RlZERhdGE7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAoc2VsZWN0ZWREYXRhLCBcIl9pZFwiKTtcclxuICAgICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2Uoc2VsZWN0ZWREYXRhKTtcclxuICAgICAgICAgIHRoaXMubGVuZ3RoID0gc2VsZWN0ZWREYXRhLmxlbmd0aDtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICAvLyB0aGlzLmFjdGlvbkNsaWNrRXZlbnQuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcInJlbW92ZV9yZWRfZXllXCIpIHtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGVsZW1lbnQsIFwidmlld1wiKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwicmVzdG9yZV9wYWdlXCIpIHtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGVsZW1lbnQsIGFjdGlvbik7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSAnYXNrZGVmYXVsdGJ1dHRvbicpIHtcclxuICAgICAgbGV0IGRzaWQgPSBlbGVtZW50Ll9pZDtcclxuICAgICAgbGV0IGRzbmFtZSA9IGVsZW1lbnQubmFtZTtcclxuICAgICAgY29uc29sZS5sb2coXCIgZHNpZCBcIiwgZHNpZCwgXCIgZHNuYW1lIFwiLCBkc25hbWUpO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YTtcclxuICAgICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICAgIGRlZmF1bHREYXRhc291cmNlIDogdHJ1ZVxyXG4gICAgICB9O1xyXG4gICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLnNldERlZmF1bHQudG9hc3RNZXNzYWdlO1xyXG4gICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLnVwZGF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLnNldERlZmF1bHQuYXBpVXJsLCBkc2lkKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmREYXRhc291cmNlKGRzaWQpO1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgIC8vIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZERhdGFzb3VyY2UoZHNpZCk7XHJcbiAgICAgIC8vIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgLy8gICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgIC8vICAgICBcIkRhdGFzb3VyY2UgRGVmYXVsdCBVcGRhdGVkIFN1Y2Nlc3NmdWxseSFcIlxyXG4gICAgICAvLyAgIClcclxuICAgICAgLy8gKTtcclxuICAgICAgdGhpcy5pbnB1dERhdGEuZGF0YXNvdXJjZUlkID0gZHNpZDtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwic2F2ZV9hbHRcIikge1xyXG4gICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnREYXRhLmRvd25sb2FkUmVxdWVzdDtcclxuICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgIHJlcG9ydElkOiBlbGVtZW50Ll9pZFxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0QWxsUmVwb25zZShxdWVyeSwgcmVxdWVzdERldGFpbHMuYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBhYiA9IG5ldyBBcnJheUJ1ZmZlcihkYXRhLmRhdGEubGVuZ3RoKTtcclxuICAgICAgICAgIGNvbnN0IHZpZXcgPSBuZXcgVWludDhBcnJheShhYik7XHJcbiAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRhdGEuZGF0YS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICB2aWV3W2ldID0gZGF0YS5kYXRhW2ldO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgbGV0IGRvd25sb2FkVHlwZSA9ICdhcHBsaWNhdGlvbi96aXAnO1xyXG4gICAgICAgICAgY29uc3QgZmlsZSA9IG5ldyBCbG9iKFthYl0sIHsgdHlwZTogZG93bmxvYWRUeXBlIH0pO1xyXG4gICAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhmaWxlLCBlbGVtZW50LmZpbGVOYW1lICsgJy56aXAnKTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwic3BlYWtlcl9ub3Rlc1wiIHx8IGFjdGlvbiA9PSBcInNwZWFrZXJfbm90ZXNfb2ZmXCIpIHtcclxuICAgICAgY29uc3QgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnREYXRhLm5vdGlmaWNhdGlvblJlYWQ7XHJcbiAgICAgIGxldCBpZHNMaXN0ID0gW107XHJcbiAgICAgIGlkc0xpc3QucHVzaChlbGVtZW50Ll9pZCk7XHJcbiAgICAgIC8vIGlmIChhY3Rpb24gPT0gXCJub3RkZWxldGVcIikge1xyXG4gICAgICAgIC8vIHF1ZXJ5T2JqW1wiZGVsZXRlXCJdID0gdHJ1ZTtcclxuICAgICAgLy8gIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgLy8gICAgIGlzcmVhZDogdHJ1ZSxcclxuICAgICAgLy8gICAgIGZlYXR1cmU6IFwiZGVsZXRlXCIsXHJcbiAgICAgIC8vICAgICBpZExpc3Q6IGlkc0xpc3RcclxuICAgICAgLy8gICB9XHJcbiAgICAgIC8vIH1cclxuICAgICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICAgIGlzcmVhZDogKGFjdGlvbiA9PSBcInNwZWFrZXJfbm90ZXNcIikgPyB0cnVlIDogZmFsc2UsXHJcbiAgICAgICAgZmVhdHVyZTogXCJpc3JlYWRcIixcclxuICAgICAgICBpZExpc3Q6IGlkc0xpc3RcclxuICAgICAgfVxyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLmFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcIm5vdGRlbGV0ZVwiKSB7XHJcbiAgICAgIGNvbnN0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50RGF0YS5ub3RpZmljYXRpb25SZWFkO1xyXG4gICAgICBsZXQgaWRzTGlzdCA9IFtdO1xyXG4gICAgICBpZHNMaXN0LnB1c2goZWxlbWVudC5faWQpO1xyXG4gICAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgICAgZmVhdHVyZTogXCJkZWxldGVcIixcclxuICAgICAgICBpZExpc3Q6IGlkc0xpc3QsXHJcbiAgICAgICAgaXNyZWFkOiB0cnVlXHJcbiAgICAgIH1cclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZS5jcmVhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgdXBkYXRlU3RhdHVzKGFjdGlvbk5hbWUsIHNlbGVjdGVkSWQpe1xyXG4gICAgbGV0IHF1ZXJ5ID0ge307XHJcbiAgICBpZih0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnN0YXR1c1VwZGF0ZSl7XHJcbiAgICAgIHZhciBjdXJyZW50UmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc3RhdHVzVXBkYXRlO1xyXG4gICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGN1cnJlbnRSZXF1ZXN0RGV0YWlscy50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIHF1ZXJ5W1widXBkYXRlSWRcIl0gPSBzZWxlY3RlZElkLl9pZDtcclxuICAgICAgcXVlcnlbXCJzdGF0dXNcIl0gPSBhY3Rpb25OYW1lO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLnVwZGF0ZVJlcXVlc3QocXVlcnksIGN1cnJlbnRSZXF1ZXN0RGV0YWlscy5hcGlVcmwsIHNlbGVjdGVkSWQuX2lkKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGFjdGlvblJlZGlyZWN0KGVsZW1lbnQsIGNvbHVtbkRhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiYWN0aW9uUmVkaXJlY3QgPj4+Pj4+IFwiLCBlbGVtZW50KTtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YVxyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge307XHJcbiAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLm9uVGFibGVVcGRhdGUudG9hc3RNZXNzYWdlO1xyXG5cclxuICAgIGNvbnNvbGUubG9nKFwicmVxdWVzdERldGFpbHMgPj4+Pj4+IFwiLCByZXF1ZXN0RGV0YWlscyk7XHJcbiAgICBpZiAoY29sdW1uRGF0YS5ydWxlc2V0Q2hlY2spIHtcclxuICAgICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLm9uVGFibGVVcGRhdGUucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgcXVlcnlPYmpbaXRlbS5uYW1lXSA9IGl0ZW0uc3Via2V5XHJcbiAgICAgICAgICA/IGVsZW1lbnRbaXRlbS52YWx1ZV1baXRlbS5zdWJrZXldXHJcbiAgICAgICAgICA6IGVsZW1lbnRbaXRlbS52YWx1ZV07XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcXVlcnlPYmogPSB7XHJcbiAgICAgICAgZGVmYXVsdERhdGFzb3VyY2UgOiB0cnVlXHJcbiAgICAgIH07XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiZGF0YSBzb3VyY2UgPj4+PlwiKTtcclxuICAgIH1cclxuXHJcbiAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAudXBkYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS5hcGlVcmwsIGVsZW1lbnQuX2lkKVxyXG4gICAgLnN1YnNjcmliZShcclxuICAgICAgcmVzID0+IHtcclxuICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgaWYgKCFjb2x1bW5EYXRhLnJ1bGVzZXRDaGVjaykge1xyXG4gICAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kRGF0YXNvdXJjZShlbGVtZW50Ll9pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgfSxcclxuICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICApO1xyXG4gICAgY29uc29sZS5sb2coJ2FjdGlvblJlZGlyZWN0ID4+Pj4+Pj4+Pj4+Pj4+PiBxdWVyeU9iaiAnLCBxdWVyeU9iailcclxuICAgIGNvbnNvbGUubG9nKFwiUXVlcnkgT2JqID4+PlwiLCBxdWVyeU9iaik7XHJcbiAgICBjb25zb2xlLmxvZyhcIlVwZGF0ZSBVUkwgPj4+XCIsIHJlcXVlc3REZXRhaWxzLm9uVGFibGVVcGRhdGUuYXBpVXJsKTtcclxuXHJcblxyXG4gIH1cclxuICBvblRvZ2dsZUNoYW5nZShldmVudCwgZWxlbWVudCkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+Pj4gZXZlbnQgXCIsIGV2ZW50LCBcIiA6ZWxlbWVudCBcIiwgZWxlbWVudCk7XHJcbiAgICBlbGVtZW50LnN0YXR1cyA9IGV2ZW50LmNoZWNrZWQgPyBcIkFDVElWRVwiIDogXCJJTkFDVElWRVwiO1xyXG4gICAgbGV0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhXHJcbiAgICAgID8gdGhpcy5jdXJyZW50VGFibGVEYXRhLm9uVG9nZ2xlQ2hhbmdlXHJcbiAgICAgIDogXCJcIjtcclxuICAgIGlmIChyZXF1ZXN0RGV0YWlscykge1xyXG4gICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBsZXQgcXVlcnlPYmogPSB7fTtcclxuICAgICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIC8vIHF1ZXJ5T2JqW2l0ZW0ubmFtZV0gPSBpdGVtLnN1YmtleVxyXG4gICAgICAgIC8vICAgPyBlbGVtZW50W2l0ZW0udmFsdWVdW2l0ZW0uc3Via2V5XVxyXG4gICAgICAgIC8vICAgOiBlbGVtZW50W2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgaWYoaXRlbS5zdWJrZXkpXHJcbiAgICAgICAgICB7XHJcbiAgICAgICAgICAgIHF1ZXJ5T2JqW2l0ZW0ubmFtZV09ZWxlbWVudFtpdGVtLnZhbHVlXVtpdGVtLnN1YmtleV07XHJcbiAgICAgICAgICB9ZWxzZSBpZihpdGVtLmZyb21UZW5hbnREYXRhKXtcclxuICAgICAgICAgICAgcXVlcnlPYmpbaXRlbS5uYW1lXT1zZWxmLmN1cnJlbnRUZW5hbnRbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICBxdWVyeU9ialtpdGVtLm5hbWVdPWVsZW1lbnRbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBjb25zb2xlLmxvZygnVG9nZ2xlIFVSTCA+Pj4nLCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpO1xyXG5cclxuICAgICAgc2VsZi5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC51cGRhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCByZXF1ZXN0RGV0YWlscy5hcGlVcmwsIGVsZW1lbnQuX2lkKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICBpZihlbGVtZW50LnN0YXR1cz09J0FDVElWRScgJiYgIHRvYXN0TWVzc2FnZURldGFpbHMuZW5hYmxlIClcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lbmFibGVcclxuICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfWVsc2UgaWYoZWxlbWVudC5zdGF0dXM9PSdJTkFDVElWRScgJiYgIHRvYXN0TWVzc2FnZURldGFpbHMuZGlzYWJsZSlcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5kaXNhYmxlXHJcbiAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICBpZihyZXF1ZXN0RGV0YWlscy5pc1BhZ2VSZWZyZXNoKXtcclxuICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHNlbGYub25Mb2FkRGF0YShudWxsKTtcclxuICAgICAgICAgICAgICB9LCAxMDApO1xyXG4gICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5zYXZlc2VsZWN0ZWRUb2dnbGUpIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbdGhpcy5jdXJyZW50VGFibGVEYXRhW1wia2V5VG9zYXZlXCJdXSA9IHRoaXMuZGF0YVNvdXJjZS5kYXRhO1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgfVxyXG4gIH1cclxuY2hlY2tDb25maXJtKGVsZW1lbnQsYWN0aW9uLGNvbHVtbil7XHJcbiAgICB0aGlzLm9wZW5Db25maXJtRGlhbG9nKGVsZW1lbnQsYWN0aW9uLGNvbHVtbik7XHJcbiB9XHJcbm9wZW5Db25maXJtRGlhbG9nKGVsZW1lbnQsYWN0aW9uLGNvbHVtbil7XHJcbiAgY29uc29sZS5sb2coXCIgb3BlbkNvbmZpcm1EaWFsb2cgRGlhbG9nICoqKioqKioqKiBlbGVtZW50OiBcIiwgZWxlbWVudCAsXCIgKioqKiBhY3Rpb24gXCIsYWN0aW9uLFwiPj4+IGNvbHVtbiBcIixjb2x1bW4pO1xyXG4gIGxldCBtb2RlbFdpZHRoID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLm1vZGVsRGF0YS5zaXplO1xyXG4gIGxldCBtb2RlbERhdGEgPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW2FjdGlvbl0ubW9kZWxEYXRhO1xyXG4gIGNvbnNvbGUubG9nKFwiPj4+IG1vZGVsV2lkdGggXCIsbW9kZWxXaWR0aCk7XHJcbiAgdGhpcy5kaWFsb2dSZWYgPSB0aGlzLl9tYXREaWFsb2dcclxuICAub3BlbihDb25maXJtRGlhbG9nQ29tcG9uZW50LCB7XHJcbiAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICB3aWR0aDogbW9kZWxXaWR0aCxcclxuICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgZGF0YToge1xyXG4gICAgICBhY3Rpb246IGFjdGlvbixcclxuICAgICAgc2F2ZWREYXRhOiBlbGVtZW50LFxyXG4gICAgICBtb2RlbERhdGE6IG1vZGVsRGF0YVxyXG4gICAgfVxyXG4gIH0pXHJcbiAgLmFmdGVyQ2xvc2VkKClcclxuICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+PiBDb25maXJtIG1vZGFsIHJlc3BvbnNlIFwiLHJlc3BvbnNlKTtcclxuICAgIGlmKHJlc3BvbnNlICYmIGNvbHVtbi5yZWRpcmVjdEFjdGlvbil7XHJcbiAgICAgIHRoaXMub3BlbkRpYWxvZyhlbGVtZW50LCBjb2x1bW4ucmVkaXJlY3RBY3Rpb24pXHJcbiAgICAgfSBlbHNlIHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gICAgIH1cclxuICB9KTtcclxufVxyXG4gIG9wZW5EaWFsb2coZWxlbWVudCwgYWN0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBPcGVuIERpYWxvZyAqKioqKioqKiogZWxlbWVudDogXCIsIGVsZW1lbnQpO1xyXG4gICAgbGV0IG1vZGVsV2lkdGggPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW2FjdGlvbl0ubW9kZWxEYXRhLnNpemU7XHJcbiAgICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZ1xyXG4gICAgICAub3BlbihNb2RlbExheW91dENvbXBvbmVudCwge1xyXG4gICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICB3aWR0aDogbW9kZWxXaWR0aCxcclxuICAgICAgICBwYW5lbENsYXNzOiBcImNvbnRhY3QtZm9ybS1kaWFsb2dcIixcclxuICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICBhY3Rpb246IGFjdGlvbixcclxuICAgICAgICAgIHNhdmVkRGF0YTogZWxlbWVudFxyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgLmFmdGVyQ2xvc2VkKClcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuICBvblNlbGVjdE9wdGlvbmJhY2t1cChpdGVtKSB7XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgaWYgKGl0ZW0udmFsdWUgPT0gJ25vbmUnKSB7XHJcbiAgICAgIF8uZm9yRWFjaChzZWxmLm5ld1RhYmxlRGF0YS5kYXRhLCBmdW5jdGlvbiAodGFibGVJdGVtKSB7XHJcbiAgICAgICAgdGFibGVJdGVtLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuICAgICAgdGhpcy5lbmFibGVEZWxldGUgPSBmYWxzZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmIChpdGVtLnZhbHVlICE9ICdhbGwnKSB7XHJcbiAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tpdGVtLm5hbWVdID0gaXRlbS52YWx1ZTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlNlbGVjdCBWYWx1ZSA+Pj4+Pj4+Pj5cIiwgaXRlbSk7XHJcbiAgICAgICAgaWYgKGl0ZW0ua2V5VG9TaG93ID09IFwiUmVhZFwiKSB7XHJcbiAgICAgICAgICBzZWxmLmN1cnJlbnREYXRhLmlzUmVhZCA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHNlbGYuY3VycmVudERhdGEuaXNSZWFkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IHRydWU7XHJcbiAgICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gdHJ1ZTtcclxuICAgICAgdGhpcy5nZXREYXRhKHRoaXMuY3VycmVudFRhYmxlRGF0YSwgbnVsbCk7XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiB0aGlzLmlucHV0RGF0YSBcIiwgdGhpcy5pbnB1dERhdGEpO1xyXG4gIH1cclxuICBvblNlbGVjdE9wdGlvbihpdGVtKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIlNlbGVjdCBWYWx1ZSA+Pj4+Pj4+Pj5cIiwgaXRlbSk7XHJcbiAgICBsZXQgZXhpc3RDaGVjayA9IF8uaGFzKHRoaXMucXVlcnlQYXJhbXMsIGl0ZW0ubmFtZSk7XHJcbiAgICBpZiAoaXRlbS5hbHRlcm5hdGl2ZUtleSA9PSAndW5yZWFkJykge1xyXG4gICAgICBkZWxldGUgdGhpcy5xdWVyeVBhcmFtc1tcInVucmVhZFwiXTtcclxuICAgICAgLy8gXy5vbWl0KHRoaXMucXVlcnlQYXJhbXMsIHRoaXMucXVlcnlQYXJhbXNbXCJ1bnJlYWRcIl0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgZGVsZXRlIHRoaXMucXVlcnlQYXJhbXNbXCJpc3JlYWRcIl07XHJcbiAgICAgIC8vIF8ub21pdCh0aGlzLnF1ZXJ5UGFyYW1zLCB0aGlzLnF1ZXJ5UGFyYW1zW1wiaXNyZWFkXCJdKTtcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKFwiUXVlcnkgUGFyYW1zID4+XCIsIHRoaXMucXVlcnlQYXJhbXMpO1xyXG4gICAgaWYgKHRoaXMucXVlcnlQYXJhbXNbXCJvcGVyYXRpb25cIl0gIT0gaXRlbS5uYW1lKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkRGF0YT1bXTtcclxuICAgIHRoaXMubmV3VGFibGVEYXRhLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgIG9iai5jaGVja2VkID0gZmFsc2U7XHJcbiAgICB9KTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gW107XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgdGhpcy5zZWxlY3RBbGxJdGVtID0gJ3VuY2hlY2tlZCc7XHJcbiAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5xdWVyeVBhcmFtc1tcIm9wZXJhdGlvblwiXSA9IGl0ZW0ubmFtZTtcclxuICAgIHRoaXMucXVlcnlQYXJhbXNbaXRlbS5uYW1lXSA9IGl0ZW0udmFsdWU7XHJcbiAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gICAgdGhpcy5nZXREYXRhKHRoaXMuY3VycmVudFRhYmxlRGF0YSwgbnVsbCk7XHJcbiAgfVxyXG4gIERlbGV0ZU9wdGlvbigpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEubm90aWZpY2F0aW9uUmVhZDtcclxuICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuZGF0YVNvdXJjZS5kYXRhLCBcIl9pZFwiKTtcclxuICAgIGNvbnN0IGlucHV0ID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXTtcclxuICAgIGNvbnNvbGUubG9nKFwiSW5wdXQgZGF0YSA+Pj4+XCIsIGlucHV0KTtcclxuXHJcbiAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgIGZlYXR1cmU6IFwiZGVsZXRlXCIsXHJcbiAgICAgIGlkTGlzdDogaW5wdXRcclxuICAgIH1cclxuICAgIGlmIChpbnB1dC5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMuYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gICAgfVxyXG5cclxuICB9XHJcbiAgUmVhZCgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLmRhdGFTb3VyY2UuZGF0YSwgXCJfaWRcIik7XHJcbiAgICBjb25zdCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEubm90aWZpY2F0aW9uUmVhZDtcclxuICAgIGNvbnN0IGlucHV0ID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXTtcclxuICAgIGNvbnNvbGUubG9nKFwiSW5wdXQgZGF0YSA+Pj4+XCIsIGlucHV0KTtcclxuICAgIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgaXNyZWFkOiB0cnVlLFxyXG4gICAgICBmZWF0dXJlOiBcImlzcmVhZFwiLFxyXG4gICAgICBpZExpc3Q6IGlucHV0XHJcbiAgICB9XHJcbiAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgXHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLmFwaVVybClcclxuICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIFVucmVhZCgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl1cclxuICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLmRhdGFTb3VyY2UuZGF0YSwgXCJfaWRcIik7XHJcbiAgICBjb25zdCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEubm90aWZpY2F0aW9uUmVhZDtcclxuICAgIGNvbnN0IGlucHV0ID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXTtcclxuICAgIGNvbnNvbGUubG9nKFwiU2VsZWN0ZWQgSUQgTGlzdCBvZiBJbnB1dCA+Pj5cIiwgaW5wdXQpO1xyXG4gICAgXHJcbiAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgaXNyZWFkOiBmYWxzZSxcclxuICAgICAgZmVhdHVyZTogXCJpc3JlYWRcIixcclxuICAgICAgaWRMaXN0OiBpbnB1dFxyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMuYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcblxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gYmFzZTY0VG9BcnJheUJ1ZmZlcihiYXNlNjQ6IHN0cmluZykge1xyXG4gIGNvbnN0IGJpbmFyeVN0cmluZyA9IHdpbmRvdy5hdG9iKGJhc2U2NCk7IC8vIENvbW1lbnQgdGhpcyBpZiBub3QgdXNpbmcgYmFzZTY0XHJcbiAgY29uc3QgYnl0ZXMgPSBuZXcgVWludDhBcnJheShiaW5hcnlTdHJpbmcubGVuZ3RoKTtcclxuICByZXR1cm4gYnl0ZXMubWFwKChieXRlLCBpKSA9PiBiaW5hcnlTdHJpbmcuY2hhckNvZGVBdChpKSk7XHJcbn1cclxuXHJcbiJdfQ==