import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { SetupAdministrationService } from './../setup-administration.service';
import { SnackBarService } from './../../shared/snackbar.service';
import { ReportManagementService } from '../../avm/report-management.service';
import { Router } from '@angular/router';
var ConfigTrackerSetupComponent = /** @class */ (function () {
    function ConfigTrackerSetupComponent(formBuilder, snackBarService, SetupAdministrationService, _reportManagementService, router) {
        this.formBuilder = formBuilder;
        this.snackBarService = snackBarService;
        this.SetupAdministrationService = SetupAdministrationService;
        this._reportManagementService = _reportManagementService;
        this.router = router;
        this.isActive = false;
        this.submitted = false;
        this.frequentData = [{
                "value": "month",
                "name": "Month"
            }, {
                "value": "day",
                "name": "Day"
            }, {
                "value": "hour",
                "name": "Hour"
            }, {
                "value": "mins",
                "name": "Minutes"
            }];
        this.days = [];
        // this.control = new Notification({});
        this.CreateForm = this.formBuilder.group({
            clientID: ['', Validators.required],
            fusionUrl: ['', Validators.required],
            module: ['', Validators.required],
            userName: ['', Validators.required],
            password: ['', Validators.required],
            object_name: ['', Validators.required],
            scheduleType: ['', Validators.required],
            scheduleTypeValue: ['', Validators.required],
            retainDays: ['', Validators.required],
            retainScheduleType: ['', Validators.required]
        });
    }
    ConfigTrackerSetupComponent.prototype.ngOnInit = function () {
        this.status = this.isActive;
        this.loadAll();
    };
    ConfigTrackerSetupComponent.prototype.loadAll = function () {
        // this.SetupAdministrationService.getNotification().subscribe((data: any) => {
        // 	this.data_source = data.body;
        // 	this.loadData(this.data_source);
        // });
        var _this = this;
        var options = localStorage.getItem("userId");
        this._reportManagementService.getAllConfigTracker(options).subscribe(function (data) {
            _this.data_source = data.body;
            console.log("this.data_source------", _this.data_source);
            if (_this.data_source.response.configDetails.length != 0) {
                _this.loadData(_this.data_source);
            }
            else {
                _this.configDetailsID = 0;
            }
        });
    };
    ConfigTrackerSetupComponent.prototype.showDetail = function (e) {
        this.isActive = e.checked;
        this.loadData(this.data_source);
    };
    ConfigTrackerSetupComponent.prototype.getfrequentData = function ($event) {
        this.scheduleType = $event;
        this.days = [];
        console.log($event, "........even");
        if ($event == "month") {
            for (var i = 0; i < 6; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
        else if ($event == "day") {
            for (var i = 0; i < 31; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
        else if ($event == "hour") {
            for (var i = 0; i < 24; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
        else if ($event == "mins") {
            for (var i = 0; i < 60; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
    };
    ConfigTrackerSetupComponent.prototype.loadData = function (dataValue) {
        this.scheduleType = dataValue.response.configDetails[0].scheduleType;
        this.scheduleTypeValue = (dataValue.response.configDetails[0].scheduleTypeValue).toString();
        this.days = [];
        if (this.scheduleType == "month") {
            for (var i = 0; i < 6; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
        else if (this.scheduleType == "day") {
            for (var i = 0; i < 31; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
        else if (this.scheduleType == "hour") {
            for (var i = 0; i < 24; i++) {
                this.days.push({
                    'name': i + 1,
                    'value': i + 1
                });
            }
        }
        this.CreateForm.patchValue({
            userName: dataValue.response.configDetails[0].userName,
            password: dataValue.response.configDetails[0].password,
            clientID: dataValue.response.configDetails[0].clientID,
            fusionUrl: dataValue.response.configDetails[0].fusionUrl,
            scheduleType: dataValue.response.configDetails[0].scheduleType,
            scheduleTypeValue: (dataValue.response.configDetails[0].scheduleTypeValue).toString(),
            retainDays: dataValue.response.configDetails[0].retainDays,
            retainScheduleType: dataValue.response.configDetails[0].retainScheduleType
        });
        this.configDetailsID = dataValue.response.configDetails[0].userId;
        this.module = dataValue.response.configDetails[0].module;
        this.object_name = dataValue.response.configDetails[0].object_name;
    };
    ConfigTrackerSetupComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        // if (this.CreateForm.invalid) {
        // 	this.snackBarService.warning("Mandatory Fields are Required");
        // 	setTimeout(()=>{   
        // 		this.submitted = false;
        //    }, 3000);
        // }
        // else if(this.nameExist){
        // 	this.snackBarService.warning("Control Name Already Exist");
        // 	setTimeout(()=>{   
        // 		this.submitted = false;
        //    }, 3000);
        // } 
        // else {
        var q_arr = [];
        var obj = {
            "clientID": this.CreateForm.controls['clientID'].value,
            "fusionUrl": this.CreateForm.controls['fusionUrl'].value,
            "userName": this.CreateForm.controls['userName'].value,
            "password": this.CreateForm.controls['password'].value,
            "scheduleType": this.CreateForm.controls['scheduleType'].value,
            "scheduleTypeValue": parseInt(this.CreateForm.controls['scheduleTypeValue'].value),
            "retainDays": this.CreateForm.controls['retainDays'].value,
            "userId": localStorage.getItem("userId"),
            "object_name": this.object_name,
            "module": this.module,
        };
        if (this.configDetailsID == 0) {
            this.SetupAdministrationService.createConfigTracker(obj).subscribe(function (res) {
                if (res.status == 201) {
                    _this.snackBarService.add(res.body.meta.msg);
                    _this.router.navigate(['/report-management/config-tracker']);
                }
                else {
                    _this.snackBarService.add(res.body.meta.msg);
                    _this.router.navigate(['/report-management/config-tracker']);
                }
            }, function (error) {
                _this.errorStatus = error.error.meta.status;
                if (_this.errorStatus == '500' || _this.errorStatus == '400') {
                    _this.errorMsg = error.error.meta.msg;
                    _this.snackBarService.warning(_this.errorMsg);
                    setTimeout(function () {
                        _this.submitted = false;
                    }, 3000);
                }
            });
        }
        else {
            this.SetupAdministrationService.updateConfigDetails(obj, this.configDetailsID).subscribe(function (res) {
                if (res.status == 201) {
                    _this.snackBarService.add(res.body.meta.msg);
                    _this.router.navigate(['/report-management/config-tracker']);
                }
                else {
                    _this.snackBarService.add(res.body.meta.msg);
                    _this.router.navigate(['/report-management/config-tracker']);
                }
            }, function (error) {
                _this.errorStatus = error.error.meta.status;
                if (_this.errorStatus == '500' || _this.errorStatus == '400') {
                    _this.errorMsg = error.error.meta.msg;
                    _this.snackBarService.warning(_this.errorMsg);
                    setTimeout(function () {
                        _this.submitted = false;
                    }, 3000);
                }
            });
        }
        // }
    };
    ConfigTrackerSetupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-config-tracker-setup',
                    template: "<div class=\"page-layout blank p-24\" fusePerfectScrollbar>\r\n\r\n\t<mat-drawer-container class=\"example-container\" autosize fxFlex>\r\n\r\n\t\t<div class=\"header-top ctrl-create header p-24\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"m-0\">\r\n\t\t\t\t\tConfig Tracker Setup\r\n\t\t\t\t</h2>\r\n\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" style=\"font-size :initial\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t<span class=\"m-0\">Track the configurations from Cloud Application</span>\r\n\t\t\t\t </div>\r\n\t\t\t\t<!-- <span>Send outputs to Emails\r\n\t\t\t\t</span> -->\r\n\t\t\t</div>\r\n\t\t\t<!-- <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<mat-slide-toggle fxFlex class=\"mat-accent\" [checked]=\"status\" name=\"status\" (change)=\"showDetail($event)\">\r\n\t\t\t\t\tClick To Show Credential\r\n\t\t\t\t</mat-slide-toggle>\r\n\t\t\t</div> -->\r\n\t\t</div>\r\n\r\n\r\n\t\t<div class=\"content p-12\" fusePerfectScrollbar>\r\n\r\n\t\t\t<form class=\"mat-card mat-elevation-z4 p-24 mr-24\" fxLayout=\"column\" fxLayoutAlign=\"start\" fxFlex=\"1 1 auto\"\r\n\t\t\t\tname=\"form\" [formGroup]=\"CreateForm\" (ngSubmit)=\"onSubmit()\" novalidate>\r\n\t\t\t\t<div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxFlex=\"1 0 auto\">\r\n\r\n\t\t\t\t\t<mat-form-field style=\"width: 45%;\" appearance=\"outline\" class=\"mr-8\">\r\n\t\t\t\t\t\t<mat-label>Client ID</mat-label>\r\n\t\t\t\t\t\t<input matInput formControlName=\"clientID\" required>\r\n\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t<mat-form-field style=\"width: 45%;\" appearance=\"outline\" class=\"mr-8\">\r\n\t\t\t\t\t\t<mat-label>Fusion URL</mat-label>\r\n\t\t\t\t\t\t<input matInput formControlName=\"fusionUrl\" required>\r\n\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxFlex=\"1 0 auto\">\r\n\r\n\t\t\t\t\t<mat-form-field style=\"width: 45%;\"  appearance=\"outline\"  class=\"mr-8\">\r\n\t\t\t\t\t\t<mat-label>Username</mat-label>\r\n\t\t\t\t\t\t<input matInput formControlName=\"userName\" required>\r\n\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t<mat-form-field style=\"width: 45%;\" appearance=\"outline\"  class=\"ml-8\">\r\n\t\t\t\t\t\t<mat-label>Password</mat-label>\r\n\t\t\t\t\t\t<input matInput formControlName=\"password\" required type=\"password\">\r\n\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div fxLayout=\"row\" fxLayoutAlign=\"start center\" fxFlex=\"1 0 auto\">\r\n\t\t\t\t\t\t<mat-label style=\"margin-top: 3%;\">How frequent do you want to compare?&nbsp;&nbsp;</mat-label>\r\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" style=\"width:17%;padding-right: 2%;\">\r\n\t\t\t\t\t\t\t<mat-label>Type</mat-label>\r\n\t\t\t\t\t\t\t<mat-select formControlName=\"scheduleType\"\r\n\t\t\t\t\t\t\t\t(selectionChange)=\"getfrequentData($event.value)\">\r\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let order of frequentData; let i = index\" value=\"{{order.value}}\">\r\n\t\t\t\t\t\t\t\t\t{{order.name}}\r\n\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"32\" style=\"width:18%\">\r\n\t\t\t\t\t\t\t<mat-label>Count</mat-label>\r\n\t\t\t\t\t\t\t<mat-select formControlName=\"scheduleTypeValue\">\r\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let d of days; let i = index\" value=\"{{d.value}}\">\r\n\t\t\t\t\t\t\t\t\t{{d.name}}\r\n\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t<!-- <p *ngIf=\"submitted && CreateForm.get('scheduleType').invalid\">\r\n\t\t\t\t\t\t\t<mat-error class=\"error_margin\">\r\n\t\t\t\t\t\t\t\tDatasource is required!\r\n\t\t\t\t\t\t\t</mat-error>\r\n\t\t\t\t\t\t</p> -->\r\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"32\" style=\"width:20%;padding:0% 2% 0% 1%;\">\r\n\t\t\t\t\t\t\t<mat-label>Retain Data</mat-label>\r\n\t\t\t\t\t\t\t<input matInput formControlName=\"retainDays\" required>\r\n\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"32\"  style=\"width:20%;padding-right: 2%;\">\r\n\t\t\t\t\t\t\t<mat-label>Type</mat-label>\r\n\t\t\t\t\t\t\t<mat-select formControlName=\"retainScheduleType\"\r\n\t\t\t\t\t\t\t\t(selectionChange)=\"getfrequentData($event.value)\">\r\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let order of frequentData; let i = index\" value=\"{{order.value}}\">\r\n\t\t\t\t\t\t\t\t\t{{order.name}}\r\n\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t<p *ngIf=\"submitted && CreateForm.get('retainDays').invalid\">\r\n\t\t\t\t\t\t\t<mat-error class=\"error_margin\">\r\n\t\t\t\t\t\t\t\tRetain Data is required!\r\n\t\t\t\t\t\t\t</mat-error>\r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t<h2 class=\"m-0\"></h2>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t<button mat-raised-button type=\"submit\" color=\"accent\"\r\n\t\t\t\t\t\t\tmat-flat-button style=\"float:right\">\r\n\t\t\t\t\t\t\tSave\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\"\r\n\t\t\t\t\t\t\t[disabled]=\"CreateForm.pristine\">\r\n\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\" ng-reflect-centered=\"false\"\r\n\t\t\t\t\t\t\t\tng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t</form>\r\n\t\t</div>\r\n\t</mat-drawer-container>\r\n</div>"
                }] }
    ];
    /** @nocollapse */
    ConfigTrackerSetupComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: SnackBarService },
        { type: SetupAdministrationService },
        { type: ReportManagementService },
        { type: Router }
    ]; };
    return ConfigTrackerSetupComponent;
}());
export { ConfigTrackerSetupComponent };
var ConfigSetup = /** @class */ (function () {
    function ConfigSetup(control) {
        this.userName = control.username || '';
        this.password = control.password || '';
        this.clientID = control.clientID || '';
        this.fusionUrl = control.fusionUrl || '';
        this.scheduleType = control.scheduleType || '';
        this.retainDays = control.retainDays || '';
        this.scheduleTypeValue = control.scheduleTypeValue || '';
    }
    return ConfigSetup;
}());
export { ConfigSetup };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLXRyYWNrZXItc2V0dXAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImF2bS9jb25maWctdHJhY2tlci1zZXR1cC9jb25maWctdHJhY2tlci1zZXR1cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsV0FBVyxFQUEwQixVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNqRixPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMvRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDbEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFFOUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXpDO0lBbUNDLHFDQUNTLFdBQXdCLEVBQ3hCLGVBQWdDLEVBQ2hDLDBCQUFzRCxFQUN0RCx3QkFBaUQsRUFDakQsTUFBYztRQUpkLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQywrQkFBMEIsR0FBMUIsMEJBQTBCLENBQTRCO1FBQ3RELDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBeUI7UUFDakQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQS9CdkIsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUUxQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBUWxCLGlCQUFZLEdBQUcsQ0FBQztnQkFDZixPQUFPLEVBQUUsT0FBTztnQkFDaEIsTUFBTSxFQUFFLE9BQU87YUFDZixFQUFDO2dCQUNELE9BQU8sRUFBRSxLQUFLO2dCQUNkLE1BQU0sRUFBRSxLQUFLO2FBQ2IsRUFBRTtnQkFDRixPQUFPLEVBQUUsTUFBTTtnQkFDZixNQUFNLEVBQUUsTUFBTTthQUNkLEVBQUU7Z0JBQ0YsT0FBTyxFQUFFLE1BQU07Z0JBQ2YsTUFBTSxFQUFFLFNBQVM7YUFDakIsQ0FBQyxDQUFBO1FBRUYsU0FBSSxHQUFHLEVBQUUsQ0FBQztRQVVULHVDQUF1QztRQUN2QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQ3hDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ25DLFNBQVMsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3BDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ2pDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ25DLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ25DLFdBQVcsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3RDLFlBQVksRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3ZDLGlCQUFpQixFQUFDLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDM0MsVUFBVSxFQUFFLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDckMsa0JBQWtCLEVBQUMsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztTQUM1QyxDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsOENBQVEsR0FBUjtRQUNDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUM1QixJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVELDZDQUFPLEdBQVA7UUFDQywrRUFBK0U7UUFDL0UsaUNBQWlDO1FBQ2pDLG9DQUFvQztRQUNwQyxNQUFNO1FBSlAsaUJBa0JDO1FBWkEsSUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUM1QyxJQUFJLENBQUMsd0JBQXdCLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBUztZQUM5RSxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUE7WUFDdEQsSUFBRyxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBQztnQkFDdEQsS0FBSSxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDaEM7aUJBQ0c7Z0JBQ0gsS0FBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUM7YUFDekI7UUFFRixDQUFDLENBQUMsQ0FBQTtJQUNILENBQUM7SUFFRCxnREFBVSxHQUFWLFVBQVcsQ0FBQztRQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztRQUMxQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNqQyxDQUFDO0lBRUQscURBQWUsR0FBZixVQUFnQixNQUFNO1FBQ3JCLElBQUksQ0FBQyxZQUFZLEdBQUcsTUFBTSxDQUFBO1FBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUMsRUFBRSxDQUFDO1FBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUMsY0FBYyxDQUFDLENBQUE7UUFDbEMsSUFBRyxNQUFNLElBQUksT0FBTyxFQUFDO1lBQ3BCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxFQUFFLEVBQUM7Z0JBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNkLE1BQU0sRUFBQyxDQUFDLEdBQUMsQ0FBQztvQkFDVixPQUFPLEVBQUMsQ0FBQyxHQUFDLENBQUM7aUJBQ1gsQ0FBQyxDQUFBO2FBQ0Y7U0FDRDthQUNJLElBQUcsTUFBTSxJQUFJLEtBQUssRUFBQztZQUN2QixLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBQyxDQUFDLEdBQUMsRUFBRSxFQUFDLENBQUMsRUFBRSxFQUFDO2dCQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDZCxNQUFNLEVBQUMsQ0FBQyxHQUFDLENBQUM7b0JBQ1YsT0FBTyxFQUFDLENBQUMsR0FBQyxDQUFDO2lCQUNYLENBQUMsQ0FBQTthQUNGO1NBQ0Q7YUFDSSxJQUFHLE1BQU0sSUFBSSxNQUFNLEVBQUM7WUFDeEIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUUsRUFBQztnQkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2QsTUFBTSxFQUFDLENBQUMsR0FBQyxDQUFDO29CQUNWLE9BQU8sRUFBQyxDQUFDLEdBQUMsQ0FBQztpQkFDWCxDQUFDLENBQUE7YUFDRjtTQUNEO2FBQ0ksSUFBRyxNQUFNLElBQUksTUFBTSxFQUFDO1lBQ3hCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFFLEVBQUM7Z0JBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNkLE1BQU0sRUFBQyxDQUFDLEdBQUMsQ0FBQztvQkFDVixPQUFPLEVBQUMsQ0FBQyxHQUFDLENBQUM7aUJBQ1gsQ0FBQyxDQUFBO2FBQ0Y7U0FDRDtJQUNGLENBQUM7SUFHRCw4Q0FBUSxHQUFSLFVBQVMsU0FBUztRQUVqQixJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQztRQUNyRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQzVGLElBQUksQ0FBQyxJQUFJLEdBQUMsRUFBRSxDQUFDO1FBRWIsSUFBRyxJQUFJLENBQUMsWUFBWSxJQUFJLE9BQU8sRUFBQztZQUUvQixLQUFJLElBQUksQ0FBQyxHQUFDLENBQUMsRUFBQyxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsRUFBRSxFQUFDO2dCQUNuQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDZCxNQUFNLEVBQUMsQ0FBQyxHQUFDLENBQUM7b0JBQ1YsT0FBTyxFQUFDLENBQUMsR0FBQyxDQUFDO2lCQUNYLENBQUMsQ0FBQTthQUNGO1NBRUQ7YUFDSSxJQUFHLElBQUksQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFDO1lBQ2xDLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsR0FBQyxFQUFFLEVBQUMsQ0FBQyxFQUFFLEVBQUM7Z0JBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNkLE1BQU0sRUFBQyxDQUFDLEdBQUMsQ0FBQztvQkFDVixPQUFPLEVBQUMsQ0FBQyxHQUFDLENBQUM7aUJBQ1gsQ0FBQyxDQUFBO2FBQ0Y7U0FDRDthQUNJLElBQUcsSUFBSSxDQUFDLFlBQVksSUFBSSxNQUFNLEVBQUM7WUFDbkMsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxHQUFDLEVBQUUsRUFBQyxDQUFDLEVBQUUsRUFBQztnQkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2QsTUFBTSxFQUFDLENBQUMsR0FBQyxDQUFDO29CQUNWLE9BQU8sRUFBQyxDQUFDLEdBQUMsQ0FBQztpQkFDWCxDQUFDLENBQUE7YUFDRjtTQUNEO1FBR0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDMUIsUUFBUSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFDdEQsUUFBUSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFDdEQsUUFBUSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFDdEQsU0FBUyxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVM7WUFDeEQsWUFBWSxFQUFFLFNBQVMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVk7WUFDOUQsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLFFBQVEsRUFBRTtZQUNyRixVQUFVLEVBQUUsU0FBUyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUMxRCxrQkFBa0IsRUFBQyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxrQkFBa0I7U0FDekUsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDbEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDekQsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7SUFDcEUsQ0FBQztJQUdELDhDQUFRLEdBQVI7UUFBQSxpQkEwRUM7UUF6RUEsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsaUNBQWlDO1FBQ2pDLGtFQUFrRTtRQUNsRSx1QkFBdUI7UUFDdkIsNEJBQTRCO1FBQzVCLGVBQWU7UUFDZixJQUFJO1FBQ0osMkJBQTJCO1FBQzNCLCtEQUErRDtRQUMvRCx1QkFBdUI7UUFDdkIsNEJBQTRCO1FBQzVCLGVBQWU7UUFDZixLQUFLO1FBQ0wsU0FBUztRQUNULElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQTtRQUNkLElBQUksR0FBRyxHQUFHO1lBQ1QsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUs7WUFDdEQsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUs7WUFDeEQsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUs7WUFDdEQsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEtBQUs7WUFDdEQsY0FBYyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUs7WUFDOUQsbUJBQW1CLEVBQUUsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ2xGLFlBQVksRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxLQUFLO1lBQzFELFFBQVEsRUFBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUN2QyxhQUFhLEVBQUMsSUFBSSxDQUFDLFdBQVc7WUFDOUIsUUFBUSxFQUFDLElBQUksQ0FBQyxNQUFNO1NBRXBCLENBQUE7UUFFRCxJQUFHLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxFQUFDO1lBQzVCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxHQUFHO2dCQUNyRSxJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO29CQUN0QixLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDNUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDLENBQUM7aUJBQzVEO3FCQUFNO29CQUNOLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUM1QyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQztpQkFDNUQ7WUFDRixDQUFDLEVBQUUsVUFBQSxLQUFLO2dCQUNQLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUMzQyxJQUFJLEtBQUksQ0FBQyxXQUFXLElBQUksS0FBSyxJQUFJLEtBQUksQ0FBQyxXQUFXLElBQUksS0FBSyxFQUFFO29CQUMzRCxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQztvQkFDckMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUM1QyxVQUFVLENBQUM7d0JBQ1YsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3hCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDVDtZQUNGLENBQUMsQ0FBQyxDQUFBO1NBQ0Y7YUFDRztZQUNILElBQUksQ0FBQywwQkFBMEIsQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLEVBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7Z0JBQzFGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7b0JBQ3RCLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUM1QyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLG1DQUFtQyxDQUFDLENBQUMsQ0FBQztpQkFDNUQ7cUJBQU07b0JBQ04sS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsbUNBQW1DLENBQUMsQ0FBQyxDQUFDO2lCQUM1RDtZQUNGLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ1AsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0JBQzNDLElBQUksS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLElBQUksS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUU7b0JBQzNELEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO29CQUNyQyxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQzVDLFVBQVUsQ0FBQzt3QkFDVixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO2lCQUNUO1lBQ0YsQ0FBQyxDQUFDLENBQUE7U0FDRjtRQUlELElBQUk7SUFDTCxDQUFDOztnQkEzUEQsU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLHdzTUFBb0Q7aUJBQ3BEOzs7O2dCQVZRLFdBQVc7Z0JBRVgsZUFBZTtnQkFEZiwwQkFBMEI7Z0JBRTFCLHVCQUF1QjtnQkFFdkIsTUFBTTs7SUFnUWYsa0NBQUM7Q0FBQSxBQTlQRCxJQThQQztTQTFQWSwyQkFBMkI7QUE0UHhDO0lBVUMscUJBQVksT0FBTztRQUNsQixJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxJQUFJLEVBQUUsQ0FBQztRQUN2QyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQ3pDLElBQUksQ0FBQyxZQUFZLEdBQUcsT0FBTyxDQUFDLFlBQVksSUFBSSxFQUFFLENBQUE7UUFDOUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQztRQUMzQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixJQUFJLEVBQUUsQ0FBQztJQUMxRCxDQUFDO0lBQ0Ysa0JBQUM7QUFBRCxDQUFDLEFBbkJELElBbUJDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUJ1aWxkZXIsIEZvcm1Hcm91cCwgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi8uLi9zZXR1cC1hZG1pbmlzdHJhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFJlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXZtL3JlcG9ydC1tYW5hZ2VtZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBsb2NhbCB9IGZyb20gJ2QzJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiAnYXBwLWNvbmZpZy10cmFja2VyLXNldHVwJyxcclxuXHR0ZW1wbGF0ZVVybDogJy4vY29uZmlnLXRyYWNrZXItc2V0dXAuY29tcG9uZW50Lmh0bWwnLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29uZmlnVHJhY2tlclNldHVwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRDcmVhdGVGb3JtOiBGb3JtR3JvdXA7XHJcblx0Y29udHJvbDogTm90aWZpY2F0aW9uO1xyXG5cdGRhdGFTb3VyY2U7XHJcblx0ZGF0YV9zb3VyY2U7XHJcblx0aXNBY3RpdmU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHRzdGF0dXM7XHJcblx0c3VibWl0dGVkID0gZmFsc2U7XHJcblx0c2NoZWR1bGVUeXBlOiBhbnk7XHJcblx0c2NoZWR1bGVUeXBlVmFsdWU6YW55O1xyXG5cdGVycm9yU3RhdHVzOiBhbnk7XHJcblx0ZXJyb3JNc2c6IGFueTtcclxuXHRjb25maWdEZXRhaWxzSUQ6YW55O1xyXG5cdG1vZHVsZTpbXTtcclxuXHRvYmplY3RfbmFtZTpbXTtcclxuXHRmcmVxdWVudERhdGEgPSBbe1xyXG5cdFx0XCJ2YWx1ZVwiOiBcIm1vbnRoXCIsXHJcblx0XHRcIm5hbWVcIjogXCJNb250aFwiXHJcblx0fSx7XHJcblx0XHRcInZhbHVlXCI6IFwiZGF5XCIsXHJcblx0XHRcIm5hbWVcIjogXCJEYXlcIlxyXG5cdH0sIHtcclxuXHRcdFwidmFsdWVcIjogXCJob3VyXCIsXHJcblx0XHRcIm5hbWVcIjogXCJIb3VyXCJcclxuXHR9LCB7XHJcblx0XHRcInZhbHVlXCI6IFwibWluc1wiLFxyXG5cdFx0XCJuYW1lXCI6IFwiTWludXRlc1wiXHJcblx0fV1cclxuXHJcblx0ZGF5cyA9IFtdO1xyXG5cclxuXHRjb25zdHJ1Y3RvcihcclxuXHRcdHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG5cdFx0cHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSxcclxuXHRcdHByaXZhdGUgU2V0dXBBZG1pbmlzdHJhdGlvblNlcnZpY2U6IFNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSBfcmVwb3J0TWFuYWdlbWVudFNlcnZpY2U6IFJlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuXHJcblx0KSB7XHJcblx0XHQvLyB0aGlzLmNvbnRyb2wgPSBuZXcgTm90aWZpY2F0aW9uKHt9KTtcclxuXHRcdHRoaXMuQ3JlYXRlRm9ybSA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG5cdFx0XHRjbGllbnRJRDogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuXHRcdFx0ZnVzaW9uVXJsOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG5cdFx0XHRtb2R1bGU6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcblx0XHRcdHVzZXJOYW1lOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG5cdFx0XHRwYXNzd29yZDogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuXHRcdFx0b2JqZWN0X25hbWU6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcblx0XHRcdHNjaGVkdWxlVHlwZTogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuXHRcdFx0c2NoZWR1bGVUeXBlVmFsdWU6WycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuXHRcdFx0cmV0YWluRGF5czogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuXHRcdFx0cmV0YWluU2NoZWR1bGVUeXBlOlsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF1cclxuXHRcdH0pO1x0XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHRcdHRoaXMuc3RhdHVzID0gdGhpcy5pc0FjdGl2ZTtcclxuXHRcdHRoaXMubG9hZEFsbCgpO1xyXG5cdH1cclxuXHJcblx0bG9hZEFsbCgpIHtcclxuXHRcdC8vIHRoaXMuU2V0dXBBZG1pbmlzdHJhdGlvblNlcnZpY2UuZ2V0Tm90aWZpY2F0aW9uKCkuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuXHRcdC8vIFx0dGhpcy5kYXRhX3NvdXJjZSA9IGRhdGEuYm9keTtcclxuXHRcdC8vIFx0dGhpcy5sb2FkRGF0YSh0aGlzLmRhdGFfc291cmNlKTtcclxuXHRcdC8vIH0pO1xyXG5cclxuXHRcdHZhciBvcHRpb25zID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ1c2VySWRcIilcclxuXHRcdHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldEFsbENvbmZpZ1RyYWNrZXIob3B0aW9ucykuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuXHRcdFx0dGhpcy5kYXRhX3NvdXJjZSA9IGRhdGEuYm9keTtcclxuXHRcdFx0Y29uc29sZS5sb2coXCJ0aGlzLmRhdGFfc291cmNlLS0tLS0tXCIsdGhpcy5kYXRhX3NvdXJjZSlcclxuXHRcdFx0aWYodGhpcy5kYXRhX3NvdXJjZS5yZXNwb25zZS5jb25maWdEZXRhaWxzLmxlbmd0aCAhPSAwKXtcclxuXHRcdFx0XHR0aGlzLmxvYWREYXRhKHRoaXMuZGF0YV9zb3VyY2UpO1xyXG5cdFx0XHR9XHJcblx0XHRcdGVsc2V7XHJcblx0XHRcdFx0dGhpcy5jb25maWdEZXRhaWxzSUQgPSAwO1xyXG5cdFx0XHR9XHJcblx0XHRcdFxyXG5cdFx0fSlcclxuXHR9XHJcblxyXG5cdHNob3dEZXRhaWwoZSkge1xyXG5cdFx0dGhpcy5pc0FjdGl2ZSA9IGUuY2hlY2tlZDtcclxuXHRcdHRoaXMubG9hZERhdGEodGhpcy5kYXRhX3NvdXJjZSk7XHJcblx0fVxyXG5cclxuXHRnZXRmcmVxdWVudERhdGEoJGV2ZW50KSB7XHJcblx0XHR0aGlzLnNjaGVkdWxlVHlwZSA9ICRldmVudFxyXG5cdFx0dGhpcy5kYXlzPVtdO1xyXG5cdFx0Y29uc29sZS5sb2coJGV2ZW50LFwiLi4uLi4uLi5ldmVuXCIpXHJcblx0XHRpZigkZXZlbnQgPT0gXCJtb250aFwiKXtcclxuXHRcdFx0Zm9yKHZhciBpPTA7aTw2O2krKyl7XHJcblx0XHRcdFx0dGhpcy5kYXlzLnB1c2goe1xyXG5cdFx0XHRcdFx0J25hbWUnOmkrMSxcclxuXHRcdFx0XHRcdCd2YWx1ZSc6aSsxXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0ZWxzZSBpZigkZXZlbnQgPT0gXCJkYXlcIil7XHJcblx0XHRcdGZvcih2YXIgaT0wO2k8MzE7aSsrKXtcclxuXHRcdFx0XHR0aGlzLmRheXMucHVzaCh7XHJcblx0XHRcdFx0XHQnbmFtZSc6aSsxLFxyXG5cdFx0XHRcdFx0J3ZhbHVlJzppKzFcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRlbHNlIGlmKCRldmVudCA9PSBcImhvdXJcIil7XHJcblx0XHRcdGZvcih2YXIgaT0wO2k8MjQ7aSsrKXtcclxuXHRcdFx0XHR0aGlzLmRheXMucHVzaCh7XHJcblx0XHRcdFx0XHQnbmFtZSc6aSsxLFxyXG5cdFx0XHRcdFx0J3ZhbHVlJzppKzFcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRlbHNlIGlmKCRldmVudCA9PSBcIm1pbnNcIil7XHJcblx0XHRcdGZvcih2YXIgaT0wO2k8NjA7aSsrKXtcclxuXHRcdFx0XHR0aGlzLmRheXMucHVzaCh7XHJcblx0XHRcdFx0XHQnbmFtZSc6aSsxLFxyXG5cdFx0XHRcdFx0J3ZhbHVlJzppKzFcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHJcblx0bG9hZERhdGEoZGF0YVZhbHVlKSB7XHJcblx0XHRcclxuXHRcdHRoaXMuc2NoZWR1bGVUeXBlID0gZGF0YVZhbHVlLnJlc3BvbnNlLmNvbmZpZ0RldGFpbHNbMF0uc2NoZWR1bGVUeXBlO1xyXG5cdFx0dGhpcy5zY2hlZHVsZVR5cGVWYWx1ZSA9IChkYXRhVmFsdWUucmVzcG9uc2UuY29uZmlnRGV0YWlsc1swXS5zY2hlZHVsZVR5cGVWYWx1ZSkudG9TdHJpbmcoKTtcclxuXHRcdHRoaXMuZGF5cz1bXTtcclxuXHRcclxuXHRcdGlmKHRoaXMuc2NoZWR1bGVUeXBlID09IFwibW9udGhcIil7XHJcblx0XHRcdFxyXG5cdFx0XHRmb3IodmFyIGk9MDtpPDY7aSsrKXtcclxuXHRcdFx0XHR0aGlzLmRheXMucHVzaCh7XHJcblx0XHRcdFx0XHQnbmFtZSc6aSsxLFxyXG5cdFx0XHRcdFx0J3ZhbHVlJzppKzFcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9XHJcblxyXG5cdFx0fVxyXG5cdFx0ZWxzZSBpZih0aGlzLnNjaGVkdWxlVHlwZSA9PSBcImRheVwiKXtcclxuXHRcdFx0Zm9yKHZhciBpPTA7aTwzMTtpKyspe1xyXG5cdFx0XHRcdHRoaXMuZGF5cy5wdXNoKHtcclxuXHRcdFx0XHRcdCduYW1lJzppKzEsXHJcblx0XHRcdFx0XHQndmFsdWUnOmkrMVxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdGVsc2UgaWYodGhpcy5zY2hlZHVsZVR5cGUgPT0gXCJob3VyXCIpe1xyXG5cdFx0XHRmb3IodmFyIGk9MDtpPDI0O2krKyl7XHJcblx0XHRcdFx0dGhpcy5kYXlzLnB1c2goe1xyXG5cdFx0XHRcdFx0J25hbWUnOmkrMSxcclxuXHRcdFx0XHRcdCd2YWx1ZSc6aSsxXHJcblx0XHRcdFx0fSlcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0XHJcblxyXG5cdFx0dGhpcy5DcmVhdGVGb3JtLnBhdGNoVmFsdWUoe1xyXG5cdFx0XHR1c2VyTmFtZTogZGF0YVZhbHVlLnJlc3BvbnNlLmNvbmZpZ0RldGFpbHNbMF0udXNlck5hbWUsXHJcblx0XHRcdHBhc3N3b3JkOiBkYXRhVmFsdWUucmVzcG9uc2UuY29uZmlnRGV0YWlsc1swXS5wYXNzd29yZCxcclxuXHRcdFx0Y2xpZW50SUQ6IGRhdGFWYWx1ZS5yZXNwb25zZS5jb25maWdEZXRhaWxzWzBdLmNsaWVudElELFxyXG5cdFx0XHRmdXNpb25Vcmw6IGRhdGFWYWx1ZS5yZXNwb25zZS5jb25maWdEZXRhaWxzWzBdLmZ1c2lvblVybCxcclxuXHRcdFx0c2NoZWR1bGVUeXBlOiBkYXRhVmFsdWUucmVzcG9uc2UuY29uZmlnRGV0YWlsc1swXS5zY2hlZHVsZVR5cGUsXHJcblx0XHRcdHNjaGVkdWxlVHlwZVZhbHVlOiAoZGF0YVZhbHVlLnJlc3BvbnNlLmNvbmZpZ0RldGFpbHNbMF0uc2NoZWR1bGVUeXBlVmFsdWUpLnRvU3RyaW5nKCksXHJcblx0XHRcdHJldGFpbkRheXM6IGRhdGFWYWx1ZS5yZXNwb25zZS5jb25maWdEZXRhaWxzWzBdLnJldGFpbkRheXMsXHJcblx0XHRcdHJldGFpblNjaGVkdWxlVHlwZTpkYXRhVmFsdWUucmVzcG9uc2UuY29uZmlnRGV0YWlsc1swXS5yZXRhaW5TY2hlZHVsZVR5cGVcclxuXHRcdH0pXHJcblx0XHR0aGlzLmNvbmZpZ0RldGFpbHNJRCA9IGRhdGFWYWx1ZS5yZXNwb25zZS5jb25maWdEZXRhaWxzWzBdLnVzZXJJZDtcclxuXHRcdHRoaXMubW9kdWxlID0gZGF0YVZhbHVlLnJlc3BvbnNlLmNvbmZpZ0RldGFpbHNbMF0ubW9kdWxlO1xyXG5cdFx0dGhpcy5vYmplY3RfbmFtZSA9IGRhdGFWYWx1ZS5yZXNwb25zZS5jb25maWdEZXRhaWxzWzBdLm9iamVjdF9uYW1lO1xyXG5cdH1cclxuXHJcblxyXG5cdG9uU3VibWl0KCkge1xyXG5cdFx0dGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG5cdFx0Ly8gaWYgKHRoaXMuQ3JlYXRlRm9ybS5pbnZhbGlkKSB7XHJcblx0XHQvLyBcdHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXCJNYW5kYXRvcnkgRmllbGRzIGFyZSBSZXF1aXJlZFwiKTtcclxuXHRcdC8vIFx0c2V0VGltZW91dCgoKT0+eyAgIFxyXG5cdFx0Ly8gXHRcdHRoaXMuc3VibWl0dGVkID0gZmFsc2U7XHJcblx0XHQvLyAgICB9LCAzMDAwKTtcclxuXHRcdC8vIH1cclxuXHRcdC8vIGVsc2UgaWYodGhpcy5uYW1lRXhpc3Qpe1xyXG5cdFx0Ly8gXHR0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFwiQ29udHJvbCBOYW1lIEFscmVhZHkgRXhpc3RcIik7XHJcblx0XHQvLyBcdHNldFRpbWVvdXQoKCk9PnsgICBcclxuXHRcdC8vIFx0XHR0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG5cdFx0Ly8gICAgfSwgMzAwMCk7XHJcblx0XHQvLyB9IFxyXG5cdFx0Ly8gZWxzZSB7XHJcblx0XHR2YXIgcV9hcnIgPSBbXVxyXG5cdFx0dmFyIG9iaiA9IHtcclxuXHRcdFx0XCJjbGllbnRJRFwiOiB0aGlzLkNyZWF0ZUZvcm0uY29udHJvbHNbJ2NsaWVudElEJ10udmFsdWUsXHJcblx0XHRcdFwiZnVzaW9uVXJsXCI6IHRoaXMuQ3JlYXRlRm9ybS5jb250cm9sc1snZnVzaW9uVXJsJ10udmFsdWUsXHJcblx0XHRcdFwidXNlck5hbWVcIjogdGhpcy5DcmVhdGVGb3JtLmNvbnRyb2xzWyd1c2VyTmFtZSddLnZhbHVlLFxyXG5cdFx0XHRcInBhc3N3b3JkXCI6IHRoaXMuQ3JlYXRlRm9ybS5jb250cm9sc1sncGFzc3dvcmQnXS52YWx1ZSxcclxuXHRcdFx0XCJzY2hlZHVsZVR5cGVcIjogdGhpcy5DcmVhdGVGb3JtLmNvbnRyb2xzWydzY2hlZHVsZVR5cGUnXS52YWx1ZSxcclxuXHRcdFx0XCJzY2hlZHVsZVR5cGVWYWx1ZVwiOiBwYXJzZUludCh0aGlzLkNyZWF0ZUZvcm0uY29udHJvbHNbJ3NjaGVkdWxlVHlwZVZhbHVlJ10udmFsdWUpLFxyXG5cdFx0XHRcInJldGFpbkRheXNcIjogdGhpcy5DcmVhdGVGb3JtLmNvbnRyb2xzWydyZXRhaW5EYXlzJ10udmFsdWUsXHJcblx0XHRcdFwidXNlcklkXCI6bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJ1c2VySWRcIiksXHJcblx0XHRcdFwib2JqZWN0X25hbWVcIjp0aGlzLm9iamVjdF9uYW1lLFxyXG5cdFx0XHRcIm1vZHVsZVwiOnRoaXMubW9kdWxlLFxyXG5cclxuXHRcdH1cclxuXHJcblx0XHRpZih0aGlzLmNvbmZpZ0RldGFpbHNJRCA9PSAwKXtcclxuXHRcdFx0dGhpcy5TZXR1cEFkbWluaXN0cmF0aW9uU2VydmljZS5jcmVhdGVDb25maWdUcmFja2VyKG9iaikuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblx0XHRcdFx0aWYgKHJlcy5zdGF0dXMgPT0gMjAxKSB7XHJcblx0XHRcdFx0XHR0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQocmVzLmJvZHkubWV0YS5tc2cpO1xyXG5cdFx0XHRcdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvcmVwb3J0LW1hbmFnZW1lbnQvY29uZmlnLXRyYWNrZXInXSk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChyZXMuYm9keS5tZXRhLm1zZyk7XHJcblx0XHRcdFx0XHR0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9yZXBvcnQtbWFuYWdlbWVudC9jb25maWctdHJhY2tlciddKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sIGVycm9yID0+IHtcclxuXHRcdFx0XHR0aGlzLmVycm9yU3RhdHVzID0gZXJyb3IuZXJyb3IubWV0YS5zdGF0dXM7XHJcblx0XHRcdFx0aWYgKHRoaXMuZXJyb3JTdGF0dXMgPT0gJzUwMCcgfHwgdGhpcy5lcnJvclN0YXR1cyA9PSAnNDAwJykge1xyXG5cdFx0XHRcdFx0dGhpcy5lcnJvck1zZyA9IGVycm9yLmVycm9yLm1ldGEubXNnO1xyXG5cdFx0XHRcdFx0dGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyh0aGlzLmVycm9yTXNnKTtcclxuXHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0fSwgMzAwMCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0fVxyXG5cdFx0ZWxzZXtcclxuXHRcdFx0dGhpcy5TZXR1cEFkbWluaXN0cmF0aW9uU2VydmljZS51cGRhdGVDb25maWdEZXRhaWxzKG9iaix0aGlzLmNvbmZpZ0RldGFpbHNJRCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblx0XHRcdFx0aWYgKHJlcy5zdGF0dXMgPT0gMjAxKSB7XHJcblx0XHRcdFx0XHR0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQocmVzLmJvZHkubWV0YS5tc2cpO1xyXG5cdFx0XHRcdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvcmVwb3J0LW1hbmFnZW1lbnQvY29uZmlnLXRyYWNrZXInXSk7XHJcblx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChyZXMuYm9keS5tZXRhLm1zZyk7XHJcblx0XHRcdFx0XHR0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9yZXBvcnQtbWFuYWdlbWVudC9jb25maWctdHJhY2tlciddKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sIGVycm9yID0+IHtcclxuXHRcdFx0XHR0aGlzLmVycm9yU3RhdHVzID0gZXJyb3IuZXJyb3IubWV0YS5zdGF0dXM7XHJcblx0XHRcdFx0aWYgKHRoaXMuZXJyb3JTdGF0dXMgPT0gJzUwMCcgfHwgdGhpcy5lcnJvclN0YXR1cyA9PSAnNDAwJykge1xyXG5cdFx0XHRcdFx0dGhpcy5lcnJvck1zZyA9IGVycm9yLmVycm9yLm1ldGEubXNnO1xyXG5cdFx0XHRcdFx0dGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyh0aGlzLmVycm9yTXNnKTtcclxuXHRcdFx0XHRcdHNldFRpbWVvdXQoKCkgPT4ge1xyXG5cdFx0XHRcdFx0XHR0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0fSwgMzAwMCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KVxyXG5cdFx0fVxyXG5cdFx0XHJcblxyXG5cclxuXHRcdC8vIH1cclxuXHR9XHJcblxyXG5cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIENvbmZpZ1NldHVwIHtcclxuXHJcblx0dXNlck5hbWU6IHN0cmluZztcclxuXHRwYXNzd29yZDogc3RyaW5nO1xyXG5cdGNsaWVudElEOiBzdHJpbmc7XHJcblx0ZnVzaW9uVXJsOiBzdHJpbmc7XHJcblx0c2NoZWR1bGVUeXBlOiBzdHJpbmc7XHJcblx0cmV0YWluRGF5czogc3RyaW5nO1xyXG5cdHNjaGVkdWxlVHlwZVZhbHVlOiBzdHJpbmc7XHJcblxyXG5cdGNvbnN0cnVjdG9yKGNvbnRyb2wpIHtcclxuXHRcdHRoaXMudXNlck5hbWUgPSBjb250cm9sLnVzZXJuYW1lIHx8ICcnO1xyXG5cdFx0dGhpcy5wYXNzd29yZCA9IGNvbnRyb2wucGFzc3dvcmQgfHwgJyc7XHJcblx0XHR0aGlzLmNsaWVudElEID0gY29udHJvbC5jbGllbnRJRCB8fCAnJztcclxuXHRcdHRoaXMuZnVzaW9uVXJsID0gY29udHJvbC5mdXNpb25VcmwgfHwgJyc7XHJcblx0XHR0aGlzLnNjaGVkdWxlVHlwZSA9IGNvbnRyb2wuc2NoZWR1bGVUeXBlIHx8ICcnXHJcblx0XHR0aGlzLnJldGFpbkRheXMgPSBjb250cm9sLnJldGFpbkRheXMgfHwgJyc7XHJcblx0XHR0aGlzLnNjaGVkdWxlVHlwZVZhbHVlID0gY29udHJvbC5zY2hlZHVsZVR5cGVWYWx1ZSB8fCAnJztcclxuXHR9XHJcbn1cclxuXHJcbiJdfQ==