import * as tslib_1 from "tslib";
import { Component, ViewChild, Output } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { SnackBarService } from './../../shared/snackbar.service';
import { Router, ActivatedRoute } from "@angular/router";
import { ReportManagementService } from '../report-management.service';
import { manageAvDetailTableConfig } from '../../table_config';
import { fuseAnimations } from '../../@fuse/animations';
import { EventEmitter } from '@angular/core';
import * as c3 from 'c3';
import { MessageService } from "../../_services/message.service";
import { process } from '@progress/kendo-data-query';
var MONTH = { JAN: 0, FEB: 1, MAR: 2, APR: 3, MAY: 4, JUN: 5, JUL: 6, AUG: 7, SEP: 8, OCT: 9, NOV: 10, DEC: 11 };
var MONTH2 = { jan: 0, feb: 1, mar: 2, apr: 3, may: 4, jun: 5, jul: 6, aug: 7, sep: 8, oct: 9, nov: 10, dec: 11 };
// declare var c3: any;
var ManageAvDetailComponent = /** @class */ (function () {
    function ManageAvDetailComponent(router, _matDialog, _reportManagementService, activeRoute, snackBarService, location, messageService) {
        this.router = router;
        this._matDialog = _matDialog;
        this._reportManagementService = _reportManagementService;
        this.activeRoute = activeRoute;
        this.snackBarService = snackBarService;
        this.location = location;
        this.messageService = messageService;
        this.myEvent = new EventEmitter();
        this.selectedTableHeader = [];
        this.displayedColumns = [
            'entitlement',
            'username',
            'firstname',
            'lastname',
            'responsibiltyname'
        ];
        this.selection = new SelectionModel(true, []);
        this.nav_position = 'end';
        this.queryParams = {};
        this.limit = 10;
        this.offset = 0;
        this.roleChartData = [];
        this.userChartData = [];
        // Bar
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Population';
        this.colorScheme = {
            domain: ['#28a3dd']
        };
        // pie
        this.showLabels = true;
        this.showPieLegend = false;
        this.explodeSlices = false;
        this.doughnut = false;
        this.colorPieScheme = {
            domain: ['#3a3f51', '#23b7e5', '#f05050', '#27c24c', '#e60050', '#2980b9', '#7266ba', '#2accdb']
        };
        this.pieChartView = [300, 250];
        this.enableFilter = false;
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.state = {};
        this.panelOpenState = false;
        // console.log("av detail URL Parms **************",this.activeRoute.snapshot.params)
        this.parmsObj = this.activeRoute.snapshot.params;
    }
    ManageAvDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.state = {
            skip: this.offset,
            take: this.limit
        };
        console.log(this.router.url, ">>>THIS");
        var urlString = decodeURIComponent(this.router.url);
        this.urlParams = urlString.split('/');
        this.displayNam = this.urlParams[5]; //this._reportManagementService.passValue['displayName'];
        this.getdatasource();
        this.loadTableColumn();
        this.queryParams.datasource2 = this.urlParams[3]; //this._reportManagementService.passValue.datasourceId;
        this.queryParams.collectionDetail = this.urlParams[4]; //this._reportManagementService.passValue.collectionDetail;
        setTimeout(function () {
            _this.onLoadChart();
        }, 500);
    };
    ManageAvDetailComponent.prototype.dataStateChange = function (state) {
        this.state = state;
        this.applyTableState(this.state);
    };
    ManageAvDetailComponent.prototype.applyTableState = function (state) {
        this.kendoGridData = process(this._data, state);
    };
    ManageAvDetailComponent.prototype.changePage = function (event) {
        console.log(event, ">>> EVENT Change Page");
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
    };
    ManageAvDetailComponent.prototype.onClick = function (action) {
        if (action == 'refresh') {
            this.ngOnInit();
        }
    };
    ManageAvDetailComponent.prototype.enableFilterOptions = function () {
        this.enableFilter = !this.enableFilter;
    };
    ManageAvDetailComponent.prototype.backClicked = function () {
        this.location.back();
        var obj = {
            id: "manageAvm"
        };
        var self = this;
        setTimeout(function () {
            self.messageService.sendRouting(obj);
        }, 100);
    };
    ManageAvDetailComponent.prototype.tableSetting = function () {
        this.panelOpenState = !this.panelOpenState;
    };
    ManageAvDetailComponent.prototype.getdatasource = function () {
        var _this = this;
        this._reportManagementService.getAllDatasources().subscribe(function (res) {
            _this.customdatasource = res.response.datasources;
            _this.defaultdatasourceId = res.response.datasources[0]._id;
            _this.onLoadmanageAvmDetail(_this.defaultdatasourceId);
        });
    };
    ManageAvDetailComponent.prototype.updateTable = function (event) {
        console.log(event, "----event");
        var selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isactive = true;
            }
            else {
                item.isactive = false;
            }
        });
        localStorage.removeItem('manageAvsDetail');
        localStorage.setItem('manageAvsDetail', JSON.stringify(this.tableConfig));
        this.loadTableColumn();
        // var index = this.tableNamesearch(event.source.value, this.tableConfig);
        // if (index >= 0) {
        // 	if (event.checked) {
        // 		let active = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: true
        // 		};
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, active);
        // 			localStorage.removeItem("manageAvsDetail");
        // 			localStorage.setItem("manageAvsDetail", JSON.stringify(this.tableConfig));
        // 		}
        // 	} else {
        // 		let inactive = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: false
        // 		};
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, inactive);
        // 			localStorage.removeItem("manageAvsDetail");
        // 			localStorage.setItem("manageAvsDetail", JSON.stringify(this.tableConfig));
        // 		}
        // 	}
        // }
        // this.loadTableColumn();
    };
    ManageAvDetailComponent.prototype.tableNamesearch = function (nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].value === nameKey) {
                return i;
            }
        }
    };
    ManageAvDetailComponent.prototype.loadTableColumn = function () {
        var e_1, _a, e_2, _b;
        this.displayedColumns = [];
        this.tableConfig = JSON.parse(localStorage.getItem('manageAvsDetail'));
        if (this.tableConfig) {
            try {
                for (var _c = tslib_1.__values(this.tableConfig), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var columns = _d.value;
                    if (columns.isactive) {
                        this.selectedTableHeader.push(columns.value);
                        // this.displayedColumns.push(columns.value);
                        this.displayedColumns.push(columns);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            this.tableConfig = manageAvDetailTableConfig;
            try {
                for (var _e = tslib_1.__values(this.tableConfig), _f = _e.next(); !_f.done; _f = _e.next()) {
                    var columns = _f.value;
                    if (columns.isactive) {
                        this.selectedTableHeader.push(columns.value);
                        //this.displayedColumns.push(columns.value);
                        this.displayedColumns.push(columns);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                }
                finally { if (e_2) throw e_2.error; }
            }
        }
    };
    ManageAvDetailComponent.prototype.getDatasourceId = function (id) {
        this.onLoadmanageAvmDetail(id);
    };
    ManageAvDetailComponent.prototype.onLoadmanageAvmDetail = function (id) {
        var _this = this;
        this.defaultdatasourceId = id;
        this._reportManagementService.getallmanageAvDetail(this.queryParams).subscribe(function (response) {
            _this.manageAvslist = response.data;
            _this.dataSource2 = new MatTableDataSource(_this.manageAvslist);
            _this.length = response.total;
            _this.dataSource2.paginator = _this.paginator;
            _this._data = _this.manageAvslist;
            _this.applyTableState(_this.state);
        });
    };
    ManageAvDetailComponent.prototype.onLoadChart = function () {
        var _this = this;
        //Chart 1
        this._reportManagementService.getCategoryPercentage(this.queryParams).subscribe(function (response) {
            var e_3, _a;
            _this.chartData = [];
            if (_this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails' || _this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails-ebs') {
                _this.title1 = 'Invoice Type';
            }
            else if (_this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails' || _this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails-ebs') {
                _this.title1 = 'Top 5 Vendors';
            }
            else {
                _this.title1 = "Journals by Category";
            }
            try {
                for (var _b = tslib_1.__values(response.data), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var result = _c.value;
                    var obj = result;
                    var ids = [];
                    ids.push(obj.name);
                    ids.push(obj.percentage);
                    _this.chartData.push(ids);
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_3) throw e_3.error; }
            }
            //this.chartData = [['Adjustmentsss', 20],['Accrual', 10],['Reclass', 45],['Other', 25]];
            var chart1 = c3.generate({
                bindto: '#chart1',
                data: {
                    columns: _this.chartData,
                    //json : this.chartData,
                    type: 'pie',
                },
                color: {
                    pattern: ['#396AB1', '#DA7C30', '#3E9651', '#CC2529', '#535154', '#6B4C9A', '#948B3D']
                },
                legend: {
                    show: true
                },
            });
        });
        //Chart 2
        this._reportManagementService.getTopAmount(this.queryParams).subscribe(function (response) {
            var e_4, _a, e_5, _b, e_6, _c, e_7, _d, e_8, _e, e_9, _f, e_10, _g;
            _this.chartData2 = [];
            if (_this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails' || _this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails-ebs') {
                _this.title2 = 'Top 5 users with amount';
            }
            else if (_this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails' || _this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails-ebs') {
                _this.title2 = 'Top 5 Users with Amount Paid';
            }
            else {
                _this.title2 = "Top 5 Journals";
            }
            var responsearray = response.data;
            responsearray.sort(function (a, b) {
                return a.year - b.year || MONTH[a.month] - MONTH[b.month];
            });
            /*
            0: {journalname: "Conversie Adj-02", month: "MAR", year: "2005", amount: 10000}
            1: {journalname: "ABN 04-FEB-05 B025", month: "FEB", year: "2005", amount: 9500}
            2: {journalname: "DRI INT EXP Correction: 12-MAY-05 14:43:25", month: "MAY", year: "2005", amount: 9000}
            3: {journalname: "APR0014", month: "MAY", year: "2005", amount: 8500}
            4: {journalname: "Purchase Invoices USD", month: "FEB", year: "2005", amount: 8000}
            */
            /*
            this.chartData2 = [
            ['month','2019-03-25', '2019-04-25'],
            ['Conversie Adj-02', 2000, 2500],
            ['KAS FEB-05', 3000, 3500],
            ['Conversie Aug-03 Conversion EUR', 4000, 4500],
            ['MEMO 31-MAR-05 M027', 4500,2500],
            ['Purchase Invoices USD', 5000, 1000]
            ];
            */
            var months = ['month'];
            var jnames = [];
            try {
                for (var responsearray_1 = tslib_1.__values(responsearray), responsearray_1_1 = responsearray_1.next(); !responsearray_1_1.done; responsearray_1_1 = responsearray_1.next()) {
                    var result = responsearray_1_1.value;
                    var obj = result;
                    var ids = [];
                    //username array object
                    months.push(obj.month);
                    ids.push(obj.journalname);
                    jnames.push(obj.journalname);
                    ids.push(obj.month);
                    ids.push(obj.year);
                    ids.push(obj.amount);
                    _this.chartData2.push(ids);
                }
            }
            catch (e_4_1) { e_4 = { error: e_4_1 }; }
            finally {
                try {
                    if (responsearray_1_1 && !responsearray_1_1.done && (_a = responsearray_1.return)) _a.call(responsearray_1);
                }
                finally { if (e_4) throw e_4.error; }
            }
            var uniquemonths = months.filter(function (elem, i, arr) {
                if (arr.indexOf(elem) === i) {
                    return elem;
                }
            });
            //unique jnames
            var uniquejnames = jnames.filter(function (elem, i, arr) {
                if (arr.indexOf(elem) === i) {
                    return elem;
                }
            });
            var journalarraysets = [];
            try {
                for (var responsearray_2 = tslib_1.__values(responsearray), responsearray_2_1 = responsearray_2.next(); !responsearray_2_1.done; responsearray_2_1 = responsearray_2.next()) {
                    var result = responsearray_2_1.value;
                    try {
                        for (var uniquemonths_1 = tslib_1.__values(uniquemonths), uniquemonths_1_1 = uniquemonths_1.next(); !uniquemonths_1_1.done; uniquemonths_1_1 = uniquemonths_1.next()) {
                            var eachmonth = uniquemonths_1_1.value;
                            if (eachmonth != 'month') {
                                var journalnames = [result.journalname];
                                if (eachmonth == result.month) {
                                    if (result.amount != '') {
                                        journalnames.push(result.amount);
                                        journalarraysets.push(journalnames);
                                    }
                                }
                                else {
                                    journalnames.push(0);
                                    journalarraysets.push(journalnames);
                                }
                            }
                        }
                    }
                    catch (e_6_1) { e_6 = { error: e_6_1 }; }
                    finally {
                        try {
                            if (uniquemonths_1_1 && !uniquemonths_1_1.done && (_c = uniquemonths_1.return)) _c.call(uniquemonths_1);
                        }
                        finally { if (e_6) throw e_6.error; }
                    }
                }
            }
            catch (e_5_1) { e_5 = { error: e_5_1 }; }
            finally {
                try {
                    if (responsearray_2_1 && !responsearray_2_1.done && (_b = responsearray_2.return)) _b.call(responsearray_2);
                }
                finally { if (e_5) throw e_5.error; }
            }
            var finalarrayset = [];
            try {
                for (var uniquejnames_1 = tslib_1.__values(uniquejnames), uniquejnames_1_1 = uniquejnames_1.next(); !uniquejnames_1_1.done; uniquejnames_1_1 = uniquejnames_1.next()) {
                    var eachjnames = uniquejnames_1_1.value;
                    var newjrnarrays = [eachjnames];
                    try {
                        for (var journalarraysets_1 = tslib_1.__values(journalarraysets), journalarraysets_1_1 = journalarraysets_1.next(); !journalarraysets_1_1.done; journalarraysets_1_1 = journalarraysets_1.next()) {
                            var entry = journalarraysets_1_1.value;
                            if (entry[0] === eachjnames) {
                                newjrnarrays.push(entry[1]);
                            }
                        }
                    }
                    catch (e_8_1) { e_8 = { error: e_8_1 }; }
                    finally {
                        try {
                            if (journalarraysets_1_1 && !journalarraysets_1_1.done && (_e = journalarraysets_1.return)) _e.call(journalarraysets_1);
                        }
                        finally { if (e_8) throw e_8.error; }
                    }
                    finalarrayset.push(newjrnarrays);
                }
            }
            catch (e_7_1) { e_7 = { error: e_7_1 }; }
            finally {
                try {
                    if (uniquejnames_1_1 && !uniquejnames_1_1.done && (_d = uniquejnames_1.return)) _d.call(uniquejnames_1);
                }
                finally { if (e_7) throw e_7.error; }
            }
            var chart2array = [];
            try {
                //chart2array.push(uniquemonths);
                for (var finalarrayset_1 = tslib_1.__values(finalarrayset), finalarrayset_1_1 = finalarrayset_1.next(); !finalarrayset_1_1.done; finalarrayset_1_1 = finalarrayset_1.next()) {
                    var eachrow = finalarrayset_1_1.value;
                    chart2array.push(eachrow);
                }
            }
            catch (e_9_1) { e_9 = { error: e_9_1 }; }
            finally {
                try {
                    if (finalarrayset_1_1 && !finalarrayset_1_1.done && (_f = finalarrayset_1.return)) _f.call(finalarrayset_1);
                }
                finally { if (e_9) throw e_9.error; }
            }
            var monthcategorized = [];
            try {
                for (var uniquemonths_2 = tslib_1.__values(uniquemonths), uniquemonths_2_1 = uniquemonths_2.next(); !uniquemonths_2_1.done; uniquemonths_2_1 = uniquemonths_2.next()) {
                    var monthname = uniquemonths_2_1.value;
                    if (monthname !== 'month') {
                        monthcategorized.push(monthname);
                    }
                }
            }
            catch (e_10_1) { e_10 = { error: e_10_1 }; }
            finally {
                try {
                    if (uniquemonths_2_1 && !uniquemonths_2_1.done && (_g = uniquemonths_2.return)) _g.call(uniquemonths_2);
                }
                finally { if (e_10) throw e_10.error; }
            }
            var chart2 = c3.generate({
                bindto: '#chart2',
                data: {
                    columns: chart2array,
                    type: 'bar'
                },
                color: {
                    pattern: ['#396AB1', '#DA7C30', '#3E9651', '#CC2529', '#535154', '#6B4C9A', '#948B3D']
                },
                bar: {
                    width: 25
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: monthcategorized
                    }
                }
            });
        });
        //Chart 3
        this._reportManagementService.getTotalNumberofJournal(this.queryParams).subscribe(function (response) {
            var e_11, _a, e_12, _b, e_13, _c, e_14, _d, e_15, _e, e_16, _f, e_17, _g;
            _this.chartData3 = [];
            if (_this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails' || _this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails-ebs') {
                _this.title3 = 'Invoice amount by month';
            }
            else if (_this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails' || _this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails-ebs') {
                _this.title3 = 'Amount paid by month';
            }
            else {
                _this.title3 = "Journals by Period";
            }
            var responsearray = response.data;
            responsearray.sort(function (a, b) {
                return a.year - b.year || MONTH2[a.month.toLowerCase()] - MONTH2[b.month.toLowerCase()];
            });
            var months = ['month'];
            try {
                for (var responsearray_3 = tslib_1.__values(responsearray), responsearray_3_1 = responsearray_3.next(); !responsearray_3_1.done; responsearray_3_1 = responsearray_3.next()) {
                    var result = responsearray_3_1.value;
                    var obj = result;
                    var ids = [];
                    //username array object
                    months.push(obj.month);
                    ids.push(obj.month);
                    ids.push(obj.year);
                    ids.push(obj.count);
                    _this.chartData3.push(ids);
                }
            }
            catch (e_11_1) { e_11 = { error: e_11_1 }; }
            finally {
                try {
                    if (responsearray_3_1 && !responsearray_3_1.done && (_a = responsearray_3.return)) _a.call(responsearray_3);
                }
                finally { if (e_11) throw e_11.error; }
            }
            var uniquemonths = months.filter(function (elem, i, arr) {
                if (arr.indexOf(elem) === i) {
                    return elem;
                }
            });
            var journalarraysets = [];
            try {
                for (var responsearray_4 = tslib_1.__values(responsearray), responsearray_4_1 = responsearray_4.next(); !responsearray_4_1.done; responsearray_4_1 = responsearray_4.next()) {
                    var result = responsearray_4_1.value;
                    try {
                        for (var uniquemonths_3 = tslib_1.__values(uniquemonths), uniquemonths_3_1 = uniquemonths_3.next(); !uniquemonths_3_1.done; uniquemonths_3_1 = uniquemonths_3.next()) {
                            var eachmonth = uniquemonths_3_1.value;
                            if (eachmonth != 'month') {
                                var journalperiod = [result.month];
                                if (eachmonth == result.month) {
                                    if (result.count != '') {
                                        journalperiod.push(result.count);
                                        journalarraysets.push(journalperiod);
                                    }
                                }
                                else {
                                    journalperiod.push(0);
                                    journalarraysets.push(journalperiod);
                                }
                            }
                        }
                    }
                    catch (e_13_1) { e_13 = { error: e_13_1 }; }
                    finally {
                        try {
                            if (uniquemonths_3_1 && !uniquemonths_3_1.done && (_c = uniquemonths_3.return)) _c.call(uniquemonths_3);
                        }
                        finally { if (e_13) throw e_13.error; }
                    }
                }
            }
            catch (e_12_1) { e_12 = { error: e_12_1 }; }
            finally {
                try {
                    if (responsearray_4_1 && !responsearray_4_1.done && (_b = responsearray_4.return)) _b.call(responsearray_4);
                }
                finally { if (e_12) throw e_12.error; }
            }
            var finalarrayset = [];
            try {
                for (var uniquemonths_4 = tslib_1.__values(uniquemonths), uniquemonths_4_1 = uniquemonths_4.next(); !uniquemonths_4_1.done; uniquemonths_4_1 = uniquemonths_4.next()) {
                    var eachjperiod = uniquemonths_4_1.value;
                    var newjrnarrays = [eachjperiod];
                    try {
                        for (var journalarraysets_2 = tslib_1.__values(journalarraysets), journalarraysets_2_1 = journalarraysets_2.next(); !journalarraysets_2_1.done; journalarraysets_2_1 = journalarraysets_2.next()) {
                            var entry = journalarraysets_2_1.value;
                            if (entry[0] === eachjperiod) {
                                newjrnarrays.push(entry[1]);
                            }
                        }
                    }
                    catch (e_15_1) { e_15 = { error: e_15_1 }; }
                    finally {
                        try {
                            if (journalarraysets_2_1 && !journalarraysets_2_1.done && (_e = journalarraysets_2.return)) _e.call(journalarraysets_2);
                        }
                        finally { if (e_15) throw e_15.error; }
                    }
                    finalarrayset.push(newjrnarrays);
                }
            }
            catch (e_14_1) { e_14 = { error: e_14_1 }; }
            finally {
                try {
                    if (uniquemonths_4_1 && !uniquemonths_4_1.done && (_d = uniquemonths_4.return)) _d.call(uniquemonths_4);
                }
                finally { if (e_14) throw e_14.error; }
            }
            var chart3array = [];
            try {
                //chart2array.push(uniquemonths);
                for (var finalarrayset_2 = tslib_1.__values(finalarrayset), finalarrayset_2_1 = finalarrayset_2.next(); !finalarrayset_2_1.done; finalarrayset_2_1 = finalarrayset_2.next()) {
                    var eachrow = finalarrayset_2_1.value;
                    chart3array.push(eachrow);
                }
            }
            catch (e_16_1) { e_16 = { error: e_16_1 }; }
            finally {
                try {
                    if (finalarrayset_2_1 && !finalarrayset_2_1.done && (_f = finalarrayset_2.return)) _f.call(finalarrayset_2);
                }
                finally { if (e_16) throw e_16.error; }
            }
            var monthcategorized = [];
            try {
                for (var uniquemonths_5 = tslib_1.__values(uniquemonths), uniquemonths_5_1 = uniquemonths_5.next(); !uniquemonths_5_1.done; uniquemonths_5_1 = uniquemonths_5.next()) {
                    var monthname = uniquemonths_5_1.value;
                    if (monthname !== 'month') {
                        monthcategorized.push(monthname);
                    }
                }
            }
            catch (e_17_1) { e_17 = { error: e_17_1 }; }
            finally {
                try {
                    if (uniquemonths_5_1 && !uniquemonths_5_1.done && (_g = uniquemonths_5.return)) _g.call(uniquemonths_5);
                }
                finally { if (e_17) throw e_17.error; }
            }
            var chart3 = c3.generate({
                bindto: '#chart3',
                data: {
                    columns: chart3array,
                    type: 'bar'
                },
                bar: {
                    width: 25
                },
                color: {
                    pattern: ['#396AB1', '#DA7C30', '#3E9651', '#CC2529', '#535154', '#6B4C9A', '#948B3D']
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: monthcategorized
                    }
                }
            });
        });
    };
    ManageAvDetailComponent.prototype.showAvDetail = function () {
        //localStorage.removeItem("editUserId");
        //localStorage.setItem("editUserId", user.id.toString());
        var obj = {
            url: 'report-management/manage-av-byuser',
            id: "manageAvUser"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['/', 'report-management/manage-av-byuser']).then(function (nav) {
        }, function (err) {
            console.log(err); // when there's an error
        });
    };
    ;
    ManageAvDetailComponent.prototype.onSelect = function (evt, selectedVal) {
        var obj = {
            url: 'report-management/manage-av-byuser',
            id: "manageAvUser"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['report-management/manage-av-byuser']);
        // this.myEvent.emit({createdby:selectedItem.username,journalname:selectedItem.entitlement});
        this._reportManagementService.passValue['createdBy'] = selectedVal.username;
        this._reportManagementService.passValue['journalname'] = selectedVal.entitlement;
    };
    ManageAvDetailComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-manage-av-detail',
                    template: "<div class=\"page-layout blank\" style=\"padding: 0px !important;\" fusePerfectScrollbar >\r\n\t<div style=\"padding: 10px\">\r\n\t\t<button mat-button (click)=\"backClicked()\">\r\n\t\t\t<mat-icon>arrow_left</mat-icon>Back to AV\r\n\t\t</button>\r\n\t</div>\r\n\t<!-- <mat-drawer-container class=\"example-container sen-bg-container\" autosize fxFlex [hasBackdrop]=\"false\"> -->\r\n      <div>\r\n\t\t<!-- CONTENT HEADER -->\r\n\t\t<div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\"\r\n\t\t\tstyle=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: space-between;\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n\t\t\t\tstyle=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: flex-start;\">\r\n\t\t\t\t<h2 class=\"m-0 senlib-top-header\">\r\n\t\t\t\t\t<span class=\"material-icons\"\r\n                    style=\"margin-right: 7px;font-size: 21px;position: relative;top:2px\">beenhere</span>\r\n\t\t\t\t\t{{displayNam}}\r\n\t\t\t\t</h2>\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilterOptions()\">\r\n\t\t\t\t\t<span class=\"k-icon k-i-filter\"\r\n\t\t\t\t\t  style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n\t\t\t\t</button>\r\n\t\t\t\t<button  class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n\t\t\t\t(click)=\"select.open()\">\r\n\t\t\t\t<span class=\"material-icons\"\r\n\t\t\t\t  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n\t\t\t\t<mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n\t\t\t\t  (selectionChange)=\"updateTable($event)\">\r\n\t\t\t\t  <mat-option *ngFor=\"let columnSetting of tableConfig\" [checked]=\"columnSetting.isactive\"\r\n\t\t\t\t\t[value]=\"columnSetting.value\">{{ columnSetting.name}}</mat-option>\r\n\t\t\t\t</mat-select>\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"  (click)=\"onClick('refresh')\">\r\n\t\t\t\t   <span class=\"material-icons\"\r\n\t\t\t\t   style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n\t\t\t\t   autorenew</span>Refresh\r\n\t\t\t\t</button>\r\n\t\t   </div>\r\n\t\t</div>\r\n\t\t<!-- / CONTENT HEADER -->\r\n\t\t<!-- <div class=\"content\"> -->\r\n\t\t\t<div class=\"ui-lib-card\">\r\n\t\t\t\t<div class=\"ui-lib-card-body\">\r\n\t\t\t\t\t<div class=\"row\" style=\"margin-right:10px;margin-left:10px;\">\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<div class=\"ui-lib-chart-header\">{{title1}}</div>\r\n\t\t\t\t\t\t\t<div id=\"chart1\"></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<div class=\"ui-lib-chart-header\">{{title2}}</div>\r\n\t\t\t\t\t\t\t<div id=\"chart2\"></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<div class=\"ui-lib-chart-header\">{{title3}}</div>\r\n\t\t\t\t\t\t\t<div id=\"chart3\"></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row\" style=\"margin-right:10px;margin-left:10px;\">\r\n\t\t\t\t\t\t<!-- Kendo UI Start -->\r\n\t\t\t\t\t\t<div class=\"mat-elevation-z8 sen-margin-10\">\r\n\t\t\t\t\t\t\t<kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n\t\t\t\t\t\t\t\t[skip]=\"state.skip\"\r\n\t\t\t\t\t\t\t\t[sort]=\"state.sort\"\r\n\t\t\t\t\t\t\t\t[filter]=\"state.filter\"\r\n\t\t\t\t\t\t\t\t[sortable]=\"true\"\r\n\t\t\t\t\t\t\t\t[pageable]=\"true\" \r\n\t\t\t\t\t\t\t\t[resizable]=\"true\"\r\n\t\t\t\t\t\t\t\t[filterable]=\"enableFilter\"\r\n\t\t\t\t\t\t\t\t(dataStateChange)=\"dataStateChange($event)\"\r\n\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n\t\t\t\t\t\t\t\t\t<kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\" *ngIf=\"!column.onSelect\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-template kendoGridCellTemplate let-dataItem>\r\n\t\t\t\t\t\t\t\t\t\t\t<span> \r\n\t\t\t\t\t\t\t\t\t\t\t\t{{dataItem[column.value]}}\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t\t\t</kendo-grid-column>\r\n\r\n\t\t\t\t\t\t\t\t\t<kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n\t\t\t\t\t\t\t\t\t*ngIf=\"column.onSelect\" title=\"{{column.name}}\" field=\"{{column.value}}\" width=\"100\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-template kendoGridCellTemplate let-dataItem>\r\n\t\t\t\t\t\t\t\t\t\t\t<a (click)=\"onSelect($event, dataItem)\">{{dataItem[column.value]}}</a>\r\n\t\t\t\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t\t\t</kendo-grid-column>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t<ng-template kendoGridNoRecordsTemplate>\r\n\t\t\t\t\t\t\t\tNo Records Found\r\n\t\t\t\t\t\t\t</ng-template> \r\n\t\t\t\t\t\t\t</kendo-grid>\r\n\t\t\t\t\t\t</div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\r\n\t\t<!-- Kendo UI End -->\r\n      </div>\r\n\t<!-- </mat-drawer-container> -->\r\n</div>",
                    animations: fuseAnimations,
                    styles: [".tick text{font-size:12px}.axis line,.axis path{fill:none;stroke:#4c5554;stroke-width:1}.x.axis .tick line{display:none}.domain{display:block!important;stroke:#4c5554!important;stroke-width:2!important}.legend{font-size:12px;font-family:sans-serif}.legend rect{stroke-width:2}.chart-row-box{width:100%;margin-bottom:5px;padding-bottom:5px}.chartonebox{background-color:#fff;float:left;padding:10px;margin-right:4px;margin-bottom:30px}.twenty{width:20%}.thirty{width:33%}.fourty{width:40%}.custom-mat-form{display:inline!important}table{width:100%}.mat-header-cell{font-weight:700!important;color:#000!important}td.mat-cell,td.mat-footer-cell,th.mat-header-cell{border-bottom-width:1px!important}.mat-form-field{font-size:12px;line-height:15px;font-weight:400}mat-label{font-size:12px;font-weight:700}app-chart-layout,app-pie-chart,app-pie-grid{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.charts-view{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;flex-basis:100%;width:33%!important;-webkit-box-flex:1;flex:1;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.mat-option{white-space:none!important}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.senlib-padding-10{padding:10px;padding-top:0!important}.senlib-m{margin-left:16px;margin-right:13px!important;margin-top:0;margin-bottom:-34px}content{position:relative;display:-webkit-box;display:flex;z-index:1;-webkit-box-flex:1;flex:1 0 auto}content>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto;width:100%;min-width:100%}.card-directive{background:#fff;border:1px solid #d3d3d3;margin:5px!important}chart-layout{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;-webkit-box-flex:1!important;flex:auto!important}.ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.sentri-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}.example-container{width:100%;height:100%}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}.ui-lib-card{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:0;word-wrap:break-word;background-clip:border-box;border:0 solid transparent;border-radius:.25rem;background-color:#fff;-webkit-transition:.5s ease-in-out;transition:.5s ease-in-out;position:relative;height:calc(100% - 30px);margin:10px}.ui-card-body{padding:1.25rem;-webkit-box-flex:1;flex:1 1 auto}.ui-lib-card-body{-webkit-box-flex:1;flex:1 1 auto;padding:1rem}.ui-lib-card-title{font-size:16px;margin-bottom:.5rem}.ui-lib-card-header{padding:.5rem 1rem;margin-bottom:0;background-color:rgba(0,0,0,.03);border-bottom:1px solid rgba(0,0,0,.125)}.ui-lib-chart-header{padding-bottom:17px;color:#06065d;font-size:16px;font-weight:600;text-align:center}"]
                }] }
    ];
    /** @nocollapse */
    ManageAvDetailComponent.ctorParameters = function () { return [
        { type: Router },
        { type: MatDialog },
        { type: ReportManagementService },
        { type: ActivatedRoute },
        { type: SnackBarService },
        { type: Location },
        { type: MessageService }
    ]; };
    ManageAvDetailComponent.propDecorators = {
        myEvent: [{ type: Output }],
        paginator: [{ type: ViewChild, args: [MatPaginator,] }]
    };
    return ManageAvDetailComponent;
}());
export { ManageAvDetailComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlLWF2LWRldGFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYXZtL21hbmFnZS1hdi1kZXRhaWwvbWFuYWdlLWF2LWRldGFpbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsU0FBUyxFQUFxQixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxZQUFZLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNyRSxPQUFPLEVBQUUsU0FBUyxFQUFjLE1BQU0sbUJBQW1CLENBQUM7QUFDMUQsT0FBTyxFQUFDLFFBQVEsRUFBQyxNQUFNLGlCQUFpQixDQUFDO0FBRXpDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ3ZFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRS9ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdDLE9BQU8sS0FBSyxFQUFFLE1BQU0sSUFBSSxDQUFDO0FBQ3pCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNqRSxPQUFPLEVBQStCLE9BQU8sRUFBUSxNQUFNLDRCQUE0QixDQUFDO0FBYXhGLElBQUksS0FBSyxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQztBQUNqSCxJQUFJLE1BQU0sR0FBRyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLENBQUM7QUFDbEgsdUJBQXVCO0FBQ3ZCO0lBcUZFLGlDQUFvQixNQUFjLEVBQVUsVUFBcUIsRUFDdkQsd0JBQWlELEVBQ2pELFdBQTBCLEVBQzFCLGVBQWdDLEVBQVMsUUFBa0IsRUFDM0QsY0FBOEI7UUFKcEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGVBQVUsR0FBVixVQUFVLENBQVc7UUFDdkQsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUF5QjtRQUNqRCxnQkFBVyxHQUFYLFdBQVcsQ0FBZTtRQUMxQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFBUyxhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQzNELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQS9FOUIsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFNdkMsd0JBQW1CLEdBQUMsRUFBRSxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFhO1lBQzNCLGFBQWE7WUFDYixVQUFVO1lBQ1YsV0FBVztZQUNYLFVBQVU7WUFDVixtQkFBbUI7U0FDcEIsQ0FBQztRQUNGLGNBQVMsR0FBRyxJQUFJLGNBQWMsQ0FBa0IsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRTFELGlCQUFZLEdBQVcsS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVEsRUFBRSxDQUFDO1FBQ3RCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsV0FBTSxHQUFXLENBQUMsQ0FBQztRQWlCbkIsa0JBQWEsR0FBUSxFQUFFLENBQUM7UUFDeEIsa0JBQWEsR0FBUSxFQUFFLENBQUM7UUFDeEIsTUFBTTtRQUNOLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsZUFBVSxHQUFHLFlBQVksQ0FBQztRQUcxQixnQkFBVyxHQUFHO1lBQ1osTUFBTSxFQUFFLENBQUMsU0FBUyxDQUFDO1NBQ3BCLENBQUM7UUFHRixNQUFNO1FBQ04sZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLG1CQUFjLEdBQUc7WUFDZixNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDO1NBQ2pHLENBQUM7UUFFRixpQkFBWSxHQUFTLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBS2hDLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRXZCLFNBQUksR0FBRyxJQUFJLENBQUM7UUFDWixTQUFJLEdBQXdCLFNBQVMsQ0FBQztRQUN0QyxjQUFTLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDbkgsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDM0IsVUFBSyxHQUFVLEVBQUUsQ0FBQztRQThEbEIsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUF0RDlCLHFGQUFxRjtRQUNyRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztJQUNuRCxDQUFDO0lBRUQsMENBQVEsR0FBUjtRQUFBLGlCQWlCQztRQWhCQyxJQUFJLENBQUMsS0FBSyxHQUFDO1lBQ1QsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSztTQUNqQixDQUFBO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBQyxTQUFTLENBQUMsQ0FBQTtRQUN0QyxJQUFJLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyx5REFBeUQ7UUFDNUYsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsdURBQXVEO1FBQ3pHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBLDJEQUEyRDtRQUNqSCxVQUFVLENBQUM7WUFDVCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBRVAsQ0FBQztJQUNELGlEQUFlLEdBQWYsVUFBZ0IsS0FBMkI7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRSxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUNELGlEQUFlLEdBQWYsVUFBZ0IsS0FBWTtRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFDRCw0Q0FBVSxHQUFWLFVBQVcsS0FBSztRQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLHVCQUF1QixDQUFDLENBQUE7UUFDM0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN2QyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7SUFDM0IsQ0FBQztJQUNELHlDQUFPLEdBQVAsVUFBUSxNQUFNO1FBQ1osSUFBRyxNQUFNLElBQUUsU0FBUyxFQUNwQjtZQUNFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtJQUNILENBQUM7SUFDRCxxREFBbUIsR0FBbkI7UUFDRSxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUN4QyxDQUFDO0lBQ0YsNkNBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxHQUFHLEdBQUc7WUFDUixFQUFFLEVBQUcsV0FBVztTQUNqQixDQUFDO1FBQ0YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNULElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQTtJQUNSLENBQUM7SUFHRiw4Q0FBWSxHQUFaO1FBQ0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUE7SUFDM0MsQ0FBQztJQUVBLCtDQUFhLEdBQWI7UUFBQSxpQkFNQztRQUxDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDN0QsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ2pELEtBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7WUFDM0QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUNELDZDQUFXLEdBQVgsVUFBWSxLQUFLO1FBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFDakMsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQzVDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQVMsSUFBSTtZQUNsQyxJQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDeEI7aUJBQUk7Z0JBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7YUFDekI7UUFDTCxDQUFDLENBQUMsQ0FBQTtRQUVFLFlBQVksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMzQyxZQUFZLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRTdCLDBFQUEwRTtRQUMxRSxvQkFBb0I7UUFDcEIsd0JBQXdCO1FBQ3hCLG1CQUFtQjtRQUNuQixnQ0FBZ0M7UUFDaEMsOEJBQThCO1FBQzlCLG9CQUFvQjtRQUNwQixPQUFPO1FBQ1Asc0JBQXNCO1FBQ3RCLGdEQUFnRDtRQUNoRCxpREFBaUQ7UUFDakQsZ0ZBQWdGO1FBQ2hGLE1BQU07UUFDTixZQUFZO1FBQ1oscUJBQXFCO1FBQ3JCLGdDQUFnQztRQUNoQyw4QkFBOEI7UUFDOUIscUJBQXFCO1FBQ3JCLE9BQU87UUFDUCxzQkFBc0I7UUFDdEIsa0RBQWtEO1FBQ2xELGlEQUFpRDtRQUNqRCxnRkFBZ0Y7UUFDaEYsTUFBTTtRQUNOLEtBQUs7UUFDTCxJQUFJO1FBQ0osMEJBQTBCO0lBQzFCLENBQUM7SUFFRixpREFBZSxHQUFmLFVBQWdCLE9BQU8sRUFBRSxPQUFPO1FBQy9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3hDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxPQUFPLEVBQUU7Z0JBQ2pDLE9BQU8sQ0FBQyxDQUFDO2FBQ1Q7U0FDRDtJQUNGLENBQUM7SUFFQSxpREFBZSxHQUFmOztRQUNFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1FBQ3ZFLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs7Z0JBQ3BCLEtBQW9CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFBLGdCQUFBLDRCQUFFO29CQUFqQyxJQUFJLE9BQU8sV0FBQTtvQkFDZCxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7d0JBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUN6Qyw2Q0FBNkM7d0JBQzdDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ3BDO2lCQUNGOzs7Ozs7Ozs7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyx5QkFBeUIsQ0FBQzs7Z0JBQzdDLEtBQW9CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFBLGdCQUFBLDRCQUFFO29CQUFqQyxJQUFJLE9BQU8sV0FBQTtvQkFDZCxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7d0JBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUN4Qyw0Q0FBNEM7d0JBQzVDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ3JDO2lCQUNGOzs7Ozs7Ozs7U0FDRjtJQUNILENBQUM7SUFFRCxpREFBZSxHQUFmLFVBQWdCLEVBQUU7UUFDaEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFBO0lBQ2hDLENBQUM7SUFFRCx1REFBcUIsR0FBckIsVUFBc0IsRUFBRTtRQUF4QixpQkFjQztRQWJDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUE7UUFFN0IsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ3JGLEtBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUNuQyxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzlELEtBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztZQUM3QixLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO1lBRzVDLEtBQUksQ0FBQyxLQUFLLEdBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQztZQUM5QixLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVuQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFHRCw2Q0FBVyxHQUFYO1FBQUEsaUJBNlNDO1FBM1NDLFNBQVM7UUFDVCxJQUFJLENBQUMsd0JBQXdCLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7O1lBRXRGLEtBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLElBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBRSwyQkFBMkIsSUFBSSxLQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLCtCQUErQixFQUFDO2dCQUNoSSxLQUFJLENBQUMsTUFBTSxHQUFFLGNBQWMsQ0FBQTthQUM1QjtpQkFBSyxJQUFHLEtBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsMkJBQTJCLElBQUksS0FBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBRSwrQkFBK0IsRUFBQztnQkFDdEksS0FBSSxDQUFDLE1BQU0sR0FBRSxlQUFlLENBQUE7YUFDN0I7aUJBQUk7Z0JBQ0gsS0FBSSxDQUFDLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQzthQUN0Qzs7Z0JBR0QsS0FBa0IsSUFBQSxLQUFBLGlCQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUEsZ0JBQUEsNEJBQUM7b0JBQTVCLElBQUksTUFBTSxXQUFBO29CQUNaLElBQUksR0FBRyxHQUFHLE1BQU0sQ0FBQztvQkFDakIsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO29CQUNiLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuQixHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDekIsS0FBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzFCOzs7Ozs7Ozs7WUFFRCx5RkFBeUY7WUFDekYsSUFBSSxNQUFNLEdBQUcsRUFBRSxDQUFDLFFBQVEsQ0FBQztnQkFDdkIsTUFBTSxFQUFFLFNBQVM7Z0JBQ2pCLElBQUksRUFBRTtvQkFDSixPQUFPLEVBQUcsS0FBSSxDQUFDLFNBQVM7b0JBQ3hCLHdCQUF3QjtvQkFDeEIsSUFBSSxFQUFHLEtBQUs7aUJBQ1o7Z0JBRUQsS0FBSyxFQUFDO29CQUNKLE9BQU8sRUFBQyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQztpQkFDdEY7Z0JBQ0gsTUFBTSxFQUFFO29CQUNGLElBQUksRUFBRSxJQUFJO2lCQUNYO2FBRUgsQ0FBQyxDQUFDO1FBRVAsQ0FBQyxDQUFDLENBQUM7UUFHQyxTQUFTO1FBQ1QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTs7WUFFN0UsS0FBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDckIsSUFBRyxLQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLDJCQUEyQixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsK0JBQStCLEVBQUU7Z0JBQ2pJLEtBQUksQ0FBQyxNQUFNLEdBQUUseUJBQXlCLENBQUE7YUFDdkM7aUJBQUssSUFBRyxLQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLDJCQUEyQixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsK0JBQStCLEVBQUU7Z0JBQ3ZJLEtBQUksQ0FBQyxNQUFNLEdBQUUsOEJBQThCLENBQUE7YUFDNUM7aUJBQUk7Z0JBQ0gsS0FBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQzthQUNoQztZQUVELElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDbEMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO2dCQUM3QixPQUFPLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUQsQ0FBQyxDQUFDLENBQUM7WUFDSDs7Ozs7O2NBTUU7WUFFRjs7Ozs7Ozs7O2NBU0U7WUFFRixJQUFJLE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQzs7Z0JBQ2hCLEtBQWtCLElBQUEsa0JBQUEsaUJBQUEsYUFBYSxDQUFBLDRDQUFBLHVFQUFDO29CQUE1QixJQUFJLE1BQU0sMEJBQUE7b0JBQ1osSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDO29CQUNqQixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7b0JBQ2IsdUJBQXVCO29CQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDdkIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUM3QixHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ25CLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNyQixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDM0I7Ozs7Ozs7OztZQUNELElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEdBQUc7Z0JBQzVDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQzNCLE9BQU8sSUFBSSxDQUFBO2lCQUNaO1lBQ0gsQ0FBQyxDQUFDLENBQUE7WUFFRixlQUFlO1lBQ2YsSUFBSSxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxVQUFDLElBQUksRUFBRSxDQUFDLEVBQUUsR0FBRztnQkFDNUMsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDM0IsT0FBTyxJQUFJLENBQUE7aUJBQ1o7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUVGLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDOztnQkFDMUIsS0FBa0IsSUFBQSxrQkFBQSxpQkFBQSxhQUFhLENBQUEsNENBQUEsdUVBQUM7b0JBQTVCLElBQUksTUFBTSwwQkFBQTs7d0JBQ2QsS0FBcUIsSUFBQSxpQkFBQSxpQkFBQSxZQUFZLENBQUEsMENBQUEsb0VBQUM7NEJBQTlCLElBQUksU0FBUyx5QkFBQTs0QkFDZixJQUFHLFNBQVMsSUFBRSxPQUFPLEVBQUM7Z0NBQ3RCLElBQUksWUFBWSxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUN0QyxJQUFHLFNBQVMsSUFBRSxNQUFNLENBQUMsS0FBSyxFQUFDO29DQUN4QixJQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFDO3dDQUN0QixZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQzt3Q0FDakMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO3FDQUNyQztpQ0FDRjtxQ0FDRztvQ0FDRixZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO29DQUNyQixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7aUNBQ3JDOzZCQUVGO3lCQUNGOzs7Ozs7Ozs7aUJBRUE7Ozs7Ozs7OztZQUdELElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQzs7Z0JBQ3ZCLEtBQXNCLElBQUEsaUJBQUEsaUJBQUEsWUFBWSxDQUFBLDBDQUFBLG9FQUFDO29CQUEvQixJQUFJLFVBQVUseUJBQUE7b0JBQ2hCLElBQUksWUFBWSxHQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7O3dCQUMvQixLQUFpQixJQUFBLHFCQUFBLGlCQUFBLGdCQUFnQixDQUFBLGtEQUFBLGdGQUFDOzRCQUE5QixJQUFJLEtBQUssNkJBQUE7NEJBRVQsSUFBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUcsVUFBVSxFQUFDO2dDQUN4QixZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOzZCQUM1Qjt5QkFFSjs7Ozs7Ozs7O29CQUNELGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7aUJBQ2pDOzs7Ozs7Ozs7WUFFRCxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7O2dCQUNyQixpQ0FBaUM7Z0JBQ2pDLEtBQW1CLElBQUEsa0JBQUEsaUJBQUEsYUFBYSxDQUFBLDRDQUFBO29CQUE1QixJQUFJLE9BQU8sMEJBQUE7b0JBQ2IsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFBQTs7Ozs7Ozs7O1lBRzVCLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDOztnQkFDMUIsS0FBcUIsSUFBQSxpQkFBQSxpQkFBQSxZQUFZLENBQUEsMENBQUEsb0VBQ2pDO29CQURJLElBQUksU0FBUyx5QkFBQTtvQkFHZixJQUFHLFNBQVMsS0FBSyxPQUFPLEVBQUM7d0JBQ3ZCLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztxQkFDbEM7aUJBRUY7Ozs7Ozs7OztZQUtELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ3ZCLE1BQU0sRUFBRSxTQUFTO2dCQUVqQixJQUFJLEVBQUU7b0JBRUYsT0FBTyxFQUFFLFdBQVc7b0JBRXBCLElBQUksRUFBRSxLQUFLO2lCQUNkO2dCQUNELEtBQUssRUFBQztvQkFDSixPQUFPLEVBQUMsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUM7aUJBQ3RGO2dCQUNELEdBQUcsRUFBRTtvQkFDSCxLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxJQUFJLEVBQUU7b0JBQ0YsQ0FBQyxFQUFFO3dCQUNDLElBQUksRUFBRSxVQUFVO3dCQUNqQixVQUFVLEVBQUUsZ0JBQWdCO3FCQUM5QjtpQkFDSjthQUNGLENBQUMsQ0FBQztRQU1MLENBQUMsQ0FBQyxDQUFDO1FBSUwsU0FBUztRQUNULElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTs7WUFFdEYsS0FBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFFckIsSUFBRyxLQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLDJCQUEyQixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsK0JBQStCLEVBQUM7Z0JBQ2hJLEtBQUksQ0FBQyxNQUFNLEdBQUUseUJBQXlCLENBQUE7YUFDdkM7aUJBQUssSUFBRyxLQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLDJCQUEyQixJQUFJLEtBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsK0JBQStCLEVBQUM7Z0JBQ3RJLEtBQUksQ0FBQyxNQUFNLEdBQUUsc0JBQXNCLENBQUE7YUFDcEM7aUJBQUk7Z0JBQ0gsS0FBSSxDQUFDLE1BQU0sR0FBRyxvQkFBb0IsQ0FBQzthQUNwQztZQUNELElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDbEMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO2dCQUM3QixPQUFPLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDNUYsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDOztnQkFFdkIsS0FBa0IsSUFBQSxrQkFBQSxpQkFBQSxhQUFhLENBQUEsNENBQUEsdUVBQUM7b0JBQTVCLElBQUksTUFBTSwwQkFBQTtvQkFDWixJQUFJLEdBQUcsR0FBRyxNQUFNLENBQUM7b0JBQ2pCLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQztvQkFDYix1QkFBdUI7b0JBQ3ZCLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN2QixHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ25CLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNwQixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDM0I7Ozs7Ozs7OztZQUNELElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEdBQUc7Z0JBQzVDLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQzNCLE9BQU8sSUFBSSxDQUFBO2lCQUNaO1lBQ0gsQ0FBQyxDQUFDLENBQUE7WUFHRixJQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQzs7Z0JBQzFCLEtBQWtCLElBQUEsa0JBQUEsaUJBQUEsYUFBYSxDQUFBLDRDQUFBLHVFQUFDO29CQUE1QixJQUFJLE1BQU0sMEJBQUE7O3dCQUNkLEtBQXFCLElBQUEsaUJBQUEsaUJBQUEsWUFBWSxDQUFBLDBDQUFBLG9FQUFDOzRCQUE5QixJQUFJLFNBQVMseUJBQUE7NEJBQ2YsSUFBRyxTQUFTLElBQUUsT0FBTyxFQUFDO2dDQUN0QixJQUFJLGFBQWEsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDakMsSUFBRyxTQUFTLElBQUUsTUFBTSxDQUFDLEtBQUssRUFBQztvQ0FDeEIsSUFBRyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUUsRUFBQzt3Q0FDckIsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7d0NBQ2pDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztxQ0FDdEM7aUNBQ0Y7cUNBQ0c7b0NBQ0YsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztvQ0FDdEIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2lDQUN0Qzs2QkFFRjt5QkFDRjs7Ozs7Ozs7O2lCQUVBOzs7Ozs7Ozs7WUFHRCxJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7O2dCQUN2QixLQUF1QixJQUFBLGlCQUFBLGlCQUFBLFlBQVksQ0FBQSwwQ0FBQSxvRUFBQztvQkFBaEMsSUFBSSxXQUFXLHlCQUFBO29CQUNqQixJQUFJLFlBQVksR0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDOzt3QkFDaEMsS0FBaUIsSUFBQSxxQkFBQSxpQkFBQSxnQkFBZ0IsQ0FBQSxrREFBQSxnRkFBQzs0QkFBOUIsSUFBSSxLQUFLLDZCQUFBOzRCQUVULElBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFHLFdBQVcsRUFBQztnQ0FDekIsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs2QkFDNUI7eUJBRUo7Ozs7Ozs7OztvQkFDRCxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO2lCQUNqQzs7Ozs7Ozs7O1lBRUQsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDOztnQkFDckIsaUNBQWlDO2dCQUNqQyxLQUFtQixJQUFBLGtCQUFBLGlCQUFBLGFBQWEsQ0FBQSw0Q0FBQTtvQkFBNUIsSUFBSSxPQUFPLDBCQUFBO29CQUNmLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQUE7Ozs7Ozs7OztZQUcxQixJQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQzs7Z0JBQzFCLEtBQXFCLElBQUEsaUJBQUEsaUJBQUEsWUFBWSxDQUFBLDBDQUFBLG9FQUNqQztvQkFESSxJQUFJLFNBQVMseUJBQUE7b0JBR2YsSUFBRyxTQUFTLEtBQUssT0FBTyxFQUFDO3dCQUN2QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7cUJBQ2xDO2lCQUVGOzs7Ozs7Ozs7WUFFRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNyQixNQUFNLEVBQUUsU0FBUztnQkFDakIsSUFBSSxFQUFFO29CQUVGLE9BQU8sRUFBRSxXQUFXO29CQUVwQixJQUFJLEVBQUUsS0FBSztpQkFDZDtnQkFDRCxHQUFHLEVBQUU7b0JBQ0gsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7Z0JBQ0QsS0FBSyxFQUFDO29CQUNKLE9BQU8sRUFBQyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQztpQkFDdEY7Z0JBQ0QsSUFBSSxFQUFFO29CQUNGLENBQUMsRUFBRTt3QkFDQyxJQUFJLEVBQUUsVUFBVTt3QkFDakIsVUFBVSxFQUFFLGdCQUFnQjtxQkFDOUI7aUJBQ0o7YUFDSixDQUFDLENBQUM7UUFHUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCw4Q0FBWSxHQUFaO1FBQ0Usd0NBQXdDO1FBQ3hDLHlEQUF5RDtRQUN6RCxJQUFJLEdBQUcsR0FBRztZQUNSLEdBQUcsRUFBRyxvQ0FBb0M7WUFDMUMsRUFBRSxFQUFHLGNBQWM7U0FDcEIsQ0FBQTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLG9DQUFvQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxHQUFHO1FBQzFFLENBQUMsRUFBRSxVQUFBLEdBQUc7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFBLENBQUMsd0JBQXdCO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUFBLENBQUM7SUFHRiwwQ0FBUSxHQUFSLFVBQVMsR0FBRyxFQUFFLFdBQVc7UUFDdkIsSUFBSSxHQUFHLEdBQUc7WUFDUixHQUFHLEVBQUcsb0NBQW9DO1lBQzFDLEVBQUUsRUFBRyxjQUFjO1NBQ3BCLENBQUE7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLG9DQUFvQyxDQUFDLENBQUMsQ0FBQztRQUM3RCw2RkFBNkY7UUFDN0YsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQzVFLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEdBQUcsV0FBVyxDQUFDLFdBQVcsQ0FBQztJQUNuRixDQUFDOztnQkF0a0JGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyw2M0tBQWdEO29CQUVoRCxVQUFVLEVBQUUsY0FBYzs7aUJBQzNCOzs7O2dCQTdCUSxNQUFNO2dCQUpOLFNBQVM7Z0JBS1QsdUJBQXVCO2dCQURmLGNBQWM7Z0JBRHRCLGVBQWU7Z0JBRmhCLFFBQVE7Z0JBVVAsY0FBYzs7OzBCQTJCcEIsTUFBTTs0QkFpQ04sU0FBUyxTQUFDLFlBQVk7O0lBNmhCekIsOEJBQUM7Q0FBQSxBQXhrQkQsSUF3a0JDO1NBaGtCWSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBWaWV3RW5jYXBzdWxhdGlvbiwgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IE1hdERpYWxvZywgTWF0U2lkZW5hdiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHtMb2NhdGlvbn0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuXHJcbmltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2hhcmVkL3NuYWNrYmFyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSB9IGZyb20gJy4uL3JlcG9ydC1tYW5hZ2VtZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBtYW5hZ2VBdkRldGFpbFRhYmxlQ29uZmlnIH0gZnJvbSAnLi4vLi4vdGFibGVfY29uZmlnJztcclxuaW1wb3J0IHsgQXZtSW1wb3J0Rm9ybUNvbXBvbmVudCB9IGZyb20gJy4uL2ltcG9ydC1mb3JtL2ltcG9ydC1mb3JtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IGZ1c2VBbmltYXRpb25zIH0gZnJvbSAnLi4vLi4vQGZ1c2UvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgKiBhcyBjMyBmcm9tICdjMyc7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgR3JvdXBEZXNjcmlwdG9yLCBEYXRhUmVzdWx0LCBwcm9jZXNzLFN0YXRlIH0gZnJvbSAnQHByb2dyZXNzL2tlbmRvLWRhdGEtcXVlcnknO1xyXG5pbXBvcnQgeyBHcmlkRGF0YVJlc3VsdCwgUGFnZUNoYW5nZUV2ZW50LFNlbGVjdEFsbENoZWNrYm94U3RhdGUsRGF0YVN0YXRlQ2hhbmdlRXZlbnQgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tYW5ndWxhci1ncmlkJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgbWFuYWdlQXZzRGV0YWlsIHtcclxuICBzb2RuYW1lOiBzdHJpbmc7XHJcbiAgZW50aXRsZW1lbnQ6IHN0cmluZztcclxuICB1c2VybmFtZTogc3RyaW5nO1xyXG4gIGZpcnN0bmFtZTogc3RyaW5nO1xyXG4gIGxhc3RuYW1lOiBzdHJpbmc7XHJcbiAgcmVzcG9uc2liaWx0eW5hbWU6IHN0cmluZzsgXHJcbn1cclxuXHJcblxyXG52YXIgTU9OVEggPSB7IEpBTjogMCwgRkVCOiAxLCBNQVI6IDIsIEFQUjogMywgTUFZOiA0LCBKVU46IDUsIEpVTDogNiwgQVVHOiA3LCBTRVA6IDgsIE9DVDogOSwgTk9WOiAxMCwgREVDOiAxMSB9O1xyXG52YXIgTU9OVEgyID0geyBqYW46IDAsIGZlYjogMSwgbWFyOiAyLCBhcHI6IDMsIG1heTogNCwganVuOiA1LCBqdWw6IDYsIGF1ZzogNywgc2VwOiA4LCBvY3Q6IDksIG5vdjogMTAsIGRlYzogMTEgfTtcclxuLy8gZGVjbGFyZSB2YXIgYzM6IGFueTtcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbWFuYWdlLWF2LWRldGFpbCcsXHJcbiAgdGVtcGxhdGVVcmw6ICcuL21hbmFnZS1hdi1kZXRhaWwuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL21hbmFnZS1hdi1kZXRhaWwuY29tcG9uZW50LnNjc3MnXSxcclxuICBhbmltYXRpb25zOiBmdXNlQW5pbWF0aW9uc1xyXG59KVxyXG5cclxuXHJcbmV4cG9ydCBjbGFzcyBNYW5hZ2VBdkRldGFpbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gIEBPdXRwdXQoKSBteUV2ZW50ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIG1hbmFnZUF2c2xpc3Q6IGFueTtcclxuICBkYXRhU291cmNlMjogYW55O1xyXG4gIGN1c3RvbWRhdGFzb3VyY2U6IG9iamVjdFtdO1xyXG4gIGRlZmF1bHRkYXRhc291cmNlSWQ6IHN0cmluZztcclxuICBkaXNwbGF5TmFtOnN0cmluZztcclxuICBzZWxlY3RlZFRhYmxlSGVhZGVyPVtdO1xyXG4gIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gW1xyXG4gICAgJ2VudGl0bGVtZW50JyxcclxuICAgICd1c2VybmFtZScsXHJcbiAgICAnZmlyc3RuYW1lJyxcclxuICAgICdsYXN0bmFtZScsXHJcbiAgICAncmVzcG9uc2liaWx0eW5hbWUnXHJcbiAgXTtcclxuICBzZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8bWFuYWdlQXZzRGV0YWlsPih0cnVlLCBbXSk7XHJcbiAgZGlhbG9nUmVmOiBhbnk7XHJcbiAgbmF2X3Bvc2l0aW9uOiBzdHJpbmcgPSAnZW5kJztcclxuICBxdWVyeVBhcmFtczogYW55ID0ge307XHJcbiAgbGltaXQ6IG51bWJlciA9IDEwO1xyXG4gIG9mZnNldDogbnVtYmVyID0gMDtcclxuICBsZW5ndGg6IG51bWJlcjtcclxuICB0YWJsZUNvbmZpZzogYW55O1xyXG5cclxuIFxyXG4gIGNoYXJ0RGF0YTogYW55O1xyXG4gIGNoYXJ0RGF0YTI6IGFueTtcclxuICBjaGFydERhdGEzOiBhbnk7XHJcblxyXG4gIGNoYXJhY3RlcnM6IGFueTtcclxuICBzZXR0aW5nczogYW55O1xyXG4gIHRpdGxlMTogc3RyaW5nO1xyXG4gIHRpdGxlMjogc3RyaW5nO1xyXG4gIHRpdGxlMzogc3RyaW5nO1xyXG4gIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuXHJcblxyXG4gIHJvbGVDaGFydERhdGE6IGFueSA9IFtdO1xyXG4gIHVzZXJDaGFydERhdGE6IGFueSA9IFtdO1xyXG4gIC8vIEJhclxyXG4gIHNob3dYQXhpcyA9IHRydWU7XHJcbiAgc2hvd1lBeGlzID0gdHJ1ZTtcclxuICBncmFkaWVudCA9IGZhbHNlO1xyXG4gIHNob3dMZWdlbmQgPSB0cnVlO1xyXG4gIHNob3dYQXhpc0xhYmVsID0gdHJ1ZTtcclxuICBzaG93WUF4aXNMYWJlbCA9IHRydWU7XHJcbiAgeUF4aXNMYWJlbCA9ICdQb3B1bGF0aW9uJztcclxuICBwYXJtc09iajphbnk7XHJcblxyXG4gIGNvbG9yU2NoZW1lID0ge1xyXG4gICAgZG9tYWluOiBbJyMyOGEzZGQnXVxyXG4gIH07XHJcblxyXG4gIHVybFBhcmFtcyA6IGFueTtcclxuICAvLyBwaWVcclxuICBzaG93TGFiZWxzID0gdHJ1ZTtcclxuICBzaG93UGllTGVnZW5kID0gZmFsc2U7XHJcbiAgZXhwbG9kZVNsaWNlcyA9IGZhbHNlO1xyXG4gIGRvdWdobnV0ID0gZmFsc2U7XHJcbiAgY29sb3JQaWVTY2hlbWUgPSB7XHJcbiAgICBkb21haW46IFsnIzNhM2Y1MScsICcjMjNiN2U1JywgJyNmMDUwNTAnLCAnIzI3YzI0YycsICcjZTYwMDUwJywgJyMyOTgwYjknLCAnIzcyNjZiYScsICcjMmFjY2RiJ11cclxuICB9O1xyXG4gIGNvbG9yQ2hhcnQxIDogYW55O1xyXG4gIHBpZUNoYXJ0VmlldyA6IGFueSA9IFszMDAsIDI1MF07XHJcbiAgcGllQ2hhcnRMYWJlbDogeyB0aXRsZTogYW55OyBub2RhdGE6IGFueTsgfTtcclxuICBjaGFydDJEYXRhOiBhbnlbXTtcclxuICBcclxuICBrZW5kb0dyaWREYXRhIDogR3JpZERhdGFSZXN1bHQ7XHJcbiAgZW5hYmxlRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIF9kYXRhOiBhbnlbXTtcclxuICBwdWJsaWMgaW5mbyA9IHRydWU7XHJcbiAgcHVibGljIHR5cGU6IFwibnVtZXJpY1wiIHwgXCJpbnB1dFwiID0gXCJudW1lcmljXCI7XHJcbiAgcHVibGljIHBhZ2VTaXplcyA9IFt7IHRleHQ6IDEwLCB2YWx1ZTogMTAgfSwgeyB0ZXh0OiAyNSwgdmFsdWU6IDI1IH0sIHsgdGV4dDogNTAsIHZhbHVlOiA1MCB9LCB7IHRleHQ6IDEwMCwgdmFsdWU6IDEwMCB9XTtcclxuICBwdWJsaWMgcHJldmlvdXNOZXh0ID0gdHJ1ZTtcclxuICBzdGF0ZTogU3RhdGUgPSB7fTtcclxuXHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSBfbWF0RGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIF9yZXBvcnRNYW5hZ2VtZW50U2VydmljZTogUmVwb3J0TWFuYWdlbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGFjdGl2ZVJvdXRlOkFjdGl2YXRlZFJvdXRlLFxyXG4gICAgcHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSxwcml2YXRlIGxvY2F0aW9uOiBMb2NhdGlvbixcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2UgOk1lc3NhZ2VTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcImF2IGRldGFpbCBVUkwgUGFybXMgKioqKioqKioqKioqKipcIix0aGlzLmFjdGl2ZVJvdXRlLnNuYXBzaG90LnBhcmFtcylcclxuICAgIHRoaXMucGFybXNPYmogPSB0aGlzLmFjdGl2ZVJvdXRlLnNuYXBzaG90LnBhcmFtcztcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5zdGF0ZT17XHJcbiAgICAgIHNraXA6IHRoaXMub2Zmc2V0LFxyXG4gICAgICB0YWtlOiB0aGlzLmxpbWl0XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnJvdXRlci51cmwsXCI+Pj5USElTXCIpXHJcbiAgICBsZXQgdXJsU3RyaW5nID0gZGVjb2RlVVJJQ29tcG9uZW50KHRoaXMucm91dGVyLnVybCk7XHJcbiAgICB0aGlzLnVybFBhcmFtcyA9IHVybFN0cmluZy5zcGxpdCgnLycpO1xyXG4gICAgdGhpcy5kaXNwbGF5TmFtPXRoaXMudXJsUGFyYW1zWzVdOyAvL3RoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZVsnZGlzcGxheU5hbWUnXTtcclxuICAgIHRoaXMuZ2V0ZGF0YXNvdXJjZSgpO1xyXG4gICAgdGhpcy5sb2FkVGFibGVDb2x1bW4oKTtcclxuICAgIHRoaXMucXVlcnlQYXJhbXMuZGF0YXNvdXJjZTIgPSB0aGlzLnVybFBhcmFtc1szXTsgLy90aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUuZGF0YXNvdXJjZUlkO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtcy5jb2xsZWN0aW9uRGV0YWlsID0gdGhpcy51cmxQYXJhbXNbNF07Ly90aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUuY29sbGVjdGlvbkRldGFpbDtcclxuICAgIHNldFRpbWVvdXQoKCk9PnsgICAgLy88PDwtLS0gICAgdXNpbmcgKCk9PiBzeW50YXhcclxuICAgICAgdGhpcy5vbkxvYWRDaGFydCgpO1xyXG4gfSwgNTAwKTtcclxuICAgIFxyXG4gIH1cclxuICBkYXRhU3RhdGVDaGFuZ2Uoc3RhdGU6IERhdGFTdGF0ZUNoYW5nZUV2ZW50KTogdm9pZCB7XHJcbiAgICB0aGlzLnN0YXRlPSBzdGF0ZTtcclxuICAgIHRoaXMuYXBwbHlUYWJsZVN0YXRlKHRoaXMuc3RhdGUpO1xyXG4gIH1cclxuICBhcHBseVRhYmxlU3RhdGUoc3RhdGU6IFN0YXRlKTogdm9pZCB7XHJcbiAgICB0aGlzLmtlbmRvR3JpZERhdGEgPSBwcm9jZXNzKHRoaXMuX2RhdGEsIHN0YXRlKTtcclxuICB9XHJcbiAgY2hhbmdlUGFnZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiPj4+IEVWRU5UIENoYW5nZSBQYWdlXCIpXHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wib2Zmc2V0XCJdID0gZXZlbnQuc2tpcDtcclxuICAgIHRoaXMucXVlcnlQYXJhbXNbXCJsaW1pdFwiXSA9IHRoaXMubGltaXQ7XHJcbiAgICB0aGlzLm9mZnNldCA9IGV2ZW50LnNraXA7XHJcbiAgfVxyXG4gIG9uQ2xpY2soYWN0aW9uKXtcclxuICAgIGlmKGFjdGlvbj09J3JlZnJlc2gnKVxyXG4gICAge1xyXG4gICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGVuYWJsZUZpbHRlck9wdGlvbnMoKSB7XHJcbiAgICB0aGlzLmVuYWJsZUZpbHRlciA9ICF0aGlzLmVuYWJsZUZpbHRlcjsgICBcclxuICAgfVxyXG4gIGJhY2tDbGlja2VkKCkge1xyXG4gICAgdGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcbiAgICBsZXQgb2JqID0ge1xyXG4gICAgICBpZCA6IFwibWFuYWdlQXZtXCJcclxuICAgIH07XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XHJcbiAgICAgIHNlbGYubWVzc2FnZVNlcnZpY2Uuc2VuZFJvdXRpbmcob2JqKTtcclxuICAgIH0sMTAwKVxyXG4gIH1cclxuICBwYW5lbE9wZW5TdGF0ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuXHR0YWJsZVNldHRpbmcoKSB7XHJcblx0XHR0aGlzLnBhbmVsT3BlblN0YXRlID0gIXRoaXMucGFuZWxPcGVuU3RhdGVcclxuXHR9XHJcblxyXG4gIGdldGRhdGFzb3VyY2UoKSB7XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRBbGxEYXRhc291cmNlcygpLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICB0aGlzLmN1c3RvbWRhdGFzb3VyY2UgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXM7XHJcbiAgICAgIHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlc1swXS5faWQ7XHJcbiAgICAgIHRoaXMub25Mb2FkbWFuYWdlQXZtRGV0YWlsKHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCk7XHJcbiAgICB9KVxyXG4gIH1cclxuICB1cGRhdGVUYWJsZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiLS0tLWV2ZW50XCIpXHJcblx0XHRsZXQgc2VsZWN0ZWRIZWFkZXIgPSB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXI7XHJcbiAgICB0aGlzLnRhYmxlQ29uZmlnLmZvckVhY2goZnVuY3Rpb24oaXRlbSl7XHJcbiAgICAgICAgaWYoc2VsZWN0ZWRIZWFkZXIuaW5kZXhPZihpdGVtLnZhbHVlKSA+PSAwKSB7XHJcbiAgICAgICAgICAgIGl0ZW0uaXNhY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICBpdGVtLmlzYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfSlcclxuXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ21hbmFnZUF2c0RldGFpbCcpO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdtYW5hZ2VBdnNEZXRhaWwnLCBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlQ29uZmlnKSk7XHJcbiAgICAgICAgdGhpcy5sb2FkVGFibGVDb2x1bW4oKTsgICAgXHJcblxyXG5cdFx0Ly8gdmFyIGluZGV4ID0gdGhpcy50YWJsZU5hbWVzZWFyY2goZXZlbnQuc291cmNlLnZhbHVlLCB0aGlzLnRhYmxlQ29uZmlnKTtcclxuXHRcdC8vIGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHQvLyBcdGlmIChldmVudC5jaGVja2VkKSB7XHJcblx0XHQvLyBcdFx0bGV0IGFjdGl2ZSA9IHtcclxuXHRcdC8vIFx0XHRcdHZhbHVlOiBldmVudC5zb3VyY2UudmFsdWUsXHJcblx0XHQvLyBcdFx0XHRuYW1lOiBldmVudC5zb3VyY2UubmFtZSxcclxuXHRcdC8vIFx0XHRcdGlzYWN0aXZlOiB0cnVlXHJcblx0XHQvLyBcdFx0fTtcclxuXHRcdC8vIFx0XHRpZiAoaW5kZXggPj0gMCkge1xyXG5cdFx0Ly8gXHRcdFx0dGhpcy50YWJsZUNvbmZpZy5zcGxpY2UoaW5kZXgsIDEsIGFjdGl2ZSk7XHJcblx0XHQvLyBcdFx0XHRsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcIm1hbmFnZUF2c0RldGFpbFwiKTtcclxuXHRcdC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwibWFuYWdlQXZzRGV0YWlsXCIsIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVDb25maWcpKTtcclxuXHRcdC8vIFx0XHR9XHJcblx0XHQvLyBcdH0gZWxzZSB7XHJcblx0XHQvLyBcdFx0bGV0IGluYWN0aXZlID0ge1xyXG5cdFx0Ly8gXHRcdFx0dmFsdWU6IGV2ZW50LnNvdXJjZS52YWx1ZSxcclxuXHRcdC8vIFx0XHRcdG5hbWU6IGV2ZW50LnNvdXJjZS5uYW1lLFxyXG5cdFx0Ly8gXHRcdFx0aXNhY3RpdmU6IGZhbHNlXHJcblx0XHQvLyBcdFx0fTtcclxuXHRcdC8vIFx0XHRpZiAoaW5kZXggPj0gMCkge1xyXG5cdFx0Ly8gXHRcdFx0dGhpcy50YWJsZUNvbmZpZy5zcGxpY2UoaW5kZXgsIDEsIGluYWN0aXZlKTtcclxuXHRcdC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwibWFuYWdlQXZzRGV0YWlsXCIpO1xyXG5cdFx0Ly8gXHRcdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJtYW5hZ2VBdnNEZXRhaWxcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy50YWJsZUNvbmZpZykpO1xyXG5cdFx0Ly8gXHRcdH1cclxuXHRcdC8vIFx0fVxyXG5cdFx0Ly8gfVxyXG5cdFx0Ly8gdGhpcy5sb2FkVGFibGVDb2x1bW4oKTtcclxuICB9XHJcbiAgXHJcblx0dGFibGVOYW1lc2VhcmNoKG5hbWVLZXksIG15QXJyYXkpIHtcclxuXHRcdGZvciAodmFyIGkgPSAwOyBpIDwgbXlBcnJheS5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRpZiAobXlBcnJheVtpXS52YWx1ZSA9PT0gbmFtZUtleSkge1xyXG5cdFx0XHRcdHJldHVybiBpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICBsb2FkVGFibGVDb2x1bW4oKSB7XHJcbiAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSBbXTtcclxuICAgIHRoaXMudGFibGVDb25maWcgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdtYW5hZ2VBdnNEZXRhaWwnKSk7XHJcbiAgICBpZiAodGhpcy50YWJsZUNvbmZpZykge1xyXG4gICAgICBmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuICAgICAgICBpZiAoY29sdW1ucy5pc2FjdGl2ZSkge1xyXG5cdFx0XHRcdFx0dGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcbiAgICAgICAgIC8vIHRoaXMuZGlzcGxheWVkQ29sdW1ucy5wdXNoKGNvbHVtbnMudmFsdWUpO1xyXG4gICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMucHVzaChjb2x1bW5zKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudGFibGVDb25maWcgPSBtYW5hZ2VBdkRldGFpbFRhYmxlQ29uZmlnO1xyXG4gICAgICBmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuICAgICAgICBpZiAoY29sdW1ucy5pc2FjdGl2ZSkge1xyXG5cdFx0XHRcdFx0dGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcbiAgICAgICAgICAvL3RoaXMuZGlzcGxheWVkQ29sdW1ucy5wdXNoKGNvbHVtbnMudmFsdWUpO1xyXG4gICAgICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXREYXRhc291cmNlSWQoaWQpIHtcclxuICAgIHRoaXMub25Mb2FkbWFuYWdlQXZtRGV0YWlsKGlkKVxyXG4gIH1cclxuXHJcbiAgb25Mb2FkbWFuYWdlQXZtRGV0YWlsKGlkKSB7XHJcbiAgICB0aGlzLmRlZmF1bHRkYXRhc291cmNlSWQgPSBpZFxyXG4gICAgXHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRhbGxtYW5hZ2VBdkRldGFpbCh0aGlzLnF1ZXJ5UGFyYW1zKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICB0aGlzLm1hbmFnZUF2c2xpc3QgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UyID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLm1hbmFnZUF2c2xpc3QpO1xyXG4gICAgICB0aGlzLmxlbmd0aCA9IHJlc3BvbnNlLnRvdGFsO1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UyLnBhZ2luYXRvciA9IHRoaXMucGFnaW5hdG9yO1xyXG4gICBcclxuXHJcbiAgICAgIHRoaXMuX2RhdGE9dGhpcy5tYW5hZ2VBdnNsaXN0O1xyXG4gICAgICB0aGlzLmFwcGx5VGFibGVTdGF0ZSh0aGlzLnN0YXRlKTtcclxuXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG5cclxuICBvbkxvYWRDaGFydCgpIHtcclxuICAgIFxyXG4gICAgLy9DaGFydCAxXHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRDYXRlZ29yeVBlcmNlbnRhZ2UodGhpcy5xdWVyeVBhcmFtcykuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgXHJcbiAgICAgIHRoaXMuY2hhcnREYXRhID0gW107XHJcbiAgICAgIGlmKHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsaWVySW52b2ljZWRldGFpbHMnIHx8IHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsaWVySW52b2ljZWRldGFpbHMtZWJzJyl7XHJcbiAgICAgICAgdGhpcy50aXRsZTEgPSdJbnZvaWNlIFR5cGUnXHJcbiAgICAgIH1lbHNlIGlmKHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsZWlyUGF5bWVudGRldGFpbHMnIHx8IHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsZWlyUGF5bWVudGRldGFpbHMtZWJzJyl7XHJcbiAgICAgICAgdGhpcy50aXRsZTEgPSdUb3AgNSBWZW5kb3JzJ1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICB0aGlzLnRpdGxlMSA9IFwiSm91cm5hbHMgYnkgQ2F0ZWdvcnlcIjtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgXHJcbiAgICAgIGZvcihsZXQgcmVzdWx0IG9mIHJlc3BvbnNlLmRhdGEpe1xyXG4gICAgICAgIHZhciBvYmogPSByZXN1bHQ7XHJcbiAgICAgICAgdmFyIGlkcyA9IFtdO1xyXG4gICAgICAgIGlkcy5wdXNoKG9iai5uYW1lKTtcclxuICAgICAgICBpZHMucHVzaChvYmoucGVyY2VudGFnZSk7XHJcbiAgICAgICAgdGhpcy5jaGFydERhdGEucHVzaChpZHMpO1xyXG4gICAgICB9XHJcbiAgICAgICBcclxuICAgICAgLy90aGlzLmNoYXJ0RGF0YSA9IFtbJ0FkanVzdG1lbnRzc3MnLCAyMF0sWydBY2NydWFsJywgMTBdLFsnUmVjbGFzcycsIDQ1XSxbJ090aGVyJywgMjVdXTtcclxuICAgICAgbGV0IGNoYXJ0MSA9IGMzLmdlbmVyYXRlKHtcclxuICAgICAgICBiaW5kdG86ICcjY2hhcnQxJyxcdFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIGNvbHVtbnMgOiB0aGlzLmNoYXJ0RGF0YSxcclxuICAgICAgICAgIC8vanNvbiA6IHRoaXMuY2hhcnREYXRhLFxyXG4gICAgICAgICAgdHlwZSA6ICdwaWUnLFxyXG4gICAgICAgICB9LFxyXG4gICAgICAgXHJcbiAgICAgICAgIGNvbG9yOntcclxuICAgICAgICAgICBwYXR0ZXJuOlsnIzM5NkFCMScsICcjREE3QzMwJywgJyMzRTk2NTEnLCAnI0NDMjUyOScsICcjNTM1MTU0JywgJyM2QjRDOUEnLCAnIzk0OEIzRCddXHJcbiAgICAgICAgIH0sXHJcbiAgICAgICBsZWdlbmQ6IHtcclxuICAgICAgICAgICAgIHNob3c6IHRydWVcclxuICAgICAgICAgICB9LFxyXG4gIFxyXG4gICAgICAgIH0pO1xyXG4gICAgICBcclxuICAgIH0pO1xyXG4gICAgXHJcblxyXG4gICAgICAgIC8vQ2hhcnQgMlxyXG4gICAgICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldFRvcEFtb3VudCh0aGlzLnF1ZXJ5UGFyYW1zKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBcclxuICAgICAgICAgIHRoaXMuY2hhcnREYXRhMiA9IFtdO1xyXG4gICAgICAgICAgaWYodGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxpZXJJbnZvaWNlZGV0YWlscycgfHwgdGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxpZXJJbnZvaWNlZGV0YWlscy1lYnMnICl7XHJcbiAgICAgICAgICAgIHRoaXMudGl0bGUyID0nVG9wIDUgdXNlcnMgd2l0aCBhbW91bnQnXHJcbiAgICAgICAgICB9ZWxzZSBpZih0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGVpclBheW1lbnRkZXRhaWxzJyB8fCB0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGVpclBheW1lbnRkZXRhaWxzLWVicycgKXtcclxuICAgICAgICAgICAgdGhpcy50aXRsZTIgPSdUb3AgNSBVc2VycyB3aXRoIEFtb3VudCBQYWlkJ1xyXG4gICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHRoaXMudGl0bGUyID0gXCJUb3AgNSBKb3VybmFsc1wiO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICBcclxuICAgICAgICAgIHZhciByZXNwb25zZWFycmF5ID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgIHJlc3BvbnNlYXJyYXkuc29ydChmdW5jdGlvbiAoYSwgYikge1xyXG4gICAgICAgICAgICAgIHJldHVybiBhLnllYXIgLSBiLnllYXIgfHwgTU9OVEhbYS5tb250aF0gLSBNT05USFtiLm1vbnRoXTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgLypcclxuICAgICAgICAgIDA6IHtqb3VybmFsbmFtZTogXCJDb252ZXJzaWUgQWRqLTAyXCIsIG1vbnRoOiBcIk1BUlwiLCB5ZWFyOiBcIjIwMDVcIiwgYW1vdW50OiAxMDAwMH1cclxuICAgICAgICAgIDE6IHtqb3VybmFsbmFtZTogXCJBQk4gMDQtRkVCLTA1IEIwMjVcIiwgbW9udGg6IFwiRkVCXCIsIHllYXI6IFwiMjAwNVwiLCBhbW91bnQ6IDk1MDB9XHJcbiAgICAgICAgICAyOiB7am91cm5hbG5hbWU6IFwiRFJJIElOVCBFWFAgQ29ycmVjdGlvbjogMTItTUFZLTA1IDE0OjQzOjI1XCIsIG1vbnRoOiBcIk1BWVwiLCB5ZWFyOiBcIjIwMDVcIiwgYW1vdW50OiA5MDAwfVxyXG4gICAgICAgICAgMzoge2pvdXJuYWxuYW1lOiBcIkFQUjAwMTRcIiwgbW9udGg6IFwiTUFZXCIsIHllYXI6IFwiMjAwNVwiLCBhbW91bnQ6IDg1MDB9XHJcbiAgICAgICAgICA0OiB7am91cm5hbG5hbWU6IFwiUHVyY2hhc2UgSW52b2ljZXMgVVNEXCIsIG1vbnRoOiBcIkZFQlwiLCB5ZWFyOiBcIjIwMDVcIiwgYW1vdW50OiA4MDAwfVxyXG4gICAgICAgICAgKi9cclxuXHJcbiAgICAgICAgICAvKlxyXG4gICAgICAgICAgdGhpcy5jaGFydERhdGEyID0gW1xyXG4gICAgICAgICAgWydtb250aCcsJzIwMTktMDMtMjUnLCAnMjAxOS0wNC0yNSddLFxyXG4gICAgICAgICAgWydDb252ZXJzaWUgQWRqLTAyJywgMjAwMCwgMjUwMF0sXHJcbiAgICAgICAgICBbJ0tBUyBGRUItMDUnLCAzMDAwLCAzNTAwXSxcclxuICAgICAgICAgIFsnQ29udmVyc2llIEF1Zy0wMyBDb252ZXJzaW9uIEVVUicsIDQwMDAsIDQ1MDBdLFxyXG4gICAgICAgICAgWydNRU1PIDMxLU1BUi0wNSBNMDI3JywgNDUwMCwyNTAwXSxcclxuICAgICAgICAgIFsnUHVyY2hhc2UgSW52b2ljZXMgVVNEJywgNTAwMCwgMTAwMF1cclxuICAgICAgICAgIF07XHJcbiAgICAgICAgICAqL1xyXG4gICAgICAgICBcclxuICAgICAgICAgIHZhciBtb250aHMgPSBbJ21vbnRoJ107XHJcbiAgICAgICAgICB2YXIgam5hbWVzID0gW107XHJcbiAgICAgICAgICBmb3IobGV0IHJlc3VsdCBvZiByZXNwb25zZWFycmF5KXtcclxuICAgICAgICAgICAgdmFyIG9iaiA9IHJlc3VsdDtcclxuICAgICAgICAgICAgdmFyIGlkcyA9IFtdO1xyXG4gICAgICAgICAgICAvL3VzZXJuYW1lIGFycmF5IG9iamVjdFxyXG4gICAgICAgICAgICBtb250aHMucHVzaChvYmoubW9udGgpO1xyXG4gICAgICAgICAgICBpZHMucHVzaChvYmouam91cm5hbG5hbWUpO1xyXG4gICAgICAgICAgICBqbmFtZXMucHVzaChvYmouam91cm5hbG5hbWUpO1xyXG4gICAgICAgICAgICBpZHMucHVzaChvYmoubW9udGgpO1xyXG4gICAgICAgICAgICBpZHMucHVzaChvYmoueWVhcik7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai5hbW91bnQpO1xyXG4gICAgICAgICAgICB0aGlzLmNoYXJ0RGF0YTIucHVzaChpZHMpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgbGV0IHVuaXF1ZW1vbnRocyA9IG1vbnRocy5maWx0ZXIoKGVsZW0sIGksIGFycikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoYXJyLmluZGV4T2YoZWxlbSkgPT09IGkpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gZWxlbVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KVxyXG5cclxuICAgICAgICAgIC8vdW5pcXVlIGpuYW1lc1xyXG4gICAgICAgICAgbGV0IHVuaXF1ZWpuYW1lcyA9IGpuYW1lcy5maWx0ZXIoKGVsZW0sIGksIGFycikgPT4ge1xyXG4gICAgICAgICAgICBpZiAoYXJyLmluZGV4T2YoZWxlbSkgPT09IGkpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gZWxlbVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICB2YXIgam91cm5hbGFycmF5c2V0cyA9IFtdO1xyXG4gICAgICAgICAgZm9yKGxldCByZXN1bHQgb2YgcmVzcG9uc2VhcnJheSl7XHJcbiAgICAgICAgICBmb3IobGV0IGVhY2htb250aCBvZiB1bmlxdWVtb250aHMpe1xyXG4gICAgICAgICAgICBpZihlYWNobW9udGghPSdtb250aCcpe1xyXG4gICAgICAgICAgICB2YXIgam91cm5hbG5hbWVzID0gW3Jlc3VsdC5qb3VybmFsbmFtZV07ICBcclxuICAgICAgICAgICAgICBpZihlYWNobW9udGg9PXJlc3VsdC5tb250aCl7XHJcbiAgICAgICAgICAgICAgICAgaWYocmVzdWx0LmFtb3VudCAhPSAnJyl7XHJcbiAgICAgICAgICAgICAgICAgIGpvdXJuYWxuYW1lcy5wdXNoKHJlc3VsdC5hbW91bnQpO1xyXG4gICAgICAgICAgICAgICAgICBqb3VybmFsYXJyYXlzZXRzLnB1c2goam91cm5hbG5hbWVzKTsgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICBqb3VybmFsbmFtZXMucHVzaCgwKTtcclxuICAgICAgICAgICAgICAgIGpvdXJuYWxhcnJheXNldHMucHVzaChqb3VybmFsbmFtZXMpOyBcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuICAgICAgIFxyXG4gICAgICAgICAgdmFyIGZpbmFsYXJyYXlzZXQgPSBbXTtcclxuICAgICAgICAgIGZvcihsZXQgZWFjaGpuYW1lcyBvZiB1bmlxdWVqbmFtZXMpe1xyXG4gICAgICAgICAgICB2YXIgbmV3anJuYXJyYXlzPVtlYWNoam5hbWVzXTtcclxuICAgICAgICAgICBmb3IobGV0IGVudHJ5IG9mIGpvdXJuYWxhcnJheXNldHMpe1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgaWYoZW50cnlbMF09PT1lYWNoam5hbWVzKXtcclxuICAgICAgICAgICAgICAgIG5ld2pybmFycmF5cy5wdXNoKGVudHJ5WzFdKTtcclxuICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIGZpbmFsYXJyYXlzZXQucHVzaChuZXdqcm5hcnJheXMpOyBcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB2YXIgY2hhcnQyYXJyYXkgPSBbXTtcclxuICAgICAgICAgIC8vY2hhcnQyYXJyYXkucHVzaCh1bmlxdWVtb250aHMpO1xyXG4gICAgICAgICAgZm9yKGxldCBlYWNocm93IG9mIGZpbmFsYXJyYXlzZXQpXHJcbiAgICAgICAgICAgIGNoYXJ0MmFycmF5LnB1c2goZWFjaHJvdyk7XHJcblxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgIHZhciBtb250aGNhdGVnb3JpemVkID0gW107XHJcbiAgICAgICAgICBmb3IobGV0IG1vbnRobmFtZSBvZiB1bmlxdWVtb250aHMpXHJcbiAgICAgICAgICB7XHJcblxyXG4gICAgICAgICAgICBpZihtb250aG5hbWUgIT09ICdtb250aCcpe1xyXG4gICAgICAgICAgICAgIG1vbnRoY2F0ZWdvcml6ZWQucHVzaChtb250aG5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgfVxyXG5cclxuXHJcblxyXG4gICAgICAgICBcclxuICAgICAgICAgIHZhciBjaGFydDIgPSBjMy5nZW5lcmF0ZSh7XHJcbiAgICAgICAgICAgIGJpbmR0bzogJyNjaGFydDInLFxyXG4gICAgICAgICAgIFxyXG4gICAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgY29sdW1uczogY2hhcnQyYXJyYXksXHJcbiAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdiYXInXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGNvbG9yOntcclxuICAgICAgICAgICAgICBwYXR0ZXJuOlsnIzM5NkFCMScsICcjREE3QzMwJywgJyMzRTk2NTEnLCAnI0NDMjUyOScsICcjNTM1MTU0JywgJyM2QjRDOUEnLCAnIzk0OEIzRCddXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGJhcjoge1xyXG4gICAgICAgICAgICAgIHdpZHRoOiAyNVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBheGlzOiB7XHJcbiAgICAgICAgICAgICAgICB4OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2NhdGVnb3J5JyAsLy8gdGhpcyBuZWVkZWQgdG8gbG9hZCBzdHJpbmcgeCB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgY2F0ZWdvcmllczogbW9udGhjYXRlZ29yaXplZFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgIFxyXG5cclxuXHJcblxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIFxyXG5cclxuXHJcbiAgICAgIC8vQ2hhcnQgM1xyXG4gICAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRUb3RhbE51bWJlcm9mSm91cm5hbCh0aGlzLnF1ZXJ5UGFyYW1zKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBcclxuICAgICAgICAgIHRoaXMuY2hhcnREYXRhMyA9IFtdO1xyXG4gICAgICAgICAgXHJcbiAgICAgICAgICBpZih0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGllckludm9pY2VkZXRhaWxzJyB8fCB0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGllckludm9pY2VkZXRhaWxzLWVicycpe1xyXG4gICAgICAgICAgICB0aGlzLnRpdGxlMyA9J0ludm9pY2UgYW1vdW50IGJ5IG1vbnRoJ1xyXG4gICAgICAgICAgfWVsc2UgaWYodGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxlaXJQYXltZW50ZGV0YWlscycgfHwgdGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxlaXJQYXltZW50ZGV0YWlscy1lYnMnKXtcclxuICAgICAgICAgICAgdGhpcy50aXRsZTMgPSdBbW91bnQgcGFpZCBieSBtb250aCdcclxuICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICB0aGlzLnRpdGxlMyA9IFwiSm91cm5hbHMgYnkgUGVyaW9kXCI7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB2YXIgcmVzcG9uc2VhcnJheSA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgICAgICByZXNwb25zZWFycmF5LnNvcnQoZnVuY3Rpb24gKGEsIGIpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gYS55ZWFyIC0gYi55ZWFyIHx8IE1PTlRIMlthLm1vbnRoLnRvTG93ZXJDYXNlKCldIC0gTU9OVEgyW2IubW9udGgudG9Mb3dlckNhc2UoKV07XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHZhciBtb250aHMgPSBbJ21vbnRoJ107XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICBmb3IobGV0IHJlc3VsdCBvZiByZXNwb25zZWFycmF5KXtcclxuICAgICAgICAgICAgdmFyIG9iaiA9IHJlc3VsdDtcclxuICAgICAgICAgICAgdmFyIGlkcyA9IFtdO1xyXG4gICAgICAgICAgICAvL3VzZXJuYW1lIGFycmF5IG9iamVjdFxyXG4gICAgICAgICAgICBtb250aHMucHVzaChvYmoubW9udGgpO1xyXG4gICAgICAgICAgICBpZHMucHVzaChvYmoubW9udGgpO1xyXG4gICAgICAgICAgICBpZHMucHVzaChvYmoueWVhcik7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai5jb3VudCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2hhcnREYXRhMy5wdXNoKGlkcyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBsZXQgdW5pcXVlbW9udGhzID0gbW9udGhzLmZpbHRlcigoZWxlbSwgaSwgYXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcnIuaW5kZXhPZihlbGVtKSA9PT0gaSkge1xyXG4gICAgICAgICAgICAgIHJldHVybiBlbGVtXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICB2YXIgam91cm5hbGFycmF5c2V0cyA9IFtdO1xyXG4gICAgICAgICAgZm9yKGxldCByZXN1bHQgb2YgcmVzcG9uc2VhcnJheSl7XHJcbiAgICAgICAgICBmb3IobGV0IGVhY2htb250aCBvZiB1bmlxdWVtb250aHMpe1xyXG4gICAgICAgICAgICBpZihlYWNobW9udGghPSdtb250aCcpe1xyXG4gICAgICAgICAgICB2YXIgam91cm5hbHBlcmlvZCA9IFtyZXN1bHQubW9udGhdOyAgXHJcbiAgICAgICAgICAgICAgaWYoZWFjaG1vbnRoPT1yZXN1bHQubW9udGgpe1xyXG4gICAgICAgICAgICAgICAgIGlmKHJlc3VsdC5jb3VudCAhPSAnJyl7XHJcbiAgICAgICAgICAgICAgICAgIGpvdXJuYWxwZXJpb2QucHVzaChyZXN1bHQuY291bnQpO1xyXG4gICAgICAgICAgICAgICAgICBqb3VybmFsYXJyYXlzZXRzLnB1c2goam91cm5hbHBlcmlvZCk7IFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBlbHNle1xyXG4gICAgICAgICAgICAgICAgam91cm5hbHBlcmlvZC5wdXNoKDApO1xyXG4gICAgICAgICAgICAgICAgam91cm5hbGFycmF5c2V0cy5wdXNoKGpvdXJuYWxwZXJpb2QpOyBcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIGZpbmFsYXJyYXlzZXQgPSBbXTtcclxuICAgICAgICAgIGZvcihsZXQgZWFjaGpwZXJpb2Qgb2YgdW5pcXVlbW9udGhzKXtcclxuICAgICAgICAgICAgdmFyIG5ld2pybmFycmF5cz1bZWFjaGpwZXJpb2RdO1xyXG4gICAgICAgICAgIGZvcihsZXQgZW50cnkgb2Ygam91cm5hbGFycmF5c2V0cyl7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICBpZihlbnRyeVswXT09PWVhY2hqcGVyaW9kKXtcclxuICAgICAgICAgICAgICAgIG5ld2pybmFycmF5cy5wdXNoKGVudHJ5WzFdKTtcclxuICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgfVxyXG4gICAgICAgICAgIGZpbmFsYXJyYXlzZXQucHVzaChuZXdqcm5hcnJheXMpOyBcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB2YXIgY2hhcnQzYXJyYXkgPSBbXTtcclxuICAgICAgICAgIC8vY2hhcnQyYXJyYXkucHVzaCh1bmlxdWVtb250aHMpO1xyXG4gICAgICAgICAgZm9yKGxldCBlYWNocm93IG9mIGZpbmFsYXJyYXlzZXQpXHJcbiAgICAgICAgICBjaGFydDNhcnJheS5wdXNoKGVhY2hyb3cpO1xyXG5cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICB2YXIgbW9udGhjYXRlZ29yaXplZCA9IFtdO1xyXG4gICAgICAgICAgZm9yKGxldCBtb250aG5hbWUgb2YgdW5pcXVlbW9udGhzKVxyXG4gICAgICAgICAge1xyXG5cclxuICAgICAgICAgICAgaWYobW9udGhuYW1lICE9PSAnbW9udGgnKXtcclxuICAgICAgICAgICAgICBtb250aGNhdGVnb3JpemVkLnB1c2gobW9udGhuYW1lKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICBsZXQgY2hhcnQzID0gYzMuZ2VuZXJhdGUoe1xyXG4gICAgICAgICAgICAgIGJpbmR0bzogJyNjaGFydDMnLFxyXG4gICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICAgICAgICAgY29sdW1uczogY2hhcnQzYXJyYXksXHJcbiAgICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICB0eXBlOiAnYmFyJ1xyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgYmFyOiB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMjVcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIGNvbG9yOntcclxuICAgICAgICAgICAgICAgIHBhdHRlcm46WycjMzk2QUIxJywgJyNEQTdDMzAnLCAnIzNFOTY1MScsICcjQ0MyNTI5JywgJyM1MzUxNTQnLCAnIzZCNEM5QScsICcjOTQ4QjNEJ11cclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIGF4aXM6IHtcclxuICAgICAgICAgICAgICAgICAgeDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2NhdGVnb3J5JyAsLy8gdGhpcyBuZWVkZWQgdG8gbG9hZCBzdHJpbmcgeCB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgICBjYXRlZ29yaWVzOiBtb250aGNhdGVnb3JpemVkXHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTsgIFxyXG4gICAgXHJcbiAgICBcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBzaG93QXZEZXRhaWwoKTogdm9pZCB7XHJcbiAgICAvL2xvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiZWRpdFVzZXJJZFwiKTtcclxuICAgIC8vbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJlZGl0VXNlcklkXCIsIHVzZXIuaWQudG9TdHJpbmcoKSk7XHJcbiAgICBsZXQgb2JqID0ge1xyXG4gICAgICB1cmwgOiAncmVwb3J0LW1hbmFnZW1lbnQvbWFuYWdlLWF2LWJ5dXNlcicsXHJcbiAgICAgIGlkIDogXCJtYW5hZ2VBdlVzZXJcIlxyXG4gICAgfVxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvJywgJ3JlcG9ydC1tYW5hZ2VtZW50L21hbmFnZS1hdi1ieXVzZXInXSkudGhlbihuYXYgPT4ge1xyXG4gICAgfSwgZXJyID0+IHtcclxuICAgICAgY29uc29sZS5sb2coZXJyKSAvLyB3aGVuIHRoZXJlJ3MgYW4gZXJyb3JcclxuICAgIH0pO1xyXG4gIH07XHJcblxyXG5cclxuICBvblNlbGVjdChldnQsIHNlbGVjdGVkVmFsKSB7XHJcbiAgICBsZXQgb2JqID0ge1xyXG4gICAgICB1cmwgOiAncmVwb3J0LW1hbmFnZW1lbnQvbWFuYWdlLWF2LWJ5dXNlcicsXHJcbiAgICAgIGlkIDogXCJtYW5hZ2VBdlVzZXJcIlxyXG4gICAgfVxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydyZXBvcnQtbWFuYWdlbWVudC9tYW5hZ2UtYXYtYnl1c2VyJ10pO1xyXG4gICAgLy8gdGhpcy5teUV2ZW50LmVtaXQoe2NyZWF0ZWRieTpzZWxlY3RlZEl0ZW0udXNlcm5hbWUsam91cm5hbG5hbWU6c2VsZWN0ZWRJdGVtLmVudGl0bGVtZW50fSk7XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWVbJ2NyZWF0ZWRCeSddID0gc2VsZWN0ZWRWYWwudXNlcm5hbWU7XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWVbJ2pvdXJuYWxuYW1lJ10gPSBzZWxlY3RlZFZhbC5lbnRpdGxlbWVudDtcclxuICB9XHJcblxyXG59XHJcblxyXG5cclxuXHJcbiJdfQ==