import * as tslib_1 from "tslib";
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatInput } from '@angular/material';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ReportManagementService } from '../report-management.service';
import * as FileSaver from 'file-saver';
import { SetupAdministrationService } from '../setup-administration.service';
import { ReportHistoryComponent } from './report-management-history/report-management.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SnackBarService } from './../../shared/snackbar.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { fuseAnimations } from '../../@fuse/animations';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
var moment = _moment;
import { MessageService } from '../../_services/message.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'; // Import NgxUiLoaderService
import { configMetaData } from './demoData';
export var MY_FORMATS = {
    parse: {
        dateInput: 'LL'
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};
var ConfigTrackerComponent = /** @class */ (function () {
    function ConfigTrackerComponent(formBuilder, _matDialog, router, snackBarService, _reportManagementService, SetupAdministrationService, messageService, ngxService) {
        this.formBuilder = formBuilder;
        this._matDialog = _matDialog;
        this.router = router;
        this.snackBarService = snackBarService;
        this._reportManagementService = _reportManagementService;
        this.SetupAdministrationService = SetupAdministrationService;
        this.messageService = messageService;
        this.ngxService = ngxService;
        this.messages = [];
        this.submitted = false;
        this.typesss = [];
        this.final_selected_data = [];
        this.app_types = [];
        this.checkedControl = [];
        this.displayedColumns = ['select', 'object_name', 'object_code'];
        this.displayColumns = [
            'module_name',
            'name',
            'control_name',
            'control_type',
            'riskRank',
            'action'
        ];
        this.displayColumnsList = [
            'module_name',
            'name',
            'control_name',
            'control_type',
            'riskRank'
        ];
        this.displayObjects = [
            'select',
            'name',
            'control_name',
            'control_type',
            'riskRank'
        ];
        this.selectedColumns = [
            'application_name',
            'object_name',
            'object_code'
        ];
        this.expandElement = false;
        this.parentLimit = 10;
        this.dSource = [
            {
                module: 'GL',
                application_name: 'General Ledger',
                object_name: 'Accounting Calendar',
                object_code: ''
            }
            // {module: 'GL', application_name: 'General Ledger', object_name: 'Column set', control_name: '',control_type:''}
        ];
        this.profileForm = new FormGroup({
            selected: new FormControl()
        });
        this.dSrcReport = [
            {
                app_name: '',
                operation: '',
                object: '',
                change_date: '',
                change_by: '',
                control_name: '',
                existing_record: '',
                new_record: ''
            }
        ];
        this.controlTypes = [
            {
                value: 'SOX',
                name: 'SOX'
            },
            {
                value: 'HIPAA',
                name: 'HIPAA'
            },
            {
                value: 'FIDA',
                name: 'FIDA'
            }
        ];
        this.riskRanks = [
            {
                value: 'critical',
                name: 'Critical'
            },
            {
                value: 'high',
                name: 'High'
            },
            {
                value: 'medium',
                name: 'Medium'
            },
            {
                value: 'low',
                name: 'Low'
            }
        ];
        // displayedColumns: string[] = ['clientID', 'fusionUrl', 'userName','queryTypes', 'scheduleType','retainDays' ];
        this.displayedReport = [
            'app_name',
            'operation',
            'object',
            'change_date',
            'change_by',
            'control_name',
            'existing_record',
            'new_record'
        ];
        this.show_data = 'dashboard';
        this.columnsToDisplay = [];
        this.fromDate = new FormControl(moment());
        this.toDate = new FormControl(moment());
        this.moduleNames = configMetaData.moduleData;
        this.selectedTabHeader = [];
        this.historyData = {};
        this.selectedTableHeader = [];
        this.tableHeader = configMetaData.tableConfig;
        this.tableConfig = [];
        this.historyConfigData = [];
        this.step = 0;
        this.conftchartbymodule = [];
        this.ebs_m_chart = [];
        this.master_chart = [];
        this.week_by_user_chart = [];
        this.month_by_user_chart = [];
        // Bar
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.showYAxisLabel = true;
        this.xlabel = 'Modules';
        this.ylabel = 'Records';
        this.xlabelweek = 'Users';
        this.ylabelweek = 'Records';
        /*pie chart*/
        // options
        this.showPieLegend = false;
        this.legendPosition = 'right';
        this.view = [700, 400];
        this.weekview = [450, 350];
        this.pieview = [460, 400];
        this.legendTitle = 'Users';
        this.masterbyuserview = [600, 350];
        this.colorPieScheme = {
            domain: ['#28a3dd', '#8ec9e5', '#1c729a', '#186184', '#14516e', '#104158']
        };
        this.limit = 10;
        this.offset = 0;
        // pie
        this.showLabels = true;
        this.explodeSlices = false;
        this.configTrackerSetupDone = false;
        this.configQTSetupDone = false;
        this.doughnut = false;
        this.isChecked = false;
        // xAxisLabel=true;
        this.barPadding = 50;
        this.colorScheme = {
            domain: ['#28a3dd']
        };
        this.onSelectedModuleObjects = [];
        this.CreateForm = this.formBuilder.group({
            select: [''],
            module: [''],
            userName: [''],
            object_name: [''],
            control_type: [''],
            control_name: [''],
            riskRank: ['']
        });
    }
    ConfigTrackerComponent.prototype.ngOnInit = function () {
        this.formAction = 'add';
        this.types = [];
        this.ngxService.start();
        this.showedit();
        this.getallconfigs();
        // this.selectedTabHeader = [
        // 	{
        // 		'value':'transactiontype',
        // 		'name': 'AR_TransactionTypes'
        // 	},{
        // 		'value':'paymentterms',
        // 		'name': 'AR_PaymentTerms'
        // 	}
        // ];
        this.mode = 'view';
    };
    ConfigTrackerComponent.prototype.openHistory = function (element) {
        // var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
        console.log(element, '.......element');
        // let selectId =  element[selectedColumn[0].name]["name"]
        this.dialogRef = this._matDialog.open(ReportHistoryComponent, {
            disableClose: true,
            width: '75%',
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
                historyData: element.historyData,
                reportData: element,
                selectedHeader: this.selectedHeaderId,
                dateColumns: this.dateColumns
            }
        });
    };
    ConfigTrackerComponent.prototype.openNavigate = function () {
        var obj = {
            url: 'setup-administration/config-tracker-setup',
            id: "manageConfigTrackerSetup"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['setup-administration/config-tracker-setup']);
    };
    ConfigTrackerComponent.prototype.getallconfigs = function () {
        var _this = this;
        var userId = localStorage.getItem('userId');
        this.selected_modules = [];
        this.final_selected_data = [];
        this.getConfigTable = [];
        this.dSource.length = 0;
        this._reportManagementService.getAllConfigTracker(userId).subscribe(function (data) {
            var data_new = data.body;
            _this.dSource = data_new.response.configDetails;
            if (_this.dSource.length != 0) {
                _this.configTrackerSetupDone = true;
                _this.selected_modules = _this.dSource[0].module;
                _this.getConfigTable = _this.dSource[0].object_name;
                _this.configTrackerID = _this.dSource[0]._id;
                _this.clientID = _this.dSource[0].clientID;
                _this.selected_data = new MatTableDataSource(_this.getConfigTable);
                _this.selected_data.paginator = _this.spaginator;
                _this.selected_datasource = _this.getConfigTable;
                _this.selected_data_length = _this.getConfigTable.length;
                _this.module_RM = _this.dSource[0].module;
                console.log('Module-------', _this.dSource[0].module);
                _this.object_name_RM_byModule = _.groupBy(_this.dSource[0].object_name, 'module_name');
                if (_this.dSource[0].module.length != 0) {
                    _this.formAction = 'edit';
                    _this.configQTSetupDone = true;
                }
                else {
                    _this.formAction = 'add';
                    _this.show_data = 'config_tracker';
                }
                _this.mode = 'view';
            }
            else {
                _this.show_data = 'config_tracker';
            }
            _this.Toggle('dashboard', null, null, null);
            _this.ngxService.stop();
        }, function (error) {
            _this.ngxService.stop();
            console.log('error:::' + error);
            _this.snackBarService.warning('Oops Something went wrong!! Please try again after sometime.');
        });
    };
    ConfigTrackerComponent.prototype.showConfigSetup = function () {
        this.show_data = 'config_tracker';
    };
    ConfigTrackerComponent.prototype.loadTableColumn = function (selectedHeader, headerValues) {
        var e_1, _a;
        this.configTrackerHeader = [];
        var tempArray = [
            {
                name: 'History',
                value: 'history',
                isActive: true
            },
            {
                name: 'Status',
                value: 'status',
                isActive: true
            }
        ];
        if (headerValues) {
            _.forEach(headerValues, function (item) {
                var tempObj = {
                    name: item.uiAttrName,
                    value: item.dbAttrName,
                    isIdentifier: item.isIdentifier,
                    isActive: false
                };
                if (item.type) {
                    tempObj['type'] = item.type;
                }
                if (item.dbAttrName === 'START_DATE' ||
                    item.dbAttrName === 'CREATED_BY_USER_ID' ||
                    item.dbAttrName === 'CREATION_DATE' ||
                    item.dbAttrName === 'LAST_UPDATED_BY_USER' ||
                    item.dbAttrName === 'NAME') {
                    tempObj['isActive'] = true;
                }
                // 	tempObj['isActive'] = false;
                // }
                tempArray.push(tempObj);
            });
            this.tableConfig = tempArray;
        }
        try {
            for (var _b = tslib_1.__values(this.tableConfig), _c = _b.next(); !_c.done; _c = _b.next()) {
                var columns = _c.value;
                if (columns.isActive) {
                    this.configTrackerHeader.push(columns.name);
                    this.selectedTableHeader.push(columns.name);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    ConfigTrackerComponent.prototype.updateTable = function (event) {
        var selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.name) >= 0) {
                item.isActive = true;
            }
            else {
                item.isActive = false;
            }
        });
        localStorage.removeItem('control');
        localStorage.setItem('control', JSON.stringify(this.tableConfig));
        this.loadTableColumn(this.selectedHeaderId, null);
    };
    ConfigTrackerComponent.prototype.onSelectModuleRM = function (event, isRedirect) {
        event = event && event.value ? event.value : event;
        var tempArray = [];
        var tempObj = this.object_name_RM_byModule;
        _.forEach(event, function (item) {
            if (tempObj[String(item)]) {
                if (tempArray && tempArray.length) {
                    tempArray = _.concat(tempArray, tempObj[String(item)]);
                }
                else {
                    tempArray = tempObj[String(item)];
                }
            }
        });
        this.select_modules_RM = event;
        this.objectName_RM = tempArray;
        this.statusFilter = [
            {
                value: 'changed',
                name: 'Changed'
            },
            {
                value: 'notChanged',
                name: 'Not Changed'
            },
            {
                value: 'created',
                name: 'Created'
            }
        ];
        if (isRedirect) {
            this.selectedTabHeader = tempArray;
            this.selectedStatus = this.statusFilter[0];
            // this.selectedTabHeader.push(tempArray[0])
            this.changeTab({ index: 0 });
        }
    };
    ConfigTrackerComponent.prototype.updateTab = function (event, fromTab) {
        console.log('updateTab >>>> ', event);
        if (event.value) {
            // this.selectobjheader = event.value;
            this.selectedHeaderId = this.selectedTabHeader[0].type;
            // this.selected_data.push(this.dSource[i].queryTypes[j].value);
        }
        else if (fromTab == 'dashboard') {
            var s_obj = [];
            this.selectedTabHeader = event;
            this.changeTab({ index: 0 });
            // this.reportmanagementObjectlist.forEach(elem =>{
            // 	let index = event.findIndex(
            // 		obj => obj.name === elem.name
            // 	);
            // 	if(index > -1){
            // 		elem.checked = true
            // 		s_obj.push(elem.name);
            // 	}
            // })
            // this.selectobjheader = s_obj;
            // console.log('this.selectobjheader',this.selectobjheader)
            // console.log("types...",this.types,"reportmanagementObjectlist---------",this.reportmanagementObjectlist)
        }
        this.getUserNameList('');
    };
    ConfigTrackerComponent.prototype.applyFilter = function () {
        // if(this.show_data == 'report_management')
        this.changeTab(null);
    };
    ConfigTrackerComponent.prototype.resetFilter = function () {
        this.select_modules_RM = [];
        this.selectedTabHeader = [];
        this.selectedUser = '';
        this.selectedStatus = '';
        this.selectedTableHeader = [];
        this.fromInput.value = '';
        this.toInput.value = '';
        this.configData = [];
        this.configTrackerHeader = [];
    };
    ConfigTrackerComponent.prototype.changeTab = function (event) {
        var _this = this;
        if (event) {
            event = event.index;
            this.selectedHeaderId = this.selectedTabHeader[event].type;
            this.selectedHeaderModulename = this.selectedTabHeader[event].module_name;
            this.selectedHeaderObjectName = this.selectedTabHeader[event].name;
        }
        else {
            // if(!this.selectedHeaderId){
            this.selectedHeaderId = this.selectedTabHeader[0].type;
            this.selectedHeaderModulename = this.selectedTabHeader[0].module_name;
            this.selectedHeaderObjectName = this.selectedTabHeader[0].name;
            // }
        }
        console.log(this.selectedHeaderId, 'SELECTED');
        console.log(this.selectedStatus, '.........STATUS');
        var headerOption = {
            type: this.selectedHeaderId
        };
        console.log(this.tableConfig, '----tableconfig');
        this._reportManagementService
            .getHeaderData(headerOption)
            .subscribe(function (headerDatas) {
            var headerData;
            _this.currentHeaderData = headerDatas.body.response;
            _this.loadTableColumn(_this.selectedHeaderId, _this.currentHeaderData.params);
            var selectedColumn = _.filter(_this.tableConfig, ['isIdentifier', true]);
            selectedColumn = _.map(selectedColumn, 'name');
            console.log(selectedColumn, '......');
            _this.dateColumns = _.filter(_this.tableConfig, ['type', 'date']);
            console.log(_this.dateColumns, '......Date');
            var dateColumns = _this.dateColumns;
            _this.uniqueID = selectedColumn;
            var options = {
                limit: _this.limit,
                offset: _this.offset,
                configTrackerId: _this.configTrackerID,
                type: _this.selectedHeaderId,
                uniqueID: _this.uniqueID,
                selectedUserName: _this.selectedUser ? _this.selectedUser : '',
                selectedStatus: _this.selectedStatus ? _this.selectedStatus.value : '',
                fromDate: _this.fromInput.value ? _this.fromInput.value : '',
                toDate: _this.toInput.value ? _this.toInput.value : ''
            };
            _this._reportManagementService
                .getRawData(options)
                .subscribe(function (data) {
                // var demoConfig = demoData.lastModifiedData;
                if (data && data.body && data.body.response) {
                    _this.configData = data.body.response.processDataResp
                        ? data.body.response.processDataResp
                        : [];
                    // this.historyData = data.body.response.historyData;
                    var processArray = data.body.response.processDataResp
                        ? data.body.response.processDataResp
                        : [];
                    _.forEach(processArray, function (item) {
                        console.log(item, '-----ITEM');
                        // let id = (item && item[String(selectedColumn[0].name)]) ? item[String(selectedColumn[0].name)]["name"] : '';
                        // let tempObj = data.body.response.historyData;
                        item['isChanged'] =
                            item && item.historyData && item.historyData.length
                                ? true
                                : false;
                        item['isAdded'] =
                            item && item.historyData && item.historyData.length
                                ? false
                                : item['isAdded'];
                        if (dateColumns && dateColumns.length) {
                            _.forEach(dateColumns, function (dateItem) {
                                var dateString = item[String(dateItem.name)]['name'];
                                var showDate = new Date(dateString);
                                var formatDate = moment(showDate).format('lll');
                                item[String(dateItem.name)]['name'] = formatDate;
                            });
                        }
                    });
                    _this.length = data.body.response.total;
                    _this.configData =
                        processArray && processArray.length ? processArray : [];
                    _this.configData.paginator = _this.reportpaginator;
                    console.log(_this.configData, '.......ConfigDATAAAAA');
                }
            });
        });
    };
    ConfigTrackerComponent.prototype.changePage = function (event) {
        if (event.pageSize != this.limit) {
            this.limit = event.pageSize;
            this.offset = 0;
        }
        else {
            this.offset = event.pageIndex * this.limit;
        }
        this.changeTab(null);
    };
    ConfigTrackerComponent.prototype.setStep = function (rows) {
        // this.step = index;
        var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
        var queryParams = {
            // type : this.selectedHeaderId,
            // uniqueID : selectedColumn[0].name,
            selectId: rows[selectedColumn[0].name]['name']
        };
        this.getConfigHistory(queryParams);
    };
    ConfigTrackerComponent.prototype.getConfigHistory = function (options) {
        console.log(this.historyData, '....this.historyData');
        this.historyConfigData = this.historyData[options.selectId];
        // this._reportManagementService.getHistoryData(options).subscribe((dataHistory: any) => {
        // 	if(dataHistory && dataHistory.body && dataHistory.body.response){
        // 		this.historyConfigData = dataHistory.body.response;
        // 	}
        // });
    };
    ConfigTrackerComponent.prototype.showedit = function () {
        var _this = this;
        // alert('jbvc')
        this.mode = 'edit';
        this._reportManagementService.getAllModules().subscribe(function (data) {
            _this.modules = [];
            _this.types = [];
            _this.reportmanagementObjectlist = [];
            _this.typesss = [];
            var res = data.body.response.Modules;
            _this.modules = data.body.response.Modules;
            _this.final_selected_data = _this.getConfigTable;
            if (_this.selected_modules.length != 0) {
                _this.getAllObjectsNameList(function () { });
            }
        });
    };
    ConfigTrackerComponent.prototype.showHistory = function (type, rows) {
        console.log('type---------', type);
        if (type == 'true') {
            this.expandElement = true;
        }
        else if (type == 'false') {
            this.expandElement = false;
        }
        console.log('this.expandElement---------', this.expandElement);
        var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
        var queryParams = {
            // type : this.selectedHeaderId,
            // uniqueID : selectedColumn[0].name,
            selectId: rows[selectedColumn[0].name]['name']
        };
        this.getConfigHistory(queryParams);
    };
    ConfigTrackerComponent.prototype.getAllObjectsNameList = function (callback) {
        var _this = this;
        var options = {
            module: this.selected_modules
        };
        this._reportManagementService.getObjects(options).subscribe(function (data) {
            var res = data.body.response.Modules;
            res.forEach(function (item) {
                var obj = {};
                obj['name'] = item.objectName[0].name;
                obj['value'] = item.objectName[0].name;
                obj['type'] = item.objectName[0].type;
                obj['module_name'] = item.objectName[0].module_name;
                var index = _this.getConfigTable.findIndex(function (obj) { return obj.name === item.objectName[0].name; });
                if (index > -1) {
                    obj['checked'] = true;
                    obj['control_name'] = _this.getConfigTable[index].control_name;
                    obj['control_type'] = _this.getConfigTable[index].control_type;
                    obj['riskRank'] = _this.getConfigTable[index].riskRank;
                }
                else {
                    obj['checked'] = false;
                    obj['control_name'] = '';
                    obj['control_type'] = '';
                    obj['riskRank'] = '';
                }
                _this.types.push(obj);
                _this.reportmanagementObjectlist.push(obj);
            });
            console.log('reportmanagementObjectlist----------->>...', _this.reportmanagementObjectlist);
            _this.object_list = new MatTableDataSource(_this.types);
            _this.object_list.paginator = _this.paginator;
            _this.object_list_length = _this.types.length;
        });
        callback();
    };
    ConfigTrackerComponent.prototype.checkExits = function (selectedRow) {
        if (selectedRow == true) {
            return true;
        }
        else {
            return false;
        }
    };
    ConfigTrackerComponent.prototype.onSearchName = function (filterValue) {
        this.selected_data.filter = filterValue.trim().toLowerCase();
        this.selected_data.paginator = this.spaginator;
    };
    ConfigTrackerComponent.prototype.onSearchObjectName = function (filterValue) {
        this.object_list.filter = filterValue.trim().toLowerCase();
        this.object_list.paginator = this.paginator;
        this.object_list_length = this.types.length;
    };
    ConfigTrackerComponent.prototype.onSelectModule = function (event) {
        var keyByObjectNames = _.groupBy(this.getConfigTable, 'module_name');
        _.forEach(event.value, function (item) {
            if (this.onSelectedModuleObjects && this.onSelectedModuleObjects.length) {
                this.onSelectedModuleObjects = _.concat(this.onSelectedModuleObjects, keyByObjectNames[item]);
            }
            else {
                this.onSelectedModuleObjects = keyByObjectNames[item];
            }
        });
    };
    ConfigTrackerComponent.prototype.onChangeDatasourceId = function (id, type) {
        var _this = this;
        //this.ngxService.start();
        var modules = [];
        if (type == 'dashboard') {
            this.select_modules = id;
            modules = id;
        }
        else {
            modules = id.value;
        }
        this.types = [];
        this.selected_objects = [];
        var options = {
            module: modules
        };
        this._reportManagementService.getObjects(options).subscribe(function (data) {
            var res = data.body.response.Modules;
            res.forEach(function (item) {
                var obj = {};
                obj['name'] = item.objectName[0].name;
                obj['value'] = item.objectName[0].name;
                obj['type'] = item.objectName[0].type;
                obj['module_name'] = item.objectName[0].module_name;
                if (_this.final_selected_data.length > 0) {
                    var index = _this.final_selected_data.findIndex(function (obj) { return obj.name === item.objectName[0].name; });
                    if (index > -1) {
                        obj['checked'] = true;
                        obj['control_name'] = _this.final_selected_data[index].control_name;
                        obj['control_type'] = _this.final_selected_data[index].control_type;
                        obj['riskRank'] = _this.final_selected_data[index].riskRank;
                    }
                    else {
                        obj['checked'] = false;
                        obj['control_name'] = '';
                        obj['control_type'] = '';
                        obj['riskRank'] = '';
                    }
                }
                else {
                    obj['checked'] = false;
                    obj['control_name'] = '';
                    obj['control_type'] = '';
                    obj['riskRank'] = '';
                }
                // if(obj["checked"] == true){
                // 	this.isChecked = true;
                // }
                // else if(obj["checked"] == false){
                // 	this.isChecked = false;
                // }
                _this.types.push(obj);
                _this.reportmanagementObjectlist.push(obj);
            });
            _this.object_list = new MatTableDataSource(_this.types);
            _this.object_list.paginator = _this.paginator;
            _this.object_list_length = _this.types.length;
            _this.final_selected_data = _.filter(_this.types, ['checked', true]);
            //this.ngxService.stop();
        });
    };
    ConfigTrackerComponent.prototype.onChangeType = function (element, event, type) {
        var _this = this;
        if (type == 'all') {
            if (event.checked == true) {
                this.isChecked = true;
                this.types.forEach(function (item) {
                    item.checked = true;
                    var obj = {};
                    obj['name'] = item.name;
                    obj['value'] = item.value;
                    obj['type'] = item.type;
                    obj['module_name'] = item.module_name;
                    obj['control_name'] = item.control_name;
                    obj['control_type'] = item.control_type;
                    obj['riskRank'] = item.riskRank;
                    obj['checked'] = item.checked;
                    _this.final_selected_data.push(obj);
                });
            }
            else if (event.checked == false) {
                this.isChecked = false;
                this.types.forEach(function (item) {
                    item.checked = false;
                });
                this.final_selected_data = [];
            }
        }
        else if (type == 'single') {
            if (this.final_selected_data.findIndex(function (temp) { return temp.name === element.name; }) < 0) {
                if (event.checked == true) {
                    this.final_selected_data.push(element);
                    var iindex = this.types.findIndex(function (objt) { return objt.name === element.name; });
                    this.types[iindex].checked = true;
                }
                else if (event.checked == false) {
                    var index = this.final_selected_data.findIndex(function (obj) { return obj.name === element.name; });
                    this.final_selected_data.splice(index, 1);
                    var iindex = this.types.findIndex(function (objt) { return objt.name === element.name; });
                    this.types[iindex].checked = false;
                }
            }
        }
    };
    ConfigTrackerComponent.prototype.ConfirmData = function () {
        var _this = this;
        console.log('ConfirmData----------->>>', this.final_selected_data);
        console.log('selectedModules----------->>>', this.selected_modules);
        this.final_selected_data.forEach(function (item, indexx) {
            var index = _this.selected_modules.findIndex(function (obj) { return obj === item.module_name; });
            if (index > -1) {
                console.log('Item-----------', item.module_name);
            }
            else {
                console.log('ItemELSE-----------', index);
                _this.final_selected_data.splice(indexx, 1);
            }
        });
        console.log('ConfirmDataFinal----------->>>', this.final_selected_data);
        this.final_selected_table = new MatTableDataSource(this.final_selected_data);
        this.final_selected_table.paginator = this.fpaginator;
        this.final_selected_length = this.final_selected_data.length;
    };
    ConfigTrackerComponent.prototype.PreviousData = function () {
        this.object_list = new MatTableDataSource(this.types);
        this.object_list.paginator = this.paginator;
        this.object_list_length = this.types.length;
    };
    ConfigTrackerComponent.prototype.onChangeName = function (event, name) {
        var iindex = this.types.findIndex(function (obj) { return obj.name === name; });
        this.types[iindex].control_name = event;
        var i = this.final_selected_data.findIndex(function (objt) { return objt.name === name; });
        this.final_selected_data[i].control_name = event;
    };
    ConfigTrackerComponent.prototype.onChangeControlType = function (event, name) {
        var iindex = this.types.findIndex(function (obj) { return obj.name === name; });
        this.types[iindex].control_type = event;
        var i = this.final_selected_data.findIndex(function (objt) { return objt.name === name; });
        this.final_selected_data[i].control_type = event;
    };
    ConfigTrackerComponent.prototype.onChangeRisk = function (event, name) {
        var iindex = this.types.findIndex(function (obj) { return obj.name === name; });
        this.types[iindex].riskRank = event;
        var i = this.final_selected_data.findIndex(function (objt) { return objt.name === name; });
        this.final_selected_data[i].riskRank = event;
    };
    ConfigTrackerComponent.prototype.exportpdf = function () {
        //this.ngxService.start();
        this.loginUser = JSON.parse(localStorage.getItem('currentLoginUser'));
        console.log('this.loginUser.username >>>>> ', this.loginUser['username']);
        var queryParams = {
            type: this.selectedHeaderId,
            username: this.loginUser.username,
            module_name: this.selectedHeaderModulename,
            object_name: this.selectedHeaderObjectName,
            selectedUserName: this.selectedUser ? this.selectedUser : '',
            selectedStatus: this.selectedStatus ? this.selectedStatus.value : '',
            configTrackerId: this.configTrackerID,
            uniqueID: this.uniqueID,
            fromDate: this.fromInput.value ? this.fromInput.value : '',
            toDate: this.toInput.value ? this.toInput.value : ''
        };
        this._reportManagementService.getSchedulePDF(queryParams).subscribe(function (res) {
            var blob = new Blob([res.body], { type: 'application/pdf' });
            FileSaver.saveAs(blob, 'ConfigTrackerReport.pdf');
            //this.ngxService.stop();
        }, function (error) {
            //this.ngxService.stop();
            console.log('error:::' + JSON.stringify(error));
        });
    };
    ConfigTrackerComponent.prototype.onSubmit = function (type) {
        var _this = this;
        if (type == 'formSubmit') {
            var s_module = [];
            var obj_name = [];
            s_module = this.CreateForm.controls['module'].value;
            obj_name = this.final_selected_data;
        }
        else if (type == 'formDelete') {
            var s_module = [];
            var obj_name = [];
            s_module = this.selected_modules;
            obj_name = this.selected_datasource;
        }
        this.submitted = true;
        var obj = {
            object_name: obj_name,
            module: s_module,
            userId: this.dSource[0].userId,
            _id: this.dSource[0]._id,
            clientID: this.dSource[0].clientID,
            fusionUrl: this.dSource[0].fusionUrl,
            userName: this.dSource[0].userName,
            password: this.dSource[0].password,
            scheduleType: this.dSource[0].scheduleType,
            scheduleTypeValue: this.dSource[0].scheduleTypeValue,
            retainDays: this.dSource[0].retainDays,
            __v: this.dSource[0].__v,
            queryTypes: _.map(obj_name, 'type'),
            deleted: false
        };
        this.SetupAdministrationService.updateConfigDetails(obj, this.dSource[0].userId).subscribe(function (res) {
            if (res.status == 201) {
                _this.snackBarService.add(res.body.meta.msg);
            }
            else {
                _this.snackBarService.add(res.body.meta.msg);
            }
        }, function (error) {
            _this.errorStatus = error.error.meta.status;
            if (_this.errorStatus == '500' || _this.errorStatus == '400') {
                _this.errorMsg = error.error.meta.msg;
                _this.snackBarService.warning(_this.errorMsg);
                setTimeout(function () {
                    _this.submitted = false;
                }, 3000);
            }
        });
        this.getallconfigs();
    };
    ConfigTrackerComponent.prototype.runData = function () {
        var _this = this;
        var userId = localStorage.getItem('userId');
        var queryParams = {
            clientID: this.clientID,
            userId: userId
        };
        this._reportManagementService
            .startDataPull(queryParams)
            .subscribe(function (data) {
            _this.snackBarService.add('Run/ Sync Data Started Successfully');
        });
    };
    ConfigTrackerComponent.prototype.deleteSelectedConfigsFinal = function (element) {
        var index = this.final_selected_data.findIndex(function (obj) { return obj.name === element.name; });
        this.final_selected_data.splice(index, 1);
        var iindex = this.types.findIndex(function (objt) { return objt.name === element.name; });
        this.types[iindex].checked = false;
        delete this.final_selected_table[element.name];
        this.final_selected_table = new MatTableDataSource(this.final_selected_data);
        this.object_list = new MatTableDataSource(this.types);
        this.final_selected_table.paginator = this.fpaginator;
        this.object_list.paginator = this.paginator;
        this.object_list_length = this.types.length;
        this.final_selected_length = this.final_selected_data.length;
        this.snackBarService.warning('Config Removed Successfully');
    };
    ConfigTrackerComponent.prototype.deleteSelectedConfigs = function (element) {
        var index = this.selected_datasource.findIndex(function (obj) { return obj.name === element.name; });
        this.selected_datasource.splice(index, 1);
        delete this.selected_datasource[element.name];
        this.selected_data = new MatTableDataSource(this.selected_datasource);
        this.selected_data.paginator = this.spaginator;
        this.onSubmit('formDelete');
        this.snackBarService.warning('Config Removed Successfully');
    };
    ConfigTrackerComponent.prototype.getUserNameList = function (tempvalues) {
        var _this = this;
        var objectLists = _.map(this.selectedTabHeader, 'type');
        var queryParams = {
            selectedObject: objectLists
        };
        this._reportManagementService
            .getUpdatedUserList(queryParams)
            .subscribe(function (data) {
            if (data && data.body && data.body['response']) {
                _this.userList = data.body['response'].length
                    ? data.body['response']
                    : [];
                if (tempvalues) {
                    _this.selectedUser = tempvalues.name;
                }
            }
        });
    };
    ConfigTrackerComponent.prototype.Toggle = function (id, tempvalues, typeChart, chartNum) {
        if (this.configQTSetupDone && this.configTrackerSetupDone) {
            if (id == 'config_tracker') {
                this.show_data = 'config_tracker';
            }
            else if (id == 'dashboard') {
                this.show_data = 'dashboard';
                this.changesbymodule();
                this.weekbyuserChart();
                this.monthbyuserChart();
            }
            else {
                this.show_data = 'report_management';
                if (tempvalues && tempvalues != '') {
                    var moduleName = [];
                    if (typeChart === 'module') {
                        moduleName.push(tempvalues.name);
                    }
                    else {
                        moduleName = this.module_RM;
                    }
                    this.onChangeDatasourceId(moduleName, 'dashboard');
                    this.onSelectModuleRM(moduleName, 'redirect');
                    this.getAllObjectsNameList(function () {
                        console.log('GET');
                    });
                    if (typeChart === 'user') {
                        if (chartNum == 1) {
                            this.getUserNameList(tempvalues);
                        }
                        else if (chartNum == 2) {
                            var tempSplitName = tempvalues.name.split('(');
                            tempvalues.name = tempSplitName[0];
                            this.getUserNameList(tempvalues);
                        }
                    }
                }
                //console.log(this._reportManagementService.configTrackerDetails);
            }
        }
    };
    //Chart portions
    ConfigTrackerComponent.prototype.changesbymodule = function () {
        var _this = this;
        this.conftchartbymodule = [];
        var queryParams = {
            modules: this.selected_modules,
            configTrackerID: this.configTrackerID
        };
        this._reportManagementService
            .confTrackByModuleChart(queryParams)
            .subscribe(function (data) {
            var result = _.chain(data.body.response)
                .groupBy('_id.moduleName')
                .toPairs()
                .map(function (objs, key) {
                console.log(objs);
                return {
                    modulename: objs[0],
                    count: _.sumBy(objs[1], 'count')
                };
            })
                .value();
            console.log(result, '...R');
            result.forEach(function (element) {
                var tempArray = {};
                tempArray.name = element.modulename;
                tempArray.value = element.count;
                _this.conftchartbymodule.push(tempArray);
            });
            console.log(_this.conftchartbymodule, '----conftchartbymodule');
            //console.log("Grouped", states);
            /*
            var res = data.response.userConflicts;
            res.forEach(element => {
                const tempArray: any = {};
                tempArray.name = element.username;
                tempArray.value = element.total;
    
                this.ebs_m_chart.push(tempArray);
            });
            */
        });
    };
    ConfigTrackerComponent.prototype.weekbyuserChart = function () {
        var _this = this;
        this.week_by_user_chart = [];
        var queryParams = {
            modules: this.selected_modules,
            configTrackerID: this.configTrackerID
        };
        this._reportManagementService
            .confTrackByUserByWeekChart(queryParams)
            .subscribe(function (data) {
            //console.log("response",data.response);
            var result = _.chain(data.body.response)
                .groupBy('_id.user')
                .toPairs()
                .map(function (objs, key) {
                //console.log(objs);
                return {
                    users: objs[0],
                    count: _.sumBy(objs[1], 'count')
                };
            })
                .value();
            //console.log(result);
            result.forEach(function (element) {
                var tempArray = {};
                tempArray.name = element.users;
                tempArray.value = element.count;
                _this.week_by_user_chart.push(tempArray);
            });
        });
    };
    ConfigTrackerComponent.prototype.monthbyuserChart = function () {
        var _this = this;
        this.month_by_user_chart = [];
        /*
        this.month_by_user_chart = [
            {
              "name": "SISMAIL",
              "value": 75
            },
            {
                "name": "Operation",
                "value": 25
            },
            {
                "name": "XYZ",
                "value": 15
            },
            {
                "name": "MNO",
                "value": 45
            }
          ];
        */
        var queryParams = {
            modules: this.selected_modules,
            configTrackerID: this.configTrackerID
        };
        this._reportManagementService
            .confTrackByUserByMonthChart(queryParams)
            .subscribe(function (data) {
            var result = _.chain(data.body.response)
                .groupBy('_id.user')
                .toPairs()
                .map(function (objs, key) {
                //console.log(objs);
                return {
                    users: objs[0],
                    count: _.sumBy(objs[1], 'count')
                };
            })
                .value();
            var sumOfCount = 0;
            result.forEach(function (element) {
                sumOfCount = sumOfCount + element.count;
            });
            var totalusercount = data.body.response.length;
            //console.log("sum", sumOfCount);
            result.forEach(function (element) {
                var tempArray = {};
                //console.log("count",element.count);
                //console.log("sum", sumOfCount);
                //console.log("total",totalusercount);
                // tempArray.value = ((element.count/sumOfCount)*totalusercount) ;
                tempArray.percentage =
                    parseFloat(((element.count * 100) / sumOfCount).toFixed(2)) + '%';
                tempArray.name = element.users + '(' + tempArray.percentage + ')';
                tempArray.value = element.count;
                _this.month_by_user_chart.push(tempArray);
            });
        });
    };
    ConfigTrackerComponent.prototype.formatPercent = function (c) {
        console.log(c);
        return c.value + '%';
    };
    ConfigTrackerComponent.prototype.axisFormat = function (val) {
        if (val % 1 === 0) {
            return val.toLocaleString();
        }
        else {
            return '';
        }
    };
    ConfigTrackerComponent.prototype.onSelect = function (emitted, typeChart, chartNum) {
        console.log(emitted);
        //this.mname.valuevalue = this.modulename;
        this.userSelectedToggle = 'reportMgmtToggle';
        this.Toggle('report_management', emitted, typeChart, chartNum);
        this._reportManagementService.configTrackerDetails = emitted;
    };
    ConfigTrackerComponent.prototype.close = function () {
        this.types = [];
        this.showedit();
        this.getallconfigs();
        this.mode = 'view';
    };
    ConfigTrackerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-config-tracker',
                    template: "<div class=\"page-layout blank p-12\" fusePerfectScrollbar>\r\n\t<mat-drawer-container class=\"example-container\" autosize fxFlex>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" style=\"background: white\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayout.sm=\"column\" fxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"m-0\">Config Tracker</h2>\r\n\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" style=\"font-size :initial\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t<span class=\"m-0\">Track the configurations from Cloud Application</span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n\t\t\t\tstyle=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: flex-start;\">\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<mat-button-toggle-group name=\"fontStyle\" aria-label=\"Font Style\" value={{show_data}} class=\"mr-12\">\r\n\t\t\t\t\t\t<mat-button-toggle value=\"dashboard\" color=\"primary\" (click)=\"Toggle('dashboard','','', '')\">\r\n\t\t\t\t\t\t\tDashboard </mat-button-toggle>\r\n\t\t\t\t\t\t<mat-button-toggle value=\"config_tracker\" (click)=\"Toggle('config_tracker','','', '')\">\r\n\t\t\t\t\t\t\tConfiguration Tracker</mat-button-toggle>\r\n\t\t\t\t\t\t<mat-button-toggle value=\"report_management\" (click)=\"Toggle('report_management','','', '')\">\r\n\t\t\t\t\t\t\tReport\r\n\t\t\t\t\t\t\tManagement </mat-button-toggle>\r\n\r\n\t\t\t\t\t</mat-button-toggle-group>\r\n\t\t\t\t</div>\r\n\t\t\t\t<button *ngIf=\"show_data == 'report_management'\" style=\"padding: 0px 10px 0px 10px;\r\n\t\t\t\tmargin: 0px 4px 0px 0px;\" mat-raised-button class=\"mat-raised-button mat-warn common-btn\" (click)=\"runData()\">\r\n\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">refresh</mat-icon>Run / Sync Data\r\n\t\t\t\t</button>\r\n\t\t\t\t<button *ngIf=\"show_data == 'report_management'\" mat-raised-button\r\n\t\t\t\t\tclass=\"mat-raised-button mat-warn common-btn\" (click)=\"exportpdf()\">\r\n\t\t\t\t\t<span>Export</span>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\" style=\"padding: 10px 0px 0px 10px; background: white; margin: 1%\"\r\n\t\t\t*ngIf=\"show_data == 'report_management' \">\r\n\r\n\t\t\t<div fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\" >\r\n\t\t\t\t\t<mat-label>Select Module</mat-label>\r\n\t\t\t\t\t<mat-select multiple (selectionChange)=\"onSelectModuleRM($event, null)\"\r\n\t\t\t\t\t\t[(ngModel)]=\"select_modules_RM\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let module of module_RM; let i = index\" [value]=\"module\">{{module}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t</div>\r\n\t\t\t<div  fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" *ngIf=\"show_data == 'report_management'\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-label>Object Name</mat-label>\r\n\t\t\t\t\t<mat-select multiple [(ngModel)]=\"selectedTabHeader\" (selectionChange)=\"updateTab($event,'')\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let type of objectName_RM; let i = index\" [value]=\"type\">{{type.name}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t\t<div  fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-label>Select User</mat-label>\r\n\t\t\t\t\t<mat-select [(ngModel)]=\"selectedUser\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let user of userList; let i = index\" [value]=\"user\">{{user}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t\t<div  fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-label>Select Status</mat-label>\r\n\t\t\t\t\t<mat-select [(ngModel)]=\"selectedStatus\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let status of statusFilter; let i = index\" [value]=\"status\">{{status.name}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"center\"\r\n\t\t\tstyle=\"padding: 10px 0px 0px 10px; background: white; margin: 1%\" *ngIf=\"show_data == 'report_management' \">\r\n\t\t\t<div fxFlex=\"80%\">\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"20%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t\t<input #fromInput matInput [matDatepicker]=\"dp\" placeholder=\"From Date\">\r\n\t\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n\t\t\t\t\t\t<mat-datepicker #dp></mat-datepicker>\r\n\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"20%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t\t<input #toInput matInput [matDatepicker]=\"dp1\" placeholder=\"To Date\">\r\n\t\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp1\"></mat-datepicker-toggle>\r\n\t\t\t\t\t\t<mat-datepicker #dp1></mat-datepicker>\r\n\t\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t</div>\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"10%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<button mat-raised-button class=\"mat-raised-button mat-warn common-btn \"\r\n\t\t\t\t\t\t[disabled]=\"!selectedTabHeader.length\" (click)=\"applyFilter()\">\r\n\t\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">search</mat-icon>Apply\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"10%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<button mat-raised-button class=\"mat-raised-button mat-warn common-btn \" (click)=\"resetFilter()\">\r\n\t\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">refresh</mat-icon>Reset\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div fxFlex=\"15%\">\r\n\t\t\t\t<div *ngIf=\"show_data == 'report_management'\" style=\"width: 140px;\">\r\n\t\t\t\t\t<div style=\"padding: 10px;\" class=\"custom-mat-form\"\r\n\t\t\t\t\t\t*ngIf=\"selectedTableHeader && selectedTableHeader.length && configData && configData.length\">\r\n\t\t\t\t\t\t<button mat-icon-button class=\"mat-warn\" matTooltip=\"Table Settings\" (click)=select.open()>\r\n\t\t\t\t\t\t\t<mat-icon style=\"font-size: 32px;\">settings</mat-icon>\r\n\t\t\t\t\t\t\t<mat-select #select multiple style=\"width: 100px; padding: 23%\"\r\n\t\t\t\t\t\t\t\t[(ngModel)]=\"selectedTableHeader\" (selectionChange)=\"updateTable($event)\">\r\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let columnSetting of tableConfig\" [value]=\"columnSetting['name'] \">\r\n\t\t\t\t\t\t\t\t\t{{columnSetting.name}}</mat-option>\r\n\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<!-- <div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between -webkit-left\" style=\"padding: 10px 0px 0px 10px; width: 50%;\"\r\n\t\t\t*ngIf=\"show_data == 'dashboard' \" >\r\n\t\t\t<div style=\"padding: 10px;\" fxLayout=\"column\" fxFlex=\"10\" fxFlex.gt-sm=\"15\" fxLayoutAlign=\"-webkit-left\"\r\n\t\t\t\tclass=\"custom-mat-form\">\r\n\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t<input matInput [matDatepicker]=\"dp\" placeholder=\"From Date\">\r\n\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n\t\t\t\t\t<mat-datepicker #dp></mat-datepicker>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t\t<div style=\"padding: 10px;\" fxLayout=\"column\" fxFlex=\"10\" fxFlex.gt-sm=\"15\" fxLayoutAlign=\"-webkit-left\"\r\n\t\t\t\tclass=\"custom-mat-form\" >\r\n\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t<input matInput [matDatepicker]=\"dp1\" placeholder=\"To Date\">\r\n\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp1\"></mat-datepicker-toggle>\r\n\t\t\t\t\t<mat-datepicker #dp1></mat-datepicker>\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t</div>\r\n\t\t\t<div style=\"padding: 10px;\" fxLayout=\"column\" fxFlex=\"10\" fxFlex.gt-sm=\"35\" fxLayoutAlign=\"-webkit-left\"\r\n\t\t\t\tclass=\"custom-mat-form\" >\r\n\t\t\t\t<button mat-raised-button class=\"mat-raised-button mat-warn common-btn \" (click)=\"applyFilter()\">\r\n\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">search</mat-icon>Apply\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div> -->\r\n\t\t<div class=\"toolbar p-6 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\tfont-size: initial;    margin: 30px;padding: 1%;\" *ngIf=\"!configTrackerSetupDone\">\r\n\t\t\tTo proceed,\r\n\t\t\t<button mat-button color=\"accent\" (click)=\"openNavigate()\">\r\n\t\t\t\tClick Here</button> to setup the Config Tracker\r\n\t\t</div>\r\n\t\t<div class=\"toolbar p-6 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\tfont-size: initial;    margin: 30px;padding: 1%;\" *ngIf=\"configTrackerSetupDone && !configQTSetupDone\">\r\n\t\t\tClick Add Button to New Module and Object Names.\r\n\t\t</div>\r\n\t\t<div class=\"content p-24\" fusePerfectScrollbar *ngIf=\"show_data == 'config_tracker' \">\r\n\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white\">\r\n\r\n\t\t\t\t<div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t<div></div>\r\n\t\t\t\t\t<div fxFlex class=\"h3\" style=\"padding: 10px;\">Configuration Details &nbsp;&nbsp;\r\n\t\t\t\t\t\t<button mat-raised-button color='accent' (click)=\"showedit()\"\r\n\t\t\t\t\t\t\t*ngIf=\"formAction == 'add' && configTrackerSetupDone\"\r\n\t\t\t\t\t\t\tstyle=\"float:right;font-size: 15px;margin-top: -1%;\">\r\n\t\t\t\t\t\t\t<mat-icon style=\"font-size: 20px;padding: 2px;\">add</mat-icon>Add\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t<button mat-raised-button color='accent' (click)=\"showedit()\" *ngIf=\"formAction == 'edit'\"\r\n\t\t\t\t\t\t\tstyle=\"float:right;font-size: 15px;margin-top: -1%;\">\r\n\t\t\t\t\t\t\t<mat-icon style=\"font-size: 20px;padding: 2px;\">edit</mat-icon>Edit\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<mat-horizontal-stepper class=\"mat-elevation-z4\" [linear]=\"true\" *ngIf=\"mode == 'edit'\" style=\"\r\n\t\t\t\tpadding-left: 3%;padding-right: 3%;\">\r\n\t\t\t\t\t<mat-step [stepControl]=\"selectControl\" style=\"margin-left: 2%;\">\r\n\t\t\t\t\t\t<div mat-dialog-content class=\"p-14 pb-0 m-0\" fusePerfectScrollbar>\r\n\t\t\t\t\t\t\t<form style=\"padding-bottom: 22px;\" fxLayout=\"column\" [formGroup]=\"CreateForm\">\r\n\t\t\t\t\t\t\t\t<ng-template matStepLabel>Set Configuration</ng-template>\r\n\t\t\t\t\t\t\t\t<div layout=\"row\" md-content layout-padding>\r\n\t\t\t\t\t\t\t\t\t<mat-label style=\"width:25%;margin-top:1%\">Module Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"30\" class=\"mr-8\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Select Module Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<mat-select required placeholder=\"Select Module Name\" formControlName=\"module\"\r\n\t\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"selected_modules\" multiple\r\n\t\t\t\t\t\t\t\t\t\t\t(selectionChange)=\"onChangeDatasourceId($event,'')\"\r\n\t\t\t\t\t\t\t\t\t\t\t[(value)]=\"selected_modules\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let md of modules\" [value]=\"md._id\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{md._id}}\r\n\t\t\t\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<p *ngIf=\"submitted && CreateForm.get('module').invalid\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-error class=\"error_margin\">\r\n\t\t\t\t\t\t\t\t\t\t\tModule Name is required!\r\n\t\t\t\t\t\t\t\t\t\t</mat-error>\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t<table mat-table [dataSource]=\"object_list\" class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"select\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"width:10%;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tSelect\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"padding-top: 9px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox (change)=\"onChangeType(null,$event,'all')\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"margin-top: 27%;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"width:2%;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox (change)=\"onChangeType(element,$event,'single')\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t[checked]=element.checked>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"padding-top: 25px;width:35%;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tObject Name\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"padding-top: 9px;max-width: 65%;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Object Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input matInput\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(input)=\"onSearchObjectName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:28%;font-size: 14px;    padding: 0px 0px 56px 0;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Name\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 10px 0px 0px 0px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Control Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input matInput formControlName=\"control_name\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t[(value)]=\"element.control_name\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t(change)=\"onChangeName($event.target.value,element.name)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_type\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:15%;font-size: 14px;    padding: 0px 0px 56px 0;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Type\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 10px 0px 0px 0px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Select Control Type</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-select [(value)]=element.control_type\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Select Control Type\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let ct of controlTypes; let i = index\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue=\"{{ct.name}}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"onChangeControlType(ct.name,element.name)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{ct.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"riskRank\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:15%;font-size: 14px;    padding: 0px 0px 56px 0;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tRisk Ranking\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 10px 0px 0px 0px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Select Risk Rank</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-select [(value)]=element.riskRank\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Select Risk Rank\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let risk of riskRanks; let i = index\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue=\"{{risk.name}}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"onChangeRisk(risk.name,element.name)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{risk.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayObjects\"></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayObjects;\"></tr>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t<mat-paginator #paginator [pageSizeOptions]=\"[5]\" [pageIndex]=0\r\n\t\t\t\t\t\t\t\t\t\t[length]=\"object_list_length\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\t\t\t\t\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"m-0\"></h2>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t<button class=\"mr-8\" mat-raised-button matStepperNext type=\"button\"\r\n\t\t\t\t\t\t\t\t\t\t\tcolor=\"accent\" (click)=\"ConfirmData()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-icon>navigate_next</mat-icon>Next\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" *ngIf=\"formAction == 'view'\" fxlayout=\"row\"\r\n\t\t\t\t\t\t\t\t\t\tfxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-step>\r\n\t\t\t\t\t<mat-step style=\"margin-right: 2%;\">\r\n\t\t\t\t\t\t<form>\r\n\t\t\t\t\t\t\t<ng-template matStepLabel>Confirmation</ng-template>\r\n\t\t\t\t\t\t\t<div class=\"content p-12\" fusePerfectScrollbar>\r\n\t\t\t\t\t\t\t\t<div class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t<table mat-table [dataSource]=\"final_selected_table\" class=\"mat-elevation-z8\">\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Name </th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.control_name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_type\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Type</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.control_type}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"riskRank\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tRisk Ranking</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.riskRank}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"module_name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tModule Name </th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.module_name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\"> Object Name\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"action\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\"> Action\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"deleteSelectedConfigsFinal(element)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<button mat-icon-button color='warn'>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon>delete</mat-icon>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayColumns\"></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayColumns;\"></tr>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t<!-- <mat-paginator #fpaginator [pageSizeOptions]=\"[5]\" [length]=\"final_selected_length\" [pageIndex]=0\r\n\t\t\t\t\t\t\t\t showFirstLastButtons></mat-paginator>\t -->\r\n\t\t\t\t\t\t\t\t\t<mat-paginator #fpaginator [pageSizeOptions]=\"[5]\" [length]=\"final_selected_length\"\r\n\t\t\t\t\t\t\t\t\t\t[pageIndex]=0 [pageSize]=\"parentLimit\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\t\t\t\t\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"m-0\"></h2>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<button class=\"mr-8\" mat-raised-button matStepperPrevious type=\"button\"\r\n\t\t\t\t\t\t\t\t\t\t\tcolor=\"accent\" (click)=\"PreviousData()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-icon>navigate_before</mat-icon>Previous\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t<button mat-raised-button class=\"mr-4\" color=\"warn\" style=\"float:right\"\r\n\t\t\t\t\t\t\t\t\t\t\t(click)=\"onSubmit('formSubmit')\">\r\n\t\t\t\t\t\t\t\t\t\t\tSubmit\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" *ngIf=\"formAction == 'view'\" fxlayout=\"row\"\r\n\t\t\t\t\t\t\t\t\t\tfxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t\t</mat-step>\r\n\t\t\t\t</mat-horizontal-stepper>\r\n\t\t\t\t<div mat-dialog-content class=\"p-24 pb-0 m-0\" fusePerfectScrollbar *ngIf=\"mode == 'view'\">\r\n\t\t\t\t\t<div class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t<table mat-table [dataSource]=\"selected_data\" class=\"mat-elevation-z8\">\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_name\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tControl Name\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Control Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.control_name}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_type\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tControl Type\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Control Type</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.control_type}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"riskRank\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tRisk Ranking\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Risk Ranking</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.riskRank}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"module_name\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tModule Name\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Module Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.module_name}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"name\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\"> Object\r\n\t\t\t\t\t\t\t\t\tName\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Object Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.name}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<!-- <ng-container matColumnDef=\"action\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-bottom: 5%;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tAction\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\">\r\n\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" (click)=\"deleteSelectedConfigs(element)\">\r\n\t\t\t\t\t\t\t\t\t<button mat-icon-button color='warn'>\r\n\t\t\t\t\t\t\t\t\t\t<mat-icon>delete</mat-icon>\r\n\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container> -->\r\n\r\n\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayColumnsList\"></tr>\r\n\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayColumnsList;\"></tr>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t<mat-paginator #spaginator [pageSizeOptions]=\"[10]\" [length]=\"selected_data_length\"\r\n\t\t\t\t\t\t\t[pageIndex]=0 [pageSize]=\"parentLimit\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"content p-6\" fusePerfectScrollbar\r\n\t\t\t*ngIf=\"show_data == 'report_management' && configTrackerSetupDone && configQTSetupDone\">\r\n\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white\"\r\n\t\t\t\t*ngIf=\"selectedTabHeader && selectedTabHeader.length\">\r\n\t\t\t\t<mat-tab-group (selectedTabChange)=\"changeTab($event)\" *ngIf=\"expandElement == false\"\r\n\t\t\t\t\tstyle=\"overflow: hidden;\">\r\n\t\t\t\t\t<mat-tab *ngFor=\"let type of selectedTabHeader\" label=\"{{type.name}}\">\r\n\t\t\t\t\t\t<div *ngIf=\"configData && configData.length\">\r\n\t\t\t\t\t\t\t<table mat-table [dataSource]=\"configData\" multiTemplateDataRows class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"{{column}}\" *ngFor=\"let column of configTrackerHeader\">\r\n\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding: 0px 0px 0px 20px;\">\r\n\t\t\t\t\t\t\t\t\t\t{{column | titlecase}} </th>\r\n\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 15px\">\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'History'\">\r\n\t\t\t\t\t\t\t\t\t\t\t<button mat-button style=\"padding-bottom: 10%\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t*ngIf=\"element !== expandedElement\" [disabled]=\"!element.isChanged\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"openHistory(element)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <mat-icon *ngIf= \"element.isChanged\" style=\"font-size: 35px;    margin: 0px 3px;color: #D35400\">history</mat-icon> -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <span  > -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<img *ngIf=\"element.isChanged\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tsrc=\"../../assets/icons/gif-icons/source-1.gif\" alt=\"HTML5 Icon\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:30px;height:30px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- </span> -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <mat-icon *ngIf= \"!element.isChanged\" style=\"font-size: 35px;    margin: 0px 3px;color:#a43dcc\">history</mat-icon> -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<img *ngIf=\"!element.isChanged\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tsrc=\"../../assets/icons/gif-icons/source-2.png\" alt=\"HTML5 Icon\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:30px;height:30px;\">\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t\t<!-- <button mat-button color=\"accent\" *ngIf=\"element === expandedElement\" (click)=\"showHistory('false')\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">expand_less</mat-icon>\r\n\t\t\t\t\t\t\t\t\t\t\t</button> -->\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t<!-- <span *ngIf=\"column === 'Status' && historyConfigData && historyConfigData.length\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tChanged\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' && !historyConfigData\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tNot Changed\r\n\t\t\t\t\t\t\t\t\t\t</span> -->\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' &&  element.isChanged && !element.isAdded\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"label bg-success\"> Changed </span>\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' && !element.isChanged && !element.isAdded\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"label bg-running\" style=\"background: #467696\"> Not Changed </span>\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' &&  element.isAdded\" class=\"label bg-success\"\r\n\t\t\t\t\t\t\t\t\t\t\tstyle=\"background: #d32c79\"> Created </span>\r\n\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t*ngIf=\"element[column] && element[column].name\">{{element[column].name}}</span>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"expandedDetail\">\r\n\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" [attr.colspan]=\"configTrackerHeader.length\">\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"example-element-detail\"\r\n\t\t\t\t\t\t\t\t\t\t\t[@detailExpand]=\"element === expandedElement ? 'expanded' : 'collapsed'\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"example-element-description-attribution\" style=\"width: 100%;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tpadding: 10px 10px 10px 50px; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <mat-drawer-container class=\"example-container\" autosize fxFlex [hasBackdrop]=\"false\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"content p-12\" fusePerfectScrollbar style=\"background: #e2e2ed\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"m-0\" style=\"font-size: 16px;\">History of Changes</span>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table mat-table [dataSource]=\"historyConfigData\" multiTemplateDataRows\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"{{column1}}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t*ngFor=\"let column1 of configTrackerHeader\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef > \r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column1 !== 'History' && column1 !== 'Status'\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{column1 | uppercase}}\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span> </th>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element1\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t[ngClass]=\"(element1[column1] && element1[column1].isChanged)?'class3':'class1'\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"padding: 20px; text-align: -webkit-left\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t*ngIf=\"element1[column1] && element1[column1].name\">{{element1[column1].name}}</span>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"configTrackerHeader\" ></tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let element; columns: configTrackerHeader;\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"example-element-row\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-drawer-container> -->\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"configTrackerHeader\"></tr>\r\n\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let element; columns: configTrackerHeader;\"\r\n\t\t\t\t\t\t\t\t\tclass=\"example-element-row\"\r\n\t\t\t\t\t\t\t\t\t[class.example-expanded-row]=\"expandedElement === element\"\r\n\t\t\t\t\t\t\t\t\t(click)=\"setStep(element)\">\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\"\r\n\t\t\t\t\t\t\t\t\tclass=\"example-detail-row\">\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t<mat-paginator #reportpaginator [pageSizeOptions]=\"[10]\" [length]=\"length\" [pageIndex]=0\r\n\t\t\t\t\t\t\t\t[pageSize]=\"limit\" (page)=\"changePage($event)\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div *ngIf=\"configData && configData.length === 0\">\r\n\t\t\t\t\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\t\t\t\t\t\t\tfont-size: large;\">\r\n\t\t\t\t\t\t\t\tNo Reports Found!\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-tab>\r\n\t\t\t\t</mat-tab-group>\r\n\t\t\t\t<!-- <mat-drawer-container class=\"example-container\" autosize fxFlex [hasBackdrop]=\"false\"\r\n\t\t\t\t*ngIf=\"expandElement == true\"> -->\r\n\t\t\t\t<!-- <div class=\"content p-12\" fusePerfectScrollbar style=\"background: #e2e2ed\"> -->\r\n\t\t\t\t<!-- <div class=\"header-top ctrl-create header p-24 mat-elevation-z8 bg-white\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\"> -->\r\n\t\t\t\t<!-- <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t<span class=\"m-0\" style=\"font-size: 16px;\">History of Changes</span>\r\n\t\t\t\t\t</div> -->\r\n\t\t\t\t<!-- </div> -->\r\n\t\t\t\t<!-- </div> -->\r\n\t\t\t\t<!-- <table mat-table [dataSource]=\"historyConfigData\" multiTemplateDataRows class=\"mat-elevation-z8\">\r\n\t\t\t\t\t<ng-container matColumnDef=\"{{column1}}\" *ngFor=\"let column1 of configTrackerHeader\">\r\n\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef>\r\n\t\t\t\t\t\t\t<span *ngIf=\"column1 !== 'History' && column1 !== 'Status'\">\r\n\t\t\t\t\t\t\t\t{{column1 | uppercase}}\r\n\t\t\t\t\t\t\t</span> </th>\r\n\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element1\"\r\n\t\t\t\t\t\t\t[ngClass]=\"(element1[column1] && element1[column1].isChanged)?'class3':'class1'\"\r\n\t\t\t\t\t\t\tstyle=\"padding: 20px; text-align: -webkit-left\">\r\n\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t*ngIf=\"element1[column1] && element1[column1].name\">{{element1[column1].name}}</span>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</ng-container>\r\n\r\n\r\n\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"configTrackerHeader\"></tr>\r\n\t\t\t\t\t<tr mat-row *matRowDef=\"let element; columns: configTrackerHeader;\" class=\"example-element-row\">\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</table> -->\r\n\t\t\t\t<!-- </mat-drawer-container> -->\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\t\tfont-style: italic;\r\n\t\t\tfont-size: large;\" *ngIf=\"selectedTabHeader && !selectedTabHeader.length\">\r\n\t\t\t\tSelect Module and Object Name\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div *ngIf=\"show_data == 'dashboard' && configTrackerSetupDone && configQTSetupDone\" style=\"padding: 1%;\">\r\n\t\t\t<div style=\" width: 100%;\r\n\t\t\t\tflex-direction: row;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\">\r\n\t\t\t\t<fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" style=\"flex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tflex: 1 1 100%;\r\n\t\t\t\tmax-width: 33.3%;\">\r\n\t\t\t\t\t<div class=\"fuse-widget-front\">\r\n\t\t\t\t\t\t<div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" style=\"background: lightgrey;\"\r\n\t\t\t\t\t\t\tfxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t\t\t<div></div>\r\n\t\t\t\t\t\t\t<div fxFlex class=\"py-16 h3\">Changes Happened in Last 1 week by Modules</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"33\">\r\n\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"conftchartbymodule && conftchartbymodule.length > 0\">\r\n\t\t\t\t\t\t\t\t<ngx-charts-bar-vertical [view]=\"weekview\" [scheme]=\"colorScheme\"\r\n\t\t\t\t\t\t\t\t\t[results]=\"conftchartbymodule\" [gradient]=\"gradient\" [yAxis]=\"showYAxis\"\r\n\t\t\t\t\t\t\t\t\t[xAxis]=\"showXAxis\" [yAxisTickFormatting]=\"axisFormat\"\r\n\t\t\t\t\t\t\t\t\t[showXAxisLabel]=\"showXAxisLabel\" [showYAxisLabel]=\"showYAxisLabel\"\r\n\t\t\t\t\t\t\t\t\t[showGridLines]=\"true\" (select)=\"onSelect($event, 'module', 0)\"\r\n\t\t\t\t\t\t\t\t\t[xAxisLabel]=\"xlabel\" [yAxisLabel]=\"ylabel\">\r\n\t\t\t\t\t\t\t\t</ngx-charts-bar-vertical>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"conftchartbymodule.length == 0\">\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t<span class=\"mat-caption\">There is No Chart Data Found</span>\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</fuse-widget>\r\n\t\t\t\t<fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" style=\"flex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tflex: 1 1 100%;\r\n\t\t\t\tmax-width: 33.3%;\">\r\n\t\t\t\t\t<div class=\"fuse-widget-front\">\r\n\t\t\t\t\t\t<div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" style=\"background: lightgrey;\"\r\n\t\t\t\t\t\t\tfxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t\t\t<div></div>\r\n\t\t\t\t\t\t\t<div fxFlex class=\"py-16 h3\">Changes Happened in Last 1 Week by Users</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"33\">\r\n\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"week_by_user_chart.length > 0\">\r\n\t\t\t\t\t\t\t\t<ngx-charts-bar-vertical [view]=\"weekview\" [scheme]=\"colorScheme\"\r\n\t\t\t\t\t\t\t\t\t[results]=\"week_by_user_chart\" [gradient]=\"gradient\" [yAxis]=\"showYAxis\"\r\n\t\t\t\t\t\t\t\t\t[xAxis]=\"showXAxis\" [showXAxisLabel]=\"showXAxisLabel\"\r\n\t\t\t\t\t\t\t\t\t[yAxisTickFormatting]=\"axisFormat\" [showYAxisLabel]=\"showYAxisLabel\"\r\n\t\t\t\t\t\t\t\t\t[barPadding]=\"barPadding\" [showGridLines]=\"true\"\r\n\t\t\t\t\t\t\t\t\t(select)=\"onSelect($event, 'user', 1)\" [xAxisLabel]=\"xlabelweek\"\r\n\t\t\t\t\t\t\t\t\t[yAxisLabel]=\"ylabel\">\r\n\t\t\t\t\t\t\t\t</ngx-charts-bar-vertical>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"week_by_user_chart.length == 0\">\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t<span class=\"mat-caption\">There is No Chart Data Found</span>\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</fuse-widget>\r\n\r\n\t\t\t\t<fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" fxLayout=\"column\" class=\"widget\" style=\"flex-direction: row;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tflex: 1 1 100%;\r\n\t\t\t\tmax-width: 34.3%;\">\r\n\t\t\t\t\t<div class=\"fuse-widget-front\">\r\n\t\t\t\t\t\t<div class=\"px-32 border-bottom\" fxLayout=\"row wrap\" style=\"background: lightgrey;\"\r\n\t\t\t\t\t\t\tfxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t\t\t<div></div>\r\n\t\t\t\t\t\t\t<div fxFlex class=\"py-16 h3\">Changes Happened in Last 1 month by Users</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"33\">\r\n\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"month_by_user_chart.length > 0\">\r\n\t\t\t\t\t\t\t\t<ngx-charts-pie-chart [view]=\"pieview\" [scheme]=\"colorPieScheme\"\r\n\t\t\t\t\t\t\t\t\t[results]=\"month_by_user_chart\" [legend]=\"showPieLegend\"\r\n\t\t\t\t\t\t\t\t\t[legendPosition]=\"legendPosition\" [legendTitle]=\"legendTitle\" [labels]=\"showLabels\"\r\n\t\t\t\t\t\t\t\t\t[doughnut]=\"doughnut\" [gradient]=\"gradient\" (select)=\"onSelect($event, 'user', 2)\">\r\n\t\t\t\t\t\t\t\t</ngx-charts-pie-chart>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"month_by_user_chart.length == 0\">\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t<span class=\"mat-caption\">There is No Chart Data Found</span>\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</fuse-widget>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t</mat-drawer-container>\r\n</div>",
                    animations: [
                        trigger('detailExpand', [
                            state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
                            state('expanded', style({ height: '*' })),
                            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
                        ]),
                        fuseAnimations
                    ],
                    providers: [
                        // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
                        // application's root module. We provide it at the component level here, due to limitations of
                        // our example generation script.
                        {
                            provide: DateAdapter,
                            useClass: MomentDateAdapter,
                            deps: [MAT_DATE_LOCALE]
                        },
                        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
                    ],
                    styles: [".chart-legend{display:inline-block!important;padding:0!important;width:auto!important;text-align:center!important}.col-4{flex-basis:33.333%}.parentBox{background:#fff;border-radius:10px}.child-border-bottom{border-bottom:1px solid rgba(0,0,0,.12)}.class1{background:#fff}.class2{background:#f0e149}.class3{background:#c7c2ba}table{width:100%}tr.example-detail-row{height:0}tr.example-element-row:not(.example-expanded-row):hover{background:#f5f5f5}tr.example-element-row:not(.example-expanded-row):active{background:#efefef}.example-element-row td{border-bottom-width:0}.example-element-detail{overflow:hidden;display:-webkit-box;display:flex}.example-element-diagram{min-width:80px;border:2px solid #000;padding:8px;font-weight:lighter;margin:8px 0;height:104px}.example-element-symbol{font-weight:700;font-size:40px;line-height:normal}.example-element-description{padding:16px}.example-element-description-attribution{opacity:.8}div.mat-horizontal-stepper-header-container{margin-right:2%!important;margin-left:2%!important}.inner{display:block;width:100%;text-align:center;margin-top:19px;min-height:372px}.inner img{width:auto;max-width:100%}ngx-charts-legend.chart-legend{width:auto;clear:both;display:block;margin:0 auto}ngx-charts-legend.chart-legend>div{display:block;width:auto!important}ngx-charts-legend.chart-legend .legend-wrap{min-width:200pxf;text-align:initial;margin:0 auto;float:none;padding-right:33px}ngx-charts-legend.chart-legend .legend-title{display:none}ngx-charts-legend.chart-legend .legend-labels{display:inline;text-align:center;margin:0 auto;background:0 0;float:none;padding:0;white-space:inherit}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important}.drawer_style{padding:3px;-webkit-transform:none;transform:none;visibility:visible;width:27%;height:100%;overflow-x:hidden}body.theme-default .mat-button-toggle-checked{background-color:blue-grey!important}.fuse-widget-front{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;position:relative;overflow:hidden;visibility:visible;width:100%;opacity:1;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(0);transform:rotateY(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}"]
                }] }
    ];
    /** @nocollapse */
    ConfigTrackerComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: MatDialog },
        { type: Router },
        { type: SnackBarService },
        { type: ReportManagementService },
        { type: SetupAdministrationService },
        { type: MessageService },
        { type: NgxUiLoaderService }
    ]; };
    ConfigTrackerComponent.propDecorators = {
        paginator: [{ type: ViewChild, args: ['paginator',] }],
        spaginator: [{ type: ViewChild, args: ['spaginator',] }],
        fpaginator: [{ type: ViewChild, args: ['fpaginator',] }],
        reportpaginator: [{ type: ViewChild, args: ['reportpaginator',] }],
        fromInput: [{ type: ViewChild, args: ['fromInput', {
                        read: MatInput
                    },] }],
        toInput: [{ type: ViewChild, args: ['toInput', {
                        read: MatInput
                    },] }]
    };
    return ConfigTrackerComponent;
}());
export { ConfigTrackerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLXRyYWNrZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImF2bS9jb25maWctdHJhY2tlci9jb25maWctdHJhY2tlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBRSxTQUFTLEVBQVUsU0FBUyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsWUFBWSxFQUFFLGtCQUFrQixFQUFFLFFBQVEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQy9FLE9BQU8sRUFDTCxTQUFTLEVBSVYsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQixPQUFPLEVBQ0wsV0FBVyxFQUNYLFNBQVMsRUFFVCxXQUFXLEVBQ1osTUFBTSxnQkFBZ0IsQ0FBQztBQUN4QixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUN2RSxPQUFPLEtBQUssU0FBUyxNQUFNLFlBQVksQ0FBQztBQUN4QyxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSx5REFBeUQsQ0FBQztBQUVqRyxPQUFPLEVBQ0wsT0FBTyxFQUNQLEtBQUssRUFDTCxLQUFLLEVBQ0wsVUFBVSxFQUNWLE9BQU8sRUFDUixNQUFNLHFCQUFxQixDQUFDO0FBQzdCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekMsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRXhELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFDTCxXQUFXLEVBQ1gsZ0JBQWdCLEVBQ2hCLGVBQWUsRUFDaEIsTUFBTSx3QkFBd0IsQ0FBQztBQUNoQyxPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQztBQUNsQyxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFFdkIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBRWpFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGVBQWUsQ0FBQyxDQUFDLDRCQUE0QjtBQUNoRixPQUFPLEVBQUMsY0FBYyxFQUFDLE1BQU0sWUFBWSxDQUFDO0FBQzFDLE1BQU0sQ0FBQyxJQUFNLFVBQVUsR0FBRztJQUN4QixLQUFLLEVBQUU7UUFDTCxTQUFTLEVBQUUsSUFBSTtLQUNoQjtJQUNELE9BQU8sRUFBRTtRQUNQLFNBQVMsRUFBRSxJQUFJO1FBQ2YsY0FBYyxFQUFFLFVBQVU7UUFDMUIsYUFBYSxFQUFFLElBQUk7UUFDbkIsa0JBQWtCLEVBQUUsV0FBVztLQUNoQztDQUNGLENBQUM7QUFZRjtJQXNSRSxnQ0FDVSxXQUF3QixFQUN4QixVQUFxQixFQUNyQixNQUFjLEVBQ2QsZUFBZ0MsRUFDaEMsd0JBQWlELEVBQ2pELDBCQUFzRCxFQUV0RCxjQUE4QixFQUM5QixVQUE4QjtRQVI5QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixlQUFVLEdBQVYsVUFBVSxDQUFXO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUF5QjtRQUNqRCwrQkFBMEIsR0FBMUIsMEJBQTBCLENBQTRCO1FBRXRELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQS9QeEMsYUFBUSxHQUFVLEVBQUUsQ0FBQztRQUdyQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBSWxCLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFJYix3QkFBbUIsR0FBRyxFQUFFLENBQUM7UUFLekIsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQVlmLG1CQUFjLEdBQWEsRUFBRSxDQUFDO1FBQzlCLHFCQUFnQixHQUFhLENBQUMsUUFBUSxFQUFFLGFBQWEsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN0RSxtQkFBYyxHQUFhO1lBQ3pCLGFBQWE7WUFDYixNQUFNO1lBQ04sY0FBYztZQUNkLGNBQWM7WUFDZCxVQUFVO1lBQ1YsUUFBUTtTQUNULENBQUM7UUFDRix1QkFBa0IsR0FBYTtZQUM3QixhQUFhO1lBQ2IsTUFBTTtZQUNOLGNBQWM7WUFDZCxjQUFjO1lBQ2QsVUFBVTtTQUNYLENBQUM7UUFDRixtQkFBYyxHQUFhO1lBQ3pCLFFBQVE7WUFDUixNQUFNO1lBQ04sY0FBYztZQUNkLGNBQWM7WUFDZCxVQUFVO1NBQ1gsQ0FBQztRQUNGLG9CQUFlLEdBQWE7WUFDMUIsa0JBQWtCO1lBQ2xCLGFBQWE7WUFDYixhQUFhO1NBQ2QsQ0FBQztRQUtGLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBRS9CLGdCQUFXLEdBQVMsRUFBRSxDQUFDO1FBcUJ2QixZQUFPLEdBQWM7WUFDbkI7Z0JBQ0UsTUFBTSxFQUFFLElBQUk7Z0JBQ1osZ0JBQWdCLEVBQUUsZ0JBQWdCO2dCQUNsQyxXQUFXLEVBQUUscUJBQXFCO2dCQUNsQyxXQUFXLEVBQUUsRUFBRTthQUNoQjtZQUNELGtIQUFrSDtTQUNuSCxDQUFDO1FBRUYsZ0JBQVcsR0FBRyxJQUFJLFNBQVMsQ0FBQztZQUMxQixRQUFRLEVBQUUsSUFBSSxXQUFXLEVBQUU7U0FDNUIsQ0FBQyxDQUFDO1FBS0gsZUFBVSxHQUFHO1lBQ1g7Z0JBQ0UsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsTUFBTSxFQUFFLEVBQUU7Z0JBQ1YsV0FBVyxFQUFFLEVBQUU7Z0JBQ2YsU0FBUyxFQUFFLEVBQUU7Z0JBQ2IsWUFBWSxFQUFFLEVBQUU7Z0JBQ2hCLGVBQWUsRUFBRSxFQUFFO2dCQUNuQixVQUFVLEVBQUUsRUFBRTthQUNmO1NBQ0YsQ0FBQztRQUVGLGlCQUFZLEdBQUc7WUFDYjtnQkFDRSxLQUFLLEVBQUUsS0FBSztnQkFDWixJQUFJLEVBQUUsS0FBSzthQUNaO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLE9BQU87Z0JBQ2QsSUFBSSxFQUFFLE9BQU87YUFDZDtZQUNEO2dCQUNFLEtBQUssRUFBRSxNQUFNO2dCQUNiLElBQUksRUFBRSxNQUFNO2FBQ2I7U0FDRixDQUFDO1FBQ0YsY0FBUyxHQUFHO1lBQ1Y7Z0JBQ0UsS0FBSyxFQUFFLFVBQVU7Z0JBQ2pCLElBQUksRUFBRSxVQUFVO2FBQ2pCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsSUFBSSxFQUFFLE1BQU07YUFDYjtZQUNEO2dCQUNFLEtBQUssRUFBRSxRQUFRO2dCQUNmLElBQUksRUFBRSxRQUFRO2FBQ2Y7WUFDRDtnQkFDRSxLQUFLLEVBQUUsS0FBSztnQkFDWixJQUFJLEVBQUUsS0FBSzthQUNaO1NBQ0YsQ0FBQztRQVFGLGlIQUFpSDtRQUNqSCxvQkFBZSxHQUFhO1lBQzFCLFVBQVU7WUFDVixXQUFXO1lBQ1gsUUFBUTtZQUNSLGFBQWE7WUFDYixXQUFXO1lBQ1gsY0FBYztZQUNkLGlCQUFpQjtZQUNqQixZQUFZO1NBQ2IsQ0FBQztRQUVGLGNBQVMsR0FBVyxXQUFXLENBQUM7UUFDaEMscUJBQWdCLEdBQWEsRUFBRSxDQUFDO1FBRWhDLGFBQVEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ3JDLFdBQU0sR0FBRyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO1FBQ25DLGdCQUFXLEdBQVEsY0FBYyxDQUFDLFVBQVUsQ0FBQztRQUU3QyxzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFDNUIsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFFdEIsd0JBQW1CLEdBQVEsRUFBRSxDQUFDO1FBQzlCLGdCQUFXLEdBQUcsY0FBYyxDQUFDLFdBQVcsQ0FBQztRQUN6QyxnQkFBVyxHQUFRLEVBQUUsQ0FBQztRQUN0QixzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFFNUIsU0FBSSxHQUFHLENBQUMsQ0FBQztRQU9ULHVCQUFrQixHQUFhLEVBQUUsQ0FBQztRQUdsQyxnQkFBVyxHQUFRLEVBQUUsQ0FBQztRQUN0QixpQkFBWSxHQUFRLEVBQUUsQ0FBQztRQUN2Qix1QkFBa0IsR0FBUSxFQUFFLENBQUM7UUFDN0Isd0JBQW1CLEdBQVEsRUFBRSxDQUFDO1FBQzlCLE1BQU07UUFDTixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBRXRCLFdBQU0sR0FBRyxTQUFTLENBQUM7UUFDbkIsV0FBTSxHQUFHLFNBQVMsQ0FBQztRQUNuQixlQUFVLEdBQUcsT0FBTyxDQUFDO1FBQ3JCLGVBQVUsR0FBRyxTQUFTLENBQUM7UUFDdkIsYUFBYTtRQUNiLFVBQVU7UUFDVixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QixtQkFBYyxHQUFHLE9BQU8sQ0FBQztRQUN6QixTQUFJLEdBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDekIsYUFBUSxHQUFVLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzdCLFlBQU8sR0FBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM1QixnQkFBVyxHQUFHLE9BQU8sQ0FBQztRQUN0QixxQkFBZ0IsR0FBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNyQyxtQkFBYyxHQUFHO1lBQ2YsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUM7U0FDM0UsQ0FBQztRQUNGLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsV0FBTSxHQUFXLENBQUMsQ0FBQztRQUVuQixNQUFNO1FBQ04sZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixrQkFBYSxHQUFHLEtBQUssQ0FBQztRQUN0QiwyQkFBc0IsR0FBRyxLQUFLLENBQUM7UUFDL0Isc0JBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQzFCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUNsQixtQkFBbUI7UUFDbkIsZUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNoQixnQkFBVyxHQUFHO1lBQ1osTUFBTSxFQUFFLENBQUMsU0FBUyxDQUFDO1NBQ3BCLENBQUM7UUFLRiw0QkFBdUIsR0FBUSxFQUFFLENBQUM7UUFvQmhDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7WUFDdkMsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ1osTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ1osUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2QsV0FBVyxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2pCLFlBQVksRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNsQixZQUFZLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDbEIsUUFBUSxFQUFFLENBQUMsRUFBRSxDQUFDO1NBQ2YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELHlDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsNkJBQTZCO1FBQzdCLEtBQUs7UUFDTCwrQkFBK0I7UUFDL0Isa0NBQWtDO1FBQ2xDLE9BQU87UUFDUCw0QkFBNEI7UUFDNUIsOEJBQThCO1FBQzlCLEtBQUs7UUFDTCxLQUFLO1FBQ0wsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7SUFDckIsQ0FBQztJQUNELDRDQUFXLEdBQVgsVUFBWSxPQUFPO1FBQ2pCLDJFQUEyRTtRQUMzRSxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3ZDLDBEQUEwRDtRQUUxRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQzVELFlBQVksRUFBRSxJQUFJO1lBQ2xCLEtBQUssRUFBRSxLQUFLO1lBQ1osVUFBVSxFQUFFLHFCQUFxQjtZQUNqQyxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLEtBQUs7Z0JBQ2IsV0FBVyxFQUFFLE9BQU8sQ0FBQyxXQUFXO2dCQUNoQyxVQUFVLEVBQUUsT0FBTztnQkFDbkIsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7Z0JBQ3JDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVzthQUM5QjtTQUNGLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCw2Q0FBWSxHQUFaO1FBQ0UsSUFBSSxHQUFHLEdBQUc7WUFDUixHQUFHLEVBQUcsMkNBQTJDO1lBQ2pELEVBQUUsRUFBRywwQkFBMEI7U0FDaEMsQ0FBQTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsMkNBQTJDLENBQUMsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7SUFDRCw4Q0FBYSxHQUFiO1FBQUEsaUJBa0RDO1FBakRDLElBQUksTUFBTSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztRQUN4QixJQUFJLENBQUMsd0JBQXdCLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUNqRSxVQUFDLElBQVM7WUFDUixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7WUFDL0MsSUFBSSxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQzVCLEtBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDL0MsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztnQkFDbEQsS0FBSSxDQUFDLGVBQWUsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztnQkFDM0MsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztnQkFDekMsS0FBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDakUsS0FBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQztnQkFDL0MsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQy9DLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztnQkFFdkQsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQkFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDckQsS0FBSSxDQUFDLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxPQUFPLENBQ3RDLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUMzQixhQUFhLENBQ2QsQ0FBQztnQkFFRixJQUFJLEtBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7b0JBQ3RDLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO29CQUN6QixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2lCQUMvQjtxQkFBTTtvQkFDTCxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztvQkFDeEIsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztpQkFDbkM7Z0JBQ0QsS0FBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7YUFDcEI7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQzthQUNuQztZQUNELEtBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDM0MsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN6QixDQUFDLEVBQ0QsVUFBQyxLQUFLO1lBQ0osS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUNoQyxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsOERBQThELENBQy9ELENBQUM7UUFDSixDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFDRCxnREFBZSxHQUFmO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztJQUNwQyxDQUFDO0lBRUQsZ0RBQWUsR0FBZixVQUFnQixjQUFjLEVBQUUsWUFBWTs7UUFDMUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM5QixJQUFJLFNBQVMsR0FBRztZQUNkO2dCQUNFLElBQUksRUFBRSxTQUFTO2dCQUNmLEtBQUssRUFBRSxTQUFTO2dCQUNoQixRQUFRLEVBQUUsSUFBSTthQUNmO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsS0FBSyxFQUFFLFFBQVE7Z0JBQ2YsUUFBUSxFQUFFLElBQUk7YUFDZjtTQUNGLENBQUM7UUFDRixJQUFJLFlBQVksRUFBRTtZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxVQUFVLElBQUk7Z0JBQ3BDLElBQUksT0FBTyxHQUFHO29CQUNaLElBQUksRUFBRSxJQUFJLENBQUMsVUFBVTtvQkFDckIsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVO29CQUN0QixZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVk7b0JBQy9CLFFBQVEsRUFBRSxLQUFLO2lCQUNoQixDQUFDO2dCQUNGLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtvQkFDYixPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztpQkFDN0I7Z0JBQ0QsSUFDRSxJQUFJLENBQUMsVUFBVSxLQUFLLFlBQVk7b0JBQ2hDLElBQUksQ0FBQyxVQUFVLEtBQUssb0JBQW9CO29CQUN4QyxJQUFJLENBQUMsVUFBVSxLQUFLLGVBQWU7b0JBQ25DLElBQUksQ0FBQyxVQUFVLEtBQUssc0JBQXNCO29CQUMxQyxJQUFJLENBQUMsVUFBVSxLQUFLLE1BQU0sRUFDMUI7b0JBQ0EsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQztpQkFDNUI7Z0JBQ0QsZ0NBQWdDO2dCQUNoQyxJQUFJO2dCQUNKLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQztTQUM5Qjs7WUFDRCxLQUFvQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQSxnQkFBQSw0QkFBRTtnQkFBakMsSUFBSSxPQUFPLFdBQUE7Z0JBQ2QsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO29CQUNwQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQzdDO2FBQ0Y7Ozs7Ozs7OztJQUNILENBQUM7SUFFRCw0Q0FBVyxHQUFYLFVBQVksS0FBSztRQUNmLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUk7WUFDckMsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxZQUFZLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELGlEQUFnQixHQUFoQixVQUFpQixLQUFLLEVBQUUsVUFBVTtRQUNoQyxLQUFLLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNuRCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO1FBQzNDLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsSUFBSTtZQUM3QixJQUFJLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtnQkFDekIsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtvQkFDakMsU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN4RDtxQkFBTTtvQkFDTCxTQUFTLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2lCQUNuQzthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1FBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUc7WUFDbEI7Z0JBQ0UsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLElBQUksRUFBRSxTQUFTO2FBQ2hCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFlBQVk7Z0JBQ25CLElBQUksRUFBRSxhQUFhO2FBQ3BCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLElBQUksRUFBRSxTQUFTO2FBQ2hCO1NBQ0YsQ0FBQztRQUNGLElBQUksVUFBVSxFQUFFO1lBQ2QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFNBQVMsQ0FBQztZQUNuQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDM0MsNENBQTRDO1lBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUM5QjtJQUNILENBQUM7SUFFRCwwQ0FBUyxHQUFULFVBQVUsS0FBSyxFQUFFLE9BQU87UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN0QyxJQUFJLEtBQUssQ0FBQyxLQUFLLEVBQUU7WUFDZixzQ0FBc0M7WUFDdEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDdkQsZ0VBQWdFO1NBQ2pFO2FBQU0sSUFBSSxPQUFPLElBQUksV0FBVyxFQUFFO1lBQ2pDLElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUNmLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdCLG1EQUFtRDtZQUNuRCxnQ0FBZ0M7WUFDaEMsa0NBQWtDO1lBQ2xDLE1BQU07WUFDTixtQkFBbUI7WUFDbkIsd0JBQXdCO1lBQ3hCLDJCQUEyQjtZQUMzQixLQUFLO1lBRUwsS0FBSztZQUNMLGdDQUFnQztZQUNoQywyREFBMkQ7WUFDM0QsMkdBQTJHO1NBQzVHO1FBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsNENBQVcsR0FBWDtRQUNFLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7SUFFRCw0Q0FBVyxHQUFYO1FBQ0UsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7UUFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFFRCwwQ0FBUyxHQUFULFVBQVUsS0FBSztRQUFmLGlCQTJGQztRQTFGQyxJQUFJLEtBQUssRUFBRTtZQUNULEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzNELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQzFFLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDO1NBQ3BFO2FBQU07WUFDTCw4QkFBOEI7WUFDOUIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDdkQsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7WUFDdEUsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDL0QsSUFBSTtTQUNMO1FBRUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFFcEQsSUFBSSxZQUFZLEdBQUc7WUFDakIsSUFBSSxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDNUIsQ0FBQztRQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyx3QkFBd0I7YUFDMUIsYUFBYSxDQUFDLFlBQVksQ0FBQzthQUMzQixTQUFTLENBQUMsVUFBQyxXQUFnQjtZQUMxQixJQUFJLFVBQVUsQ0FBQztZQUVmLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUNuRCxLQUFJLENBQUMsZUFBZSxDQUNsQixLQUFJLENBQUMsZ0JBQWdCLEVBQ3JCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQzlCLENBQUM7WUFDRixJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN4RSxjQUFjLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDdEMsS0FBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUMsQ0FBQztZQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxXQUFXLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDNUMsSUFBSSxXQUFXLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQztZQUNuQyxLQUFJLENBQUMsUUFBUSxHQUFHLGNBQWMsQ0FBQztZQUMvQixJQUFJLE9BQU8sR0FBRztnQkFDWixLQUFLLEVBQUUsS0FBSSxDQUFDLEtBQUs7Z0JBQ2pCLE1BQU0sRUFBRSxLQUFJLENBQUMsTUFBTTtnQkFDbkIsZUFBZSxFQUFFLEtBQUksQ0FBQyxlQUFlO2dCQUNyQyxJQUFJLEVBQUUsS0FBSSxDQUFDLGdCQUFnQjtnQkFDM0IsUUFBUSxFQUFFLEtBQUksQ0FBQyxRQUFRO2dCQUN2QixnQkFBZ0IsRUFBRSxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFO2dCQUM1RCxjQUFjLEVBQUUsS0FBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3BFLFFBQVEsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzFELE1BQU0sRUFBRSxLQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7YUFDckQsQ0FBQztZQUNGLEtBQUksQ0FBQyx3QkFBd0I7aUJBQzFCLFVBQVUsQ0FBQyxPQUFPLENBQUM7aUJBQ25CLFNBQVMsQ0FBQyxVQUFDLElBQVM7Z0JBQ25CLDhDQUE4QztnQkFDOUMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDM0MsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlO3dCQUNsRCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZTt3QkFDcEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDUCxxREFBcUQ7b0JBQ3JELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWU7d0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlO3dCQUNwQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUVQLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLFVBQVUsSUFBSTt3QkFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7d0JBQy9CLCtHQUErRzt3QkFDL0csZ0RBQWdEO3dCQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDOzRCQUNmLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTtnQ0FDakQsQ0FBQyxDQUFDLElBQUk7Z0NBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQzt3QkFDWixJQUFJLENBQUMsU0FBUyxDQUFDOzRCQUNiLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTtnQ0FDakQsQ0FBQyxDQUFDLEtBQUs7Z0NBQ1AsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDdEIsSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTs0QkFDckMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxRQUFRO2dDQUN2QyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUNyRCxJQUFJLFFBQVEsR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDcEMsSUFBSSxVQUFVLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDaEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxVQUFVLENBQUM7NEJBQ25ELENBQUMsQ0FBQyxDQUFDO3lCQUNKO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29CQUN2QyxLQUFJLENBQUMsVUFBVTt3QkFDYixZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQzFELEtBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLEtBQUksQ0FBQyxlQUFlLENBQUM7b0JBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFVBQVUsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2lCQUN2RDtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0QsMkNBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNoQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDNUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDakI7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQzVDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBQ0Qsd0NBQU8sR0FBUCxVQUFRLElBQUk7UUFDVixxQkFBcUI7UUFFckIsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxXQUFXLEdBQUc7WUFDaEIsZ0NBQWdDO1lBQ2hDLHFDQUFxQztZQUNyQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDL0MsQ0FBQztRQUNGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsaURBQWdCLEdBQWhCLFVBQWlCLE9BQU87UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVELDBGQUEwRjtRQUMxRixxRUFBcUU7UUFDckUsd0RBQXdEO1FBQ3hELEtBQUs7UUFDTCxNQUFNO0lBQ1IsQ0FBQztJQUVELHlDQUFRLEdBQVI7UUFBQSxpQkFnQkM7UUFmQyxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7UUFFbkIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGFBQWEsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQVM7WUFDaEUsS0FBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsS0FBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsS0FBSSxDQUFDLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztZQUNyQyxLQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDckMsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDMUMsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUM7WUFDL0MsSUFBSSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDckMsS0FBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWEsQ0FBQyxDQUFDLENBQUM7YUFDNUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCw0Q0FBVyxHQUFYLFVBQVksSUFBSSxFQUFFLElBQUk7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQzNCO2FBQU0sSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFL0QsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxXQUFXLEdBQUc7WUFDaEIsZ0NBQWdDO1lBQ2hDLHFDQUFxQztZQUNyQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDL0MsQ0FBQztRQUNGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsc0RBQXFCLEdBQXJCLFVBQXNCLFFBQVE7UUFBOUIsaUJBc0NDO1FBckNDLElBQUksT0FBTyxHQUFHO1lBQ1osTUFBTSxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7U0FDOUIsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBUztZQUNwRSxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDckMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7Z0JBQ2YsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO2dCQUNiLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2QyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RDLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztnQkFDcEQsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQ3ZDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBcEMsQ0FBb0MsQ0FDOUMsQ0FBQztnQkFDRixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDZCxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUN0QixHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLENBQUM7b0JBQzlELEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQztvQkFDOUQsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDO2lCQUN2RDtxQkFBTTtvQkFDTCxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsS0FBSyxDQUFDO29CQUN2QixHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUN6QixHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUN6QixHQUFHLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2lCQUN0QjtnQkFDRCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDckIsS0FBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQ1QsNENBQTRDLEVBQzVDLEtBQUksQ0FBQywwQkFBMEIsQ0FDaEMsQ0FBQztZQUNGLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxLQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEQsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQztZQUM1QyxLQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUM7UUFDSCxRQUFRLEVBQUUsQ0FBQztJQUNiLENBQUM7SUFFRCwyQ0FBVSxHQUFWLFVBQVcsV0FBVztRQUNwQixJQUFJLFdBQVcsSUFBSSxJQUFJLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUM7U0FDYjthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7SUFFRCw2Q0FBWSxHQUFaLFVBQWEsV0FBbUI7UUFDOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDakQsQ0FBQztJQUVELG1EQUFrQixHQUFsQixVQUFtQixXQUFtQjtRQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDM0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDOUMsQ0FBQztJQUVELCtDQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2xCLElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3JFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxVQUFVLElBQUk7WUFDbkMsSUFBSSxJQUFJLENBQUMsdUJBQXVCLElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sRUFBRTtnQkFDdkUsSUFBSSxDQUFDLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxNQUFNLENBQ3JDLElBQUksQ0FBQyx1QkFBdUIsRUFDNUIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQ3ZCLENBQUM7YUFDSDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkQ7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxxREFBb0IsR0FBcEIsVUFBcUIsRUFBRSxFQUFFLElBQUk7UUFBN0IsaUJBaUVDO1FBaEVDLDBCQUEwQjtRQUMxQixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxJQUFJLElBQUksV0FBVyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLE9BQU8sR0FBRyxFQUFFLENBQUM7U0FDZDthQUFNO1lBQ0wsT0FBTyxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7U0FDcEI7UUFFRCxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUVoQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRTNCLElBQUksT0FBTyxHQUFHO1lBQ1osTUFBTSxFQUFFLE9BQU87U0FDaEIsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBUztZQUNwRSxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDckMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUk7Z0JBQ2YsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO2dCQUViLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2QyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RDLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztnQkFDcEQsSUFBSSxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDdkMsSUFBSSxLQUFLLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FDNUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFwQyxDQUFvQyxDQUM5QyxDQUFDO29CQUVGLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxFQUFFO3dCQUNkLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7d0JBQ3RCLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxLQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxDQUFDO3dCQUNuRSxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQzt3QkFDbkUsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUM7cUJBQzVEO3lCQUFNO3dCQUNMLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBQ3ZCLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBQ3pCLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7d0JBQ3pCLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7cUJBQ3RCO2lCQUNGO3FCQUFNO29CQUNMLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQ3pCLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQ3pCLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ3RCO2dCQUVELDhCQUE4QjtnQkFDOUIsMEJBQTBCO2dCQUMxQixJQUFJO2dCQUNKLG9DQUFvQztnQkFDcEMsMkJBQTJCO2dCQUMzQixJQUFJO2dCQUVKLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQixLQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsS0FBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLGtCQUFrQixDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0RCxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsU0FBUyxDQUFDO1lBQzVDLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUM1QyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDbkUseUJBQXlCO1FBQzNCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELDZDQUFZLEdBQVosVUFBYSxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUk7UUFBakMsaUJBZ0RDO1FBL0NDLElBQUksSUFBSSxJQUFJLEtBQUssRUFBRTtZQUNqQixJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksSUFBSSxFQUFFO2dCQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO29CQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDcEIsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO29CQUNiLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUN4QixHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDMUIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ3hCLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO29CQUN0QyxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDeEMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7b0JBQ3hDLEdBQUcsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO29CQUNoQyxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztvQkFDOUIsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksS0FBSyxFQUFFO2dCQUNqQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQkFDdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO29CQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQzthQUMvQjtTQUNGO2FBQU0sSUFBSSxJQUFJLElBQUksUUFBUSxFQUFFO1lBQzNCLElBQ0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FDaEMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxJQUFJLEVBQTFCLENBQTBCLENBQ3JDLEdBQUcsQ0FBQyxFQUNMO2dCQUNBLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxJQUFJLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3ZDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUMvQixVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDLElBQUksRUFBMUIsQ0FBMEIsQ0FDckMsQ0FBQztvQkFDRixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7aUJBQ25DO3FCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUU7b0JBQ2pDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQzVDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsSUFBSSxFQUF6QixDQUF5QixDQUNuQyxDQUFDO29CQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FDL0IsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxJQUFJLEVBQTFCLENBQTBCLENBQ3JDLENBQUM7b0JBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO2lCQUNwQzthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsNENBQVcsR0FBWDtRQUFBLGlCQXFCQztRQXBCQyxPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ25FLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxVQUFDLElBQUksRUFBRSxNQUFNO1lBQzVDLElBQUksS0FBSyxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQ3pDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxLQUFLLElBQUksQ0FBQyxXQUFXLEVBQXhCLENBQXdCLENBQ2xDLENBQUM7WUFFRixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNsRDtpQkFBTTtnQkFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUMxQyxLQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM1QztRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxrQkFBa0IsQ0FDaEQsSUFBSSxDQUFDLG1CQUFtQixDQUN6QixDQUFDO1FBQ0YsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3RELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDO0lBQy9ELENBQUM7SUFFRCw2Q0FBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzVDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUM5QyxDQUFDO0lBRUQsNkNBQVksR0FBWixVQUFhLEtBQUssRUFBRSxJQUFJO1FBQ3RCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLEVBQWpCLENBQWlCLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFFeEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFsQixDQUFrQixDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDbkQsQ0FBQztJQUVELG9EQUFtQixHQUFuQixVQUFvQixLQUFLLEVBQUUsSUFBSTtRQUM3QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFqQixDQUFpQixDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXhDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO0lBQ25ELENBQUM7SUFFRCw2Q0FBWSxHQUFaLFVBQWEsS0FBSyxFQUFFLElBQUk7UUFDdEIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksRUFBakIsQ0FBaUIsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUVwQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEVBQWxCLENBQWtCLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUMvQyxDQUFDO0lBRUQsMENBQVMsR0FBVDtRQUNFLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7UUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDM0IsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUTtZQUNqQyxXQUFXLEVBQUUsSUFBSSxDQUFDLHdCQUF3QjtZQUMxQyxXQUFXLEVBQUUsSUFBSSxDQUFDLHdCQUF3QjtZQUMxQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVELGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNwRSxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDckMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDMUQsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtTQUNyRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQ2pFLFVBQUMsR0FBRztZQUNGLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGlCQUFpQixFQUFFLENBQUMsQ0FBQztZQUMvRCxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSx5QkFBeUIsQ0FBQyxDQUFDO1lBQ2xELHlCQUF5QjtRQUMzQixDQUFDLEVBQ0QsVUFBQyxLQUFLO1lBQ0oseUJBQXlCO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNsRCxDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCx5Q0FBUSxHQUFSLFVBQVMsSUFBSTtRQUFiLGlCQXFEQztRQXBEQyxJQUFJLElBQUksSUFBSSxZQUFZLEVBQUU7WUFDeEIsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNsQixRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3BELFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7U0FDckM7YUFBTSxJQUFJLElBQUksSUFBSSxZQUFZLEVBQUU7WUFDL0IsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNsQixRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1lBQ2pDLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7U0FDckM7UUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLEdBQUcsR0FBRztZQUNSLFdBQVcsRUFBRSxRQUFRO1lBQ3JCLE1BQU0sRUFBRSxRQUFRO1lBQ2hCLE1BQU0sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU07WUFDOUIsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRztZQUN4QixRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO1lBQ2xDLFNBQVMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVM7WUFDcEMsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTtZQUNsQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRO1lBQ2xDLFlBQVksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVk7WUFDMUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxpQkFBaUI7WUFDcEQsVUFBVSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUN0QyxHQUFHLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHO1lBQ3hCLFVBQVUsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7WUFDbkMsT0FBTyxFQUFFLEtBQUs7U0FDZixDQUFDO1FBQ0YsSUFBSSxDQUFDLDBCQUEwQixDQUFDLG1CQUFtQixDQUNqRCxHQUFHLEVBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQ3ZCLENBQUMsU0FBUyxDQUNULFVBQUMsR0FBRztZQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7Z0JBQ3JCLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQzdDO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQzdDO1FBQ0gsQ0FBQyxFQUNELFVBQUMsS0FBSztZQUNKLEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzNDLElBQUksS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLElBQUksS0FBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUU7Z0JBQzFELEtBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO2dCQUNyQyxLQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxLQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVDLFVBQVUsQ0FBQztvQkFDVCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQkFDekIsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ1Y7UUFDSCxDQUFDLENBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQsd0NBQU8sR0FBUDtRQUFBLGlCQVdDO1FBVkMsSUFBSSxNQUFNLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUM1QyxJQUFJLFdBQVcsR0FBRztZQUNoQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsTUFBTSxFQUFFLE1BQU07U0FDZixDQUFDO1FBQ0YsSUFBSSxDQUFDLHdCQUF3QjthQUMxQixhQUFhLENBQUMsV0FBVyxDQUFDO2FBQzFCLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDZCxLQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsQ0FBQyxDQUFDO1FBQ2xFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDJEQUEwQixHQUExQixVQUEyQixPQUFPO1FBQ2hDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQzVDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsSUFBSSxFQUF6QixDQUF5QixDQUNuQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFFMUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxJQUFJLEVBQTFCLENBQTBCLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFFbkMsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLGtCQUFrQixDQUNoRCxJQUFJLENBQUMsbUJBQW1CLENBQ3pCLENBQUM7UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN0RCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzVDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUM1QyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQztRQUM3RCxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDO0lBQzlELENBQUM7SUFFRCxzREFBcUIsR0FBckIsVUFBc0IsT0FBTztRQUMzQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUM1QyxVQUFDLEdBQUcsSUFBSyxPQUFBLEdBQUcsQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDLElBQUksRUFBekIsQ0FBeUIsQ0FDbkMsQ0FBQztRQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzFDLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM5QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUMvQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLDZCQUE2QixDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELGdEQUFlLEdBQWYsVUFBZ0IsVUFBVTtRQUExQixpQkFpQkM7UUFoQkMsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDeEQsSUFBSSxXQUFXLEdBQUc7WUFDaEIsY0FBYyxFQUFFLFdBQVc7U0FDNUIsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0I7YUFDMUIsa0JBQWtCLENBQUMsV0FBVyxDQUFDO2FBQy9CLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDZCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUU7Z0JBQzlDLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNO29CQUMxQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7b0JBQ3ZCLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1AsSUFBSSxVQUFVLEVBQUU7b0JBQ2QsS0FBSSxDQUFDLFlBQVksR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDO2lCQUNyQzthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsdUNBQU0sR0FBTixVQUFPLEVBQUUsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFFBQVE7UUFDeEMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLHNCQUFzQixFQUFFO1lBQ3pELElBQUksRUFBRSxJQUFJLGdCQUFnQixFQUFFO2dCQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO2FBQ25DO2lCQUFNLElBQUksRUFBRSxJQUFJLFdBQVcsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxXQUFXLENBQUM7Z0JBQzdCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzthQUN6QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFtQixDQUFDO2dCQUNyQyxJQUFJLFVBQVUsSUFBSSxVQUFVLElBQUksRUFBRSxFQUFFO29CQUNsQyxJQUFJLFVBQVUsR0FBRyxFQUFFLENBQUM7b0JBQ3BCLElBQUksU0FBUyxLQUFLLFFBQVEsRUFBRTt3QkFDMUIsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ2xDO3lCQUFNO3dCQUNMLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO3FCQUM3QjtvQkFDRCxJQUFJLENBQUMsb0JBQW9CLENBQUMsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUNuRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxDQUFDO29CQUM5QyxJQUFJLENBQUMscUJBQXFCLENBQUM7d0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3JCLENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksU0FBUyxLQUFLLE1BQU0sRUFBRTt3QkFDeEIsSUFBSSxRQUFRLElBQUksQ0FBQyxFQUFFOzRCQUNqQixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUNsQzs2QkFBTSxJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUU7NEJBQ3hCLElBQUksYUFBYSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDOzRCQUMvQyxVQUFVLENBQUMsSUFBSSxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDbEM7cUJBQ0Y7aUJBQ0Y7Z0JBQ0Qsa0VBQWtFO2FBQ25FO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsZ0JBQWdCO0lBQ2hCLGdEQUFlLEdBQWY7UUFBQSxpQkF5Q0M7UUF4Q0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM3QixJQUFJLFdBQVcsR0FBRztZQUNoQixPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtZQUM5QixlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7U0FDdEMsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0I7YUFDMUIsc0JBQXNCLENBQUMsV0FBVyxDQUFDO2FBQ25DLFNBQVMsQ0FBQyxVQUFDLElBQUk7WUFDZCxJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUNyQyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7aUJBQ3pCLE9BQU8sRUFBRTtpQkFDVCxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsR0FBRztnQkFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixPQUFPO29CQUNMLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUNuQixLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDO2lCQUNqQyxDQUFDO1lBQ0osQ0FBQyxDQUFDO2lCQUNELEtBQUssRUFBRSxDQUFDO1lBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFFNUIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLE9BQU87Z0JBQ3JCLElBQU0sU0FBUyxHQUFRLEVBQUUsQ0FBQztnQkFDMUIsU0FBUyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO2dCQUNwQyxTQUFTLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ2hDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO1lBQy9ELGlDQUFpQztZQUNqQzs7Ozs7Ozs7O2NBU0o7UUFDRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnREFBZSxHQUFmO1FBQUEsaUJBNkJDO1FBNUJDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDOUIsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO1NBQ3RDLENBQUM7UUFDRixJQUFJLENBQUMsd0JBQXdCO2FBQzFCLDBCQUEwQixDQUFDLFdBQVcsQ0FBQzthQUN2QyxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQ2Qsd0NBQXdDO1lBQ3hDLElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7aUJBQ3JDLE9BQU8sQ0FBQyxVQUFVLENBQUM7aUJBQ25CLE9BQU8sRUFBRTtpQkFDVCxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsR0FBRztnQkFDYixvQkFBb0I7Z0JBQ3BCLE9BQU87b0JBQ0wsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2QsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQztpQkFDakMsQ0FBQztZQUNKLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztZQUNYLHNCQUFzQjtZQUN0QixNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsT0FBTztnQkFDckIsSUFBTSxTQUFTLEdBQVEsRUFBRSxDQUFDO2dCQUMxQixTQUFTLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQy9CLFNBQVMsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDaEMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMxQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGlEQUFnQixHQUFoQjtRQUFBLGlCQTREQztRQTNEQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQzlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1VBbUJEO1FBQ0MsSUFBSSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDOUIsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO1NBQ3RDLENBQUM7UUFDRixJQUFJLENBQUMsd0JBQXdCO2FBQzFCLDJCQUEyQixDQUFDLFdBQVcsQ0FBQzthQUN4QyxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQ2QsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQztpQkFDckMsT0FBTyxDQUFDLFVBQVUsQ0FBQztpQkFDbkIsT0FBTyxFQUFFO2lCQUNULEdBQUcsQ0FBQyxVQUFDLElBQUksRUFBRSxHQUFHO2dCQUNiLG9CQUFvQjtnQkFDcEIsT0FBTztvQkFDTCxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztvQkFDZCxLQUFLLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDO2lCQUNqQyxDQUFDO1lBQ0osQ0FBQyxDQUFDO2lCQUNELEtBQUssRUFBRSxDQUFDO1lBQ1gsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDO1lBQ25CLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFPO2dCQUNyQixVQUFVLEdBQUcsVUFBVSxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7WUFDL0MsaUNBQWlDO1lBQ2pDLE1BQU0sQ0FBQyxPQUFPLENBQUMsVUFBQyxPQUFPO2dCQUNyQixJQUFNLFNBQVMsR0FBUSxFQUFFLENBQUM7Z0JBRTFCLHFDQUFxQztnQkFDckMsaUNBQWlDO2dCQUNqQyxzQ0FBc0M7Z0JBQ3RDLGtFQUFrRTtnQkFDbEUsU0FBUyxDQUFDLFVBQVU7b0JBQ2xCLFVBQVUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7Z0JBQ3BFLFNBQVMsQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsU0FBUyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7Z0JBQ2xFLFNBQVMsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDaEMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMzQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhDQUFhLEdBQWIsVUFBYyxDQUFDO1FBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNmLE9BQU8sQ0FBQyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7SUFDdkIsQ0FBQztJQUNELDJDQUFVLEdBQVYsVUFBVyxHQUFHO1FBQ1osSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqQixPQUFPLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUM3QjthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUM7U0FDWDtJQUNILENBQUM7SUFFRCx5Q0FBUSxHQUFSLFVBQVMsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckIsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztRQUM3QyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFFL0QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG9CQUFvQixHQUFHLE9BQU8sQ0FBQztJQUMvRCxDQUFDO0lBRUQsc0NBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7SUFDckIsQ0FBQzs7Z0JBL3dDRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsODk5Q0FBOEM7b0JBRTlDLFVBQVUsRUFBRTt3QkFDVixPQUFPLENBQUMsY0FBYyxFQUFFOzRCQUN0QixLQUFLLENBQ0gsV0FBVyxFQUNYLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FDMUQ7NEJBQ0QsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDekMsVUFBVSxDQUNSLHdCQUF3QixFQUN4QixPQUFPLENBQUMsc0NBQXNDLENBQUMsQ0FDaEQ7eUJBQ0YsQ0FBQzt3QkFDRixjQUFjO3FCQUNmO29CQUNELFNBQVMsRUFBRTt3QkFDVCw0RkFBNEY7d0JBQzVGLDhGQUE4Rjt3QkFDOUYsaUNBQWlDO3dCQUNqQzs0QkFDRSxPQUFPLEVBQUUsV0FBVzs0QkFDcEIsUUFBUSxFQUFFLGlCQUFpQjs0QkFDM0IsSUFBSSxFQUFFLENBQUMsZUFBZSxDQUFDO3lCQUN4Qjt3QkFFRCxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFO3FCQUNwRDs7aUJBQ0Y7Ozs7Z0JBdkZDLFdBQVc7Z0JBTlgsU0FBUztnQkF3QkYsTUFBTTtnQkFETixlQUFlO2dCQVpmLHVCQUF1QjtnQkFFdkIsMEJBQTBCO2dCQXdCMUIsY0FBYztnQkFFZCxrQkFBa0I7Ozs0QkE4SHhCLFNBQVMsU0FBQyxXQUFXOzZCQUNyQixTQUFTLFNBQUMsWUFBWTs2QkFDdEIsU0FBUyxTQUFDLFlBQVk7a0NBQ3RCLFNBQVMsU0FBQyxpQkFBaUI7NEJBQzNCLFNBQVMsU0FBQyxXQUFXLEVBQUU7d0JBQ3RCLElBQUksRUFBRSxRQUFRO3FCQUNmOzBCQUdBLFNBQVMsU0FBQyxTQUFTLEVBQUU7d0JBQ3BCLElBQUksRUFBRSxRQUFRO3FCQUNmOztJQStwQ0gsNkJBQUM7Q0FBQSxBQWh4Q0QsSUFneENDO1NBanZDWSxzQkFBc0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFBhZ2luYXRvciwgTWF0VGFibGVEYXRhU291cmNlLCBNYXRJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHtcclxuICBNYXREaWFsb2csXHJcbiAgTWF0U2lkZW5hdixcclxuICBNYXREaWFsb2dSZWYsXHJcbiAgTUFUX0RJQUxPR19EQVRBXHJcbn0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQge1xyXG4gIEZvcm1CdWlsZGVyLFxyXG4gIEZvcm1Hcm91cCxcclxuICBWYWxpZGF0b3JzLFxyXG4gIEZvcm1Db250cm9sXHJcbn0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSB9IGZyb20gJy4uL3JlcG9ydC1tYW5hZ2VtZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgKiBhcyBGaWxlU2F2ZXIgZnJvbSAnZmlsZS1zYXZlcic7XHJcbmltcG9ydCB7IFNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2V0dXAtYWRtaW5pc3RyYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFJlcG9ydEhpc3RvcnlDb21wb25lbnQgfSBmcm9tICcuL3JlcG9ydC1tYW5hZ2VtZW50LWhpc3RvcnkvcmVwb3J0LW1hbmFnZW1lbnQuY29tcG9uZW50JztcclxuXHJcbmltcG9ydCB7XHJcbiAgYW5pbWF0ZSxcclxuICBzdGF0ZSxcclxuICBzdHlsZSxcclxuICB0cmFuc2l0aW9uLFxyXG4gIHRyaWdnZXJcclxufSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSAnbG9kYXNoJztcclxuaW1wb3J0IHsgZnVzZUFuaW1hdGlvbnMgfSBmcm9tICcuLi8uLi9AZnVzZS9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBNb21lbnREYXRlQWRhcHRlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsLW1vbWVudC1hZGFwdGVyJztcclxuaW1wb3J0IHtcclxuICBEYXRlQWRhcHRlcixcclxuICBNQVRfREFURV9GT1JNQVRTLFxyXG4gIE1BVF9EQVRFX0xPQ0FMRVxyXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2NvcmUnO1xyXG5pbXBvcnQgKiBhcyBfbW9tZW50IGZyb20gJ21vbWVudCc7XHJcbmNvbnN0IG1vbWVudCA9IF9tb21lbnQ7XHJcblxyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gJy4uLy4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTmd4VWlMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnbmd4LXVpLWxvYWRlcic7IC8vIEltcG9ydCBOZ3hVaUxvYWRlclNlcnZpY2VcclxuaW1wb3J0IHtjb25maWdNZXRhRGF0YX0gZnJvbSAnLi9kZW1vRGF0YSc7XHJcbmV4cG9ydCBjb25zdCBNWV9GT1JNQVRTID0ge1xyXG4gIHBhcnNlOiB7XHJcbiAgICBkYXRlSW5wdXQ6ICdMTCdcclxuICB9LFxyXG4gIGRpc3BsYXk6IHtcclxuICAgIGRhdGVJbnB1dDogJ0xMJyxcclxuICAgIG1vbnRoWWVhckxhYmVsOiAnTU1NIFlZWVknLFxyXG4gICAgZGF0ZUExMXlMYWJlbDogJ0xMJyxcclxuICAgIG1vbnRoWWVhckExMXlMYWJlbDogJ01NTU0gWVlZWSdcclxuICB9XHJcbn07XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEVsZW1lbnQge1xyXG4gIFt4OiBzdHJpbmddOiBhbnk7XHJcbiAgbW9kdWxlOiBzdHJpbmc7XHJcbiAgYXBwbGljYXRpb25fbmFtZTogc3RyaW5nO1xyXG4gIG9iamVjdF9uYW1lOiBzdHJpbmc7XHJcbiAgb2JqZWN0X2NvZGU6IHN0cmluZztcclxuICAvLyBjb250cm9sX25hbWU6IHN0cmluZztcclxuICAvLyBjb250cm9sX3R5cGU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtY29uZmlnLXRyYWNrZXInLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9jb25maWctdHJhY2tlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vY29uZmlnLXRyYWNrZXIuY29tcG9uZW50LnNjc3MnXSxcclxuICBhbmltYXRpb25zOiBbXHJcbiAgICB0cmlnZ2VyKCdkZXRhaWxFeHBhbmQnLCBbXHJcbiAgICAgIHN0YXRlKFxyXG4gICAgICAgICdjb2xsYXBzZWQnLFxyXG4gICAgICAgIHN0eWxlKHsgaGVpZ2h0OiAnMHB4JywgbWluSGVpZ2h0OiAnMCcsIGRpc3BsYXk6ICdub25lJyB9KVxyXG4gICAgICApLFxyXG4gICAgICBzdGF0ZSgnZXhwYW5kZWQnLCBzdHlsZSh7IGhlaWdodDogJyonIH0pKSxcclxuICAgICAgdHJhbnNpdGlvbihcclxuICAgICAgICAnZXhwYW5kZWQgPD0+IGNvbGxhcHNlZCcsXHJcbiAgICAgICAgYW5pbWF0ZSgnMjI1bXMgY3ViaWMtYmV6aWVyKDAuNCwgMC4wLCAwLjIsIDEpJylcclxuICAgICAgKVxyXG4gICAgXSksXHJcbiAgICBmdXNlQW5pbWF0aW9uc1xyXG4gIF0sXHJcbiAgcHJvdmlkZXJzOiBbXHJcbiAgICAvLyBgTW9tZW50RGF0ZUFkYXB0ZXJgIGNhbiBiZSBhdXRvbWF0aWNhbGx5IHByb3ZpZGVkIGJ5IGltcG9ydGluZyBgTW9tZW50RGF0ZU1vZHVsZWAgaW4geW91clxyXG4gICAgLy8gYXBwbGljYXRpb24ncyByb290IG1vZHVsZS4gV2UgcHJvdmlkZSBpdCBhdCB0aGUgY29tcG9uZW50IGxldmVsIGhlcmUsIGR1ZSB0byBsaW1pdGF0aW9ucyBvZlxyXG4gICAgLy8gb3VyIGV4YW1wbGUgZ2VuZXJhdGlvbiBzY3JpcHQuXHJcbiAgICB7XHJcbiAgICAgIHByb3ZpZGU6IERhdGVBZGFwdGVyLFxyXG4gICAgICB1c2VDbGFzczogTW9tZW50RGF0ZUFkYXB0ZXIsXHJcbiAgICAgIGRlcHM6IFtNQVRfREFURV9MT0NBTEVdXHJcbiAgICB9LFxyXG5cclxuICAgIHsgcHJvdmlkZTogTUFUX0RBVEVfRk9STUFUUywgdXNlVmFsdWU6IE1ZX0ZPUk1BVFMgfVxyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbmZpZ1RyYWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIG1lc3NhZ2VzOiBhbnlbXSA9IFtdO1xyXG4gIHN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICBzdWJtaXR0ZWQgPSBmYWxzZTtcclxuICBlcnJvclN0YXR1czogYW55O1xyXG4gIGVycm9yTXNnOiBhbnk7XHJcbiAgdHlwZXM6IGFueTtcclxuICB0eXBlc3NzID0gW107XHJcbiAgc2VsZWN0ZWRfbW9kdWxlczogYW55O1xyXG4gIG1vZHVsZXM6IGFueTtcclxuICBzZWxlY3RlZF9kYXRhX2xlbmd0aDogYW55O1xyXG4gIGZpbmFsX3NlbGVjdGVkX2RhdGEgPSBbXTtcclxuICBmaW5hbF9zZWxlY3RlZF90YWJsZTogYW55O1xyXG4gIGZpbmFsX3NlbGVjdGVkX2xlbmd0aDogYW55O1xyXG4gIGdldENvbmZpZ1RhYmxlOiBhbnk7XHJcbiAgbW9kZTogYW55O1xyXG4gIGFwcF90eXBlcyA9IFtdO1xyXG4gIGRhdGFzb3VyY2U6IGFueTtcclxuICBkYXRhc291cmNlZTogYW55O1xyXG4gIHNlbGVjdGVkX2RhdGFzb3VyY2U6IGFueTtcclxuICBDcmVhdGVGb3JtOiBGb3JtR3JvdXA7XHJcbiAgZGF0YVNvdXJjZTogYW55O1xyXG4gIG9iamVjdF9saXN0OiBhbnk7XHJcbiAgb2JqZWN0X2xpc3RfbGVuZ3RoOiBhbnk7XHJcbiAgc2VsZXRlZERhdGFzb3VyY2VJZDogYW55O1xyXG4gIGRlZmF1bHREYXRhc291cmNlTmFtZTogYW55O1xyXG4gIGRlZmF1bHREYXRhc291cmNlOiBhbnk7XHJcbiAgc2NoZWR1bGVUeXBlOiBhbnk7XHJcbiAgY2hlY2tlZENvbnRyb2w6IHN0cmluZ1tdID0gW107XHJcbiAgZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbJ3NlbGVjdCcsICdvYmplY3RfbmFtZScsICdvYmplY3RfY29kZSddO1xyXG4gIGRpc3BsYXlDb2x1bW5zOiBzdHJpbmdbXSA9IFtcclxuICAgICdtb2R1bGVfbmFtZScsXHJcbiAgICAnbmFtZScsXHJcbiAgICAnY29udHJvbF9uYW1lJyxcclxuICAgICdjb250cm9sX3R5cGUnLFxyXG4gICAgJ3Jpc2tSYW5rJyxcclxuICAgICdhY3Rpb24nXHJcbiAgXTtcclxuICBkaXNwbGF5Q29sdW1uc0xpc3Q6IHN0cmluZ1tdID0gW1xyXG4gICAgJ21vZHVsZV9uYW1lJyxcclxuICAgICduYW1lJyxcclxuICAgICdjb250cm9sX25hbWUnLFxyXG4gICAgJ2NvbnRyb2xfdHlwZScsXHJcbiAgICAncmlza1JhbmsnXHJcbiAgXTtcclxuICBkaXNwbGF5T2JqZWN0czogc3RyaW5nW10gPSBbXHJcbiAgICAnc2VsZWN0JyxcclxuICAgICduYW1lJyxcclxuICAgICdjb250cm9sX25hbWUnLFxyXG4gICAgJ2NvbnRyb2xfdHlwZScsXHJcbiAgICAncmlza1JhbmsnXHJcbiAgXTtcclxuICBzZWxlY3RlZENvbHVtbnM6IHN0cmluZ1tdID0gW1xyXG4gICAgJ2FwcGxpY2F0aW9uX25hbWUnLFxyXG4gICAgJ29iamVjdF9uYW1lJyxcclxuICAgICdvYmplY3RfY29kZSdcclxuICBdO1xyXG4gIHNlbGVjdENvbnRyb2wgOiBhbnk7XHJcbiAgc2VsZWN0ZWRfb2JqZWN0czogYW55O1xyXG4gIHNlbGVjdF9tb2R1bGVzOiBhbnk7XHJcbiAgcmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3Q6IGFueTtcclxuICBleHBhbmRFbGVtZW50OiBib29sZWFuID0gZmFsc2U7XHJcbiAgZGF0ZUNvbHVtbnM6IGFueTtcclxuICBwYXJlbnRMaW1pdCA6IGFueSA9IDEwO1xyXG4gIGV4cGFuZGVkRWxlbWVudCA6IGFueTtcclxuICBcclxuICAvLyBAVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgLy8gQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIHNwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuICAvLyBAVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgZnBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG5cclxuICBAVmlld0NoaWxkKCdwYWdpbmF0b3InKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuICBAVmlld0NoaWxkKCdzcGFnaW5hdG9yJykgc3BhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIEBWaWV3Q2hpbGQoJ2ZwYWdpbmF0b3InKSBmcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgQFZpZXdDaGlsZCgncmVwb3J0cGFnaW5hdG9yJykgcmVwb3J0cGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgQFZpZXdDaGlsZCgnZnJvbUlucHV0Jywge1xyXG4gICAgcmVhZDogTWF0SW5wdXRcclxuICB9KVxyXG4gIGZyb21JbnB1dDogTWF0SW5wdXQ7XHJcblxyXG4gIEBWaWV3Q2hpbGQoJ3RvSW5wdXQnLCB7XHJcbiAgICByZWFkOiBNYXRJbnB1dFxyXG4gIH0pXHJcbiAgdG9JbnB1dDogTWF0SW5wdXQ7XHJcblxyXG4gIGRTb3VyY2U6IEVsZW1lbnRbXSA9IFtcclxuICAgIHtcclxuICAgICAgbW9kdWxlOiAnR0wnLFxyXG4gICAgICBhcHBsaWNhdGlvbl9uYW1lOiAnR2VuZXJhbCBMZWRnZXInLFxyXG4gICAgICBvYmplY3RfbmFtZTogJ0FjY291bnRpbmcgQ2FsZW5kYXInLFxyXG4gICAgICBvYmplY3RfY29kZTogJydcclxuICAgIH1cclxuICAgIC8vIHttb2R1bGU6ICdHTCcsIGFwcGxpY2F0aW9uX25hbWU6ICdHZW5lcmFsIExlZGdlcicsIG9iamVjdF9uYW1lOiAnQ29sdW1uIHNldCcsIGNvbnRyb2xfbmFtZTogJycsY29udHJvbF90eXBlOicnfVxyXG4gIF07XHJcblxyXG4gIHByb2ZpbGVGb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICBzZWxlY3RlZDogbmV3IEZvcm1Db250cm9sKClcclxuICB9KTtcclxuXHJcbiAgc2NoZWR1bGVvYmo6IGFueTtcclxuICBsb2dpblVzZXI6IGFueTtcclxuXHJcbiAgZFNyY1JlcG9ydCA9IFtcclxuICAgIHtcclxuICAgICAgYXBwX25hbWU6ICcnLFxyXG4gICAgICBvcGVyYXRpb246ICcnLFxyXG4gICAgICBvYmplY3Q6ICcnLFxyXG4gICAgICBjaGFuZ2VfZGF0ZTogJycsXHJcbiAgICAgIGNoYW5nZV9ieTogJycsXHJcbiAgICAgIGNvbnRyb2xfbmFtZTogJycsXHJcbiAgICAgIGV4aXN0aW5nX3JlY29yZDogJycsXHJcbiAgICAgIG5ld19yZWNvcmQ6ICcnXHJcbiAgICB9XHJcbiAgXTtcclxuXHJcbiAgY29udHJvbFR5cGVzID0gW1xyXG4gICAge1xyXG4gICAgICB2YWx1ZTogJ1NPWCcsXHJcbiAgICAgIG5hbWU6ICdTT1gnXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICB2YWx1ZTogJ0hJUEFBJyxcclxuICAgICAgbmFtZTogJ0hJUEFBJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgdmFsdWU6ICdGSURBJyxcclxuICAgICAgbmFtZTogJ0ZJREEnXHJcbiAgICB9XHJcbiAgXTtcclxuICByaXNrUmFua3MgPSBbXHJcbiAgICB7XHJcbiAgICAgIHZhbHVlOiAnY3JpdGljYWwnLFxyXG4gICAgICBuYW1lOiAnQ3JpdGljYWwnXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICB2YWx1ZTogJ2hpZ2gnLFxyXG4gICAgICBuYW1lOiAnSGlnaCdcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHZhbHVlOiAnbWVkaXVtJyxcclxuICAgICAgbmFtZTogJ01lZGl1bSdcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHZhbHVlOiAnbG93JyxcclxuICAgICAgbmFtZTogJ0xvdydcclxuICAgIH1cclxuICBdO1xyXG5cclxuICBzdGF0dXNGaWx0ZXI6IGFueTtcclxuXHJcbiAgY29uZmlnczogYW55O1xyXG4gIGRpYWxvZ1JlZjogYW55O1xyXG4gIC8vIGRhdGFTb3VyY2U6YW55O1xyXG4gIGRhdGFTb3VyY2VSZXBvcnQ6IGFueTtcclxuICAvLyBkaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFsnY2xpZW50SUQnLCAnZnVzaW9uVXJsJywgJ3VzZXJOYW1lJywncXVlcnlUeXBlcycsICdzY2hlZHVsZVR5cGUnLCdyZXRhaW5EYXlzJyBdO1xyXG4gIGRpc3BsYXllZFJlcG9ydDogc3RyaW5nW10gPSBbXHJcbiAgICAnYXBwX25hbWUnLFxyXG4gICAgJ29wZXJhdGlvbicsXHJcbiAgICAnb2JqZWN0JyxcclxuICAgICdjaGFuZ2VfZGF0ZScsXHJcbiAgICAnY2hhbmdlX2J5JyxcclxuICAgICdjb250cm9sX25hbWUnLFxyXG4gICAgJ2V4aXN0aW5nX3JlY29yZCcsXHJcbiAgICAnbmV3X3JlY29yZCdcclxuICBdO1xyXG4gIHNlbGVjdGVkX2RhdGE6IGFueTtcclxuICBzaG93X2RhdGE6IHN0cmluZyA9ICdkYXNoYm9hcmQnO1xyXG4gIGNvbHVtbnNUb0Rpc3BsYXk6IHN0cmluZ1tdID0gW107XHJcbiAgY29uZmlnRGF0YTogYW55O1xyXG4gIGZyb21EYXRlID0gbmV3IEZvcm1Db250cm9sKG1vbWVudCgpKTtcclxuICB0b0RhdGUgPSBuZXcgRm9ybUNvbnRyb2wobW9tZW50KCkpO1xyXG4gIG1vZHVsZU5hbWVzOiBhbnkgPSBjb25maWdNZXRhRGF0YS5tb2R1bGVEYXRhO1xyXG4gIGNvbmZpZ1RyYWNrZXJIZWFkZXI6IGFueTtcclxuICBzZWxlY3RlZFRhYkhlYWRlcjogYW55ID0gW107XHJcbiAgaGlzdG9yeURhdGE6IGFueSA9IHt9O1xyXG5cclxuICBzZWxlY3RlZFRhYmxlSGVhZGVyOiBhbnkgPSBbXTtcclxuICB0YWJsZUhlYWRlciA9IGNvbmZpZ01ldGFEYXRhLnRhYmxlQ29uZmlnO1xyXG4gIHRhYmxlQ29uZmlnOiBhbnkgPSBbXTtcclxuICBoaXN0b3J5Q29uZmlnRGF0YTogYW55ID0gW107XHJcblxyXG4gIHN0ZXAgPSAwO1xyXG4gIHNlbGVjdGVkSGVhZGVySWQ6IGFueTtcclxuICBzZWxlY3RlZEhlYWRlck1vZHVsZW5hbWU6IGFueTtcclxuICBzZWxlY3RlZEhlYWRlck9iamVjdE5hbWU6IGFueTtcclxuXHJcbiAgLyogY2hhcnQgYXNzaWdubWVudCAqL1xyXG4gIG1vZHVsZU9iamVjdE5hbWU6IGFueTtcclxuICBjb25mdGNoYXJ0Ynltb2R1bGU6IHN0cmluZ1tdID0gW107XHJcbiAgdXNlclNlbGVjdGVkVG9nZ2xlOiBzdHJpbmc7XHJcbiAgdmFsOiBhbnk7XHJcbiAgZWJzX21fY2hhcnQ6IGFueSA9IFtdO1xyXG4gIG1hc3Rlcl9jaGFydDogYW55ID0gW107XHJcbiAgd2Vla19ieV91c2VyX2NoYXJ0OiBhbnkgPSBbXTtcclxuICBtb250aF9ieV91c2VyX2NoYXJ0OiBhbnkgPSBbXTtcclxuICAvLyBCYXJcclxuICBzaG93WEF4aXMgPSB0cnVlO1xyXG4gIHNob3dZQXhpcyA9IHRydWU7XHJcbiAgZ3JhZGllbnQgPSBmYWxzZTtcclxuICBzaG93TGVnZW5kID0gdHJ1ZTtcclxuICBzaG93WEF4aXNMYWJlbCA9IHRydWU7XHJcbiAgc2hvd1lBeGlzTGFiZWwgPSB0cnVlO1xyXG5cclxuICB4bGFiZWwgPSAnTW9kdWxlcyc7XHJcbiAgeWxhYmVsID0gJ1JlY29yZHMnO1xyXG4gIHhsYWJlbHdlZWsgPSAnVXNlcnMnO1xyXG4gIHlsYWJlbHdlZWsgPSAnUmVjb3Jkcyc7XHJcbiAgLypwaWUgY2hhcnQqL1xyXG4gIC8vIG9wdGlvbnNcclxuICBzaG93UGllTGVnZW5kID0gZmFsc2U7XHJcbiAgbGVnZW5kUG9zaXRpb24gPSAncmlnaHQnO1xyXG4gIHZpZXc6IGFueVtdID0gWzcwMCwgNDAwXTtcclxuICB3ZWVrdmlldzogYW55W10gPSBbNDUwLCAzNTBdO1xyXG4gIHBpZXZpZXc6IGFueVtdID0gWzQ2MCwgNDAwXTtcclxuICBsZWdlbmRUaXRsZSA9ICdVc2Vycyc7XHJcbiAgbWFzdGVyYnl1c2VydmlldzogYW55W10gPSBbNjAwLCAzNTBdO1xyXG4gIGNvbG9yUGllU2NoZW1lID0ge1xyXG4gICAgZG9tYWluOiBbJyMyOGEzZGQnLCAnIzhlYzllNScsICcjMWM3MjlhJywgJyMxODYxODQnLCAnIzE0NTE2ZScsICcjMTA0MTU4J11cclxuICB9O1xyXG4gIGxpbWl0OiBudW1iZXIgPSAxMDtcclxuICBvZmZzZXQ6IG51bWJlciA9IDA7XHJcbiAgbGVuZ3RoOiBhbnk7XHJcbiAgLy8gcGllXHJcbiAgc2hvd0xhYmVscyA9IHRydWU7XHJcbiAgZXhwbG9kZVNsaWNlcyA9IGZhbHNlO1xyXG4gIGNvbmZpZ1RyYWNrZXJTZXR1cERvbmUgPSBmYWxzZTtcclxuICBjb25maWdRVFNldHVwRG9uZSA9IGZhbHNlO1xyXG4gIGRvdWdobnV0ID0gZmFsc2U7XHJcbiAgaXNDaGVja2VkID0gZmFsc2U7XHJcbiAgLy8geEF4aXNMYWJlbD10cnVlO1xyXG4gIGJhclBhZGRpbmcgPSA1MDtcclxuICBjb2xvclNjaGVtZSA9IHtcclxuICAgIGRvbWFpbjogWycjMjhhM2RkJ11cclxuICB9O1xyXG4gIGZvcm1BY3Rpb246IGFueTtcclxuICBjdXJyZW50SGVhZGVyRGF0YTogYW55O1xyXG4gIHNlbGVjdG9iamhlYWRlcjogYW55O1xyXG4gIGNvbmZpZ1RyYWNrZXJJRDogYW55O1xyXG4gIG9uU2VsZWN0ZWRNb2R1bGVPYmplY3RzOiBhbnkgPSBbXTtcclxuICB1bmlxdWVJRDogYW55O1xyXG4gIG9iamVjdF9uYW1lX1JNX2J5TW9kdWxlOiBhbnk7XHJcbiAgbW9kdWxlX1JNOiBhbnk7XHJcbiAgc2VsZWN0X21vZHVsZXNfUk06IGFueTtcclxuICBvYmplY3ROYW1lX1JNOiBhbnk7XHJcbiAgY2xpZW50SUQ6IGFueTtcclxuICB1c2VyTGlzdDogYW55O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBfbWF0RGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlOiBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgU2V0dXBBZG1pbmlzdHJhdGlvblNlcnZpY2U6IFNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlLFxyXG5cclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBuZ3hTZXJ2aWNlOiBOZ3hVaUxvYWRlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMuQ3JlYXRlRm9ybSA9IHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICBzZWxlY3Q6IFsnJ10sXHJcbiAgICAgIG1vZHVsZTogWycnXSxcclxuICAgICAgdXNlck5hbWU6IFsnJ10sXHJcbiAgICAgIG9iamVjdF9uYW1lOiBbJyddLFxyXG4gICAgICBjb250cm9sX3R5cGU6IFsnJ10sXHJcbiAgICAgIGNvbnRyb2xfbmFtZTogWycnXSxcclxuICAgICAgcmlza1Jhbms6IFsnJ11cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmZvcm1BY3Rpb24gPSAnYWRkJztcclxuICAgIHRoaXMudHlwZXMgPSBbXTtcclxuICAgIHRoaXMubmd4U2VydmljZS5zdGFydCgpO1xyXG4gICAgdGhpcy5zaG93ZWRpdCgpO1xyXG4gICAgdGhpcy5nZXRhbGxjb25maWdzKCk7XHJcbiAgICAvLyB0aGlzLnNlbGVjdGVkVGFiSGVhZGVyID0gW1xyXG4gICAgLy8gXHR7XHJcbiAgICAvLyBcdFx0J3ZhbHVlJzondHJhbnNhY3Rpb250eXBlJyxcclxuICAgIC8vIFx0XHQnbmFtZSc6ICdBUl9UcmFuc2FjdGlvblR5cGVzJ1xyXG4gICAgLy8gXHR9LHtcclxuICAgIC8vIFx0XHQndmFsdWUnOidwYXltZW50dGVybXMnLFxyXG4gICAgLy8gXHRcdCduYW1lJzogJ0FSX1BheW1lbnRUZXJtcydcclxuICAgIC8vIFx0fVxyXG4gICAgLy8gXTtcclxuICAgIHRoaXMubW9kZSA9ICd2aWV3JztcclxuICB9XHJcbiAgb3Blbkhpc3RvcnkoZWxlbWVudCk6IHZvaWQge1xyXG4gICAgLy8gdmFyIHNlbGVjdGVkQ29sdW1uID0gXy5maWx0ZXIodGhpcy50YWJsZUNvbmZpZywgWydpc0lkZW50aWZpZXInLCB0cnVlXSk7XHJcbiAgICBjb25zb2xlLmxvZyhlbGVtZW50LCAnLi4uLi4uLmVsZW1lbnQnKTtcclxuICAgIC8vIGxldCBzZWxlY3RJZCA9ICBlbGVtZW50W3NlbGVjdGVkQ29sdW1uWzBdLm5hbWVdW1wibmFtZVwiXVxyXG5cclxuICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nLm9wZW4oUmVwb3J0SGlzdG9yeUNvbXBvbmVudCwge1xyXG4gICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgIHdpZHRoOiAnNzUlJyxcclxuICAgICAgcGFuZWxDbGFzczogJ2NvbnRhY3QtZm9ybS1kaWFsb2cnLFxyXG4gICAgICBkYXRhOiB7XHJcbiAgICAgICAgYWN0aW9uOiAnbmV3JyxcclxuICAgICAgICBoaXN0b3J5RGF0YTogZWxlbWVudC5oaXN0b3J5RGF0YSxcclxuICAgICAgICByZXBvcnREYXRhOiBlbGVtZW50LFxyXG4gICAgICAgIHNlbGVjdGVkSGVhZGVyOiB0aGlzLnNlbGVjdGVkSGVhZGVySWQsXHJcbiAgICAgICAgZGF0ZUNvbHVtbnM6IHRoaXMuZGF0ZUNvbHVtbnNcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIG9wZW5OYXZpZ2F0ZSgpIHtcclxuICAgIGxldCBvYmogPSB7XHJcbiAgICAgIHVybCA6ICdzZXR1cC1hZG1pbmlzdHJhdGlvbi9jb25maWctdHJhY2tlci1zZXR1cCcsXHJcbiAgICAgIGlkIDogXCJtYW5hZ2VDb25maWdUcmFja2VyU2V0dXBcIlxyXG4gICAgfVxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydzZXR1cC1hZG1pbmlzdHJhdGlvbi9jb25maWctdHJhY2tlci1zZXR1cCddKTtcclxuICB9XHJcbiAgZ2V0YWxsY29uZmlncygpIHtcclxuICAgIHZhciB1c2VySWQgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcklkJyk7XHJcbiAgICB0aGlzLnNlbGVjdGVkX21vZHVsZXMgPSBbXTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YSA9IFtdO1xyXG4gICAgdGhpcy5nZXRDb25maWdUYWJsZSA9IFtdO1xyXG4gICAgdGhpcy5kU291cmNlLmxlbmd0aCA9IDA7XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRBbGxDb25maWdUcmFja2VyKHVzZXJJZCkuc3Vic2NyaWJlKFxyXG4gICAgICAoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgdmFyIGRhdGFfbmV3ID0gZGF0YS5ib2R5O1xyXG4gICAgICAgIHRoaXMuZFNvdXJjZSA9IGRhdGFfbmV3LnJlc3BvbnNlLmNvbmZpZ0RldGFpbHM7XHJcbiAgICAgICAgaWYgKHRoaXMuZFNvdXJjZS5sZW5ndGggIT0gMCkge1xyXG4gICAgICAgICAgdGhpcy5jb25maWdUcmFja2VyU2V0dXBEb25lID0gdHJ1ZTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRfbW9kdWxlcyA9IHRoaXMuZFNvdXJjZVswXS5tb2R1bGU7XHJcbiAgICAgICAgICB0aGlzLmdldENvbmZpZ1RhYmxlID0gdGhpcy5kU291cmNlWzBdLm9iamVjdF9uYW1lO1xyXG4gICAgICAgICAgdGhpcy5jb25maWdUcmFja2VySUQgPSB0aGlzLmRTb3VyY2VbMF0uX2lkO1xyXG4gICAgICAgICAgdGhpcy5jbGllbnRJRCA9IHRoaXMuZFNvdXJjZVswXS5jbGllbnRJRDtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRfZGF0YSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy5nZXRDb25maWdUYWJsZSk7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkX2RhdGEucGFnaW5hdG9yID0gdGhpcy5zcGFnaW5hdG9yO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZF9kYXRhc291cmNlID0gdGhpcy5nZXRDb25maWdUYWJsZTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRfZGF0YV9sZW5ndGggPSB0aGlzLmdldENvbmZpZ1RhYmxlLmxlbmd0aDtcclxuXHJcbiAgICAgICAgICB0aGlzLm1vZHVsZV9STSA9IHRoaXMuZFNvdXJjZVswXS5tb2R1bGU7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZygnTW9kdWxlLS0tLS0tLScsIHRoaXMuZFNvdXJjZVswXS5tb2R1bGUpO1xyXG4gICAgICAgICAgdGhpcy5vYmplY3RfbmFtZV9STV9ieU1vZHVsZSA9IF8uZ3JvdXBCeShcclxuICAgICAgICAgICAgdGhpcy5kU291cmNlWzBdLm9iamVjdF9uYW1lLFxyXG4gICAgICAgICAgICAnbW9kdWxlX25hbWUnXHJcbiAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgIGlmICh0aGlzLmRTb3VyY2VbMF0ubW9kdWxlLmxlbmd0aCAhPSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybUFjdGlvbiA9ICdlZGl0JztcclxuICAgICAgICAgICAgdGhpcy5jb25maWdRVFNldHVwRG9uZSA9IHRydWU7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1BY3Rpb24gPSAnYWRkJztcclxuICAgICAgICAgICAgdGhpcy5zaG93X2RhdGEgPSAnY29uZmlnX3RyYWNrZXInO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5tb2RlID0gJ3ZpZXcnO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnNob3dfZGF0YSA9ICdjb25maWdfdHJhY2tlcic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuVG9nZ2xlKCdkYXNoYm9hcmQnLCBudWxsLCBudWxsLCBudWxsKTtcclxuICAgICAgICB0aGlzLm5neFNlcnZpY2Uuc3RvcCgpO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLm5neFNlcnZpY2Uuc3RvcCgpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdlcnJvcjo6OicgKyBlcnJvcik7XHJcbiAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICdPb3BzIFNvbWV0aGluZyB3ZW50IHdyb25nISEgUGxlYXNlIHRyeSBhZ2FpbiBhZnRlciBzb21ldGltZS4nXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgKTtcclxuICB9XHJcbiAgc2hvd0NvbmZpZ1NldHVwKCkge1xyXG4gICAgdGhpcy5zaG93X2RhdGEgPSAnY29uZmlnX3RyYWNrZXInO1xyXG4gIH1cclxuXHJcbiAgbG9hZFRhYmxlQ29sdW1uKHNlbGVjdGVkSGVhZGVyLCBoZWFkZXJWYWx1ZXMpIHtcclxuICAgIHRoaXMuY29uZmlnVHJhY2tlckhlYWRlciA9IFtdO1xyXG4gICAgdmFyIHRlbXBBcnJheSA9IFtcclxuICAgICAge1xyXG4gICAgICAgIG5hbWU6ICdIaXN0b3J5JyxcclxuICAgICAgICB2YWx1ZTogJ2hpc3RvcnknLFxyXG4gICAgICAgIGlzQWN0aXZlOiB0cnVlXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICBuYW1lOiAnU3RhdHVzJyxcclxuICAgICAgICB2YWx1ZTogJ3N0YXR1cycsXHJcbiAgICAgICAgaXNBY3RpdmU6IHRydWVcclxuICAgICAgfVxyXG4gICAgXTtcclxuICAgIGlmIChoZWFkZXJWYWx1ZXMpIHtcclxuICAgICAgXy5mb3JFYWNoKGhlYWRlclZhbHVlcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICB2YXIgdGVtcE9iaiA9IHtcclxuICAgICAgICAgIG5hbWU6IGl0ZW0udWlBdHRyTmFtZSxcclxuICAgICAgICAgIHZhbHVlOiBpdGVtLmRiQXR0ck5hbWUsXHJcbiAgICAgICAgICBpc0lkZW50aWZpZXI6IGl0ZW0uaXNJZGVudGlmaWVyLFxyXG4gICAgICAgICAgaXNBY3RpdmU6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgICAgICBpZiAoaXRlbS50eXBlKSB7XHJcbiAgICAgICAgICB0ZW1wT2JqWyd0eXBlJ10gPSBpdGVtLnR5cGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgIGl0ZW0uZGJBdHRyTmFtZSA9PT0gJ1NUQVJUX0RBVEUnIHx8XHJcbiAgICAgICAgICBpdGVtLmRiQXR0ck5hbWUgPT09ICdDUkVBVEVEX0JZX1VTRVJfSUQnIHx8XHJcbiAgICAgICAgICBpdGVtLmRiQXR0ck5hbWUgPT09ICdDUkVBVElPTl9EQVRFJyB8fFxyXG4gICAgICAgICAgaXRlbS5kYkF0dHJOYW1lID09PSAnTEFTVF9VUERBVEVEX0JZX1VTRVInIHx8XHJcbiAgICAgICAgICBpdGVtLmRiQXR0ck5hbWUgPT09ICdOQU1FJ1xyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgdGVtcE9ialsnaXNBY3RpdmUnXSA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIFx0dGVtcE9ialsnaXNBY3RpdmUnXSA9IGZhbHNlO1xyXG4gICAgICAgIC8vIH1cclxuICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMudGFibGVDb25maWcgPSB0ZW1wQXJyYXk7XHJcbiAgICB9XHJcbiAgICBmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuICAgICAgaWYgKGNvbHVtbnMuaXNBY3RpdmUpIHtcclxuICAgICAgICB0aGlzLmNvbmZpZ1RyYWNrZXJIZWFkZXIucHVzaChjb2x1bW5zLm5hbWUpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlci5wdXNoKGNvbHVtbnMubmFtZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIHVwZGF0ZVRhYmxlKGV2ZW50KSB7XHJcbiAgICBsZXQgc2VsZWN0ZWRIZWFkZXIgPSB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXI7XHJcbiAgICB0aGlzLnRhYmxlQ29uZmlnLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKHNlbGVjdGVkSGVhZGVyLmluZGV4T2YoaXRlbS5uYW1lKSA+PSAwKSB7XHJcbiAgICAgICAgaXRlbS5pc0FjdGl2ZSA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaXRlbS5pc0FjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdjb250cm9sJyk7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY29udHJvbCcsIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVDb25maWcpKTtcclxuICAgIHRoaXMubG9hZFRhYmxlQ29sdW1uKHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCwgbnVsbCk7XHJcbiAgfVxyXG5cclxuICBvblNlbGVjdE1vZHVsZVJNKGV2ZW50LCBpc1JlZGlyZWN0KSB7XHJcbiAgICBldmVudCA9IGV2ZW50ICYmIGV2ZW50LnZhbHVlID8gZXZlbnQudmFsdWUgOiBldmVudDtcclxuICAgIHZhciB0ZW1wQXJyYXkgPSBbXTtcclxuICAgIGxldCB0ZW1wT2JqID0gdGhpcy5vYmplY3RfbmFtZV9STV9ieU1vZHVsZTtcclxuICAgIF8uZm9yRWFjaChldmVudCwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKHRlbXBPYmpbU3RyaW5nKGl0ZW0pXSkge1xyXG4gICAgICAgIGlmICh0ZW1wQXJyYXkgJiYgdGVtcEFycmF5Lmxlbmd0aCkge1xyXG4gICAgICAgICAgdGVtcEFycmF5ID0gXy5jb25jYXQodGVtcEFycmF5LCB0ZW1wT2JqW1N0cmluZyhpdGVtKV0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0ZW1wQXJyYXkgPSB0ZW1wT2JqW1N0cmluZyhpdGVtKV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuc2VsZWN0X21vZHVsZXNfUk0gPSBldmVudDtcclxuICAgIHRoaXMub2JqZWN0TmFtZV9STSA9IHRlbXBBcnJheTtcclxuICAgIHRoaXMuc3RhdHVzRmlsdGVyID0gW1xyXG4gICAgICB7XHJcbiAgICAgICAgdmFsdWU6ICdjaGFuZ2VkJyxcclxuICAgICAgICBuYW1lOiAnQ2hhbmdlZCdcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIHZhbHVlOiAnbm90Q2hhbmdlZCcsXHJcbiAgICAgICAgbmFtZTogJ05vdCBDaGFuZ2VkJ1xyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgdmFsdWU6ICdjcmVhdGVkJyxcclxuICAgICAgICBuYW1lOiAnQ3JlYXRlZCdcclxuICAgICAgfVxyXG4gICAgXTtcclxuICAgIGlmIChpc1JlZGlyZWN0KSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXIgPSB0ZW1wQXJyYXk7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRTdGF0dXMgPSB0aGlzLnN0YXR1c0ZpbHRlclswXTtcclxuICAgICAgLy8gdGhpcy5zZWxlY3RlZFRhYkhlYWRlci5wdXNoKHRlbXBBcnJheVswXSlcclxuICAgICAgdGhpcy5jaGFuZ2VUYWIoeyBpbmRleDogMCB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHVwZGF0ZVRhYihldmVudCwgZnJvbVRhYikge1xyXG4gICAgY29uc29sZS5sb2coJ3VwZGF0ZVRhYiA+Pj4+ICcsIGV2ZW50KTtcclxuICAgIGlmIChldmVudC52YWx1ZSkge1xyXG4gICAgICAvLyB0aGlzLnNlbGVjdG9iamhlYWRlciA9IGV2ZW50LnZhbHVlO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSGVhZGVySWQgPSB0aGlzLnNlbGVjdGVkVGFiSGVhZGVyWzBdLnR5cGU7XHJcbiAgICAgIC8vIHRoaXMuc2VsZWN0ZWRfZGF0YS5wdXNoKHRoaXMuZFNvdXJjZVtpXS5xdWVyeVR5cGVzW2pdLnZhbHVlKTtcclxuICAgIH0gZWxzZSBpZiAoZnJvbVRhYiA9PSAnZGFzaGJvYXJkJykge1xyXG4gICAgICB2YXIgc19vYmogPSBbXTtcclxuICAgICAgdGhpcy5zZWxlY3RlZFRhYkhlYWRlciA9IGV2ZW50O1xyXG4gICAgICB0aGlzLmNoYW5nZVRhYih7IGluZGV4OiAwIH0pO1xyXG4gICAgICAvLyB0aGlzLnJlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0LmZvckVhY2goZWxlbSA9PntcclxuICAgICAgLy8gXHRsZXQgaW5kZXggPSBldmVudC5maW5kSW5kZXgoXHJcbiAgICAgIC8vIFx0XHRvYmogPT4gb2JqLm5hbWUgPT09IGVsZW0ubmFtZVxyXG4gICAgICAvLyBcdCk7XHJcbiAgICAgIC8vIFx0aWYoaW5kZXggPiAtMSl7XHJcbiAgICAgIC8vIFx0XHRlbGVtLmNoZWNrZWQgPSB0cnVlXHJcbiAgICAgIC8vIFx0XHRzX29iai5wdXNoKGVsZW0ubmFtZSk7XHJcbiAgICAgIC8vIFx0fVxyXG5cclxuICAgICAgLy8gfSlcclxuICAgICAgLy8gdGhpcy5zZWxlY3RvYmpoZWFkZXIgPSBzX29iajtcclxuICAgICAgLy8gY29uc29sZS5sb2coJ3RoaXMuc2VsZWN0b2JqaGVhZGVyJyx0aGlzLnNlbGVjdG9iamhlYWRlcilcclxuICAgICAgLy8gY29uc29sZS5sb2coXCJ0eXBlcy4uLlwiLHRoaXMudHlwZXMsXCJyZXBvcnRtYW5hZ2VtZW50T2JqZWN0bGlzdC0tLS0tLS0tLVwiLHRoaXMucmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3QpXHJcbiAgICB9XHJcbiAgICB0aGlzLmdldFVzZXJOYW1lTGlzdCgnJyk7XHJcbiAgfVxyXG5cclxuICBhcHBseUZpbHRlcigpIHtcclxuICAgIC8vIGlmKHRoaXMuc2hvd19kYXRhID09ICdyZXBvcnRfbWFuYWdlbWVudCcpXHJcbiAgICB0aGlzLmNoYW5nZVRhYihudWxsKTtcclxuICB9XHJcblxyXG4gIHJlc2V0RmlsdGVyKCkge1xyXG4gICAgdGhpcy5zZWxlY3RfbW9kdWxlc19STSA9IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZFRhYkhlYWRlciA9IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZFVzZXIgPSAnJztcclxuICAgIHRoaXMuc2VsZWN0ZWRTdGF0dXMgPSAnJztcclxuICAgIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlciA9IFtdO1xyXG4gICAgdGhpcy5mcm9tSW5wdXQudmFsdWUgPSAnJztcclxuICAgIHRoaXMudG9JbnB1dC52YWx1ZSA9ICcnO1xyXG4gICAgdGhpcy5jb25maWdEYXRhID0gW107XHJcbiAgICB0aGlzLmNvbmZpZ1RyYWNrZXJIZWFkZXIgPSBbXTtcclxuICB9XHJcblxyXG4gIGNoYW5nZVRhYihldmVudCkge1xyXG4gICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgIGV2ZW50ID0gZXZlbnQuaW5kZXg7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCA9IHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXJbZXZlbnRdLnR5cGU7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRIZWFkZXJNb2R1bGVuYW1lID0gdGhpcy5zZWxlY3RlZFRhYkhlYWRlcltldmVudF0ubW9kdWxlX25hbWU7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRIZWFkZXJPYmplY3ROYW1lID0gdGhpcy5zZWxlY3RlZFRhYkhlYWRlcltldmVudF0ubmFtZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIC8vIGlmKCF0aGlzLnNlbGVjdGVkSGVhZGVySWQpe1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSGVhZGVySWQgPSB0aGlzLnNlbGVjdGVkVGFiSGVhZGVyWzBdLnR5cGU7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRIZWFkZXJNb2R1bGVuYW1lID0gdGhpcy5zZWxlY3RlZFRhYkhlYWRlclswXS5tb2R1bGVfbmFtZTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEhlYWRlck9iamVjdE5hbWUgPSB0aGlzLnNlbGVjdGVkVGFiSGVhZGVyWzBdLm5hbWU7XHJcbiAgICAgIC8vIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdGVkSGVhZGVySWQsICdTRUxFQ1RFRCcpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZFN0YXR1cywgJy4uLi4uLi4uLlNUQVRVUycpO1xyXG5cclxuICAgIHZhciBoZWFkZXJPcHRpb24gPSB7XHJcbiAgICAgIHR5cGU6IHRoaXMuc2VsZWN0ZWRIZWFkZXJJZFxyXG4gICAgfTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMudGFibGVDb25maWcsICctLS0tdGFibGVjb25maWcnKTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRIZWFkZXJEYXRhKGhlYWRlck9wdGlvbilcclxuICAgICAgLnN1YnNjcmliZSgoaGVhZGVyRGF0YXM6IGFueSkgPT4ge1xyXG4gICAgICAgIHZhciBoZWFkZXJEYXRhO1xyXG5cclxuICAgICAgICB0aGlzLmN1cnJlbnRIZWFkZXJEYXRhID0gaGVhZGVyRGF0YXMuYm9keS5yZXNwb25zZTtcclxuICAgICAgICB0aGlzLmxvYWRUYWJsZUNvbHVtbihcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCxcclxuICAgICAgICAgIHRoaXMuY3VycmVudEhlYWRlckRhdGEucGFyYW1zXHJcbiAgICAgICAgKTtcclxuICAgICAgICB2YXIgc2VsZWN0ZWRDb2x1bW4gPSBfLmZpbHRlcih0aGlzLnRhYmxlQ29uZmlnLCBbJ2lzSWRlbnRpZmllcicsIHRydWVdKTtcclxuICAgICAgICBzZWxlY3RlZENvbHVtbiA9IF8ubWFwKHNlbGVjdGVkQ29sdW1uLCAnbmFtZScpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkQ29sdW1uLCAnLi4uLi4uJyk7XHJcbiAgICAgICAgdGhpcy5kYXRlQ29sdW1ucyA9IF8uZmlsdGVyKHRoaXMudGFibGVDb25maWcsIFsndHlwZScsICdkYXRlJ10pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0ZUNvbHVtbnMsICcuLi4uLi5EYXRlJyk7XHJcbiAgICAgICAgbGV0IGRhdGVDb2x1bW5zID0gdGhpcy5kYXRlQ29sdW1ucztcclxuICAgICAgICB0aGlzLnVuaXF1ZUlEID0gc2VsZWN0ZWRDb2x1bW47XHJcbiAgICAgICAgdmFyIG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICBsaW1pdDogdGhpcy5saW1pdCxcclxuICAgICAgICAgIG9mZnNldDogdGhpcy5vZmZzZXQsXHJcbiAgICAgICAgICBjb25maWdUcmFja2VySWQ6IHRoaXMuY29uZmlnVHJhY2tlcklELFxyXG4gICAgICAgICAgdHlwZTogdGhpcy5zZWxlY3RlZEhlYWRlcklkLFxyXG4gICAgICAgICAgdW5pcXVlSUQ6IHRoaXMudW5pcXVlSUQsXHJcbiAgICAgICAgICBzZWxlY3RlZFVzZXJOYW1lOiB0aGlzLnNlbGVjdGVkVXNlciA/IHRoaXMuc2VsZWN0ZWRVc2VyIDogJycsXHJcbiAgICAgICAgICBzZWxlY3RlZFN0YXR1czogdGhpcy5zZWxlY3RlZFN0YXR1cyA/IHRoaXMuc2VsZWN0ZWRTdGF0dXMudmFsdWUgOiAnJyxcclxuICAgICAgICAgIGZyb21EYXRlOiB0aGlzLmZyb21JbnB1dC52YWx1ZSA/IHRoaXMuZnJvbUlucHV0LnZhbHVlIDogJycsXHJcbiAgICAgICAgICB0b0RhdGU6IHRoaXMudG9JbnB1dC52YWx1ZSA/IHRoaXMudG9JbnB1dC52YWx1ZSA6ICcnXHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZVxyXG4gICAgICAgICAgLmdldFJhd0RhdGEob3B0aW9ucylcclxuICAgICAgICAgIC5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAvLyB2YXIgZGVtb0NvbmZpZyA9IGRlbW9EYXRhLmxhc3RNb2RpZmllZERhdGE7XHJcbiAgICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEuYm9keSAmJiBkYXRhLmJvZHkucmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICB0aGlzLmNvbmZpZ0RhdGEgPSBkYXRhLmJvZHkucmVzcG9uc2UucHJvY2Vzc0RhdGFSZXNwXHJcbiAgICAgICAgICAgICAgICA/IGRhdGEuYm9keS5yZXNwb25zZS5wcm9jZXNzRGF0YVJlc3BcclxuICAgICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICAgICAgLy8gdGhpcy5oaXN0b3J5RGF0YSA9IGRhdGEuYm9keS5yZXNwb25zZS5oaXN0b3J5RGF0YTtcclxuICAgICAgICAgICAgICBsZXQgcHJvY2Vzc0FycmF5ID0gZGF0YS5ib2R5LnJlc3BvbnNlLnByb2Nlc3NEYXRhUmVzcFxyXG4gICAgICAgICAgICAgICAgPyBkYXRhLmJvZHkucmVzcG9uc2UucHJvY2Vzc0RhdGFSZXNwXHJcbiAgICAgICAgICAgICAgICA6IFtdO1xyXG5cclxuICAgICAgICAgICAgICBfLmZvckVhY2gocHJvY2Vzc0FycmF5LCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coaXRlbSwgJy0tLS0tSVRFTScpO1xyXG4gICAgICAgICAgICAgICAgLy8gbGV0IGlkID0gKGl0ZW0gJiYgaXRlbVtTdHJpbmcoc2VsZWN0ZWRDb2x1bW5bMF0ubmFtZSldKSA/IGl0ZW1bU3RyaW5nKHNlbGVjdGVkQ29sdW1uWzBdLm5hbWUpXVtcIm5hbWVcIl0gOiAnJztcclxuICAgICAgICAgICAgICAgIC8vIGxldCB0ZW1wT2JqID0gZGF0YS5ib2R5LnJlc3BvbnNlLmhpc3RvcnlEYXRhO1xyXG4gICAgICAgICAgICAgICAgaXRlbVsnaXNDaGFuZ2VkJ10gPVxyXG4gICAgICAgICAgICAgICAgICBpdGVtICYmIGl0ZW0uaGlzdG9yeURhdGEgJiYgaXRlbS5oaXN0b3J5RGF0YS5sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgICAgICAgICAgICA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgaXRlbVsnaXNBZGRlZCddID1cclxuICAgICAgICAgICAgICAgICAgaXRlbSAmJiBpdGVtLmhpc3RvcnlEYXRhICYmIGl0ZW0uaGlzdG9yeURhdGEubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgPyBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIDogaXRlbVsnaXNBZGRlZCddO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGVDb2x1bW5zICYmIGRhdGVDb2x1bW5zLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICBfLmZvckVhY2goZGF0ZUNvbHVtbnMsIGZ1bmN0aW9uIChkYXRlSXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBkYXRlU3RyaW5nID0gaXRlbVtTdHJpbmcoZGF0ZUl0ZW0ubmFtZSldWyduYW1lJ107XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIHNob3dEYXRlID0gbmV3IERhdGUoZGF0ZVN0cmluZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGZvcm1hdERhdGUgPSBtb21lbnQoc2hvd0RhdGUpLmZvcm1hdCgnbGxsJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbVtTdHJpbmcoZGF0ZUl0ZW0ubmFtZSldWyduYW1lJ10gPSBmb3JtYXREYXRlO1xyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB0aGlzLmxlbmd0aCA9IGRhdGEuYm9keS5yZXNwb25zZS50b3RhbDtcclxuICAgICAgICAgICAgICB0aGlzLmNvbmZpZ0RhdGEgPVxyXG4gICAgICAgICAgICAgICAgcHJvY2Vzc0FycmF5ICYmIHByb2Nlc3NBcnJheS5sZW5ndGggPyBwcm9jZXNzQXJyYXkgOiBbXTtcclxuICAgICAgICAgICAgICB0aGlzLmNvbmZpZ0RhdGEucGFnaW5hdG9yID0gdGhpcy5yZXBvcnRwYWdpbmF0b3I7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5jb25maWdEYXRhLCAnLi4uLi4uLkNvbmZpZ0RBVEFBQUFBJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICB9XHJcbiAgc2VsZWN0ZWRTdGF0dXM6IGFueTtcclxuICBzZWxlY3RlZFVzZXI6IGFueTtcclxuICBjaGFuZ2VQYWdlKGV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQucGFnZVNpemUgIT0gdGhpcy5saW1pdCkge1xyXG4gICAgICB0aGlzLmxpbWl0ID0gZXZlbnQucGFnZVNpemU7XHJcbiAgICAgIHRoaXMub2Zmc2V0ID0gMDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMub2Zmc2V0ID0gZXZlbnQucGFnZUluZGV4ICogdGhpcy5saW1pdDtcclxuICAgIH1cclxuICAgIHRoaXMuY2hhbmdlVGFiKG51bGwpO1xyXG4gIH1cclxuICBzZXRTdGVwKHJvd3MpIHtcclxuICAgIC8vIHRoaXMuc3RlcCA9IGluZGV4O1xyXG5cclxuICAgIHZhciBzZWxlY3RlZENvbHVtbiA9IF8uZmlsdGVyKHRoaXMudGFibGVDb25maWcsIFsnaXNJZGVudGlmaWVyJywgdHJ1ZV0pO1xyXG4gICAgdmFyIHF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICAvLyB0eXBlIDogdGhpcy5zZWxlY3RlZEhlYWRlcklkLFxyXG4gICAgICAvLyB1bmlxdWVJRCA6IHNlbGVjdGVkQ29sdW1uWzBdLm5hbWUsXHJcbiAgICAgIHNlbGVjdElkOiByb3dzW3NlbGVjdGVkQ29sdW1uWzBdLm5hbWVdWyduYW1lJ11cclxuICAgIH07XHJcbiAgICB0aGlzLmdldENvbmZpZ0hpc3RvcnkocXVlcnlQYXJhbXMpO1xyXG4gIH1cclxuXHJcbiAgZ2V0Q29uZmlnSGlzdG9yeShvcHRpb25zKSB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmhpc3RvcnlEYXRhLCAnLi4uLnRoaXMuaGlzdG9yeURhdGEnKTtcclxuICAgIHRoaXMuaGlzdG9yeUNvbmZpZ0RhdGEgPSB0aGlzLmhpc3RvcnlEYXRhW29wdGlvbnMuc2VsZWN0SWRdO1xyXG4gICAgLy8gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0SGlzdG9yeURhdGEob3B0aW9ucykuc3Vic2NyaWJlKChkYXRhSGlzdG9yeTogYW55KSA9PiB7XHJcbiAgICAvLyBcdGlmKGRhdGFIaXN0b3J5ICYmIGRhdGFIaXN0b3J5LmJvZHkgJiYgZGF0YUhpc3RvcnkuYm9keS5yZXNwb25zZSl7XHJcbiAgICAvLyBcdFx0dGhpcy5oaXN0b3J5Q29uZmlnRGF0YSA9IGRhdGFIaXN0b3J5LmJvZHkucmVzcG9uc2U7XHJcbiAgICAvLyBcdH1cclxuICAgIC8vIH0pO1xyXG4gIH1cclxuXHJcbiAgc2hvd2VkaXQoKSB7XHJcbiAgICAvLyBhbGVydCgnamJ2YycpXHJcbiAgICB0aGlzLm1vZGUgPSAnZWRpdCc7XHJcblxyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0QWxsTW9kdWxlcygpLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgIHRoaXMubW9kdWxlcyA9IFtdO1xyXG4gICAgICB0aGlzLnR5cGVzID0gW107XHJcbiAgICAgIHRoaXMucmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3QgPSBbXTtcclxuICAgICAgdGhpcy50eXBlc3NzID0gW107XHJcbiAgICAgIHZhciByZXMgPSBkYXRhLmJvZHkucmVzcG9uc2UuTW9kdWxlcztcclxuICAgICAgdGhpcy5tb2R1bGVzID0gZGF0YS5ib2R5LnJlc3BvbnNlLk1vZHVsZXM7XHJcbiAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YSA9IHRoaXMuZ2V0Q29uZmlnVGFibGU7XHJcbiAgICAgIGlmICh0aGlzLnNlbGVjdGVkX21vZHVsZXMubGVuZ3RoICE9IDApIHtcclxuICAgICAgICB0aGlzLmdldEFsbE9iamVjdHNOYW1lTGlzdChmdW5jdGlvbiAoKSB7fSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgc2hvd0hpc3RvcnkodHlwZSwgcm93cykge1xyXG4gICAgY29uc29sZS5sb2coJ3R5cGUtLS0tLS0tLS0nLCB0eXBlKTtcclxuICAgIGlmICh0eXBlID09ICd0cnVlJykge1xyXG4gICAgICB0aGlzLmV4cGFuZEVsZW1lbnQgPSB0cnVlO1xyXG4gICAgfSBlbHNlIGlmICh0eXBlID09ICdmYWxzZScpIHtcclxuICAgICAgdGhpcy5leHBhbmRFbGVtZW50ID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZygndGhpcy5leHBhbmRFbGVtZW50LS0tLS0tLS0tJywgdGhpcy5leHBhbmRFbGVtZW50KTtcclxuXHJcbiAgICB2YXIgc2VsZWN0ZWRDb2x1bW4gPSBfLmZpbHRlcih0aGlzLnRhYmxlQ29uZmlnLCBbJ2lzSWRlbnRpZmllcicsIHRydWVdKTtcclxuICAgIHZhciBxdWVyeVBhcmFtcyA9IHtcclxuICAgICAgLy8gdHlwZSA6IHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCxcclxuICAgICAgLy8gdW5pcXVlSUQgOiBzZWxlY3RlZENvbHVtblswXS5uYW1lLFxyXG4gICAgICBzZWxlY3RJZDogcm93c1tzZWxlY3RlZENvbHVtblswXS5uYW1lXVsnbmFtZSddXHJcbiAgICB9O1xyXG4gICAgdGhpcy5nZXRDb25maWdIaXN0b3J5KHF1ZXJ5UGFyYW1zKTtcclxuICB9XHJcblxyXG4gIGdldEFsbE9iamVjdHNOYW1lTGlzdChjYWxsYmFjaykge1xyXG4gICAgdmFyIG9wdGlvbnMgPSB7XHJcbiAgICAgIG1vZHVsZTogdGhpcy5zZWxlY3RlZF9tb2R1bGVzXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0T2JqZWN0cyhvcHRpb25zKS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICB2YXIgcmVzID0gZGF0YS5ib2R5LnJlc3BvbnNlLk1vZHVsZXM7XHJcbiAgICAgIHJlcy5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgICAgdmFyIG9iaiA9IHt9O1xyXG4gICAgICAgIG9ialsnbmFtZSddID0gaXRlbS5vYmplY3ROYW1lWzBdLm5hbWU7XHJcbiAgICAgICAgb2JqWyd2YWx1ZSddID0gaXRlbS5vYmplY3ROYW1lWzBdLm5hbWU7XHJcbiAgICAgICAgb2JqWyd0eXBlJ10gPSBpdGVtLm9iamVjdE5hbWVbMF0udHlwZTtcclxuICAgICAgICBvYmpbJ21vZHVsZV9uYW1lJ10gPSBpdGVtLm9iamVjdE5hbWVbMF0ubW9kdWxlX25hbWU7XHJcbiAgICAgICAgbGV0IGluZGV4ID0gdGhpcy5nZXRDb25maWdUYWJsZS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAob2JqKSA9PiBvYmoubmFtZSA9PT0gaXRlbS5vYmplY3ROYW1lWzBdLm5hbWVcclxuICAgICAgICApO1xyXG4gICAgICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICBvYmpbJ2NoZWNrZWQnXSA9IHRydWU7XHJcbiAgICAgICAgICBvYmpbJ2NvbnRyb2xfbmFtZSddID0gdGhpcy5nZXRDb25maWdUYWJsZVtpbmRleF0uY29udHJvbF9uYW1lO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX3R5cGUnXSA9IHRoaXMuZ2V0Q29uZmlnVGFibGVbaW5kZXhdLmNvbnRyb2xfdHlwZTtcclxuICAgICAgICAgIG9ialsncmlza1JhbmsnXSA9IHRoaXMuZ2V0Q29uZmlnVGFibGVbaW5kZXhdLnJpc2tSYW5rO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBvYmpbJ2NoZWNrZWQnXSA9IGZhbHNlO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX25hbWUnXSA9ICcnO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX3R5cGUnXSA9ICcnO1xyXG4gICAgICAgICAgb2JqWydyaXNrUmFuayddID0gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMudHlwZXMucHVzaChvYmopO1xyXG4gICAgICAgIHRoaXMucmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3QucHVzaChvYmopO1xyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2coXHJcbiAgICAgICAgJ3JlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0LS0tLS0tLS0tLS0+Pi4uLicsXHJcbiAgICAgICAgdGhpcy5yZXBvcnRtYW5hZ2VtZW50T2JqZWN0bGlzdFxyXG4gICAgICApO1xyXG4gICAgICB0aGlzLm9iamVjdF9saXN0ID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLnR5cGVzKTtcclxuICAgICAgdGhpcy5vYmplY3RfbGlzdC5wYWdpbmF0b3IgPSB0aGlzLnBhZ2luYXRvcjtcclxuICAgICAgdGhpcy5vYmplY3RfbGlzdF9sZW5ndGggPSB0aGlzLnR5cGVzLmxlbmd0aDtcclxuICAgIH0pO1xyXG4gICAgY2FsbGJhY2soKTtcclxuICB9XHJcblxyXG4gIGNoZWNrRXhpdHMoc2VsZWN0ZWRSb3cpOiBib29sZWFuIHtcclxuICAgIGlmIChzZWxlY3RlZFJvdyA9PSB0cnVlKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25TZWFyY2hOYW1lKGZpbHRlclZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRfZGF0YS5maWx0ZXIgPSBmaWx0ZXJWYWx1ZS50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRfZGF0YS5wYWdpbmF0b3IgPSB0aGlzLnNwYWdpbmF0b3I7XHJcbiAgfVxyXG5cclxuICBvblNlYXJjaE9iamVjdE5hbWUoZmlsdGVyVmFsdWU6IHN0cmluZykge1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdC5maWx0ZXIgPSBmaWx0ZXJWYWx1ZS50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuICAgIHRoaXMub2JqZWN0X2xpc3QucGFnaW5hdG9yID0gdGhpcy5wYWdpbmF0b3I7XHJcbiAgICB0aGlzLm9iamVjdF9saXN0X2xlbmd0aCA9IHRoaXMudHlwZXMubGVuZ3RoO1xyXG4gIH1cclxuXHJcbiAgb25TZWxlY3RNb2R1bGUoZXZlbnQpIHtcclxuICAgIGxldCBrZXlCeU9iamVjdE5hbWVzID0gXy5ncm91cEJ5KHRoaXMuZ2V0Q29uZmlnVGFibGUsICdtb2R1bGVfbmFtZScpO1xyXG4gICAgXy5mb3JFYWNoKGV2ZW50LnZhbHVlLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBpZiAodGhpcy5vblNlbGVjdGVkTW9kdWxlT2JqZWN0cyAmJiB0aGlzLm9uU2VsZWN0ZWRNb2R1bGVPYmplY3RzLmxlbmd0aCkge1xyXG4gICAgICAgIHRoaXMub25TZWxlY3RlZE1vZHVsZU9iamVjdHMgPSBfLmNvbmNhdChcclxuICAgICAgICAgIHRoaXMub25TZWxlY3RlZE1vZHVsZU9iamVjdHMsXHJcbiAgICAgICAgICBrZXlCeU9iamVjdE5hbWVzW2l0ZW1dXHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLm9uU2VsZWN0ZWRNb2R1bGVPYmplY3RzID0ga2V5QnlPYmplY3ROYW1lc1tpdGVtXTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZURhdGFzb3VyY2VJZChpZCwgdHlwZSkge1xyXG4gICAgLy90aGlzLm5neFNlcnZpY2Uuc3RhcnQoKTtcclxuICAgIHZhciBtb2R1bGVzID0gW107XHJcbiAgICBpZiAodHlwZSA9PSAnZGFzaGJvYXJkJykge1xyXG4gICAgICB0aGlzLnNlbGVjdF9tb2R1bGVzID0gaWQ7XHJcbiAgICAgIG1vZHVsZXMgPSBpZDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG1vZHVsZXMgPSBpZC52YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnR5cGVzID0gW107XHJcblxyXG4gICAgdGhpcy5zZWxlY3RlZF9vYmplY3RzID0gW107XHJcblxyXG4gICAgdmFyIG9wdGlvbnMgPSB7XHJcbiAgICAgIG1vZHVsZTogbW9kdWxlc1xyXG4gICAgfTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldE9iamVjdHMob3B0aW9ucykuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgdmFyIHJlcyA9IGRhdGEuYm9keS5yZXNwb25zZS5Nb2R1bGVzO1xyXG4gICAgICByZXMuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICAgIHZhciBvYmogPSB7fTtcclxuXHJcbiAgICAgICAgb2JqWyduYW1lJ10gPSBpdGVtLm9iamVjdE5hbWVbMF0ubmFtZTtcclxuICAgICAgICBvYmpbJ3ZhbHVlJ10gPSBpdGVtLm9iamVjdE5hbWVbMF0ubmFtZTtcclxuICAgICAgICBvYmpbJ3R5cGUnXSA9IGl0ZW0ub2JqZWN0TmFtZVswXS50eXBlO1xyXG4gICAgICAgIG9ialsnbW9kdWxlX25hbWUnXSA9IGl0ZW0ub2JqZWN0TmFtZVswXS5tb2R1bGVfbmFtZTtcclxuICAgICAgICBpZiAodGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgIGxldCBpbmRleCA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIChvYmopID0+IG9iai5uYW1lID09PSBpdGVtLm9iamVjdE5hbWVbMF0ubmFtZVxyXG4gICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICAgICAgICBvYmpbJ2NoZWNrZWQnXSA9IHRydWU7XHJcbiAgICAgICAgICAgIG9ialsnY29udHJvbF9uYW1lJ10gPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFbaW5kZXhdLmNvbnRyb2xfbmFtZTtcclxuICAgICAgICAgICAgb2JqWydjb250cm9sX3R5cGUnXSA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YVtpbmRleF0uY29udHJvbF90eXBlO1xyXG4gICAgICAgICAgICBvYmpbJ3Jpc2tSYW5rJ10gPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFbaW5kZXhdLnJpc2tSYW5rO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgb2JqWydjaGVja2VkJ10gPSBmYWxzZTtcclxuICAgICAgICAgICAgb2JqWydjb250cm9sX25hbWUnXSA9ICcnO1xyXG4gICAgICAgICAgICBvYmpbJ2NvbnRyb2xfdHlwZSddID0gJyc7XHJcbiAgICAgICAgICAgIG9ialsncmlza1JhbmsnXSA9ICcnO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBvYmpbJ2NoZWNrZWQnXSA9IGZhbHNlO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX25hbWUnXSA9ICcnO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX3R5cGUnXSA9ICcnO1xyXG4gICAgICAgICAgb2JqWydyaXNrUmFuayddID0gJyc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBpZihvYmpbXCJjaGVja2VkXCJdID09IHRydWUpe1xyXG4gICAgICAgIC8vIFx0dGhpcy5pc0NoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgIC8vIH1cclxuICAgICAgICAvLyBlbHNlIGlmKG9ialtcImNoZWNrZWRcIl0gPT0gZmFsc2Upe1xyXG4gICAgICAgIC8vIFx0dGhpcy5pc0NoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAvLyB9XHJcblxyXG4gICAgICAgIHRoaXMudHlwZXMucHVzaChvYmopO1xyXG4gICAgICAgIHRoaXMucmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3QucHVzaChvYmopO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5vYmplY3RfbGlzdCA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy50eXBlcyk7XHJcbiAgICAgIHRoaXMub2JqZWN0X2xpc3QucGFnaW5hdG9yID0gdGhpcy5wYWdpbmF0b3I7XHJcbiAgICAgIHRoaXMub2JqZWN0X2xpc3RfbGVuZ3RoID0gdGhpcy50eXBlcy5sZW5ndGg7XHJcbiAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YSA9IF8uZmlsdGVyKHRoaXMudHlwZXMsIFsnY2hlY2tlZCcsIHRydWVdKTtcclxuICAgICAgLy90aGlzLm5neFNlcnZpY2Uuc3RvcCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZVR5cGUoZWxlbWVudCwgZXZlbnQsIHR5cGUpIHtcclxuICAgIGlmICh0eXBlID09ICdhbGwnKSB7XHJcbiAgICAgIGlmIChldmVudC5jaGVja2VkID09IHRydWUpIHtcclxuICAgICAgICB0aGlzLmlzQ2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy50eXBlcy5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgICAgICBpdGVtLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgdmFyIG9iaiA9IHt9O1xyXG4gICAgICAgICAgb2JqWyduYW1lJ10gPSBpdGVtLm5hbWU7XHJcbiAgICAgICAgICBvYmpbJ3ZhbHVlJ10gPSBpdGVtLnZhbHVlO1xyXG4gICAgICAgICAgb2JqWyd0eXBlJ10gPSBpdGVtLnR5cGU7XHJcbiAgICAgICAgICBvYmpbJ21vZHVsZV9uYW1lJ10gPSBpdGVtLm1vZHVsZV9uYW1lO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX25hbWUnXSA9IGl0ZW0uY29udHJvbF9uYW1lO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX3R5cGUnXSA9IGl0ZW0uY29udHJvbF90eXBlO1xyXG4gICAgICAgICAgb2JqWydyaXNrUmFuayddID0gaXRlbS5yaXNrUmFuaztcclxuICAgICAgICAgIG9ialsnY2hlY2tlZCddID0gaXRlbS5jaGVja2VkO1xyXG4gICAgICAgICAgdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLnB1c2gob2JqKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIGlmIChldmVudC5jaGVja2VkID09IGZhbHNlKSB7XHJcbiAgICAgICAgdGhpcy5pc0NoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnR5cGVzLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICAgIGl0ZW0uY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YSA9IFtdO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ3NpbmdsZScpIHtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAodGVtcCkgPT4gdGVtcC5uYW1lID09PSBlbGVtZW50Lm5hbWVcclxuICAgICAgICApIDwgMFxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoZXZlbnQuY2hlY2tlZCA9PSB0cnVlKSB7XHJcbiAgICAgICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEucHVzaChlbGVtZW50KTtcclxuICAgICAgICAgIGxldCBpaW5kZXggPSB0aGlzLnR5cGVzLmZpbmRJbmRleChcclxuICAgICAgICAgICAgKG9ianQpID0+IG9ianQubmFtZSA9PT0gZWxlbWVudC5uYW1lXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy50eXBlc1tpaW5kZXhdLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZXZlbnQuY2hlY2tlZCA9PSBmYWxzZSkge1xyXG4gICAgICAgICAgbGV0IGluZGV4ID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmZpbmRJbmRleChcclxuICAgICAgICAgICAgKG9iaikgPT4gb2JqLm5hbWUgPT09IGVsZW1lbnQubmFtZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgbGV0IGlpbmRleCA9IHRoaXMudHlwZXMuZmluZEluZGV4KFxyXG4gICAgICAgICAgICAob2JqdCkgPT4gb2JqdC5uYW1lID09PSBlbGVtZW50Lm5hbWVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLnR5cGVzW2lpbmRleF0uY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgQ29uZmlybURhdGEoKSB7XHJcbiAgICBjb25zb2xlLmxvZygnQ29uZmlybURhdGEtLS0tLS0tLS0tLT4+PicsIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YSk7XHJcbiAgICBjb25zb2xlLmxvZygnc2VsZWN0ZWRNb2R1bGVzLS0tLS0tLS0tLS0+Pj4nLCB0aGlzLnNlbGVjdGVkX21vZHVsZXMpO1xyXG4gICAgdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmZvckVhY2goKGl0ZW0sIGluZGV4eCkgPT4ge1xyXG4gICAgICBsZXQgaW5kZXggPSB0aGlzLnNlbGVjdGVkX21vZHVsZXMuZmluZEluZGV4KFxyXG4gICAgICAgIChvYmopID0+IG9iaiA9PT0gaXRlbS5tb2R1bGVfbmFtZVxyXG4gICAgICApO1xyXG5cclxuICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnSXRlbS0tLS0tLS0tLS0tJywgaXRlbS5tb2R1bGVfbmFtZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ0l0ZW1FTFNFLS0tLS0tLS0tLS0nLCBpbmRleCk7XHJcbiAgICAgICAgdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLnNwbGljZShpbmRleHgsIDEpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGNvbnNvbGUubG9nKCdDb25maXJtRGF0YUZpbmFsLS0tLS0tLS0tLS0+Pj4nLCB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEpO1xyXG4gICAgdGhpcy5maW5hbF9zZWxlY3RlZF90YWJsZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UoXHJcbiAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YVxyXG4gICAgKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfdGFibGUucGFnaW5hdG9yID0gdGhpcy5mcGFnaW5hdG9yO1xyXG4gICAgdGhpcy5maW5hbF9zZWxlY3RlZF9sZW5ndGggPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEubGVuZ3RoO1xyXG4gIH1cclxuXHJcbiAgUHJldmlvdXNEYXRhKCkge1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdCA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy50eXBlcyk7XHJcbiAgICB0aGlzLm9iamVjdF9saXN0LnBhZ2luYXRvciA9IHRoaXMucGFnaW5hdG9yO1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdF9sZW5ndGggPSB0aGlzLnR5cGVzLmxlbmd0aDtcclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlTmFtZShldmVudCwgbmFtZSkge1xyXG4gICAgbGV0IGlpbmRleCA9IHRoaXMudHlwZXMuZmluZEluZGV4KChvYmopID0+IG9iai5uYW1lID09PSBuYW1lKTtcclxuICAgIHRoaXMudHlwZXNbaWluZGV4XS5jb250cm9sX25hbWUgPSBldmVudDtcclxuXHJcbiAgICBsZXQgaSA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoKG9ianQpID0+IG9ianQubmFtZSA9PT0gbmFtZSk7XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFbaV0uY29udHJvbF9uYW1lID0gZXZlbnQ7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZUNvbnRyb2xUeXBlKGV2ZW50LCBuYW1lKSB7XHJcbiAgICBsZXQgaWluZGV4ID0gdGhpcy50eXBlcy5maW5kSW5kZXgoKG9iaikgPT4gb2JqLm5hbWUgPT09IG5hbWUpO1xyXG4gICAgdGhpcy50eXBlc1tpaW5kZXhdLmNvbnRyb2xfdHlwZSA9IGV2ZW50O1xyXG5cclxuICAgIGxldCBpID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmZpbmRJbmRleCgob2JqdCkgPT4gb2JqdC5uYW1lID09PSBuYW1lKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YVtpXS5jb250cm9sX3R5cGUgPSBldmVudDtcclxuICB9XHJcblxyXG4gIG9uQ2hhbmdlUmlzayhldmVudCwgbmFtZSkge1xyXG4gICAgbGV0IGlpbmRleCA9IHRoaXMudHlwZXMuZmluZEluZGV4KChvYmopID0+IG9iai5uYW1lID09PSBuYW1lKTtcclxuICAgIHRoaXMudHlwZXNbaWluZGV4XS5yaXNrUmFuayA9IGV2ZW50O1xyXG5cclxuICAgIGxldCBpID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmZpbmRJbmRleCgob2JqdCkgPT4gb2JqdC5uYW1lID09PSBuYW1lKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YVtpXS5yaXNrUmFuayA9IGV2ZW50O1xyXG4gIH1cclxuXHJcbiAgZXhwb3J0cGRmKCkge1xyXG4gICAgLy90aGlzLm5neFNlcnZpY2Uuc3RhcnQoKTtcclxuICAgIHRoaXMubG9naW5Vc2VyID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnY3VycmVudExvZ2luVXNlcicpKTtcclxuICAgIGNvbnNvbGUubG9nKCd0aGlzLmxvZ2luVXNlci51c2VybmFtZSA+Pj4+PiAnLCB0aGlzLmxvZ2luVXNlclsndXNlcm5hbWUnXSk7XHJcbiAgICB2YXIgcXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIHR5cGU6IHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCxcclxuICAgICAgdXNlcm5hbWU6IHRoaXMubG9naW5Vc2VyLnVzZXJuYW1lLFxyXG4gICAgICBtb2R1bGVfbmFtZTogdGhpcy5zZWxlY3RlZEhlYWRlck1vZHVsZW5hbWUsXHJcbiAgICAgIG9iamVjdF9uYW1lOiB0aGlzLnNlbGVjdGVkSGVhZGVyT2JqZWN0TmFtZSxcclxuICAgICAgc2VsZWN0ZWRVc2VyTmFtZTogdGhpcy5zZWxlY3RlZFVzZXIgPyB0aGlzLnNlbGVjdGVkVXNlciA6ICcnLFxyXG4gICAgICBzZWxlY3RlZFN0YXR1czogdGhpcy5zZWxlY3RlZFN0YXR1cyA/IHRoaXMuc2VsZWN0ZWRTdGF0dXMudmFsdWUgOiAnJyxcclxuICAgICAgY29uZmlnVHJhY2tlcklkOiB0aGlzLmNvbmZpZ1RyYWNrZXJJRCxcclxuICAgICAgdW5pcXVlSUQ6IHRoaXMudW5pcXVlSUQsXHJcbiAgICAgIGZyb21EYXRlOiB0aGlzLmZyb21JbnB1dC52YWx1ZSA/IHRoaXMuZnJvbUlucHV0LnZhbHVlIDogJycsXHJcbiAgICAgIHRvRGF0ZTogdGhpcy50b0lucHV0LnZhbHVlID8gdGhpcy50b0lucHV0LnZhbHVlIDogJydcclxuICAgIH07XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRTY2hlZHVsZVBERihxdWVyeVBhcmFtcykuc3Vic2NyaWJlKFxyXG4gICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtyZXMuYm9keV0sIHsgdHlwZTogJ2FwcGxpY2F0aW9uL3BkZicgfSk7XHJcbiAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhibG9iLCAnQ29uZmlnVHJhY2tlclJlcG9ydC5wZGYnKTtcclxuICAgICAgICAvL3RoaXMubmd4U2VydmljZS5zdG9wKCk7XHJcbiAgICAgIH0sXHJcbiAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgIC8vdGhpcy5uZ3hTZXJ2aWNlLnN0b3AoKTtcclxuICAgICAgICBjb25zb2xlLmxvZygnZXJyb3I6OjonICsgSlNPTi5zdHJpbmdpZnkoZXJyb3IpKTtcclxuICAgICAgfVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIG9uU3VibWl0KHR5cGUpIHtcclxuICAgIGlmICh0eXBlID09ICdmb3JtU3VibWl0Jykge1xyXG4gICAgICB2YXIgc19tb2R1bGUgPSBbXTtcclxuICAgICAgdmFyIG9ial9uYW1lID0gW107XHJcbiAgICAgIHNfbW9kdWxlID0gdGhpcy5DcmVhdGVGb3JtLmNvbnRyb2xzWydtb2R1bGUnXS52YWx1ZTtcclxuICAgICAgb2JqX25hbWUgPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGE7XHJcbiAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ2Zvcm1EZWxldGUnKSB7XHJcbiAgICAgIHZhciBzX21vZHVsZSA9IFtdO1xyXG4gICAgICB2YXIgb2JqX25hbWUgPSBbXTtcclxuICAgICAgc19tb2R1bGUgPSB0aGlzLnNlbGVjdGVkX21vZHVsZXM7XHJcbiAgICAgIG9ial9uYW1lID0gdGhpcy5zZWxlY3RlZF9kYXRhc291cmNlO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgdmFyIG9iaiA9IHtcclxuICAgICAgb2JqZWN0X25hbWU6IG9ial9uYW1lLFxyXG4gICAgICBtb2R1bGU6IHNfbW9kdWxlLFxyXG4gICAgICB1c2VySWQ6IHRoaXMuZFNvdXJjZVswXS51c2VySWQsXHJcbiAgICAgIF9pZDogdGhpcy5kU291cmNlWzBdLl9pZCxcclxuICAgICAgY2xpZW50SUQ6IHRoaXMuZFNvdXJjZVswXS5jbGllbnRJRCxcclxuICAgICAgZnVzaW9uVXJsOiB0aGlzLmRTb3VyY2VbMF0uZnVzaW9uVXJsLFxyXG4gICAgICB1c2VyTmFtZTogdGhpcy5kU291cmNlWzBdLnVzZXJOYW1lLFxyXG4gICAgICBwYXNzd29yZDogdGhpcy5kU291cmNlWzBdLnBhc3N3b3JkLFxyXG4gICAgICBzY2hlZHVsZVR5cGU6IHRoaXMuZFNvdXJjZVswXS5zY2hlZHVsZVR5cGUsXHJcbiAgICAgIHNjaGVkdWxlVHlwZVZhbHVlOiB0aGlzLmRTb3VyY2VbMF0uc2NoZWR1bGVUeXBlVmFsdWUsXHJcbiAgICAgIHJldGFpbkRheXM6IHRoaXMuZFNvdXJjZVswXS5yZXRhaW5EYXlzLFxyXG4gICAgICBfX3Y6IHRoaXMuZFNvdXJjZVswXS5fX3YsXHJcbiAgICAgIHF1ZXJ5VHlwZXM6IF8ubWFwKG9ial9uYW1lLCAndHlwZScpLFxyXG4gICAgICBkZWxldGVkOiBmYWxzZVxyXG4gICAgfTtcclxuICAgIHRoaXMuU2V0dXBBZG1pbmlzdHJhdGlvblNlcnZpY2UudXBkYXRlQ29uZmlnRGV0YWlscyhcclxuICAgICAgb2JqLFxyXG4gICAgICB0aGlzLmRTb3VyY2VbMF0udXNlcklkXHJcbiAgICApLnN1YnNjcmliZShcclxuICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgIGlmIChyZXMuc3RhdHVzID09IDIwMSkge1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKHJlcy5ib2R5Lm1ldGEubXNnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKHJlcy5ib2R5Lm1ldGEubXNnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgIHRoaXMuZXJyb3JTdGF0dXMgPSBlcnJvci5lcnJvci5tZXRhLnN0YXR1cztcclxuICAgICAgICBpZiAodGhpcy5lcnJvclN0YXR1cyA9PSAnNTAwJyB8fCB0aGlzLmVycm9yU3RhdHVzID09ICc0MDAnKSB7XHJcbiAgICAgICAgICB0aGlzLmVycm9yTXNnID0gZXJyb3IuZXJyb3IubWV0YS5tc2c7XHJcbiAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKHRoaXMuZXJyb3JNc2cpO1xyXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc3VibWl0dGVkID0gZmFsc2U7XHJcbiAgICAgICAgICB9LCAzMDAwKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICk7XHJcblxyXG4gICAgdGhpcy5nZXRhbGxjb25maWdzKCk7XHJcbiAgfVxyXG5cclxuICBydW5EYXRhKCkge1xyXG4gICAgdmFyIHVzZXJJZCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCd1c2VySWQnKTtcclxuICAgIHZhciBxdWVyeVBhcmFtcyA9IHtcclxuICAgICAgY2xpZW50SUQ6IHRoaXMuY2xpZW50SUQsXHJcbiAgICAgIHVzZXJJZDogdXNlcklkXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2VcclxuICAgICAgLnN0YXJ0RGF0YVB1bGwocXVlcnlQYXJhbXMpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQoJ1J1bi8gU3luYyBEYXRhIFN0YXJ0ZWQgU3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlU2VsZWN0ZWRDb25maWdzRmluYWwoZWxlbWVudCkge1xyXG4gICAgbGV0IGluZGV4ID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmZpbmRJbmRleChcclxuICAgICAgKG9iaikgPT4gb2JqLm5hbWUgPT09IGVsZW1lbnQubmFtZVxyXG4gICAgKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG5cclxuICAgIGxldCBpaW5kZXggPSB0aGlzLnR5cGVzLmZpbmRJbmRleCgob2JqdCkgPT4gb2JqdC5uYW1lID09PSBlbGVtZW50Lm5hbWUpO1xyXG4gICAgdGhpcy50eXBlc1tpaW5kZXhdLmNoZWNrZWQgPSBmYWxzZTtcclxuXHJcbiAgICBkZWxldGUgdGhpcy5maW5hbF9zZWxlY3RlZF90YWJsZVtlbGVtZW50Lm5hbWVdO1xyXG4gICAgdGhpcy5maW5hbF9zZWxlY3RlZF90YWJsZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UoXHJcbiAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YVxyXG4gICAgKTtcclxuICAgIHRoaXMub2JqZWN0X2xpc3QgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMudHlwZXMpO1xyXG4gICAgdGhpcy5maW5hbF9zZWxlY3RlZF90YWJsZS5wYWdpbmF0b3IgPSB0aGlzLmZwYWdpbmF0b3I7XHJcbiAgICB0aGlzLm9iamVjdF9saXN0LnBhZ2luYXRvciA9IHRoaXMucGFnaW5hdG9yO1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdF9sZW5ndGggPSB0aGlzLnR5cGVzLmxlbmd0aDtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfbGVuZ3RoID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmxlbmd0aDtcclxuICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoJ0NvbmZpZyBSZW1vdmVkIFN1Y2Nlc3NmdWxseScpO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlU2VsZWN0ZWRDb25maWdzKGVsZW1lbnQpIHtcclxuICAgIGxldCBpbmRleCA9IHRoaXMuc2VsZWN0ZWRfZGF0YXNvdXJjZS5maW5kSW5kZXgoXHJcbiAgICAgIChvYmopID0+IG9iai5uYW1lID09PSBlbGVtZW50Lm5hbWVcclxuICAgICk7XHJcbiAgICB0aGlzLnNlbGVjdGVkX2RhdGFzb3VyY2Uuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIGRlbGV0ZSB0aGlzLnNlbGVjdGVkX2RhdGFzb3VyY2VbZWxlbWVudC5uYW1lXTtcclxuICAgIHRoaXMuc2VsZWN0ZWRfZGF0YSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy5zZWxlY3RlZF9kYXRhc291cmNlKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRfZGF0YS5wYWdpbmF0b3IgPSB0aGlzLnNwYWdpbmF0b3I7XHJcbiAgICB0aGlzLm9uU3VibWl0KCdmb3JtRGVsZXRlJyk7XHJcbiAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKCdDb25maWcgUmVtb3ZlZCBTdWNjZXNzZnVsbHknKTtcclxuICB9XHJcblxyXG4gIGdldFVzZXJOYW1lTGlzdCh0ZW1wdmFsdWVzKSB7XHJcbiAgICBsZXQgb2JqZWN0TGlzdHMgPSBfLm1hcCh0aGlzLnNlbGVjdGVkVGFiSGVhZGVyLCAndHlwZScpO1xyXG4gICAgdmFyIHF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICBzZWxlY3RlZE9iamVjdDogb2JqZWN0TGlzdHNcclxuICAgIH07XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZVxyXG4gICAgICAuZ2V0VXBkYXRlZFVzZXJMaXN0KHF1ZXJ5UGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5ib2R5ICYmIGRhdGEuYm9keVsncmVzcG9uc2UnXSkge1xyXG4gICAgICAgICAgdGhpcy51c2VyTGlzdCA9IGRhdGEuYm9keVsncmVzcG9uc2UnXS5sZW5ndGhcclxuICAgICAgICAgICAgPyBkYXRhLmJvZHlbJ3Jlc3BvbnNlJ11cclxuICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgIGlmICh0ZW1wdmFsdWVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRVc2VyID0gdGVtcHZhbHVlcy5uYW1lO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBUb2dnbGUoaWQsIHRlbXB2YWx1ZXMsIHR5cGVDaGFydCwgY2hhcnROdW0pIHtcclxuICAgIGlmICh0aGlzLmNvbmZpZ1FUU2V0dXBEb25lICYmIHRoaXMuY29uZmlnVHJhY2tlclNldHVwRG9uZSkge1xyXG4gICAgICBpZiAoaWQgPT0gJ2NvbmZpZ190cmFja2VyJykge1xyXG4gICAgICAgIHRoaXMuc2hvd19kYXRhID0gJ2NvbmZpZ190cmFja2VyJztcclxuICAgICAgfSBlbHNlIGlmIChpZCA9PSAnZGFzaGJvYXJkJykge1xyXG4gICAgICAgIHRoaXMuc2hvd19kYXRhID0gJ2Rhc2hib2FyZCc7XHJcbiAgICAgICAgdGhpcy5jaGFuZ2VzYnltb2R1bGUoKTtcclxuICAgICAgICB0aGlzLndlZWtieXVzZXJDaGFydCgpO1xyXG4gICAgICAgIHRoaXMubW9udGhieXVzZXJDaGFydCgpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuc2hvd19kYXRhID0gJ3JlcG9ydF9tYW5hZ2VtZW50JztcclxuICAgICAgICBpZiAodGVtcHZhbHVlcyAmJiB0ZW1wdmFsdWVzICE9ICcnKSB7XHJcbiAgICAgICAgICB2YXIgbW9kdWxlTmFtZSA9IFtdO1xyXG4gICAgICAgICAgaWYgKHR5cGVDaGFydCA9PT0gJ21vZHVsZScpIHtcclxuICAgICAgICAgICAgbW9kdWxlTmFtZS5wdXNoKHRlbXB2YWx1ZXMubmFtZSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBtb2R1bGVOYW1lID0gdGhpcy5tb2R1bGVfUk07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLm9uQ2hhbmdlRGF0YXNvdXJjZUlkKG1vZHVsZU5hbWUsICdkYXNoYm9hcmQnKTtcclxuICAgICAgICAgIHRoaXMub25TZWxlY3RNb2R1bGVSTShtb2R1bGVOYW1lLCAncmVkaXJlY3QnKTtcclxuICAgICAgICAgIHRoaXMuZ2V0QWxsT2JqZWN0c05hbWVMaXN0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coJ0dFVCcpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBpZiAodHlwZUNoYXJ0ID09PSAndXNlcicpIHtcclxuICAgICAgICAgICAgaWYgKGNoYXJ0TnVtID09IDEpIHtcclxuICAgICAgICAgICAgICB0aGlzLmdldFVzZXJOYW1lTGlzdCh0ZW1wdmFsdWVzKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChjaGFydE51bSA9PSAyKSB7XHJcbiAgICAgICAgICAgICAgbGV0IHRlbXBTcGxpdE5hbWUgPSB0ZW1wdmFsdWVzLm5hbWUuc3BsaXQoJygnKTtcclxuICAgICAgICAgICAgICB0ZW1wdmFsdWVzLm5hbWUgPSB0ZW1wU3BsaXROYW1lWzBdO1xyXG4gICAgICAgICAgICAgIHRoaXMuZ2V0VXNlck5hbWVMaXN0KHRlbXB2YWx1ZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vY29uc29sZS5sb2codGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuY29uZmlnVHJhY2tlckRldGFpbHMpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvL0NoYXJ0IHBvcnRpb25zXHJcbiAgY2hhbmdlc2J5bW9kdWxlKCkge1xyXG4gICAgdGhpcy5jb25mdGNoYXJ0Ynltb2R1bGUgPSBbXTtcclxuICAgIGxldCBxdWVyeVBhcmFtcyA9IHtcclxuICAgICAgbW9kdWxlczogdGhpcy5zZWxlY3RlZF9tb2R1bGVzLFxyXG4gICAgICBjb25maWdUcmFja2VySUQ6IHRoaXMuY29uZmlnVHJhY2tlcklEXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2VcclxuICAgICAgLmNvbmZUcmFja0J5TW9kdWxlQ2hhcnQocXVlcnlQYXJhbXMpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICB2YXIgcmVzdWx0ID0gXy5jaGFpbihkYXRhLmJvZHkucmVzcG9uc2UpXHJcbiAgICAgICAgICAuZ3JvdXBCeSgnX2lkLm1vZHVsZU5hbWUnKVxyXG4gICAgICAgICAgLnRvUGFpcnMoKVxyXG4gICAgICAgICAgLm1hcCgob2Jqcywga2V5KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKG9ianMpO1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgIG1vZHVsZW5hbWU6IG9ianNbMF0sXHJcbiAgICAgICAgICAgICAgY291bnQ6IF8uc3VtQnkob2Jqc1sxXSwgJ2NvdW50JylcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAudmFsdWUoKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQsICcuLi5SJyk7XHJcblxyXG4gICAgICAgIHJlc3VsdC5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB0ZW1wQXJyYXk6IGFueSA9IHt9O1xyXG4gICAgICAgICAgdGVtcEFycmF5Lm5hbWUgPSBlbGVtZW50Lm1vZHVsZW5hbWU7XHJcbiAgICAgICAgICB0ZW1wQXJyYXkudmFsdWUgPSBlbGVtZW50LmNvdW50O1xyXG4gICAgICAgICAgdGhpcy5jb25mdGNoYXJ0Ynltb2R1bGUucHVzaCh0ZW1wQXJyYXkpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY29uZnRjaGFydGJ5bW9kdWxlLCAnLS0tLWNvbmZ0Y2hhcnRieW1vZHVsZScpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJHcm91cGVkXCIsIHN0YXRlcyk7XHJcbiAgICAgICAgLypcclxuXHRcdHZhciByZXMgPSBkYXRhLnJlc3BvbnNlLnVzZXJDb25mbGljdHM7XHJcblx0XHRyZXMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuXHRcdFx0Y29uc3QgdGVtcEFycmF5OiBhbnkgPSB7fTtcclxuXHRcdFx0dGVtcEFycmF5Lm5hbWUgPSBlbGVtZW50LnVzZXJuYW1lO1xyXG5cdFx0XHR0ZW1wQXJyYXkudmFsdWUgPSBlbGVtZW50LnRvdGFsO1xyXG5cclxuXHRcdFx0dGhpcy5lYnNfbV9jaGFydC5wdXNoKHRlbXBBcnJheSk7XHJcblx0XHR9KTtcclxuXHRcdCovXHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgd2Vla2J5dXNlckNoYXJ0KCkge1xyXG4gICAgdGhpcy53ZWVrX2J5X3VzZXJfY2hhcnQgPSBbXTtcclxuICAgIGxldCBxdWVyeVBhcmFtcyA9IHtcclxuICAgICAgbW9kdWxlczogdGhpcy5zZWxlY3RlZF9tb2R1bGVzLFxyXG4gICAgICBjb25maWdUcmFja2VySUQ6IHRoaXMuY29uZmlnVHJhY2tlcklEXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2VcclxuICAgICAgLmNvbmZUcmFja0J5VXNlckJ5V2Vla0NoYXJ0KHF1ZXJ5UGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhcInJlc3BvbnNlXCIsZGF0YS5yZXNwb25zZSk7XHJcbiAgICAgICAgdmFyIHJlc3VsdCA9IF8uY2hhaW4oZGF0YS5ib2R5LnJlc3BvbnNlKVxyXG4gICAgICAgICAgLmdyb3VwQnkoJ19pZC51c2VyJylcclxuICAgICAgICAgIC50b1BhaXJzKClcclxuICAgICAgICAgIC5tYXAoKG9ianMsIGtleSkgPT4ge1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKG9ianMpO1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgIHVzZXJzOiBvYmpzWzBdLFxyXG4gICAgICAgICAgICAgIGNvdW50OiBfLnN1bUJ5KG9ianNbMV0sICdjb3VudCcpXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLnZhbHVlKCk7XHJcbiAgICAgICAgLy9jb25zb2xlLmxvZyhyZXN1bHQpO1xyXG4gICAgICAgIHJlc3VsdC5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB0ZW1wQXJyYXk6IGFueSA9IHt9O1xyXG4gICAgICAgICAgdGVtcEFycmF5Lm5hbWUgPSBlbGVtZW50LnVzZXJzO1xyXG4gICAgICAgICAgdGVtcEFycmF5LnZhbHVlID0gZWxlbWVudC5jb3VudDtcclxuICAgICAgICAgIHRoaXMud2Vla19ieV91c2VyX2NoYXJ0LnB1c2godGVtcEFycmF5KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBtb250aGJ5dXNlckNoYXJ0KCkge1xyXG4gICAgdGhpcy5tb250aF9ieV91c2VyX2NoYXJ0ID0gW107XHJcbiAgICAvKlxyXG5cdHRoaXMubW9udGhfYnlfdXNlcl9jaGFydCA9IFtcclxuXHRcdHtcclxuXHRcdCAgXCJuYW1lXCI6IFwiU0lTTUFJTFwiLFxyXG5cdFx0ICBcInZhbHVlXCI6IDc1XHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRcIm5hbWVcIjogXCJPcGVyYXRpb25cIixcclxuXHRcdFx0XCJ2YWx1ZVwiOiAyNVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0XCJuYW1lXCI6IFwiWFlaXCIsXHJcblx0XHRcdFwidmFsdWVcIjogMTVcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdFwibmFtZVwiOiBcIk1OT1wiLFxyXG5cdFx0XHRcInZhbHVlXCI6IDQ1XHJcblx0XHR9XHJcblx0ICBdO1xyXG5cdCovXHJcbiAgICBsZXQgcXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIG1vZHVsZXM6IHRoaXMuc2VsZWN0ZWRfbW9kdWxlcyxcclxuICAgICAgY29uZmlnVHJhY2tlcklEOiB0aGlzLmNvbmZpZ1RyYWNrZXJJRFxyXG4gICAgfTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlXHJcbiAgICAgIC5jb25mVHJhY2tCeVVzZXJCeU1vbnRoQ2hhcnQocXVlcnlQYXJhbXMpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICB2YXIgcmVzdWx0ID0gXy5jaGFpbihkYXRhLmJvZHkucmVzcG9uc2UpXHJcbiAgICAgICAgICAuZ3JvdXBCeSgnX2lkLnVzZXInKVxyXG4gICAgICAgICAgLnRvUGFpcnMoKVxyXG4gICAgICAgICAgLm1hcCgob2Jqcywga2V5KSA9PiB7XHJcbiAgICAgICAgICAgIC8vY29uc29sZS5sb2cob2Jqcyk7XHJcbiAgICAgICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgICAgdXNlcnM6IG9ianNbMF0sXHJcbiAgICAgICAgICAgICAgY291bnQ6IF8uc3VtQnkob2Jqc1sxXSwgJ2NvdW50JylcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAudmFsdWUoKTtcclxuICAgICAgICB2YXIgc3VtT2ZDb3VudCA9IDA7XHJcbiAgICAgICAgcmVzdWx0LmZvckVhY2goKGVsZW1lbnQpID0+IHtcclxuICAgICAgICAgIHN1bU9mQ291bnQgPSBzdW1PZkNvdW50ICsgZWxlbWVudC5jb3VudDtcclxuICAgICAgICB9KTtcclxuICAgICAgICB2YXIgdG90YWx1c2VyY291bnQgPSBkYXRhLmJvZHkucmVzcG9uc2UubGVuZ3RoO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJzdW1cIiwgc3VtT2ZDb3VudCk7XHJcbiAgICAgICAgcmVzdWx0LmZvckVhY2goKGVsZW1lbnQpID0+IHtcclxuICAgICAgICAgIGNvbnN0IHRlbXBBcnJheTogYW55ID0ge307XHJcblxyXG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcImNvdW50XCIsZWxlbWVudC5jb3VudCk7XHJcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKFwic3VtXCIsIHN1bU9mQ291bnQpO1xyXG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcInRvdGFsXCIsdG90YWx1c2VyY291bnQpO1xyXG4gICAgICAgICAgLy8gdGVtcEFycmF5LnZhbHVlID0gKChlbGVtZW50LmNvdW50L3N1bU9mQ291bnQpKnRvdGFsdXNlcmNvdW50KSA7XHJcbiAgICAgICAgICB0ZW1wQXJyYXkucGVyY2VudGFnZSA9XHJcbiAgICAgICAgICAgIHBhcnNlRmxvYXQoKChlbGVtZW50LmNvdW50ICogMTAwKSAvIHN1bU9mQ291bnQpLnRvRml4ZWQoMikpICsgJyUnO1xyXG4gICAgICAgICAgdGVtcEFycmF5Lm5hbWUgPSBlbGVtZW50LnVzZXJzICsgJygnICsgdGVtcEFycmF5LnBlcmNlbnRhZ2UgKyAnKSc7XHJcbiAgICAgICAgICB0ZW1wQXJyYXkudmFsdWUgPSBlbGVtZW50LmNvdW50O1xyXG4gICAgICAgICAgdGhpcy5tb250aF9ieV91c2VyX2NoYXJ0LnB1c2godGVtcEFycmF5KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBmb3JtYXRQZXJjZW50KGMpOiBzdHJpbmcge1xyXG4gICAgY29uc29sZS5sb2coYyk7XHJcbiAgICByZXR1cm4gYy52YWx1ZSArICclJztcclxuICB9XHJcbiAgYXhpc0Zvcm1hdCh2YWwpIHtcclxuICAgIGlmICh2YWwgJSAxID09PSAwKSB7XHJcbiAgICAgIHJldHVybiB2YWwudG9Mb2NhbGVTdHJpbmcoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uU2VsZWN0KGVtaXR0ZWQsIHR5cGVDaGFydCwgY2hhcnROdW0pIHtcclxuICAgIGNvbnNvbGUubG9nKGVtaXR0ZWQpO1xyXG4gICAgLy90aGlzLm1uYW1lLnZhbHVldmFsdWUgPSB0aGlzLm1vZHVsZW5hbWU7XHJcbiAgICB0aGlzLnVzZXJTZWxlY3RlZFRvZ2dsZSA9ICdyZXBvcnRNZ210VG9nZ2xlJztcclxuICAgIHRoaXMuVG9nZ2xlKCdyZXBvcnRfbWFuYWdlbWVudCcsIGVtaXR0ZWQsIHR5cGVDaGFydCwgY2hhcnROdW0pO1xyXG5cclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmNvbmZpZ1RyYWNrZXJEZXRhaWxzID0gZW1pdHRlZDtcclxuICB9XHJcblxyXG4gIGNsb3NlKCkge1xyXG4gICAgdGhpcy50eXBlcyA9IFtdO1xyXG4gICAgdGhpcy5zaG93ZWRpdCgpO1xyXG4gICAgdGhpcy5nZXRhbGxjb25maWdzKCk7XHJcbiAgICB0aGlzLm1vZGUgPSAndmlldyc7XHJcbiAgfVxyXG59XHJcbiJdfQ==