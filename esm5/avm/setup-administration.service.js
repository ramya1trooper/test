import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { createRequestOption } from '../shared/model/request-util';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var SetupAdministrationService = /** @class */ (function () {
    function SetupAdministrationService(http, environment) {
        this.http = http;
        this.environment = environment;
        this.baseURL = this.environment.baseUrl;
    }
    // baseURL: string = environment.baseUrl;
    SetupAdministrationService.prototype.getDataSource = function (req) {
        var params = createRequestOption(req);
        return this.http.get(this.baseURL + "/datasources", {
            params: params,
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getDataSourceEdit = function (id) {
        return this.http.get(this.baseURL + "/datasources/" + id, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.postImportCSV = function (req) {
        console.log(req);
        return this.http.post(this.baseURL + "/datasources", req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.gets3Policy = function () {
        return this.http.get(this.baseURL + "/s3Policy?mimeType=zip&type=datasource", {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.putImportCSV = function (req, id) {
        return this.http.put(this.baseURL + "/datasources/" + id, req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.webService = function (req) {
        return this.http.post(this.baseURL + "/datasources", req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getNotification = function () {
        return this.http.get(this.baseURL + "/auth/notification", {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.postNotification = function (req) {
        return this.http.post(this.baseURL + "/auth/notification", req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getBusinessProcess = function (req) {
        var params = createRequestOption(req);
        return this.http.get(this.baseURL + "/business-process", {
            params: params,
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getApplication = function () {
        return this.http.get(this.baseURL + "/applications/all", {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getRolePermission = function (req) {
        var params = createRequestOption(req);
        return this.http.get(this.baseURL + "/roles-permission", {
            params: params,
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getRolePermissionEdit = function (id) {
        return this.http.get(this.baseURL + "/roles-permission/" + id, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getRoleAccess = function (req) {
        var params = createRequestOption(req);
        return this.http.get(this.baseURL + "/roles-permission/permissions", {
            params: params,
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getRoleStatus = function (id, status) {
        return this.http.put(this.baseURL + "/roles/" + id + "/status", status, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.createRole = function (req) {
        return this.http.post(this.baseURL + "/roles-permission", req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.updateRole = function (req, id) {
        return this.http.put(this.baseURL + "/roles-permission/" + id, req, { observe: 'response' })
            .pipe(map(function (resp) {
            return resp;
        }));
    };
    SetupAdministrationService.prototype.getSAUsers = function (req) {
        var params = createRequestOption(req);
        return this.http.get(this.baseURL + "/sa-users", {
            params: params,
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getSAUsersStatus = function (id, status) {
        return this.http.put(this.baseURL + "/sa-users/" + id + "/status", status, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getSAUsersEdit = function (id) {
        return this.http.get(this.baseURL + "/sa-users/" + id, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.createUser = function (req) {
        return this.http.post(this.baseURL + "/sa-users", req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.editUser = function (req, id) {
        return this.http.put(this.baseURL + "/sa-users/" + id, req, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getRealm = function () {
        return this.http.get(this.baseURL + "/realm/all", {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getProductBased = function () {
        return this.http.get(this.baseURL + "/roles-permission/product-based", {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getCustomerInfo = function (id) {
        return this.http.get(this.baseURL + "/customers/" + id, {
            observe: 'response'
        });
    };
    SetupAdministrationService.prototype.getBusinessProcessFormById = function (data) {
        return this.http.get(this.baseURL + "/business-process/" + data);
    };
    SetupAdministrationService.prototype.createBusinessProcess = function (data) {
        return this.http.post(this.baseURL + "/business-process/", data, { observe: 'response' })
            .pipe(map(function (resp) {
            return resp;
        }));
    };
    SetupAdministrationService.prototype.updateBusinessProcess = function (data, id) {
        return this.http.put(this.baseURL + "/business-process/" + id, data, { observe: 'response' })
            .pipe(map(function (resp) {
            return resp;
        }));
    };
    SetupAdministrationService.prototype.avmImport = function (data, file) {
        var formData = new FormData();
        Object.keys(data).map(function (key) {
            formData.append(key, data[key]);
        });
        formData.append('file', file);
        console.log('file >>', file);
        return this.http.post(this.baseURL + "/avm-fusion/import", formData, { observe: 'response' })
            .pipe(map(function (resp) {
            return resp;
        }));
    };
    SetupAdministrationService.prototype.checkBusinessName = function (param) {
        return this.http.get(this.baseURL + "/business-process/check-name", { params: param });
    };
    SetupAdministrationService.prototype.scheduleService = function (query) {
        var url = this.baseURL + "/schedules?";
        return this.http.get(url, { params: query });
    };
    //create config tracker
    SetupAdministrationService.prototype.createConfigTracker = function (config) {
        return this.http.post(this.baseURL + "/config-tracker/createconfig", config, { observe: 'response' });
    };
    SetupAdministrationService.prototype.updateConfigDetails = function (data, id) {
        return this.http.put(this.baseURL + "/config-tracker/" + id, data, { observe: 'response' })
            .pipe(map(function (resp) {
            return resp;
        }));
    };
    SetupAdministrationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SetupAdministrationService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
    ]; };
    SetupAdministrationService.ngInjectableDef = i0.defineInjectable({ factory: function SetupAdministrationService_Factory() { return new SetupAdministrationService(i0.inject(i1.HttpClient), i0.inject("environment")); }, token: SetupAdministrationService, providedIn: "root" });
    return SetupAdministrationService;
}());
export { SetupAdministrationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dXAtYWRtaW5pc3RyYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJhdm0vc2V0dXAtYWRtaW5pc3RyYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUE0QixNQUFNLHNCQUFzQixDQUFDO0FBRTVFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ25FLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBRXJDO0lBTUMsb0NBQ1MsSUFBZ0IsRUFDTyxXQUFXO1FBRGxDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDTyxnQkFBVyxHQUFYLFdBQVcsQ0FBQTtRQUcxQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO0lBQ3pDLENBQUM7SUFFRCx5Q0FBeUM7SUFFekMsa0RBQWEsR0FBYixVQUFjLEdBQVM7UUFDdEIsSUFBTSxNQUFNLEdBQWUsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxpQkFBYyxFQUFFO1lBQ3hELE1BQU0sUUFBQTtZQUNOLE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxzREFBaUIsR0FBakIsVUFBa0IsRUFBTztRQUN4QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFTLElBQUksQ0FBQyxPQUFPLGtCQUFlLEdBQUcsRUFBRSxFQUFFO1lBQzlELE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxrREFBYSxHQUFiLFVBQWMsR0FBUztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVMsSUFBSSxDQUFDLE9BQU8saUJBQWMsRUFBRSxHQUFHLEVBQUU7WUFDOUQsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUNELGdEQUFXLEdBQVg7UUFDQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFTLElBQUksQ0FBQyxPQUFPLDJDQUF3QyxFQUFFO1lBQ2xGLE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxpREFBWSxHQUFaLFVBQWEsR0FBUSxFQUFFLEVBQU87UUFDN0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxrQkFBZSxHQUFHLEVBQUUsRUFBRSxHQUFHLEVBQUU7WUFDbkUsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELCtDQUFVLEdBQVYsVUFBVyxHQUFRO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVMsSUFBSSxDQUFDLE9BQU8saUJBQWMsRUFBRSxHQUFHLEVBQUU7WUFDOUQsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELG9EQUFlLEdBQWY7UUFDQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFTLElBQUksQ0FBQyxPQUFPLHVCQUFvQixFQUFFO1lBQzlELE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxxREFBZ0IsR0FBaEIsVUFBaUIsR0FBUTtRQUN4QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFTLElBQUksQ0FBQyxPQUFPLHVCQUFvQixFQUFFLEdBQUcsRUFBRTtZQUNwRSxPQUFPLEVBQUUsVUFBVTtTQUNuQixDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsdURBQWtCLEdBQWxCLFVBQW1CLEdBQVM7UUFDM0IsSUFBTSxNQUFNLEdBQWUsbUJBQW1CLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDcEQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxzQkFBbUIsRUFBRTtZQUM3RCxNQUFNLFFBQUE7WUFDTixPQUFPLEVBQUUsVUFBVTtTQUNuQixDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsbURBQWMsR0FBZDtRQUNDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sc0JBQW1CLEVBQUU7WUFDN0QsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELHNEQUFpQixHQUFqQixVQUFrQixHQUFTO1FBQzFCLElBQU0sTUFBTSxHQUFlLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3BELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sc0JBQW1CLEVBQUU7WUFDN0QsTUFBTSxRQUFBO1lBQ04sT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUNELDBEQUFxQixHQUFyQixVQUFzQixFQUFPO1FBQzVCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sdUJBQW9CLEdBQUcsRUFBRSxFQUFFO1lBQ25FLE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxrREFBYSxHQUFiLFVBQWMsR0FBUztRQUN0QixJQUFNLE1BQU0sR0FBZSxtQkFBbUIsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNwRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFTLElBQUksQ0FBQyxPQUFPLGtDQUErQixFQUFFO1lBQ3pFLE1BQU0sUUFBQTtZQUNOLE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxrREFBYSxHQUFiLFVBQWMsRUFBTyxFQUFFLE1BQVc7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxZQUFTLEdBQUcsRUFBRSxHQUFHLFNBQVMsRUFBRSxNQUFNLEVBQUU7WUFDNUUsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUNELCtDQUFVLEdBQVYsVUFBVyxHQUFRO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVMsSUFBSSxDQUFDLE9BQU8sc0JBQW1CLEVBQUUsR0FBRyxFQUFFO1lBQ25FLE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCwrQ0FBVSxHQUFWLFVBQVcsR0FBUSxFQUFFLEVBQUU7UUFDdEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyx1QkFBb0IsR0FBRyxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDO2FBQy9GLElBQUksQ0FDSixHQUFHLENBQUMsVUFBQSxJQUFJO1lBQ1AsT0FBTyxJQUFJLENBQUM7UUFDYixDQUFDLENBQUMsQ0FDRixDQUFBO0lBQ0gsQ0FBQztJQUVELCtDQUFVLEdBQVYsVUFBVyxHQUFTO1FBQ25CLElBQU0sTUFBTSxHQUFlLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3BELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sY0FBVyxFQUFFO1lBQ3JELE1BQU0sUUFBQTtZQUNOLE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFFRCxxREFBZ0IsR0FBaEIsVUFBaUIsRUFBTyxFQUFFLE1BQVc7UUFDcEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxlQUFZLEdBQUcsRUFBRSxHQUFHLFNBQVMsRUFBRSxNQUFNLEVBQUU7WUFDL0UsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUNELG1EQUFjLEdBQWQsVUFBZSxFQUFPO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sZUFBWSxHQUFHLEVBQUUsRUFBRTtZQUMzRCxPQUFPLEVBQUUsVUFBVTtTQUNuQixDQUFDLENBQUM7SUFDSixDQUFDO0lBQ0QsK0NBQVUsR0FBVixVQUFXLEdBQVE7UUFDbEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBUyxJQUFJLENBQUMsT0FBTyxjQUFXLEVBQUUsR0FBRyxFQUFFO1lBQzNELE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCw2Q0FBUSxHQUFSLFVBQVMsR0FBUSxFQUFFLEVBQU87UUFFekIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxlQUFZLEdBQUcsRUFBRSxFQUFFLEdBQUcsRUFBRTtZQUNoRSxPQUFPLEVBQUUsVUFBVTtTQUNuQixDQUFDLENBQUM7SUFDSixDQUFDO0lBQ0QsNkNBQVEsR0FBUjtRQUNDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sZUFBWSxFQUFFO1lBQ3RELE9BQU8sRUFBRSxVQUFVO1NBQ25CLENBQUMsQ0FBQztJQUNKLENBQUM7SUFDRCxvREFBZSxHQUFmO1FBQ0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxvQ0FBaUMsRUFBRTtZQUMzRSxPQUFPLEVBQUUsVUFBVTtTQUNuQixDQUFDLENBQUM7SUFDSixDQUFDO0lBRUQsb0RBQWUsR0FBZixVQUFnQixFQUFPO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sZ0JBQWEsR0FBRyxFQUFFLEVBQUU7WUFDNUQsT0FBTyxFQUFFLFVBQVU7U0FDbkIsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELCtEQUEwQixHQUExQixVQUEyQixJQUFJO1FBQzlCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8sdUJBQW9CLEdBQUcsSUFBSSxDQUFDLENBQUE7SUFDdEUsQ0FBQztJQUVELDBEQUFxQixHQUFyQixVQUFzQixJQUFTO1FBQzlCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVMsSUFBSSxDQUFDLE9BQU8sdUJBQW9CLEVBQUUsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDO2FBQzVGLElBQUksQ0FDSixHQUFHLENBQUMsVUFBQSxJQUFJO1lBQ1AsT0FBTyxJQUFJLENBQUM7UUFDYixDQUFDLENBQUMsQ0FDRixDQUFBO0lBQ0gsQ0FBQztJQUVELDBEQUFxQixHQUFyQixVQUFzQixJQUFTLEVBQUUsRUFBRTtRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFTLElBQUksQ0FBQyxPQUFPLHVCQUFvQixHQUFHLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUM7YUFDaEcsSUFBSSxDQUNKLEdBQUcsQ0FBQyxVQUFBLElBQUk7WUFDUCxPQUFPLElBQUksQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUNGLENBQUE7SUFDSCxDQUFDO0lBRUQsOENBQVMsR0FBVCxVQUFVLElBQVMsRUFBRSxJQUFTO1FBQzdCLElBQUksUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7UUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLENBQUMsVUFBQyxHQUFHO1lBQ3pCLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUMsSUFBSSxDQUFDLENBQUE7UUFDM0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBUyxJQUFJLENBQUMsT0FBTyx1QkFBb0IsRUFBRSxRQUFRLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUM7YUFDaEcsSUFBSSxDQUNKLEdBQUcsQ0FBQyxVQUFBLElBQUk7WUFDUCxPQUFPLElBQUksQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUNGLENBQUE7SUFDSCxDQUFDO0lBRUQsc0RBQWlCLEdBQWpCLFVBQWtCLEtBQVU7UUFDM0IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBUyxJQUFJLENBQUMsT0FBTyxpQ0FBOEIsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQzdGLENBQUM7SUFFRCxvREFBZSxHQUFmLFVBQWdCLEtBQVU7UUFDekIsSUFBSSxHQUFHLEdBQU0sSUFBSSxDQUFDLE9BQU8sZ0JBQWEsQ0FBQztRQUN2QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFNLEdBQUcsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO0lBQ2xELENBQUM7SUFFRCx1QkFBdUI7SUFDdkIsd0RBQW1CLEdBQW5CLFVBQW9CLE1BQVc7UUFDOUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBUyxJQUFJLENBQUMsT0FBTyxpQ0FBOEIsRUFBRSxNQUFNLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztJQUM1RyxDQUFDO0lBR0Qsd0RBQW1CLEdBQW5CLFVBQXFCLElBQVMsRUFBRSxFQUFFO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQVMsSUFBSSxDQUFDLE9BQU8scUJBQWtCLEdBQUcsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQzthQUM5RixJQUFJLENBQ0osR0FBRyxDQUFDLFVBQUEsSUFBSTtZQUNQLE9BQU8sSUFBSSxDQUFDO1FBQ2IsQ0FBQyxDQUFDLENBQ0YsQ0FBQTtJQUNILENBQUM7O2dCQTNORCxVQUFVLFNBQUM7b0JBQ1gsVUFBVSxFQUFFLE1BQU07aUJBQ2xCOzs7O2dCQVBRLFVBQVU7Z0RBYWhCLE1BQU0sU0FBQyxhQUFhOzs7cUNBZHZCO0NBa09DLEFBNU5ELElBNE5DO1NBek5ZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zLCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBjcmVhdGVSZXF1ZXN0T3B0aW9uIH0gZnJvbSAnLi4vc2hhcmVkL21vZGVsL3JlcXVlc3QtdXRpbCc7XHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuXHRwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlIHtcclxuXHRiYXNlVVJMOiBzdHJpbmc7XHJcblxyXG5cdGNvbnN0cnVjdG9yKFxyXG5cdFx0cHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG5cdFx0QEluamVjdChcImVudmlyb25tZW50XCIpIHByaXZhdGUgZW52aXJvbm1lbnRcclxuXHJcblx0KSB7IFxyXG5cdFx0dGhpcy5iYXNlVVJMID0gdGhpcy5lbnZpcm9ubWVudC5iYXNlVXJsO1xyXG5cdH1cclxuXHJcblx0Ly8gYmFzZVVSTDogc3RyaW5nID0gZW52aXJvbm1lbnQuYmFzZVVybDtcclxuXHJcblx0Z2V0RGF0YVNvdXJjZShyZXE/OiBhbnkpOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxhbnlbXT4+IHtcclxuXHRcdGNvbnN0IHBhcmFtczogSHR0cFBhcmFtcyA9IGNyZWF0ZVJlcXVlc3RPcHRpb24ocmVxKTtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9kYXRhc291cmNlc2AsIHtcclxuXHRcdFx0cGFyYW1zLFxyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0Z2V0RGF0YVNvdXJjZUVkaXQoaWQ6IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55PihgJHt0aGlzLmJhc2VVUkx9L2RhdGFzb3VyY2VzL2AgKyBpZCwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0cG9zdEltcG9ydENTVihyZXE/OiBhbnkpOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxhbnlbXT4+IHtcclxuXHRcdGNvbnNvbGUubG9nKHJlcSk7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55PihgJHt0aGlzLmJhc2VVUkx9L2RhdGFzb3VyY2VzYCwgcmVxLCB7XHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRnZXRzM1BvbGljeSgpOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxhbnlbXT4+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9zM1BvbGljeT9taW1lVHlwZT16aXAmdHlwZT1kYXRhc291cmNlYCwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0cHV0SW1wb3J0Q1NWKHJlcTogYW55LCBpZDogYW55KTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLnB1dDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vZGF0YXNvdXJjZXMvYCArIGlkLCByZXEsIHtcclxuXHRcdFx0b2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHR3ZWJTZXJ2aWNlKHJlcTogYW55KTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55PihgJHt0aGlzLmJhc2VVUkx9L2RhdGFzb3VyY2VzYCwgcmVxLCB7XHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Z2V0Tm90aWZpY2F0aW9uKCk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55PihgJHt0aGlzLmJhc2VVUkx9L2F1dGgvbm90aWZpY2F0aW9uYCwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0cG9zdE5vdGlmaWNhdGlvbihyZXE6IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9hdXRoL25vdGlmaWNhdGlvbmAsIHJlcSwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGdldEJ1c2luZXNzUHJvY2VzcyhyZXE/OiBhbnkpOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxhbnlbXT4+IHtcclxuXHRcdGNvbnN0IHBhcmFtczogSHR0cFBhcmFtcyA9IGNyZWF0ZVJlcXVlc3RPcHRpb24ocmVxKTtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9idXNpbmVzcy1wcm9jZXNzYCwge1xyXG5cdFx0XHRwYXJhbXMsXHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0Z2V0QXBwbGljYXRpb24oKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vYXBwbGljYXRpb25zL2FsbGAsIHtcclxuXHRcdFx0b2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cdFxyXG5cdGdldFJvbGVQZXJtaXNzaW9uKHJlcT86IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cdFx0Y29uc3QgcGFyYW1zOiBIdHRwUGFyYW1zID0gY3JlYXRlUmVxdWVzdE9wdGlvbihyZXEpO1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55PihgJHt0aGlzLmJhc2VVUkx9L3JvbGVzLXBlcm1pc3Npb25gLCB7XHJcblx0XHRcdHBhcmFtcyxcclxuXHRcdFx0b2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cdGdldFJvbGVQZXJtaXNzaW9uRWRpdChpZDogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9yb2xlcy1wZXJtaXNzaW9uL2AgKyBpZCwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0Z2V0Um9sZUFjY2VzcyhyZXE/OiBhbnkpOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxhbnlbXT4+IHtcclxuXHRcdGNvbnN0IHBhcmFtczogSHR0cFBhcmFtcyA9IGNyZWF0ZVJlcXVlc3RPcHRpb24ocmVxKTtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9yb2xlcy1wZXJtaXNzaW9uL3Blcm1pc3Npb25zYCwge1xyXG5cdFx0XHRwYXJhbXMsXHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRnZXRSb2xlU3RhdHVzKGlkOiBhbnksIHN0YXR1czogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAucHV0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9yb2xlcy9gICsgaWQgKyBgL3N0YXR1c2AsIHN0YXR1cywge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0Y3JlYXRlUm9sZShyZXE6IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9yb2xlcy1wZXJtaXNzaW9uYCwgcmVxLCB7XHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHR1cGRhdGVSb2xlKHJlcTogYW55LCBpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLnB1dDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vcm9sZXMtcGVybWlzc2lvbi9gICsgaWQsIHJlcSwgeyBvYnNlcnZlOiAncmVzcG9uc2UnIH0pXHJcblx0XHRcdC5waXBlKFxyXG5cdFx0XHRcdG1hcChyZXNwID0+IHtcclxuXHRcdFx0XHRcdHJldHVybiByZXNwO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdClcclxuXHR9XHJcblxyXG5cdGdldFNBVXNlcnMocmVxPzogYW55KTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcblx0XHRjb25zdCBwYXJhbXM6IEh0dHBQYXJhbXMgPSBjcmVhdGVSZXF1ZXN0T3B0aW9uKHJlcSk7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vc2EtdXNlcnNgLCB7XHJcblx0XHRcdHBhcmFtcyxcclxuXHRcdFx0b2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRnZXRTQVVzZXJzU3RhdHVzKGlkOiBhbnksIHN0YXR1czogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAucHV0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9zYS11c2Vycy9gICsgaWQgKyBgL3N0YXR1c2AsIHN0YXR1cywge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0Z2V0U0FVc2Vyc0VkaXQoaWQ6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vc2EtdXNlcnMvYCArIGlkLCB7XHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRjcmVhdGVVc2VyKHJlcTogYW55KTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55PihgJHt0aGlzLmJhc2VVUkx9L3NhLXVzZXJzYCwgcmVxLCB7XHJcblx0XHRcdG9ic2VydmU6ICdyZXNwb25zZSdcclxuXHRcdH0pO1xyXG5cdH1cclxuXHRlZGl0VXNlcihyZXE6IGFueSwgaWQ6IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cclxuXHRcdHJldHVybiB0aGlzLmh0dHAucHV0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9zYS11c2Vycy9gICsgaWQsIHJlcSwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0Z2V0UmVhbG0oKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vcmVhbG0vYWxsYCwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblx0Z2V0UHJvZHVjdEJhc2VkKCk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55PihgJHt0aGlzLmJhc2VVUkx9L3JvbGVzLXBlcm1pc3Npb24vcHJvZHVjdC1iYXNlZGAsIHtcclxuXHRcdFx0b2JzZXJ2ZTogJ3Jlc3BvbnNlJ1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRnZXRDdXN0b21lckluZm8oaWQ6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vY3VzdG9tZXJzL2AgKyBpZCwge1xyXG5cdFx0XHRvYnNlcnZlOiAncmVzcG9uc2UnXHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdGdldEJ1c2luZXNzUHJvY2Vzc0Zvcm1CeUlkKGRhdGEpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5nZXQ8YW55PihgJHt0aGlzLmJhc2VVUkx9L2J1c2luZXNzLXByb2Nlc3MvYCArIGRhdGEpXHJcblx0fVxyXG5cclxuXHRjcmVhdGVCdXNpbmVzc1Byb2Nlc3MoZGF0YTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAucG9zdDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vYnVzaW5lc3MtcHJvY2Vzcy9gLCBkYXRhLCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSlcclxuXHRcdFx0LnBpcGUoXHJcblx0XHRcdFx0bWFwKHJlc3AgPT4ge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHJlc3A7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0KVxyXG5cdH1cclxuXHJcblx0dXBkYXRlQnVzaW5lc3NQcm9jZXNzKGRhdGE6IGFueSwgaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wdXQ8YW55PihgJHt0aGlzLmJhc2VVUkx9L2J1c2luZXNzLXByb2Nlc3MvYCArIGlkLCBkYXRhLCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSlcclxuXHRcdFx0LnBpcGUoXHJcblx0XHRcdFx0bWFwKHJlc3AgPT4ge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHJlc3A7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0KVxyXG5cdH1cclxuXHJcblx0YXZtSW1wb3J0KGRhdGE6IGFueSwgZmlsZTogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuXHRcdHZhciBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG5cdFx0T2JqZWN0LmtleXMoZGF0YSkubWFwKChrZXkpID0+IHtcclxuXHRcdFx0Zm9ybURhdGEuYXBwZW5kKGtleSwgZGF0YVtrZXldKTtcclxuXHRcdH0pO1xyXG5cdFx0Zm9ybURhdGEuYXBwZW5kKCdmaWxlJywgZmlsZSk7XHJcblx0XHRjb25zb2xlLmxvZygnZmlsZSA+PicsZmlsZSlcclxuXHRcdHJldHVybiB0aGlzLmh0dHAucG9zdDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vYXZtLWZ1c2lvbi9pbXBvcnRgLCBmb3JtRGF0YSwgeyBvYnNlcnZlOiAncmVzcG9uc2UnIH0pXHJcblx0XHRcdC5waXBlKFxyXG5cdFx0XHRcdG1hcChyZXNwID0+IHtcclxuXHRcdFx0XHRcdHJldHVybiByZXNwO1xyXG5cdFx0XHRcdH0pXHJcblx0XHRcdClcclxuXHR9XHJcblx0XHJcblx0Y2hlY2tCdXNpbmVzc05hbWUocGFyYW06IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vYnVzaW5lc3MtcHJvY2Vzcy9jaGVjay1uYW1lYCwgeyBwYXJhbXM6IHBhcmFtIH0pO1xyXG5cdH1cclxuXHJcblx0c2NoZWR1bGVTZXJ2aWNlKHF1ZXJ5OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG5cdFx0bGV0IHVybCA9IGAke3RoaXMuYmFzZVVSTH0vc2NoZWR1bGVzP2A7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLmdldDxhbnk+KHVybCwgeyBwYXJhbXM6IHF1ZXJ5IH0pXHJcblx0fVxyXG5cclxuXHQvL2NyZWF0ZSBjb25maWcgdHJhY2tlclxyXG5cdGNyZWF0ZUNvbmZpZ1RyYWNrZXIoY29uZmlnOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG5cdFx0cmV0dXJuIHRoaXMuaHR0cC5wb3N0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9jb25maWctdHJhY2tlci9jcmVhdGVjb25maWdgLCBjb25maWcsIHsgb2JzZXJ2ZTogJ3Jlc3BvbnNlJyB9KTtcclxuXHR9XHJcblxyXG5cclxuXHR1cGRhdGVDb25maWdEZXRhaWxzXHQoZGF0YTogYW55LCBpZCk6IE9ic2VydmFibGU8YW55PiB7XHJcblx0XHRyZXR1cm4gdGhpcy5odHRwLnB1dDxhbnk+KGAke3RoaXMuYmFzZVVSTH0vY29uZmlnLXRyYWNrZXIvYCArIGlkLCBkYXRhLCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSlcclxuXHRcdFx0LnBpcGUoXHJcblx0XHRcdFx0bWFwKHJlc3AgPT4ge1xyXG5cdFx0XHRcdFx0cmV0dXJuIHJlc3A7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0KVxyXG5cdH1cclxufVxyXG4iXX0=