import * as tslib_1 from "tslib";
import { Component, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MatTableDataSource } from '@angular/material';
import { fuseAnimations } from '../../@fuse/animations';
import { ReportManagementService } from './../report-management.service';
import { SnackBarService } from './../../shared/snackbar.service';
var AvmImportFormComponent = /** @class */ (function () {
    function AvmImportFormComponent(matDialogRef, formBuilder, reportManagementService, cd, snackBarService) {
        this.matDialogRef = matDialogRef;
        this.formBuilder = formBuilder;
        this.reportManagementService = reportManagementService;
        this.cd = cd;
        this.snackBarService = snackBarService;
        this.submitted = false;
        this.afterClose = new EventEmitter();
        this.panelOpenState = false;
        this.errorLogsHeader = ['type', 'desc'];
        this.dialogTitle = 'Import Access Entitlements';
        this.control = new EntitlementImport({});
    }
    AvmImportFormComponent.prototype.ngOnInit = function () {
        this.getDatasource();
        this.btnSubmit = false;
        this.importAccessEntitlementForm = this.formBuilder.group({
            data_source: [this.control.data_source],
            file: [this.control.file],
        });
    };
    AvmImportFormComponent.prototype.onSubmit = function () {
        var _this = this;
        this.submitted = true;
        if (this.importAccessEntitlementForm.invalid) {
            this.snackBarService.add("Fields Required");
        }
        else if (!this.selectedFileName) {
            this.snackBarService.warning("File Required");
        }
        else {
            this.btnSubmit = true;
            var obj = {
                "data": this.selectedDatasourceId,
                'dsname': this.selectedDatasourceName
            };
            this.reportManagementService.avmImport(obj, this.fileUpload).subscribe(function (res) {
                if (res.body.meta.status == 201) {
                    _this.snackBarService.add(res.body.meta.msg);
                    _this.logsArr = res.body.response.logsArr;
                    _this.logsData = new MatTableDataSource(_this.logsArr);
                    _this.totalCount = res.body.response.total;
                    _this.saveCount = res.body.response.saveCount;
                    _this.failedCount = res.body.response.failedCount;
                    _this.duplicateCount = res.body.response.duplicateCount;
                }
                else {
                    _this.snackBarService.add(res.body.meta.msg);
                    _this.matDialogRef.close();
                    _this.afterClose.emit(null);
                }
            }, function (error) {
                if (error.error.meta.status == 406) {
                    _this.btnSubmit = false;
                    _this.snackBarService.warning(error.error.meta.msg);
                    _this.logsArr = error.error.response.logsArr;
                    _this.logsData = new MatTableDataSource(_this.logsArr);
                    _this.totalCount = error.error.response.total;
                    _this.saveCount = error.error.response.saveCount;
                    _this.failedCount = error.error.response.failedCount;
                    _this.duplicateCount = error.error.response.duplicateCount;
                }
                if (error.error.meta.status == 500) {
                    _this.btnSubmit = false;
                    _this.snackBarService.warning(error.error.meta.msg);
                    _this.logsArr = error.error.response.logsArr;
                    _this.logsData = new MatTableDataSource(_this.logsArr);
                    _this.totalCount = error.error.response.total;
                    _this.saveCount = error.error.response.saveCount;
                    _this.failedCount = error.error.response.failedCount;
                    _this.duplicateCount = error.error.response.duplicateCount;
                }
            });
        }
    };
    AvmImportFormComponent.prototype.getDatasource = function () {
        var _this = this;
        this.reportManagementService.getAllDatasources().subscribe(function (res) {
            _this.dataSource = res.response.datasources;
            _this.defaultDatasourceName = res.response.datasources[0].name;
            _this.defaultDatasource = res.response.datasources[0]._id;
            if (_this.defaultDatasource && _this.defaultDatasourceName) {
                _this.importAccessEntitlementForm.controls['data_source'].setValue(_this.defaultDatasourceName);
                _this.onChangeDatasourceId(_this.defaultDatasource, _this.defaultDatasourceName);
            }
        });
    };
    AvmImportFormComponent.prototype.onChangeDatasourceId = function (dsID, dsIname) {
        this.selectedDatasourceId = dsID;
        this.selectedDatasourceName = dsIname;
    };
    AvmImportFormComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            var _a = tslib_1.__read(event.target.files, 1), file_1 = _a[0];
            reader.readAsDataURL(file_1);
            reader.onload = function () {
                _this.importAccessEntitlementForm.patchValue({
                    file: reader.result
                });
                _this.selectedFileName = file_1.name;
                _this.fileUpload = file_1;
                _this.cd.markForCheck();
            };
        }
    };
    AvmImportFormComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-control-form',
                    template: "<scrumboard-board-card-dialog>\r\n\t<div class=\"dialog-content-wrapper\" fusePerfectScrollbar>\r\n\r\n\t\t<div class=\"header-top accent ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"ml-4 font-bold\">{{dialogTitle}}</h2>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<button mat-icon-button (click)=\"matDialogRef.close()\" aria-label=\"Close Dialog\" style=\"float:right\">\r\n\t\t\t\t\t<mat-icon>close</mat-icon>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t\t<div layout=\"row\" *ngIf=\"logsArr?.length > 0\" class=\"p-12\" md-content layout-padding>\r\n\t\t\t<div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\">\r\n\t\t\t\t<mat-accordion>\r\n\t\t\t\t\t<mat-expansion-panel (opened)=\"panelOpenState = true\" (closed)=\"panelOpenState = false\">\r\n\t\t\t\t\t\t<mat-expansion-panel-header>\r\n\t\t\t\t\t\t\t<mat-panel-title>\r\n\t\t\t\t\t\t\t\tTotal:{{totalCount}} SaveCount:{{saveCount}} FailedCount:{{failedCount}} DuplicateCount:{{duplicateCount}}\r\n\t\t\t\t\t\t\t</mat-panel-title>\r\n\t\t\t\t\t\t</mat-expansion-panel-header>\r\n\t\t\t\t\t\t<div style=\"max-height: 100px;overflow: scroll;\">\r\n\t\t\t\t\t\t\t<table mat-table [dataSource]=\"logsData\" fusePerfectScrollbar>\r\n\r\n\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"type\">\r\n\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef> Type </th>\r\n\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let row\">\r\n\t\t\t\t\t\t\t\t\t\t<p *ngIf=\"row.type == 'danger'\" class=\"label bg-failed\"> High </p>\r\n\t\t\t\t\t\t\t\t\t\t<p *ngIf=\"row.type == 'warning'\" class=\"label bg-running\"> {{row.type}} </p>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"desc\">\r\n\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef> Description </th>\r\n\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let row\"> {{row.desc}} </td>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"errorLogsHeader; sticky: true\"></tr>\r\n\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: errorLogsHeader;\">\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</table>\r\n\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-expansion-panel>\r\n\t\t\t\t</mat-accordion>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t\t<div mat-dialog-content class=\"p-24 pb-0 m-0\">\r\n\t\t\t<form [formGroup]=\"importAccessEntitlementForm\">\r\n\t\t\t\t<div layout=\"row\" md-content layout-padding>\r\n\t\t\t\t\t<div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n\t\t\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"100\">\r\n\t\t\t\t\t\t\t<mat-label>Select Data Source</mat-label>\r\n\t\t\t\t\t\t\t<mat-select formControlName=\"data_source\" required placeholder=\"'Select Data Source'\">\r\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let ds of dataSource\" value=\"{{ds.name}}\" (click)=\"onChangeDatasourceId(ds._id,ds.name)\">\r\n\t\t\t\t\t\t\t\t\t{{ds.name}}\r\n\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div layout=\"row\" md-content layout-padding class=\"import_border\">\r\n\t\t\t\t\t<div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n\t\t\t\t\t\t<div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" style=\"text-align: center;\" class=\"mr-8\">\r\n\t\t\t\t\t\t\t<div class=\"board-list-item add-new-board ng-trigger ng-trigger-animate md-headline\" fxlayout=\"column\" layout-align=\"center center\">\r\n\t\t\t\t\t\t\t\t<mat-icon class=\"s-56 mat-icon material-icons\" role=\"img\" aria-hidden=\"true\">photo</mat-icon>\r\n\t\t\t\t\t\t\t\t<div class=\"board-name\">Drag and Drop</div>\r\n\t\t\t\t\t\t\t\t<div class=\"board-name\" style=\"font-size:normal\">or select an option below</div>\r\n\t\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<button mat-button color=\"accent\" class=\"common-btn\" (click)=\"fileInput.click()\" style=\"border:2px solid\">Browse</button>\r\n\t\t\t\t\t\t\t\t\t<input hidden type=\"file\" required #fileInput (change)=\"onFileChange($event)\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t\t\t<div *ngIf=\"selectedFileName\">\r\n\t\t\t\t\t\t\t\t<P>{{selectedFileName}}</P>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<div layout=\"row\" md-content layout-padding>\r\n\t\t\t\t\t<div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\">\r\n\t\t\t\t\t\t<p style=\"color:#2979ff;text-decoration: underline;\">\r\n\t\t\t\t\t\t\t<a target=\"_self\" href=\"assets/template/AccessEntitlement-Import-Template.csv\" download=\"AccessGroup-Import-Template.csv\">\r\n\t\t\t\t\t\t\t\t<span>\r\n\t\t\t\t\t\t\t\t\tSample Download Format\r\n\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t</a>\r\n\t\t\t\t\t\t</p>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</form>\r\n\t\t\t<div layout=\"row\" md-content layout-padding>\r\n\t\t\t\t<div *ngIf=\"btnSubmit\">\r\n\t\t\t\t\t<mat-progress-bar class=\" mat-progress-bar mat-accent\" mode=\"query\" value=\"40\"></mat-progress-bar>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"m-0\"></h2>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<button mat-raised-button class=\"mr-4\" color=\"warn\" [disabled]=\"btnSubmit\" class=\"common-btn w-100\" (click)=\"onSubmit()\"\r\n\t\t\t\t style=\"float:right;\">\r\n\t\t\t\t\tImport\r\n\t\t\t\t</button>\r\n\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\" ng-reflect-type=\"button\"\r\n\t\t\t\t\tstyle=\"background-color: grey !important;\r\n\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\"\r\n\t\t\t\t\t(click)=\"matDialogRef.close()\">\r\n\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\" ng-reflect-centered=\"false\"\r\n\t\t\t\t\t\tng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t</div>\r\n</scrumboard-board-card-dialog>",
                    animations: fuseAnimations
                }] }
    ];
    /** @nocollapse */
    AvmImportFormComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: FormBuilder },
        { type: ReportManagementService },
        { type: ChangeDetectorRef },
        { type: SnackBarService }
    ]; };
    AvmImportFormComponent.propDecorators = {
        afterClose: [{ type: Output }]
    };
    return AvmImportFormComponent;
}());
export { AvmImportFormComponent };
var EntitlementImport = /** @class */ (function () {
    function EntitlementImport(control) {
        this.data_source = control.data_source || '';
        this.file = control.file || '';
    }
    return EntitlementImport;
}());
export { EntitlementImport };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1wb3J0LWZvcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImF2bS9pbXBvcnQtZm9ybS9pbXBvcnQtZm9ybS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQWdELGlCQUFpQixFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakksT0FBTyxFQUFFLFdBQVcsRUFBYSxNQUFNLGdCQUFnQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxZQUFZLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNyRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDeEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDekUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFO0lBdUJDLGdDQUNRLFlBQWtELEVBQ2pELFdBQXdCLEVBQ3hCLHVCQUFnRCxFQUNoRCxFQUFxQixFQUNyQixlQUFnQztRQUpqQyxpQkFBWSxHQUFaLFlBQVksQ0FBc0M7UUFDakQsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsNEJBQXVCLEdBQXZCLHVCQUF1QixDQUF5QjtRQUNoRCxPQUFFLEdBQUYsRUFBRSxDQUFtQjtRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFsQnpDLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFDUixlQUFVLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7UUFnQzdELG1CQUFjLEdBQUcsS0FBSyxDQUFDO1FBT3ZCLG9CQUFlLEdBQWEsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUE7UUFwQjNDLElBQUksQ0FBQyxXQUFXLEdBQUcsNEJBQTRCLENBQUM7UUFDaEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzFDLENBQUM7SUFFRCx5Q0FBUSxHQUFSO1FBQ0MsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ3ZCLElBQUksQ0FBQywyQkFBMkIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN6RCxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztZQUN2QyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztTQUN6QixDQUFDLENBQUM7SUFDSixDQUFDO0lBV0QseUNBQVEsR0FBUjtRQUFBLGlCQWtEQztRQWpEQSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxPQUFPLEVBQUU7WUFDN0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUM1QzthQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDOUM7YUFBTTtZQUNOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksR0FBRyxHQUFHO2dCQUNULE1BQU0sRUFBRSxJQUFJLENBQUMsb0JBQW9CO2dCQUNqQyxRQUFRLEVBQUMsSUFBSSxDQUFDLHNCQUFzQjthQUNwQyxDQUFBO1lBQ0QsSUFBSSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7Z0JBQ3pFLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtvQkFDaEMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO29CQUN6QyxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNyRCxLQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDMUMsS0FBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7b0JBQzdDLEtBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO29CQUNqRCxLQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQztpQkFDdkQ7cUJBQU07b0JBQ04sS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzVDLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzFCLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUMzQjtZQUNGLENBQUMsRUFBRSxVQUFBLEtBQUs7Z0JBQ1AsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO29CQUNuQyxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ25ELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO29CQUM1QyxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNyRCxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDN0MsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7b0JBQ2hELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO29CQUNwRCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQztpQkFDMUQ7Z0JBQ0QsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO29CQUNuQyxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ25ELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO29CQUM1QyxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNyRCxLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDN0MsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7b0JBQ2hELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO29CQUNwRCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQztpQkFDMUQ7WUFDRixDQUFDLENBQUMsQ0FBQTtTQUNGO0lBQ0YsQ0FBQztJQUVELDhDQUFhLEdBQWI7UUFBQSxpQkFVQztRQVRBLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDN0QsS0FBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztZQUMzQyxLQUFJLENBQUMscUJBQXFCLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzlELEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7WUFDekQsSUFBSSxLQUFJLENBQUMsaUJBQWlCLElBQUksS0FBSSxDQUFDLHFCQUFxQixFQUFFO2dCQUN6RCxLQUFJLENBQUMsMkJBQTJCLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDOUYsS0FBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUksQ0FBQyxpQkFBaUIsRUFBQyxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQTthQUM1RTtRQUNGLENBQUMsQ0FBQyxDQUFBO0lBQ0gsQ0FBQztJQUVELHFEQUFvQixHQUFwQixVQUFxQixJQUFJLEVBQUMsT0FBTztRQUNoQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDO1FBQ2pDLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxPQUFPLENBQUM7SUFDdkMsQ0FBQztJQUdELDZDQUFZLEdBQVosVUFBYSxLQUFLO1FBQWxCLGlCQWdCQztRQWZBLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFFOUIsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7WUFDOUMsSUFBQSwwQ0FBMkIsRUFBMUIsY0FBMEIsQ0FBQztZQUNsQyxNQUFNLENBQUMsYUFBYSxDQUFDLE1BQUksQ0FBQyxDQUFDO1lBRTNCLE1BQU0sQ0FBQyxNQUFNLEdBQUc7Z0JBQ2YsS0FBSSxDQUFDLDJCQUEyQixDQUFDLFVBQVUsQ0FBQztvQkFDM0MsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNO2lCQUNuQixDQUFDLENBQUM7Z0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUE7Z0JBQ2pDLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBSSxDQUFDO2dCQUN2QixLQUFJLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxDQUFDO1lBQ3hCLENBQUMsQ0FBQztTQUNGO0lBQ0YsQ0FBQzs7Z0JBMUlELFNBQVMsU0FBQztvQkFDVixRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixxbU5BQTJDO29CQUMzQyxVQUFVLEVBQUUsY0FBYztpQkFDMUI7Ozs7Z0JBUlEsWUFBWTtnQkFEWixXQUFXO2dCQUdYLHVCQUF1QjtnQkFKa0MsaUJBQWlCO2dCQUsxRSxlQUFlOzs7NkJBWXRCLE1BQU07O0lBa0lSLDZCQUFDO0NBQUEsQUE3SUQsSUE2SUM7U0F2SVksc0JBQXNCO0FBeUluQztJQUtDLDJCQUFZLE9BQU87UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFPLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO0lBQ2hDLENBQUM7SUFDRix3QkFBQztBQUFELENBQUMsQUFURCxJQVNDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCwgVmlld0NoaWxkLCBWaWV3RW5jYXBzdWxhdGlvbiwgQ2hhbmdlRGV0ZWN0b3JSZWYsIE91dHB1dCwgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1CdWlsZGVyLCBGb3JtR3JvdXAgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBmdXNlQW5pbWF0aW9ucyB9IGZyb20gJy4uLy4uL0BmdXNlL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSB9IGZyb20gJy4vLi4vcmVwb3J0LW1hbmFnZW1lbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2hhcmVkL3NuYWNrYmFyLnNlcnZpY2UnO1xyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogJ2FwcC1jb250cm9sLWZvcm0nLFxyXG5cdHRlbXBsYXRlVXJsOiAnLi9pbXBvcnQtZm9ybS5jb21wb25lbnQuaHRtbCcsXHJcblx0YW5pbWF0aW9uczogZnVzZUFuaW1hdGlvbnNcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBBdm1JbXBvcnRGb3JtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHsgXHJcblx0ZGlhbG9nVGl0bGU6IHN0cmluZztcclxuXHRpbXBvcnRBY2Nlc3NFbnRpdGxlbWVudEZvcm06IEZvcm1Hcm91cDtcclxuXHRjb250cm9sOiBFbnRpdGxlbWVudEltcG9ydDtcclxuXHRzdWJtaXR0ZWQgPSBmYWxzZTtcclxuXHRAT3V0cHV0KCkgYWZ0ZXJDbG9zZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblx0ZGF0YVNvdXJjZTogYW55O1xyXG5cdHNlbGVjdGVkRGF0YXNvdXJjZUlkOiBhbnk7XHJcblx0c2VsZWN0ZWREYXRhc291cmNlTmFtZTphbnk7XHJcblx0ZmlsZVVwbG9hZDogYW55O1xyXG5cclxuXHRkZWZhdWx0RGF0YXNvdXJjZTogc3RyaW5nO1xyXG5cdGRlZmF1bHREYXRhc291cmNlTmFtZTogc3RyaW5nO1xyXG5cclxuXHRzZWxlY3RlZEZpbGVOYW1lOiBzdHJpbmc7XHJcblxyXG5cdGJ0blN1Ym1pdDogYm9vbGVhbjtcclxuXHRjb25zdHJ1Y3RvcihcclxuXHRcdHB1YmxpYyBtYXREaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxBdm1JbXBvcnRGb3JtQ29tcG9uZW50PixcclxuXHRcdHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG5cdFx0cHJpdmF0ZSByZXBvcnRNYW5hZ2VtZW50U2VydmljZTogUmVwb3J0TWFuYWdlbWVudFNlcnZpY2UsXHJcblx0XHRwcml2YXRlIGNkOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuXHRcdHByaXZhdGUgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsXHJcblx0KSB7XHJcblx0XHR0aGlzLmRpYWxvZ1RpdGxlID0gJ0ltcG9ydCBBY2Nlc3MgRW50aXRsZW1lbnRzJztcclxuXHRcdHRoaXMuY29udHJvbCA9IG5ldyBFbnRpdGxlbWVudEltcG9ydCh7fSk7XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpIHtcclxuXHRcdHRoaXMuZ2V0RGF0YXNvdXJjZSgpO1xyXG5cdFx0dGhpcy5idG5TdWJtaXQgPSBmYWxzZTtcclxuXHRcdHRoaXMuaW1wb3J0QWNjZXNzRW50aXRsZW1lbnRGb3JtID0gdGhpcy5mb3JtQnVpbGRlci5ncm91cCh7XHJcblx0XHRcdGRhdGFfc291cmNlOiBbdGhpcy5jb250cm9sLmRhdGFfc291cmNlXSxcclxuXHRcdFx0ZmlsZTogW3RoaXMuY29udHJvbC5maWxlXSxcclxuXHRcdH0pO1xyXG5cdH1cclxuXHJcblx0cGFuZWxPcGVuU3RhdGUgPSBmYWxzZTtcclxuXHRsb2dzQXJyOiBhbnk7XHJcblx0bG9nc0RhdGE6IGFueTtcclxuXHR0b3RhbENvdW50OiBudW1iZXI7XHJcblx0c2F2ZUNvdW50OiBudW1iZXI7XHJcblx0ZmFpbGVkQ291bnQ6IG51bWJlcjtcclxuXHRkdXBsaWNhdGVDb3VudDogbnVtYmVyO1xyXG5cdGVycm9yTG9nc0hlYWRlcjogc3RyaW5nW10gPSBbJ3R5cGUnLCAnZGVzYyddXHJcblxyXG5cdG9uU3VibWl0KCkge1xyXG5cdFx0dGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG5cdFx0aWYgKHRoaXMuaW1wb3J0QWNjZXNzRW50aXRsZW1lbnRGb3JtLmludmFsaWQpIHtcclxuXHRcdFx0dGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFwiRmllbGRzIFJlcXVpcmVkXCIpO1xyXG5cdFx0fVxyXG5cdFx0ZWxzZSBpZiAoIXRoaXMuc2VsZWN0ZWRGaWxlTmFtZSkge1xyXG5cdFx0XHR0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFwiRmlsZSBSZXF1aXJlZFwiKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdHRoaXMuYnRuU3VibWl0ID0gdHJ1ZTtcclxuXHRcdFx0dmFyIG9iaiA9IHtcclxuXHRcdFx0XHRcImRhdGFcIjogdGhpcy5zZWxlY3RlZERhdGFzb3VyY2VJZCxcclxuXHRcdFx0XHQnZHNuYW1lJzp0aGlzLnNlbGVjdGVkRGF0YXNvdXJjZU5hbWVcclxuXHRcdFx0fVxyXG5cdFx0XHR0aGlzLnJlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmF2bUltcG9ydChvYmosIHRoaXMuZmlsZVVwbG9hZCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblx0XHRcdFx0aWYgKHJlcy5ib2R5Lm1ldGEuc3RhdHVzID09IDIwMSkge1xyXG5cdFx0XHRcdFx0dGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKHJlcy5ib2R5Lm1ldGEubXNnKTtcclxuXHRcdFx0XHRcdHRoaXMubG9nc0FyciA9IHJlcy5ib2R5LnJlc3BvbnNlLmxvZ3NBcnI7XHJcblx0XHRcdFx0XHR0aGlzLmxvZ3NEYXRhID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLmxvZ3NBcnIpO1xyXG5cdFx0XHRcdFx0dGhpcy50b3RhbENvdW50ID0gcmVzLmJvZHkucmVzcG9uc2UudG90YWw7XHJcblx0XHRcdFx0XHR0aGlzLnNhdmVDb3VudCA9IHJlcy5ib2R5LnJlc3BvbnNlLnNhdmVDb3VudDtcclxuXHRcdFx0XHRcdHRoaXMuZmFpbGVkQ291bnQgPSByZXMuYm9keS5yZXNwb25zZS5mYWlsZWRDb3VudDtcclxuXHRcdFx0XHRcdHRoaXMuZHVwbGljYXRlQ291bnQgPSByZXMuYm9keS5yZXNwb25zZS5kdXBsaWNhdGVDb3VudDtcclxuXHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0dGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKHJlcy5ib2R5Lm1ldGEubXNnKTtcclxuXHRcdFx0XHRcdHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcblx0XHRcdFx0XHR0aGlzLmFmdGVyQ2xvc2UuZW1pdChudWxsKTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0sIGVycm9yID0+IHtcclxuXHRcdFx0XHRpZiAoZXJyb3IuZXJyb3IubWV0YS5zdGF0dXMgPT0gNDA2KSB7XHJcblx0XHRcdFx0XHR0aGlzLmJ0blN1Ym1pdCA9IGZhbHNlO1xyXG5cdFx0XHRcdFx0dGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhlcnJvci5lcnJvci5tZXRhLm1zZyk7XHJcblx0XHRcdFx0XHR0aGlzLmxvZ3NBcnIgPSBlcnJvci5lcnJvci5yZXNwb25zZS5sb2dzQXJyO1xyXG5cdFx0XHRcdFx0dGhpcy5sb2dzRGF0YSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy5sb2dzQXJyKTtcclxuXHRcdFx0XHRcdHRoaXMudG90YWxDb3VudCA9IGVycm9yLmVycm9yLnJlc3BvbnNlLnRvdGFsO1xyXG5cdFx0XHRcdFx0dGhpcy5zYXZlQ291bnQgPSBlcnJvci5lcnJvci5yZXNwb25zZS5zYXZlQ291bnQ7XHJcblx0XHRcdFx0XHR0aGlzLmZhaWxlZENvdW50ID0gZXJyb3IuZXJyb3IucmVzcG9uc2UuZmFpbGVkQ291bnQ7XHJcblx0XHRcdFx0XHR0aGlzLmR1cGxpY2F0ZUNvdW50ID0gZXJyb3IuZXJyb3IucmVzcG9uc2UuZHVwbGljYXRlQ291bnQ7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHRcdGlmIChlcnJvci5lcnJvci5tZXRhLnN0YXR1cyA9PSA1MDApIHtcclxuXHRcdFx0XHRcdHRoaXMuYnRuU3VibWl0ID0gZmFsc2U7XHJcblx0XHRcdFx0XHR0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKGVycm9yLmVycm9yLm1ldGEubXNnKTtcclxuXHRcdFx0XHRcdHRoaXMubG9nc0FyciA9IGVycm9yLmVycm9yLnJlc3BvbnNlLmxvZ3NBcnI7XHJcblx0XHRcdFx0XHR0aGlzLmxvZ3NEYXRhID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLmxvZ3NBcnIpO1xyXG5cdFx0XHRcdFx0dGhpcy50b3RhbENvdW50ID0gZXJyb3IuZXJyb3IucmVzcG9uc2UudG90YWw7XHJcblx0XHRcdFx0XHR0aGlzLnNhdmVDb3VudCA9IGVycm9yLmVycm9yLnJlc3BvbnNlLnNhdmVDb3VudDtcclxuXHRcdFx0XHRcdHRoaXMuZmFpbGVkQ291bnQgPSBlcnJvci5lcnJvci5yZXNwb25zZS5mYWlsZWRDb3VudDtcclxuXHRcdFx0XHRcdHRoaXMuZHVwbGljYXRlQ291bnQgPSBlcnJvci5lcnJvci5yZXNwb25zZS5kdXBsaWNhdGVDb3VudDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHRnZXREYXRhc291cmNlKCkge1xyXG5cdFx0dGhpcy5yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRBbGxEYXRhc291cmNlcygpLnN1YnNjcmliZShyZXMgPT4ge1xyXG5cdFx0XHR0aGlzLmRhdGFTb3VyY2UgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXM7XHJcblx0XHRcdHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLm5hbWU7XHJcblx0XHRcdHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXNbMF0uX2lkO1xyXG5cdFx0XHRpZiAodGhpcy5kZWZhdWx0RGF0YXNvdXJjZSAmJiB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSkge1xyXG5cdFx0XHRcdHRoaXMuaW1wb3J0QWNjZXNzRW50aXRsZW1lbnRGb3JtLmNvbnRyb2xzWydkYXRhX3NvdXJjZSddLnNldFZhbHVlKHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lKTtcclxuXHRcdFx0XHR0aGlzLm9uQ2hhbmdlRGF0YXNvdXJjZUlkKHRoaXMuZGVmYXVsdERhdGFzb3VyY2UsdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUpXHJcblx0XHRcdH1cclxuXHRcdH0pXHJcblx0fVxyXG5cclxuXHRvbkNoYW5nZURhdGFzb3VyY2VJZChkc0lELGRzSW5hbWUpIHtcclxuXHRcdHRoaXMuc2VsZWN0ZWREYXRhc291cmNlSWQgPSBkc0lEO1xyXG5cdFx0dGhpcy5zZWxlY3RlZERhdGFzb3VyY2VOYW1lID0gZHNJbmFtZTtcclxuXHR9XHJcblxyXG5cclxuXHRvbkZpbGVDaGFuZ2UoZXZlbnQpIHtcclxuXHRcdGxldCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG5cclxuXHRcdGlmIChldmVudC50YXJnZXQuZmlsZXMgJiYgZXZlbnQudGFyZ2V0LmZpbGVzLmxlbmd0aCkge1xyXG5cdFx0XHRjb25zdCBbZmlsZV0gPSBldmVudC50YXJnZXQuZmlsZXM7XHJcblx0XHRcdHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xyXG5cclxuXHRcdFx0cmVhZGVyLm9ubG9hZCA9ICgpID0+IHtcclxuXHRcdFx0XHR0aGlzLmltcG9ydEFjY2Vzc0VudGl0bGVtZW50Rm9ybS5wYXRjaFZhbHVlKHtcclxuXHRcdFx0XHRcdGZpbGU6IHJlYWRlci5yZXN1bHRcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHR0aGlzLnNlbGVjdGVkRmlsZU5hbWUgPSBmaWxlLm5hbWVcclxuXHRcdFx0XHR0aGlzLmZpbGVVcGxvYWQgPSBmaWxlO1xyXG5cdFx0XHRcdHRoaXMuY2QubWFya0ZvckNoZWNrKCk7XHJcblx0XHRcdH07XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBFbnRpdGxlbWVudEltcG9ydCB7XHJcblxyXG5cdGRhdGFfc291cmNlOiBzdHJpbmc7XHJcblx0ZmlsZTogc3RyaW5nO1xyXG5cclxuXHRjb25zdHJ1Y3Rvcihjb250cm9sKSB7XHJcblx0XHR0aGlzLmRhdGFfc291cmNlID0gY29udHJvbC5kYXRhX3NvdXJjZSB8fCAnJztcclxuXHRcdHRoaXMuZmlsZSA9IGNvbnRyb2wuZmlsZSB8fCAnJztcclxuXHR9XHJcbn1cclxuIl19