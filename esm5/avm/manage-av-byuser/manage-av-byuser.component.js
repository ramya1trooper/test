import * as tslib_1 from "tslib";
import { SelectionModel } from '@angular/cdk/collections';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatSidenav } from '@angular/material';
import { Location } from '@angular/common';
import { ReportManagementService } from '../report-management.service';
import { SnackBarService } from './../../shared/snackbar.service';
import { manageAvbyLedgerUserTableConfig } from '../../table_config';
import { manageAvbyVendorUserTableConfig } from '../../table_config';
import { manageAvbyInvoiceUserTableConfig } from '../../table_config';
import 'rxjs/add/observable/of';
import * as FileSaver from 'file-saver';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from '../../_services/message.service';
import { process } from '@progress/kendo-data-query';
var ManageAvbyuserComponent = /** @class */ (function () {
    function ManageAvbyuserComponent(_reportManagementService, activeRoute, snackBarService, location, messageService, router) {
        this._reportManagementService = _reportManagementService;
        this.activeRoute = activeRoute;
        this.snackBarService = snackBarService;
        this.location = location;
        this.messageService = messageService;
        this.router = router;
        //displayedColumns: string[] = ['ledger', 'journalname', 'journaldescription', 'journalsource', 'journalcategory','journalperiod','currencycode','status','createdby','postedby','amount','posteddate'];
        //displayedColumns: string[] = ['select', 'controlname', 'description', 'usercounts', 'datasource', 'controltype','action'];
        this.selection = new SelectionModel(true, []);
        this.nav_position = 'end';
        this.queryParams = {};
        this.limit = 10;
        this.offset = 0;
        this.isLedgerTable = false;
        this.isVendorTable = false;
        this.isInvoiceTable = false;
        this.columns = [];
        this.selectedTableHeader = [];
        this.enableFilter = false;
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.state = {};
        this.viewData = {
            employees: []
        };
        this.panelOpenState = false;
        console.log("av detail URL Parms **************", this.activeRoute.snapshot.params);
    }
    ManageAvbyuserComponent.prototype.getNotification = function () {
        var _this = this;
        console.log('Do something with the notification (evt) sent by the child!', this._reportManagementService.passValue);
        this.queryParams.createdby = this._reportManagementService.passValue.createdBy;
        this.queryParams.journalname = this._reportManagementService.passValue.journalname;
        this.queryParams.collectionDetail = this._reportManagementService.passValue.collectionDetail;
        this.queryParams.datasource2 = this._reportManagementService.passValue.datasourceId;
        console.log('this.queryParams', this.queryParams);
        this._reportManagementService.getallAvByuser(this.queryParams).subscribe(function (response) {
            _this.manageAvsbyuser = response.data;
            console.log(response.type);
            var cols = Object.keys(response.data[0]);
            _this.settings = { actions: false };
            _this.settings["columns"] = {};
            //var arr=[id,name,amount]
            for (var i = 0; i <= cols.length; i++) {
                var colDisplayname = cols[i];
                if (colDisplayname && colDisplayname != undefined && colDisplayname != 'status') {
                    console.log(colDisplayname);
                    _this.settings["columns"][cols[i]] = { title: colDisplayname[0].toUpperCase() + colDisplayname.substr(1).toLowerCase() };
                }
            }
            for (var i = 0; i <= cols.length; i++) {
                var col = cols[i];
                if (col && col != undefined) {
                    var objCols = {};
                    objCols['columnDef'] = col;
                    objCols['formCtl'] = col + 'Filter';
                    objCols['placeHoldeName'] = col.toLocaleUpperCase() + ' Filter';
                    objCols['header'] = col.toLocaleUpperCase();
                    objCols['title'] = col.toUpperCase() + col.substr(1).toLowerCase();
                    _this.columns.push(objCols);
                }
            }
            if (response && response.data) {
                if (response.data[0].ledger) {
                    _this.isLedgerTable = true;
                    _this.tableConfig = manageAvbyLedgerUserTableConfig;
                }
                else if (response.data[0].vendorname) {
                    _this.isVendorTable = true;
                    _this.tableConfig = manageAvbyVendorUserTableConfig;
                }
                else {
                    _this.isInvoiceTable = true;
                    _this.tableConfig = manageAvbyInvoiceUserTableConfig;
                }
            }
            //this.displayedColumns = Object.keys(response.data[0]);
            _this.loadTableColumn();
            _this.dataSource = new MatTableDataSource(response.data);
            _this._data = response.data;
            _this.length = response.data.length;
            _this.dataSource.paginator = _this.paginator;
            console.log(">>>>>>>> response.data ", response.data);
            _this.applyTableState(_this.state);
        });
        /*this.settings = {
            columns: {
              id: {
                title: 'ID'
              },
              name: {
                title: 'Full Name'
              },
              username: {
                title: 'User Name'
              },
              email: {
                title: 'Email'
              }
            }
          };

          this.data = [
            {
              id: 1,
              name: "Leanne Graham",
              username: "Bret",
              email: "Sincere@april.biz"
            },
            {
              id: 2,
              name: "Ervin Howell",
              username: "Antonette",
              email: "Shanna@melissa.tv"
            },
            
            // ... list of items
            
            {
              id: 11,
              name: "Nicholas DuBuque",
              username: "Nicholas.Stanton",
              email: "Rey.Padberg@rosamond.biz"
            }
          ];*/
        // Do something with the notification (evt) sent by the child!
        console.log(this.viewData);
    };
    ManageAvbyuserComponent.prototype.ngOnInit = function () {
        this.state = {
            skip: this.offset,
            take: this.limit
        };
        this.getdatasource();
        this.getNotification();
    };
    ManageAvbyuserComponent.prototype.dataStateChange = function (state) {
        this.state = state;
        this.applyTableState(this.state);
    };
    ManageAvbyuserComponent.prototype.applyTableState = function (state) {
        this.kendoGridData = process(this._data, state);
    };
    ManageAvbyuserComponent.prototype.onClick = function (action) {
        if (action == 'refresh') {
            this.ngOnInit();
        }
    };
    ManageAvbyuserComponent.prototype.changePage = function (event) {
        console.log(event, ">>> EVENT Change Page");
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
    };
    ManageAvbyuserComponent.prototype.enableFilterOptions = function () {
        this.enableFilter = !this.enableFilter;
    };
    ManageAvbyuserComponent.prototype.applyFilter = function (filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    };
    ManageAvbyuserComponent.prototype.backToAVDetail = function () {
        this.location.back();
        var obj = {
            id: "manageAvDetail"
        };
        var self = this;
        setTimeout(function () {
            self.messageService.sendRouting(obj);
        }, 100);
    };
    ManageAvbyuserComponent.prototype.backToAV = function () {
        var obj = {
            id: "manageAvm"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['report-management/manage-av']);
    };
    ManageAvbyuserComponent.prototype.getdatasource = function () {
        var _this = this;
        this._reportManagementService.getAllDatasources().subscribe(function (res) {
            _this.customdatasource = res.response.datasources;
            _this.defaultdatasourceId = res.response.datasources[0]._id;
            _this.onLoadmanageAvs(_this.defaultdatasourceId);
        });
    };
    ManageAvbyuserComponent.prototype.getDatasourceId = function (id) {
        this.onLoadmanageAvs(id);
    };
    ManageAvbyuserComponent.prototype.updateTable = function (event) {
        console.log(event, "----event");
        var selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isactive = true;
            }
            else {
                item.isactive = false;
            }
        });
        // var index = this.tableNamesearch(event.source.value, this.tableConfig)
        // if (index >= 0) {
        // 	if (event.checked) {
        // 		let active = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: true
        // 		}
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, active);
        // 			localStorage.removeItem('control');
        // 			localStorage.setItem('control', JSON.stringify(this.tableConfig));
        // 		}
        // 	} else {
        // 		let inactive = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: false
        // 		}
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, inactive);
        // 			localStorage.removeItem('control');
        // 			localStorage.setItem('control', JSON.stringify(this.tableConfig));
        // 		}
        // 	}
        // }
        localStorage.removeItem('manageAvbyuser');
        localStorage.setItem('manageAvbyuser', JSON.stringify(this.tableConfig));
        this.loadTableColumn();
    };
    ManageAvbyuserComponent.prototype.tableNamesearch = function (nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].value === nameKey) {
                return i;
            }
        }
    };
    ManageAvbyuserComponent.prototype.tableSetting = function () {
        this.panelOpenState = !this.panelOpenState;
    };
    ManageAvbyuserComponent.prototype.loadTableColumn = function () {
        var e_1, _a, e_2, _b;
        console.log(this.displayedColumns, "......displayedColumns");
        this.displayedColumns = [];
        this.tableConfig = JSON.parse(localStorage.getItem('manageAvbyuser'));
        if (this.tableConfig) {
            try {
                for (var _c = tslib_1.__values(this.tableConfig), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var columns = _d.value;
                    if (columns.isactive) {
                        // this.displayedColumns.push(columns.value);
                        // this.selectedTableHeader.push(columns.value);
                        this.displayedColumns.push(columns);
                        this.selectedTableHeader.push(columns.value);
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        else {
            if (this.isLedgerTable) {
                this.tableConfig = manageAvbyLedgerUserTableConfig;
            }
            else if (this.isVendorTable) {
                this.tableConfig = manageAvbyVendorUserTableConfig;
            }
            else if (this.isInvoiceTable) {
                this.tableConfig = manageAvbyInvoiceUserTableConfig;
            }
            console.log(this.tableConfig, "...........tableconfig");
            try {
                for (var _e = tslib_1.__values(this.tableConfig), _f = _e.next(); !_f.done; _f = _e.next()) {
                    var columns = _f.value;
                    if (columns.isactive) {
                        // this.displayedColumns.push(columns.value);
                        // this.selectedTableHeader.push(columns.value);
                        this.displayedColumns.push(columns);
                        this.selectedTableHeader.push(columns.value);
                    }
                }
            }
            catch (e_2_1) { e_2 = { error: e_2_1 }; }
            finally {
                try {
                    if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                }
                finally { if (e_2) throw e_2.error; }
            }
            console.log(this.displayedColumns, "....this.displayedColumns");
            console.log(">>>> selectedTableHeader ", this.selectedTableHeader);
        }
    };
    ManageAvbyuserComponent.prototype.onLoadmanageAvs = function (id) {
        this.defaultdatasourceId = id;
    };
    ManageAvbyuserComponent.prototype.exportAvmUserdetail = function () {
        console.log('data source id', this._reportManagementService.passValue.datasourceId);
        var value = 1;
        this.avmExportObj = {};
        this.avmExportObj['datasource'] = this._reportManagementService.passValue.datasourceId;
        this.avmExportObj['report_type'] = 'CSV';
        this.avmExportObj['collectionDetail'] = this._reportManagementService.passValue.collectionDetail;
        this.avmExportObj['createdby'] = this._reportManagementService.passValue.createdBy;
        this.avmExportObj['journalname'] = this._reportManagementService.passValue.journalname;
        this._reportManagementService.exportAVMUSerdetails(this.avmExportObj).subscribe(function (resp) {
            if (value == 1) {
                var blob = new Blob([resp.body], { type: 'text/csv' });
                FileSaver.saveAs(blob, "AccessViolationByUser.csv");
            }
            else {
                var blob = new Blob([resp.body], { type: 'text/xlsx' });
                FileSaver.saveAs(blob, "UserConflict.xlsx");
            }
        }, function (error) {
            console.log("error:::" + JSON.stringify(error));
        });
    };
    ManageAvbyuserComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-manage-av-byuser',
                    template: "<div class=\"page-layout blank\" style=\"padding: 0px !important;\" fusePerfectScrollbar >\r\n   <div style=\"padding: 10px\">\r\n      <button mat-button (click)=\"backToAV()\"><mat-icon>arrow_left</mat-icon>Back to AV</button>\r\n      <button mat-button (click)=\"backToAVDetail()\"><mat-icon>arrow_left</mat-icon>Back to AV Detail</button>\r\n   </div>\r\n   <mat-drawer-container class=\"example-container sen-bg-container\" autosize fxFlex [hasBackdrop]=\"false\">\r\n      <div>\r\n      <div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\"\r\n         style=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: space-between;\">\r\n         <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n         style=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: flex-start;\">\r\n         </div>\r\n         <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n               <button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilterOptions()\">\r\n                  <span class=\"k-icon k-i-filter\"\r\n                  style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n               </button>\r\n               <button  class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n               (click)=\"select.open()\">\r\n               <span class=\"material-icons\"\r\n               style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n               <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n               (selectionChange)=\"updateTable($event)\">\r\n               <mat-option *ngFor=\"let columnSetting of tableConfig\" [checked]=\"columnSetting.isactive\"\r\n                  [value]=\"columnSetting.value\">{{ columnSetting.name}}</mat-option>\r\n               </mat-select>\r\n               </button>\r\n               <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"  (click)=\"onClick('refresh')\">\r\n                  <span class=\"material-icons\"\r\n                  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n                  autorenew</span>Refresh\r\n               </button>\r\n               <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"   (click)=\"exportAvmUserdetail()\">\r\n                  <span class=\"material-icons\"\r\n                  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n                  cloud_download</span>Export\r\n               </button>\r\n         </div>\r\n      </div>\r\n      <div style=\"overflow-x: auto;\">\r\n         <div *ngIf=\"isLedgerTable\">\r\n            <div class=\"mat-elevation-z8 sen-margin-10\">\r\n               <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n               [skip]=\"state.skip\"\r\n               [sort]=\"state.sort\"\r\n               [filter]=\"state.filter\"\r\n               [sortable]=\"true\"\r\n               [pageable]=\"true\" \r\n               [resizable]=\"true\"\r\n               [filterable]=\"enableFilter\"\r\n               (dataStateChange)=\"dataStateChange($event)\">\r\n               <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n                  <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\">\r\n                     <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <span> \r\n                           {{dataItem[column.value]}}\r\n                        </span>\r\n                     </ng-template>\r\n                  </kendo-grid-column>\r\n               </ng-container>\r\n            </kendo-grid>\r\n            </div>\r\n         </div>\r\n         <div *ngIf=\"isVendorTable\">\r\n            <div class=\"mat-elevation-z8 sen-margin-10\">\r\n               <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n               [skip]=\"state.skip\"\r\n               [sort]=\"state.sort\"\r\n               [filter]=\"state.filter\"\r\n               [sortable]=\"true\"\r\n               [pageable]=\"true\" \r\n               [resizable]=\"true\"\r\n               [filterable]=\"enableFilter\"\r\n               (dataStateChange)=\"dataStateChange($event)\">\r\n               <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n                  <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\">\r\n                     <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <span> \r\n                           {{dataItem[column.value]}}\r\n                        </span>\r\n                     </ng-template>\r\n                  </kendo-grid-column>\r\n               </ng-container>\r\n            </kendo-grid>\r\n            </div>\r\n         </div>\r\n         <div *ngIf=\"isInvoiceTable\">\r\n            <div class=\"mat-elevation-z8 sen-margin-10\">\r\n               <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n               [skip]=\"state.skip\"\r\n               [sort]=\"state.sort\"\r\n               [filter]=\"state.filter\"\r\n               [sortable]=\"true\"\r\n               [pageable]=\"true\" \r\n               [resizable]=\"true\"\r\n               [filterable]=\"enableFilter\"\r\n               (dataStateChange)=\"dataStateChange($event)\">\r\n               <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n                  <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\">\r\n                     <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <span> \r\n                           {{dataItem[column.value]}}\r\n                        </span>\r\n                     </ng-template>\r\n                  </kendo-grid-column>\r\n               </ng-container>\r\n            </kendo-grid>\r\n            </div>\r\n         </div>\r\n      </div>\r\n  </div>\r\n </mat-drawer-container>\r\n</div>\r\n",
                    styles: ["content{position:relative;display:-webkit-box;display:flex;z-index:1;-webkit-box-flex:1;flex:1 0 auto}content>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto;width:100%;min-width:100%}.card-directive{background:#fff;border:1px solid #d3d3d3;margin:5px!important}chart-layout{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;-webkit-box-flex:1!important;flex:auto!important}.ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.ui-common-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}.example-container{width:100%;height:100%}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}"]
                }] }
    ];
    /** @nocollapse */
    ManageAvbyuserComponent.ctorParameters = function () { return [
        { type: ReportManagementService },
        { type: ActivatedRoute },
        { type: SnackBarService },
        { type: Location },
        { type: MessageService },
        { type: Router }
    ]; };
    ManageAvbyuserComponent.propDecorators = {
        paginator: [{ type: ViewChild, args: [MatPaginator,] }],
        drawer: [{ type: ViewChild, args: ['drawer',] }]
    };
    return ManageAvbyuserComponent;
}());
export { ManageAvbyuserComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlLWF2LWJ5dXNlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYXZtL21hbmFnZS1hdi1ieXVzZXIvbWFuYWdlLWF2LWJ5dXNlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsU0FBUyxFQUFVLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsWUFBWSxFQUFFLGtCQUFrQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUV2RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDbEUsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckUsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckUsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFHdEUsT0FBTyx3QkFBd0IsQ0FBQztBQUNoQyxPQUFPLEtBQUssU0FBUyxNQUFNLFlBQVksQ0FBQztBQUN4QyxPQUFPLEVBQUUsTUFBTSxFQUFFLGNBQWMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUVqRSxPQUFPLEVBQStCLE9BQU8sRUFBUSxNQUFNLDRCQUE0QixDQUFDO0FBa0J4RjtJQWlLRyxpQ0FBb0Isd0JBQWlELEVBQzlELFdBQTBCLEVBQzFCLGVBQWdDLEVBQVUsUUFBa0IsRUFDN0QsY0FBK0IsRUFBUyxNQUFjO1FBSHhDLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBeUI7UUFDOUQsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFDMUIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUM3RCxtQkFBYyxHQUFkLGNBQWMsQ0FBaUI7UUFBUyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBMUo5RCx3TUFBd007UUFDeE0sNEhBQTRIO1FBQzVILGNBQVMsR0FBRyxJQUFJLGNBQWMsQ0FBaUIsSUFBSSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRXpELGlCQUFZLEdBQVcsS0FBSyxDQUFDO1FBQzdCLGdCQUFXLEdBQVEsRUFBRSxDQUFDO1FBQ3RCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsV0FBTSxHQUFXLENBQUMsQ0FBQztRQUluQixrQkFBYSxHQUFhLEtBQUssQ0FBQztRQUNoQyxrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUMvQixtQkFBYyxHQUFZLEtBQUssQ0FBQztRQUdoQyxZQUFPLEdBQUcsRUFBRSxDQUFDO1FBT1osd0JBQW1CLEdBQUUsRUFBRSxDQUFDO1FBSXhCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRXZCLFNBQUksR0FBRyxJQUFJLENBQUM7UUFDWixTQUFJLEdBQXdCLFNBQVMsQ0FBQztRQUN0QyxjQUFTLEdBQUcsQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7UUFDbkgsaUJBQVksR0FBRyxJQUFJLENBQUM7UUFDM0IsVUFBSyxHQUFVLEVBQUUsQ0FBQztRQStHcEIsYUFBUSxHQUFHO1lBQ1AsU0FBUyxFQUFHLEVBQUU7U0FDakIsQ0FBQztRQWdJRCxtQkFBYyxHQUFZLEtBQUssQ0FBQztRQXJIL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQ0FBb0MsRUFBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQTtJQUVsRixDQUFDO0lBNUhGLGlEQUFlLEdBQWY7UUFBQSxpQkEyR0E7UUExR0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2REFBNkQsRUFBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFbkgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7UUFDbkYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDO1FBQzdGLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1FBQ3JGLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLFFBQVE7WUFFaEYsS0FBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ3JDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRzNCLElBQUksSUFBSSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBRXpDLEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBQyxPQUFPLEVBQUUsS0FBSyxFQUFDLENBQUM7WUFFakMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsR0FBQyxFQUFFLENBQUE7WUFDM0IsMEJBQTBCO1lBQzFCLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsSUFBRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO2dCQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLElBQUcsY0FBYyxJQUFHLGNBQWMsSUFBRSxTQUFTLElBQUksY0FBYyxJQUFFLFFBQVEsRUFBQztvQkFDekUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQztvQkFDNUIsS0FBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBQyxFQUFDLEtBQUssRUFBRyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBQyxDQUFBO2lCQUNwSDthQUNEO1lBRUQsS0FBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQ3BDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQTtnQkFDakIsSUFBRyxHQUFHLElBQUcsR0FBRyxJQUFFLFNBQVMsRUFBQztvQkFDdkIsSUFBSSxPQUFPLEdBQUMsRUFBRSxDQUFDO29CQUNmLE9BQU8sQ0FBQyxXQUFXLENBQUMsR0FBQyxHQUFHLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBQyxHQUFHLEdBQUMsUUFBUSxDQUFDO29CQUNoQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsR0FBQyxTQUFTLENBQUM7b0JBQzVELE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztvQkFDMUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsR0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUMvRCxLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFHM0I7YUFFRDtZQUNELElBQUcsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLEVBQUU7Z0JBQzdCLElBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLEVBQUM7b0JBQzFCLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO29CQUMxQixLQUFJLENBQUMsV0FBVyxHQUFHLCtCQUErQixDQUFDO2lCQUNuRDtxQkFBSyxJQUFJLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxFQUFFO29CQUN0QyxLQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztvQkFDMUIsS0FBSSxDQUFDLFdBQVcsR0FBRywrQkFBK0IsQ0FBQztpQkFDbkQ7cUJBQUs7b0JBQ0wsS0FBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7b0JBQzNCLEtBQUksQ0FBQyxXQUFXLEdBQUcsZ0NBQWdDLENBQUM7aUJBQ3BEO2FBQ0Q7WUFDRCx3REFBd0Q7WUFDeEQsS0FBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3ZCLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDeEQsS0FBSSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQzNCLEtBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDbkMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQztZQUMzQyxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyRCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNsQyxDQUFDLENBQUMsQ0FBQztRQUVIOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Y0F1Q007UUFDTiw4REFBOEQ7UUFDOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7SUFDNUIsQ0FBQztJQW1CRSwwQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLEtBQUssR0FBQztZQUNWLElBQUksRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDakIsQ0FBQTtRQUNELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFHeEIsQ0FBQztJQUNELGlEQUFlLEdBQWYsVUFBZ0IsS0FBMkI7UUFDMUMsSUFBSSxDQUFDLEtBQUssR0FBRSxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUNILGlEQUFlLEdBQWYsVUFBZ0IsS0FBWTtRQUMzQixJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDSCx5Q0FBTyxHQUFQLFVBQVEsTUFBTTtRQUNiLElBQUcsTUFBTSxJQUFFLFNBQVMsRUFDcEI7WUFDRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDQSxDQUFDO0lBQ0gsNENBQVUsR0FBVixVQUFXLEtBQUs7UUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSx1QkFBdUIsQ0FBQyxDQUFBO1FBQzNDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFDRCxxREFBbUIsR0FBbkI7UUFDRCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUNyQyxDQUFDO0lBQ0osNkNBQVcsR0FBWCxVQUFZLFdBQW1CO1FBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM3RCxDQUFDO0lBRUEsZ0RBQWMsR0FBZDtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxHQUFHLEdBQUc7WUFDVCxFQUFFLEVBQUcsZ0JBQWdCO1NBQ3JCLENBQUM7UUFDRixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsVUFBVSxDQUFDO1lBQ1YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUFDLEdBQUcsQ0FBQyxDQUFBO0lBQ0wsQ0FBQztJQUVGLDBDQUFRLEdBQVI7UUFDQyxJQUFJLEdBQUcsR0FBRztZQUNULEVBQUUsRUFBRyxXQUFXO1NBQ2hCLENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLDZCQUE2QixDQUFDLENBQUMsQ0FBQztJQUN2RCxDQUFDO0lBRUEsK0NBQWEsR0FBYjtRQUFBLGlCQU1BO1FBTEEsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGlCQUFpQixFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsR0FBRztZQUM5RCxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDakQsS0FBSSxDQUFDLG1CQUFtQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUMzRCxLQUFJLENBQUMsZUFBZSxDQUFDLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFBO0lBQ0gsQ0FBQztJQUVBLGlEQUFlLEdBQWYsVUFBZ0IsRUFBRTtRQUNsQixJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFDRCw2Q0FBVyxHQUFYLFVBQVksS0FBSztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQTtRQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBUyxJQUFJO1lBQ2xDLElBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN4QjtpQkFBSTtnQkFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ1IseUVBQXlFO1FBQ3pFLG9CQUFvQjtRQUNwQix3QkFBd0I7UUFDeEIsbUJBQW1CO1FBQ25CLGdDQUFnQztRQUNoQyw4QkFBOEI7UUFDOUIsb0JBQW9CO1FBQ3BCLE1BQU07UUFDTixzQkFBc0I7UUFDdEIsZ0RBQWdEO1FBQ2hELHlDQUF5QztRQUN6Qyx3RUFBd0U7UUFDeEUsTUFBTTtRQUNOLFlBQVk7UUFDWixxQkFBcUI7UUFDckIsZ0NBQWdDO1FBQ2hDLDhCQUE4QjtRQUM5QixxQkFBcUI7UUFDckIsTUFBTTtRQUNOLHNCQUFzQjtRQUN0QixrREFBa0Q7UUFDbEQseUNBQXlDO1FBQ3pDLHdFQUF3RTtRQUN4RSxNQUFNO1FBQ04sS0FBSztRQUNMLElBQUk7UUFDSixZQUFZLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDMUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBQ0QsaURBQWUsR0FBZixVQUFnQixPQUFPLEVBQUUsT0FBTztRQUMvQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN4QyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssT0FBTyxFQUFFO2dCQUNqQyxPQUFPLENBQUMsQ0FBQzthQUNUO1NBQ0Q7SUFDRixDQUFDO0lBR0QsOENBQVksR0FBWjtRQUNDLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFBO0lBQzNDLENBQUM7SUFDRCxpREFBZSxHQUFmOztRQUNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDLHdCQUF3QixDQUFDLENBQUE7UUFDM0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7UUFDdEUsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFOztnQkFDckIsS0FBb0IsSUFBQSxLQUFBLGlCQUFBLElBQUksQ0FBQyxXQUFXLENBQUEsZ0JBQUEsNEJBQUU7b0JBQWpDLElBQUksT0FBTyxXQUFBO29CQUNmLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTt3QkFDckIsNkNBQTZDO3dCQUM3QyxnREFBZ0Q7d0JBQ2hELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3BDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM3QztpQkFDRDs7Ozs7Ozs7O1NBQ0Q7YUFBTTtZQUNOLElBQUcsSUFBSSxDQUFDLGFBQWEsRUFBQztnQkFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRywrQkFBK0IsQ0FBQzthQUNuRDtpQkFBSyxJQUFHLElBQUksQ0FBQyxhQUFhLEVBQUM7Z0JBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsK0JBQStCLENBQUM7YUFDbkQ7aUJBQUssSUFBRyxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLGdDQUFnQyxDQUFDO2FBQ3BEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFDLHdCQUF3QixDQUFDLENBQUE7O2dCQUN0RCxLQUFvQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLFdBQVcsQ0FBQSxnQkFBQSw0QkFBRTtvQkFBakMsSUFBSSxPQUFPLFdBQUE7b0JBQ2YsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO3dCQUNyQiw2Q0FBNkM7d0JBQzdDLGdEQUFnRDt3QkFDaEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDcEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBRTdDO2lCQUNEOzs7Ozs7Ozs7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBQywyQkFBMkIsQ0FBQyxDQUFBO1lBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDbEU7SUFDRixDQUFDO0lBRUEsaURBQWUsR0FBZixVQUFnQixFQUFFO1FBQ2xCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUE7SUFFOUIsQ0FBQztJQUVELHFEQUFtQixHQUFuQjtRQUNPLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQTtRQUNsRixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDZCxJQUFJLENBQUMsWUFBWSxHQUFDLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1FBQ3ZGLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsR0FBRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDO1FBQ2hHLElBQUksQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7UUFDbkYsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQztRQUV2RixJQUFJLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFFaEYsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUNaLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pELFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLDJCQUEyQixDQUFDLENBQUM7YUFDdkQ7aUJBQ0k7Z0JBQ0QsSUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztnQkFDMUQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsQ0FBQzthQUMvQztRQUNMLENBQUMsRUFBRSxVQUFBLEtBQUs7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDcEQsQ0FBQyxDQUFDLENBQUM7SUFHUCxDQUFDOztnQkFuV0osU0FBUyxTQUFDO29CQUNWLFFBQVEsRUFBRSxzQkFBc0I7b0JBQy9CLDhnTkFBZ0Q7O2lCQUVqRDs7OztnQkFuQ1EsdUJBQXVCO2dCQVVmLGNBQWM7Z0JBUnRCLGVBQWU7Z0JBSGYsUUFBUTtnQkFZUixjQUFjO2dCQURkLE1BQU07Ozs0QkFrTGQsU0FBUyxTQUFDLFlBQVk7eUJBQ3JCLFNBQVMsU0FBQyxRQUFROztJQXdNbEIsOEJBQUM7Q0FBQSxBQXRXSCxJQXNXRztTQWpXVSx1QkFBdUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0UGFnaW5hdG9yLCBNYXRUYWJsZURhdGFTb3VyY2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IE1hdFNpZGVuYXYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IExvY2F0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgUmVwb3J0TWFuYWdlbWVudFNlcnZpY2UgfSBmcm9tICcuLi9yZXBvcnQtbWFuYWdlbWVudC5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2hhcmVkL3NuYWNrYmFyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBtYW5hZ2VBdmJ5TGVkZ2VyVXNlclRhYmxlQ29uZmlnIH0gZnJvbSAnLi4vLi4vdGFibGVfY29uZmlnJztcclxuaW1wb3J0IHsgbWFuYWdlQXZieVZlbmRvclVzZXJUYWJsZUNvbmZpZyB9IGZyb20gJy4uLy4uL3RhYmxlX2NvbmZpZyc7XHJcbmltcG9ydCB7IG1hbmFnZUF2YnlJbnZvaWNlVXNlclRhYmxlQ29uZmlnIH0gZnJvbSAnLi4vLi4vdGFibGVfY29uZmlnJztcclxuaW1wb3J0IHtEYXRhU291cmNlfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcy9SeCdcclxuaW1wb3J0ICdyeGpzL2FkZC9vYnNlcnZhYmxlL29mJztcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gJ2ZpbGUtc2F2ZXInO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9fc2VydmljZXMvbWVzc2FnZS5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7IEdyb3VwRGVzY3JpcHRvciwgRGF0YVJlc3VsdCwgcHJvY2VzcyxTdGF0ZSB9IGZyb20gJ0Bwcm9ncmVzcy9rZW5kby1kYXRhLXF1ZXJ5JztcclxuaW1wb3J0IHsgR3JpZERhdGFSZXN1bHQsIFBhZ2VDaGFuZ2VFdmVudCxTZWxlY3RBbGxDaGVja2JveFN0YXRlLERhdGFTdGF0ZUNoYW5nZUV2ZW50IH0gZnJvbSAnQHByb2dyZXNzL2tlbmRvLWFuZ3VsYXItZ3JpZCc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIG1hbmFnZUF2Ynl1c2VyIHtcclxuICBsZWRnZXI6IHN0cmluZztcclxuICBqb3VybmFsbmFtZTogc3RyaW5nO1xyXG4gIGpvdXJuYWxkZXNjcmlwdGlvbjogc3RyaW5nO1xyXG4gIGpvdXJuYWxzb3VyY2U6IHN0cmluZztcclxuXHRqb3VybmFsY2F0ZWdvcnk6IHN0cmluZztcclxuXHRqb3VybmFscGVyaW9kOiBzdHJpbmc7XHJcblx0Y3VycmVuY3ljb2RlOiBzdHJpbmc7XHJcblx0c3RhdHVzOiBzdHJpbmc7XHJcblx0Y3JlYXRlZGJ5OiBzdHJpbmc7XHJcblx0cG9zdGVkYnk6IHN0cmluZztcclxuXHRhbW91bnQ6IG51bWJlcjtcclxuXHRwb3N0ZWRkYXRlOiBEYXRlO1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuXHRzZWxlY3RvcjogJ2FwcC1tYW5hZ2UtYXYtYnl1c2VyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbWFuYWdlLWF2LWJ5dXNlci5jb21wb25lbnQuaHRtbCcsXHJcblx0c3R5bGVVcmxzOiBbJy4vbWFuYWdlLWF2LWJ5dXNlci5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNYW5hZ2VBdmJ5dXNlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblx0bWFuYWdlQXZzYnl1c2VyOmFueTtcclxuXHQvL2RhdGFTb3VyY2U6YW55O1xyXG5cdGN1c3RvbWRhdGFzb3VyY2U6b2JqZWN0W107XHJcblx0ZGVmYXVsdGRhdGFzb3VyY2VJZDpzdHJpbmc7XHJcblx0Ly9kaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFsnbGVkZ2VyJywgJ2pvdXJuYWxuYW1lJywgJ2pvdXJuYWxkZXNjcmlwdGlvbicsICdqb3VybmFsc291cmNlJywgJ2pvdXJuYWxjYXRlZ29yeScsJ2pvdXJuYWxwZXJpb2QnLCdjdXJyZW5jeWNvZGUnLCdzdGF0dXMnLCdjcmVhdGVkYnknLCdwb3N0ZWRieScsJ2Ftb3VudCcsJ3Bvc3RlZGRhdGUnXTtcclxuXHQvL2Rpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gWydzZWxlY3QnLCAnY29udHJvbG5hbWUnLCAnZGVzY3JpcHRpb24nLCAndXNlcmNvdW50cycsICdkYXRhc291cmNlJywgJ2NvbnRyb2x0eXBlJywnYWN0aW9uJ107XHJcblx0c2VsZWN0aW9uID0gbmV3IFNlbGVjdGlvbk1vZGVsPG1hbmFnZUF2Ynl1c2VyPih0cnVlLCBbXSk7XHJcblx0ZGlhbG9nUmVmOiBhbnk7XHJcblx0bmF2X3Bvc2l0aW9uOiBzdHJpbmcgPSAnZW5kJztcclxuXHRxdWVyeVBhcmFtczogYW55ID0ge307XHJcblx0bGltaXQ6IG51bWJlciA9IDEwO1xyXG5cdG9mZnNldDogbnVtYmVyID0gMDtcclxuXHRsZW5ndGg6IG51bWJlcjtcclxuXHR0YWJsZUNvbmZpZzogYW55O1xyXG5cdGF2bUV4cG9ydE9iajp7fTtcclxuXHRpc0xlZGdlclRhYmxlIDogQm9vbGVhbiA9IGZhbHNlO1xyXG5cdGlzVmVuZG9yVGFibGU6IEJvb2xlYW4gPSBmYWxzZTtcclxuXHRpc0ludm9pY2VUYWJsZTogQm9vbGVhbiA9IGZhbHNlO1xyXG5cdFxyXG5cclxuXHRjb2x1bW5zID0gW107XHJcblx0c2V0dGluZ3M6YW55O1xyXG5cdGRhdGE6YW55O1xyXG5cdHRlbXBzZXR0aW5nczpvYmplY3Q7XHJcblx0dGVtcENvbHM6b2JqZWN0O1xyXG5cdGRhdGFTb3VyY2UgOmFueTtcclxuXHRkaXNwbGF5ZWRDb2x1bW5zOmFueTtcclxuXHRcdHNlbGVjdGVkVGFibGVIZWFkZXI9IFtdO1xyXG5cclxuXHJcbiAga2VuZG9HcmlkRGF0YSA6IEdyaWREYXRhUmVzdWx0O1xyXG4gIGVuYWJsZUZpbHRlcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHB1YmxpYyBfZGF0YTogYW55W107XHJcbiAgcHVibGljIGluZm8gPSB0cnVlO1xyXG4gIHB1YmxpYyB0eXBlOiBcIm51bWVyaWNcIiB8IFwiaW5wdXRcIiA9IFwibnVtZXJpY1wiO1xyXG4gIHB1YmxpYyBwYWdlU2l6ZXMgPSBbeyB0ZXh0OiAxMCwgdmFsdWU6IDEwIH0sIHsgdGV4dDogMjUsIHZhbHVlOiAyNSB9LCB7IHRleHQ6IDUwLCB2YWx1ZTogNTAgfSwgeyB0ZXh0OiAxMDAsIHZhbHVlOiAxMDAgfV07XHJcbiAgcHVibGljIHByZXZpb3VzTmV4dCA9IHRydWU7XHJcbiAgc3RhdGU6IFN0YXRlID0ge307XHJcblxyXG5cdGdldE5vdGlmaWNhdGlvbigpIHtcclxuXHRcdGNvbnNvbGUubG9nKCdEbyBzb21ldGhpbmcgd2l0aCB0aGUgbm90aWZpY2F0aW9uIChldnQpIHNlbnQgYnkgdGhlIGNoaWxkIScsdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlKTtcclxuXHJcblx0XHR0aGlzLnF1ZXJ5UGFyYW1zLmNyZWF0ZWRieSA9IHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5jcmVhdGVkQnk7XHJcblx0XHR0aGlzLnF1ZXJ5UGFyYW1zLmpvdXJuYWxuYW1lID0gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmpvdXJuYWxuYW1lO1xyXG5cdFx0dGhpcy5xdWVyeVBhcmFtcy5jb2xsZWN0aW9uRGV0YWlsID0gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmNvbGxlY3Rpb25EZXRhaWw7XHJcblx0XHR0aGlzLnF1ZXJ5UGFyYW1zLmRhdGFzb3VyY2UyID0gIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5kYXRhc291cmNlSWQ7XHJcblx0XHRjb25zb2xlLmxvZygndGhpcy5xdWVyeVBhcmFtcycsdGhpcy5xdWVyeVBhcmFtcyk7XHJcblx0XHR0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRhbGxBdkJ5dXNlcih0aGlzLnF1ZXJ5UGFyYW1zKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBcclxuXHRcdFx0dGhpcy5tYW5hZ2VBdnNieXVzZXIgPSByZXNwb25zZS5kYXRhO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhyZXNwb25zZS50eXBlKTtcclxuXHJcblx0XHRcclxuXHRcdFx0dmFyIGNvbHMgPSBPYmplY3Qua2V5cyhyZXNwb25zZS5kYXRhWzBdKTtcclxuXHRcdFx0XHJcblx0XHRcdHRoaXMuc2V0dGluZ3MgPSB7YWN0aW9uczogZmFsc2V9O1xyXG5cclxuXHRcdFx0dGhpcy5zZXR0aW5nc1tcImNvbHVtbnNcIl09e31cclxuXHRcdFx0Ly92YXIgYXJyPVtpZCxuYW1lLGFtb3VudF1cclxuXHRcdFx0Zm9yKHZhciBpPTA7aTw9Y29scy5sZW5ndGg7IGkrKyl7XHJcblx0XHRcdFx0dmFyIGNvbERpc3BsYXluYW1lID0gY29sc1tpXTtcclxuXHRcdFx0XHRpZihjb2xEaXNwbGF5bmFtZSAmJmNvbERpc3BsYXluYW1lIT11bmRlZmluZWQgJiYgY29sRGlzcGxheW5hbWUhPSdzdGF0dXMnKXtcclxuXHRcdFx0XHRcdGNvbnNvbGUubG9nKGNvbERpc3BsYXluYW1lKTtcclxuXHRcdFx0XHRcdHRoaXMuc2V0dGluZ3NbXCJjb2x1bW5zXCJdW2NvbHNbaV1dPXt0aXRsZTogIGNvbERpc3BsYXluYW1lWzBdLnRvVXBwZXJDYXNlKCkgKyBjb2xEaXNwbGF5bmFtZS5zdWJzdHIoMSkudG9Mb3dlckNhc2UoKX1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGZvcih2YXIgaSA9IDA7IGkgPD0gY29scy5sZW5ndGg7IGkrKyl7XHJcblx0XHRcdFx0dmFyIGNvbCA9IGNvbHNbaV1cclxuXHRcdFx0XHRpZihjb2wgJiZjb2whPXVuZGVmaW5lZCl7XHJcblx0XHRcdFx0XHR2YXIgb2JqQ29scz17fTtcclxuXHRcdFx0XHRcdG9iakNvbHNbJ2NvbHVtbkRlZiddPWNvbDtcclxuXHRcdFx0XHRcdG9iakNvbHNbJ2Zvcm1DdGwnXT1jb2wrJ0ZpbHRlcic7XHJcblx0XHRcdFx0XHRvYmpDb2xzWydwbGFjZUhvbGRlTmFtZSddPWNvbC50b0xvY2FsZVVwcGVyQ2FzZSgpKycgRmlsdGVyJztcclxuXHRcdFx0XHRcdG9iakNvbHNbJ2hlYWRlciddPWNvbC50b0xvY2FsZVVwcGVyQ2FzZSgpO1xyXG5cdFx0XHRcdFx0b2JqQ29sc1sndGl0bGUnXT1jb2wudG9VcHBlckNhc2UoKStjb2wuc3Vic3RyKDEpLnRvTG93ZXJDYXNlKCk7XHJcblx0XHRcdFx0XHR0aGlzLmNvbHVtbnMucHVzaChvYmpDb2xzKTtcclxuXHRcdFx0XHRcdFxyXG5cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0XHRcclxuXHRcdCB9XHJcblx0XHRcdGlmKHJlc3BvbnNlICYmIHJlc3BvbnNlLmRhdGEpIHtcclxuXHRcdFx0XHRpZihyZXNwb25zZS5kYXRhWzBdLmxlZGdlcil7XHJcblx0XHRcdFx0XHR0aGlzLmlzTGVkZ2VyVGFibGUgPSB0cnVlO1xyXG5cdFx0XHRcdFx0dGhpcy50YWJsZUNvbmZpZyA9IG1hbmFnZUF2YnlMZWRnZXJVc2VyVGFibGVDb25maWc7XHJcblx0XHRcdFx0fWVsc2UgaWYgKHJlc3BvbnNlLmRhdGFbMF0udmVuZG9ybmFtZSkge1xyXG5cdFx0XHRcdFx0dGhpcy5pc1ZlbmRvclRhYmxlID0gdHJ1ZTtcclxuXHRcdFx0XHRcdHRoaXMudGFibGVDb25maWcgPSBtYW5hZ2VBdmJ5VmVuZG9yVXNlclRhYmxlQ29uZmlnO1xyXG5cdFx0XHRcdH1lbHNlIHtcclxuXHRcdFx0XHRcdHRoaXMuaXNJbnZvaWNlVGFibGUgPSB0cnVlO1xyXG5cdFx0XHRcdFx0dGhpcy50YWJsZUNvbmZpZyA9IG1hbmFnZUF2YnlJbnZvaWNlVXNlclRhYmxlQ29uZmlnO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHQvL3RoaXMuZGlzcGxheWVkQ29sdW1ucyA9IE9iamVjdC5rZXlzKHJlc3BvbnNlLmRhdGFbMF0pO1xyXG5cdFx0XHR0aGlzLmxvYWRUYWJsZUNvbHVtbigpO1xyXG5cdFx0XHR0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlLmRhdGEpO1xyXG5cdFx0XHR0aGlzLl9kYXRhID0gcmVzcG9uc2UuZGF0YTtcclxuXHRcdFx0dGhpcy5sZW5ndGggPSByZXNwb25zZS5kYXRhLmxlbmd0aDtcclxuXHRcdFx0dGhpcy5kYXRhU291cmNlLnBhZ2luYXRvciA9IHRoaXMucGFnaW5hdG9yO1xyXG5cdFx0XHRjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+IHJlc3BvbnNlLmRhdGEgXCIscmVzcG9uc2UuZGF0YSk7XHJcblx0XHRcdHRoaXMuYXBwbHlUYWJsZVN0YXRlKHRoaXMuc3RhdGUpO1xyXG5cdFx0fSk7XHJcblxyXG5cdFx0Lyp0aGlzLnNldHRpbmdzID0ge1xyXG5cdFx0XHRjb2x1bW5zOiB7XHJcblx0XHRcdCAgaWQ6IHtcclxuXHRcdFx0XHR0aXRsZTogJ0lEJ1xyXG5cdFx0XHQgIH0sXHJcblx0XHRcdCAgbmFtZToge1xyXG5cdFx0XHRcdHRpdGxlOiAnRnVsbCBOYW1lJ1xyXG5cdFx0XHQgIH0sXHJcblx0XHRcdCAgdXNlcm5hbWU6IHtcclxuXHRcdFx0XHR0aXRsZTogJ1VzZXIgTmFtZSdcclxuXHRcdFx0ICB9LFxyXG5cdFx0XHQgIGVtYWlsOiB7XHJcblx0XHRcdFx0dGl0bGU6ICdFbWFpbCdcclxuXHRcdFx0ICB9XHJcblx0XHRcdH1cclxuXHRcdCAgfTtcclxuXHJcblx0XHQgIHRoaXMuZGF0YSA9IFtcclxuXHRcdFx0e1xyXG5cdFx0XHQgIGlkOiAxLFxyXG5cdFx0XHQgIG5hbWU6IFwiTGVhbm5lIEdyYWhhbVwiLFxyXG5cdFx0XHQgIHVzZXJuYW1lOiBcIkJyZXRcIixcclxuXHRcdFx0ICBlbWFpbDogXCJTaW5jZXJlQGFwcmlsLmJpelwiXHJcblx0XHRcdH0sXHJcblx0XHRcdHtcclxuXHRcdFx0ICBpZDogMixcclxuXHRcdFx0ICBuYW1lOiBcIkVydmluIEhvd2VsbFwiLFxyXG5cdFx0XHQgIHVzZXJuYW1lOiBcIkFudG9uZXR0ZVwiLFxyXG5cdFx0XHQgIGVtYWlsOiBcIlNoYW5uYUBtZWxpc3NhLnR2XCJcclxuXHRcdFx0fSxcclxuXHRcdFx0XHJcblx0XHRcdC8vIC4uLiBsaXN0IG9mIGl0ZW1zXHJcblx0XHRcdFxyXG5cdFx0XHR7XHJcblx0XHRcdCAgaWQ6IDExLFxyXG5cdFx0XHQgIG5hbWU6IFwiTmljaG9sYXMgRHVCdXF1ZVwiLFxyXG5cdFx0XHQgIHVzZXJuYW1lOiBcIk5pY2hvbGFzLlN0YW50b25cIixcclxuXHRcdFx0ICBlbWFpbDogXCJSZXkuUGFkYmVyZ0Byb3NhbW9uZC5iaXpcIlxyXG5cdFx0XHR9XHJcblx0XHQgIF07Ki9cclxuXHRcdC8vIERvIHNvbWV0aGluZyB3aXRoIHRoZSBub3RpZmljYXRpb24gKGV2dCkgc2VudCBieSB0aGUgY2hpbGQhXHJcblx0XHRjb25zb2xlLmxvZyh0aGlzLnZpZXdEYXRhKVxyXG59XHJcblxyXG52aWV3RGF0YSA9IHsgXHJcbiAgICBlbXBsb3llZXMgOiBbXSBcclxufTtcclxuQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG5cdEBWaWV3Q2hpbGQoJ2RyYXdlcicpIGRyYXdlcjogTWF0U2lkZW5hdjtcclxuXHJcblx0IFxyXG5cdCAgY29uc3RydWN0b3IocHJpdmF0ZSBfcmVwb3J0TWFuYWdlbWVudFNlcnZpY2U6IFJlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLFxyXG5cdFx0cHJpdmF0ZSBhY3RpdmVSb3V0ZTpBY3RpdmF0ZWRSb3V0ZSxcclxuXHRcdHByaXZhdGUgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsIHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uLFxyXG5cdHByaXZhdGUgbWVzc2FnZVNlcnZpY2UgOiBNZXNzYWdlU2VydmljZSxwcml2YXRlIHJvdXRlcjogUm91dGVyXHJcblx0XHJcbiApIHsgXHJcblx0IGNvbnNvbGUubG9nKFwiYXYgZGV0YWlsIFVSTCBQYXJtcyAqKioqKioqKioqKioqKlwiLHRoaXMuYWN0aXZlUm91dGUuc25hcHNob3QucGFyYW1zKVxyXG5cdFxyXG4gIH1cclxuICBcclxuICAgbmdPbkluaXQoKSB7XHJcbiAgICAgdGhpcy5zdGF0ZT17XHJcbiAgICAgIHNraXA6IHRoaXMub2Zmc2V0LFxyXG4gICAgICB0YWtlOiB0aGlzLmxpbWl0XHJcbiAgICB9XHJcbiAgICB0aGlzLmdldGRhdGFzb3VyY2UoKTtcclxuXHRcdHRoaXMuZ2V0Tm90aWZpY2F0aW9uKCk7XHJcblx0XHRcclxuXHJcblx0fVxyXG5cdGRhdGFTdGF0ZUNoYW5nZShzdGF0ZTogRGF0YVN0YXRlQ2hhbmdlRXZlbnQpOiB2b2lkIHtcclxuXHRcdHRoaXMuc3RhdGU9IHN0YXRlO1xyXG5cdFx0dGhpcy5hcHBseVRhYmxlU3RhdGUodGhpcy5zdGF0ZSk7XHJcblx0ICB9XHJcblx0YXBwbHlUYWJsZVN0YXRlKHN0YXRlOiBTdGF0ZSk6IHZvaWQge1xyXG5cdFx0dGhpcy5rZW5kb0dyaWREYXRhID0gcHJvY2Vzcyh0aGlzLl9kYXRhLCBzdGF0ZSk7XHJcblx0ICB9XHJcblx0b25DbGljayhhY3Rpb24pe1xyXG5cdFx0aWYoYWN0aW9uPT0ncmVmcmVzaCcpXHJcblx0XHR7XHJcblx0XHQgIHRoaXMubmdPbkluaXQoKTtcclxuXHRcdH1cclxuXHQgIH0gIFxyXG5cdGNoYW5nZVBhZ2UoZXZlbnQpIHtcclxuXHRcdGNvbnNvbGUubG9nKGV2ZW50LCBcIj4+PiBFVkVOVCBDaGFuZ2UgUGFnZVwiKVxyXG5cdFx0dGhpcy5xdWVyeVBhcmFtc1tcIm9mZnNldFwiXSA9IGV2ZW50LnNraXA7XHJcblx0XHR0aGlzLnF1ZXJ5UGFyYW1zW1wibGltaXRcIl0gPSB0aGlzLmxpbWl0O1xyXG5cdFx0dGhpcy5vZmZzZXQgPSBldmVudC5za2lwO1xyXG5cdCAgfVxyXG5cdCAgZW5hYmxlRmlsdGVyT3B0aW9ucygpIHtcclxuXHRcdHRoaXMuZW5hYmxlRmlsdGVyID0gIXRoaXMuZW5hYmxlRmlsdGVyOyAgIFxyXG5cdCAgIH1cclxuXHRhcHBseUZpbHRlcihmaWx0ZXJWYWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyID0gZmlsdGVyVmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCk7XHJcblx0fVxyXG5cdFxyXG4gIGJhY2tUb0FWRGV0YWlsKCkge1xyXG5cdHRoaXMubG9jYXRpb24uYmFjaygpO1xyXG5cdGxldCBvYmogPSB7XHJcblx0XHRpZCA6IFwibWFuYWdlQXZEZXRhaWxcIlxyXG5cdH07XHJcblx0bGV0IHNlbGYgPSB0aGlzO1xyXG5cdHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuXHRcdHNlbGYubWVzc2FnZVNlcnZpY2Uuc2VuZFJvdXRpbmcob2JqKTtcclxuXHR9LDEwMClcclxuICB9XHJcblxyXG5cdGJhY2tUb0FWKCkge1xyXG5cdFx0bGV0IG9iaiA9IHtcclxuXHRcdFx0aWQgOiBcIm1hbmFnZUF2bVwiXHJcblx0XHR9O1xyXG5cdFx0dGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG5cdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGUoWydyZXBvcnQtbWFuYWdlbWVudC9tYW5hZ2UtYXYnXSk7XHJcblx0fVxyXG5cdFxyXG4gIGdldGRhdGFzb3VyY2UoKSB7XHJcblx0XHR0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRBbGxEYXRhc291cmNlcygpLnN1YnNjcmliZShyZXMgPT4ge1xyXG5cdFx0XHR0aGlzLmN1c3RvbWRhdGFzb3VyY2UgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXM7XHJcblx0XHRcdHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlc1swXS5faWQ7XHJcblx0XHRcdHRoaXMub25Mb2FkbWFuYWdlQXZzKHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCk7XHJcblx0XHR9KVxyXG5cdH1cclxuXHJcbiAgZ2V0RGF0YXNvdXJjZUlkKGlkKSB7XHJcblx0XHR0aGlzLm9uTG9hZG1hbmFnZUF2cyhpZCk7XHJcblx0fVxyXG5cdHVwZGF0ZVRhYmxlKGV2ZW50KSB7XHJcblx0XHRjb25zb2xlLmxvZyhldmVudCwgXCItLS0tZXZlbnRcIilcclxuXHRcdGxldCBzZWxlY3RlZEhlYWRlciA9IHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlcjtcclxuICAgICAgICB0aGlzLnRhYmxlQ29uZmlnLmZvckVhY2goZnVuY3Rpb24oaXRlbSl7XHJcbiAgICAgICAgICAgIGlmKHNlbGVjdGVkSGVhZGVyLmluZGV4T2YoaXRlbS52YWx1ZSkgPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgaXRlbS5pc2FjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgaXRlbS5pc2FjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuXHRcdC8vIHZhciBpbmRleCA9IHRoaXMudGFibGVOYW1lc2VhcmNoKGV2ZW50LnNvdXJjZS52YWx1ZSwgdGhpcy50YWJsZUNvbmZpZylcclxuXHRcdC8vIGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHQvLyBcdGlmIChldmVudC5jaGVja2VkKSB7XHJcblx0XHQvLyBcdFx0bGV0IGFjdGl2ZSA9IHtcclxuXHRcdC8vIFx0XHRcdHZhbHVlOiBldmVudC5zb3VyY2UudmFsdWUsXHJcblx0XHQvLyBcdFx0XHRuYW1lOiBldmVudC5zb3VyY2UubmFtZSxcclxuXHRcdC8vIFx0XHRcdGlzYWN0aXZlOiB0cnVlXHJcblx0XHQvLyBcdFx0fVxyXG5cdFx0Ly8gXHRcdGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHQvLyBcdFx0XHR0aGlzLnRhYmxlQ29uZmlnLnNwbGljZShpbmRleCwgMSwgYWN0aXZlKTtcclxuXHRcdC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdjb250cm9sJyk7XHJcblx0XHQvLyBcdFx0XHRsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY29udHJvbCcsIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVDb25maWcpKTtcclxuXHRcdC8vIFx0XHR9XHJcblx0XHQvLyBcdH0gZWxzZSB7XHJcblx0XHQvLyBcdFx0bGV0IGluYWN0aXZlID0ge1xyXG5cdFx0Ly8gXHRcdFx0dmFsdWU6IGV2ZW50LnNvdXJjZS52YWx1ZSxcclxuXHRcdC8vIFx0XHRcdG5hbWU6IGV2ZW50LnNvdXJjZS5uYW1lLFxyXG5cdFx0Ly8gXHRcdFx0aXNhY3RpdmU6IGZhbHNlXHJcblx0XHQvLyBcdFx0fVxyXG5cdFx0Ly8gXHRcdGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHQvLyBcdFx0XHR0aGlzLnRhYmxlQ29uZmlnLnNwbGljZShpbmRleCwgMSwgaW5hY3RpdmUpO1xyXG5cdFx0Ly8gXHRcdFx0bG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2NvbnRyb2wnKTtcclxuXHRcdC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjb250cm9sJywgSlNPTi5zdHJpbmdpZnkodGhpcy50YWJsZUNvbmZpZykpO1xyXG5cdFx0Ly8gXHRcdH1cclxuXHRcdC8vIFx0fVxyXG5cdFx0Ly8gfVxyXG5cdFx0bG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ21hbmFnZUF2Ynl1c2VyJyk7XHJcblx0XHRsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbWFuYWdlQXZieXVzZXInLCBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlQ29uZmlnKSk7XHJcblx0XHR0aGlzLmxvYWRUYWJsZUNvbHVtbigpO1xyXG5cdH1cclxuXHR0YWJsZU5hbWVzZWFyY2gobmFtZUtleSwgbXlBcnJheSkge1xyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBteUFycmF5Lmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGlmIChteUFycmF5W2ldLnZhbHVlID09PSBuYW1lS2V5KSB7XHJcblx0XHRcdFx0cmV0dXJuIGk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblx0cGFuZWxPcGVuU3RhdGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcblx0dGFibGVTZXR0aW5nKCkge1xyXG5cdFx0dGhpcy5wYW5lbE9wZW5TdGF0ZSA9ICF0aGlzLnBhbmVsT3BlblN0YXRlXHJcblx0fVxyXG5cdGxvYWRUYWJsZUNvbHVtbigpIHtcclxuXHRcdGNvbnNvbGUubG9nKHRoaXMuZGlzcGxheWVkQ29sdW1ucyxcIi4uLi4uLmRpc3BsYXllZENvbHVtbnNcIilcclxuXHRcdHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IFtdO1xyXG5cdFx0dGhpcy50YWJsZUNvbmZpZyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ21hbmFnZUF2Ynl1c2VyJykpO1xyXG5cdFx0aWYgKHRoaXMudGFibGVDb25maWcpIHtcclxuXHRcdFx0Zm9yIChsZXQgY29sdW1ucyBvZiB0aGlzLnRhYmxlQ29uZmlnKSB7XHJcblx0XHRcdFx0aWYgKGNvbHVtbnMuaXNhY3RpdmUpIHtcclxuXHRcdFx0XHRcdC8vIHRoaXMuZGlzcGxheWVkQ29sdW1ucy5wdXNoKGNvbHVtbnMudmFsdWUpO1xyXG5cdFx0XHRcdFx0Ly8gdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcblx0XHRcdFx0XHR0aGlzLmRpc3BsYXllZENvbHVtbnMucHVzaChjb2x1bW5zKTtcclxuXHRcdFx0XHRcdHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlci5wdXNoKGNvbHVtbnMudmFsdWUpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0aWYodGhpcy5pc0xlZGdlclRhYmxlKXtcclxuXHRcdFx0XHR0aGlzLnRhYmxlQ29uZmlnID0gbWFuYWdlQXZieUxlZGdlclVzZXJUYWJsZUNvbmZpZztcclxuXHRcdFx0fWVsc2UgaWYodGhpcy5pc1ZlbmRvclRhYmxlKXtcclxuXHRcdFx0XHR0aGlzLnRhYmxlQ29uZmlnID0gbWFuYWdlQXZieVZlbmRvclVzZXJUYWJsZUNvbmZpZztcclxuXHRcdFx0fWVsc2UgaWYodGhpcy5pc0ludm9pY2VUYWJsZSkge1xyXG5cdFx0XHRcdHRoaXMudGFibGVDb25maWcgPSBtYW5hZ2VBdmJ5SW52b2ljZVVzZXJUYWJsZUNvbmZpZztcclxuXHRcdFx0fVxyXG5cdFx0XHRjb25zb2xlLmxvZyh0aGlzLnRhYmxlQ29uZmlnLFwiLi4uLi4uLi4uLi50YWJsZWNvbmZpZ1wiKVxyXG5cdFx0XHRmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuXHRcdFx0XHRpZiAoY29sdW1ucy5pc2FjdGl2ZSkge1xyXG5cdFx0XHRcdFx0Ly8gdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcblx0XHRcdFx0XHQvLyB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuXHRcdFx0XHRcdHRoaXMuZGlzcGxheWVkQ29sdW1ucy5wdXNoKGNvbHVtbnMpO1xyXG5cdFx0XHRcdFx0dGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcblxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRjb25zb2xlLmxvZyh0aGlzLmRpc3BsYXllZENvbHVtbnMsXCIuLi4udGhpcy5kaXNwbGF5ZWRDb2x1bW5zXCIpXHJcblx0XHRcdGNvbnNvbGUubG9nKFwiPj4+PiBzZWxlY3RlZFRhYmxlSGVhZGVyIFwiLHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlcik7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuICBvbkxvYWRtYW5hZ2VBdnMoaWQpIHtcclxuXHRcdHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCA9IGlkXHJcblx0XHRcclxuXHR9XHJcblxyXG5cdGV4cG9ydEF2bVVzZXJkZXRhaWwoKXtcclxuICAgICAgICBjb25zb2xlLmxvZygnZGF0YSBzb3VyY2UgaWQnLHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5kYXRhc291cmNlSWQpXHJcbiAgICAgICAgdmFyIHZhbHVlID0gMTtcclxuICAgICAgICB0aGlzLmF2bUV4cG9ydE9iaj17fTtcclxuICAgICAgICB0aGlzLmF2bUV4cG9ydE9ialsnZGF0YXNvdXJjZSddID0gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmRhdGFzb3VyY2VJZDtcclxuICAgICAgICB0aGlzLmF2bUV4cG9ydE9ialsncmVwb3J0X3R5cGUnXSA9ICdDU1YnO1xyXG4gICAgICAgIHRoaXMuYXZtRXhwb3J0T2JqWydjb2xsZWN0aW9uRGV0YWlsJ109IHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5jb2xsZWN0aW9uRGV0YWlsO1xyXG4gICAgICAgIHRoaXMuYXZtRXhwb3J0T2JqWydjcmVhdGVkYnknXSA9IHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5jcmVhdGVkQnk7XHJcbiAgICAgICAgdGhpcy5hdm1FeHBvcnRPYmpbJ2pvdXJuYWxuYW1lJ10gPSB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUuam91cm5hbG5hbWU7XHJcbiAgICAgICAgXHJcbiAgICAgICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZXhwb3J0QVZNVVNlcmRldGFpbHModGhpcy5hdm1FeHBvcnRPYmopLnN1YnNjcmliZShyZXNwID0+IHtcclxuXHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBibG9iID0gbmV3IEJsb2IoW3Jlc3AuYm9keV0sIHsgdHlwZTogJ3RleHQvY3N2JyB9KTtcclxuICAgICAgICAgICAgICAgIEZpbGVTYXZlci5zYXZlQXMoYmxvYiwgXCJBY2Nlc3NWaW9sYXRpb25CeVVzZXIuY3N2XCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtyZXNwLmJvZHldLCB7IHR5cGU6ICd0ZXh0L3hsc3gnIH0pO1xyXG4gICAgICAgICAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhibG9iLCBcIlVzZXJDb25mbGljdC54bHN4XCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImVycm9yOjo6XCIgKyBKU09OLnN0cmluZ2lmeShlcnJvcikpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICB9XHJcblxyXG5cclxuICB9XHJcbiJdfQ==