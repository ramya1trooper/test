import { Component, Inject } from "@angular/core";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import { FormBuilder, FormControl, Validators } from "@angular/forms";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { MessageService } from "../_services/message.service";
var ModelLayoutComponent = /** @class */ (function () {
    // modalData: any; 
    // modalIndex: any; 
    // currentModelData: any;
    function ModelLayoutComponent(_fuseTranslationLoaderService, matDialogRef, data, _formBuilder, messageService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.matDialogRef = matDialogRef;
        this.data = data;
        this._formBuilder = _formBuilder;
        this.messageService = messageService;
        this.english = english;
        this.modelData = {};
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    ModelLayoutComponent.prototype.ngOnInit = function () {
        console.log(this.data, ".......current open model");
        if (this.data && this.data.modelData) {
            this.modelData = this.data.modelData;
        }
        else {
            this.currentConfigData = JSON.parse(localStorage.getItem('currentConfigData'));
            this.modelData = this.currentConfigData[this.data.action] ? this.currentConfigData[this.data.action].modelData : null;
        }
        if (this.modelData.dynamicHeader) {
            var selectedData = this.data.savedData;
            this.modelData.header = selectedData[this.modelData.headerId];
            selectedData.ReportName = selectedData[this.modelData.headerId];
            localStorage.setItem("CurrentReportData", JSON.stringify(selectedData));
        }
        console.log(this.modelData, "...modelData");
        // this.currentModelData = this.currentConfigData["listView"].createModelData;
        // this.modalIndex = this.data.modalIndex; 
        // if(this.modalIndex)
        //     this.getModalData(this.modalIndex);
        this.formGroup = this._formBuilder.group({
            form: this._formBuilder.array([this.init()])
        });
    };
    // getModalData(modalIndex) { 
    //     let FormData = Object.values(this.currentModelData) 
    //     for (let id = 0; id < FormData.length; id++) { 
    //         if (modalIndex === FormData[id]["id"]){ 
    //             return  this.modalData = FormData[id]; 
    //         }            
    //     } 
    // } 
    ModelLayoutComponent.prototype.init = function () {
        return this._formBuilder.group({
            cont: new FormControl('', [Validators.required]),
        });
    };
    ModelLayoutComponent.prototype.selectionChange = function (event) {
        this.messageService.sendClickEvent(event.previouslySelectedIndex);
    };
    ModelLayoutComponent.prototype.closeModel = function () {
        localStorage.removeItem("currentInput");
        this.matDialogRef.close();
        this.messageService.sendModelCloseEvent("listView");
    };
    ModelLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "model-layout",
                    template: "<!-- Work by Ramya -->\r\n<div class=\"dialog-content-wrapper mat-dialog-container\" [ngClass]=\"modelData.accesgroupHeight\">\r\n  <div class=\"header-top  ctrl-create header p-12 ui-common-lib-popupheader\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n    fxlayoutalign=\"space-between center\">\r\n    <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n      <h2 class=\"m-0 font-weight-900\">{{modelData.header | translate}}</h2>\r\n    </div>\r\n    <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n      <button mat-icon-button (click)=\"closeModel()\" aria-label=\"Close Dialog\" class=\"ui-common-lib-outline\"\r\n        style=\"float:right;margin-right:12px;\">\r\n        <mat-icon>close</mat-icon>\r\n      </button>\r\n    </div>\r\n  </div>\r\n  <div mat-dialog-content [ngClass]=\"modelData.modelContentView\" class=\"p-24 pb-0 m-0\"\r\n    *ngIf=\"modelData && modelData.stepperData\" fusePerfectScrollbar>\r\n    <form [formGroup]=\"formGroup\">\r\n      <mat-horizontal-stepper #stepper (selectionChange)=\"selectionChange($event)\" [linear]=true formArrayName=\"form\">\r\n        <mat-step *ngFor=\"let stepperValue of modelData.stepperData; let index = index;\">\r\n          <ng-template matStepLabel>{{stepperValue.stepper_header | translate}}</ng-template>\r\n\r\n          <form-layout [onLoadData]=\"data\" [stepperVal]=\"stepper\" [formValues]=\"stepperValue\"></form-layout>\r\n        </mat-step>\r\n      </mat-horizontal-stepper>\r\n    </form>\r\n  </div>\r\n  <div mat-dialog-content [ngClass]=\"modelData.modelContentView\" class=\"p-24 pb-0 m-0\"\r\n    *ngIf=\"modelData && modelData.formData\" fusePerfectScrollbar>\r\n    <div *ngFor=\"let formField of modelData.formData\">\r\n      <form-layout [onLoadData]=\"data\" [importData]=\"formField.importData\" [formValues]=\"formField\"></form-layout>\r\n    </div>\r\n  </div>\r\n\r\n</div>\r\n",
                    styles: ["::ng-deep .mat-horizontal-stepper-header{pointer-events:none!important}.ui-common-lib-outline{outline:0!important}.ui-common-lib-popupheader{padding:5px 0 5px 12px!important;background-color:#223664!important;color:#fff!important}.ui-common-lib-popupheader h2{font-size:17px!important;font-weight:500}.sen-lib-access-group-100vh{height:100vh}.sen-lib-content-view{max-height:85vh!important}.sen-lib-access-group-70vh{height:70vh!important}"]
                }] }
    ];
    /** @nocollapse */
    ModelLayoutComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
        { type: FormBuilder },
        { type: MessageService },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    return ModelLayoutComponent;
}());
export { ModelLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJtb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUc3RCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFDbEQsT0FBTyxFQUNMLFdBQVcsRUFDWCxXQUFXLEVBRVgsVUFBVSxFQUVYLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEIsT0FBTyxFQUNMLFlBQVksRUFHWixlQUFlLEVBR2hCLE1BQU0sbUJBQW1CLENBQUM7QUFHM0IsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBRTlEO0lBV0ksbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQix5QkFBeUI7SUFDM0IsOEJBQ1UsNkJBQTJELEVBQzVELFlBQWdELEVBQ3ZCLElBQVMsRUFDakMsWUFBeUIsRUFDekIsY0FBOEIsRUFDWCxPQUFPO1FBTDFCLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBOEI7UUFDNUQsaUJBQVksR0FBWixZQUFZLENBQW9DO1FBQ3ZCLFNBQUksR0FBSixJQUFJLENBQUs7UUFDakMsaUJBQVksR0FBWixZQUFZLENBQWE7UUFDekIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQ1gsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQVhsQyxjQUFTLEdBQVMsRUFBRSxDQUFDO1FBYXJCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBQ0QsdUNBQVEsR0FBUjtRQUNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksRUFBQywyQkFBMkIsQ0FBQyxDQUFBO1FBQ2xELElBQUcsSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBQztZQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3hDO2FBQUk7WUFDRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztTQUN6SDtRQUNELElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLEVBQUU7WUFDL0IsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7WUFFOUQsWUFBWSxDQUFDLFVBQVUsR0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUM5RCxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztTQUV4RTtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBQyxjQUFjLENBQUMsQ0FBQTtRQUMxQyw4RUFBOEU7UUFDOUUsMkNBQTJDO1FBQzNDLHNCQUFzQjtRQUN0QiwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQztZQUNyQyxJQUFJLEVBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztTQUNoRCxDQUFDLENBQUE7SUFDTixDQUFDO0lBRUQsOEJBQThCO0lBQzlCLDJEQUEyRDtJQUMzRCxzREFBc0Q7SUFDdEQsbURBQW1EO0lBQ25ELHNEQUFzRDtJQUN0RCx3QkFBd0I7SUFDeEIsU0FBUztJQUNULEtBQUs7SUFFSCxtQ0FBSSxHQUFKO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQztZQUM3QixJQUFJLEVBQUUsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2pELENBQUMsQ0FBQTtJQUNOLENBQUM7SUFDRCw4Q0FBZSxHQUFmLFVBQWdCLEtBQUs7UUFDakIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUNELHlDQUFVLEdBQVY7UUFDRSxZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0RCxDQUFDOztnQkF2RUYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4QiwrNURBQTRDOztpQkFFN0M7Ozs7Z0JBekJRLDRCQUE0QjtnQkFVbkMsWUFBWTtnREE0QlQsTUFBTSxTQUFDLGVBQWU7Z0JBbkN6QixXQUFXO2dCQWdCSixjQUFjO2dEQXNCbEIsTUFBTSxTQUFDLFNBQVM7O0lBb0RyQiwyQkFBQztDQUFBLEFBeEVELElBd0VDO1NBbkVZLG9CQUFvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5qZWN0LCBWaWV3Q2hpbGQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbFwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcbmltcG9ydCB7XHJcbiAgRm9ybUJ1aWxkZXIsXHJcbiAgRm9ybUNvbnRyb2wsXHJcbiAgRm9ybUdyb3VwLFxyXG4gIFZhbGlkYXRvcnMsXHJcbiAgRm9ybUFycmF5XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7XHJcbiAgTWF0RGlhbG9nUmVmLFxyXG4gIE1hdENoaXBJbnB1dEV2ZW50LFxyXG4gIE1hdEF1dG9jb21wbGV0ZSxcclxuICBNQVRfRElBTE9HX0RBVEEsXHJcbiAgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCxcclxuICBWRVJTSU9OXHJcbn0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsXCI7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSBcIkBhbmd1bGFyL2Nkay9jb2xsZWN0aW9uc1wiO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gXCJsb2Rhc2hcIjtcclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi4vX3NlcnZpY2VzL21lc3NhZ2Uuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwibW9kZWwtbGF5b3V0XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9tb2RlbC1sYXlvdXQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vbW9kZWwtbGF5b3V0LmNvbXBvbmVudC5zY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNb2RlbExheW91dENvbXBvbmVudCB7XHJcbiAgZm9ybUdyb3VwIDogRm9ybUdyb3VwO1xyXG4gICAgZm9ybTogRm9ybUFycmF5O1xyXG4gICAgY3VycmVudENvbmZpZ0RhdGEgOiBhbnk7XHJcbiAgICBtb2RlbERhdGEgOiBhbnkgPSB7fTtcclxuICAgIHN1Ym1pdHRlZCA6IGJvb2xlYW47XHJcbiAgICAvLyBtb2RhbERhdGE6IGFueTsgXHJcbiAgICAvLyBtb2RhbEluZGV4OiBhbnk7IFxyXG4gICAgLy8gY3VycmVudE1vZGVsRGF0YTogYW55O1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZTogRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSxcclxuICAgIHB1YmxpYyBtYXREaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxNb2RlbExheW91dENvbXBvbmVudD4sXHJcbiAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHVibGljIGRhdGE6IGFueSxcclxuICAgIHByaXZhdGUgX2Zvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHJpdmF0ZSBlbmdsaXNoXHJcbiAgKSB7XHJcbiAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmxvYWRUcmFuc2xhdGlvbnMoZW5nbGlzaCk7XHJcbiAgfVxyXG4gIG5nT25Jbml0KCkgeyAgXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmRhdGEsXCIuLi4uLi4uY3VycmVudCBvcGVuIG1vZGVsXCIpXHJcbiAgICBpZih0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLm1vZGVsRGF0YSl7XHJcbiAgICAgICAgdGhpcy5tb2RlbERhdGEgPSB0aGlzLmRhdGEubW9kZWxEYXRhO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRDb25maWdEYXRhJykpO1xyXG4gICAgICAgIHRoaXMubW9kZWxEYXRhID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVt0aGlzLmRhdGEuYWN0aW9uXSA/IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbdGhpcy5kYXRhLmFjdGlvbl0ubW9kZWxEYXRhIDogbnVsbDsgICAgXHJcbiAgICB9XHJcbiAgICBpZih0aGlzLm1vZGVsRGF0YS5keW5hbWljSGVhZGVyKSB7XHJcbiAgICAgIGxldCBzZWxlY3RlZERhdGEgPSB0aGlzLmRhdGEuc2F2ZWREYXRhO1xyXG4gICAgICB0aGlzLm1vZGVsRGF0YS5oZWFkZXIgPSBzZWxlY3RlZERhdGFbdGhpcy5tb2RlbERhdGEuaGVhZGVySWRdO1xyXG4gICAgICBcclxuICAgICAgc2VsZWN0ZWREYXRhLlJlcG9ydE5hbWU9c2VsZWN0ZWREYXRhW3RoaXMubW9kZWxEYXRhLmhlYWRlcklkXTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJDdXJyZW50UmVwb3J0RGF0YVwiLEpTT04uc3RyaW5naWZ5KHNlbGVjdGVkRGF0YSkpO1xyXG5cclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKHRoaXMubW9kZWxEYXRhLFwiLi4ubW9kZWxEYXRhXCIpXHJcbiAgICAvLyB0aGlzLmN1cnJlbnRNb2RlbERhdGEgPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl0uY3JlYXRlTW9kZWxEYXRhO1xyXG4gICAgLy8gdGhpcy5tb2RhbEluZGV4ID0gdGhpcy5kYXRhLm1vZGFsSW5kZXg7IFxyXG4gICAgLy8gaWYodGhpcy5tb2RhbEluZGV4KVxyXG4gICAgLy8gICAgIHRoaXMuZ2V0TW9kYWxEYXRhKHRoaXMubW9kYWxJbmRleCk7XHJcbiAgICB0aGlzLmZvcm1Hcm91cCA9IHRoaXMuX2Zvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgICBmb3JtIDogdGhpcy5fZm9ybUJ1aWxkZXIuYXJyYXkoW3RoaXMuaW5pdCgpXSlcclxuICAgIH0pXHJcbn1cclxuXHJcbi8vIGdldE1vZGFsRGF0YShtb2RhbEluZGV4KSB7IFxyXG4vLyAgICAgbGV0IEZvcm1EYXRhID0gT2JqZWN0LnZhbHVlcyh0aGlzLmN1cnJlbnRNb2RlbERhdGEpIFxyXG4vLyAgICAgZm9yIChsZXQgaWQgPSAwOyBpZCA8IEZvcm1EYXRhLmxlbmd0aDsgaWQrKykgeyBcclxuLy8gICAgICAgICBpZiAobW9kYWxJbmRleCA9PT0gRm9ybURhdGFbaWRdW1wiaWRcIl0peyBcclxuLy8gICAgICAgICAgICAgcmV0dXJuICB0aGlzLm1vZGFsRGF0YSA9IEZvcm1EYXRhW2lkXTsgXHJcbi8vICAgICAgICAgfSAgICAgICAgICAgIFxyXG4vLyAgICAgfSBcclxuLy8gfSBcclxuXHJcbiAgaW5pdCgpe1xyXG4gICAgICByZXR1cm4gdGhpcy5fZm9ybUJ1aWxkZXIuZ3JvdXAoe1xyXG4gICAgICAgIGNvbnQgOm5ldyBGb3JtQ29udHJvbCgnJywgW1ZhbGlkYXRvcnMucmVxdWlyZWRdKSxcclxuICAgICAgfSlcclxuICB9XHJcbiAgc2VsZWN0aW9uQ2hhbmdlKGV2ZW50KXtcclxuICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kQ2xpY2tFdmVudChldmVudC5wcmV2aW91c2x5U2VsZWN0ZWRJbmRleCk7XHJcbiAgfVxyXG4gIGNsb3NlTW9kZWwoKSB7XHJcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICB9XHJcbn1cclxuXHJcbiJdfQ==