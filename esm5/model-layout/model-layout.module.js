import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { FormLayoutModule } from "../form-layout/form-layout.module";
import { ButtonLayoutModule } from "../button-layout/button-layout.module";
var ModelLayoutModule = /** @class */ (function () {
    function ModelLayoutModule() {
    }
    ModelLayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ModelLayoutComponent],
                    imports: [
                        RouterModule,
                        FormLayoutModule,
                        ButtonLayoutModule,
                        // Material
                        MaterialModule,
                        TranslateModule,
                        FuseSharedModule
                    ],
                    exports: [ModelLayoutComponent]
                },] }
    ];
    return ModelLayoutModule;
}());
export { ModelLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwtbGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJtb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzNFO0lBQUE7SUFjZ0MsQ0FBQzs7Z0JBZGhDLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztvQkFDcEMsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osZ0JBQWdCO3dCQUNoQixrQkFBa0I7d0JBQ2xCLFdBQVc7d0JBQ1gsY0FBYzt3QkFDZCxlQUFlO3dCQUVmLGdCQUFnQjtxQkFDakI7b0JBQ0QsT0FBTyxFQUFFLENBQUMsb0JBQW9CLENBQUM7aUJBQ2hDOztJQUMrQix3QkFBQztDQUFBLEFBZGpDLElBY2lDO1NBQXBCLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBNb2RlbExheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi9tb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBGb3JtTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2Zvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBCdXR0b25MYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vYnV0dG9uLWxheW91dC9idXR0b24tbGF5b3V0Lm1vZHVsZVwiO1xyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW01vZGVsTGF5b3V0Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICBGb3JtTGF5b3V0TW9kdWxlLFxyXG4gICAgQnV0dG9uTGF5b3V0TW9kdWxlLFxyXG4gICAgLy8gTWF0ZXJpYWxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG5cclxuICAgIEZ1c2VTaGFyZWRNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtNb2RlbExheW91dENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIE1vZGVsTGF5b3V0TW9kdWxlIHt9XHJcbiJdfQ==