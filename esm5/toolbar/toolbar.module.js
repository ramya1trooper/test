import { AccountService } from "./../shared/auth/account.service";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSearchBarModule } from "../@fuse/components/search-bar/search-bar.module";
import { FuseShortcutsModule } from "../@fuse/components/shortcuts/shortcuts.module";
import { FuseSharedModule } from "../@fuse/shared.module";
import { MaterialModule } from "../material.module";
import { ToolbarComponent } from "./toolbar.component";
import { SessionModule } from "../session-expiration/session-expiration.module";
import { AuthModelDialog } from '../session-expiration/session-expiration-alert.component';
var ToolbarModule = /** @class */ (function () {
    function ToolbarModule() {
    }
    ToolbarModule.forRoot = function (metaData, english) {
        return {
            ngModule: ToolbarModule,
            providers: [
                AccountService,
                {
                    provide: "metaData",
                    useValue: metaData
                },
                {
                    provide: "english",
                    useValue: english
                }
            ]
        };
    };
    ToolbarModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ToolbarComponent],
                    imports: [
                        RouterModule,
                        MaterialModule,
                        FuseSharedModule,
                        FuseSearchBarModule,
                        FuseShortcutsModule,
                        SessionModule
                    ],
                    exports: [ToolbarComponent],
                    entryComponents: [AuthModelDialog],
                    schemas: [CUSTOM_ELEMENTS_SCHEMA]
                },] }
    ];
    return ToolbarModule;
}());
export { ToolbarModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbGJhci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsidG9vbGJhci90b29sYmFyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDbEUsT0FBTyxFQUFFLFFBQVEsRUFBdUIsc0JBQXNCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzFELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN2RCxPQUFPLEVBQUMsYUFBYSxFQUFFLE1BQU0saURBQWlELENBQUE7QUFFOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBRTNGO0lBQUE7SUFnQ0EsQ0FBQztJQWhCUSxxQkFBTyxHQUFkLFVBQWUsUUFBUSxFQUFFLE9BQU87UUFDOUIsT0FBTztZQUNMLFFBQVEsRUFBRSxhQUFhO1lBQ3ZCLFNBQVMsRUFBRTtnQkFDVCxjQUFjO2dCQUNkO29CQUNFLE9BQU8sRUFBRSxVQUFVO29CQUNuQixRQUFRLEVBQUUsUUFBUTtpQkFDbkI7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLFNBQVM7b0JBQ2xCLFFBQVEsRUFBRSxPQUFPO2lCQUNsQjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7O2dCQS9CRixRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsZ0JBQWdCLENBQUM7b0JBQ2hDLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLGNBQWM7d0JBRWQsZ0JBQWdCO3dCQUNoQixtQkFBbUI7d0JBQ25CLG1CQUFtQjt3QkFDbkIsYUFBYTtxQkFDZDtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxnQkFBZ0IsQ0FBQztvQkFDM0IsZUFBZSxFQUFDLENBQUUsZUFBZSxDQUFDO29CQUNsQyxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztpQkFDbEM7O0lBa0JELG9CQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7U0FqQlksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSBcIi4vLi4vc2hhcmVkL2F1dGgvYWNjb3VudC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzLCBDVVNUT01fRUxFTUVOVFNfU0NIRU1BIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVNlYXJjaEJhck1vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9jb21wb25lbnRzL3NlYXJjaC1iYXIvc2VhcmNoLWJhci5tb2R1bGVcIjtcclxuaW1wb3J0IHsgRnVzZVNob3J0Y3V0c01vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9jb21wb25lbnRzL3Nob3J0Y3V0cy9zaG9ydGN1dHMubW9kdWxlXCI7XHJcbmltcG9ydCB7IEZ1c2VTaGFyZWRNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2Uvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gXCIuLi9tYXRlcmlhbC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IFRvb2xiYXJDb21wb25lbnQgfSBmcm9tIFwiLi90b29sYmFyLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQge1Nlc3Npb25Nb2R1bGUgfSBmcm9tIFwiLi4vc2Vzc2lvbi1leHBpcmF0aW9uL3Nlc3Npb24tZXhwaXJhdGlvbi5tb2R1bGVcIlxyXG5pbXBvcnQgeyBTZXNzaW9uQ29tcG9uZW50IH0gZnJvbSAnLi4vc2Vzc2lvbi1leHBpcmF0aW9uL3Nlc3Npb24tZXhwaXJhdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBdXRoTW9kZWxEaWFsb2cgfSBmcm9tICcuLi9zZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLWFsZXJ0LmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1Rvb2xiYXJDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG5cclxuICAgIEZ1c2VTaGFyZWRNb2R1bGUsXHJcbiAgICBGdXNlU2VhcmNoQmFyTW9kdWxlLFxyXG4gICAgRnVzZVNob3J0Y3V0c01vZHVsZSxcclxuICAgIFNlc3Npb25Nb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtUb29sYmFyQ29tcG9uZW50XSxcclxuICBlbnRyeUNvbXBvbmVudHM6WyBBdXRoTW9kZWxEaWFsb2ddLFxyXG4gIHNjaGVtYXM6IFtDVVNUT01fRUxFTUVOVFNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVG9vbGJhck1vZHVsZSB7XHJcbiAgc3RhdGljIGZvclJvb3QobWV0YURhdGEsIGVuZ2xpc2gpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgIHJldHVybiB7XHJcbiAgICAgIG5nTW9kdWxlOiBUb29sYmFyTW9kdWxlLFxyXG4gICAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBBY2NvdW50U2VydmljZSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBcIm1ldGFEYXRhXCIsXHJcbiAgICAgICAgICB1c2VWYWx1ZTogbWV0YURhdGFcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IFwiZW5nbGlzaFwiLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IGVuZ2xpc2hcclxuICAgICAgICB9XHJcbiAgICAgIF1cclxuICAgIH07XHJcbiAgfVxyXG59XHJcbiJdfQ==