import * as tslib_1 from "tslib";
import { MessageService } from "./../_services/message.service";
import { Component, ViewEncapsulation, Inject } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as _ from "lodash";
import { FuseConfigService } from "../@fuse/services/config.service";
import { FuseSidebarService } from "../@fuse/components/sidebar/sidebar.service";
import { ContentService } from "../content/content.service";
import { AccountService } from "../shared/auth/account.service";
import { OAuthService } from "angular-oauth2-oidc";
import { CookieService } from 'ngx-cookie-service';
import { LoaderService } from '../loader.service';
import { FuseTranslationLoaderService } from '../@fuse/services/translation-loader.service';
// import * as moment from 'moment';
import * as moment_ from 'moment';
var moment = moment_;
import * as FileSaver from "file-saver";
var ToolbarComponent = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    function ToolbarComponent(_fuseTranslationLoaderService, _fuseConfigService, _fuseSidebarService, _translateService, contentService, accountService, oauthService, messageService, _cookieService, loaderService, _metaData, english) {
        var _this = this;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this._fuseConfigService = _fuseConfigService;
        this._fuseSidebarService = _fuseSidebarService;
        this._translateService = _translateService;
        this.contentService = contentService;
        this.accountService = accountService;
        this.oauthService = oauthService;
        this.messageService = messageService;
        this._cookieService = _cookieService;
        this.loaderService = loaderService;
        this._metaData = _metaData;
        this.english = english;
        this._fuseTranslationLoaderService.loadTranslations(english);
        // Set the defaults
        this.userStatusOptions = [
            {
                title: "Online",
                icon: "icon-checkbox-marked-circle",
                color: "#4CAF50"
            },
            {
                title: "Away",
                icon: "icon-clock",
                color: "#FFC107"
            },
            {
                title: "Do not Disturb",
                icon: "icon-minus-circle",
                color: "#F44336"
            },
            {
                title: "Invisible",
                icon: "icon-checkbox-blank-circle-outline",
                color: "#BDBDBD"
            },
            {
                title: "Offline",
                icon: "icon-checkbox-blank-circle-outline",
                color: "#616161"
            }
        ];
        this.languages = [
            {
                id: "en",
                title: "English",
                flag: "us"
            },
            {
                id: "tr",
                title: "Turkish",
                flag: "tr"
            }
        ];
        this.messageService.getDatasource().subscribe(function (response) {
            if (_this.realm_data) {
                _this.getAllDatasource();
            }
            console.log(" setDatasouce ", response);
            if (response && response.text && response.text != 'null') {
                _this.defaultDatasource = response.text;
                _this.sendMessage(response.text, null);
            }
        });
        this.messageService.getTriggerNotification()
            .subscribe(function (data) {
            console.log(data, ">>>>>>>>>>> DATA");
            if (_this.realm_data) {
                _this.getAllNotification();
            }
        });
        this.navigation = this.getMetaData();
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ToolbarComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function (settings) {
            _this.horizontalNavbar = settings.layout.navbar.position === "top";
            _this.rightNavbar = settings.layout.navbar.position === "right";
            _this.hiddenNavbar = settings.layout.navbar.hidden === true;
        });
        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {
            id: this._translateService.currentLang
        });
        var realmData = this._cookieService.get("realm");
        this.accountService.switchEvent.subscribe(function (data) {
            //get current username
            _this.user = data.username;
            _this.data = data.realm;
            // this.realm_data = data.realm[0]._id;
            console.log(_this.user);
            if (realmData) {
                _this.defaultRealm = _this._cookieService.get("realmName");
                _this.realm_data = realmData;
            }
            else {
                _this.defaultRealm = _this.data[0].name;
                _this.realm_data = _this.data[0]._id;
            }
            localStorage.setItem("realm", _this.realm_data);
            localStorage.setItem("realmName", _this.defaultRealm);
            console.log(">>>> realm_data toolbar ngOnInit ", _this.realm_data);
            _this.getAllDatasource();
            _this.getAllNotification();
        });
        // this.getAllDatasource();
        // this.getAllNotification();
    };
    ToolbarComponent.prototype.getId = function (event) {
        var _this = this;
        console.log("@@@@", event);
        this.accountService.changeRealm(event).subscribe(function (data) {
            console.log("data", data);
            localStorage.removeItem("access_token");
            localStorage.setItem("access_token", data.body.response.access_token);
            localStorage.setItem("realm", event._id);
            localStorage.setItem("realmName", event.name);
            _this._cookieService.set('realm', event._id); // new one
            _this._cookieService.set('realmName', event.name); // new one
            localStorage.removeItem("datasource");
            window.location.reload();
        }, function (error) {
            console.log("err", error);
        });
    };
    ToolbarComponent.prototype.getAllDatasource = function () {
        var _this = this;
        this.loaderService.startLoader();
        var query = {
            realm: this.realm_data
        };
        var apiUrl = "/datasources/all";
        console.log(">>> realm dtaa ", this.realm_data);
        console.log(">>> query ", query);
        this.contentService.getAllReponse(query, apiUrl).subscribe(function (res) {
            _this.loaderService.stopLoader();
            _this.dataSource = res.response.datasources;
            var dataSource = localStorage.getItem("datasource"); // check on Local Storage
            _this.cookieDS = _this._cookieService.get("datasource"); // check on cookie
            // if(dataSource && dataSource != 'null') 
            // {
            //   this.selectedDatasourceId = dataSource;
            //   this.defaultDatasourceName = localStorage.getItem("datasourceName");
            //   this.defaultDatasource = dataSource;
            // } else 
            if (_this.cookieDS && _this.cookieDS != 'null') {
                _this.selectedDatasourceId = _this.cookieDS;
                _this.defaultDatasourceName = _this._cookieService.get("datasourceName");
                _this.defaultDatasource = _this.cookieDS;
                localStorage.setItem("datasource", _this.selectedDatasourceId);
                localStorage.setItem("datasourceName", _this.defaultDatasourceName);
            }
            else {
                var defaultDatasourceFilter = [];
                defaultDatasourceFilter = _.filter(res.response.datasources, { defaultDatasource: true });
                if (defaultDatasourceFilter && defaultDatasourceFilter.length) {
                    _this.selectedDatasourceId = defaultDatasourceFilter[0]._id;
                    _this.defaultDatasourceName = defaultDatasourceFilter[0].name;
                    _this.defaultDatasource = defaultDatasourceFilter[0]._id;
                    localStorage.setItem("datasource", _this.selectedDatasourceId);
                    localStorage.setItem("datasourceName", _this.defaultDatasourceName);
                }
                else {
                    _this.selectedDatasourceId = res.response.datasources[0]._id;
                    _this.defaultDatasourceName = res.response.datasources[0].name;
                    _this.defaultDatasource = res.response.datasources[0]._id;
                    localStorage.setItem("datasource", _this.selectedDatasourceId);
                    localStorage.setItem("datasourceName", _this.defaultDatasourceName);
                }
            }
        });
    };
    ToolbarComponent.prototype.clearallNotification = function (notificationList) {
        var _this = this;
        var apiUrl = "/notification/updateAll/";
        var queryObj = {
            isread: true,
            feature: "isread",
            realm: this.realm_data
        };
        console.log(queryObj, ",,.,.,..,.");
        this.contentService.updateRequest(queryObj, apiUrl, true)
            .subscribe(function (res) {
            _this.enableNotification = false;
            _this.ngOnInit();
        });
    };
    ToolbarComponent.prototype.getAllNotification = function () {
        var _this = this;
        var query = {
            unread: true,
            toolbar: true,
            realm: this.realm_data
        };
        var apiUrl = "/notification";
        this.notificationData = [];
        this.contentService.getAllReponse(query, apiUrl).subscribe(function (res) {
            _this.notificationList = res.response.notificationList;
            var self = _this;
            _.forEach(_this.notificationList, function (listItem) {
                var lastIndex = listItem.data ? listItem.data.length - 1 : 0;
                var item = listItem.data[lastIndex];
                console.log(item, ">>>>>item");
                item.feature = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.FEATURE." + item.messageObj.feature);
                var operationStatus = item.messageObj.action + "_" + item.messageObj.status;
                console.log("Operation Data", operationStatus);
                item.status_action = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION_STATUS." + operationStatus);
                item.msg = item.feature + "   " + item.status_action;
                console.log(item, ">>>>> item");
                item.StatusNew = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.STATUS." + item.messageObj.status);
                item.dateTimeAgo = moment(item.created_at).fromNow();
                item.newstring = item.userName.charAt(0);
                console.log("Convert hours >>>", item.dateTimeAgo);
                item.percentage = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.PROGRESS." + item.messageObj.status);
                item.operationStatus = operationStatus;
                listItem = tslib_1.__assign({}, listItem, item);
                self.notificationData.push(listItem);
                // $(".progress-bar-striped").width(item.percentage);
            });
            _this.count = res.response.total;
        });
    };
    ToolbarComponent.prototype.navigateToDownloads = function (msg) {
        console.log("Message value >>>>>>", msg);
        // this.closeSideNav();
        // this.router.navigate(['/pages/download-report']);
        // tslint:disable-next-line:no-conditional-assignment
        // if (msg.messageObj.feature = 'MANAGE_RULESET') {
        //   let mainMenu = this._metaData.filter(obj => obj["menuName"] == "controlManagement");
        //   console.log(">>> Meta data ", this._metaData);
        //   if(mainMenu.length > 0)
        //    {
        //     let submenus = mainMenu[0].subMenu;
        //     console.log("submenu >>>>>", submenus);
        //     let subMenu = submenus.filter(obj => obj["menuName"] == "ruleset");
        //     let menuItem = subMenu[0];
        //     let tempItem = {
        //       id: menuItem.menuName,
        //       title: menuItem.title,
        //       type: 
        //         menuItem.subMenu
        //           ? "collapsable"
        //           : "item",
        //       url: menuItem.routingUI,
        //       configFilePath: menuItem.configFilePath
        //     };
        //     console.log(">>> menuItem ", tempItem);
        //     this.closeSideNav();
        //     this.messageService.sendRouting(tempItem);
        //   }
        // }
        if (msg.additionalData && msg.additionalData.length) {
            var downloadId = msg.additionalData[0].instanceId;
        }
        else {
            var downloadId = null;
        }
        console.log("Report id >>>>>", downloadId);
        var query = {
            reportId: downloadId
        };
        var apiUrl = '/reports/downloadReport';
        if (downloadId == undefined || downloadId == null || downloadId == '') {
            console.log("Report Id Null");
        }
        else {
            this.contentService
                .getAllReponse(query, apiUrl)
                .subscribe(function (data) {
                var ab = new ArrayBuffer(data.data.length);
                var view = new Uint8Array(ab);
                for (var i = 0; i < data.data.length; i++) {
                    view[i] = data.data[i];
                }
                var downloadType = 'application/zip';
                var file = new Blob([ab], { type: downloadType });
                FileSaver.saveAs(file, msg.additionalData[0].name + '.zip');
            });
        }
    };
    ToolbarComponent.prototype.getMetaData = function () {
        console.log(this._metaData);
        var navigationList = [];
        _.forEach(this._metaData, function (item) {
            var childrenList = [];
            if (item.subMenu && item.subMenu.length) {
                _.forEach(item.subMenu, function (menuItem) {
                    var tempItem = {
                        id: menuItem.menuName,
                        title: menuItem.title,
                        type: menuItem.subMenu && menuItem.subMenu.length
                            ? "collapsable"
                            : "item",
                        url: menuItem.routingUI,
                        configFilePath: menuItem.configFilePath
                    };
                    childrenList.push(tempItem);
                });
            }
            var temp = {
                id: item.menuName,
                title: item.title,
                type: item.subMenu && item.subMenu.length ? "collapsable" : "item",
                translate: item.translate,
                icon: item.icon,
                children: childrenList,
                configFilePath: item.configFilePath
            };
            navigationList.push(temp);
        });
        return navigationList;
    };
    ToolbarComponent.prototype.sendMessage = function (id, triggerFrom) {
        for (var i = 0; i < this.dataSource.length; i++) {
            if (id == this.dataSource[i]._id) {
                localStorage.setItem("datasourceName", this.dataSource[i].name);
            }
        }
        localStorage.setItem("datasource", id);
        if (triggerFrom) {
            this.updateDatasource(id);
        }
        else {
            this.messageService.sendMessage(id);
        }
    };
    ToolbarComponent.prototype.updateDatasource = function (id) {
        var _this = this;
        var queryObj = {
            defaultDatasource: true
        };
        this.contentService
            .updateRequest(queryObj, '/datasources/setDefault/', id)
            .subscribe(function (res) {
            _this.messageService.sendMessage(id);
        }, function (error) {
        });
        console.log('actionRedirect >>>>>>>>>>>>>>> queryObj ', queryObj);
    };
    ToolbarComponent.prototype.redirectToNotification = function () {
        console.log(">>> redirectTo Notification link");
        var mainMenu = this._metaData.filter(function (obj) { return obj["menuName"] == "manageAccount"; });
        console.log(">>> mainMenu ", mainMenu);
        if (mainMenu.length > 0) {
            var submenus = mainMenu[0].subMenu;
            var subMenu = submenus.filter(function (obj) { return obj["menuId"] == "notification"; });
            var menuItem = subMenu[0];
            var tempItem = {
                id: menuItem.menuName,
                title: menuItem.title,
                type: menuItem.subMenu && menuItem.subMenu.length
                    ? "collapsable"
                    : "item",
                url: menuItem.routingUI,
                configFilePath: menuItem.configFilePath
            };
            console.log(">>> menuItem ", tempItem);
            this.messageService.sendRouting(tempItem);
        }
        this.enableNotification = false;
    };
    ToolbarComponent.prototype.ousideClick = function (e) {
        this.enableNotification = false;
    };
    ToolbarComponent.prototype.Read = function (input) {
        var _this = this;
        var apiUrl = "/notification/update";
        var queryObj = {
            isread: true,
            feature: "isread",
            idList: input._id
        };
        this.contentService.createRequest(queryObj, apiUrl)
            .subscribe(function (res) {
            _this.getAllNotification();
        });
        // this.ngOnInit();
    };
    ToolbarComponent.prototype.Remove = function (input) {
        var _this = this;
        var apiUrl = "/notification/update";
        var queryObj = {
            feature: "delete",
            idList: input._id
        };
        this.contentService.createRequest(queryObj, apiUrl)
            .subscribe(function (res) {
            _this.ngOnInit();
        });
    };
    /**
     * On destroy
     */
    ToolbarComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle sidebar open
     *
     * @param key
     */
    ToolbarComponent.prototype.toggleSidebarOpen = function (key) {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    };
    /**
     * Search
     *
     * @param value
     */
    ToolbarComponent.prototype.search = function (value) {
        // Do your search here...
        console.log(value);
    };
    /**
     * Set the language
     *
     * @param lang
     */
    ToolbarComponent.prototype.setLanguage = function (lang) {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;
        // Use the selected language for translations
        this._translateService.use(lang.id);
    };
    ToolbarComponent.prototype.logout = function () {
        console.log("Logout...");
        var dataSource = localStorage.getItem("datasource");
        var datasourceName = localStorage.getItem("datasourceName");
        this._cookieService.set('datasource', dataSource);
        this._cookieService.set('datasourceName', datasourceName);
        localStorage.clear();
        sessionStorage.clear();
        this.oauthService.logOut(false);
    };
    ToolbarComponent.prototype.changePwd = function () {
        // window.location.href = 'http://192.168.2.192:9014/change-password';
        console.log("user:::" + localStorage.getItem("access_token"));
        var access_token = localStorage.getItem("access_token");
        this.accountService.changePassword(access_token);
    };
    // sideNodifi
    ToolbarComponent.prototype.openSideNodifiNav = function (e) {
        console.log(e, "check");
        this.enableNotification = !this.enableNotification;
    };
    ToolbarComponent.prototype.closeSideNav = function () {
        this.enableNotification = false;
    };
    ToolbarComponent.prototype.close = function () {
        this.enableNotification = false;
    };
    ToolbarComponent.decorators = [
        { type: Component, args: [{
                    selector: "toolbar",
                    template: "<mat-toolbar class=\"p-0 mat-elevation-z1\">\r\n\r\n  <div fxFlex fxFill fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n    <div fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n      <button mat-icon-button class=\"navbar-toggle-button\" *ngIf=\"!hiddenNavbar && !rightNavbar\"\r\n        (click)=\"toggleSidebarOpen('navbar')\" fxHide.gt-md>\r\n        <mat-icon class=\"secondary-text\">menu</mat-icon>\r\n      </button>\r\n\r\n      <div class=\"toolbar-separator\" *ngIf=\"!hiddenNavbar && !rightNavbar\" fxHide.gt-md></div>\r\n\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"horizontalNavbar\">\r\n        <div class=\"logo ml-16\">\r\n          <img class=\"logo-icon\" src=\"assets/images/logos/sentri.png\">\r\n        </div>\r\n      </div>\r\n\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n        <!-- <button mat-button [matMenuTriggerFor]=\"realmItems\" class=\"user-button\">\r\n          <div fxLayout=\"row\">\r\n            <span class=\"username mr-12\" fxHide fxShow.gt-sm>{{realm_data}}</span>\r\n            <mat-icon class=\"s-16\" fxHide.xs>keyboard_arrow_down</mat-icon>\r\n          </div>\r\n          <mat-menu #realmItems=\"matMenu\" [overlapTrigger]=\"false\">\r\n            <button mat-menu-item *ngFor=\"let data_obj of data\">\r\n              <span value=\"data_obj._id\">{{data_obj.name}}</span>\r\n            </button>\r\n          </mat-menu>\r\n        </button> -->\r\n        <div class=\"h2 mb-12\" style=\"width: 180px;padding: 10px;padding-top: 45px;\">\r\n          <mat-form-field appearance=\"outline\" fxFlex=\"100\" fxlayout=\"row\" fxlayout.xs=\"column\" style=\"width:100%\">\r\n            <mat-label>Select Tenant</mat-label>\r\n            <mat-select required placeholder=\"Select Tenant\" [(ngModel)]=\"realm_data\">\r\n              <mat-option *ngFor=\"let data_obj of data\" [value]=\"data_obj._id\" (click)=\"getId(data_obj)\">\r\n                {{data_obj.name}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"px-8 px-md-16\">\r\n        <fuse-shortcuts [navigation]=\"navigation\"></fuse-shortcuts>\r\n      </div>\r\n      <div class=\"px-8 px-md-16\">\r\n        <session-expiration></session-expiration>\r\n      </div>\r\n    </div>\r\n    <img [src]=\"english.logoUrl\" style=\"height: 58%;\" alt=\"\">\r\n\r\n    <div class=\"\" fxFlex=\"0 1 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <div class=\"h2 mb-12\" style=\"width: 180px;padding: 10px;padding-top: 45px;\">\r\n        <mat-form-field appearance=\"outline\" fxFlex=\"100\" fxlayout=\"row\" fxlayout.xs=\"column\" style=\"width:100%\">\r\n          <mat-label>Select Datasource</mat-label>\r\n          <mat-select [(ngModel)]=\"defaultDatasource\" required placeholder=\"Select Datasource\"\r\n            (selectionChange)=\"sendMessage($event.value, 'toolbar')\">\r\n            <mat-option *ngFor=\"let ds of dataSource\" value=\"{{ds._id}}\">\r\n              {{ds.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n     <div>\r\n      <span class=\"sen-lib-notcount\" *ngIf=\"count > 0\"> {{count}}</span>\r\n      <button mat-button (click)=\"openSideNodifiNav($event)\" class=\"sen-lib-outline\">\r\n        <mat-icon matBadgeColor=\"accent\">\r\n          notifications\r\n        </mat-icon>\r\n      </button>\r\n      <mat-menu #notification=\"matMenu\" class=\"sen-lib-mat\">\r\n        <div class=\"sen-lib-ntytopheader\">\r\n          <p class=\"text-center\" style=\"font-size: 16px;\r\n          font-weight: 500;margin-bottom:0\">Notifications</p>\r\n        </div>\r\n        <mat-list role=\"list\">\r\n          <!-- <ng-container *ngFor=\"let notify of notificationList | slice:0:5; let i=index\"> -->\r\n            <ng-container *ngFor=\"let notify of notificationList; let i=index\">\r\n            <mat-list-item role=\"listitem\" class=\"sen-lib-mtlist\">\r\n              <span class=\"sen-lib-mbtm\" style=\"font-weight: 700;\">{{notify.msg}} ,\r\n\r\n                {{notify.created_at | date: 'dd/MM/yyyy hh:mm a'}}</span>\r\n              <button mat-icon-button style=\"float:right\">\r\n                <mat-icon style=\"color:red;\">close</mat-icon>\r\n              </button>\r\n            </mat-list-item>\r\n\r\n          </ng-container>\r\n          <p class=\"text-center\" style=\"margin-top:12px;\">\r\n            <button (click)=\"redirectToNotification()\" class=\"sen-lib-ntybtn btn-warning\">\r\n              <span>View All Notification</span>\r\n            </button>\r\n          </p>\r\n        </mat-list>\r\n      </mat-menu>\r\n\r\n    </div>\r\n    <div class=\"\" fxFlex=\"0 1 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n      <button mat-button [matMenuTriggerFor]=\"userMenu\" class=\"user-button\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <img class=\"avatar mr-0 mr-sm-16\" src=\"assets/images/avatars/profile.jpg\">\r\n          <span class=\"username mr-12\" fxHide fxShow.gt-sm>{{user}}</span>\r\n          <mat-icon class=\"s-16\" fxHide.xs>keyboard_arrow_down</mat-icon>\r\n        </div>\r\n      </button>\r\n\r\n      <mat-menu #userMenu=\"matMenu\" [overlapTrigger]=\"false\">\r\n\r\n        <button mat-menu-item (click)=\"changePwd()\" class=\"sen-lib-outline\">\r\n          <mat-icon>https</mat-icon>\r\n          <span>Change Password</span>\r\n        </button>\r\n\r\n        <button mat-menu-item class=\"sen-lib-outline\">\r\n          <mat-icon>help_outline</mat-icon>\r\n          <span>Help / Support</span>\r\n        </button>\r\n\r\n        <button mat-menu-item class=\"\" (click)=\"logout()\" class=\"sen-lib-outline\">\r\n          <mat-icon>exit_to_app</mat-icon>\r\n          <span>Logout</span>\r\n        </button>\r\n\r\n      </mat-menu>\r\n\r\n    </div>\r\n\r\n  </div>\r\n\r\n</mat-toolbar>\r\n\r\n<!-- New UI Raj  -->\r\n<!-- aside nodification side bar -->\r\n<ng-container *ngIf=\"enableNotification \">\r\n  <div class=\"sen-lib-notifiright\">\r\n    <div class=\"sen-lib-simple-wrapper\">\r\n      <div class=\"sen-lib-right-topheader p-3\">\r\n        <div class=\"card-title sen-card-n-title\">Notifications</div>\r\n        <div class=\"card-options ml-auto\">\r\n          <a class=\"sen-lib-sidetop-close sen-lib-anchorclose\" (click)=\"closeSideNav()\">\r\n            <span class=\"material-icons sen-lib-f-20 \">\r\n              highlight_off\r\n            </span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n      <ng-container *ngIf=\"notificationList && notificationList.length==0\">\r\n        <div class=\"panel-body\">\r\n          <!-- <div class=\"list-group list-group-flush\"> -->\r\n          <!-- <div class=\"list-group-item d-flex align-items-center\"> -->\r\n          <div class=\"text-center\">\r\n            <img src=\"/assets/no.PNG\" class=\"sen-lib-no-image\">\r\n          </div>\r\n          <div class=\"text-center\">\r\n            <h6 class=\"sen-lib-notifi-text\">No Notifications Found</h6>\r\n            <p class=\"sen-lib-notifi-message-content\">You have currently no notifications. We'll notify\r\n              you when something new arrives</p>\r\n            <button class=\"btn btn-close-nfbtm\" (click)=\"close()\">Close</button>\r\n          </div>\r\n          <!-- </div> -->\r\n          <!-- </div> -->\r\n        </div>\r\n      </ng-container>\r\n      <div class=\"panel-body\" *ngIf=\"notificationData && notificationData.length > 0\" style=\"overflow-y:auto;\"\r\n        [class.sen-lib-scrollbar]=\"notificationData.length > 5\">\r\n        <div class=\"text-right\">\r\n          <a class=\"sen-lib-clear\" (click)=\"clearallNotification(notificationList)\">Clear All</a>\r\n        </div>\r\n        <ng-container *ngFor=\"let notify of notificationData;let i= index;\">\r\n          <div class=\"list-group list-group-flush\">\r\n            <div class=\"list-group-item d-flex  align-items-center\" style=\"border-bottom:1px solid lightgray;\">\r\n              <div class=\"mr-3\">\r\n                <span class=\"avatar avatar-lg bround  cover-image\">\r\n                  {{notify.newstring}}\r\n                  <span class=\"avatar-status bg-success\"></span>\r\n                </span>\r\n              </div>\r\n              <div class=\"sen-lib-wordbreak\">\r\n                <strong></strong>\r\n                <span *ngIf=\"notify.additionalData && notify.additionalData.length\">\r\n                  {{notify.additionalData[0].name}}\r\n                </span>\r\n                {{notify.msg}}\r\n                <!-- <span> By - {{notify.userName}}</span> -->\r\n                <div class=\"\">\r\n                  <span class=\"material-icons sen-book-markcolor\">\r\n                    bookmark\r\n                  </span>\r\n                  <span class=\"sen-lib-feature\"> {{notify.feature}}</span>\r\n                  <!-- <span class=\"badge badge-success sen-lib-badgedone\">\r\n                    {{notify.StatusNew}}\r\n                  </span> -->\r\n                </div>\r\n                <div class=\"small text-muted\"> {{notify.dateTimeAgo}} ago </div>\r\n                <!-- <span style=\"font-size: 10px;\" *ngIf=\"notify.percentage\"> {{notify.percentage}}</span> -->\r\n                <span class=\"badge badge-success sen-lib-badgedone\">\r\n                  {{notify.StatusNew}}\r\n                </span>\r\n                <span *ngIf=\"notify.percentage == '100 %' && notify.messageObj && (notify.messageObj.feature == 'DOWNLOAD_REPORT_EXPORT' || notify.messageObj.feature == 'RUN_REPORT')\" class=\"material-icons sen-download\" (click)=\"navigateToDownloads(notify)\" matTooltip=\"Download\">\r\n                  get_app\r\n                </span>\r\n                <!-- <ng-container>\r\n                  <div style=\"cursor: pointer;\" class=\"sen-lib-wordbreak\">\r\n\r\n                </div>\r\n                </ng-container> -->\r\n                <ng-container *ngIf=\"notify.messageObj && notify.messageObj.type == 'auditing'\">\r\n                  <span style=\"padding-left: 14px;\r\n                  font-size: 10px;\">{{notify.percentage}}</span>\r\n                <!-- Dynamic Progress Bar style css working Neeed to Verify minor style-->\r\n                  <!-- <div class=\"progress\" role=\"progressbar\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\" [ngClass]=\"{'bg-warning': notify.percentage =='25 %','bg-failed':notify.percentage =='0 %','bg-success': notify.percentage =='100 %','bg-intiated': notify.percentage =='10 %',\r\n                  'bg-reimport': notify.percentage =='50 %'}\">\r\n                    <div  role=\"progressbar\"  \r\n                    class=\"progress-bar progress-bar-striped\">\r\n                    </div> \r\n                  </div> -->\r\n                <!-- Dynamic Progress bar end -->\r\n                <!-- <div class=\"progress\">\r\n                  <div [ngStyle]=\"\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div> -->\r\n                \r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '50 %'\">\r\n                <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 50%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '100 %' && (notify.operationStatus !== 'Create_Success' || notify.operationStatus !== 'Update_Success' || notify.operationStatus !== 'Default_Success')\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 95%; background: green;\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '10 %'\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 20%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '0 %'\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 99%; background: red;\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '25 %'\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 35%; background: rgb(255, 230, 0);\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                </ng-container>\r\n              </div>\r\n              <!-- <div class=\"sen-lib-markusread\" *ngIf=\"notify.isread == false\">\r\n                <button class=\"sen-lib-btn-tooltip\" matTooltip=\"Mark us read\" (click)=\"Read(notify)\"></button>\r\n              </div> -->\r\n              <div>\r\n                <span class=\"material-icons sen-lib-notify-remove\" (click)=\"Read(notify)\"> \r\n                  close\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </ng-container>\r\n      </div>\r\n      <div class=\"sen-lib-seeAll\" *ngIf=\"notificationData && notificationData.length > 0\">\r\n        <div class=\"text-center\">\r\n          <button class=\"btn btn-warning btn-all-nodifi\" (click)=\"redirectToNotification()\">See All\r\n            Notification</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div (click)=\"ousideClick($event)\" [class.ui-lib-overlay]=\"enableNotification\"></div>\r\n  <!-- <div class=\"sen-lib-rightbar-overlay\"></div> -->\r\n</ng-container>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: ["toolbar{position:relative;display:-webkit-box;display:flex;-webkit-box-flex:0;flex:0 0 auto;z-index:4}toolbar.below{z-index:2}toolbar .mat-toolbar{position:relative;background:inherit!important;color:inherit!important}toolbar .logo{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}toolbar .logo .logo-icon{width:38px}toolbar .chat-panel-toggle-button,toolbar .language-button,toolbar .quick-panel-toggle-button,toolbar .user-button,toolbar fuse-search-bar{min-width:64px;height:64px}@media screen and (max-width:599px){toolbar .chat-panel-toggle-button,toolbar .language-button,toolbar .quick-panel-toggle-button,toolbar .user-button,toolbar fuse-search-bar{height:56px}}toolbar .navbar-toggle-button{min-width:56px;height:56px}toolbar .toolbar-separator{height:64px;width:1px}@media screen and (max-width:599px){toolbar .toolbar-separator{height:56px}}.sen-lib-outline,.user-button{outline:0!important}.sen-lib-mbtm{margin-bottom:5px!important}.sen-lib-mtlist{border-bottom:1px solid #ccc!important;height:initial!important;font-size:14px!important}.sen-lib-ntybtn{text-align:center;padding:12px;text-decoration:none!important;font-size:14px;font-weight:500;width:68%;border-radius:25px;border:0;color:#fff!important;outline:0!important;background:#2c69f5}.sen-lib-notcount{border-radius:20px;background:#ec1707;margin-right:-22px;margin-top:3px;width:17px;height:17px;background-color:#ff8f00!important;position:relative;top:-9px;z-index:999;left:25px;font-size:9px;line-height:12px;font-family:Roboto,sans-serif;color:#fff;font-weight:700;padding:2px}.sen-lib-ntytopheader{background:linear-gradient(135deg,#fad961 0,#f76b1c 100%);margin-top:-9px;padding-top:10px;padding-bottom:10px;color:#fff}.sen-lib-right-topheader{top:0;width:100%;z-index:1;display:-webkit-box;display:flex;margin-bottom:12px;background:#ecf0fa}.card-title{font-weight:700;color:#242f48;font-size:14px;text-transform:uppercase}.sen-lib-anchorclose{cursor:pointer;text-decoration:none!important;position:relative;top:6px}.sen-lib-f-20{font-size:20px}.cover-image{text-transform:capitalize;width:50px!important;height:50px!important;background:#4161ef;color:#fff!important;font-size:13px;padding:5px 12px;border-radius:50%}.sen-lib-wordbreak{width:100%;word-break:break-all!important;font-size:13px;font-weight:500}.sen-book-markcolor{color:red}.sen-lib-feature{color:#0e6377!important}.sen-lib-btn-tooltip{padding:0;-webkit-transition:none;transition:none;width:18px!important;height:18px;font-size:0;background:#0052cc;border:4px solid #ebecf0;border-radius:100%;margin-right:5px;visibility:visible}.sen-lib-badgedone{background:#e3fcef!important;color:#064!important;position:relative;left:12px}.sen-lib-seeAll{margin-top:12px}button.btn.btn-warning.btn-all-nodifi{background:#4161ef;color:#fff;font-size:14px;border-color:#4161ef;width:66%}.sen-card-n-title{top:4px;position:relative}.sen-lib-notify-remove{cursor:pointer;color:red}.sen-lib-scrollbar{height:600px!important;margin-right:15px}.sen-lib-scrollbar::-webkit-scrollbar{width:8px!important}.sen-lib-scrollbar::-webkit-scrollbar-track{background:#f1f1f1!important}.sen-lib-scrollbar::-webkit-scrollbar-thumb{background:#dedee0!important}.sen-lib-notifi-text{font-size:20px;font-weight:700}.sen-lib-notifi-message-content{font-size:14px;font-style:italic;color:gray}.sen-lib-no-image{width:200px;margin-top:70px}button.btn.btn-close-nfbtm{font-size:14px;font-weight:600;background:red;color:#fff;width:35%}.sen-download{color:red;font-size:20px;margin-left:6px;cursor:pointer}.progress-bar.progress-bar-striped.bg-warning{width:35%;background:#ffe600}.progress-bar.progress-bar-striped.bg-success{width:95%;background:green}.progress-bar.progress-bar-striped.bg-failed{width:99%;background:red}.progress-bar.progress-bar-striped.bg-danger{width:50%;background:#00f}.sen-lib-clear{color:#072bbf;font-size:14px;font-weight:500;margin-right:12px;position:relative;bottom:5px;cursor:pointer}.progress-bar-striped{background-image:linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)!important;background-size:1rem 1rem!important}.ui-lib-overlay{background-color:rgba(52,58,64,.55);position:fixed;left:0;right:0;top:0;bottom:0;z-index:9998;-webkit-transition:.2s ease-out;transition:.2s ease-out;display:block;height:100vh}"]
                }] }
    ];
    /** @nocollapse */
    ToolbarComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: FuseConfigService },
        { type: FuseSidebarService },
        { type: TranslateService },
        { type: ContentService },
        { type: AccountService },
        { type: OAuthService },
        { type: MessageService },
        { type: CookieService },
        { type: LoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["metaData",] }] },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    return ToolbarComponent;
}());
export { ToolbarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbGJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsidG9vbGJhci90b29sYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2hFLE9BQU8sRUFDTCxTQUFTLEVBR1QsaUJBQWlCLEVBQ2pCLE1BQU0sRUFDUCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN2RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNqRixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDbkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2xELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLG9DQUFvQztBQUNwQyxPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQztBQUNsQyxJQUFNLE1BQU0sR0FBRyxPQUFPLENBQUM7QUFDdkIsT0FBTyxLQUFLLFNBQVMsTUFBTSxZQUFZLENBQUM7QUFHeEM7SUE4QkU7Ozs7OztPQU1HO0lBQ0gsMEJBQ1UsNkJBQTJELEVBQzNELGtCQUFxQyxFQUNyQyxtQkFBdUMsRUFDdkMsaUJBQW1DLEVBQ25DLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLFlBQTBCLEVBQzFCLGNBQThCLEVBQzlCLGNBQTZCLEVBQzdCLGFBQTRCLEVBQ1IsU0FBUyxFQUNYLE9BQU87UUFabkMsaUJBNkVDO1FBNUVTLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBOEI7UUFDM0QsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQW9CO1FBQ3ZDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDUixjQUFTLEdBQVQsU0FBUyxDQUFBO1FBQ1gsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUVqQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0QsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxpQkFBaUIsR0FBRztZQUN2QjtnQkFDRSxLQUFLLEVBQUUsUUFBUTtnQkFDZixJQUFJLEVBQUUsNkJBQTZCO2dCQUNuQyxLQUFLLEVBQUUsU0FBUzthQUNqQjtZQUNEO2dCQUNFLEtBQUssRUFBRSxNQUFNO2dCQUNiLElBQUksRUFBRSxZQUFZO2dCQUNsQixLQUFLLEVBQUUsU0FBUzthQUNqQjtZQUNEO2dCQUNFLEtBQUssRUFBRSxnQkFBZ0I7Z0JBQ3ZCLElBQUksRUFBRSxtQkFBbUI7Z0JBQ3pCLEtBQUssRUFBRSxTQUFTO2FBQ2pCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFdBQVc7Z0JBQ2xCLElBQUksRUFBRSxvQ0FBb0M7Z0JBQzFDLEtBQUssRUFBRSxTQUFTO2FBQ2pCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLElBQUksRUFBRSxvQ0FBb0M7Z0JBQzFDLEtBQUssRUFBRSxTQUFTO2FBQ2pCO1NBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxTQUFTLEdBQUc7WUFDZjtnQkFDRSxFQUFFLEVBQUUsSUFBSTtnQkFDUixLQUFLLEVBQUUsU0FBUztnQkFDaEIsSUFBSSxFQUFFLElBQUk7YUFDWDtZQUNEO2dCQUNFLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEtBQUssRUFBRSxTQUFTO2dCQUNoQixJQUFJLEVBQUUsSUFBSTthQUNYO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNwRCxJQUFHLEtBQUksQ0FBQyxVQUFVLEVBQUM7Z0JBQ2pCLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFBO2FBQ3hCO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN4QyxJQUFHLFFBQVEsSUFBSSxRQUFRLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxJQUFJLElBQUksTUFBTSxFQUFDO2dCQUN0RCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztnQkFDdkMsS0FBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3ZDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLHNCQUFzQixFQUFFO2FBQzNDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3JDLElBQUcsS0FBSSxDQUFDLFVBQVUsRUFBQztnQkFDakIsS0FBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7YUFDM0I7UUFDSCxDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRXJDLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsbUNBQVEsR0FBUjtRQUFBLGlCQW9DQztRQW5DQyxrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU07YUFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLFVBQUEsUUFBUTtZQUNqQixLQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQztZQUNsRSxLQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUM7WUFDL0QsS0FBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDO1FBQzdELENBQUMsQ0FBQyxDQUFDO1FBRUwsbURBQW1EO1FBQ25ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDN0MsRUFBRSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXO1NBQ3ZDLENBQUMsQ0FBQztRQUNILElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxVQUFBLElBQUk7WUFDNUMsc0JBQXNCO1lBQ3RCLEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUMxQixLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDdkIsdUNBQXVDO1lBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3ZCLElBQUksU0FBUyxFQUFFO2dCQUNiLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3pELEtBQUksQ0FBQyxVQUFVLEdBQUcsU0FBUyxDQUFDO2FBQzdCO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7YUFDcEM7WUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDL0MsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLEVBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ2pFLEtBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1lBQ3hCLEtBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsMkJBQTJCO1FBQzNCLDZCQUE2QjtJQUMvQixDQUFDO0lBQ0QsZ0NBQUssR0FBTCxVQUFNLEtBQUs7UUFBWCxpQkFrQkM7UUFqQkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUyxDQUM5QyxVQUFDLElBQVM7WUFDUixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMxQixZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3hDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3RFLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN6QyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFVBQVU7WUFDdkQsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFVBQVU7WUFDNUQsWUFBWSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUN0QyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQzNCLENBQUMsRUFDRCxVQUFBLEtBQUs7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM1QixDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCwyQ0FBZ0IsR0FBaEI7UUFBQSxpQkE2Q0M7UUEzQ0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNqQyxJQUFJLEtBQUssR0FBRztZQUNWLEtBQUssRUFBRyxJQUFJLENBQUMsVUFBVTtTQUN4QixDQUFDO1FBQ0YsSUFBSSxNQUFNLEdBQUcsa0JBQWtCLENBQUM7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDNUQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxLQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQzNDLElBQUksVUFBVSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBSSx5QkFBeUI7WUFDakYsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFJLGtCQUFrQjtZQUM1RSwwQ0FBMEM7WUFDMUMsSUFBSTtZQUNKLDRDQUE0QztZQUM1Qyx5RUFBeUU7WUFDekUseUNBQXlDO1lBQ3pDLFVBQVU7WUFDVixJQUFJLEtBQUksQ0FBQyxRQUFRLElBQUksS0FBSSxDQUFDLFFBQVEsSUFBSSxNQUFNLEVBQUU7Z0JBQzVDLEtBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDO2dCQUMxQyxLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztnQkFDdkUsS0FBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ3ZDLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO2dCQUM5RCxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQ3BFO2lCQUFNO2dCQUNMLElBQUksdUJBQXVCLEdBQUcsRUFBRSxDQUFDO2dCQUNqQyx1QkFBdUIsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxFQUFFLEVBQUMsaUJBQWlCLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztnQkFDeEYsSUFBRyx1QkFBdUIsSUFBSSx1QkFBdUIsQ0FBQyxNQUFNLEVBQUM7b0JBQzNELEtBQUksQ0FBQyxvQkFBb0IsR0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7b0JBQzNELEtBQUksQ0FBQyxxQkFBcUIsR0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQzdELEtBQUksQ0FBQyxpQkFBaUIsR0FBRyx1QkFBdUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7b0JBQ3hELFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUM5RCxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2lCQUNwRTtxQkFBSTtvQkFDSCxLQUFJLENBQUMsb0JBQW9CLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO29CQUM1RCxLQUFJLENBQUMscUJBQXFCLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUM5RCxLQUFJLENBQUMsaUJBQWlCLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO29CQUN6RCxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxLQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztvQkFDOUQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxLQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztpQkFDcEU7YUFFRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELCtDQUFvQixHQUFwQixVQUFxQixnQkFBZ0I7UUFBckMsaUJBYUM7UUFaQyxJQUFJLE1BQU0sR0FBRywwQkFBMEIsQ0FBQztRQUN4QyxJQUFJLFFBQVEsR0FBRztZQUNiLE1BQU0sRUFBRSxJQUFJO1lBQ1osT0FBTyxFQUFFLFFBQVE7WUFDakIsS0FBSyxFQUFHLElBQUksQ0FBQyxVQUFVO1NBQ3hCLENBQUM7UUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQTtRQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQzthQUN0RCxTQUFTLENBQUMsVUFBQSxHQUFHO1lBQ1osS0FBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUNoQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsNkNBQWtCLEdBQWxCO1FBQUEsaUJBc0NDO1FBckNDLElBQUksS0FBSyxHQUFHO1lBQ1YsTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsSUFBSTtZQUNiLEtBQUssRUFBRyxJQUFJLENBQUMsVUFBVTtTQUN4QixDQUFDO1FBQ0YsSUFBSSxNQUFNLEdBQUUsZUFBZSxDQUFDO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDNUQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUM7WUFDdEQsSUFBSSxJQUFJLEdBQUcsS0FBSSxDQUFDO1lBRWhCLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSSxDQUFDLGdCQUFnQixFQUFFLFVBQVMsUUFBUTtnQkFDaEQsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdELElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFdBQVcsQ0FBQyxDQUFBO2dCQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3ZELCtCQUErQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzdELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztnQkFDNUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxlQUFlLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLHFDQUFxQyxHQUFHLGVBQWUsQ0FBQyxDQUFDO2dCQUN6SCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3pELDhCQUE4QixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyxnQ0FBZ0MsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4SCxJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztnQkFDdkMsUUFBUSx3QkFBTyxRQUFRLEVBQUssSUFBSSxDQUFDLENBQUE7Z0JBQ2pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3JDLHFEQUFxRDtZQUV2RCxDQUFDLENBQ0YsQ0FBQztZQUNGLEtBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsOENBQW1CLEdBQW5CLFVBQW9CLEdBQUc7UUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN6Qyx1QkFBdUI7UUFDdkIsb0RBQW9EO1FBQ3BELHFEQUFxRDtRQUNyRCxtREFBbUQ7UUFDbkQseUZBQXlGO1FBQ3pGLG1EQUFtRDtRQUNuRCw0QkFBNEI7UUFDNUIsT0FBTztRQUNQLDBDQUEwQztRQUMxQyw4Q0FBOEM7UUFFOUMsMEVBQTBFO1FBQzFFLGlDQUFpQztRQUNqQyx1QkFBdUI7UUFDdkIsK0JBQStCO1FBQy9CLCtCQUErQjtRQUMvQixlQUFlO1FBQ2YsMkJBQTJCO1FBQzNCLDRCQUE0QjtRQUM1QixzQkFBc0I7UUFDdEIsaUNBQWlDO1FBQ2pDLGdEQUFnRDtRQUNoRCxTQUFTO1FBQ1QsOENBQThDO1FBQzlDLDJCQUEyQjtRQUMzQixpREFBaUQ7UUFDakQsTUFBTTtRQUNOLElBQUk7UUFDSixJQUFJLEdBQUcsQ0FBQyxjQUFjLElBQUksR0FBRyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDbkQsSUFBSSxVQUFVLEdBQUcsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7U0FDbkQ7YUFBTTtZQUNMLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDM0MsSUFBSSxLQUFLLEdBQUc7WUFDVixRQUFRLEVBQUUsVUFBVTtTQUNyQixDQUFBO1FBQ0QsSUFBSSxNQUFNLEdBQUcseUJBQXlCLENBQUM7UUFDdkMsSUFBSSxVQUFVLElBQUksU0FBUyxJQUFJLFVBQVUsSUFBSSxJQUFJLElBQUksVUFBVSxJQUFJLEVBQUUsRUFBRTtZQUNyRSxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDL0I7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjO2lCQUNsQixhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQztpQkFDNUIsU0FBUyxDQUFDLFVBQUEsSUFBSTtnQkFDYixJQUFNLEVBQUUsR0FBRyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUM3QyxJQUFNLElBQUksR0FBRyxJQUFJLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDaEMsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUN6QyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDeEI7Z0JBQ0QsSUFBSSxZQUFZLEdBQUcsaUJBQWlCLENBQUM7Z0JBQ3JDLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztnQkFDcEQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDaEUsQ0FBQyxDQUFDLENBQUM7U0FDRjtJQUVELENBQUM7SUFLSCxzQ0FBVyxHQUFYO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDNUIsSUFBSSxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBRXhCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxVQUFVLElBQUk7WUFDdEMsSUFBSSxZQUFZLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTtnQkFDdkMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsUUFBUTtvQkFDeEMsSUFBSSxRQUFRLEdBQUc7d0JBQ2IsRUFBRSxFQUFFLFFBQVEsQ0FBQyxRQUFRO3dCQUNyQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7d0JBQ3JCLElBQUksRUFDRixRQUFRLENBQUMsT0FBTyxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTTs0QkFDekMsQ0FBQyxDQUFDLGFBQWE7NEJBQ2YsQ0FBQyxDQUFDLE1BQU07d0JBQ1osR0FBRyxFQUFFLFFBQVEsQ0FBQyxTQUFTO3dCQUV2QixjQUFjLEVBQUUsUUFBUSxDQUFDLGNBQWM7cUJBQ3hDLENBQUM7b0JBQ0YsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUVELElBQUksSUFBSSxHQUFHO2dCQUNULEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUTtnQkFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxNQUFNO2dCQUNsRSxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7Z0JBQ3pCLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDZixRQUFRLEVBQUUsWUFBWTtnQkFDdEIsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO2FBQ3BDLENBQUM7WUFDRixjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO1FBRUgsT0FBTyxjQUFjLENBQUM7SUFDeEIsQ0FBQztJQUNELHNDQUFXLEdBQVgsVUFBWSxFQUFFLEVBQUUsV0FBVztRQUN6QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBSSxFQUFFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2hDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNqRTtTQUNGO1FBQ0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFDdkMsSUFBRyxXQUFXLEVBQUM7WUFDYixJQUFJLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDM0I7YUFBSTtZQUNILElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3JDO0lBQ0gsQ0FBQztJQUNELDJDQUFnQixHQUFoQixVQUFrQixFQUFFO1FBQXBCLGlCQWNDO1FBYkMsSUFBSSxRQUFRLEdBQUc7WUFDWCxpQkFBaUIsRUFBRyxJQUFJO1NBQ3pCLENBQUM7UUFDSixJQUFJLENBQUMsY0FBYzthQUNsQixhQUFhLENBQUMsUUFBUSxFQUFDLDBCQUEwQixFQUFFLEVBQUUsQ0FBQzthQUN0RCxTQUFTLENBQ1IsVUFBQSxHQUFHO1lBQ0QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxFQUNELFVBQUEsS0FBSztRQUNMLENBQUMsQ0FDRixDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsRUFBRSxRQUFRLENBQUMsQ0FBQTtJQUNuRSxDQUFDO0lBQ0QsaURBQXNCLEdBQXRCO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsQ0FBQyxDQUFDO1FBQ2hELElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLGVBQWUsRUFBbEMsQ0FBa0MsQ0FBQyxDQUFDO1FBQ2hGLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLElBQUcsUUFBUSxDQUFDLE1BQU0sR0FBQyxDQUFDLEVBQ25CO1lBQ0MsSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNuQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLGNBQWMsRUFBL0IsQ0FBK0IsQ0FBQyxDQUFDO1lBQ3RFLElBQUksUUFBUSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxQixJQUFJLFFBQVEsR0FBRztnQkFDYixFQUFFLEVBQUUsUUFBUSxDQUFDLFFBQVE7Z0JBQ3JCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSztnQkFDckIsSUFBSSxFQUNGLFFBQVEsQ0FBQyxPQUFPLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNO29CQUN6QyxDQUFDLENBQUMsYUFBYTtvQkFDZixDQUFDLENBQUMsTUFBTTtnQkFDWixHQUFHLEVBQUUsUUFBUSxDQUFDLFNBQVM7Z0JBQ3ZCLGNBQWMsRUFBRSxRQUFRLENBQUMsY0FBYzthQUN4QyxDQUFDO1lBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDM0M7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxzQ0FBVyxHQUFYLFVBQVksQ0FBQztRQUNYLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7SUFDbEMsQ0FBQztJQUNELCtCQUFJLEdBQUosVUFBSyxLQUFLO1FBQVYsaUJBYUM7UUFaQyxJQUFJLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQztRQUNwQyxJQUFJLFFBQVEsR0FBRztZQUNiLE1BQU0sRUFBRSxJQUFJO1lBQ1osT0FBTyxFQUFFLFFBQVE7WUFDakIsTUFBTSxFQUFFLEtBQUssQ0FBQyxHQUFHO1NBQ2xCLENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDO2FBQ2hELFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztRQUNILG1CQUFtQjtJQUVyQixDQUFDO0lBR0QsaUNBQU0sR0FBTixVQUFPLEtBQUs7UUFBWixpQkFVQztRQVRDLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ3BDLElBQUksUUFBUSxHQUFHO1lBQ2IsT0FBTyxFQUFFLFFBQVE7WUFDakIsTUFBTSxFQUFFLEtBQUssQ0FBQyxHQUFHO1NBQ2xCLENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDO2FBQ2hELFNBQVMsQ0FBQyxVQUFBLEdBQUc7WUFDWixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBS0Q7O09BRUc7SUFDSCxzQ0FBVyxHQUFYO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILDRDQUFpQixHQUFqQixVQUFrQixHQUFHO1FBQ25CLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDeEQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxpQ0FBTSxHQUFOLFVBQU8sS0FBSztRQUNWLHlCQUF5QjtRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsc0NBQVcsR0FBWCxVQUFZLElBQUk7UUFDZCw0Q0FBNEM7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUU3Qiw2Q0FBNkM7UUFDN0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUNELGlDQUFNLEdBQU47UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3pCLElBQUksVUFBVSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDcEQsSUFBSSxjQUFjLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUMxRCxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckIsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxvQ0FBUyxHQUFUO1FBQ0Usc0VBQXNFO1FBQ3RFLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUM5RCxJQUFJLFlBQVksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFDRCxhQUFhO0lBQ2IsNENBQWlCLEdBQWpCLFVBQWtCLENBQUM7UUFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUE7UUFDdkIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDO0lBQ3JELENBQUM7SUFDRCx1Q0FBWSxHQUFaO1FBQ0UsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztJQUNsQyxDQUFDO0lBQ0QsZ0NBQUssR0FBTDtRQUNFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7SUFDbEMsQ0FBQzs7Z0JBdmhCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLFNBQVM7b0JBQ25CLHF6YkFBdUM7b0JBRXZDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDdEM7Ozs7Z0JBWlEsNEJBQTRCO2dCQVQ1QixpQkFBaUI7Z0JBQ2pCLGtCQUFrQjtnQkFKbEIsZ0JBQWdCO2dCQUtoQixjQUFjO2dCQUdkLGNBQWM7Z0JBQ2QsWUFBWTtnQkFuQlosY0FBYztnQkFvQmQsYUFBYTtnQkFDYixhQUFhO2dEQXdEakIsTUFBTSxTQUFDLFVBQVU7Z0RBQ2pCLE1BQU0sU0FBQyxTQUFTOztJQXVlckIsdUJBQUM7Q0FBQSxBQXhoQkQsSUF3aEJDO1NBbGhCWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLy4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25EZXN0cm95LFxyXG4gIE9uSW5pdCxcclxuICBWaWV3RW5jYXBzdWxhdGlvbixcclxuICBJbmplY3RcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSBcInJ4anMvU3ViamVjdFwiO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZUNvbmZpZ1NlcnZpY2UgfSBmcm9tIFwiLi4vQGZ1c2Uvc2VydmljZXMvY29uZmlnLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgRnVzZVNpZGViYXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tIFwiLi4vY29udGVudC9jb250ZW50LnNlcnZpY2VcIjtcclxuXHJcbmltcG9ydCB7IG5hdmlnYXRpb24gfSBmcm9tIFwiLi4vbmF2aWdhdGlvbi9uYXZpZ2F0aW9uXCI7XHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSBcIi4uL3NoYXJlZC9hdXRoL2FjY291bnQuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBPQXV0aFNlcnZpY2UgfSBmcm9tIFwiYW5ndWxhci1vYXV0aDItb2lkY1wiO1xyXG5pbXBvcnQgeyBDb29raWVTZXJ2aWNlIH0gZnJvbSAnbmd4LWNvb2tpZS1zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uL2xvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gJy4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlJztcclxuLy8gaW1wb3J0ICogYXMgbW9tZW50IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSAnbW9tZW50JztcclxuY29uc3QgbW9tZW50ID0gbW9tZW50XztcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcblxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwidG9vbGJhclwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vdG9vbGJhci5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi90b29sYmFyLmNvbXBvbmVudC5zY3NzXCJdLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFRvb2xiYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgaG9yaXpvbnRhbE5hdmJhcjogYm9vbGVhbjtcclxuICByaWdodE5hdmJhcjogYm9vbGVhbjtcclxuICBoaWRkZW5OYXZiYXI6IGJvb2xlYW47XHJcbiAgbGFuZ3VhZ2VzOiBhbnk7XHJcbiAgbmF2aWdhdGlvbjogYW55O1xyXG4gIHNlbGVjdGVkTGFuZ3VhZ2U6IGFueTtcclxuICB1c2VyU3RhdHVzT3B0aW9uczogYW55W107XHJcbiAgZGF0YVNvdXJjZTogYW55O1xyXG4gIG5vdGlmaWNhdGlvbkxpc3Q6IGFueVtdO1xyXG4gIHNlbGVjdGVkRGF0YXNvdXJjZUlkOiBhbnk7XHJcbiAgZGVmYXVsdERhdGFzb3VyY2VOYW1lOiBhbnk7XHJcbiAgZGVmYXVsdERhdGFzb3VyY2U6IGFueTtcclxuICBkZWZhdWx0UmVhbG06IHN0cmluZztcclxuICBub3RpZmljYXRpb25EYXRhIDogYW55W107XHJcbiAgdXNlcjogYW55O1xyXG4gIGRhdGE6IGFueTtcclxuICByZWFsbV9kYXRhOiBhbnk7XHJcbiAgY29va2llRFM6IGFueTtcclxuICBjb3VudDogbnVtYmVyO1xyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG4gIGVuYWJsZU5vdGlmaWNhdGlvbjogYm9vbGVhbjtcclxuXHJcbiAgLyoqXHJcbiAgICogQ29uc3RydWN0b3JcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7RnVzZUNvbmZpZ1NlcnZpY2V9IF9mdXNlQ29uZmlnU2VydmljZVxyXG4gICAqIEBwYXJhbSB7RnVzZVNpZGViYXJTZXJ2aWNlfSBfZnVzZVNpZGViYXJTZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtUcmFuc2xhdGVTZXJ2aWNlfSBfdHJhbnNsYXRlU2VydmljZVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZTogRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgX2Z1c2VDb25maWdTZXJ2aWNlOiBGdXNlQ29uZmlnU2VydmljZSxcclxuICAgIHByaXZhdGUgX2Z1c2VTaWRlYmFyU2VydmljZTogRnVzZVNpZGViYXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfdHJhbnNsYXRlU2VydmljZTogVHJhbnNsYXRlU2VydmljZSxcclxuICAgIHByaXZhdGUgY29udGVudFNlcnZpY2U6IENvbnRlbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG9hdXRoU2VydmljZTogT0F1dGhTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9jb29raWVTZXJ2aWNlOiBDb29raWVTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgQEluamVjdChcIm1ldGFEYXRhXCIpIHByaXZhdGUgX21ldGFEYXRhLFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHVibGljIGVuZ2xpc2hcclxuICApIHtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICAgIC8vIFNldCB0aGUgZGVmYXVsdHNcclxuICAgIHRoaXMudXNlclN0YXR1c09wdGlvbnMgPSBbXHJcbiAgICAgIHtcclxuICAgICAgICB0aXRsZTogXCJPbmxpbmVcIixcclxuICAgICAgICBpY29uOiBcImljb24tY2hlY2tib3gtbWFya2VkLWNpcmNsZVwiLFxyXG4gICAgICAgIGNvbG9yOiBcIiM0Q0FGNTBcIlxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgdGl0bGU6IFwiQXdheVwiLFxyXG4gICAgICAgIGljb246IFwiaWNvbi1jbG9ja1wiLFxyXG4gICAgICAgIGNvbG9yOiBcIiNGRkMxMDdcIlxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgdGl0bGU6IFwiRG8gbm90IERpc3R1cmJcIixcclxuICAgICAgICBpY29uOiBcImljb24tbWludXMtY2lyY2xlXCIsXHJcbiAgICAgICAgY29sb3I6IFwiI0Y0NDMzNlwiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICB0aXRsZTogXCJJbnZpc2libGVcIixcclxuICAgICAgICBpY29uOiBcImljb24tY2hlY2tib3gtYmxhbmstY2lyY2xlLW91dGxpbmVcIixcclxuICAgICAgICBjb2xvcjogXCIjQkRCREJEXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIHRpdGxlOiBcIk9mZmxpbmVcIixcclxuICAgICAgICBpY29uOiBcImljb24tY2hlY2tib3gtYmxhbmstY2lyY2xlLW91dGxpbmVcIixcclxuICAgICAgICBjb2xvcjogXCIjNjE2MTYxXCJcclxuICAgICAgfVxyXG4gICAgXTtcclxuXHJcbiAgICB0aGlzLmxhbmd1YWdlcyA9IFtcclxuICAgICAge1xyXG4gICAgICAgIGlkOiBcImVuXCIsXHJcbiAgICAgICAgdGl0bGU6IFwiRW5nbGlzaFwiLFxyXG4gICAgICAgIGZsYWc6IFwidXNcIlxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgaWQ6IFwidHJcIixcclxuICAgICAgICB0aXRsZTogXCJUdXJraXNoXCIsXHJcbiAgICAgICAgZmxhZzogXCJ0clwiXHJcbiAgICAgIH1cclxuICAgIF07XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLmdldERhdGFzb3VyY2UoKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBpZih0aGlzLnJlYWxtX2RhdGEpe1xyXG4gICAgICAgIHRoaXMuZ2V0QWxsRGF0YXNvdXJjZSgpXHJcbiAgICAgIH1cclxuICAgICAgY29uc29sZS5sb2coXCIgc2V0RGF0YXNvdWNlIFwiLCByZXNwb25zZSk7XHJcbiAgICAgIGlmKHJlc3BvbnNlICYmIHJlc3BvbnNlLnRleHQgJiYgcmVzcG9uc2UudGV4dCAhPSAnbnVsbCcpe1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSByZXNwb25zZS50ZXh0O1xyXG4gICAgICAgIHRoaXMuc2VuZE1lc3NhZ2UocmVzcG9uc2UudGV4dCwgbnVsbCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5nZXRUcmlnZ2VyTm90aWZpY2F0aW9uKClcclxuICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGRhdGEsXCI+Pj4+Pj4+Pj4+PiBEQVRBXCIpO1xyXG4gICAgICBpZih0aGlzLnJlYWxtX2RhdGEpe1xyXG4gICAgICAgIHRoaXMuZ2V0QWxsTm90aWZpY2F0aW9uKCk7XHJcbiAgICAgIH1cclxuICAgIH0pXHJcbiAgICB0aGlzLm5hdmlnYXRpb24gPSB0aGlzLmdldE1ldGFEYXRhKCk7XHJcblxyXG4gICAgLy8gU2V0IHRoZSBwcml2YXRlIGRlZmF1bHRzXHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogT24gaW5pdFxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgLy8gU3Vic2NyaWJlIHRvIHRoZSBjb25maWcgY2hhbmdlc1xyXG4gICAgdGhpcy5fZnVzZUNvbmZpZ1NlcnZpY2UuY29uZmlnXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoc2V0dGluZ3MgPT4ge1xyXG4gICAgICAgIHRoaXMuaG9yaXpvbnRhbE5hdmJhciA9IHNldHRpbmdzLmxheW91dC5uYXZiYXIucG9zaXRpb24gPT09IFwidG9wXCI7XHJcbiAgICAgICAgdGhpcy5yaWdodE5hdmJhciA9IHNldHRpbmdzLmxheW91dC5uYXZiYXIucG9zaXRpb24gPT09IFwicmlnaHRcIjtcclxuICAgICAgICB0aGlzLmhpZGRlbk5hdmJhciA9IHNldHRpbmdzLmxheW91dC5uYXZiYXIuaGlkZGVuID09PSB0cnVlO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAvLyBTZXQgdGhlIHNlbGVjdGVkIGxhbmd1YWdlIGZyb20gZGVmYXVsdCBsYW5ndWFnZXNcclxuICAgIHRoaXMuc2VsZWN0ZWRMYW5ndWFnZSA9IF8uZmluZCh0aGlzLmxhbmd1YWdlcywge1xyXG4gICAgICBpZDogdGhpcy5fdHJhbnNsYXRlU2VydmljZS5jdXJyZW50TGFuZ1xyXG4gICAgfSk7XHJcbiAgICBsZXQgcmVhbG1EYXRhID0gdGhpcy5fY29va2llU2VydmljZS5nZXQoXCJyZWFsbVwiKTtcclxuICAgIHRoaXMuYWNjb3VudFNlcnZpY2Uuc3dpdGNoRXZlbnQuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAvL2dldCBjdXJyZW50IHVzZXJuYW1lXHJcbiAgICAgIHRoaXMudXNlciA9IGRhdGEudXNlcm5hbWU7XHJcbiAgICAgIHRoaXMuZGF0YSA9IGRhdGEucmVhbG07XHJcbiAgICAgIC8vIHRoaXMucmVhbG1fZGF0YSA9IGRhdGEucmVhbG1bMF0uX2lkO1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLnVzZXIpO1xyXG4gICAgICBpZiAocmVhbG1EYXRhKSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0UmVhbG0gPSB0aGlzLl9jb29raWVTZXJ2aWNlLmdldChcInJlYWxtTmFtZVwiKTtcclxuICAgICAgICB0aGlzLnJlYWxtX2RhdGEgPSByZWFsbURhdGE7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0UmVhbG0gPSB0aGlzLmRhdGFbMF0ubmFtZTtcclxuICAgICAgICB0aGlzLnJlYWxtX2RhdGEgPSB0aGlzLmRhdGFbMF0uX2lkO1xyXG4gICAgICB9XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwicmVhbG1cIiwgdGhpcy5yZWFsbV9kYXRhKTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJyZWFsbU5hbWVcIiwgdGhpcy5kZWZhdWx0UmVhbG0pO1xyXG4gICAgICBjb25zb2xlLmxvZyhcIj4+Pj4gcmVhbG1fZGF0YSB0b29sYmFyIG5nT25Jbml0IFwiLHRoaXMucmVhbG1fZGF0YSk7XHJcbiAgICAgIHRoaXMuZ2V0QWxsRGF0YXNvdXJjZSgpO1xyXG4gICAgICB0aGlzLmdldEFsbE5vdGlmaWNhdGlvbigpO1xyXG4gICAgfSk7XHJcbiAgICAvLyB0aGlzLmdldEFsbERhdGFzb3VyY2UoKTtcclxuICAgIC8vIHRoaXMuZ2V0QWxsTm90aWZpY2F0aW9uKCk7XHJcbiAgfVxyXG4gIGdldElkKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIkBAQEBcIiwgZXZlbnQpO1xyXG4gICAgdGhpcy5hY2NvdW50U2VydmljZS5jaGFuZ2VSZWFsbShldmVudCkuc3Vic2NyaWJlKFxyXG4gICAgICAoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJkYXRhXCIsIGRhdGEpO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiYWNjZXNzX3Rva2VuXCIpO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiYWNjZXNzX3Rva2VuXCIsIGRhdGEuYm9keS5yZXNwb25zZS5hY2Nlc3NfdG9rZW4pO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwicmVhbG1cIiwgZXZlbnQuX2lkKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJlYWxtTmFtZVwiLCBldmVudC5uYW1lKTtcclxuICAgICAgICB0aGlzLl9jb29raWVTZXJ2aWNlLnNldCgncmVhbG0nLCBldmVudC5faWQpOyAvLyBuZXcgb25lXHJcbiAgICAgICAgdGhpcy5fY29va2llU2VydmljZS5zZXQoJ3JlYWxtTmFtZScsIGV2ZW50Lm5hbWUpOyAvLyBuZXcgb25lXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJkYXRhc291cmNlXCIpO1xyXG4gICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZWxvYWQoKTtcclxuICAgICAgfSxcclxuICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiZXJyXCIsIGVycm9yKTtcclxuICAgICAgfVxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIGdldEFsbERhdGFzb3VyY2UoKVxyXG4gIHtcclxuICAgIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICByZWFsbSA6IHRoaXMucmVhbG1fZGF0YVxyXG4gICAgfTtcclxuICAgIGxldCBhcGlVcmwgPSBcIi9kYXRhc291cmNlcy9hbGxcIjtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHJlYWxtIGR0YWEgXCIsdGhpcy5yZWFsbV9kYXRhKTtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHF1ZXJ5IFwiLHF1ZXJ5KTtcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgdGhpcy5kYXRhU291cmNlID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzO1xyXG4gICAgICB2YXIgZGF0YVNvdXJjZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZVwiKTsgICAgLy8gY2hlY2sgb24gTG9jYWwgU3RvcmFnZVxyXG4gICAgICB0aGlzLmNvb2tpZURTID0gdGhpcy5fY29va2llU2VydmljZS5nZXQoXCJkYXRhc291cmNlXCIpOyAgICAvLyBjaGVjayBvbiBjb29raWVcclxuICAgICAgLy8gaWYoZGF0YVNvdXJjZSAmJiBkYXRhU291cmNlICE9ICdudWxsJykgXHJcbiAgICAgIC8vIHtcclxuICAgICAgLy8gICB0aGlzLnNlbGVjdGVkRGF0YXNvdXJjZUlkID0gZGF0YVNvdXJjZTtcclxuICAgICAgLy8gICB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZU5hbWVcIik7XHJcbiAgICAgIC8vICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIC8vIH0gZWxzZSBcclxuICAgICAgaWYgKHRoaXMuY29va2llRFMgJiYgdGhpcy5jb29raWVEUyAhPSAnbnVsbCcpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGF0YXNvdXJjZUlkID0gdGhpcy5jb29raWVEUztcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSA9IHRoaXMuX2Nvb2tpZVNlcnZpY2UuZ2V0KFwiZGF0YXNvdXJjZU5hbWVcIik7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSA9IHRoaXMuY29va2llRFM7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlXCIsIHRoaXMuc2VsZWN0ZWREYXRhc291cmNlSWQpO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZGF0YXNvdXJjZU5hbWVcIiwgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHZhciBkZWZhdWx0RGF0YXNvdXJjZUZpbHRlciA9IFtdO1xyXG4gICAgICAgIGRlZmF1bHREYXRhc291cmNlRmlsdGVyID0gXy5maWx0ZXIocmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzLCB7ZGVmYXVsdERhdGFzb3VyY2U6IHRydWV9KTtcclxuICAgICAgICBpZihkZWZhdWx0RGF0YXNvdXJjZUZpbHRlciAmJiBkZWZhdWx0RGF0YXNvdXJjZUZpbHRlci5sZW5ndGgpe1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZERhdGFzb3VyY2VJZCA9IGRlZmF1bHREYXRhc291cmNlRmlsdGVyWzBdLl9pZDtcclxuICAgICAgICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lID0gZGVmYXVsdERhdGFzb3VyY2VGaWx0ZXJbMF0ubmFtZTtcclxuICAgICAgICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSBkZWZhdWx0RGF0YXNvdXJjZUZpbHRlclswXS5faWQ7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImRhdGFzb3VyY2VcIiwgdGhpcy5zZWxlY3RlZERhdGFzb3VyY2VJZCk7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImRhdGFzb3VyY2VOYW1lXCIsIHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lKTtcclxuICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhc291cmNlSWQgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXNbMF0uX2lkO1xyXG4gICAgICAgICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXNbMF0ubmFtZTtcclxuICAgICAgICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXNbMF0uX2lkO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlXCIsIHRoaXMuc2VsZWN0ZWREYXRhc291cmNlSWQpO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlTmFtZVwiLCB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIFxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgY2xlYXJhbGxOb3RpZmljYXRpb24obm90aWZpY2F0aW9uTGlzdCkge1xyXG4gICAgbGV0IGFwaVVybCA9IFwiL25vdGlmaWNhdGlvbi91cGRhdGVBbGwvXCI7XHJcbiAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgIGlzcmVhZDogdHJ1ZSxcclxuICAgICAgZmVhdHVyZTogXCJpc3JlYWRcIixcclxuICAgICAgcmVhbG0gOiB0aGlzLnJlYWxtX2RhdGFcclxuICAgIH07XHJcbiAgICBjb25zb2xlLmxvZyhxdWVyeU9iaiwgXCIsLC4sLiwuLiwuXCIpXHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLnVwZGF0ZVJlcXVlc3QocXVlcnlPYmosIGFwaVVybCwgdHJ1ZSlcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGdldEFsbE5vdGlmaWNhdGlvbigpIHtcclxuICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgdW5yZWFkOiB0cnVlLFxyXG4gICAgICB0b29sYmFyOiB0cnVlLFxyXG4gICAgICByZWFsbSA6IHRoaXMucmVhbG1fZGF0YVxyXG4gICAgfTtcclxuICAgIGxldCBhcGlVcmwgPVwiL25vdGlmaWNhdGlvblwiO1xyXG4gICAgdGhpcy5ub3RpZmljYXRpb25EYXRhID0gW107XHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgIHRoaXMubm90aWZpY2F0aW9uTGlzdCA9IHJlcy5yZXNwb25zZS5ub3RpZmljYXRpb25MaXN0O1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICBfLmZvckVhY2godGhpcy5ub3RpZmljYXRpb25MaXN0LCBmdW5jdGlvbihsaXN0SXRlbSkge1xyXG4gICAgICAgIGxldCBsYXN0SW5kZXggPSBsaXN0SXRlbS5kYXRhID8gbGlzdEl0ZW0uZGF0YS5sZW5ndGggLSAxIDogMDtcclxuICAgICAgICBsZXQgaXRlbSA9IGxpc3RJdGVtLmRhdGFbbGFzdEluZGV4XTtcclxuICAgICAgICBjb25zb2xlLmxvZyhpdGVtLFwiPj4+Pj5pdGVtXCIpXHJcbiAgICAgICAgICBpdGVtLmZlYXR1cmUgPSBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgIFwiTk9USUZJQ0FUSU9OX0ZPUk1BVFMuRkVBVFVSRS5cIiArIGl0ZW0ubWVzc2FnZU9iai5mZWF0dXJlKTtcclxuICAgICAgICAgIGxldCBvcGVyYXRpb25TdGF0dXMgPSBpdGVtLm1lc3NhZ2VPYmouYWN0aW9uICsgXCJfXCIgKyBpdGVtLm1lc3NhZ2VPYmouc3RhdHVzO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJPcGVyYXRpb24gRGF0YVwiLCBvcGVyYXRpb25TdGF0dXMpO1xyXG4gICAgICAgICAgaXRlbS5zdGF0dXNfYWN0aW9uID0gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFwiTk9USUZJQ0FUSU9OX0ZPUk1BVFMuQUNUSU9OX1NUQVRVUy5cIiArIG9wZXJhdGlvblN0YXR1cyk7XHJcbiAgICAgICAgICBpdGVtLm1zZyA9IGl0ZW0uZmVhdHVyZSArIFwiICAgXCIgKyBpdGVtLnN0YXR1c19hY3Rpb247XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhpdGVtLFwiPj4+Pj4gaXRlbVwiKTtcclxuICAgICAgICAgIGl0ZW0uU3RhdHVzTmV3ID0gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICBcIk5PVElGSUNBVElPTl9GT1JNQVRTLlNUQVRVUy5cIiArIGl0ZW0ubWVzc2FnZU9iai5zdGF0dXMpO1xyXG4gICAgICAgICAgaXRlbS5kYXRlVGltZUFnbyA9IG1vbWVudChpdGVtLmNyZWF0ZWRfYXQpLmZyb21Ob3coKTtcclxuICAgICAgICAgIGl0ZW0ubmV3c3RyaW5nID0gaXRlbS51c2VyTmFtZS5jaGFyQXQoMCk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkNvbnZlcnQgaG91cnMgPj4+XCIsIGl0ZW0uZGF0ZVRpbWVBZ28pO1xyXG4gICAgICAgICAgaXRlbS5wZXJjZW50YWdlID0gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFwiTk9USUZJQ0FUSU9OX0ZPUk1BVFMuUFJPR1JFU1MuXCIgKyBpdGVtLm1lc3NhZ2VPYmouc3RhdHVzKTtcclxuICAgICAgICAgIGl0ZW0ub3BlcmF0aW9uU3RhdHVzID0gb3BlcmF0aW9uU3RhdHVzO1xyXG4gICAgICAgICAgbGlzdEl0ZW0gPSB7Li4ubGlzdEl0ZW0sIC4uLml0ZW19XHJcbiAgICAgICAgICBzZWxmLm5vdGlmaWNhdGlvbkRhdGEucHVzaChsaXN0SXRlbSk7XHJcbiAgICAgICAgICAvLyAkKFwiLnByb2dyZXNzLWJhci1zdHJpcGVkXCIpLndpZHRoKGl0ZW0ucGVyY2VudGFnZSk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICAgICAgdGhpcy5jb3VudCA9IHJlcy5yZXNwb25zZS50b3RhbDtcclxuICAgIH0pO1xyXG4gIH1cclxuICBuYXZpZ2F0ZVRvRG93bmxvYWRzKG1zZykge1xyXG4gICAgY29uc29sZS5sb2coXCJNZXNzYWdlIHZhbHVlID4+Pj4+PlwiLCBtc2cpO1xyXG4gICAgLy8gdGhpcy5jbG9zZVNpZGVOYXYoKTtcclxuICAgIC8vIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnL3BhZ2VzL2Rvd25sb2FkLXJlcG9ydCddKTtcclxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1jb25kaXRpb25hbC1hc3NpZ25tZW50XHJcbiAgICAvLyBpZiAobXNnLm1lc3NhZ2VPYmouZmVhdHVyZSA9ICdNQU5BR0VfUlVMRVNFVCcpIHtcclxuICAgIC8vICAgbGV0IG1haW5NZW51ID0gdGhpcy5fbWV0YURhdGEuZmlsdGVyKG9iaiA9PiBvYmpbXCJtZW51TmFtZVwiXSA9PSBcImNvbnRyb2xNYW5hZ2VtZW50XCIpO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyhcIj4+PiBNZXRhIGRhdGEgXCIsIHRoaXMuX21ldGFEYXRhKTtcclxuICAgIC8vICAgaWYobWFpbk1lbnUubGVuZ3RoID4gMClcclxuICAgIC8vICAgIHtcclxuICAgIC8vICAgICBsZXQgc3VibWVudXMgPSBtYWluTWVudVswXS5zdWJNZW51O1xyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKFwic3VibWVudSA+Pj4+PlwiLCBzdWJtZW51cyk7XHJcbiAgICAgICAgXHJcbiAgICAvLyAgICAgbGV0IHN1Yk1lbnUgPSBzdWJtZW51cy5maWx0ZXIob2JqID0+IG9ialtcIm1lbnVOYW1lXCJdID09IFwicnVsZXNldFwiKTtcclxuICAgIC8vICAgICBsZXQgbWVudUl0ZW0gPSBzdWJNZW51WzBdO1xyXG4gICAgLy8gICAgIGxldCB0ZW1wSXRlbSA9IHtcclxuICAgIC8vICAgICAgIGlkOiBtZW51SXRlbS5tZW51TmFtZSxcclxuICAgIC8vICAgICAgIHRpdGxlOiBtZW51SXRlbS50aXRsZSxcclxuICAgIC8vICAgICAgIHR5cGU6IFxyXG4gICAgLy8gICAgICAgICBtZW51SXRlbS5zdWJNZW51XHJcbiAgICAvLyAgICAgICAgICAgPyBcImNvbGxhcHNhYmxlXCJcclxuICAgIC8vICAgICAgICAgICA6IFwiaXRlbVwiLFxyXG4gICAgLy8gICAgICAgdXJsOiBtZW51SXRlbS5yb3V0aW5nVUksXHJcbiAgICAvLyAgICAgICBjb25maWdGaWxlUGF0aDogbWVudUl0ZW0uY29uZmlnRmlsZVBhdGhcclxuICAgIC8vICAgICB9O1xyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKFwiPj4+IG1lbnVJdGVtIFwiLCB0ZW1wSXRlbSk7XHJcbiAgICAvLyAgICAgdGhpcy5jbG9zZVNpZGVOYXYoKTtcclxuICAgIC8vICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKHRlbXBJdGVtKTtcclxuICAgIC8vICAgfVxyXG4gICAgLy8gfVxyXG4gICAgaWYgKG1zZy5hZGRpdGlvbmFsRGF0YSAmJiBtc2cuYWRkaXRpb25hbERhdGEubGVuZ3RoKSB7XHJcbiAgICAgIHZhciBkb3dubG9hZElkID0gbXNnLmFkZGl0aW9uYWxEYXRhWzBdLmluc3RhbmNlSWQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB2YXIgZG93bmxvYWRJZCA9IG51bGw7XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyhcIlJlcG9ydCBpZCA+Pj4+PlwiLCBkb3dubG9hZElkKTtcclxuICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgcmVwb3J0SWQ6IGRvd25sb2FkSWRcclxuICAgIH1cclxuICAgIGxldCBhcGlVcmwgPSAnL3JlcG9ydHMvZG93bmxvYWRSZXBvcnQnO1xyXG4gICAgaWYgKGRvd25sb2FkSWQgPT0gdW5kZWZpbmVkIHx8IGRvd25sb2FkSWQgPT0gbnVsbCB8fCBkb3dubG9hZElkID09ICcnKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiUmVwb3J0IElkIE51bGxcIik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgY29uc3QgYWIgPSBuZXcgQXJyYXlCdWZmZXIoZGF0YS5kYXRhLmxlbmd0aCk7XHJcbiAgICAgICAgY29uc3QgdmlldyA9IG5ldyBVaW50OEFycmF5KGFiKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGRhdGEuZGF0YS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgdmlld1tpXSA9IGRhdGEuZGF0YVtpXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IGRvd25sb2FkVHlwZSA9ICdhcHBsaWNhdGlvbi96aXAnO1xyXG4gICAgICAgIGNvbnN0IGZpbGUgPSBuZXcgQmxvYihbYWJdLCB7IHR5cGU6IGRvd25sb2FkVHlwZSB9KTtcclxuICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGZpbGUsIG1zZy5hZGRpdGlvbmFsRGF0YVswXS5uYW1lICsgJy56aXAnKTtcclxuICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIH1cclxuXHJcblxyXG5cclxuXHJcbiAgZ2V0TWV0YURhdGEoKSB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLl9tZXRhRGF0YSk7XHJcbiAgICBsZXQgbmF2aWdhdGlvbkxpc3QgPSBbXTtcclxuXHJcbiAgICBfLmZvckVhY2godGhpcy5fbWV0YURhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIHZhciBjaGlsZHJlbkxpc3QgPSBbXTtcclxuICAgICAgaWYgKGl0ZW0uc3ViTWVudSAmJiBpdGVtLnN1Yk1lbnUubGVuZ3RoKSB7XHJcbiAgICAgICAgXy5mb3JFYWNoKGl0ZW0uc3ViTWVudSwgZnVuY3Rpb24gKG1lbnVJdGVtKSB7XHJcbiAgICAgICAgICBsZXQgdGVtcEl0ZW0gPSB7XHJcbiAgICAgICAgICAgIGlkOiBtZW51SXRlbS5tZW51TmFtZSxcclxuICAgICAgICAgICAgdGl0bGU6IG1lbnVJdGVtLnRpdGxlLFxyXG4gICAgICAgICAgICB0eXBlOlxyXG4gICAgICAgICAgICAgIG1lbnVJdGVtLnN1Yk1lbnUgJiYgbWVudUl0ZW0uc3ViTWVudS5sZW5ndGhcclxuICAgICAgICAgICAgICAgID8gXCJjb2xsYXBzYWJsZVwiXHJcbiAgICAgICAgICAgICAgICA6IFwiaXRlbVwiLFxyXG4gICAgICAgICAgICB1cmw6IG1lbnVJdGVtLnJvdXRpbmdVSSxcclxuXHJcbiAgICAgICAgICAgIGNvbmZpZ0ZpbGVQYXRoOiBtZW51SXRlbS5jb25maWdGaWxlUGF0aFxyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgIGNoaWxkcmVuTGlzdC5wdXNoKHRlbXBJdGVtKTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG5cclxuICAgICAgbGV0IHRlbXAgPSB7XHJcbiAgICAgICAgaWQ6IGl0ZW0ubWVudU5hbWUsXHJcbiAgICAgICAgdGl0bGU6IGl0ZW0udGl0bGUsXHJcbiAgICAgICAgdHlwZTogaXRlbS5zdWJNZW51ICYmIGl0ZW0uc3ViTWVudS5sZW5ndGggPyBcImNvbGxhcHNhYmxlXCIgOiBcIml0ZW1cIixcclxuICAgICAgICB0cmFuc2xhdGU6IGl0ZW0udHJhbnNsYXRlLFxyXG4gICAgICAgIGljb246IGl0ZW0uaWNvbixcclxuICAgICAgICBjaGlsZHJlbjogY2hpbGRyZW5MaXN0LFxyXG4gICAgICAgIGNvbmZpZ0ZpbGVQYXRoOiBpdGVtLmNvbmZpZ0ZpbGVQYXRoXHJcbiAgICAgIH07XHJcbiAgICAgIG5hdmlnYXRpb25MaXN0LnB1c2godGVtcCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICByZXR1cm4gbmF2aWdhdGlvbkxpc3Q7XHJcbiAgfVxyXG4gIHNlbmRNZXNzYWdlKGlkLCB0cmlnZ2VyRnJvbSkge1xyXG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmRhdGFTb3VyY2UubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKGlkID09IHRoaXMuZGF0YVNvdXJjZVtpXS5faWQpIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImRhdGFzb3VyY2VOYW1lXCIsIHRoaXMuZGF0YVNvdXJjZVtpXS5uYW1lKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlXCIsIGlkKTtcclxuICAgIGlmKHRyaWdnZXJGcm9tKXtcclxuICAgICAgdGhpcy51cGRhdGVEYXRhc291cmNlKGlkKTtcclxuICAgIH1lbHNle1xyXG4gICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNZXNzYWdlKGlkKTtcclxuICAgIH1cclxuICB9XHJcbiAgdXBkYXRlRGF0YXNvdXJjZSAoaWQpe1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICAgIGRlZmF1bHREYXRhc291cmNlIDogdHJ1ZVxyXG4gICAgICB9O1xyXG4gICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgLnVwZGF0ZVJlcXVlc3QocXVlcnlPYmosJy9kYXRhc291cmNlcy9zZXREZWZhdWx0LycsIGlkKVxyXG4gICAgLnN1YnNjcmliZShcclxuICAgICAgcmVzID0+IHtcclxuICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNZXNzYWdlKGlkKTtcclxuICAgICAgfSxcclxuICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICB9XHJcbiAgICApO1xyXG4gICAgY29uc29sZS5sb2coJ2FjdGlvblJlZGlyZWN0ID4+Pj4+Pj4+Pj4+Pj4+PiBxdWVyeU9iaiAnLCBxdWVyeU9iailcclxuICB9XHJcbiAgcmVkaXJlY3RUb05vdGlmaWNhdGlvbigpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHJlZGlyZWN0VG8gTm90aWZpY2F0aW9uIGxpbmtcIik7XHJcbiAgICBsZXQgbWFpbk1lbnUgPSB0aGlzLl9tZXRhRGF0YS5maWx0ZXIob2JqID0+IG9ialtcIm1lbnVOYW1lXCJdID09IFwibWFuYWdlQWNjb3VudFwiKTtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IG1haW5NZW51IFwiLCBtYWluTWVudSk7XHJcbiAgICBpZihtYWluTWVudS5sZW5ndGg+MClcclxuICAgICB7XHJcbiAgICAgIGxldCBzdWJtZW51cyA9IG1haW5NZW51WzBdLnN1Yk1lbnU7XHJcbiAgICAgIGxldCBzdWJNZW51ID0gc3VibWVudXMuZmlsdGVyKG9iaiA9PiBvYmpbXCJtZW51SWRcIl0gPT0gXCJub3RpZmljYXRpb25cIik7XHJcbiAgICAgIGxldCBtZW51SXRlbSA9IHN1Yk1lbnVbMF07XHJcbiAgICAgIGxldCB0ZW1wSXRlbSA9IHtcclxuICAgICAgICBpZDogbWVudUl0ZW0ubWVudU5hbWUsXHJcbiAgICAgICAgdGl0bGU6IG1lbnVJdGVtLnRpdGxlLFxyXG4gICAgICAgIHR5cGU6XHJcbiAgICAgICAgICBtZW51SXRlbS5zdWJNZW51ICYmIG1lbnVJdGVtLnN1Yk1lbnUubGVuZ3RoXHJcbiAgICAgICAgICAgID8gXCJjb2xsYXBzYWJsZVwiXHJcbiAgICAgICAgICAgIDogXCJpdGVtXCIsXHJcbiAgICAgICAgdXJsOiBtZW51SXRlbS5yb3V0aW5nVUksXHJcbiAgICAgICAgY29uZmlnRmlsZVBhdGg6IG1lbnVJdGVtLmNvbmZpZ0ZpbGVQYXRoXHJcbiAgICAgIH07XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj4+IG1lbnVJdGVtIFwiLCB0ZW1wSXRlbSk7XHJcbiAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFJvdXRpbmcodGVtcEl0ZW0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5lbmFibGVOb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICB9XHJcbiAgb3VzaWRlQ2xpY2soZSkge1xyXG4gICAgdGhpcy5lbmFibGVOb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICB9XHJcbiAgUmVhZChpbnB1dCkge1xyXG4gICAgbGV0IGFwaVVybCA9IFwiL25vdGlmaWNhdGlvbi91cGRhdGVcIjtcclxuICAgIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgaXNyZWFkOiB0cnVlLFxyXG4gICAgICBmZWF0dXJlOiBcImlzcmVhZFwiLFxyXG4gICAgICBpZExpc3Q6IGlucHV0Ll9pZFxyXG4gICAgfTtcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChxdWVyeU9iaiwgYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5nZXRBbGxOb3RpZmljYXRpb24oKTtcclxuICAgIH0pO1xyXG4gICAgLy8gdGhpcy5uZ09uSW5pdCgpO1xyXG5cclxuICB9XHJcblxyXG5cclxuICBSZW1vdmUoaW5wdXQpIHtcclxuICAgIGxldCBhcGlVcmwgPSBcIi9ub3RpZmljYXRpb24vdXBkYXRlXCI7XHJcbiAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgIGZlYXR1cmU6IFwiZGVsZXRlXCIsXHJcbiAgICAgIGlkTGlzdDogaW5wdXQuX2lkXHJcbiAgICB9O1xyXG4gICAgdGhpcy5jb250ZW50U2VydmljZS5jcmVhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCBhcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG5cclxuXHJcbiAgLyoqXHJcbiAgICogT24gZGVzdHJveVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgLy8gVW5zdWJzY3JpYmUgZnJvbSBhbGwgc3Vic2NyaXB0aW9uc1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwubmV4dCgpO1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIFRvZ2dsZSBzaWRlYmFyIG9wZW5cclxuICAgKlxyXG4gICAqIEBwYXJhbSBrZXlcclxuICAgKi9cclxuICB0b2dnbGVTaWRlYmFyT3BlbihrZXkpOiB2b2lkIHtcclxuICAgIHRoaXMuX2Z1c2VTaWRlYmFyU2VydmljZS5nZXRTaWRlYmFyKGtleSkudG9nZ2xlT3BlbigpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2VhcmNoXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdmFsdWVcclxuICAgKi9cclxuICBzZWFyY2godmFsdWUpOiB2b2lkIHtcclxuICAgIC8vIERvIHlvdXIgc2VhcmNoIGhlcmUuLi5cclxuICAgIGNvbnNvbGUubG9nKHZhbHVlKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldCB0aGUgbGFuZ3VhZ2VcclxuICAgKlxyXG4gICAqIEBwYXJhbSBsYW5nXHJcbiAgICovXHJcbiAgc2V0TGFuZ3VhZ2UobGFuZyk6IHZvaWQge1xyXG4gICAgLy8gU2V0IHRoZSBzZWxlY3RlZCBsYW5ndWFnZSBmb3IgdGhlIHRvb2xiYXJcclxuICAgIHRoaXMuc2VsZWN0ZWRMYW5ndWFnZSA9IGxhbmc7XHJcblxyXG4gICAgLy8gVXNlIHRoZSBzZWxlY3RlZCBsYW5ndWFnZSBmb3IgdHJhbnNsYXRpb25zXHJcbiAgICB0aGlzLl90cmFuc2xhdGVTZXJ2aWNlLnVzZShsYW5nLmlkKTtcclxuICB9XHJcbiAgbG9nb3V0KCkge1xyXG4gICAgY29uc29sZS5sb2coXCJMb2dvdXQuLi5cIik7XHJcbiAgICB2YXIgZGF0YVNvdXJjZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZVwiKTtcclxuICAgIHZhciBkYXRhc291cmNlTmFtZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZU5hbWVcIik7XHJcbiAgICB0aGlzLl9jb29raWVTZXJ2aWNlLnNldCgnZGF0YXNvdXJjZScsIGRhdGFTb3VyY2UpO1xyXG4gICAgdGhpcy5fY29va2llU2VydmljZS5zZXQoJ2RhdGFzb3VyY2VOYW1lJywgZGF0YXNvdXJjZU5hbWUpO1xyXG4gICAgbG9jYWxTdG9yYWdlLmNsZWFyKCk7XHJcbiAgICBzZXNzaW9uU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgdGhpcy5vYXV0aFNlcnZpY2UubG9nT3V0KGZhbHNlKTtcclxuICB9XHJcbiAgY2hhbmdlUHdkKCkge1xyXG4gICAgLy8gd2luZG93LmxvY2F0aW9uLmhyZWYgPSAnaHR0cDovLzE5Mi4xNjguMi4xOTI6OTAxNC9jaGFuZ2UtcGFzc3dvcmQnO1xyXG4gICAgY29uc29sZS5sb2coXCJ1c2VyOjo6XCIgKyBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImFjY2Vzc190b2tlblwiKSk7XHJcbiAgICB2YXIgYWNjZXNzX3Rva2VuID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJhY2Nlc3NfdG9rZW5cIik7XHJcbiAgICB0aGlzLmFjY291bnRTZXJ2aWNlLmNoYW5nZVBhc3N3b3JkKGFjY2Vzc190b2tlbik7XHJcbiAgfVxyXG4gIC8vIHNpZGVOb2RpZmlcclxuICBvcGVuU2lkZU5vZGlmaU5hdihlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlLCBcImNoZWNrXCIpXHJcbiAgICB0aGlzLmVuYWJsZU5vdGlmaWNhdGlvbiA9ICF0aGlzLmVuYWJsZU5vdGlmaWNhdGlvbjtcclxuICB9XHJcbiAgY2xvc2VTaWRlTmF2KCkge1xyXG4gICAgdGhpcy5lbmFibGVOb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICB9XHJcbiAgY2xvc2UoKSB7XHJcbiAgICB0aGlzLmVuYWJsZU5vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gIH1cclxufVxyXG5cclxuXHJcbiJdfQ==