import { Directive, Host, Optional, Renderer2, Self, ViewContainerRef, Input } from "@angular/core";
import { MatPaginator } from "@angular/material";
var StylePaginatorDirective = /** @class */ (function () {
    function StylePaginatorDirective(matPag, vr, ren) {
        var _this = this;
        this.matPag = matPag;
        this.vr = vr;
        this.ren = ren;
        this._currentPage = 1;
        this._pageGapTxt = "...";
        this._buttons = [];
        this._showTotalPages = 2;
        //Sub to rerender buttons when next page and last page is used
        this.matPag.page.subscribe(function (v) {
            _this.switchPage(v.pageIndex);
        });
    }
    Object.defineProperty(StylePaginatorDirective.prototype, "showTotalPages", {
        get: function () { return this._showTotalPages; },
        set: function (value) {
            this._showTotalPages = value % 2 == 0 ? value + 1 : value;
        },
        enumerable: true,
        configurable: true
    });
    StylePaginatorDirective.prototype.buildPageNumbers = function () {
        var _this = this;
        var actionContainer = this.vr.element.nativeElement.querySelector("div.mat-paginator-range-actions");
        var nextPageNode = this.vr.element.nativeElement.querySelector("button.mat-paginator-navigation-next");
        var prevButtonCount = this._buttons.length;
        // remove buttons before creating new ones
        if (this._buttons.length > 0) {
            this._buttons.forEach(function (button) {
                _this.ren.removeChild(actionContainer, button);
            });
            //Empty state array
            this._buttons.length = 0;
        }
        //initialize next page and last page buttons
        if (this._buttons.length == 0) {
            var nodeArray_1 = this.vr.element.nativeElement.childNodes[0].childNodes[0]
                .childNodes[2].childNodes;
            setTimeout(function () {
                for (var i = 0; i < nodeArray_1.length; i++) {
                    if (nodeArray_1[i].nodeName === "BUTTON") {
                        if (nodeArray_1[i].disabled) {
                            _this.ren.setStyle(nodeArray_1[i], "background-color", "0 0");
                            _this.ren.setStyle(nodeArray_1[i], "color", "rgba(0,0,0,.26)");
                            _this.ren.setStyle(nodeArray_1[i], "margin", ".5%");
                        }
                        else {
                            _this.ren.setStyle(nodeArray_1[i], "background-color", "0 0");
                            _this.ren.setStyle(nodeArray_1[i], "color", "inherit");
                            _this.ren.setStyle(nodeArray_1[i], "margin", ".5%");
                        }
                    }
                }
            });
        }
        var dots = false;
        for (var i = 0; i < this.matPag.getNumberOfPages(); i = i + 1) {
            if ((i < this._showTotalPages && this._currentPage < this._showTotalPages && i > this._rangeStart) ||
                (i >= this._rangeStart && i <= this._rangeEnd)) {
                this.ren.insertBefore(actionContainer, this.createButton(i, this.matPag.pageIndex), nextPageNode);
            }
            else {
                if (i > this._rangeEnd && !dots) {
                    this.ren.insertBefore(actionContainer, this.createButton(this._pageGapTxt, this.matPag.pageIndex), nextPageNode);
                    dots = true;
                }
            }
        }
    };
    StylePaginatorDirective.prototype.createButton = function (i, pageIndex) {
        var _this = this;
        var linkBtn = this.ren.createElement("mat-button");
        this.ren.addClass(linkBtn, "mat-icon-button");
        this.ren.setStyle(linkBtn, "margin", "1%");
        var pagingTxt = isNaN(i) ? this._pageGapTxt : +(i + 1);
        var text = this.ren.createText(pagingTxt + "");
        this.ren.addClass(linkBtn, "mat-custom-page");
        switch (i) {
            case pageIndex:
                this.ren.setAttribute(linkBtn, "disabled", "disabled");
                break;
            case this._pageGapTxt:
                this.ren.listen(linkBtn, "click", function () {
                    _this.switchPage(_this._currentPage + _this._showTotalPages);
                });
                break;
            default:
                this.ren.listen(linkBtn, "click", function () {
                    _this.switchPage(i);
                });
                break;
        }
        this.ren.appendChild(linkBtn, text);
        //Add button to private array for state
        this._buttons.push(linkBtn);
        return linkBtn;
    };
    StylePaginatorDirective.prototype.initPageRange = function () {
        this._rangeStart = this._currentPage - this._showTotalPages / 2;
        this._rangeEnd = this._currentPage + this._showTotalPages / 2;
        this.buildPageNumbers();
    };
    StylePaginatorDirective.prototype.switchPage = function (i) {
        this._currentPage = i;
        this.matPag.pageIndex = i;
        this.initPageRange();
    };
    // public ngAfterViewChecked() {
    //   this.initPageRange();
    // }
    StylePaginatorDirective.prototype.ngAfterViewInit = function () {
        this.initPageRange();
    };
    StylePaginatorDirective.decorators = [
        { type: Directive, args: [{
                    selector: "[style-paginator]"
                },] }
    ];
    /** @nocollapse */
    StylePaginatorDirective.ctorParameters = function () { return [
        { type: MatPaginator, decorators: [{ type: Host }, { type: Self }, { type: Optional }] },
        { type: ViewContainerRef },
        { type: Renderer2 }
    ]; };
    StylePaginatorDirective.propDecorators = {
        showTotalPages: [{ type: Input }]
    };
    return StylePaginatorDirective;
}());
export { StylePaginatorDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUtcGFnaW5hdG9yLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ0YWJsZS1sYXlvdXQvc3R5bGUtcGFnaW5hdG9yLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBR0gsU0FBUyxFQUNULElBQUksRUFDSixRQUFRLEVBQ1IsU0FBUyxFQUNULElBQUksRUFDSixnQkFBZ0IsRUFDaEIsS0FBSyxFQUNOLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVqRDtJQWtCRSxpQ0FDK0MsTUFBb0IsRUFDekQsRUFBb0IsRUFDcEIsR0FBYztRQUh4QixpQkFTQztRQVI4QyxXQUFNLEdBQU4sTUFBTSxDQUFjO1FBQ3pELE9BQUUsR0FBRixFQUFFLENBQWtCO1FBQ3BCLFFBQUcsR0FBSCxHQUFHLENBQVc7UUFqQmhCLGlCQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBR3BCLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFRZCxvQkFBZSxHQUFHLENBQUMsQ0FBQztRQU8xQiw4REFBOEQ7UUFDOUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQUMsQ0FBQztZQUMzQixLQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQTtJQUNKLENBQUM7SUFqQkgsc0JBRU0sbURBQWM7YUFGcEIsY0FFaUMsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQzthQUM3RCxVQUFtQixLQUFhO1lBQzlCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUM1RCxDQUFDOzs7T0FINEQ7SUFpQnJELGtEQUFnQixHQUF4QjtRQUFBLGlCQXNFQztRQXJFQyxJQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUNqRSxpQ0FBaUMsQ0FDbEMsQ0FBQztRQUNGLElBQU0sWUFBWSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQzlELHNDQUFzQyxDQUN2QyxDQUFDO1FBQ0YsSUFBTSxlQUFlLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFFN0MsMENBQTBDO1FBQzFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzVCLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLFVBQUEsTUFBTTtnQkFDMUIsS0FBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsZUFBZSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELENBQUMsQ0FBQyxDQUFDO1lBQ0gsbUJBQW1CO1lBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztTQUMxQjtRQUVELDRDQUE0QztRQUM1QyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUM3QixJQUFJLFdBQVMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7aUJBQ3RFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7WUFDNUIsVUFBVSxDQUFDO2dCQUNULEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUN6QyxJQUFJLFdBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEtBQUssUUFBUSxFQUFFO3dCQUN0QyxJQUFJLFdBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7NEJBQ3pCLEtBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUNmLFdBQVMsQ0FBQyxDQUFDLENBQUMsRUFDWixrQkFBa0IsRUFDbEIsS0FBSyxDQUNOLENBQUM7NEJBQ0YsS0FBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDOzRCQUM1RCxLQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO3lCQUNsRDs2QkFBTTs0QkFDTCxLQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FDZixXQUFTLENBQUMsQ0FBQyxDQUFDLEVBQ1osa0JBQWtCLEVBQ2xCLEtBQUssQ0FDTixDQUFDOzRCQUNGLEtBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsU0FBUyxDQUFDLENBQUM7NEJBQ3BELEtBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQ2xEO3FCQUNGO2lCQUNGO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQztRQUVqQixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzdELElBQ0UsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsSUFBSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQzlGLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsRUFDOUM7Z0JBQ0EsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQ25CLGVBQWUsRUFDZixJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUMzQyxZQUFZLENBQ2IsQ0FBQzthQUNIO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQy9CLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUNuQixlQUFlLEVBQ2YsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLEVBQzFELFlBQVksQ0FDYixDQUFDO29CQUNGLElBQUksR0FBRyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVPLDhDQUFZLEdBQXBCLFVBQXFCLENBQU0sRUFBRSxTQUFpQjtRQUE5QyxpQkE2QkM7UUE1QkMsSUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUUzQyxJQUFNLFNBQVMsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDekQsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBRWpELElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQzlDLFFBQVEsQ0FBQyxFQUFFO1lBQ1QsS0FBSyxTQUFTO2dCQUNaLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7Z0JBQ3ZELE1BQU07WUFDUixLQUFLLElBQUksQ0FBQyxXQUFXO2dCQUNuQixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsT0FBTyxFQUFFO29CQUNoQyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUM1RCxDQUFDLENBQUMsQ0FBQztnQkFDSCxNQUFNO1lBQ1I7Z0JBQ0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE9BQU8sRUFBRTtvQkFDaEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDckIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsTUFBTTtTQUNUO1FBRUQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3BDLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1QixPQUFPLE9BQU8sQ0FBQztJQUNqQixDQUFDO0lBRU8sK0NBQWEsR0FBckI7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxDQUFDO1FBRTlELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFTyw0Q0FBVSxHQUFsQixVQUFtQixDQUFTO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELGdDQUFnQztJQUNoQywwQkFBMEI7SUFDMUIsSUFBSTtJQUNHLGlEQUFlLEdBQXRCO1FBQ0UsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7O2dCQXRKRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG1CQUFtQjtpQkFDOUI7Ozs7Z0JBSlEsWUFBWSx1QkFxQmhCLElBQUksWUFBSSxJQUFJLFlBQUksUUFBUTtnQkF4QjNCLGdCQUFnQjtnQkFGaEIsU0FBUzs7O2lDQWlCVixLQUFLOztJQTZJTiw4QkFBQztDQUFBLEFBdkpELElBdUpDO1NBcEpZLHVCQUF1QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgICBFbGVtZW50UmVmLFxyXG4gICAgQWZ0ZXJWaWV3SW5pdCxcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIEhvc3QsXHJcbiAgICBPcHRpb25hbCxcclxuICAgIFJlbmRlcmVyMixcclxuICAgIFNlbGYsXHJcbiAgICBWaWV3Q29udGFpbmVyUmVmLFxyXG4gICAgSW5wdXRcclxuICB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbiAgaW1wb3J0IHsgTWF0UGFnaW5hdG9yIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsXCI7XHJcbiAgXHJcbiAgQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogXCJbc3R5bGUtcGFnaW5hdG9yXVwiXHJcbiAgfSlcclxuICBleHBvcnQgY2xhc3MgU3R5bGVQYWdpbmF0b3JEaXJlY3RpdmUge1xyXG4gICAgcHJpdmF0ZSBfY3VycmVudFBhZ2UgPSAxO1xyXG4gICAgcHJpdmF0ZSBfcGFnZUdhcFR4dCA9IFwiLi4uXCI7XHJcbiAgICBwcml2YXRlIF9yYW5nZVN0YXJ0O1xyXG4gICAgcHJpdmF0ZSBfcmFuZ2VFbmQ7XHJcbiAgICBwcml2YXRlIF9idXR0b25zID0gW107XHJcbiAgXHJcbiAgQElucHV0KClcclxuICBcclxuICAgIGdldCBzaG93VG90YWxQYWdlcygpOiBudW1iZXIgeyByZXR1cm4gdGhpcy5fc2hvd1RvdGFsUGFnZXM7IH1cclxuICAgIHNldCBzaG93VG90YWxQYWdlcyh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgIHRoaXMuX3Nob3dUb3RhbFBhZ2VzID0gdmFsdWUgJSAyID09IDAgPyB2YWx1ZSArIDEgOiB2YWx1ZTtcclxuICAgIH1cclxuICAgIHByaXZhdGUgX3Nob3dUb3RhbFBhZ2VzID0gMjtcclxuICBcclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICBASG9zdCgpIEBTZWxmKCkgQE9wdGlvbmFsKCkgcHJpdmF0ZSByZWFkb25seSBtYXRQYWc6IE1hdFBhZ2luYXRvcixcclxuICAgICAgcHJpdmF0ZSB2cjogVmlld0NvbnRhaW5lclJlZixcclxuICAgICAgcHJpdmF0ZSByZW46IFJlbmRlcmVyMlxyXG4gICAgKSB7XHJcbiAgICAgIC8vU3ViIHRvIHJlcmVuZGVyIGJ1dHRvbnMgd2hlbiBuZXh0IHBhZ2UgYW5kIGxhc3QgcGFnZSBpcyB1c2VkXHJcbiAgICAgIHRoaXMubWF0UGFnLnBhZ2Uuc3Vic2NyaWJlKCh2KT0+e1xyXG4gICAgICAgIHRoaXMuc3dpdGNoUGFnZSh2LnBhZ2VJbmRleCk7XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcbiAgXHJcbiAgICBwcml2YXRlIGJ1aWxkUGFnZU51bWJlcnMoKSB7XHJcbiAgICAgIGNvbnN0IGFjdGlvbkNvbnRhaW5lciA9IHRoaXMudnIuZWxlbWVudC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXHJcbiAgICAgICAgXCJkaXYubWF0LXBhZ2luYXRvci1yYW5nZS1hY3Rpb25zXCJcclxuICAgICAgKTtcclxuICAgICAgY29uc3QgbmV4dFBhZ2VOb2RlID0gdGhpcy52ci5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcihcclxuICAgICAgICBcImJ1dHRvbi5tYXQtcGFnaW5hdG9yLW5hdmlnYXRpb24tbmV4dFwiXHJcbiAgICAgICk7XHJcbiAgICAgIGNvbnN0IHByZXZCdXR0b25Db3VudCA9IHRoaXMuX2J1dHRvbnMubGVuZ3RoO1xyXG4gIFxyXG4gICAgICAvLyByZW1vdmUgYnV0dG9ucyBiZWZvcmUgY3JlYXRpbmcgbmV3IG9uZXNcclxuICAgICAgaWYgKHRoaXMuX2J1dHRvbnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgIHRoaXMuX2J1dHRvbnMuZm9yRWFjaChidXR0b24gPT4ge1xyXG4gICAgICAgICAgdGhpcy5yZW4ucmVtb3ZlQ2hpbGQoYWN0aW9uQ29udGFpbmVyLCBidXR0b24pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vRW1wdHkgc3RhdGUgYXJyYXlcclxuICAgICAgICB0aGlzLl9idXR0b25zLmxlbmd0aCA9IDA7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgLy9pbml0aWFsaXplIG5leHQgcGFnZSBhbmQgbGFzdCBwYWdlIGJ1dHRvbnNcclxuICAgICAgaWYgKHRoaXMuX2J1dHRvbnMubGVuZ3RoID09IDApIHtcclxuICAgICAgICBsZXQgbm9kZUFycmF5ID0gdGhpcy52ci5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuY2hpbGROb2Rlc1swXS5jaGlsZE5vZGVzWzBdXHJcbiAgICAgICAgICAuY2hpbGROb2Rlc1syXS5jaGlsZE5vZGVzO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBub2RlQXJyYXkubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKG5vZGVBcnJheVtpXS5ub2RlTmFtZSA9PT0gXCJCVVRUT05cIikge1xyXG4gICAgICAgICAgICAgIGlmIChub2RlQXJyYXlbaV0uZGlzYWJsZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucmVuLnNldFN0eWxlKFxyXG4gICAgICAgICAgICAgICAgICBub2RlQXJyYXlbaV0sXHJcbiAgICAgICAgICAgICAgICAgIFwiYmFja2dyb3VuZC1jb2xvclwiLFxyXG4gICAgICAgICAgICAgICAgICBcIjAgMFwiXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW4uc2V0U3R5bGUobm9kZUFycmF5W2ldLCBcImNvbG9yXCIsIFwicmdiYSgwLDAsMCwuMjYpXCIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW4uc2V0U3R5bGUobm9kZUFycmF5W2ldLCBcIm1hcmdpblwiLCBcIi41JVwiKTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW4uc2V0U3R5bGUoXHJcbiAgICAgICAgICAgICAgICAgIG5vZGVBcnJheVtpXSxcclxuICAgICAgICAgICAgICAgICAgXCJiYWNrZ3JvdW5kLWNvbG9yXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiMCAwXCJcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJlbi5zZXRTdHlsZShub2RlQXJyYXlbaV0sIFwiY29sb3JcIiwgXCJpbmhlcml0XCIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZW4uc2V0U3R5bGUobm9kZUFycmF5W2ldLCBcIm1hcmdpblwiLCBcIi41JVwiKTsgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICBcclxuICAgICAgbGV0IGRvdHMgPSBmYWxzZTtcclxuICBcclxuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm1hdFBhZy5nZXROdW1iZXJPZlBhZ2VzKCk7IGkgPSBpICsgMSkge1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgIChpIDwgdGhpcy5fc2hvd1RvdGFsUGFnZXMgJiYgdGhpcy5fY3VycmVudFBhZ2UgPCB0aGlzLl9zaG93VG90YWxQYWdlcyAmJiBpID4gdGhpcy5fcmFuZ2VTdGFydCkgfHxcclxuICAgICAgICAgIChpID49IHRoaXMuX3JhbmdlU3RhcnQgJiYgaSA8PSB0aGlzLl9yYW5nZUVuZClcclxuICAgICAgICApIHtcclxuICAgICAgICAgIHRoaXMucmVuLmluc2VydEJlZm9yZShcclxuICAgICAgICAgICAgYWN0aW9uQ29udGFpbmVyLFxyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUJ1dHRvbihpLCB0aGlzLm1hdFBhZy5wYWdlSW5kZXgpLFxyXG4gICAgICAgICAgICBuZXh0UGFnZU5vZGVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmIChpID4gdGhpcy5fcmFuZ2VFbmQgJiYgIWRvdHMpIHtcclxuICAgICAgICAgICAgdGhpcy5yZW4uaW5zZXJ0QmVmb3JlKFxyXG4gICAgICAgICAgICAgIGFjdGlvbkNvbnRhaW5lcixcclxuICAgICAgICAgICAgICB0aGlzLmNyZWF0ZUJ1dHRvbih0aGlzLl9wYWdlR2FwVHh0LCB0aGlzLm1hdFBhZy5wYWdlSW5kZXgpLFxyXG4gICAgICAgICAgICAgIG5leHRQYWdlTm9kZVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBkb3RzID0gdHJ1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICBcclxuICAgIHByaXZhdGUgY3JlYXRlQnV0dG9uKGk6IGFueSwgcGFnZUluZGV4OiBudW1iZXIpOiBhbnkge1xyXG4gICAgICBjb25zdCBsaW5rQnRuID0gdGhpcy5yZW4uY3JlYXRlRWxlbWVudChcIm1hdC1idXR0b25cIik7XHJcbiAgICAgIHRoaXMucmVuLmFkZENsYXNzKGxpbmtCdG4sIFwibWF0LWljb24tYnV0dG9uXCIpO1xyXG4gICAgICB0aGlzLnJlbi5zZXRTdHlsZShsaW5rQnRuLCBcIm1hcmdpblwiLCBcIjElXCIpO1xyXG4gIFxyXG4gICAgICBjb25zdCBwYWdpbmdUeHQgPSBpc05hTihpKSA/IHRoaXMuX3BhZ2VHYXBUeHQgOiArKGkgKyAxKTtcclxuICAgICAgY29uc3QgdGV4dCA9IHRoaXMucmVuLmNyZWF0ZVRleHQocGFnaW5nVHh0ICsgXCJcIik7XHJcbiAgXHJcbiAgICAgIHRoaXMucmVuLmFkZENsYXNzKGxpbmtCdG4sIFwibWF0LWN1c3RvbS1wYWdlXCIpO1xyXG4gICAgICBzd2l0Y2ggKGkpIHtcclxuICAgICAgICBjYXNlIHBhZ2VJbmRleDpcclxuICAgICAgICAgIHRoaXMucmVuLnNldEF0dHJpYnV0ZShsaW5rQnRuLCBcImRpc2FibGVkXCIsIFwiZGlzYWJsZWRcIik7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgICBjYXNlIHRoaXMuX3BhZ2VHYXBUeHQ6XHJcbiAgICAgICAgICB0aGlzLnJlbi5saXN0ZW4obGlua0J0biwgXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc3dpdGNoUGFnZSh0aGlzLl9jdXJyZW50UGFnZSArIHRoaXMuX3Nob3dUb3RhbFBhZ2VzKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgIHRoaXMucmVuLmxpc3RlbihsaW5rQnRuLCBcImNsaWNrXCIsICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zd2l0Y2hQYWdlKGkpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBicmVhaztcclxuICAgICAgfVxyXG4gIFxyXG4gICAgICB0aGlzLnJlbi5hcHBlbmRDaGlsZChsaW5rQnRuLCB0ZXh0KTtcclxuICAgICAgLy9BZGQgYnV0dG9uIHRvIHByaXZhdGUgYXJyYXkgZm9yIHN0YXRlXHJcbiAgICAgIHRoaXMuX2J1dHRvbnMucHVzaChsaW5rQnRuKTtcclxuICAgICAgcmV0dXJuIGxpbmtCdG47XHJcbiAgICB9XHJcbiAgXHJcbiAgICBwcml2YXRlIGluaXRQYWdlUmFuZ2UoKTogdm9pZCB7XHJcbiAgICAgIHRoaXMuX3JhbmdlU3RhcnQgPSB0aGlzLl9jdXJyZW50UGFnZSAtIHRoaXMuX3Nob3dUb3RhbFBhZ2VzIC8gMjtcclxuICAgICAgdGhpcy5fcmFuZ2VFbmQgPSB0aGlzLl9jdXJyZW50UGFnZSArIHRoaXMuX3Nob3dUb3RhbFBhZ2VzIC8gMjtcclxuICBcclxuICAgICAgdGhpcy5idWlsZFBhZ2VOdW1iZXJzKCk7XHJcbiAgICB9XHJcbiAgXHJcbiAgICBwcml2YXRlIHN3aXRjaFBhZ2UoaTogbnVtYmVyKTogdm9pZCB7XHJcbiAgICAgIHRoaXMuX2N1cnJlbnRQYWdlID0gaTtcclxuICAgICAgdGhpcy5tYXRQYWcucGFnZUluZGV4ID0gaTtcclxuICAgICAgdGhpcy5pbml0UGFnZVJhbmdlKCk7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAvLyBwdWJsaWMgbmdBZnRlclZpZXdDaGVja2VkKCkge1xyXG4gICAgLy8gICB0aGlzLmluaXRQYWdlUmFuZ2UoKTtcclxuICAgIC8vIH1cclxuICAgIHB1YmxpYyBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICAgIHRoaXMuaW5pdFBhZ2VSYW5nZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuICAiXX0=