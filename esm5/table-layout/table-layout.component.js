import * as tslib_1 from "tslib";
import { Component, ViewChild, Input, Output, EventEmitter, Inject, ChangeDetectorRef } from "@angular/core";
import { MatPaginator, MatTableDataSource, MatDialog, MatCheckbox, MatTable } from "@angular/material";
import { SnackBarService } from "./../shared/snackbar.service";
import { animate, state, style, transition, trigger } from '@angular/animations';
import * as FileSaver from "file-saver";
import { FormControl } from "@angular/forms";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import * as _ from "lodash";
import { ContentService } from "../content/content.service";
import { MessageService } from "../_services/index";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from '../loader.service';
var TableLayoutComponent = /** @class */ (function () {
    function TableLayoutComponent(_fuseTranslationLoaderService, contentService, messageService, _matDialog, snackBarService, changeDetectorRef, environment, english, loaderService) {
        var _this = this;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.contentService = contentService;
        this.messageService = messageService;
        this._matDialog = _matDialog;
        this.snackBarService = snackBarService;
        this.changeDetectorRef = changeDetectorRef;
        this.environment = environment;
        this.english = english;
        this.loaderService = loaderService;
        this.disablePagination = false;
        this.checkClickEventMessage = new EventEmitter();
        this.actionClickEvent = new EventEmitter();
        this.enableSelectOption = false;
        this.enableSearch = false;
        this.tableData = {};
        this.enableAction = false;
        this.limit = 10;
        this.offset = 0;
        this.selectedData = [];
        this.inputData = {};
        this.unsubscribe = new Subject();
        this.unsubscribeModel = new Subject();
        this.currentFilteredValue = {};
        this.enableOptionFilter = false;
        this.enableUIfilter = false;
        this.disablePaginator = false;
        this.redirectUri = this.environment.redirectUri;
        this._fuseTranslationLoaderService.loadTranslations(english);
        /* // temporarily commented
        this.messageService.modelCloseMessage
          .pipe(takeUntil(this.unsubscribeModel))
          .subscribe(data => {
            console.log(data, ">>>>data")
            this.selectedData = [];
            if (data != 0) {
              this.inputData["selectedData"] = [];
              this.inputData["selectedIdList"] = [];
              this.inputData["selectAll"] = false;
            }
            console.log(this.tableData);
            console.log(this.dataSource);
            // _.forEach(this.tableData, function(item) {
            //   item.checked = false;
            // });
    
            // this.dataSource.data.map(obj => {
            //   obj.checked = false;
            // });
            // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
            // // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            // this.checkClickEventMessage.emit("clicked");
            this.data = data;
            // if(this.data === 'listView'){
            if (this.data) {
              this.currentConfigData = JSON.parse(
                localStorage.getItem("currentConfigData")
              );
              this.ngOnInit();
            }
            // }
          });
          */
        this.messageService.getMessage().subscribe(function (message) {
            _this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            if (_this.currentConfigData &&
                _this.currentConfigData.listView &&
                _this.currentConfigData.listView.enableTableLayout) {
                _this.ngOnInit();
            }
        });
        this.messageService
            .getTableHeaderUpdate()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(function (data) {
            _this.updateTableView();
        });
    }
    TableLayoutComponent.prototype.ngOnInit = function () {
        // this.changeDetectorRef.detectChanges();
        this.inputData["selectAll"] = false;
        console.log(this, "......table this");
        this.defaultDatasource = localStorage.getItem("datasource");
        this.queryParams = {
            offset: 0,
            limit: 10,
            datasource: this.defaultDatasource
        };
        //  this.paginator["pageIndex"] = 0;
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        // if (this.data == 'listView' || this.data == 'refreshPage') {
        //   this.inputData["selectedData"] = [];
        //   this.inputData["selectedIdList"] = [];
        // }
        this.inputData["datasourceId"] = this.defaultDatasource;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        // this.changeDetectorRef.detectChanges();
        this.fromFieldValue = new FormControl([""]);
        this.toFieldValue = new FormControl([""]);
        this.onLoadData(null);
    };
    TableLayoutComponent.prototype.ngAfterViewInit = function () {
        console.log("ngAfterViewInit ");
        console.log(this.selectedPaginator, "selectedPag>>>>");
        if (this.dataSource && this.selectedPaginator) {
            this.dataSource.paginator = this.selectedPaginator;
        }
    };
    TableLayoutComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
        this.unsubscribeModel.next();
    };
    TableLayoutComponent.prototype.updateTableView = function () {
        var currentTableHeader = JSON.parse(localStorage.getItem("selectedTableHeaders"));
        this.columns = currentTableHeader;
        this.displayedColumns = this.columns
            .filter(function (val) {
            return val.isActive;
        })
            .map(function (val) {
            return val.value;
        });
    };
    TableLayoutComponent.prototype.onLoadData = function (enableNext) {
        this.currentFilteredValue = !_.isEmpty(this.currentFilteredValue) ? this.currentFilteredValue : {};
        this.columns = [];
        this.displayedColumns = [];
        var tableArray = this.onLoad
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        var responseKey = this.onLoad && this.onLoad.responseKey ? this.onLoad.responseKey : null;
        var index = _.findIndex(tableArray, { tableId: this.tableId });
        //if(index>-1)
        this.currentTableData = tableArray[index];
        console.log(tableArray, ".....tableArray");
        console.log(this.currentTableData, ".....currentTableData");
        var checkOptionFilter = (this.currentTableData && this.currentTableData.enableOptionsFilter) ? true : false;
        if (checkOptionFilter) {
            var self = this;
            this.enableOptionFilter = true;
            var Optiondata = this.currentTableData.filterOptionLoadFunction;
            this.optionFilterFields = this.currentTableData.optionFields;
            var apiUrl = Optiondata.apiUrl;
            var responseName = Optiondata.response;
            var self = this;
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(function (res) {
                var tempArray = (res.response[responseName]) ? res.response[responseName] : [];
                self.optionFilterdata = tempArray;
            });
        }
        if (this.currentTableData && this.currentTableData.enableUifilter) {
            var filterKey = this.currentTableData.uiFilterKey;
            this.enableUIfilter = true;
            // this.dataSource.filterPredicate = (data: Element, filter: string) => {
            //   return data[filterKey] == filter;
            //  };
        }
        // if(this.onLoad && this.onLoad.getFromcurrentInput){
        if (this.onLoad && this.currentTableData.checkFromInputData) {
            var temp = localStorage.getItem("currentInput");
            this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
            var data = (this.currentTableData.keyTosave && this.inputData[this.currentTableData.keyTosave]) ? this.inputData[this.currentTableData.keyTosave] : [];
            if (data.length > 0) {
                this.onLoad.response = data;
                this.onLoad.total = data.length;
            }
        }
        console.log(">>>> this.onLoad ", this.onLoad);
        if (this.onLoad && this.onLoad.response) {
            this.getLoadData(this.currentTableData, this.onLoad.response, this.onLoad.total, responseKey);
        }
        else {
            if (this.currentTableData)
                this.getData(this.currentTableData);
        }
        if (!enableNext) {
            // this.paginator.firstPage();
        }
    };
    TableLayoutComponent.prototype.getData = function (currentTableValue) {
        var _this = this;
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.columns = currentTableValue.tableHeader;
        if (currentTableValue.onLoadFunction) {
            var apiUrl = currentTableValue.onLoadFunction.apiUrl;
            var responseName = currentTableValue.onLoadFunction.response;
            var keyToSet_1 = currentTableValue.onLoadFunction.keyForStringResponse;
            this.currentData = this.currentConfigData["listView"];
            var includeRequest = currentTableValue.onLoadFunction.includerequestData ? true : false;
            //if ( currentTableValue.onLoadFunction.enableLoader) {
            this.loaderService.startLoader();
            //}
            var dynamicLoad = currentTableValue.onLoadFunction.dynamicReportLoad; // condition checked when implement additional report view
            var requestedQuery = currentTableValue.onLoadFunction.requestData;
            var self = this;
            var requestQueryParam = {};
            if (dynamicLoad) {
                var tempObj = localStorage.getItem("CurrentReportData");
                var reportITem_1 = JSON.parse(tempObj);
                _.forEach(requestedQuery, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else {
                        requestQueryParam[requestItem.name] = (reportITem_1[requestItem.value]) ? reportITem_1[requestItem.value] : requestItem.value;
                    }
                });
            }
            else {
                _.forEach(requestedQuery, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else if (requestItem.directAssign) {
                        self.queryParams[requestItem.name] = requestItem.convertToString
                            ? JSON.stringify(requestItem.value)
                            : requestItem.value;
                    }
                    else {
                        self.queryParams[requestItem.name] = requestItem.convertToString
                            ? JSON.stringify(self.inputData[requestItem.value])
                            : self.inputData[requestItem.value];
                    }
                });
            }
            this.queryParams = tslib_1.__assign({}, this.queryParams, requestQueryParam);
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(function (data) {
                // if ( currentTableValue.onLoadFunction.enableLoader) {
                _this.loaderService.stopLoader();
                //}
                _this.enableSearch = _this.currentData.enableGlobalSearch;
                _this.displayedColumns = _this.columns
                    .filter(function (val) {
                    return val.isActive;
                })
                    .map(function (val) {
                    return val.value;
                });
                var tempArray = [];
                var self = _this;
                console.log(self, ">>>>>>>>>>THISSSS");
                _.forEach(data.response[responseName], function (item, index) {
                    if (typeof item !== "object") {
                        var tempObj = {};
                        tempObj[keyToSet_1] = item;
                        var filterObj = {};
                        filterObj[currentTableValue.filterKey] = tempObj[currentTableValue.dataFilterKey];
                        var selectedValue = _.find(self.inputData[currentTableValue.selectedValuesKey], filterObj);
                        if (selectedValue) {
                            tempObj['checked'] = true;
                        }
                        tempArray.push(tempObj);
                    }
                    else {
                        var tempObj_1 = {};
                        _.forEach(currentTableValue.dataViewFormat, function (viewItem) {
                            if (viewItem.subkey) {
                                if (viewItem.assignFirstIndexval) {
                                    var resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                    tempObj_1[viewItem.name] = item[viewItem.value]
                                        ? resultValue
                                        : "";
                                }
                                else {
                                    tempObj_1[viewItem.name] = item[viewItem.value]
                                        ? item[viewItem.value][viewItem.subkey]
                                        : "";
                                }
                            }
                            else if (viewItem.isCondition) {
                                tempObj_1[viewItem.name] = eval(viewItem.condition);
                            }
                            else if (viewItem.isAddDefaultValue) {
                                tempObj_1[viewItem.name] = viewItem.value;
                            }
                            else {
                                tempObj_1[viewItem.name] = item[viewItem.value];
                            }
                            if (currentTableValue.loadLabelFromConfig) {
                                tempObj_1[viewItem.name] =
                                    currentTableValue.headerList[item[viewItem.value]];
                            }
                        });
                        var filterObj = {};
                        filterObj[currentTableValue.filterKey] = item[currentTableValue.dataFilterKey];
                        var selectedValue = _.find(self.inputData[currentTableValue.selectedValuesKey], filterObj);
                        if (selectedValue) {
                            item.checked = true;
                        }
                        tempObj_1 = tslib_1.__assign({}, item, tempObj_1);
                        tempArray.push(tempObj_1);
                    }
                });
                if (tempArray && tempArray.length) {
                    _this.tableData = tempArray;
                }
                else {
                    _this.tableData =
                        data && data.response && data.response[responseName]
                            ? data.response[responseName]
                            : [];
                }
                if (data && data.response) {
                    _this.length = data.response.total;
                }
                _this.dataSource = new MatTableDataSource(_this.tableData);
                // this.dataSource.paginator = this.selectedPaginator; // newly added 
                localStorage.setItem("currentInput", JSON.stringify(_this.inputData));
                _this.inputData["selectAll"] = false;
                var filteredKey = (_this.currentFilteredValue.searchKey) ? _this.currentFilteredValue.searchKey : '';
                _.forEach(self.columns, function (x) {
                    if (filteredKey == x.value) {
                        self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                    }
                    else {
                        self.inputData[x.keyToSave] = null;
                    }
                });
                // this.selectAllCheck({ checked: false });
                // this.selectAll = false;
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.inputData["selectedData"] = [];
                // this.inputData["selectedIdList"] = [];
                // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.changeDetectorRef.detectChanges();
                _this.checkClickEventMessage.emit("clicked");
            }, function (err) {
                _this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
    };
    TableLayoutComponent.prototype.getLoadData = function (currentValue, responseValue, totalLength, responseKey) {
        console.log(currentValue, "....currentValue");
        var tempArray = [];
        // if (responseValue && responseValue[responseKey]) {
        responseValue = responseKey ? responseValue[responseKey] : responseValue;
        if (responseValue && responseValue.length) {
            _.forEach(responseValue, function (item, index) {
                if (typeof item !== "object") {
                    var tempObj = {};
                    tempObj[currentValue.keyToSet] = item;
                    tempArray.push(tempObj);
                }
                else {
                    var tempObj_2 = {};
                    _.forEach(currentValue.dataViewFormat, function (viewItem) {
                        if (viewItem.subkey) {
                            if (viewItem.assignFirstIndexval) {
                                var resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                tempObj_2[viewItem.name] = item[viewItem.value]
                                    ? resultValue
                                    : "";
                            }
                            else {
                                tempObj_2[viewItem.name] = item[viewItem.value]
                                    ? item[viewItem.value][viewItem.subkey]
                                    : "";
                            }
                        }
                        else {
                            tempObj_2[viewItem.name] = item[viewItem.value];
                        }
                    });
                    tempObj_2 = tslib_1.__assign({}, item, tempObj_2);
                    tempArray.push(tempObj_2);
                }
            });
        }
        this.columns = currentValue.tableHeader;
        responseValue = tempArray.length ? tempArray : responseValue;
        console.log(this.inputData, "...INPUTDATA");
        if (currentValue && currentValue.mapDataFunction) {
            var self_1 = this;
            _.forEach(currentValue.mapDataFunction, function (mapItem) {
                _.forEach(mapItem.requestData, function (requestItem) {
                    self_1.queryParams[requestItem.name] = requestItem.convertToString
                        ? JSON.stringify(self_1.inputData[requestItem.value])
                        : self_1.inputData[requestItem.value];
                });
                self_1.contentService
                    .getAllReponse(self_1.queryParams, mapItem.apiUrl)
                    .subscribe(function (data) {
                    console.log(mapItem, ">>>>>>> MAP ITEM");
                    console.log(data, ".....DATAAAA");
                    var tempObj = _.keyBy(data.response[mapItem.response], "_id");
                    console.log(tempObj, "....tempObj");
                    _.forEach(responseValue, function (responseItem) {
                        if (data.response &&
                            data.response.keyForCheck == mapItem.keyForCheck &&
                            responseItem[mapItem.keyForCheck]) {
                            responseItem[mapItem.showKey] =
                                tempObj && responseItem[mapItem.keyForCheck]
                                    ? tempObj[responseItem[mapItem.keyForCheck]].objectTypes
                                    : [];
                            responseItem[mapItem.modelKey] = responseItem[mapItem.showKey];
                        }
                        else
                            console.log(responseItem, ".....responseItem");
                    });
                    console.log(responseValue, ">>>>>responseValue");
                });
            });
        }
        else {
            this.dataSource = new MatTableDataSource(responseValue);
            this.length = totalLength ? totalLength : responseValue.length;
        }
        this.tableData = tempArray;
        this.dataSource = new MatTableDataSource(responseValue);
        this.length = totalLength ? totalLength : responseValue.length;
        console.log(this.columns, ">>>>>COLUMNSS");
        // this.displayedColumns = _.map(this.columns, "value");
        this.displayedColumns = this.columns
            .filter(function (val) {
            return val.isActive;
        })
            .map(function (val) {
            return val.value;
        });
        this.dataSource.paginator = this.selectedPaginator;
        console.log(this.selectedPaginator, ".................... selectedPaginator");
        console.log(this.dataSource, ".........DATASOURCEEEE");
        this.disablePaginator = currentValue.disablePagination ? currentValue.disablePagination : false;
        //paginatorEnable
        // this.dataSource = responseKey
        //   ? new MatTableDataSource(responseValue[responseKey])
        //   : new MatTableDataSource(responseValue);
    };
    TableLayoutComponent.prototype.updateCheckClick = function (selected, event) {
        // selected.type = this.currentTableData.tableType
        //   ? this.currentTableData.tableType
        //   : "";
        if (this.currentTableData && this.currentTableData.tableType) {
            selected.type = this.currentTableData.tableType;
        }
        // selected.typeId = this.currentTableData.typeId
        //   ? this.currentTableData.typeId
        //   : "";
        var securityTypeLength = this.currentTableData.securityTypeLength ? Number(this.currentTableData.securityTypeLength) : 3;
        console.log("this.currentTableData.securityTypeLength????", this.currentTableData.securityTypeLength);
        console.log(">>> securityTypeLengthsss ", securityTypeLength);
        // if (selected && selected.typeId) {
        //   if (selected.typeId == "2") {
        //     selected["security_type"] = [4];
        //     selected["level"] = [4];
        //   } else {
        //     selected["security_type"] = [1, 2, 3];
        //     selected["level"] = [1, 2, 3];
        //    }
        var tempArray = [];
        var predefinedArray = this.currentTableData.securityArrayValue ? JSON.parse("[" + this.currentTableData.securityArrayValue + "]") : tempArray;
        console.log(">>> predefinedArray ", predefinedArray);
        if (securityTypeLength && securityTypeLength > 0) {
            console.log("inside");
            if (predefinedArray && predefinedArray.length) {
                tempArray = predefinedArray;
            }
            else {
                for (var i = 1; i <= securityTypeLength; i++) {
                    tempArray.push(i);
                }
            }
            selected["security_type"] = tempArray;
        }
        else {
            console.log("outside");
            if (securityTypeLength == 0) {
                selected["security_type"] = tempArray;
            }
            else {
                selected["security_type"] = [1, 2, 3];
            }
        }
        //}
        console.log(">>>  selected security_type ", selected);
        if (this.currentTableData && this.currentTableData.dataFormatToSave) {
            _.forEach(this.currentTableData.dataFormatToSave, function (item) {
                selected[item.name] = selected[item.value];
            });
        }
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        var savedSelectedData = this.inputData.selectedData && this.inputData.selectedData.length
            ? this.inputData.selectedData
            : [];
        this.selectedData = _.merge(savedSelectedData, this.selectedData);
        if (event.checked) {
            this.selectedData.push(selected);
        }
        else {
            // let index = this.selectedData.findIndex(
            //   obj => obj.object_name === selected.object_name
            // );
            var objectKey_1 = this.currentTableData.objectKeyToCheck ? this.currentTableData.objectKeyToCheck : "obj_name";
            var index = this.selectedData.findIndex(function (obj) { return obj[objectKey_1] === selected[objectKey_1]; });
            this.selectedData.splice(index, 1);
        }
        console.log(">>>> this.selectedData ", this.selectedData);
        this.inputData["selectedData"] = this.selectedData;
        if (this.currentTableData && this.currentTableData.saveInArray) {
            var arrayObj = this.currentTableData.saveInArray;
            if (this.currentTableData.tableId === arrayObj.key) {
                var tempArray_1 = this.selectedData || this.selectedData.length
                    ? _.map(this.selectedData, arrayObj.valueToMap)
                    : [];
                tempArray_1 = tempArray_1.filter(function (element) {
                    return element != null;
                });
                this.inputData[arrayObj.key] = tempArray_1;
            }
        }
        // check box remain search column value start
        var filteredKey = (this.currentFilteredValue.searchKey) ? this.currentFilteredValue.searchKey : '';
        if (filteredKey) {
            var self = this;
            _.forEach(self.columns, function (x) {
                if (filteredKey == x.value) {
                    self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                }
                else {
                    self.inputData[x.keyToSave] = null;
                }
            });
        }
        // check box remain search column value start
        this.inputData["selectedIdList"] = _.map(this.selectedData, "_id");
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    };
    TableLayoutComponent.prototype.selectAllCheck = function (event) {
        if (event.checked === true) {
            this.dataSource.data.map(function (obj) {
                obj.checked = true;
            });
            this.inputData["selectedData"] = this.dataSource.data;
            this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        }
        else {
            this.dataSource.data.map(function (obj) {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    };
    TableLayoutComponent.prototype.addFieldClick = function (item, objectList) {
        console.log(item, "..........ITEM");
        console.log(this.inputData, ".......INPUT DAta");
        console.log(objectList, "......ObjectList");
    };
    TableLayoutComponent.prototype.selectAllData = function () {
        var _this = this;
        var requestDetails = this.currentData.selectAllRequest;
        var query = {};
        var self = this;
        _.forEach(requestDetails.requestData, function (item) {
            query[item.name] = self.inputData[item.value];
        });
        this.contentService
            .getAllReponse(query, requestDetails.apiUrl)
            .subscribe(function (data) {
            _this.inputData["selectedIdList"] =
                data.response[requestDetails.responseName];
            localStorage.setItem("currentInput", JSON.stringify(_this.inputData));
        });
    };
    TableLayoutComponent.prototype.clearSelectAllData = function () {
        this.ngOnInit();
    };
    TableLayoutComponent.prototype.applyFilter = function (event, key) {
        console.log(">>>>>>>>>> event ", event, " key ", key);
        this.currentFilteredValue = {
            searchKey: key,
            searchValue: event
        };
        console.log(">>> this.currentFilteredValue  ", this.currentFilteredValue);
        this.inputData[key] = event; // searched value set
        var tableArray = this.onLoad && this.onLoad.tableData
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        var index = _.findIndex(tableArray, { tableId: this.tableId });
        var searchRequest = tableArray[index].onTableSearch;
        var request = searchRequest.requestData;
        var isUiserach = (searchRequest.uiSearch) ? true : false;
        if (isUiserach) {
            this.dataSource.filter = event;
        }
        else {
            this.currentTableData = tableArray[index];
            var query_1 = {
                offset: 0,
                limit: 10,
                datasource: this.defaultDatasource
            };
            if (searchRequest && searchRequest.isSinglerequest) {
                query_1[searchRequest.singleRequestKey] = event;
            }
            else {
                var self = this;
                _.forEach(request, function (item) {
                    if (self.inputData[item.name])
                        query_1[item.value] = self.inputData[item.name]; // onTable Search Value passed instead of name 
                });
            }
            this.queryParams = query_1;
            this.getData(this.currentTableData);
        }
    };
    TableLayoutComponent.prototype.applyFilterSearch = function (eventItem, key) {
        console.log(">>> applyFilterSearch");
        eventItem.preventDefault();
        var event = eventItem.target.value;
        this.currentFilteredValue = {};
        var temp = localStorage.getItem("currentInput");
        console.log(">>>>>>>>>> event ", event, " key ", key);
        this.currentFilteredValue = {
            searchKey: key,
            searchValue: event
        };
        this.inputData[key] = event; // searched value set
        var tableArray = this.onLoad && this.onLoad.tableData
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        var index = _.findIndex(tableArray, { tableId: this.tableId });
        var searchRequest = tableArray[index].onTableSearch;
        var request = searchRequest.requestData;
        var isUiserach = (searchRequest.uiSearch) ? true : false;
        if (isUiserach) {
            this.dataSource.filter = event;
        }
        else {
            this.currentTableData = tableArray[index];
            var query_2 = {
                offset: 0,
                limit: 10,
                datasource: this.defaultDatasource
            };
            console.log(">>>> searchRequest ", searchRequest);
            if (searchRequest && searchRequest.isSinglerequest) {
                query_2[searchRequest.singleRequestKey] = event;
            }
            else {
                var self = this;
                _.forEach(request, function (item) {
                    if (self.inputData[item.name])
                        query_2[item.value] = self.inputData[item.name]; // onTable Search Value passed instead of name 
                });
            }
            this.queryParams = query_2;
            this.getData(this.currentTableData);
        }
    };
    TableLayoutComponent.prototype.filterReset = function (field) {
        delete this.queryParams[field.requestKey];
        this.ngOnInit();
    };
    TableLayoutComponent.prototype.onMultiSelect = function (event, rowVal) {
        var temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        var selectedData = this.inputData.selectedData;
        var index = _.findIndex(selectedData, { obj_name: rowVal.obj_name });
        if (index > -1 && selectedData && selectedData.length) {
            this.inputData.selectedData[index].security_type = event.value;
            this.inputData.selectedData[index].level = event.value;
            this.inputData.selectedData[index].objectLevel = event.value;
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    };
    TableLayoutComponent.prototype.changePage = function (event) {
        if (event.pageSize != this.limit) {
            this.queryParams["limit"] = event.pageSize;
            this.queryParams["offset"] = 0;
        }
        else {
            this.queryParams["offset"] = event.pageIndex * this.limit;
            this.queryParams["limit"] = this.limit;
        }
        this.onLoadData("next");
    };
    TableLayoutComponent.prototype.viewData = function (element, col) {
        console.log(col, "viewData >>>>>>>>> isRedirect ", col.isRedirect, this.environment);
        if (col.isRedirect) {
            var testStr = "/control-management/access-control";
            window.location.href = "" + this.redirectUri + testStr;
            console.log("Redirected ********************");
            // this.route.navigateByUrl("/control-management/access-control");
        }
        else {
            this.actionClick(element, "view");
        }
    };
    TableLayoutComponent.prototype.addButtonClick = function (element, action) {
        console.log("addButtonClick ");
        console.log(" element ", element);
        console.log(" action ", action);
    };
    TableLayoutComponent.prototype.actionClick = function (element, action) {
        console.log(element, ">>>>element");
        if (action == "edit" || action == "view" || action == "info") {
            this.openDialog(element, action);
        }
        else if (action == "delete") {
            console.log(this, ">>>>>>>this");
            if (this.onLoad && this.onLoad.inputKey) {
                var inputKey = this.onLoad.inputKey;
                if (this.inputData && this.inputData[inputKey]) {
                    var selectedData = this.inputData[inputKey];
                    // let index = selectedData.findIndex(
                    //   obj => obj == element
                    // );
                    var index = _.findIndex(selectedData, element);
                    console.log(">>>> delete index ", index);
                    if (index > -1) {
                        selectedData.splice(index, 1);
                    }
                    this.inputData[inputKey] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    this.actionClickEvent.emit("clicked");
                }
            }
            else {
                if (this.inputData && this.inputData.selectedData) {
                    var objectKey_2 = this.currentTableData.objectKeyToCheck ? this.currentTableData.objectKeyToCheck : "obj_name";
                    console.log(">>>> objectKey ", objectKey_2);
                    var selectedData = this.inputData.selectedData;
                    // let index = selectedData.findIndex(
                    //   obj => obj.object_name == element.object_name
                    // );
                    var index = selectedData.findIndex(function (obj) { return obj[objectKey_2] == element[objectKey_2]; });
                    selectedData.splice(index, 1);
                    this.inputData["selectedData"] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    this.actionClickEvent.emit("clicked");
                }
            }
        }
        else if (action == "remove_red_eye") {
            this.openDialog(element, "view");
        }
        else if (action == "restore_page") {
            this.openDialog(element, action);
        }
        else if (action == 'askdefaultbutton') {
            var dsid = element._id;
            var dsname = element.name;
            console.log(" dsid ", dsid, " dsname ", dsname);
            this.messageService.sendDatasource(dsid);
            this.snackBarService.add(this._fuseTranslationLoaderService.instant("Datasource Default Updated Successfully!"));
        }
        else if (action == "save_alt") {
            var requestDetails = this.currentData.downloadRequest;
            var query = {
                reportId: element._id
            };
            this.contentService
                .getAllReponse(query, requestDetails.apiUrl)
                .subscribe(function (data) {
                var ab = new ArrayBuffer(data.data.length);
                var view = new Uint8Array(ab);
                for (var i = 0; i < data.data.length; i++) {
                    view[i] = data.data[i];
                }
                var downloadType = (element.fileType == "CSV") ? "text/csv" : "text/xlsx";
                var file = new Blob([ab], { type: downloadType });
                FileSaver.saveAs(file, element.fileName + element.fileExtension);
            });
        }
    };
    TableLayoutComponent.prototype.actionRedirect = function (element, columnData) {
        var _this = this;
        console.log("actionRedirect >>>>>> ", element);
        var requestDetails = this.currentTableData;
        var queryObj = {};
        var self = this;
        console.log("requestDetails >>>>>> ", requestDetails);
        if (columnData.rulesetCheck) {
            var toastMessageDetails_1 = requestDetails.onTableUpdate.toastMessage;
            _.forEach(requestDetails.onTableUpdate.requestData, function (item) {
                queryObj[item.name] = item.subkey
                    ? element[item.value][item.subkey]
                    : element[item.value];
            });
            self.contentService
                .updateRequest(queryObj, requestDetails.onTableUpdate.apiUrl, element._id)
                .subscribe(function (res) {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_1.success));
                _this.ngOnInit();
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_1.error));
            });
            console.log('actionRedirect >>>>>>>>>>>>>>> queryObj ', queryObj);
        }
        else {
        }
    };
    TableLayoutComponent.prototype.onToggleChange = function (event, element) {
        console.log(">>>>>>>>>>>> event ", event, " :element ", element);
        element.status = event.checked ? "ACTIVE" : "INACTIVE";
        var requestDetails = this.currentTableData
            ? this.currentTableData.onToggleChange
            : "";
        if (requestDetails) {
            var toastMessageDetails_2 = requestDetails.toastMessage;
            var self = this;
            var queryObj_1 = {};
            _.forEach(requestDetails.requestData, function (item) {
                queryObj_1[item.name] = item.subkey
                    ? element[item.value][item.subkey]
                    : element[item.value];
            });
            self.contentService
                .updateRequest(queryObj_1, requestDetails.apiUrl, element._id)
                .subscribe(function (res) {
                if (element.status == 'ACTIVE' && toastMessageDetails_2.enable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.enable));
                }
                else if (element.status == 'INACTIVE' && toastMessageDetails_2.disable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.disable));
                }
                else {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.success));
                }
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.error));
            });
        }
        else if (this.currentTableData && this.currentTableData.saveselectedToggle) {
            this.inputData[this.currentTableData["keyTosave"]] = this.dataSource.data;
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    };
    TableLayoutComponent.prototype.openDialog = function (element, action) {
        var _this = this;
        console.log(" Open Dialog ********* element: ", element);
        var modelWidth = this.currentConfigData[action].modelData.size;
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                savedData: element
            }
        })
            .afterClosed()
            .subscribe(function (response) {
            localStorage.removeItem("currentInput");
            _this.messageService.sendModelCloseEvent("listView");
        });
    };
    TableLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "table-layout",
                    template: "<ng-container *ngIf=\"inputData.selectAll && length > 10\">\r\n\r\n  <div *ngIf=\"inputData.selectAll && length > 10 && (inputData.selectedIdList.length != length)\" class=\"header-top header selectall-export\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n    fxlayoutalign=\"space-between center\">\r\n    <!-- <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\"> -->\r\n    <span class=\"m-0\">{{currentData.selectAllText | translate}}\r\n      <button mat-button color=\"accent\" (click)=\"selectAllData()\">Select All {{length}}\r\n        {{currentData.selectAccesGroup}}</button>\r\n      </span>\r\n    <!-- </div> -->\r\n  </div>\r\n  <div *ngIf=\"inputData.selectAll && length > 10 && (inputData.selectedIdList.length == length)\" class=\"header-top header selectall-export\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n    fxlayoutalign=\"space-between center\">\r\n    <span class=\"m-0\">All {{length}} {{currentData.afterSelectAllText}}\r\n      <button mat-button color=\"accent\" (click)=\"clearSelectAllData()\">Clear Selection</button></span>\r\n  </div>\r\n\r\n</ng-container>\r\n\r\n<!--  dropdown filter start-->\r\n\r\n<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\" *ngIf=\"enableOptionFilter\">\r\n\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n  </div>\r\n  <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"margin-top: -39px;margin-bottom: -26px;\">\r\n    <div *ngFor=\"let optionField of optionFilterFields\">\r\n    <!-- <button mat-raised-button class=\"warn ml-4 mt-8\" style=\"float:right\" (click)=\"filterReset(optionField)\">\r\n      Reset\r\n    </button> -->\r\n    <button mat-raised-button class=\"mat-raised-button ml-4 mt-8\" style=\"float:right\" (click)=\"filterReset(optionField)\">\r\n     <mat-icon>autorenew</mat-icon>\r\n    </button>\r\n    <mat-form-field appearance=\"outline\" class=\"right\" style=\"float:right;\">\r\n      <mat-label>{{optionField.label| translate}}</mat-label>\r\n      <mat-select name=\"data_source\" (selectionChange)=\"applyFilter($event.value,optionField.keyToSave)\" placeholder=\"'Select Data Source'\">\r\n        <mat-option *ngFor=\"let item of optionFilterdata\" value=\"{{item[optionField.keyToSave] | translate}}\">{{item[optionField.keyToShow] | translate}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    </div>\r\n  </div>\r\n \r\n</div> <!--  dropdown filter end -->\r\n<!-- input search filter start -->\r\n<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\" *ngIf=\"enableUIfilter\">\r\n  <div class=\"example-header\">\r\n    <mat-form-field style=\"padding-left: 10px;\">\r\n      <input matInput (keyup)=\"applyFilter($event.target.value,'')\" placeholder=\"Filter\">\r\n    </mat-form-field>\r\n  </div>\r\n</div>\r\n\r\n<!-- input search filter end -->\r\n\r\n<!-- <p>this.data Before : {{this.data}} - condition :{{(this.data=='listView'||this.data=='refreshPage'||this.data==0)}}</p>  -->  \r\n<div class=\"content p-12\" fusePerfectScrollbar >\r\n  <div class=\"res-table\">\r\n    <table mat-table [dataSource]=\"dataSource\" multiTemplateDataRows>\r\n      <ng-container *ngFor=\"let column of columns; let i = index\" [matColumnDef]=\"column.value\">\r\n        <th mat-header-cell *matHeaderCellDef class=\"tbl-header-style heading-label\">\r\n          <span [ngClass]=\"{'selectTH' : column.enableSelectAll}\">{{ column.name | translate}}</span>\r\n          <div class=\"tabheade\"  *ngIf=\"column.enableSelectAll\">\r\n            <mat-checkbox [(ngModel)]=\"inputData.selectAll\" (change)=\"selectAllCheck($event)\">\r\n            </mat-checkbox>\r\n          </div>\r\n          <div class=\"tabheade\" *ngIf=\"column.enableSearch\">\r\n            <mat-form-field appearance=\"outline\" fxFlex=\"80\" class=\"custom-mat-form\">\r\n              <mat-label>{{'SEARCH.TABLE_PLACE_HOLDER_LBL' | translate}}</mat-label>\r\n              <input matInput [(ngModel)]=\"inputData[column.keyToSave]\"\r\n                (keydown.enter)=\"applyFilterSearch($event, column.value)\"  placeholder=\"Search\"\r\n                [disabled]=\"column.disableSearch\" />\r\n            </mat-form-field>\r\n          </div>\r\n        </th>\r\n      \r\n      \r\n        <td mat-cell *matCellDef=\"let row\">\r\n         <!-- <p> row value {{row | json}} </p>  -->\r\n          <!-- <span>{{ row[column.defaultVal] }}- ROW</span>\r\n          <span>{{row[column.value] }}- COLUMN</span> \r\n          <span>{{column.defaultVal &&  row[column.defaultVal] && row[column.value]=='Imported'}} - INPUTDATA</span> -->\r\n          <span *ngIf=\"column.value=='issuetype'\" [ngClass]=\"{'bg-warning':row[column.value]=='warning','bg-failed':row[column.value]=='danger','bg-success':row[column.value]=='success'}\" class=\"ng-star-inserted \">\r\n            {{row[column.value]=='warning'?'MEDIUM':''}}\r\n            {{row[column.value]=='danger'?'HIGH':''}}\r\n            {{row[column.value]=='success'?'SUCCESS':''}}\r\n           </span> \r\n           <span *ngIf=\"column.isNotArray\">\r\n            {{column.data[row[column.value]] | translate}} \r\n          </span>\r\n\r\n          <span *ngIf=\"column.value=='errorDescription'  && row.total\">\r\n            <mat-panel-title>\r\n              Total:&nbsp;{{row.total}}&nbsp;&nbsp;&nbsp;SaveCount:&nbsp;{{row.saveCount}}&nbsp;&nbsp;&nbsp;FailedCount:&nbsp;{{row.failedCount}}&nbsp;&nbsp;&nbsp;DuplicateCount:&nbsp;{{row.duplicateCount}}\r\n            </mat-panel-title>\r\n          </span>\r\n          <span *ngIf=\"column.value=='errorDescription' && !row.total\">\r\n            {{row[column.value]}}\r\n          </span>\r\n\r\n          <span *ngIf=\"column.value == 'select'\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"updateCheckClick(row, $event)\"\r\n              [checked]=\"row.checked\">\r\n            </mat-checkbox>\r\n          </span>\r\n          <span *ngIf=\"column.isStopDialog\" class=\"view-mode\">{{row[column.value]}}</span>\r\n        \r\n          <span *ngIf=\"column.isViewMode\" class=\"view-mode\" (click)=\"viewData(row,column)\">{{row[column.value]}}</span>\r\n          <span *ngIf=\"(column.value !== 'security_type' && column.value!== 'object_type' && column.value !== 'level')  \r\n            && column.type !== 'date' && !column.isViewMode && !column.showStatusToggle && !column.isStopDialog\r\n            && !column.mapFromConfig && column.value!=='issuetype' && column.value!=='securityType' && !column.expandCollapse && column.value!=='errorDescription'\" \r\n            [ngClass]=\"(row.statusClass && row.statusClass[row[column.value]] )? row.statusClass[row[column.value]] : ''\">\r\n            {{row[column.value]}}\r\n          </span>\r\n          <span *ngIf=\"column.expandCollapse\" [ngClass]=\"(row.expandCollapse) ? 'view-mode' : ''\" >{{row[column.value]}}</span>\r\n          <span *ngIf=\"column.type == 'date'\">{{row[column.value] | date:'short'}}</span>\r\n          <span *ngIf=\"column.showStatusToggle\">\r\n            <mat-slide-toggle fxFlex class=\"mat-accent\" aria-label=\"Cloud\" (change)=\"onToggleChange($event, row)\"\r\n              [checked]=\"(row[column.value] == 'ACTIVE' || row[column.value] === true)? true : false\">\r\n            </mat-slide-toggle>\r\n          </span>\r\n          <span *ngIf=\"column.value == 'action'\">\r\n            <!-- <span> Condition1 - {{column.condition | translate}}</span>\r\n            <span>R1 -{{column.condition}} </span> -->\r\n            <button *ngFor=\"let button of column.buttons\" mat-icon-button color=\"accent\"\r\n              (click)=\"actionClick(row, button)\">\r\n              <mat-icon *ngIf=\"column.isCondition && button=='save_alt' && (row.status=='Completed')\">{{button}}</mat-icon>\r\n              <mat-icon *ngIf=\"!column.isCondition\">{{button}}</mat-icon>\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.value == 'object_type'\">\r\n            <mat-form-field class=\"multi-select-dropdown\">\r\n              <mat-select [(ngModel)]=\"column.keyToShow ? row[column.keyToShow] : row[column.value]\"\r\n                (selectionChange)=\"onMultiSelect($event, row)\" multiple>\r\n                <mat-option *ngFor=\"let item of row.objectTypes\" [value]=\"item\">{{item}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </span>\r\n          <ng-container *ngIf=\"column.value == 'status' && column.conditioncheck && !column.rulesetCheck && row[column.value]==column.conditioncheck && inputData.datasourceId!=row._id\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-intiated ng-star-inserted reimport \"\r\n            (click)=\"actionClick(row,'askdefaultbutton' )\" >\r\n                 Set as Default\r\n               <!-- {{row[column.value]}} -->\r\n          </span> \r\n          </ng-container>\r\n          <ng-container *ngIf=\"column.value == 'status' && column.conditioncheck && column.rulesetCheck && row[column.value]==column.conditioncheck && !(column.defaultVal &&  row[column.defaultVal])\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-intiated ng-star-inserted reimport \"\r\n            (click)=\"actionRedirect(row,column)\" >\r\n                 Set as Default\r\n               <!-- {{row[column.value]}} -->\r\n          </span> \r\n          </ng-container>\r\n            <ng-container *ngIf=\"(column.value == 'status' || (column.defaultVal &&  row[column.defaultVal]))&& column.conditioncheck && row[column.value]==column.conditioncheck && (inputData.datasourceId==row._id || (column.defaultVal &&  row[column.defaultVal]))\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-success ng-star-inserted reimport \" \r\n            >\r\n                Default\r\n          </span> \r\n          </ng-container> \r\n          <!-- column.defaultVal &&  row[column.defaultVal] && row[column.value]=='Imported' -->\r\n          <!-- <ng-container *ngIf=\"column.value == 'status' && column.defaultVal =='defaultRuleset' && column.conditioncheck && row[column.value]==column.conditioncheck && inputData.rulesetId==row.defaultRuleset\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-success ng-star-inserted reimport \"\r\n            >\r\n                Default\r\n          </span> \r\n          </ng-container>  -->\r\n\r\n          <span *ngIf=\"column.value == 'reimport'\" class=\"label bg-success ng-star-inserted reimport\"\r\n            (click)=\"actionClick(row,'edit' )\">\r\n            Re-import\r\n          </span>\r\n          <span *ngIf=\"(column.value == 'security_type'|| column.value == 'level') && row['type'] != 'Role' && !column.mapFromConfig\" >\r\n            <mat-form-field class=\"multi-select-dropdown\" *ngIf=\"column.showOnlyselectedObj\">\r\n              <mat-select [(ngModel)]=\"row[column.value]\" (selectionChange)=\"onMultiSelect($event, row)\" multiple>\r\n                <mat-option *ngFor=\"let item of column[row[column.mapbyObject]]\" [value]=\"item.value\">{{item.name | translate}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"multi-select-dropdown\" *ngIf=\"!column.showOnlyselectedObj\">\r\n              <mat-select [(ngModel)]=\"row[column.value]\" (selectionChange)=\"onMultiSelect($event, row)\" multiple>\r\n                <mat-option *ngFor=\"let item of column.data\" [value]=\"item.value\">{{item.name | translate}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </span>\r\n          <span *ngIf=\"column.isArray && !column.fromElement\">\r\n            <ng-container *ngFor=\"let data of row[column.keyToShow]; let i=index\">\r\n              {{column.data[data] | translate}}<span *ngIf=\"row[column.keyToShow].length-1>i\">, </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"column.isArray && column.fromElement\">\r\n            <ng-container *ngFor=\"let data of row[column.keyToShow]; let i=index\">\r\n              {{data}}<span *ngIf=\"row[column.keyToShow].length-1>i\">, </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"column.isArryObj\">\r\n            <ng-container *ngFor=\"let data of row[column.keyToShow]; let i=index\">\r\n              {{data[column.subKey] | translate }}<span *ngIf=\"row[column.keyToShow].length-1>i\">, </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"column.mapFromConfig\">\r\n            {{column.mapData[row[column.value]] | translate}}\r\n          </span>\r\n          <!-- <span *ngIf=\"column.value == 'role_type' || column.value=='role_desc'\">{{row[column.value]}}</span> -->\r\n          <!-- <span *ngIf=\"column.type == 'date'\">{{row[column.value] | date}}</span> -->\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"expandedDetail\">\r\n        <td mat-cell *matCellDef=\"let element\" [attr.colspan]=\"displayedColumns.length\">\r\n           <div\r\n              class=\"example-element-detail\"\r\n              [@detailExpand]=\"element == expandedElement ? 'expanded' : 'collapsed'\"\r\n              *ngFor=\"let elementItem of element[element.expandElementKey]\"\r\n           >\r\n              <div class=\"example-element-description\" *ngIf=\"element.expandCollapse\">\r\n                 <ul>\r\n                    <li>\r\n                       {{ elementItem[element.keyToShowExpand] }}\r\n                    </li>\r\n                 </ul>\r\n              </div>\r\n           </div>\r\n        </td>\r\n     </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr\r\n               mat-row\r\n               *matRowDef=\"let element; columns: displayedColumns\"\r\n               class=\"example-element-row\"\r\n               [class.example-expanded-row]=\"expandedElement === element\"\r\n               (click)=\"expandedElement = element\"\r\n            ></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\"  class=\"example-detail-row\"></tr>\r\n    </table>\r\n    <div [hidden]=\"tableData.length==0\">    \r\n     <mat-paginator #paginator *ngIf=\"!disablePagination&& !disablePaginator && viewFrom!='view';else select_Paginator\" [pageSizeOptions]=\"[10,25,50,100]\" [length]=\"length\"\r\n        [pageSize]=\"limit\" showFirstLastButtons (page)=\"changePage($event)\">\r\n      </mat-paginator>\r\n      <ng-template #select_Paginator>\r\n        <mat-paginator  *ngIf=\"!disablePaginator\" #selectedPaginator [pageSizeOptions]=\"[10,25,50,100]\" [length]=\"length\"\r\n        [pageSize]=\"limit\" showFirstLastButtons (page)=\"changePage($event)\">\r\n        </mat-paginator>\r\n     </ng-template> \r\n    </div>\r\n    \r\n    <div class=\"p-10\" style=\"    text-align: center;\r\n    \" *ngIf=\"tableData.length==0\">\r\n      <h2 class=\"m-0 font-weight-900 noRecords\">No Records Found</h2>\r\n\r\n    </div>\r\n  </div>\r\n  <!-- <div *ngIf=\"!length\">\r\n    <div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n    font-size: large;\">\r\n        No Reports Found!\r\n    </div>\r\n  </div> -->\r\n</div>\r\n\r\n<!-- How to include this directive -->\r\n\r\n<!-- <table-layout [enableSearch]= \"true\" [tableData] = \"contentData\" [displayedColumns] = \"displayedColumns\"></table-layout> -->\r\n",
                    animations: [
                        trigger('detailExpand', [
                            state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
                            state('expanded', style({ height: '*' })),
                            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                        ]),
                    ],
                    styles: [".custom-mat-form{display:inline!important}.tabheade{padding-top:3px}.heading-label{font-size:14px;padding-top:10px}tr.example-detail-row{height:0}tr.example-element-row:not(.example-expanded-row):hover{background:#f5f5f5}tr.example-element-row:not(.example-expanded-row):active{background:#efefef}.res-table{height:100%;overflow-x:auto}.mat-table-sticky{background:#59abfd;opacity:1}.mat-cell,.mat-footer-cell,.mat-header-cell{min-width:80px;box-sizing:border-box}.mat-footer-row,.mat-header-row,.mat-row{min-width:1920px}table{width:100%;padding-top:10px}.mat-form-field-wrapper{padding-bottom:10px}.mat-header-cell{font-weight:700!important;color:#000!important}.selectall-export{text-align:center;margin:0 1%;padding:4px;font-size:14px;border-radius:4px;background:#ececeaab}.multi-select-dropdown{min-width:0;width:180px!important}.selectBoxStyle{font-size:50px!important}.view-mode{color:#039be5;font-size:14px;font-weight:600;cursor:pointer}.selectTH{position:absolute;top:31px}.noRecords{padding:25px 0;color:gray}.reimport{cursor:pointer}"]
                }] }
    ];
    /** @nocollapse */
    TableLayoutComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: ContentService },
        { type: MessageService },
        { type: MatDialog },
        { type: SnackBarService },
        { type: ChangeDetectorRef },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] },
        { type: LoaderService }
    ]; };
    TableLayoutComponent.propDecorators = {
        viewFrom: [{ type: Input }],
        onLoad: [{ type: Input }],
        tableId: [{ type: Input }],
        disablePagination: [{ type: Input }],
        selectAllBox: [{ type: ViewChild, args: ["selectAllBox",] }],
        table: [{ type: ViewChild, args: ["MatTable",] }],
        checkClickEventMessage: [{ type: Output }],
        actionClickEvent: [{ type: Output }],
        paginator: [{ type: ViewChild, args: ["paginator",] }],
        selectedPaginator: [{ type: ViewChild, args: ["selectedPaginator",] }]
    };
    return TableLayoutComponent;
}());
export { TableLayoutComponent };
export function base64ToArrayBuffer(base64) {
    var binaryString = window.atob(base64); // Comment this if not using base64
    var bytes = new Uint8Array(binaryString.length);
    return bytes.map(function (byte, i) { return binaryString.charCodeAt(i); });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ0YWJsZS1sYXlvdXQvdGFibGUtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUNMLFNBQVMsRUFDVCxTQUFTLEVBQ1QsS0FBSyxFQUNMLE1BQU0sRUFDTixZQUFZLEVBQ1osTUFBTSxFQUVOLGlCQUFpQixFQUNsQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQ0wsWUFBWSxFQUNaLGtCQUFrQixFQUNsQixTQUFTLEVBQ1QsV0FBVyxFQUNYLFFBQVEsRUFDVCxNQUFNLG1CQUFtQixDQUFDO0FBQzNCLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMvRCxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ2pGLE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBRXhDLE9BQU8sRUFFTCxXQUFXLEVBSVosTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QixPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFDbEQsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzVELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUM5RSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFHbEQ7SUF1REUsOEJBQ1UsNkJBQTJELEVBQzNELGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLFVBQXFCLEVBQ3JCLGVBQWdDLEVBQ2hDLGlCQUFvQyxFQUNiLFdBQVcsRUFFZixPQUFPLEVBQzFCLGFBQTRCO1FBVnRDLGlCQW1FQztRQWxFUyxrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQzNELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNiLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBRWYsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQWpEN0Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBR2xDLDJCQUFzQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDcEQscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUN4RCx1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUNwQixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQVM5QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFFbkIsaUJBQVksR0FBUSxFQUFFLENBQUM7UUFDdkIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQU1aLGdCQUFXLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBSS9DLHlCQUFvQixHQUFRLEVBQUUsQ0FBQztRQUUvQix1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFFcEMsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFFaEMscUJBQWdCLEdBQVksS0FBSyxDQUFDO1FBYWhDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7UUFDaEQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzdEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFpQ0k7UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE9BQU87WUFDaEQsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztZQUNGLElBQ0UsS0FBSSxDQUFDLGlCQUFpQjtnQkFDdEIsS0FBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVE7Z0JBQy9CLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEVBQ2pEO2dCQUNBLEtBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqQjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGNBQWM7YUFDaEIsb0JBQW9CLEVBQUU7YUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDakMsU0FBUyxDQUFDLFVBQUEsSUFBSTtZQUNiLEtBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCx1Q0FBUSxHQUFSO1FBQ0UsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBRXBDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNqQixNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxFQUFFO1lBQ1QsVUFBVSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7U0FDbkMsQ0FBQztRQUNGLG9DQUFvQztRQUNwQyxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztRQUNGLCtEQUErRDtRQUMvRCx5Q0FBeUM7UUFDekMsMkNBQTJDO1FBQzNDLElBQUk7UUFDSixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUN4RCxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLDBDQUEwQztRQUMxQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRXhCLENBQUM7SUFDRCw4Q0FBZSxHQUFmO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDdkQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7U0FDcEQ7SUFFSCxDQUFDO0lBQ0QsMENBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCw4Q0FBZSxHQUFmO1FBQ0UsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQzdDLENBQUM7UUFDRixJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFDO1FBQ2xDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTzthQUNqQyxNQUFNLENBQUMsVUFBVSxHQUFHO1lBQ25CLE9BQU8sR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUN0QixDQUFDLENBQUM7YUFDRCxHQUFHLENBQUMsVUFBVSxHQUFHO1lBQ2hCLE9BQU8sR0FBRyxDQUFDLEtBQUssQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCx5Q0FBVSxHQUFWLFVBQVcsVUFBVTtRQUNuQixJQUFJLENBQUMsb0JBQW9CLEdBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNqRyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNO1lBQzFCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDakQsSUFBSSxXQUFXLEdBQ2IsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMxRSxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUMvRCxjQUFjO1FBQ2QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLHVCQUF1QixDQUFDLENBQUM7UUFDNUQsSUFBSSxpQkFBaUIsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDNUcsSUFBSSxpQkFBaUIsRUFBRTtZQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztZQUMvQixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsd0JBQXdCLENBQUM7WUFDaEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUM7WUFDN0QsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLE1BQU0sQ0FBQztZQUMvQixJQUFJLFlBQVksR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ3ZDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN2QyxTQUFTLENBQUMsVUFBQSxHQUFHO2dCQUNaLElBQUksU0FBUyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBRS9FLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUU7WUFDakUsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztZQUNsRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQix5RUFBeUU7WUFDekUsc0NBQXNDO1lBQ3RDLE1BQU07U0FDUDtRQUNGLHNEQUFzRDtRQUNyRCxJQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixFQUFDO1lBQ3pELElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUMxRCxJQUFJLElBQUksR0FBQyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQSxDQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUEsQ0FBQyxDQUFBLEVBQUUsQ0FBQztZQUNqSixJQUFHLElBQUksQ0FBQyxNQUFNLEdBQUMsQ0FBQyxFQUFDO2dCQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxHQUFDLElBQUksQ0FBQztnQkFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUMvQjtTQUNGO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFN0MsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLENBQ2QsSUFBSSxDQUFDLGdCQUFnQixFQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFDcEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQ2pCLFdBQVcsQ0FDWixDQUFDO1NBQ0g7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLGdCQUFnQjtnQkFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNmLDhCQUE4QjtTQUMvQjtJQUNILENBQUM7SUFDRCxzQ0FBTyxHQUFQLFVBQVEsaUJBQWlCO1FBQXpCLGlCQW9KQztRQW5KQyxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQyxXQUFXLENBQUM7UUFDN0MsSUFBSSxpQkFBaUIsQ0FBQyxjQUFjLEVBQUU7WUFDcEMsSUFBSSxNQUFNLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztZQUNyRCxJQUFJLFlBQVksR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1lBQzdELElBQUksVUFBUSxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQztZQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN0RCxJQUFJLGNBQWMsR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3hGLHVEQUF1RDtZQUN2RCxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2pDLEdBQUc7WUFDSCxJQUFJLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsQ0FBRSwwREFBMEQ7WUFDakksSUFBSSxjQUFjLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztZQUNsRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsSUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFlBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLFdBQVc7b0JBQzdDLElBQUksV0FBVyxDQUFDLGVBQWUsRUFBRTt3QkFDL0IsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO3FCQUMxRTt5QkFBTTt3QkFDTCxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7cUJBQzNIO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxXQUFXO29CQUM3QyxJQUFJLFdBQVcsQ0FBQyxlQUFlLEVBQUU7d0JBQy9CLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtxQkFDMUU7eUJBQU0sSUFBSSxXQUFXLENBQUMsWUFBWSxFQUFFO3dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTs0QkFDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzs0QkFDbkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7cUJBQ3ZCO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxlQUFlOzRCQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2QztnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsSUFBSSxDQUFDLFdBQVcsd0JBQVEsSUFBSSxDQUFDLFdBQVcsRUFBSyxpQkFBaUIsQ0FBRSxDQUFDO1lBQ2pFLElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUM7aUJBQ3ZDLFNBQVMsQ0FBQyxVQUFBLElBQUk7Z0JBQ2Isd0RBQXdEO2dCQUN4RCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxHQUFHO2dCQUNILEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQztnQkFDeEQsS0FBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUksQ0FBQyxPQUFPO3FCQUNqQyxNQUFNLENBQUMsVUFBVSxHQUFHO29CQUNuQixPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQztxQkFDRCxHQUFHLENBQUMsVUFBVSxHQUFHO29CQUNoQixPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUM7Z0JBQ25CLENBQUMsQ0FBQyxDQUFDO2dCQUNMLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztnQkFDbkIsSUFBSSxJQUFJLEdBQUcsS0FBSSxDQUFDO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN0QyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUUsVUFBVSxJQUFJLEVBQUUsS0FBSztvQkFDMUQsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7d0JBQzVCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDakIsT0FBTyxDQUFDLFVBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQzt3QkFDekIsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO3dCQUNuQixTQUFTLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUNsRixJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQzt3QkFDM0YsSUFBRyxhQUFhLEVBQUM7NEJBQ2YsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQzt5QkFDM0I7d0JBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDekI7eUJBQU07d0JBQ0wsSUFBSSxTQUFPLEdBQUcsRUFBRSxDQUFDO3dCQUNqQixDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGNBQWMsRUFBRSxVQUFVLFFBQVE7NEJBQzVELElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTtnQ0FDbkIsSUFBSSxRQUFRLENBQUMsbUJBQW1CLEVBQUU7b0NBQ2hDLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQ0FDbkosU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzt3Q0FDM0MsQ0FBQyxDQUFDLFdBQVc7d0NBQ2IsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDUjtxQ0FBTTtvQ0FDTCxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO3dDQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO3dDQUN2QyxDQUFDLENBQUMsRUFBRSxDQUFDO2lDQUNSOzZCQUNGO2lDQUFNLElBQUksUUFBUSxDQUFDLFdBQVcsRUFBRTtnQ0FDL0IsU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDOzZCQUNuRDtpQ0FBTSxJQUFJLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRTtnQ0FDckMsU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDOzZCQUN6QztpQ0FBTTtnQ0FDTCxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NkJBQy9DOzRCQUVELElBQUksaUJBQWlCLENBQUMsbUJBQW1CLEVBQUU7Z0NBQ3pDLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29DQUNwQixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzZCQUN0RDt3QkFDSCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7d0JBQ25CLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQy9FLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3dCQUMzRixJQUFHLGFBQWEsRUFBQzs0QkFDZixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzt5QkFDckI7d0JBQ0QsU0FBTyx3QkFBUSxJQUFJLEVBQUssU0FBTyxDQUFFLENBQUM7d0JBQ2xDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBTyxDQUFDLENBQUM7cUJBQ3pCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7b0JBQ2pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsU0FBcUIsQ0FBQztpQkFDeEM7cUJBQU07b0JBQ0wsS0FBSSxDQUFDLFNBQVM7d0JBQ1osSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7NEJBQ2xELENBQUMsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBYzs0QkFDM0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDVjtnQkFDRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUN6QixLQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2lCQUNuQztnQkFDRCxLQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6RCxzRUFBc0U7Z0JBQ3RFLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JFLEtBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUNwQyxJQUFJLFdBQVcsR0FBRyxDQUFDLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNuRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDO29CQUNqQyxJQUFJLFdBQVcsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO3dCQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDO3FCQUNyRTt5QkFBTTt3QkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ3BDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILDJDQUEyQztnQkFDM0MsMEJBQTBCO2dCQUMxQixtREFBbUQ7Z0JBQ25ELHVDQUF1QztnQkFDdkMseUNBQXlDO2dCQUN6Qyx3RUFBd0U7Z0JBQ3hFLG1EQUFtRDtnQkFDbkQsMENBQTBDO2dCQUUxQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzlDLENBQUMsRUFDQyxVQUFBLEdBQUc7Z0JBQ0QsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUM7U0FDUjtJQUNILENBQUM7SUFDRCwwQ0FBVyxHQUFYLFVBQVksWUFBWSxFQUFFLGFBQWEsRUFBRSxXQUFXLEVBQUUsV0FBVztRQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQzlDLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNuQixxREFBcUQ7UUFDckQsYUFBYSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDekUsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUN6QyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxVQUFVLElBQUksRUFBRSxLQUFLO2dCQUM1QyxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtvQkFDNUIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNqQixPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFDdEMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDekI7cUJBQU07b0JBQ0wsSUFBSSxTQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNqQixDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxjQUFjLEVBQUUsVUFBVSxRQUFRO3dCQUN2RCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEVBQUU7NEJBQ25CLElBQUksUUFBUSxDQUFDLG1CQUFtQixFQUFFO2dDQUNoQyxJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0NBQ25KLFNBQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7b0NBQzNDLENBQUMsQ0FBQyxXQUFXO29DQUNiLENBQUMsQ0FBQyxFQUFFLENBQUM7NkJBQ1I7aUNBQU07Z0NBQ0wsU0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQ0FDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztvQ0FDdkMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs2QkFDUjt5QkFDRjs2QkFBTTs0QkFDTCxTQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQy9DO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILFNBQU8sd0JBQVEsSUFBSSxFQUFLLFNBQU8sQ0FBRSxDQUFDO29CQUNsQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQU8sQ0FBQyxDQUFDO2lCQUN6QjtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLENBQUMsT0FBTyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUM7UUFDeEMsYUFBYSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQzdELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUM1QyxJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsZUFBZSxFQUFFO1lBQ2hELElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxlQUFlLEVBQUUsVUFBVSxPQUFPO2dCQUN2RCxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO29CQUNsRCxNQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTt3QkFDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQ25ELENBQUMsQ0FBQyxNQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDeEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsTUFBSSxDQUFDLGNBQWM7cUJBQ2hCLGFBQWEsQ0FBQyxNQUFJLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxNQUFNLENBQUM7cUJBQy9DLFNBQVMsQ0FBQyxVQUFBLElBQUk7b0JBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztvQkFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7b0JBQ2xDLElBQUksT0FBTyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGFBQWEsQ0FBQyxDQUFDO29CQUNwQyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxVQUFVLFlBQVk7d0JBQzdDLElBQ0UsSUFBSSxDQUFDLFFBQVE7NEJBQ2IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLElBQUksT0FBTyxDQUFDLFdBQVc7NEJBQ2hELFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEVBQ2pDOzRCQUNBLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO2dDQUMzQixPQUFPLElBQUksWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUM7b0NBQzFDLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVc7b0NBQ3hELENBQUMsQ0FBQyxFQUFFLENBQUM7NEJBQ1QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUNoRTs7NEJBQ0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDbkQsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDbkQsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDeEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztTQUNoRTtRQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBcUIsQ0FBQztRQUN2QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztRQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsZUFBZSxDQUFDLENBQUM7UUFDM0Msd0RBQXdEO1FBQ3hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTzthQUNqQyxNQUFNLENBQUMsVUFBVSxHQUFHO1lBQ25CLE9BQU8sR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUN0QixDQUFDLENBQUM7YUFDRCxHQUFHLENBQUMsVUFBVSxHQUFHO1lBQ2hCLE9BQU8sR0FBRyxDQUFDLEtBQUssQ0FBQztRQUNuQixDQUFDLENBQUMsQ0FBQztRQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSx3Q0FBd0MsQ0FBQyxDQUFDO1FBQzlFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3ZELElBQUksQ0FBQyxnQkFBZ0IsR0FBQyxZQUFZLENBQUMsaUJBQWlCLENBQUEsQ0FBQyxDQUFBLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDO1FBQzFGLGlCQUFpQjtRQUNqQixnQ0FBZ0M7UUFDaEMseURBQXlEO1FBQ3pELDZDQUE2QztJQUMvQyxDQUFDO0lBQ0QsK0NBQWdCLEdBQWhCLFVBQWlCLFFBQVEsRUFBRSxLQUFLO1FBQzlCLGtEQUFrRDtRQUNsRCxzQ0FBc0M7UUFDdEMsVUFBVTtRQUNWLElBQUcsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQzNEO1lBQ0UsUUFBUSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO1NBQ2pEO1FBRUQsaURBQWlEO1FBQ2pELG1DQUFtQztRQUNuQyxVQUFVO1FBQ1IsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUEsQ0FBQyxDQUFBLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUMsQ0FBQSxDQUFDLENBQUEsQ0FBQyxDQUFDO1FBQ3JILE9BQU8sQ0FBQyxHQUFHLENBQUMsOENBQThDLEVBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUE7UUFDcEcsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsRUFBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQy9ELHFDQUFxQztRQUNyQyxrQ0FBa0M7UUFDbEMsdUNBQXVDO1FBQ3ZDLCtCQUErQjtRQUMvQixhQUFhO1FBQ2IsNkNBQTZDO1FBQzdDLHFDQUFxQztRQUNyQyxPQUFPO1FBQ0YsSUFBSSxTQUFTLEdBQUMsRUFBRSxDQUFDO1FBQ2pCLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQSxDQUFDLENBQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixHQUFHLEdBQUcsQ0FBQyxDQUFBLENBQUMsQ0FBQSxTQUFTLENBQUM7UUFDMUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBQyxlQUFlLENBQUMsQ0FBQztRQUNwRCxJQUFHLGtCQUFrQixJQUFJLGtCQUFrQixHQUFDLENBQUMsRUFDN0M7WUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQ3BCLElBQUcsZUFBZSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEVBQzVDO2dCQUNFLFNBQVMsR0FBQyxlQUFlLENBQUM7YUFDM0I7aUJBQ0Q7Z0JBQ0UsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxJQUFFLGtCQUFrQixFQUFDLENBQUMsRUFBRSxFQUNyQztvQkFDQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNsQjthQUNGO1lBQ0gsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFFLFNBQVMsQ0FBQztTQUNyQzthQUFLO1lBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQTtZQUNsQixJQUFHLGtCQUFrQixJQUFFLENBQUMsRUFDeEI7Z0JBQ0EsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFFLFNBQVMsQ0FBQzthQUNwQztpQkFDRDtnQkFDQSxRQUFRLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2FBQ3JDO1NBQ0w7UUFDTixHQUFHO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN0RCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLEVBQUU7WUFDbkUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLEVBQUUsVUFBVSxJQUFJO2dCQUM5RCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDN0MsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLGlCQUFpQixHQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNO1lBQy9ELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVk7WUFDN0IsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNULElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDbEUsSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2xDO2FBQU07WUFFTCwyQ0FBMkM7WUFDM0Msb0RBQW9EO1lBQ3BELEtBQUs7WUFDTCxJQUFJLFdBQVMsR0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQSxDQUFDLENBQUEsVUFBVSxDQUFDO1lBQ3ZHLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUNwQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxXQUFTLENBQUMsS0FBSyxRQUFRLENBQUMsV0FBUyxDQUFDLEVBQXRDLENBQXNDLENBQzlDLENBQUM7WUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDcEM7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN6RCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDcEQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRTtZQUM3RCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDO1lBQ2pELElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sS0FBSyxRQUFRLENBQUMsR0FBRyxFQUFFO2dCQUNsRCxJQUFJLFdBQVMsR0FDWCxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTTtvQkFDM0MsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsVUFBVSxDQUFDO29CQUMvQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNULFdBQVMsR0FBRyxXQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsT0FBTztvQkFDNUMsT0FBTyxPQUFPLElBQUksSUFBSSxDQUFDO2dCQUN6QixDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxXQUFTLENBQUM7YUFDMUM7U0FDRjtRQUVHLDZDQUE2QztRQUM5QyxJQUFJLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ2xHLElBQUcsV0FBVyxFQUFDO1lBQ2YsSUFBSSxJQUFJLEdBQUMsSUFBSSxDQUFDO1lBQ2QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQztnQkFDakMsSUFBSSxXQUFXLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQztpQkFDckU7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRSw2Q0FBNkM7UUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNuRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELDZDQUFjLEdBQWQsVUFBZSxLQUFLO1FBQ2xCLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxJQUFJLEVBQUU7WUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQUEsR0FBRztnQkFDMUIsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3RELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3ZFO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBQSxHQUFHO2dCQUMxQixHQUFHLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztZQUN0QixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDdkM7UUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUNELDRDQUFhLEdBQWIsVUFBYyxJQUFJLEVBQUUsVUFBVTtRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO1FBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGtCQUFrQixDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELDRDQUFhLEdBQWI7UUFBQSxpQkFjQztRQWJDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDdkQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUk7WUFDbEQsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjO2FBQ2hCLGFBQWEsQ0FBQyxLQUFLLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQzthQUMzQyxTQUFTLENBQUMsVUFBQSxJQUFJO1lBQ2IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0MsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxpREFBa0IsR0FBbEI7UUFDRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEIsQ0FBQztJQUNELDBDQUFXLEdBQVgsVUFBWSxLQUFLLEVBQUUsR0FBRztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLEtBQUssRUFBRSxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHO1lBQzFCLFNBQVMsRUFBRSxHQUFHO1lBQ2QsV0FBVyxFQUFFLEtBQUs7U0FDbkIsQ0FBQztRQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLEVBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFFLENBQUM7UUFFMUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBQyxLQUFLLENBQUMsQ0FBQyxxQkFBcUI7UUFDaEQsSUFBSSxVQUFVLEdBQ1osSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztZQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUNuRCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLGFBQWEsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQ3BELElBQUksT0FBTyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7UUFDeEMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3pELElBQUksVUFBVSxFQUFFO1lBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ2hDO2FBQU07WUFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFDLElBQUksT0FBSyxHQUFHO2dCQUNWLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO2FBQ25DLENBQUM7WUFFRixJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsZUFBZSxFQUFFO2dCQUNsRCxPQUFLLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQy9DO2lCQUFNO2dCQUNMLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsVUFBVSxJQUFJO29CQUMvQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDM0IsT0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLCtDQUErQztnQkFDbEcsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBSyxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDckM7SUFFSCxDQUFDO0lBRUQsZ0RBQWlCLEdBQWpCLFVBQWtCLFNBQVMsRUFBRSxHQUFHO1FBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNyQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDM0IsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsb0JBQW9CLEdBQUc7WUFDeEIsU0FBUyxFQUFFLEdBQUc7WUFDZCxXQUFXLEVBQUUsS0FBSztTQUNyQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxxQkFBcUI7UUFDbEQsSUFBSSxVQUFVLEdBQ1YsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztZQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUN2RCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLGFBQWEsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQ3BELElBQUksT0FBTyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7UUFDeEMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3pELElBQUksVUFBVSxFQUFFO1lBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ2xDO2FBQU07WUFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFDLElBQUksT0FBSyxHQUFHO2dCQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO2FBQ3JDLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQ2xELElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxlQUFlLEVBQUU7Z0JBQ2hELE9BQUssQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxLQUFLLENBQUM7YUFDakQ7aUJBQ0k7Z0JBQ0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUk7b0JBQzdCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3dCQUN6QixPQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsK0NBQStDO2dCQUN0RyxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxPQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFDQywwQ0FBVyxHQUFYLFVBQVksS0FBSztRQUNmLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFDRCw0Q0FBYSxHQUFiLFVBQWMsS0FBSyxFQUFFLE1BQU07UUFDekIsSUFBSSxJQUFJLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1FBQy9DLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3JFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1NBQzlEO1FBQ0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQseUNBQVUsR0FBVixVQUFXLEtBQUs7UUFDZCxJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDM0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEM7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1lBQzFELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUN4QztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUNELHVDQUFRLEdBQVIsVUFBUyxPQUFPLEVBQUUsR0FBRztRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxnQ0FBZ0MsRUFBRSxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNyRixJQUFJLEdBQUcsQ0FBQyxVQUFVLEVBQUU7WUFDbEIsSUFBSSxPQUFPLEdBQUcsb0NBQW9DLENBQUE7WUFDbEQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsS0FBRyxJQUFJLENBQUMsV0FBYSxHQUFHLE9BQU8sQ0FBQztZQUV2RCxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUE7WUFDOUMsa0VBQWtFO1NBRW5FO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNuQztJQUNILENBQUM7SUFDRCw2Q0FBYyxHQUFkLFVBQWUsT0FBTyxFQUFFLE1BQU07UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFDRCwwQ0FBVyxHQUFYLFVBQVksT0FBTyxFQUFFLE1BQU07UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUE7UUFDbkMsSUFBSSxNQUFNLElBQUksTUFBTSxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksTUFBTSxJQUFHLE1BQU0sRUFBRTtZQUMzRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLFFBQVEsRUFBRTtZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztZQUNqQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQ3ZDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDOUMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDNUMsc0NBQXNDO29CQUN0QywwQkFBMEI7b0JBQzFCLEtBQUs7b0JBQ0wsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUMsT0FBTyxDQUFDLENBQUM7b0JBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3hDLElBQUcsS0FBSyxHQUFDLENBQUMsQ0FBQyxFQUFDO3dCQUNWLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO3FCQUMvQjtvQkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFlBQVksQ0FBQztvQkFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDdkM7YUFDRjtpQkFBTTtnQkFDTCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLEVBQUU7b0JBQ2pELElBQUksV0FBUyxHQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQSxDQUFDLENBQUEsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixDQUFBLENBQUMsQ0FBQSxVQUFVLENBQUM7b0JBQ3ZHLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUMsV0FBUyxDQUFDLENBQUM7b0JBQ3pDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO29CQUMvQyxzQ0FBc0M7b0JBQ3RDLGtEQUFrRDtvQkFDbEQsS0FBSztvQkFDTCxJQUFJLEtBQUssR0FBRyxZQUFZLENBQUMsU0FBUyxDQUNoQyxVQUFBLEdBQUcsSUFBSSxPQUFBLEdBQUcsQ0FBQyxXQUFTLENBQUMsSUFBSSxPQUFPLENBQUMsV0FBUyxDQUFDLEVBQXBDLENBQW9DLENBQzVDLENBQUM7b0JBQ0YsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsWUFBWSxDQUFDO29CQUM5QyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO29CQUNsQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNyRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUN2QzthQUNGO1NBQ0Y7YUFBTSxJQUFJLE1BQU0sSUFBSSxnQkFBZ0IsRUFBRTtZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLGNBQWMsRUFBRTtZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLGtCQUFrQixFQUFFO1lBQ3ZDLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDdkIsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QywwQ0FBMEMsQ0FDM0MsQ0FDRixDQUFDO1NBQ0g7YUFBTSxJQUFJLE1BQU0sSUFBSSxVQUFVLEVBQUU7WUFDL0IsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUM7WUFDdEQsSUFBSSxLQUFLLEdBQUc7Z0JBQ1YsUUFBUSxFQUFFLE9BQU8sQ0FBQyxHQUFHO2FBQ3RCLENBQUE7WUFDRCxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLEtBQUssRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDO2lCQUMzQyxTQUFTLENBQUMsVUFBQSxJQUFJO2dCQUNiLElBQU0sRUFBRSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzdDLElBQU0sSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3pDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN4QjtnQkFDRCxJQUFJLFlBQVksR0FBRyxDQUFDLE9BQU8sQ0FBQyxRQUFRLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO2dCQUMxRSxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxDQUFDLENBQUM7Z0JBQ3BELFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ25FLENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBQ0QsNkNBQWMsR0FBZCxVQUFlLE9BQU8sRUFBQyxVQUFVO1FBQWpDLGlCQXNDQztRQXJDQyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQy9DLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQTtRQUMxQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDdEQsSUFBSSxVQUFVLENBQUMsWUFBWSxFQUFFO1lBQzNCLElBQUkscUJBQW1CLEdBQUcsY0FBYyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7WUFDcEUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUk7Z0JBQ2hFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU07b0JBQy9CLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1lBRUgsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQztpQkFDekUsU0FBUyxDQUNSLFVBQUEsR0FBRztnQkFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xCLENBQUMsRUFDRCxVQUFBLEtBQUs7Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO1lBQ0osQ0FBQyxDQUNGLENBQUM7WUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLDBDQUEwQyxFQUFFLFFBQVEsQ0FBQyxDQUFBO1NBQ2xFO2FBQU07U0FFTjtJQUVILENBQUM7SUFDRCw2Q0FBYyxHQUFkLFVBQWUsS0FBSyxFQUFFLE9BQU87UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2pFLE9BQU8sQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUM7UUFDdkQsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGdCQUFnQjtZQUN4QyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGNBQWM7WUFDdEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNQLElBQUksY0FBYyxFQUFFO1lBQ2xCLElBQUkscUJBQW1CLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUN0RCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxVQUFRLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUk7Z0JBQ2xELFVBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU07b0JBQy9CLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ2xDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxVQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDO2lCQUMzRCxTQUFTLENBQ1IsVUFBQSxHQUFHO2dCQUNBLElBQUcsT0FBTyxDQUFDLE1BQU0sSUFBRSxRQUFRLElBQUsscUJBQW1CLENBQUMsTUFBTSxFQUMxRDtvQkFDQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsTUFBTSxDQUMzQixDQUNGLENBQUM7aUJBQ0Y7cUJBQUssSUFBRyxPQUFPLENBQUMsTUFBTSxJQUFFLFVBQVUsSUFBSyxxQkFBbUIsQ0FBQyxPQUFPLEVBQ25FO29CQUNDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztpQkFDRjtxQkFDRDtvQkFDQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7aUJBQ0Y7WUFFSixDQUFDLEVBQ0QsVUFBQSxLQUFLO2dCQUNILElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FDRixDQUFDO1NBQ0w7YUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUU7WUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUMxRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1NBQ3RFO0lBQ0gsQ0FBQztJQUNELHlDQUFVLEdBQVYsVUFBVyxPQUFPLEVBQUUsTUFBTTtRQUExQixpQkFrQkM7UUFqQkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQ0FBa0MsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN6RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUMvRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVO2FBQzdCLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtZQUMxQixZQUFZLEVBQUUsSUFBSTtZQUNsQixLQUFLLEVBQUUsVUFBVTtZQUNqQixVQUFVLEVBQUUscUJBQXFCO1lBQ2pDLElBQUksRUFBRTtnQkFDSixNQUFNLEVBQUUsTUFBTTtnQkFDZCxTQUFTLEVBQUUsT0FBTzthQUNuQjtTQUNGLENBQUM7YUFDRCxXQUFXLEVBQUU7YUFDYixTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2pCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDeEMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQW44QkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxjQUFjO29CQUN4Qiw0MWVBQTRDO29CQUU1QyxVQUFVLEVBQUU7d0JBQ1YsT0FBTyxDQUFDLGNBQWMsRUFBRTs0QkFDdEIsS0FBSyxDQUFDLFdBQVcsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQzdFLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ3pDLFVBQVUsQ0FBQyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsc0NBQXNDLENBQUMsQ0FBQzt5QkFDdEYsQ0FBQztxQkFDSDs7aUJBQ0Y7Ozs7Z0JBeEJRLDRCQUE0QjtnQkFJNUIsY0FBYztnQkFDZCxjQUFjO2dCQXJCckIsU0FBUztnQkFJRixlQUFlO2dCQVR0QixpQkFBaUI7Z0RBZ0dkLE1BQU0sU0FBQyxhQUFhO2dEQUVwQixNQUFNLFNBQUMsU0FBUztnQkFuRVosYUFBYTs7OzJCQWdCbkIsS0FBSzt5QkFDTCxLQUFLOzBCQUNMLEtBQUs7b0NBQ0wsS0FBSzsrQkFDTCxTQUFTLFNBQUMsY0FBYzt3QkFDeEIsU0FBUyxTQUFDLFVBQVU7eUNBQ3BCLE1BQU07bUNBQ04sTUFBTTs0QkF1R04sU0FBUyxTQUFDLFdBQVc7b0NBQ3JCLFNBQVMsU0FBQyxtQkFBbUI7O0lBdzBCaEMsMkJBQUM7Q0FBQSxBQXA4QkQsSUFvOEJDO1NBeDdCWSxvQkFBb0I7QUEwN0JqQyxNQUFNLFVBQVUsbUJBQW1CLENBQUMsTUFBYztJQUNoRCxJQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsbUNBQW1DO0lBQzdFLElBQU0sS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsRCxPQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsVUFBQyxJQUFJLEVBQUUsQ0FBQyxJQUFLLE9BQUEsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQyxDQUFDO0FBQzVELENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBWaWV3Q2hpbGQsXHJcbiAgSW5wdXQsXHJcbiAgT3V0cHV0LFxyXG4gIEV2ZW50RW1pdHRlcixcclxuICBJbmplY3QsXHJcbiAgT25Jbml0LFxyXG4gIENoYW5nZURldGVjdG9yUmVmXHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtcclxuICBNYXRQYWdpbmF0b3IsXHJcbiAgTWF0VGFibGVEYXRhU291cmNlLFxyXG4gIE1hdERpYWxvZyxcclxuICBNYXRDaGVja2JveCxcclxuICBNYXRUYWJsZVxyXG59IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbFwiO1xyXG5pbXBvcnQgeyBTbmFja0JhclNlcnZpY2UgfSBmcm9tIFwiLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBhbmltYXRlLCBzdGF0ZSwgc3R5bGUsIHRyYW5zaXRpb24sIHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcblxyXG5pbXBvcnQge1xyXG4gIEZvcm1CdWlsZGVyLFxyXG4gIEZvcm1Db250cm9sLFxyXG4gIEZvcm1Hcm91cCxcclxuICBWYWxpZGF0b3JzLFxyXG4gIEZvcm1BcnJheVxyXG59IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5cclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tIFwiLi4vY29udGVudC9jb250ZW50LnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi4vX3NlcnZpY2VzL2luZGV4XCI7XHJcbmltcG9ydCB7IE1vZGVsTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGVsLWxheW91dC9tb2RlbC1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqcy9TdWJqZWN0XCI7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5pbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gXCJAYW5ndWxhci9jZGsvY29sbGVjdGlvbnNcIjtcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uL2xvYWRlci5zZXJ2aWNlJztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJ0YWJsZS1sYXlvdXRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL3RhYmxlLWxheW91dC5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi90YWJsZS1sYXlvdXQuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgYW5pbWF0aW9uczogW1xyXG4gICAgdHJpZ2dlcignZGV0YWlsRXhwYW5kJywgW1xyXG4gICAgICBzdGF0ZSgnY29sbGFwc2VkJywgc3R5bGUoeyBoZWlnaHQ6ICcwcHgnLCBtaW5IZWlnaHQ6ICcwJywgZGlzcGxheTogJ25vbmUnIH0pKSxcclxuICAgICAgc3RhdGUoJ2V4cGFuZGVkJywgc3R5bGUoeyBoZWlnaHQ6ICcqJyB9KSksXHJcbiAgICAgIHRyYW5zaXRpb24oJ2V4cGFuZGVkIDw9PiBjb2xsYXBzZWQnLCBhbmltYXRlKCcyMjVtcyBjdWJpYy1iZXppZXIoMC40LCAwLjAsIDAuMiwgMSknKSksXHJcbiAgICBdKSxcclxuICBdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGFibGVMYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIHZpZXdGcm9tOiBhbnk7XHJcbiAgQElucHV0KCkgb25Mb2FkOiBhbnk7XHJcbiAgQElucHV0KCkgdGFibGVJZDogYW55O1xyXG4gIEBJbnB1dCgpIGRpc2FibGVQYWdpbmF0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgQFZpZXdDaGlsZChcInNlbGVjdEFsbEJveFwiKSBzZWxlY3RBbGxCb3g6IE1hdENoZWNrYm94O1xyXG4gIEBWaWV3Q2hpbGQoXCJNYXRUYWJsZVwiKSB0YWJsZTogTWF0VGFibGU8YW55PjtcclxuICBAT3V0cHV0KCkgY2hlY2tDbGlja0V2ZW50TWVzc2FnZSA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gIEBPdXRwdXQoKSBhY3Rpb25DbGlja0V2ZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XHJcbiAgZW5hYmxlU2VsZWN0T3B0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZW5hYmxlU2VhcmNoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgdGFibGVEYXRhOiBhbnkgPSB7fTtcclxuICBlbmFibGVBY3Rpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBkaXNwbGF5ZWRDb2x1bW5zOiBhbnk7XHJcbiAgY29sdW1uczogYW55O1xyXG4gIHRhYmxlTGlzdDogYW55O1xyXG4gIGN1cnJlbnRDb25maWdEYXRhOiBhbnk7XHJcbiAgY3VycmVudERhdGE6IGFueTtcclxuICBleHBhbmRlZEVsZW1lbnQ6IGFueTtcclxuICAvLyBAVmlld0NoaWxkKCd2aWV3TWUnLCB7IHN0YXRpYzogZmFsc2UgfSlcclxuICBkYXRhU291cmNlOiBhbnk7XHJcbiAgbGltaXQ6IG51bWJlciA9IDEwO1xyXG4gIG9mZnNldDogbnVtYmVyID0gMDtcclxuICBsZW5ndGg6IG51bWJlcjtcclxuICBzZWxlY3RlZERhdGE6IGFueSA9IFtdO1xyXG4gIGlucHV0RGF0YTogYW55ID0ge307XHJcbiAgcXVlcnlQYXJhbXM6IGFueTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIGRpYWxvZ1JlZjogYW55O1xyXG4gIGN1cnJlbnRUYWJsZURhdGE6IGFueTtcclxuICBkYXRhOiBhbnk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZU1vZGVsID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBpbnB1dFZhbHVlOiBhbnk7XHJcbiAgZnJvbUZpZWxkVmFsdWU6IGFueTtcclxuICB0b0ZpZWxkVmFsdWU6IGFueTtcclxuICBjdXJyZW50RmlsdGVyZWRWYWx1ZTogYW55ID0ge307XHJcbiAgb3B0aW9uRmlsdGVyZGF0YTogYW55O1xyXG4gIGVuYWJsZU9wdGlvbkZpbHRlcjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIG9wdGlvbkZpbHRlckZpZWxkczogYW55O1xyXG4gIGVuYWJsZVVJZmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcmVkaXJlY3RVcmk6IHN0cmluZztcclxuICBkaXNhYmxlUGFnaW5hdG9yOiBib29sZWFuID0gZmFsc2U7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlOiBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG1lc3NhZ2VTZXJ2aWNlOiBNZXNzYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgX21hdERpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgQEluamVjdChcImVudmlyb25tZW50XCIpIHByaXZhdGUgZW52aXJvbm1lbnQsXHJcblxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHJpdmF0ZSBlbmdsaXNoLFxyXG4gICAgcHJpdmF0ZSBsb2FkZXJTZXJ2aWNlOiBMb2FkZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLnJlZGlyZWN0VXJpID0gdGhpcy5lbnZpcm9ubWVudC5yZWRpcmVjdFVyaTtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICAgIC8qIC8vIHRlbXBvcmFyaWx5IGNvbW1lbnRlZFxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5tb2RlbENsb3NlTWVzc2FnZVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZU1vZGVsKSlcclxuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIj4+Pj5kYXRhXCIpXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERhdGEgPSBbXTtcclxuICAgICAgICBpZiAoZGF0YSAhPSAwKSB7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy50YWJsZURhdGEpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0YVNvdXJjZSk7XHJcbiAgICAgICAgLy8gXy5mb3JFYWNoKHRoaXMudGFibGVEYXRhLCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgLy8gICBpdGVtLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICAvLyB9KTtcclxuXHJcbiAgICAgICAgLy8gdGhpcy5kYXRhU291cmNlLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgICAgLy8gICBvYmouY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIH0pO1xyXG4gICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLmRhdGFTb3VyY2UuZGF0YSwgXCJfaWRcIik7XHJcbiAgICAgICAgLy8gLy8gbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAvLyB0aGlzLmNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcclxuICAgICAgICAvLyBpZih0aGlzLmRhdGEgPT09ICdsaXN0Vmlldycpe1xyXG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcclxuICAgICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyB9XHJcbiAgICAgIH0pO1xyXG4gICAgICAqL1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5nZXRNZXNzYWdlKCkuc3Vic2NyaWJlKG1lc3NhZ2UgPT4ge1xyXG4gICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICAgICk7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhICYmXHJcbiAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5saXN0VmlldyAmJlxyXG4gICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEubGlzdFZpZXcuZW5hYmxlVGFibGVMYXlvdXRcclxuICAgICAgKSB7XHJcbiAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlXHJcbiAgICAgIC5nZXRUYWJsZUhlYWRlclVwZGF0ZSgpXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlKSlcclxuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICB0aGlzLnVwZGF0ZVRhYmxlVmlldygpO1xyXG4gICAgICB9KTtcclxuICB9XHJcbiAgQFZpZXdDaGlsZChcInBhZ2luYXRvclwiKSBwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuICBAVmlld0NoaWxkKFwic2VsZWN0ZWRQYWdpbmF0b3JcIikgc2VsZWN0ZWRQYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuICBuZ09uSW5pdCgpIHtcclxuICAgIC8vIHRoaXMuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLCBcIi4uLi4uLnRhYmxlIHRoaXNcIik7XHJcbiAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJkYXRhc291cmNlXCIpO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtcyA9IHtcclxuICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgICBsaW1pdDogMTAsXHJcbiAgICAgIGRhdGFzb3VyY2U6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2VcclxuICAgIH07XHJcbiAgICAvLyAgdGhpcy5wYWdpbmF0b3JbXCJwYWdlSW5kZXhcIl0gPSAwO1xyXG4gICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wKSA/IEpTT04ucGFyc2UodGVtcCkgOiB7fTtcclxuICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICApO1xyXG4gICAgLy8gaWYgKHRoaXMuZGF0YSA9PSAnbGlzdFZpZXcnIHx8IHRoaXMuZGF0YSA9PSAncmVmcmVzaFBhZ2UnKSB7XHJcbiAgICAvLyAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAvLyAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgIC8vIH1cclxuICAgIHRoaXMuaW5wdXREYXRhW1wiZGF0YXNvdXJjZUlkXCJdID0gdGhpcy5kZWZhdWx0RGF0YXNvdXJjZTtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAvLyB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgIHRoaXMuZnJvbUZpZWxkVmFsdWUgPSBuZXcgRm9ybUNvbnRyb2woW1wiXCJdKTtcclxuICAgIHRoaXMudG9GaWVsZFZhbHVlID0gbmV3IEZvcm1Db250cm9sKFtcIlwiXSk7XHJcbiAgICB0aGlzLm9uTG9hZERhdGEobnVsbCk7XHJcblxyXG4gIH1cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIm5nQWZ0ZXJWaWV3SW5pdCBcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdGVkUGFnaW5hdG9yLCBcInNlbGVjdGVkUGFnPj4+PlwiKTtcclxuICAgIGlmICh0aGlzLmRhdGFTb3VyY2UgJiYgdGhpcy5zZWxlY3RlZFBhZ2luYXRvcikge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5zZWxlY3RlZFBhZ2luYXRvcjtcclxuICAgIH1cclxuXHJcbiAgfVxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy51bnN1YnNjcmliZS5uZXh0KCk7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlTW9kZWwubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlVGFibGVWaWV3KCkge1xyXG4gICAgbGV0IGN1cnJlbnRUYWJsZUhlYWRlciA9IEpTT04ucGFyc2UoXHJcbiAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwic2VsZWN0ZWRUYWJsZUhlYWRlcnNcIilcclxuICAgICk7XHJcbiAgICB0aGlzLmNvbHVtbnMgPSBjdXJyZW50VGFibGVIZWFkZXI7XHJcbiAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmNvbHVtbnNcclxuICAgICAgLmZpbHRlcihmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbC5pc0FjdGl2ZTtcclxuICAgICAgfSlcclxuICAgICAgLm1hcChmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgcmV0dXJuIHZhbC52YWx1ZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG4gIG9uTG9hZERhdGEoZW5hYmxlTmV4dCkge1xyXG4gICAgdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZT0hXy5pc0VtcHR5KHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUpID8gdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSA6IHt9O1xyXG4gICAgdGhpcy5jb2x1bW5zID0gW107XHJcbiAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSBbXTtcclxuICAgIGxldCB0YWJsZUFycmF5ID0gdGhpcy5vbkxvYWRcclxuICAgICAgPyB0aGlzLm9uTG9hZC50YWJsZURhdGFcclxuICAgICAgOiB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl0udGFibGVEYXRhO1xyXG4gICAgbGV0IHJlc3BvbnNlS2V5ID1cclxuICAgICAgdGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQucmVzcG9uc2VLZXkgPyB0aGlzLm9uTG9hZC5yZXNwb25zZUtleSA6IG51bGw7XHJcbiAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleCh0YWJsZUFycmF5LCB7IHRhYmxlSWQ6IHRoaXMudGFibGVJZCB9KTtcclxuICAgIC8vaWYoaW5kZXg+LTEpXHJcbiAgICB0aGlzLmN1cnJlbnRUYWJsZURhdGEgPSB0YWJsZUFycmF5W2luZGV4XTtcclxuICAgIGNvbnNvbGUubG9nKHRhYmxlQXJyYXksIFwiLi4uLi50YWJsZUFycmF5XCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50VGFibGVEYXRhLCBcIi4uLi4uY3VycmVudFRhYmxlRGF0YVwiKTtcclxuICAgIGxldCBjaGVja09wdGlvbkZpbHRlciA9ICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLmVuYWJsZU9wdGlvbnNGaWx0ZXIpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgaWYgKGNoZWNrT3B0aW9uRmlsdGVyKSB7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgdGhpcy5lbmFibGVPcHRpb25GaWx0ZXIgPSB0cnVlO1xyXG4gICAgICBsZXQgT3B0aW9uZGF0YSA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5maWx0ZXJPcHRpb25Mb2FkRnVuY3Rpb247XHJcbiAgICAgIHRoaXMub3B0aW9uRmlsdGVyRmllbGRzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLm9wdGlvbkZpZWxkcztcclxuICAgICAgdmFyIGFwaVVybCA9IE9wdGlvbmRhdGEuYXBpVXJsO1xyXG4gICAgICB2YXIgcmVzcG9uc2VOYW1lID0gT3B0aW9uZGF0YS5yZXNwb25zZTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UodGhpcy5xdWVyeVBhcmFtcywgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICAgIGxldCB0ZW1wQXJyYXkgPSAocmVzLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0pID8gcmVzLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gOiBbXTtcclxuXHJcbiAgICAgICAgICBzZWxmLm9wdGlvbkZpbHRlcmRhdGEgPSB0ZW1wQXJyYXk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5lbmFibGVVaWZpbHRlcikge1xyXG4gICAgICBsZXQgZmlsdGVyS2V5ID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnVpRmlsdGVyS2V5O1xyXG4gICAgICB0aGlzLmVuYWJsZVVJZmlsdGVyID0gdHJ1ZTtcclxuICAgICAgLy8gdGhpcy5kYXRhU291cmNlLmZpbHRlclByZWRpY2F0ZSA9IChkYXRhOiBFbGVtZW50LCBmaWx0ZXI6IHN0cmluZykgPT4ge1xyXG4gICAgICAvLyAgIHJldHVybiBkYXRhW2ZpbHRlcktleV0gPT0gZmlsdGVyO1xyXG4gICAgICAvLyAgfTtcclxuICAgIH1cclxuICAgLy8gaWYodGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQuZ2V0RnJvbWN1cnJlbnRJbnB1dCl7XHJcbiAgICBpZih0aGlzLm9uTG9hZCAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEuY2hlY2tGcm9tSW5wdXREYXRhKXtcclxuICAgICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgICBsZXQgZGF0YT0odGhpcy5jdXJyZW50VGFibGVEYXRhLmtleVRvc2F2ZSAmJiB0aGlzLmlucHV0RGF0YVt0aGlzLmN1cnJlbnRUYWJsZURhdGEua2V5VG9zYXZlXSk/dGhpcy5pbnB1dERhdGFbdGhpcy5jdXJyZW50VGFibGVEYXRhLmtleVRvc2F2ZV06W107XHJcbiAgICAgIGlmKGRhdGEubGVuZ3RoPjApe1xyXG4gICAgICAgIHRoaXMub25Mb2FkLnJlc3BvbnNlPWRhdGE7XHJcbiAgICAgICAgdGhpcy5vbkxvYWQudG90YWw9ZGF0YS5sZW5ndGg7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKFwiPj4+PiB0aGlzLm9uTG9hZCBcIix0aGlzLm9uTG9hZCk7XHJcblxyXG4gICAgaWYgKHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLnJlc3BvbnNlKSB7XHJcbiAgICAgIHRoaXMuZ2V0TG9hZERhdGEoXHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVEYXRhLFxyXG4gICAgICAgIHRoaXMub25Mb2FkLnJlc3BvbnNlLFxyXG4gICAgICAgIHRoaXMub25Mb2FkLnRvdGFsLFxyXG4gICAgICAgIHJlc3BvbnNlS2V5XHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhKSB0aGlzLmdldERhdGEodGhpcy5jdXJyZW50VGFibGVEYXRhKTtcclxuICAgIH1cclxuICAgIGlmICghZW5hYmxlTmV4dCkge1xyXG4gICAgICAvLyB0aGlzLnBhZ2luYXRvci5maXJzdFBhZ2UoKTtcclxuICAgIH1cclxuICB9XHJcbiAgZ2V0RGF0YShjdXJyZW50VGFibGVWYWx1ZSkge1xyXG4gICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wKSA/IEpTT04ucGFyc2UodGVtcCkgOiB7fTtcclxuICAgIHRoaXMuY29sdW1ucyA9IGN1cnJlbnRUYWJsZVZhbHVlLnRhYmxlSGVhZGVyO1xyXG4gICAgaWYgKGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uKSB7XHJcbiAgICAgIHZhciBhcGlVcmwgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5hcGlVcmw7XHJcbiAgICAgIHZhciByZXNwb25zZU5hbWUgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5yZXNwb25zZTtcclxuICAgICAgbGV0IGtleVRvU2V0ID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24ua2V5Rm9yU3RyaW5nUmVzcG9uc2U7XHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEgPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl07XHJcbiAgICAgIHZhciBpbmNsdWRlUmVxdWVzdCA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmluY2x1ZGVyZXF1ZXN0RGF0YSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgLy9pZiAoIGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmVuYWJsZUxvYWRlcikge1xyXG4gICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgICAgLy99XHJcbiAgICAgIGxldCBkeW5hbWljTG9hZCA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmR5bmFtaWNSZXBvcnRMb2FkOyAgLy8gY29uZGl0aW9uIGNoZWNrZWQgd2hlbiBpbXBsZW1lbnQgYWRkaXRpb25hbCByZXBvcnQgdmlld1xyXG4gICAgICBsZXQgcmVxdWVzdGVkUXVlcnkgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB2YXIgcmVxdWVzdFF1ZXJ5UGFyYW0gPSB7fTtcclxuICAgICAgaWYgKGR5bmFtaWNMb2FkKSB7XHJcbiAgICAgICAgbGV0IHRlbXBPYmogPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIkN1cnJlbnRSZXBvcnREYXRhXCIpO1xyXG4gICAgICAgIGxldCByZXBvcnRJVGVtID0gSlNPTi5wYXJzZSh0ZW1wT2JqKTtcclxuICAgICAgICBfLmZvckVhY2gocmVxdWVzdGVkUXVlcnksIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmZyb21DdXJyZW50RGF0YSkge1xyXG4gICAgICAgICAgICByZXF1ZXN0UXVlcnlQYXJhbVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuY3VycmVudERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXF1ZXN0UXVlcnlQYXJhbVtyZXF1ZXN0SXRlbS5uYW1lXSA9IChyZXBvcnRJVGVtW3JlcXVlc3RJdGVtLnZhbHVlXSkgPyByZXBvcnRJVGVtW3JlcXVlc3RJdGVtLnZhbHVlXSA6IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0ZWRRdWVyeSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uZnJvbUN1cnJlbnREYXRhKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RRdWVyeVBhcmFtW3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5jdXJyZW50RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgIH0gZWxzZSBpZiAocmVxdWVzdEl0ZW0uZGlyZWN0QXNzaWduKSB7XHJcbiAgICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHJlcXVlc3RJdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICAgIDogcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0pXHJcbiAgICAgICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IHsgLi4udGhpcy5xdWVyeVBhcmFtcywgLi4ucmVxdWVzdFF1ZXJ5UGFyYW0gfTtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRBbGxSZXBvbnNlKHRoaXMucXVlcnlQYXJhbXMsIGFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgLy8gaWYgKCBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5lbmFibGVMb2FkZXIpIHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAvL31cclxuICAgICAgICAgIHRoaXMuZW5hYmxlU2VhcmNoID0gdGhpcy5jdXJyZW50RGF0YS5lbmFibGVHbG9iYWxTZWFyY2g7XHJcbiAgICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmNvbHVtbnNcclxuICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHZhbC5pc0FjdGl2ZTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm1hcChmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHZhbC52YWx1ZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICBsZXQgdGVtcEFycmF5ID0gW107XHJcbiAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhzZWxmLFwiPj4+Pj4+Pj4+PlRISVNTU1NcIik7XHJcbiAgICAgICAgICBfLmZvckVhY2goZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdLCBmdW5jdGlvbiAoaXRlbSwgaW5kZXgpIHtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBpdGVtICE9PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgICAgICB0ZW1wT2JqW2tleVRvU2V0XSA9IGl0ZW07XHJcbiAgICAgICAgICAgICAgbGV0IGZpbHRlck9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgIGZpbHRlck9ialtjdXJyZW50VGFibGVWYWx1ZS5maWx0ZXJLZXldID0gdGVtcE9ialtjdXJyZW50VGFibGVWYWx1ZS5kYXRhRmlsdGVyS2V5XTtcclxuICAgICAgICAgICAgICBsZXQgc2VsZWN0ZWRWYWx1ZSA9IF8uZmluZChzZWxmLmlucHV0RGF0YVtjdXJyZW50VGFibGVWYWx1ZS5zZWxlY3RlZFZhbHVlc0tleV0sIGZpbHRlck9iaik7XHJcbiAgICAgICAgICAgICAgaWYoc2VsZWN0ZWRWYWx1ZSl7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqWydjaGVja2VkJ10gPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50VGFibGVWYWx1ZS5kYXRhVmlld0Zvcm1hdCwgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodmlld0l0ZW0uc3Via2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgIGlmICh2aWV3SXRlbS5hc3NpZ25GaXJzdEluZGV4dmFsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlc3VsdFZhbHVlID0gKCFfLmlzRW1wdHkoaXRlbVt2aWV3SXRlbS52YWx1ZV0pICYmIGl0ZW1bdmlld0l0ZW0udmFsdWVdWzBdW3ZpZXdJdGVtLnN1YmtleV0pID8gKGl0ZW1bdmlld0l0ZW0udmFsdWVdWzBdW3ZpZXdJdGVtLnN1YmtleV0pIDogXCJcIjtcclxuICAgICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgICAgID8gcmVzdWx0VmFsdWVcclxuICAgICAgICAgICAgICAgICAgICAgIDogXCJcIjtcclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgICAgID8gaXRlbVt2aWV3SXRlbS52YWx1ZV1bdmlld0l0ZW0uc3Via2V5XVxyXG4gICAgICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZpZXdJdGVtLmlzQ29uZGl0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBldmFsKHZpZXdJdGVtLmNvbmRpdGlvbik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZpZXdJdGVtLmlzQWRkRGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSB2aWV3SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFRhYmxlVmFsdWUubG9hZExhYmVsRnJvbUNvbmZpZykge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID1cclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50VGFibGVWYWx1ZS5oZWFkZXJMaXN0W2l0ZW1bdmlld0l0ZW0udmFsdWVdXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICBsZXQgZmlsdGVyT2JqID0ge307XHJcbiAgICAgICAgICAgICAgZmlsdGVyT2JqW2N1cnJlbnRUYWJsZVZhbHVlLmZpbHRlcktleV0gPSBpdGVtW2N1cnJlbnRUYWJsZVZhbHVlLmRhdGFGaWx0ZXJLZXldO1xyXG4gICAgICAgICAgICAgIGxldCBzZWxlY3RlZFZhbHVlID0gXy5maW5kKHNlbGYuaW5wdXREYXRhW2N1cnJlbnRUYWJsZVZhbHVlLnNlbGVjdGVkVmFsdWVzS2V5XSwgZmlsdGVyT2JqKTtcclxuICAgICAgICAgICAgICBpZihzZWxlY3RlZFZhbHVlKXtcclxuICAgICAgICAgICAgICAgIGl0ZW0uY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHRlbXBPYmogPSB7IC4uLml0ZW0sIC4uLnRlbXBPYmogfTtcclxuICAgICAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBpZiAodGVtcEFycmF5ICYmIHRlbXBBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy50YWJsZURhdGEgPSB0ZW1wQXJyYXkgYXMgb2JqZWN0W107XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnRhYmxlRGF0YSA9XHJcbiAgICAgICAgICAgICAgZGF0YSAmJiBkYXRhLnJlc3BvbnNlICYmIGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXVxyXG4gICAgICAgICAgICAgICAgPyAoZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdKVxyXG4gICAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChkYXRhICYmIGRhdGEucmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgdGhpcy5sZW5ndGggPSBkYXRhLnJlc3BvbnNlLnRvdGFsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLnRhYmxlRGF0YSk7XHJcbiAgICAgICAgICAvLyB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5zZWxlY3RlZFBhZ2luYXRvcjsgLy8gbmV3bHkgYWRkZWQgXHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuICAgICAgICAgIHZhciBmaWx0ZXJlZEtleSA9ICh0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSkgPyB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSA6ICcnO1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHNlbGYuY29sdW1ucywgZnVuY3Rpb24gKHgpIHtcclxuICAgICAgICAgICAgaWYgKGZpbHRlcmVkS2V5ID09IHgudmFsdWUpIHtcclxuICAgICAgICAgICAgICBzZWxmLmlucHV0RGF0YVt4LmtleVRvU2F2ZV0gPSBzZWxmLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3gua2V5VG9TYXZlXSA9IG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgIC8vIHRoaXMuc2VsZWN0QWxsQ2hlY2soeyBjaGVja2VkOiBmYWxzZSB9KTtcclxuICAgICAgICAgIC8vIHRoaXMuc2VsZWN0QWxsID0gZmFsc2U7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdEFsbCwgXCJTZWxlY3RBbGw+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgICAgICAgIC8vIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdEFsbCwgXCJTZWxlY3RBbGw+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgLy8gdGhpcy5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcblxyXG4gICAgICAgICAgdGhpcy5jaGVja0NsaWNrRXZlbnRNZXNzYWdlLmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiBFcnJvciBcIiwgZXJyKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuICBnZXRMb2FkRGF0YShjdXJyZW50VmFsdWUsIHJlc3BvbnNlVmFsdWUsIHRvdGFsTGVuZ3RoLCByZXNwb25zZUtleSkge1xyXG4gICAgY29uc29sZS5sb2coY3VycmVudFZhbHVlLCBcIi4uLi5jdXJyZW50VmFsdWVcIik7XHJcbiAgICBsZXQgdGVtcEFycmF5ID0gW107XHJcbiAgICAvLyBpZiAocmVzcG9uc2VWYWx1ZSAmJiByZXNwb25zZVZhbHVlW3Jlc3BvbnNlS2V5XSkge1xyXG4gICAgcmVzcG9uc2VWYWx1ZSA9IHJlc3BvbnNlS2V5ID8gcmVzcG9uc2VWYWx1ZVtyZXNwb25zZUtleV0gOiByZXNwb25zZVZhbHVlO1xyXG4gICAgaWYgKHJlc3BvbnNlVmFsdWUgJiYgcmVzcG9uc2VWYWx1ZS5sZW5ndGgpIHtcclxuICAgICAgXy5mb3JFYWNoKHJlc3BvbnNlVmFsdWUsIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xyXG4gICAgICAgIGlmICh0eXBlb2YgaXRlbSAhPT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgIHRlbXBPYmpbY3VycmVudFZhbHVlLmtleVRvU2V0XSA9IGl0ZW07XHJcbiAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50VmFsdWUuZGF0YVZpZXdGb3JtYXQsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICBpZiAodmlld0l0ZW0uc3Via2V5KSB7XHJcbiAgICAgICAgICAgICAgaWYgKHZpZXdJdGVtLmFzc2lnbkZpcnN0SW5kZXh2YWwpIHtcclxuICAgICAgICAgICAgICAgIGxldCByZXN1bHRWYWx1ZSA9ICghXy5pc0VtcHR5KGl0ZW1bdmlld0l0ZW0udmFsdWVdKSAmJiBpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA/IChpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgPyByZXN1bHRWYWx1ZVxyXG4gICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICA/IGl0ZW1bdmlld0l0ZW0udmFsdWVdW3ZpZXdJdGVtLnN1YmtleV1cclxuICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGVtcE9iaiA9IHsgLi4uaXRlbSwgLi4udGVtcE9iaiB9O1xyXG4gICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHRoaXMuY29sdW1ucyA9IGN1cnJlbnRWYWx1ZS50YWJsZUhlYWRlcjtcclxuICAgIHJlc3BvbnNlVmFsdWUgPSB0ZW1wQXJyYXkubGVuZ3RoID8gdGVtcEFycmF5IDogcmVzcG9uc2VWYWx1ZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIi4uLklOUFVUREFUQVwiKTtcclxuICAgIGlmIChjdXJyZW50VmFsdWUgJiYgY3VycmVudFZhbHVlLm1hcERhdGFGdW5jdGlvbikge1xyXG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICAgIF8uZm9yRWFjaChjdXJyZW50VmFsdWUubWFwRGF0YUZ1bmN0aW9uLCBmdW5jdGlvbiAobWFwSXRlbSkge1xyXG4gICAgICAgIF8uZm9yRWFjaChtYXBJdGVtLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0pXHJcbiAgICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgIC5nZXRBbGxSZXBvbnNlKHNlbGYucXVlcnlQYXJhbXMsIG1hcEl0ZW0uYXBpVXJsKVxyXG4gICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cobWFwSXRlbSwgXCI+Pj4+Pj4+IE1BUCBJVEVNXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIi4uLi4uREFUQUFBQVwiKTtcclxuICAgICAgICAgICAgbGV0IHRlbXBPYmogPSBfLmtleUJ5KGRhdGEucmVzcG9uc2VbbWFwSXRlbS5yZXNwb25zZV0sIFwiX2lkXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0ZW1wT2JqLCBcIi4uLi50ZW1wT2JqXCIpO1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVzcG9uc2VWYWx1ZSwgZnVuY3Rpb24gKHJlc3BvbnNlSXRlbSkge1xyXG4gICAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgIGRhdGEucmVzcG9uc2UgJiZcclxuICAgICAgICAgICAgICAgIGRhdGEucmVzcG9uc2Uua2V5Rm9yQ2hlY2sgPT0gbWFwSXRlbS5rZXlGb3JDaGVjayAmJlxyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2VJdGVtW21hcEl0ZW0ua2V5Rm9yQ2hlY2tdXHJcbiAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZUl0ZW1bbWFwSXRlbS5zaG93S2V5XSA9XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmogJiYgcmVzcG9uc2VJdGVtW21hcEl0ZW0ua2V5Rm9yQ2hlY2tdXHJcbiAgICAgICAgICAgICAgICAgICAgPyB0ZW1wT2JqW3Jlc3BvbnNlSXRlbVttYXBJdGVtLmtleUZvckNoZWNrXV0ub2JqZWN0VHlwZXNcclxuICAgICAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgICAgICAgcmVzcG9uc2VJdGVtW21hcEl0ZW0ubW9kZWxLZXldID0gcmVzcG9uc2VJdGVtW21hcEl0ZW0uc2hvd0tleV07XHJcbiAgICAgICAgICAgICAgfSBlbHNlXHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZUl0ZW0sIFwiLi4uLi5yZXNwb25zZUl0ZW1cIik7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZVZhbHVlLCBcIj4+Pj4+cmVzcG9uc2VWYWx1ZVwiKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZSk7XHJcbiAgICAgIHRoaXMubGVuZ3RoID0gdG90YWxMZW5ndGggPyB0b3RhbExlbmd0aCA6IHJlc3BvbnNlVmFsdWUubGVuZ3RoO1xyXG4gICAgfVxyXG4gICAgdGhpcy50YWJsZURhdGEgPSB0ZW1wQXJyYXkgYXMgb2JqZWN0W107XHJcbiAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlVmFsdWUpO1xyXG4gICAgdGhpcy5sZW5ndGggPSB0b3RhbExlbmd0aCA/IHRvdGFsTGVuZ3RoIDogcmVzcG9uc2VWYWx1ZS5sZW5ndGg7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNvbHVtbnMsIFwiPj4+Pj5DT0xVTU5TU1wiKTtcclxuICAgIC8vIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IF8ubWFwKHRoaXMuY29sdW1ucywgXCJ2YWx1ZVwiKTtcclxuICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IHRoaXMuY29sdW1uc1xyXG4gICAgICAuZmlsdGVyKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsLmlzQWN0aXZlO1xyXG4gICAgICB9KVxyXG4gICAgICAubWFwKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsLnZhbHVlO1xyXG4gICAgICB9KTtcclxuICAgIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnNlbGVjdGVkUGFnaW5hdG9yO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZFBhZ2luYXRvciwgXCIuLi4uLi4uLi4uLi4uLi4uLi4uLiBzZWxlY3RlZFBhZ2luYXRvclwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0YVNvdXJjZSwgXCIuLi4uLi4uLi5EQVRBU09VUkNFRUVFXCIpO1xyXG4gICAgdGhpcy5kaXNhYmxlUGFnaW5hdG9yPWN1cnJlbnRWYWx1ZS5kaXNhYmxlUGFnaW5hdGlvbj9jdXJyZW50VmFsdWUuZGlzYWJsZVBhZ2luYXRpb246ZmFsc2U7XHJcbiAgICAvL3BhZ2luYXRvckVuYWJsZVxyXG4gICAgLy8gdGhpcy5kYXRhU291cmNlID0gcmVzcG9uc2VLZXlcclxuICAgIC8vICAgPyBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlVmFsdWVbcmVzcG9uc2VLZXldKVxyXG4gICAgLy8gICA6IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZSk7XHJcbiAgfVxyXG4gIHVwZGF0ZUNoZWNrQ2xpY2soc2VsZWN0ZWQsIGV2ZW50KSB7XHJcbiAgICAvLyBzZWxlY3RlZC50eXBlID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlVHlwZVxyXG4gICAgLy8gICA/IHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZVR5cGVcclxuICAgIC8vICAgOiBcIlwiO1xyXG4gICAgaWYodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZVR5cGUpXHJcbiAgICB7XHJcbiAgICAgIHNlbGVjdGVkLnR5cGUgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVUeXBlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHNlbGVjdGVkLnR5cGVJZCA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS50eXBlSWRcclxuICAgIC8vICAgPyB0aGlzLmN1cnJlbnRUYWJsZURhdGEudHlwZUlkXHJcbiAgICAvLyAgIDogXCJcIjtcclxuICAgICAgbGV0IHNlY3VyaXR5VHlwZUxlbmd0aCA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5zZWN1cml0eVR5cGVMZW5ndGg/TnVtYmVyKHRoaXMuY3VycmVudFRhYmxlRGF0YS5zZWN1cml0eVR5cGVMZW5ndGgpOjM7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwidGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5VHlwZUxlbmd0aD8/Pz9cIix0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlUeXBlTGVuZ3RoKVxyXG4gICAgICBjb25zb2xlLmxvZyhcIj4+PiBzZWN1cml0eVR5cGVMZW5ndGhzc3MgXCIsc2VjdXJpdHlUeXBlTGVuZ3RoKTtcclxuICAgIC8vIGlmIChzZWxlY3RlZCAmJiBzZWxlY3RlZC50eXBlSWQpIHtcclxuICAgIC8vICAgaWYgKHNlbGVjdGVkLnR5cGVJZCA9PSBcIjJcIikge1xyXG4gICAgLy8gICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9IFs0XTtcclxuICAgIC8vICAgICBzZWxlY3RlZFtcImxldmVsXCJdID0gWzRdO1xyXG4gICAgLy8gICB9IGVsc2Uge1xyXG4gICAgLy8gICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9IFsxLCAyLCAzXTtcclxuICAgIC8vICAgICBzZWxlY3RlZFtcImxldmVsXCJdID0gWzEsIDIsIDNdO1xyXG4gICAgLy8gICAgfVxyXG4gICAgICAgICBsZXQgdGVtcEFycmF5PVtdO1xyXG4gICAgICAgICBsZXQgcHJlZGVmaW5lZEFycmF5ID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5QXJyYXlWYWx1ZT9KU09OLnBhcnNlKFwiW1wiICsgdGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5QXJyYXlWYWx1ZSArIFwiXVwiKTp0ZW1wQXJyYXk7XHJcbiAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+IHByZWRlZmluZWRBcnJheSBcIixwcmVkZWZpbmVkQXJyYXkpO1xyXG4gICAgICAgICBpZihzZWN1cml0eVR5cGVMZW5ndGggJiYgc2VjdXJpdHlUeXBlTGVuZ3RoPjApXHJcbiAgICAgICAgIHtcclxuICAgICAgICAgICBjb25zb2xlLmxvZyhcImluc2lkZVwiKVxyXG4gICAgICAgICAgICBpZihwcmVkZWZpbmVkQXJyYXkgJiYgcHJlZGVmaW5lZEFycmF5Lmxlbmd0aClcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIHRlbXBBcnJheT1wcmVkZWZpbmVkQXJyYXk7XHJcbiAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBmb3IobGV0IGk9MTtpPD1zZWN1cml0eVR5cGVMZW5ndGg7aSsrKVxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgdGVtcEFycmF5LnB1c2goaSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0gPXRlbXBBcnJheTtcclxuICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJvdXRzaWRlXCIpXHJcbiAgICAgICAgICAgICAgaWYoc2VjdXJpdHlUeXBlTGVuZ3RoPT0wKVxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl09IHRlbXBBcnJheTtcclxuICAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAgIHsgICBcclxuICAgICAgICAgICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0gPSBbMSwgMiwgM107ICAgXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICB9XHJcbiAgICAvL31cclxuICAgIGNvbnNvbGUubG9nKFwiPj4+ICBzZWxlY3RlZCBzZWN1cml0eV90eXBlIFwiLCBzZWxlY3RlZCk7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5kYXRhRm9ybWF0VG9TYXZlKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmN1cnJlbnRUYWJsZURhdGEuZGF0YUZvcm1hdFRvU2F2ZSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBzZWxlY3RlZFtpdGVtLm5hbWVdID0gc2VsZWN0ZWRbaXRlbS52YWx1ZV07XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBsZXQgc2F2ZWRTZWxlY3RlZERhdGEgPVxyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEgJiYgdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhLmxlbmd0aFxyXG4gICAgICAgID8gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhXHJcbiAgICAgICAgOiBbXTtcclxuICAgIHRoaXMuc2VsZWN0ZWREYXRhID0gXy5tZXJnZShzYXZlZFNlbGVjdGVkRGF0YSwgdGhpcy5zZWxlY3RlZERhdGEpO1xyXG4gICAgaWYgKGV2ZW50LmNoZWNrZWQpIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGEucHVzaChzZWxlY3RlZCk7XHJcbiAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgLy8gbGV0IGluZGV4ID0gdGhpcy5zZWxlY3RlZERhdGEuZmluZEluZGV4KFxyXG4gICAgICAvLyAgIG9iaiA9PiBvYmoub2JqZWN0X25hbWUgPT09IHNlbGVjdGVkLm9iamVjdF9uYW1lXHJcbiAgICAgIC8vICk7XHJcbiAgICAgIGxldCBvYmplY3RLZXk9dGhpcy5jdXJyZW50VGFibGVEYXRhLm9iamVjdEtleVRvQ2hlY2s/dGhpcy5jdXJyZW50VGFibGVEYXRhLm9iamVjdEtleVRvQ2hlY2s6XCJvYmpfbmFtZVwiO1xyXG4gICAgICBsZXQgaW5kZXggPSB0aGlzLnNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgIG9iaiA9PiBvYmpbb2JqZWN0S2V5XSA9PT0gc2VsZWN0ZWRbb2JqZWN0S2V5XVxyXG4gICAgICAgKTtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKFwiPj4+PiB0aGlzLnNlbGVjdGVkRGF0YSBcIix0aGlzLnNlbGVjdGVkRGF0YSk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IHRoaXMuc2VsZWN0ZWREYXRhO1xyXG4gICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5zYXZlSW5BcnJheSkge1xyXG4gICAgICBsZXQgYXJyYXlPYmogPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2F2ZUluQXJyYXk7XHJcbiAgICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVJZCA9PT0gYXJyYXlPYmoua2V5KSB7XHJcbiAgICAgICAgbGV0IHRlbXBBcnJheSA9XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRGF0YSB8fCB0aGlzLnNlbGVjdGVkRGF0YS5sZW5ndGhcclxuICAgICAgICAgICAgPyBfLm1hcCh0aGlzLnNlbGVjdGVkRGF0YSwgYXJyYXlPYmoudmFsdWVUb01hcClcclxuICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICB0ZW1wQXJyYXkgPSB0ZW1wQXJyYXkuZmlsdGVyKGZ1bmN0aW9uIChlbGVtZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gZWxlbWVudCAhPSBudWxsO1xyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW2FycmF5T2JqLmtleV0gPSB0ZW1wQXJyYXk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAgICAgLy8gY2hlY2sgYm94IHJlbWFpbiBzZWFyY2ggY29sdW1uIHZhbHVlIHN0YXJ0XHJcbiAgICAgICB2YXIgZmlsdGVyZWRLZXkgPSAodGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZS5zZWFyY2hLZXkpID8gdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZS5zZWFyY2hLZXkgOiAnJztcclxuICAgICAgICBpZihmaWx0ZXJlZEtleSl7XHJcbiAgICAgICAgdmFyIHNlbGY9dGhpcztcclxuICAgICAgICBfLmZvckVhY2goc2VsZi5jb2x1bW5zLCBmdW5jdGlvbiAoeCkge1xyXG4gICAgICAgICAgaWYgKGZpbHRlcmVkS2V5ID09IHgudmFsdWUpIHtcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbeC5rZXlUb1NhdmVdID0gc2VsZi5jdXJyZW50RmlsdGVyZWRWYWx1ZS5zZWFyY2hWYWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3gua2V5VG9TYXZlXSA9IG51bGw7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgICAgICAgLy8gY2hlY2sgYm94IHJlbWFpbiBzZWFyY2ggY29sdW1uIHZhbHVlIHN0YXJ0XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAodGhpcy5zZWxlY3RlZERhdGEsIFwiX2lkXCIpO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICB9XHJcblxyXG4gIHNlbGVjdEFsbENoZWNrKGV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQuY2hlY2tlZCA9PT0gdHJ1ZSkge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICBvYmouY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IHRoaXMuZGF0YVNvdXJjZS5kYXRhO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAodGhpcy5kYXRhU291cmNlLmRhdGEsIFwiX2lkXCIpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLmRhdGEubWFwKG9iaiA9PiB7XHJcbiAgICAgICAgb2JqLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgIH1cclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB0aGlzLmNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgfVxyXG4gIGFkZEZpZWxkQ2xpY2soaXRlbSwgb2JqZWN0TGlzdCkge1xyXG4gICAgY29uc29sZS5sb2coaXRlbSwgXCIuLi4uLi4uLi4uSVRFTVwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIi4uLi4uLi5JTlBVVCBEQXRhXCIpXHJcbiAgICBjb25zb2xlLmxvZyhvYmplY3RMaXN0LCBcIi4uLi4uLk9iamVjdExpc3RcIik7XHJcbiAgfVxyXG5cclxuICBzZWxlY3RBbGxEYXRhKCkge1xyXG4gICAgbGV0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50RGF0YS5zZWxlY3RBbGxSZXF1ZXN0O1xyXG4gICAgbGV0IHF1ZXJ5ID0ge307XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIHF1ZXJ5W2l0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAuZ2V0QWxsUmVwb25zZShxdWVyeSwgcmVxdWVzdERldGFpbHMuYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPVxyXG4gICAgICAgICAgZGF0YS5yZXNwb25zZVtyZXF1ZXN0RGV0YWlscy5yZXNwb25zZU5hbWVdO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgY2xlYXJTZWxlY3RBbGxEYXRhICgpIHtcclxuICAgIHRoaXMubmdPbkluaXQoKTtcclxuICB9XHJcbiAgYXBwbHlGaWx0ZXIoZXZlbnQsIGtleSkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+IGV2ZW50IFwiLCBldmVudCwgXCIga2V5IFwiLCBrZXkpO1xyXG4gICAgdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSA9IHtcclxuICAgICAgc2VhcmNoS2V5OiBrZXksXHJcbiAgICAgIHNlYXJjaFZhbHVlOiBldmVudFxyXG4gICAgfTtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUgIFwiLHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUgKTtcclxuXHJcbiAgICB0aGlzLmlucHV0RGF0YVtrZXldPWV2ZW50OyAvLyBzZWFyY2hlZCB2YWx1ZSBzZXRcclxuICAgIGxldCB0YWJsZUFycmF5ID1cclxuICAgICAgdGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQudGFibGVEYXRhXHJcbiAgICAgICAgPyB0aGlzLm9uTG9hZC50YWJsZURhdGFcclxuICAgICAgICA6IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXS50YWJsZURhdGE7XHJcbiAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleCh0YWJsZUFycmF5LCB7IHRhYmxlSWQ6IHRoaXMudGFibGVJZCB9KTtcclxuICAgIGxldCBzZWFyY2hSZXF1ZXN0ID0gdGFibGVBcnJheVtpbmRleF0ub25UYWJsZVNlYXJjaDtcclxuICAgIGxldCByZXF1ZXN0ID0gc2VhcmNoUmVxdWVzdC5yZXF1ZXN0RGF0YTtcclxuICAgIGxldCBpc1Vpc2VyYWNoID0gKHNlYXJjaFJlcXVlc3QudWlTZWFyY2gpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgaWYgKGlzVWlzZXJhY2gpIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlLmZpbHRlciA9IGV2ZW50O1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jdXJyZW50VGFibGVEYXRhID0gdGFibGVBcnJheVtpbmRleF07XHJcbiAgICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgICBvZmZzZXQ6IDAsXHJcbiAgICAgICAgbGltaXQ6IDEwLFxyXG4gICAgICAgIGRhdGFzb3VyY2U6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2VcclxuICAgICAgfTtcclxuXHJcbiAgICAgIGlmIChzZWFyY2hSZXF1ZXN0ICYmIHNlYXJjaFJlcXVlc3QuaXNTaW5nbGVyZXF1ZXN0KSB7XHJcbiAgICAgICAgcXVlcnlbc2VhcmNoUmVxdWVzdC5zaW5nbGVSZXF1ZXN0S2V5XSA9IGV2ZW50O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICBfLmZvckVhY2gocmVxdWVzdCwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgIGlmIChzZWxmLmlucHV0RGF0YVtpdGVtLm5hbWVdKVxyXG4gICAgICAgICAgICBxdWVyeVtpdGVtLnZhbHVlXSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZV07IC8vIG9uVGFibGUgU2VhcmNoIFZhbHVlIHBhc3NlZCBpbnN0ZWFkIG9mIG5hbWUgXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IHF1ZXJ5O1xyXG4gICAgICB0aGlzLmdldERhdGEodGhpcy5jdXJyZW50VGFibGVEYXRhKTtcclxuICAgIH1cclxuXHJcbiAgfVxyXG5cclxuICBhcHBseUZpbHRlclNlYXJjaChldmVudEl0ZW0sIGtleSkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gYXBwbHlGaWx0ZXJTZWFyY2hcIik7XHJcbiAgICBldmVudEl0ZW0ucHJldmVudERlZmF1bHQoKTtcclxuICAgIGxldCBldmVudCA9IGV2ZW50SXRlbS50YXJnZXQudmFsdWU7XHJcbiAgICB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlID0ge307XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+IGV2ZW50IFwiLCBldmVudCwgXCIga2V5IFwiLCBrZXkpO1xyXG4gICAgdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSA9IHtcclxuICAgICAgICBzZWFyY2hLZXk6IGtleSxcclxuICAgICAgICBzZWFyY2hWYWx1ZTogZXZlbnRcclxuICAgIH07XHJcbiAgICB0aGlzLmlucHV0RGF0YVtrZXldID0gZXZlbnQ7IC8vIHNlYXJjaGVkIHZhbHVlIHNldFxyXG4gICAgbGV0IHRhYmxlQXJyYXkgPVxyXG4gICAgICAgIHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLnRhYmxlRGF0YVxyXG4gICAgICAgICAgICA/IHRoaXMub25Mb2FkLnRhYmxlRGF0YVxyXG4gICAgICAgICAgICA6IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXS50YWJsZURhdGE7XHJcbiAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleCh0YWJsZUFycmF5LCB7IHRhYmxlSWQ6IHRoaXMudGFibGVJZCB9KTtcclxuICAgIGxldCBzZWFyY2hSZXF1ZXN0ID0gdGFibGVBcnJheVtpbmRleF0ub25UYWJsZVNlYXJjaDtcclxuICAgIGxldCByZXF1ZXN0ID0gc2VhcmNoUmVxdWVzdC5yZXF1ZXN0RGF0YTtcclxuICAgIGxldCBpc1Vpc2VyYWNoID0gKHNlYXJjaFJlcXVlc3QudWlTZWFyY2gpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgaWYgKGlzVWlzZXJhY2gpIHtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyID0gZXZlbnQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlRGF0YSA9IHRhYmxlQXJyYXlbaW5kZXhdO1xyXG4gICAgICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgICAgICAgICBsaW1pdDogMTAsXHJcbiAgICAgICAgICAgIGRhdGFzb3VyY2U6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2VcclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiPj4+PiBzZWFyY2hSZXF1ZXN0IFwiLCBzZWFyY2hSZXF1ZXN0KTtcclxuICAgICAgICBpZiAoc2VhcmNoUmVxdWVzdCAmJiBzZWFyY2hSZXF1ZXN0LmlzU2luZ2xlcmVxdWVzdCkge1xyXG4gICAgICAgICAgICBxdWVyeVtzZWFyY2hSZXF1ZXN0LnNpbmdsZVJlcXVlc3RLZXldID0gZXZlbnQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2Uge1xyXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0LCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZV0pXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnlbaXRlbS52YWx1ZV0gPSBzZWxmLmlucHV0RGF0YVtpdGVtLm5hbWVdOyAvLyBvblRhYmxlIFNlYXJjaCBWYWx1ZSBwYXNzZWQgaW5zdGVhZCBvZiBuYW1lIFxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5xdWVyeVBhcmFtcyA9IHF1ZXJ5O1xyXG4gICAgICAgIHRoaXMuZ2V0RGF0YSh0aGlzLmN1cnJlbnRUYWJsZURhdGEpO1xyXG4gICAgfVxyXG59XHJcbiAgZmlsdGVyUmVzZXQoZmllbGQpIHtcclxuICAgIGRlbGV0ZSB0aGlzLnF1ZXJ5UGFyYW1zW2ZpZWxkLnJlcXVlc3RLZXldO1xyXG4gICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gIH1cclxuICBvbk11bHRpU2VsZWN0KGV2ZW50LCByb3dWYWwpIHtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBsZXQgc2VsZWN0ZWREYXRhID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhO1xyXG4gICAgbGV0IGluZGV4ID0gXy5maW5kSW5kZXgoc2VsZWN0ZWREYXRhLCB7IG9ial9uYW1lOiByb3dWYWwub2JqX25hbWUgfSk7XHJcbiAgICBpZiAoaW5kZXggPiAtMSAmJiBzZWxlY3RlZERhdGEgJiYgc2VsZWN0ZWREYXRhLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFbaW5kZXhdLnNlY3VyaXR5X3R5cGUgPSBldmVudC52YWx1ZTtcclxuICAgICAgdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhW2luZGV4XS5sZXZlbCA9IGV2ZW50LnZhbHVlO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFbaW5kZXhdLm9iamVjdExldmVsID0gZXZlbnQudmFsdWU7XHJcbiAgICB9XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gIH1cclxuXHJcbiAgY2hhbmdlUGFnZShldmVudCkge1xyXG4gICAgaWYgKGV2ZW50LnBhZ2VTaXplICE9IHRoaXMubGltaXQpIHtcclxuICAgICAgdGhpcy5xdWVyeVBhcmFtc1tcImxpbWl0XCJdID0gZXZlbnQucGFnZVNpemU7XHJcbiAgICAgIHRoaXMucXVlcnlQYXJhbXNbXCJvZmZzZXRcIl0gPSAwO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5xdWVyeVBhcmFtc1tcIm9mZnNldFwiXSA9IGV2ZW50LnBhZ2VJbmRleCAqIHRoaXMubGltaXQ7XHJcbiAgICAgIHRoaXMucXVlcnlQYXJhbXNbXCJsaW1pdFwiXSA9IHRoaXMubGltaXQ7XHJcbiAgICB9XHJcbiAgICB0aGlzLm9uTG9hZERhdGEoXCJuZXh0XCIpO1xyXG4gIH1cclxuICB2aWV3RGF0YShlbGVtZW50LCBjb2wpIHtcclxuICAgIGNvbnNvbGUubG9nKGNvbCwgXCJ2aWV3RGF0YSA+Pj4+Pj4+Pj4gaXNSZWRpcmVjdCBcIiwgY29sLmlzUmVkaXJlY3QsIHRoaXMuZW52aXJvbm1lbnQpO1xyXG4gICAgaWYgKGNvbC5pc1JlZGlyZWN0KSB7XHJcbiAgICAgIGxldCB0ZXN0U3RyID0gXCIvY29udHJvbC1tYW5hZ2VtZW50L2FjY2Vzcy1jb250cm9sXCJcclxuICAgICAgd2luZG93LmxvY2F0aW9uLmhyZWYgPSBgJHt0aGlzLnJlZGlyZWN0VXJpfWAgKyB0ZXN0U3RyO1xyXG5cclxuICAgICAgY29uc29sZS5sb2coXCJSZWRpcmVjdGVkICoqKioqKioqKioqKioqKioqKioqXCIpXHJcbiAgICAgIC8vIHRoaXMucm91dGUubmF2aWdhdGVCeVVybChcIi9jb250cm9sLW1hbmFnZW1lbnQvYWNjZXNzLWNvbnRyb2xcIik7XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5hY3Rpb25DbGljayhlbGVtZW50LCBcInZpZXdcIik7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGFkZEJ1dHRvbkNsaWNrKGVsZW1lbnQsIGFjdGlvbikge1xyXG4gICAgY29uc29sZS5sb2coXCJhZGRCdXR0b25DbGljayBcIik7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBlbGVtZW50IFwiLCBlbGVtZW50KTtcclxuICAgIGNvbnNvbGUubG9nKFwiIGFjdGlvbiBcIiwgYWN0aW9uKTtcclxuICB9XHJcbiAgYWN0aW9uQ2xpY2soZWxlbWVudCwgYWN0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlbGVtZW50LCBcIj4+Pj5lbGVtZW50XCIpXHJcbiAgICBpZiAoYWN0aW9uID09IFwiZWRpdFwiIHx8IGFjdGlvbiA9PSBcInZpZXdcIiB8fCBhY3Rpb24gPT1cImluZm9cIikge1xyXG4gICAgICB0aGlzLm9wZW5EaWFsb2coZWxlbWVudCwgYWN0aW9uKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwiZGVsZXRlXCIpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4+Pj4+dGhpc1wiKTtcclxuICAgICAgaWYgKHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLmlucHV0S2V5KSB7XHJcbiAgICAgICAgbGV0IGlucHV0S2V5ID0gdGhpcy5vbkxvYWQuaW5wdXRLZXk7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5wdXREYXRhICYmIHRoaXMuaW5wdXREYXRhW2lucHV0S2V5XSkge1xyXG4gICAgICAgICAgbGV0IHNlbGVjdGVkRGF0YSA9IHRoaXMuaW5wdXREYXRhW2lucHV0S2V5XTtcclxuICAgICAgICAgIC8vIGxldCBpbmRleCA9IHNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAvLyAgIG9iaiA9PiBvYmogPT0gZWxlbWVudFxyXG4gICAgICAgICAgLy8gKTtcclxuICAgICAgICAgIGxldCBpbmRleCA9IF8uZmluZEluZGV4KHNlbGVjdGVkRGF0YSxlbGVtZW50KTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+PiBkZWxldGUgaW5kZXggXCIsaW5kZXgpO1xyXG4gICAgICAgICAgaWYoaW5kZXg+LTEpe1xyXG4gICAgICAgICAgICBzZWxlY3RlZERhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW2lucHV0S2V5XSA9IHNlbGVjdGVkRGF0YTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcChzZWxlY3RlZERhdGEsIFwiX2lkXCIpO1xyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShzZWxlY3RlZERhdGEpO1xyXG4gICAgICAgICAgdGhpcy5sZW5ndGggPSBzZWxlY3RlZERhdGEubGVuZ3RoO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgIHRoaXMuYWN0aW9uQ2xpY2tFdmVudC5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7ICAgIFxyXG4gICAgICAgIGlmICh0aGlzLmlucHV0RGF0YSAmJiB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEpIHtcclxuICAgICAgICAgIGxldCBvYmplY3RLZXk9dGhpcy5jdXJyZW50VGFibGVEYXRhLm9iamVjdEtleVRvQ2hlY2s/dGhpcy5jdXJyZW50VGFibGVEYXRhLm9iamVjdEtleVRvQ2hlY2s6XCJvYmpfbmFtZVwiO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4+IG9iamVjdEtleSBcIixvYmplY3RLZXkpO1xyXG4gICAgICAgICAgbGV0IHNlbGVjdGVkRGF0YSA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YTtcclxuICAgICAgICAgIC8vIGxldCBpbmRleCA9IHNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAvLyAgIG9iaiA9PiBvYmoub2JqZWN0X25hbWUgPT0gZWxlbWVudC5vYmplY3RfbmFtZVxyXG4gICAgICAgICAgLy8gKTtcclxuICAgICAgICAgIGxldCBpbmRleCA9IHNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIG9iaiA9PiBvYmpbb2JqZWN0S2V5XSA9PSBlbGVtZW50W29iamVjdEtleV1cclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBzZWxlY3RlZERhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHNlbGVjdGVkRGF0YSwgXCJfaWRcIik7XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHNlbGVjdGVkRGF0YSk7XHJcbiAgICAgICAgICB0aGlzLmxlbmd0aCA9IHNlbGVjdGVkRGF0YS5sZW5ndGg7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgdGhpcy5hY3Rpb25DbGlja0V2ZW50LmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gXCJyZW1vdmVfcmVkX2V5ZVwiKSB7XHJcbiAgICAgIHRoaXMub3BlbkRpYWxvZyhlbGVtZW50LCBcInZpZXdcIik7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcInJlc3RvcmVfcGFnZVwiKSB7XHJcbiAgICAgIHRoaXMub3BlbkRpYWxvZyhlbGVtZW50LCBhY3Rpb24pO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gJ2Fza2RlZmF1bHRidXR0b24nKSB7XHJcbiAgICAgIGxldCBkc2lkID0gZWxlbWVudC5faWQ7XHJcbiAgICAgIGxldCBkc25hbWUgPSBlbGVtZW50Lm5hbWU7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiIGRzaWQgXCIsIGRzaWQsIFwiIGRzbmFtZSBcIiwgZHNuYW1lKTtcclxuICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kRGF0YXNvdXJjZShkc2lkKTtcclxuICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgIFwiRGF0YXNvdXJjZSBEZWZhdWx0IFVwZGF0ZWQgU3VjY2Vzc2Z1bGx5IVwiXHJcbiAgICAgICAgKVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gXCJzYXZlX2FsdFwiKSB7XHJcbiAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEuZG93bmxvYWRSZXF1ZXN0O1xyXG4gICAgICBsZXQgcXVlcnkgPSB7XHJcbiAgICAgICAgcmVwb3J0SWQ6IGVsZW1lbnQuX2lkXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgIGNvbnN0IGFiID0gbmV3IEFycmF5QnVmZmVyKGRhdGEuZGF0YS5sZW5ndGgpO1xyXG4gICAgICAgICAgY29uc3QgdmlldyA9IG5ldyBVaW50OEFycmF5KGFiKTtcclxuICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGF0YS5kYXRhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHZpZXdbaV0gPSBkYXRhLmRhdGFbaV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBsZXQgZG93bmxvYWRUeXBlID0gKGVsZW1lbnQuZmlsZVR5cGUgPT0gXCJDU1ZcIikgPyBcInRleHQvY3N2XCIgOiBcInRleHQveGxzeFwiO1xyXG4gICAgICAgICAgY29uc3QgZmlsZSA9IG5ldyBCbG9iKFthYl0sIHsgdHlwZTogZG93bmxvYWRUeXBlIH0pO1xyXG4gICAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhmaWxlLCBlbGVtZW50LmZpbGVOYW1lICsgZWxlbWVudC5maWxlRXh0ZW5zaW9uKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbiAgYWN0aW9uUmVkaXJlY3QoZWxlbWVudCxjb2x1bW5EYXRhKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImFjdGlvblJlZGlyZWN0ID4+Pj4+PiBcIiwgZWxlbWVudCk7XHJcbiAgICBsZXQgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGFcclxuICAgIGxldCBxdWVyeU9iaiA9IHt9O1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgY29uc29sZS5sb2coXCJyZXF1ZXN0RGV0YWlscyA+Pj4+Pj4gXCIsIHJlcXVlc3REZXRhaWxzKTtcclxuICAgIGlmIChjb2x1bW5EYXRhLnJ1bGVzZXRDaGVjaykge1xyXG4gICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLm9uVGFibGVVcGRhdGUudG9hc3RNZXNzYWdlO1xyXG4gICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBxdWVyeU9ialtpdGVtLm5hbWVdID0gaXRlbS5zdWJrZXlcclxuICAgICAgICAgID8gZWxlbWVudFtpdGVtLnZhbHVlXVtpdGVtLnN1YmtleV1cclxuICAgICAgICAgIDogZWxlbWVudFtpdGVtLnZhbHVlXTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLnVwZGF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLm9uVGFibGVVcGRhdGUuYXBpVXJsLCBlbGVtZW50Ll9pZClcclxuICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIGNvbnNvbGUubG9nKCdhY3Rpb25SZWRpcmVjdCA+Pj4+Pj4+Pj4+Pj4+Pj4gcXVlcnlPYmogJywgcXVlcnlPYmopXHJcbiAgICB9IGVsc2Uge1xyXG5cclxuICAgIH1cclxuXHJcbiAgfVxyXG4gIG9uVG9nZ2xlQ2hhbmdlKGV2ZW50LCBlbGVtZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+PiBldmVudCBcIiwgZXZlbnQsIFwiIDplbGVtZW50IFwiLCBlbGVtZW50KTtcclxuICAgIGVsZW1lbnQuc3RhdHVzID0gZXZlbnQuY2hlY2tlZCA/IFwiQUNUSVZFXCIgOiBcIklOQUNUSVZFXCI7XHJcbiAgICBsZXQgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGFcclxuICAgICAgPyB0aGlzLmN1cnJlbnRUYWJsZURhdGEub25Ub2dnbGVDaGFuZ2VcclxuICAgICAgOiBcIlwiO1xyXG4gICAgaWYgKHJlcXVlc3REZXRhaWxzKSB7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCBxdWVyeU9iaiA9IHt9O1xyXG4gICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgcXVlcnlPYmpbaXRlbS5uYW1lXSA9IGl0ZW0uc3Via2V5XHJcbiAgICAgICAgICA/IGVsZW1lbnRbaXRlbS52YWx1ZV1baXRlbS5zdWJrZXldXHJcbiAgICAgICAgICA6IGVsZW1lbnRbaXRlbS52YWx1ZV07XHJcbiAgICAgIH0pO1xyXG4gICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLnVwZGF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLmFwaVVybCwgZWxlbWVudC5faWQpXHJcbiAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgICBpZihlbGVtZW50LnN0YXR1cz09J0FDVElWRScgJiYgIHRvYXN0TWVzc2FnZURldGFpbHMuZW5hYmxlIClcclxuICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZW5hYmxlXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgIH1lbHNlIGlmKGVsZW1lbnQuc3RhdHVzPT0nSU5BQ1RJVkUnICYmICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmRpc2FibGUpXHJcbiAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmRpc2FibGVcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICB9XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnNhdmVzZWxlY3RlZFRvZ2dsZSkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVt0aGlzLmN1cnJlbnRUYWJsZURhdGFbXCJrZXlUb3NhdmVcIl1dID0gdGhpcy5kYXRhU291cmNlLmRhdGE7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9wZW5EaWFsb2coZWxlbWVudCwgYWN0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBPcGVuIERpYWxvZyAqKioqKioqKiogZWxlbWVudDogXCIsIGVsZW1lbnQpO1xyXG4gICAgbGV0IG1vZGVsV2lkdGggPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW2FjdGlvbl0ubW9kZWxEYXRhLnNpemU7XHJcbiAgICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZ1xyXG4gICAgICAub3BlbihNb2RlbExheW91dENvbXBvbmVudCwge1xyXG4gICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICB3aWR0aDogbW9kZWxXaWR0aCxcclxuICAgICAgICBwYW5lbENsYXNzOiBcImNvbnRhY3QtZm9ybS1kaWFsb2dcIixcclxuICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICBhY3Rpb246IGFjdGlvbixcclxuICAgICAgICAgIHNhdmVkRGF0YTogZWxlbWVudFxyXG4gICAgICAgIH1cclxuICAgICAgfSlcclxuICAgICAgLmFmdGVyQ2xvc2VkKClcclxuICAgICAgLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxufVxyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGJhc2U2NFRvQXJyYXlCdWZmZXIoYmFzZTY0OiBzdHJpbmcpIHtcclxuICBjb25zdCBiaW5hcnlTdHJpbmcgPSB3aW5kb3cuYXRvYihiYXNlNjQpOyAvLyBDb21tZW50IHRoaXMgaWYgbm90IHVzaW5nIGJhc2U2NFxyXG4gIGNvbnN0IGJ5dGVzID0gbmV3IFVpbnQ4QXJyYXkoYmluYXJ5U3RyaW5nLmxlbmd0aCk7XHJcbiAgcmV0dXJuIGJ5dGVzLm1hcCgoYnl0ZSwgaSkgPT4gYmluYXJ5U3RyaW5nLmNoYXJDb2RlQXQoaSkpO1xyXG59XHJcblxyXG4iXX0=