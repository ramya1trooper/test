import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { TableLayoutComponent } from "../table-layout/table-layout.component";
import { MaterialModule } from "../material.module";
import { StylePaginatorDirective } from './style-paginator.directive';
var TableLayoutModule = /** @class */ (function () {
    function TableLayoutModule() {
    }
    TableLayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [TableLayoutComponent, StylePaginatorDirective],
                    imports: [
                        RouterModule,
                        // Material
                        MaterialModule,
                        TranslateModule,
                        FuseSharedModule
                    ],
                    exports: [TableLayoutComponent]
                },] }
    ];
    return TableLayoutModule;
}());
export { TableLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtbGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ0YWJsZS1sYXlvdXQvdGFibGUtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBQyx1QkFBdUIsRUFBQyxNQUFNLDZCQUE2QixDQUFDO0FBRXBFO0lBQUE7SUFjZ0MsQ0FBQzs7Z0JBZGhDLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxvQkFBb0IsRUFBRSx1QkFBdUIsQ0FBQztvQkFDN0QsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBRVosV0FBVzt3QkFDWCxjQUFjO3dCQUVkLGVBQWU7d0JBRWYsZ0JBQWdCO3FCQUNqQjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxvQkFBb0IsQ0FBQztpQkFDaEM7O0lBQytCLHdCQUFDO0NBQUEsQUFkakMsSUFjaUM7U0FBcEIsaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuXHJcbmltcG9ydCB7IFRhYmxlTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL3RhYmxlLWxheW91dC90YWJsZS1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5pbXBvcnQge1N0eWxlUGFnaW5hdG9yRGlyZWN0aXZlfSBmcm9tICcuL3N0eWxlLXBhZ2luYXRvci5kaXJlY3RpdmUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtUYWJsZUxheW91dENvbXBvbmVudCwgU3R5bGVQYWdpbmF0b3JEaXJlY3RpdmVdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuXHJcbiAgICAvLyBNYXRlcmlhbFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcblxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG5cclxuICAgIEZ1c2VTaGFyZWRNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtUYWJsZUxheW91dENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYmxlTGF5b3V0TW9kdWxlIHt9XHJcbiJdfQ==