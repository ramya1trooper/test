import { Injectable } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import * as i0 from "@angular/core";
import * as i1 from "ngx-ui-loader";
var LoaderService = /** @class */ (function () {
    function LoaderService(ngxService) {
        this.ngxService = ngxService;
    }
    LoaderService.prototype.startLoader = function () {
        this.ngxService.start();
    };
    LoaderService.prototype.stopLoader = function () {
        this.ngxService.stop();
    };
    LoaderService.decorators = [
        { type: Injectable, args: [{ providedIn: 'root' },] }
    ];
    /** @nocollapse */
    LoaderService.ctorParameters = function () { return [
        { type: NgxUiLoaderService }
    ]; };
    LoaderService.ngInjectableDef = i0.defineInjectable({ factory: function LoaderService_Factory() { return new LoaderService(i0.inject(i1.NgxUiLoaderService)); }, token: LoaderService, providedIn: "root" });
    return LoaderService;
}());
export { LoaderService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9hZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibG9hZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxlQUFlLENBQUM7OztBQUVuRDtJQUdJLHVCQUFvQixVQUE4QjtRQUE5QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtJQUdsRCxDQUFDO0lBQ0QsbUNBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNELGtDQUFVLEdBQVY7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNCLENBQUM7O2dCQVpKLFVBQVUsU0FDVixFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUU7Ozs7Z0JBSGQsa0JBQWtCOzs7d0JBRDNCO0NBZ0JDLEFBYkQsSUFhQztTQVhZLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5neFVpTG9hZGVyU2VydmljZSB9IGZyb20gJ25neC11aS1sb2FkZXInO1xyXG5cclxuQEluamVjdGFibGVcclxuKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBMb2FkZXJTZXJ2aWNle1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBuZ3hTZXJ2aWNlOiBOZ3hVaUxvYWRlclNlcnZpY2UpXHJcbiAgICB7XHJcbiAgICAgXHJcbiAgICB9XHJcbiAgICBzdGFydExvYWRlcigpe1xyXG4gICAgICAgIHRoaXMubmd4U2VydmljZS5zdGFydCgpO1xyXG4gICAgfVxyXG4gICAgc3RvcExvYWRlcigpe1xyXG4gICAgICAgIHRoaXMubmd4U2VydmljZS5zdG9wKCk7XHJcbiAgICB9XHJcbn0iXX0=