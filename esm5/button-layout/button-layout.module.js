import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
// import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { ButtonLayoutComponent } from "./button-layout.component";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
var ButtonLayoutModule = /** @class */ (function () {
    function ButtonLayoutModule() {
    }
    ButtonLayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ButtonLayoutComponent],
                    imports: [RouterModule, MaterialModule, TranslateModule, FuseSharedModule],
                    exports: [ButtonLayoutComponent],
                    schemas: [CUSTOM_ELEMENTS_SCHEMA],
                    entryComponents: [ModelLayoutComponent]
                },] }
    ];
    return ButtonLayoutModule;
}());
export { ButtonLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLWxheW91dC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYnV0dG9uLWxheW91dC9idXR0b24tbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELGlGQUFpRjtBQUNqRixPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUU5RTtJQUFBO0lBT2lDLENBQUM7O2dCQVBqQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMscUJBQXFCLENBQUM7b0JBQ3JDLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxjQUFjLEVBQUUsZUFBZSxFQUFFLGdCQUFnQixDQUFDO29CQUMxRSxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztvQkFDaEMsT0FBTyxFQUFFLENBQUMsc0JBQXNCLENBQUM7b0JBQ2pDLGVBQWUsRUFBRSxDQUFDLG9CQUFvQixDQUFDO2lCQUN4Qzs7SUFDZ0MseUJBQUM7Q0FBQSxBQVBsQyxJQU9rQztTQUFyQixrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VTaGFyZWRNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2Uvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gXCIuLi9tYXRlcmlhbC5tb2R1bGVcIjtcclxuXHJcbi8vIGltcG9ydCB7IE1vZGVsTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGVsLWxheW91dC9tb2RlbC1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IEJ1dHRvbkxheW91dENvbXBvbmVudCB9IGZyb20gXCIuL2J1dHRvbi1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE1vZGVsTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGVsLWxheW91dC9tb2RlbC1sYXlvdXQuY29tcG9uZW50XCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0J1dHRvbkxheW91dENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZSwgTWF0ZXJpYWxNb2R1bGUsIFRyYW5zbGF0ZU1vZHVsZSwgRnVzZVNoYXJlZE1vZHVsZV0sXHJcbiAgZXhwb3J0czogW0J1dHRvbkxheW91dENvbXBvbmVudF0sXHJcbiAgc2NoZW1hczogW0NVU1RPTV9FTEVNRU5UU19TQ0hFTUFdLFxyXG4gIGVudHJ5Q29tcG9uZW50czogW01vZGVsTGF5b3V0Q29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQnV0dG9uTGF5b3V0TW9kdWxlIHt9XHJcbiJdfQ==