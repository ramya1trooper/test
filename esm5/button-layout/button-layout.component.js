import { SnackBarService } from "./../shared/snackbar.service";
import { Component, Input, Inject } from "@angular/core";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import * as FileSaver from "file-saver";
import { ContentService } from "../content/content.service";
import * as _ from "lodash";
import { MessageService } from "../_services/message.service";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
var ButtonLayoutComponent = /** @class */ (function () {
    function ButtonLayoutComponent(_fuseTranslationLoaderService, _matDialog, httpClient, contentService, snackBarService, messageService, environment, english) {
        var _this = this;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this._matDialog = _matDialog;
        this.httpClient = httpClient;
        this.contentService = contentService;
        this.snackBarService = snackBarService;
        this.messageService = messageService;
        this.environment = environment;
        this.english = english;
        this.enableHeader = true;
        this.enableButtonLayout = false;
        this.buttonsConfigData = {};
        this.unsubscribe = new Subject();
        this.tableHeaderConfig = [];
        this.selectedTableHeader = [];
        this._fuseTranslationLoaderService.loadTranslations(english);
        this.messageService
            .getButtonEnableMessage()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(function (message) {
            _this.reinitializeButtons();
        });
    }
    ButtonLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.httpClient
            .get("assets/configFiles/buttons_configFile.json")
            .subscribe(function (fileResponse) {
            localStorage.setItem("buttonConfig", JSON.stringify(fileResponse));
            _this.buttonsConfigData = fileResponse;
        });
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.defaultDatasource = localStorage.getItem("datasource");
        this.userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        this.onLoad();
    };
    ButtonLayoutComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
    };
    ButtonLayoutComponent.prototype.reinitializeButtons = function () {
        // is alow datasouce check
        var userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        var tempObj = localStorage.getItem("currentInput");
        tempObj = tempObj ? JSON.parse(tempObj) : {};
        _.forEach(this.currentData.enabledButtons, function (item) {
            if (item.keyToCheck) {
                item.disable =
                    tempObj && tempObj[item.keyToCheck] && tempObj[item.keyToCheck].length
                        ? false
                        : true;
            }
            if (item.checkimportAccess) {
                var currentRealm = localStorage.getItem("realm");
                var matchedRealm = _.filter(userData.realm, { _id: currentRealm });
                console.log(">>> matchedrealm", matchedRealm);
                //  console.log(">>> current Realm access ",matchedRealm[0]['allow_datasource']);
                //  item.disable =(matchedRealm.length>0 && matchedRealm[0]['allow_datasource'] == "YES") ? false : true;
                item.disable = userData && userData.allow_datasource == "YES" ? false : true;
            }
        });
    };
    ButtonLayoutComponent.prototype.onLoad = function () {
        this.currentData = this.fromRouting ? this.currentConfigData['pageRoutingView'] : this.currentConfigData["listView"];
        if (this.currentData &&
            this.currentData.enableTableSettings &&
            this.currentData.tableData &&
            this.currentData.tableData[0]) {
            this.tableHeaderConfig = this.currentData.tableData[0].tableHeader;
            var tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
            this.selectedTableHeader = _.map(tempArray, "value");
            _.remove(this.tableHeaderConfig, { value: "select" });
            _.remove(this.tableHeaderConfig, { value: "action" });
        }
        else if (this.currentData &&
            this.currentData.enableButtonToggle &&
            this.currentData.formData &&
            this.currentData.formData[0] &&
            this.currentData.formData[0].tableData) {
            this.tableHeaderConfig = this.currentData.formData[0].tableData[0].tableHeader;
            var tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
            this.selectedTableHeader = _.map(tempArray, "value");
        }
        else if (this.currentData &&
            this.currentData.enableTableSettings &&
            this.currentData.enableFormLayout) {
            this.tableHeaderConfig = this.currentData.formData[0].tableData[0].tableHeader;
            var tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
            this.selectedTableHeader = _.map(tempArray, "value");
        }
        // if(this.currentData && this.currentData.enableTableSettings){
        //   this.tableHeaderConfig = this.currentData.tableData[0].tableHeader;
        //   let tempArray = _.filter(this.tableHeaderConfig, {isActive : true})
        //   this.selectedTableHeader = _.map(tempArray, 'value');
        // }
        console.log(this.currentData, ".......CURRENT");
        this.formValues = this.currentData.formData
            ? this.currentData.formData[0]
            : {};
        console.log(this.formValues, ".....formValues");
        this.headerData = {
            pageHeader: this.currentData.pageHeader,
            pageTagLine: this.currentData.pageTagLine,
            pageHeaderIcon: this.currentData.pageHeaderIcon,
        };
        this.enabledButtons = this.currentData.enabledButtons;
        this.enableButtonLayout =
            this.enabledButtons && this.enabledButtons.length ? true : false;
        if (this.currentData.enableButtonToggle) {
            this.enableButtonLayout = true;
            this.defaultToggleValue = this.currentData.buttonToggles[0];
            this.toggleChange(this.defaultToggleValue);
        }
        this.reinitializeButtons();
    };
    ButtonLayoutComponent.prototype.backClick = function () {
        console.log("BACK");
        localStorage.removeItem("currentInput");
        var obj = {
            id: this.currentData.backNavId,
            url: this.currentData.backNavUrl,
            configFilePath: this.currentData.backNavConfigFilePath
        };
        this.messageService.sendRouting(obj);
    };
    ButtonLayoutComponent.prototype.updateTableSettings = function (event) {
        var selectedHeader = this.selectedTableHeader;
        this.tableHeaderConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isActive = true;
            }
            else {
                item.isActive = false;
            }
        });
        localStorage.removeItem("selectedTableHeaders");
        localStorage.setItem("selectedTableHeaders", JSON.stringify(this.tableHeaderConfig));
        this.messageService.sendTableHeaderUpdate("update");
    };
    ButtonLayoutComponent.prototype.enableFilter = function () {
        this.messageService.sendTableHeaderUpdate("filter");
    };
    ButtonLayoutComponent.prototype.onClick = function (action) {
        var _this = this;
        console.log(">> onClick Export action ", action);
        if (action === "exportData") {
            this.exportData(action);
        }
        else if (action === "refreshPage") {
            this.reloadData(action);
        }
        else if (action == "runAnalyze") {
            console.log(this.currentConfigData[action]);
            var runData = this.currentConfigData[action].runRequest;
            var requestDetails = runData.requestData;
            var apiUrl = runData.apiUrl;
            var toastMessageDetails_1 = runData.toastMessage;
            var currentInputData_1 = JSON.parse(localStorage.getItem("currentInput"));
            var requestData_1 = {};
            _.forEach(requestDetails, function (item) {
                if (item.isDefault) {
                    requestData_1[item.name] = item.value;
                }
                else {
                    requestData_1[item.name] = item.convertToString
                        ? JSON.stringify(currentInputData_1[item.value])
                        : currentInputData_1[item.value];
                }
            });
            console.log(requestData_1, "....requestData");
            this.contentService
                .getExportResponse(requestData_1, apiUrl)
                .subscribe(function (data) {
                _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails_1.success));
                _this.messageService.sendModelCloseEvent("listView");
            });
        }
        else if (action == "exportView") {
            console.log(this, "............THIS");
            var modelData = {};
            if (this.defaultToggleValue) {
                var toggleId = this.defaultToggleValue.toggleId;
                console.log(toggleId, ">>>>>toggleId>>>");
                var modelList = this.currentConfigData[action].modelData;
                console.log(modelList, ".....modelList>>>modelList");
                modelData = _.find(modelList, { modelId: toggleId });
            }
            else {
                modelData = this.currentConfigData[action].modelData;
            }
            var modelWidth = modelData["size"];
            this.dialogRef = this._matDialog
                .open(ModelLayoutComponent, {
                disableClose: true,
                width: modelWidth,
                panelClass: "contact-form-dialog",
                data: {
                    action: "exportView",
                    modelData: modelData,
                },
            })
                .afterClosed()
                .subscribe(function (response) {
                localStorage.removeItem("currentInput");
            });
        }
        else if (action == "edit") {
            console.log(this, "............THIS");
            var modelData = {};
            if (this.defaultToggleValue) {
                var toggleId = this.defaultToggleValue.toggleId;
                console.log(toggleId, ">>>>>toggleId>>>");
                var modelList = this.currentConfigData[action].modelData;
                console.log(modelList, ".....modelList>>>modelList");
                modelData = _.find(modelList, { modelId: toggleId });
            }
            else {
                modelData = this.currentConfigData[action].modelData;
            }
            var modelWidth = modelData["size"];
            this.dialogRef = this._matDialog
                .open(ModelLayoutComponent, {
                disableClose: true,
                width: modelWidth,
                panelClass: "contact-form-dialog",
                data: {
                    action: "update",
                    modelData: modelData,
                },
            })
                .afterClosed()
                .subscribe(function (response) {
                localStorage.removeItem("currentInput");
            });
        }
        else {
            this.messageService.sendModelCloseEvent(action);
            this.openDialog(action);
        }
    };
    ButtonLayoutComponent.prototype.exportData = function (action) {
        var _this = this;
        console.log(">>>>>>>> ExportData ", action);
        var currentInputData = JSON.parse(localStorage.getItem("currentInput"));
        var exportData = this.currentConfigData[action];
        currentInputData["exportType"] = exportData.exportType;
        var requestDetails = exportData.exportRequest.requestData;
        var apiUrl = exportData.exportRequest.apiUrl;
        var toastMessageDetails = exportData.exportRequest.toastMessage;
        var requestData = {};
        var self = this;
        var reportTemp;
        console.log(">>>>>>>>>> currentInputData ", currentInputData);
        _.forEach(requestDetails, function (item) {
            // requestData[item.name] = item.convertToString
            //   ? JSON.stringify(currentInputData[item.value])
            //   : currentInputData[item.value];
            var tempData;
            if (item.convertToString) {
                requestData[item.name] = JSON.stringify(currentInputData[item.value]);
            }
            else if (item.isDefault) {
                requestData[item.name] = item.value;
            }
            else if (item.fromLoginData) {
                requestData[item.name] = self.userData ? self.userData[item.value] : "";
            }
            else {
                requestData[item.name] = currentInputData[item.value];
            }
        });
        if (exportData.exportRequest.isReportCreate) {
            // button based report create
            console.log(">>>>>>>>> requestDetails ", requestData);
            var toastMessageDetails_2 = exportData.exportRequest.toastMessage;
            self.contentService.createRequest(requestData, apiUrl).subscribe(function (res) {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.success));
                if (res.status == 200) {
                    localStorage.removeItem("CurrentReportData");
                    self.messageService.sendModelCloseEvent("listView");
                }
            }, function (error) {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_2.error));
            });
        }
        else {
            this.contentService
                .getExportResponse(requestData, apiUrl)
                .subscribe(function (data) {
                var fileName = _this._fuseTranslationLoaderService.instant(exportData.exportRequest.downloadFileName);
                _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                _this.reloadData(action);
                _this.downloadFile(data, fileName + exportData.exportRequest.downloadFiletype);
            });
        }
    };
    ButtonLayoutComponent.prototype.reloadData = function (action) {
        localStorage.removeItem("currentInput");
        this.messageService.sendModelCloseEvent(action);
        console.log(action, "....action");
        this.reinitializeButtons();
    };
    ButtonLayoutComponent.prototype.toggleChange = function (event) {
        console.log(event);
        var valueToSave = event && event.value ? event.value : event;
        localStorage.setItem("selectedToggleDetail", JSON.stringify(valueToSave));
        // this.currentTableLoad = this.formValues;
        // this.currentTableLoad["response"] = "";
        // this.currentTableLoad["total"] = ""
        // this.enableTableLayout = true
        this.messageService.sendMessage("toggle");
    };
    ButtonLayoutComponent.prototype.openDialog = function (action) {
        var _this = this;
        var modelWidth = this.currentConfigData[action]
            ? this.currentConfigData[action].modelData.size
            : "80%";
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                title: this.headerText,
                modalIndex: this.modalIndex,
            },
        })
            .afterClosed()
            .subscribe(function (response) {
            localStorage.removeItem("currentInput");
            _this.messageService.sendModelCloseEvent("listView");
        });
    };
    ButtonLayoutComponent.prototype.downloadFile = function (reportValue, fileName) {
        var blob = new Blob([reportValue], { type: "text/csv" });
        FileSaver.saveAs(blob, fileName);
    };
    ButtonLayoutComponent.prototype.getEnabledbuttons = function (buttonsList) {
        var temp = {};
        this.enableButtonLayout = buttonsList && buttonsList.length ? true : false;
        this.enabledButtons = buttonsList;
    };
    ButtonLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "button-layout",
                    template: "<div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n  fxlayoutalign=\"space-between center\">\r\n  <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"width:40%;\">\r\n    <h2 class=\"m-0 senlib-top-header\">\r\n      <span class=\"material-icons\" *ngIf=\"fromRouting\" (click)=\"backClick()\"\r\n        style=\"margin-right: 7px;font-size: 25px;position: relative;top:2px; color: #59588b;cursor: pointer;\">arrow_back</span>\r\n      <span class=\"material-icons\"\r\n        style=\"margin-right: 7px;font-size: 21px;position: relative;top:2px\">{{headerData.pageHeaderIcon}}</span>\r\n      {{headerData.pageHeader | translate}}\r\n    </h2>\r\n    <span class=\"senlib-top-italic\"> {{headerData.pageTagLine | translate}} </span>\r\n  </div>\r\n  <!-- <div *ngIf=\"fromRouting\" class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"width:40%;\">\r\n    <button class=\"btn btn-primary\">BACK</button>\r\n  </div> -->\r\n  <!-- <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\"> -->\r\n\r\n  <!-- </div> -->\r\n  <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\" *ngIf=\"enableButtonLayout && buttonsConfigData\">\r\n    <!-- new button -->\r\n    <button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilter()\">\r\n      <span class=\"k-icon k-i-filter\"\r\n        style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n    </button>\r\n    <button *ngIf=\"currentData && currentData.enableTableSettings\" class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n      (click)=\"select.open()\">\r\n      <span class=\"material-icons\"\r\n        style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n      <!-- <mat-icon style=\"font-size: 26px;\">settings</mat-icon> -->\r\n      <!-- <mat-form-field style=\"margin: 0px 10px 0 0;width:0\"> -->\r\n      <!-- <mat-label>Table Settings</mat-label> -->\r\n      <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n        (selectionChange)=\"updateTableSettings($event)\">\r\n        <mat-option *ngFor=\"let columnSetting of tableHeaderConfig\" [checked]=\"columnSetting.isActive\"\r\n          [value]=\"columnSetting.value\">{{ columnSetting.name | translate}}</mat-option>\r\n      </mat-select>\r\n      <!-- </mat-form-field> -->\r\n    </button>\r\n    <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\" *ngFor=\"let buttonData of enabledButtons\"\r\n      [disabled]=\"buttonData.disable\" (click)=\"onClick(buttonsConfigData[buttonData.buttonId].action)\"\r\n      [ngClass]=\"(buttonsConfigData[buttonData.buttonId] && buttonsConfigData[buttonData.buttonId].style )? buttonsConfigData[buttonData.buttonId].style : ''\">\r\n      <span class=\"material-icons\" *ngIf=\"buttonsConfigData[buttonData.buttonId]\"\r\n        style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n        {{buttonsConfigData[buttonData.buttonId].icon}}</span>\r\n      <span>\r\n        {{buttonsConfigData[buttonData.buttonId] ? (buttonsConfigData[buttonData.buttonId].label | translate) : \"\" }}</span>\r\n    </button>\r\n\r\n    <!-- end  -->\r\n    <!-- SELECT COMPONENT IN HEADER FUNCTIONALITY NOT IMPLEMENTED -->\r\n    <ng-container *ngIf=\"currentData.selectFields && currentData.selectFields.length>0\">\r\n      <ng-container *ngFor=\"let selectField of currentData.selectFields\">\r\n        <mat-label *ngIf=\"selectField.showSideLabel\" class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n        <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;width: 100px;\" appearance=\"outline\">\r\n          <mat-label>{{selectField.label |translate}}</mat-label>\r\n          <mat-select [formControlName]=\"selectField.name\" [required]=\"selectField.validations\">\r\n            <mat-option *ngFor=\"let item of selectField.data\" value=\"{{item[selectField.keyToShow] | translate}}\">\r\n              {{item[selectField.keyToShow] | translate}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n      </ng-container>\r\n    </ng-container>\r\n    <!-- <button mat-icon-button class=\"mat-warn\" (click)=\"enableFilter()\">\r\n      <span class=\"k-icon k-i-filter\" style=\"font-size: 26px;\"></span>\r\n    </button>\r\n    <button *ngIf=\"currentData && currentData.enableTableSettings\" style=\"width:75px\" mat-icon-button class=\"mat-warn\"\r\n      matTooltip=\"Table Settings\" (click)=\"select.open()\">\r\n      <mat-icon style=\"font-size: 26px;\">settings</mat-icon>\r\n      <mat-form-field style=\"margin: 0px 10px 0 0;width:0\">\r\n        <mat-label>Table Settings</mat-label>\r\n        <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n          (selectionChange)=\"updateTableSettings($event)\">\r\n          <mat-option *ngFor=\"let columnSetting of tableHeaderConfig\" [checked]=\"columnSetting.isActive\"\r\n            [value]=\"columnSetting.value\">{{ columnSetting.name | translate}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </button>\r\n    <button *ngFor=\"let buttonData of enabledButtons\" (click)=\"onClick(buttonsConfigData[buttonData.buttonId].action)\"\r\n      [ngClass]=\"(buttonsConfigData[buttonData.buttonId] && buttonsConfigData[buttonData.buttonId].style )? buttonsConfigData[buttonData.buttonId].style : ''\"\r\n      mat-raised-button class=\"mat-raised-button mr-4\" [disabled]=\"buttonData.disable\">\r\n      <mat-icon class=\"icon-size\" *ngIf=\"buttonsConfigData[buttonData.buttonId]\">\r\n        {{buttonsConfigData[buttonData.buttonId].icon}}</mat-icon>\r\n      {{buttonsConfigData[buttonData.buttonId] ? (buttonsConfigData[buttonData.buttonId].label | translate) : \"\" }}\r\n    </button>-->\r\n    <mat-button-toggle-group class=\"sentri-group-toggle\" [(value)]=\"defaultToggleValue\"\r\n      *ngIf=\"currentData && currentData.enableButtonToggle\">\r\n      <mat-button-toggle class=\"ui-common-lib-btn-toggle\" *ngFor=\"let toggleItem of currentData.buttonToggles;\"\r\n        (change)=\"toggleChange($event)\" [value]=\"toggleItem\">{{toggleItem.labelName}}</mat-button-toggle>\r\n    </mat-button-toggle-group>\r\n  </div>\r\n  <div fxlayout=\"row\" fxlayoutalign=\"center stretch\" style=\"margin: -19px;\" *ngIf=\"enableModelButtonLayout\">\r\n    <button class=\"modal-view-buttons\" mat-raised-button type=\"button\" color=\"accent\" (click)=\"openDialog('new')\">\r\n      <mat-icon>play_arrow</mat-icon>{{'BUTTONS.RUN_BTN_LBL' | translate}}\r\n    </button>\r\n    <button class=\"modal-view-buttons\" mat-raised-button type=\"button\" color=\"warn\" (click)=\"openDialog('new')\">\r\n      <mat-icon>schedule</mat-icon>{{'BUTTONS.SCHEDULE_BTN_LBL' | translate}}\r\n    </button>\r\n  </div>\r\n</div>\r\n",
                    styles: [".ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{color:#223664!important;border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.sentri-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}"]
                }] }
    ];
    /** @nocollapse */
    ButtonLayoutComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: MatDialog },
        { type: HttpClient },
        { type: ContentService },
        { type: SnackBarService },
        { type: MessageService },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    ButtonLayoutComponent.propDecorators = {
        enableModelButtonLayout: [{ type: Input }],
        headerText: [{ type: Input }],
        modalIndex: [{ type: Input }],
        enableHeader: [{ type: Input }],
        fromRouting: [{ type: Input }]
    };
    return ButtonLayoutComponent;
}());
export { ButtonLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLWxheW91dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYnV0dG9uLWxheW91dC9idXR0b24tbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLFNBQVMsRUFBYSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXBFLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLGtEQUFrRDtBQUVsRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUU5RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFbEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBRXhDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUU1RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN2QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFM0M7SUEyQkUsK0JBQ1UsNkJBQTJELEVBQzNELFVBQXFCLEVBQ3JCLFVBQXNCLEVBQ3RCLGNBQThCLEVBQy9CLGVBQWdDLEVBRS9CLGNBQThCLEVBQ1AsV0FBVyxFQUNmLE9BQU87UUFUcEMsaUJBa0JDO1FBakJTLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBOEI7UUFDM0QsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQUNyQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUMvQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFFL0IsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQ1AsZ0JBQVcsR0FBWCxXQUFXLENBQUE7UUFDZixZQUFPLEdBQVAsT0FBTyxDQUFBO1FBM0IzQixpQkFBWSxHQUFZLElBQUksQ0FBQztRQUd0Qyx1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFHcEMsc0JBQWlCLEdBQVEsRUFBRSxDQUFDO1FBQ3BCLGdCQUFXLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUsxQyxzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFDNUIsd0JBQW1CLEdBQVEsRUFBRSxDQUFDO1FBZ0I1QixJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLGNBQWM7YUFDaEIsc0JBQXNCLEVBQUU7YUFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDakMsU0FBUyxDQUFDLFVBQUMsT0FBTztZQUNqQixLQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUM3QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQUEsaUJBYUM7UUFaQyxJQUFJLENBQUMsVUFBVTthQUNaLEdBQUcsQ0FBQyw0Q0FBNEMsQ0FBQzthQUNqRCxTQUFTLENBQUMsVUFBQyxZQUFZO1lBQ3RCLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNuRSxLQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDO1FBQ3hDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztRQUNGLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUNELDJDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxtREFBbUIsR0FBbkI7UUFDRSwwQkFBMEI7UUFDMUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUNwRSxJQUFJLE9BQU8sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ25ELE9BQU8sR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM3QyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxFQUFFLFVBQVUsSUFBSTtZQUN2RCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ25CLElBQUksQ0FBQyxPQUFPO29CQUNWLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsTUFBTTt3QkFDcEUsQ0FBQyxDQUFDLEtBQUs7d0JBQ1AsQ0FBQyxDQUFDLElBQUksQ0FBQzthQUNaO1lBQ0QsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ3pCLElBQUksWUFBWSxHQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQy9DLElBQUksWUFBWSxHQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBQyxFQUFDLEdBQUcsRUFBQyxZQUFZLEVBQUMsQ0FBQyxDQUFDO2dCQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvQyxpRkFBaUY7Z0JBQ2pGLHlHQUF5RztnQkFDdkcsSUFBSSxDQUFDLE9BQU8sR0FBRSxRQUFRLElBQUksUUFBUSxDQUFDLGdCQUFnQixJQUFJLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7YUFDOUU7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxzQ0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3BILElBQ0UsSUFBSSxDQUFDLFdBQVc7WUFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTO1lBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUM3QjtZQUNBLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7WUFDbkUsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7WUFDckQsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztZQUN0RCxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3ZEO2FBQU0sSUFDTCxJQUFJLENBQUMsV0FBVztZQUNoQixJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQjtZQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7WUFDekIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFDdEM7WUFDQSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUMvRSxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUN0RDthQUFNLElBQ0wsSUFBSSxDQUFDLFdBQVc7WUFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxtQkFBbUI7WUFDcEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFDakM7WUFDQSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUMvRSxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUN0RDtRQUNELGdFQUFnRTtRQUNoRSx3RUFBd0U7UUFDeEUsd0VBQXdFO1FBQ3hFLDBEQUEwRDtRQUMxRCxJQUFJO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVE7WUFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFVBQVUsR0FBRztZQUNoQixVQUFVLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVO1lBQ3ZDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVc7WUFDekMsY0FBYyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYztTQUNoRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQztRQUN0RCxJQUFJLENBQUMsa0JBQWtCO1lBQ3JCLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ25FLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRTtZQUN2QyxJQUFJLENBQUMsa0JBQWtCLEdBQUUsSUFBSSxDQUFBO1lBQzdCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUM1RCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQzVDO1FBQ0QsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQUNELHlDQUFTLEdBQVQ7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3BCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDeEMsSUFBSSxHQUFHLEdBQUc7WUFDUixFQUFFLEVBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTO1lBQy9CLEdBQUcsRUFBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVU7WUFDakMsY0FBYyxFQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCO1NBQ3hELENBQUE7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBQ0QsbURBQW1CLEdBQW5CLFVBQW9CLEtBQUs7UUFDdkIsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQzlDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJO1lBQzNDLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsWUFBWSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ2hELFlBQVksQ0FBQyxPQUFPLENBQ2xCLHNCQUFzQixFQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUN2QyxDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsNENBQVksR0FBWjtRQUNFLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUNELHVDQUFPLEdBQVAsVUFBUSxNQUFNO1FBQWQsaUJBNkZDO1FBNUZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUMsTUFBTSxDQUFDLENBQUE7UUFDL0MsSUFBSSxNQUFNLEtBQUssWUFBWSxFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDekI7YUFBTSxJQUFJLE1BQU0sS0FBSyxhQUFhLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6QjthQUFNLElBQUksTUFBTSxJQUFJLFlBQVksRUFBRTtZQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzVDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxVQUFVLENBQUM7WUFDeEQsSUFBSSxjQUFjLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztZQUN6QyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQzVCLElBQUkscUJBQW1CLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUMvQyxJQUFJLGtCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksYUFBVyxHQUFHLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7Z0JBQ3RDLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDbEIsYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNyQztxQkFBTTtvQkFDTCxhQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlO3dCQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzlDLENBQUMsQ0FBQyxrQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2xDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQVcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxjQUFjO2lCQUNoQixpQkFBaUIsQ0FBQyxhQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN0QyxTQUFTLENBQUMsVUFBQyxJQUFJO2dCQUNkLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixLQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztnQkFDRixLQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RELENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTSxJQUFJLE1BQU0sSUFBSSxZQUFZLEVBQUU7WUFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztZQUN0QyxJQUFJLFNBQVMsR0FBQyxFQUFFLENBQUM7WUFDakIsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7Z0JBQzNCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUM7Z0JBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLENBQUM7Z0JBQzFDLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLDRCQUE0QixDQUFDLENBQUM7Z0JBQ3BELFNBQVMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO2FBQ3ZEO2lCQUFJO2dCQUNGLFNBQVMsR0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDO2FBQ3JEO1lBQ0QsSUFBSSxVQUFVLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVU7aUJBQzdCLElBQUksQ0FBQyxvQkFBb0IsRUFBRTtnQkFDMUIsWUFBWSxFQUFFLElBQUk7Z0JBQ2xCLEtBQUssRUFBRSxVQUFVO2dCQUNqQixVQUFVLEVBQUUscUJBQXFCO2dCQUNqQyxJQUFJLEVBQUU7b0JBQ0osTUFBTSxFQUFFLFlBQVk7b0JBQ3BCLFNBQVMsRUFBRSxTQUFTO2lCQUNyQjthQUNGLENBQUM7aUJBQ0QsV0FBVyxFQUFFO2lCQUNiLFNBQVMsQ0FBQyxVQUFDLFFBQVE7Z0JBQ2xCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7U0FFTjthQUFLLElBQUcsTUFBTSxJQUFJLE1BQU0sRUFBQztZQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3RDLElBQUksU0FBUyxHQUFDLEVBQUUsQ0FBQztZQUNqQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDM0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQztnQkFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQztnQkFDekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztnQkFDcEQsU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7YUFDdkQ7aUJBQUk7Z0JBQ0YsU0FBUyxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUM7YUFDckQ7WUFDRCxJQUFJLFVBQVUsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTtpQkFDN0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUMxQixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsS0FBSyxFQUFFLFVBQVU7Z0JBQ2pCLFVBQVUsRUFBRSxxQkFBcUI7Z0JBQ2pDLElBQUksRUFBRTtvQkFDSixNQUFNLEVBQUUsUUFBUTtvQkFDaEIsU0FBUyxFQUFFLFNBQVM7aUJBQ3JCO2FBQ0YsQ0FBQztpQkFDRCxXQUFXLEVBQUU7aUJBQ2IsU0FBUyxDQUFDLFVBQUMsUUFBUTtnQkFDbEIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUMxQyxDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDekI7SUFDSCxDQUFDO0lBRUQsMENBQVUsR0FBVixVQUFXLE1BQU07UUFBakIsaUJBc0VDO1FBckVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDNUMsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUN4RSxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEQsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLGNBQWMsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUMxRCxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztRQUM3QyxJQUFJLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1FBQ2hFLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUNyQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxVQUFVLENBQUM7UUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDOUQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxJQUFJO1lBQ3RDLGdEQUFnRDtZQUNoRCxtREFBbUQ7WUFDbkQsb0NBQW9DO1lBQ3BDLElBQUksUUFBUSxDQUFDO1lBQ2IsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO2dCQUN4QixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7YUFDdkU7aUJBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUN6QixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7YUFDckM7aUJBQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO2dCQUM3QixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDekU7aUJBQU07Z0JBQ0wsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkQ7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksVUFBVSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUU7WUFDM0MsNkJBQTZCO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDdEQsSUFBSSxxQkFBbUIsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztZQUNoRSxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUM5RCxVQUFDLEdBQUc7Z0JBQ0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7b0JBQ3JCLFlBQVksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFDN0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDckQ7WUFDSCxDQUFDLEVBQ0QsVUFBQyxLQUFLO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FDRixDQUFDO1NBQ0g7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjO2lCQUNoQixpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN0QyxTQUFTLENBQUMsVUFBQyxJQUFJO2dCQUNkLElBQUksUUFBUSxHQUFHLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3ZELFVBQVUsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQzFDLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hCLEtBQUksQ0FBQyxZQUFZLENBQ2YsSUFBSSxFQUNKLFFBQVEsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUNyRCxDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFFRCwwQ0FBVSxHQUFWLFVBQVcsTUFBTTtRQUNmLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsNENBQVksR0FBWixVQUFhLEtBQUs7UUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixJQUFJLFdBQVcsR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzdELFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzFFLDJDQUEyQztRQUMzQywwQ0FBMEM7UUFDMUMsc0NBQXNDO1FBQ3RDLGdDQUFnQztRQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBQ0QsMENBQVUsR0FBVixVQUFXLE1BQU07UUFBakIsaUJBb0JDO1FBbkJDLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUM7WUFDN0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSTtZQUMvQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ1YsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTthQUM3QixJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDMUIsWUFBWSxFQUFFLElBQUk7WUFDbEIsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLHFCQUFxQjtZQUNqQyxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLE1BQU07Z0JBQ2QsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVO2dCQUN0QixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7YUFDNUI7U0FDRixDQUFDO2FBQ0QsV0FBVyxFQUFFO2FBQ2IsU0FBUyxDQUFDLFVBQUMsUUFBUTtZQUNsQixZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3hDLEtBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0QsNENBQVksR0FBWixVQUFhLFdBQVcsRUFBRSxRQUFRO1FBQ2hDLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsV0FBVyxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztRQUMzRCxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsaURBQWlCLEdBQWpCLFVBQWtCLFdBQVc7UUFDM0IsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1FBQ2QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMzRSxJQUFJLENBQUMsY0FBYyxHQUFHLFdBQVcsQ0FBQztJQUNwQyxDQUFDOztnQkFwWUYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxlQUFlO29CQUN6Qix3Mk5BQTZDOztpQkFFOUM7Ozs7Z0JBckJRLDRCQUE0QjtnQkFPNUIsU0FBUztnQkFGVCxVQUFVO2dCQUtWLGNBQWM7Z0JBYmQsZUFBZTtnQkFnQmYsY0FBYztnREF1Q2xCLE1BQU0sU0FBQyxhQUFhO2dEQUNwQixNQUFNLFNBQUMsU0FBUzs7OzBDQTlCbEIsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7K0JBQ0wsS0FBSzs4QkFDTCxLQUFLOztJQTJYUiw0QkFBQztDQUFBLEFBcllELElBcVlDO1NBaFlZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9zbmFja2Jhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgVmlld0NoaWxkLCBJbnB1dCwgSW5qZWN0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UgfSBmcm9tIFwiLi4vQGZ1c2Uvc2VydmljZXMvdHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2VcIjtcclxuLy8gaW1wb3J0IHsgbG9jYWxlIGFzIGVuZ2xpc2ggfSBmcm9tIFwiLi4vaTE4bi9lblwiO1xyXG5cclxuaW1wb3J0IHsgTW9kZWxMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kZWwtbGF5b3V0L21vZGVsLWxheW91dC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgVGFibGVMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi4vdGFibGUtbGF5b3V0L3RhYmxlLWxheW91dC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5cclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsXCI7XHJcbmltcG9ydCAqIGFzIEZpbGVTYXZlciBmcm9tIFwiZmlsZS1zYXZlclwiO1xyXG5cclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tIFwiLi4vY29udGVudC9jb250ZW50LnNlcnZpY2VcIjtcclxuXHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLi9fc2VydmljZXMvbWVzc2FnZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqcy9TdWJqZWN0XCI7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiYnV0dG9uLWxheW91dFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vYnV0dG9uLWxheW91dC5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9idXR0b24tbGF5b3V0LmNvbXBvbmVudC5zY3NzXCJdLFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQnV0dG9uTGF5b3V0Q29tcG9uZW50IHtcclxuICBASW5wdXQoKSBlbmFibGVNb2RlbEJ1dHRvbkxheW91dDogYW55O1xyXG4gIEBJbnB1dCgpIGhlYWRlclRleHQ6IHN0cmluZztcclxuICBASW5wdXQoKSBtb2RhbEluZGV4OiBhbnk7XHJcbiAgQElucHV0KCkgZW5hYmxlSGVhZGVyOiBib29sZWFuID0gdHJ1ZTtcclxuICBASW5wdXQoKSBmcm9tUm91dGluZyA6IGFueTtcclxuICBoZWFkZXJEYXRhOiBhbnk7XHJcbiAgZW5hYmxlQnV0dG9uTGF5b3V0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgZW5hYmxlZEJ1dHRvbnM6IGFueTtcclxuICBkaWFsb2dSZWY6IGFueTtcclxuICBidXR0b25zQ29uZmlnRGF0YTogYW55ID0ge307XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgZGVmYXVsdERhdGFzb3VyY2U6IGFueTtcclxuICBjdXJyZW50RGF0YTogYW55O1xyXG4gIGN1cnJlbnRDb25maWdEYXRhOiBhbnk7XHJcbiAgZGVmYXVsdFRvZ2dsZVZhbHVlOiBhbnk7XHJcbiAgdGFibGVIZWFkZXJDb25maWc6IGFueSA9IFtdO1xyXG4gIHNlbGVjdGVkVGFibGVIZWFkZXI6IGFueSA9IFtdO1xyXG4gIC8vIGVuYWJsZVRhYmxlTGF5b3V0IDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGZvcm1WYWx1ZXM6IGFueTtcclxuICBjdXJyZW50VGFibGVMb2FkOiBhbnk7XHJcbiAgdXNlckRhdGE6IGFueTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9tYXREaWFsb2c6IE1hdERpYWxvZyxcclxuICAgIHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCxcclxuICAgIHByaXZhdGUgY29udGVudFNlcnZpY2U6IENvbnRlbnRTZXJ2aWNlLFxyXG4gICAgcHVibGljIHNuYWNrQmFyU2VydmljZTogU25hY2tCYXJTZXJ2aWNlLFxyXG5cclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgQEluamVjdChcImVudmlyb25tZW50XCIpIHByaXZhdGUgZW52aXJvbm1lbnQsXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2hcclxuICApIHtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2VcclxuICAgICAgLmdldEJ1dHRvbkVuYWJsZU1lc3NhZ2UoKVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZSkpXHJcbiAgICAgIC5zdWJzY3JpYmUoKG1lc3NhZ2UpID0+IHtcclxuICAgICAgICB0aGlzLnJlaW5pdGlhbGl6ZUJ1dHRvbnMoKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuaHR0cENsaWVudFxyXG4gICAgICAuZ2V0KFwiYXNzZXRzL2NvbmZpZ0ZpbGVzL2J1dHRvbnNfY29uZmlnRmlsZS5qc29uXCIpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGZpbGVSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiYnV0dG9uQ29uZmlnXCIsIEpTT04uc3RyaW5naWZ5KGZpbGVSZXNwb25zZSkpO1xyXG4gICAgICAgIHRoaXMuYnV0dG9uc0NvbmZpZ0RhdGEgPSBmaWxlUmVzcG9uc2U7XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICk7XHJcbiAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJkYXRhc291cmNlXCIpO1xyXG4gICAgdGhpcy51c2VyRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50TG9naW5Vc2VyXCIpKTtcclxuICAgIHRoaXMub25Mb2FkKCk7XHJcbiAgfVxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy51bnN1YnNjcmliZS5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICByZWluaXRpYWxpemVCdXR0b25zKCkge1xyXG4gICAgLy8gaXMgYWxvdyBkYXRhc291Y2UgY2hlY2tcclxuICAgIGxldCB1c2VyRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50TG9naW5Vc2VyXCIpKTtcclxuICAgIGxldCB0ZW1wT2JqID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0ZW1wT2JqID0gdGVtcE9iaiA/IEpTT04ucGFyc2UodGVtcE9iaikgOiB7fTtcclxuICAgIF8uZm9yRWFjaCh0aGlzLmN1cnJlbnREYXRhLmVuYWJsZWRCdXR0b25zLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBpZiAoaXRlbS5rZXlUb0NoZWNrKSB7XHJcbiAgICAgICAgaXRlbS5kaXNhYmxlID1cclxuICAgICAgICAgIHRlbXBPYmogJiYgdGVtcE9ialtpdGVtLmtleVRvQ2hlY2tdICYmIHRlbXBPYmpbaXRlbS5rZXlUb0NoZWNrXS5sZW5ndGhcclxuICAgICAgICAgICAgPyBmYWxzZVxyXG4gICAgICAgICAgICA6IHRydWU7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGl0ZW0uY2hlY2tpbXBvcnRBY2Nlc3MpIHtcclxuICAgICAgICAgbGV0IGN1cnJlbnRSZWFsbT1sb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInJlYWxtXCIpO1xyXG4gICAgICAgICBsZXQgbWF0Y2hlZFJlYWxtPV8uZmlsdGVyKHVzZXJEYXRhLnJlYWxtLHtfaWQ6Y3VycmVudFJlYWxtfSk7XHJcbiAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+IG1hdGNoZWRyZWFsbVwiLG1hdGNoZWRSZWFsbSk7XHJcbiAgICAgICAvLyAgY29uc29sZS5sb2coXCI+Pj4gY3VycmVudCBSZWFsbSBhY2Nlc3MgXCIsbWF0Y2hlZFJlYWxtWzBdWydhbGxvd19kYXRhc291cmNlJ10pO1xyXG4gICAgICAgLy8gIGl0ZW0uZGlzYWJsZSA9KG1hdGNoZWRSZWFsbS5sZW5ndGg+MCAmJiBtYXRjaGVkUmVhbG1bMF1bJ2FsbG93X2RhdGFzb3VyY2UnXSA9PSBcIllFU1wiKSA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgICAgaXRlbS5kaXNhYmxlID11c2VyRGF0YSAmJiB1c2VyRGF0YS5hbGxvd19kYXRhc291cmNlID09IFwiWUVTXCIgPyBmYWxzZSA6IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBvbkxvYWQoKSB7XHJcbiAgICB0aGlzLmN1cnJlbnREYXRhID0gdGhpcy5mcm9tUm91dGluZyA/IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbJ3BhZ2VSb3V0aW5nVmlldyddIDp0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl07XHJcbiAgICBpZiAoXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEgJiZcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YS5lbmFibGVUYWJsZVNldHRpbmdzICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEudGFibGVEYXRhICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEudGFibGVEYXRhWzBdXHJcbiAgICApIHtcclxuICAgICAgdGhpcy50YWJsZUhlYWRlckNvbmZpZyA9IHRoaXMuY3VycmVudERhdGEudGFibGVEYXRhWzBdLnRhYmxlSGVhZGVyO1xyXG4gICAgICBsZXQgdGVtcEFycmF5ID0gXy5maWx0ZXIodGhpcy50YWJsZUhlYWRlckNvbmZpZywgeyBpc0FjdGl2ZTogdHJ1ZSB9KTtcclxuICAgICAgdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyID0gXy5tYXAodGVtcEFycmF5LCBcInZhbHVlXCIpO1xyXG4gICAgICBfLnJlbW92ZSh0aGlzLnRhYmxlSGVhZGVyQ29uZmlnLCB7IHZhbHVlOiBcInNlbGVjdFwiIH0pO1xyXG4gICAgICBfLnJlbW92ZSh0aGlzLnRhYmxlSGVhZGVyQ29uZmlnLCB7IHZhbHVlOiBcImFjdGlvblwiIH0pO1xyXG4gICAgfSBlbHNlIGlmIChcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSAmJlxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZUJ1dHRvblRvZ2dsZSAmJlxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhLmZvcm1EYXRhICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEuZm9ybURhdGFbMF0gJiZcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YS5mb3JtRGF0YVswXS50YWJsZURhdGFcclxuICAgICkge1xyXG4gICAgICB0aGlzLnRhYmxlSGVhZGVyQ29uZmlnID0gdGhpcy5jdXJyZW50RGF0YS5mb3JtRGF0YVswXS50YWJsZURhdGFbMF0udGFibGVIZWFkZXI7XHJcbiAgICAgIGxldCB0ZW1wQXJyYXkgPSBfLmZpbHRlcih0aGlzLnRhYmxlSGVhZGVyQ29uZmlnLCB7IGlzQWN0aXZlOiB0cnVlIH0pO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIgPSBfLm1hcCh0ZW1wQXJyYXksIFwidmFsdWVcIik7XHJcbiAgICB9IGVsc2UgaWYgKFxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEuZW5hYmxlVGFibGVTZXR0aW5ncyAmJlxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZUZvcm1MYXlvdXRcclxuICAgICkge1xyXG4gICAgICB0aGlzLnRhYmxlSGVhZGVyQ29uZmlnID0gdGhpcy5jdXJyZW50RGF0YS5mb3JtRGF0YVswXS50YWJsZURhdGFbMF0udGFibGVIZWFkZXI7XHJcbiAgICAgIGxldCB0ZW1wQXJyYXkgPSBfLmZpbHRlcih0aGlzLnRhYmxlSGVhZGVyQ29uZmlnLCB7IGlzQWN0aXZlOiB0cnVlIH0pO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIgPSBfLm1hcCh0ZW1wQXJyYXksIFwidmFsdWVcIik7XHJcbiAgICB9XHJcbiAgICAvLyBpZih0aGlzLmN1cnJlbnREYXRhICYmIHRoaXMuY3VycmVudERhdGEuZW5hYmxlVGFibGVTZXR0aW5ncyl7XHJcbiAgICAvLyAgIHRoaXMudGFibGVIZWFkZXJDb25maWcgPSB0aGlzLmN1cnJlbnREYXRhLnRhYmxlRGF0YVswXS50YWJsZUhlYWRlcjtcclxuICAgIC8vICAgbGV0IHRlbXBBcnJheSA9IF8uZmlsdGVyKHRoaXMudGFibGVIZWFkZXJDb25maWcsIHtpc0FjdGl2ZSA6IHRydWV9KVxyXG4gICAgLy8gICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIgPSBfLm1hcCh0ZW1wQXJyYXksICd2YWx1ZScpO1xyXG4gICAgLy8gfVxyXG4gICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50RGF0YSwgXCIuLi4uLi4uQ1VSUkVOVFwiKTtcclxuICAgIHRoaXMuZm9ybVZhbHVlcyA9IHRoaXMuY3VycmVudERhdGEuZm9ybURhdGFcclxuICAgICAgPyB0aGlzLmN1cnJlbnREYXRhLmZvcm1EYXRhWzBdXHJcbiAgICAgIDoge307XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmZvcm1WYWx1ZXMsIFwiLi4uLi5mb3JtVmFsdWVzXCIpO1xyXG4gICAgdGhpcy5oZWFkZXJEYXRhID0ge1xyXG4gICAgICBwYWdlSGVhZGVyOiB0aGlzLmN1cnJlbnREYXRhLnBhZ2VIZWFkZXIsXHJcbiAgICAgIHBhZ2VUYWdMaW5lOiB0aGlzLmN1cnJlbnREYXRhLnBhZ2VUYWdMaW5lLFxyXG4gICAgICBwYWdlSGVhZGVySWNvbjogdGhpcy5jdXJyZW50RGF0YS5wYWdlSGVhZGVySWNvbixcclxuICAgIH07XHJcbiAgICB0aGlzLmVuYWJsZWRCdXR0b25zID0gdGhpcy5jdXJyZW50RGF0YS5lbmFibGVkQnV0dG9ucztcclxuICAgIHRoaXMuZW5hYmxlQnV0dG9uTGF5b3V0ID1cclxuICAgICAgdGhpcy5lbmFibGVkQnV0dG9ucyAmJiB0aGlzLmVuYWJsZWRCdXR0b25zLmxlbmd0aCA/IHRydWUgOiBmYWxzZTtcclxuICAgIGlmICh0aGlzLmN1cnJlbnREYXRhLmVuYWJsZUJ1dHRvblRvZ2dsZSkge1xyXG4gICAgICB0aGlzLmVuYWJsZUJ1dHRvbkxheW91dCA9dHJ1ZVxyXG4gICAgICB0aGlzLmRlZmF1bHRUb2dnbGVWYWx1ZSA9IHRoaXMuY3VycmVudERhdGEuYnV0dG9uVG9nZ2xlc1swXTtcclxuICAgICAgdGhpcy50b2dnbGVDaGFuZ2UodGhpcy5kZWZhdWx0VG9nZ2xlVmFsdWUpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5yZWluaXRpYWxpemVCdXR0b25zKCk7IFxyXG4gIH1cclxuICBiYWNrQ2xpY2soKXtcclxuICAgIGNvbnNvbGUubG9nKFwiQkFDS1wiKTtcclxuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgbGV0IG9iaiA9IHtcclxuICAgICAgaWQgOiB0aGlzLmN1cnJlbnREYXRhLmJhY2tOYXZJZCxcclxuICAgICAgdXJsIDogdGhpcy5jdXJyZW50RGF0YS5iYWNrTmF2VXJsLFxyXG4gICAgICBjb25maWdGaWxlUGF0aCA6IHRoaXMuY3VycmVudERhdGEuYmFja05hdkNvbmZpZ0ZpbGVQYXRoXHJcbiAgICB9XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKG9iaik7XHJcbiAgfVxyXG4gIHVwZGF0ZVRhYmxlU2V0dGluZ3MoZXZlbnQpIHtcclxuICAgIGxldCBzZWxlY3RlZEhlYWRlciA9IHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlcjtcclxuICAgIHRoaXMudGFibGVIZWFkZXJDb25maWcuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBpZiAoc2VsZWN0ZWRIZWFkZXIuaW5kZXhPZihpdGVtLnZhbHVlKSA+PSAwKSB7XHJcbiAgICAgICAgaXRlbS5pc0FjdGl2ZSA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaXRlbS5pc0FjdGl2ZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwic2VsZWN0ZWRUYWJsZUhlYWRlcnNcIik7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcclxuICAgICAgXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiLFxyXG4gICAgICBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlSGVhZGVyQ29uZmlnKVxyXG4gICAgKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRhYmxlSGVhZGVyVXBkYXRlKFwidXBkYXRlXCIpO1xyXG4gIH1cclxuXHJcbiAgZW5hYmxlRmlsdGVyKCkge1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kVGFibGVIZWFkZXJVcGRhdGUoXCJmaWx0ZXJcIik7XHJcbiAgfVxyXG4gIG9uQ2xpY2soYWN0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+IG9uQ2xpY2sgRXhwb3J0IGFjdGlvbiBcIixhY3Rpb24pXHJcbiAgICBpZiAoYWN0aW9uID09PSBcImV4cG9ydERhdGFcIikge1xyXG4gICAgICB0aGlzLmV4cG9ydERhdGEoYWN0aW9uKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSBcInJlZnJlc2hQYWdlXCIpIHtcclxuICAgICAgdGhpcy5yZWxvYWREYXRhKGFjdGlvbik7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcInJ1bkFuYWx5emVcIikge1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmN1cnJlbnRDb25maWdEYXRhW2FjdGlvbl0pO1xyXG4gICAgICBsZXQgcnVuRGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5ydW5SZXF1ZXN0O1xyXG4gICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSBydW5EYXRhLnJlcXVlc3REYXRhO1xyXG4gICAgICBsZXQgYXBpVXJsID0gcnVuRGF0YS5hcGlVcmw7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcnVuRGF0YS50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIGxldCBjdXJyZW50SW5wdXREYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcbiAgICAgIGxldCByZXF1ZXN0RGF0YSA9IHt9O1xyXG4gICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgaWYgKGl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS52YWx1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkoY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXSlcclxuICAgICAgICAgICAgOiBjdXJyZW50SW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKHJlcXVlc3REYXRhLCBcIi4uLi5yZXF1ZXN0RGF0YVwiKTtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRFeHBvcnRSZXNwb25zZShyZXF1ZXN0RGF0YSwgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcImV4cG9ydFZpZXdcIikge1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLCBcIi4uLi4uLi4uLi4uLlRISVNcIik7XHJcbiAgICAgIGxldCBtb2RlbERhdGE9e307XHJcbiAgICAgIGlmICh0aGlzLmRlZmF1bHRUb2dnbGVWYWx1ZSkge1xyXG4gICAgICAgIGxldCB0b2dnbGVJZCA9IHRoaXMuZGVmYXVsdFRvZ2dsZVZhbHVlLnRvZ2dsZUlkO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRvZ2dsZUlkLCBcIj4+Pj4+dG9nZ2xlSWQ+Pj5cIik7XHJcbiAgICAgICAgbGV0IG1vZGVsTGlzdCA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGE7XHJcbiAgICAgICAgY29uc29sZS5sb2cobW9kZWxMaXN0LCBcIi4uLi4ubW9kZWxMaXN0Pj4+bW9kZWxMaXN0XCIpO1xyXG4gICAgICAgICBtb2RlbERhdGEgPSBfLmZpbmQobW9kZWxMaXN0LCB7IG1vZGVsSWQ6IHRvZ2dsZUlkIH0pO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICAgbW9kZWxEYXRhPXRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGE7XHJcbiAgICAgIH1cclxuICAgICAgbGV0IG1vZGVsV2lkdGggPSBtb2RlbERhdGFbXCJzaXplXCJdO1xyXG4gICAgICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZ1xyXG4gICAgICAgIC5vcGVuKE1vZGVsTGF5b3V0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICB3aWR0aDogbW9kZWxXaWR0aCxcclxuICAgICAgICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICBhY3Rpb246IFwiZXhwb3J0Vmlld1wiLFxyXG4gICAgICAgICAgICBtb2RlbERhdGE6IG1vZGVsRGF0YSxcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgfSlcclxuICAgICAgICAuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKHJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICB9ZWxzZSBpZihhY3Rpb24gPT0gXCJlZGl0XCIpe1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLCBcIi4uLi4uLi4uLi4uLlRISVNcIik7XHJcbiAgICAgIGxldCBtb2RlbERhdGE9e307XHJcbiAgICAgIGlmICh0aGlzLmRlZmF1bHRUb2dnbGVWYWx1ZSkge1xyXG4gICAgICAgIGxldCB0b2dnbGVJZCA9IHRoaXMuZGVmYXVsdFRvZ2dsZVZhbHVlLnRvZ2dsZUlkO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRvZ2dsZUlkLCBcIj4+Pj4+dG9nZ2xlSWQ+Pj5cIik7XHJcbiAgICAgICAgbGV0IG1vZGVsTGlzdCA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGE7XHJcbiAgICAgICAgY29uc29sZS5sb2cobW9kZWxMaXN0LCBcIi4uLi4ubW9kZWxMaXN0Pj4+bW9kZWxMaXN0XCIpO1xyXG4gICAgICAgICBtb2RlbERhdGEgPSBfLmZpbmQobW9kZWxMaXN0LCB7IG1vZGVsSWQ6IHRvZ2dsZUlkIH0pO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICAgbW9kZWxEYXRhPXRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGE7XHJcbiAgICAgIH1cclxuICAgICAgbGV0IG1vZGVsV2lkdGggPSBtb2RlbERhdGFbXCJzaXplXCJdO1xyXG4gICAgICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZ1xyXG4gICAgICAgIC5vcGVuKE1vZGVsTGF5b3V0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgICBkaXNhYmxlQ2xvc2U6IHRydWUsXHJcbiAgICAgICAgICB3aWR0aDogbW9kZWxXaWR0aCxcclxuICAgICAgICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICBhY3Rpb246IFwidXBkYXRlXCIsXHJcbiAgICAgICAgICAgIG1vZGVsRGF0YTogbW9kZWxEYXRhLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5hZnRlckNsb3NlZCgpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KGFjdGlvbik7XHJcbiAgICAgIHRoaXMub3BlbkRpYWxvZyhhY3Rpb24pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZXhwb3J0RGF0YShhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4gRXhwb3J0RGF0YSBcIiwgYWN0aW9uKTtcclxuICAgIGxldCBjdXJyZW50SW5wdXREYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcbiAgICBsZXQgZXhwb3J0RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXTtcclxuICAgIGN1cnJlbnRJbnB1dERhdGFbXCJleHBvcnRUeXBlXCJdID0gZXhwb3J0RGF0YS5leHBvcnRUeXBlO1xyXG4gICAgbGV0IHJlcXVlc3REZXRhaWxzID0gZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LnJlcXVlc3REYXRhO1xyXG4gICAgbGV0IGFwaVVybCA9IGV4cG9ydERhdGEuZXhwb3J0UmVxdWVzdC5hcGlVcmw7XHJcbiAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGV4cG9ydERhdGEuZXhwb3J0UmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICBsZXQgcmVxdWVzdERhdGEgPSB7fTtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGxldCByZXBvcnRUZW1wO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+IGN1cnJlbnRJbnB1dERhdGEgXCIsIGN1cnJlbnRJbnB1dERhdGEpO1xyXG4gICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAvLyByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgLy8gICA/IEpTT04uc3RyaW5naWZ5KGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV0pXHJcbiAgICAgIC8vICAgOiBjdXJyZW50SW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICBsZXQgdGVtcERhdGE7XHJcbiAgICAgIGlmIChpdGVtLmNvbnZlcnRUb1N0cmluZykge1xyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBKU09OLnN0cmluZ2lmeShjdXJyZW50SW5wdXREYXRhW2l0ZW0udmFsdWVdKTtcclxuICAgICAgfSBlbHNlIGlmIChpdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnZhbHVlO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uZnJvbUxvZ2luRGF0YSkge1xyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBzZWxmLnVzZXJEYXRhID8gc2VsZi51c2VyRGF0YVtpdGVtLnZhbHVlXSA6IFwiXCI7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgaWYgKGV4cG9ydERhdGEuZXhwb3J0UmVxdWVzdC5pc1JlcG9ydENyZWF0ZSkge1xyXG4gICAgICAvLyBidXR0b24gYmFzZWQgcmVwb3J0IGNyZWF0ZVxyXG4gICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+PiByZXF1ZXN0RGV0YWlscyBcIiwgcmVxdWVzdERhdGEpO1xyXG4gICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGV4cG9ydERhdGEuZXhwb3J0UmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChyZXF1ZXN0RGF0YSwgYXBpVXJsKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDApIHtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJDdXJyZW50UmVwb3J0RGF0YVwiKTtcclxuICAgICAgICAgICAgc2VsZi5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRFeHBvcnRSZXNwb25zZShyZXF1ZXN0RGF0YSwgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgIGxldCBmaWxlTmFtZSA9IHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LmRvd25sb2FkRmlsZU5hbWVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMucmVsb2FkRGF0YShhY3Rpb24pO1xyXG4gICAgICAgICAgdGhpcy5kb3dubG9hZEZpbGUoXHJcbiAgICAgICAgICAgIGRhdGEsXHJcbiAgICAgICAgICAgIGZpbGVOYW1lICsgZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LmRvd25sb2FkRmlsZXR5cGVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZWxvYWREYXRhKGFjdGlvbikge1xyXG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoYWN0aW9uKTtcclxuICAgIGNvbnNvbGUubG9nKGFjdGlvbiwgXCIuLi4uYWN0aW9uXCIpO1xyXG4gICAgdGhpcy5yZWluaXRpYWxpemVCdXR0b25zKCk7XHJcbiAgfVxyXG5cclxuICB0b2dnbGVDaGFuZ2UoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50KTtcclxuICAgIGxldCB2YWx1ZVRvU2F2ZSA9IGV2ZW50ICYmIGV2ZW50LnZhbHVlID8gZXZlbnQudmFsdWUgOiBldmVudDtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwic2VsZWN0ZWRUb2dnbGVEZXRhaWxcIiwgSlNPTi5zdHJpbmdpZnkodmFsdWVUb1NhdmUpKTtcclxuICAgIC8vIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgIC8vIHRoaXMuY3VycmVudFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gXCJcIjtcclxuICAgIC8vIHRoaXMuY3VycmVudFRhYmxlTG9hZFtcInRvdGFsXCJdID0gXCJcIlxyXG4gICAgLy8gdGhpcy5lbmFibGVUYWJsZUxheW91dCA9IHRydWVcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZE1lc3NhZ2UoXCJ0b2dnbGVcIik7XHJcbiAgfVxyXG4gIG9wZW5EaWFsb2coYWN0aW9uKSB7XHJcbiAgICBsZXQgbW9kZWxXaWR0aCA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXVxyXG4gICAgICA/IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGEuc2l6ZVxyXG4gICAgICA6IFwiODAlXCI7XHJcbiAgICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZ1xyXG4gICAgICAub3BlbihNb2RlbExheW91dENvbXBvbmVudCwge1xyXG4gICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICB3aWR0aDogbW9kZWxXaWR0aCxcclxuICAgICAgICBwYW5lbENsYXNzOiBcImNvbnRhY3QtZm9ybS1kaWFsb2dcIixcclxuICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICBhY3Rpb246IGFjdGlvbixcclxuICAgICAgICAgIHRpdGxlOiB0aGlzLmhlYWRlclRleHQsXHJcbiAgICAgICAgICBtb2RhbEluZGV4OiB0aGlzLm1vZGFsSW5kZXgsXHJcbiAgICAgICAgfSxcclxuICAgICAgfSlcclxuICAgICAgLmFmdGVyQ2xvc2VkKClcclxuICAgICAgLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG4gIGRvd25sb2FkRmlsZShyZXBvcnRWYWx1ZSwgZmlsZU5hbWUpIHtcclxuICAgIGNvbnN0IGJsb2IgPSBuZXcgQmxvYihbcmVwb3J0VmFsdWVdLCB7IHR5cGU6IFwidGV4dC9jc3ZcIiB9KTtcclxuICAgIEZpbGVTYXZlci5zYXZlQXMoYmxvYiwgZmlsZU5hbWUpO1xyXG4gIH1cclxuXHJcbiAgZ2V0RW5hYmxlZGJ1dHRvbnMoYnV0dG9uc0xpc3QpIHtcclxuICAgIGxldCB0ZW1wID0ge307XHJcbiAgICB0aGlzLmVuYWJsZUJ1dHRvbkxheW91dCA9IGJ1dHRvbnNMaXN0ICYmIGJ1dHRvbnNMaXN0Lmxlbmd0aCA/IHRydWUgOiBmYWxzZTtcclxuICAgIHRoaXMuZW5hYmxlZEJ1dHRvbnMgPSBidXR0b25zTGlzdDtcclxuICB9XHJcbn1cclxuIl19