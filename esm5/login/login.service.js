import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/router";
var LoginService = /** @class */ (function () {
    function LoginService(http, router, environment) {
        this.http = http;
        this.router = router;
        this.environment = environment;
        this.baseURL = environment.baseUrl;
    }
    LoginService.prototype.loginAuth = function (obj) {
        var _this = this;
        return this.http
            .post(this.baseURL + "/auth/login", obj, { observe: "response" })
            .pipe(map(function (resp) {
            _this.retriveJwt(resp);
            return resp;
        }));
    };
    LoginService.prototype.retriveJwt = function (resp) {
        var jwt = resp.body.response.access_token;
        this.storeAuthToken(jwt);
        return jwt;
    };
    LoginService.prototype.storeAuthToken = function (jwt) {
        localStorage.setItem("access_token", jwt);
    };
    LoginService.prototype.logout = function () {
        localStorage.clear();
        // return this.http.get(`${this.baseURL}/users/me`);
        this.router.navigate(["/login"]);
    };
    LoginService.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    LoginService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: Router },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
    ]; };
    LoginService.ngInjectableDef = i0.defineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.inject(i1.HttpClient), i0.inject(i2.Router), i0.inject("environment")); }, token: LoginService, providedIn: "root" });
    return LoginService;
}());
export { LoginService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJsb2dpbi9sb2dpbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVsRCxPQUFPLEVBQUUsR0FBRyxFQUFjLE1BQU0sZ0JBQWdCLENBQUM7QUFFakQsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDOzs7O0FBRXpDO0lBTUUsc0JBQ1UsSUFBZ0IsRUFDaEIsTUFBYyxFQUNTLFdBQVc7UUFGbEMsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ1MsZ0JBQVcsR0FBWCxXQUFXLENBQUE7UUFFMUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO0lBQ3JDLENBQUM7SUFFRCxnQ0FBUyxHQUFULFVBQVUsR0FBUTtRQUFsQixpQkFTQztRQVJDLE9BQU8sSUFBSSxDQUFDLElBQUk7YUFDYixJQUFJLENBQVMsSUFBSSxDQUFDLE9BQU8sZ0JBQWEsRUFBRSxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLENBQUM7YUFDckUsSUFBSSxDQUNILEdBQUcsQ0FBQyxVQUFBLElBQUk7WUFDTixLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RCLE9BQU8sSUFBSSxDQUFDO1FBQ2QsQ0FBQyxDQUFDLENBQ0gsQ0FBQztJQUNOLENBQUM7SUFFRCxpQ0FBVSxHQUFWLFVBQVcsSUFBSTtRQUNiLElBQU0sR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztRQUM1QyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3pCLE9BQU8sR0FBRyxDQUFDO0lBQ2IsQ0FBQztJQUVELHFDQUFjLEdBQWQsVUFBZSxHQUFHO1FBQ2hCLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCw2QkFBTSxHQUFOO1FBQ0UsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLG9EQUFvRDtRQUNwRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDbkMsQ0FBQzs7Z0JBdkNGLFVBQVUsU0FBQztvQkFDVixVQUFVLEVBQUUsTUFBTTtpQkFDbkI7Ozs7Z0JBUlEsVUFBVTtnQkFJVixNQUFNO2dEQVdWLE1BQU0sU0FBQyxhQUFhOzs7dUJBaEJ6QjtDQStDQyxBQXhDRCxJQXdDQztTQXJDWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcblxyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogXCJyb290XCJcclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ2luU2VydmljZSB7XHJcbiAgYmFzZVVSTDogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBASW5qZWN0KFwiZW52aXJvbm1lbnRcIikgcHJpdmF0ZSBlbnZpcm9ubWVudFxyXG4gICkge1xyXG4gICAgdGhpcy5iYXNlVVJMID0gZW52aXJvbm1lbnQuYmFzZVVybDtcclxuICB9XHJcblxyXG4gIGxvZ2luQXV0aChvYmo6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwXHJcbiAgICAgIC5wb3N0PGFueT4oYCR7dGhpcy5iYXNlVVJMfS9hdXRoL2xvZ2luYCwgb2JqLCB7IG9ic2VydmU6IFwicmVzcG9uc2VcIiB9KVxyXG4gICAgICAucGlwZShcclxuICAgICAgICBtYXAocmVzcCA9PiB7XHJcbiAgICAgICAgICB0aGlzLnJldHJpdmVKd3QocmVzcCk7XHJcbiAgICAgICAgICByZXR1cm4gcmVzcDtcclxuICAgICAgICB9KVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcbiAgcmV0cml2ZUp3dChyZXNwKSB7XHJcbiAgICBjb25zdCBqd3QgPSByZXNwLmJvZHkucmVzcG9uc2UuYWNjZXNzX3Rva2VuO1xyXG4gICAgdGhpcy5zdG9yZUF1dGhUb2tlbihqd3QpO1xyXG4gICAgcmV0dXJuIGp3dDtcclxuICB9XHJcblxyXG4gIHN0b3JlQXV0aFRva2VuKGp3dCkge1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJhY2Nlc3NfdG9rZW5cIiwgand0KTtcclxuICB9XHJcblxyXG4gIGxvZ291dCgpIHtcclxuICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgLy8gcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7dGhpcy5iYXNlVVJMfS91c2Vycy9tZWApO1xyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiL2xvZ2luXCJdKTtcclxuICB9XHJcbn1cclxuIl19