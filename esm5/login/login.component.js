import { Component, ViewEncapsulation } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { FuseConfigService } from "../@fuse/services/config.service";
import { LoginService } from "./login.service";
import { OAuthService } from "angular-oauth2-oidc";
var LoginComponent = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    function LoginComponent(_fuseConfigService, _formBuilder, loginService, router, oauthService) {
        this._fuseConfigService = _fuseConfigService;
        this._formBuilder = _formBuilder;
        this.loginService = loginService;
        this.router = router;
        this.oauthService = oauthService;
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    LoginComponent.prototype.ngOnInit = function () {
        if (!localStorage.getItem("access_token")) {
            this.oauthService.initImplicitFlow();
        }
        else {
            this.router.navigate([""]);
        }
        this.loginForm = this._formBuilder.group({
            username: ["", Validators.required],
            password: ["", Validators.required]
        });
    };
    Object.defineProperty(LoginComponent.prototype, "f", {
        get: function () {
            return this.loginForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    LoginComponent.prototype.login = function () {
        var _this = this;
        var dataObj = {
            username: this.loginForm.controls["username"].value,
            password: this.loginForm.controls["password"].value
        };
        this.loginService.loginAuth(dataObj).subscribe(function (data) {
            if (_this.router.url === "/login") {
                _this.router.navigate([""]);
            }
        }, function (error) {
            _this.errorStatus = error.error.meta.status;
            _this.errorMsg = error.error.meta.msg;
        });
    };
    LoginComponent.decorators = [
        { type: Component, args: [{
                    selector: "login",
                    template: "<div id=\"login\" fxLayout=\"column\">\r\n\r\n\t<div id=\"login-form-wrapper\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n\r\n\t\t<div id=\"login-form\" [@animate]=\"{value:'*',params:{duration:'300ms',y:'100px'}}\">\r\n\r\n\t\t\t<div class=\"logo\">\r\n\t\t\t\t<img src=\"assets/images/logos/2.png\">\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"title\">LOGIN TO YOUR ACCOUNT</div>\r\n\r\n\t\t\t<form name=\"loginForm\" [formGroup]=\"loginForm\" novalidate>\r\n\r\n\t\t\t\t<mat-form-field appearance=\"outline\">\r\n\t\t\t\t\t<mat-label>Username</mat-label>\r\n\t\t\t\t\t<input matInput formControlName=\"username\" style=\"font-size: 16px;\">\r\n\t\t\t\t\t<mat-icon matSuffix class=\"secondary-text\">person</mat-icon>\r\n\t\t\t\t\t<mat-error>\r\n\t\t\t\t\t\tUsername is required\r\n\t\t\t\t\t</mat-error>\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t<mat-form-field appearance=\"outline\">\r\n\t\t\t\t\t<mat-label>Password</mat-label>\r\n\t\t\t\t\t<input matInput type=\"password\" formControlName=\"password\" style=\"font-size: 16px;\">\r\n\t\t\t\t\t<mat-icon matSuffix class=\"secondary-text\">vpn_key</mat-icon>\r\n\t\t\t\t\t<mat-error>\r\n\t\t\t\t\t\tPassword is requiredaa\r\n\t\t\t\t\t</mat-error>\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t<!-- <div class=\"remember-forgot-password\" fxLayout=\"row\" fxLayout.xs=\"column\" fxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t<mat-checkbox class=\"remember-me\" aria-label=\"Remember Me\">\r\n\t\t\t\t\t\tRemember Me\r\n\t\t\t\t\t</mat-checkbox>\r\n\r\n\t\t\t\t\t<a class=\"forgot-password\" [routerLink]=\"'/pages/auth/forgot-password'\">\r\n\t\t\t\t\t\tForgot Password?\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div> -->\r\n\r\n\t\t\t\t<div class=\"message-box error\" *ngIf=\"errorStatus==401\">\r\n\t\t\t\t\t<span >{{this.errorMsg}}</span>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<button mat-raised-button color=\"accent\" class=\"submit-button\" (click)=login(); aria-label=\"LOG IN\" [disabled]=\"loginForm.invalid\">\r\n\t\t\t\t\tLOGIN\r\n\t\t\t\t</button>\r\n\r\n\r\n\t\t\t</form>\r\n\r\n\t\t\t<!-- <div class=\"separator\">\r\n                <span class=\"text\">OR</span>\r\n            </div>\r\n\r\n            <button mat-raised-button class=\"google\">\r\n                Log in with Google\r\n            </button>\r\n\r\n            <button mat-raised-button class=\"facebook\">\r\n                Log in with Facebook\r\n            </button>\r\n\r\n            <div class=\"register\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                <span class=\"text\">Don't have an account?</span>\r\n                <a class=\"link\" [routerLink]=\"'/pages/auth/register'\">Create an account</a>\r\n            </div> -->\r\n\r\n\t\t</div>\r\n\r\n\t</div>\r\n\r\n</div>",
                    encapsulation: ViewEncapsulation.None
                    // animations: fuseAnimations
                    ,
                    styles: ["login #login{width:100%;background:url(/assets/images/backgrounds/dark-material-bg.jpg) 0 0/cover no-repeat}login #login #login-form-wrapper{-webkit-box-flex:1;flex:1 0 auto;padding:32px}login #login #login-form-wrapper #login-form{width:384px;max-width:384px;padding:32px;text-align:center;background-color:#fff;box-shadow:0 8px 10px -5px rgba(0,0,0,.2),0 16px 24px 2px rgba(0,0,0,.14),0 6px 30px 5px rgba(0,0,0,.12)}login #login #login-form-wrapper #login-form .logo{width:128px;margin:32px auto}login #login #login-form-wrapper #login-form .title{font-size:20px;margin:16px 0 32px}login #login #login-form-wrapper #login-form form{width:100%;text-align:left}login #login #login-form-wrapper #login-form form mat-form-field{width:100%}login #login #login-form-wrapper #login-form form mat-checkbox{margin:0}login #login #login-form-wrapper #login-form form .remember-forgot-password{font-size:13px;margin-top:8px}login #login #login-form-wrapper #login-form form .remember-forgot-password .remember-me{margin-bottom:16px}login #login #login-form-wrapper #login-form form .remember-forgot-password .forgot-password{font-size:13px;font-weight:600;margin-bottom:16px}login #login #login-form-wrapper #login-form form .submit-button{width:220px;margin:16px auto;display:block}@media screen and (max-width:599px){login #login #login-form-wrapper{padding:16px}login #login #login-form-wrapper #login-form{padding:24px;width:100%}login #login #login-form-wrapper #login-form form .submit-button{width:90%}login #login #login-form-wrapper #login-form button{width:80%}}login #login #login-form-wrapper #login-form .register{margin:32px auto 24px;font-weight:600}login #login #login-form-wrapper #login-form .register .text{margin-right:8px}login #login #login-form-wrapper #login-form .separator{font-size:15px;font-weight:600;margin:24px auto;position:relative;overflow:hidden;width:100px}login #login #login-form-wrapper #login-form .separator .text{display:-webkit-inline-box;display:inline-flex;position:relative;padding:0 8px;z-index:9999}login #login #login-form-wrapper #login-form .separator .text:after,login #login #login-form-wrapper #login-form .separator .text:before{content:'';display:block;width:30px;position:absolute;top:10px;border-top:1px solid}login #login #login-form-wrapper #login-form .separator .text:before{right:100%}login #login #login-form-wrapper #login-form .separator .text:after{left:100%}login #login #login-form-wrapper #login-form button.facebook,login #login #login-form-wrapper #login-form button.google{width:192px;text-transform:none;color:#fff;font-size:13px}login #login #login-form-wrapper #login-form button.google{background-color:#d73d32;margin-bottom:8px}login #login #login-form-wrapper #login-form button.facebook{background-color:#3f5c9a}"]
                }] }
    ];
    /** @nocollapse */
    LoginComponent.ctorParameters = function () { return [
        { type: FuseConfigService },
        { type: FormBuilder },
        { type: LoginService },
        { type: Router },
        { type: OAuthService }
    ]; };
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImxvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQWEsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXpDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQ0wsWUFBWSxFQUdiLE1BQU0scUJBQXFCLENBQUM7QUFFN0I7SUFnQkU7Ozs7O09BS0c7SUFDSCx3QkFDVSxrQkFBcUMsRUFDckMsWUFBeUIsRUFDekIsWUFBMEIsRUFDMUIsTUFBYyxFQUNkLFlBQTBCO1FBSjFCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBbUI7UUFDckMsaUJBQVksR0FBWixZQUFZLENBQWE7UUFDekIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBRWxDLHVCQUF1QjtRQUN2QixJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxHQUFHO1lBQy9CLE1BQU0sRUFBRTtnQkFDTixNQUFNLEVBQUU7b0JBQ04sTUFBTSxFQUFFLElBQUk7aUJBQ2I7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLE1BQU0sRUFBRSxJQUFJO2lCQUNiO2dCQUNELE1BQU0sRUFBRTtvQkFDTixNQUFNLEVBQUUsSUFBSTtpQkFDYjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLElBQUk7aUJBQ2I7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxpQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLEVBQUU7WUFDekMsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1NBQ3RDO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDNUI7UUFFRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO1lBQ3ZDLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ25DLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1NBQ3BDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxzQkFBSSw2QkFBQzthQUFMO1lBQ0UsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztRQUNqQyxDQUFDOzs7T0FBQTtJQUVELDhCQUFLLEdBQUw7UUFBQSxpQkFnQkM7UUFmQyxJQUFJLE9BQU8sR0FBRztZQUNaLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLO1lBQ25ELFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLO1NBQ3BELENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQzVDLFVBQUEsSUFBSTtZQUNGLElBQUksS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEtBQUssUUFBUSxFQUFFO2dCQUNoQyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDNUI7UUFDSCxDQUFDLEVBQ0QsVUFBQSxLQUFLO1lBQ0gsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDM0MsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUM7UUFDdkMsQ0FBQyxDQUNGLENBQUM7SUFDSixDQUFDOztnQkF4RkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxPQUFPO29CQUNqQiw4c0ZBQXFDO29CQUVyQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsNkJBQTZCOzs7aUJBQzlCOzs7O2dCQWRRLGlCQUFpQjtnQkFIakIsV0FBVztnQkFJWCxZQUFZO2dCQUhaLE1BQU07Z0JBS2IsWUFBWTs7SUE4RmQscUJBQUM7Q0FBQSxBQXpGRCxJQXlGQztTQWxGWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgRm9ybUJ1aWxkZXIsIEZvcm1Hcm91cCwgVmFsaWRhdG9ycyB9IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlQ29uZmlnU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy9jb25maWcuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBMb2dpblNlcnZpY2UgfSBmcm9tIFwiLi9sb2dpbi5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7XHJcbiAgT0F1dGhTZXJ2aWNlLFxyXG4gIEF1dGhDb25maWcsXHJcbiAgTnVsbFZhbGlkYXRpb25IYW5kbGVyXHJcbn0gZnJvbSBcImFuZ3VsYXItb2F1dGgyLW9pZGNcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImxvZ2luXCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9sb2dpbi5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9sb2dpbi5jb21wb25lbnQuc2Nzc1wiXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbiAgLy8gYW5pbWF0aW9uczogZnVzZUFuaW1hdGlvbnNcclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ2luQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBsb2dpbkZvcm06IEZvcm1Hcm91cDtcclxuXHJcbiAgYXV0aGVudGljYXRpb25FcnJvcjogYm9vbGVhbjtcclxuICB1c2VybmFtZTogc3RyaW5nO1xyXG4gIHBhc3N3b3JkOiBzdHJpbmc7XHJcbiAgZXJyb3JTdGF0dXM6IGFueTtcclxuICBlcnJvck1zZzogYW55O1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtGdXNlQ29uZmlnU2VydmljZX0gX2Z1c2VDb25maWdTZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtGb3JtQnVpbGRlcn0gX2Zvcm1CdWlsZGVyXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9mdXNlQ29uZmlnU2VydmljZTogRnVzZUNvbmZpZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9mb3JtQnVpbGRlcjogRm9ybUJ1aWxkZXIsXHJcbiAgICBwcml2YXRlIGxvZ2luU2VydmljZTogTG9naW5TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgb2F1dGhTZXJ2aWNlOiBPQXV0aFNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIENvbmZpZ3VyZSB0aGUgbGF5b3V0XHJcbiAgICB0aGlzLl9mdXNlQ29uZmlnU2VydmljZS5jb25maWcgPSB7XHJcbiAgICAgIGxheW91dDoge1xyXG4gICAgICAgIG5hdmJhcjoge1xyXG4gICAgICAgICAgaGlkZGVuOiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0b29sYmFyOiB7XHJcbiAgICAgICAgICBoaWRkZW46IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZvb3Rlcjoge1xyXG4gICAgICAgICAgaGlkZGVuOiB0cnVlXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzaWRlcGFuZWw6IHtcclxuICAgICAgICAgIGhpZGRlbjogdHJ1ZVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBPbiBpbml0XHJcbiAgICovXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICBpZiAoIWxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiYWNjZXNzX3Rva2VuXCIpKSB7XHJcbiAgICAgIHRoaXMub2F1dGhTZXJ2aWNlLmluaXRJbXBsaWNpdEZsb3coKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIlwiXSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5sb2dpbkZvcm0gPSB0aGlzLl9mb3JtQnVpbGRlci5ncm91cCh7XHJcbiAgICAgIHVzZXJuYW1lOiBbXCJcIiwgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgIHBhc3N3b3JkOiBbXCJcIiwgVmFsaWRhdG9ycy5yZXF1aXJlZF1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGYoKSB7XHJcbiAgICByZXR1cm4gdGhpcy5sb2dpbkZvcm0uY29udHJvbHM7XHJcbiAgfVxyXG5cclxuICBsb2dpbigpIHtcclxuICAgIHZhciBkYXRhT2JqID0ge1xyXG4gICAgICB1c2VybmFtZTogdGhpcy5sb2dpbkZvcm0uY29udHJvbHNbXCJ1c2VybmFtZVwiXS52YWx1ZSxcclxuICAgICAgcGFzc3dvcmQ6IHRoaXMubG9naW5Gb3JtLmNvbnRyb2xzW1wicGFzc3dvcmRcIl0udmFsdWVcclxuICAgIH07XHJcbiAgICB0aGlzLmxvZ2luU2VydmljZS5sb2dpbkF1dGgoZGF0YU9iaikuc3Vic2NyaWJlKFxyXG4gICAgICBkYXRhID0+IHtcclxuICAgICAgICBpZiAodGhpcy5yb3V0ZXIudXJsID09PSBcIi9sb2dpblwiKSB7XHJcbiAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCJcIl0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgIHRoaXMuZXJyb3JTdGF0dXMgPSBlcnJvci5lcnJvci5tZXRhLnN0YXR1cztcclxuICAgICAgICB0aGlzLmVycm9yTXNnID0gZXJyb3IuZXJyb3IubWV0YS5tc2c7XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiJdfQ==