import { Component, ViewEncapsulation, Input, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
var VerticalBarChartComponent = /** @class */ (function () {
    function VerticalBarChartComponent(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.showLegend = true;
        this.explodeSlices = false;
        this.doughnut = false;
        this.showXAxis = true;
        this.showYAxis = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Population';
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    VerticalBarChartComponent.prototype.ngOnInit = function () {
        this.chartData = this.result;
    };
    VerticalBarChartComponent.prototype.onLegendLabelClick = function (event) { };
    VerticalBarChartComponent.prototype.select = function (event) { };
    VerticalBarChartComponent.decorators = [
        { type: Component, args: [{
                    selector: "vertical-bar-chart",
                    template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\" style=\"align-self: center;\" class=\"h-400 p-10  mat-body-2\">\r\n    <div class=\"inner\" *ngIf=\"chartData.length > 0\">\r\n\r\n      <ngx-charts-bar-vertical class=\"chart-container\" [view]=\"view\" [scheme]=\"scheme\" [results]=\"chartData\"\r\n        [gradient]=\"gradient\"\r\n        [xAxis]=\"showXAxis\"\r\n        [yAxis]=\"showYAxis\"\r\n        [legend]=\"showLegend\"\r\n        [showXAxisLabel]=\"showXAxisLabel\"\r\n        [showYAxisLabel]=\"showYAxisLabel\"\r\n        [xAxisLabel]=\"xAxisLabel\"\r\n        [yAxisLabel]=\"yAxisLabel\"\r\n        (select)=\"select($event)\"> \r\n      </ngx-charts-bar-vertical>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"chartData.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{label.nodata | translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                    encapsulation: ViewEncapsulation.None,
                    animations: fuseAnimations,
                    styles: [".piechartdiv{display:block!important}.piechartdiv .legend-label-text{text-align:left}.inner{display:block;width:100%;text-align:center;margin-left:0!important}.inner img{width:auto;max-width:100%}.piechartdiv .chart-legend{padding-top:10px}.piechartdiv .legend-label{padding-bottom:7px}.piechartdiv .graphdiv{width:auto!important;padding-top:4px}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}"]
                }] }
    ];
    /** @nocollapse */
    VerticalBarChartComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    VerticalBarChartComponent.propDecorators = {
        result: [{ type: Input }],
        scheme: [{ type: Input }],
        view: [{ type: Input }],
        label: [{ type: Input }]
    };
    return VerticalBarChartComponent;
}());
export { VerticalBarChartComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmVydGljYWwtYmFyLWNoYXJ0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ2ZXJ0aWNhbC1iYXItY2hhcnQvdmVydGljYWwtYmFyLWNoYXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULGlCQUFpQixFQUNqQixLQUFLLEVBQ0wsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVyRCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFFbEQ7SUFnQ0UsbUNBQ1UsNkJBQTJELEVBQ3hDLE9BQU87UUFEMUIsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUE4QjtRQUN4QyxZQUFPLEdBQVAsT0FBTyxDQUFBO1FBMUJwQyxlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFlakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLGVBQVUsR0FBRyxTQUFTLENBQUM7UUFDdkIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsZUFBVSxHQUFHLFlBQVksQ0FBQztRQU14QixJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELDRDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQUNELHNEQUFrQixHQUFsQixVQUFtQixLQUFLLElBQUcsQ0FBQztJQUM1QiwwQ0FBTSxHQUFOLFVBQU8sS0FBSyxJQUFHLENBQUM7O2dCQTNDakIsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLGcwQ0FBa0Q7b0JBRWxELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO29CQUNyQyxVQUFVLEVBQUUsY0FBYzs7aUJBQzNCOzs7O2dCQVRRLDRCQUE0QjtnREFxQ2hDLE1BQU0sU0FBQyxTQUFTOzs7eUJBdkJsQixLQUFLO3lCQUNMLEtBQUs7dUJBQ0wsS0FBSzt3QkFDTCxLQUFLOztJQThCUixnQ0FBQztDQUFBLEFBNUNELElBNENDO1NBckNZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uSW5pdCxcclxuICBWaWV3RW5jYXBzdWxhdGlvbixcclxuICBJbnB1dCxcclxuICBJbmplY3RcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBmdXNlQW5pbWF0aW9ucyB9IGZyb20gXCIuLi9AZnVzZS9hbmltYXRpb25zXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5pbXBvcnQgeyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlXCI7XHJcbi8vIGltcG9ydCB7IGxvY2FsZSBhcyBlbmdsaXNoIH0gZnJvbSBcIi4uL2kxOG4vZW5cIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInZlcnRpY2FsLWJhci1jaGFydFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vdmVydGljYWwtYmFyLWNoYXJ0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3ZlcnRpY2FsLWJhci1jaGFydC5jb21wb25lbnQuc2Nzc1wiXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gIGFuaW1hdGlvbnM6IGZ1c2VBbmltYXRpb25zXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWZXJ0aWNhbEJhckNoYXJ0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBzaG93TGVnZW5kID0gdHJ1ZTtcclxuICBleHBsb2RlU2xpY2VzID0gZmFsc2U7XHJcbiAgZG91Z2hudXQgPSBmYWxzZTtcclxuICBASW5wdXQoKSByZXN1bHQ6IGFueTtcclxuICBASW5wdXQoKSBzY2hlbWU6IGFueTtcclxuICBASW5wdXQoKSB2aWV3OiBhbnk7XHJcbiAgQElucHV0KCkgbGFiZWw6IGFueTtcclxuXHJcbiAgY2hhcnREYXRhOiBhbnk7XHJcbiAgdHJhbnNsYXRlOiBhbnk7XHJcbiAgYW5pbWF0aW9ucztcclxuICBsZWdlbmRUaXRsZTtcclxuICBhcmNXaWR0aDtcclxuICBncmFkaWVudDtcclxuICBsZWdlbmRQb3NpdGlvbjtcclxuICBwaWVUb29sdGlwVGV4dDtcclxuXHJcbiAgc2hvd1hBeGlzID0gdHJ1ZTtcclxuICBzaG93WUF4aXMgPSB0cnVlO1xyXG4gIHNob3dYQXhpc0xhYmVsID0gdHJ1ZTtcclxuICB4QXhpc0xhYmVsID0gJ0NvdW50cnknO1xyXG4gIHNob3dZQXhpc0xhYmVsID0gdHJ1ZTtcclxuICB5QXhpc0xhYmVsID0gJ1BvcHVsYXRpb24nO1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2hcclxuICApIHtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5jaGFydERhdGEgPSB0aGlzLnJlc3VsdDtcclxuICB9XHJcbiAgb25MZWdlbmRMYWJlbENsaWNrKGV2ZW50KSB7fVxyXG4gIHNlbGVjdChldmVudCkge31cclxufVxyXG4iXX0=