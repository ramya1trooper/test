import { ButtonLayoutModule } from "./../button-layout/button-layout.module";
import { NewTableLayoutModule } from './../new-table-layout/new-table-layout.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { FuseSharedModule } from "../@fuse/shared.module";
import { HttpClientModule } from "@angular/common/http";
import { GridListLayoutComponent } from "./grid-list-layout.component";
import { CommonModule } from "@angular/common";
import { MaterialModule } from "../material.module";
import { FormLayoutModule } from "../form-layout/form-layout.module";
var GridListLayoutModule = /** @class */ (function () {
    function GridListLayoutModule() {
    }
    GridListLayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [GridListLayoutComponent],
                    imports: [
                        RouterModule,
                        CommonModule,
                        HttpClientModule,
                        MaterialModule,
                        ButtonLayoutModule,
                        NewTableLayoutModule,
                        FormLayoutModule,
                        FuseSharedModule,
                        TranslateModule.forRoot()
                    ],
                    exports: [GridListLayoutComponent],
                    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
                },] }
    ];
    return GridListLayoutModule;
}());
export { GridListLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC1saXN0LWxheW91dC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiZ3JpZC1saXN0LWxheW91dC9ncmlkLWxpc3QtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM3RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUNyRixPQUFPLEVBQ0wsUUFBUSxFQUNSLHNCQUFzQixFQUN0QixnQkFBZ0IsRUFDakIsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBRXJFO0lBQUE7SUFnQm9DLENBQUM7O2dCQWhCcEMsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLHVCQUF1QixDQUFDO29CQUN2QyxPQUFPLEVBQUU7d0JBQ1AsWUFBWTt3QkFDWixZQUFZO3dCQUNaLGdCQUFnQjt3QkFDaEIsY0FBYzt3QkFDZCxrQkFBa0I7d0JBQ2xCLG9CQUFvQjt3QkFDcEIsZ0JBQWdCO3dCQUNoQixnQkFBZ0I7d0JBQ2hCLGVBQWUsQ0FBQyxPQUFPLEVBQUU7cUJBQzFCO29CQUNELE9BQU8sRUFBRSxDQUFDLHVCQUF1QixDQUFDO29CQUNsQyxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsRUFBRSxnQkFBZ0IsQ0FBQztpQkFDcEQ7O0lBQ21DLDJCQUFDO0NBQUEsQUFoQnJDLElBZ0JxQztTQUF4QixvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBCdXR0b25MYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi8uLi9idXR0b24tbGF5b3V0L2J1dHRvbi1sYXlvdXQubW9kdWxlXCI7XHJcbmltcG9ydCB7IE5ld1RhYmxlTGF5b3V0TW9kdWxlIH0gZnJvbSAnLi8uLi9uZXctdGFibGUtbGF5b3V0L25ldy10YWJsZS1sYXlvdXQubW9kdWxlJztcclxuaW1wb3J0IHtcclxuICBOZ01vZHVsZSxcclxuICBDVVNUT01fRUxFTUVOVFNfU0NIRU1BLFxyXG4gIE5PX0VSUk9SU19TQ0hFTUFcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgeyBHcmlkTGlzdExheW91dENvbXBvbmVudCB9IGZyb20gXCIuL2dyaWQtbGlzdC1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBGb3JtTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2Zvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0Lm1vZHVsZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtHcmlkTGlzdExheW91dENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgUm91dGVyTW9kdWxlLFxyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgQnV0dG9uTGF5b3V0TW9kdWxlLFxyXG4gICAgTmV3VGFibGVMYXlvdXRNb2R1bGUsXHJcbiAgICBGb3JtTGF5b3V0TW9kdWxlLFxyXG4gICAgRnVzZVNoYXJlZE1vZHVsZSxcclxuICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KClcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtHcmlkTGlzdExheW91dENvbXBvbmVudF0sXHJcbiAgc2NoZW1hczogW0NVU1RPTV9FTEVNRU5UU19TQ0hFTUEsIE5PX0VSUk9SU19TQ0hFTUFdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBHcmlkTGlzdExheW91dE1vZHVsZSB7IH1cclxuIl19