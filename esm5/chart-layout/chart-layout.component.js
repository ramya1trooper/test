import { MessageService } from "./../_services/message.service";
import { Component } from "@angular/core";
import { ContentService } from "../content/content.service";
import * as _ from "lodash";
import { LoaderService } from '../loader.service';
var ChartLayoutComponent = /** @class */ (function () {
    function ChartLayoutComponent(contentService, messageService, loaderService) {
        var _this = this;
        this.contentService = contentService;
        this.messageService = messageService;
        this.loaderService = loaderService;
        this.enableChart = false;
        this.enablePieChart = false;
        this.enablePieGrid = false;
        this.chartData = [];
        this.pieChartData = [];
        this.pieGridData = [];
        this.inputData = {};
        this.messageService.getMessage().subscribe(function (message) {
            _this.ngOnInit();
        });
    }
    ChartLayoutComponent.prototype.ngOnInit = function () {
        this.defaultDatasource = localStorage.getItem("datasource");
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.queryParams = {
            datasource: this.defaultDatasource
        };
        this.onLoad();
    };
    ChartLayoutComponent.prototype.onLoad = function () {
        var _this = this;
        var currentData = this.currentConfigData["listView"].chartData;
        this.inputData["datasourceId"] = this.defaultDatasource;
        if (currentData) {
            this.colors = currentData.colors;
            this.view = currentData.View;
            var self = this;
            this.pieGridData = [];
            this.pieChartData = [];
            this.chartData = [];
            if (currentData.onLoadFunction && currentData.onLoadFunction.requestData) {
                _.forEach(currentData.onLoadFunction.requestData, function (requestItem) {
                    if (requestItem.isDefault) {
                        self.queryParams[requestItem.name] = requestItem.value;
                    }
                    else {
                        self.queryParams[requestItem.name] = self.inputData[requestItem.value];
                    }
                });
            }
            if (currentData.singleRequest) {
                var requestDetails = currentData.onLoadFunction;
                var apiUrl = requestDetails.apiUrl;
                this.contentService
                    .getAllReponse(this.queryParams, apiUrl)
                    .subscribe(function (resp) {
                    var response = resp.response[currentData.onLoadFunction.response];
                    var keyByResponse = _.keyBy(response, "countsourceName");
                    var self = _this;
                    _.forEach(currentData.chartRequest, function (item) {
                        var data = (keyByResponse[item.chartId] && keyByResponse[item.chartId].countDetails) ? keyByResponse[item.chartId].countDetails : [];
                        var total = _.sumBy(data, "count");
                        self.constructData(data, total, item);
                    });
                });
            }
            else {
                _.forEach(currentData.chartRequest, function (item) {
                    var requestDetails = item.onLoadFunction;
                    var apiUrl = requestDetails.apiUrl;
                    var responseName = requestDetails.response;
                    // if (item.View) {
                    //   this.view = item["View"];
                    // }
                    self.contentService
                        .getAllReponse(self.queryParams, apiUrl)
                        .subscribe(function (resp) {
                        var data = resp.response[responseName];
                        var total = _.sumBy(data, "count");
                        self.constructData(data, total, item);
                    });
                });
            }
        }
    };
    ChartLayoutComponent.prototype.constructData = function (data, total, chartResp) {
        console.log(chartResp, "...chartResp");
        console.log(data, "...data");
        if (chartResp.enableChart) {
            this.enableChart = true;
            this.chartView = chartResp.View
                ? chartResp.View
                : this.currentConfigData["listView"].chartData.View;
            this.chartLabel = {
                title: chartResp.label,
                nodata: chartResp.nodata
            };
        }
        if (chartResp.enablePieChart) {
            this.enablePieChart = true;
            this.pieChartView = chartResp.View ? chartResp.View : this.view;
            this.pieChartLabel = {
                title: chartResp.label,
                nodata: chartResp.nodata
            };
        }
        if (chartResp.enablePieGrid) {
            this.enablePieGrid = true;
            this.pieGridView = chartResp.View ? chartResp.View : this.view;
            this.pieGridLabel = {
                title: chartResp.label,
                nodata: chartResp.nodata
            };
        }
        var self = this;
        _.forEach(data, function (item) {
            var tempObj = {
                name: item.name,
                value: item.count
            };
            if (!chartResp.enablePieChart) {
                tempObj["percentage"] = Math.floor((item.count / total) * 100);
            }
            if (chartResp.enableChart) {
                self.chartData.push(tempObj);
            }
            if (chartResp.enablePieChart) {
                self.pieChartData.push(tempObj);
            }
            if (chartResp.enablePieGrid) {
                self.pieGridData.push(tempObj);
            }
        });
    };
    ChartLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "chart-layout",
                    template: "<div class=\"senlib-padding-10 senlib-m\">\r\n  <div class=\"page-layout \" fxLayoutAlign=\" stretch\" fxLayout=\"row wrap\" fxFlex=\"100\" *fuseIfOnDom>\r\n    <fuse-widget fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"33\" class=\"widget charts-view\" *ngIf=\"enableChart\">\r\n\r\n      <horizontal-chart-layout [result]=\"chartData\" [label]=\"chartLabel\" [color]=\"colors\" [view]=\"chartView\"\r\n        class=\"fusewidgetClass\">\r\n      </horizontal-chart-layout>\r\n\r\n    </fuse-widget>\r\n    <fuse-widget fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"33\" class=\"widget charts-view\" *ngIf=\"enablePieChart\">\r\n\r\n\r\n      <pie-chart [result]=\"pieChartData\" [label]=\"pieChartLabel\" [scheme]=\"colors\" [view]=\"pieChartView\"\r\n        class=\"fusewidgetClass\">\r\n      </pie-chart>\r\n\r\n    </fuse-widget>\r\n    <fuse-widget fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"33\" class=\"widget charts-view\" *ngIf=\"enablePieGrid\">\r\n\r\n\r\n      <pie-grid [result]=\"pieGridData\" [label]=\"pieGridLabel\" [scheme]=\"colors\" [view]=\"pieGridView\"\r\n        class=\"fusewidgetClass\">\r\n      </pie-grid>\r\n\r\n\r\n    </fuse-widget>\r\n\r\n    <!-- <div class=\"charts-view\">\r\n      <ng-container *ngIf=\"enablePieChart\">\r\n        <pie-chart [result]=\"pieChartData\" [label]=\"pieChartLabel\" [scheme]=\"colors\" [view]=\"view\">\r\n        </pie-chart>\r\n      </ng-container>\r\n    </div>\r\n    <div class=\"charts-view\">\r\n      <ng-container *ngIf=\"enablePieGrid\">\r\n        <pie-grid [result]=\"pieGridData\" [label]=\"pieGridLabel\" [scheme]=\"colors\" [view]=\"view\">\r\n        </pie-grid>\r\n      </ng-container>\r\n    </div> -->\r\n  </div>\r\n  <!-- \r\n    <div class=\"page-layout \" fxLayoutAlign=\" stretch\" fxLayout=\"row wrap\" fxFlex=\"100\" *fuseIfOnDom\r\n    [@animateStagger]=\"{value:'50'}\">\r\n    Annimate for widget [@animate]=\"{value:'*',params:{y:'100%'}}\" -->\r\n\r\n</div>\r\n",
                    styles: ["app-chart-layout,app-pie-chart,app-pie-grid{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.charts-view{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;flex-basis:100%;-webkit-box-flex:1;flex:1;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.page-layout{width:auto!important;position:relative;display:-webkit-box;display:flex;z-index:1;flex-wrap:wrap}.page-layout>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1!important;flex:auto!important}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.senlib-padding-10{padding:10px;padding-top:0!important}.senlib-m{margin-left:16px;margin-right:6px!important;margin-top:0;margin-bottom:12px}"]
                }] }
    ];
    /** @nocollapse */
    ChartLayoutComponent.ctorParameters = function () { return [
        { type: ContentService },
        { type: MessageService },
        { type: LoaderService }
    ]; };
    return ChartLayoutComponent;
}());
export { ChartLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhcnQtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJjaGFydC1sYXlvdXQvY2hhcnQtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFNBQVMsRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRTVELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRDtJQTJCRSw4QkFDVSxjQUE4QixFQUM5QixjQUE4QixFQUM5QixhQUE0QjtRQUh0QyxpQkFRQztRQVBTLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUF4QnRDLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBWS9CLGNBQVMsR0FBUSxFQUFFLENBQUM7UUFDcEIsaUJBQVksR0FBUSxFQUFFLENBQUM7UUFDdkIsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFHdEIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQU9sQixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE9BQU87WUFDaEQsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELHVDQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsR0FBRztZQUNqQixVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtTQUNuQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxxQ0FBTSxHQUFOO1FBQUEsaUJBeURDO1FBeERDLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDeEQsSUFBSSxXQUFXLEVBQUU7WUFDZixJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7WUFDakMsSUFBSSxDQUFDLElBQUksR0FBRyxXQUFXLENBQUMsSUFBSSxDQUFDO1lBQzdCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNwQixJQUFHLFdBQVcsQ0FBQyxjQUFjLElBQUksV0FBVyxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQ3ZFO2dCQUNFLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsVUFDaEQsV0FBVztvQkFFWCxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUU7d0JBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUE7cUJBQ3ZEO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN4RTtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBRUQsSUFBRyxXQUFXLENBQUMsYUFBYSxFQUFDO2dCQUMzQixJQUFJLGNBQWMsR0FBRyxXQUFXLENBQUMsY0FBYyxDQUFDO2dCQUM5QyxJQUFJLE1BQU0sR0FBRyxjQUFjLENBQUMsTUFBTSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsY0FBYztxQkFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO3FCQUN2QyxTQUFTLENBQUMsVUFBQSxJQUFJO29CQUNiLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbEUsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztvQkFDekQsSUFBSSxJQUFJLEdBQUcsS0FBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxZQUFZLEVBQUUsVUFBUyxJQUFJO3dCQUM3QyxJQUFJLElBQUksR0FBRyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxZQUF3QixDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ2pKLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO3dCQUNuQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzFDLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ047aUJBQUk7Z0JBQ0gsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLFVBQVMsSUFBSTtvQkFDL0MsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztvQkFDekMsSUFBSSxNQUFNLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQztvQkFDbkMsSUFBSSxZQUFZLEdBQUcsY0FBYyxDQUFDLFFBQVEsQ0FBQztvQkFDM0MsbUJBQW1CO29CQUNuQiw4QkFBOEI7b0JBQzlCLElBQUk7b0JBQ0osSUFBSSxDQUFDLGNBQWM7eUJBQ2hCLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQzt5QkFDdkMsU0FBUyxDQUFDLFVBQUEsSUFBSTt3QkFDYixJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBYSxDQUFDO3dCQUVuRCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzt3QkFDbkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUN4QyxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDLENBQUMsQ0FBQzthQUNKO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsNENBQWEsR0FBYixVQUFjLElBQUksRUFBRSxLQUFLLEVBQUUsU0FBUztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUM3QixJQUFJLFNBQVMsQ0FBQyxXQUFXLEVBQUU7WUFDekIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsSUFBSTtnQkFDN0IsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJO2dCQUNoQixDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7WUFDdEQsSUFBSSxDQUFDLFVBQVUsR0FBRztnQkFDaEIsS0FBSyxFQUFFLFNBQVMsQ0FBQyxLQUFLO2dCQUN0QixNQUFNLEVBQUUsU0FBUyxDQUFDLE1BQU07YUFDekIsQ0FBQztTQUNIO1FBQ0QsSUFBSSxTQUFTLENBQUMsY0FBYyxFQUFFO1lBQzVCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLElBQUksQ0FBQyxZQUFZLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUVoRSxJQUFJLENBQUMsYUFBYSxHQUFHO2dCQUNuQixLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUs7Z0JBQ3RCLE1BQU0sRUFBRSxTQUFTLENBQUMsTUFBTTthQUN6QixDQUFDO1NBQ0g7UUFDRCxJQUFJLFNBQVMsQ0FBQyxhQUFhLEVBQUU7WUFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBRS9ELElBQUksQ0FBQyxZQUFZLEdBQUc7Z0JBQ2xCLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSztnQkFDdEIsTUFBTSxFQUFFLFNBQVMsQ0FBQyxNQUFNO2FBQ3pCLENBQUM7U0FDSDtRQUNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFTLElBQUk7WUFDM0IsSUFBSSxPQUFPLEdBQUc7Z0JBQ1osSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dCQUNmLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSzthQUNsQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLEVBQUU7Z0JBQzdCLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQzthQUNoRTtZQUNELElBQUksU0FBUyxDQUFDLFdBQVcsRUFBRTtnQkFDekIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDOUI7WUFDRCxJQUFJLFNBQVMsQ0FBQyxjQUFjLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ2pDO1lBQ0QsSUFBSSxTQUFTLENBQUMsYUFBYSxFQUFFO2dCQUMzQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNoQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQzs7Z0JBNUpGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsczhEQUE0Qzs7aUJBRTdDOzs7O2dCQVJRLGNBQWM7Z0JBRmQsY0FBYztnQkFLZCxhQUFhOztJQThKdEIsMkJBQUM7Q0FBQSxBQTdKRCxJQTZKQztTQXhKWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLy4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tIFwiLi4vY29udGVudC9jb250ZW50LnNlcnZpY2VcIjtcclxuXHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyLnNlcnZpY2UnO1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJjaGFydC1sYXlvdXRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL2NoYXJ0LWxheW91dC5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9jaGFydC1sYXlvdXQuY29tcG9uZW50LnNjc3NcIl1cclxufSlcclxuZXhwb3J0IGNsYXNzIENoYXJ0TGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBlbmFibGVDaGFydDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGVuYWJsZVBpZUNoYXJ0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgZW5hYmxlUGllR3JpZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICBjdXJyZW50Q29uZmlnRGF0YTogYW55O1xyXG5cclxuICBjaGFydExhYmVsOiBhbnk7XHJcbiAgcGllQ2hhcnRMYWJlbDogYW55O1xyXG4gIHBpZUdyaWRMYWJlbDogYW55O1xyXG4gIGNoYXJ0VmlldzogYW55O1xyXG4gIHBpZUNoYXJ0VmlldzogYW55O1xyXG4gIHBpZUdyaWRWaWV3OiBhbnk7XHJcbiAgcXVlcnlQYXJhbXM6IGFueTtcclxuICBjb2xvcnM6IGFueTtcclxuICBjaGFydERhdGE6IGFueSA9IFtdO1xyXG4gIHBpZUNoYXJ0RGF0YTogYW55ID0gW107XHJcbiAgcGllR3JpZERhdGE6IGFueSA9IFtdO1xyXG4gIHZpZXc6IGFueTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIGlucHV0RGF0YTogYW55ID0ge307XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG1lc3NhZ2VTZXJ2aWNlOiBNZXNzYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5nZXRNZXNzYWdlKCkuc3Vic2NyaWJlKG1lc3NhZ2UgPT4ge1xyXG4gICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJkYXRhc291cmNlXCIpO1xyXG4gICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICk7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICBkYXRhc291cmNlOiB0aGlzLmRlZmF1bHREYXRhc291cmNlXHJcbiAgICB9O1xyXG4gICAgdGhpcy5vbkxvYWQoKTtcclxuICB9XHJcblxyXG4gIG9uTG9hZCgpIHtcclxuICAgIGxldCBjdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXS5jaGFydERhdGE7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImRhdGFzb3VyY2VJZFwiXSA9IHRoaXMuZGVmYXVsdERhdGFzb3VyY2U7XHJcbiAgICBpZiAoY3VycmVudERhdGEpIHtcclxuICAgICAgdGhpcy5jb2xvcnMgPSBjdXJyZW50RGF0YS5jb2xvcnM7XHJcbiAgICAgIHRoaXMudmlldyA9IGN1cnJlbnREYXRhLlZpZXc7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgdGhpcy5waWVHcmlkRGF0YSA9IFtdO1xyXG4gICAgICB0aGlzLnBpZUNoYXJ0RGF0YSA9IFtdO1xyXG4gICAgICB0aGlzLmNoYXJ0RGF0YSA9IFtdO1xyXG4gICAgICBpZihjdXJyZW50RGF0YS5vbkxvYWRGdW5jdGlvbiAmJiBjdXJyZW50RGF0YS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YSlcclxuICAgICAge1xyXG4gICAgICAgIF8uZm9yRWFjaChjdXJyZW50RGF0YS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKFxyXG4gICAgICAgICAgcmVxdWVzdEl0ZW1cclxuICAgICAgICApIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlXHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICBcclxuICAgICAgaWYoY3VycmVudERhdGEuc2luZ2xlUmVxdWVzdCl7XHJcbiAgICAgICAgbGV0IHJlcXVlc3REZXRhaWxzID0gY3VycmVudERhdGEub25Mb2FkRnVuY3Rpb247XHJcbiAgICAgICAgICBsZXQgYXBpVXJsID0gcmVxdWVzdERldGFpbHMuYXBpVXJsO1xyXG4gICAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgIC5nZXRBbGxSZXBvbnNlKHRoaXMucXVlcnlQYXJhbXMsIGFwaVVybClcclxuICAgICAgICAgIC5zdWJzY3JpYmUocmVzcCA9PiB7XHJcbiAgICAgICAgICAgIGxldCByZXNwb25zZSA9IHJlc3AucmVzcG9uc2VbY3VycmVudERhdGEub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2VdO1xyXG4gICAgICAgICAgICBsZXQga2V5QnlSZXNwb25zZSA9IF8ua2V5QnkocmVzcG9uc2UsIFwiY291bnRzb3VyY2VOYW1lXCIpO1xyXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50RGF0YS5jaGFydFJlcXVlc3QsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAgICAgICAgIGxldCBkYXRhID0gKGtleUJ5UmVzcG9uc2VbaXRlbS5jaGFydElkXSAmJiBrZXlCeVJlc3BvbnNlW2l0ZW0uY2hhcnRJZF0uY291bnREZXRhaWxzKSA/IGtleUJ5UmVzcG9uc2VbaXRlbS5jaGFydElkXS5jb3VudERldGFpbHMgYXMgb2JqZWN0W10gOiBbXTtcclxuICAgICAgICAgICAgICAgIGxldCB0b3RhbCA9IF8uc3VtQnkoZGF0YSwgXCJjb3VudFwiKTtcclxuICAgICAgICAgICAgICAgIHNlbGYuY29uc3RydWN0RGF0YShkYXRhLCB0b3RhbCwgaXRlbSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIF8uZm9yRWFjaChjdXJyZW50RGF0YS5jaGFydFJlcXVlc3QsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IGl0ZW0ub25Mb2FkRnVuY3Rpb247XHJcbiAgICAgICAgICBsZXQgYXBpVXJsID0gcmVxdWVzdERldGFpbHMuYXBpVXJsO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IHJlcXVlc3REZXRhaWxzLnJlc3BvbnNlO1xyXG4gICAgICAgICAgLy8gaWYgKGl0ZW0uVmlldykge1xyXG4gICAgICAgICAgLy8gICB0aGlzLnZpZXcgPSBpdGVtW1wiVmlld1wiXTtcclxuICAgICAgICAgIC8vIH1cclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgICAgLmdldEFsbFJlcG9uc2Uoc2VsZi5xdWVyeVBhcmFtcywgYXBpVXJsKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHJlc3AgPT4ge1xyXG4gICAgICAgICAgICAgIGxldCBkYXRhID0gcmVzcC5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdO1xyXG4gIFxyXG4gICAgICAgICAgICAgIGxldCB0b3RhbCA9IF8uc3VtQnkoZGF0YSwgXCJjb3VudFwiKTtcclxuICAgICAgICAgICAgICBzZWxmLmNvbnN0cnVjdERhdGEoZGF0YSwgdG90YWwsIGl0ZW0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY29uc3RydWN0RGF0YShkYXRhLCB0b3RhbCwgY2hhcnRSZXNwKSB7XHJcbiAgICBjb25zb2xlLmxvZyhjaGFydFJlc3AsIFwiLi4uY2hhcnRSZXNwXCIpO1xyXG4gICAgY29uc29sZS5sb2coZGF0YSwgXCIuLi5kYXRhXCIpO1xyXG4gICAgaWYgKGNoYXJ0UmVzcC5lbmFibGVDaGFydCkge1xyXG4gICAgICB0aGlzLmVuYWJsZUNoYXJ0ID0gdHJ1ZTtcclxuICAgICAgdGhpcy5jaGFydFZpZXcgPSBjaGFydFJlc3AuVmlld1xyXG4gICAgICAgID8gY2hhcnRSZXNwLlZpZXdcclxuICAgICAgICA6IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXS5jaGFydERhdGEuVmlldztcclxuICAgICAgdGhpcy5jaGFydExhYmVsID0ge1xyXG4gICAgICAgIHRpdGxlOiBjaGFydFJlc3AubGFiZWwsXHJcbiAgICAgICAgbm9kYXRhOiBjaGFydFJlc3Aubm9kYXRhXHJcbiAgICAgIH07XHJcbiAgICB9XHJcbiAgICBpZiAoY2hhcnRSZXNwLmVuYWJsZVBpZUNoYXJ0KSB7XHJcbiAgICAgIHRoaXMuZW5hYmxlUGllQ2hhcnQgPSB0cnVlO1xyXG4gICAgICB0aGlzLnBpZUNoYXJ0VmlldyA9IGNoYXJ0UmVzcC5WaWV3ID8gY2hhcnRSZXNwLlZpZXcgOiB0aGlzLnZpZXc7XHJcblxyXG4gICAgICB0aGlzLnBpZUNoYXJ0TGFiZWwgPSB7XHJcbiAgICAgICAgdGl0bGU6IGNoYXJ0UmVzcC5sYWJlbCxcclxuICAgICAgICBub2RhdGE6IGNoYXJ0UmVzcC5ub2RhdGFcclxuICAgICAgfTtcclxuICAgIH1cclxuICAgIGlmIChjaGFydFJlc3AuZW5hYmxlUGllR3JpZCkge1xyXG4gICAgICB0aGlzLmVuYWJsZVBpZUdyaWQgPSB0cnVlO1xyXG4gICAgICB0aGlzLnBpZUdyaWRWaWV3ID0gY2hhcnRSZXNwLlZpZXcgPyBjaGFydFJlc3AuVmlldyA6IHRoaXMudmlldztcclxuXHJcbiAgICAgIHRoaXMucGllR3JpZExhYmVsID0ge1xyXG4gICAgICAgIHRpdGxlOiBjaGFydFJlc3AubGFiZWwsXHJcbiAgICAgICAgbm9kYXRhOiBjaGFydFJlc3Aubm9kYXRhXHJcbiAgICAgIH07XHJcbiAgICB9XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICBfLmZvckVhY2goZGF0YSwgZnVuY3Rpb24oaXRlbSkge1xyXG4gICAgICBsZXQgdGVtcE9iaiA9IHtcclxuICAgICAgICBuYW1lOiBpdGVtLm5hbWUsXHJcbiAgICAgICAgdmFsdWU6IGl0ZW0uY291bnRcclxuICAgICAgfTtcclxuICAgICAgaWYgKCFjaGFydFJlc3AuZW5hYmxlUGllQ2hhcnQpIHtcclxuICAgICAgICB0ZW1wT2JqW1wicGVyY2VudGFnZVwiXSA9IE1hdGguZmxvb3IoKGl0ZW0uY291bnQgLyB0b3RhbCkgKiAxMDApO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChjaGFydFJlc3AuZW5hYmxlQ2hhcnQpIHtcclxuICAgICAgICBzZWxmLmNoYXJ0RGF0YS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChjaGFydFJlc3AuZW5hYmxlUGllQ2hhcnQpIHtcclxuICAgICAgICBzZWxmLnBpZUNoYXJ0RGF0YS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChjaGFydFJlc3AuZW5hYmxlUGllR3JpZCkge1xyXG4gICAgICAgIHNlbGYucGllR3JpZERhdGEucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==