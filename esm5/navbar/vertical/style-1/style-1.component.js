import { Component, ViewChild, ViewEncapsulation, Inject } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { Subject } from "rxjs";
import { delay, filter, take, takeUntil } from "rxjs/operators";
import { FuseConfigService } from "../../../@fuse/services/config.service";
import { FuseNavigationService } from "../../../@fuse/components/navigation/navigation.service";
import { FusePerfectScrollbarDirective } from "../../../@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";
import { FuseSidebarService } from "../../../@fuse/components/sidebar/sidebar.service";
var NavbarVerticalStyle1Component = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     */
    function NavbarVerticalStyle1Component(_fuseConfigService, _fuseNavigationService, _fuseSidebarService, _router, english) {
        this._fuseConfigService = _fuseConfigService;
        this._fuseNavigationService = _fuseNavigationService;
        this._fuseSidebarService = _fuseSidebarService;
        this._router = _router;
        this.english = english;
        this.toggleOpen = true;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        // console.log(english, "::::::::::english>>>>>>>");
    }
    Object.defineProperty(NavbarVerticalStyle1Component.prototype, "directive", {
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        // Directive
        set: function (theDirective) {
            var _this = this;
            if (!theDirective) {
                return;
            }
            this._fusePerfectScrollbar = theDirective;
            // Update the scrollbar on collapsable item toggle
            this._fuseNavigationService.onItemCollapseToggled
                .pipe(delay(500), takeUntil(this._unsubscribeAll))
                .subscribe(function () {
                _this._fusePerfectScrollbar.update();
            });
            // Scroll to the active item position
            this._router.events
                .pipe(filter(function (event) { return event instanceof NavigationEnd; }), take(1))
                .subscribe(function () {
                setTimeout(function () {
                    var activeNavItem = document.querySelector("navbar .nav-link.active");
                    if (activeNavItem) {
                        var activeItemOffsetTop = activeNavItem.offsetTop, activeItemOffsetParentTop = activeNavItem.offsetParent.offsetTop, scrollDistance = activeItemOffsetTop - activeItemOffsetParentTop - 48 * 3 - 168;
                        _this._fusePerfectScrollbar.scrollToTop(scrollDistance);
                    }
                });
            });
        },
        enumerable: true,
        configurable: true
    });
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    NavbarVerticalStyle1Component.prototype.ngOnInit = function () {
        var _this = this;
        this._router.events
            .pipe(filter(function (event) { return event instanceof NavigationEnd; }), takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            if (_this._fuseSidebarService.getSidebar("navbar")) {
                _this._fuseSidebarService.getSidebar("navbar").close();
            }
        });
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function (config) {
            _this.fuseConfig = config;
        });
        // Get current navigation
        this._fuseNavigationService.onNavigationChanged
            .pipe(filter(function (value) { return value !== null; }), takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            _this.navigation = _this._fuseNavigationService.getCurrentNavigation();
        });
    };
    /**
     * On destroy
     */
    NavbarVerticalStyle1Component.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle sidebar opened status
     */
    NavbarVerticalStyle1Component.prototype.toggleSidebarOpened = function () {
        this.toggleOpen = true;
        this._fuseSidebarService.getSidebar("navbar").toggleOpen();
    };
    /**
     * Toggle sidebar folded status
     */
    NavbarVerticalStyle1Component.prototype.toggleSidebarFolded = function () {
        this.toggleOpen = false;
        this._fuseSidebarService.getSidebar("navbar").toggleFold();
    };
    NavbarVerticalStyle1Component.decorators = [
        { type: Component, args: [{
                    selector: "navbar-vertical-style-1",
                    template: "<div class=\"navbar-top\" [ngClass]=\"fuseConfig.layout.navbar.secondaryBackground\">\r\n\r\n  <div class=\"logo\">\r\n    <img *ngIf=\"!toggleOpen\" class=\"logo-icon\" src=\"assets/images/logos/trooper.png\">\r\n    <img *ngIf=\"toggleOpen\" src=\"assets/images/logos/trooper.png\">\r\n  </div>\r\n\r\n  <div class=\"buttons\">\r\n\r\n    <button mat-icon-button class=\"toggle-sidebar-folded\" (click)=\"toggleSidebarFolded()\" fxHide.lt-lg>\r\n      <mat-icon class=\"secondary-text\" style=\"color:#8c51ff !important\">menu</mat-icon>\r\n    </button>\r\n\r\n    <button mat-icon-button class=\"toggle-sidebar-opened\" (click)=\"toggleSidebarOpened()\" fxHide.gt-md>\r\n      <mat-icon class=\"secondary-text\" style=\"color:#8c51ff !important\">arrow_back</mat-icon>\r\n    </button>\r\n\r\n  </div>\r\n\r\n</div>\r\n\r\n\r\n<div class=\"navbar-scroll-container\" [ngClass]=\"fuseConfig.layout.navbar.primaryBackground\" fusePerfectScrollbar\r\n  [fusePerfectScrollbarOptions]=\"{suppressScrollX: true}\">\r\n\r\n  <div class=\"navbar-content\">\r\n    <fuse-navigation class=\"material2\" layout=\"vertical\"></fuse-navigation>\r\n  </div>\r\n\r\n</div>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: ["fuse-sidebar.navbar-fuse-sidebar{overflow:hidden}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top{padding:12px 0;-webkit-box-pack:center;justify-content:center;background-color:#fff!important}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top .buttons{display:none}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top .logo .logo-icon{width:100%}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top .logo .logo-text{display:none}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user{padding:12px 0;height:64px;min-height:64px;max-height:64px}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container{position:relative;top:auto;padding:0;-webkit-transform:translateX(0);transform:translateX(0);left:auto}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container .avatar{width:40px;height:40px}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .email,fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .username{display:none}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .navbar-content{padding-top:0}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .navbar-content .material2 .nav-item .nav-link{border-radius:20px;margin:0 12px;padding:0 12px}navbar.vertical-style-1{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;width:100%;height:100%}navbar.vertical-style-1.right-navbar .toggle-sidebar-opened mat-icon{-webkit-transform:rotate(180deg);transform:rotate(180deg)}navbar navbar-vertical-style-1{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;width:100%;height:100%}navbar navbar-vertical-style-1 .navbar-top{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-flex:1;flex:1 0 auto;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between;min-height:64px;max-height:64px;height:64px;padding:12px 12px 12px 20px;background-color:#fff!important;border-bottom:.5px solid grey}@media screen and (max-width:599px){navbar navbar-vertical-style-1 .navbar-top{min-height:56px;max-height:56px;height:56px}}navbar navbar-vertical-style-1 .navbar-top .logo{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}navbar navbar-vertical-style-1 .navbar-top .logo .logo-icon{width:100%}navbar navbar-vertical-style-1 .navbar-top .logo .logo-text{margin-left:12px;font-size:16px;font-weight:300;letter-spacing:.4px;line-height:normal}navbar navbar-vertical-style-1 .navbar-top .buttons{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}navbar navbar-vertical-style-1 .navbar-scroll-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;overflow-y:auto;-webkit-overflow-scrolling:touch;background:url(assets/images/logos/1.png)!important}navbar navbar-vertical-style-1 .navbar-scroll-container .user{position:relative;display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;-webkit-box-pack:start;justify-content:flex-start;width:100%;height:136px;min-height:136px;max-height:136px;padding:24px 0 64px}navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container{position:absolute;top:92px;border-radius:50%;padding:8px;-webkit-transform:translateX(-50%);transform:translateX(-50%);left:50%}navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container .avatar{width:72px;height:72px;margin:0}navbar navbar-vertical-style-1 .navbar-scroll-container .navbar-content{-webkit-box-flex:1;flex:1 1 auto;padding-top:32px}"]
                }] }
    ];
    /** @nocollapse */
    NavbarVerticalStyle1Component.ctorParameters = function () { return [
        { type: FuseConfigService },
        { type: FuseNavigationService },
        { type: FuseSidebarService },
        { type: Router },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    NavbarVerticalStyle1Component.propDecorators = {
        directive: [{ type: ViewChild, args: [FusePerfectScrollbarDirective,] }]
    };
    return NavbarVerticalStyle1Component;
}());
export { NavbarVerticalStyle1Component };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUtMS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibmF2YmFyL3ZlcnRpY2FsL3N0eWxlLTEvc3R5bGUtMS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFHVCxTQUFTLEVBQ1QsaUJBQWlCLEVBQ2pCLE1BQU0sRUFDUCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWhFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG1GQUFtRixDQUFDO0FBQ2xJLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBRXZGO0lBZUU7Ozs7Ozs7T0FPRztJQUNILHVDQUNVLGtCQUFxQyxFQUNyQyxzQkFBNkMsRUFDN0MsbUJBQXVDLEVBQ3ZDLE9BQWUsRUFDRyxPQUFPO1FBSnpCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBbUI7UUFDckMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF1QjtRQUM3Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQW9CO1FBQ3ZDLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFDRyxZQUFPLEdBQVAsT0FBTyxDQUFBO1FBbkJuQyxlQUFVLEdBQVksSUFBSSxDQUFDO1FBcUJ6QiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLG9EQUFvRDtJQUN0RCxDQUFDO0lBT0Qsc0JBQ0ksb0RBQVM7UUFOYix3R0FBd0c7UUFDeEcsY0FBYztRQUNkLHdHQUF3RztRQUV4RyxZQUFZO2FBQ1osVUFDYyxZQUEyQztZQUR6RCxpQkFxQ0M7WUFuQ0MsSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDakIsT0FBTzthQUNSO1lBRUQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLFlBQVksQ0FBQztZQUUxQyxrREFBa0Q7WUFDbEQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQjtpQkFDOUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2lCQUNqRCxTQUFTLENBQUM7Z0JBQ1QsS0FBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ3RDLENBQUMsQ0FBQyxDQUFDO1lBRUwscUNBQXFDO1lBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTtpQkFDaEIsSUFBSSxDQUNILE1BQU0sQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssWUFBWSxhQUFhLEVBQTlCLENBQThCLENBQUMsRUFDL0MsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUNSO2lCQUNBLFNBQVMsQ0FBQztnQkFDVCxVQUFVLENBQUM7b0JBQ1QsSUFBTSxhQUFhLEdBQVEsUUFBUSxDQUFDLGFBQWEsQ0FDL0MseUJBQXlCLENBQzFCLENBQUM7b0JBRUYsSUFBSSxhQUFhLEVBQUU7d0JBQ2pCLElBQU0sbUJBQW1CLEdBQUcsYUFBYSxDQUFDLFNBQVMsRUFDakQseUJBQXlCLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQ2hFLGNBQWMsR0FDWixtQkFBbUIsR0FBRyx5QkFBeUIsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQzt3QkFFbkUsS0FBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQztxQkFDeEQ7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7OztPQUFBO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxnREFBUSxHQUFSO1FBQUEsaUJBNEJDO1FBM0JDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTthQUNoQixJQUFJLENBQ0gsTUFBTSxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLGFBQWEsRUFBOUIsQ0FBOEIsQ0FBQyxFQUMvQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUNoQzthQUNBLFNBQVMsQ0FBQztZQUNULElBQUksS0FBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN2RDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNO2FBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQyxVQUFBLE1BQU07WUFDZixLQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztRQUVMLHlCQUF5QjtRQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CO2FBQzVDLElBQUksQ0FDSCxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLEtBQUssSUFBSSxFQUFkLENBQWMsQ0FBQyxFQUMvQixTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUNoQzthQUNBLFNBQVMsQ0FBQztZQUNULEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDdkUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxtREFBVyxHQUFYO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCwyREFBbUIsR0FBbkI7UUFDRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztRQUN2QixJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzdELENBQUM7SUFFRDs7T0FFRztJQUNILDJEQUFtQixHQUFuQjtRQUNFLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDN0QsQ0FBQzs7Z0JBL0lGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUseUJBQXlCO29CQUNuQyx3cENBQXVDO29CQUV2QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3RDOzs7O2dCQVZRLGlCQUFpQjtnQkFDakIscUJBQXFCO2dCQUVyQixrQkFBa0I7Z0JBUEgsTUFBTTtnREFxQ3pCLE1BQU0sU0FBQyxTQUFTOzs7NEJBWWxCLFNBQVMsU0FBQyw2QkFBNkI7O0lBd0cxQyxvQ0FBQztDQUFBLEFBaEpELElBZ0pDO1NBMUlZLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uRGVzdHJveSxcclxuICBPbkluaXQsXHJcbiAgVmlld0NoaWxkLFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gIEluamVjdFxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCB7IGRlbGF5LCBmaWx0ZXIsIHRha2UsIHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5cclxuaW1wb3J0IHsgRnVzZUNvbmZpZ1NlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2Uvc2VydmljZXMvY29uZmlnLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25TZXJ2aWNlIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgRnVzZVBlcmZlY3RTY3JvbGxiYXJEaXJlY3RpdmUgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2UvZGlyZWN0aXZlcy9mdXNlLXBlcmZlY3Qtc2Nyb2xsYmFyL2Z1c2UtcGVyZmVjdC1zY3JvbGxiYXIuZGlyZWN0aXZlXCI7XHJcbmltcG9ydCB7IEZ1c2VTaWRlYmFyU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL3NpZGViYXIvc2lkZWJhci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJuYXZiYXItdmVydGljYWwtc3R5bGUtMVwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vc3R5bGUtMS5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9zdHlsZS0xLmNvbXBvbmVudC5zY3NzXCJdLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmJhclZlcnRpY2FsU3R5bGUxQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIGZ1c2VDb25maWc6IGFueTtcclxuICBuYXZpZ2F0aW9uOiBhbnk7XHJcbiAgdG9nZ2xlT3BlbjogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF9mdXNlUGVyZmVjdFNjcm9sbGJhcjogRnVzZVBlcmZlY3RTY3JvbGxiYXJEaXJlY3RpdmU7XHJcbiAgcHJpdmF0ZSBfdW5zdWJzY3JpYmVBbGw6IFN1YmplY3Q8YW55PjtcclxuXHJcbiAgLyoqXHJcbiAgICogQ29uc3RydWN0b3JcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7RnVzZUNvbmZpZ1NlcnZpY2V9IF9mdXNlQ29uZmlnU2VydmljZVxyXG4gICAqIEBwYXJhbSB7RnVzZU5hdmlnYXRpb25TZXJ2aWNlfSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtGdXNlU2lkZWJhclNlcnZpY2V9IF9mdXNlU2lkZWJhclNlcnZpY2VcclxuICAgKiBAcGFyYW0ge1JvdXRlcn0gX3JvdXRlclxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZUNvbmZpZ1NlcnZpY2U6IEZ1c2VDb25maWdTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlOiBGdXNlTmF2aWdhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9mdXNlU2lkZWJhclNlcnZpY2U6IEZ1c2VTaWRlYmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgX3JvdXRlcjogUm91dGVyLFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHVibGljIGVuZ2xpc2hcclxuICApIHtcclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgLy8gY29uc29sZS5sb2coZW5nbGlzaCwgXCI6Ojo6Ojo6Ojo6ZW5nbGlzaD4+Pj4+Pj5cIik7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgQWNjZXNzb3JzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLy8gRGlyZWN0aXZlXHJcbiAgQFZpZXdDaGlsZChGdXNlUGVyZmVjdFNjcm9sbGJhckRpcmVjdGl2ZSlcclxuICBzZXQgZGlyZWN0aXZlKHRoZURpcmVjdGl2ZTogRnVzZVBlcmZlY3RTY3JvbGxiYXJEaXJlY3RpdmUpIHtcclxuICAgIGlmICghdGhlRGlyZWN0aXZlKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl9mdXNlUGVyZmVjdFNjcm9sbGJhciA9IHRoZURpcmVjdGl2ZTtcclxuXHJcbiAgICAvLyBVcGRhdGUgdGhlIHNjcm9sbGJhciBvbiBjb2xsYXBzYWJsZSBpdGVtIHRvZ2dsZVxyXG4gICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uSXRlbUNvbGxhcHNlVG9nZ2xlZFxyXG4gICAgICAucGlwZShkZWxheSg1MDApLCB0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICB0aGlzLl9mdXNlUGVyZmVjdFNjcm9sbGJhci51cGRhdGUoKTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gU2Nyb2xsIHRvIHRoZSBhY3RpdmUgaXRlbSBwb3NpdGlvblxyXG4gICAgdGhpcy5fcm91dGVyLmV2ZW50c1xyXG4gICAgICAucGlwZShcclxuICAgICAgICBmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSxcclxuICAgICAgICB0YWtlKDEpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICBjb25zdCBhY3RpdmVOYXZJdGVtOiBhbnkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFxyXG4gICAgICAgICAgICBcIm5hdmJhciAubmF2LWxpbmsuYWN0aXZlXCJcclxuICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgaWYgKGFjdGl2ZU5hdkl0ZW0pIHtcclxuICAgICAgICAgICAgY29uc3QgYWN0aXZlSXRlbU9mZnNldFRvcCA9IGFjdGl2ZU5hdkl0ZW0ub2Zmc2V0VG9wLFxyXG4gICAgICAgICAgICAgIGFjdGl2ZUl0ZW1PZmZzZXRQYXJlbnRUb3AgPSBhY3RpdmVOYXZJdGVtLm9mZnNldFBhcmVudC5vZmZzZXRUb3AsXHJcbiAgICAgICAgICAgICAgc2Nyb2xsRGlzdGFuY2UgPVxyXG4gICAgICAgICAgICAgICAgYWN0aXZlSXRlbU9mZnNldFRvcCAtIGFjdGl2ZUl0ZW1PZmZzZXRQYXJlbnRUb3AgLSA0OCAqIDMgLSAxNjg7XHJcblxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlUGVyZmVjdFNjcm9sbGJhci5zY3JvbGxUb1RvcChzY3JvbGxEaXN0YW5jZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIE9uIGluaXRcclxuICAgKi9cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIHRoaXMuX3JvdXRlci5ldmVudHNcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCksXHJcbiAgICAgICAgdGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLl9mdXNlU2lkZWJhclNlcnZpY2UuZ2V0U2lkZWJhcihcIm5hdmJhclwiKSkge1xyXG4gICAgICAgICAgdGhpcy5fZnVzZVNpZGViYXJTZXJ2aWNlLmdldFNpZGViYXIoXCJuYXZiYXJcIikuY2xvc2UoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIFN1YnNjcmliZSB0byB0aGUgY29uZmlnIGNoYW5nZXNcclxuICAgIHRoaXMuX2Z1c2VDb25maWdTZXJ2aWNlLmNvbmZpZ1xyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKGNvbmZpZyA9PiB7XHJcbiAgICAgICAgdGhpcy5mdXNlQ29uZmlnID0gY29uZmlnO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAvLyBHZXQgY3VycmVudCBuYXZpZ2F0aW9uXHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uQ2hhbmdlZFxyXG4gICAgICAucGlwZShcclxuICAgICAgICBmaWx0ZXIodmFsdWUgPT4gdmFsdWUgIT09IG51bGwpLFxyXG4gICAgICAgIHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbClcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICB0aGlzLm5hdmlnYXRpb24gPSB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2UuZ2V0Q3VycmVudE5hdmlnYXRpb24oKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBPbiBkZXN0cm95XHJcbiAgICovXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAvLyBVbnN1YnNjcmliZSBmcm9tIGFsbCBzdWJzY3JpcHRpb25zXHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5uZXh0KCk7XHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5jb21wbGV0ZSgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogVG9nZ2xlIHNpZGViYXIgb3BlbmVkIHN0YXR1c1xyXG4gICAqL1xyXG4gIHRvZ2dsZVNpZGViYXJPcGVuZWQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnRvZ2dsZU9wZW4gPSB0cnVlO1xyXG4gICAgdGhpcy5fZnVzZVNpZGViYXJTZXJ2aWNlLmdldFNpZGViYXIoXCJuYXZiYXJcIikudG9nZ2xlT3BlbigpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVG9nZ2xlIHNpZGViYXIgZm9sZGVkIHN0YXR1c1xyXG4gICAqL1xyXG4gIHRvZ2dsZVNpZGViYXJGb2xkZWQoKTogdm9pZCB7XHJcbiAgICB0aGlzLnRvZ2dsZU9wZW4gPSBmYWxzZTtcclxuICAgIHRoaXMuX2Z1c2VTaWRlYmFyU2VydmljZS5nZXRTaWRlYmFyKFwibmF2YmFyXCIpLnRvZ2dsZUZvbGQoKTtcclxuICB9XHJcbn1cclxuIl19