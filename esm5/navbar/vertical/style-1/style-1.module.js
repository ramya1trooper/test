import { NgModule } from "@angular/core";
import { FuseNavigationModule } from "../../../@fuse/components/navigation/navigation.module";
import { FuseSharedModule } from "../../../@fuse/shared.module";
import { NavbarVerticalStyle1Component } from "./style-1.component";
import { MaterialModule } from "../../../material.module";
var NavbarVerticalStyle1Module = /** @class */ (function () {
    function NavbarVerticalStyle1Module() {
    }
    NavbarVerticalStyle1Module.decorators = [
        { type: NgModule, args: [{
                    declarations: [NavbarVerticalStyle1Component],
                    imports: [MaterialModule, FuseSharedModule, FuseNavigationModule],
                    exports: [NavbarVerticalStyle1Component]
                },] }
    ];
    return NavbarVerticalStyle1Module;
}());
export { NavbarVerticalStyle1Module };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUtMS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibmF2YmFyL3ZlcnRpY2FsL3N0eWxlLTEvc3R5bGUtMS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3REFBd0QsQ0FBQztBQUM5RixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUVoRSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFMUQ7SUFBQTtJQUt5QyxDQUFDOztnQkFMekMsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLDZCQUE2QixDQUFDO29CQUM3QyxPQUFPLEVBQUUsQ0FBQyxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUM7b0JBQ2pFLE9BQU8sRUFBRSxDQUFDLDZCQUE2QixDQUFDO2lCQUN6Qzs7SUFDd0MsaUNBQUM7Q0FBQSxBQUwxQyxJQUswQztTQUE3QiwwQkFBMEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlTmF2aWdhdGlvbk1vZHVsZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5tb2R1bGVcIjtcclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9zaGFyZWQubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBOYXZiYXJWZXJ0aWNhbFN0eWxlMUNvbXBvbmVudCB9IGZyb20gXCIuL3N0eWxlLTEuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtOYXZiYXJWZXJ0aWNhbFN0eWxlMUNvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW01hdGVyaWFsTW9kdWxlLCBGdXNlU2hhcmVkTW9kdWxlLCBGdXNlTmF2aWdhdGlvbk1vZHVsZV0sXHJcbiAgZXhwb3J0czogW05hdmJhclZlcnRpY2FsU3R5bGUxQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmF2YmFyVmVydGljYWxTdHlsZTFNb2R1bGUge31cclxuIl19