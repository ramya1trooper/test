import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
// import { FormLayoutModule } from "../form-layout/form-layout.module";
import { ButtonLayoutModule } from "../button-layout/button-layout.module";
import { ConfirmDialogComponent } from './confirm-dialog.component';
var ConfirmDialogModule = /** @class */ (function () {
    function ConfirmDialogModule() {
    }
    ConfirmDialogModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [ConfirmDialogComponent],
                    imports: [
                        RouterModule,
                        // FormLayoutModule,
                        ButtonLayoutModule,
                        // Material
                        MaterialModule,
                        TranslateModule,
                        FuseSharedModule
                    ],
                    exports: [ConfirmDialogComponent]
                },] }
    ];
    return ConfirmDialogModule;
}());
export { ConfirmDialogModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1kaWFsb2cubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBR3BELHdFQUF3RTtBQUN4RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUMzRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRTtJQUFBO0lBZ0JBLENBQUM7O2dCQWhCQSxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMsc0JBQXNCLENBQUM7b0JBQ3RDLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLG9CQUFvQjt3QkFDcEIsa0JBQWtCO3dCQUNsQixXQUFXO3dCQUNYLGNBQWM7d0JBQ2QsZUFBZTt3QkFFZixnQkFBZ0I7cUJBQ2pCO29CQUNELE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2lCQUNsQzs7SUFHRCwwQkFBQztDQUFBLEFBaEJELElBZ0JDO1NBRlksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBNb2RlbExheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi9tb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0LmNvbXBvbmVudFwiO1xyXG4vLyBpbXBvcnQgeyBGb3JtTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2Zvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBCdXR0b25MYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vYnV0dG9uLWxheW91dC9idXR0b24tbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBDb25maXJtRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi9jb25maXJtLWRpYWxvZy5jb21wb25lbnQnO1xyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0NvbmZpcm1EaWFsb2dDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIC8vIEZvcm1MYXlvdXRNb2R1bGUsXHJcbiAgICBCdXR0b25MYXlvdXRNb2R1bGUsXHJcbiAgICAvLyBNYXRlcmlhbFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUsXHJcblxyXG4gICAgRnVzZVNoYXJlZE1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW0NvbmZpcm1EaWFsb2dDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb25maXJtRGlhbG9nTW9kdWxlIHtcclxuXHJcbn1cclxuIl19