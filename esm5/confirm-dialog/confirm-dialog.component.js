import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Component, Inject } from '@angular/core';
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
var ConfirmDialogComponent = /** @class */ (function () {
    function ConfirmDialogComponent(dialogRef, _fuseTranslationLoaderService, english, data) {
        this.dialogRef = dialogRef;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.data = data;
        this.modelData = {};
        this.includeNote = false;
        this._fuseTranslationLoaderService.loadTranslations(english);
        // Update view with given values
        console.log(">>>> Confirm-dialog Constructor received ", data);
    }
    ConfirmDialogComponent.prototype.ngOnInit = function () {
        console.log(">>> ng on init this.data ", this.data);
        if (this.data && this.data.modelData) {
            this.modelData = this.data.modelData;
        }
        else {
            var defaultmodelData = {
                messageData: {
                    messsage: "Are you sure you want to do this?"
                },
                noteData: {
                    messsage: ""
                }
            };
            this.modelData = defaultmodelData;
        }
        if (this.modelData.includeNote)
            this.includeNote = true;
        console.log('>>> this.modelData ', this.modelData);
        console.log(">>> includeNote ", this.includeNote);
    };
    ConfirmDialogComponent.prototype.onConfirm = function () {
        // Close the dialog, return true
        this.dialogRef.close(true);
    };
    ConfirmDialogComponent.prototype.onDismiss = function () {
        // Close the dialog, return false
        this.dialogRef.close(false);
    };
    ConfirmDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-confirm-dialog',
                    template: "<div class=\"card ui-common-lib-card\">\r\n  <div class=\"card-header ui-common-lib-popupheader\">\r\n      <div class=\"card-title\">\r\n        <div class=\"header-top  header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n        fxlayoutalign=\"space-between center\">\r\n            <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n             <h2 class=\"m-0 font-weight-900 sen-lib-white\" > <span>\r\n              <mat-icon  class=\"material-icons ui-common-info-icon\"\r\n               >\r\n              info\r\n                 </mat-icon> \r\n            </span>{{modelData.header | translate}}</h2>\r\n            </div>\r\n            <!-- <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n              <button mat-icon-button (click)=\"onDismiss()\" aria-label=\"Close Dialog\" class=\"ui-common-lib-outline\"\r\n                style=\"float:right;margin-right:12px;\">\r\n                <mat-icon>close</mat-icon>\r\n              </button>\r\n            </div> -->\r\n        </div>\r\n      </div>\r\n  </div>\r\n  <div class=\"card-body\">\r\n    <div class=\"text-center sen-lib-p\">\r\n      <div class=\"ui-common-lib-alert-text\">\r\n        {{modelData.messageData.message | translate}}\r\n        <ng-container *ngIf=\"includeNote\">\r\n          <p class=\"ui-common-note\">\r\n            <span><strong>Note:&nbsp;&nbsp;</strong></span>{{modelData.noteData.message | translate}}</p>\r\n        </ng-container>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"card-footer ui-common-lib-card-footer\">\r\n    <div class=\"col-md-12 text-right\" >\r\n      <button class=\"btn btn-primary ui-common-confirm-btn\"  (click)=\"onConfirm()\">{{modelData.confirmLabel.YES | translate}}</button>\r\n      <button class=\"btn btn-primary ui-common-cancel-btn\"\r\n        (click)=\"onDismiss()\">{{modelData.confirmLabel.NO | translate}}</button>\r\n    </div>   \r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n",
                    styles: [".sen-lib-p{padding:1rem 1.25rem}.ui-common-lib-alert-text{font-weight:600;text-transform:none;position:relative;display:block;padding:13px 16px;text-align:left;font-size:20px;line-height:normal}.ui-common-lib-popupheader{padding:5px 0 5px 12px!important;background:-webkit-gradient(linear,left top,right top,from(#141e30),to(#243b55))!important;background:linear-gradient(to right,#141e30,#243b55)!important;color:#fff!important}.sen-lib-white{color:#fff!important}.ui-common-lib-popupheader h2{font-size:17px!important;font-weight:500;text-transform:capitalize}.ui-common-lib-card{border:0!important}.ui-common-lib-card-footer{background:#cecece;padding-top:5px;padding-bottom:5px}.ui-common-confirm-btn{margin-right:15px;border-radius:50px;font-size:15px!important;border:none!important;width:13%}.ui-common-cancel-btn{border-radius:50px;background-color:#e53935;font-size:15px!important;border:none!important;width:13%}.ui-common-note{margin-top:14px;font-weight:600;font-size:13px;color:#f32900;text-transform:capitalize}.ui-common-info-icon{margin-right:8px;margin-left:12px;position:relative;top:5px}"]
                }] }
    ];
    /** @nocollapse */
    ConfirmDialogComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: FuseTranslationLoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    return ConfirmDialogComponent;
}());
export { ConfirmDialogComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1kaWFsb2cuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsWUFBWSxFQUFFLGVBQWUsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzFELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGO0lBV0UsZ0NBQW1CLFNBQStDLEVBQ3hELDZCQUEyRCxFQUN4QyxPQUFPLEVBQ0YsSUFBUztRQUh4QixjQUFTLEdBQVQsU0FBUyxDQUFzQztRQUN4RCxrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQ3hDLFlBQU8sR0FBUCxPQUFPLENBQUE7UUFDRixTQUFJLEdBQUosSUFBSSxDQUFLO1FBTDNDLGNBQVMsR0FBUyxFQUFFLENBQUM7UUFDckIsZ0JBQVcsR0FBWSxLQUFLLENBQUM7UUFLekIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9ELGdDQUFnQztRQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLDJDQUEyQyxFQUFDLElBQUksQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFRCx5Q0FBUSxHQUFSO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsRUFBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkQsSUFBRyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFDO1lBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDdEM7YUFBSTtZQUNILElBQUksZ0JBQWdCLEdBQUM7Z0JBQ25CLFdBQVcsRUFBQztvQkFDVixRQUFRLEVBQUMsbUNBQW1DO2lCQUM3QztnQkFDRCxRQUFRLEVBQUM7b0JBQ1AsUUFBUSxFQUFDLEVBQUU7aUJBQ1o7YUFDRixDQUFDO1lBQ0YsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztTQUNwQztRQUVELElBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXO1lBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUMsSUFBSSxDQUFDO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCwwQ0FBUyxHQUFUO1FBQ0UsZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFRCwwQ0FBUyxHQUFUO1FBQ0UsaUNBQWlDO1FBQ2pDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUM7O2dCQWxERixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsNDlEQUE4Qzs7aUJBRS9DOzs7O2dCQVBRLFlBQVk7Z0JBRVosNEJBQTRCO2dEQWNoQyxNQUFNLFNBQUMsU0FBUztnREFDaEIsTUFBTSxTQUFDLGVBQWU7O0lBcUMzQiw2QkFBQztDQUFBLEFBbkRELElBbURDO1NBOUNZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIEluamVjdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlXCI7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWNvbmZpcm0tZGlhbG9nJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29uZmlybS1kaWFsb2cuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbmZpcm1EaWFsb2dDb21wb25lbnQgIHtcclxuICB0aXRsZTogYW55O1xyXG4gIG1lc3NhZ2U6IGFueTtcclxuICBub3RlOiBhbnk7XHJcbiAgbW9kZWxEYXRhIDogYW55ID0ge307XHJcbiAgaW5jbHVkZU5vdGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBjb25zdHJ1Y3RvcihwdWJsaWMgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8Q29uZmlybURpYWxvZ0NvbXBvbmVudD4sXHJcbiAgICBwcml2YXRlIF9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlOiBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHJpdmF0ZSBlbmdsaXNoLFxyXG4gICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBhbnkpIHtcclxuICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gICAgLy8gVXBkYXRlIHZpZXcgd2l0aCBnaXZlbiB2YWx1ZXNcclxuICAgICBjb25zb2xlLmxvZyhcIj4+Pj4gQ29uZmlybS1kaWFsb2cgQ29uc3RydWN0b3IgcmVjZWl2ZWQgXCIsZGF0YSk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IG5nIG9uIGluaXQgdGhpcy5kYXRhIFwiLHRoaXMuZGF0YSk7XHJcbiAgICBpZih0aGlzLmRhdGEgJiYgdGhpcy5kYXRhLm1vZGVsRGF0YSl7XHJcbiAgICAgIHRoaXMubW9kZWxEYXRhID0gdGhpcy5kYXRhLm1vZGVsRGF0YTtcclxuICAgIH1lbHNle1xyXG4gICAgICBsZXQgZGVmYXVsdG1vZGVsRGF0YT17XHJcbiAgICAgICAgbWVzc2FnZURhdGE6e1xyXG4gICAgICAgICAgbWVzc3NhZ2U6XCJBcmUgeW91IHN1cmUgeW91IHdhbnQgdG8gZG8gdGhpcz9cIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgbm90ZURhdGE6e1xyXG4gICAgICAgICAgbWVzc3NhZ2U6XCJcIlxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgdGhpcy5tb2RlbERhdGEgPSBkZWZhdWx0bW9kZWxEYXRhO1xyXG4gICB9XHJcblxyXG4gICBpZih0aGlzLm1vZGVsRGF0YS5pbmNsdWRlTm90ZSlcclxuICAgICB0aGlzLmluY2x1ZGVOb3RlPXRydWU7XHJcbiAgICBjb25zb2xlLmxvZygnPj4+IHRoaXMubW9kZWxEYXRhICcsdGhpcy5tb2RlbERhdGEpO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gaW5jbHVkZU5vdGUgXCIsdGhpcy5pbmNsdWRlTm90ZSk7XHJcbiAgfVxyXG5cclxuICBvbkNvbmZpcm0oKTogdm9pZCB7XHJcbiAgICAvLyBDbG9zZSB0aGUgZGlhbG9nLCByZXR1cm4gdHJ1ZVxyXG4gICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UodHJ1ZSk7XHJcbiAgfVxyXG5cclxuICBvbkRpc21pc3MoKTogdm9pZCB7XHJcbiAgICAvLyBDbG9zZSB0aGUgZGlhbG9nLCByZXR1cm4gZmFsc2VcclxuICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKGZhbHNlKTtcclxuICB9XHJcbn0iXX0=