import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import * as i0 from "@angular/core";
var MessageService = /** @class */ (function () {
    function MessageService() {
        this.subject = new Subject();
        this.routing = new Subject();
        this.clickEventSource = new BehaviorSubject(0);
        this.clickEventMessage = this.clickEventSource.asObservable();
        this.modelCloseSource = new BehaviorSubject(0);
        this.modelCloseMessage = this.modelCloseSource.asObservable();
        this.enableMessage = new Subject();
        this.setDatasouce = new Subject();
        this.pageRoutingMsg = new Subject();
        this.triggerNotification = new Subject();
        this.tableHeaderUpdate = new Subject();
    }
    MessageService.prototype.sendButtonEnableMessage = function (message) {
        this.enableMessage.next({ data: message });
    };
    MessageService.prototype.getButtonEnableMessage = function () {
        return this.enableMessage.asObservable();
    };
    MessageService.prototype.sendRoutingMessage = function (data) {
        this.pageRoutingMsg.next({ pageData: data });
    };
    MessageService.prototype.getRoutingMessage = function () {
        return this.pageRoutingMsg.asObservable();
    };
    MessageService.prototype.sendTriggerNotification = function (data) {
        this.triggerNotification.next({ trigger: data });
    };
    MessageService.prototype.getTriggerNotification = function () {
        return this.triggerNotification.asObservable();
    };
    MessageService.prototype.sendTableHeaderUpdate = function (data) {
        this.tableHeaderUpdate.next({ data: data });
    };
    MessageService.prototype.getTableHeaderUpdate = function () {
        return this.tableHeaderUpdate.asObservable();
    };
    MessageService.prototype.sendMessage = function (message) {
        this.subject.next({ text: message });
    };
    MessageService.prototype.clearMessages = function () {
        this.subject.next();
    };
    MessageService.prototype.getMessage = function () {
        return this.subject.asObservable();
    };
    MessageService.prototype.sendRouting = function (data) {
        this.routing.next({ data: data });
    };
    MessageService.prototype.getRouting = function () {
        return this.routing.asObservable();
    };
    MessageService.prototype.sendClickEvent = function (message) {
        this.clickEventSource.next(message);
    };
    MessageService.prototype.sendModelCloseEvent = function (message) {
        this.modelCloseSource.next(message);
    };
    MessageService.prototype.isObject = function (source) {
        return !!source && source.constructor === Object;
    };
    MessageService.prototype.isArray = function (source) {
        return !!source && source.constructor === Array;
    };
    MessageService.prototype.sendDatasource = function (message) {
        this.setDatasouce.next({ text: message });
    };
    MessageService.prototype.getDatasource = function () {
        return this.setDatasouce.asObservable();
    };
    MessageService.decorators = [
        { type: Injectable, args: [{ providedIn: "root" },] }
    ];
    MessageService.ngInjectableDef = i0.defineInjectable({ factory: function MessageService_Factory() { return new MessageService(); }, token: MessageService, providedIn: "root" });
    return MessageService;
}());
export { MessageService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIl9zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFFdkQ7SUFBQTtRQUVVLFlBQU8sR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO1FBQzdCLFlBQU8sR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO1FBQzdCLHFCQUFnQixHQUFHLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xELHNCQUFpQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUVqRCxxQkFBZ0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUVsRCxzQkFBaUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFakQsa0JBQWEsR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO1FBQ25DLGlCQUFZLEdBQUUsSUFBSSxPQUFPLEVBQU8sQ0FBQztRQUNqQyxtQkFBYyxHQUFHLElBQUksT0FBTyxFQUFPLENBQUM7UUFDcEMsd0JBQW1CLEdBQUcsSUFBSSxPQUFPLEVBQU8sQ0FBQztRQXlCekMsc0JBQWlCLEdBQUcsSUFBSSxPQUFPLEVBQU8sQ0FBQztLQThDaEQ7SUFyRUMsZ0RBQXVCLEdBQXZCLFVBQXlCLE9BQWdCO1FBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFHLE9BQU8sRUFBQyxDQUFDLENBQUE7SUFDM0MsQ0FBQztJQUVELCtDQUFzQixHQUF0QjtRQUNFLE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUMzQyxDQUFDO0lBRUQsMkNBQWtCLEdBQWxCLFVBQW1CLElBQVU7UUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtJQUM1QyxDQUFDO0lBRUQsMENBQWlCLEdBQWpCO1FBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRCxnREFBdUIsR0FBdkIsVUFBd0IsSUFBVTtRQUNoQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUMsT0FBTyxFQUFFLElBQUksRUFBQyxDQUFDLENBQUE7SUFDaEQsQ0FBQztJQUNELCtDQUFzQixHQUF0QjtRQUNFLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ2pELENBQUM7SUFHRCw4Q0FBcUIsR0FBckIsVUFBdUIsSUFBWTtRQUNqQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELDZDQUFvQixHQUFwQjtRQUNFLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFDRCxvQ0FBVyxHQUFYLFVBQVksT0FBZTtRQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxzQ0FBYSxHQUFiO1FBQ0UsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsbUNBQVUsR0FBVjtRQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsb0NBQVcsR0FBWCxVQUFZLElBQVM7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBQ0QsbUNBQVUsR0FBVjtRQUNFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsdUNBQWMsR0FBZCxVQUFlLE9BQVk7UUFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsNENBQW1CLEdBQW5CLFVBQW9CLE9BQVk7UUFDOUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBQ0QsaUNBQVEsR0FBUixVQUFTLE1BQU07UUFDYixPQUFPLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxNQUFNLENBQUM7SUFDbkQsQ0FBQztJQUVELGdDQUFPLEdBQVAsVUFBUSxNQUFNO1FBQ1osT0FBTyxDQUFDLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxXQUFXLEtBQUssS0FBSyxDQUFDO0lBQ2xELENBQUM7SUFDRCx1Q0FBYyxHQUFkLFVBQWUsT0FBZTtRQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFDRCxzQ0FBYSxHQUFiO1FBQ0UsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzFDLENBQUM7O2dCQXBGRixVQUFVLFNBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFOzs7eUJBTGxDO0NBMEZDLEFBckZELElBcUZDO1NBcEZZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gXCJyeGpzL09ic2VydmFibGVcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSBcInJ4anMvQmVoYXZpb3JTdWJqZWN0XCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46IFwicm9vdFwiIH0pXHJcbmV4cG9ydCBjbGFzcyBNZXNzYWdlU2VydmljZSB7XHJcbiAgcHJpdmF0ZSBzdWJqZWN0ID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHByaXZhdGUgcm91dGluZyA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuICBwcml2YXRlIGNsaWNrRXZlbnRTb3VyY2UgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KDApO1xyXG4gIGNsaWNrRXZlbnRNZXNzYWdlID0gdGhpcy5jbGlja0V2ZW50U291cmNlLmFzT2JzZXJ2YWJsZSgpO1xyXG5cclxuICBwcml2YXRlIG1vZGVsQ2xvc2VTb3VyY2UgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KDApO1xyXG5cclxuICBtb2RlbENsb3NlTWVzc2FnZSA9IHRoaXMubW9kZWxDbG9zZVNvdXJjZS5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgcHJpdmF0ZSBlbmFibGVNZXNzYWdlID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHByaXZhdGUgc2V0RGF0YXNvdWNlPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcbiAgcHJpdmF0ZSBwYWdlUm91dGluZ01zZyA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuICBwcml2YXRlIHRyaWdnZXJOb3RpZmljYXRpb24gPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcblxyXG4gIHNlbmRCdXR0b25FbmFibGVNZXNzYWdlIChtZXNzYWdlIDogc3RyaW5nKSB7XHJcbiAgICB0aGlzLmVuYWJsZU1lc3NhZ2UubmV4dCh7ZGF0YSA6IG1lc3NhZ2V9KVxyXG4gIH1cclxuXHJcbiAgZ2V0QnV0dG9uRW5hYmxlTWVzc2FnZSAoKSA6IE9ic2VydmFibGU8YW55PntcclxuICAgIHJldHVybiB0aGlzLmVuYWJsZU1lc3NhZ2UuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZW5kUm91dGluZ01lc3NhZ2UoZGF0YSA6IGFueSl7XHJcbiAgICB0aGlzLnBhZ2VSb3V0aW5nTXNnLm5leHQoe3BhZ2VEYXRhOiBkYXRhfSlcclxuICB9XHJcblxyXG4gIGdldFJvdXRpbmdNZXNzYWdlICgpIDogT2JzZXJ2YWJsZTxhbnk+e1xyXG4gICAgcmV0dXJuIHRoaXMucGFnZVJvdXRpbmdNc2cuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZW5kVHJpZ2dlck5vdGlmaWNhdGlvbihkYXRhIDogYW55KXtcclxuICAgIHRoaXMudHJpZ2dlck5vdGlmaWNhdGlvbi5uZXh0KHt0cmlnZ2VyOiBkYXRhfSlcclxuICB9XHJcbiAgZ2V0VHJpZ2dlck5vdGlmaWNhdGlvbiAoKSA6IE9ic2VydmFibGU8YW55PntcclxuICAgIHJldHVybiB0aGlzLnRyaWdnZXJOb3RpZmljYXRpb24uYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHRhYmxlSGVhZGVyVXBkYXRlID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHNlbmRUYWJsZUhlYWRlclVwZGF0ZSAoZGF0YTogc3RyaW5nKXtcclxuICAgIHRoaXMudGFibGVIZWFkZXJVcGRhdGUubmV4dCh7ZGF0YTogZGF0YX0pO1xyXG4gIH1cclxuICBnZXRUYWJsZUhlYWRlclVwZGF0ZSAoKSA6IE9ic2VydmFibGU8YW55PntcclxuICAgIHJldHVybiB0aGlzLnRhYmxlSGVhZGVyVXBkYXRlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuICBzZW5kTWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuc3ViamVjdC5uZXh0KHsgdGV4dDogbWVzc2FnZSB9KTtcclxuICB9XHJcbiAgXHJcbiAgY2xlYXJNZXNzYWdlcygpIHtcclxuICAgIHRoaXMuc3ViamVjdC5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICBnZXRNZXNzYWdlKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgc2VuZFJvdXRpbmcoZGF0YTogYW55KSB7XHJcbiAgICB0aGlzLnJvdXRpbmcubmV4dCh7IGRhdGE6IGRhdGEgfSk7XHJcbiAgfVxyXG4gIGdldFJvdXRpbmcoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiB0aGlzLnJvdXRpbmcuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZW5kQ2xpY2tFdmVudChtZXNzYWdlOiBhbnkpIHtcclxuICAgIHRoaXMuY2xpY2tFdmVudFNvdXJjZS5uZXh0KG1lc3NhZ2UpO1xyXG4gIH1cclxuXHJcbiAgc2VuZE1vZGVsQ2xvc2VFdmVudChtZXNzYWdlOiBhbnkpIHtcclxuICAgIHRoaXMubW9kZWxDbG9zZVNvdXJjZS5uZXh0KG1lc3NhZ2UpO1xyXG4gIH1cclxuICBpc09iamVjdChzb3VyY2UpIHtcclxuICAgIHJldHVybiAhIXNvdXJjZSAmJiBzb3VyY2UuY29uc3RydWN0b3IgPT09IE9iamVjdDtcclxuICB9XHJcblxyXG4gIGlzQXJyYXkoc291cmNlKSB7XHJcbiAgICByZXR1cm4gISFzb3VyY2UgJiYgc291cmNlLmNvbnN0cnVjdG9yID09PSBBcnJheTtcclxuICB9XHJcbiAgc2VuZERhdGFzb3VyY2UobWVzc2FnZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnNldERhdGFzb3VjZS5uZXh0KHsgdGV4dDogbWVzc2FnZSB9KTtcclxuICB9XHJcbiAgZ2V0RGF0YXNvdXJjZSgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc2V0RGF0YXNvdWNlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxufVxyXG4iXX0=