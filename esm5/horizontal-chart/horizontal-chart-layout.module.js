import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HorizontalChartLayoutComponent } from './horizontal-chart-layout.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { FuseWidgetModule } from '../@fuse/components/widget/widget.module';
import { FuseProgressBarModule } from '../@fuse/components/progress-bar/progress-bar.module';
import { FuseThemeOptionsModule } from '../@fuse/components/theme-options/theme-options.module';
import { FuseSharedModule } from '../@fuse/shared.module';
import { TranslateModule } from '@ngx-translate/core';
var HorizontalChartlayoutModule = /** @class */ (function () {
    function HorizontalChartlayoutModule() {
    }
    HorizontalChartlayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        HorizontalChartLayoutComponent
                    ],
                    imports: [
                        RouterModule,
                        NgxChartsModule,
                        ChartsModule,
                        FuseWidgetModule,
                        FuseSharedModule,
                        TranslateModule.forRoot(),
                        FuseProgressBarModule, FuseThemeOptionsModule
                    ],
                    exports: [
                        HorizontalChartLayoutComponent
                    ]
                },] }
    ];
    return HorizontalChartlayoutModule;
}());
export { HorizontalChartlayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImhvcml6b250YWwtY2hhcnQvaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSw4QkFBOEIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sWUFBWSxDQUFBO0FBQ3pDLE9BQU8sRUFBQyxnQkFBZ0IsRUFBQyxNQUFNLDBDQUEwQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNEQUFzRCxDQUFDO0FBQzdGLE9BQU8sRUFBQyxzQkFBc0IsRUFBQyxNQUFNLHdEQUF3RCxDQUFBO0FBQzdGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzFELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUV0RDtJQUFBO0lBbUJBLENBQUM7O2dCQW5CQSxRQUFRLFNBQUM7b0JBQ04sWUFBWSxFQUFFO3dCQUNWLDhCQUE4QjtxQkFDakM7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLFlBQVk7d0JBQ1osZUFBZTt3QkFDZixZQUFZO3dCQUNaLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixlQUFlLENBQUMsT0FBTyxFQUFFO3dCQUN6QixxQkFBcUIsRUFBRSxzQkFBc0I7cUJBQ2hEO29CQUNELE9BQU8sRUFBRTt3QkFDTCw4QkFBOEI7cUJBQ2pDO2lCQUNKOztJQUdELGtDQUFDO0NBQUEsQUFuQkQsSUFtQkM7U0FGWSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgSG9yaXpvbnRhbENoYXJ0TGF5b3V0Q29tcG9uZW50IH0gZnJvbSAnLi9ob3Jpem9udGFsLWNoYXJ0LWxheW91dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBOZ3hDaGFydHNNb2R1bGUgfSBmcm9tICdAc3dpbWxhbmUvbmd4LWNoYXJ0cyc7XHJcbmltcG9ydCB7IENoYXJ0c01vZHVsZSB9IGZyb20gJ25nMi1jaGFydHMnXHJcbmltcG9ydCB7RnVzZVdpZGdldE1vZHVsZX0gZnJvbSAnLi4vQGZ1c2UvY29tcG9uZW50cy93aWRnZXQvd2lkZ2V0Lm1vZHVsZSc7XHJcbmltcG9ydCB7IEZ1c2VQcm9ncmVzc0Jhck1vZHVsZSB9IGZyb20gJy4uL0BmdXNlL2NvbXBvbmVudHMvcHJvZ3Jlc3MtYmFyL3Byb2dyZXNzLWJhci5tb2R1bGUnO1xyXG5pbXBvcnQge0Z1c2VUaGVtZU9wdGlvbnNNb2R1bGV9IGZyb20gJy4uL0BmdXNlL2NvbXBvbmVudHMvdGhlbWUtb3B0aW9ucy90aGVtZS1vcHRpb25zLm1vZHVsZSdcclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gJy4uL0BmdXNlL3NoYXJlZC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBIb3Jpem9udGFsQ2hhcnRMYXlvdXRDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgUm91dGVyTW9kdWxlLFxyXG4gICAgICAgIE5neENoYXJ0c01vZHVsZSxcclxuICAgICAgICBDaGFydHNNb2R1bGUsXHJcbiAgICAgICAgRnVzZVdpZGdldE1vZHVsZSxcclxuICAgICAgICBGdXNlU2hhcmVkTW9kdWxlLFxyXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KCksXHJcbiAgICAgICAgRnVzZVByb2dyZXNzQmFyTW9kdWxlLCBGdXNlVGhlbWVPcHRpb25zTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIEhvcml6b250YWxDaGFydExheW91dENvbXBvbmVudFxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9yaXpvbnRhbENoYXJ0bGF5b3V0TW9kdWxlIHtcclxuXHJcbn0iXX0=