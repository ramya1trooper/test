import { Component, Input, ViewEncapsulation, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
var HorizontalChartLayoutComponent = /** @class */ (function () {
    function HorizontalChartLayoutComponent(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.gradient = false;
        this.showXAxis = true;
        this.showXAxisLabel = true;
        this.showYAxisLabel = true;
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    HorizontalChartLayoutComponent.prototype.ngOnInit = function () { };
    HorizontalChartLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "horizontal-chart-layout",
                    template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\">\r\n\r\n    <div fxLayout=\"row wrap\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" class=\"p-10  mat-body-2\">\r\n      <div class=\"inner\" *ngIf=\"result && result.length > 0\">\r\n        <ngx-charts-bar-horizontal [view]=\"view\" [scheme]=\"color\" [results]=\"result\" [gradient]=\"gradient\"\r\n          [legend]=\"true\" [legendPosition]=\"'below'\" [showXAxisLabel]=\"showXAxisLabel\" [showYAxisLabel]=\"showYAxisLabel\"\r\n          [xAxisLabel]=\"Users\" [yAxisLabel]=\"Controls\" [xAxis]=\"showXAxis\" [barPadding]=\"12\" [showDataLabel]=\"true\">\r\n        </ngx-charts-bar-horizontal>\r\n      </div>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"result.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{ label.nodata | translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                    animations: fuseAnimations,
                    encapsulation: ViewEncapsulation.None,
                    styles: ["fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.inner{display:block;width:100%;text-align:center;margin-top:19px;margin-left:0!important}.inner img{width:auto;max-width:100%}ngx-charts-legend.chart-legend{width:auto;clear:both;display:block;margin:0 auto}ngx-charts-legend.chart-legend>div{display:block;width:500px!important}ngx-charts-legend.chart-legend .legend-wrap{min-width:200px;text-align:left;margin:0 auto;float:none;padding-right:33px}ngx-charts-legend.chart-legend .legend-title{display:none}ngx-charts-legend.chart-legend .legend-labels{display:inline;text-align:left;margin:0 auto;background:0 0;float:none;padding:0;white-space:inherit}.h-400{min-height:434px!important}"]
                }] }
    ];
    /** @nocollapse */
    HorizontalChartLayoutComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    HorizontalChartLayoutComponent.propDecorators = {
        result: [{ type: Input }],
        color: [{ type: Input }],
        view: [{ type: Input }],
        label: [{ type: Input }]
    };
    return HorizontalChartLayoutComponent;
}());
export { HorizontalChartLayoutComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImhvcml6b250YWwtY2hhcnQvaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBRVQsS0FBSyxFQUNMLGlCQUFpQixFQUNqQixNQUFNLEVBQ1AsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRXJELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLGtEQUFrRDtBQUVsRDtJQW1CRSx3Q0FDVSw2QkFBMkQsRUFDeEMsT0FBTztRQUQxQixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQ3hDLFlBQU8sR0FBUCxPQUFPLENBQUE7UUFUcEMsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBUXBCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsaURBQVEsR0FBUixjQUFZLENBQUM7O2dCQTFCZCxTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtvQkFDbkMsODBDQUF1RDtvQkFFdkQsVUFBVSxFQUFFLGNBQWM7b0JBQzFCLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDdEM7Ozs7Z0JBVFEsNEJBQTRCO2dEQXdCaEMsTUFBTSxTQUFDLFNBQVM7Ozt5QkFibEIsS0FBSzt3QkFDTCxLQUFLO3VCQUNMLEtBQUs7d0JBQ0wsS0FBSzs7SUFnQlIscUNBQUM7Q0FBQSxBQTNCRCxJQTJCQztTQXBCWSw4QkFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgSW5wdXQsXHJcbiAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgSW5qZWN0XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgZnVzZUFuaW1hdGlvbnMgfSBmcm9tIFwiLi4vQGZ1c2UvYW5pbWF0aW9uc1wiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJob3Jpem9udGFsLWNoYXJ0LWxheW91dFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgYW5pbWF0aW9uczogZnVzZUFuaW1hdGlvbnMsXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSG9yaXpvbnRhbENoYXJ0TGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSByZXN1bHQ6IGFueTtcclxuICBASW5wdXQoKSBjb2xvcjogYW55O1xyXG4gIEBJbnB1dCgpIHZpZXc6IGFueTtcclxuICBASW5wdXQoKSBsYWJlbDogYW55O1xyXG4gIGdyYWRpZW50ID0gZmFsc2U7XHJcbiAgc2hvd1hBeGlzID0gdHJ1ZTtcclxuICBzaG93WEF4aXNMYWJlbCA9IHRydWU7XHJcbiAgc2hvd1lBeGlzTGFiZWwgPSB0cnVlO1xyXG4gIHRyYW5zbGF0ZTogYW55O1xyXG4gIFVzZXJzO1xyXG4gIENvbnRyb2xzO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZTogRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSxcclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHByaXZhdGUgZW5nbGlzaFxyXG4gICkge1xyXG4gICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7fVxyXG59XHJcbiJdfQ==