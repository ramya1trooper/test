import * as tslib_1 from "tslib";
import { SnackBarService } from "./../shared/snackbar.service";
import { AwsS3UploadService } from "./../shared/aws-s3-upload.service";
import { Component, Inject, ViewChild, ElementRef, Input, ChangeDetectorRef, } from "@angular/core";
import { MatAutocomplete, } from "@angular/material/autocomplete";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import { FormBuilder, FormControl, FormGroup, Validators, } from "@angular/forms";
import { ContentService } from "../content/content.service";
import * as _ from "lodash";
import { MessageService } from "../_services/index";
import * as FileSaver from "file-saver";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { MatDialogRef } from "@angular/material/dialog";
import * as moment_ from "moment";
import { LoaderService } from "../loader.service";
var moment = moment_;
var FormLayoutComponent = /** @class */ (function () {
    function FormLayoutComponent(_fuseTranslationLoaderService, contentService, formBuilder, messageService, changeDetector, matDialogRef, AwsS3UploadService, ref, snackBarService, loaderService, english) {
        var _this = this;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.contentService = contentService;
        this.formBuilder = formBuilder;
        this.messageService = messageService;
        this.changeDetector = changeDetector;
        this.matDialogRef = matDialogRef;
        this.AwsS3UploadService = AwsS3UploadService;
        this.ref = ref;
        this.snackBarService = snackBarService;
        this.loaderService = loaderService;
        this.english = english;
        this.enableSelectOption = true;
        this.enableNewTableLayout = false;
        this.unsubscribe = new Subject();
        this.unsubscribeClick = new Subject();
        this.buttonsConfigData = {};
        this.enableSchedule = false;
        this.removable = true;
        this.selectedChip = {};
        this.panelOpenState = true;
        this.enableImportData = false;
        this.errorShow = {};
        this.enableSelectValueView = false;
        this.enableOptionFields = false;
        this.selectedChips = {};
        this.selectedChipKeyList = {};
        this.enableSelectable = true;
        this.enableRemovable = true;
        this.image = {};
        this.scheduleField = {};
        this.enableRepeatData = false;
        this.recipient = [];
        this.isNameAvailable = true;
        this.charlimitcheck = true;
        this.chipLength = 0;
        this.selectedchipLength = 0;
        this.dateFields = {};
        this.minDate = new Date();
        this.minEndDate = new Date();
        this.enabledateFields = false;
        this.dateFormatcheck = true;
        //for cron schedule expression by Raj
        this.secondsList = [];
        this.secondsLastDigits = [];
        this.hourListView = [];
        this.hourstwothree = [];
        this.days = [];
        this.month = [];
        this.monthsCount = [];
        this.dateCount = [];
        this.numOfdays = [];
        this.numOfmonths = [];
        this.cropExpressionSelection = {};
        this._fuseTranslationLoaderService.loadTranslations(english);
        this.defaultDatasource = localStorage.getItem("datasource");
        this.defaultDatasourceName = localStorage.getItem("datasourceName");
        this.realm = localStorage.getItem("realm");
        this.queryParams = {
            offset: 0,
            limit: 10,
            datasource: this.defaultDatasource,
            realm: this.realm
        };
        this.messageService.clickEventMessage
            .pipe(takeUntil(this.unsubscribeClick))
            .subscribe(function (data) {
            console.log(_this.formValues, ">>>>>>FORM VALUESSS");
            var tempData = localStorage.getItem("currentInput");
            console.log(tempData, ">>>>tempData");
            _this.inputData = !_.isEmpty(JSON.parse(tempData))
                ? JSON.parse(tempData)
                : {};
            _this.showData(_this.inputData);
        });
        this.enableSelectValueView = false;
        this.messageService
            .getMessage()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(function (message) {
            console.log(message, "....MESSAGE");
            _this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            // if(this.currentConfigData && this.currentConfigData.listView &&  this.currentConfigData.listView.enableTableLayout){
            _this.enableTableLayout = false;
            _this.enableNewTableLayout = false;
            _this.ngOnInit();
            // }
        });
        this.importControl = new ImportValue({});
        this.messageService.modelCloseMessage // whatif - new
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(function (data) {
            console.log(data, ">>>>data Refresh from fromdata to tabledata");
            _this.data = data;
            if (_this.data) {
                _this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
                _this.ngOnInit();
            }
        });
    }
    FormLayoutComponent.prototype.ngOnInit = function () {
        this.getRulesWidth = null;
        this.hiddenFixedLayout = null;
        console.log(this.formValues, ".....formValues");
        console.log(this.importData, "importData????");
        this.selectedChips = {};
        this.selectedChipKeyList = {};
        //get values for cron expression dropdown
        this.viewOfseconds();
        var initialObj = {
            datasourceId: this.defaultDatasource,
            datasourceName: this.defaultDatasourceName,
            limit: 10,
            offset: 0,
        };
        this.userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        if (this.userData) {
            this.recipient.push(this.userData.email);
        }
        this.buttonsConfigData = localStorage.getItem("buttonConfig")
            ? JSON.parse(localStorage.getItem("buttonConfig"))
            : {};
        this.formData = this.data;
        var tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(tempData)
            ? JSON.parse(tempData)
            : {
                datasourceId: this.defaultDatasource,
                datasourceName: this.defaultDatasourceName,
                limit: 10,
                offset: 0,
            };
        this.inputData = tslib_1.__assign({}, this.inputData, initialObj);
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.currentData = this.fromRouting ? this.currentConfigData['pageRoutingView'] : this.currentConfigData["listView"];
        this.stepperData = this.currentData.createModelData;
        this.classDiv =
            this.currentData && this.currentData.beforeFromDivElementstyle
                ? this.currentData.beforeFromDivElementstyle
                : null;
        // debugger;
        this.initialHeight =
            this.currentData && this.currentData.height
                ? this.currentData.height
                : null;
        this.getRulesWidth =
            this.formValues && this.formValues.widthReduce
                ? this.formValues.widthReduce
                : null;
        this.hiddenFixedLayout =
            this.formValues && this.formValues.hiddenFixedLayout
                ? this.formValues.hiddenFixedLayout
                : null;
        this.dynamicFormHeight =
            this.formValues && this.formValues.dynamicFormHeight
                ? this.formValues.dynamicFormHeight
                : null;
        // this.hiddenFixedLayout = this.currentData.hiddenFixedLayout;
        console.log(this.classDiv);
        if (this.formValues) {
            this.enableTextFields = this.formValues.textFields ? true : false;
            this.enableSelectFields = this.formValues.selectFields ? true : false;
            this.enableOptionFields = this.formValues.chooseOneFields ? true : false;
            // this.enableChipFields = (this.chipListFields && this.chipListFields.length) ? true : false;
            console.log(this.formValues.chipFields, "this.formValues.chipFields");
            this.enableChipFields = this.formValues.chipFields ? true : false;
            this.enableTabs = this.formValues.tabData ? true : false;
            this.enableMultiselectFields = this.formValues.multiSelectFields
                ? true
                : false;
            this.enabledateFields = this.formValues.dateFields ? true : false;
        }
        this.enableTableLayout = false;
        this.enableNewTableLayout = false;
        this.submitted = false;
        this.viewData = {};
        var tempObj = {};
        if (this.importData) {
            this.enableImportData = true;
        }
        if (this.enableTextFields) {
            _.forEach(this.formValues.textFields, function (item) {
                if (item.inputType == "email") {
                    tempObj[item.name] = new FormControl("", Validators.email);
                }
                else {
                    tempObj[item.name] = new FormControl("");
                }
            });
        }
        if (this.enabledateFields) {
            var self = this;
            _.forEach(this.formValues.dateFields, function (item) {
                tempObj[item.name] = new FormControl(new Date());
            });
            // this.dateFields = this.formValues.dateFields;
        }
        if (this.formValues && this.formValues.autoCompleteFields) {
            _.forEach(this.formValues.autoCompleteFields, function (item) {
                tempObj[item.name] = new FormControl({
                    value: "",
                    disabled: item.disable,
                });
            });
        }
        if (this.enableSelectFields) {
            _.forEach(this.formValues.selectFields, function (item) {
                console.log(item);
                tempObj[item.name] = new FormControl("");
            });
        }
        if (this.enableMultiselectFields) {
            _.forEach(this.formValues.multiSelectFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        if (this.enableOptionFields) {
            // this.formValues.chooseOneFields.forEach(item => {
            //   tempObj[item.name] = new FormControl("");
            //   this.inputData[item.name] = item.fields[0].value;
            //   console.log(this.inputData, "///////");
            //   console.log(item, "///////item");
            // });
            var self = this;
            _.forEach(this.formValues.chooseOneFields, function (item) {
                tempObj[item.name] = new FormControl("");
                self.inputData[item.name] = item.fields[0].value;
                console.log(self.ChipLimit, "chipLimit>>>");
                console.log(self.inputData, "///////");
                console.log(item, "///////item");
            });
            if (this.formValues.isSetDefaultValue) {
                this.inputData[this.formValues.chooseOneFields[0].defaultKey] = this.formValues.chooseOneFields[0].defaultValue;
            }
        }
        console.log(this.enableChipFields, "enableChipFields");
        if (this.enableChipFields) {
            console.log(this.enableChipFields, "enableChipFields");
            var self = this;
            _.forEach(this.formValues.chipFields, function (item) {
                console.log(item, "selfchiplimit");
                console.log(item.chipCondition, "data>>>>>>>>>>>");
                tempObj[item.name] = new FormControl([""]);
                if (item.chipCondition) {
                    self.selectedChips[item.name] = [];
                    var temp = item.chipCondition.variable;
                    if (item.chipCondition.data &&
                        self.inputData &&
                        self.inputData[temp] &&
                        item.chipCondition.data[self.inputData[temp]]) {
                        self.ChipLimit =
                            item.chipCondition.data[self.inputData[temp]]["limit"];
                        self.ChipOperator =
                            item.chipCondition.data[self.inputData[temp]]["operator"];
                        self.ChipLimit =
                            item.chipCondition.data[self.inputData[temp]]["limit"];
                        self.ChipOperator =
                            item.chipCondition.data[self.inputData[temp]]["operator"];
                    }
                }
                if (item.onLoadFunction) {
                    // chipfields OnLoad function added when implement run reports
                    var apiUrl = item.onLoadFunction.apiUrl;
                    var responseName_1 = item.onLoadFunction.response;
                    var requestData_1 = item.onLoadFunction.requestData;
                    var query_1 = { realm: self.realm };
                    //console.log(self.inputData, ">>>>>>>")
                    //debugger;
                    _.forEach(requestData_1, function (requestItem) {
                        if (requestItem.isDefault) {
                            query_1[requestItem.name] = requestItem.value;
                        }
                        else {
                            query_1[requestItem.name] = self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query_1, apiUrl).subscribe(function (data) {
                        item.data = data.response[responseName_1];
                        var validateData = item.data[0];
                        var isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                        if (!isArrayOfJSON) {
                            var tempList_1 = [];
                            if (item.onLoadFunction.subResponse) {
                                item.data = item.data[item.onLoadFunction.subResponse];
                                var keys = _.keys(item.data);
                                _.forEach(keys, function (itemKey) {
                                    tempList_1.push({
                                        name: item.data[itemKey][requestData_1["key"]],
                                        _id: itemKey,
                                    });
                                });
                            }
                            else {
                                _.forEach(item.data, function (item) {
                                    tempList_1.push({ name: item, _id: item });
                                });
                            }
                            item.data = tempList_1;
                        }
                    });
                }
                // self.ChipLimit[item.name] =
                //   item.chipCondition.data[
                //     self.inputData[]
                //   ];
            });
        }
        console.log(this.selectedChips, ">>>>>>>>>> selectedchips");
        // this.selectedChips = this.selectedChips;
        if (this.formValues && this.formValues.scheduleFields) {
            var item = this.formValues.scheduleFields;
            Object.keys(item).forEach(function (value) {
                tempObj[value] = new FormControl([""]);
            });
            // _.forEach(this.formValues.scheduleFields, function(item) {
            // tempObj[item.name] = new FormControl(['']);
            // tempObj[item.time] = new FormControl(['']);
            // tempObj[item.meridian] = new FormControl(['']);
            // tempObj[item.repeatType] = new FormControl(['']);
            // tempObj[item.endDate] = new FormControl(['']);
            // tempObj[item.endDateTime] = new FormControl([''])
            // })
            this.scheduleField = this.formValues.scheduleFields;
        }
        if (this.formValues && this.formValues.formArrayValues) {
            tempObj[this.formValues.formFieldName] = this.formBuilder.array([
                this.createCondition(),
            ]);
        }
        this.inputGroup = new FormGroup(tempObj);
        this.onLoad();
        if (this.enableTabs) {
            this.changeTab(0);
        }
        if (this.formValues && this.formValues.tableData) {
            this.tableList = this.inputData[this.formValues.tableKey];
            if (this.tableList && this.tableList.length) {
                this.currentTableLoad = this.formValues;
                this.currentTableLoad["response"] = this.tableList;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            else if (this.formValues.isToggleBased) {
                var tempObj_1 = localStorage.getItem("selectedToggleDetail");
                var selectedToggleData = JSON.parse(tempObj_1);
                console.log(selectedToggleData, "....selectedToggleData");
                this.tableList = this.currentData.formData[0].tableData;
                var tempTable = _.filter(this.tableList, {
                    tableId: selectedToggleData.toggleId,
                });
                console.log(tempTable, "........................tempTable");
                this.formValues.tableData = tempTable;
                this.currentTableLoad = this.formValues;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            else if (this.formValues.isdyamicData) {
                // added additional report implementation
                // this.currentConfigData = JSON.parse(
                //   localStorage.getItem("currentConfigData")
                // );
                // this.currentData = this.currentConfigData["view"];
                var tempObj_2 = localStorage.getItem("CurrentReportData");
                var Reportitem = JSON.parse(tempObj_2);
                console.log(Reportitem, "....CurrentReportData");
                var selectedReport = eval(this.formValues.condition);
                this.tableList = this.formValues.tableData;
                var tempTable = _.filter(this.tableList, {
                    tableId: selectedReport,
                });
                this.formValues.tableData = tempTable;
                // this.selectedmodelId = this.inputData._id;
                this.currentTableLoad = this.formValues;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            else if (this.formValues.enableTableLayout) {
                this.tableList = this.formValues.tableData;
                this.currentTableLoad = this.formValues;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            // let currentTabTable = this.formValues;
            // this.currentTableLoad = currentTabTable;
            // console.log(this.tableList, "<<<<<<<tableList");
        }
    };
    FormLayoutComponent.prototype.createCondition = function () {
        var formArrayValue = this.formValues.formArrayValues;
        var tempObj = {};
        if (formArrayValue.selectFields) {
            _.forEach(formArrayValue.selectFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        if (formArrayValue.autoCompleteFields) {
            _.forEach(formArrayValue.autoCompleteFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        return this.formBuilder.group(tempObj);
    };
    FormLayoutComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe.next();
        this.unsubscribeClick.next();
    };
    FormLayoutComponent.prototype.addNewFormArray = function (formFieldName) {
        // this.inputData[formFieldName] = this.inputGroup.get(formFieldName) as FormArray;
        // if (this.inputData[formFieldName].controls.length == 2) {
        // 	this.isDisabled = true;
        // }
        // this.inputData[formFieldName].push(this.createCondition());
        var control = this.inputGroup.controls[formFieldName];
        control.push(this.createCondition());
    };
    FormLayoutComponent.prototype.onLoad = function () {
        var self = this;
        console.log(".>.... self.inputData ", this.inputData);
        if (this.enableSelectFields) {
            _.forEach(this.formValues.selectFields, function (item) {
                if (item.onLoadFunction) {
                    var apiUrl = item.onLoadFunction.apiUrl;
                    var responseName_2 = item.onLoadFunction.response;
                    var requestData = item.onLoadFunction.requestData;
                    var query_2 = { realm: self.realm };
                    _.forEach(requestData, function (requestItem) {
                        if (requestItem.isDefault) {
                            query_2[requestItem.name] = requestItem.value;
                        }
                        else {
                            query_2[requestItem.name] = self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query_2, apiUrl).subscribe(function (data) {
                        // if(item.isFilter)
                        // {
                        //   let reponseData= data.response[responseName] as object[];
                        //   var tempData=reponseData.filter(function(val)
                        //   {
                        //     return val[item.filterKey]==item.filtervalue;
                        //   })
                        //   item.data=tempData;
                        // }
                        item.data = data.response[responseName_2];
                        // temp.selectedDataField = item.data[0].name;
                        // temp.dataId = item.data[0]._id;
                        // temp.getChipList(temp.dataId)
                        //console.log(">>>>>>>>>>>>>>>>> onLoad seletfield  ",item.name);
                        console.log(">>>>>> OnLoad data", data);
                        if (item.setDefaultDs) {
                            self.inputData[item.name] = self.inputData.datasourceName;
                            self.inputGroup.controls[item.name].setValue(self.inputData.datasourceId);
                        }
                        if (item.setDefaultRuleset && ((self.onLoadData && !self.onLoadData.savedData) || !self.onLoadData)) {
                            _.forEach(item.data, function (subitem) {
                                if (subitem.defaultRuleset) {
                                    var id = subitem._id;
                                    self.inputGroup.controls[item.name].setValue(id);
                                    self.inputData[item.name + "Id"] = subitem._id;
                                    self.inputData[item.name + "Name"] = subitem.name;
                                }
                            });
                        }
                        console.log(">>>>>>>>>>>> self.inputData OnLoad ", self.inputData);
                        localStorage.setItem("currentInput", JSON.stringify(self.inputData));
                    });
                }
            });
        }
        if (this.enableMultiselectFields) {
            _.forEach(this.formValues.multiSelectFields, function (item) {
                if (item.onLoadFunction) {
                    var apiUrl = item.onLoadFunction.apiUrl;
                    var responseName_3 = item.onLoadFunction.response;
                    var query = {
                        realm: self.realm
                    };
                    self.contentService.getAllReponse(query, apiUrl).subscribe(function (data) {
                        item.data = data.response[responseName_3];
                    });
                }
            });
        }
        if (this.enableChipFields) {
            console.log(this.enableChipFields, "enableChipFields");
            var self = this;
            _.forEach(this.formValues.chipFields, function (item) {
                if (item.onLoadFunction) {
                    // chipfields OnLoad function added when implement run reports
                    var apiUrl_1 = item.onLoadFunction.apiUrl;
                    var responseName_4 = item.onLoadFunction.response;
                    var requestData_2 = item.onLoadFunction.requestData;
                    var query_3 = { realm: self.realm };
                    console.log(self.inputData, ">>>>>>>");
                    setTimeout(function () {
                        _.forEach(requestData_2, function (requestItem) {
                            if (requestItem.isDefault) {
                                query_3[requestItem.name] = requestItem.value;
                            }
                            else if (requestItem.subKey) {
                                if (requestItem.isMap && self.inputData[requestItem.value]) {
                                    // query[requestItem.name] = self.inputData[requestItem.value] ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey) : []
                                    // query[requestItem.name] = requestItem.isConvertToString ? JSON.stringify(query[requestItem.name]) : query[requestItem.name];
                                    var t1 = self.inputData[requestItem.value]
                                        ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey)
                                        : [];
                                    if (t1.length)
                                        query_3[requestItem.name] =
                                            requestItem.isConvertToString && typeof t1 !== "string"
                                                ? JSON.stringify(t1)
                                                : t1;
                                }
                                else if (self.inputData[requestItem.value]) {
                                    query_3[requestItem.name] = self.inputData[requestItem.value]
                                        ? self.inputData[requestItem.value][requestItem.subKey]
                                        : [];
                                    query_3[requestItem.name] = requestItem.isConvertToString
                                        ? JSON.stringify(query_3[requestItem.name])
                                        : query_3[requestItem.name];
                                }
                            }
                            else {
                                query_3[requestItem.name] = self.inputData[requestItem.value]
                                    ? self.inputData[requestItem.value]
                                    : [];
                            }
                        });
                        self.contentService
                            .getAllReponse(query_3, apiUrl_1)
                            .subscribe(function (data) {
                            item.data = data.response[responseName_4];
                            var validateData = item.data[0];
                            var isArrayOfJSON = _.isPlainObject(validateData)
                                ? true
                                : false;
                            if (!isArrayOfJSON) {
                                var tempList_2 = [];
                                if (item.onLoadFunction.subResponse) {
                                    item.data = item.data[item.onLoadFunction.subResponse];
                                    var keys = _.keys(item.data);
                                    _.forEach(keys, function (itemKey) {
                                        tempList_2.push({
                                            name: item.data[itemKey][requestData_2["key"]],
                                            _id: itemKey,
                                        });
                                    });
                                }
                                else {
                                    _.forEach(item.data, function (item) {
                                        tempList_2.push({ name: item, _id: item });
                                    });
                                }
                                item.data = tempList_2;
                            }
                        });
                    }, 2000);
                }
                // self.ChipLimit[item.name] =
                //   item.chipCondition.data[
                //     self.inputData[]
                //   ];
            });
        }
        if (this.onLoadData && this.onLoadData.savedData) {
            this.loadSavedData();
        }
    };
    // getOptionText(option) {
    //   // if (condition) {
    //     console.log(option,)
    //   // }
    //   return option.name;
    // }
    FormLayoutComponent.prototype.addChipItem = function (event) { };
    FormLayoutComponent.prototype.validateDate = function (type, event, dateField) {
        this.dateFormatcheck = true;
        if (event.value) {
            var disbalePreviousDate = dateField.validatewithnextdate ? true : false;
            if (disbalePreviousDate)
                this.minEndDate = event.value.add(1, "days")._d;
        }
        else {
            this.dateFormatcheck = false;
        }
    };
    FormLayoutComponent.prototype.onChipSearch = function (searchValue, selectedChip) {
        var _this = this;
        console.log(searchValue, "searchValue>>>>>>");
        var charLength = searchValue.length;
        var selectedIndex = this.formValues.chipFields.indexOf(selectedChip);
        this.inputData["searchValue"] = searchValue;
        if (selectedChip.onSearchFunction) {
            var apiUrl = selectedChip.onSearchFunction.apiUrl;
            var responseName_5 = selectedChip.onSearchFunction.response;
            var query_4 = { realm: this.realm };
            var self = this;
            var temp = {};
            _.forEach(selectedChip.onSearchFunction.requestData, function (requestItem) {
                if (requestItem.subKey) {
                    temp =
                        self.inputData[requestItem.value] &&
                            self.inputData[requestItem.value][requestItem.subKey]
                            ? self.inputData[requestItem.value][requestItem.subKey]
                            : "";
                    query_4[requestItem.name] = temp;
                }
                else if (requestItem.isDefault) {
                    query_4[requestItem.name] = requestItem.value;
                }
                else if (requestItem.alternativeKeyCheck) {
                    query_4[requestItem.name] = (self.inputData[requestItem.value] === undefined) ? self.inputData[requestItem.alternativeKey] : self.inputData[requestItem.value];
                }
                else {
                    query_4[requestItem.name] = self.inputData[requestItem.value];
                }
            });
            var limit = selectedChip.charlimit ? selectedChip.charlimit : 3;
            // console.log("Limit ", limit);
            // if (charLength >= limit || !searchValue) {
            this.charlimitcheck = true;
            this.contentService.getAllReponse(query_4, apiUrl).subscribe(function (data) {
                _this.formValues.chipFields[selectedIndex].data = data.response[responseName_5];
                var item = _this.formValues.chipFields[selectedIndex];
                var validateData = item.data[0];
                var isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                if (!isArrayOfJSON) {
                    var tempList_3 = [];
                    _.forEach(item.data, function (item) {
                        tempList_3.push({ name: item, _id: item });
                    });
                    item.data = tempList_3;
                }
            });
            // } else {
            //   console.log("Stop API ");
            //   this.charlimitcheck = false;
            // }
        }
    };
    FormLayoutComponent.prototype.onLoadChipChange = function (item) {
        if (item.onLoadFunction) {
            // chipfields OnLoad function added when implement run reports
            var apiUrl = item.onLoadFunction.apiUrl;
            var responseName_6 = item.onLoadFunction.response;
            var requestData = item.onLoadFunction.requestData;
            var query_5 = { realm: this.realm };
            var self_1 = this;
            _.forEach(requestData, function (requestItem) {
                console.log(requestItem, ">>requestItem");
                if (requestItem.isDefault) {
                    query_5[requestItem.name] = requestItem.value;
                }
                else if (requestItem.subKey) {
                    if (requestItem.isMap && self_1.inputData[requestItem.value]) {
                        // query[requestItem.name] = self.inputData[requestItem.value] ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey) : []
                        // query[requestItem.name] = requestItem.isConvertToString ? JSON.stringify(query[requestItem.name]) : query[requestItem.name];
                        var t1 = self_1.inputData[requestItem.value]
                            ? _.map(self_1.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey)
                            : [];
                        if (t1.length) {
                            query_5[requestItem.name] =
                                requestItem.isConvertToString && typeof t1 !== "string"
                                    ? JSON.stringify(t1)
                                    : t1;
                        }
                    }
                    else if (self_1.inputData[requestItem.value]) {
                        query_5[requestItem.name] = self_1.inputData[requestItem.value]
                            ? self_1.inputData[requestItem.value][requestItem.subKey]
                            : [];
                        query_5[requestItem.name] = requestItem.isConvertToString
                            ? JSON.stringify(query_5[requestItem.name])
                            : query_5[requestItem.name];
                    }
                }
                else {
                    query_5[requestItem.name] = self_1.inputData[requestItem.value];
                }
            });
            console.log(query_5, ">>>query");
            console.log(this, ">>> THIS");
            self_1.contentService.getAllReponse(query_5, apiUrl).subscribe(function (data) {
                item.data = data.response[responseName_6];
                self_1.inputData["selectedChips"][item.name] = item.data;
                self_1.inputData["selectedChipKeyList"][item.name] = _.map(item.data, item.onLoadFunction.keyToSave);
                console.log(self_1.inputData, ">>> INPUT");
                localStorage.setItem("currentInput", JSON.stringify(self_1.inputData));
            });
        }
    };
    FormLayoutComponent.prototype.onChipSelect = function (event, selectedChipId, keyToSave, chipError) {
        this.chipListOpenView = true;
        console.log(event, "Event>>>>>>>", this.ChipLimit, this.chipLength);
        console.log(selectedChipId, ">>selectedChipId");
        // console.log(chipError, "Event>>>>>>>");
        if (this.ChipLimit == this.chipLength) {
            this.chipInput.nativeElement.value = "";
            // this.snackBarService.warning(
            //   this._fuseTranslationLoaderService.instant(chipError)
            // );
        }
        else {
            this.chipValiditaion = false;
            this.selectedChips[selectedChipId] = this.selectedChips[selectedChipId]
                ? this.selectedChips[selectedChipId]
                : [];
            // if (this.selectedChips[selectedChipId].indexOf(event.option.value) < 0) {
            //   this.selectedChips[selectedChipId].push(event.option.value);
            // }
            var exitIndex = _.findIndex(this.selectedChips[selectedChipId], event.option.value);
            console.log('>> exitIndex ', exitIndex);
            if (exitIndex < 0) {
                this.selectedChips[selectedChipId].push(event.option.value);
            }
            console.log(this.formValues.chipFields, "<<<<<>>>>>");
            var selectedFormChip = _.find(this.formValues.chipFields, {
                name: selectedChipId,
            });
            console.log(selectedFormChip, ">>>selectedFormChip");
            console.log(this.inputData, "<<<<<inputData>>>>>");
            event["value"] = null;
            this.chipInput.nativeElement.value = "";
            this.selectedChipKeyList[selectedChipId] = _.map(this.selectedChips[selectedChipId], keyToSave);
            // this.onLoadChipChange();
            this.inputData["selectedChipKeyList"] = this.selectedChipKeyList;
            this.inputData["selectedChips"] = this.selectedChips;
            if (selectedFormChip && selectedFormChip["onChangeLoad"]) {
                var onLoadChipData = _.find(this.formValues.chipFields, {
                    name: selectedFormChip["loadchildKey"],
                });
                this.onLoadChipChange(onLoadChipData);
            }
            this.chipLength = this.inputData["selectedChips"][selectedChipId].length;
            this.selectedchipLength = this.selectedChips[selectedChipId].length;
            this.inputGroup.get(selectedChipId).setValue("");
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    };
    FormLayoutComponent.prototype.removeSelectedChip = function (value, selectedChipId, keyToSave) {
        console.log("removeSelectedChip ", this.inputData);
        var selectedIndex = this.selectedChips[selectedChipId].indexOf(value);
        if (selectedIndex >= 0) {
            this.selectedChips[selectedChipId].splice(selectedIndex, 1);
        }
        this.selectedChipKeyList[selectedChipId] = _.map(this.selectedChips[selectedChipId], keyToSave);
        this.chipLength = this.inputData["selectedChips"][selectedChipId].length;
        console.log(this.chipLength, ">>>>> SELECTED CHIP VALUE LENGTH");
        var selectedFormChip = _.find(this.formValues.chipFields, {
            name: selectedChipId,
        });
        console.log(selectedFormChip);
        if (this.chipLength) {
            console.log(selectedFormChip, ">>>selectedFormChip");
            if (selectedFormChip && selectedFormChip["onChangeLoad"]) {
                var onLoadChipData = _.find(this.formValues.chipFields, {
                    name: selectedFormChip["loadchildKey"],
                });
                this.onLoadChipChange(onLoadChipData);
            }
        }
        else {
            this.selectedChips[selectedFormChip["loadchildKey"]] = [];
        }
        this.inputData["selectedChipKeyList"] = this.selectedChipKeyList;
        this.inputData["selectedChips"] = this.selectedChips;
    };
    FormLayoutComponent.prototype.loadSavedData = function () {
        var savedData = this.onLoadData.savedData;
        var viewSavedData = this.currentConfigData[this.onLoadData.action]
            .savedDataDetails;
        var self = this;
        this.inputData["_id"] = savedData._id;
        _.forEach(viewSavedData, function (item) {
            if (item.subKey) {
                self.inputData[item.keyToShow] = self.inputData[item.keyToShow]
                    ? self.inputData[item.keyToShow]
                    : {};
                if (item.assignFirstIndexval) {
                    self.inputData[item.keyToShow] = (savedData[item.name] && savedData[item.name].length) ? savedData[item.name][0][item.subKey] : "";
                }
                else {
                    // self.inputData[item.keyToShow] = savedData[item.name][item.subKey];
                    self.inputData[item.keyToShow] =
                        savedData[item.name] && savedData[item.name][item.subKey]
                            ? savedData[item.name][item.subKey]
                            : "";
                }
            }
            else if (item.isCondition) {
                self.inputData[item.keyToShow] = eval(item.condition);
            }
            else {
                self.inputData[item.keyToShow] = savedData[item.name];
            }
            if (item.takeKeyList) {
                if (item.keyListName) {
                    self.inputData[item.keyListName] = self.inputData[item.keyListName]
                        ? self.inputData[item.keyListName]
                        : {};
                    self.inputData[item.keyListName][item.chipId] = [];
                    self.inputData[item.keyListName][item.chipId] = _.map(savedData[item.name], item.keyToSave);
                }
                self.inputData[item.keyToShow] = self.inputData[item.keyToShow] ? self.inputData[item.keyToShow] : {};
                self.inputData[item.keyToShow][item.chipId] = savedData[item.name];
                if (item.keyToShow == "selectedChips") {
                    self.selectedChips[item.chipId] = savedData[item.name];
                    self.inputData["selectedChips"] = self.selectedChips;
                }
                if (item.putKeyList) {
                    self.inputData[item.name] =
                        self.inputData[item.keyListName][item.chipId];
                }
            }
        });
        if (this.formValues && this.formValues.selectedDataKey) {
            var selectedDataKey = this.formValues.selectedDataKey;
            if (this.inputData[selectedDataKey] &&
                this.inputData[selectedDataKey].length) {
                this.selectedTableView =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues.tableData
                        : [];
                this.selectTableLoad =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues
                        : {};
                this.selectTableLoad["response"] = this.inputData[selectedDataKey];
                this.enableSelectValueView = true;
            }
        }
        else {
            if (this.inputData.selectedData && this.inputData.selectedData.length) {
                this.selectedTableView =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues.tableData
                        : [];
                this.selectTableLoad =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues
                        : {};
                this.selectTableLoad["response"] = this.inputData.selectedData;
                this.enableSelectValueView = true;
            }
        }
        if (this.formValues && this.formValues.showSelectedChips) {
            if (this.formValues && this.formValues.chipCondition) {
                var temp = this.formValues.chipCondition.variable;
                if (this.formValues.chipCondition.data &&
                    this.inputData &&
                    this.inputData[temp] &&
                    this.formValues.chipCondition.data[this.inputData[temp]]) {
                    this.ChipLimit = this.formValues.chipCondition.data[this.inputData[temp]]["limit"];
                    this.ChipOperator = this.formValues.chipCondition.data[this.inputData[temp]]["operator"];
                    this.inputData["chipOperator"] = this.ChipOperator;
                }
            }
        }
        this.viewData = this.inputData;
        // this.inputGroup.controls['ruleset'].setValue(this.inputData.ruleset);
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    };
    FormLayoutComponent.prototype.getChipList = function () {
        console.log(">>>>>>> entered chip fields ");
        if (this.enableChipFields) {
            var self = this;
            _.forEach(this.formValues.chipFields, function (item) {
                console.log("item ", item);
                if (item.onLoadFunction) {
                    var apiUrl = item.onLoadFunction.apiUrl;
                    var responseName_7 = item.onLoadFunction.response;
                    var requestData_3 = item.onLoadFunction.requestData;
                    var query_6 = { realm: self.realm };
                    _.forEach(requestData_3, function (requestItem) {
                        if (requestItem.isDefault) {
                            query_6[requestItem.name] = requestItem.value;
                        }
                        else {
                            query_6[requestItem.name] = requestItem.isConvertToString
                                ? JSON.stringify(self.inputData[requestItem.value])
                                : self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query_6, apiUrl).subscribe(function (data) {
                        item.data = data.response[responseName_7];
                        var validateData = item.data[0];
                        var isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                        if (!isArrayOfJSON) {
                            var tempList_4 = [];
                            if (item.onLoadFunction.subResponse) {
                                item.data = item.data[item.onLoadFunction.subResponse];
                                var keys = _.keys(item.data);
                                _.forEach(keys, function (itemKey) {
                                    tempList_4.push({
                                        name: item.data[itemKey][requestData_3["key"]],
                                        _id: itemKey,
                                    });
                                });
                            }
                            else {
                                _.forEach(item.data, function (item) {
                                    tempList_4.push({ name: item, _id: item });
                                });
                            }
                            item.data = tempList_4;
                        }
                    });
                }
            });
        }
        else {
        }
    };
    FormLayoutComponent.prototype.removeTag = function (chip, id) {
        this.selectedChip[id] =
            this.selectedChip[id] && this.selectedChip[id].length
                ? this.selectedChip[id]
                : [];
        var index = this.selectedChip[id].indexOf(chip);
        if (index >= 0) {
            this.selectedChip[id].splice(index, 1);
        }
    };
    FormLayoutComponent.prototype.onButtonClick = function (data, formValues) {
        var _this = this;
        console.log(data, ".....DATA");
        console.log(formValues, "....formValues");
        if (data.action == "export") {
            this.onExportClick(data, formValues);
        }
        else if (data.action == "scheduleView") {
            this.inputData["reportName"] = this._fuseTranslationLoaderService.instant(formValues.name);
            this.inputData["reportType"] = formValues.reportType;
            if (!this.inputData["repeatType"])
                this.inputData["repeatType"] = "run_once";
            //  this.inputData["scheduleType"] = formValues.isSingle ? 3 : 2;
            var requestData_4 = formValues.scheduleRequest
                ? formValues.scheduleRequest.requestData
                : [];
            var self = this;
            this.submitted = true;
            var toastMessageDetails_1 = formValues.scheduleRequest.toastMessage;
            requestData_4["realm"] = this.realm;
            this.validateSchduleData(function (result) {
                if (result) {
                    self.constructScheduleData(requestData_4, function (returnData) {
                        console.log(">>>>>>>>>>>>.. returnData ", returnData);
                        if (returnData) {
                            self.contentService
                                .createRequest(returnData, formValues.scheduleRequest.apiUrl)
                                .subscribe(function (res) {
                                console.log(" cretae Request success ", res);
                                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_1.success));
                                if (res.status == 201) {
                                    localStorage.removeItem("currentInput");
                                    self.matDialogRef.close();
                                    self.messageService.sendModelCloseEvent("listView");
                                }
                            }, function (error) {
                                console.log(" cretae Request Error ", error);
                                self.submitted = false;
                                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_1.error));
                            });
                        }
                    });
                }
            });
        }
        else if (data.action == "resetView") {
            this.ngOnInit();
        }
        else if (data.action == "cancel") {
            this.matDialogRef.close();
        }
        else if (data.action == 'import') {
            this.onImport();
        }
        else if (data.action == "submitView" || data.action == "checkConflict" || data.action == "update") {
            console.log(this.inputGroup, ">>>>>>>>>>>ss");
            if (this.formValues.getFormControlValue) {
                this.inputData[this.formValues.valueToSave] = this.inputGroup.value[this.formValues.formFieldName];
                if (this.formValues.isMergeObject) {
                    var obj = this.inputGroup.value[this.formValues.mapObjectName];
                    this.inputData = tslib_1.__assign({}, this.inputData, obj[0]);
                }
                console.log(this.inputData, "......INPUT DATA");
            }
            else if (this.formValues.getFromcurrentInput) {
                var tempData = JSON.parse(localStorage.getItem("currentInput"));
                this.inputData = tslib_1.__assign({}, this.inputData, tempData);
            }
            this.viewData = this.inputData;
            this.onSubmit();
        }
        else {
            this.enableTableLayout = false;
            this.enableNewTableLayout = false;
            console.log(data, "....DATA");
            console.log(formValues, "......FormValues");
            this.submitted = true;
            if (formValues.validateForm) {
                var self_2 = this;
                this.validationCheck(function (result) {
                    console.log("result ><><><><><", result);
                    if (result) {
                        var conflictRequest_1 = formValues.requestDetails;
                        var requestDetails = conflictRequest_1.requestData;
                        var apiUrl = conflictRequest_1.apiUrl;
                        var toastMessageDetails_2 = conflictRequest_1.toastMessage;
                        var currentInputData_1 = JSON.parse(localStorage.getItem("currentInput"));
                        var requestData_5 = { realm: self_2.realm };
                        _.forEach(requestDetails, function (item) {
                            var tempData = item.subKey
                                ? currentInputData_1[item.value][item.subKey]
                                : currentInputData_1[item.value];
                            if (tempData) {
                                requestData_5[item.name] = item.convertToString
                                    ? JSON.stringify(tempData)
                                    : tempData;
                            }
                            else if (item.directAssign) {
                                requestData_5[item.name] = item.convertToString
                                    ? JSON.stringify(item.value)
                                    : item.value;
                            }
                        });
                        console.log(requestData_5, "....requestData");
                        self_2.loaderService.startLoader();
                        self_2.contentService.getAllReponse(requestData_5, apiUrl).subscribe(function (result) {
                            self_2.loaderService.stopLoader();
                            console.log(result.response, "....data.response");
                            console.log(conflictRequest_1.responseName, ".....RESPONSE NAME");
                            var tempData = result.response[conflictRequest_1.responseName];
                            console.log(tempData, "....tempData");
                            self_2.tableList = tempData;
                            // let currentTabTable = this.formValues;
                            // this.currentTableLoad = currentTabTable;
                            self_2.currentTableLoad = self_2.formValues;
                            self_2.currentTableLoad["response"] = self_2.tableList;
                            self_2.currentTableLoad["total"] = result.response.total;
                            self_2.enableTableLayout = self_2.formValues.enableNewTableLayout
                                ? false
                                : true;
                            self_2.enableNewTableLayout = self_2.formValues.enableNewTableLayout
                                ? true
                                : false;
                            // self.enable = true;
                            self_2.snackBarService.add(self_2._fuseTranslationLoaderService.instant(toastMessageDetails_2.success));
                        }, function (err) {
                            self_2.loaderService.stopLoader();
                            console.log("Err ", err);
                        });
                    }
                    else {
                    }
                });
            }
            else {
                var conflictRequest_2 = formValues.requestDetails;
                var requestDetails = conflictRequest_2.requestData;
                var apiUrl = conflictRequest_2.apiUrl;
                var toastMessageDetails_3 = conflictRequest_2.toastMessage;
                var currentInputData_2 = JSON.parse(localStorage.getItem("currentInput"));
                var requestData_6 = { realm: this.realm };
                _.forEach(requestDetails, function (item) {
                    var tempData = item.subKey
                        ? currentInputData_2[item.value][item.subKey]
                        : currentInputData_2[item.value];
                    if (tempData) {
                        requestData_6[item.name] = item.convertToString
                            ? JSON.stringify(tempData)
                            : tempData;
                    }
                    else if (item.directAssign) {
                        requestData_6[item.name] = item.convertToString
                            ? JSON.stringify(item.value)
                            : item.value;
                    }
                });
                console.log(requestData_6, "....requestData");
                this.loaderService.startLoader();
                this.contentService.getAllReponse(requestData_6, apiUrl).subscribe(function (result) {
                    _this.loaderService.stopLoader();
                    console.log(result.response, "....data.response");
                    console.log(conflictRequest_2.responseName, ".....RESPONSE NAME");
                    var tempData = result.response[conflictRequest_2.responseName];
                    console.log(tempData, "....tempData");
                    _this.tableList = tempData;
                    // let currentTabTable = this.formValues;
                    // this.currentTableLoad = currentTabTable;
                    _this.currentTableLoad = _this.formValues;
                    _this.currentTableLoad["response"] = _this.tableList;
                    _this.currentTableLoad["total"] = result.response.total;
                    _this.enableTableLayout = true;
                    _this.enableNewTableLayout = _this.formValues.enableNewTableLayout
                        ? true
                        : false;
                    _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails_3.success));
                }, function (err) {
                    _this.loaderService.stopLoader();
                    console.log("Err ", err);
                });
            }
        }
    };
    FormLayoutComponent.prototype.validateSchduleData = function (callback) {
        this.submitted = true;
        var tempData = JSON.parse(localStorage.getItem("currentInput"));
        if (tempData && tempData.selectedData && tempData.selectedData.length) {
            this.inputData = tslib_1.__assign({}, this.inputData, tempData);
        }
        if (this.formValues.formusedForCheck) {
        }
        this.inputData["chipLimit"] = this.ChipLimit;
        this.inputData["chipOperator"] = this.ChipOperator;
        console.log(this.inputData, " Schedule inputDatainputData");
        console.log(this.inputGroup, "....... this.inputGroup ");
        // console.log(this.inputGroup.get("scheduleField") , "-- scheduleField");
        var dateField = this.inputGroup.get("startDate").value;
        // console.log(">>>>>>>>>>> dateField ",dateField);
        //  if(dateField==undefined){
        //   this.dateFormatcheck = false;
        //   this.inputGroup.get("meridian").invalid;
        //  }
        if (this.formValues.validateSelectedVal) {
            if (this.inputData &&
                this.inputData["selectedData"] &&
                this.inputData["selectedData"].length) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
            else {
                var item = this.formValues.scheduleFields;
                Object.keys(item).forEach(function (value) {
                    // tempObj[value] = new FormControl([""]);
                    // let val=this.inputGroup.value[value];
                    //  if(!val)
                    //  {
                    //   this.inputGroup.controls(item).setErrors({'incorrect': true});
                    //  }
                });
                var toastMessageDetails = this.formValues.validations.toastMessage;
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                callback(false);
            }
        }
        else {
            if (this.inputGroup && this.inputGroup.valid) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
        }
    };
    FormLayoutComponent.prototype.constructScheduleData = function (requestDetails, callback) {
        var date = moment(this.inputData.startDate).format("DD-MM-YYYY");
        var dateConcat = moment(date + " " + this.inputData.time + " " + this.inputData.meridian, "DD-MM-YYYY HH:mm a").format();
        this.inputData["dateTime"] = dateConcat;
        this.inputData["time"] = dateConcat;
        this.inputData["userRecipients"] = this.recipient;
        this.inputData["selectedDays"] = [];
        this.inputData["date"] = moment().format("DD-MMM-YYYY");
        var currentInputData = JSON.parse(localStorage.getItem("currentInput"));
        this.inputData = tslib_1.__assign({}, this.inputData, currentInputData);
        this.inputData = tslib_1.__assign({}, this.inputData, this.inputGroup.value);
        var self = this;
        var requestData = {};
        _.forEach(requestDetails, function (item) {
            var tempData;
            if (item.subKey &&
                self.inputData[item.value] &&
                self.inputData[item.value][item.subKey]) {
                tempData = self.inputData[item.value][item.subKey];
            }
            else if (!item.subKey) {
                if (item.isDefault) {
                    tempData = item.value;
                }
                else if (item.AssignFirstIndexval) {
                    tempData = self.inputData[item.value][0];
                }
                // else if(item.fromFormControl){
                //   tempData = self.inputGroup.value
                // }
                else {
                    tempData = self.inputData[item.value];
                }
            }
            if (tempData) {
                requestData[item.name] = item.convertToString
                    ? JSON.stringify(tempData)
                    : tempData;
            }
        });
        callback(requestData);
    };
    FormLayoutComponent.prototype.showRepeatData = function (val) {
        if (val.checked) {
            this.enableRepeatData = true;
            this.inputGroup.get("repeatType").setValue("daily");
            this.inputData["daily"] = true;
        }
        else {
            this.enableRepeatData = false;
            this.inputGroup.get("repeatType").setValue("");
            this.inputData["daily"] = false;
        }
    };
    FormLayoutComponent.prototype.getSelecteddays = function ($event, element) {
        var index = this.inputData["selectedDays"].indexOf(element);
        if (index === -1) {
            this.inputData["selectedDays"].push(element);
        }
        else {
            this.inputData["selectedDays"].splice(index, 1);
        }
    };
    FormLayoutComponent.prototype.onRepeatSelectChange = function (value, data) {
        var self = this;
        _.forEach(data, function (item) {
            if (item.value == value) {
                self.inputData[value] = true;
            }
            else {
                self.inputData[item.value] = false;
            }
        });
    };
    FormLayoutComponent.prototype.onExportClick = function (buttonData, request) {
        var _this = this;
        console.log(">>>>>>>>>>>>onExportClick ", request);
        var exportRequest, requestDetails, apiUrl;
        var requestData = {}, reportTemp;
        var dynamicReport = request.isdyamicData ? request.isdyamicData : false;
        if (dynamicReport) {
            this.currentData = this.currentConfigData.exportData;
            exportRequest = this.currentData.exportRequest;
            var ReportData = JSON.parse(localStorage.getItem("CurrentReportData"));
            apiUrl = exportRequest.apiUrl;
            reportTemp = tslib_1.__assign({}, ReportData, buttonData);
            requestDetails = exportRequest.RequestData;
        }
        else {
            var formRequest = request.exportRequest;
            exportRequest = formRequest
                ? formRequest
                : this.formValues.exportData.exportRequest;
            this.inputData["exportType"] = buttonData.exportType;
            requestDetails = exportRequest.requestData;
            apiUrl = exportRequest.apiUrl;
        }
        var self = this;
        _.forEach(requestDetails, function (item) {
            var tempData;
            if (item.subKey &&
                self.inputData[item.value] &&
                self.inputData[item.value][item.subKey]) {
                if (item.convertString) {
                    var temp_1 = [];
                    _.forEach(self.inputData[item.value][item.subKey], function (dataItem) {
                        temp_1.push(dataItem);
                    });
                    tempData = JSON.stringify(temp_1);
                }
                else {
                    tempData = self.inputData[item.value][item.subKey];
                }
            }
            else if (!item.subKey) {
                if (item.reportCheck) {
                    var t1 = item.fromTranslation
                        ? self._fuseTranslationLoaderService.instant(item.value)
                        : item.value;
                    tempData = item.conditionCheck ? reportTemp[item.value] : t1;
                }
                else if (item.fromLoginData) {
                    tempData = self.userData ? self.userData[item.value] : "";
                }
                else {
                    tempData = item.isDefault ? item.value : self.inputData[item.value];
                }
            }
            if (self.formValues.getValueFromConfig && self.formValues[item.value]) {
                tempData = self.formValues[item.value];
            }
            if (tempData)
                requestData[item.name] = item.convertToString
                    ? JSON.stringify(tempData)
                    : tempData;
        });
        if (exportRequest.isReportCreate) {
            // form based report create
            // Export report
            var self = this;
            this.submitted = true;
            console.log(">>Form report requestData ", requestData);
            this.validationCheck(function (result) {
                console.log(">>>>>>>> result ", result);
                var toastMessageDetails = exportRequest.toastMessage;
                self.contentService.createRequest(requestData, apiUrl).subscribe(function (res) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                    if (res.status == 200) {
                        localStorage.removeItem("CurrentReportData");
                        self.matDialogRef.close();
                        self.messageService.sendModelCloseEvent("listView");
                    }
                }, function (error) {
                    self.submitted = false;
                    self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                });
            });
        }
        else {
            this.contentService
                .getExportResponse(requestData, apiUrl)
                .subscribe(function (data) {
                if (_this.formValues.exportData &&
                    _this.formValues.exportData.s3Download) {
                    var toastMessageDetails = exportRequest.toastMessage;
                    _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                    _this.matDialogRef.close();
                }
                else {
                    var fileName = _this._fuseTranslationLoaderService.instant(exportRequest.downloadFileName);
                    _this.downloadExportFile(data, fileName + buttonData.fileType, buttonData.selectionType);
                }
            });
        }
    };
    FormLayoutComponent.prototype.downloadExportFile = function (reportValue, fileName, selectionType) {
        var blob = new Blob([reportValue], { type: selectionType });
        FileSaver.saveAs(blob, fileName);
    };
    FormLayoutComponent.prototype.onSelectionChanged = function (event, id) {
        this.selectedChip[id] =
            this.selectedChip[id] && this.selectedChip[id].length
                ? this.selectedChip[id]
                : [];
        var index = this.selectedChip[id].indexOf(event.option.value);
        if (index < 0) {
            this.selectedChip[id].push(event.option.value);
        }
        this.accessInput.nativeElement.value = "";
    };
    FormLayoutComponent.prototype.onSearchAccessControl = function (event) {
        var value = event.value;
        if ((value || "").trim()) {
            this.chipListFields.forEach(function (item) {
                item.data.push(value.trim());
            });
        }
        this.accessInput.nativeElement.value = "";
    };
    FormLayoutComponent.prototype.ExportButtons = function (value) {
        var _this = this;
        var self = this;
        var apiUrl;
        var inputFieldObj = {};
        var queryObj = {};
        var requestObj = {};
        if (self.formData.buttons) {
            _.forEach(self.formData.buttons, function (item) {
                apiUrl = item.onLoadFunction.apiUrl;
                Object.keys(self.selectedChip).forEach(function (keys) {
                    inputFieldObj[keys] = JSON.stringify(self.selectedChip[keys]);
                    queryObj = {
                        datasource: self.dataId,
                        report_type: value,
                        type: "SOD",
                    };
                });
            });
            requestObj = tslib_1.__assign({}, inputFieldObj, queryObj);
            self.contentService.getExportResponse(apiUrl, requestObj).subscribe(function (data) {
                _this.downloadFile(data.body, value);
            }, function (error) {
                console.log(error);
            });
        }
    };
    FormLayoutComponent.prototype.downloadFile = function (response, value) {
        this.runReport = this._fuseTranslationLoaderService.instant(this.formData.header);
        if (value === "CSV") {
            var blob = new Blob([response], { type: "text/csv" });
            FileSaver.saveAs(blob, this.runReport + ".csv");
        }
        else {
            var blob = new Blob([response], { type: "text/xlsx" });
            FileSaver.saveAs(blob, this.runReport + ".xlsx");
        }
    };
    FormLayoutComponent.prototype.receiveClick = function (event) {
        var _this = this;
        this.enableSelectValueView = false;
        var tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(tempData) ? JSON.parse(tempData) : {};
        this.selectedTableView = this.formValues.viewSelectedValues
            ? this.formValues.viewSelectedValues.tableData
            : [];
        this.selectTableLoad = this.formValues.viewSelectedValues || {};
        if (this.formValues &&
            this.formValues.viewSelectedValues &&
            this.formValues.viewSelectedValues.inputKey) {
            this.selectTableLoad["response"] = this.inputData[this.formValues.viewSelectedValues.inputKey];
        }
        else {
            this.selectTableLoad["response"] = this.inputData.selectedData;
        }
        if (this.selectedTableView && this.selectedTableView.length) {
            setTimeout(function () {
                _this.enableSelectValueView = true;
            }, 100);
        }
        this.messageService.sendButtonEnableMessage("data");
    };
    FormLayoutComponent.prototype.deleteClick = function (event) {
        var _this = this;
        this.enableTableLayout = false;
        console.log(event, ">>>>>EVENT");
        console.log(this, ">>> THIS");
        setTimeout(function () {
            _this.enableTableLayout = true;
        }, 200);
    };
    FormLayoutComponent.prototype.changeTab = function (event) {
        event = event && event.index ? event.index : 0;
        var currentTabTable = this.formValues.tabData[event].loadTable;
        this.tableList = currentTabTable.tableData ? currentTabTable.tableData : [];
        this.currentTableLoad = currentTabTable;
        console.log(">>>  this.currentTableLoad ", this.currentTableLoad);
        if (this.formValues.fromLocalStorage && this.formValues.assignFirstTable) {
            // added for ruleset error log
            var responseKey = this.tableList[0].responseKey;
            var tableId = this.tableList[0].tableId;
            var subKey = this.tableList[0].subResponseKey;
            if (this.inputData[responseKey] &&
                this.inputData[responseKey][tableId] &&
                this.inputData[responseKey][tableId][subKey]) {
                this.currentTableLoad["response"] = this.inputData[responseKey][tableId][subKey]
                    ? this.inputData[responseKey][tableId][subKey]
                    : [];
            }
            if (this.tableList[0].enableErrorCount && this.tableList[0].countKey) {
                // errorShow key and value
                this.currentTableLoad[this.tableList[0].countKey] = this.inputData[responseKey][tableId];
            }
        }
        else {
        }
        this.enableTableLayout = true;
        this.enableNewTableLayout = this.formValues.enableNewTableLayout
            ? true
            : false;
    };
    FormLayoutComponent.prototype.navigateNext = function () {
        console.log(this.ChipLimit, "::::");
        var temp = this.isNameAvailable;
        this.submitted = true;
        var self = this;
        if (this.formValues.getFormControlValue) {
            this.inputData[this.formValues.valueToSave] = this.inputGroup.value[this.formValues.formFieldName];
        }
        console.log(this.inputData, "......INPUT DATA");
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.validationCheck(function (result) {
            if (result && temp) {
                self.stepperVal.next();
            }
        });
    };
    FormLayoutComponent.prototype.validationCheck = function (callback) {
        var tempData = JSON.parse(localStorage.getItem("currentInput"));
        console.log(">>> valaidationcheck tempData ", tempData);
        if (tempData && tempData.selectedData && tempData.selectedData.length) {
            this.inputData = tslib_1.__assign({}, this.inputData, tempData);
        }
        if (tempData &&
            tempData[this.formValues.validateSelectKey] &&
            tempData[this.formValues.validateSelectKey].length) {
            this.inputData = tslib_1.__assign({}, this.inputData, tempData);
        }
        if (this.formValues.formusedForCheck) {
        }
        this.inputData["chipLimit"] = this.ChipLimit;
        this.inputData["chipOperator"] = this.ChipOperator;
        console.log(this.inputData, "inputDatainputData");
        console.log(this.inputData, "TESTTT");
        console.log("this.formValues >>>>", this.formValues);
        if (this.formValues.validateSelectedVal) {
            if (this.inputData &&
                this.inputData[this.formValues.validateSelectKey] &&
                this.inputData[this.formValues.validateSelectKey].length) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
            else if (this.inputData &&
                this.inputData["selectedData"] &&
                this.inputData["selectedData"].length &&
                !this.formValues.validateSelectKey) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
            else {
                var toastMessageDetails = this.formValues.validations.toastMessage;
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                callback(false);
            }
        }
        else if (this.formValues.validateValueStatus &&
            this.formValues.validateSubKey) {
            // added When Manage role - access check
            var checkVal = _.find(this.inputData[this.formValues.validateSubKey], function (item) {
                return item.status == "ACTIVE";
            });
            if (checkVal) {
                callback(true);
            }
            else {
                var toastMessageDetails = this.formValues.validations.toastMessage;
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                this.chipValiditaion = true;
                callback(false);
            }
        }
        else if (this.formValues.validateSelectedChip) {
            if (this.inputData &&
                this.inputData["selectedChipKeyList"] &&
                this.inputData["selectedChipKeyList"][this.formValues.validationChipId] &&
                this.inputData["selectedChipKeyList"][this.formValues.validationChipId]
                    .length) {
                if (this.inputData.controlType == "SOD" &&
                    this.inputData["selectedChipKeyList"][this.formValues.validationChipId].length >= 2) {
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    callback(true);
                }
                else if (this.inputData.controlType == "Sensitive" &&
                    this.inputData["selectedChipKeyList"][this.formValues.validationChipId].length >= 1) {
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    callback(true);
                }
                else if (!this.inputData.controlType) {
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    callback(true);
                }
                else {
                    var toastMessageDetails = this.formValues.validations.toastMessage;
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    this.chipValiditaion = true;
                    callback(false);
                }
            }
            else {
                // console.log("this.selectedChips.length ",this.selectedChips)
                // console.log("this.formValues.chipFields ",this.formValues.chipFields)
                if (_.isEmpty(this.selectedChip)) {
                    var toastMessageDetails = this.formValues.validations.toastMessage;
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    this.chipValiditaion = true;
                    callback(false);
                }
                else if (!_.isEmpty(this.selectedChip)) {
                    var self_3 = this;
                    _.forEach(this.formValues.requiredChips, function (item) {
                        // console.log("item >>>>>>>>",item)
                        if (self_3.selectedChip[item] &&
                            self_3.selectedChip[item].length <= 0) {
                            self_3.chipValiditaion = true;
                            callback(false);
                        }
                        else if (self_3.selectedChip[item] &&
                            self_3.selectedChip[item].length > 0) {
                            self_3.chipValiditaion = false;
                            callback(true);
                        }
                    });
                }
                else {
                    var toastMessageDetails = this.formValues.validations.toastMessage;
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    callback(false);
                }
            }
        }
        else {
            if (this.inputGroup && this.inputGroup.valid) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
        }
    };
    FormLayoutComponent.prototype.navigatePrevious = function () {
        this.stepperVal.previous();
    };
    FormLayoutComponent.prototype.showData = function (tempData) {
        var _this = this;
        console.log(tempData, "DDDD");
        console.log(this.formValues, ">>>>>>formValues");
        this.selectedChips = this.inputData.selectedChips;
        this.selectedChipKeyList = this.inputData.selectedChipKeyList;
        // debugger;
        if (this.formValues && this.formValues.chipFields) {
            var self_4 = this;
            _.forEach(this.formValues.chipFields, function (x) {
                if (x && x.chipCondition) {
                    var temp = x.chipCondition.variable;
                    if (x.chipCondition.data &&
                        self_4.inputData &&
                        self_4.inputData[temp] &&
                        x.chipCondition.data[self_4.inputData[temp]]) {
                        self_4.ChipLimit =
                            x.chipCondition.data[self_4.inputData[temp]]["limit"];
                        self_4.ChipOperator =
                            x.chipCondition.data[self_4.inputData[temp]]["operator"];
                    }
                }
                if (x.onLoadFunction) {
                    // chipfields OnLoad function added when implement run reports
                    var apiUrl = x.onLoadFunction.apiUrl;
                    var responseName_8 = x.onLoadFunction.response;
                    var requestData = x.onLoadFunction.requestData;
                    var query_7 = { realm: self_4.realm };
                    _.forEach(requestData, function (requestItem) {
                        if (requestItem.isDefault) {
                            query_7[requestItem.name] = requestItem.value;
                        }
                        else {
                            query_7[requestItem.name] = self_4.inputData[requestItem.value];
                        }
                    });
                    self_4.contentService.getAllReponse(query_7, apiUrl).subscribe(function (data) {
                        x.data = data.response[responseName_8];
                    });
                }
            });
        }
        this.viewData = tempData;
        console.log(this.viewData, "......VIEW DATA");
        if (this.formValues &&
            (this.formValues.confirmData || this.formValues.showSelectedValues)) {
            this.enableSelectValueView = false;
            if (this.formValues && this.formValues.viewSelectedValues) {
                this.selectedTableView = this.formValues.viewSelectedValues.tableData
                    ? this.formValues.viewSelectedValues.tableData
                    : [];
                this.selectTableLoad = this.formValues.viewSelectedValues;
                if (this.formValues &&
                    this.formValues.viewSelectedValues &&
                    this.formValues.viewSelectedValues.inputKey) {
                    this.selectTableLoad["response"] = this.inputData[this.formValues.viewSelectedValues.inputKey];
                }
                else {
                    this.selectTableLoad["response"] = this.inputData.selectedData;
                }
                setTimeout(function () {
                    _this.enableSelectValueView = true;
                }, 100);
            }
        }
        if (this.formValues && this.formValues.autoCompleteFields) {
            _.forEach(this.formValues.autoCompleteFields, function (autoCompleteItem) {
                console.log(autoCompleteItem, ">>>>>>autoCompleteItem");
                if (autoCompleteItem.valueFromLocal) {
                    console.log(autoCompleteItem.dataKeyToSave, ">>>autoCompleteItem.dataKeyToSave");
                    autoCompleteItem[autoCompleteItem.dataKeyToSave] =
                        tempData[autoCompleteItem.inputKey];
                    //       // autoCompleteItem[autoCompleteItem.dataKeyToSave] = _.map(self.inputData[autoCompleteItem.inputKey], autoCompleteItem.keyToShow);
                }
            });
            //   console.log(this.formValues.autoCompleteFields, ">>>>>this.formValues.autoCompleteFields")
        }
        // if(this.formValues && this.formValues.showSelectedChips) {
        // }
    };
    FormLayoutComponent.prototype.validateObjectFields = function (inputData, mandatoryFields, callback) {
        var valid = true;
        _.forEach(mandatoryFields, function (item) {
            if (!inputData[item]) {
                valid = false;
            }
        });
        callback(valid);
    };
    FormLayoutComponent.prototype.addNewValue = function () {
        console.log(this, "......");
        if (this.formValues && this.formValues.enableAddButton) {
            var tempData = localStorage.getItem("currentInput");
            this.inputData = !_.isEmpty(JSON.parse(tempData))
                ? JSON.parse(tempData)
                : {};
            var tempArray_1 = this.inputData[this.formValues.dataToSave]
                ? this.inputData[this.formValues.dataToSave]
                : [];
            var tempObj_3 = {};
            var inputValues_1 = this.inputGroup.value;
            console.log(this.formValues.mandatoryFields, ">>>>>this.formValues.mandatoryFields");
            if (this.formValues && this.formValues.mandatoryFields) {
                var self = this;
                this.validateObjectFields(inputValues_1, this.formValues.mandatoryFields, function (valid) {
                    if (valid) {
                        _.forEach(self.formValues.objectFormat, function (objItem) {
                            tempObj_3[objItem.name] = inputValues_1[objItem.value];
                        });
                        var exist = tempArray_1 && tempArray_1.length
                            ? _.findIndex(tempArray_1, tempObj_3)
                            : -1;
                        if (exist < 0) {
                            tempArray_1.push(tempObj_3);
                        }
                        self.inputData[self.formValues.dataToSave] = tempArray_1;
                        localStorage.setItem("currentInput", JSON.stringify(self.inputData));
                        self.receiveClick(null);
                    }
                    else {
                        self.snackBarService.warning("Enter Mandatory Fields!");
                    }
                });
                this.inputGroup.reset();
            }
            // if(_.isEqual(inputValueKeys, this.formValues.mandatoryFields)){
            //   _.forEach(this.formValues.objectFormat, function(objItem){
            //     tempObj[objItem.name] = inputValues[objItem.value]
            //   })
            // }else{
            //   this.snackBarService.warning("Enter Mandatory Fields!");
            // }
        }
    };
    FormLayoutComponent.prototype.onChange = function (data, selectedField) {
        console.log(" data ", data);
        console.log(" selectedField ", selectedField);
        var tempData = JSON.parse(localStorage.getItem("currentInput"));
        var selectId = selectedField.name + "Id";
        var existCheck = _.has(this.inputData, selectId);
        var idKey = existCheck ? selectId : selectedField.name + "Id";
        var nameKey = selectedField.name + "Name";
        this.inputData[idKey] = data[selectedField.keyToSave];
        if (selectedField.additionalKeyToSave) {
            this.inputData[selectedField.name + "Name"] =
                data[selectedField.additionalKeyToSave];
            nameKey = selectedField.name + "Name";
        }
        tempData = tslib_1.__assign({}, tempData, this.inputData);
        if (selectedField.refreshChipField && this.selectedChips) {
            var self_5 = this;
            _.forEach(selectedField.refreshChipFieldKey, function (refreshItem) {
                self_5.selectedChips[refreshItem] = [];
                self_5.selectedChipKeyList[refreshItem] = [];
            });
        }
        tempData['selectedChips'] = this.selectedChips;
        tempData['selectedChipKeyList'] = this.selectedChipKeyList;
        this.inputData = tempData;
        if (_.has(this.inputData, selectedField.name) &&
            tempData[selectedField.name] === "") {
            tempData[selectedField.name] = this.inputData[selectedField.name];
            tempData[idKey] = this.inputData[idKey];
            tempData[nameKey] = this.inputData[nameKey];
            localStorage.setItem("currentInput", JSON.stringify(tempData));
        }
        else {
            localStorage.setItem("currentInput", JSON.stringify(tempData));
        }
        if (selectedField.getAutocompleteData) {
            var selectedRequestData = _.find(this.formValues.requestList, {
                requestId: data.value,
            });
            console.log(selectedRequestData, ">>>>>>selectedRequestData");
            this.getAutoCompleteValues(selectedRequestData, data, selectedField);
        }
        else if (selectedField.onLoadValueKey) {
            if (selectedField.onChipLoad) {
                _.forEach(this.formValues.chipFields, function (selectFieldItem) {
                    if (selectFieldItem.name == selectedField.onLoadValueKey) {
                        selectFieldItem.data = data[selectedField.onChipLoad];
                    }
                });
            }
            else {
                _.forEach(this.formValues.selectFields, function (selectFieldItem) {
                    if (selectFieldItem.name == selectedField.onLoadValueKey) {
                        selectFieldItem.data = data[selectedField.onLoadValueKey];
                    }
                });
            }
        }
        else {
            this.getChipList();
        }
    };
    FormLayoutComponent.prototype.getAutoCompleteValues = function (selectedRequestData, selectedData, selectedField) {
        var _this = this;
        console.log("** ", ">>>selectedField", selectedField);
        console.log("** selectedRequestData ", selectedRequestData);
        // console.log(" ** selectedRequestData.textFieldName ",selectedRequestData.textFieldName);
        if (selectedRequestData && selectedRequestData.keyToSaveOnInput) {
            this.inputData[selectedRequestData.keyToSaveOnInput] =
                selectedData[selectedRequestData.keyToSaveOnInput];
        }
        var self = this;
        var index;
        var currentObj;
        if (this.formValues && this.formValues.formArrayValues) {
            index = _.findIndex(this.formValues.formArrayValues.autoCompleteFields, {
                name: selectedRequestData.textFieldName,
            });
            currentObj = this.formValues.formArrayValues.autoCompleteFields[index];
        }
        else {
            index = _.findIndex(this.formValues.autoCompleteFields, selectedRequestData);
            currentObj = this.formValues.autoCompleteFields[index];
        }
        console.log(index, ">>>>>>index");
        // debugger;
        // let autoCompleteField = this.formValues.formArrayValues.autoCompleteFields;
        // _.forEach(this.formValues.formArrayValues.autoCompleteFields, function(
        //   item
        // ) {
        // let item = selectedRequestData;
        if (selectedRequestData && selectedRequestData.data) {
            console.log(" entered default data ");
            currentObj[selectedField.onLoadDataKey] = selectedRequestData.data;
        }
        else {
            if (selectedRequestData.onLoadFunction) {
                var apiUrl = selectedRequestData.onLoadFunction.apiUrl;
                var responseName_9 = selectedRequestData.onLoadFunction.response;
                var query_8 = { realm: self.realm };
                _.forEach(selectedRequestData.onLoadFunction.requestData, function (requestItem) {
                    if (requestItem.isDefault) {
                        query_8[requestItem.name] = requestItem.value;
                    }
                    else {
                        if (!requestItem.isSearch) {
                            query_8[requestItem.name] = self.inputData[requestItem.value];
                        }
                        if (requestItem.isEventValue) {
                            query_8[requestItem.name] = selectedData;
                        }
                    }
                });
                console.log(" query ", query_8, " apiurl ", apiUrl);
                //this.loaderService.startLoader();
                self.contentService.getAllReponse(query_8, apiUrl).subscribe(function (data) {
                    // this.loaderService.stopLoader();
                    currentObj[selectedField.onLoadDataKey] = data.response[responseName_9];
                    var validateData = currentObj[selectedField.onLoadDataKey][0];
                    var isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                    if (!isArrayOfJSON) {
                        var tempList_5 = [];
                        _.forEach(currentObj[selectedField.onLoadDataKey], function (val) {
                            tempList_5.push({ obj_name: val, obj_desc: val });
                        });
                        currentObj[selectedField.onLoadDataKey] = tempList_5;
                    }
                    _this.viewData[selectedField.onLoadDataKey] =
                        currentObj[selectedField.onLoadDataKey];
                    if (index == selectedField.enableConditionIndex) {
                        console.log(_this, ">>>>>");
                        _this.inputGroup.controls[selectedRequestData.name].enable();
                    }
                }, function (err) {
                    // this.loaderService.stopLoader();
                    console.log(" Error ", err);
                });
            }
        }
        // console.log(it/em, ".....>>>ITEMMM");
        // tempArray.push(item);
        // });
        currentObj = tslib_1.__assign({}, currentObj, selectedRequestData);
        console.log(currentObj, ".....currentObj");
        // this.formValues.formArrayValues.autoCompleteFields[index] = {
        //   ...currentObj,
        //   ...item
        // };
        if (this.formValues && this.formValues.formArrayValues) {
            this.formValues.formArrayValues.autoCompleteFields[index] = currentObj;
        }
        else {
            this.formValues.autoCompleteFields[index] = currentObj;
        }
        // console.log(
        //   this.formValues.formArrayValues.autoCompleteFields,
        //   ">>>>>>>>>>>>>>>>>"
        // );
    };
    FormLayoutComponent.prototype.onAutoCompleteSearch = function (searchValue, inputField) {
        console.log(searchValue, ">>>>>searchValue");
        console.log(inputField, ">>>>> INPUT FIELD");
        if (inputField.valueFromLocal || inputField.enableFilter) {
            if (searchValue) {
                var searchObj = {};
                searchObj[inputField.keyToShow] = searchValue;
                inputField[inputField.dataKeyToSave] = _.filter(inputField[inputField.dataKeyToSave], function (item) {
                    var filterValue = searchValue.toLowerCase();
                    if (item[inputField.keyToShow].toLowerCase().indexOf(filterValue) == 0) {
                        return item;
                    }
                });
            }
            else {
                inputField[inputField.dataKeyToSave] =
                    this.viewData && this.viewData[inputField.dataKeyToSave]
                        ? this.viewData[inputField.dataKeyToSave]
                        : this.viewData[inputField.inputKey];
            }
        }
    };
    FormLayoutComponent.prototype.onSelectAutoComplete = function (event, index, selectedField) {
        console.log(event, ">>>>>EVENT");
        console.log(index, ">>>>>INDEX");
        console.log(selectedField, ">>>>> SELECTED FIELD");
        var selectedRequestData = _.find(this.formValues.autoCompleteFields, {
            functionCallIndex: index + 1,
        });
        console.log(selectedRequestData, ">>>>>>selectedRequestData");
        this.getAutoCompleteValues(selectedRequestData, event, selectedField);
    };
    FormLayoutComponent.prototype.onSearchChange = function (searchValue, inputField) {
        var _this = this;
        console.log(inputField, "..inputField");
        console.log(searchValue, ".........searchvalue");
        var charLength = searchValue.length;
        var limit = this.formValues.formArrayValues.autoCompleteFields.charlimit
            ? this.formValues.formArrayValues.autoCompleteFields.charlimit
            : 3;
        console.log("Limit ", limit);
        // if (charLength >= limit || !searchValue) {
        console.log("call API ");
        this.charlimitcheck = true;
        var self = this;
        var index = _.findIndex(this.formValues.formArrayValues.autoCompleteFields, { name: inputField.textFieldName });
        var currentObj = this.formValues.formArrayValues.autoCompleteFields[index];
        if (inputField.onLoadFunction) {
            var apiUrl = inputField.onLoadFunction.apiUrl;
            var responseName_10 = inputField.onLoadFunction.response;
            var query_9 = { realm: self.realm };
            _.forEach(inputField.onLoadFunction.requestData, function (requestItem) {
                if (requestItem.isSearch) {
                    query_9[requestItem.name] = searchValue;
                }
                else if (requestItem.isDefault) {
                    query_9[requestItem.name] = requestItem.value;
                }
                else {
                    query_9[requestItem.name] = self.inputData[requestItem.value];
                }
            });
            //this.loaderService.startLoader();
            self.contentService.getAllReponse(query_9, apiUrl).subscribe(function (data) {
                console.log(">> onsearch Change ");
                // this.loaderService.stopLoader();
                currentObj[inputField.onLoadDataKey] = data.response[responseName_10];
                var validateData = currentObj[inputField.onLoadDataKey][0];
                var isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                if (!isArrayOfJSON) {
                    var tempList_6 = [];
                    _.forEach(currentObj[inputField.onLoadDataKey], function (val) {
                        tempList_6.push({ obj_name: val, obj_desc: val });
                    });
                    currentObj[inputField.onLoadDataKey] = tempList_6;
                }
                _this.viewData[inputField.onLoadDataKey] =
                    currentObj[inputField.onLoadDataKey];
                console.log(currentObj, ">>>>>currentObj");
            }, function (err) {
                // this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
        this.formValues.formArrayValues.autoCompleteFields[index] = currentObj;
        // } else {
        //   console.log("Stop API ");
        //   this.charlimitcheck = false;
        // }
    };
    FormLayoutComponent.prototype.onOptionChange = function (event, selectedOption) {
        console.log(this.inputData, "inputData>>>>>>>>");
        this.inputData[selectedOption.name] = event.value;
        console.log(this.formValues);
        // if (this.enableChipFields) {
        //   console.log(this.enableChipFields, "enableChipFields");
        //   var self = this;
        //   _.forEach(this.formValues.chipFields, function(x) {
        //     console.log("x");
        //     if (x.chipCondition && x.chipCondition.variable) {
        //       var self = this;
        //       let temp = x.chipCondition.variable;
        //       self.ChipLimit = x.chipCondition.data[self.inputData[temp]]["limit"];
        //       self.ChipOperator =
        //         x.chipCondition.data[self.inputData[temp]]["operator"];
        //       console.log(self.ChipLimit, "ChipLimit>>>");
        //     }
        //   });
        //   console.log(this.ChipLimit, "1");
        //   console.log(this.ChipOperator, "2");
        //   self.inputData["chipLimit"] = this.ChipLimit;
        //   self.inputData["chipOperator"] = this.ChipOperator;
        //   // localStorage.setItem('chipData',event.value)
        // }
        console.log(event, "....event.value");
        console.log(selectedOption, "....selectedOption");
        this.inputData[selectedOption.name] = event.value;
        var self = this;
        if (selectedOption && selectedOption.isImportType) {
            this.selectImportView(event.value);
        }
        else if (selectedOption && selectedOption.name == 'chechImport') {
            if (event.value == 'NO') {
                this.enableImportData = false;
            }
            else {
                this.enableImportData = true;
            }
        }
        else {
            _.forEach(selectedOption.fields, function (optionItem) {
                if (optionItem.checkedType) {
                    self.inputData[optionItem.checkedType] =
                        optionItem.value === event.value ? true : false;
                }
            });
            console.log(self.inputData, "self");
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    };
    FormLayoutComponent.prototype.toggleChanged = function (event) { };
    FormLayoutComponent.prototype.onSearchName = function (event, fieldData) {
        var _this = this;
        console.log(" on Search Name ************************** ");
        var charLength = event.length;
        console.log(" On search Name event ", event, " fielddata ", fieldData, " Length: ", charLength);
        if (fieldData.enableOnTypeSearch && event) {
            var limit = fieldData.charlimit ? fieldData.charlimit : 3;
            if (limit <= charLength) {
                this.charlimitcheck = true;
                console.log("proceed API call");
                var requestDetails_1 = fieldData.onTypeSearch;
                var query_10 = { realm: this.realm };
                var self = this;
                console.log(self.currentData, ">>>> CURRENT");
                _.forEach(requestDetails_1.requestData, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        query_10[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else if (requestItem.isDefault) {
                        query_10[requestItem.name] = requestItem.value;
                    }
                    else {
                        query_10[requestItem.name] = self.inputData[requestItem.value];
                    }
                });
                console.log("Query ", query_10);
                // this.loaderService.startLoader();
                this.contentService
                    .getAllReponse(query_10, requestDetails_1.apiUrl)
                    .subscribe(function (data) {
                    //  this.loaderService.stopLoader();
                    console.log(data, "data:::::");
                    // let responseLength =
                    //   data.response &&
                    //   data.response[requestDetails.responseName].length > 0
                    //     ? data.response[requestDetails.responseName].length
                    //     : 0;
                    if (data.response && data.response[requestDetails_1.responseName]) {
                        _this.isNameAvailable =
                            data.response[requestDetails_1.responseName].length == 0
                                ? true
                                : false;
                        console.log(_this.isNameAvailable, "isNameAvilable1");
                    }
                    else if (data.status == 409) {
                        _this.isNameAvailable = data.status == 409 ? false : true;
                        console.log(_this.isNameAvailable, "isNameAvilable2");
                    }
                    // this.isNameAvailable =
                    //   responseLength === 0 || data.status != 409 ? true : false;
                    // console.log(
                    //   data.response[requestDetails.responseName].length,
                    //   "IIIIII",
                    //   this.isNameAvailable
                    // );
                }, function (err) {
                    // this.loaderService.stopLoader();
                    if (err.status == 409) {
                        _this.isNameAvailable = err.status == 409 ? false : true;
                        console.log(_this.isNameAvailable, "isNameAvilable2");
                    }
                });
            }
            else {
                console.log("STOP APi call");
                // this.isNameAvailable=false;
                this.charlimitcheck = false;
            }
        }
    };
    FormLayoutComponent.prototype.onSubmit = function () {
        var self = this;
        console.log(this, ">>>>>>>>>>>>&&&&&THIS");
        this.submitted = true;
        this.validationCheck(function (result) {
            if (result) {
                var requestDetails = (self.onLoadData && self.onLoadData.action &&
                    self.currentConfigData[self.onLoadData.action] &&
                    self.currentConfigData[self.onLoadData.action].requestDetails)
                    ? self.currentConfigData[self.onLoadData.action].requestDetails
                    : {};
                if (!self.onLoadData && self.formValues.analysisRequest) {
                    requestDetails = self.formValues.analysisRequest;
                }
                else if (self.formValues.requestDetails) {
                    requestDetails = self.formValues.requestDetails;
                }
                var details_1 = self.viewData;
                // details["status"] = "ACTIVE";
                // details["operators"] = ["AND"];
                console.log(">>> details ", details_1);
                var apiUrl = requestDetails.apiUrl;
                var requestData_7 = {
                    realm: self.realm
                };
                _.forEach(requestDetails.requestData, function (item) {
                    if (item.isDefault) {
                        requestData_7[item.name] = item.value;
                    }
                    else if (item.controlTypeCheck) {
                        //  for storing Operators
                        //  requestData[item.name] = eval(item.condition);
                        requestData_7[item.name] =
                            details_1["controlType"] && details_1["controlType"] == "SOD"
                                ? "AND"
                                : "OR";
                    }
                    else if (item.fromFormGroup) {
                        var formGroupvalue = self.inputGroup.value;
                        requestData_7[item.name] = formGroupvalue[item.value];
                    }
                    else {
                        var existCheck = _.has(details_1, item.value);
                        console.log(">>> existCheck ", existCheck);
                        if (existCheck) {
                            requestData_7[item.name] = item.subKey
                                ? details_1[item.value][item.subKey]
                                : details_1[item.value];
                        }
                        if (requestData_7[item.name] === undefined &&
                            item.alternativeKeyCheck) {
                            // addded for storing  default selecteld field objectId . (Edit)
                            requestData_7[item.name] = details_1[item.alternativeKey];
                        }
                    }
                });
                var toastMessageDetails_4 = requestDetails.toastMessage;
                if (self.onLoadData && (self.onLoadData.action === "edit" || self.onLoadData.action === "update")) {
                    var idVal = requestDetails.multipleId ? true : self.inputData._id;
                    self.contentService
                        .updateRequest(requestData_7, apiUrl, idVal)
                        .subscribe(function (res) {
                        self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_4.success));
                        if (res.status == 201 || res.status == 200) {
                            localStorage.removeItem("currentInput");
                            self.matDialogRef.close();
                            self.messageService.sendModelCloseEvent("listView");
                        }
                    }, function (error) {
                        self.submitted = false;
                        self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_4.error));
                    });
                }
                else {
                    self.contentService.createRequest(requestData_7, apiUrl).subscribe(function (res) {
                        self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails_4.success));
                        if (res.status == 201) {
                            localStorage.removeItem("currentInput");
                            self.matDialogRef.close();
                            self.messageService.sendModelCloseEvent("listView");
                        }
                    }, function (error) {
                        self.submitted = false;
                        self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails_4.error));
                    });
                }
            }
        });
    };
    FormLayoutComponent.prototype.onFileChange = function (event, fileType, importData) {
        var _this = this;
        console.log(">>> onFileChange event ", event);
        var reader = new FileReader();
        if (event.length) {
            var file_1 = event[0];
            var fileName = file_1.name;
            console.log(">>> file ", file_1);
            console.log(">>> fileName ", fileName);
            this.fileType = file_1.type;
            var fileExt = fileName.split(".").pop();
            var fileValidateFlag = false;
            if (importData && importData.multiFileSupport) {
                var validFileTypes = importData.supportedFiles.split(",");
                fileValidateFlag = (validFileTypes.indexOf(fileExt) >= 0) ? true : false;
            }
            else {
                fileValidateFlag = (fileExt == fileType) ? true : false;
            }
            if (fileValidateFlag) {
                console.log(" Proceed ");
                reader.readAsDataURL(file_1);
                reader.onload = function () {
                    _this.image = file_1;
                    _this.inputGroup.patchValue({
                        file: reader.result,
                    });
                    _this.selectedFileName = file_1.name;
                    _this.fileUpload = file_1;
                    _this.changeDetector.markForCheck();
                };
            }
            else {
                console.log(" File Extension Error ");
                this.importData.uploadFile = {};
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant("File Extension Error. File extension should be " +
                    fileType +
                    " format"));
            }
        }
        else {
            console.log(">>> file required ");
        }
    };
    FormLayoutComponent.prototype.onFileChange_backup = function (event, fileType, importData) {
        var _this = this;
        var reader = new FileReader();
        if (importData && importData.multiFileSupport) {
            var file_2 = event.target.files[0];
            this.fileType = file_2.type;
            var fileName = file_2.name;
            var fileExt = fileName.split(".").pop();
            var validFileTypes = importData.supportedFiles.split(",");
            console.log(fileExt, "importData.supportedFiles >>>>>", validFileTypes);
            if (validFileTypes.indexOf(fileExt) >= 0) {
                console.log(" Proceed ");
                reader.readAsDataURL(file_2);
                reader.onload = function () {
                    _this.image = file_2;
                    _this.inputGroup.patchValue({
                        file: reader.result,
                    });
                    _this.selectedFileName = file_2.name;
                    _this.fileUpload = file_2;
                    _this.changeDetector.markForCheck();
                };
            }
            else {
                console.log(" File Extension Error ");
                this.importData.uploadFile = {};
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant("File Extension Error. File extension should be " +
                    importData.supportedFiles +
                    " format"));
            }
        }
        else {
            if (event.target.files && event.target.files.length) {
                var file_3 = event.target.files[0];
                this.fileType = file_3.type;
                var fileName = file_3.name;
                console.log(fileName, " File Name");
                var fileExt = fileName.split(".").pop();
                // console.log(fileType," File Type ");
                // console.log(fileExt," fileExt ");
                if (fileExt == fileType) {
                    console.log(" Proceed ");
                    reader.readAsDataURL(file_3);
                    reader.onload = function () {
                        _this.image = file_3;
                        _this.inputGroup.patchValue({
                            file: reader.result,
                        });
                        _this.selectedFileName = file_3.name;
                        _this.fileUpload = file_3;
                        _this.changeDetector.markForCheck();
                    };
                }
                else {
                    console.log(" File Extension Error ");
                    this.importData.uploadFile = {};
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant("File Extension Error. File extension should be " +
                        fileType +
                        " format"));
                }
            }
        }
    };
    FormLayoutComponent.prototype.closeModel = function () {
        this.matDialogRef.close();
        this.messageService.sendModelCloseEvent("listView");
    };
    FormLayoutComponent.prototype.onImport = function () {
        var _this = this;
        this.submitted = true;
        var requestData = {};
        var self = this;
        this.inputData.cronExpression = this.cronExpression ? this.cronExpression : "runOnce";
        var importDetails = this.importData.onLoadFunction;
        _.forEach(importDetails.requestData, function (item) {
            if (item.isDefault) {
                requestData[item.name] = item.value;
            }
            else {
                requestData[item.name] = item.subKey
                    ? self.inputData[item.value][item.subKey]
                    : self.inputData[item.value];
            }
        });
        if (this.importData.uploadFile) {
            var formData = new FormData();
            Object.keys(requestData).map(function (key) {
                formData.append(key, requestData[key]);
            });
            formData.append("file", this.fileUpload);
            requestData = formData;
        }
        var toastMessageDetails = importDetails.toastMessage;
        console.log(this, ">>>>>> THIS");
        requestData["realm"] = this.realm;
        if (this.inputGroup && this.inputGroup.valid) {
            this.loaderService.startLoader();
            //this.matDialogRef.close();
            console.log("This.input .....  ", this.inputData);
            // debugger;
            if (this.importData.function && this.importData.function == "edit") { // ruleset, datasource - reimport
                if (this.fileUpload !== undefined) {
                    this.contentService
                        .updateRequest(requestData, importDetails.apiUrl, this.inputData["_id"]
                    // this.inputData["datasourceId"]
                    )
                        .subscribe(function (res) {
                        console.log(res, ".....res");
                        _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        if (res.status == 201 || res.status == 200) {
                            if (_this.importData.s3upload) {
                                var response = res.body.response;
                                if (response && response.job) {
                                    _this.matDialogRef.close();
                                    _this.imageUpload(response.job.datasource, _this.importData);
                                }
                                else if (response && response.datasource) {
                                    _this.matDialogRef.close();
                                    console.log("<<<<DATA IMPORT>>>", response.datasource);
                                    _this.imageUpload(response.datasource, _this.importData);
                                }
                                else if (response && response.ruleset) {
                                    _this.matDialogRef.close();
                                    console.log("<<<<Ruleset IMPORT>>>", response.ruleset);
                                    _this.imageUpload(response.ruleset, _this.importData);
                                }
                            }
                            else {
                                _this.matDialogRef.close();
                            }
                        }
                        _this.loaderService.stopLoader();
                    }, function (error) {
                        // this.matDialogRef.close();
                        _this.snackBarService.warning(_this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                        _this.errorTableList = _this.importData.tableData;
                        _this.errorLogTable = _this.importData;
                        _this.errorLogTable["response"] = error.error.response;
                        _this.errorLogTable["responseKey"] = "logsArr";
                        _this.errorShow = error.error.response;
                        console.log(_this.errorLogTable, "errorLog>>>>>>>");
                    });
                }
                else {
                    this.loaderService.stopLoader();
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(this.importData.validations.message));
                }
            }
            else {
                if (this.inputData && this.inputData.importType == "avm") {
                    this.contentService.avmImport(requestData, this.fileUpload, importDetails.avmApi).subscribe(function (res) {
                        _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        _this.loaderService.stopLoader();
                        _this.matDialogRef.close();
                    }, function (error) {
                        _this.loaderService.stopLoader();
                        _this.snackBarService.warning(_this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    });
                }
                else if (this.inputData && this.inputData.importType == "sodAgent") {
                    if (importDetails.sodAgentRequest) {
                        _.forEach(importDetails.sodAgentRequest, function (item) {
                            if (item.isDefault) {
                                requestData[item.name] = item.value;
                            }
                            else if (item.fromUserData) {
                                if (item.subKey && item.assigneFirstValue) {
                                    requestData[item.name] = (self.userData && self.userData[item.value] && self.userData[item.value][0]) ? self.userData[item.value][0][item.subKey] : "";
                                }
                                else if (item.subKey) {
                                    requestData[item.name] = (self.userData && self.userData[item.value]) ? self.userData[item.value] : "";
                                }
                                else {
                                    requestData[item.name] = (self.userData && self.userData[item.value]) ? self.userData[item.value] : "";
                                }
                            }
                            else {
                                requestData[item.name] = item.subKey
                                    ? self.inputData[item.value][item.subKey]
                                    : self.inputData[item.value];
                            }
                        });
                    }
                    console.log(this, ">>>>>> THIS");
                    console.log(">>>> requestData ", requestData);
                    var produtName_1 = requestData && requestData["productName"] ? requestData["productName"] : "Ebs";
                    console.log(">>> produtName ", produtName_1);
                    this.contentService
                        .createRequest(requestData, importDetails.sodAgentCreate + this.inputData[importDetails.sodAgentParameterKey])
                        .subscribe(function (res) {
                        console.log(res, ".....res");
                        if (res && res.body && res.body.data) {
                            _this.contentService
                                .getS3Response(requestData, importDetails.sodAgentDownloadApi
                                + _this.inputData[importDetails.sodAgentParameterKey]
                                + '/' + _this.userData.customer + '/' + produtName_1)
                                .subscribe(function (data) {
                                console.log(data, ">>>>data");
                                var localfilename = "Sod_Agent_" + produtName_1;
                                //var localfilename = "Sod_Agent_Ebs";
                                localfilename = localfilename.replace(/\s/gi, "_");
                                _this.downloadExportFile(data.body, localfilename + '.zip', 'application/zip');
                                _this.matDialogRef.close();
                                _this.loaderService.stopLoader();
                            });
                        }
                        else {
                            _this.snackBarService.warning("Unable To Download agent!");
                        }
                    }, function (error) {
                    });
                }
                else {
                    var apiurl = importDetails.apiUrl;
                    if (this.inputData.importType == 'webservice') {
                        // apiurl=importDetails.webserviceApi;
                        this.importData.s3upload = false;
                        requestData["webservice"] = true;
                    }
                    this.contentService
                        .createRequest(requestData, apiurl)
                        .subscribe(function (res) {
                        console.log(res, ".....res");
                        _this.snackBarService.add(_this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        if (res.status == 201 || res.status == 200) {
                            // this.matDialogRef.close();
                            if (_this.importData.s3upload) {
                                var response = (res.body && res.body.response && res.body.response.datasource) ? res.body.response.datasource : res.body.response;
                                if (response && response.job) {
                                    _this.matDialogRef.close();
                                    _this.imageUpload(response.job.datasource, _this.importData);
                                }
                                else {
                                    _this.matDialogRef.close();
                                    _this.imageUpload(response, _this.importData);
                                }
                            }
                            else {
                                _this.matDialogRef.close();
                            }
                        }
                        else if (res.status == 206) {
                            _this.errorTableList = _this.importData.tableData;
                            _this.errorLogTable = _this.importData;
                            _this.errorLogTable["response"] = res.body.response;
                            _this.errorLogTable["responseKey"] = "logsArr";
                            _this.errorShow = res.body.response;
                            console.log(_this.errorLogTable, "errorLog on different status code >>>>>>>");
                        }
                    }, function (error) {
                        // this.matDialogRef.close();
                        _this.snackBarService.warning(_this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                        _this.errorTableList = _this.importData.tableData;
                        _this.errorLogTable = _this.importData;
                        _this.errorLogTable["response"] = error.error.response;
                        _this.errorLogTable["responseKey"] = "logsArr";
                        _this.errorShow = error.error.response;
                        console.log(_this.errorLogTable, "errorLog>>>>>>>");
                    });
                }
            }
        }
    };
    FormLayoutComponent.prototype.selectImportView = function (event) {
        console.log(event, ".....event");
        this.enableTextFields = false;
        this.enableOptionFields = false;
        var toggleFields = this.formValues.enableToggleFields;
        if (event != "csv" && event != "avm") {
            this.enableImportData = false;
            if (toggleFields && toggleFields.mergeTextFields) {
                this.formValues.textFields = _.concat(this.formValues.textFields, toggleFields.textFields);
            }
            if (toggleFields && toggleFields.additionalOption) {
                this.formValues.additionalOptionFields = toggleFields.additionalOptionFields;
            }
            if (toggleFields && toggleFields.enableSchedule) {
                this.enableSchedule = true;
            }
            else {
                this.enableSchedule = false;
            }
            if (toggleFields && toggleFields.enableAgentNote) {
                this.formValues.enableAgentNote = toggleFields.enableAgentNote;
            }
            var self = this;
            if (toggleFields.isSetDefaultValue) {
                _.forEach(toggleFields.additionalOptionFields, function (optionItem) {
                    self.inputData[optionItem.defaultKey] = optionItem.defaultValue;
                });
            }
            this.enableTextFields = true;
            this.enableOptionFields = true;
        }
        else {
            this.formValues.enableAgentNote = false;
            this.enableSchedule = false;
            this.formValues.textFields = this.formValues.textFields.filter(function (item) {
                return !toggleFields.textFields.includes(item);
            });
            if (toggleFields.additionalOptionFields) {
                this.formValues.additionalOptionFields = [];
            }
            this.enableImportData = true;
            // this.formValues.textFields =
            this.enableTextFields = true;
            this.enableOptionFields = true;
        }
        var tempObj = {};
        if (this.enableTextFields) {
            _.forEach(this.formValues.textFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        if (toggleFields.additionalOption) {
            tempObj["everyMinute"] = new FormControl("");
            tempObj["mainType"] = new FormControl("");
            tempObj["hourListvalue"] = new FormControl("");
            tempObj["everyDays"] = new FormControl("");
            tempObj["dayStartingOn"] = new FormControl("");
            tempObj["monthStartingOn"] = new FormControl("");
        }
        this.inputGroup = new FormGroup(tempObj);
    };
    FormLayoutComponent.prototype.imageUpload = function (data, importData) {
        var _this = this;
        console.log(data, ".....data");
        this.AwsS3UploadService.find({
            mimeType: this.fileType,
            type: importData.s3RequestType ? importData.s3RequestType : "datasource",
        }).subscribe(function (s3Credentials) {
            if (s3Credentials) {
                _this.AwsS3UploadService.uploadS3(_this.image, s3Credentials.response, importData)
                    .then(function (result) {
                    console.log(result, "...result");
                    var url = decodeURIComponent(result.PostResponse.Location);
                    console.log(url, "...url");
                    _this.inputData["_id"] = data._id;
                    _this.inputData["datasourcePath"] = url;
                    _this.inputData["status"] = "Uploaded";
                    if (_this.importData.function &&
                        _this.importData.function == "edit") {
                        _this.inputData["reimport"] = true;
                    }
                    else {
                        _this.inputData["reimport"] = false;
                    }
                    var requestData = {};
                    console.log(_this, "....THSIIISSS");
                    var updateDetails = _this.importData.updateRequest;
                    console.log(updateDetails, ".....updateDetails");
                    var self = _this;
                    _.forEach(updateDetails.requestData, function (item) {
                        requestData[item.name] = self.inputData[item.value];
                    });
                    console.log(requestData, "....REQUEST DATA");
                    _this.contentService
                        .updateRequest(requestData, updateDetails.apiUrl, _this.inputData._id)
                        .subscribe(function (res) {
                        _this.matDialogRef.close();
                        _this.messageService.sendModelCloseEvent("listView");
                    });
                    // this.formData['erp_instance'] = this.csvForm.value.erp_instance;
                    // this.formData['name'] = this.csvForm.value.name;
                    // this.formData['status'] = "Uploaded";
                    // this.formData['ds_path'] = url;
                    // this.SetupAdministrationService.putImportCSV(this.formData, this.id).subscribe((dataNew: any) => {
                    // 	this.matDialogRef.close(dataNew.status);
                    // });
                })
                    .catch(function (error) { });
            }
        });
    };
    FormLayoutComponent.prototype.viewOfseconds = function () {
        var _this = this;
        for (var i = 1; i <= 60; i++) {
            // console.log(i);
            this.secondsList.push(i);
        }
        this.secondsLastDigits = [];
        this.hourListView = [];
        this.hourstwothree = [];
        for (var j = 0; j <= 59; j++) {
            // console.log(i);
            this.secondsLastDigits.push(j);
        }
        for (var k = 1; k <= 24; k++) {
            this.hourListView.push(k);
        }
        for (var hours = 1; hours <= 23; hours++) {
            this.hourstwothree.push(hours);
        }
        this.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        this.month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
            "November", "December "];
        this.monthsCount = [];
        this.dateCount = [];
        for (var dateCounts = 1; dateCounts <= 7; dateCounts++) {
            this.dateCount.push(dateCounts);
        }
        for (var monthCount = 1; monthCount <= 12; monthCount++) {
            this.monthsCount.push(monthCount);
        }
        this.numOfdays = [];
        this.days.forEach(function (element) {
            console.log(element);
            _this.numOfdays.push(element);
        });
        console.log(this.numOfdays);
        this.numOfmonths = [];
        this.month.forEach(function (element) {
            _this.numOfmonths.push(element);
        });
    };
    FormLayoutComponent.prototype.secondChange = function () {
        this.secondsViewShow = true;
        this.MinutesViewShow = false;
        this.hourViewShow = false;
        this.dayviewShow = false;
        this.monthviewShow = false;
    };
    FormLayoutComponent.prototype.minutesChange = function () {
        this.secondsViewShow = false;
        this.hourViewShow = false;
        this.MinutesViewShow = true;
        this.dayviewShow = false;
        this.monthviewShow = false;
    };
    FormLayoutComponent.prototype.hourChange = function () {
        this.hourViewShow = true;
        this.MinutesViewShow = false;
        this.secondsViewShow = false;
        this.dayviewShow = false;
        this.monthviewShow = false;
    };
    FormLayoutComponent.prototype.dayChange = function () {
        this.dayviewShow = true;
        this.hourViewShow = false;
        this.MinutesViewShow = false;
        this.secondsViewShow = false;
        this.monthviewShow = false;
    };
    FormLayoutComponent.prototype.monthChange = function () {
        this.monthviewShow = true;
        this.hourViewShow = false;
        this.MinutesViewShow = false;
        this.secondsViewShow = false;
        this.dayviewShow = false;
    };
    FormLayoutComponent.prototype.everyMinutes = function (event) {
        console.log(event, "rrr");
        this.getmin = true;
        var everyMinute = event.value;
        console.log(everyMinute, ">>>everyMinute");
        this.cropExpressionSelection.everyminuteMain = everyMinute;
        // console.log(this.onPermSelectionOption.everyminuteMain);
    };
    FormLayoutComponent.prototype.everyMinuteSelect = function (e) {
        console.log(e, "defe");
        this.getmin = true;
        this.hourExp = false;
        this.dateCExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysExp = false;
        this.cronExpression = '*/' + e.value + ' * * * *';
        console.log(this.cronExpression);
    };
    FormLayoutComponent.prototype.everySecondOfMinutes = function (e) {
        console.log(e, "dddd");
        // this.getmin = false;
        // this.hourExp = true;
        this.everySecondOfMinutesExp = e.value;
        console.log(this.everySecondOfMinutesExp);
    };
    FormLayoutComponent.prototype.selectHour = function (e) {
        this.getmin = false;
        this.hourExp = true;
        this.dateCExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysExp = false;
        this.everHourExp = e.value;
        this.cronExpression = "0 0/" + e.value + ' * * *';
        console.log(this.cronExpression);
    };
    FormLayoutComponent.prototype.dateCounts = function (e) {
        this.dateCExp = true;
        this.getmin = false;
        this.hourExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysExp = false;
        this.dateCountDetailsExp = e.value;
        console.log(this.dateCountDetailsExp);
        var dayIdx = this.days.indexOf(this.daysCountsExp);
        this.cronExpression = "0 0 */" + e.value + " * " + dayIdx + '-6';
    };
    FormLayoutComponent.prototype.daysCounts = function (e) {
        this.daysExp = true;
        this.dateCExp = false;
        this.getmin = false;
        this.hourExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysCountsExp = e.value;
        var dayIdx = this.days.indexOf(this.daysCountsExp);
        this.cronExpression = "0 0 */" + this.dateCountDetailsExp + " * " + dayIdx + '-6';
    };
    FormLayoutComponent.prototype.monthExp = function (e) {
        this.dateCExp = false;
        this.getmin = false;
        this.hourExp = false;
        this.daysExp = false;
        this.monthViewfull = true;
        this.fullMonth = true;
        this.monthexp = e.value;
        var mnthIdx = this.month.indexOf(this.monthexp);
        this.cronExpression = "0 0 1 " + mnthIdx + '/' + this.monthCountExp + " *";
    };
    FormLayoutComponent.prototype.monthCount = function (e) {
        this.dateCExp = false;
        this.daysExp = false;
        this.getmin = false;
        this.hourExp = false;
        this.monthViewfull = false;
        this.fullMonth = true;
        this.monthCountExp = e.value;
        var mnthIdx = this.month.indexOf(this.monthexp);
        this.cronExpression = "0 0 1 " + mnthIdx + '/' + e.value + " *";
    };
    FormLayoutComponent.prototype.onSelectionStartDay = function (event) { };
    FormLayoutComponent.prototype.SelectedDayCount = function (event) { };
    FormLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: "form-layout",
                    template: "<div [ngClass]=\"classDiv\">\r\n  <!--WORK BY RAMYA-->\r\n  <form [formGroup]=\"inputGroup\" [ngClass]=\"dynamicFormHeight?dynamicFormHeight:initialHeight\"\r\n    class=\"sentri-form-height\" fxflex=\"1 1 auto\">\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"enableOptionFields\">\r\n      <div *ngFor=\"let optionField of formValues.chooseOneFields\"\r\n        style=\"display: flex;margin-top:12px;margin-bottom:20px;\">\r\n        <div *ngIf=\"optionField.label\" class=\"mr-8\" [ngClass]=\"optionField.widthBase\" style=\"width: 20%;\">\r\n          <mat-label class=\"mr-8 mr-res-5btm\"\r\n            style=\"font-size : 14px;padding: 0px 0px;margin-top:12px;margin-bottom:10px\">\r\n            {{optionField.label | translate}}:\r\n          </mat-label>\r\n        </div>\r\n        <mat-radio-group (change)=\"onOptionChange($event, optionField)\" [value]=\"inputData[optionField.name]\" required>\r\n          <mat-radio-button [ngClass]=\"fieldData.resWidth\"\r\n            [ngStyle]=\"{'width': fieldData.width, 'padding' : fieldData.padding}\"\r\n            style=\"width: 40%; padding: 0 0 10px 60px;\" *ngFor=\"let fieldData of optionField.fields\"\r\n            value=\"{{fieldData[optionField.keyToSave] | translate}}\"><span\r\n              class=\"p-4\">{{fieldData[optionField.keyToShow] | translate}}</span>&nbsp;&nbsp;\r\n          </mat-radio-button>\r\n        </mat-radio-group>\r\n\r\n        <p *ngIf=\"optionField.validations && submitted && inputGroup.get(optionField.name) && inputGroup.get(optionField.name).invalid\"\r\n          style=\"padding-top: 14px;\">\r\n          <mat-error class=\"error_margin\">\r\n            {{optionField.validations.message | translate}}\r\n          </mat-error>\r\n        </p>\r\n      </div>\r\n    </div>\r\n    <!-- <br> -->\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formHeader\">\r\n      <div class=\"h2 mb-6 ui-common-lib-f17\">{{formValues.formHeader | translate}}</div>\r\n    </div>\r\n    <!-- <div class=\"row\" md-content layout-padding *ngIf=\"enableTextFields\"> -->\r\n\r\n    <!--form new layout  -->\r\n    <div [ngClass]=\"formValues.listDetail ? 'sen-lib-list-view' : 'row'\" >\r\n      <!-- formtext -->\r\n      <ng-container *ngIf=\"enableTextFields\">\r\n        <ng-container *ngFor=\"let textField of formValues.textFields\">\r\n          <div class=\"col-md-6\">\r\n            <mat-form-field class=\"full-width\" appearance=\"outline\">\r\n              <mat-label>{{textField.label | translate}}</mat-label>\r\n              <input matInput [(ngModel)]=\"inputData[textField.name]\" [maxlength]=\"textField.maxlength\"\r\n                (input)=\"onSearchName($event.target.value, textField)\" [formControlName]=\"textField.name\"\r\n                [type]=\"textField.inputType\" [required]=\"textField.validations\" [readonly]=\"textField.isReadonly\">\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"!isNameAvailable\">\r\n                {{textField.existNameMessage | translate}}\r\n              </mat-error>\r\n\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.validations && submitted && inputGroup.get(textField.name).invalid\">\r\n                {{textField.validations.message | translate}}\r\n              </mat-error>\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.inputType=='email' && inputGroup.get(textField.name).invalid && (inputGroup.get(textField.name).dirty || inputGroup.get(textField.name).touched)\">\r\n                {{textField.emailValidate | translate}}\r\n              </mat-error>\r\n            </p>\r\n\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- end text -->\r\n      <ng-container *ngIf=\"enableSelectFields\">\r\n        <ng-container *ngFor=\"let selectField of formValues.selectFields\">\r\n          <div class=\"col-md-6\">\r\n            <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n              {{selectField.sideLabel| translate}}</mat-label>\r\n            <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n              {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n            <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{selectField.label |translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                [required]=\"selectField.validations\" [disabled]=\"selectField.disable\">\r\n                <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                  value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                  {{item[selectField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                {{selectField.validations.message | translate}}\r\n              </mat-error>\r\n            </p>\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"formValues.autoCompleteFields\">\r\n        <ng-container *ngFor=\"let inputField of formValues.autoCompleteFields; let i = index;\">\r\n          <div class=\"col\" [ngStyle]=\"{'width': inputField.width}\">\r\n            <mat-form-field appearance=\"outline\" class=\"full-width\" style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{inputField.label | translate}}</mat-label>\r\n              <input matInput [formControlName]=\"inputField.name\"\r\n                (input)=\"onAutoCompleteSearch($event.target.value,inputField)\" [disabled]=\"inputField.disable\"\r\n                [matAutocomplete]=\"auto\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)='onSelectAutoComplete($event.option.value, i, inputField)'>\r\n                <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.dataKeyToSave]\"\r\n                  [value]=\"fieldValue[inputField.keyToSave]\">\r\n                  {{fieldValue[inputField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <!-- <ng-container *ngIf=\"formValues.enableAddButton\">\r\n              <button mat-mini-fab (click)=\"addNewValue()\">\r\n                <mat-icon class=\"white-icon\">add</mat-icon>\r\n              </button>\r\n            </ng-container> -->\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"formValues.enableAddButton\">\r\n        <button mat-mini-fab [ngClass]=\"formValues.UIaddButtonClass\" (click)=\"addNewValue()\">\r\n          <mat-icon class=\"white-icon\">add</mat-icon>\r\n        </button>\r\n      </ng-container>\r\n      <div layout=\"row\" class=\"col-12\" md-content layout-padding\r\n        *ngIf=\"formValues.additionalOptionFields && formValues.additionalOptionFields.length\">\r\n        <div *ngFor=\"let optionField of formValues.additionalOptionFields\"\r\n          style=\"display: flex;margin-top:12px;margin-bottom:12px;\">\r\n          <div *ngIf=\"optionField.label\" class=\"mr-8\" [ngClass]=\"optionField.widthBase\" style=\"width: 20%;\">\r\n            <mat-label class=\"mr-8\" style=\"font-size : 14px;padding: 0px 0px;margin-top:12px;margin-bottom:10px\">\r\n              {{optionField.label | translate}}:\r\n            </mat-label>\r\n          </div>\r\n          <mat-radio-group (change)=\"onOptionChange($event, optionField)\" [value]=\"inputData[optionField.name]\"\r\n            required>\r\n            <mat-radio-button *ngFor=\"let fieldData of optionField.fields\"\r\n              value=\"{{fieldData[optionField.keyToSave] | translate}}\"><span\r\n                class=\"p-4\">{{fieldData[optionField.keyToShow] | translate}}</span>&nbsp;&nbsp;\r\n            </mat-radio-button>\r\n          </mat-radio-group>\r\n\r\n          <p *ngIf=\"optionField.validations && submitted && inputGroup.get(optionField.name) && inputGroup.get(optionField.name).invalid\"\r\n            style=\"padding-top: 14px;\">\r\n            <mat-error class=\"error_margin\">\r\n              {{optionField.validations.message | translate}}\r\n            </mat-error>\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <!-- enable select fields -->\r\n      \r\n      <!-- multi -->\r\n      <ng-container *ngIf=\"enableMultiselectFields\">\r\n        <ng-container *ngFor=\"let selectField of formValues.multiSelectFields\">\r\n          <ng-container>\r\n            <!-- <td class=\"column\" > -->\r\n            <div class=\"col-6\">\r\n              <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n                {{selectField.sideLabel| translate}}</mat-label>\r\n              <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n                {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n              <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n                <mat-label>{{selectField.label |translate}}</mat-label>\r\n                <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                  [required]=\"selectField.validations\" multiple>\r\n                  <mat-option *ngFor=\"let item of selectField.data\" value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                    {{item[selectField.keyToShow] | translate}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n              <p class=\"errorP\">\r\n                <mat-error class=\"error_margin\"\r\n                  *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                  {{selectField.validations.message | translate}}\r\n                </mat-error>\r\n              </p>\r\n            </div>\r\n            <!-- </td> -->\r\n          </ng-container>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- chiplist -->\r\n      <ng-container *ngIf=\"formValues.chipFields && formValues.chipFields.length > 0 \">\r\n        <ng-container *ngFor=\"let chipField of formValues.chipFields\">\r\n          <div class=\"col-md-6\" [ngClass]=\"chipField.addFull\">\r\n            <mat-form-field appearance=\"outline\" [ngClass]=\"chipField.addFull ? 'half-width' : 'full-width'\"\r\n              style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{chipField.label | translate}}</mat-label>\r\n              <!-- <p>{{selectedChips[chipField.name] | json}}</p> -->\r\n              <input matInput #chipInput [formControlName]=\"chipField.name\"\r\n                *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable\"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <input matInput #chipInput [formControlName]=\"chipField.name\" *ngIf=\"!chipField.chipCondition \"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)=\"onChipSelect($event, chipField.name, chipField.keyToSave,chipField.chipCondition)\">\r\n                <mat-option *ngFor=\"let item of chipField.data \" [value]=\"item\">\r\n                  {{item[chipField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"chipField.validations && submitted && chipValiditaion\">\r\n                {{chipField.validations.message | translate}}\r\n              </mat-error>\r\n            </p>\r\n            <mat-chip-list #chipList>\r\n              <ng-container\r\n                *ngIf=\"!chipField.accordion && inputData.selectedChips && inputData.selectedChips[chipField.name] && inputData.selectedChips[chipField.name].length\">\r\n                <div *ngFor=\"let chip of inputData.selectedChips[chipField.name]; let i = index;\">\r\n                  <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n                    (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n                    {{chip[chipField.keyToShow]}}\r\n                    <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                  </mat-chip>\r\n                  <span\r\n                    *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n                  <span\r\n                    *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n                </div>\r\n              </ng-container>\r\n            </mat-chip-list>\r\n\r\n            <mat-accordion\r\n              *ngIf=\"chipField.accordion && inputData.selectedChips && inputData.selectedChips[chipField.name] && inputData.selectedChips[chipField.name].length\">\r\n              <mat-expansion-panel [expanded]=\"panelOpenState === true\" (opened)=\"panelOpenState = true\"\r\n                (closed)=\"panelOpenState = false\" *ngIf=\"chipListOpenView\" [class.sentri-active]=\"chipField.accordion\">\r\n                <mat-expansion-panel-header>\r\n                  <mat-panel-title>\r\n                    Selected Roles\r\n                  </mat-panel-title>\r\n                  <mat-panel-description>\r\n\r\n                  </mat-panel-description>\r\n                </mat-expansion-panel-header>\r\n                <mat-chip-list #chipList>\r\n                  <div *ngFor=\"let chip of inputData.selectedChips[chipField.name]; let i = index;\">\r\n                    <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n                      (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n                      {{chip[chipField.keyToShow]}}\r\n                      <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                    </mat-chip>\r\n                    <span\r\n                      *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n                    <span\r\n                      *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n                  </div>\r\n                </mat-chip-list>\r\n              </mat-expansion-panel>\r\n            </mat-accordion>\r\n          </div>\r\n\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- end -->\r\n\r\n      <!-- end  -->\r\n      <!-- confirmation section -->\r\n      <div class=\"col-md-12\">\r\n        <ng-container *ngIf=\"formValues.confirmData\">\r\n          <ng-container *ngIf=\"formValues.confirmDetailsHeading\">\r\n            <h6 class=\"mb-6 ui-common-lib-confirmation-header\">{{formValues.confirmDetailsHeading | translate}}</h6>\r\n          </ng-container>\r\n          <div class=\"row\">\r\n            <ng-container *ngFor=\"let confirmField of formValues.confirmData\">\r\n              <div class=\"col-md-6\">\r\n                <p style=\"padding-left: 12px;padding-top: 12px;\">\r\n                  <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}:</b> </mat-label>\r\n                  <!-- <span *ngIf=\"confirmField.type == 'date'\"\r\n                    style=\"padding-left: 5px;\">{{viewData[confirmField.value] | date:'short'}}</span> -->\r\n                    <span *ngIf=\"confirmField.type == 'date' && confirmField.dateFormat\"\r\n                    style=\"padding-left: 5px;\">\r\n                    {{viewData[confirmField.value] | date: confirmField.dateFormat}}\r\n                   </span>\r\n                   <span *ngIf=\"confirmField.type == 'date' && !confirmField.dateFormat\"\r\n                    style=\"padding-left: 5px;\">\r\n                    {{viewData[confirmField.value] | date:'short'}}\r\n                   </span>\r\n                  <span *ngIf=\"confirmField.type != 'date'\"\r\n                    style=\"padding-left: 5px;\">{{viewData[confirmField.value]}}</span>\r\n                </p>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n          <!-- <td class=\"column\" style=\"padding: 10px;\" *ngFor=\"let confirmField of formValues.confirmData\">\r\n            <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}</b> </mat-label>\r\n            <span *ngIf=\"confirmField.type == 'date'\"\r\n              style=\"padding: 10%;\">{{viewData[confirmField.value] | date:'short'}}</span>\r\n            <span *ngIf=\"confirmField.type != 'date'\" style=\"padding: 10%;\">{{viewData[confirmField.value]}}</span>\r\n            <br>\r\n          </td> -->\r\n        </ng-container>\r\n      </div>\r\n      <!-- end conf -->\r\n      <!-- schedule form view design -->\r\n      <ng-container *ngIf=\"formValues.scheduleFields\">\r\n        <div class=\"col-md-6\">\r\n          <mat-form-field class=\"full-width\" appearance=\"outline\" floatLabel=\"always\" style=\"padding-right: 1.7%;\">\r\n            <!-- <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\"> -->\r\n            <mat-label>{{scheduleField.startDateLabel | translate}}</mat-label>\r\n            <input matInput [matDatepicker]=\"startDatePicker\" [(ngModel)]=\"inputData[scheduleField.startDate]\"\r\n              (dateChange)=\"validateDate('change', $event,scheduleField)\" required\r\n              [formControlName]=\"scheduleField.startDate\" name=\"startdatetime\" [min]=\"minDate\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"startDatePicker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #startDatePicker></mat-datepicker>\r\n          </mat-form-field>\r\n          <p class=\"errorP\">\r\n            <mat-error class=\"error_margin\" *ngIf=\"formValues.scheduleFields.validations && !dateFormatcheck\">\r\n              {{formValues.scheduleFields.validations.datemessage | translate}}\r\n            </mat-error>\r\n          </p>\r\n        </div>\r\n        <div class=\"col-md-3\">\r\n          <mat-form-field class=\"full-width\" appearance=\"outline\" class=\"pr-4\" floatLabel=\"always\" ngDefaultControl>\r\n            <!-- <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\"> -->\r\n            <mat-label>{{scheduleField.timeLabel | translate}}</mat-label>\r\n            <input matInput type=\"time\" [(ngModel)]=\"inputData[scheduleField.time]\"\r\n              [formControlName]=\"scheduleField.time\" min=\"01:00\" max=\"12:00\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-3 sentri-responsive-col-3\">\r\n          <mat-checkbox style=\"position: relative;top:25px;\" (change)=\"showRepeatData($event)\">\r\n            {{scheduleField.repeatLabel | translate}}</mat-checkbox>\r\n        </div>\r\n        <ng-container *ngIf=\"enableRepeatData\">\r\n          <div class=\"col-md-9\">\r\n            <mat-form-field class=\"full-width\" appearance=\"outline\" style=\"padding-top: 3%;\">\r\n              <mat-label>{{scheduleField.selectRepeatLabel | translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData.repeatType\" [formControlName]=\"scheduleField.repeatType\">\r\n                <mat-option *ngFor=\"let itemData of scheduleField.repeatData\" value=\"{{itemData.value}}\"\r\n                  (click)=\"onRepeatSelectChange(itemData.value, scheduleField.repeatData)\">\r\n                  {{itemData.name | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>Repeats is required!</mat-error>\r\n            </mat-form-field>\r\n            <ng-container *ngIf=\"inputData.weekly\">\r\n              <mat-checkbox *ngFor=\"let days of scheduleField.weekDaysData; let i = index\" class=\"example-margin\"\r\n                value=\"{{days.value}}\" [formControlName]=\"scheduleField.weeklyDays\"\r\n                (change)=\"getSelecteddays($event,days.value)\" id=\"checkbox_{{i}}\"\r\n                style=\"padding-right: 0px;padding-left: 10px;\">\r\n                {{days.name}}\r\n              </mat-checkbox>\r\n            </ng-container>\r\n          </div>\r\n          <div class=\"col-md-6\" *ngIf=\"inputData.daily\">\r\n            <ng-container>\r\n              <mat-radio-group floatLabel=\"always\" [formControlName]=\"scheduleField.repeatByDay\">\r\n                <mat-radio-button value=\"day\" (click)=\"onSelectionStartDay('day')\">\r\n                  <span style=\"margin-top: 8%;\">{{scheduleField.everyLabel | translate}}</span>&nbsp;&nbsp;\r\n                  <mat-form-field class=\"full-width\">\r\n                    <mat-label>{{scheduleField.daysLabel | translate}}</mat-label>\r\n                    <input matInput [formControlName]=\"scheduleField.repeatDaysCount\"\r\n                      (change)=\"SelectedDayCount($event)\">\r\n                  </mat-form-field>&nbsp;&nbsp;<span\r\n                    style=\"margin-top: 8%;\">{{scheduleField.daysLabel | translate}}</span>\r\n                </mat-radio-button>\r\n                <mat-radio-button value=\"week\" (click)=\"onSelectionStartDay('week')\">\r\n                  {{scheduleField.weekDayLabel | translate}}&nbsp;&nbsp;\r\n                </mat-radio-button>\r\n              </mat-radio-group>\r\n            </ng-container>\r\n          </div>\r\n          <div class=\"col-md-12\" *ngIf=\"inputData.monthly\">\r\n            <ng-container>\r\n              <span style=\"margin-top: 4%;\">{{scheduleField.dayLabel | translate}}23</span>&nbsp;&nbsp;<mat-form-field\r\n                appearance=\"outline\">\r\n                <mat-label></mat-label>\r\n                <input matInput [formControlName]=\"scheduleField.monthlyDate\">\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.everyofLabel | translate}}</span>&nbsp;&nbsp;\r\n              <mat-form-field appearance=\"outline\" style=\"width:20%;\">\r\n                <mat-label>Select</mat-label>\r\n                <mat-select placeholder=\"Select\" [formControlName]=\"scheduleField.selectMonth\">\r\n                  <mat-option *ngFor=\"let month of scheduleField.monthListData\" value=\"{{month.value}}\">\r\n                    {{month.name}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.monthsLabel | translate}}</span>\r\n            </ng-container>\r\n          </div>\r\n          <div class=\"col-md-6 sen-lib-responsive-enddate\">\r\n            <!-- <p class=\"mr-8\">{{scheduleField.endsLabel | translate}}</p> -->\r\n            <mat-radio-group floatLabel=\"always\" style=\"position: relative;top:13px;\"\r\n              [formControlName]=\"scheduleField.endDate\">\r\n              <mat-radio-button value=\"no_end_date\">\r\n                {{scheduleField.noEndDate | translate}}&nbsp;&nbsp;\r\n              </mat-radio-button>\r\n              <br>\r\n              <mat-radio-button value=\"until\" style=\"margin-top: 3%;\" (click)=\"showEndDate = true\">\r\n                <span style=\"margin-top: 8%;\">{{scheduleField.untillLabel | translate}}</span>&nbsp;&nbsp;\r\n                <mat-form-field appearance=\"outline\" floatLabel=\"always\" class=\"mr-sm-12\" fxFlex=\"45\" ngDefaultControl\r\n                  *ngIf=\"showEndDate\">\r\n                  <mat-label>{{scheduleField.endDateLabel | translate}}</mat-label>\r\n                  <input matInput [matDatepicker]=\"endDatePicker\" [formControlName]=\"scheduleField.endDateTime\"\r\n                    [min]=\"minEndDate\">\r\n                  <mat-datepicker-toggle matSuffix [for]=\"endDatePicker\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #endDatePicker></mat-datepicker>\r\n                </mat-form-field>\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </ng-container>\r\n\r\n      </ng-container>\r\n      <ng-container *ngIf=\"formValues.listDetail\">\r\n        <ng-container *ngFor=\"let item of inputData[formValues.keyToShow]\">\r\n          <ul style=\"padding-left: 15px;\">\r\n            <li>\r\n              {{item[formValues.keyToDisplay]}}\r\n            </li>\r\n          </ul>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- end schedule view -->\r\n    </div>\r\n    <!-- end new layout -->\r\n\r\n\r\n    <table class=\"full-width\" cellspacing=\"0\">\r\n      <tr class=\"row\">\r\n        <!-- <ng-container *ngIf=\"formValues.autoCompleteFields\">\r\n          <td class=\"column\" [ngStyle]=\"{'width': inputField.width}\"\r\n            *ngFor=\"let inputField of formValues.autoCompleteFields; let i = index;\">\r\n            <mat-form-field appearance=\"outline\" class=\"full-width\" style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{inputField.label | translate}}</mat-label>\r\n              <input matInput [formControlName]=\"inputField.name\"\r\n                (input)=\"onAutoCompleteSearch($event.target.value,inputField)\" [disabled]=\"inputField.disable\"\r\n                [matAutocomplete]=\"auto\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)='onSelectAutoComplete($event.option.value, i, inputField)'>\r\n                <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.dataKeyToSave]\"\r\n                  [value]=\"fieldValue[inputField.keyToSave]\">\r\n                  {{fieldValue[inputField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n          </td>\r\n          <div fxlayout=\"row\" fxlayoutalign=\"start center\" *ngIf=\"formValues.enableAddButton\">\r\n            <button mat-mini-fab (click)=\"addNewValue()\">\r\n              <mat-icon class=\"white-icon\">add</mat-icon>\r\n            </button>\r\n          </div>\r\n        </ng-container> -->\r\n        <ng-container *ngIf=\"enableTextFields\">\r\n          <td class=\"column\" *ngFor=\"let textField of formValues.textFields\">\r\n            <!-- <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{textField.label | translate}}</mat-label>\r\n              <input matInput [(ngModel)]=\"inputData[textField.name]\" [maxlength]=\"textField.maxlength\"\r\n                (input)=\"onSearchName($event.target.value, textField)\" [formControlName]=\"textField.name\"\r\n                [type]=\"textField.inputType\" [required]=\"textField.validations\" [readonly]=\"textField.isReadonly\">\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"!isNameAvailable\">\r\n                {{textField.existNameMessage | translate}}\r\n              </mat-error>\r\n              \r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.validations && submitted && inputGroup.get(textField.name).invalid\">\r\n                {{textField.validations.message | translate}}\r\n              </mat-error>\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.inputType=='email' && inputGroup.get(textField.name).invalid && (inputGroup.get(textField.name).dirty || inputGroup.get(textField.name).touched)\">\r\n                {{textField.emailValidate | translate}}\r\n              </mat-error>\r\n            </p> -->\r\n\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"formValues.enableSlideToggle\">\r\n          <div layout=\"row\" class=\"slide-toggle\" md-content layout-padding>\r\n            {{formValues.toggleLabel | translate}}\r\n            <mat-slide-toggle (change)=\"toggleChanged($event)\" class=\"slide-toggle-padding\">\r\n            </mat-slide-toggle>\r\n          </div>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"enableSelectFields\">\r\n          <td class=\"column\" *ngFor=\"let selectField of formValues.selectFields\">\r\n            <!-- <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n              {{selectField.sideLabel| translate}}</mat-label>\r\n            <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n              {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n            <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{selectField.label |translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                [required]=\"selectField.validations\">\r\n                <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                  value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                  {{item[selectField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                {{selectField.validations.message | translate}}\r\n              </mat-error>\r\n            </p> -->\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"enableMultiselectFields\">\r\n          <td class=\"column\" *ngFor=\"let selectField of formValues.multiSelectFields\">\r\n            <!-- <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n              {{selectField.sideLabel| translate}}</mat-label>\r\n            <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n              {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n            <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{selectField.label |translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                [required]=\"selectField.validations\" multiple>\r\n                <mat-option *ngFor=\"let item of selectField.data\" value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                  {{item[selectField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                {{selectField.validations.message | translate}}\r\n              </mat-error>\r\n            </p> -->\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"formValues.confirmData\">\r\n          <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.confirmDetailsHeading\">\r\n            <div class=\"h2 mb-6\">{{formValues.confirmDetailsHeading | translate}}</div>\r\n          </div>\r\n          <td class=\"column\" style=\"padding: 10px;\" *ngFor=\"let confirmField of formValues.confirmData\">\r\n            <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}</b> </mat-label>\r\n            <span *ngIf=\"confirmField.type == 'date'\"\r\n              style=\"padding: 10%;\">{{viewData[confirmField.value] | date:'short'}}</span>\r\n            <span *ngIf=\"confirmField.type != 'date'\" style=\"padding: 10%;\">{{viewData[confirmField.value]}}</span>\r\n            <br>\r\n          </td> -->\r\n        </ng-container>\r\n        <ng-container *ngIf=\"formValues.chipFields\">\r\n          <td class=\"column\" *ngFor=\"let chipField of formValues.chipFields\">\r\n            <!-- <mat-form-field appearance=\"outline\" class=\"full-width\" style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{chipField.label | translate}}</mat-label>\r\n            \r\n              <input matInput #chipInput [formControlName]=\"chipField.name\"\r\n                *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable\"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <input matInput #chipInput [formControlName]=\"chipField.name\" *ngIf=\"!chipField.chipCondition \"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)=\"onChipSelect($event, chipField.name, chipField.keyToSave,chipField.chipCondition)\">\r\n                <mat-option *ngFor=\"let item of chipField.data \" [value]=\"item\">\r\n                  {{item[chipField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"chipField.validations && submitted && chipValiditaion\">\r\n                {{chipField.validations.message | translate}}\r\n              </mat-error>\r\n            </p>\r\n            <mat-chip-list #chipList>\r\n              <div *ngFor=\"let chip of selectedChips[chipField.name]; let i = index;\">\r\n                <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n                  (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n                  {{chip[chipField.keyToShow]}}\r\n                  <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                </mat-chip>\r\n                <span\r\n                  *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n                <span\r\n                  *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n              </div>\r\n            </mat-chip-list> -->\r\n          </td>\r\n        </ng-container>\r\n      </tr>\r\n    </table>\r\n    <!-- </div> -->\r\n    <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"enableSelectFields\">\r\n    <div *ngFor=\"let selectField of formValues.selectFields\" fxFlex.gt-sm=\"50\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n      <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"30\" *ngIf=\"selectField.showSideLabel\" fxLayoutAlign=\"center\"\r\n        class=\"mr-8\">\r\n        <div layout=\"row\">\r\n          <mat-label class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" style=\"display: inline;\">\r\n        <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n          <mat-label>{{selectField.label |translate}}</mat-label>\r\n          <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n            [required]=\"selectField.validations\">\r\n            <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(selectField, item, selectField)\"\r\n              value=\"{{item[selectField.keyToShow] | translate}}\">\r\n              {{item[selectField.keyToShow] | translate}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <p *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n          <mat-error class=\"error_margin\">\r\n            {{selectField.validations.message | translate}}\r\n          </mat-error>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div> -->\r\n    <!-- <div class=\"row\" md-content layout-padding *ngIf=\"enableChipFields\">\r\n    <div class=\"column\" *ngFor=\"let chipField of formValues.chipFields\">\r\n      <mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n        <mat-label>{{chipField.label | translate}}</mat-label>\r\n        <input matInput #chipInput [formControlName]=\"chipField.name\"\r\n          *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable\"\r\n          (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\" [matChipInputFor]=\"chipList\"\r\n          (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n        <input matInput #chipInput [formControlName]=\"chipField.name\" *ngIf=\"!chipField.chipCondition \"\r\n          (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\" [matChipInputFor]=\"chipList\"\r\n          (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\"\r\n          (optionSelected)=\"onChipSelect($event, chipField.name, chipField.keyToSave,chipField.chipCondition)\">\r\n          <mat-option *ngFor=\"let item of chipField.data \" [value]=\"item\">\r\n            {{item[chipField.keyToShow] | translate}}\r\n          </mat-option>\r\n        </mat-autocomplete>\r\n      </mat-form-field>\r\n      <p class=\"errorP\">\r\n        <mat-error class=\"error_margin\"\r\n          *ngIf=\"chipField.validations && submitted && chipValiditaion\">\r\n          {{chipField.validations.message | translate}}\r\n        </mat-error>\r\n      </p>\r\n      <mat-chip-list #chipList>\r\n        <div *ngFor=\"let chip of selectedChips[chipField.name]; let i = index;\">\r\n          <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n            (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n            {{chip[chipField.keyToShow]}}\r\n            <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n          </mat-chip>\r\n          <span\r\n            *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n          <span\r\n            *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n        </div>\r\n      </mat-chip-list>\r\n    </div>\r\n  </div> -->\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.scheduleFields\">\r\n      <div>\r\n        <div>\r\n          <!-- <mat-form-field appearance=\"outline\" floatLabel=\"always\" fxFlex=\"60\" style=\"padding-right: 1.7%;\">\r\n            <mat-label>{{scheduleField.startDateLabel | translate}}</mat-label>\r\n            <input matInput [matDatepicker]=\"startDatePicker\" [(ngModel)]=\"inputData[scheduleField.startDate]\"\r\n              (dateChange)=\"validateDate('change', $event,scheduleField)\" required\r\n              [formControlName]=\"scheduleField.startDate\" name=\"startdatetime\" [min]=\"minDate\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"startDatePicker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #startDatePicker></mat-datepicker>\r\n          </mat-form-field>\r\n\r\n          <mat-form-field appearance=\"outline\" fxFlex=23 class=\"pr-4\" floatLabel=\"always\" ngDefaultControl>\r\n            <mat-label>{{scheduleField.timeLabel | translate}}</mat-label>\r\n            <input matInput type=\"time\" [(ngModel)]=\"inputData[scheduleField.time]\"\r\n              [formControlName]=\"scheduleField.time\" min=\"01:00\" max=\"12:00\">\r\n          </mat-form-field> -->\r\n        </div>\r\n\r\n        <!-- <p class=\"errorP\">\r\n          <mat-error class=\"error_margin\" *ngIf=\"formValues.scheduleFields.validations && !dateFormatcheck\">\r\n            {{formValues.scheduleFields.validations.datemessage | translate}}\r\n          </mat-error>\r\n        </p>\r\n\r\n        <div>\r\n          <mat-checkbox (change)=\"showRepeatData($event)\">{{scheduleField.repeatLabel | translate}}</mat-checkbox>\r\n        </div> -->\r\n\r\n        <div layout=\"row\" md-content layout-padding *ngIf=\"enableRepeatData\">\r\n          <!-- <div>\r\n            <mat-form-field appearance=\"outline\" fxFlex=\"85\" style=\"padding-top: 3%;\">\r\n              <mat-label>{{scheduleField.selectRepeatLabel | translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData.repeatType\" [formControlName]=\"scheduleField.repeatType\">\r\n                <mat-option *ngFor=\"let itemData of scheduleField.repeatData\" value=\"{{itemData.value}}\"\r\n                  (click)=\"onRepeatSelectChange(itemData.value, scheduleField.repeatData)\">\r\n                  {{itemData.name | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>Repeats is required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div>\r\n            <mat-radio-group floatLabel=\"always\" *ngIf=\"inputData.daily\" [formControlName]=\"scheduleField.repeatByDay\">\r\n              <mat-radio-button value=\"day\" (click)=\"onSelectionStartDay('day')\">\r\n                <span style=\"margin-top: 8%;\">{{scheduleField.everyLabel | translate}}</span>&nbsp;&nbsp;<mat-form-field\r\n                  appearance=\"outline\" fxFlex=\"20\">\r\n                  <mat-label>{{scheduleField.daysLabel | translate}}</mat-label>\r\n                  <input matInput [formControlName]=\"scheduleField.repeatDaysCount\" (change)=\"SelectedDayCount($event)\">\r\n                </mat-form-field>&nbsp;&nbsp;<span\r\n                  style=\"margin-top: 8%;\">{{scheduleField.daysLabel | translate}}</span>\r\n              </mat-radio-button>\r\n              <br>\r\n              <mat-radio-button value=\"week\" (click)=\"onSelectionStartDay('week')\">\r\n                {{scheduleField.weekDayLabel | translate}}&nbsp;&nbsp;\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n            <div *ngIf=\"inputData.weekly\">\r\n              <mat-checkbox *ngFor=\"let days of scheduleField.weekDaysData; let i = index\" class=\"example-margin\"\r\n                value=\"{{days.value}}\" [formControlName]=\"scheduleField.weeklyDays\"\r\n                (change)=\"getSelecteddays($event,days.value)\" id=\"checkbox_{{i}}\"\r\n                style=\"padding-right: 0px;padding-left: 10px;\">\r\n                {{days.name}}\r\n              </mat-checkbox>\r\n            </div>\r\n            <div *ngIf=\"inputData.monthly\">\r\n              <span style=\"margin-top: 4%;\">{{scheduleField.dayLabel | translate}}</span>&nbsp;&nbsp;<mat-form-field\r\n                appearance=\"outline\" fxFlex=\"28\">\r\n                <mat-label></mat-label>\r\n                <input matInput [formControlName]=\"scheduleField.monthlyDate\">\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.everyofLabel | translate}}</span>&nbsp;&nbsp;\r\n              <mat-form-field appearance=\"outline\" fxFlex=\"28\">\r\n                <mat-label>Select</mat-label>\r\n                <mat-select placeholder=\"Select\" [formControlName]=\"scheduleField.selectMonth\">\r\n                  <mat-option *ngFor=\"let month of scheduleField.monthListData\" value=\"{{month.value}}\">\r\n                    {{month.name}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.monthsLabel | translate}}</span>\r\n            </div>\r\n          </div>\r\n          <br>\r\n          <div>\r\n            <mat-label class=\"mr-8\">{{scheduleField.endsLabel | translate}}</mat-label>&nbsp;\r\n            <br>\r\n            <mat-radio-group floatLabel=\"always\" [formControlName]=\"scheduleField.endDate\">\r\n              <mat-radio-button value=\"no_end_date\">\r\n                {{scheduleField.noEndDate | translate}}&nbsp;&nbsp;\r\n              </mat-radio-button>\r\n              <br>\r\n              <mat-radio-button value=\"until\" style=\"margin-top: 3%;\" (click)=\"showEndDate = true\">\r\n                <span style=\"margin-top: 8%;\">{{scheduleField.untillLabel | translate}}</span>&nbsp;&nbsp;\r\n                <mat-form-field appearance=\"outline\" floatLabel=\"always\" class=\"mr-sm-12\" fxFlex=\"45\" ngDefaultControl\r\n                  *ngIf=\"showEndDate\">\r\n                  <mat-label>{{scheduleField.endDateLabel | translate}}</mat-label>\r\n                  <input matInput [matDatepicker]=\"endDatePicker\" [formControlName]=\"scheduleField.endDateTime\"\r\n                    [min]=\"minEndDate\">\r\n                  <mat-datepicker-toggle matSuffix [for]=\"endDatePicker\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #endDatePicker></mat-datepicker>\r\n                </mat-form-field>\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div> -->\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.dateFields\">\r\n      <div fxLayout=\"column\" fxFlex=\"100\" *ngFor=\"let dateField of formValues.dateFields; let i = index;\"\r\n        fxFlex.gt-sm=\"50\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n        <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n          <mat-label>{{dateField.dateLabel | translate}}</mat-label>\r\n          <input matInput [matDatepicker]=\"i\" [formControlName]=\"dateField.dateName\" [min]=\"minDate\"\r\n            [(ngModel)]=\"inputData[dateField.dateName]\" *ngIf=\"dateField.dateName=='startDate' && !inputData['_id']\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n          <input matInput [matDatepicker]=\"i\" [formControlName]=\"dateField.dateName\"\r\n            [(ngModel)]=\"inputData[dateField.dateName]\" *ngIf=\"dateField.dateName=='startDate' && inputData['_id']\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n\r\n          <input *ngIf=\"dateField.dateName=='endDate'\" matInput [matDatepicker]=\"i\"\r\n            [formControlName]=\"dateField.dateName\" [(ngModel)]=\"inputData[dateField.dateName]\" [min]=\"minEndDate\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n          <input *ngIf=\"dateField.dateName!='endDate' && dateField.dateName!='startDate' \" matInput [matDatepicker]=\"i\"\r\n            [(ngModel)]=\"inputData[dateField.dateName]\" [formControlName]=\"dateField.dateName\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"i\"></mat-datepicker-toggle>\r\n          <mat-datepicker #i></mat-datepicker>\r\n        </mat-form-field>\r\n        <p class=\"errorP\">\r\n          <!-- <mat-error class=\"error_margin\" *ngIf=\"!dateFormatcheck\">\r\n          {{dateField.validateDateFormat | translate}}\r\n        </mat-error> -->\r\n          <mat-error class=\"error_margin\"\r\n            *ngIf=\"dateField.validations && submitted && inputGroup.get(dateField.name).invalid\">\r\n            {{dateField.validations.message | translate}}\r\n          </mat-error>\r\n        </p>\r\n      </div>\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formArrayValues\">\r\n      <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.fieldSetHeader\">\r\n        <div class=\"h2 mb-6  ui-common-lib-f17\">{{formValues.fieldSetHeader | translate}}</div>\r\n      </div>\r\n      <div style=\"float: right; margin-bottom: -16px;margin-top: -25px;\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n        *ngIf=\"!formValues.singleField\">\r\n        <button mat-mini-fab class=\"ui-common-lib-outline\" (click)=\"addNewFormArray(formValues.formFieldName)\">\r\n          <mat-icon class=\"white-icon\">add</mat-icon>\r\n        </button>\r\n      </div>\r\n      <div [formArrayName]=\"formValues.formFieldName\">\r\n        <div *ngFor=\"let comp of inputGroup.get(formValues.formFieldName)['controls']; let i=index\">\r\n\r\n          <fieldset>\r\n            <!-- <div *ngIf=\"formValues.singleField\">\r\n            <legend>  <h3>Condition : </h3></legend>\r\n           </div>\r\n           <div *ngIf=\"!formValues.singleField\">\r\n            <legend>  <h3>Condition {{i+1}}: </h3></legend>\r\n           </div> -->\r\n            <legend>\r\n              <span *ngIf=\"!formValues.singleField\">\r\n                <h3>Condition {{i+1}}:</h3>\r\n              </span>\r\n              <span *ngIf=\"formValues.singleField\">\r\n                <h3>Condition:</h3>\r\n              </span>\r\n\r\n            </legend>\r\n            <div [formGroupName]=\"i\">\r\n              <!-- condition form -->\r\n              <div class=\"row\">\r\n                <ng-container *ngIf=\"formValues.formArrayValues.selectFields\">\r\n                  <ng-container *ngFor=\"let selectField of formValues.formArrayValues.selectFields\">\r\n                    <div [ngClass]=\"(selectField && selectField.className) ? selectField.className : 'col-md-6'\">\r\n                      <ng-container *ngIf=\"selectField.showSideLabel\">\r\n                        <div>\r\n                          <mat-label class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n                        </div>\r\n                      </ng-container>\r\n                      <mat-form-field class=\"full-width\" appearance=\"outline\" class=\"custom-mat-form\">\r\n                        <mat-label>{{selectField.label |translate}}</mat-label>\r\n                        <mat-select [formControlName]=\"selectField.name\" [required]=\"selectField.validations\">\r\n                          <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                            value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                            {{item[selectField.keyToShow] | translate}}\r\n                          </mat-option>\r\n                        </mat-select>\r\n                      </mat-form-field>\r\n                    </div>\r\n\r\n                  </ng-container>\r\n                </ng-container>\r\n                <ng-container *ngIf=\"formValues.formArrayValues.autoCompleteFields\">\r\n                  <ng-container *ngFor=\"let inputField of formValues.formArrayValues.autoCompleteFields\">\r\n                    <div [ngClass]=\"(inputField && inputField.className) ? inputField.className : 'col-md-6'\">\r\n                      <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                        <mat-label>Enter Value</mat-label>\r\n                        <input matInput #accessInput [formControlName]=\"inputField.name\"\r\n                          (input)=\"onSearchChange($event.target.value,inputField)\" [matAutocomplete]=\"auto\">\r\n                        <mat-autocomplete #auto=\"matAutocomplete\">\r\n                          <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.name]\"\r\n                            [value]=\"fieldValue[inputField.keyToSave]\">\r\n                            {{fieldValue[inputField.keyToShow] | translate}}\r\n                          </mat-option>\r\n                        </mat-autocomplete>\r\n                      </mat-form-field>\r\n                    </div>\r\n                  </ng-container>\r\n                </ng-container>\r\n              </div>\r\n              <!-- end condition form -->\r\n              <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formArrayValues.selectFields\">\r\n                <div *ngFor=\"let selectField of formValues.formArrayValues.selectFields\" fxFlex.gt-sm=\"50\"\r\n                  fxLayoutAlign=\"center\" class=\"mr-8\">\r\n                  <!-- <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"30\" *ngIf=\"selectField.showSideLabel\"\r\n                    fxLayoutAlign=\"center\" class=\"mr-8\">\r\n                    <div layout=\"row\">\r\n                      <mat-label class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n                    </div>\r\n                  </div> -->\r\n                  <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\"\r\n                    style=\"display: inline;\">\r\n                    <!-- <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                      <mat-label>{{selectField.label |translate}}</mat-label>\r\n                      <mat-select [formControlName]=\"selectField.name\" [required]=\"selectField.validations\">\r\n                        <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                          value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                          {{item[selectField.keyToShow] | translate}}\r\n                        </mat-option>\r\n                      </mat-select>\r\n                    </mat-form-field> -->\r\n                    <!-- <p *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                  <mat-error class=\"error_margin\">\r\n                    {{selectField.validations.message | translate}}\r\n                  </mat-error>\r\n                </p> -->\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formArrayValues.autoCompleteFields\">\r\n                <div *ngFor=\"let inputField of formValues.formArrayValues.autoCompleteFields\" fxFlex.gt-sm=\"50\"\r\n                  fxLayoutAlign=\"center\" class=\"mr-8\">\r\n                  <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\"\r\n                    style=\"display: inline;\">\r\n                    <!-- <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                      <mat-label>Enter Value</mat-label>\r\n                      <input matInput #accessInput [formControlName]=\"inputField.name\"\r\n                        (input)=\"onSearchChange($event.target.value,inputField)\" [matAutocomplete]=\"auto\">\r\n                      <mat-autocomplete #auto=\"matAutocomplete\">\r\n                        <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.name]\"\r\n                          [value]=\"fieldValue[inputField.keyToSave]\">\r\n                          {{fieldValue[inputField.keyToShow] | translate}}\r\n                        </mat-option>\r\n                      </mat-autocomplete>\r\n                    </mat-form-field> -->\r\n                    <!-- <p *ngIf=\"inputField.validations && submitted && inputGroup.get(inputField.name).invalid\">\r\n                  <mat-error class=\"error_margin\">\r\n                    {{inputField.validations.message | translate}}\r\n                  </mat-error>\r\n                </p> -->\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </fieldset>\r\n        </div>\r\n      </div><br>\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=enableTabs>\r\n      <mat-tab-group (selectedTabChange)=\"changeTab($event)\">\r\n        <mat-tab *ngFor=\"let tab of formValues.tabData;let index = index\" [label]=\"tab.label | translate\">\r\n          <div layout=\"row\" md-content layout-padding>\r\n            <div fxLayout=\"column\" fxFlex=\"100\" *ngFor=\"let textField of tab.textFields\" fxFlex.gt-sm=\"50\"\r\n              fxLayoutAlign=\"center\" class=\"mr-8\">\r\n              <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                <mat-label>{{textField.label | translate}}</mat-label>\r\n                <input matInput>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div *ngIf=\"enableTableLayout\">\r\n            <div *ngIf=\"currentTableLoad.errorShow\" style=\"padding-top: 15px;padding-inline-start: 26px;\">\r\n              <mat-panel-title>\r\n                Total:&nbsp;{{currentTableLoad.errorShow.total}}&nbsp;&nbsp;&nbsp;SaveCount:&nbsp;{{currentTableLoad.errorShow.saveCount}}&nbsp;&nbsp;&nbsp;FailedCount:&nbsp;{{currentTableLoad.errorShow.failedCount}}&nbsp;&nbsp;&nbsp;DuplicateCount:&nbsp;{{currentTableLoad.errorShow.duplicateCount}}\r\n              </mat-panel-title>\r\n            </div>\r\n            <ng-container *ngFor=\"let tableItem of tableList\">\r\n              <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n                [viewFrom]=\"'create'\" [onLoad]=\"currentTableLoad\"></table-layout>\r\n            </ng-container>\r\n          </div>\r\n        </mat-tab>\r\n      </mat-tab-group>\r\n    </div>\r\n    <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.confirmData\">\r\n    <div fxLayout=\"column\" fxFlex=\"50\" fxLayoutAlign=\"center\" class=\"mr-8\"\r\n      *ngFor=\"let confirmField of formValues.confirmData\" fxFlex.gt-sm=\"100\">\r\n      <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}</b> </mat-label>\r\n      <span style=\"padding: 10%;\">{{viewData[confirmField.value]}}</span>\r\n    </div>\r\n  </div> -->\r\n    <div layout=\"row\" md-content layout-padding\r\n      *ngIf=\"formValues.showSelectedChips && formValues.viewSelectedChips && inputData.selectedChips\">\r\n      <div class=\"h3 mb-24 ui-common-lib-confirmation-header\">{{formValues.viewSelectedChips.heading | translate}}\r\n      </div>\r\n      <mat-chip-list #chipList>\r\n        <ng-container\r\n          *ngFor=\"let chip of inputData.selectedChips[formValues.viewSelectedChips.chipId]; let ind = index\">\r\n          <!-- <p>{{inputData | json}}</p> -->\r\n          <mat-chip role=\"option\" id=\"matchipID\">\r\n            {{chip[formValues.viewSelectedChips.keyToShow]}}\r\n          </mat-chip>\r\n          <span\r\n            *ngIf=\"inputData.operators && inputData.operators.length && (ind !== inputData.selectedChips[formValues.viewSelectedChips.chipId].length-1)\">{{inputData[\"chipOperator\"] | translate}}</span>\r\n          <span\r\n            *ngIf=\"!inputData.operators && !inputData['chipOperator'] && (ind !== inputData.selectedChips[formValues.viewSelectedChips.chipId].length-1)\">{{formValues.viewSelectedChips.operator | translate}}</span>\r\n          <span\r\n            *ngIf=\"!inputData.operators && inputData['chipOperator']  && (ind !== inputData.selectedChips[formValues.viewSelectedChips.chipId].length-1)\">{{inputData[\"chipOperator\"] | translate}}</span>\r\n\r\n        </ng-container>\r\n      </mat-chip-list>\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding\r\n      *ngIf=\"formValues.showSelectedChips && formValues.viewSelectedChipsArray &&  inputData.selectedChips\">\r\n      <div *ngFor=\"let viewChip of formValues.viewSelectedChipsArray\">\r\n        <div class=\"h3 mb-24 ui-common-lib-confirmation-header\">{{viewChip.heading | translate}}\r\n        </div>\r\n        <mat-chip-list #chipList>\r\n          <ng-container *ngFor=\"let chip of inputData.selectedChips[viewChip.chipId]; let ind = index\">\r\n            <mat-chip role=\"option\" id=\"matchipID\">\r\n              {{viewChip.keyToShow ? chip[viewChip.keyToShow] : chip}}\r\n            </mat-chip>\r\n            <span\r\n            *ngIf=\"viewChip.operatorsCheck && inputData.chipOperator && (ind !== inputData.selectedChips[viewChip.chipId].length-1)\">{{inputData[\"chipOperator\"] | translate}}</span>\r\n          </ng-container>\r\n        </mat-chip-list>\r\n      </div>\r\n\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"enableSelectValueView\">\r\n      <div layout=\"row\" md-content layout-padding style=\"padding-top: 25px;\">\r\n        <ng-container *ngIf=\"formValues.selected_view_heading \">\r\n          <div class=\"h2 mb-6 ui-common-lib-confirmation-header\">{{formValues.selected_view_heading | translate}}</div>\r\n        </ng-container>\r\n      </div>\r\n      <ng-container *ngFor=\"let tableItem of selectedTableView\">\r\n        <div style=\"margin:-16px;z-index: 1;\">\r\n          <table-layout [tableId]=\"tableItem.tableId\" (actionClickEvent)=\"deleteClick($event)\" [viewFrom]=\"'view'\"\r\n            [onLoad]=\"selectTableLoad\"></table-layout>\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n    <div layout=\"row\" *ngIf=\"errorShow.logsArr?.length > 0\" class=\"p-12\" md-content layout-padding>\r\n      <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\">\r\n        <mat-accordion>\r\n          <mat-expansion-panel (opened)=\"panelOpenState = true\" (closed)=\"panelOpenState = false\">\r\n            <mat-expansion-panel-header>\r\n              <mat-panel-title>\r\n                Total:&nbsp;{{errorShow.total}}&nbsp;&nbsp;&nbsp;SaveCount:&nbsp;{{errorShow.saveCount}}&nbsp;&nbsp;&nbsp;FailedCount:&nbsp;{{errorShow.failedCount}}&nbsp;&nbsp;&nbsp;DuplicateCount:&nbsp;{{errorShow.duplicateCount}}\r\n              </mat-panel-title>\r\n            </mat-expansion-panel-header>\r\n            <div class=\"import-error-view\">\r\n              <ng-container *ngFor=\"let tableItem of errorTableList\">\r\n                <table-layout [tableId]=\"tableItem.tableId\" [disablePagination]=\"true\" [viewFrom]=\"'import'\"\r\n                  [onLoad]=\"errorLogTable\"></table-layout>\r\n              </ng-container>\r\n            </div>\r\n          </mat-expansion-panel>\r\n        </mat-accordion>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"enableImportData\" style=\"padding : 13px 0 0 0;margin-top:-23px;\">\r\n      <div layout=\"row\" md-content layout-padding class=\"import_border\">\r\n        <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n          <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" style=\"text-align: center;\"\r\n            class=\"mr-8\">\r\n            <div class=\"board-list-item add-new-board ng-trigger ng-trigger-animate md-headline\" fxlayout=\"column\"\r\n              layout-align=\"center center\" fileDragDrop (onFileDropped)=\"onFileChange($event, importData.fileType,importData)\">\r\n              <mat-icon class=\"s-56 mat-icon material-icons import-icon\" role=\"img\" aria-hidden=\"true\">backup\r\n              </mat-icon>\r\n              <!-- <mat-icon class=\"s-56 mat-icon material-icons import-icon\" role=\"img\" aria-hidden=\"true\">unarchive\r\n              </mat-icon> -->\r\n              <div class=\"board-name\">{{'FORMS.DRAG_&_DROP_LBL' |translate}}</div>\r\n              <div class=\"board-name\" style=\"font-size:normal\">{{'FORMS.SELECT_BELOW_LBL' | translate}}</div>\r\n              <div style=\"padding: 10px 0 5px 0;\">\r\n                <button mat-button color=\"accent\" type=\"button\"\r\n                  class=\"ui-common-lib-browse-btn ui-common-lib-outline accent\" (click)=\"fileInput.click()\"\r\n                  style=\"border:2px solid\">{{'BUTTONS.BROWSE_BTN_LBL' | translate}}</button>\r\n                <input hidden type=\"file\" accept=\"'.'+importData.fileType\" required #fileInput\r\n                  (change)=\"onFileChange($event.target.files, importData.fileType,importData)\">\r\n\r\n                  <p class=\"sentri-filename\" *ngIf=\"selectedFileName\">{{selectedFileName}}</p>\r\n              </div>\r\n            </div>\r\n            <div *ngIf=\"importData.validations && submitted && !selectedFileName\">\r\n              <mat-error>\r\n                {{importData.validations.message | translate}}\r\n              </mat-error>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <div *ngIf=\"importData.uploadInfo\" class=\"import-file-info\">\r\n        <p style=\"color:#d2584f;margin-bottom:0px !important;\">\r\n          {{importData.uploadInfo | translate}}\r\n        </p>\r\n      </div>\r\n      <div *ngIf=\"importData.tagLine\">\r\n        <p style=\"color:#d2584f;margin-bottom:0px !important;\">\r\n          {{importData.tagLine | translate}}\r\n          <a target=\"_self\" [attr.href]=\"importData.sampleXlsx\" [attr.download]=\"importData.downloadFileNameXlsx\">\r\n            <span>\r\n              {{'XLSX'}}\r\n            </span>\r\n          </a> /\r\n          <a target=\"_self\" [attr.href]=\"importData.sampleFileZip\" [attr.download]=\"importData.downloadFileNameZip\">\r\n            <span>\r\n              {{'ZIP'}}\r\n            </span>\r\n          </a>\r\n        </p>\r\n      </div>\r\n      <div *ngIf=\"!importData.s3upload || importData.showSampleFormat\" layout=\"row\" md-content layout-padding>\r\n        <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\">\r\n          <p style=\"color:#2979ff;text-decoration: underline;\">\r\n            <a target=\"_self\" [attr.href]=\"importData.sampleFile\" [attr.download]=\"importData.downloadFileName\">\r\n              <span>\r\n                {{'FORMS.SAMPLE_FORMAT_LBL' | translate}}\r\n              </span>\r\n            </a>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <ng-container *ngIf=\"formValues.buttonsList && formValues.beforeTableView\">\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"end end\" [class.bottom-layout]=\"formValues.buttonsList\"\r\n        *ngIf=\"formValues.buttonsList\">\r\n        <button *ngFor=\"let buttonId of formValues.buttonsList\"\r\n          (click)=\"onButtonClick(buttonsConfigData[buttonId],formValues)\"\r\n          [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n          mat-raised-button class=\"btn-common-view\">\r\n          {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n        </button>\r\n      </div>\r\n    </ng-container>\r\n    <div layout=\"row\" md-content layout-padding\r\n      *ngIf=\"formValues.tableData && enableTableLayout && !formValues.enableNewTableLayout\">\r\n      <ng-container *ngFor=\"let tableItem of formValues.tableData\">\r\n        <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\" [viewFrom]=\"'view'\"\r\n          [onLoad]=\"currentTableLoad\"></table-layout>\r\n      </ng-container>\r\n    </div>\r\n    <div *ngIf=\"formValues.enableNewTableLayout && enableNewTableLayout\">\r\n      <ng-container *ngFor=\"let tableItem of formValues.tableData\">\r\n        <new-table-layout (checkClickEventMessage)=\"receiveClick($event)\"  [tableId]=\"tableItem.tableId\" [viewFrom]=\"'view'\" [onLoad]=\"currentTableLoad\">\r\n        </new-table-layout>\r\n      </ng-container>\r\n    </div>\r\n    <ng-container *ngIf=\"!formValues.buttonsList\">\r\n      <!-- button last day fixed next button set starting -->\r\n    </ng-container>\r\n    <ng-container *ngIf=\"formValues.buttonsList && formValues.enableFormButton\">\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"end end\" [class.bottom-layout]=\"formValues.buttonsList\"\r\n        *ngIf=\"formValues.buttonsList\">\r\n        <button *ngFor=\"let buttonId of formValues.buttonsList\"\r\n          (click)=\"onButtonClick(buttonsConfigData[buttonId],formValues)\"\r\n          [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n          mat-raised-button class=\"btn-common-view\" style=\"margin: 0 3px;\">\r\n          {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n        </button>\r\n      </div>\r\n    </ng-container>\r\n    <ng-container *ngIf=\"enableSchedule\">\r\n      <div class=\"col-md-12\">\r\n        <div class=\"sen-header-id\">\r\n          <h6>Schedule Information</h6>\r\n        </div>\r\n        <div class=\"col-md-12\" style=\"margin-top:15px;\">\r\n          <mat-radio-group formControlName=\"mainType\" [(ngModel)]=\"cropExpressionSelection.selectionOption\"\r\n            aria-label=\"Select an option\" class=\"fullwidth\">\r\n            <mat-radio-button value=\"Hours\" (change)=\"hourChange()\" style=\"margin-right:15px;\">Hours\r\n            </mat-radio-button>\r\n            <mat-radio-button value=\"Day\" (change)=\"dayChange()\" style=\"margin-right:15px;z-index: 999;\">Day\r\n            </mat-radio-button>\r\n            <mat-radio-button value=\"Month\" (change)=\"monthChange()\" style=\"margin-right:15px\">Month\r\n            </mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <ng-container *ngIf=\"MinutesViewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\">\r\n                  <mat-select formControlName=\"everyMinute\" (selectionChange)=\"everyMinuteSelect($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.everyMinute\" single>\r\n                    <mat-option *ngFor=\"let topping of secondsList \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"hourViewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <br>\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\" appearance=\"outline\">\r\n                  <mat-select formControlName=\"hourListvalue\" (selectionChange)=\"selectHour($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.hourListvalue\" single>\r\n                    <mat-option *ngFor=\"let topping of hourListView \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"dayviewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <br>\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\">\r\n                  <mat-select formControlName=\"everyDays\" (selectionChange)=\"dateCounts($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.everyDays\" single>\r\n                    <mat-option *ngFor=\"let topping of dateCount \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <span style=\"margin-right: 12px;;\">\r\n                  <mat-label>day(s) starting on</mat-label>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\" style=\"margin-left:7px;\">\r\n                  <mat-select formControlName=\"dayStartingOn\" (selectionChange)=\"daysCounts($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.dayStartingOn\" single>\r\n                    <mat-option *ngFor=\"let topping of days \" [value]=\"topping\">{{topping}}\r\n                    </mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n\r\n          <ng-container *ngIf=\"monthviewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\">\r\n                  <mat-select (selectionChange)=\"monthCount($event)\" formControlName=\"monthStartingOn\"\r\n                    [(ngModel)]=\"cropExpressionSelection.monthStartingOn\" single>\r\n                    <mat-option *ngFor=\"let topping of monthsCount \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <span style=\"margin-right: 12px;;\">\r\n                  <mat-label>months(s) starting in</mat-label>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\" style=\"margin-left:7px;\">\r\n                  <mat-select (selectionChange)=\"monthExp($event)\" formControlName=\"monthStartingOn\"\r\n                    [(ngModel)]=\"cropExpressionSelection.monthDetailsShow\" single>\r\n                    <mat-option *ngFor=\"let topping of numOfmonths \" [value]=\"topping\">{{topping}}\r\n                    </mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n          <!-- end exp -->\r\n          <!-- end -->\r\n        </div>\r\n      </div>\r\n\r\n\r\n    </ng-container>\r\n    <ng-container *ngIf=\"formValues.enableAgentNote\">\r\n      <div>\r\n        <p>Note: Once Download the agent the following steps to be followed to run the agent</p>\r\n        <ul>\r\n          <li>Extract the Downloaded package into a folder</li>\r\n          <li>Execute the commad to provide root permission to run the script file (run chmod 777 sodagent.sh)</li>\r\n          <li>Execute the commad sh sodagent.sh to run the script file</li>\r\n        </ul>\r\n        <p>Please Refer the Readme file for further clarificaion on installation</p>\r\n        <p>Note: To check the test connection Plese make sure that you have access to the database system</p>\r\n      </div>\r\n    </ng-container>\r\n    <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.tableData && enableTableLayout\">\r\n    <ng-container *ngFor=\"let tableItem of formValues.tableData\">\r\n      <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n        [viewFrom]=\"'create'\" [onLoad]=\"currentTableLoad\"></table-layout>\r\n    </ng-container>\r\n  </div> -->\r\n  </form>\r\n  <ng-container *ngIf=\"!formValues.buttonsList\">\r\n    <div>\r\n      <div class=\"col-12 sentri-btn-footer\" [ngClass]=\"getRulesWidth ? getRulesWidth:hiddenFixedLayout\">\r\n        <!-- fxLayout=\"row\" fxLayoutAlign=\"end end\" -->\r\n        <div class=\"ui-common-lib-whitebg\">\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"end end\" style=\"padding: 10px 0px 0px 0px;padding-bottom:10px !important;\"\r\n            [class.bottom-layout]=\"formValues.buttonsList\" *ngIf=\"!formValues.buttonsList\">\r\n            <!-- <button *ngIf=\"formValues.enableTestConnection\" mat-raised-button class=\"btn-common-view\" type=\"button\" color=\"warn\">Test Connection</button> -->\r\n            <button type=\"button\" *ngIf=\"stepperVal\"\r\n              class=\"mat-raised-button ui-common-lib-outline cancel-btn btn-common-view sen-cancel\" ng-reflect-color=\"accent\"\r\n              ng-reflect-type=\"button\" (click)=\"closeModel()\">\r\n              <span class=\"mat-button-wrapper\">{{\"BUTTONS.CANCEL_BTN_LBL\" | translate}}</span>\r\n            </button>\r\n            <button class=\"btn-common-view mrStyle ui-common-lib-outline\" mat-raised-button\r\n              *ngIf=\"stepperVal && stepperVal.selectedIndex > 0\" type=\"button\" color=\"accent\"\r\n              (click)=\"navigatePrevious()\">\r\n              <mat-icon>navigate_before</mat-icon>{{\"BUTTONS.PREVIOUS_BTN_LBL\" | translate}}\r\n            </button>\r\n            <button class=\"btn-common-view ui-common-lib-outline sen-next-btn\" mat-raised-button type=\"button\" color=\"accent\"\r\n              (click)=\"navigateNext()\"\r\n              *ngIf=\"stepperVal && stepperVal['_steps'] && (stepperVal.selectedIndex !== stepperVal._steps.length-1) \">\r\n              <mat-icon>navigate_next</mat-icon>{{\"BUTTONS.NEXT_BTN_LBL\" | translate}}\r\n            </button>\r\n            <button mat-raised-button type=\"button\"\r\n              *ngIf=\"stepperVal && stepperVal['_steps'] && (stepperVal.selectedIndex === stepperVal._steps.length-1)\"\r\n              class=\"btn-common-view ui-common-lib-outline sen-next-btn\" color=\"warn\" (click)=\"onSubmit()\">\r\n              {{\"BUTTONS.SUBMIT_BTN_LBL\" | translate}}\r\n            </button>\r\n          <!--\r\n            <button *ngIf=\"importData && importData.function !=='edit'\" mat-raised-button\r\n              class=\"btn-common-view ui-common-lib-outline\" type=\"button\" color=\"warn\" (click)=\"onImport()\">\r\n              {{\"BUTTONS.IMPORT_BTN_LBL\" | translate}}\r\n            </button>\r\n            <button *ngIf=\"importData && importData.function =='edit'\" mat-raised-button\r\n              class=\"btn-common-view ui-common-lib-outline\" type=\"button\" color=\"warn\" (click)=\"onImport()\">\r\n              {{\"BUTTONS.RE_IMPORT_BTN_LBL\" | translate}}\r\n            </button> -->\r\n             <span *ngIf=\"importData && importData.buttonsList\">\r\n              <button *ngFor=\"let buttonId of importData.buttonsList\"\r\n              (click)=\"onButtonClick(buttonsConfigData[buttonId],importData)\"\r\n              [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n              mat-raised-button class=\"btn-common-view\" style=\"margin: 0 3px;\">\r\n              {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n            </button>\r\n             </span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n  <ng-container *ngIf=\"formValues.buttonsList && !formValues.beforeTableView\">\r\n    <div class=\"col-12 sentri-btn-footer\" [ngClass]=\"getRulesWidth ? getRulesWidth:hiddenFixedLayout\">\r\n      <!-- fxLayout=\"row\" fxLayoutAlign=\"end end\" -->\r\n      <div class=\"ui-common-lib-whitebg\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"end end\" [class.bottom-layout]=\"formValues.buttonsList\"\r\n          *ngIf=\"formValues.buttonsList\">\r\n          <button *ngFor=\"let buttonId of formValues.buttonsList\"\r\n            (click)=\"onButtonClick(buttonsConfigData[buttonId],formValues)\"\r\n            [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n            mat-raised-button class=\"btn-common-view\" style=\"margin: 0 3px;\">\r\n            {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n\r\n</div>\r\n",
                    styles: [".cancel-btn{border:1px solid #4527a0!important;background-color:#fff!important;color:#000!important;margin-right:8px!important}.btn-common-view{border-radius:25px!important}.error_margin{margin-top:-30px}p.errorP{margin-top:10px}.ui-common-lib-outline{outline:0!important}.bottom-layout{margin:1%}.import-error-view{max-height:250px;overflow:scroll}.btnformat{border-radius:25px;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-webkit-tap-highlight-color:transparent;margin-right:5px}.chip-label,.select-label{margin-top:2%;width:50%;float:left}.slide-toggle{font-weight:700;font-size:initial}.slide-toggle-padding{padding:0 0 0 10px}.import-icon{background:-webkit-gradient(linear,left top,left bottom,from(#8c51ff),to(#5198fe));-webkit-background-clip:text;-webkit-text-fill-color:transparent}.column{float:left;width:50%}.row:after{content:\"\";display:table;clear:both}.full-width{width:100%}.half-width{width:48.5%}.confirm-data-view{margin:0 0 0 10px;border-radius:4px;font-size:16px;background-color:#fff;border:2px solid}.mrStyle{margin-right:8px}div.column{padding-right:35px!important}.sen-lib-form-layout{margin-left:28px;margin-right:28px;background:#fff;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12);padding:28px 27px}.sen-m-0{margin:0!important}input.mat-chip-input{width:150px;margin:0!important;-webkit-box-flex:1;flex:1 0 150px}.ui-common-lib-confirmation-header{background:#394e6661;font-size:16px!important;padding:4px 4px 6px;margin-top:8px;font-weight:600;color:#000;box-shadow:2px 2px 1px 1px #dadada;border-bottom:1px solid #c4c4c4;position:relative;z-index:22}.ui-common-lib-padding-10{padding:12px}.ui-common-lib-browse-btn{background:linear-gradient(45deg,#8c51ff,#5198fe) i!important;color:#fff!important;border-radius:20px!important}.ui-common-lib-f17{font-size:17px!important}.paddingAdd{padding-bottom:10px!important}.widthBase{width:25%!important}.sentri-active{border-bottom:2px solid currentColor}.sentri-form-height{height:303px;padding-left:12px;overflow-y:auto;overflow-x:hidden;margin-bottom:60px}.sen-lib-height{height:auto!important;overflow:unset!important}.sentri-btn-footer{right:0;position:absolute;bottom:0;width:100%;background:#f7f7f7;border-top:2px solid #d8d7d7;z-index:99999}.sen-lib-width{width:100%!important;right:0!important}.sen-lib-width-report{width:50%!important;right:25%!important;bottom:20%!important}.sen-lib-hidden{display:none!important}.sen-lib-height-conflict{height:303px;overflow-y:auto;overflow-x:hidden;margin-bottom:20px}.sentri-ruleset-height{height:355px!important}.sentri-margin-set{margin-bottom:38px!important}.sentri-form-height.sen-lib-height-btm{height:auto!important;margin-bottom:0;overflow:hidden}.sentri-margin-height{height:auto!important;margin-bottom:0!important}.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:154px}.sentri-height-auto{height:100%;margin:0!important;overflow:hidden!important;padding:12px!important}.sen-lib-footer-fixedfordatasource{position:fixed!important;width:50%!important;left:25%!important}.sen-lib-footer-fixedforruleset{bottom:15%;position:fixed;width:50%;left:25%}.sen-next-btn{color:#fff!important;background-color:#e91e63!important}.sen-cancel{border:1px solid #4527a0!important;background-color:#fff!important;color:#000!important;margin-right:8px!important}@media only screen and (max-width:1300px){.sentri-responsive-col-3{width:100%!important;max-width:100%!important;-webkit-box-flex:0;flex:0 0 26%;top:-37px}.sen-lib-responsive-enddate{max-width:100%;margin-left:1px}.sen-res-width100{width:100%!important}.mr-res-5btm{margin-bottom:8px}}@media only screen and (max-width:12){.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:144px}}@media only screen and (max-width:1440px){.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:144px}}@media only screen and (max-width:2560px){.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:10%}}.sen-header-id{background:#bab8b8;box-shadow:4px 3px 1px 0 #dcdcdc}.sen-header-id h6{padding:5px;font-size:14px}.sen-lib-width-18{width:18%}.sen-lib-list-view{display:block;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.import-file-info{color:#d2584f;margin-bottom:-1rem!important}.btn-submit{background-color:#2b14bd!important}.ui-lib-accessgroup-add{position:relative;right:10px;top:5px}"]
                }] }
    ];
    /** @nocollapse */
    FormLayoutComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: ContentService },
        { type: FormBuilder },
        { type: MessageService },
        { type: ChangeDetectorRef },
        { type: MatDialogRef },
        { type: AwsS3UploadService },
        { type: ChangeDetectorRef },
        { type: SnackBarService },
        { type: LoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    FormLayoutComponent.propDecorators = {
        formValues: [{ type: Input }],
        stepperVal: [{ type: Input }],
        importData: [{ type: Input }],
        onLoadData: [{ type: Input }],
        chipListFields: [{ type: Input }],
        title: [{ type: Input }],
        data: [{ type: Input }],
        fromRouting: [{ type: Input }],
        accessInput: [{ type: ViewChild, args: ["accessInput",] }],
        chipInput: [{ type: ViewChild, args: ["chipInput",] }],
        matAutocomplete: [{ type: ViewChild, args: ["auto",] }]
    };
    return FormLayoutComponent;
}());
export { FormLayoutComponent };
var ImportValue = /** @class */ (function () {
    function ImportValue(control) {
        this.data_source = control.data_source || "";
        this.file = control.file || "";
    }
    return ImportValue;
}());
export { ImportValue };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImZvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3ZFLE9BQU8sRUFDTCxTQUFTLEVBQ1QsTUFBTSxFQUNOLFNBQVMsRUFDVCxVQUFVLEVBQ1YsS0FBSyxFQUNMLGlCQUFpQixHQUdsQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBRUwsZUFBZSxHQUNoQixNQUFNLGdDQUFnQyxDQUFDO0FBR3hDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLGtEQUFrRDtBQUVsRCxPQUFPLEVBQ0wsV0FBVyxFQUNYLFdBQVcsRUFDWCxTQUFTLEVBQ1QsVUFBVSxHQUVYLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRTVELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEtBQUssU0FBUyxNQUFNLFlBQVksQ0FBQztBQUN4QyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDeEQsT0FBTyxLQUFLLE9BQU8sTUFBTSxRQUFRLENBQUM7QUFDbEMsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRWxELElBQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUV2QjtJQStGRSw2QkFDVSw2QkFBMkQsRUFDM0QsY0FBOEIsRUFDOUIsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsY0FBaUMsRUFDbEMsWUFBZ0QsRUFDL0Msa0JBQXNDLEVBQ3RDLEdBQXNCLEVBQ3RCLGVBQWdDLEVBQ2hDLGFBQTRCLEVBQ1QsT0FBTztRQVhwQyxpQkErREM7UUE5RFMsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUE4QjtRQUMzRCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtRQUNsQyxpQkFBWSxHQUFaLFlBQVksQ0FBb0M7UUFDL0MsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxRQUFHLEdBQUgsR0FBRyxDQUFtQjtRQUN0QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDVCxZQUFPLEdBQVAsT0FBTyxDQUFBO1FBckZwQyx1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFnQjFCLHlCQUFvQixHQUFZLEtBQUssQ0FBQztRQUM5QixnQkFBVyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFDbEMscUJBQWdCLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUMvQyxzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFDNUIsbUJBQWMsR0FBYSxLQUFLLENBQUM7UUFDakMsY0FBUyxHQUFZLElBQUksQ0FBQztRQUMxQixpQkFBWSxHQUFRLEVBQUUsQ0FBQztRQUt2QixtQkFBYyxHQUFXLElBQUksQ0FBQztRQUM5QixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFVbEMsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUdwQiwwQkFBcUIsR0FBWSxLQUFLLENBQUM7UUFHdkMsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBRXBDLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLHdCQUFtQixHQUFRLEVBQUUsQ0FBQztRQUM5QixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFFdkIsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUNoQixrQkFBYSxHQUFRLEVBQUUsQ0FBQztRQUN4QixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFFbEMsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUNiLG9CQUFlLEdBQVksSUFBSSxDQUFDO1FBQ2hDLG1CQUFjLEdBQVksSUFBSSxDQUFDO1FBRXRDLGVBQVUsR0FBRyxDQUFDLENBQUM7UUFDZix1QkFBa0IsR0FBRyxDQUFDLENBQUM7UUFHdkIsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQUNyQixZQUFPLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNyQixlQUFVLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN4QixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFDM0Isb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFneUZ2QyxxQ0FBcUM7UUFDckMsZ0JBQVcsR0FBUyxFQUFFLENBQUM7UUFDdkIsc0JBQWlCLEdBQVEsRUFBRSxDQUFDO1FBQzVCLGlCQUFZLEdBQVMsRUFBRSxDQUFDO1FBQ3hCLGtCQUFhLEdBQVMsRUFBRSxDQUFDO1FBQ3pCLFNBQUksR0FBUyxFQUFFLENBQUM7UUFDaEIsVUFBSyxHQUFTLEVBQUUsQ0FBQztRQUNqQixnQkFBVyxHQUFTLEVBQUUsQ0FBQztRQUN2QixjQUFTLEdBQVMsRUFBRSxDQUFDO1FBQ3JCLGNBQVMsR0FBUyxFQUFFLENBQUM7UUFDckIsZ0JBQVcsR0FBUyxFQUFFLENBQUM7UUFPdkIsNEJBQXVCLEdBQVMsRUFBRSxDQUFDO1FBN3hGakMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLEtBQUssR0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO1lBQ2xDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztTQUNsQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUI7YUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUN0QyxTQUFTLENBQUMsVUFBQyxJQUFJO1lBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsVUFBVSxFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDcEQsSUFBSSxRQUFRLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQztZQUN0QyxLQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUMvQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDUCxLQUFJLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNoQyxDQUFDLENBQUMsQ0FBQztRQUNMLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLGNBQWM7YUFDaEIsVUFBVSxFQUFFO2FBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDakMsU0FBUyxDQUFDLFVBQUMsT0FBTztZQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQztZQUNwQyxLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1lBQ0YsdUhBQXVIO1lBQ3ZILEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztZQUNsQyxLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUV6QyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLGVBQWU7YUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDakMsU0FBUyxDQUFDLFVBQUMsSUFBSTtZQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLDZDQUE2QyxDQUFDLENBQUM7WUFDakUsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDakIsSUFBSSxLQUFJLENBQUMsSUFBSSxFQUFFO2dCQUNiLEtBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQzFDLENBQUM7Z0JBQ0YsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBQ0Qsc0NBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDL0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM5Qix5Q0FBeUM7UUFDekMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXJCLElBQUksVUFBVSxHQUFHO1lBQ2YsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7WUFDcEMsY0FBYyxFQUFFLElBQUksQ0FBQyxxQkFBcUI7WUFDMUMsS0FBSyxFQUFFLEVBQUU7WUFDVCxNQUFNLEVBQUUsQ0FBQztTQUNWLENBQUM7UUFDRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDMUM7UUFDRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUM7WUFDM0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNsRCxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1AsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQzFCLElBQUksUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDcEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1lBQ25DLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztZQUN0QixDQUFDLENBQUM7Z0JBQ0UsWUFBWSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7Z0JBQ3BDLGNBQWMsRUFBRSxJQUFJLENBQUMscUJBQXFCO2dCQUMxQyxLQUFLLEVBQUUsRUFBRTtnQkFDVCxNQUFNLEVBQUUsQ0FBQzthQUNWLENBQUM7UUFDTixJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLFVBQVUsQ0FBRSxDQUFDO1FBQ3RELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztRQUNGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNySCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRO1lBQ1gsSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLHlCQUF5QjtnQkFDNUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCO2dCQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ1AsWUFBWTtRQUNoQixJQUFJLENBQUMsYUFBYTtZQUNoQixJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTtnQkFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTtnQkFDekIsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNYLElBQUksQ0FBQyxhQUFhO1lBQ2hCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXO2dCQUM1QyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXO2dCQUM3QixDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ1gsSUFBSSxDQUFDLGlCQUFpQjtZQUNwQixJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCO2dCQUNsRCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUI7Z0JBQ25DLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFFWCxJQUFJLENBQUMsaUJBQWlCO1lBQ2xCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUI7Z0JBQ2xELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQjtnQkFDbkMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUNiLCtEQUErRDtRQUMvRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUMzQixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNsRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3RFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDekUsOEZBQThGO1lBQzlGLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztZQUN0RSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ2xFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3pELElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQjtnQkFDOUQsQ0FBQyxDQUFDLElBQUk7Z0JBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNWLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDbkU7UUFFRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxLQUFLLENBQUM7UUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1NBQzlCO1FBQ0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxVQUFVLElBQUk7Z0JBQ2xELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxPQUFPLEVBQUU7b0JBQzdCLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDNUQ7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDMUM7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxJQUFJO2dCQUNsRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztZQUNuRCxDQUFDLENBQUMsQ0FBQztZQUNILGdEQUFnRDtTQUNqRDtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pELENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLElBQUk7Z0JBQzFELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUM7b0JBQ25DLEtBQUssRUFBRSxFQUFFO29CQUNULFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTztpQkFDdkIsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJO2dCQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNsQixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyx1QkFBdUIsRUFBRTtZQUNoQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUUsVUFBVSxJQUFJO2dCQUN6RCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUMzQixvREFBb0Q7WUFDcEQsOENBQThDO1lBQzlDLHNEQUFzRDtZQUN0RCw0Q0FBNEM7WUFDNUMsc0NBQXNDO1lBQ3RDLE1BQU07WUFDTixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFBRSxVQUFVLElBQUk7Z0JBQ3ZELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUVqRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLENBQUM7Z0JBQzVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7WUFDbkMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxTQUFTLENBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUM5QyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQzthQUNyRDtTQUNGO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUN2RCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3ZELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsZUFBZSxDQUFDLENBQUM7Z0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO2dCQUNuRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDM0MsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7b0JBQ25DLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO29CQUN2QyxJQUNFLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSTt3QkFDdkIsSUFBSSxDQUFDLFNBQVM7d0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7d0JBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsRUFDN0M7d0JBQ0EsSUFBSSxDQUFDLFNBQVM7NEJBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUN6RCxJQUFJLENBQUMsWUFBWTs0QkFDZixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7d0JBQzVELElBQUksQ0FBQyxTQUFTOzRCQUNaLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDekQsSUFBSSxDQUFDLFlBQVk7NEJBQ2YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUM3RDtpQkFDRjtnQkFFRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3ZCLDhEQUE4RDtvQkFDOUQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ3hDLElBQUksY0FBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO29CQUNoRCxJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztvQkFDbEQsSUFBSSxPQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO29CQUNoQyx3Q0FBd0M7b0JBQ3hDLFdBQVc7b0JBQ1gsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFXLEVBQUUsVUFBVSxXQUFXO3dCQUMxQyxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUU7NEJBQ3pCLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQzt5QkFDN0M7NkJBQU07NEJBQ0wsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDN0Q7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsT0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7d0JBQzlELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFZLENBQWEsQ0FBQzt3QkFDcEQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7d0JBQ2pFLElBQUksQ0FBQyxhQUFhLEVBQUU7NEJBQ2xCLElBQU0sVUFBUSxHQUFHLEVBQUUsQ0FBQzs0QkFDcEIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRTtnQ0FDbkMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7Z0NBQ3ZELElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUM3QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFVLE9BQU87b0NBQy9CLFVBQVEsQ0FBQyxJQUFJLENBQUM7d0NBQ1osSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsYUFBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dDQUM1QyxHQUFHLEVBQUUsT0FBTztxQ0FDYixDQUFDLENBQUM7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NkJBQ0o7aUNBQU07Z0NBQ0wsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsSUFBSTtvQ0FDakMsVUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0NBQzNDLENBQUMsQ0FBQyxDQUFDOzZCQUNKOzRCQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBUSxDQUFDO3lCQUN0QjtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCw4QkFBOEI7Z0JBQzlCLDZCQUE2QjtnQkFDN0IsdUJBQXVCO2dCQUN2QixPQUFPO1lBQ1QsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSwwQkFBMEIsQ0FBQyxDQUFDO1FBQzVELDJDQUEyQztRQUMzQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDckQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUM7WUFDMUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLO2dCQUN2QyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsNkRBQTZEO1lBQzdELDhDQUE4QztZQUM5Qyw4Q0FBOEM7WUFDOUMsa0RBQWtEO1lBQ2xELG9EQUFvRDtZQUNwRCxpREFBaUQ7WUFDakQsb0RBQW9EO1lBQ3BELEtBQUs7WUFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDO1NBQ3JEO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUM5RCxJQUFJLENBQUMsZUFBZSxFQUFFO2FBQ3ZCLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFZCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNuQjtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN4QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO29CQUM5RCxDQUFDLENBQUMsSUFBSTtvQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ1g7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRTtnQkFDeEMsSUFBSSxTQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUMzRCxJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBTyxDQUFDLENBQUM7Z0JBQzdDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsd0JBQXdCLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hELElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDdkMsT0FBTyxFQUFFLGtCQUFrQixDQUFDLFFBQVE7aUJBQ3JDLENBQUMsQ0FBQztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2dCQUM5QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0I7b0JBQzlELENBQUMsQ0FBQyxJQUFJO29CQUNOLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDWDtpQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFO2dCQUN2Qyx5Q0FBeUM7Z0JBQ3pDLHVDQUF1QztnQkFDdkMsOENBQThDO2dCQUM5QyxLQUFLO2dCQUNMLHFEQUFxRDtnQkFFckQsSUFBSSxTQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQU8sQ0FBQyxDQUFDO2dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztnQkFFM0MsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO29CQUN2QyxPQUFPLEVBQUUsY0FBYztpQkFDeEIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztnQkFDdEMsNkNBQTZDO2dCQUM3QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO29CQUM5RCxDQUFDLENBQUMsSUFBSTtvQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ1g7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixFQUFFO2dCQUM1QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO29CQUM5RCxDQUFDLENBQUMsSUFBSTtvQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ1g7WUFDRCx5Q0FBeUM7WUFDekMsMkNBQTJDO1lBRTNDLG1EQUFtRDtTQUNwRDtJQUNILENBQUM7SUFFRCw2Q0FBZSxHQUFmO1FBQ0UsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7UUFDckQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQUksY0FBYyxDQUFDLFlBQVksRUFBRTtZQUMvQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJO2dCQUNuRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRTtZQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLElBQUk7Z0JBQ3pELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUNELHlDQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0QsNkNBQWUsR0FBZixVQUFnQixhQUFhO1FBQzNCLG1GQUFtRjtRQUNuRiw0REFBNEQ7UUFDNUQsMkJBQTJCO1FBQzNCLElBQUk7UUFDSiw4REFBOEQ7UUFDOUQsSUFBSSxPQUFPLEdBQWMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDakUsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBQ0Qsb0NBQU0sR0FBTjtRQUNFLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0RCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUMzQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLFVBQVUsSUFBSTtnQkFDcEQsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO29CQUN2QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztvQkFDeEMsSUFBSSxjQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7b0JBQ2hELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO29CQUNsRCxJQUFJLE9BQUssR0FBRyxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7b0JBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFVBQVUsV0FBVzt3QkFDMUMsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFOzRCQUN6QixPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7eUJBQzdDOzZCQUFNOzRCQUNMLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQzdEO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFJO3dCQUM5RCxvQkFBb0I7d0JBQ3BCLElBQUk7d0JBQ0osOERBQThEO3dCQUM5RCxrREFBa0Q7d0JBQ2xELE1BQU07d0JBQ04sb0RBQW9EO3dCQUNwRCxPQUFPO3dCQUNQLHdCQUF3Qjt3QkFDeEIsSUFBSTt3QkFDSixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBWSxDQUFhLENBQUM7d0JBRXBELDhDQUE4Qzt3QkFDOUMsa0NBQWtDO3dCQUNsQyxnQ0FBZ0M7d0JBQ2hDLGlFQUFpRTt3QkFFakUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDeEMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFOzRCQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQzs0QkFDMUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQzVCLENBQUM7eUJBQ0g7d0JBRUQsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFOzRCQUNuRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxPQUFPO2dDQUNwQyxJQUFJLE9BQU8sQ0FBQyxjQUFjLEVBQUU7b0NBQzFCLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7b0NBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7b0NBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDO29DQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztpQ0FDbkQ7NEJBQ0gsQ0FBQyxDQUFDLENBQUM7eUJBQ0o7d0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ25FLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBRXZFLENBQUMsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLElBQUk7Z0JBQ3pELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ3hDLElBQUksY0FBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO29CQUNoRCxJQUFJLEtBQUssR0FBRzt3QkFDVixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7cUJBQ2xCLENBQUM7b0JBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7d0JBQzlELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFZLENBQWEsQ0FBQztvQkFDdEQsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztZQUN2RCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxVQUFVLElBQUk7Z0JBQ2xELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsOERBQThEO29CQUM5RCxJQUFJLFFBQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztvQkFDeEMsSUFBSSxjQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7b0JBQ2hELElBQUksYUFBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO29CQUNsRCxJQUFJLE9BQUssR0FBRyxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7b0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxTQUFTLENBQUMsQ0FBQztvQkFDdkMsVUFBVSxDQUFDO3dCQUNULENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBVyxFQUFFLFVBQVUsV0FBVzs0QkFDMUMsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO2dDQUN6QixPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7NkJBQzdDO2lDQUFNLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTtnQ0FDN0IsSUFBSSxXQUFXLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO29DQUMxRCxzSkFBc0o7b0NBQ3RKLCtIQUErSDtvQ0FDL0gsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO3dDQUN4QyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FDSCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQ3JELFdBQVcsQ0FBQyxNQUFNLENBQ25CO3dDQUNILENBQUMsQ0FBQyxFQUFFLENBQUM7b0NBQ1AsSUFBSSxFQUFFLENBQUMsTUFBTTt3Q0FDWCxPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQzs0Q0FDckIsV0FBVyxDQUFDLGlCQUFpQixJQUFJLE9BQU8sRUFBRSxLQUFLLFFBQVE7Z0RBQ3JELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztnREFDcEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDWjtxQ0FBTSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO29DQUM1QyxPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzt3Q0FDekQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7d0NBQ3ZELENBQUMsQ0FBQyxFQUFFLENBQUM7b0NBQ1AsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsaUJBQWlCO3dDQUNyRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO3dDQUN6QyxDQUFDLENBQUMsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQ0FDN0I7NkJBQ0Y7aUNBQU07Z0NBQ0wsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7b0NBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7b0NBQ25DLENBQUMsQ0FBQyxFQUFFLENBQUM7NkJBQ1I7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7d0JBQ0gsSUFBSSxDQUFDLGNBQWM7NkJBQ2hCLGFBQWEsQ0FBQyxPQUFLLEVBQUUsUUFBTSxDQUFDOzZCQUM1QixTQUFTLENBQUMsVUFBQyxJQUFJOzRCQUNkLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFZLENBQWEsQ0FBQzs0QkFDcEQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDaEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7Z0NBQy9DLENBQUMsQ0FBQyxJQUFJO2dDQUNOLENBQUMsQ0FBQyxLQUFLLENBQUM7NEJBQ1YsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQ0FDbEIsSUFBTSxVQUFRLEdBQUcsRUFBRSxDQUFDO2dDQUNwQixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFO29DQUNuQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQ0FDdkQsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0NBQzdCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLFVBQVUsT0FBTzt3Q0FDL0IsVUFBUSxDQUFDLElBQUksQ0FBQzs0Q0FDWixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxhQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7NENBQzVDLEdBQUcsRUFBRSxPQUFPO3lDQUNiLENBQUMsQ0FBQztvQ0FDTCxDQUFDLENBQUMsQ0FBQztpQ0FDSjtxQ0FBTTtvQ0FDTCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxJQUFJO3dDQUNqQyxVQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztvQ0FDM0MsQ0FBQyxDQUFDLENBQUM7aUNBQ0o7Z0NBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxVQUFRLENBQUM7NkJBQ3RCO3dCQUNILENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDVjtnQkFDRCw4QkFBOEI7Z0JBQzlCLDZCQUE2QjtnQkFDN0IsdUJBQXVCO2dCQUN2QixPQUFPO1lBQ1QsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUNoRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBQ0QsMEJBQTBCO0lBQzFCLHdCQUF3QjtJQUN4QiwyQkFBMkI7SUFDM0IsU0FBUztJQUNULHdCQUF3QjtJQUN4QixJQUFJO0lBQ0oseUNBQVcsR0FBWCxVQUFZLEtBQXdCLElBQVMsQ0FBQztJQUM5QywwQ0FBWSxHQUFaLFVBQWEsSUFBWSxFQUFFLEtBQVUsRUFBRSxTQUFjO1FBQ25ELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNmLElBQUksbUJBQW1CLEdBQUcsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN4RSxJQUFJLG1CQUFtQjtnQkFBRSxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDMUU7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1NBQzlCO0lBQ0gsQ0FBQztJQUNELDBDQUFZLEdBQVosVUFBYSxXQUFtQixFQUFFLFlBQVk7UUFBOUMsaUJBcURDO1FBcERDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLG1CQUFtQixDQUFDLENBQUM7UUFDOUMsSUFBSSxVQUFVLEdBQUcsV0FBVyxDQUFDLE1BQU0sQ0FBQztRQUNwQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsR0FBRyxXQUFXLENBQUM7UUFDNUMsSUFBSSxZQUFZLENBQUMsZ0JBQWdCLEVBQUU7WUFDakMsSUFBSSxNQUFNLEdBQUcsWUFBWSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztZQUNsRCxJQUFJLGNBQVksR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBQzFELElBQUksT0FBSyxHQUFHLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztZQUNoQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxJQUFJLEdBQUcsRUFBRSxDQUFDO1lBQ2QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFVBQ25ELFdBQVc7Z0JBRVgsSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFO29CQUN0QixJQUFJO3dCQUNGLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzs0QkFDakMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzs0QkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7NEJBQ3ZELENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ1QsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUM7aUJBQ2hDO3FCQUFNLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTtvQkFDaEMsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO2lCQUM3QztxQkFBTSxJQUFHLFdBQVcsQ0FBQyxtQkFBbUIsRUFBQztvQkFDeEMsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFHLFNBQVMsQ0FBRSxDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsQ0FBQSxDQUFDLENBQUEsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3pKO3FCQUFNO29CQUNMLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzdEO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLEtBQUssR0FBRyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDaEUsZ0NBQWdDO1lBQ2hDLDZDQUE2QztZQUMzQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxPQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBSTtnQkFDOUQsS0FBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQzVELGNBQVksQ0FDRCxDQUFDO2dCQUNkLElBQUksSUFBSSxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDakUsSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDbEIsSUFBTSxVQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNwQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxJQUFJO3dCQUNqQyxVQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFDM0MsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLElBQUksR0FBRyxVQUFRLENBQUM7aUJBQ3RCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDTCxXQUFXO1lBQ1gsOEJBQThCO1lBQzlCLGlDQUFpQztZQUNqQyxJQUFJO1NBQ0w7SUFDSCxDQUFDO0lBQ0QsOENBQWdCLEdBQWhCLFVBQWlCLElBQUk7UUFDbkIsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3ZCLDhEQUE4RDtZQUM5RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztZQUN4QyxJQUFJLGNBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztZQUNoRCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztZQUNsRCxJQUFJLE9BQUssR0FBRyxFQUFDLEtBQUssRUFBQyxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7WUFDL0IsSUFBSSxNQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFVBQVUsV0FBVztnQkFDMUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsZUFBZSxDQUFDLENBQUM7Z0JBQzFDLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTtvQkFDekIsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO2lCQUM3QztxQkFBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUU7b0JBQzdCLElBQUksV0FBVyxDQUFDLEtBQUssSUFBSSxNQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDMUQsc0pBQXNKO3dCQUN0SiwrSEFBK0g7d0JBQy9ILElBQUksRUFBRSxHQUFHLE1BQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzs0QkFDeEMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQ0gsTUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUNyRCxXQUFXLENBQUMsTUFBTSxDQUNuQjs0QkFDSCxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUNQLElBQUksRUFBRSxDQUFDLE1BQU0sRUFBRTs0QkFDYixPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztnQ0FDckIsV0FBVyxDQUFDLGlCQUFpQixJQUFJLE9BQU8sRUFBRSxLQUFLLFFBQVE7b0NBQ3JELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQztvQ0FDcEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt5QkFDVjtxQkFDRjt5QkFBTSxJQUFJLE1BQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUM1QyxPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzs0QkFDekQsQ0FBQyxDQUFDLE1BQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUM7NEJBQ3ZELENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ1AsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsaUJBQWlCOzRCQUNyRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUN6QyxDQUFDLENBQUMsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDN0I7aUJBQ0Y7cUJBQU07b0JBQ0wsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDN0Q7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzlCLE1BQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFJO2dCQUM5RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBWSxDQUFhLENBQUM7Z0JBQ3BELE1BQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZELE1BQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FDdEQsSUFBSSxDQUFDLElBQUksRUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FDOUIsQ0FBQztnQkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQUksQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkUsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFDRCwwQ0FBWSxHQUFaLFVBQ0UsS0FBbUMsRUFDbkMsY0FBYyxFQUNkLFNBQVMsRUFDVCxTQUFTO1FBRVQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUNoRCwwQ0FBMEM7UUFDMUMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUN4QyxnQ0FBZ0M7WUFDaEMsMERBQTBEO1lBQzFELEtBQUs7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQztnQkFDckUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDO2dCQUNwQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ1AsNEVBQTRFO1lBQzVFLGlFQUFpRTtZQUNqRSxJQUFJO1lBQ0osSUFBSSxTQUFTLEdBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDbEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxFQUFFO2dCQUNqQixJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzdEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQztZQUN0RCxJQUFJLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7Z0JBQ3hELElBQUksRUFBRSxjQUFjO2FBQ3JCLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUNuRCxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDeEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLEVBQ2xDLFNBQVMsQ0FDVixDQUFDO1lBQ0YsMkJBQTJCO1lBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDakUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ3JELElBQUksZ0JBQWdCLElBQUksZ0JBQWdCLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ3hELElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7b0JBQ3RELElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7aUJBQ3ZDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxDQUFDO1lBQ3pFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNwRSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztTQUN0RTtJQUNILENBQUM7SUFDRCxnREFBa0IsR0FBbEIsVUFBbUIsS0FBSyxFQUFFLGNBQWMsRUFBRSxTQUFTO1FBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25ELElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3RFLElBQUksYUFBYSxJQUFJLENBQUMsRUFBRTtZQUN0QixJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDN0Q7UUFDRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsRUFDbEMsU0FBUyxDQUNWLENBQUM7UUFFRixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQ3pFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBQyxrQ0FBa0MsQ0FBQyxDQUFBO1FBQy9ELElBQUksZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRTtZQUN4RCxJQUFJLEVBQUUsY0FBYztTQUNyQixDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDN0IsSUFBRyxJQUFJLENBQUMsVUFBVSxFQUFDO1lBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQ3JELElBQUksZ0JBQWdCLElBQUksZ0JBQWdCLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ3hELElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7b0JBQ3RELElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7aUJBQ3ZDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDdkM7U0FDSjthQUFJO1lBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUMzRDtRQUNELElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDakUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQ3ZELENBQUM7SUFDRCwyQ0FBYSxHQUFiO1FBQ0UsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7UUFDMUMsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO2FBQy9ELGdCQUFnQixDQUFDO1FBQ3BCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUM7UUFDdEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxJQUFJO1lBQ3JDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDZixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQzdELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ2hDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1AsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7b0JBQzVCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNwSTtxQkFBTTtvQkFDTCxzRUFBc0U7b0JBQ3RFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzt3QkFDNUIsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7NEJBQ3ZELENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7NEJBQ25DLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ1Y7YUFDRjtpQkFBSyxJQUFHLElBQUksQ0FBQyxXQUFXLEVBQUM7Z0JBQ3JCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDM0Q7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN2RDtZQUNELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtnQkFDcEIsSUFBRyxJQUFJLENBQUMsV0FBVyxFQUFDO29CQUNsQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7d0JBQ25FLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7d0JBQ2xDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsQ0FBQztvQkFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQ25ELFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQ3BCLElBQUksQ0FBQyxTQUFTLENBQ2YsQ0FBQztpQkFDSDtnQkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFBLEVBQUUsQ0FBQztnQkFDckcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25FLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxlQUFlLEVBQUU7b0JBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLEdBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQztpQkFDdkQ7Z0JBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDakQ7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDO1lBQ3RELElBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsTUFBTSxFQUN0QztnQkFDQSxJQUFJLENBQUMsaUJBQWlCO29CQUNwQixJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO3dCQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTO3dCQUM5QyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNULElBQUksQ0FBQyxlQUFlO29CQUNsQixJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO3dCQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7d0JBQ3BDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUNuRSxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2FBQ25DO1NBQ0Y7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO2dCQUNyRSxJQUFJLENBQUMsaUJBQWlCO29CQUNwQixJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO3dCQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTO3dCQUM5QyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNULElBQUksQ0FBQyxlQUFlO29CQUNsQixJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO3dCQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7d0JBQ3BDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztnQkFDL0QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzthQUNuQztTQUNGO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQUU7WUFDeEQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFO2dCQUNwRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7Z0JBQ2xELElBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSTtvQkFDbEMsSUFBSSxDQUFDLFNBQVM7b0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQ3hEO29CQUNBLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNqRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUNyQixDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNYLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNwRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUNyQixDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUVkLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztpQkFDcEQ7YUFDRjtTQUNGO1FBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQy9CLHdFQUF3RTtRQUN4RSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRCx5Q0FBVyxHQUFYO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO1FBRTVDLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzNCLElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ3hDLElBQUksY0FBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO29CQUNoRCxJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztvQkFDbEQsSUFBSSxPQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO29CQUNoQyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQVcsRUFBRSxVQUFVLFdBQVc7d0JBQzFDLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTs0QkFDekIsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO3lCQUM3Qzs2QkFBTTs0QkFDTCxPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxpQkFBaUI7Z0NBQ3JELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dDQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ3ZDO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFJO3dCQUM5RCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBWSxDQUFhLENBQUM7d0JBQ3BELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2hDLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO3dCQUNqRSxJQUFJLENBQUMsYUFBYSxFQUFFOzRCQUNsQixJQUFNLFVBQVEsR0FBRyxFQUFFLENBQUM7NEJBQ3BCLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUU7Z0NBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUN2RCxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDN0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBVSxPQUFPO29DQUMvQixVQUFRLENBQUMsSUFBSSxDQUFDO3dDQUNaLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLGFBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3Q0FDNUMsR0FBRyxFQUFFLE9BQU87cUNBQ2IsQ0FBQyxDQUFDO2dDQUNMLENBQUMsQ0FBQyxDQUFDOzZCQUNKO2lDQUFNO2dDQUNMLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLElBQUk7b0NBQ2pDLFVBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dDQUMzQyxDQUFDLENBQUMsQ0FBQzs2QkFDSjs0QkFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVEsQ0FBQzt5QkFDdEI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07U0FDTjtJQUNILENBQUM7SUFDRCx1Q0FBUyxHQUFULFVBQVUsSUFBSSxFQUFFLEVBQUU7UUFDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU07Z0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNULElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7SUFDRCwyQ0FBYSxHQUFiLFVBQWMsSUFBSSxFQUFFLFVBQVU7UUFBOUIsaUJBOE1DO1FBN01DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDMUMsSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLFFBQVEsRUFBRTtZQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztTQUN0QzthQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxjQUFjLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN2RSxVQUFVLENBQUMsSUFBSSxDQUNoQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsR0FBRyxVQUFVLENBQUMsVUFBVSxDQUFDO1lBQ3JELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsR0FBRyxVQUFVLENBQUM7WUFFNUMsaUVBQWlFO1lBQ2pFLElBQUksYUFBVyxHQUFHLFVBQVUsQ0FBQyxlQUFlO2dCQUMxQyxDQUFDLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxXQUFXO2dCQUN4QyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ1AsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUkscUJBQW1CLEdBQUcsVUFBVSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7WUFDbEUsYUFBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDbEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsTUFBTTtnQkFDdkMsSUFBSSxNQUFNLEVBQUU7b0JBQ1YsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGFBQVcsRUFBRSxVQUFVLFVBQVU7d0JBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsVUFBVSxDQUFDLENBQUM7d0JBQ3RELElBQUksVUFBVSxFQUFFOzRCQUNkLElBQUksQ0FBQyxjQUFjO2lDQUNoQixhQUFhLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDO2lDQUM1RCxTQUFTLENBQ1IsVUFBQyxHQUFHO2dDQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0NBQzdDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztnQ0FDRixJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO29DQUNyQixZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO29DQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2lDQUNyRDs0QkFDSCxDQUFDLEVBQ0QsVUFBQyxLQUFLO2dDQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0NBQzdDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dDQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7NEJBQ0osQ0FBQyxDQUNGLENBQUM7eUJBQ0w7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLFdBQVcsRUFBRTtZQUNyQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7YUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksUUFBUSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDM0I7YUFBTSxJQUFHLElBQUksQ0FBQyxNQUFNLElBQUUsUUFBUSxFQUFDO1lBQzlCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjthQUNJLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxZQUFZLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxlQUFlLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxRQUFRLEVBQUU7WUFDakcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGVBQWUsQ0FBQyxDQUFDO1lBQzlDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRTtnQkFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUNqRSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDOUIsQ0FBQztnQkFDRixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFO29CQUNqQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUMvRCxJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBRSxDQUFDO2lCQUNuRDtnQkFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsa0JBQWtCLENBQUMsQ0FBQzthQUNqRDtpQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzlDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUNoRSxJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLFFBQVEsQ0FBRSxDQUFDO2FBQ3JEO1lBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQy9CLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjthQUFNO1lBQ0wsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUMvQixJQUFJLENBQUMsb0JBQW9CLEdBQUcsS0FBSyxDQUFDO1lBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1lBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGtCQUFrQixDQUFDLENBQUM7WUFDNUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxVQUFVLENBQUMsWUFBWSxFQUFFO2dCQUMzQixJQUFJLE1BQUksR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxNQUFNO29CQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxDQUFDO29CQUN6QyxJQUFJLE1BQU0sRUFBRTt3QkFDVixJQUFJLGlCQUFlLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQzt3QkFDaEQsSUFBSSxjQUFjLEdBQUcsaUJBQWUsQ0FBQyxXQUFXLENBQUM7d0JBQ2pELElBQUksTUFBTSxHQUFHLGlCQUFlLENBQUMsTUFBTSxDQUFDO3dCQUNwQyxJQUFJLHFCQUFtQixHQUFHLGlCQUFlLENBQUMsWUFBWSxDQUFDO3dCQUN2RCxJQUFJLGtCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQy9CLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQ3JDLENBQUM7d0JBQ0YsSUFBSSxhQUFXLEdBQUcsRUFBQyxLQUFLLEVBQUUsTUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO3dCQUN0QyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7NEJBQ3RDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNO2dDQUN4QixDQUFDLENBQUMsa0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0NBQzNDLENBQUMsQ0FBQyxrQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ2pDLElBQUksUUFBUSxFQUFFO2dDQUNaLGFBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztvQ0FDMUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQzs2QkFDZDtpQ0FBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0NBQzVCLGFBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7b0NBQzVCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDOzZCQUNoQjt3QkFDSCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQVcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO3dCQUM1QyxNQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUNqQyxNQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxhQUFXLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUM5RCxVQUFDLE1BQU07NEJBQ0wsTUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQzs0QkFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLG1CQUFtQixDQUFDLENBQUM7NEJBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWUsQ0FBQyxZQUFZLEVBQUUsb0JBQW9CLENBQUMsQ0FBQzs0QkFDaEUsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FDNUIsaUJBQWUsQ0FBQyxZQUFZLENBQ2pCLENBQUM7NEJBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7NEJBQ3RDLE1BQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDOzRCQUMxQix5Q0FBeUM7NEJBQ3pDLDJDQUEyQzs0QkFDM0MsTUFBSSxDQUFDLGdCQUFnQixHQUFHLE1BQUksQ0FBQyxVQUFVLENBQUM7NEJBQ3hDLE1BQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxNQUFJLENBQUMsU0FBUyxDQUFDOzRCQUNuRCxNQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7NEJBQ3ZELE1BQUksQ0FBQyxpQkFBaUIsR0FBRyxNQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQjtnQ0FDM0QsQ0FBQyxDQUFDLEtBQUs7Z0NBQ1AsQ0FBQyxDQUFDLElBQUksQ0FBQzs0QkFDVCxNQUFJLENBQUMsb0JBQW9CLEdBQUcsTUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0I7Z0NBQzlELENBQUMsQ0FBQyxJQUFJO2dDQUNOLENBQUMsQ0FBQyxLQUFLLENBQUM7NEJBQ1Ysc0JBQXNCOzRCQUN0QixNQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsTUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7d0JBQ0osQ0FBQyxFQUNELFVBQUMsR0FBRzs0QkFDRixNQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsQ0FBQyxDQUNGLENBQUM7cUJBQ0g7eUJBQU07cUJBQ047Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxJQUFJLGlCQUFlLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQztnQkFDaEQsSUFBSSxjQUFjLEdBQUcsaUJBQWUsQ0FBQyxXQUFXLENBQUM7Z0JBQ2pELElBQUksTUFBTSxHQUFHLGlCQUFlLENBQUMsTUFBTSxDQUFDO2dCQUNwQyxJQUFJLHFCQUFtQixHQUFHLGlCQUFlLENBQUMsWUFBWSxDQUFDO2dCQUN2RCxJQUFJLGtCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLGFBQVcsR0FBRyxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7Z0JBQ3RDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLFVBQVUsSUFBSTtvQkFDdEMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU07d0JBQ3hCLENBQUMsQ0FBQyxrQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzt3QkFDM0MsQ0FBQyxDQUFDLGtCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDakMsSUFBSSxRQUFRLEVBQUU7d0JBQ1osYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZTs0QkFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDOzRCQUMxQixDQUFDLENBQUMsUUFBUSxDQUFDO3FCQUNkO3lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTt3QkFDNUIsYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZTs0QkFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzs0QkFDNUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7cUJBQ2hCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBVyxFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7Z0JBQ2pDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLGFBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQzlELFVBQUMsTUFBTTtvQkFDTCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBZSxDQUFDLFlBQVksRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO29CQUNoRSxJQUFJLFFBQVEsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUM1QixpQkFBZSxDQUFDLFlBQVksQ0FDakIsQ0FBQztvQkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQztvQkFDdEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7b0JBQzFCLHlDQUF5QztvQkFDekMsMkNBQTJDO29CQUMzQyxLQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQztvQkFDeEMsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ25ELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQkFDdkQsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztvQkFDOUIsS0FBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO3dCQUM5RCxDQUFDLENBQUMsSUFBSTt3QkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO29CQUNWLEtBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixLQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxxQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztnQkFDSixDQUFDLEVBQ0QsVUFBQyxHQUFHO29CQUNGLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUMzQixDQUFDLENBQ0YsQ0FBQzthQUNIO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsaURBQW1CLEdBQW5CLFVBQW9CLFFBQVE7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFDaEUsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNyRSxJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLFFBQVEsQ0FBRSxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixFQUFFO1NBQ3JDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzdDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztRQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsOEJBQThCLENBQUMsQ0FBQztRQUM1RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsMEJBQTBCLENBQUMsQ0FBQztRQUN6RCwwRUFBMEU7UUFDMUUsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3ZELG1EQUFtRDtRQUNuRCw2QkFBNkI7UUFDN0Isa0NBQWtDO1FBQ2xDLDZDQUE2QztRQUM3QyxLQUFLO1FBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixFQUFFO1lBQ3ZDLElBQ0UsSUFBSSxDQUFDLFNBQVM7Z0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxFQUNyQztnQkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEI7aUJBQU07Z0JBQ0wsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUM7Z0JBQzFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSztvQkFDdkMsMENBQTBDO29CQUMxQyx3Q0FBd0M7b0JBQ3hDLFlBQVk7b0JBQ1osS0FBSztvQkFDTCxtRUFBbUU7b0JBQ25FLEtBQUs7Z0JBQ1AsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUN0RSxDQUFDO2dCQUNGLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNqQjtTQUNGO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUU7Z0JBQzVDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNoQjtTQUNGO0lBQ0gsQ0FBQztJQUNELG1EQUFxQixHQUFyQixVQUFzQixjQUFjLEVBQUUsUUFBUTtRQUM1QyxJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakUsSUFBSSxVQUFVLEdBQUcsTUFBTSxDQUNyQixJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFDaEUsb0JBQW9CLENBQ3JCLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUN4QyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN4RCxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxTQUFTLHdCQUFRLElBQUksQ0FBQyxTQUFTLEVBQUssZ0JBQWdCLENBQUUsQ0FBQztRQUM1RCxJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFFLENBQUM7UUFDakUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUNyQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7WUFDdEMsSUFBSSxRQUFRLENBQUM7WUFDYixJQUNFLElBQUksQ0FBQyxNQUFNO2dCQUNYLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUN2QztnQkFDQSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3BEO2lCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUN2QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUN2QjtxQkFBTSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDbkMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQztnQkFDRCxpQ0FBaUM7Z0JBQ2pDLHFDQUFxQztnQkFDckMsSUFBSTtxQkFDQztvQkFDSCxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0Y7WUFDRCxJQUFJLFFBQVEsRUFBRTtnQkFDWixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlO29CQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7b0JBQzFCLENBQUMsQ0FBQyxRQUFRLENBQUM7YUFDZDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFDRCw0Q0FBYyxHQUFkLFVBQWUsR0FBRztRQUNoQixJQUFJLEdBQUcsQ0FBQyxPQUFPLEVBQUU7WUFDZixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztZQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxLQUFLLENBQUM7U0FDakM7SUFDSCxDQUFDO0lBRUQsNkNBQWUsR0FBZixVQUFnQixNQUFNLEVBQUUsT0FBTztRQUM3QixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM1RCxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM5QzthQUFNO1lBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0gsQ0FBQztJQUNELGtEQUFvQixHQUFwQixVQUFxQixLQUFLLEVBQUUsSUFBSTtRQUM5QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBVSxJQUFJO1lBQzVCLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxLQUFLLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDO2FBQzlCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQzthQUNwQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELDJDQUFhLEdBQWIsVUFBYyxVQUFVLEVBQUUsT0FBTztRQUFqQyxpQkF3SEM7UUF2SEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNuRCxJQUFJLGFBQWEsRUFBRSxjQUFjLEVBQUUsTUFBTSxDQUFDO1FBQzFDLElBQUksV0FBVyxHQUFHLEVBQUUsRUFDbEIsVUFBVSxDQUFDO1FBQ2IsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3hFLElBQUksYUFBYSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztZQUNyRCxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7WUFDL0MsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUN2RSxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixVQUFVLHdCQUFRLFVBQVUsRUFBSyxVQUFVLENBQUUsQ0FBQztZQUM5QyxjQUFjLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztTQUM1QzthQUFNO1lBQ0wsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQztZQUN4QyxhQUFhLEdBQUcsV0FBVztnQkFDekIsQ0FBQyxDQUFDLFdBQVc7Z0JBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztZQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDckQsY0FBYyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7WUFDM0MsTUFBTSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUM7U0FDL0I7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxJQUFJO1lBQ3RDLElBQUksUUFBUSxDQUFDO1lBQ2IsSUFDRSxJQUFJLENBQUMsTUFBTTtnQkFDWCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDdkM7Z0JBQ0EsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN0QixJQUFJLE1BQUksR0FBRyxFQUFFLENBQUM7b0JBQ2QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFDakQsUUFBUTt3QkFFUixNQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN0QixDQUFDLENBQUMsQ0FBQztvQkFDSCxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFJLENBQUMsQ0FBQztpQkFDakM7cUJBQU07b0JBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDcEQ7YUFDRjtpQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDdkIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNwQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZTt3QkFDM0IsQ0FBQyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzt3QkFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7b0JBQ2YsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDOUQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUM3QixRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDM0Q7cUJBQU07b0JBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNyRTthQUNGO1lBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNyRSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDeEM7WUFDRCxJQUFJLFFBQVE7Z0JBQ1YsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZTtvQkFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO29CQUMxQixDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxhQUFhLENBQUMsY0FBYyxFQUFFO1lBQ2hDLDJCQUEyQjtZQUMzQixnQkFBZ0I7WUFDaEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLE1BQU07Z0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLElBQUksbUJBQW1CLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQztnQkFDckQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDOUQsVUFBQyxHQUFHO29CQUNGLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztvQkFDRixJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO3dCQUNyQixZQUFZLENBQUMsVUFBVSxDQUFDLG1CQUFtQixDQUFDLENBQUM7d0JBQzdDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQ3JEO2dCQUNILENBQUMsRUFDRCxVQUFDLEtBQUs7b0JBQ0osSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3ZCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztnQkFDSixDQUFDLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxjQUFjO2lCQUNoQixpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN0QyxTQUFTLENBQUMsVUFBQyxJQUFJO2dCQUNkLElBQ0UsS0FBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVO29CQUMxQixLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQ3JDO29CQUNBLElBQUksbUJBQW1CLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQztvQkFDckQsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO29CQUNGLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7aUJBQzNCO3FCQUFNO29CQUNMLElBQUksUUFBUSxHQUFHLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3ZELGFBQWEsQ0FBQyxnQkFBZ0IsQ0FDL0IsQ0FBQztvQkFDRixLQUFJLENBQUMsa0JBQWtCLENBQ3JCLElBQUksRUFDSixRQUFRLEdBQUcsVUFBVSxDQUFDLFFBQVEsRUFDOUIsVUFBVSxDQUFDLGFBQWEsQ0FDekIsQ0FBQztpQkFDSDtZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ047SUFDSCxDQUFDO0lBRUQsZ0RBQWtCLEdBQWxCLFVBQW1CLFdBQVcsRUFBRSxRQUFRLEVBQUUsYUFBYTtRQUNyRCxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7UUFDOUQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELGdEQUFrQixHQUFsQixVQUFtQixLQUFtQyxFQUFFLEVBQUU7UUFDeEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU07Z0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNULElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ2IsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoRDtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUVELG1EQUFxQixHQUFyQixVQUFzQixLQUF3QjtRQUM1QyxJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsVUFBQyxJQUFJO2dCQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztZQUMvQixDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBQ0QsMkNBQWEsR0FBYixVQUFjLEtBQUs7UUFBbkIsaUJBNEJDO1FBM0JDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLE1BQU0sQ0FBQztRQUNYLElBQUksYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUU7WUFDekIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUk7Z0JBQzdDLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztnQkFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQUMsSUFBSTtvQkFDMUMsYUFBYSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO29CQUM5RCxRQUFRLEdBQUc7d0JBQ1QsVUFBVSxFQUFFLElBQUksQ0FBQyxNQUFNO3dCQUN2QixXQUFXLEVBQUUsS0FBSzt3QkFDbEIsSUFBSSxFQUFFLEtBQUs7cUJBQ1osQ0FBQztnQkFDSixDQUFDLENBQUMsQ0FBQztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsVUFBVSx3QkFBUSxhQUFhLEVBQUssUUFBUSxDQUFFLENBQUM7WUFDL0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUNqRSxVQUFDLElBQUk7Z0JBQ0gsS0FBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLENBQUMsRUFDRCxVQUFDLEtBQUs7Z0JBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQixDQUFDLENBQ0YsQ0FBQztTQUNIO0lBQ0gsQ0FBQztJQUNELDBDQUFZLEdBQVosVUFBYSxRQUFRLEVBQUUsS0FBSztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3pELElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUNyQixDQUFDO1FBQ0YsSUFBSSxLQUFLLEtBQUssS0FBSyxFQUFFO1lBQ25CLElBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLENBQUMsQ0FBQztZQUN4RCxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQ2pEO2FBQU07WUFDTCxJQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxDQUFDLENBQUM7WUFDekQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUMsQ0FBQztTQUNsRDtJQUNILENBQUM7SUFDRCwwQ0FBWSxHQUFaLFVBQWEsS0FBSztRQUFsQixpQkF5QkM7UUF4QkMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO1lBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7WUFDOUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNQLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsSUFBSSxFQUFFLENBQUM7UUFDaEUsSUFDRSxJQUFJLENBQUMsVUFBVTtZQUNmLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO1lBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUMzQztZQUNBLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQzVDLENBQUM7U0FDSDthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztTQUNoRTtRQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7WUFDM0QsVUFBVSxDQUFDO2dCQUNULEtBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7WUFDcEMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ1Q7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRCx5Q0FBVyxHQUFYLFVBQVksS0FBSztRQUFqQixpQkFPQztRQU5DLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUMsWUFBWSxDQUFDLENBQUM7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsVUFBVSxDQUFDLENBQUM7UUFDN0IsVUFBVSxDQUFDO1lBQ1QsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUNoQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUE7SUFDVCxDQUFDO0lBRUQsdUNBQVMsR0FBVCxVQUFVLEtBQUs7UUFDYixLQUFLLEdBQUcsS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQyxJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDL0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDNUUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGVBQWUsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLDZCQUE2QixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ2xFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixFQUFFO1lBQ3hFLDhCQUE4QjtZQUM5QixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUNoRCxJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUN4QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGNBQWMsQ0FBQztZQUM5QyxJQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDO2dCQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQztnQkFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUMsRUFDNUM7Z0JBQ0EsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQzdELE9BQU8sQ0FDUixDQUFDLE1BQU0sQ0FBQztvQkFDUCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBQzlDLENBQUMsQ0FBQyxFQUFFLENBQUM7YUFDUjtZQUNELElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtnQkFDcEUsMEJBQTBCO2dCQUMxQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUNoRSxXQUFXLENBQ1osQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNaO1NBQ0Y7YUFBTTtTQUNOO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0I7WUFDOUQsQ0FBQyxDQUFDLElBQUk7WUFDTixDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ1osQ0FBQztJQUVELDBDQUFZLEdBQVo7UUFDRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDcEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixFQUFFO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FDakUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzlCLENBQUM7U0FDSDtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUE7UUFDcEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLE1BQU07WUFDbkMsSUFBSSxNQUFNLElBQUksSUFBSSxFQUFFO2dCQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3hCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsNkNBQWUsR0FBZixVQUFnQixRQUFRO1FBQ3RCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFFeEQsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNyRSxJQUFJLENBQUMsU0FBUyx3QkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLFFBQVEsQ0FBRSxDQUFDO1NBQ3JEO1FBQ0QsSUFDRSxRQUFRO1lBQ1IsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUM7WUFDM0MsUUFBUSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxNQUFNLEVBQ2xEO1lBQ0EsSUFBSSxDQUFDLFNBQVMsd0JBQVEsSUFBSSxDQUFDLFNBQVMsRUFBSyxRQUFRLENBQUUsQ0FBQztTQUNyRDtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRTtTQUNyQztRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLG9CQUFvQixDQUFDLENBQUM7UUFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRTtZQUN2QyxJQUNFLElBQUksQ0FBQyxTQUFTO2dCQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQztnQkFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsTUFBTSxFQUN4RDtnQkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEI7aUJBQU0sSUFDTCxJQUFJLENBQUMsU0FBUztnQkFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNO2dCQUNyQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLEVBQ2xDO2dCQUNBLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNoQjtpQkFBTTtnQkFDTCxJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQztnQkFDbkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQ3RFLENBQUM7Z0JBQ0YsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2pCO1NBQ0Y7YUFBTSxJQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CO1lBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUM5QjtZQUNBLHdDQUF3QztZQUN4QyxJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLEVBQzlDLFVBQVUsSUFBSTtnQkFDWixPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDO1lBQ2pDLENBQUMsQ0FDRixDQUFDO1lBQ0YsSUFBSSxRQUFRLEVBQUU7Z0JBQ1osUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hCO2lCQUFNO2dCQUNMLElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDO2dCQUNuRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FDdEUsQ0FBQztnQkFDRixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztnQkFDNUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2pCO1NBQ0Y7YUFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CLEVBQUU7WUFDL0MsSUFDRSxJQUFJLENBQUMsU0FBUztnQkFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDO2dCQUNyQyxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQ2pDO2dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDO3FCQUNwRSxNQUFNLEVBQ1Q7Z0JBQ0EsSUFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsSUFBSSxLQUFLO29CQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQ2pDLENBQUMsTUFBTSxJQUFJLENBQUMsRUFDYjtvQkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2hCO3FCQUFNLElBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQUksV0FBVztvQkFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUNqQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQ2I7b0JBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNoQjtxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUU7b0JBQ3RDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3JFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDaEI7cUJBQU07b0JBQ0wsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7b0JBQ25FLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztvQkFDRixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztvQkFDNUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQjthQUNGO2lCQUFNO2dCQUNMLCtEQUErRDtnQkFDL0Qsd0VBQXdFO2dCQUN4RSxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFO29CQUNoQyxJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQztvQkFDbkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO29CQUNGLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO29CQUM1QixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2pCO3FCQUFNLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTtvQkFDeEMsSUFBSSxNQUFJLEdBQUcsSUFBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLFVBQVUsSUFBSTt3QkFDckQsb0NBQW9DO3dCQUNwQyxJQUNFLE1BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDOzRCQUN2QixNQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQ25DOzRCQUNBLE1BQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDOzRCQUM1QixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ2pCOzZCQUFNLElBQ0wsTUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7NEJBQ3ZCLE1BQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsRUFDbEM7NEJBQ0EsTUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7NEJBQzdCLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDaEI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7cUJBQU07b0JBQ0wsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7b0JBQ25FLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztvQkFDRixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2pCO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFO2dCQUM1QyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEI7U0FDRjtJQUNILENBQUM7SUFDRCw4Q0FBZ0IsR0FBaEI7UUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRCxzQ0FBUSxHQUFSLFVBQVMsUUFBUTtRQUFqQixpQkE0RkM7UUEzRkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztRQUNsRCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQztRQUM5RCxZQUFZO1FBQ1osSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFO1lBQ2pELElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVUsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLGFBQWEsRUFBRTtvQkFDeEIsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7b0JBQ3BDLElBQ0UsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJO3dCQUNwQixNQUFJLENBQUMsU0FBUzt3QkFDZCxNQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzt3QkFDcEIsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUMxQzt3QkFDQSxNQUFJLENBQUMsU0FBUzs0QkFDWixDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3RELE1BQUksQ0FBQyxZQUFZOzRCQUNmLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDMUQ7aUJBQ0Y7Z0JBQ0QsSUFBSSxDQUFDLENBQUMsY0FBYyxFQUFFO29CQUNwQiw4REFBOEQ7b0JBQzlELElBQUksTUFBTSxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO29CQUNyQyxJQUFJLGNBQVksR0FBRyxDQUFDLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztvQkFDN0MsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUM7b0JBQy9DLElBQUksT0FBSyxHQUFHLEVBQUMsS0FBSyxFQUFFLE1BQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO3dCQUMxQyxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUU7NEJBQ3pCLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQzt5QkFDN0M7NkJBQU07NEJBQ0wsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDN0Q7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsTUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsT0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLElBQUk7d0JBQzlELENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFZLENBQWEsQ0FBQztvQkFDbkQsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDOUMsSUFDRSxJQUFJLENBQUMsVUFBVTtZQUNmLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxFQUNuRTtZQUNBLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7WUFDbkMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3pELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7b0JBQ25FLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7b0JBQzlDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1AsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDO2dCQUMxRCxJQUNFLElBQUksQ0FBQyxVQUFVO29CQUNmLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO29CQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFDM0M7b0JBQ0EsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FDNUMsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO2lCQUNoRTtnQkFDRCxVQUFVLENBQUM7b0JBQ1QsS0FBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztnQkFDcEMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ1Q7U0FDRjtRQUVELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pELENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxVQUM1QyxnQkFBZ0I7Z0JBRWhCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsd0JBQXdCLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUU7b0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQ1QsZ0JBQWdCLENBQUMsYUFBYSxFQUM5QixtQ0FBbUMsQ0FDcEMsQ0FBQztvQkFDRixnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUM7d0JBQzlDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDdEMsNElBQTRJO2lCQUM3STtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsK0ZBQStGO1NBQ2hHO1FBQ0QsNkRBQTZEO1FBRTdELElBQUk7SUFDTixDQUFDO0lBRUQsa0RBQW9CLEdBQXBCLFVBQXFCLFNBQVMsRUFBRSxlQUFlLEVBQUUsUUFBUTtRQUN2RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsVUFBVSxJQUFJO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUssR0FBRyxLQUFLLENBQUM7YUFDZjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xCLENBQUM7SUFDRCx5Q0FBVyxHQUFYO1FBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUIsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELElBQUksUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDL0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO2dCQUN0QixDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ1AsSUFBSSxXQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDUCxJQUFJLFNBQU8sR0FBRyxFQUFFLENBQUM7WUFDakIsSUFBSSxhQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUM7WUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FDVCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFDL0Isc0NBQXNDLENBQ3ZDLENBQUM7WUFDRixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsSUFBSSxDQUFDLG9CQUFvQixDQUN2QixhQUFXLEVBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQy9CLFVBQVUsS0FBSztvQkFDYixJQUFJLEtBQUssRUFBRTt3QkFDVCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLFVBQVUsT0FBTzs0QkFDdkQsU0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxhQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNyRCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLEtBQUssR0FDUCxXQUFTLElBQUksV0FBUyxDQUFDLE1BQU07NEJBQzNCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFdBQVMsRUFBRSxTQUFPLENBQUM7NEJBQ2pDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDVCxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7NEJBQ2IsV0FBUyxDQUFDLElBQUksQ0FBQyxTQUFPLENBQUMsQ0FBQzt5QkFDekI7d0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxHQUFHLFdBQVMsQ0FBQzt3QkFDdkQsWUFBWSxDQUFDLE9BQU8sQ0FDbEIsY0FBYyxFQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUMvQixDQUFDO3dCQUNGLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3pCO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUM7cUJBQ3pEO2dCQUNILENBQUMsQ0FDRixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDekI7WUFDRCxrRUFBa0U7WUFDbEUsK0RBQStEO1lBQy9ELHlEQUF5RDtZQUN6RCxPQUFPO1lBQ1AsU0FBUztZQUNULDZEQUE2RDtZQUM3RCxJQUFJO1NBQ0w7SUFDSCxDQUFDO0lBQ0Qsc0NBQVEsR0FBUixVQUFTLElBQUksRUFBRSxhQUFhO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDOUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFDaEUsSUFBSSxRQUFRLEdBQUcsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDekMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2pELElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUM5RCxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEQsSUFBSSxhQUFhLENBQUMsbUJBQW1CLEVBQUU7WUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzFDLE9BQU8sR0FBRyxhQUFhLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztTQUN2QztRQUNELFFBQVEsd0JBQU8sUUFBUSxFQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1QyxJQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZELElBQUksTUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsRUFBRSxVQUFTLFdBQVc7Z0JBQy9ELE1BQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNyQyxNQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFBO1NBQ0g7UUFFRCxRQUFRLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUMvQyxRQUFRLENBQUMscUJBQXFCLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDMUIsSUFDRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQztZQUN6QyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFDbkM7WUFDQSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xFLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNoRTthQUFJO1lBQ0gsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBQ2hFO1FBRUQsSUFBSSxhQUFhLENBQUMsbUJBQW1CLEVBQUU7WUFDckMsSUFBSSxtQkFBbUIsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFO2dCQUM1RCxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUs7YUFDdEIsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7U0FDdEU7YUFBSyxJQUFHLGFBQWEsQ0FBQyxjQUFjLEVBQUM7WUFDbEMsSUFBRyxhQUFhLENBQUMsVUFBVSxFQUMzQjtnQkFDRSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVMsZUFBZTtvQkFDNUQsSUFBRyxlQUFlLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxjQUFjLEVBQUM7d0JBQ3RELGVBQWUsQ0FBQyxJQUFJLEdBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDeEQ7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7YUFDRDtpQkFBSTtnQkFDSCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLFVBQVMsZUFBZTtvQkFDOUQsSUFBRyxlQUFlLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxjQUFjLEVBQUM7d0JBQ3RELGVBQWUsQ0FBQyxJQUFJLEdBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQTtxQkFDM0Q7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7YUFDRDtTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7SUFDSCxDQUFDO0lBRUQsbURBQXFCLEdBQXJCLFVBQXNCLG1CQUFtQixFQUFFLFlBQVksRUFBRSxhQUFhO1FBQXRFLGlCQXlHQztRQXhHQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxrQkFBa0IsRUFBRSxhQUFhLENBQUMsQ0FBQztRQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLG1CQUFtQixDQUFDLENBQUM7UUFFNUQsMkZBQTJGO1FBQzNGLElBQUksbUJBQW1CLElBQUksbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUU7WUFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDbEQsWUFBWSxDQUFDLG1CQUFtQixDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDdEQ7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxLQUFLLENBQUM7UUFDVixJQUFJLFVBQVUsQ0FBQztRQUNmLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFBRTtZQUN0RCxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsRUFBRTtnQkFDdEUsSUFBSSxFQUFFLG1CQUFtQixDQUFDLGFBQWE7YUFDeEMsQ0FBQyxDQUFDO1lBQ0gsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3hFO2FBQU07WUFDTCxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FDakIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFDbEMsbUJBQW1CLENBQ3BCLENBQUM7WUFDRixVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN4RDtRQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ2xDLFlBQVk7UUFDWiw4RUFBOEU7UUFDOUUsMEVBQTBFO1FBQzFFLFNBQVM7UUFDVCxNQUFNO1FBQ04sa0NBQWtDO1FBQ2xDLElBQUksbUJBQW1CLElBQUksbUJBQW1CLENBQUMsSUFBSSxFQUFFO1lBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUN0QyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxHQUFHLG1CQUFtQixDQUFDLElBQUksQ0FBQztTQUNwRTthQUFNO1lBQ0wsSUFBSSxtQkFBbUIsQ0FBQyxjQUFjLEVBQUU7Z0JBQ3RDLElBQUksTUFBTSxHQUFHLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3ZELElBQUksY0FBWSxHQUFHLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7Z0JBQy9ELElBQUksT0FBSyxHQUFHLEVBQUMsS0FBSyxFQUFHLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztnQkFDakMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQ3hELFdBQVc7b0JBRVgsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO3dCQUN6QixPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7cUJBQzdDO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFOzRCQUN6QixPQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUM3RDt3QkFDRCxJQUFJLFdBQVcsQ0FBQyxZQUFZLEVBQUU7NEJBQzVCLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsWUFBWSxDQUFDO3lCQUN4QztxQkFDRjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNsRCxtQ0FBbUM7Z0JBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLE9BQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQ3hELFVBQUMsSUFBSTtvQkFDSCxtQ0FBbUM7b0JBQ25DLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FDckQsY0FBWSxDQUNELENBQUM7b0JBQ2QsSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDOUQsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7b0JBQ2pFLElBQUksQ0FBQyxhQUFhLEVBQUU7d0JBQ2xCLElBQU0sVUFBUSxHQUFHLEVBQUUsQ0FBQzt3QkFDcEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxFQUFFLFVBQ2pELEdBQUc7NEJBRUgsVUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ2xELENBQUMsQ0FBQyxDQUFDO3dCQUNILFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEdBQUcsVUFBUSxDQUFDO3FCQUNwRDtvQkFDRCxLQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUM7d0JBQ3hDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQzFDLElBQUksS0FBSyxJQUFJLGFBQWEsQ0FBQyxvQkFBb0IsRUFBRTt3QkFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQzNCLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO3FCQUM3RDtnQkFDSCxDQUFDLEVBQ0QsVUFBQyxHQUFHO29CQUNGLG1DQUFtQztvQkFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQzlCLENBQUMsQ0FDRixDQUFDO2FBQ0g7U0FDRjtRQUNELHdDQUF3QztRQUN4Qyx3QkFBd0I7UUFDeEIsTUFBTTtRQUNOLFVBQVUsd0JBQVEsVUFBVSxFQUFLLG1CQUFtQixDQUFFLENBQUM7UUFDdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUMzQyxnRUFBZ0U7UUFDaEUsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWixLQUFLO1FBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLFVBQVUsQ0FBQztTQUN4RTthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxVQUFVLENBQUM7U0FDeEQ7UUFDRCxlQUFlO1FBQ2Ysd0RBQXdEO1FBQ3hELHdCQUF3QjtRQUN4QixLQUFLO0lBQ1AsQ0FBQztJQUVELGtEQUFvQixHQUFwQixVQUFxQixXQUFXLEVBQUUsVUFBVTtRQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQzdDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLG1CQUFtQixDQUFDLENBQUM7UUFDN0MsSUFBSSxVQUFVLENBQUMsY0FBYyxJQUFJLFVBQVUsQ0FBQyxZQUFZLEVBQUU7WUFDeEQsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO2dCQUNuQixTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxHQUFHLFdBQVcsQ0FBQztnQkFDOUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUM3QyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxFQUNwQyxVQUFVLElBQUk7b0JBQ1osSUFBTSxXQUFXLEdBQUcsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUM5QyxJQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFDbEU7d0JBQ0EsT0FBTyxJQUFJLENBQUM7cUJBQ2I7Z0JBQ0gsQ0FBQyxDQUNGLENBQUM7YUFDSDtpQkFBTTtnQkFDTCxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztvQkFDbEMsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7d0JBQ3RELENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7d0JBQ3pDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUMxQztTQUNGO0lBQ0gsQ0FBQztJQUVELGtEQUFvQixHQUFwQixVQUFxQixLQUFLLEVBQUUsS0FBSyxFQUFFLGFBQWE7UUFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDakMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUNuRCxJQUFJLG1CQUFtQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRTtZQUNuRSxpQkFBaUIsRUFBRSxLQUFLLEdBQUcsQ0FBQztTQUM3QixDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFFLDJCQUEyQixDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixFQUFFLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBQ0QsNENBQWMsR0FBZCxVQUFlLFdBQVcsRUFBRSxVQUFVO1FBQXRDLGlCQWtFQztRQWpFQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ2pELElBQUksVUFBVSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7UUFDcEMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsU0FBUztZQUN0RSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsU0FBUztZQUM5RCxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0IsNkNBQTZDO1FBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixFQUNsRCxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsYUFBYSxFQUFFLENBQ25DLENBQUM7UUFDRixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FDakUsS0FBSyxDQUNOLENBQUM7UUFDRixJQUFJLFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDN0IsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDOUMsSUFBSSxlQUFZLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7WUFDdEQsSUFBSSxPQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsVUFDL0MsV0FBVztnQkFFWCxJQUFJLFdBQVcsQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDO2lCQUN2QztxQkFBTSxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUU7b0JBQ2hDLE9BQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztpQkFDN0M7cUJBQU07b0JBQ0wsT0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDN0Q7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILG1DQUFtQztZQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxPQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUN4RCxVQUFDLElBQUk7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2dCQUNuQyxtQ0FBbUM7Z0JBQ25DLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FDbEQsZUFBWSxDQUNELENBQUM7Z0JBQ2QsSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0QsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ2pFLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQ2xCLElBQU0sVUFBUSxHQUFHLEVBQUUsQ0FBQztvQkFDcEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxFQUFFLFVBQVUsR0FBRzt3QkFDM0QsVUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7b0JBQ2xELENBQUMsQ0FBQyxDQUFDO29CQUNILFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsVUFBUSxDQUFDO2lCQUNqRDtnQkFDRCxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7b0JBQ3JDLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3ZDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7WUFDN0MsQ0FBQyxFQUNELFVBQUMsR0FBRztnQkFDRixtQ0FBbUM7Z0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLENBQUMsQ0FDRixDQUFDO1NBQ0g7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxVQUFVLENBQUM7UUFDekUsV0FBVztRQUNYLDhCQUE4QjtRQUM5QixpQ0FBaUM7UUFDakMsSUFBSTtJQUNOLENBQUM7SUFDRCw0Q0FBYyxHQUFkLFVBQWUsS0FBSyxFQUFFLGNBQWM7UUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLG1CQUFtQixDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM3QiwrQkFBK0I7UUFDL0IsNERBQTREO1FBQzVELHFCQUFxQjtRQUNyQix3REFBd0Q7UUFDeEQsd0JBQXdCO1FBQ3hCLHlEQUF5RDtRQUN6RCx5QkFBeUI7UUFDekIsNkNBQTZDO1FBQzdDLDhFQUE4RTtRQUM5RSw0QkFBNEI7UUFDNUIsa0VBQWtFO1FBQ2xFLHFEQUFxRDtRQUNyRCxRQUFRO1FBQ1IsUUFBUTtRQUNSLHNDQUFzQztRQUN0Qyx5Q0FBeUM7UUFDekMsa0RBQWtEO1FBQ2xELHdEQUF3RDtRQUN4RCxvREFBb0Q7UUFDcEQsSUFBSTtRQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2xELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLGNBQWMsSUFBSSxjQUFjLENBQUMsWUFBWSxFQUFFO1lBQ2pELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDcEM7YUFBTSxJQUFHLGNBQWMsSUFBSSxjQUFjLENBQUMsSUFBSSxJQUFFLGFBQWEsRUFBQztZQUM3RCxJQUFHLEtBQUssQ0FBQyxLQUFLLElBQUUsSUFBSSxFQUFDO2dCQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUMsS0FBSyxDQUFDO2FBQzdCO2lCQUFJO2dCQUNILElBQUksQ0FBQyxnQkFBZ0IsR0FBQyxJQUFJLENBQUM7YUFDNUI7U0FDRjthQUNLO1lBQ0osQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFVBQVUsVUFBVTtnQkFDbkQsSUFBSSxVQUFVLENBQUMsV0FBVyxFQUFFO29CQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7d0JBQ3BDLFVBQVUsQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7aUJBQ25EO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDcEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztTQUN0RTtJQUNILENBQUM7SUFDRCwyQ0FBYSxHQUFiLFVBQWMsS0FBSyxJQUFHLENBQUM7SUFDdkIsMENBQVksR0FBWixVQUFhLEtBQUssRUFBRSxTQUFTO1FBQTdCLGlCQTBFQztRQXpFQyxPQUFPLENBQUMsR0FBRyxDQUFDLDZDQUE2QyxDQUFDLENBQUM7UUFDM0QsSUFBSSxVQUFVLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztRQUM5QixPQUFPLENBQUMsR0FBRyxDQUNULHdCQUF3QixFQUN4QixLQUFLLEVBQ0wsYUFBYSxFQUNiLFNBQVMsRUFDVCxXQUFXLEVBQ1gsVUFBVSxDQUNYLENBQUM7UUFDRixJQUFJLFNBQVMsQ0FBQyxrQkFBa0IsSUFBSSxLQUFLLEVBQUU7WUFDekMsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzFELElBQUksS0FBSyxJQUFJLFVBQVUsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7Z0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztnQkFDaEMsSUFBSSxnQkFBYyxHQUFHLFNBQVMsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLElBQUksUUFBSyxHQUFHLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztnQkFDaEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDLENBQUM7Z0JBQzlDLENBQUMsQ0FBQyxPQUFPLENBQUMsZ0JBQWMsQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO29CQUN6RCxJQUFJLFdBQVcsQ0FBQyxlQUFlLEVBQUU7d0JBQy9CLFFBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQy9EO3lCQUFNLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTt3QkFDaEMsUUFBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO3FCQUM3Qzt5QkFBTTt3QkFDTCxRQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM3RDtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxRQUFLLENBQUMsQ0FBQztnQkFDN0Isb0NBQW9DO2dCQUNwQyxJQUFJLENBQUMsY0FBYztxQkFDaEIsYUFBYSxDQUFDLFFBQUssRUFBRSxnQkFBYyxDQUFDLE1BQU0sQ0FBQztxQkFDM0MsU0FBUyxDQUNSLFVBQUMsSUFBSTtvQkFDSCxvQ0FBb0M7b0JBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUMvQix1QkFBdUI7b0JBQ3ZCLHFCQUFxQjtvQkFDckIsMERBQTBEO29CQUMxRCwwREFBMEQ7b0JBQzFELFdBQVc7b0JBQ1gsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWMsQ0FBQyxZQUFZLENBQUMsRUFBRTt3QkFDL0QsS0FBSSxDQUFDLGVBQWU7NEJBQ2xCLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQztnQ0FDcEQsQ0FBQyxDQUFDLElBQUk7Z0NBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQzt3QkFDWixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUksQ0FBQyxlQUFlLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztxQkFDdEQ7eUJBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTt3QkFDN0IsS0FBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO3FCQUN0RDtvQkFDRCx5QkFBeUI7b0JBQ3pCLCtEQUErRDtvQkFDL0QsZUFBZTtvQkFDZix1REFBdUQ7b0JBQ3ZELGNBQWM7b0JBQ2QseUJBQXlCO29CQUN6QixLQUFLO2dCQUNQLENBQUMsRUFDRCxVQUFDLEdBQUc7b0JBQ0YsbUNBQW1DO29CQUNuQyxJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO3dCQUNyQixLQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDeEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFJLENBQUMsZUFBZSxFQUFFLGlCQUFpQixDQUFDLENBQUM7cUJBQ3REO2dCQUNILENBQUMsQ0FDRixDQUFDO2FBQ0w7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDN0IsOEJBQThCO2dCQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLEtBQUssQ0FBQzthQUM3QjtTQUNGO0lBQ0gsQ0FBQztJQUNELHNDQUFRLEdBQVI7UUFDRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsTUFBTTtZQUNuQyxJQUFJLE1BQU0sRUFBRTtnQkFDVixJQUFJLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNO29CQUM3RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7b0JBQzlDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLGNBQWMsQ0FBQztvQkFDNUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDLGNBQWM7b0JBQy9ELENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1QsSUFBRyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUM7b0JBQ3JELGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQztpQkFDbEQ7cUJBQUssSUFBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBQztvQkFDdEMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFBO2lCQUNoRDtnQkFDRCxJQUFJLFNBQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO2dCQUM1QixnQ0FBZ0M7Z0JBQ2hDLGtDQUFrQztnQkFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsU0FBTyxDQUFDLENBQUM7Z0JBQ3JDLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUM7Z0JBQ25DLElBQUksYUFBVyxHQUFHO29CQUNoQixLQUFLLEVBQUcsSUFBSSxDQUFDLEtBQUs7aUJBQ25CLENBQUM7Z0JBQ0YsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtvQkFDbEQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO3dCQUNsQixhQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7cUJBQ3JDO3lCQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO3dCQUNoQyx5QkFBeUI7d0JBQ3pCLGtEQUFrRDt3QkFDbEQsYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7NEJBQ3BCLFNBQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxTQUFPLENBQUMsYUFBYSxDQUFDLElBQUksS0FBSztnQ0FDdkQsQ0FBQyxDQUFDLEtBQUs7Z0NBQ1AsQ0FBQyxDQUFDLElBQUksQ0FBQztxQkFDWjt5QkFBTSxJQUFHLElBQUksQ0FBQyxhQUFhLEVBQUM7d0JBQzNCLElBQUksY0FBYyxHQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDO3dCQUN6QyxhQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFFLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQ3BEO3lCQUFLO3dCQUNKLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBTyxFQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxVQUFVLENBQUMsQ0FBQzt3QkFDMUMsSUFBRyxVQUFVLEVBQUM7NEJBQ2IsYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTTtnQ0FDcEMsQ0FBQyxDQUFDLFNBQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztnQ0FDbEMsQ0FBQyxDQUFDLFNBQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7eUJBQ3RCO3dCQUVELElBQ0UsYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTOzRCQUNwQyxJQUFJLENBQUMsbUJBQW1CLEVBQ3hCOzRCQUNBLGdFQUFnRTs0QkFDaEUsYUFBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxTQUFPLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO3lCQUN2RDtxQkFDRjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLHFCQUFtQixHQUFHLGNBQWMsQ0FBQyxZQUFZLENBQUM7Z0JBQ3RELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxLQUFLLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsRUFBRTtvQkFDakcsSUFBSSxLQUFLLEdBQUcsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQztvQkFDbEUsSUFBSSxDQUFDLGNBQWM7eUJBQ2hCLGFBQWEsQ0FBQyxhQUFXLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQzt5QkFDekMsU0FBUyxDQUNSLFVBQUMsR0FBRzt3QkFDRixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7d0JBQ0YsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTs0QkFDMUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQzs0QkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQzs0QkFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDckQ7b0JBQ0gsQ0FBQyxFQUNELFVBQUMsS0FBSzt3QkFDSixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3QkFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO29CQUNKLENBQUMsQ0FDRixDQUFDO2lCQUNMO3FCQUFNO29CQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLGFBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQzlELFVBQUMsR0FBRzt3QkFDRixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMscUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7d0JBQ0YsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTs0QkFDckIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQzs0QkFDeEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQzs0QkFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQzt5QkFDckQ7b0JBQ0gsQ0FBQyxFQUNELFVBQUMsS0FBSzt3QkFDSixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3QkFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLHFCQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO29CQUNKLENBQUMsQ0FDRixDQUFDO2lCQUNIO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCwwQ0FBWSxHQUFaLFVBQWEsS0FBSyxFQUFFLFFBQVEsRUFBRSxVQUFVO1FBQXhDLGlCQTRDQztRQTNDQyxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzlDLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFDOUIsSUFBRyxLQUFLLENBQUMsTUFBTSxFQUFDO1lBQ2QsSUFBSSxNQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ3BCLElBQUksUUFBUSxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7WUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUMsTUFBSSxDQUFDLENBQUM7WUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUMsUUFBUSxDQUFDLENBQUM7WUFDdEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1lBQzFCLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDeEMsSUFBSSxnQkFBZ0IsR0FBQyxLQUFLLENBQUM7WUFDM0IsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLGdCQUFnQixFQUFFO2dCQUM5QyxJQUFJLGNBQWMsR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDeEQsZ0JBQWdCLEdBQUMsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUEsQ0FBQyxDQUFBLEtBQUssQ0FBQzthQUNyRTtpQkFBSTtnQkFDSCxnQkFBZ0IsR0FBRyxDQUFDLE9BQU8sSUFBSSxRQUFRLENBQUMsQ0FBQSxDQUFDLENBQUMsSUFBSSxDQUFBLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDdkQ7WUFDRCxJQUFHLGdCQUFnQixFQUFDO2dCQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN6QixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQUksQ0FBQyxDQUFDO2dCQUMzQixNQUFNLENBQUMsTUFBTSxHQUFHO29CQUNkLEtBQUksQ0FBQyxLQUFLLEdBQUcsTUFBSSxDQUFDO29CQUNsQixLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQzt3QkFDekIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNO3FCQUNwQixDQUFDLENBQUM7b0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBSSxDQUFDO29CQUN2QixLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUNyQyxDQUFDLENBQUM7YUFDSDtpQkFBSTtnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLGlEQUFpRDtvQkFDL0MsUUFBUTtvQkFDUixTQUFTLENBQ1osQ0FDRixDQUFDO2FBQ0g7U0FDRjthQUFJO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1NBQ25DO0lBRUgsQ0FBQztJQUNELGlEQUFtQixHQUFuQixVQUFvQixLQUFLLEVBQUUsUUFBUSxFQUFFLFVBQVU7UUFBL0MsaUJBa0VDO1FBakVDLElBQUksTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFLENBQUM7UUFDOUIsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLGdCQUFnQixFQUFFO1lBQzdDLElBQUksTUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixJQUFJLFFBQVEsR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3pCLElBQUksT0FBTyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDeEMsSUFBSSxjQUFjLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDMUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsaUNBQWlDLEVBQUUsY0FBYyxDQUFDLENBQUM7WUFDeEUsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxNQUFJLENBQUMsQ0FBQztnQkFDM0IsTUFBTSxDQUFDLE1BQU0sR0FBRztvQkFDZCxLQUFJLENBQUMsS0FBSyxHQUFHLE1BQUksQ0FBQztvQkFDbEIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7d0JBQ3pCLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTTtxQkFDcEIsQ0FBQyxDQUFDO29CQUNILEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFJLENBQUMsSUFBSSxDQUFDO29CQUNsQyxLQUFJLENBQUMsVUFBVSxHQUFHLE1BQUksQ0FBQztvQkFDdkIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDckMsQ0FBQyxDQUFDO2FBQ0g7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxpREFBaUQ7b0JBQy9DLFVBQVUsQ0FBQyxjQUFjO29CQUN6QixTQUFTLENBQ1osQ0FDRixDQUFDO2FBQ0g7U0FDRjthQUFNO1lBQ0wsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ25ELElBQUksTUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzFCLElBQUksUUFBUSxHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN4Qyx1Q0FBdUM7Z0JBQ3ZDLG9DQUFvQztnQkFDcEMsSUFBSSxPQUFPLElBQUksUUFBUSxFQUFFO29CQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN6QixNQUFNLENBQUMsYUFBYSxDQUFDLE1BQUksQ0FBQyxDQUFDO29CQUMzQixNQUFNLENBQUMsTUFBTSxHQUFHO3dCQUNkLEtBQUksQ0FBQyxLQUFLLEdBQUcsTUFBSSxDQUFDO3dCQUNsQixLQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQzs0QkFDekIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNO3lCQUNwQixDQUFDLENBQUM7d0JBQ0gsS0FBSSxDQUFDLGdCQUFnQixHQUFHLE1BQUksQ0FBQyxJQUFJLENBQUM7d0JBQ2xDLEtBQUksQ0FBQyxVQUFVLEdBQUcsTUFBSSxDQUFDO3dCQUN2QixLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO29CQUNyQyxDQUFDLENBQUM7aUJBQ0g7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO29CQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7b0JBQ2hDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxpREFBaUQ7d0JBQy9DLFFBQVE7d0JBQ1IsU0FBUyxDQUNaLENBQ0YsQ0FBQztpQkFDSDthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBQ0Qsd0NBQVUsR0FBVjtRQUNFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBQ0Qsc0NBQVEsR0FBUjtRQUFBLGlCQXlPQztRQXhPQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDckIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUN2RixJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQztRQUNuRCxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsVUFBVSxJQUFJO1lBQ2pELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbEIsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ3JDO2lCQUFNO2dCQUNMLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU07b0JBQ2xDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUN6QyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDaEM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7WUFDOUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFDLEdBQUc7Z0JBQy9CLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3hDLFdBQVcsR0FBRyxRQUFRLENBQUM7U0FDekI7UUFDRCxJQUFJLG1CQUFtQixHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsYUFBYSxDQUFDLENBQUE7UUFDL0IsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDaEMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFHO1lBQzdDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDakMsNEJBQTRCO1lBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ2xELFlBQVk7WUFDWixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxJQUFJLE1BQU0sRUFBRSxFQUFFLGlDQUFpQztnQkFDckcsSUFBRyxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFDaEM7b0JBQ0EsSUFBSSxDQUFDLGNBQWM7eUJBQ2hCLGFBQWEsQ0FDWixXQUFXLEVBQ1gsYUFBYSxDQUFDLE1BQU0sRUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7b0JBQ3JCLGlDQUFpQztxQkFDbEM7eUJBQ0EsU0FBUyxDQUNSLFVBQUMsR0FBRzt3QkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDN0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO3dCQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7NEJBQzFDLElBQUksS0FBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0NBQzVCLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dDQUNqQyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsR0FBRyxFQUFFO29DQUM1QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO29DQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQ0FDNUQ7cUNBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFVBQVUsRUFBRTtvQ0FDMUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQ0FDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3ZELEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7aUNBQ3hEO3FDQUFLLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUU7b0NBQ3RDLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0NBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29DQUN2RCxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsS0FBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lDQUNyRDs2QkFDRjtpQ0FBTTtnQ0FDTCxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDOzZCQUMzQjt5QkFDRjt3QkFDRCxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNsQyxDQUFDLEVBQ0QsVUFBQyxLQUFLO3dCQUNKLDZCQUE2Qjt3QkFDN0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO3dCQUNGLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7d0JBQ2hELEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQzt3QkFDckMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzt3QkFDdEQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxTQUFTLENBQUM7d0JBQzlDLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7d0JBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNyRCxDQUFDLENBQ0YsQ0FBQztpQkFDSjtxQkFBSTtvQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUNwQyxDQUNGLENBQUM7aUJBQ0Y7YUFDRDtpQkFBTTtnQkFDTCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLElBQUksS0FBSyxFQUFFO29CQUN4RCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUN6RixVQUFDLEdBQUc7d0JBQ0YsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO3dCQUNGLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7d0JBQ2hDLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQzVCLENBQUMsRUFDRCxVQUFDLEtBQUs7d0JBQ0osS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDaEMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO29CQUNKLENBQUMsQ0FDRixDQUFDO2lCQUNIO3FCQUFNLElBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsSUFBSSxVQUFVLEVBQUM7b0JBQ2xFLElBQUcsYUFBYSxDQUFDLGVBQWUsRUFBRTt3QkFDaEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLFVBQVUsSUFBSTs0QkFDckQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dDQUNsQixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7NkJBQ3JDO2lDQUFNLElBQUcsSUFBSSxDQUFDLFlBQVksRUFBQztnQ0FDMUIsSUFBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQ0FDdkMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUNBQ3hKO3FDQUFLLElBQUcsSUFBSSxDQUFDLE1BQU0sRUFBQztvQ0FDbkIsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDeEc7cUNBQUk7b0NBQ0gsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDeEc7NkJBQ0Y7aUNBQUs7Z0NBQ0osV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTTtvQ0FDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0NBQ3pDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDaEM7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7b0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzdDLElBQUksWUFBVSxHQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQyxDQUFBLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDO29CQUMxRixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFDLFlBQVUsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLENBQUMsY0FBYzt5QkFDaEIsYUFBYSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUM7eUJBQzdHLFNBQVMsQ0FDUixVQUFDLEdBQUc7d0JBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsVUFBVSxDQUFDLENBQUM7d0JBQzdCLElBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUM7NEJBQ2xDLEtBQUksQ0FBQyxjQUFjO2lDQUNkLGFBQWEsQ0FBQyxXQUFXLEVBQUUsYUFBYSxDQUFDLG1CQUFtQjtrQ0FDekQsS0FBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUM7a0NBQ2xELEdBQUcsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRSxHQUFHLEdBQUMsWUFBVSxDQUM3QztpQ0FDRixTQUFTLENBQUMsVUFBQyxJQUFJO2dDQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFVBQVUsQ0FBQyxDQUFBO2dDQUN6QixJQUFJLGFBQWEsR0FBRyxZQUFZLEdBQUMsWUFBVSxDQUFDO2dDQUM3QyxzQ0FBc0M7Z0NBQ3pDLGFBQWEsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztnQ0FDaEQsS0FBSSxDQUFDLGtCQUFrQixDQUNyQixJQUFJLENBQUMsSUFBSSxFQUNULGFBQWEsR0FBRyxNQUFNLEVBQUUsaUJBQWlCLENBQzFDLENBQUM7Z0NBQ0YsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQ0FDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQzs0QkFDcEMsQ0FBQyxDQUFDLENBQUM7eUJBQ1I7NkJBQUk7NEJBQ0gsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsMkJBQTJCLENBQUMsQ0FBQzt5QkFDM0Q7b0JBQ0gsQ0FBQyxFQUNELFVBQUMsS0FBSztvQkFDTixDQUFDLENBQ0YsQ0FBQztpQkFFTDtxQkFBSztvQkFDSixJQUFJLE1BQU0sR0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO29CQUNoQyxJQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxJQUFFLFlBQVksRUFBQzt3QkFDMUMsc0NBQXNDO3dCQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7d0JBQ2pDLFdBQVcsQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ2xDO29CQUNELElBQUksQ0FBQyxjQUFjO3lCQUNoQixhQUFhLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQzt5QkFDbEMsU0FBUyxDQUNSLFVBQUMsR0FBRzt3QkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDN0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO3dCQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7NEJBQzFDLDZCQUE2Qjs0QkFDN0IsSUFBSSxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRTtnQ0FDNUIsSUFBSSxRQUFRLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dDQUNqSSxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsR0FBRyxFQUFFO29DQUM1QixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO29DQUMxQixLQUFJLENBQUMsV0FBVyxDQUNkLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUN2QixLQUFJLENBQUMsVUFBVSxDQUNoQixDQUFDO2lDQUNIO3FDQUFNO29DQUNMLEtBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0NBQzFCLEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQ0FDN0M7NkJBQ0Y7aUNBQU07Z0NBQ0wsS0FBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQzs2QkFDM0I7eUJBQ0Y7NkJBQU0sSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTs0QkFDNUIsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQzs0QkFDaEQsS0FBSSxDQUFDLGFBQWEsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDOzRCQUNyQyxLQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDOzRCQUNuRCxLQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxHQUFHLFNBQVMsQ0FBQzs0QkFDOUMsS0FBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQzs0QkFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FDVCxLQUFJLENBQUMsYUFBYSxFQUNsQiwyQ0FBMkMsQ0FDNUMsQ0FBQzt5QkFDSDtvQkFDSCxDQUFDLEVBQ0QsVUFBQyxLQUFLO3dCQUNKLDZCQUE2Qjt3QkFDN0IsS0FBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLEtBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO3dCQUNGLEtBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7d0JBQ2hELEtBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQzt3QkFDckMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzt3QkFDdEQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxTQUFTLENBQUM7d0JBQzlDLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7d0JBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNyRCxDQUFDLENBQ0YsQ0FBQztpQkFDTDthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsOENBQWdCLEdBQWhCLFVBQWlCLEtBQUs7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFDakMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUM5QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO1FBQ2hDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUM7UUFDdEQsSUFBSSxLQUFLLElBQUksS0FBSyxJQUFJLEtBQUssSUFBSSxLQUFLLEVBQUU7WUFDcEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztZQUM5QixJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsZUFBZSxFQUFFO2dCQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFDMUIsWUFBWSxDQUFDLFVBQVUsQ0FDeEIsQ0FBQzthQUNIO1lBQ0QsSUFBRyxZQUFZLElBQUksWUFBWSxDQUFDLGdCQUFnQixFQUFDO2dCQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLFlBQVksQ0FBQyxzQkFBc0IsQ0FBQzthQUM5RTtZQUNELElBQUcsWUFBWSxJQUFJLFlBQVksQ0FBQyxjQUFjLEVBQUM7Z0JBQzdDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQzVCO2lCQUFJO2dCQUNILElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO2FBQzdCO1lBQ0QsSUFBRyxZQUFZLElBQUksWUFBWSxDQUFDLGVBQWUsRUFBRTtnQkFDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEdBQUcsWUFBWSxDQUFDLGVBQWUsQ0FBQzthQUNoRTtZQUNELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLFlBQVksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDbEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsc0JBQXNCLEVBQUUsVUFBUyxVQUFVO29CQUNoRSxJQUFJLENBQUMsU0FBUyxDQUNaLFVBQVUsQ0FBQyxVQUFVLENBQ3RCLEdBQUcsVUFBVSxDQUFDLFlBQVksQ0FBQztnQkFDOUIsQ0FBQyxDQUFDLENBQUE7YUFDSDtZQUNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztTQUVoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxjQUFjLEdBQUcsS0FBSyxDQUFDO1lBQzVCLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxVQUM3RCxJQUFJO2dCQUVKLE9BQU8sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNqRCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUcsWUFBWSxDQUFDLHNCQUFzQixFQUFFO2dCQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLHNCQUFzQixHQUFHLEVBQUUsQ0FBQTthQUM1QztZQUVELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFDN0IsK0JBQStCO1lBQy9CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztTQUNoQztRQUNELElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMzQyxDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBRyxZQUFZLENBQUMsZ0JBQWdCLEVBQUU7WUFDaEMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzdDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMxQyxPQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDL0MsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMvQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNsRDtRQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUNELHlDQUFXLEdBQVgsVUFBWSxJQUFJLEVBQUUsVUFBVTtRQUE1QixpQkF5REM7UUF4REMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDL0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztZQUMzQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7WUFDdkIsSUFBSSxFQUFFLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLFlBQVk7U0FDekUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFDLGFBQWtCO1lBQzlCLElBQUksYUFBYSxFQUFFO2dCQUNqQixLQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUM5QixLQUFJLENBQUMsS0FBSyxFQUNWLGFBQWEsQ0FBQyxRQUFRLEVBQ3RCLFVBQVUsQ0FDWDtxQkFDRSxJQUFJLENBQUMsVUFBQyxNQUFNO29CQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDM0IsS0FBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO29CQUNqQyxLQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsR0FBRyxDQUFDO29CQUN2QyxLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFVBQVUsQ0FBQztvQkFDdEMsSUFDRSxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVE7d0JBQ3hCLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxJQUFJLE1BQU0sRUFDbEM7d0JBQ0EsS0FBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ25DO3lCQUFNO3dCQUNMLEtBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxDQUFDO3FCQUNwQztvQkFDRCxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7b0JBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDO29CQUNuQyxJQUFJLGFBQWEsR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztvQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztvQkFDakQsSUFBSSxJQUFJLEdBQUcsS0FBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsVUFBVSxJQUFJO3dCQUNqRCxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN0RCxDQUFDLENBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO29CQUM3QyxLQUFJLENBQUMsY0FBYzt5QkFDaEIsYUFBYSxDQUNaLFdBQVcsRUFDWCxhQUFhLENBQUMsTUFBTSxFQUNwQixLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FDbkI7eUJBQ0EsU0FBUyxDQUFDLFVBQUMsR0FBRzt3QkFDYixLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUMxQixLQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUN0RCxDQUFDLENBQUMsQ0FBQztvQkFDTCxtRUFBbUU7b0JBQ25FLG1EQUFtRDtvQkFDbkQsd0NBQXdDO29CQUN4QyxrQ0FBa0M7b0JBQ2xDLHFHQUFxRztvQkFDckcsNENBQTRDO29CQUM1QyxNQUFNO2dCQUNSLENBQUMsQ0FBQztxQkFDRCxLQUFLLENBQUMsVUFBQyxLQUFLLElBQU0sQ0FBQyxDQUFDLENBQUM7YUFDekI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFvQkQsMkNBQWEsR0FBYjtRQUFBLGlCQXVDQztRQXRDQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzVCLGtCQUFrQjtZQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMxQjtRQUNELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QixrQkFBa0I7WUFDbEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNoQztRQUNELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDNUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDM0I7UUFDRCxLQUFLLElBQUksS0FBSyxHQUFHLENBQUMsRUFBRSxLQUFLLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hDO1FBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQzNGLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxTQUFTO1lBQzVHLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixLQUFLLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRSxVQUFVLElBQUksQ0FBQyxFQUFFLFVBQVUsRUFBRSxFQUFFO1lBQ3RELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ2pDO1FBQ0QsS0FBSyxJQUFJLFVBQVUsR0FBRyxDQUFDLEVBQUUsVUFBVSxJQUFJLEVBQUUsRUFBRSxVQUFVLEVBQUUsRUFBRTtZQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNuQztRQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQUEsT0FBTztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3JCLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQy9CLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUE7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxPQUFPO1lBQ3hCLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUNELDBDQUFZLEdBQVo7UUFDRSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBQ0QsMkNBQWEsR0FBYjtRQUNFLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFDRCx3Q0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUNELHVDQUFTLEdBQVQ7UUFDRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBQ0QseUNBQVcsR0FBWDtRQUNFLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFDRCwwQ0FBWSxHQUFaLFVBQWEsS0FBSztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDekMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUM7UUFDM0QsMkRBQTJEO0lBQzdELENBQUM7SUFVRCwrQ0FBaUIsR0FBakIsVUFBa0IsQ0FBQztRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN2QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksR0FBRSxDQUFDLENBQUMsS0FBSyxHQUFFLFVBQVUsQ0FBQztRQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBQ0Qsa0RBQW9CLEdBQXBCLFVBQXFCLENBQUM7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDdkIsdUJBQXVCO1FBQ3ZCLHVCQUF1QjtRQUN2QixJQUFJLENBQUMsdUJBQXVCLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFDRCx3Q0FBVSxHQUFWLFVBQVcsQ0FBQztRQUNWLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFBO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFHLE1BQU0sR0FBRSxDQUFDLENBQUMsS0FBSyxHQUFFLFFBQVEsQ0FBQztRQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsd0NBQVUsR0FBVixVQUFXLENBQUM7UUFDVixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNuQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3RDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQTtRQUNsRCxJQUFJLENBQUMsY0FBYyxHQUFFLFFBQVEsR0FBRSxDQUFDLENBQUMsS0FBSyxHQUFJLEtBQUssR0FBQyxNQUFNLEdBQUMsSUFBSSxDQUFDO0lBQzlELENBQUM7SUFFRCx3Q0FBVSxHQUFWLFVBQVcsQ0FBQztRQUNWLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFBO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUM3QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRSxRQUFRLEdBQUUsSUFBSSxDQUFDLG1CQUFtQixHQUFJLEtBQUssR0FBQyxNQUFNLEdBQUMsSUFBSSxDQUFDO0lBRS9FLENBQUM7SUFFRCxzQ0FBUSxHQUFSLFVBQVMsQ0FBQztRQUNSLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFBO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN4QixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUE7UUFDL0MsSUFBSSxDQUFDLGNBQWMsR0FBRSxRQUFRLEdBQUUsT0FBTyxHQUFFLEdBQUcsR0FBRSxJQUFJLENBQUMsYUFBYSxHQUFJLElBQUksQ0FBQztJQUMxRSxDQUFDO0lBRUQsd0NBQVUsR0FBVixVQUFXLENBQUM7UUFDVixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDN0IsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQy9DLElBQUksQ0FBQyxjQUFjLEdBQUUsUUFBUSxHQUFFLE9BQU8sR0FBRSxHQUFHLEdBQUUsQ0FBQyxDQUFDLEtBQUssR0FBSSxJQUFJLENBQUM7SUFDL0QsQ0FBQztJQUVELGlEQUFtQixHQUFuQixVQUFvQixLQUFLLElBQUcsQ0FBQztJQUM3Qiw4Q0FBZ0IsR0FBaEIsVUFBaUIsS0FBSyxJQUFHLENBQUM7O2dCQXZqRzNCLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIseTRnRkFBMkM7O2lCQUU1Qzs7OztnQkE3QlEsNEJBQTRCO2dCQVc1QixjQUFjO2dCQVByQixXQUFXO2dCQVVKLGNBQWM7Z0JBeEJyQixpQkFBaUI7Z0JBNkJWLFlBQVk7Z0JBcENaLGtCQUFrQjtnQkFPekIsaUJBQWlCO2dCQVJWLGVBQWU7Z0JBdUNmLGFBQWE7Z0RBOEdqQixNQUFNLFNBQUMsU0FBUzs7OzZCQXBHbEIsS0FBSzs2QkFDTCxLQUFLOzZCQUNMLEtBQUs7NkJBQ0wsS0FBSztpQ0FDTCxLQUFLO3dCQUNMLEtBQUs7dUJBQ0wsS0FBSzs4QkFDTCxLQUFLOzhCQXFDTCxTQUFTLFNBQUMsYUFBYTs0QkFDdkIsU0FBUyxTQUFDLFdBQVc7a0NBQ3JCLFNBQVMsU0FBQyxNQUFNOztJQW9nR25CLDBCQUFDO0NBQUEsQUF4akdELElBd2pHQztTQW5qR1ksbUJBQW1CO0FBcWpHaEM7SUFJRSxxQkFBWSxPQUFPO1FBQ2pCLElBQUksQ0FBQyxXQUFXLEdBQUcsT0FBTyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBQ0gsa0JBQUM7QUFBRCxDQUFDLEFBUkQsSUFRQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9zbmFja2Jhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEF3c1MzVXBsb2FkU2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9hd3MtczMtdXBsb2FkLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgSW5qZWN0LFxyXG4gIFZpZXdDaGlsZCxcclxuICBFbGVtZW50UmVmLFxyXG4gIElucHV0LFxyXG4gIENoYW5nZURldGVjdG9yUmVmLFxyXG4gIEFmdGVyQ29udGVudEluaXQsXHJcbiAgT25Jbml0LFxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7XHJcbiAgTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCxcclxuICBNYXRBdXRvY29tcGxldGUsXHJcbn0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2F1dG9jb21wbGV0ZVwiO1xyXG5pbXBvcnQgeyBNYXRDaGlwSW5wdXRFdmVudCB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9jaGlwc1wiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcblxyXG5pbXBvcnQge1xyXG4gIEZvcm1CdWlsZGVyLFxyXG4gIEZvcm1Db250cm9sLFxyXG4gIEZvcm1Hcm91cCxcclxuICBWYWxpZGF0b3JzLFxyXG4gIEZvcm1BcnJheSxcclxufSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuXHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSBcIi4uL2NvbnRlbnQvY29udGVudC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE1vZGVsTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGVsLWxheW91dC9tb2RlbC1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLi9fc2VydmljZXMvaW5kZXhcIjtcclxuXHJcbmltcG9ydCAqIGFzIEZpbGVTYXZlciBmcm9tIFwiZmlsZS1zYXZlclwiO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSBcInJ4anMvU3ViamVjdFwiO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmIH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2RpYWxvZ1wiO1xyXG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gXCJtb21lbnRcIjtcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9sb2FkZXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBNYXREYXRlcGlja2VySW5wdXRFdmVudCB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9kYXRlcGlja2VyXCI7XHJcbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJmb3JtLWxheW91dFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vZm9ybS1sYXlvdXQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vZm9ybS1sYXlvdXQuY29tcG9uZW50LnNjc3NcIl0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBmb3JtVmFsdWVzOiBhbnk7XHJcbiAgQElucHV0KCkgc3RlcHBlclZhbDogYW55O1xyXG4gIEBJbnB1dCgpIGltcG9ydERhdGE6IGFueTtcclxuICBASW5wdXQoKSBvbkxvYWREYXRhOiBhbnk7XHJcbiAgQElucHV0KCkgY2hpcExpc3RGaWVsZHM6IGFueTtcclxuICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIGRhdGE6IGFueTtcclxuICBASW5wdXQoKSBmcm9tUm91dGluZzogYW55O1xyXG4gIGltcG9ydENvbnRyb2w6IEltcG9ydFZhbHVlO1xyXG4gIGN1cnJlbnRUYWJsZUxvYWQ6IGFueTtcclxuICBlbmFibGVUZXh0RmllbGRzOiBib29sZWFuO1xyXG4gIGVuYWJsZVNlbGVjdEZpZWxkczogYm9vbGVhbjtcclxuICBlbmFibGVDaGlwRmllbGRzOiBib29sZWFuO1xyXG4gIGVuYWJsZU11bHRpc2VsZWN0RmllbGRzOiBib29sZWFuO1xyXG4gIGVuYWJsZVRhYnM6IGJvb2xlYW47XHJcbiAgZW5hYmxlU2VsZWN0T3B0aW9uID0gdHJ1ZTtcclxuICBxdWVyeVBhcmFtczogYW55O1xyXG4gIGNvbnRlbnREYXRhOiBhbnk7XHJcbiAgY29sdW1uczogYW55O1xyXG4gIGRpc3BsYXllZENvbHVtbnM6IGFueTtcclxuICB0b3RhbDogYW55O1xyXG4gIGVuYWJsZVRhYmxlTGF5b3V0OiBib29sZWFuO1xyXG4gIHZpZXdEYXRhOiBhbnk7XHJcbiAgc3VibWl0dGVkOiBib29sZWFuO1xyXG4gIGlucHV0R3JvdXA6IEZvcm1Hcm91cDtcclxuICBpbnB1dERhdGE6IGFueTtcclxuICBjdXJyZW50Q29uZmlnRGF0YTogYW55O1xyXG4gIHNlbGVjdGVkRmlsZU5hbWU6IGFueTtcclxuICBmaWxlVXBsb2FkOiBhbnk7XHJcbiAgdGFibGVMaXN0OiBhbnk7XHJcbiAgY2hpcFZhbGlkaXRhaW9uOiBib29sZWFuO1xyXG4gIGVuYWJsZU5ld1RhYmxlTGF5b3V0OiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZUNsaWNrID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBidXR0b25zQ29uZmlnRGF0YTogYW55ID0ge307XHJcbiAgZW5hYmxlU2NoZWR1bGUgOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcmVtb3ZhYmxlOiBib29sZWFuID0gdHJ1ZTtcclxuICBzZWxlY3RlZENoaXA6IGFueSA9IHt9O1xyXG4gIHN0ZXBwZXJEYXRhOiBhbnk7XHJcbiAgZm9ybURhdGE6IGFueTtcclxuICBydW5SZXBvcnQ6IGFueTtcclxuICBzaG93RW5kRGF0ZTtcclxuICBwYW5lbE9wZW5TdGF0ZTpib29sZWFuID0gdHJ1ZTtcclxuICBlbmFibGVJbXBvcnREYXRhOiBib29sZWFuID0gZmFsc2U7XHJcbiAgQFZpZXdDaGlsZChcImFjY2Vzc0lucHV0XCIpIGFjY2Vzc0lucHV0OiBFbGVtZW50UmVmO1xyXG4gIEBWaWV3Q2hpbGQoXCJjaGlwSW5wdXRcIikgY2hpcElucHV0OiBFbGVtZW50UmVmPEhUTUxJbnB1dEVsZW1lbnQ+O1xyXG4gIEBWaWV3Q2hpbGQoXCJhdXRvXCIpIG1hdEF1dG9jb21wbGV0ZTogTWF0QXV0b2NvbXBsZXRlO1xyXG5cclxuICBzZWxlY3RlZERhdGFGaWVsZDogYW55O1xyXG4gIGRhdGFJZDogc3RyaW5nO1xyXG4gIGFjY2Vzc0NvbnRyb2xJZDogYW55O1xyXG4gIGVycm9yVGFibGVMaXN0OiBhbnk7XHJcbiAgZXJyb3JMb2dUYWJsZTogYW55O1xyXG4gIGVycm9yU2hvdzogYW55ID0ge307XHJcbiAgc2VsZWN0ZWRUYWJsZVZpZXc6IGFueTtcclxuICBzZWxlY3RUYWJsZUxvYWQ6IGFueTtcclxuICBlbmFibGVTZWxlY3RWYWx1ZVZpZXc6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIHJlYWxtOiBhbnk7XHJcbiAgZW5hYmxlT3B0aW9uRmllbGRzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZGVmYXVsdERhdGFzb3VyY2VOYW1lOiBhbnk7XHJcbiAgc2VsZWN0ZWRDaGlwczogYW55ID0ge307XHJcbiAgc2VsZWN0ZWRDaGlwS2V5TGlzdDogYW55ID0ge307XHJcbiAgZW5hYmxlU2VsZWN0YWJsZSA9IHRydWU7XHJcbiAgZW5hYmxlUmVtb3ZhYmxlID0gdHJ1ZTtcclxuICBmaWxlVHlwZTogYW55O1xyXG4gIGltYWdlOiBhbnkgPSB7fTtcclxuICBzY2hlZHVsZUZpZWxkOiBhbnkgPSB7fTtcclxuICBlbmFibGVSZXBlYXREYXRhOiBib29sZWFuID0gZmFsc2U7XHJcbiAgdXNlckRhdGE6IGFueTtcclxuICByZWNpcGllbnQ6IGFueSA9IFtdO1xyXG4gIHB1YmxpYyBpc05hbWVBdmFpbGFibGU6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIHB1YmxpYyBjaGFybGltaXRjaGVjazogYm9vbGVhbiA9IHRydWU7XHJcbiAgY3VycmVudERhdGE6IGFueTtcclxuICBjaGlwTGVuZ3RoID0gMDtcclxuICBzZWxlY3RlZGNoaXBMZW5ndGggPSAwO1xyXG4gIENoaXBMaW1pdDogYW55O1xyXG4gIENoaXBPcGVyYXRvcjogYW55O1xyXG4gIGRhdGVGaWVsZHM6IGFueSA9IHt9O1xyXG4gIG1pbkRhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gIG1pbkVuZERhdGUgPSBuZXcgRGF0ZSgpO1xyXG4gIGVuYWJsZWRhdGVGaWVsZHM6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgZGF0ZUZvcm1hdGNoZWNrOiBib29sZWFuID0gdHJ1ZTtcclxuICBjbGFzc0RpdjogYW55O1xyXG4gIGNoaXBMaXN0T3BlblZpZXc6IGJvb2xlYW47XHJcbiAgaW5pdGlhbEhlaWdodDogYW55O1xyXG4gIGdldFJ1bGVzV2lkdGg6IGFueTtcclxuICBoaWRkZW5GaXhlZExheW91dDogYW55O1xyXG4gIGR5bmFtaWNGb3JtSGVpZ2h0OiBhbnk7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlOiBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjaGFuZ2VEZXRlY3RvcjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBwdWJsaWMgbWF0RGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8TW9kZWxMYXlvdXRDb21wb25lbnQ+LFxyXG4gICAgcHJpdmF0ZSBBd3NTM1VwbG9hZFNlcnZpY2U6IEF3c1MzVXBsb2FkU2VydmljZSxcclxuICAgIHByaXZhdGUgcmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIHByaXZhdGUgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2UsXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2hcclxuICApIHtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VcIik7XHJcbiAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZU5hbWVcIik7XHJcbiAgICB0aGlzLnJlYWxtPWxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmVhbG1cIik7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICBvZmZzZXQ6IDAsXHJcbiAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSxcclxuICAgICAgcmVhbG06IHRoaXMucmVhbG1cclxuICAgIH07XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLmNsaWNrRXZlbnRNZXNzYWdlXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlQ2xpY2spKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5mb3JtVmFsdWVzLCBcIj4+Pj4+PkZPUk0gVkFMVUVTU1NcIik7XHJcbiAgICAgICAgbGV0IHRlbXBEYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgICAgY29uc29sZS5sb2codGVtcERhdGEsIFwiPj4+PnRlbXBEYXRhXCIpO1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eShKU09OLnBhcnNlKHRlbXBEYXRhKSlcclxuICAgICAgICAgID8gSlNPTi5wYXJzZSh0ZW1wRGF0YSlcclxuICAgICAgICAgIDoge307IFxyXG4gICAgICAgIHRoaXMuc2hvd0RhdGEodGhpcy5pbnB1dERhdGEpO1xyXG4gICAgICB9KTtcclxuICAgIHRoaXMuZW5hYmxlU2VsZWN0VmFsdWVWaWV3ID0gZmFsc2U7XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlXHJcbiAgICAgIC5nZXRNZXNzYWdlKClcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmUpKVxyXG4gICAgICAuc3Vic2NyaWJlKChtZXNzYWdlKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2cobWVzc2FnZSwgXCIuLi4uTUVTU0FHRVwiKTtcclxuICAgICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgICApO1xyXG4gICAgICAgIC8vIGlmKHRoaXMuY3VycmVudENvbmZpZ0RhdGEgJiYgdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5saXN0VmlldyAmJiAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5saXN0Vmlldy5lbmFibGVUYWJsZUxheW91dCl7XHJcbiAgICAgICAgdGhpcy5lbmFibGVUYWJsZUxheW91dCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICB9KTtcclxuICAgIHRoaXMuaW1wb3J0Q29udHJvbCA9IG5ldyBJbXBvcnRWYWx1ZSh7fSk7XHJcblxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5tb2RlbENsb3NlTWVzc2FnZSAvLyB3aGF0aWYgLSBuZXdcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmUpKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj4+ZGF0YSBSZWZyZXNoIGZyb20gZnJvbWRhdGEgdG8gdGFibGVkYXRhXCIpO1xyXG4gICAgICAgIHRoaXMuZGF0YSA9IGRhdGE7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YSkge1xyXG4gICAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICB9XHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmdldFJ1bGVzV2lkdGggPSBudWxsO1xyXG4gICAgdGhpcy5oaWRkZW5GaXhlZExheW91dCA9IG51bGw7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmZvcm1WYWx1ZXMsIFwiLi4uLi5mb3JtVmFsdWVzXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbXBvcnREYXRhLCBcImltcG9ydERhdGE/Pz8/XCIpO1xyXG4gICAgdGhpcy5zZWxlY3RlZENoaXBzID0ge307XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2hpcEtleUxpc3QgPSB7fTtcclxuICAgIC8vZ2V0IHZhbHVlcyBmb3IgY3JvbiBleHByZXNzaW9uIGRyb3Bkb3duXHJcbiAgICB0aGlzLnZpZXdPZnNlY29uZHMoKTtcclxuXHJcbiAgICBsZXQgaW5pdGlhbE9iaiA9IHtcclxuICAgICAgZGF0YXNvdXJjZUlkOiB0aGlzLmRlZmF1bHREYXRhc291cmNlLFxyXG4gICAgICBkYXRhc291cmNlTmFtZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUsXHJcbiAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgfTtcclxuICAgIHRoaXMudXNlckRhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudExvZ2luVXNlclwiKSk7XHJcbiAgICBpZiAodGhpcy51c2VyRGF0YSkge1xyXG4gICAgICB0aGlzLnJlY2lwaWVudC5wdXNoKHRoaXMudXNlckRhdGEuZW1haWwpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5idXR0b25zQ29uZmlnRGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiYnV0dG9uQ29uZmlnXCIpXHJcbiAgICAgID8gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImJ1dHRvbkNvbmZpZ1wiKSlcclxuICAgICAgOiB7fTtcclxuICAgIHRoaXMuZm9ybURhdGEgPSB0aGlzLmRhdGE7XHJcbiAgICBsZXQgdGVtcERhdGEgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wRGF0YSlcclxuICAgICAgPyBKU09OLnBhcnNlKHRlbXBEYXRhKVxyXG4gICAgICA6IHtcclxuICAgICAgICAgIGRhdGFzb3VyY2VJZDogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSxcclxuICAgICAgICAgIGRhdGFzb3VyY2VOYW1lOiB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSxcclxuICAgICAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgICAgIG9mZnNldDogMCxcclxuICAgICAgICB9O1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSB7IC4uLnRoaXMuaW5wdXREYXRhLCAuLi5pbml0aWFsT2JqIH07XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICk7XHJcbiAgICB0aGlzLmN1cnJlbnREYXRhID0gdGhpcy5mcm9tUm91dGluZyA/IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbJ3BhZ2VSb3V0aW5nVmlldyddIDogdGhpcy5jdXJyZW50Q29uZmlnRGF0YVtcImxpc3RWaWV3XCJdO1xyXG4gICAgdGhpcy5zdGVwcGVyRGF0YSA9IHRoaXMuY3VycmVudERhdGEuY3JlYXRlTW9kZWxEYXRhO1xyXG4gICAgdGhpcy5jbGFzc0RpdiA9XHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEgJiYgdGhpcy5jdXJyZW50RGF0YS5iZWZvcmVGcm9tRGl2RWxlbWVudHN0eWxlXHJcbiAgICAgICAgPyB0aGlzLmN1cnJlbnREYXRhLmJlZm9yZUZyb21EaXZFbGVtZW50c3R5bGVcclxuICAgICAgICA6IG51bGw7XHJcbiAgICAgICAgLy8gZGVidWdnZXI7XHJcbiAgICB0aGlzLmluaXRpYWxIZWlnaHQgPVxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhICYmIHRoaXMuY3VycmVudERhdGEuaGVpZ2h0XHJcbiAgICAgICAgPyB0aGlzLmN1cnJlbnREYXRhLmhlaWdodFxyXG4gICAgICAgIDogbnVsbDtcclxuICAgIHRoaXMuZ2V0UnVsZXNXaWR0aCA9XHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMud2lkdGhSZWR1Y2VcclxuICAgICAgICA/IHRoaXMuZm9ybVZhbHVlcy53aWR0aFJlZHVjZVxyXG4gICAgICAgIDogbnVsbDtcclxuICAgIHRoaXMuaGlkZGVuRml4ZWRMYXlvdXQgPVxyXG4gICAgICB0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLmhpZGRlbkZpeGVkTGF5b3V0XHJcbiAgICAgICAgPyB0aGlzLmZvcm1WYWx1ZXMuaGlkZGVuRml4ZWRMYXlvdXRcclxuICAgICAgICA6IG51bGw7XHJcbiAgICAgICAgXHJcbiAgICB0aGlzLmR5bmFtaWNGb3JtSGVpZ2h0ID1cclxuICAgICAgICB0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLmR5bmFtaWNGb3JtSGVpZ2h0XHJcbiAgICAgICAgICA/IHRoaXMuZm9ybVZhbHVlcy5keW5hbWljRm9ybUhlaWdodFxyXG4gICAgICAgICAgOiBudWxsO1xyXG4gICAgLy8gdGhpcy5oaWRkZW5GaXhlZExheW91dCA9IHRoaXMuY3VycmVudERhdGEuaGlkZGVuRml4ZWRMYXlvdXQ7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNsYXNzRGl2KTtcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMpIHtcclxuICAgICAgdGhpcy5lbmFibGVUZXh0RmllbGRzID0gdGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIHRoaXMuZW5hYmxlU2VsZWN0RmllbGRzID0gdGhpcy5mb3JtVmFsdWVzLnNlbGVjdEZpZWxkcyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgdGhpcy5lbmFibGVPcHRpb25GaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMuY2hvb3NlT25lRmllbGRzID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAvLyB0aGlzLmVuYWJsZUNoaXBGaWVsZHMgPSAodGhpcy5jaGlwTGlzdEZpZWxkcyAmJiB0aGlzLmNoaXBMaXN0RmllbGRzLmxlbmd0aCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBcInRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzXCIpO1xyXG4gICAgICB0aGlzLmVuYWJsZUNoaXBGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkcyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgdGhpcy5lbmFibGVUYWJzID0gdGhpcy5mb3JtVmFsdWVzLnRhYkRhdGEgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIHRoaXMuZW5hYmxlTXVsdGlzZWxlY3RGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMubXVsdGlTZWxlY3RGaWVsZHNcclxuICAgICAgICA/IHRydWVcclxuICAgICAgICA6IGZhbHNlO1xyXG4gICAgICB0aGlzLmVuYWJsZWRhdGVGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMuZGF0ZUZpZWxkcyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICB0aGlzLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgdGhpcy52aWV3RGF0YSA9IHt9O1xyXG4gICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgIGlmICh0aGlzLmltcG9ydERhdGEpIHtcclxuICAgICAgdGhpcy5lbmFibGVJbXBvcnREYXRhID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmVuYWJsZVRleHRGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy50ZXh0RmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIGlmIChpdGVtLmlucHV0VHlwZSA9PSBcImVtYWlsXCIpIHtcclxuICAgICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiLCBWYWxpZGF0b3JzLmVtYWlsKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5lbmFibGVkYXRlRmllbGRzKSB7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5kYXRlRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChuZXcgRGF0ZSgpKTtcclxuICAgICAgfSk7XHJcbiAgICAgIC8vIHRoaXMuZGF0ZUZpZWxkcyA9IHRoaXMuZm9ybVZhbHVlcy5kYXRlRmllbGRzO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbCh7XHJcbiAgICAgICAgICB2YWx1ZTogXCJcIixcclxuICAgICAgICAgIGRpc2FibGVkOiBpdGVtLmRpc2FibGUsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZW5hYmxlU2VsZWN0RmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuc2VsZWN0RmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGl0ZW0pO1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5lbmFibGVNdWx0aXNlbGVjdEZpZWxkcykge1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLm11bHRpU2VsZWN0RmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5lbmFibGVPcHRpb25GaWVsZHMpIHtcclxuICAgICAgLy8gdGhpcy5mb3JtVmFsdWVzLmNob29zZU9uZUZpZWxkcy5mb3JFYWNoKGl0ZW0gPT4ge1xyXG4gICAgICAvLyAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgLy8gICB0aGlzLmlucHV0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5maWVsZHNbMF0udmFsdWU7XHJcbiAgICAgIC8vICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiLy8vLy8vL1wiKTtcclxuICAgICAgLy8gICBjb25zb2xlLmxvZyhpdGVtLCBcIi8vLy8vLy9pdGVtXCIpO1xyXG4gICAgICAvLyB9KTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmNob29zZU9uZUZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICB0ZW1wT2JqW2l0ZW0ubmFtZV0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0uZmllbGRzWzBdLnZhbHVlO1xyXG5cclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxmLkNoaXBMaW1pdCwgXCJjaGlwTGltaXQ+Pj5cIik7XHJcbiAgICAgICAgY29uc29sZS5sb2coc2VsZi5pbnB1dERhdGEsIFwiLy8vLy8vL1wiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhpdGVtLCBcIi8vLy8vLy9pdGVtXCIpO1xyXG4gICAgICB9KTtcclxuICAgICAgaWYgKHRoaXMuZm9ybVZhbHVlcy5pc1NldERlZmF1bHRWYWx1ZSkge1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1xyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmNob29zZU9uZUZpZWxkc1swXS5kZWZhdWx0S2V5XHJcbiAgICAgICAgXSA9IHRoaXMuZm9ybVZhbHVlcy5jaG9vc2VPbmVGaWVsZHNbMF0uZGVmYXVsdFZhbHVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmVuYWJsZUNoaXBGaWVsZHMsIFwiZW5hYmxlQ2hpcEZpZWxkc1wiKTtcclxuICAgIGlmICh0aGlzLmVuYWJsZUNoaXBGaWVsZHMpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5lbmFibGVDaGlwRmllbGRzLCBcImVuYWJsZUNoaXBGaWVsZHNcIik7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGl0ZW0sIFwic2VsZmNoaXBsaW1pdFwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhpdGVtLmNoaXBDb25kaXRpb24sIFwiZGF0YT4+Pj4+Pj4+Pj4+XCIpO1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChbXCJcIl0pO1xyXG4gICAgICAgIGlmIChpdGVtLmNoaXBDb25kaXRpb24pIHtcclxuICAgICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwc1tpdGVtLm5hbWVdID0gW107XHJcbiAgICAgICAgICBsZXQgdGVtcCA9IGl0ZW0uY2hpcENvbmRpdGlvbi52YXJpYWJsZTtcclxuICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGEgJiZcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGEgJiZcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbdGVtcF0gJiZcclxuICAgICAgICAgICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dXHJcbiAgICAgICAgICApIHtcclxuICAgICAgICAgICAgc2VsZi5DaGlwTGltaXQgPVxyXG4gICAgICAgICAgICAgIGl0ZW0uY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcImxpbWl0XCJdO1xyXG4gICAgICAgICAgICBzZWxmLkNoaXBPcGVyYXRvciA9XHJcbiAgICAgICAgICAgICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dW1wib3BlcmF0b3JcIl07XHJcbiAgICAgICAgICAgIHNlbGYuQ2hpcExpbWl0ID1cclxuICAgICAgICAgICAgICBpdGVtLmNoaXBDb25kaXRpb24uZGF0YVtzZWxmLmlucHV0RGF0YVt0ZW1wXV1bXCJsaW1pdFwiXTtcclxuICAgICAgICAgICAgc2VsZi5DaGlwT3BlcmF0b3IgPVxyXG4gICAgICAgICAgICAgIGl0ZW0uY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcIm9wZXJhdG9yXCJdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgICAgIC8vIGNoaXBmaWVsZHMgT25Mb2FkIGZ1bmN0aW9uIGFkZGVkIHdoZW4gaW1wbGVtZW50IHJ1biByZXBvcnRzXHJcbiAgICAgICAgICBsZXQgYXBpVXJsID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5hcGlVcmw7XHJcbiAgICAgICAgICBsZXQgcmVzcG9uc2VOYW1lID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5yZXNwb25zZTtcclxuICAgICAgICAgIGxldCByZXF1ZXN0RGF0YSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVxdWVzdERhdGE7XHJcbiAgICAgICAgICBsZXQgcXVlcnkgPSB7cmVhbG06IHNlbGYucmVhbG19O1xyXG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhzZWxmLmlucHV0RGF0YSwgXCI+Pj4+Pj4+XCIpXHJcbiAgICAgICAgICAvL2RlYnVnZ2VyO1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBpdGVtLmRhdGEgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgICAgIGxldCB2YWxpZGF0ZURhdGEgPSBpdGVtLmRhdGFbMF07XHJcbiAgICAgICAgICAgIGxldCBpc0FycmF5T2ZKU09OID0gXy5pc1BsYWluT2JqZWN0KHZhbGlkYXRlRGF0YSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICghaXNBcnJheU9mSlNPTikge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHRlbXBMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24uc3ViUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgIGl0ZW0uZGF0YSA9IGl0ZW0uZGF0YVtpdGVtLm9uTG9hZEZ1bmN0aW9uLnN1YlJlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgIGxldCBrZXlzID0gXy5rZXlzKGl0ZW0uZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goa2V5cywgZnVuY3Rpb24gKGl0ZW1LZXkpIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogaXRlbS5kYXRhW2l0ZW1LZXldW3JlcXVlc3REYXRhW1wia2V5XCJdXSxcclxuICAgICAgICAgICAgICAgICAgICBfaWQ6IGl0ZW1LZXksXHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIF8uZm9yRWFjaChpdGVtLmRhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goeyBuYW1lOiBpdGVtLCBfaWQ6IGl0ZW0gfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaXRlbS5kYXRhID0gdGVtcExpc3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBzZWxmLkNoaXBMaW1pdFtpdGVtLm5hbWVdID1cclxuICAgICAgICAvLyAgIGl0ZW0uY2hpcENvbmRpdGlvbi5kYXRhW1xyXG4gICAgICAgIC8vICAgICBzZWxmLmlucHV0RGF0YVtdXHJcbiAgICAgICAgLy8gICBdO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRDaGlwcywgXCI+Pj4+Pj4+Pj4+IHNlbGVjdGVkY2hpcHNcIik7XHJcbiAgICAvLyB0aGlzLnNlbGVjdGVkQ2hpcHMgPSB0aGlzLnNlbGVjdGVkQ2hpcHM7XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5zY2hlZHVsZUZpZWxkcykge1xyXG4gICAgICBsZXQgaXRlbSA9IHRoaXMuZm9ybVZhbHVlcy5zY2hlZHVsZUZpZWxkcztcclxuICAgICAgT2JqZWN0LmtleXMoaXRlbSkuZm9yRWFjaChmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICB0ZW1wT2JqW3ZhbHVlXSA9IG5ldyBGb3JtQ29udHJvbChbXCJcIl0pO1xyXG4gICAgICB9KTtcclxuICAgICAgLy8gXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5zY2hlZHVsZUZpZWxkcywgZnVuY3Rpb24oaXRlbSkge1xyXG4gICAgICAvLyB0ZW1wT2JqW2l0ZW0ubmFtZV0gPSBuZXcgRm9ybUNvbnRyb2woWycnXSk7XHJcbiAgICAgIC8vIHRlbXBPYmpbaXRlbS50aW1lXSA9IG5ldyBGb3JtQ29udHJvbChbJyddKTtcclxuICAgICAgLy8gdGVtcE9ialtpdGVtLm1lcmlkaWFuXSA9IG5ldyBGb3JtQ29udHJvbChbJyddKTtcclxuICAgICAgLy8gdGVtcE9ialtpdGVtLnJlcGVhdFR5cGVdID0gbmV3IEZvcm1Db250cm9sKFsnJ10pO1xyXG4gICAgICAvLyB0ZW1wT2JqW2l0ZW0uZW5kRGF0ZV0gPSBuZXcgRm9ybUNvbnRyb2woWycnXSk7XHJcbiAgICAgIC8vIHRlbXBPYmpbaXRlbS5lbmREYXRlVGltZV0gPSBuZXcgRm9ybUNvbnRyb2woWycnXSlcclxuICAgICAgLy8gfSlcclxuICAgICAgdGhpcy5zY2hlZHVsZUZpZWxkID0gdGhpcy5mb3JtVmFsdWVzLnNjaGVkdWxlRmllbGRzO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcykge1xyXG4gICAgICB0ZW1wT2JqW3RoaXMuZm9ybVZhbHVlcy5mb3JtRmllbGROYW1lXSA9IHRoaXMuZm9ybUJ1aWxkZXIuYXJyYXkoW1xyXG4gICAgICAgIHRoaXMuY3JlYXRlQ29uZGl0aW9uKCksXHJcbiAgICAgIF0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5pbnB1dEdyb3VwID0gbmV3IEZvcm1Hcm91cCh0ZW1wT2JqKTtcclxuICAgIHRoaXMub25Mb2FkKCk7XHJcblxyXG4gICAgaWYgKHRoaXMuZW5hYmxlVGFicykge1xyXG4gICAgICB0aGlzLmNoYW5nZVRhYigwKTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLnRhYmxlRGF0YSkge1xyXG4gICAgICB0aGlzLnRhYmxlTGlzdCA9IHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy50YWJsZUtleV07XHJcbiAgICAgIGlmICh0aGlzLnRhYmxlTGlzdCAmJiB0aGlzLnRhYmxlTGlzdC5sZW5ndGgpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWQgPSB0aGlzLmZvcm1WYWx1ZXM7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLnRhYmxlTGlzdDtcclxuICAgICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gdGhpcy5mb3JtVmFsdWVzLmVuYWJsZU5ld1RhYmxlTGF5b3V0XHJcbiAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtVmFsdWVzLmlzVG9nZ2xlQmFzZWQpIHtcclxuICAgICAgICBsZXQgdGVtcE9iaiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwic2VsZWN0ZWRUb2dnbGVEZXRhaWxcIik7XHJcbiAgICAgICAgbGV0IHNlbGVjdGVkVG9nZ2xlRGF0YSA9IEpTT04ucGFyc2UodGVtcE9iaik7XHJcbiAgICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRUb2dnbGVEYXRhLCBcIi4uLi5zZWxlY3RlZFRvZ2dsZURhdGFcIik7XHJcbiAgICAgICAgdGhpcy50YWJsZUxpc3QgPSB0aGlzLmN1cnJlbnREYXRhLmZvcm1EYXRhWzBdLnRhYmxlRGF0YTtcclxuICAgICAgICBsZXQgdGVtcFRhYmxlID0gXy5maWx0ZXIodGhpcy50YWJsZUxpc3QsIHtcclxuICAgICAgICAgIHRhYmxlSWQ6IHNlbGVjdGVkVG9nZ2xlRGF0YS50b2dnbGVJZCxcclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0ZW1wVGFibGUsIFwiLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi4udGVtcFRhYmxlXCIpO1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy50YWJsZURhdGEgPSB0ZW1wVGFibGU7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLmZvcm1WYWx1ZXMuaXNkeWFtaWNEYXRhKSB7XHJcbiAgICAgICAgLy8gYWRkZWQgYWRkaXRpb25hbCByZXBvcnQgaW1wbGVtZW50YXRpb25cclxuICAgICAgICAvLyB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgICAvLyAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgICAvLyApO1xyXG4gICAgICAgIC8vIHRoaXMuY3VycmVudERhdGEgPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1widmlld1wiXTtcclxuXHJcbiAgICAgICAgbGV0IHRlbXBPYmogPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcIkN1cnJlbnRSZXBvcnREYXRhXCIpO1xyXG4gICAgICAgIGxldCBSZXBvcnRpdGVtID0gSlNPTi5wYXJzZSh0ZW1wT2JqKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhSZXBvcnRpdGVtLCBcIi4uLi5DdXJyZW50UmVwb3J0RGF0YVwiKTtcclxuICAgICAgICB2YXIgc2VsZWN0ZWRSZXBvcnQgPSBldmFsKHRoaXMuZm9ybVZhbHVlcy5jb25kaXRpb24pO1xyXG4gICAgICAgIHRoaXMudGFibGVMaXN0ID0gdGhpcy5mb3JtVmFsdWVzLnRhYmxlRGF0YTtcclxuXHJcbiAgICAgICAgbGV0IHRlbXBUYWJsZSA9IF8uZmlsdGVyKHRoaXMudGFibGVMaXN0LCB7XHJcbiAgICAgICAgICB0YWJsZUlkOiBzZWxlY3RlZFJlcG9ydCxcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmZvcm1WYWx1ZXMudGFibGVEYXRhID0gdGVtcFRhYmxlO1xyXG4gICAgICAgIC8vIHRoaXMuc2VsZWN0ZWRtb2RlbElkID0gdGhpcy5pbnB1dERhdGEuX2lkO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gdGhpcy5mb3JtVmFsdWVzLmVuYWJsZU5ld1RhYmxlTGF5b3V0XHJcbiAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtVmFsdWVzLmVuYWJsZVRhYmxlTGF5b3V0KSB7XHJcbiAgICAgICAgdGhpcy50YWJsZUxpc3QgPSB0aGlzLmZvcm1WYWx1ZXMudGFibGVEYXRhO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gdGhpcy5mb3JtVmFsdWVzLmVuYWJsZU5ld1RhYmxlTGF5b3V0XHJcbiAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgLy8gbGV0IGN1cnJlbnRUYWJUYWJsZSA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgLy8gdGhpcy5jdXJyZW50VGFibGVMb2FkID0gY3VycmVudFRhYlRhYmxlO1xyXG5cclxuICAgICAgLy8gY29uc29sZS5sb2codGhpcy50YWJsZUxpc3QsIFwiPDw8PDw8PHRhYmxlTGlzdFwiKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNyZWF0ZUNvbmRpdGlvbigpOiBGb3JtR3JvdXAge1xyXG4gICAgbGV0IGZvcm1BcnJheVZhbHVlID0gdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcztcclxuICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICBpZiAoZm9ybUFycmF5VmFsdWUuc2VsZWN0RmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaChmb3JtQXJyYXlWYWx1ZS5zZWxlY3RGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmIChmb3JtQXJyYXlWYWx1ZS5hdXRvQ29tcGxldGVGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKGZvcm1BcnJheVZhbHVlLmF1dG9Db21wbGV0ZUZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICB0ZW1wT2JqW2l0ZW0ubmFtZV0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIHRoaXMuZm9ybUJ1aWxkZXIuZ3JvdXAodGVtcE9iaik7XHJcbiAgfVxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy51bnN1YnNjcmliZS5uZXh0KCk7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlQ2xpY2submV4dCgpO1xyXG4gIH1cclxuICBhZGROZXdGb3JtQXJyYXkoZm9ybUZpZWxkTmFtZSkge1xyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbZm9ybUZpZWxkTmFtZV0gPSB0aGlzLmlucHV0R3JvdXAuZ2V0KGZvcm1GaWVsZE5hbWUpIGFzIEZvcm1BcnJheTtcclxuICAgIC8vIGlmICh0aGlzLmlucHV0RGF0YVtmb3JtRmllbGROYW1lXS5jb250cm9scy5sZW5ndGggPT0gMikge1xyXG4gICAgLy8gXHR0aGlzLmlzRGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgLy8gfVxyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbZm9ybUZpZWxkTmFtZV0ucHVzaCh0aGlzLmNyZWF0ZUNvbmRpdGlvbigpKTtcclxuICAgIGxldCBjb250cm9sID0gPEZvcm1BcnJheT50aGlzLmlucHV0R3JvdXAuY29udHJvbHNbZm9ybUZpZWxkTmFtZV07XHJcbiAgICBjb250cm9sLnB1c2godGhpcy5jcmVhdGVDb25kaXRpb24oKSk7XHJcbiAgfVxyXG4gIG9uTG9hZCgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGNvbnNvbGUubG9nKFwiLj4uLi4uIHNlbGYuaW5wdXREYXRhIFwiLCB0aGlzLmlucHV0RGF0YSk7XHJcbiAgICBpZiAodGhpcy5lbmFibGVTZWxlY3RGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5zZWxlY3RGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgICAgIGxldCBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IHJlcXVlc3REYXRhID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YTtcclxuICAgICAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTogc2VsZi5yZWFsbX07XHJcbiAgICAgICAgICBfLmZvckVhY2gocmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGlmKGl0ZW0uaXNGaWx0ZXIpXHJcbiAgICAgICAgICAgIC8vIHtcclxuICAgICAgICAgICAgLy8gICBsZXQgcmVwb25zZURhdGE9IGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgICAgLy8gICB2YXIgdGVtcERhdGE9cmVwb25zZURhdGEuZmlsdGVyKGZ1bmN0aW9uKHZhbClcclxuICAgICAgICAgICAgLy8gICB7XHJcbiAgICAgICAgICAgIC8vICAgICByZXR1cm4gdmFsW2l0ZW0uZmlsdGVyS2V5XT09aXRlbS5maWx0ZXJ2YWx1ZTtcclxuICAgICAgICAgICAgLy8gICB9KVxyXG4gICAgICAgICAgICAvLyAgIGl0ZW0uZGF0YT10ZW1wRGF0YTtcclxuICAgICAgICAgICAgLy8gfVxyXG4gICAgICAgICAgICBpdGVtLmRhdGEgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcblxyXG4gICAgICAgICAgICAvLyB0ZW1wLnNlbGVjdGVkRGF0YUZpZWxkID0gaXRlbS5kYXRhWzBdLm5hbWU7XHJcbiAgICAgICAgICAgIC8vIHRlbXAuZGF0YUlkID0gaXRlbS5kYXRhWzBdLl9pZDtcclxuICAgICAgICAgICAgLy8gdGVtcC5nZXRDaGlwTGlzdCh0ZW1wLmRhdGFJZClcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+Pj4+Pj4+IG9uTG9hZCBzZWxldGZpZWxkICBcIixpdGVtLm5hbWUpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4+Pj4gT25Mb2FkIGRhdGFcIiwgZGF0YSk7XHJcbiAgICAgICAgICAgIGlmIChpdGVtLnNldERlZmF1bHREcykge1xyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YS5kYXRhc291cmNlTmFtZTtcclxuICAgICAgICAgICAgICBzZWxmLmlucHV0R3JvdXAuY29udHJvbHNbaXRlbS5uYW1lXS5zZXRWYWx1ZShcclxuICAgICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhLmRhdGFzb3VyY2VJZFxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChpdGVtLnNldERlZmF1bHRSdWxlc2V0ICYmICgoc2VsZi5vbkxvYWREYXRhICYmICFzZWxmLm9uTG9hZERhdGEuc2F2ZWREYXRhKSB8fCAhc2VsZi5vbkxvYWREYXRhKSkge1xyXG4gICAgICAgICAgICAgIF8uZm9yRWFjaChpdGVtLmRhdGEsIGZ1bmN0aW9uIChzdWJpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc3ViaXRlbS5kZWZhdWx0UnVsZXNldCkge1xyXG4gICAgICAgICAgICAgICAgICBsZXQgaWQgPSBzdWJpdGVtLl9pZDtcclxuICAgICAgICAgICAgICAgICAgc2VsZi5pbnB1dEdyb3VwLmNvbnRyb2xzW2l0ZW0ubmFtZV0uc2V0VmFsdWUoaWQpO1xyXG4gICAgICAgICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLm5hbWUgKyBcIklkXCJdID0gc3ViaXRlbS5faWQ7XHJcbiAgICAgICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZSArIFwiTmFtZVwiXSA9IHN1Yml0ZW0ubmFtZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+PiBzZWxmLmlucHV0RGF0YSBPbkxvYWQgXCIsIHNlbGYuaW5wdXREYXRhKTtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkoc2VsZi5pbnB1dERhdGEpKTtcclxuXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmVuYWJsZU11bHRpc2VsZWN0RmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMubXVsdGlTZWxlY3RGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgICAgIGxldCBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgICAgICByZWFsbTogc2VsZi5yZWFsbVxyXG4gICAgICAgICAgfTtcclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgaXRlbS5kYXRhID0gZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpZiAodGhpcy5lbmFibGVDaGlwRmllbGRzKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuZW5hYmxlQ2hpcEZpZWxkcywgXCJlbmFibGVDaGlwRmllbGRzXCIpO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgICAgLy8gY2hpcGZpZWxkcyBPbkxvYWQgZnVuY3Rpb24gYWRkZWQgd2hlbiBpbXBsZW1lbnQgcnVuIHJlcG9ydHNcclxuICAgICAgICAgIGxldCBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IHJlcXVlc3REYXRhID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YTtcclxuICAgICAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTogc2VsZi5yZWFsbX07XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhzZWxmLmlucHV0RGF0YSwgXCI+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLnN1YktleSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzTWFwICYmIHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSkge1xyXG4gICAgICAgICAgICAgICAgICAvLyBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSA/IF8ubWFwKHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldLCByZXF1ZXN0SXRlbS5tYXBLZXkpIDogW11cclxuICAgICAgICAgICAgICAgICAgLy8gcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5pc0NvbnZlcnRUb1N0cmluZyA/IEpTT04uc3RyaW5naWZ5KHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdKSA6IHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdO1xyXG4gICAgICAgICAgICAgICAgICBsZXQgdDEgPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgICA/IF8ubWFwKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdEl0ZW0ubWFwS2V5XHJcbiAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgICAgICAgICAgaWYgKHQxLmxlbmd0aClcclxuICAgICAgICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9XHJcbiAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0SXRlbS5pc0NvbnZlcnRUb1N0cmluZyAmJiB0eXBlb2YgdDEgIT09IFwic3RyaW5nXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0MSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgOiB0MTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdKSB7XHJcbiAgICAgICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uaXNDb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdKVxyXG4gICAgICAgICAgICAgICAgICAgIDogcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV07XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgID8gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgc2VsZi5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpXHJcbiAgICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaXRlbS5kYXRhID0gZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgICAgICAgbGV0IHZhbGlkYXRlRGF0YSA9IGl0ZW0uZGF0YVswXTtcclxuICAgICAgICAgICAgICAgIGxldCBpc0FycmF5T2ZKU09OID0gXy5pc1BsYWluT2JqZWN0KHZhbGlkYXRlRGF0YSlcclxuICAgICAgICAgICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWlzQXJyYXlPZkpTT04pIHtcclxuICAgICAgICAgICAgICAgICAgY29uc3QgdGVtcExpc3QgPSBbXTtcclxuICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24uc3ViUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBpdGVtLmRhdGEgPSBpdGVtLmRhdGFbaXRlbS5vbkxvYWRGdW5jdGlvbi5zdWJSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGtleXMgPSBfLmtleXMoaXRlbS5kYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2goa2V5cywgZnVuY3Rpb24gKGl0ZW1LZXkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBpdGVtLmRhdGFbaXRlbUtleV1bcmVxdWVzdERhdGFbXCJrZXlcIl1dLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBfaWQ6IGl0ZW1LZXksXHJcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2goaXRlbS5kYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7IG5hbWU6IGl0ZW0sIF9pZDogaXRlbSB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICBpdGVtLmRhdGEgPSB0ZW1wTGlzdDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0sIDIwMDApO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBzZWxmLkNoaXBMaW1pdFtpdGVtLm5hbWVdID1cclxuICAgICAgICAvLyAgIGl0ZW0uY2hpcENvbmRpdGlvbi5kYXRhW1xyXG4gICAgICAgIC8vICAgICBzZWxmLmlucHV0RGF0YVtdXHJcbiAgICAgICAgLy8gICBdO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLm9uTG9hZERhdGEgJiYgdGhpcy5vbkxvYWREYXRhLnNhdmVkRGF0YSkge1xyXG4gICAgICB0aGlzLmxvYWRTYXZlZERhdGEoKTtcclxuICAgIH1cclxuICB9XHJcbiAgLy8gZ2V0T3B0aW9uVGV4dChvcHRpb24pIHtcclxuICAvLyAgIC8vIGlmIChjb25kaXRpb24pIHtcclxuICAvLyAgICAgY29uc29sZS5sb2cob3B0aW9uLClcclxuICAvLyAgIC8vIH1cclxuICAvLyAgIHJldHVybiBvcHRpb24ubmFtZTtcclxuICAvLyB9XHJcbiAgYWRkQ2hpcEl0ZW0oZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KTogdm9pZCB7fVxyXG4gIHZhbGlkYXRlRGF0ZSh0eXBlOiBTdHJpbmcsIGV2ZW50OiBhbnksIGRhdGVGaWVsZDogYW55KSB7XHJcbiAgICB0aGlzLmRhdGVGb3JtYXRjaGVjayA9IHRydWU7XHJcbiAgICBpZiAoZXZlbnQudmFsdWUpIHtcclxuICAgICAgbGV0IGRpc2JhbGVQcmV2aW91c0RhdGUgPSBkYXRlRmllbGQudmFsaWRhdGV3aXRobmV4dGRhdGUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIGlmIChkaXNiYWxlUHJldmlvdXNEYXRlKSB0aGlzLm1pbkVuZERhdGUgPSBldmVudC52YWx1ZS5hZGQoMSwgXCJkYXlzXCIpLl9kO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5kYXRlRm9ybWF0Y2hlY2sgPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcbiAgb25DaGlwU2VhcmNoKHNlYXJjaFZhbHVlOiBzdHJpbmcsIHNlbGVjdGVkQ2hpcCkge1xyXG4gICAgY29uc29sZS5sb2coc2VhcmNoVmFsdWUsIFwic2VhcmNoVmFsdWU+Pj4+Pj5cIik7XHJcbiAgICB2YXIgY2hhckxlbmd0aCA9IHNlYXJjaFZhbHVlLmxlbmd0aDtcclxuICAgIGxldCBzZWxlY3RlZEluZGV4ID0gdGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMuaW5kZXhPZihzZWxlY3RlZENoaXApO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWFyY2hWYWx1ZVwiXSA9IHNlYXJjaFZhbHVlO1xyXG4gICAgaWYgKHNlbGVjdGVkQ2hpcC5vblNlYXJjaEZ1bmN0aW9uKSB7XHJcbiAgICAgIGxldCBhcGlVcmwgPSBzZWxlY3RlZENoaXAub25TZWFyY2hGdW5jdGlvbi5hcGlVcmw7XHJcbiAgICAgIGxldCByZXNwb25zZU5hbWUgPSBzZWxlY3RlZENoaXAub25TZWFyY2hGdW5jdGlvbi5yZXNwb25zZTtcclxuICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiB0aGlzLnJlYWxtfTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB2YXIgdGVtcCA9IHt9O1xyXG4gICAgICBfLmZvckVhY2goc2VsZWN0ZWRDaGlwLm9uU2VhcmNoRnVuY3Rpb24ucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChcclxuICAgICAgICByZXF1ZXN0SXRlbVxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAocmVxdWVzdEl0ZW0uc3ViS2V5KSB7XHJcbiAgICAgICAgICB0ZW1wID1cclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdICYmXHJcbiAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgIDogXCJcIjtcclxuICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gdGVtcDtcclxuICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICB9IGVsc2UgaWYocmVxdWVzdEl0ZW0uYWx0ZXJuYXRpdmVLZXlDaGVjayl7XHJcbiAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IChzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV09PT11bmRlZmluZWQgKT9zZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS5hbHRlcm5hdGl2ZUtleV06c2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICB2YXIgbGltaXQgPSBzZWxlY3RlZENoaXAuY2hhcmxpbWl0ID8gc2VsZWN0ZWRDaGlwLmNoYXJsaW1pdCA6IDM7XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKFwiTGltaXQgXCIsIGxpbWl0KTtcclxuICAgICAgLy8gaWYgKGNoYXJMZW5ndGggPj0gbGltaXQgfHwgIXNlYXJjaFZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5jaGFybGltaXRjaGVjayA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHNbc2VsZWN0ZWRJbmRleF0uZGF0YSA9IGRhdGEucmVzcG9uc2VbXHJcbiAgICAgICAgICAgIHJlc3BvbnNlTmFtZVxyXG4gICAgICAgICAgXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgIGxldCBpdGVtID0gdGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHNbc2VsZWN0ZWRJbmRleF07XHJcbiAgICAgICAgICBsZXQgdmFsaWRhdGVEYXRhID0gaXRlbS5kYXRhWzBdO1xyXG4gICAgICAgICAgbGV0IGlzQXJyYXlPZkpTT04gPSBfLmlzUGxhaW5PYmplY3QodmFsaWRhdGVEYXRhKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgIGlmICghaXNBcnJheU9mSlNPTikge1xyXG4gICAgICAgICAgICBjb25zdCB0ZW1wTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICBfLmZvckVhY2goaXRlbS5kYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goeyBuYW1lOiBpdGVtLCBfaWQ6IGl0ZW0gfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBpdGVtLmRhdGEgPSB0ZW1wTGlzdDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgLy8gfSBlbHNlIHtcclxuICAgICAgLy8gICBjb25zb2xlLmxvZyhcIlN0b3AgQVBJIFwiKTtcclxuICAgICAgLy8gICB0aGlzLmNoYXJsaW1pdGNoZWNrID0gZmFsc2U7XHJcbiAgICAgIC8vIH1cclxuICAgIH1cclxuICB9XHJcbiAgb25Mb2FkQ2hpcENoYW5nZShpdGVtKSB7XHJcbiAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAvLyBjaGlwZmllbGRzIE9uTG9hZCBmdW5jdGlvbiBhZGRlZCB3aGVuIGltcGxlbWVudCBydW4gcmVwb3J0c1xyXG4gICAgICBsZXQgYXBpVXJsID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5hcGlVcmw7XHJcbiAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICBsZXQgcmVxdWVzdERhdGEgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICBsZXQgcXVlcnkgPSB7cmVhbG06dGhpcy5yZWFsbX07XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXF1ZXN0SXRlbSwgXCI+PnJlcXVlc3RJdGVtXCIpO1xyXG4gICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0SXRlbS5zdWJLZXkpIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc01hcCAmJiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0pIHtcclxuICAgICAgICAgICAgLy8gcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0gPyBfLm1hcChzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XSwgcmVxdWVzdEl0ZW0ubWFwS2V5KSA6IFtdXHJcbiAgICAgICAgICAgIC8vIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uaXNDb252ZXJ0VG9TdHJpbmcgPyBKU09OLnN0cmluZ2lmeShxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSkgOiBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXTtcclxuICAgICAgICAgICAgbGV0IHQxID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgPyBfLm1hcChcclxuICAgICAgICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdW3JlcXVlc3RJdGVtLnN1YktleV0sXHJcbiAgICAgICAgICAgICAgICAgIHJlcXVlc3RJdGVtLm1hcEtleVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICAgIGlmICh0MS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0SXRlbS5pc0NvbnZlcnRUb1N0cmluZyAmJiB0eXBlb2YgdDEgIT09IFwic3RyaW5nXCJcclxuICAgICAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0MSlcclxuICAgICAgICAgICAgICAgICAgOiB0MTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIGlmIChzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0pIHtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5pc0NvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkocXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0pXHJcbiAgICAgICAgICAgICAgOiBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2cocXVlcnksIFwiPj4+cXVlcnlcIik7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiPj4+IFRISVNcIik7XHJcbiAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICBpdGVtLmRhdGEgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdW2l0ZW0ubmFtZV0gPSBpdGVtLmRhdGE7XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdW2l0ZW0ubmFtZV0gPSBfLm1hcChcclxuICAgICAgICAgIGl0ZW0uZGF0YSxcclxuICAgICAgICAgIGl0ZW0ub25Mb2FkRnVuY3Rpb24ua2V5VG9TYXZlXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxmLmlucHV0RGF0YSwgXCI+Pj4gSU5QVVRcIik7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkoc2VsZi5pbnB1dERhdGEpKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9uQ2hpcFNlbGVjdChcclxuICAgIGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50LFxyXG4gICAgc2VsZWN0ZWRDaGlwSWQsXHJcbiAgICBrZXlUb1NhdmUsXHJcbiAgICBjaGlwRXJyb3JcclxuICApOiB2b2lkIHtcclxuICAgIHRoaXMuY2hpcExpc3RPcGVuVmlldyA9IHRydWU7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCJFdmVudD4+Pj4+Pj5cIiwgdGhpcy5DaGlwTGltaXQsIHRoaXMuY2hpcExlbmd0aCk7XHJcbiAgICBjb25zb2xlLmxvZyhzZWxlY3RlZENoaXBJZCwgXCI+PnNlbGVjdGVkQ2hpcElkXCIpO1xyXG4gICAgLy8gY29uc29sZS5sb2coY2hpcEVycm9yLCBcIkV2ZW50Pj4+Pj4+PlwiKTtcclxuICAgIGlmICh0aGlzLkNoaXBMaW1pdCA9PSB0aGlzLmNoaXBMZW5ndGgpIHtcclxuICAgICAgdGhpcy5jaGlwSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9IFwiXCI7XHJcbiAgICAgIC8vIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgIC8vICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KGNoaXBFcnJvcilcclxuICAgICAgLy8gKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY2hpcFZhbGlkaXRhaW9uID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0gPSB0aGlzLnNlbGVjdGVkQ2hpcHNbc2VsZWN0ZWRDaGlwSWRdXHJcbiAgICAgICAgPyB0aGlzLnNlbGVjdGVkQ2hpcHNbc2VsZWN0ZWRDaGlwSWRdXHJcbiAgICAgICAgOiBbXTtcclxuICAgICAgLy8gaWYgKHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0uaW5kZXhPZihldmVudC5vcHRpb24udmFsdWUpIDwgMCkge1xyXG4gICAgICAvLyAgIHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0ucHVzaChldmVudC5vcHRpb24udmFsdWUpO1xyXG4gICAgICAvLyB9XHJcbiAgICAgIGxldCBleGl0SW5kZXg9IF8uZmluZEluZGV4KHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0sIGV2ZW50Lm9wdGlvbi52YWx1ZSlcclxuICAgICAgY29uc29sZS5sb2coJz4+IGV4aXRJbmRleCAnLGV4aXRJbmRleCk7XHJcbiAgICAgIGlmIChleGl0SW5kZXggPCAwKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXS5wdXNoKGV2ZW50Lm9wdGlvbi52YWx1ZSk7XHJcbiAgICAgIH1cclxuICAgICAgY29uc29sZS5sb2codGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIFwiPDw8PDw+Pj4+PlwiKTtcclxuICAgICAgbGV0IHNlbGVjdGVkRm9ybUNoaXAgPSBfLmZpbmQodGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIHtcclxuICAgICAgICBuYW1lOiBzZWxlY3RlZENoaXBJZCxcclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkRm9ybUNoaXAsIFwiPj4+c2VsZWN0ZWRGb3JtQ2hpcFwiKTtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiPDw8PDxpbnB1dERhdGE+Pj4+PlwiKTtcclxuICAgICAgZXZlbnRbXCJ2YWx1ZVwiXSA9IG51bGw7XHJcbiAgICAgIHRoaXMuY2hpcElucHV0Lm5hdGl2ZUVsZW1lbnQudmFsdWUgPSBcIlwiO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkQ2hpcEtleUxpc3Rbc2VsZWN0ZWRDaGlwSWRdID0gXy5tYXAoXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXSxcclxuICAgICAgICBrZXlUb1NhdmVcclxuICAgICAgKTtcclxuICAgICAgLy8gdGhpcy5vbkxvYWRDaGlwQ2hhbmdlKCk7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwS2V5TGlzdFwiXSA9IHRoaXMuc2VsZWN0ZWRDaGlwS2V5TGlzdDtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdID0gdGhpcy5zZWxlY3RlZENoaXBzO1xyXG4gICAgICBpZiAoc2VsZWN0ZWRGb3JtQ2hpcCAmJiBzZWxlY3RlZEZvcm1DaGlwW1wib25DaGFuZ2VMb2FkXCJdKSB7XHJcbiAgICAgICAgbGV0IG9uTG9hZENoaXBEYXRhID0gXy5maW5kKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCB7XHJcbiAgICAgICAgICBuYW1lOiBzZWxlY3RlZEZvcm1DaGlwW1wibG9hZGNoaWxkS2V5XCJdLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMub25Mb2FkQ2hpcENoYW5nZShvbkxvYWRDaGlwRGF0YSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jaGlwTGVuZ3RoID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdW3NlbGVjdGVkQ2hpcElkXS5sZW5ndGg7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRjaGlwTGVuZ3RoID0gdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXS5sZW5ndGg7XHJcbiAgICAgIHRoaXMuaW5wdXRHcm91cC5nZXQoc2VsZWN0ZWRDaGlwSWQpLnNldFZhbHVlKFwiXCIpO1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgfVxyXG4gIH1cclxuICByZW1vdmVTZWxlY3RlZENoaXAodmFsdWUsIHNlbGVjdGVkQ2hpcElkLCBrZXlUb1NhdmUpIHtcclxuICAgIGNvbnNvbGUubG9nKFwicmVtb3ZlU2VsZWN0ZWRDaGlwIFwiLCB0aGlzLmlucHV0RGF0YSk7XHJcbiAgICBsZXQgc2VsZWN0ZWRJbmRleCA9IHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0uaW5kZXhPZih2YWx1ZSk7XHJcbiAgICBpZiAoc2VsZWN0ZWRJbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0uc3BsaWNlKHNlbGVjdGVkSW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zZWxlY3RlZENoaXBLZXlMaXN0W3NlbGVjdGVkQ2hpcElkXSA9IF8ubWFwKFxyXG4gICAgICB0aGlzLnNlbGVjdGVkQ2hpcHNbc2VsZWN0ZWRDaGlwSWRdLFxyXG4gICAgICBrZXlUb1NhdmVcclxuICAgICk7XHJcbiAgICBcclxuICAgIHRoaXMuY2hpcExlbmd0aCA9IHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwc1wiXVtzZWxlY3RlZENoaXBJZF0ubGVuZ3RoO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5jaGlwTGVuZ3RoLFwiPj4+Pj4gU0VMRUNURUQgQ0hJUCBWQUxVRSBMRU5HVEhcIilcclxuICAgIGxldCBzZWxlY3RlZEZvcm1DaGlwID0gXy5maW5kKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCB7XHJcbiAgICAgIG5hbWU6IHNlbGVjdGVkQ2hpcElkLFxyXG4gICAgfSk7XHJcbiAgICBjb25zb2xlLmxvZyhzZWxlY3RlZEZvcm1DaGlwKVxyXG4gICAgaWYodGhpcy5jaGlwTGVuZ3RoKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZEZvcm1DaGlwLCBcIj4+PnNlbGVjdGVkRm9ybUNoaXBcIik7XHJcbiAgICAgICAgaWYgKHNlbGVjdGVkRm9ybUNoaXAgJiYgc2VsZWN0ZWRGb3JtQ2hpcFtcIm9uQ2hhbmdlTG9hZFwiXSkge1xyXG4gICAgICAgICAgbGV0IG9uTG9hZENoaXBEYXRhID0gXy5maW5kKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCB7XHJcbiAgICAgICAgICAgIG5hbWU6IHNlbGVjdGVkRm9ybUNoaXBbXCJsb2FkY2hpbGRLZXlcIl0sXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMub25Mb2FkQ2hpcENoYW5nZShvbkxvYWRDaGlwRGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgfWVsc2V7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZEZvcm1DaGlwW1wibG9hZGNoaWxkS2V5XCJdXSA9IFtdO1xyXG4gICAgfVxyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdID0gdGhpcy5zZWxlY3RlZENoaXBLZXlMaXN0O1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdID0gdGhpcy5zZWxlY3RlZENoaXBzO1xyXG4gIH1cclxuICBsb2FkU2F2ZWREYXRhKCkge1xyXG4gICAgbGV0IHNhdmVkRGF0YSA9IHRoaXMub25Mb2FkRGF0YS5zYXZlZERhdGE7XHJcbiAgICBsZXQgdmlld1NhdmVkRGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbdGhpcy5vbkxvYWREYXRhLmFjdGlvbl1cclxuICAgICAgLnNhdmVkRGF0YURldGFpbHM7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcIl9pZFwiXSA9IHNhdmVkRGF0YS5faWQ7XHJcbiAgICBfLmZvckVhY2godmlld1NhdmVkRGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKGl0ZW0uc3ViS2V5KSB7XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddID0gc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddXHJcbiAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XVxyXG4gICAgICAgICAgOiB7fTtcclxuICAgICAgICBpZiAoaXRlbS5hc3NpZ25GaXJzdEluZGV4dmFsKSB7XHJcbiAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd10gPSAoc2F2ZWREYXRhW2l0ZW0ubmFtZV0gJiYgc2F2ZWREYXRhW2l0ZW0ubmFtZV0ubGVuZ3RoKSA/IHNhdmVkRGF0YVtpdGVtLm5hbWVdWzBdW2l0ZW0uc3ViS2V5XSA6IFwiXCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIC8vIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA9IHNhdmVkRGF0YVtpdGVtLm5hbWVdW2l0ZW0uc3ViS2V5XTtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA9XHJcbiAgICAgICAgICAgIHNhdmVkRGF0YVtpdGVtLm5hbWVdICYmIHNhdmVkRGF0YVtpdGVtLm5hbWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgID8gc2F2ZWREYXRhW2l0ZW0ubmFtZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgIH1cclxuICAgICAgfWVsc2UgaWYoaXRlbS5pc0NvbmRpdGlvbil7XHJcbiAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddID0gIGV2YWwoaXRlbS5jb25kaXRpb24pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA9IHNhdmVkRGF0YVtpdGVtLm5hbWVdO1xyXG4gICAgICB9XHJcbiAgICAgIGlmIChpdGVtLnRha2VLZXlMaXN0KSB7XHJcbiAgICAgICAgaWYoaXRlbS5rZXlMaXN0TmFtZSl7XHJcbiAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleUxpc3ROYW1lXSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5TGlzdE5hbWVdXHJcbiAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5TGlzdE5hbWVdXHJcbiAgICAgICAgICA6IHt9O1xyXG4gICAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlMaXN0TmFtZV1baXRlbS5jaGlwSWRdID0gW107XHJcbiAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleUxpc3ROYW1lXVtpdGVtLmNoaXBJZF0gPSBfLm1hcChcclxuICAgICAgICAgICAgc2F2ZWREYXRhW2l0ZW0ubmFtZV0sXHJcbiAgICAgICAgICAgIGl0ZW0ua2V5VG9TYXZlXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd10gPSBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd10gPyBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd10gOnt9O1xyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XVtpdGVtLmNoaXBJZF0gPSBzYXZlZERhdGFbaXRlbS5uYW1lXTtcclxuICAgICAgICBpZiAoaXRlbS5rZXlUb1Nob3cgPT0gXCJzZWxlY3RlZENoaXBzXCIpIHtcclxuICAgICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwc1tpdGVtLmNoaXBJZF0gPSBzYXZlZERhdGFbaXRlbS5uYW1lXTtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwc1wiXSA9ICBzZWxmLnNlbGVjdGVkQ2hpcHM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChpdGVtLnB1dEtleUxpc3QpIHtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZV0gPVxyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleUxpc3ROYW1lXVtpdGVtLmNoaXBJZF07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLnNlbGVjdGVkRGF0YUtleSkge1xyXG4gICAgICBsZXQgc2VsZWN0ZWREYXRhS2V5ID0gdGhpcy5mb3JtVmFsdWVzLnNlbGVjdGVkRGF0YUtleTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW3NlbGVjdGVkRGF0YUtleV0gJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtzZWxlY3RlZERhdGFLZXldLmxlbmd0aFxyXG4gICAgICApIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkVGFibGVWaWV3ID1cclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzXHJcbiAgICAgICAgICAgID8gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy50YWJsZURhdGFcclxuICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICB0aGlzLnNlbGVjdFRhYmxlTG9hZCA9XHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlc1xyXG4gICAgICAgICAgICA/IHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXNcclxuICAgICAgICAgICAgOiB7fTtcclxuICAgICAgICB0aGlzLnNlbGVjdFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gdGhpcy5pbnB1dERhdGFbc2VsZWN0ZWREYXRhS2V5XTtcclxuICAgICAgICB0aGlzLmVuYWJsZVNlbGVjdFZhbHVlVmlldyA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEgJiYgdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhLmxlbmd0aCkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRUYWJsZVZpZXcgPVxyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXNcclxuICAgICAgICAgICAgPyB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzLnRhYmxlRGF0YVxyXG4gICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkID1cclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzXHJcbiAgICAgICAgICAgID8gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlc1xyXG4gICAgICAgICAgICA6IHt9O1xyXG4gICAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGE7XHJcbiAgICAgICAgdGhpcy5lbmFibGVTZWxlY3RWYWx1ZVZpZXcgPSB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5zaG93U2VsZWN0ZWRDaGlwcykge1xyXG4gICAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5jaGlwQ29uZGl0aW9uKSB7XHJcbiAgICAgICAgbGV0IHRlbXAgPSB0aGlzLmZvcm1WYWx1ZXMuY2hpcENvbmRpdGlvbi52YXJpYWJsZTtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuY2hpcENvbmRpdGlvbi5kYXRhICYmXHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YSAmJlxyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbdGVtcF0gJiZcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy5jaGlwQ29uZGl0aW9uLmRhdGFbdGhpcy5pbnB1dERhdGFbdGVtcF1dXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICB0aGlzLkNoaXBMaW1pdCA9IHRoaXMuZm9ybVZhbHVlcy5jaGlwQ29uZGl0aW9uLmRhdGFbXHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW3RlbXBdXHJcbiAgICAgICAgICBdW1wibGltaXRcIl07XHJcbiAgICAgICAgICB0aGlzLkNoaXBPcGVyYXRvciA9IHRoaXMuZm9ybVZhbHVlcy5jaGlwQ29uZGl0aW9uLmRhdGFbXHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW3RlbXBdXHJcbiAgICAgICAgICBdW1wib3BlcmF0b3JcIl07XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wiY2hpcE9wZXJhdG9yXCJdID0gdGhpcy5DaGlwT3BlcmF0b3I7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICB0aGlzLnZpZXdEYXRhID0gdGhpcy5pbnB1dERhdGE7XHJcbiAgICAvLyB0aGlzLmlucHV0R3JvdXAuY29udHJvbHNbJ3J1bGVzZXQnXS5zZXRWYWx1ZSh0aGlzLmlucHV0RGF0YS5ydWxlc2V0KTtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgfVxyXG5cclxuICBnZXRDaGlwTGlzdCgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+PiBlbnRlcmVkIGNoaXAgZmllbGRzIFwiKTtcclxuXHJcbiAgICBpZiAodGhpcy5lbmFibGVDaGlwRmllbGRzKSB7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiaXRlbSBcIiwgaXRlbSk7XHJcbiAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgICAgIGxldCBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IHJlcXVlc3REYXRhID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YTtcclxuICAgICAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTogc2VsZi5yZWFsbX07XHJcbiAgICAgICAgICBfLmZvckVhY2gocmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmlzQ29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSlcclxuICAgICAgICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgaXRlbS5kYXRhID0gZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgICBsZXQgdmFsaWRhdGVEYXRhID0gaXRlbS5kYXRhWzBdO1xyXG4gICAgICAgICAgICBsZXQgaXNBcnJheU9mSlNPTiA9IF8uaXNQbGFpbk9iamVjdCh2YWxpZGF0ZURhdGEpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAoIWlzQXJyYXlPZkpTT04pIHtcclxuICAgICAgICAgICAgICBjb25zdCB0ZW1wTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgIGlmIChpdGVtLm9uTG9hZEZ1bmN0aW9uLnN1YlJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICBpdGVtLmRhdGEgPSBpdGVtLmRhdGFbaXRlbS5vbkxvYWRGdW5jdGlvbi5zdWJSZXNwb25zZV07XHJcbiAgICAgICAgICAgICAgICBsZXQga2V5cyA9IF8ua2V5cyhpdGVtLmRhdGEpO1xyXG4gICAgICAgICAgICAgICAgXy5mb3JFYWNoKGtleXMsIGZ1bmN0aW9uIChpdGVtS2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IGl0ZW0uZGF0YVtpdGVtS2V5XVtyZXF1ZXN0RGF0YVtcImtleVwiXV0sXHJcbiAgICAgICAgICAgICAgICAgICAgX2lkOiBpdGVtS2V5LFxyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goaXRlbS5kYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wTGlzdC5wdXNoKHsgbmFtZTogaXRlbSwgX2lkOiBpdGVtIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGl0ZW0uZGF0YSA9IHRlbXBMaXN0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgIH1cclxuICB9XHJcbiAgcmVtb3ZlVGFnKGNoaXAsIGlkKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0gPVxyXG4gICAgICB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0gJiYgdGhpcy5zZWxlY3RlZENoaXBbaWRdLmxlbmd0aFxyXG4gICAgICAgID8gdGhpcy5zZWxlY3RlZENoaXBbaWRdXHJcbiAgICAgICAgOiBbXTtcclxuICAgIGNvbnN0IGluZGV4ID0gdGhpcy5zZWxlY3RlZENoaXBbaWRdLmluZGV4T2YoY2hpcCk7XHJcbiAgICBpZiAoaW5kZXggPj0gMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0uc3BsaWNlKGluZGV4LCAxKTtcclxuICAgIH1cclxuICB9XHJcbiAgb25CdXR0b25DbGljayhkYXRhLCBmb3JtVmFsdWVzKSB7XHJcbiAgICBjb25zb2xlLmxvZyhkYXRhLCBcIi4uLi4uREFUQVwiKTtcclxuICAgIGNvbnNvbGUubG9nKGZvcm1WYWx1ZXMsIFwiLi4uLmZvcm1WYWx1ZXNcIik7XHJcbiAgICBpZiAoZGF0YS5hY3Rpb24gPT0gXCJleHBvcnRcIikge1xyXG4gICAgICB0aGlzLm9uRXhwb3J0Q2xpY2soZGF0YSwgZm9ybVZhbHVlcyk7XHJcbiAgICB9IGVsc2UgaWYgKGRhdGEuYWN0aW9uID09IFwic2NoZWR1bGVWaWV3XCIpIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJyZXBvcnROYW1lXCJdID0gdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgIGZvcm1WYWx1ZXMubmFtZVxyXG4gICAgICApO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInJlcG9ydFR5cGVcIl0gPSBmb3JtVmFsdWVzLnJlcG9ydFR5cGU7XHJcbiAgICAgIGlmICghdGhpcy5pbnB1dERhdGFbXCJyZXBlYXRUeXBlXCJdKVxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wicmVwZWF0VHlwZVwiXSA9IFwicnVuX29uY2VcIjtcclxuXHJcbiAgICAgIC8vICB0aGlzLmlucHV0RGF0YVtcInNjaGVkdWxlVHlwZVwiXSA9IGZvcm1WYWx1ZXMuaXNTaW5nbGUgPyAzIDogMjtcclxuICAgICAgbGV0IHJlcXVlc3REYXRhID0gZm9ybVZhbHVlcy5zY2hlZHVsZVJlcXVlc3RcclxuICAgICAgICA/IGZvcm1WYWx1ZXMuc2NoZWR1bGVSZXF1ZXN0LnJlcXVlc3REYXRhXHJcbiAgICAgICAgOiBbXTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gZm9ybVZhbHVlcy5zY2hlZHVsZVJlcXVlc3QudG9hc3RNZXNzYWdlO1xyXG4gICAgICByZXF1ZXN0RGF0YVtcInJlYWxtXCJdID0gdGhpcy5yZWFsbTtcclxuICAgICAgdGhpcy52YWxpZGF0ZVNjaGR1bGVEYXRhKGZ1bmN0aW9uIChyZXN1bHQpIHtcclxuICAgICAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgICAgICBzZWxmLmNvbnN0cnVjdFNjaGVkdWxlRGF0YShyZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJldHVybkRhdGEpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+Pj4uLiByZXR1cm5EYXRhIFwiLCByZXR1cm5EYXRhKTtcclxuICAgICAgICAgICAgaWYgKHJldHVybkRhdGEpIHtcclxuICAgICAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAuY3JlYXRlUmVxdWVzdChyZXR1cm5EYXRhLCBmb3JtVmFsdWVzLnNjaGVkdWxlUmVxdWVzdC5hcGlVcmwpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCIgY3JldGFlIFJlcXVlc3Qgc3VjY2VzcyBcIiwgcmVzKTtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlcy5zdGF0dXMgPT0gMjAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgIHNlbGYubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICBzZWxmLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIGNyZXRhZSBSZXF1ZXN0IEVycm9yIFwiLCBlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgc2VsZi5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSBpZiAoZGF0YS5hY3Rpb24gPT0gXCJyZXNldFZpZXdcIikge1xyXG4gICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICB9IGVsc2UgaWYgKGRhdGEuYWN0aW9uID09IFwiY2FuY2VsXCIpIHtcclxuICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgIH0gZWxzZSBpZihkYXRhLmFjdGlvbj09J2ltcG9ydCcpe1xyXG4gICAgICB0aGlzLm9uSW1wb3J0KCk7XHJcbiAgICB9XHJcbiAgICBlbHNlIGlmIChkYXRhLmFjdGlvbiA9PSBcInN1Ym1pdFZpZXdcIiB8fCBkYXRhLmFjdGlvbiA9PSBcImNoZWNrQ29uZmxpY3RcIiB8fCBkYXRhLmFjdGlvbiA9PSBcInVwZGF0ZVwiKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXRHcm91cCwgXCI+Pj4+Pj4+Pj4+PnNzXCIpO1xyXG4gICAgICBpZiAodGhpcy5mb3JtVmFsdWVzLmdldEZvcm1Db250cm9sVmFsdWUpIHtcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVt0aGlzLmZvcm1WYWx1ZXMudmFsdWVUb1NhdmVdID0gdGhpcy5pbnB1dEdyb3VwLnZhbHVlW1xyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmZvcm1GaWVsZE5hbWVcclxuICAgICAgICBdO1xyXG4gICAgICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMuaXNNZXJnZU9iamVjdCkge1xyXG4gICAgICAgICAgbGV0IG9iaiA9IHRoaXMuaW5wdXRHcm91cC52YWx1ZVt0aGlzLmZvcm1WYWx1ZXMubWFwT2JqZWN0TmFtZV07XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLm9ialswXSB9O1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmlucHV0RGF0YSwgXCIuLi4uLi5JTlBVVCBEQVRBXCIpO1xyXG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZm9ybVZhbHVlcy5nZXRGcm9tY3VycmVudElucHV0KSB7XHJcbiAgICAgICAgbGV0IHRlbXBEYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGEgPSB7IC4uLnRoaXMuaW5wdXREYXRhLCAuLi50ZW1wRGF0YSB9O1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMudmlld0RhdGEgPSB0aGlzLmlucHV0RGF0YTtcclxuICAgICAgdGhpcy5vblN1Ym1pdCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5lbmFibGVUYWJsZUxheW91dCA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiLi4uLkRBVEFcIik7XHJcbiAgICAgIGNvbnNvbGUubG9nKGZvcm1WYWx1ZXMsIFwiLi4uLi4uRm9ybVZhbHVlc1wiKTtcclxuICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgICBpZiAoZm9ybVZhbHVlcy52YWxpZGF0ZUZvcm0pIHtcclxuICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0aW9uQ2hlY2soZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJyZXN1bHQgPjw+PD48Pjw+PFwiLCByZXN1bHQpO1xyXG4gICAgICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgICAgICBsZXQgY29uZmxpY3RSZXF1ZXN0ID0gZm9ybVZhbHVlcy5yZXF1ZXN0RGV0YWlscztcclxuICAgICAgICAgICAgbGV0IHJlcXVlc3REZXRhaWxzID0gY29uZmxpY3RSZXF1ZXN0LnJlcXVlc3REYXRhO1xyXG4gICAgICAgICAgICBsZXQgYXBpVXJsID0gY29uZmxpY3RSZXF1ZXN0LmFwaVVybDtcclxuICAgICAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBjb25mbGljdFJlcXVlc3QudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgICAgICBsZXQgY3VycmVudElucHV0RGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIilcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgbGV0IHJlcXVlc3REYXRhID0ge3JlYWxtOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgIGxldCB0ZW1wRGF0YSA9IGl0ZW0uc3ViS2V5XHJcbiAgICAgICAgICAgICAgICA/IGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgICA6IGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgICAgaWYgKHRlbXBEYXRhKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSlcclxuICAgICAgICAgICAgICAgICAgOiB0ZW1wRGF0YTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uZGlyZWN0QXNzaWduKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShpdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICAgICAgICA6IGl0ZW0udmFsdWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVxdWVzdERhdGEsIFwiLi4uLnJlcXVlc3REYXRhXCIpO1xyXG4gICAgICAgICAgICBzZWxmLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgICAgICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHJlcXVlc3REYXRhLCBhcGlVcmwpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LnJlc3BvbnNlLCBcIi4uLi5kYXRhLnJlc3BvbnNlXCIpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coY29uZmxpY3RSZXF1ZXN0LnJlc3BvbnNlTmFtZSwgXCIuLi4uLlJFU1BPTlNFIE5BTUVcIik7XHJcbiAgICAgICAgICAgICAgICBsZXQgdGVtcERhdGEgPSByZXN1bHQucmVzcG9uc2VbXHJcbiAgICAgICAgICAgICAgICAgIGNvbmZsaWN0UmVxdWVzdC5yZXNwb25zZU5hbWVcclxuICAgICAgICAgICAgICAgIF0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0ZW1wRGF0YSwgXCIuLi4udGVtcERhdGFcIik7XHJcbiAgICAgICAgICAgICAgICBzZWxmLnRhYmxlTGlzdCA9IHRlbXBEYXRhO1xyXG4gICAgICAgICAgICAgICAgLy8gbGV0IGN1cnJlbnRUYWJUYWJsZSA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IGN1cnJlbnRUYWJUYWJsZTtcclxuICAgICAgICAgICAgICAgIHNlbGYuY3VycmVudFRhYmxlTG9hZCA9IHNlbGYuZm9ybVZhbHVlcztcclxuICAgICAgICAgICAgICAgIHNlbGYuY3VycmVudFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gc2VsZi50YWJsZUxpc3Q7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmN1cnJlbnRUYWJsZUxvYWRbXCJ0b3RhbFwiXSA9IHJlc3VsdC5yZXNwb25zZS50b3RhbDtcclxuICAgICAgICAgICAgICAgIHNlbGYuZW5hYmxlVGFibGVMYXlvdXQgPSBzZWxmLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgICAgICAgICAgICAgPyBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICA6IHRydWU7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gc2VsZi5mb3JtVmFsdWVzLmVuYWJsZU5ld1RhYmxlTGF5b3V0XHJcbiAgICAgICAgICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgLy8gc2VsZi5lbmFibGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJFcnIgXCIsIGVycik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBsZXQgY29uZmxpY3RSZXF1ZXN0ID0gZm9ybVZhbHVlcy5yZXF1ZXN0RGV0YWlscztcclxuICAgICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSBjb25mbGljdFJlcXVlc3QucmVxdWVzdERhdGE7XHJcbiAgICAgICAgbGV0IGFwaVVybCA9IGNvbmZsaWN0UmVxdWVzdC5hcGlVcmw7XHJcbiAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBjb25mbGljdFJlcXVlc3QudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgIGxldCBjdXJyZW50SW5wdXREYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcbiAgICAgICAgbGV0IHJlcXVlc3REYXRhID0ge3JlYWxtOiB0aGlzLnJlYWxtfTtcclxuICAgICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICBsZXQgdGVtcERhdGEgPSBpdGVtLnN1YktleVxyXG4gICAgICAgICAgICA/IGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgIDogY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgIGlmICh0ZW1wRGF0YSkge1xyXG4gICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHRlbXBEYXRhKVxyXG4gICAgICAgICAgICAgIDogdGVtcERhdGE7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uZGlyZWN0QXNzaWduKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkoaXRlbS52YWx1ZSlcclxuICAgICAgICAgICAgICA6IGl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVxdWVzdERhdGEsIFwiLi4uLnJlcXVlc3REYXRhXCIpO1xyXG4gICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAgIHRoaXMuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShyZXF1ZXN0RGF0YSwgYXBpVXJsKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3VsdC5yZXNwb25zZSwgXCIuLi4uZGF0YS5yZXNwb25zZVwiKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coY29uZmxpY3RSZXF1ZXN0LnJlc3BvbnNlTmFtZSwgXCIuLi4uLlJFU1BPTlNFIE5BTUVcIik7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wRGF0YSA9IHJlc3VsdC5yZXNwb25zZVtcclxuICAgICAgICAgICAgICBjb25mbGljdFJlcXVlc3QucmVzcG9uc2VOYW1lXHJcbiAgICAgICAgICAgIF0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHRlbXBEYXRhLCBcIi4uLi50ZW1wRGF0YVwiKTtcclxuICAgICAgICAgICAgdGhpcy50YWJsZUxpc3QgPSB0ZW1wRGF0YTtcclxuICAgICAgICAgICAgLy8gbGV0IGN1cnJlbnRUYWJUYWJsZSA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgICAgICAgLy8gdGhpcy5jdXJyZW50VGFibGVMb2FkID0gY3VycmVudFRhYlRhYmxlO1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWQgPSB0aGlzLmZvcm1WYWx1ZXM7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gdGhpcy50YWJsZUxpc3Q7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZFtcInRvdGFsXCJdID0gcmVzdWx0LnJlc3BvbnNlLnRvdGFsO1xyXG4gICAgICAgICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGVOZXdUYWJsZUxheW91dCA9IHRoaXMuZm9ybVZhbHVlcy5lbmFibGVOZXdUYWJsZUxheW91dFxyXG4gICAgICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVyciBcIiwgZXJyKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIHZhbGlkYXRlU2NoZHVsZURhdGEoY2FsbGJhY2spIHtcclxuICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgIGxldCB0ZW1wRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIikpO1xyXG4gICAgaWYgKHRlbXBEYXRhICYmIHRlbXBEYXRhLnNlbGVjdGVkRGF0YSAmJiB0ZW1wRGF0YS5zZWxlY3RlZERhdGEubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhID0geyAuLi50aGlzLmlucHV0RGF0YSwgLi4udGVtcERhdGEgfTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMuZm9ybXVzZWRGb3JDaGVjaykge1xyXG4gICAgfVxyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJjaGlwTGltaXRcIl0gPSB0aGlzLkNoaXBMaW1pdDtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wiY2hpcE9wZXJhdG9yXCJdID0gdGhpcy5DaGlwT3BlcmF0b3I7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmlucHV0RGF0YSwgXCIgU2NoZWR1bGUgaW5wdXREYXRhaW5wdXREYXRhXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dEdyb3VwLCBcIi4uLi4uLi4gdGhpcy5pbnB1dEdyb3VwIFwiKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuaW5wdXRHcm91cC5nZXQoXCJzY2hlZHVsZUZpZWxkXCIpICwgXCItLSBzY2hlZHVsZUZpZWxkXCIpO1xyXG4gICAgbGV0IGRhdGVGaWVsZCA9IHRoaXMuaW5wdXRHcm91cC5nZXQoXCJzdGFydERhdGVcIikudmFsdWU7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+IGRhdGVGaWVsZCBcIixkYXRlRmllbGQpO1xyXG4gICAgLy8gIGlmKGRhdGVGaWVsZD09dW5kZWZpbmVkKXtcclxuICAgIC8vICAgdGhpcy5kYXRlRm9ybWF0Y2hlY2sgPSBmYWxzZTtcclxuICAgIC8vICAgdGhpcy5pbnB1dEdyb3VwLmdldChcIm1lcmlkaWFuXCIpLmludmFsaWQ7XHJcbiAgICAvLyAgfVxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVNlbGVjdGVkVmFsKSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLmlucHV0RGF0YSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0ubGVuZ3RoXHJcbiAgICAgICkge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGV0IGl0ZW0gPSB0aGlzLmZvcm1WYWx1ZXMuc2NoZWR1bGVGaWVsZHM7XHJcbiAgICAgICAgT2JqZWN0LmtleXMoaXRlbSkuZm9yRWFjaChmdW5jdGlvbiAodmFsdWUpIHtcclxuICAgICAgICAgIC8vIHRlbXBPYmpbdmFsdWVdID0gbmV3IEZvcm1Db250cm9sKFtcIlwiXSk7XHJcbiAgICAgICAgICAvLyBsZXQgdmFsPXRoaXMuaW5wdXRHcm91cC52YWx1ZVt2YWx1ZV07XHJcbiAgICAgICAgICAvLyAgaWYoIXZhbClcclxuICAgICAgICAgIC8vICB7XHJcbiAgICAgICAgICAvLyAgIHRoaXMuaW5wdXRHcm91cC5jb250cm9scyhpdGVtKS5zZXRFcnJvcnMoeydpbmNvcnJlY3QnOiB0cnVlfSk7XHJcbiAgICAgICAgICAvLyAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRpb25zLnRvYXN0TWVzc2FnZTtcclxuICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3IpXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjYWxsYmFjayhmYWxzZSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLmlucHV0R3JvdXAgJiYgdGhpcy5pbnB1dEdyb3VwLnZhbGlkKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBjb25zdHJ1Y3RTY2hlZHVsZURhdGEocmVxdWVzdERldGFpbHMsIGNhbGxiYWNrKSB7XHJcbiAgICBsZXQgZGF0ZSA9IG1vbWVudCh0aGlzLmlucHV0RGF0YS5zdGFydERhdGUpLmZvcm1hdChcIkRELU1NLVlZWVlcIik7XHJcbiAgICBsZXQgZGF0ZUNvbmNhdCA9IG1vbWVudChcclxuICAgICAgZGF0ZSArIFwiIFwiICsgdGhpcy5pbnB1dERhdGEudGltZSArIFwiIFwiICsgdGhpcy5pbnB1dERhdGEubWVyaWRpYW4sXHJcbiAgICAgIFwiREQtTU0tWVlZWSBISDptbSBhXCJcclxuICAgICkuZm9ybWF0KCk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImRhdGVUaW1lXCJdID0gZGF0ZUNvbmNhdDtcclxuICAgIHRoaXMuaW5wdXREYXRhW1widGltZVwiXSA9IGRhdGVDb25jYXQ7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInVzZXJSZWNpcGllbnRzXCJdID0gdGhpcy5yZWNpcGllbnQ7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF5c1wiXSA9IFtdO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJkYXRlXCJdID0gbW9tZW50KCkuZm9ybWF0KFwiREQtTU1NLVlZWVlcIik7XHJcbiAgICBsZXQgY3VycmVudElucHV0RGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIikpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSB7IC4uLnRoaXMuaW5wdXREYXRhLCAuLi5jdXJyZW50SW5wdXREYXRhIH07XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLnRoaXMuaW5wdXRHcm91cC52YWx1ZSB9O1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgbGV0IHJlcXVlc3REYXRhID0ge307XHJcbiAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGxldCB0ZW1wRGF0YTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIGl0ZW0uc3ViS2V5ICYmXHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV0gJiZcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgKSB7XHJcbiAgICAgICAgdGVtcERhdGEgPSBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV07XHJcbiAgICAgIH0gZWxzZSBpZiAoIWl0ZW0uc3ViS2V5KSB7XHJcbiAgICAgICAgaWYgKGl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IGl0ZW0udmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLkFzc2lnbkZpcnN0SW5kZXh2YWwpIHtcclxuICAgICAgICAgIHRlbXBEYXRhID0gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV1bMF07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIGVsc2UgaWYoaXRlbS5mcm9tRm9ybUNvbnRyb2wpe1xyXG4gICAgICAgIC8vICAgdGVtcERhdGEgPSBzZWxmLmlucHV0R3JvdXAudmFsdWVcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAodGVtcERhdGEpIHtcclxuICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkodGVtcERhdGEpXHJcbiAgICAgICAgICA6IHRlbXBEYXRhO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIGNhbGxiYWNrKHJlcXVlc3REYXRhKTtcclxuICB9XHJcbiAgc2hvd1JlcGVhdERhdGEodmFsKSB7XHJcbiAgICBpZiAodmFsLmNoZWNrZWQpIHtcclxuICAgICAgdGhpcy5lbmFibGVSZXBlYXREYXRhID0gdHJ1ZTtcclxuICAgICAgdGhpcy5pbnB1dEdyb3VwLmdldChcInJlcGVhdFR5cGVcIikuc2V0VmFsdWUoXCJkYWlseVwiKTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJkYWlseVwiXSA9IHRydWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmVuYWJsZVJlcGVhdERhdGEgPSBmYWxzZTtcclxuICAgICAgdGhpcy5pbnB1dEdyb3VwLmdldChcInJlcGVhdFR5cGVcIikuc2V0VmFsdWUoXCJcIik7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wiZGFpbHlcIl0gPSBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldFNlbGVjdGVkZGF5cygkZXZlbnQsIGVsZW1lbnQpIHtcclxuICAgIHZhciBpbmRleCA9IHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXlzXCJdLmluZGV4T2YoZWxlbWVudCk7XHJcbiAgICBpZiAoaW5kZXggPT09IC0xKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXlzXCJdLnB1c2goZWxlbWVudCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF5c1wiXS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gIH1cclxuICBvblJlcGVhdFNlbGVjdENoYW5nZSh2YWx1ZSwgZGF0YSkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgXy5mb3JFYWNoKGRhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmIChpdGVtLnZhbHVlID09IHZhbHVlKSB7XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbdmFsdWVdID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgb25FeHBvcnRDbGljayhidXR0b25EYXRhLCByZXF1ZXN0KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+Pm9uRXhwb3J0Q2xpY2sgXCIsIHJlcXVlc3QpO1xyXG4gICAgbGV0IGV4cG9ydFJlcXVlc3QsIHJlcXVlc3REZXRhaWxzLCBhcGlVcmw7XHJcbiAgICBsZXQgcmVxdWVzdERhdGEgPSB7fSxcclxuICAgICAgcmVwb3J0VGVtcDtcclxuICAgIGxldCBkeW5hbWljUmVwb3J0ID0gcmVxdWVzdC5pc2R5YW1pY0RhdGEgPyByZXF1ZXN0LmlzZHlhbWljRGF0YSA6IGZhbHNlO1xyXG4gICAgaWYgKGR5bmFtaWNSZXBvcnQpIHtcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGEuZXhwb3J0RGF0YTtcclxuICAgICAgZXhwb3J0UmVxdWVzdCA9IHRoaXMuY3VycmVudERhdGEuZXhwb3J0UmVxdWVzdDtcclxuICAgICAgbGV0IFJlcG9ydERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiQ3VycmVudFJlcG9ydERhdGFcIikpO1xyXG4gICAgICBhcGlVcmwgPSBleHBvcnRSZXF1ZXN0LmFwaVVybDtcclxuICAgICAgcmVwb3J0VGVtcCA9IHsgLi4uUmVwb3J0RGF0YSwgLi4uYnV0dG9uRGF0YSB9O1xyXG4gICAgICByZXF1ZXN0RGV0YWlscyA9IGV4cG9ydFJlcXVlc3QuUmVxdWVzdERhdGE7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBsZXQgZm9ybVJlcXVlc3QgPSByZXF1ZXN0LmV4cG9ydFJlcXVlc3Q7XHJcbiAgICAgIGV4cG9ydFJlcXVlc3QgPSBmb3JtUmVxdWVzdFxyXG4gICAgICAgID8gZm9ybVJlcXVlc3RcclxuICAgICAgICA6IHRoaXMuZm9ybVZhbHVlcy5leHBvcnREYXRhLmV4cG9ydFJlcXVlc3Q7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wiZXhwb3J0VHlwZVwiXSA9IGJ1dHRvbkRhdGEuZXhwb3J0VHlwZTtcclxuICAgICAgcmVxdWVzdERldGFpbHMgPSBleHBvcnRSZXF1ZXN0LnJlcXVlc3REYXRhO1xyXG4gICAgICBhcGlVcmwgPSBleHBvcnRSZXF1ZXN0LmFwaVVybDtcclxuICAgIH1cclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgbGV0IHRlbXBEYXRhO1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgaXRlbS5zdWJLZXkgJiZcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXSAmJlxyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoaXRlbS5jb252ZXJ0U3RyaW5nKSB7XHJcbiAgICAgICAgICBsZXQgdGVtcCA9IFtdO1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XSwgZnVuY3Rpb24gKFxyXG4gICAgICAgICAgICBkYXRhSXRlbVxyXG4gICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHRlbXAucHVzaChkYXRhSXRlbSk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRlbXBEYXRhID0gSlNPTi5zdHJpbmdpZnkodGVtcCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRlbXBEYXRhID0gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIGlmICghaXRlbS5zdWJLZXkpIHtcclxuICAgICAgICBpZiAoaXRlbS5yZXBvcnRDaGVjaykge1xyXG4gICAgICAgICAgdmFyIHQxID0gaXRlbS5mcm9tVHJhbnNsYXRpb25cclxuICAgICAgICAgICAgPyBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoaXRlbS52YWx1ZSlcclxuICAgICAgICAgICAgOiBpdGVtLnZhbHVlO1xyXG4gICAgICAgICAgdGVtcERhdGEgPSBpdGVtLmNvbmRpdGlvbkNoZWNrID8gcmVwb3J0VGVtcFtpdGVtLnZhbHVlXSA6IHQxO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXRlbS5mcm9tTG9naW5EYXRhKSB7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IHNlbGYudXNlckRhdGEgPyBzZWxmLnVzZXJEYXRhW2l0ZW0udmFsdWVdIDogXCJcIjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGVtcERhdGEgPSBpdGVtLmlzRGVmYXVsdCA/IGl0ZW0udmFsdWUgOiBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHNlbGYuZm9ybVZhbHVlcy5nZXRWYWx1ZUZyb21Db25maWcgJiYgc2VsZi5mb3JtVmFsdWVzW2l0ZW0udmFsdWVdKSB7XHJcbiAgICAgICAgdGVtcERhdGEgPSBzZWxmLmZvcm1WYWx1ZXNbaXRlbS52YWx1ZV07XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRlbXBEYXRhKVxyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSlcclxuICAgICAgICAgIDogdGVtcERhdGE7XHJcbiAgICB9KTtcclxuICAgIGlmIChleHBvcnRSZXF1ZXN0LmlzUmVwb3J0Q3JlYXRlKSB7XHJcbiAgICAgIC8vIGZvcm0gYmFzZWQgcmVwb3J0IGNyZWF0ZVxyXG4gICAgICAvLyBFeHBvcnQgcmVwb3J0XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgICBjb25zb2xlLmxvZyhcIj4+Rm9ybSByZXBvcnQgcmVxdWVzdERhdGEgXCIsIHJlcXVlc3REYXRhKTtcclxuICAgICAgdGhpcy52YWxpZGF0aW9uQ2hlY2soZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4gcmVzdWx0IFwiLCByZXN1bHQpO1xyXG4gICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gZXhwb3J0UmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgc2VsZi5jb250ZW50U2VydmljZS5jcmVhdGVSZXF1ZXN0KHJlcXVlc3REYXRhLCBhcGlVcmwpLnN1YnNjcmliZShcclxuICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgaWYgKHJlcy5zdGF0dXMgPT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJDdXJyZW50UmVwb3J0RGF0YVwiKTtcclxuICAgICAgICAgICAgICBzZWxmLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgIHNlbGYubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc3VibWl0dGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICB9KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0RXhwb3J0UmVzcG9uc2UocmVxdWVzdERhdGEsIGFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy5leHBvcnREYXRhICYmXHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy5leHBvcnREYXRhLnMzRG93bmxvYWRcclxuICAgICAgICAgICkge1xyXG4gICAgICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGV4cG9ydFJlcXVlc3QudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGV0IGZpbGVOYW1lID0gdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIGV4cG9ydFJlcXVlc3QuZG93bmxvYWRGaWxlTmFtZVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB0aGlzLmRvd25sb2FkRXhwb3J0RmlsZShcclxuICAgICAgICAgICAgICBkYXRhLFxyXG4gICAgICAgICAgICAgIGZpbGVOYW1lICsgYnV0dG9uRGF0YS5maWxlVHlwZSxcclxuICAgICAgICAgICAgICBidXR0b25EYXRhLnNlbGVjdGlvblR5cGVcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGRvd25sb2FkRXhwb3J0RmlsZShyZXBvcnRWYWx1ZSwgZmlsZU5hbWUsIHNlbGVjdGlvblR5cGUpIHtcclxuICAgIGNvbnN0IGJsb2IgPSBuZXcgQmxvYihbcmVwb3J0VmFsdWVdLCB7IHR5cGU6IHNlbGVjdGlvblR5cGUgfSk7XHJcbiAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIGZpbGVOYW1lKTtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0aW9uQ2hhbmdlZChldmVudDogTWF0QXV0b2NvbXBsZXRlU2VsZWN0ZWRFdmVudCwgaWQpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGlwW2lkXSA9XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwW2lkXSAmJiB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0ubGVuZ3RoXHJcbiAgICAgICAgPyB0aGlzLnNlbGVjdGVkQ2hpcFtpZF1cclxuICAgICAgICA6IFtdO1xyXG4gICAgbGV0IGluZGV4ID0gdGhpcy5zZWxlY3RlZENoaXBbaWRdLmluZGV4T2YoZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICAgIGlmIChpbmRleCA8IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZENoaXBbaWRdLnB1c2goZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICAgIH1cclxuICAgIHRoaXMuYWNjZXNzSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9IFwiXCI7XHJcbiAgfVxyXG5cclxuICBvblNlYXJjaEFjY2Vzc0NvbnRyb2woZXZlbnQ6IE1hdENoaXBJbnB1dEV2ZW50KSB7XHJcbiAgICBjb25zdCB2YWx1ZSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgaWYgKCh2YWx1ZSB8fCBcIlwiKS50cmltKCkpIHtcclxuICAgICAgdGhpcy5jaGlwTGlzdEZpZWxkcy5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgICAgaXRlbS5kYXRhLnB1c2godmFsdWUudHJpbSgpKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmFjY2Vzc0lucHV0Lm5hdGl2ZUVsZW1lbnQudmFsdWUgPSBcIlwiO1xyXG4gIH1cclxuICBFeHBvcnRCdXR0b25zKHZhbHVlKSB7XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICBsZXQgYXBpVXJsO1xyXG4gICAgbGV0IGlucHV0RmllbGRPYmogPSB7fTtcclxuICAgIGxldCBxdWVyeU9iaiA9IHt9O1xyXG4gICAgbGV0IHJlcXVlc3RPYmogPSB7fTtcclxuICAgIGlmIChzZWxmLmZvcm1EYXRhLmJ1dHRvbnMpIHtcclxuICAgICAgXy5mb3JFYWNoKHNlbGYuZm9ybURhdGEuYnV0dG9ucywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICBPYmplY3Qua2V5cyhzZWxmLnNlbGVjdGVkQ2hpcCkuZm9yRWFjaCgoa2V5cykgPT4ge1xyXG4gICAgICAgICAgaW5wdXRGaWVsZE9ialtrZXlzXSA9IEpTT04uc3RyaW5naWZ5KHNlbGYuc2VsZWN0ZWRDaGlwW2tleXNdKTtcclxuICAgICAgICAgIHF1ZXJ5T2JqID0ge1xyXG4gICAgICAgICAgICBkYXRhc291cmNlOiBzZWxmLmRhdGFJZCxcclxuICAgICAgICAgICAgcmVwb3J0X3R5cGU6IHZhbHVlLFxyXG4gICAgICAgICAgICB0eXBlOiBcIlNPRFwiLFxyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICAgIHJlcXVlc3RPYmogPSB7IC4uLmlucHV0RmllbGRPYmosIC4uLnF1ZXJ5T2JqIH07XHJcbiAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0RXhwb3J0UmVzcG9uc2UoYXBpVXJsLCByZXF1ZXN0T2JqKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgKGRhdGEpID0+IHtcclxuICAgICAgICAgIHRoaXMuZG93bmxvYWRGaWxlKGRhdGEuYm9keSwgdmFsdWUpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhlcnJvcik7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gIH1cclxuICBkb3dubG9hZEZpbGUocmVzcG9uc2UsIHZhbHVlKSB7XHJcbiAgICB0aGlzLnJ1blJlcG9ydCA9IHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgdGhpcy5mb3JtRGF0YS5oZWFkZXJcclxuICAgICk7XHJcbiAgICBpZiAodmFsdWUgPT09IFwiQ1NWXCIpIHtcclxuICAgICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtyZXNwb25zZV0sIHsgdHlwZTogXCJ0ZXh0L2NzdlwiIH0pO1xyXG4gICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIHRoaXMucnVuUmVwb3J0ICsgXCIuY3N2XCIpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtyZXNwb25zZV0sIHsgdHlwZTogXCJ0ZXh0L3hsc3hcIiB9KTtcclxuICAgICAgRmlsZVNhdmVyLnNhdmVBcyhibG9iLCB0aGlzLnJ1blJlcG9ydCArIFwiLnhsc3hcIik7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJlY2VpdmVDbGljayhldmVudCkge1xyXG4gICAgdGhpcy5lbmFibGVTZWxlY3RWYWx1ZVZpZXcgPSBmYWxzZTtcclxuICAgIGxldCB0ZW1wRGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXBEYXRhKSA/IEpTT04ucGFyc2UodGVtcERhdGEpIDoge307XHJcbiAgICB0aGlzLnNlbGVjdGVkVGFibGVWaWV3ID0gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlc1xyXG4gICAgICA/IHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMudGFibGVEYXRhXHJcbiAgICAgIDogW107XHJcbiAgICB0aGlzLnNlbGVjdFRhYmxlTG9hZCA9IHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMgfHwge307XHJcbiAgICBpZiAoXHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJlxyXG4gICAgICB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzICYmXHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMuaW5wdXRLZXlcclxuICAgICkge1xyXG4gICAgICB0aGlzLnNlbGVjdFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gdGhpcy5pbnB1dERhdGFbXHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy5pbnB1dEtleVxyXG4gICAgICBdO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RUYWJsZUxvYWRbXCJyZXNwb25zZVwiXSA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLnNlbGVjdGVkVGFibGVWaWV3ICYmIHRoaXMuc2VsZWN0ZWRUYWJsZVZpZXcubGVuZ3RoKSB7XHJcbiAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgIHRoaXMuZW5hYmxlU2VsZWN0VmFsdWVWaWV3ID0gdHJ1ZTtcclxuICAgICAgfSwgMTAwKTtcclxuICAgIH1cclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZEJ1dHRvbkVuYWJsZU1lc3NhZ2UoXCJkYXRhXCIpO1xyXG4gIH1cclxuXHJcbiAgZGVsZXRlQ2xpY2soZXZlbnQpe1xyXG4gICAgdGhpcy5lbmFibGVUYWJsZUxheW91dCA9IGZhbHNlO1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsXCI+Pj4+PkVWRU5UXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcyxcIj4+PiBUSElTXCIpO1xyXG4gICAgc2V0VGltZW91dCgoKT0+e1xyXG4gICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZTtcclxuICAgIH0sIDIwMClcclxuICB9XHJcblxyXG4gIGNoYW5nZVRhYihldmVudCkge1xyXG4gICAgZXZlbnQgPSBldmVudCAmJiBldmVudC5pbmRleCA/IGV2ZW50LmluZGV4IDogMDtcclxuICAgIGxldCBjdXJyZW50VGFiVGFibGUgPSB0aGlzLmZvcm1WYWx1ZXMudGFiRGF0YVtldmVudF0ubG9hZFRhYmxlO1xyXG4gICAgdGhpcy50YWJsZUxpc3QgPSBjdXJyZW50VGFiVGFibGUudGFibGVEYXRhID8gY3VycmVudFRhYlRhYmxlLnRhYmxlRGF0YSA6IFtdO1xyXG4gICAgdGhpcy5jdXJyZW50VGFibGVMb2FkID0gY3VycmVudFRhYlRhYmxlO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gIHRoaXMuY3VycmVudFRhYmxlTG9hZCBcIiwgdGhpcy5jdXJyZW50VGFibGVMb2FkKTtcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMuZnJvbUxvY2FsU3RvcmFnZSAmJiB0aGlzLmZvcm1WYWx1ZXMuYXNzaWduRmlyc3RUYWJsZSkge1xyXG4gICAgICAvLyBhZGRlZCBmb3IgcnVsZXNldCBlcnJvciBsb2dcclxuICAgICAgbGV0IHJlc3BvbnNlS2V5ID0gdGhpcy50YWJsZUxpc3RbMF0ucmVzcG9uc2VLZXk7XHJcbiAgICAgIGxldCB0YWJsZUlkID0gdGhpcy50YWJsZUxpc3RbMF0udGFibGVJZDtcclxuICAgICAgbGV0IHN1YktleSA9IHRoaXMudGFibGVMaXN0WzBdLnN1YlJlc3BvbnNlS2V5O1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbcmVzcG9uc2VLZXldICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbcmVzcG9uc2VLZXldW3RhYmxlSWRdICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbcmVzcG9uc2VLZXldW3RhYmxlSWRdW3N1YktleV1cclxuICAgICAgKSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLmlucHV0RGF0YVtyZXNwb25zZUtleV1bXHJcbiAgICAgICAgICB0YWJsZUlkXHJcbiAgICAgICAgXVtzdWJLZXldXHJcbiAgICAgICAgICA/IHRoaXMuaW5wdXREYXRhW3Jlc3BvbnNlS2V5XVt0YWJsZUlkXVtzdWJLZXldXHJcbiAgICAgICAgICA6IFtdO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLnRhYmxlTGlzdFswXS5lbmFibGVFcnJvckNvdW50ICYmIHRoaXMudGFibGVMaXN0WzBdLmNvdW50S2V5KSB7XHJcbiAgICAgICAgLy8gZXJyb3JTaG93IGtleSBhbmQgdmFsdWVcclxuICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWRbdGhpcy50YWJsZUxpc3RbMF0uY291bnRLZXldID0gdGhpcy5pbnB1dERhdGFbXHJcbiAgICAgICAgICByZXNwb25zZUtleVxyXG4gICAgICAgIF1bdGFibGVJZF07XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICB9XHJcbiAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZTtcclxuICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgPyB0cnVlXHJcbiAgICAgIDogZmFsc2U7XHJcbiAgfVxyXG5cclxuICBuYXZpZ2F0ZU5leHQoKSB7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLkNoaXBMaW1pdCwgXCI6Ojo6XCIpO1xyXG4gICAgbGV0IHRlbXAgPSB0aGlzLmlzTmFtZUF2YWlsYWJsZTtcclxuICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMuZ2V0Rm9ybUNvbnRyb2xWYWx1ZSkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVt0aGlzLmZvcm1WYWx1ZXMudmFsdWVUb1NhdmVdID0gdGhpcy5pbnB1dEdyb3VwLnZhbHVlW1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy5mb3JtRmllbGROYW1lXHJcbiAgICAgIF07XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmlucHV0RGF0YSwgXCIuLi4uLi5JTlBVVCBEQVRBXCIpO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKVxyXG4gICAgdGhpcy52YWxpZGF0aW9uQ2hlY2soZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICBpZiAocmVzdWx0ICYmIHRlbXApIHtcclxuICAgICAgICBzZWxmLnN0ZXBwZXJWYWwubmV4dCgpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgdmFsaWRhdGlvbkNoZWNrKGNhbGxiYWNrKSB7XHJcbiAgICBsZXQgdGVtcERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpKTtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHZhbGFpZGF0aW9uY2hlY2sgdGVtcERhdGEgXCIsIHRlbXBEYXRhKTtcclxuXHJcbiAgICBpZiAodGVtcERhdGEgJiYgdGVtcERhdGEuc2VsZWN0ZWREYXRhICYmIHRlbXBEYXRhLnNlbGVjdGVkRGF0YS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGEgPSB7IC4uLnRoaXMuaW5wdXREYXRhLCAuLi50ZW1wRGF0YSB9O1xyXG4gICAgfVxyXG4gICAgaWYgKFxyXG4gICAgICB0ZW1wRGF0YSAmJlxyXG4gICAgICB0ZW1wRGF0YVt0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGVTZWxlY3RLZXldICYmXHJcbiAgICAgIHRlbXBEYXRhW3RoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVNlbGVjdEtleV0ubGVuZ3RoXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGEgPSB7IC4uLnRoaXMuaW5wdXREYXRhLCAuLi50ZW1wRGF0YSB9O1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcy5mb3JtdXNlZEZvckNoZWNrKSB7XHJcbiAgICB9XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImNoaXBMaW1pdFwiXSA9IHRoaXMuQ2hpcExpbWl0O1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJjaGlwT3BlcmF0b3JcIl0gPSB0aGlzLkNoaXBPcGVyYXRvcjtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcImlucHV0RGF0YWlucHV0RGF0YVwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIlRFU1RUVFwiKTtcclxuICAgIGNvbnNvbGUubG9nKFwidGhpcy5mb3JtVmFsdWVzID4+Pj5cIiwgdGhpcy5mb3JtVmFsdWVzKTtcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGVTZWxlY3RlZFZhbCkge1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGEgJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVt0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGVTZWxlY3RLZXldICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU2VsZWN0S2V5XS5sZW5ndGhcclxuICAgICAgKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICB0aGlzLmlucHV0RGF0YSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0ubGVuZ3RoICYmXHJcbiAgICAgICAgIXRoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVNlbGVjdEtleVxyXG4gICAgICApIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRpb25zLnRvYXN0TWVzc2FnZTtcclxuICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3IpXHJcbiAgICAgICAgKTtcclxuICAgICAgICBjYWxsYmFjayhmYWxzZSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVZhbHVlU3RhdHVzICYmXHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVN1YktleVxyXG4gICAgKSB7XHJcbiAgICAgIC8vIGFkZGVkIFdoZW4gTWFuYWdlIHJvbGUgLSBhY2Nlc3MgY2hlY2tcclxuICAgICAgdmFyIGNoZWNrVmFsID0gXy5maW5kKFxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVN1YktleV0sXHJcbiAgICAgICAgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgIHJldHVybiBpdGVtLnN0YXR1cyA9PSBcIkFDVElWRVwiO1xyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICAgICAgaWYgKGNoZWNrVmFsKSB7XHJcbiAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSB0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbnMudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQodG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvcilcclxuICAgICAgICApO1xyXG4gICAgICAgIHRoaXMuY2hpcFZhbGlkaXRhaW9uID0gdHJ1ZTtcclxuICAgICAgICBjYWxsYmFjayhmYWxzZSk7XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU2VsZWN0ZWRDaGlwKSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLmlucHV0RGF0YSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwS2V5TGlzdFwiXSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwS2V5TGlzdFwiXVtcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9uQ2hpcElkXHJcbiAgICAgICAgXSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwS2V5TGlzdFwiXVt0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbkNoaXBJZF1cclxuICAgICAgICAgIC5sZW5ndGhcclxuICAgICAgKSB7XHJcbiAgICAgICAgaWYgKFxyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGEuY29udHJvbFR5cGUgPT0gXCJTT0RcIiAmJlxyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdW1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbkNoaXBJZFxyXG4gICAgICAgICAgXS5sZW5ndGggPj0gMlxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YS5jb250cm9sVHlwZSA9PSBcIlNlbnNpdGl2ZVwiICYmXHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcEtleUxpc3RcIl1bXHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9uQ2hpcElkXHJcbiAgICAgICAgICBdLmxlbmd0aCA+PSAxXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghdGhpcy5pbnB1dERhdGEuY29udHJvbFR5cGUpIHtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSB0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbnMudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMuY2hpcFZhbGlkaXRhaW9uID0gdHJ1ZTtcclxuICAgICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJ0aGlzLnNlbGVjdGVkQ2hpcHMubGVuZ3RoIFwiLHRoaXMuc2VsZWN0ZWRDaGlwcylcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcInRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzIFwiLHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzKVxyXG4gICAgICAgIGlmIChfLmlzRW1wdHkodGhpcy5zZWxlY3RlZENoaXApKSB7XHJcbiAgICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9ucy50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5jaGlwVmFsaWRpdGFpb24gPSB0cnVlO1xyXG4gICAgICAgICAgY2FsbGJhY2soZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoIV8uaXNFbXB0eSh0aGlzLnNlbGVjdGVkQ2hpcCkpIHtcclxuICAgICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMucmVxdWlyZWRDaGlwcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJpdGVtID4+Pj4+Pj4+XCIsaXRlbSlcclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwW2l0ZW1dICYmXHJcbiAgICAgICAgICAgICAgc2VsZi5zZWxlY3RlZENoaXBbaXRlbV0ubGVuZ3RoIDw9IDBcclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5jaGlwVmFsaWRpdGFpb24gPSB0cnVlO1xyXG4gICAgICAgICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICAgICAgICBzZWxmLnNlbGVjdGVkQ2hpcFtpdGVtXSAmJlxyXG4gICAgICAgICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwW2l0ZW1dLmxlbmd0aCA+IDBcclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5jaGlwVmFsaWRpdGFpb24gPSBmYWxzZTtcclxuICAgICAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRpb25zLnRvYXN0TWVzc2FnZTtcclxuICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBjYWxsYmFjayhmYWxzZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5pbnB1dEdyb3VwICYmIHRoaXMuaW5wdXRHcm91cC52YWxpZCkge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgbmF2aWdhdGVQcmV2aW91cygpIHtcclxuICAgIHRoaXMuc3RlcHBlclZhbC5wcmV2aW91cygpO1xyXG4gIH1cclxuXHJcbiAgc2hvd0RhdGEodGVtcERhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKHRlbXBEYXRhLCBcIkRERERcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmZvcm1WYWx1ZXMsIFwiPj4+Pj4+Zm9ybVZhbHVlc1wiKTtcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGlwcyA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkQ2hpcHM7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2hpcEtleUxpc3QgPSB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZENoaXBLZXlMaXN0O1xyXG4gICAgLy8gZGVidWdnZXI7XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBmdW5jdGlvbiAoeCkge1xyXG4gICAgICAgIGlmICh4ICYmIHguY2hpcENvbmRpdGlvbikge1xyXG4gICAgICAgICAgbGV0IHRlbXAgPSB4LmNoaXBDb25kaXRpb24udmFyaWFibGU7XHJcbiAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIHguY2hpcENvbmRpdGlvbi5kYXRhICYmXHJcbiAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhICYmXHJcbiAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3RlbXBdICYmXHJcbiAgICAgICAgICAgIHguY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVxyXG4gICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHNlbGYuQ2hpcExpbWl0ID1cclxuICAgICAgICAgICAgICB4LmNoaXBDb25kaXRpb24uZGF0YVtzZWxmLmlucHV0RGF0YVt0ZW1wXV1bXCJsaW1pdFwiXTtcclxuICAgICAgICAgICAgc2VsZi5DaGlwT3BlcmF0b3IgPVxyXG4gICAgICAgICAgICAgIHguY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcIm9wZXJhdG9yXCJdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoeC5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgICAgLy8gY2hpcGZpZWxkcyBPbkxvYWQgZnVuY3Rpb24gYWRkZWQgd2hlbiBpbXBsZW1lbnQgcnVuIHJlcG9ydHNcclxuICAgICAgICAgIGxldCBhcGlVcmwgPSB4Lm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSB4Lm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IHJlcXVlc3REYXRhID0geC5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YTtcclxuICAgICAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTogc2VsZi5yZWFsbX07XHJcbiAgICAgICAgICBfLmZvckVhY2gocmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIHguZGF0YSA9IGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy52aWV3RGF0YSA9IHRlbXBEYXRhO1xyXG4gICAgY29uc29sZS5sb2codGhpcy52aWV3RGF0YSwgXCIuLi4uLi5WSUVXIERBVEFcIik7XHJcbiAgICBpZiAoXHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJlxyXG4gICAgICAodGhpcy5mb3JtVmFsdWVzLmNvbmZpcm1EYXRhIHx8IHRoaXMuZm9ybVZhbHVlcy5zaG93U2VsZWN0ZWRWYWx1ZXMpXHJcbiAgICApIHtcclxuICAgICAgdGhpcy5lbmFibGVTZWxlY3RWYWx1ZVZpZXcgPSBmYWxzZTtcclxuICAgICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFRhYmxlVmlldyA9IHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMudGFibGVEYXRhXHJcbiAgICAgICAgICA/IHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMudGFibGVEYXRhXHJcbiAgICAgICAgICA6IFtdO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkID0gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcztcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMgJiZcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMgJiZcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMuaW5wdXRLZXlcclxuICAgICAgICApIHtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLmlucHV0RGF0YVtcclxuICAgICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy5pbnB1dEtleVxyXG4gICAgICAgICAgXTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RUYWJsZUxvYWRbXCJyZXNwb25zZVwiXSA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YTtcclxuICAgICAgICB9XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmVuYWJsZVNlbGVjdFZhbHVlVmlldyA9IHRydWU7XHJcbiAgICAgICAgfSwgMTAwKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcykge1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcywgZnVuY3Rpb24gKFxyXG4gICAgICAgIGF1dG9Db21wbGV0ZUl0ZW1cclxuICAgICAgKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coYXV0b0NvbXBsZXRlSXRlbSwgXCI+Pj4+Pj5hdXRvQ29tcGxldGVJdGVtXCIpO1xyXG4gICAgICAgIGlmIChhdXRvQ29tcGxldGVJdGVtLnZhbHVlRnJvbUxvY2FsKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcclxuICAgICAgICAgICAgYXV0b0NvbXBsZXRlSXRlbS5kYXRhS2V5VG9TYXZlLFxyXG4gICAgICAgICAgICBcIj4+PmF1dG9Db21wbGV0ZUl0ZW0uZGF0YUtleVRvU2F2ZVwiXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgYXV0b0NvbXBsZXRlSXRlbVthdXRvQ29tcGxldGVJdGVtLmRhdGFLZXlUb1NhdmVdID1cclxuICAgICAgICAgICAgdGVtcERhdGFbYXV0b0NvbXBsZXRlSXRlbS5pbnB1dEtleV07XHJcbiAgICAgICAgICAvLyAgICAgICAvLyBhdXRvQ29tcGxldGVJdGVtW2F1dG9Db21wbGV0ZUl0ZW0uZGF0YUtleVRvU2F2ZV0gPSBfLm1hcChzZWxmLmlucHV0RGF0YVthdXRvQ29tcGxldGVJdGVtLmlucHV0S2V5XSwgYXV0b0NvbXBsZXRlSXRlbS5rZXlUb1Nob3cpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIC8vICAgY29uc29sZS5sb2codGhpcy5mb3JtVmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcywgXCI+Pj4+PnRoaXMuZm9ybVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNcIilcclxuICAgIH1cclxuICAgIC8vIGlmKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuc2hvd1NlbGVjdGVkQ2hpcHMpIHtcclxuXHJcbiAgICAvLyB9XHJcbiAgfVxyXG5cclxuICB2YWxpZGF0ZU9iamVjdEZpZWxkcyhpbnB1dERhdGEsIG1hbmRhdG9yeUZpZWxkcywgY2FsbGJhY2spIHtcclxuICAgIGxldCB2YWxpZCA9IHRydWU7XHJcbiAgICBfLmZvckVhY2gobWFuZGF0b3J5RmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBpZiAoIWlucHV0RGF0YVtpdGVtXSkge1xyXG4gICAgICAgIHZhbGlkID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgY2FsbGJhY2sodmFsaWQpO1xyXG4gIH1cclxuICBhZGROZXdWYWx1ZSgpIHtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiLi4uLi4uXCIpO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlQWRkQnV0dG9uKSB7XHJcbiAgICAgIGxldCB0ZW1wRGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkoSlNPTi5wYXJzZSh0ZW1wRGF0YSkpXHJcbiAgICAgICAgPyBKU09OLnBhcnNlKHRlbXBEYXRhKVxyXG4gICAgICAgIDoge307XHJcbiAgICAgIGxldCB0ZW1wQXJyYXkgPSB0aGlzLmlucHV0RGF0YVt0aGlzLmZvcm1WYWx1ZXMuZGF0YVRvU2F2ZV1cclxuICAgICAgICA/IHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy5kYXRhVG9TYXZlXVxyXG4gICAgICAgIDogW107XHJcbiAgICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICAgIGxldCBpbnB1dFZhbHVlcyA9IHRoaXMuaW5wdXRHcm91cC52YWx1ZTtcclxuICAgICAgY29uc29sZS5sb2coXHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLm1hbmRhdG9yeUZpZWxkcyxcclxuICAgICAgICBcIj4+Pj4+dGhpcy5mb3JtVmFsdWVzLm1hbmRhdG9yeUZpZWxkc1wiXHJcbiAgICAgICk7XHJcbiAgICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLm1hbmRhdG9yeUZpZWxkcykge1xyXG4gICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICB0aGlzLnZhbGlkYXRlT2JqZWN0RmllbGRzKFxyXG4gICAgICAgICAgaW5wdXRWYWx1ZXMsXHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMubWFuZGF0b3J5RmllbGRzLFxyXG4gICAgICAgICAgZnVuY3Rpb24gKHZhbGlkKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWxpZCkge1xyXG4gICAgICAgICAgICAgIF8uZm9yRWFjaChzZWxmLmZvcm1WYWx1ZXMub2JqZWN0Rm9ybWF0LCBmdW5jdGlvbiAob2JqSXRlbSkge1xyXG4gICAgICAgICAgICAgICAgdGVtcE9ialtvYmpJdGVtLm5hbWVdID0gaW5wdXRWYWx1ZXNbb2JqSXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgbGV0IGV4aXN0ID1cclxuICAgICAgICAgICAgICAgIHRlbXBBcnJheSAmJiB0ZW1wQXJyYXkubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgID8gXy5maW5kSW5kZXgodGVtcEFycmF5LCB0ZW1wT2JqKVxyXG4gICAgICAgICAgICAgICAgICA6IC0xO1xyXG4gICAgICAgICAgICAgIGlmIChleGlzdCA8IDApIHtcclxuICAgICAgICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtzZWxmLmZvcm1WYWx1ZXMuZGF0YVRvU2F2ZV0gPSB0ZW1wQXJyYXk7XHJcbiAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXHJcbiAgICAgICAgICAgICAgICBcImN1cnJlbnRJbnB1dFwiLFxyXG4gICAgICAgICAgICAgICAgSlNPTi5zdHJpbmdpZnkoc2VsZi5pbnB1dERhdGEpXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICBzZWxmLnJlY2VpdmVDbGljayhudWxsKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFwiRW50ZXIgTWFuZGF0b3J5IEZpZWxkcyFcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICAgIHRoaXMuaW5wdXRHcm91cC5yZXNldCgpO1xyXG4gICAgICB9XHJcbiAgICAgIC8vIGlmKF8uaXNFcXVhbChpbnB1dFZhbHVlS2V5cywgdGhpcy5mb3JtVmFsdWVzLm1hbmRhdG9yeUZpZWxkcykpe1xyXG4gICAgICAvLyAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMub2JqZWN0Rm9ybWF0LCBmdW5jdGlvbihvYmpJdGVtKXtcclxuICAgICAgLy8gICAgIHRlbXBPYmpbb2JqSXRlbS5uYW1lXSA9IGlucHV0VmFsdWVzW29iakl0ZW0udmFsdWVdXHJcbiAgICAgIC8vICAgfSlcclxuICAgICAgLy8gfWVsc2V7XHJcbiAgICAgIC8vICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcIkVudGVyIE1hbmRhdG9yeSBGaWVsZHMhXCIpO1xyXG4gICAgICAvLyB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9uQ2hhbmdlKGRhdGEsIHNlbGVjdGVkRmllbGQpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiIGRhdGEgXCIsIGRhdGEpO1xyXG4gICAgY29uc29sZS5sb2coXCIgc2VsZWN0ZWRGaWVsZCBcIiwgc2VsZWN0ZWRGaWVsZCk7XHJcbiAgICBsZXQgdGVtcERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpKTtcclxuICAgIGxldCBzZWxlY3RJZCA9IHNlbGVjdGVkRmllbGQubmFtZSArIFwiSWRcIjtcclxuICAgIGxldCBleGlzdENoZWNrID0gXy5oYXModGhpcy5pbnB1dERhdGEsIHNlbGVjdElkKTtcclxuICAgIGxldCBpZEtleSA9IGV4aXN0Q2hlY2sgPyBzZWxlY3RJZCA6IHNlbGVjdGVkRmllbGQubmFtZSArIFwiSWRcIjtcclxuICAgIGxldCBuYW1lS2V5ID0gc2VsZWN0ZWRGaWVsZC5uYW1lICsgXCJOYW1lXCI7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtpZEtleV0gPSBkYXRhW3NlbGVjdGVkRmllbGQua2V5VG9TYXZlXTtcclxuICAgIGlmIChzZWxlY3RlZEZpZWxkLmFkZGl0aW9uYWxLZXlUb1NhdmUpIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbc2VsZWN0ZWRGaWVsZC5uYW1lICsgXCJOYW1lXCJdID1cclxuICAgICAgICBkYXRhW3NlbGVjdGVkRmllbGQuYWRkaXRpb25hbEtleVRvU2F2ZV07XHJcbiAgICAgIG5hbWVLZXkgPSBzZWxlY3RlZEZpZWxkLm5hbWUgKyBcIk5hbWVcIjtcclxuICAgIH1cclxuICAgIHRlbXBEYXRhID0gey4uLnRlbXBEYXRhLCAuLi50aGlzLmlucHV0RGF0YX07XHJcbiAgICBpZihzZWxlY3RlZEZpZWxkLnJlZnJlc2hDaGlwRmllbGQgJiYgdGhpcy5zZWxlY3RlZENoaXBzKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHNlbGVjdGVkRmllbGQucmVmcmVzaENoaXBGaWVsZEtleSwgZnVuY3Rpb24ocmVmcmVzaEl0ZW0pe1xyXG4gICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwc1tyZWZyZXNoSXRlbV0gPSBbXTtcclxuICAgICAgICBzZWxmLnNlbGVjdGVkQ2hpcEtleUxpc3RbcmVmcmVzaEl0ZW1dID0gW107XHJcbiAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgdGVtcERhdGFbJ3NlbGVjdGVkQ2hpcHMnXSA9IHRoaXMuc2VsZWN0ZWRDaGlwcztcclxuICAgIHRlbXBEYXRhWydzZWxlY3RlZENoaXBLZXlMaXN0J10gPSB0aGlzLnNlbGVjdGVkQ2hpcEtleUxpc3Q7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9IHRlbXBEYXRhO1xyXG4gICAgaWYgKFxyXG4gICAgICBfLmhhcyh0aGlzLmlucHV0RGF0YSwgc2VsZWN0ZWRGaWVsZC5uYW1lKSAmJlxyXG4gICAgICB0ZW1wRGF0YVtzZWxlY3RlZEZpZWxkLm5hbWVdID09PSBcIlwiXHJcbiAgICApIHtcclxuICAgICAgdGVtcERhdGFbc2VsZWN0ZWRGaWVsZC5uYW1lXSA9IHRoaXMuaW5wdXREYXRhW3NlbGVjdGVkRmllbGQubmFtZV07XHJcbiAgICAgIHRlbXBEYXRhW2lkS2V5XSA9IHRoaXMuaW5wdXREYXRhW2lkS2V5XTtcclxuICAgICAgdGVtcERhdGFbbmFtZUtleV0gPSB0aGlzLmlucHV0RGF0YVtuYW1lS2V5XTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGVtcERhdGEpKTtcclxuICAgIH1lbHNle1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChzZWxlY3RlZEZpZWxkLmdldEF1dG9jb21wbGV0ZURhdGEpIHtcclxuICAgICAgbGV0IHNlbGVjdGVkUmVxdWVzdERhdGEgPSBfLmZpbmQodGhpcy5mb3JtVmFsdWVzLnJlcXVlc3RMaXN0LCB7XHJcbiAgICAgICAgcmVxdWVzdElkOiBkYXRhLnZhbHVlLFxyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRSZXF1ZXN0RGF0YSwgXCI+Pj4+Pj5zZWxlY3RlZFJlcXVlc3REYXRhXCIpO1xyXG4gICAgICB0aGlzLmdldEF1dG9Db21wbGV0ZVZhbHVlcyhzZWxlY3RlZFJlcXVlc3REYXRhLCBkYXRhLCBzZWxlY3RlZEZpZWxkKTtcclxuICAgIH1lbHNlIGlmKHNlbGVjdGVkRmllbGQub25Mb2FkVmFsdWVLZXkpe1xyXG4gICAgICAgIGlmKHNlbGVjdGVkRmllbGQub25DaGlwTG9hZClcclxuICAgICAgICB7IFxyXG4gICAgICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBmdW5jdGlvbihzZWxlY3RGaWVsZEl0ZW0pe1xyXG4gICAgICAgICAgICBpZihzZWxlY3RGaWVsZEl0ZW0ubmFtZSA9PSBzZWxlY3RlZEZpZWxkLm9uTG9hZFZhbHVlS2V5KXtcclxuICAgICAgICAgICAgICBzZWxlY3RGaWVsZEl0ZW0uZGF0YSA9ICBkYXRhW3NlbGVjdGVkRmllbGQub25DaGlwTG9hZF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5zZWxlY3RGaWVsZHMsIGZ1bmN0aW9uKHNlbGVjdEZpZWxkSXRlbSl7XHJcbiAgICAgICAgICAgIGlmKHNlbGVjdEZpZWxkSXRlbS5uYW1lID09IHNlbGVjdGVkRmllbGQub25Mb2FkVmFsdWVLZXkpe1xyXG4gICAgICAgICAgICAgIHNlbGVjdEZpZWxkSXRlbS5kYXRhID0gIGRhdGFbc2VsZWN0ZWRGaWVsZC5vbkxvYWRWYWx1ZUtleV1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5nZXRDaGlwTGlzdCgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0QXV0b0NvbXBsZXRlVmFsdWVzKHNlbGVjdGVkUmVxdWVzdERhdGEsIHNlbGVjdGVkRGF0YSwgc2VsZWN0ZWRGaWVsZCkge1xyXG4gICAgY29uc29sZS5sb2coXCIqKiBcIiwgXCI+Pj5zZWxlY3RlZEZpZWxkXCIsIHNlbGVjdGVkRmllbGQpO1xyXG4gICAgY29uc29sZS5sb2coXCIqKiBzZWxlY3RlZFJlcXVlc3REYXRhIFwiLCBzZWxlY3RlZFJlcXVlc3REYXRhKTtcclxuXHJcbiAgICAvLyBjb25zb2xlLmxvZyhcIiAqKiBzZWxlY3RlZFJlcXVlc3REYXRhLnRleHRGaWVsZE5hbWUgXCIsc2VsZWN0ZWRSZXF1ZXN0RGF0YS50ZXh0RmllbGROYW1lKTtcclxuICAgIGlmIChzZWxlY3RlZFJlcXVlc3REYXRhICYmIHNlbGVjdGVkUmVxdWVzdERhdGEua2V5VG9TYXZlT25JbnB1dCkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtzZWxlY3RlZFJlcXVlc3REYXRhLmtleVRvU2F2ZU9uSW5wdXRdID1cclxuICAgICAgICBzZWxlY3RlZERhdGFbc2VsZWN0ZWRSZXF1ZXN0RGF0YS5rZXlUb1NhdmVPbklucHV0XTtcclxuICAgIH1cclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGxldCBpbmRleDtcclxuICAgIGxldCBjdXJyZW50T2JqO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzKSB7XHJcbiAgICAgIGluZGV4ID0gXy5maW5kSW5kZXgodGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHMsIHtcclxuICAgICAgICBuYW1lOiBzZWxlY3RlZFJlcXVlc3REYXRhLnRleHRGaWVsZE5hbWUsXHJcbiAgICAgIH0pO1xyXG4gICAgICBjdXJyZW50T2JqID0gdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbaW5kZXhdO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaW5kZXggPSBfLmZpbmRJbmRleChcclxuICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLFxyXG4gICAgICAgIHNlbGVjdGVkUmVxdWVzdERhdGFcclxuICAgICAgKTtcclxuICAgICAgY3VycmVudE9iaiA9IHRoaXMuZm9ybVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbaW5kZXhdO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+Pj4+PmluZGV4XCIpO1xyXG4gICAgLy8gZGVidWdnZXI7XHJcbiAgICAvLyBsZXQgYXV0b0NvbXBsZXRlRmllbGQgPSB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcztcclxuICAgIC8vIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcywgZnVuY3Rpb24oXHJcbiAgICAvLyAgIGl0ZW1cclxuICAgIC8vICkge1xyXG4gICAgLy8gbGV0IGl0ZW0gPSBzZWxlY3RlZFJlcXVlc3REYXRhO1xyXG4gICAgaWYgKHNlbGVjdGVkUmVxdWVzdERhdGEgJiYgc2VsZWN0ZWRSZXF1ZXN0RGF0YS5kYXRhKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiIGVudGVyZWQgZGVmYXVsdCBkYXRhIFwiKTtcclxuICAgICAgY3VycmVudE9ialtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldID0gc2VsZWN0ZWRSZXF1ZXN0RGF0YS5kYXRhO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHNlbGVjdGVkUmVxdWVzdERhdGEub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgICBsZXQgYXBpVXJsID0gc2VsZWN0ZWRSZXF1ZXN0RGF0YS5vbkxvYWRGdW5jdGlvbi5hcGlVcmw7XHJcbiAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IHNlbGVjdGVkUmVxdWVzdERhdGEub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtIDogc2VsZi5yZWFsbX07XHJcbiAgICAgICAgXy5mb3JFYWNoKHNlbGVjdGVkUmVxdWVzdERhdGEub25Mb2FkRnVuY3Rpb24ucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChcclxuICAgICAgICAgIHJlcXVlc3RJdGVtXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAoIXJlcXVlc3RJdGVtLmlzU2VhcmNoKSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzRXZlbnRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCIgcXVlcnkgXCIsIHF1ZXJ5LCBcIiBhcGl1cmwgXCIsIGFwaVVybCk7XHJcbiAgICAgICAgLy90aGlzLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgLy8gdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY3VycmVudE9ialtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldID0gZGF0YS5yZXNwb25zZVtcclxuICAgICAgICAgICAgICByZXNwb25zZU5hbWVcclxuICAgICAgICAgICAgXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgICAgbGV0IHZhbGlkYXRlRGF0YSA9IGN1cnJlbnRPYmpbc2VsZWN0ZWRGaWVsZC5vbkxvYWREYXRhS2V5XVswXTtcclxuICAgICAgICAgICAgbGV0IGlzQXJyYXlPZkpTT04gPSBfLmlzUGxhaW5PYmplY3QodmFsaWRhdGVEYXRhKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKCFpc0FycmF5T2ZKU09OKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdGVtcExpc3QgPSBbXTtcclxuICAgICAgICAgICAgICBfLmZvckVhY2goY3VycmVudE9ialtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldLCBmdW5jdGlvbiAoXHJcbiAgICAgICAgICAgICAgICB2YWxcclxuICAgICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goeyBvYmpfbmFtZTogdmFsLCBvYmpfZGVzYzogdmFsIH0pO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIGN1cnJlbnRPYmpbc2VsZWN0ZWRGaWVsZC5vbkxvYWREYXRhS2V5XSA9IHRlbXBMaXN0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudmlld0RhdGFbc2VsZWN0ZWRGaWVsZC5vbkxvYWREYXRhS2V5XSA9XHJcbiAgICAgICAgICAgICAgY3VycmVudE9ialtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldO1xyXG4gICAgICAgICAgICBpZiAoaW5kZXggPT0gc2VsZWN0ZWRGaWVsZC5lbmFibGVDb25kaXRpb25JbmRleCkge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiPj4+Pj5cIik7XHJcbiAgICAgICAgICAgICAgdGhpcy5pbnB1dEdyb3VwLmNvbnRyb2xzW3NlbGVjdGVkUmVxdWVzdERhdGEubmFtZV0uZW5hYmxlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIEVycm9yIFwiLCBlcnIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC8vIGNvbnNvbGUubG9nKGl0L2VtLCBcIi4uLi4uPj4+SVRFTU1NXCIpO1xyXG4gICAgLy8gdGVtcEFycmF5LnB1c2goaXRlbSk7XHJcbiAgICAvLyB9KTtcclxuICAgIGN1cnJlbnRPYmogPSB7IC4uLmN1cnJlbnRPYmosIC4uLnNlbGVjdGVkUmVxdWVzdERhdGEgfTtcclxuICAgIGNvbnNvbGUubG9nKGN1cnJlbnRPYmosIFwiLi4uLi5jdXJyZW50T2JqXCIpO1xyXG4gICAgLy8gdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbaW5kZXhdID0ge1xyXG4gICAgLy8gICAuLi5jdXJyZW50T2JqLFxyXG4gICAgLy8gICAuLi5pdGVtXHJcbiAgICAvLyB9O1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzKSB7XHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzW2luZGV4XSA9IGN1cnJlbnRPYmo7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzW2luZGV4XSA9IGN1cnJlbnRPYmo7XHJcbiAgICB9XHJcbiAgICAvLyBjb25zb2xlLmxvZyhcclxuICAgIC8vICAgdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHMsXHJcbiAgICAvLyAgIFwiPj4+Pj4+Pj4+Pj4+Pj4+Pj5cIlxyXG4gICAgLy8gKTtcclxuICB9XHJcblxyXG4gIG9uQXV0b0NvbXBsZXRlU2VhcmNoKHNlYXJjaFZhbHVlLCBpbnB1dEZpZWxkKSB7XHJcbiAgICBjb25zb2xlLmxvZyhzZWFyY2hWYWx1ZSwgXCI+Pj4+PnNlYXJjaFZhbHVlXCIpO1xyXG4gICAgY29uc29sZS5sb2coaW5wdXRGaWVsZCwgXCI+Pj4+PiBJTlBVVCBGSUVMRFwiKTtcclxuICAgIGlmIChpbnB1dEZpZWxkLnZhbHVlRnJvbUxvY2FsIHx8IGlucHV0RmllbGQuZW5hYmxlRmlsdGVyKSB7XHJcbiAgICAgIGlmIChzZWFyY2hWYWx1ZSkge1xyXG4gICAgICAgIGxldCBzZWFyY2hPYmogPSB7fTtcclxuICAgICAgICBzZWFyY2hPYmpbaW5wdXRGaWVsZC5rZXlUb1Nob3ddID0gc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgaW5wdXRGaWVsZFtpbnB1dEZpZWxkLmRhdGFLZXlUb1NhdmVdID0gXy5maWx0ZXIoXHJcbiAgICAgICAgICBpbnB1dEZpZWxkW2lucHV0RmllbGQuZGF0YUtleVRvU2F2ZV0sXHJcbiAgICAgICAgICBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICBjb25zdCBmaWx0ZXJWYWx1ZSA9IHNlYXJjaFZhbHVlLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICBpdGVtW2lucHV0RmllbGQua2V5VG9TaG93XS50b0xvd2VyQ2FzZSgpLmluZGV4T2YoZmlsdGVyVmFsdWUpID09IDBcclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIGl0ZW07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGlucHV0RmllbGRbaW5wdXRGaWVsZC5kYXRhS2V5VG9TYXZlXSA9XHJcbiAgICAgICAgICB0aGlzLnZpZXdEYXRhICYmIHRoaXMudmlld0RhdGFbaW5wdXRGaWVsZC5kYXRhS2V5VG9TYXZlXVxyXG4gICAgICAgICAgICA/IHRoaXMudmlld0RhdGFbaW5wdXRGaWVsZC5kYXRhS2V5VG9TYXZlXVxyXG4gICAgICAgICAgICA6IHRoaXMudmlld0RhdGFbaW5wdXRGaWVsZC5pbnB1dEtleV07XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uU2VsZWN0QXV0b0NvbXBsZXRlKGV2ZW50LCBpbmRleCwgc2VsZWN0ZWRGaWVsZCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiPj4+Pj5FVkVOVFwiKTtcclxuICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+Pj4+SU5ERVhcIik7XHJcbiAgICBjb25zb2xlLmxvZyhzZWxlY3RlZEZpZWxkLCBcIj4+Pj4+IFNFTEVDVEVEIEZJRUxEXCIpO1xyXG4gICAgbGV0IHNlbGVjdGVkUmVxdWVzdERhdGEgPSBfLmZpbmQodGhpcy5mb3JtVmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcywge1xyXG4gICAgICBmdW5jdGlvbkNhbGxJbmRleDogaW5kZXggKyAxLFxyXG4gICAgfSk7XHJcbiAgICBjb25zb2xlLmxvZyhzZWxlY3RlZFJlcXVlc3REYXRhLCBcIj4+Pj4+PnNlbGVjdGVkUmVxdWVzdERhdGFcIik7XHJcbiAgICB0aGlzLmdldEF1dG9Db21wbGV0ZVZhbHVlcyhzZWxlY3RlZFJlcXVlc3REYXRhLCBldmVudCwgc2VsZWN0ZWRGaWVsZCk7XHJcbiAgfVxyXG4gIG9uU2VhcmNoQ2hhbmdlKHNlYXJjaFZhbHVlLCBpbnB1dEZpZWxkKSB7XHJcbiAgICBjb25zb2xlLmxvZyhpbnB1dEZpZWxkLCBcIi4uaW5wdXRGaWVsZFwiKTtcclxuICAgIGNvbnNvbGUubG9nKHNlYXJjaFZhbHVlLCBcIi4uLi4uLi4uLnNlYXJjaHZhbHVlXCIpO1xyXG4gICAgdmFyIGNoYXJMZW5ndGggPSBzZWFyY2hWYWx1ZS5sZW5ndGg7XHJcbiAgICB2YXIgbGltaXQgPSB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcy5jaGFybGltaXRcclxuICAgICAgPyB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcy5jaGFybGltaXRcclxuICAgICAgOiAzO1xyXG4gICAgY29uc29sZS5sb2coXCJMaW1pdCBcIiwgbGltaXQpO1xyXG4gICAgLy8gaWYgKGNoYXJMZW5ndGggPj0gbGltaXQgfHwgIXNlYXJjaFZhbHVlKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiY2FsbCBBUEkgXCIpO1xyXG4gICAgICB0aGlzLmNoYXJsaW1pdGNoZWNrID0gdHJ1ZTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleChcclxuICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcyxcclxuICAgICAgICB7IG5hbWU6IGlucHV0RmllbGQudGV4dEZpZWxkTmFtZSB9XHJcbiAgICAgICk7XHJcbiAgICAgIGxldCBjdXJyZW50T2JqID0gdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbXHJcbiAgICAgICAgaW5kZXhcclxuICAgICAgXTtcclxuICAgICAgaWYgKGlucHV0RmllbGQub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgICBsZXQgYXBpVXJsID0gaW5wdXRGaWVsZC5vbkxvYWRGdW5jdGlvbi5hcGlVcmw7XHJcbiAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IGlucHV0RmllbGQub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICBfLmZvckVhY2goaW5wdXRGaWVsZC5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKFxyXG4gICAgICAgICAgcmVxdWVzdEl0ZW1cclxuICAgICAgICApIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc1NlYXJjaCkge1xyXG4gICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vdGhpcy5sb2FkZXJTZXJ2aWNlLnN0YXJ0TG9hZGVyKCk7XHJcbiAgICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZShcclxuICAgICAgICAgIChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4gb25zZWFyY2ggQ2hhbmdlIFwiKTtcclxuICAgICAgICAgICAgLy8gdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY3VycmVudE9ialtpbnB1dEZpZWxkLm9uTG9hZERhdGFLZXldID0gZGF0YS5yZXNwb25zZVtcclxuICAgICAgICAgICAgICByZXNwb25zZU5hbWVcclxuICAgICAgICAgICAgXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgICAgbGV0IHZhbGlkYXRlRGF0YSA9IGN1cnJlbnRPYmpbaW5wdXRGaWVsZC5vbkxvYWREYXRhS2V5XVswXTtcclxuICAgICAgICAgICAgbGV0IGlzQXJyYXlPZkpTT04gPSBfLmlzUGxhaW5PYmplY3QodmFsaWRhdGVEYXRhKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKCFpc0FycmF5T2ZKU09OKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdGVtcExpc3QgPSBbXTtcclxuICAgICAgICAgICAgICBfLmZvckVhY2goY3VycmVudE9ialtpbnB1dEZpZWxkLm9uTG9hZERhdGFLZXldLCBmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wTGlzdC5wdXNoKHsgb2JqX25hbWU6IHZhbCwgb2JqX2Rlc2M6IHZhbCB9KTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICBjdXJyZW50T2JqW2lucHV0RmllbGQub25Mb2FkRGF0YUtleV0gPSB0ZW1wTGlzdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLnZpZXdEYXRhW2lucHV0RmllbGQub25Mb2FkRGF0YUtleV0gPVxyXG4gICAgICAgICAgICAgIGN1cnJlbnRPYmpbaW5wdXRGaWVsZC5vbkxvYWREYXRhS2V5XTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coY3VycmVudE9iaiwgXCI+Pj4+PmN1cnJlbnRPYmpcIik7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAvLyB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiBFcnJvciBcIiwgZXJyKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzW2luZGV4XSA9IGN1cnJlbnRPYmo7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyhcIlN0b3AgQVBJIFwiKTtcclxuICAgIC8vICAgdGhpcy5jaGFybGltaXRjaGVjayA9IGZhbHNlO1xyXG4gICAgLy8gfVxyXG4gIH1cclxuICBvbk9wdGlvbkNoYW5nZShldmVudCwgc2VsZWN0ZWRPcHRpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcImlucHV0RGF0YT4+Pj4+Pj4+XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbc2VsZWN0ZWRPcHRpb24ubmFtZV0gPSBldmVudC52YWx1ZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZm9ybVZhbHVlcyk7XHJcbiAgICAvLyBpZiAodGhpcy5lbmFibGVDaGlwRmllbGRzKSB7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKHRoaXMuZW5hYmxlQ2hpcEZpZWxkcywgXCJlbmFibGVDaGlwRmllbGRzXCIpO1xyXG4gICAgLy8gICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAvLyAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkcywgZnVuY3Rpb24oeCkge1xyXG4gICAgLy8gICAgIGNvbnNvbGUubG9nKFwieFwiKTtcclxuICAgIC8vICAgICBpZiAoeC5jaGlwQ29uZGl0aW9uICYmIHguY2hpcENvbmRpdGlvbi52YXJpYWJsZSkge1xyXG4gICAgLy8gICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgLy8gICAgICAgbGV0IHRlbXAgPSB4LmNoaXBDb25kaXRpb24udmFyaWFibGU7XHJcbiAgICAvLyAgICAgICBzZWxmLkNoaXBMaW1pdCA9IHguY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcImxpbWl0XCJdO1xyXG4gICAgLy8gICAgICAgc2VsZi5DaGlwT3BlcmF0b3IgPVxyXG4gICAgLy8gICAgICAgICB4LmNoaXBDb25kaXRpb24uZGF0YVtzZWxmLmlucHV0RGF0YVt0ZW1wXV1bXCJvcGVyYXRvclwiXTtcclxuICAgIC8vICAgICAgIGNvbnNvbGUubG9nKHNlbGYuQ2hpcExpbWl0LCBcIkNoaXBMaW1pdD4+PlwiKTtcclxuICAgIC8vICAgICB9XHJcbiAgICAvLyAgIH0pO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyh0aGlzLkNoaXBMaW1pdCwgXCIxXCIpO1xyXG4gICAgLy8gICBjb25zb2xlLmxvZyh0aGlzLkNoaXBPcGVyYXRvciwgXCIyXCIpO1xyXG4gICAgLy8gICBzZWxmLmlucHV0RGF0YVtcImNoaXBMaW1pdFwiXSA9IHRoaXMuQ2hpcExpbWl0O1xyXG4gICAgLy8gICBzZWxmLmlucHV0RGF0YVtcImNoaXBPcGVyYXRvclwiXSA9IHRoaXMuQ2hpcE9wZXJhdG9yO1xyXG4gICAgLy8gICAvLyBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY2hpcERhdGEnLGV2ZW50LnZhbHVlKVxyXG4gICAgLy8gfVxyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiLi4uLmV2ZW50LnZhbHVlXCIpO1xyXG4gICAgY29uc29sZS5sb2coc2VsZWN0ZWRPcHRpb24sIFwiLi4uLnNlbGVjdGVkT3B0aW9uXCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbc2VsZWN0ZWRPcHRpb24ubmFtZV0gPSBldmVudC52YWx1ZTtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGlmIChzZWxlY3RlZE9wdGlvbiAmJiBzZWxlY3RlZE9wdGlvbi5pc0ltcG9ydFR5cGUpIHtcclxuICAgICAgdGhpcy5zZWxlY3RJbXBvcnRWaWV3KGV2ZW50LnZhbHVlKTtcclxuICAgIH0gZWxzZSBpZihzZWxlY3RlZE9wdGlvbiAmJiBzZWxlY3RlZE9wdGlvbi5uYW1lPT0nY2hlY2hJbXBvcnQnKXtcclxuICAgICAgaWYoZXZlbnQudmFsdWU9PSdOTycpe1xyXG4gICAgICAgIHRoaXMuZW5hYmxlSW1wb3J0RGF0YT1mYWxzZTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5lbmFibGVJbXBvcnREYXRhPXRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgICBlbHNlIHtcclxuICAgICAgXy5mb3JFYWNoKHNlbGVjdGVkT3B0aW9uLmZpZWxkcywgZnVuY3Rpb24gKG9wdGlvbkl0ZW0pIHtcclxuICAgICAgICBpZiAob3B0aW9uSXRlbS5jaGVja2VkVHlwZSkge1xyXG4gICAgICAgICAgc2VsZi5pbnB1dERhdGFbb3B0aW9uSXRlbS5jaGVja2VkVHlwZV0gPVxyXG4gICAgICAgICAgICBvcHRpb25JdGVtLnZhbHVlID09PSBldmVudC52YWx1ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBjb25zb2xlLmxvZyhzZWxmLmlucHV0RGF0YSwgXCJzZWxmXCIpO1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgfVxyXG4gIH1cclxuICB0b2dnbGVDaGFuZ2VkKGV2ZW50KSB7fVxyXG4gIG9uU2VhcmNoTmFtZShldmVudCwgZmllbGREYXRhKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBvbiBTZWFyY2ggTmFtZSAqKioqKioqKioqKioqKioqKioqKioqKioqKiBcIik7XHJcbiAgICBsZXQgY2hhckxlbmd0aCA9IGV2ZW50Lmxlbmd0aDtcclxuICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICBcIiBPbiBzZWFyY2ggTmFtZSBldmVudCBcIixcclxuICAgICAgZXZlbnQsXHJcbiAgICAgIFwiIGZpZWxkZGF0YSBcIixcclxuICAgICAgZmllbGREYXRhLFxyXG4gICAgICBcIiBMZW5ndGg6IFwiLFxyXG4gICAgICBjaGFyTGVuZ3RoXHJcbiAgICApO1xyXG4gICAgaWYgKGZpZWxkRGF0YS5lbmFibGVPblR5cGVTZWFyY2ggJiYgZXZlbnQpIHtcclxuICAgICAgdmFyIGxpbWl0ID0gZmllbGREYXRhLmNoYXJsaW1pdCA/IGZpZWxkRGF0YS5jaGFybGltaXQgOiAzO1xyXG4gICAgICBpZiAobGltaXQgPD0gY2hhckxlbmd0aCkge1xyXG4gICAgICAgIHRoaXMuY2hhcmxpbWl0Y2hlY2sgPSB0cnVlO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwicHJvY2VlZCBBUEkgY2FsbFwiKTtcclxuICAgICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSBmaWVsZERhdGEub25UeXBlU2VhcmNoO1xyXG4gICAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTogdGhpcy5yZWFsbX07XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHNlbGYuY3VycmVudERhdGEsIFwiPj4+PiBDVVJSRU5UXCIpO1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uZnJvbUN1cnJlbnREYXRhKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5jdXJyZW50RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJRdWVyeSBcIiwgcXVlcnkpO1xyXG4gICAgICAgIC8vIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgIC8vICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiZGF0YTo6Ojo6XCIpO1xyXG4gICAgICAgICAgICAgIC8vIGxldCByZXNwb25zZUxlbmd0aCA9XHJcbiAgICAgICAgICAgICAgLy8gICBkYXRhLnJlc3BvbnNlICYmXHJcbiAgICAgICAgICAgICAgLy8gICBkYXRhLnJlc3BvbnNlW3JlcXVlc3REZXRhaWxzLnJlc3BvbnNlTmFtZV0ubGVuZ3RoID4gMFxyXG4gICAgICAgICAgICAgIC8vICAgICA/IGRhdGEucmVzcG9uc2VbcmVxdWVzdERldGFpbHMucmVzcG9uc2VOYW1lXS5sZW5ndGhcclxuICAgICAgICAgICAgICAvLyAgICAgOiAwO1xyXG4gICAgICAgICAgICAgIGlmIChkYXRhLnJlc3BvbnNlICYmIGRhdGEucmVzcG9uc2VbcmVxdWVzdERldGFpbHMucmVzcG9uc2VOYW1lXSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc05hbWVBdmFpbGFibGUgPVxyXG4gICAgICAgICAgICAgICAgICBkYXRhLnJlc3BvbnNlW3JlcXVlc3REZXRhaWxzLnJlc3BvbnNlTmFtZV0ubGVuZ3RoID09IDBcclxuICAgICAgICAgICAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgICAgICAgICAgICA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5pc05hbWVBdmFpbGFibGUsIFwiaXNOYW1lQXZpbGFibGUxXCIpO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoZGF0YS5zdGF0dXMgPT0gNDA5KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzTmFtZUF2YWlsYWJsZSA9IGRhdGEuc3RhdHVzID09IDQwOSA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuaXNOYW1lQXZhaWxhYmxlLCBcImlzTmFtZUF2aWxhYmxlMlwiKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgLy8gdGhpcy5pc05hbWVBdmFpbGFibGUgPVxyXG4gICAgICAgICAgICAgIC8vICAgcmVzcG9uc2VMZW5ndGggPT09IDAgfHwgZGF0YS5zdGF0dXMgIT0gNDA5ID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICAgIC8vICAgZGF0YS5yZXNwb25zZVtyZXF1ZXN0RGV0YWlscy5yZXNwb25zZU5hbWVdLmxlbmd0aCxcclxuICAgICAgICAgICAgICAvLyAgIFwiSUlJSUlJXCIsXHJcbiAgICAgICAgICAgICAgLy8gICB0aGlzLmlzTmFtZUF2YWlsYWJsZVxyXG4gICAgICAgICAgICAgIC8vICk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAvLyB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICAgIGlmIChlcnIuc3RhdHVzID09IDQwOSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5pc05hbWVBdmFpbGFibGUgPSBlcnIuc3RhdHVzID09IDQwOSA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuaXNOYW1lQXZhaWxhYmxlLCBcImlzTmFtZUF2aWxhYmxlMlwiKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJTVE9QIEFQaSBjYWxsXCIpO1xyXG4gICAgICAgIC8vIHRoaXMuaXNOYW1lQXZhaWxhYmxlPWZhbHNlO1xyXG4gICAgICAgIHRoaXMuY2hhcmxpbWl0Y2hlY2sgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBvblN1Ym1pdCgpIHtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsXCI+Pj4+Pj4+Pj4+Pj4mJiYmJlRISVNcIik7XHJcbiAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICB0aGlzLnZhbGlkYXRpb25DaGVjayhmdW5jdGlvbiAocmVzdWx0KSB7XHJcbiAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSAoc2VsZi5vbkxvYWREYXRhICYmIHNlbGYub25Mb2FkRGF0YS5hY3Rpb24gJiZcclxuICAgICAgICAgIHNlbGYuY3VycmVudENvbmZpZ0RhdGFbc2VsZi5vbkxvYWREYXRhLmFjdGlvbl0gJiZcclxuICAgICAgICAgIHNlbGYuY3VycmVudENvbmZpZ0RhdGFbc2VsZi5vbkxvYWREYXRhLmFjdGlvbl0ucmVxdWVzdERldGFpbHMpXHJcbiAgICAgICAgICAgID8gc2VsZi5jdXJyZW50Q29uZmlnRGF0YVtzZWxmLm9uTG9hZERhdGEuYWN0aW9uXS5yZXF1ZXN0RGV0YWlsc1xyXG4gICAgICAgICAgICA6IHt9O1xyXG4gICAgICAgIGlmKCFzZWxmLm9uTG9hZERhdGEgJiYgc2VsZi5mb3JtVmFsdWVzLmFuYWx5c2lzUmVxdWVzdCl7XHJcbiAgICAgICAgICByZXF1ZXN0RGV0YWlscyA9IHNlbGYuZm9ybVZhbHVlcy5hbmFseXNpc1JlcXVlc3Q7XHJcbiAgICAgICAgfWVsc2UgaWYoc2VsZi5mb3JtVmFsdWVzLnJlcXVlc3REZXRhaWxzKXtcclxuICAgICAgICAgIHJlcXVlc3REZXRhaWxzID0gc2VsZi5mb3JtVmFsdWVzLnJlcXVlc3REZXRhaWxzXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBkZXRhaWxzID0gc2VsZi52aWV3RGF0YTtcclxuICAgICAgICAvLyBkZXRhaWxzW1wic3RhdHVzXCJdID0gXCJBQ1RJVkVcIjtcclxuICAgICAgICAvLyBkZXRhaWxzW1wib3BlcmF0b3JzXCJdID0gW1wiQU5EXCJdO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiPj4+IGRldGFpbHMgXCIsIGRldGFpbHMpO1xyXG4gICAgICAgIGxldCBhcGlVcmwgPSByZXF1ZXN0RGV0YWlscy5hcGlVcmw7XHJcbiAgICAgICAgbGV0IHJlcXVlc3REYXRhID0ge1xyXG4gICAgICAgICAgcmVhbG0gOiBzZWxmLnJlYWxtXHJcbiAgICAgICAgfTtcclxuICAgICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICBpZiAoaXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uY29udHJvbFR5cGVDaGVjaykge1xyXG4gICAgICAgICAgICAvLyAgZm9yIHN0b3JpbmcgT3BlcmF0b3JzXHJcbiAgICAgICAgICAgIC8vICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gZXZhbChpdGVtLmNvbmRpdGlvbik7XHJcbiAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPVxyXG4gICAgICAgICAgICAgIGRldGFpbHNbXCJjb250cm9sVHlwZVwiXSAmJiBkZXRhaWxzW1wiY29udHJvbFR5cGVcIl0gPT0gXCJTT0RcIlxyXG4gICAgICAgICAgICAgICAgPyBcIkFORFwiXHJcbiAgICAgICAgICAgICAgICA6IFwiT1JcIjtcclxuICAgICAgICAgIH0gZWxzZSBpZihpdGVtLmZyb21Gb3JtR3JvdXApe1xyXG4gICAgICAgICAgICBsZXQgZm9ybUdyb3VwdmFsdWU9c2VsZi5pbnB1dEdyb3VwLnZhbHVlO1xyXG4gICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID1mb3JtR3JvdXB2YWx1ZVtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH1lbHNlIHtcclxuICAgICAgICAgICAgbGV0IGV4aXN0Q2hlY2sgPSBfLmhhcyhkZXRhaWxzLCBpdGVtLnZhbHVlKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4gZXhpc3RDaGVjayBcIixleGlzdENoZWNrKTtcclxuICAgICAgICAgICAgaWYoZXhpc3RDaGVjayl7XHJcbiAgICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5zdWJLZXlcclxuICAgICAgICAgICAgID8gZGV0YWlsc1tpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgICAgIDogZGV0YWlsc1tpdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPT09IHVuZGVmaW5lZCAmJlxyXG4gICAgICAgICAgICAgIGl0ZW0uYWx0ZXJuYXRpdmVLZXlDaGVja1xyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAvLyBhZGRkZWQgZm9yIHN0b3JpbmcgIGRlZmF1bHQgc2VsZWN0ZWxkIGZpZWxkIG9iamVjdElkIC4gKEVkaXQpXHJcbiAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGRldGFpbHNbaXRlbS5hbHRlcm5hdGl2ZUtleV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHJlcXVlc3REZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgICAgICBpZiAoc2VsZi5vbkxvYWREYXRhICYmIChzZWxmLm9uTG9hZERhdGEuYWN0aW9uID09PSBcImVkaXRcIiB8fCBzZWxmLm9uTG9hZERhdGEuYWN0aW9uID09PSBcInVwZGF0ZVwiKSkge1xyXG4gICAgICAgICAgbGV0IGlkVmFsID0gcmVxdWVzdERldGFpbHMubXVsdGlwbGVJZCA/IHRydWUgOiBzZWxmLmlucHV0RGF0YS5faWQ7XHJcbiAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC51cGRhdGVSZXF1ZXN0KHJlcXVlc3REYXRhLCBhcGlVcmwsIGlkVmFsKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlcy5zdGF0dXMgPT0gMjAxIHx8IHJlcy5zdGF0dXMgPT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICAgICAgICAgICAgICBzZWxmLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICBzZWxmLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2VsZi5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVJlcXVlc3QocmVxdWVzdERhdGEsIGFwaVVybCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzID09IDIwMSkge1xyXG4gICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgICAgICAgICAgICBzZWxmLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICBzZWxmLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIG9uRmlsZUNoYW5nZShldmVudCwgZmlsZVR5cGUsIGltcG9ydERhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IG9uRmlsZUNoYW5nZSBldmVudCBcIiwgZXZlbnQpO1xyXG4gICAgbGV0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICBpZihldmVudC5sZW5ndGgpe1xyXG4gICAgICBsZXQgZmlsZSA9IGV2ZW50WzBdO1xyXG4gICAgICBsZXQgZmlsZU5hbWUgPSBmaWxlLm5hbWU7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj4+IGZpbGUgXCIsZmlsZSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj4+IGZpbGVOYW1lIFwiLGZpbGVOYW1lKTtcclxuICAgICAgdGhpcy5maWxlVHlwZSA9IGZpbGUudHlwZTtcclxuICAgICAgbGV0IGZpbGVFeHQgPSBmaWxlTmFtZS5zcGxpdChcIi5cIikucG9wKCk7XHJcbiAgICAgIGxldCBmaWxlVmFsaWRhdGVGbGFnPWZhbHNlO1xyXG4gICAgICBpZiAoaW1wb3J0RGF0YSAmJiBpbXBvcnREYXRhLm11bHRpRmlsZVN1cHBvcnQpIHtcclxuICAgICAgIGxldCB2YWxpZEZpbGVUeXBlcyA9IGltcG9ydERhdGEuc3VwcG9ydGVkRmlsZXMuc3BsaXQoXCIsXCIpO1xyXG4gICAgICAgICBmaWxlVmFsaWRhdGVGbGFnPSh2YWxpZEZpbGVUeXBlcy5pbmRleE9mKGZpbGVFeHQpID49IDApP3RydWU6ZmFsc2U7IFxyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICBmaWxlVmFsaWRhdGVGbGFnID0gKGZpbGVFeHQgPT0gZmlsZVR5cGUpPyB0cnVlOiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBpZihmaWxlVmFsaWRhdGVGbGFnKXtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIiBQcm9jZWVkIFwiKTtcclxuICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcclxuICAgICAgICByZWFkZXIub25sb2FkID0gKCkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5pbWFnZSA9IGZpbGU7XHJcbiAgICAgICAgICB0aGlzLmlucHV0R3JvdXAucGF0Y2hWYWx1ZSh7XHJcbiAgICAgICAgICAgIGZpbGU6IHJlYWRlci5yZXN1bHQsXHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlTmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZCA9IGZpbGU7XHJcbiAgICAgICAgICB0aGlzLmNoYW5nZURldGVjdG9yLm1hcmtGb3JDaGVjaygpO1xyXG4gICAgICAgIH07XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiIEZpbGUgRXh0ZW5zaW9uIEVycm9yIFwiKTtcclxuICAgICAgICB0aGlzLmltcG9ydERhdGEudXBsb2FkRmlsZSA9IHt9O1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgIFwiRmlsZSBFeHRlbnNpb24gRXJyb3IuIEZpbGUgZXh0ZW5zaW9uIHNob3VsZCBiZSBcIiArXHJcbiAgICAgICAgICAgICAgZmlsZVR5cGUgK1xyXG4gICAgICAgICAgICAgIFwiIGZvcm1hdFwiXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfWVsc2V7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj4+IGZpbGUgcmVxdWlyZWQgXCIpO1xyXG4gICAgfVxyXG4gICBcclxuICB9XHJcbiAgb25GaWxlQ2hhbmdlX2JhY2t1cChldmVudCwgZmlsZVR5cGUsIGltcG9ydERhdGEpIHtcclxuICAgIGxldCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgaWYgKGltcG9ydERhdGEgJiYgaW1wb3J0RGF0YS5tdWx0aUZpbGVTdXBwb3J0KSB7XHJcbiAgICAgIGxldCBmaWxlID0gZXZlbnQudGFyZ2V0LmZpbGVzWzBdO1xyXG4gICAgICB0aGlzLmZpbGVUeXBlID0gZmlsZS50eXBlO1xyXG4gICAgICBsZXQgZmlsZU5hbWUgPSBmaWxlLm5hbWU7XHJcbiAgICAgIGxldCBmaWxlRXh0ID0gZmlsZU5hbWUuc3BsaXQoXCIuXCIpLnBvcCgpO1xyXG4gICAgICBsZXQgdmFsaWRGaWxlVHlwZXMgPSBpbXBvcnREYXRhLnN1cHBvcnRlZEZpbGVzLnNwbGl0KFwiLFwiKTtcclxuICAgICAgY29uc29sZS5sb2coZmlsZUV4dCwgXCJpbXBvcnREYXRhLnN1cHBvcnRlZEZpbGVzID4+Pj4+XCIsIHZhbGlkRmlsZVR5cGVzKTtcclxuICAgICAgaWYgKHZhbGlkRmlsZVR5cGVzLmluZGV4T2YoZmlsZUV4dCkgPj0gMCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiIFByb2NlZWQgXCIpO1xyXG4gICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xyXG4gICAgICAgIHJlYWRlci5vbmxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmltYWdlID0gZmlsZTtcclxuICAgICAgICAgIHRoaXMuaW5wdXRHcm91cC5wYXRjaFZhbHVlKHtcclxuICAgICAgICAgICAgZmlsZTogcmVhZGVyLnJlc3VsdCxcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEZpbGVOYW1lID0gZmlsZS5uYW1lO1xyXG4gICAgICAgICAgdGhpcy5maWxlVXBsb2FkID0gZmlsZTtcclxuICAgICAgICAgIHRoaXMuY2hhbmdlRGV0ZWN0b3IubWFya0ZvckNoZWNrKCk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIiBGaWxlIEV4dGVuc2lvbiBFcnJvciBcIik7XHJcbiAgICAgICAgdGhpcy5pbXBvcnREYXRhLnVwbG9hZEZpbGUgPSB7fTtcclxuICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICBcIkZpbGUgRXh0ZW5zaW9uIEVycm9yLiBGaWxlIGV4dGVuc2lvbiBzaG91bGQgYmUgXCIgK1xyXG4gICAgICAgICAgICAgIGltcG9ydERhdGEuc3VwcG9ydGVkRmlsZXMgK1xyXG4gICAgICAgICAgICAgIFwiIGZvcm1hdFwiXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKGV2ZW50LnRhcmdldC5maWxlcyAmJiBldmVudC50YXJnZXQuZmlsZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgbGV0IGZpbGUgPSBldmVudC50YXJnZXQuZmlsZXNbMF07XHJcbiAgICAgICAgdGhpcy5maWxlVHlwZSA9IGZpbGUudHlwZTtcclxuICAgICAgICBsZXQgZmlsZU5hbWUgPSBmaWxlLm5hbWU7XHJcbiAgICAgICAgY29uc29sZS5sb2coZmlsZU5hbWUsIFwiIEZpbGUgTmFtZVwiKTtcclxuICAgICAgICBsZXQgZmlsZUV4dCA9IGZpbGVOYW1lLnNwbGl0KFwiLlwiKS5wb3AoKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhmaWxlVHlwZSxcIiBGaWxlIFR5cGUgXCIpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGZpbGVFeHQsXCIgZmlsZUV4dCBcIik7XHJcbiAgICAgICAgaWYgKGZpbGVFeHQgPT0gZmlsZVR5cGUpIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiIFByb2NlZWQgXCIpO1xyXG4gICAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XHJcbiAgICAgICAgICByZWFkZXIub25sb2FkID0gKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmltYWdlID0gZmlsZTtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dEdyb3VwLnBhdGNoVmFsdWUoe1xyXG4gICAgICAgICAgICAgIGZpbGU6IHJlYWRlci5yZXN1bHQsXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZU5hbWUgPSBmaWxlLm5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZCA9IGZpbGU7XHJcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlRGV0ZWN0b3IubWFya0ZvckNoZWNrKCk7XHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIiBGaWxlIEV4dGVuc2lvbiBFcnJvciBcIik7XHJcbiAgICAgICAgICB0aGlzLmltcG9ydERhdGEudXBsb2FkRmlsZSA9IHt9O1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIFwiRmlsZSBFeHRlbnNpb24gRXJyb3IuIEZpbGUgZXh0ZW5zaW9uIHNob3VsZCBiZSBcIiArXHJcbiAgICAgICAgICAgICAgICBmaWxlVHlwZSArXHJcbiAgICAgICAgICAgICAgICBcIiBmb3JtYXRcIlxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBjbG9zZU1vZGVsKCkge1xyXG4gICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gIH1cclxuICBvbkltcG9ydCgpIHtcclxuICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgIGxldCByZXF1ZXN0RGF0YSA9IHt9O1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgdGhpcy5pbnB1dERhdGEuY3JvbkV4cHJlc3Npb24gPSB0aGlzLmNyb25FeHByZXNzaW9uID8gIHRoaXMuY3JvbkV4cHJlc3Npb24gOiBcInJ1bk9uY2VcIjtcclxuICAgIGxldCBpbXBvcnREZXRhaWxzID0gdGhpcy5pbXBvcnREYXRhLm9uTG9hZEZ1bmN0aW9uO1xyXG4gICAgXy5mb3JFYWNoKGltcG9ydERldGFpbHMucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmIChpdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnZhbHVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnN1YktleVxyXG4gICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgaWYgKHRoaXMuaW1wb3J0RGF0YS51cGxvYWRGaWxlKSB7XHJcbiAgICAgIHZhciBmb3JtRGF0YSA9IG5ldyBGb3JtRGF0YSgpO1xyXG4gICAgICBPYmplY3Qua2V5cyhyZXF1ZXN0RGF0YSkubWFwKChrZXkpID0+IHtcclxuICAgICAgICBmb3JtRGF0YS5hcHBlbmQoa2V5LCByZXF1ZXN0RGF0YVtrZXldKTtcclxuICAgICAgfSk7XHJcbiAgICAgIGZvcm1EYXRhLmFwcGVuZChcImZpbGVcIiwgdGhpcy5maWxlVXBsb2FkKTtcclxuICAgICAgIHJlcXVlc3REYXRhID0gZm9ybURhdGE7IFxyXG4gICAgfVxyXG4gICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBpbXBvcnREZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsXCI+Pj4+Pj4gVEhJU1wiKVxyXG4gICAgcmVxdWVzdERhdGFbXCJyZWFsbVwiXT10aGlzLnJlYWxtO1xyXG4gICAgaWYgKHRoaXMuaW5wdXRHcm91cCAmJiB0aGlzLmlucHV0R3JvdXAudmFsaWQgKSB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAvL3RoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiVGhpcy5pbnB1dCAuLi4uLiAgXCIsIHRoaXMuaW5wdXREYXRhKTtcclxuICAgICAgLy8gZGVidWdnZXI7XHJcbiAgICAgIGlmICh0aGlzLmltcG9ydERhdGEuZnVuY3Rpb24gJiYgdGhpcy5pbXBvcnREYXRhLmZ1bmN0aW9uID09IFwiZWRpdFwiKSB7IC8vIHJ1bGVzZXQsIGRhdGFzb3VyY2UgLSByZWltcG9ydFxyXG4gICAgICAgIGlmKHRoaXMuZmlsZVVwbG9hZCAhPT0gdW5kZWZpbmVkIClcclxuICAgICAgICB7IFxyXG4gICAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgIC51cGRhdGVSZXF1ZXN0KFxyXG4gICAgICAgICAgICByZXF1ZXN0RGF0YSxcclxuICAgICAgICAgICAgaW1wb3J0RGV0YWlscy5hcGlVcmwsXHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wiX2lkXCJdXHJcbiAgICAgICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wiZGF0YXNvdXJjZUlkXCJdXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzLCBcIi4uLi4ucmVzXCIpO1xyXG4gICAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDEgfHwgcmVzLnN0YXR1cyA9PSAyMDApIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmltcG9ydERhdGEuczN1cGxvYWQpIHtcclxuICAgICAgICAgICAgICAgICAgbGV0IHJlc3BvbnNlID0gcmVzLmJvZHkucmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5qb2IpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VVcGxvYWQocmVzcG9uc2Uuam9iLmRhdGFzb3VyY2UsIHRoaXMuaW1wb3J0RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UuZGF0YXNvdXJjZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI8PDw8REFUQSBJTVBPUlQ+Pj5cIiwgcmVzcG9uc2UuZGF0YXNvdXJjZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVVwbG9hZChyZXNwb25zZS5kYXRhc291cmNlLCB0aGlzLmltcG9ydERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICB9ZWxzZSBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2UucnVsZXNldCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCI8PDw8UnVsZXNldCBJTVBPUlQ+Pj5cIiwgcmVzcG9uc2UucnVsZXNldCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVVwbG9hZChyZXNwb25zZS5ydWxlc2V0LCB0aGlzLmltcG9ydERhdGEpO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAvLyB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgIHRoaXMuZXJyb3JUYWJsZUxpc3QgPSB0aGlzLmltcG9ydERhdGEudGFibGVEYXRhO1xyXG4gICAgICAgICAgICAgIHRoaXMuZXJyb3JMb2dUYWJsZSA9IHRoaXMuaW1wb3J0RGF0YTtcclxuICAgICAgICAgICAgICB0aGlzLmVycm9yTG9nVGFibGVbXCJyZXNwb25zZVwiXSA9IGVycm9yLmVycm9yLnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgIHRoaXMuZXJyb3JMb2dUYWJsZVtcInJlc3BvbnNlS2V5XCJdID0gXCJsb2dzQXJyXCI7XHJcbiAgICAgICAgICAgICAgdGhpcy5lcnJvclNob3cgPSBlcnJvci5lcnJvci5yZXNwb25zZTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmVycm9yTG9nVGFibGUsIFwiZXJyb3JMb2c+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICApO1xyXG4gICAgICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICB0aGlzLmltcG9ydERhdGEudmFsaWRhdGlvbnMubWVzc2FnZVxyXG4gICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5wdXREYXRhICYmIHRoaXMuaW5wdXREYXRhLmltcG9ydFR5cGUgPT0gXCJhdm1cIikge1xyXG4gICAgICAgICAgdGhpcy5jb250ZW50U2VydmljZS5hdm1JbXBvcnQocmVxdWVzdERhdGEsIHRoaXMuZmlsZVVwbG9hZCwgaW1wb3J0RGV0YWlscy5hdm1BcGkpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIGlmKHRoaXMuaW5wdXREYXRhICYmIHRoaXMuaW5wdXREYXRhLmltcG9ydFR5cGUgPT0gXCJzb2RBZ2VudFwiKXtcclxuICAgICAgICAgIGlmKGltcG9ydERldGFpbHMuc29kQWdlbnRSZXF1ZXN0KSB7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChpbXBvcnREZXRhaWxzLnNvZEFnZW50UmVxdWVzdCwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgICAgICBpZiAoaXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZihpdGVtLmZyb21Vc2VyRGF0YSl7XHJcbiAgICAgICAgICAgICAgICBpZihpdGVtLnN1YktleSAmJiBpdGVtLmFzc2lnbmVGaXJzdFZhbHVlKXtcclxuICAgICAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IChzZWxmLnVzZXJEYXRhICYmIHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV0gJiYgc2VsZi51c2VyRGF0YVtpdGVtLnZhbHVlXVswXSkgPyBzZWxmLnVzZXJEYXRhW2l0ZW0udmFsdWVdWzBdW2l0ZW0uc3ViS2V5XSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZSBpZihpdGVtLnN1YktleSl7XHJcbiAgICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSAoc2VsZi51c2VyRGF0YSAmJiBzZWxmLnVzZXJEYXRhW2l0ZW0udmFsdWVdKSA/IHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV0gOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSAoc2VsZi51c2VyRGF0YSAmJiBzZWxmLnVzZXJEYXRhW2l0ZW0udmFsdWVdKSA/IHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV0gOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1lbHNlIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnN1YktleVxyXG4gICAgICAgICAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgICAgICA6IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLFwiPj4+Pj4+IFRISVNcIik7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4gcmVxdWVzdERhdGEgXCIscmVxdWVzdERhdGEpO1xyXG4gICAgICAgICAgbGV0IHByb2R1dE5hbWU9cmVxdWVzdERhdGEgJiYgcmVxdWVzdERhdGFbXCJwcm9kdWN0TmFtZVwiXT9yZXF1ZXN0RGF0YVtcInByb2R1Y3ROYW1lXCJdOlwiRWJzXCI7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+PiBwcm9kdXROYW1lIFwiLHByb2R1dE5hbWUpO1xyXG4gICAgICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgICAuY3JlYXRlUmVxdWVzdChyZXF1ZXN0RGF0YSwgaW1wb3J0RGV0YWlscy5zb2RBZ2VudENyZWF0ZSArIHRoaXMuaW5wdXREYXRhW2ltcG9ydERldGFpbHMuc29kQWdlbnRQYXJhbWV0ZXJLZXldKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcywgXCIuLi4uLnJlc1wiKTtcclxuICAgICAgICAgICAgICAgIGlmKHJlcyAmJiByZXMuYm9keSAmJiByZXMuYm9keS5kYXRhKXtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgICAgICAgICAgICAgLmdldFMzUmVzcG9uc2UocmVxdWVzdERhdGEsIGltcG9ydERldGFpbHMuc29kQWdlbnREb3dubG9hZEFwaSBcclxuICAgICAgICAgICAgICAgICAgICAgICAgKyB0aGlzLmlucHV0RGF0YVtpbXBvcnREZXRhaWxzLnNvZEFnZW50UGFyYW1ldGVyS2V5XVxyXG4gICAgICAgICAgICAgICAgICAgICAgICArICcvJyArIHRoaXMudXNlckRhdGEuY3VzdG9tZXIgKycvJytwcm9kdXROYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSxcIj4+Pj5kYXRhXCIpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBsb2NhbGZpbGVuYW1lID0gXCJTb2RfQWdlbnRfXCIrcHJvZHV0TmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAvL3ZhciBsb2NhbGZpbGVuYW1lID0gXCJTb2RfQWdlbnRfRWJzXCI7XHJcblx0XHRcdCAgICAgICAgICAgICAgICAgICAgbG9jYWxmaWxlbmFtZSA9IGxvY2FsZmlsZW5hbWUucmVwbGFjZSgvXFxzL2dpLCBcIl9cIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kb3dubG9hZEV4cG9ydEZpbGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhLmJvZHksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsb2NhbGZpbGVuYW1lICsgJy56aXAnLCAnYXBwbGljYXRpb24vemlwJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcIlVuYWJsZSBUbyBEb3dubG9hZCBhZ2VudCFcIik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICBcclxuICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICBsZXQgYXBpdXJsPWltcG9ydERldGFpbHMuYXBpVXJsO1xyXG4gICAgICAgICAgaWYodGhpcy5pbnB1dERhdGEuaW1wb3J0VHlwZT09J3dlYnNlcnZpY2UnKXtcclxuICAgICAgICAgICAvLyBhcGl1cmw9aW1wb3J0RGV0YWlscy53ZWJzZXJ2aWNlQXBpO1xyXG4gICAgICAgICAgICB0aGlzLmltcG9ydERhdGEuczN1cGxvYWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgcmVxdWVzdERhdGFbXCJ3ZWJzZXJ2aWNlXCJdID0gdHJ1ZTtcclxuICAgICAgICAgIH0gIFxyXG4gICAgICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgICAuY3JlYXRlUmVxdWVzdChyZXF1ZXN0RGF0YSwgYXBpdXJsKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcywgXCIuLi4uLnJlc1wiKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlcy5zdGF0dXMgPT0gMjAxIHx8IHJlcy5zdGF0dXMgPT0gMjAwKSB7XHJcbiAgICAgICAgICAgICAgICAgIC8vIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmltcG9ydERhdGEuczN1cGxvYWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSAocmVzLmJvZHkgJiYgcmVzLmJvZHkucmVzcG9uc2UgJiZyZXMuYm9keS5yZXNwb25zZS5kYXRhc291cmNlKSA/IHJlcy5ib2R5LnJlc3BvbnNlLmRhdGFzb3VyY2UgOiByZXMuYm9keS5yZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzcG9uc2UgJiYgcmVzcG9uc2Uuam9iKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVVwbG9hZChcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2Uuam9iLmRhdGFzb3VyY2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW1wb3J0RGF0YVxyXG4gICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW1hZ2VVcGxvYWQocmVzcG9uc2UsIHRoaXMuaW1wb3J0RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVzLnN0YXR1cyA9PSAyMDYpIHtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvclRhYmxlTGlzdCA9IHRoaXMuaW1wb3J0RGF0YS50YWJsZURhdGE7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JMb2dUYWJsZSA9IHRoaXMuaW1wb3J0RGF0YTtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlW1wicmVzcG9uc2VcIl0gPSByZXMuYm9keS5yZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlW1wicmVzcG9uc2VLZXlcIl0gPSBcImxvZ3NBcnJcIjtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvclNob3cgPSByZXMuYm9keS5yZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlLFxyXG4gICAgICAgICAgICAgICAgICAgIFwiZXJyb3JMb2cgb24gZGlmZmVyZW50IHN0YXR1cyBjb2RlID4+Pj4+Pj5cIlxyXG4gICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JUYWJsZUxpc3QgPSB0aGlzLmltcG9ydERhdGEudGFibGVEYXRhO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlID0gdGhpcy5pbXBvcnREYXRhO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlW1wicmVzcG9uc2VcIl0gPSBlcnJvci5lcnJvci5yZXNwb25zZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JMb2dUYWJsZVtcInJlc3BvbnNlS2V5XCJdID0gXCJsb2dzQXJyXCI7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yU2hvdyA9IGVycm9yLmVycm9yLnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5lcnJvckxvZ1RhYmxlLCBcImVycm9yTG9nPj4+Pj4+PlwiKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIHNlbGVjdEltcG9ydFZpZXcoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcIi4uLi4uZXZlbnRcIik7XHJcbiAgICB0aGlzLmVuYWJsZVRleHRGaWVsZHMgPSBmYWxzZTtcclxuICAgIHRoaXMuZW5hYmxlT3B0aW9uRmllbGRzID0gZmFsc2U7XHJcbiAgICBsZXQgdG9nZ2xlRmllbGRzID0gdGhpcy5mb3JtVmFsdWVzLmVuYWJsZVRvZ2dsZUZpZWxkcztcclxuICAgIGlmIChldmVudCAhPSBcImNzdlwiICYmIGV2ZW50ICE9IFwiYXZtXCIpIHtcclxuICAgICAgdGhpcy5lbmFibGVJbXBvcnREYXRhID0gZmFsc2U7XHJcbiAgICAgIGlmICh0b2dnbGVGaWVsZHMgJiYgdG9nZ2xlRmllbGRzLm1lcmdlVGV4dEZpZWxkcykge1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy50ZXh0RmllbGRzID0gXy5jb25jYXQoXHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMudGV4dEZpZWxkcyxcclxuICAgICAgICAgIHRvZ2dsZUZpZWxkcy50ZXh0RmllbGRzXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgICBpZih0b2dnbGVGaWVsZHMgJiYgdG9nZ2xlRmllbGRzLmFkZGl0aW9uYWxPcHRpb24pe1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy5hZGRpdGlvbmFsT3B0aW9uRmllbGRzID0gdG9nZ2xlRmllbGRzLmFkZGl0aW9uYWxPcHRpb25GaWVsZHM7XHJcbiAgICAgIH1cclxuICAgICAgaWYodG9nZ2xlRmllbGRzICYmIHRvZ2dsZUZpZWxkcy5lbmFibGVTY2hlZHVsZSl7XHJcbiAgICAgICAgdGhpcy5lbmFibGVTY2hlZHVsZSA9IHRydWU7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIHRoaXMuZW5hYmxlU2NoZWR1bGUgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICBpZih0b2dnbGVGaWVsZHMgJiYgdG9nZ2xlRmllbGRzLmVuYWJsZUFnZW50Tm90ZSkge1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy5lbmFibGVBZ2VudE5vdGUgPSB0b2dnbGVGaWVsZHMuZW5hYmxlQWdlbnROb3RlO1xyXG4gICAgICB9XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgaWYgKHRvZ2dsZUZpZWxkcy5pc1NldERlZmF1bHRWYWx1ZSkge1xyXG4gICAgICAgIF8uZm9yRWFjaCh0b2dnbGVGaWVsZHMuYWRkaXRpb25hbE9wdGlvbkZpZWxkcywgZnVuY3Rpb24ob3B0aW9uSXRlbSl7XHJcbiAgICAgICAgICBzZWxmLmlucHV0RGF0YVtcclxuICAgICAgICAgICAgb3B0aW9uSXRlbS5kZWZhdWx0S2V5XHJcbiAgICAgICAgICBdID0gb3B0aW9uSXRlbS5kZWZhdWx0VmFsdWU7XHJcbiAgICAgICAgfSlcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmVuYWJsZVRleHRGaWVsZHMgPSB0cnVlO1xyXG4gICAgICB0aGlzLmVuYWJsZU9wdGlvbkZpZWxkcyA9IHRydWU7XHJcblxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLmVuYWJsZUFnZW50Tm90ZSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmVuYWJsZVNjaGVkdWxlID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy50ZXh0RmllbGRzID0gdGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMuZmlsdGVyKGZ1bmN0aW9uIChcclxuICAgICAgICBpdGVtXHJcbiAgICAgICkge1xyXG4gICAgICAgIHJldHVybiAhdG9nZ2xlRmllbGRzLnRleHRGaWVsZHMuaW5jbHVkZXMoaXRlbSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICBpZih0b2dnbGVGaWVsZHMuYWRkaXRpb25hbE9wdGlvbkZpZWxkcykge1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy5hZGRpdGlvbmFsT3B0aW9uRmllbGRzID0gW11cclxuICAgICAgfVxyXG5cclxuICAgICAgdGhpcy5lbmFibGVJbXBvcnREYXRhID0gdHJ1ZTtcclxuICAgICAgLy8gdGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMgPVxyXG4gICAgICB0aGlzLmVuYWJsZVRleHRGaWVsZHMgPSB0cnVlO1xyXG4gICAgICB0aGlzLmVuYWJsZU9wdGlvbkZpZWxkcyA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlVGV4dEZpZWxkcykge1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpZih0b2dnbGVGaWVsZHMuYWRkaXRpb25hbE9wdGlvbikge1xyXG4gICAgICB0ZW1wT2JqW1wiZXZlcnlNaW51dGVcIl0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIHRlbXBPYmpbXCJtYWluVHlwZVwiXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgdGVtcE9ialtcImhvdXJMaXN0dmFsdWVcIl0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIHRlbXBPYmpbXCJldmVyeURheXNcIl0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIHRlbXBPYmpbXCJkYXlTdGFydGluZ09uXCJdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICB0ZW1wT2JqW1wibW9udGhTdGFydGluZ09uXCJdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuaW5wdXRHcm91cCA9IG5ldyBGb3JtR3JvdXAodGVtcE9iaik7XHJcbiAgfVxyXG4gIGltYWdlVXBsb2FkKGRhdGEsIGltcG9ydERhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiLi4uLi5kYXRhXCIpO1xyXG4gICAgdGhpcy5Bd3NTM1VwbG9hZFNlcnZpY2UuZmluZCh7XHJcbiAgICAgIG1pbWVUeXBlOiB0aGlzLmZpbGVUeXBlLFxyXG4gICAgICB0eXBlOiBpbXBvcnREYXRhLnMzUmVxdWVzdFR5cGUgPyBpbXBvcnREYXRhLnMzUmVxdWVzdFR5cGUgOiBcImRhdGFzb3VyY2VcIixcclxuICAgIH0pLnN1YnNjcmliZSgoczNDcmVkZW50aWFsczogYW55KSA9PiB7XHJcbiAgICAgIGlmIChzM0NyZWRlbnRpYWxzKSB7XHJcbiAgICAgICAgdGhpcy5Bd3NTM1VwbG9hZFNlcnZpY2UudXBsb2FkUzMoXHJcbiAgICAgICAgICB0aGlzLmltYWdlLFxyXG4gICAgICAgICAgczNDcmVkZW50aWFscy5yZXNwb25zZSxcclxuICAgICAgICAgIGltcG9ydERhdGFcclxuICAgICAgICApXHJcbiAgICAgICAgICAudGhlbigocmVzdWx0KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3VsdCwgXCIuLi5yZXN1bHRcIik7XHJcbiAgICAgICAgICAgIGxldCB1cmwgPSBkZWNvZGVVUklDb21wb25lbnQocmVzdWx0LlBvc3RSZXNwb25zZS5Mb2NhdGlvbik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHVybCwgXCIuLi51cmxcIik7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wiX2lkXCJdID0gZGF0YS5faWQ7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wiZGF0YXNvdXJjZVBhdGhcIl0gPSB1cmw7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic3RhdHVzXCJdID0gXCJVcGxvYWRlZFwiO1xyXG4gICAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICAgdGhpcy5pbXBvcnREYXRhLmZ1bmN0aW9uICYmXHJcbiAgICAgICAgICAgICAgdGhpcy5pbXBvcnREYXRhLmZ1bmN0aW9uID09IFwiZWRpdFwiXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wicmVpbXBvcnRcIl0gPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wicmVpbXBvcnRcIl0gPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSB7fTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codGhpcywgXCIuLi4uVEhTSUlJU1NTXCIpO1xyXG4gICAgICAgICAgICBsZXQgdXBkYXRlRGV0YWlscyA9IHRoaXMuaW1wb3J0RGF0YS51cGRhdGVSZXF1ZXN0O1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh1cGRhdGVEZXRhaWxzLCBcIi4uLi4udXBkYXRlRGV0YWlsc1wiKTtcclxuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgICBfLmZvckVhY2godXBkYXRlRGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXF1ZXN0RGF0YSwgXCIuLi4uUkVRVUVTVCBEQVRBXCIpO1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgLnVwZGF0ZVJlcXVlc3QoXHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0RGF0YSxcclxuICAgICAgICAgICAgICAgIHVwZGF0ZURldGFpbHMuYXBpVXJsLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5pbnB1dERhdGEuX2lkXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgIC5zdWJzY3JpYmUoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmZvcm1EYXRhWydlcnBfaW5zdGFuY2UnXSA9IHRoaXMuY3N2Rm9ybS52YWx1ZS5lcnBfaW5zdGFuY2U7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZm9ybURhdGFbJ25hbWUnXSA9IHRoaXMuY3N2Rm9ybS52YWx1ZS5uYW1lO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmZvcm1EYXRhWydzdGF0dXMnXSA9IFwiVXBsb2FkZWRcIjtcclxuICAgICAgICAgICAgLy8gdGhpcy5mb3JtRGF0YVsnZHNfcGF0aCddID0gdXJsO1xyXG4gICAgICAgICAgICAvLyB0aGlzLlNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlLnB1dEltcG9ydENTVih0aGlzLmZvcm1EYXRhLCB0aGlzLmlkKS5zdWJzY3JpYmUoKGRhdGFOZXc6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAvLyBcdHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKGRhdGFOZXcuc3RhdHVzKTtcclxuICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLmNhdGNoKChlcnJvcikgPT4ge30pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgXHJcbiAgLy9mb3IgY3JvbiBzY2hlZHVsZSBleHByZXNzaW9uIGJ5IFJhalxyXG4gIHNlY29uZHNMaXN0IDogYW55ID0gW107XHJcbiAgc2Vjb25kc0xhc3REaWdpdHM6IGFueSA9IFtdO1xyXG4gIGhvdXJMaXN0VmlldyA6IGFueSA9IFtdO1xyXG4gIGhvdXJzdHdvdGhyZWUgOiBhbnkgPSBbXTtcclxuICBkYXlzIDogYW55ID0gW107XHJcbiAgbW9udGggOiBhbnkgPSBbXTtcclxuICBtb250aHNDb3VudCA6IGFueSA9IFtdO1xyXG4gIGRhdGVDb3VudCA6IGFueSA9IFtdO1xyXG4gIG51bU9mZGF5cyA6IGFueSA9IFtdO1xyXG4gIG51bU9mbW9udGhzIDogYW55ID0gW107XHJcbiAgc2Vjb25kc1ZpZXdTaG93IDogYm9vbGVhbjtcclxuICBob3VyVmlld1Nob3cgOiBib29sZWFuO1xyXG4gIE1pbnV0ZXNWaWV3U2hvdyA6IGJvb2xlYW47XHJcbiAgZGF5dmlld1Nob3cgOiBib29sZWFuO1xyXG4gIG1vbnRodmlld1Nob3cgOiBib29sZWFuO1xyXG4gIGdldG1pbiA6IGJvb2xlYW47XHJcbiAgY3JvcEV4cHJlc3Npb25TZWxlY3Rpb24gOiBhbnkgPSB7fTtcclxuICB2aWV3T2ZzZWNvbmRzKCkge1xyXG4gICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gNjA7IGkrKykge1xyXG4gICAgICAvLyBjb25zb2xlLmxvZyhpKTtcclxuICAgICAgdGhpcy5zZWNvbmRzTGlzdC5wdXNoKGkpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5zZWNvbmRzTGFzdERpZ2l0cyA9IFtdO1xyXG4gICAgdGhpcy5ob3VyTGlzdFZpZXcgPSBbXTtcclxuICAgIHRoaXMuaG91cnN0d290aHJlZSA9IFtdO1xyXG4gICAgZm9yIChsZXQgaiA9IDA7IGogPD0gNTk7IGorKykge1xyXG4gICAgICAvLyBjb25zb2xlLmxvZyhpKTtcclxuICAgICAgdGhpcy5zZWNvbmRzTGFzdERpZ2l0cy5wdXNoKGopO1xyXG4gICAgfVxyXG4gICAgZm9yIChsZXQgayA9IDE7IGsgPD0gMjQ7IGsrKykge1xyXG4gICAgICB0aGlzLmhvdXJMaXN0Vmlldy5wdXNoKGspO1xyXG4gICAgfVxyXG4gICAgZm9yIChsZXQgaG91cnMgPSAxOyBob3VycyA8PSAyMzsgaG91cnMrKykge1xyXG4gICAgICB0aGlzLmhvdXJzdHdvdGhyZWUucHVzaChob3Vycyk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmRheXMgPSBbXCJTdW5kYXlcIiwgXCJNb25kYXlcIiwgXCJUdWVzZGF5XCIsIFwiV2VkbmVzZGF5XCIsIFwiVGh1cnNkYXlcIiwgXCJGcmlkYXlcIiwgXCJTYXR1cmRheVwiXTtcclxuICAgIHRoaXMubW9udGggPSBbXCJKYW51YXJ5XCIsIFwiRmVicnVhcnlcIiwgXCJNYXJjaFwiLCBcIkFwcmlsXCIsIFwiTWF5XCIsIFwiSnVuZVwiLCBcIkp1bHlcIiwgXCJBdWd1c3RcIiwgXCJTZXB0ZW1iZXJcIiwgXCJPY3RvYmVyXCIsXHJcbiAgICAgIFwiTm92ZW1iZXJcIiwgXCJEZWNlbWJlciBcIl07XHJcbiAgICB0aGlzLm1vbnRoc0NvdW50ID0gW107XHJcbiAgICB0aGlzLmRhdGVDb3VudCA9IFtdO1xyXG4gICAgZm9yIChsZXQgZGF0ZUNvdW50cyA9IDE7IGRhdGVDb3VudHMgPD0gNzsgZGF0ZUNvdW50cysrKSB7XHJcbiAgICAgIHRoaXMuZGF0ZUNvdW50LnB1c2goZGF0ZUNvdW50cyk7XHJcbiAgICB9XHJcbiAgICBmb3IgKGxldCBtb250aENvdW50ID0gMTsgbW9udGhDb3VudCA8PSAxMjsgbW9udGhDb3VudCsrKSB7XHJcbiAgICAgIHRoaXMubW9udGhzQ291bnQucHVzaChtb250aENvdW50KTtcclxuICAgIH1cclxuICAgIHRoaXMubnVtT2ZkYXlzID0gW107XHJcbiAgICB0aGlzLmRheXMuZm9yRWFjaChlbGVtZW50ID0+IHtcclxuICAgICAgY29uc29sZS5sb2coZWxlbWVudCk7XHJcbiAgICAgIHRoaXMubnVtT2ZkYXlzLnB1c2goZWxlbWVudCk7XHJcbiAgICB9KTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMubnVtT2ZkYXlzKVxyXG4gICAgdGhpcy5udW1PZm1vbnRocyA9IFtdO1xyXG4gICAgdGhpcy5tb250aC5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICB0aGlzLm51bU9mbW9udGhzLnB1c2goZWxlbWVudCk7XHJcbiAgICB9KVxyXG4gIH1cclxuICBzZWNvbmRDaGFuZ2UoKSB7XHJcbiAgICB0aGlzLnNlY29uZHNWaWV3U2hvdyA9IHRydWU7XHJcbiAgICB0aGlzLk1pbnV0ZXNWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5ob3VyVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuZGF5dmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGh2aWV3U2hvdyA9IGZhbHNlO1xyXG4gIH1cclxuICBtaW51dGVzQ2hhbmdlKCkge1xyXG4gICAgdGhpcy5zZWNvbmRzVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuaG91clZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLk1pbnV0ZXNWaWV3U2hvdyA9IHRydWU7XHJcbiAgICB0aGlzLmRheXZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRodmlld1Nob3cgPSBmYWxzZTtcclxuICB9XHJcbiAgaG91ckNoYW5nZSgpIHtcclxuICAgIHRoaXMuaG91clZpZXdTaG93ID0gdHJ1ZTtcclxuICAgIHRoaXMuTWludXRlc1ZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlY29uZHNWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYXl2aWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5tb250aHZpZXdTaG93ID0gZmFsc2U7XHJcbiAgfVxyXG4gIGRheUNoYW5nZSgpIHtcclxuICAgIHRoaXMuZGF5dmlld1Nob3cgPSB0cnVlO1xyXG4gICAgdGhpcy5ob3VyVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuTWludXRlc1ZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlY29uZHNWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5tb250aHZpZXdTaG93ID0gZmFsc2U7XHJcbiAgfVxyXG4gIG1vbnRoQ2hhbmdlKCkge1xyXG4gICAgdGhpcy5tb250aHZpZXdTaG93ID0gdHJ1ZTtcclxuICAgIHRoaXMuaG91clZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLk1pbnV0ZXNWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5zZWNvbmRzVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuZGF5dmlld1Nob3cgPSBmYWxzZTtcclxuICB9XHJcbiAgZXZlcnlNaW51dGVzKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCJycnJcIik7XHJcbiAgICB0aGlzLmdldG1pbiA9IHRydWU7XHJcbiAgICBsZXQgZXZlcnlNaW51dGUgPSBldmVudC52YWx1ZTtcclxuICAgIGNvbnNvbGUubG9nKGV2ZXJ5TWludXRlLFwiPj4+ZXZlcnlNaW51dGVcIilcclxuICAgIHRoaXMuY3JvcEV4cHJlc3Npb25TZWxlY3Rpb24uZXZlcnltaW51dGVNYWluID0gZXZlcnlNaW51dGU7XHJcbiAgICAvLyBjb25zb2xlLmxvZyh0aGlzLm9uUGVybVNlbGVjdGlvbk9wdGlvbi5ldmVyeW1pbnV0ZU1haW4pO1xyXG4gIH1cclxuICBob3VyRXhwIDogYm9vbGVhbjtcclxuICBkYXRlQ0V4cCA6IGJvb2xlYW47XHJcbiAgbW9udGhWaWV3ZnVsbCA6IGJvb2xlYW47XHJcbiAgZnVsbE1vbnRoIDogYm9vbGVhbjtcclxuICBkYXlzRXhwIDogYm9vbGVhbjtcclxuICBldmVyeU1pbnV0ZXNFeHAgOiBhbnk7XHJcbiAgY3JvbkV4cHJlc3Npb24gOiBhbnk7XHJcbiAgZXZlcnlTZWNvbmRPZk1pbnV0ZXNFeHAgOiBhbnk7XHJcbiAgZXZlckhvdXJFeHAgOiBhbnk7XHJcbiAgZXZlcnlNaW51dGVTZWxlY3QoZSkge1xyXG4gICAgY29uc29sZS5sb2coZSwgXCJkZWZlXCIpO1xyXG4gICAgdGhpcy5nZXRtaW4gPSB0cnVlO1xyXG4gICAgdGhpcy5ob3VyRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmRhdGVDRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRoVmlld2Z1bGwgPSBmYWxzZTtcclxuICAgIHRoaXMuZnVsbE1vbnRoID0gZmFsc2VcclxuICAgIHRoaXMuZGF5c0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5jcm9uRXhwcmVzc2lvbiA9ICcqLycrIGUudmFsdWUgKycgKiAqICogKic7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNyb25FeHByZXNzaW9uKTtcclxuICB9XHJcbiAgZXZlcnlTZWNvbmRPZk1pbnV0ZXMoZSkge1xyXG4gICAgY29uc29sZS5sb2coZSwgXCJkZGRkXCIpO1xyXG4gICAgLy8gdGhpcy5nZXRtaW4gPSBmYWxzZTtcclxuICAgIC8vIHRoaXMuaG91ckV4cCA9IHRydWU7XHJcbiAgICB0aGlzLmV2ZXJ5U2Vjb25kT2ZNaW51dGVzRXhwID0gZS52YWx1ZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZXZlcnlTZWNvbmRPZk1pbnV0ZXNFeHApO1xyXG4gIH1cclxuICBzZWxlY3RIb3VyKGUpIHtcclxuICAgIHRoaXMuZ2V0bWluID0gZmFsc2U7XHJcbiAgICB0aGlzLmhvdXJFeHAgPSB0cnVlO1xyXG4gICAgdGhpcy5kYXRlQ0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5tb250aFZpZXdmdWxsID0gZmFsc2U7XHJcbiAgICB0aGlzLmZ1bGxNb250aCA9IGZhbHNlXHJcbiAgICB0aGlzLmRheXNFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMuZXZlckhvdXJFeHAgPSBlLnZhbHVlO1xyXG4gICAgdGhpcy5jcm9uRXhwcmVzc2lvbiA9IFwiMCAwL1wiICtlLnZhbHVlICsnICogKiAqJztcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY3JvbkV4cHJlc3Npb24pO1xyXG4gIH1cclxuICBkYXRlQ291bnREZXRhaWxzRXhwIDogYW55O1xyXG4gIGRhdGVDb3VudHMoZSkge1xyXG4gICAgdGhpcy5kYXRlQ0V4cCA9IHRydWU7XHJcbiAgICB0aGlzLmdldG1pbiA9IGZhbHNlO1xyXG4gICAgdGhpcy5ob3VyRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRoVmlld2Z1bGwgPSBmYWxzZTtcclxuICAgIHRoaXMuZnVsbE1vbnRoID0gZmFsc2VcclxuICAgIHRoaXMuZGF5c0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYXRlQ291bnREZXRhaWxzRXhwID0gZS52YWx1ZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0ZUNvdW50RGV0YWlsc0V4cCk7XHJcbiAgICBsZXQgZGF5SWR4ID0gdGhpcy5kYXlzLmluZGV4T2YodGhpcy5kYXlzQ291bnRzRXhwKVxyXG4gICAgdGhpcy5jcm9uRXhwcmVzc2lvbiA9XCIwIDAgKi9cIisgZS52YWx1ZSArICBcIiAqIFwiK2RheUlkeCsnLTYnO1xyXG4gIH1cclxuICBkYXlzQ291bnRzRXhwIDogYW55O1xyXG4gIGRheXNDb3VudHMoZSkge1xyXG4gICAgdGhpcy5kYXlzRXhwID0gdHJ1ZTtcclxuICAgIHRoaXMuZGF0ZUNFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMuZ2V0bWluID0gZmFsc2U7XHJcbiAgICB0aGlzLmhvdXJFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGhWaWV3ZnVsbCA9IGZhbHNlO1xyXG4gICAgdGhpcy5mdWxsTW9udGggPSBmYWxzZVxyXG4gICAgdGhpcy5kYXlzQ291bnRzRXhwID0gZS52YWx1ZTtcclxuICAgIGxldCBkYXlJZHggPSB0aGlzLmRheXMuaW5kZXhPZih0aGlzLmRheXNDb3VudHNFeHApXHJcbiAgICB0aGlzLmNyb25FeHByZXNzaW9uID1cIjAgMCAqL1wiKyB0aGlzLmRhdGVDb3VudERldGFpbHNFeHAgKyAgXCIgKiBcIitkYXlJZHgrJy02JztcclxuICAgIFxyXG4gIH1cclxuICBtb250aGV4cCA6IGFueTtcclxuICBtb250aEV4cChlKSB7XHJcbiAgICB0aGlzLmRhdGVDRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmdldG1pbiA9IGZhbHNlO1xyXG4gICAgdGhpcy5ob3VyRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmRheXNFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGhWaWV3ZnVsbCA9IHRydWU7XHJcbiAgICB0aGlzLmZ1bGxNb250aCA9IHRydWVcclxuICAgIHRoaXMubW9udGhleHAgPSBlLnZhbHVlO1xyXG4gICAgbGV0IG1udGhJZHggPSB0aGlzLm1vbnRoLmluZGV4T2YodGhpcy5tb250aGV4cClcclxuICAgIHRoaXMuY3JvbkV4cHJlc3Npb24gPVwiMCAwIDEgXCIrIG1udGhJZHggKycvJysgdGhpcy5tb250aENvdW50RXhwICsgIFwiICpcIjtcclxuICB9XHJcbiAgbW9udGhDb3VudEV4cCA6IGFueVxyXG4gIG1vbnRoQ291bnQoZSkge1xyXG4gICAgdGhpcy5kYXRlQ0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYXlzRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmdldG1pbiA9IGZhbHNlO1xyXG4gICAgdGhpcy5ob3VyRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRoVmlld2Z1bGwgPSBmYWxzZTtcclxuICAgIHRoaXMuZnVsbE1vbnRoID0gdHJ1ZTtcclxuICAgIHRoaXMubW9udGhDb3VudEV4cCA9IGUudmFsdWU7XHJcbiAgICBsZXQgbW50aElkeCA9IHRoaXMubW9udGguaW5kZXhPZih0aGlzLm1vbnRoZXhwKVxyXG4gICAgdGhpcy5jcm9uRXhwcmVzc2lvbiA9XCIwIDAgMSBcIisgbW50aElkeCArJy8nKyBlLnZhbHVlICsgIFwiICpcIjtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0aW9uU3RhcnREYXkoZXZlbnQpIHt9XHJcbiAgU2VsZWN0ZWREYXlDb3VudChldmVudCkge31cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEltcG9ydFZhbHVlIHtcclxuICBkYXRhX3NvdXJjZTogc3RyaW5nO1xyXG4gIGZpbGU6IHN0cmluZztcclxuXHJcbiAgY29uc3RydWN0b3IoY29udHJvbCkge1xyXG4gICAgdGhpcy5kYXRhX3NvdXJjZSA9IGNvbnRyb2wuZGF0YV9zb3VyY2UgfHwgXCJcIjtcclxuICAgIHRoaXMuZmlsZSA9IGNvbnRyb2wuZmlsZSB8fCBcIlwiO1xyXG4gIH1cclxufVxyXG4iXX0=