import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
import { FormLayoutComponent } from "../form-layout/form-layout.component";
import { TableLayoutModule } from "../table-layout/table-layout.module";
import { NewTableLayoutModule } from "../new-table-layout/new-table-layout.module";
var FormLayoutModule = /** @class */ (function () {
    function FormLayoutModule() {
    }
    FormLayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [FormLayoutComponent],
                    imports: [
                        RouterModule,
                        TableLayoutModule,
                        MaterialModule,
                        TranslateModule,
                        NewTableLayoutModule,
                        FuseSharedModule
                    ],
                    exports: [FormLayoutComponent]
                },] }
    ];
    return FormLayoutModule;
}());
export { FormLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1sYXlvdXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImZvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRW5GO0lBQUE7SUFZK0IsQ0FBQzs7Z0JBWi9CLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztvQkFDbkMsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osaUJBQWlCO3dCQUNqQixjQUFjO3dCQUNkLGVBQWU7d0JBQ2Ysb0JBQW9CO3dCQUNwQixnQkFBZ0I7cUJBQ2pCO29CQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixDQUFDO2lCQUMvQjs7SUFDOEIsdUJBQUM7Q0FBQSxBQVpoQyxJQVlnQztTQUFuQixnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9zaGFyZWQubW9kdWxlXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5cclxuaW1wb3J0IHsgRm9ybUxheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi9mb3JtLWxheW91dC9mb3JtLWxheW91dC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgVGFibGVMYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vdGFibGUtbGF5b3V0L3RhYmxlLWxheW91dC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgTmV3VGFibGVMYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vbmV3LXRhYmxlLWxheW91dC9uZXctdGFibGUtbGF5b3V0Lm1vZHVsZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtGb3JtTGF5b3V0Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICBUYWJsZUxheW91dE1vZHVsZSxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG4gICAgTmV3VGFibGVMYXlvdXRNb2R1bGUsXHJcbiAgICBGdXNlU2hhcmVkTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbRm9ybUxheW91dENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1MYXlvdXRNb2R1bGUge31cclxuIl19