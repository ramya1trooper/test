import { Component } from '@angular/core';
var FooterComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function FooterComponent() {
        this.currentYear = (new Date()).getFullYear();
    }
    FooterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'footer',
                    template: "<mat-toolbar class=\"sen-footer\">\r\n  <div fxLayout=\"row\" fxLayoutAlign=\"center center\" fxLayoutAlign.gt-xs=\"space-between center\" class=\"h4\" fxFlex>\r\n    <span class=\"sen-text-black\">Copyrights &copy; {{currentYear}}, Trooper. All Rights Reserved.</span>\r\n  </div>\r\n</mat-toolbar>\r\n",
                    styles: [":host{display:-webkit-box;display:flex;-webkit-box-flex:0;flex:0 0 auto;z-index:3}:host .mat-toolbar{background:inherit;color:inherit;box-shadow:0 -1px 1px -1px rgba(0,0,0,.2),0 0 1px 0 rgba(0,0,0,.14),0 -1px 3px 0 rgba(0,0,0,.12)}:host.above{position:relative;z-index:99}.sen-footer{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;height:42px;bottom:0;color:rgba(0,0,0,.65);text-align:center;padding:12px 30px;right:0;left:250px;background-color:#fff!important;border-top:1px solid rgba(0,0,0,.15)}.sen-text-black{color:#000!important}"]
                }] }
    ];
    /** @nocollapse */
    FooterComponent.ctorParameters = function () { return []; };
    return FooterComponent;
}());
export { FooterComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJmb290ZXIvZm9vdGVyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTFDO0lBU0k7O09BRUc7SUFDSDtRQUxBLGdCQUFXLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7SUFPekMsQ0FBQzs7Z0JBZEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBSyxRQUFRO29CQUNyQiwyVEFBc0M7O2lCQUV6Qzs7OztJQVdELHNCQUFDO0NBQUEsQUFmRCxJQWVDO1NBVlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvciAgIDogJ2Zvb3RlcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZm9vdGVyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJscyAgOiBbJy4vZm9vdGVyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvb3RlckNvbXBvbmVudFxyXG57XHJcbiAgICBjdXJyZW50WWVhciA9IChuZXcgRGF0ZSgpKS5nZXRGdWxsWWVhcigpO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoKVxyXG4gICAge1xyXG4gICAgfVxyXG59XHJcbiJdfQ==