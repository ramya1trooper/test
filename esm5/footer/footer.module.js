import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { FooterComponent } from "./footer.component";
import { MaterialModule } from "../material.module";
var FooterModule = /** @class */ (function () {
    function FooterModule() {
    }
    FooterModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [FooterComponent],
                    imports: [RouterModule, MaterialModule, FuseSharedModule],
                    exports: [FooterComponent]
                },] }
    ];
    return FooterModule;
}());
export { FooterModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9vdGVyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJmb290ZXIvZm9vdGVyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUUxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDckQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBEO0lBQUE7SUFLMkIsQ0FBQzs7Z0JBTDNCLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxlQUFlLENBQUM7b0JBQy9CLE9BQU8sRUFBRSxDQUFDLFlBQVksRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLENBQUM7b0JBQ3pELE9BQU8sRUFBRSxDQUFDLGVBQWUsQ0FBQztpQkFDM0I7O0lBQzBCLG1CQUFDO0NBQUEsQUFMNUIsSUFLNEI7U0FBZixZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IEZvb3RlckNvbXBvbmVudCB9IGZyb20gXCIuL2Zvb3Rlci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0Zvb3RlckNvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1JvdXRlck1vZHVsZSwgTWF0ZXJpYWxNb2R1bGUsIEZ1c2VTaGFyZWRNb2R1bGVdLFxyXG4gIGV4cG9ydHM6IFtGb290ZXJDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb290ZXJNb2R1bGUge31cclxuIl19