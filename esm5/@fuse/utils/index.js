import * as tslib_1 from "tslib";
// @dynamic
var FuseUtils = /** @class */ (function () {
    function FuseUtils() {
    }
    /**
     * Filter array by string
     *
     * @param mainArr
     * @param searchText
     * @returns {any}
     */
    FuseUtils.filterArrayByString = function (mainArr, searchText) {
        var _this = this;
        if (searchText === "") {
            return mainArr;
        }
        searchText = searchText.toLowerCase();
        return mainArr.filter(function (itemObj) {
            return _this.searchInObj(itemObj, searchText);
        });
    };
    /**
     * Search in object
     *
     * @param itemObj
     * @param searchText
     * @returns {boolean}
     */
    FuseUtils.searchInObj = function (itemObj, searchText) {
        for (var prop in itemObj) {
            if (!itemObj.hasOwnProperty(prop)) {
                continue;
            }
            var value = itemObj[prop];
            if (typeof value === "string") {
                if (this.searchInString(value, searchText)) {
                    return true;
                }
            }
            else if (Array.isArray(value)) {
                if (this.searchInArray(value, searchText)) {
                    return true;
                }
            }
            if (typeof value === "object") {
                if (this.searchInObj(value, searchText)) {
                    return true;
                }
            }
        }
    };
    /**
     * Search in array
     *
     * @param arr
     * @param searchText
     * @returns {boolean}
     */
    FuseUtils.searchInArray = function (arr, searchText) {
        var e_1, _a;
        try {
            for (var arr_1 = tslib_1.__values(arr), arr_1_1 = arr_1.next(); !arr_1_1.done; arr_1_1 = arr_1.next()) {
                var value = arr_1_1.value;
                if (typeof value === "string") {
                    if (this.searchInString(value, searchText)) {
                        return true;
                    }
                }
                if (typeof value === "object") {
                    if (this.searchInObj(value, searchText)) {
                        return true;
                    }
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (arr_1_1 && !arr_1_1.done && (_a = arr_1.return)) _a.call(arr_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * Search in string
     *
     * @param value
     * @param searchText
     * @returns {any}
     */
    FuseUtils.searchInString = function (value, searchText) {
        return value.toLowerCase().includes(searchText);
    };
    /**
     * Generate a unique GUID
     *
     * @returns {string}
     */
    FuseUtils.generateGUID = function () {
        function S4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return S4() + S4();
    };
    /**
     * Toggle in array
     *
     * @param item
     * @param array
     */
    FuseUtils.toggleInArray = function (item, array) {
        if (array.indexOf(item) === -1) {
            array.push(item);
        }
        else {
            array.splice(array.indexOf(item), 1);
        }
    };
    /**
     * Handleize
     *
     * @param text
     * @returns {string}
     */
    FuseUtils.handleize = function (text) {
        return text
            .toString()
            .toLowerCase()
            .replace(/\s+/g, "-") // Replace spaces with -
            .replace(/[^\w\-]+/g, "") // Remove all non-word chars
            .replace(/\-\-+/g, "-") // Replace multiple - with single -
            .replace(/^-+/, "") // Trim - from start of text
            .replace(/-+$/, ""); // Trim - from end of text
    };
    return FuseUtils;
}());
export { FuseUtils };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvdXRpbHMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLFdBQVc7QUFDWDtJQUFBO0lBb0lBLENBQUM7SUFuSUM7Ozs7OztPQU1HO0lBQ1csNkJBQW1CLEdBQWpDLFVBQWtDLE9BQU8sRUFBRSxVQUFVO1FBQXJELGlCQVVDO1FBVEMsSUFBSSxVQUFVLEtBQUssRUFBRSxFQUFFO1lBQ3JCLE9BQU8sT0FBTyxDQUFDO1NBQ2hCO1FBRUQsVUFBVSxHQUFHLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUV0QyxPQUFPLE9BQU8sQ0FBQyxNQUFNLENBQUMsVUFBQSxPQUFPO1lBQzNCLE9BQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDL0MsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ1cscUJBQVcsR0FBekIsVUFBMEIsT0FBTyxFQUFFLFVBQVU7UUFDM0MsS0FBSyxJQUFNLElBQUksSUFBSSxPQUFPLEVBQUU7WUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2pDLFNBQVM7YUFDVjtZQUVELElBQU0sS0FBSyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUU1QixJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDN0IsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtvQkFDMUMsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQy9CLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLEVBQUU7b0JBQ3pDLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFFRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtvQkFDdkMsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNXLHVCQUFhLEdBQTNCLFVBQTRCLEdBQUcsRUFBRSxVQUFVOzs7WUFDekMsS0FBb0IsSUFBQSxRQUFBLGlCQUFBLEdBQUcsQ0FBQSx3QkFBQSx5Q0FBRTtnQkFBcEIsSUFBTSxLQUFLLGdCQUFBO2dCQUNkLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFO29CQUM3QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxFQUFFO3dCQUMxQyxPQUFPLElBQUksQ0FBQztxQkFDYjtpQkFDRjtnQkFFRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtvQkFDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTt3QkFDdkMsT0FBTyxJQUFJLENBQUM7cUJBQ2I7aUJBQ0Y7YUFDRjs7Ozs7Ozs7O0lBQ0gsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNXLHdCQUFjLEdBQTVCLFVBQTZCLEtBQUssRUFBRSxVQUFVO1FBQzVDLE9BQU8sS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUNsRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNXLHNCQUFZLEdBQTFCO1FBQ0UsU0FBUyxFQUFFO1lBQ1QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQztpQkFDN0MsUUFBUSxDQUFDLEVBQUUsQ0FBQztpQkFDWixTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEIsQ0FBQztRQUVELE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ1csdUJBQWEsR0FBM0IsVUFBNEIsSUFBSSxFQUFFLEtBQUs7UUFDckMsSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQzlCLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEI7YUFBTTtZQUNMLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN0QztJQUNILENBQUM7SUFFRDs7Ozs7T0FLRztJQUNXLG1CQUFTLEdBQXZCLFVBQXdCLElBQUk7UUFDMUIsT0FBTyxJQUFJO2FBQ1IsUUFBUSxFQUFFO2FBQ1YsV0FBVyxFQUFFO2FBQ2IsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQyx3QkFBd0I7YUFDN0MsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQyw0QkFBNEI7YUFDckQsT0FBTyxDQUFDLFFBQVEsRUFBRSxHQUFHLENBQUMsQ0FBQyxtQ0FBbUM7YUFDMUQsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyw0QkFBNEI7YUFDL0MsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLDBCQUEwQjtJQUNuRCxDQUFDO0lBQ0gsZ0JBQUM7QUFBRCxDQUFDLEFBcElELElBb0lDIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQGR5bmFtaWNcclxuZXhwb3J0IGNsYXNzIEZ1c2VVdGlscyB7XHJcbiAgLyoqXHJcbiAgICogRmlsdGVyIGFycmF5IGJ5IHN0cmluZ1xyXG4gICAqXHJcbiAgICogQHBhcmFtIG1haW5BcnJcclxuICAgKiBAcGFyYW0gc2VhcmNoVGV4dFxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBmaWx0ZXJBcnJheUJ5U3RyaW5nKG1haW5BcnIsIHNlYXJjaFRleHQpOiBhbnkge1xyXG4gICAgaWYgKHNlYXJjaFRleHQgPT09IFwiXCIpIHtcclxuICAgICAgcmV0dXJuIG1haW5BcnI7XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoVGV4dCA9IHNlYXJjaFRleHQudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICByZXR1cm4gbWFpbkFyci5maWx0ZXIoaXRlbU9iaiA9PiB7XHJcbiAgICAgIHJldHVybiB0aGlzLnNlYXJjaEluT2JqKGl0ZW1PYmosIHNlYXJjaFRleHQpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZWFyY2ggaW4gb2JqZWN0XHJcbiAgICpcclxuICAgKiBAcGFyYW0gaXRlbU9ialxyXG4gICAqIEBwYXJhbSBzZWFyY2hUZXh0XHJcbiAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBzZWFyY2hJbk9iaihpdGVtT2JqLCBzZWFyY2hUZXh0KTogYm9vbGVhbiB7XHJcbiAgICBmb3IgKGNvbnN0IHByb3AgaW4gaXRlbU9iaikge1xyXG4gICAgICBpZiAoIWl0ZW1PYmouaGFzT3duUHJvcGVydHkocHJvcCkpIHtcclxuICAgICAgICBjb250aW51ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgdmFsdWUgPSBpdGVtT2JqW3Byb3BdO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJzdHJpbmdcIikge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEluU3RyaW5nKHZhbHVlLCBzZWFyY2hUZXh0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VhcmNoSW5BcnJheSh2YWx1ZSwgc2VhcmNoVGV4dCkpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEluT2JqKHZhbHVlLCBzZWFyY2hUZXh0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZWFyY2ggaW4gYXJyYXlcclxuICAgKlxyXG4gICAqIEBwYXJhbSBhcnJcclxuICAgKiBAcGFyYW0gc2VhcmNoVGV4dFxyXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxyXG4gICAqL1xyXG4gIHB1YmxpYyBzdGF0aWMgc2VhcmNoSW5BcnJheShhcnIsIHNlYXJjaFRleHQpOiBib29sZWFuIHtcclxuICAgIGZvciAoY29uc3QgdmFsdWUgb2YgYXJyKSB7XHJcbiAgICAgIGlmICh0eXBlb2YgdmFsdWUgPT09IFwic3RyaW5nXCIpIHtcclxuICAgICAgICBpZiAodGhpcy5zZWFyY2hJblN0cmluZyh2YWx1ZSwgc2VhcmNoVGV4dCkpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEluT2JqKHZhbHVlLCBzZWFyY2hUZXh0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZWFyY2ggaW4gc3RyaW5nXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdmFsdWVcclxuICAgKiBAcGFyYW0gc2VhcmNoVGV4dFxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBzZWFyY2hJblN0cmluZyh2YWx1ZSwgc2VhcmNoVGV4dCk6IGFueSB7XHJcbiAgICByZXR1cm4gdmFsdWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hUZXh0KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdlbmVyYXRlIGEgdW5pcXVlIEdVSURcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBnZW5lcmF0ZUdVSUQoKTogc3RyaW5nIHtcclxuICAgIGZ1bmN0aW9uIFM0KCk6IHN0cmluZyB7XHJcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKCgxICsgTWF0aC5yYW5kb20oKSkgKiAweDEwMDAwKVxyXG4gICAgICAgIC50b1N0cmluZygxNilcclxuICAgICAgICAuc3Vic3RyaW5nKDEpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBTNCgpICsgUzQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRvZ2dsZSBpbiBhcnJheVxyXG4gICAqXHJcbiAgICogQHBhcmFtIGl0ZW1cclxuICAgKiBAcGFyYW0gYXJyYXlcclxuICAgKi9cclxuICBwdWJsaWMgc3RhdGljIHRvZ2dsZUluQXJyYXkoaXRlbSwgYXJyYXkpOiB2b2lkIHtcclxuICAgIGlmIChhcnJheS5pbmRleE9mKGl0ZW0pID09PSAtMSkge1xyXG4gICAgICBhcnJheS5wdXNoKGl0ZW0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgYXJyYXkuc3BsaWNlKGFycmF5LmluZGV4T2YoaXRlbSksIDEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogSGFuZGxlaXplXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdGV4dFxyXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBoYW5kbGVpemUodGV4dCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGV4dFxyXG4gICAgICAudG9TdHJpbmcoKVxyXG4gICAgICAudG9Mb3dlckNhc2UoKVxyXG4gICAgICAucmVwbGFjZSgvXFxzKy9nLCBcIi1cIikgLy8gUmVwbGFjZSBzcGFjZXMgd2l0aCAtXHJcbiAgICAgIC5yZXBsYWNlKC9bXlxcd1xcLV0rL2csIFwiXCIpIC8vIFJlbW92ZSBhbGwgbm9uLXdvcmQgY2hhcnNcclxuICAgICAgLnJlcGxhY2UoL1xcLVxcLSsvZywgXCItXCIpIC8vIFJlcGxhY2UgbXVsdGlwbGUgLSB3aXRoIHNpbmdsZSAtXHJcbiAgICAgIC5yZXBsYWNlKC9eLSsvLCBcIlwiKSAvLyBUcmltIC0gZnJvbSBzdGFydCBvZiB0ZXh0XHJcbiAgICAgIC5yZXBsYWNlKC8tKyQvLCBcIlwiKTsgLy8gVHJpbSAtIGZyb20gZW5kIG9mIHRleHRcclxuICB9XHJcbn1cclxuIl19