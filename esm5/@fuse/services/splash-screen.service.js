import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { animate, AnimationBuilder, style } from '@angular/animations';
import { NavigationEnd, Router } from '@angular/router';
import { filter, take } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/animations";
import * as i2 from "@angular/common";
import * as i3 from "@angular/router";
var FuseSplashScreenService = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {AnimationBuilder} _animationBuilder
     * @param _document
     * @param {Router} _router
     */
    function FuseSplashScreenService(_animationBuilder, _document, _router) {
        this._animationBuilder = _animationBuilder;
        this._document = _document;
        this._router = _router;
        // Initialize
        this._init();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    FuseSplashScreenService.prototype._init = function () {
        var _this = this;
        // Get the splash screen element
        this.splashScreenEl = this._document.body.querySelector('#fuse-splash-screen');
        // If the splash screen element exists...
        if (this.splashScreenEl) {
            // Hide it on the first NavigationEnd event
            this._router.events
                .pipe(filter((function (event) { return event instanceof NavigationEnd; })), take(1))
                .subscribe(function () {
                setTimeout(function () {
                    _this.hide();
                });
            });
        }
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Show the splash screen
     */
    FuseSplashScreenService.prototype.show = function () {
        var _this = this;
        this.player =
            this._animationBuilder
                .build([
                style({
                    opacity: '0',
                    zIndex: '99999'
                }),
                animate('400ms ease', style({ opacity: '1' }))
            ]).create(this.splashScreenEl);
        setTimeout(function () {
            _this.player.play();
        }, 0);
    };
    /**
     * Hide the splash screen
     */
    FuseSplashScreenService.prototype.hide = function () {
        var _this = this;
        this.player =
            this._animationBuilder
                .build([
                style({ opacity: '1' }),
                animate('400ms ease', style({
                    opacity: '0',
                    zIndex: '-10'
                }))
            ]).create(this.splashScreenEl);
        setTimeout(function () {
            _this.player.play();
        }, 0);
    };
    FuseSplashScreenService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    FuseSplashScreenService.ctorParameters = function () { return [
        { type: AnimationBuilder },
        { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] }] },
        { type: Router }
    ]; };
    FuseSplashScreenService.ngInjectableDef = i0.defineInjectable({ factory: function FuseSplashScreenService_Factory() { return new FuseSplashScreenService(i0.inject(i1.AnimationBuilder), i0.inject(i2.DOCUMENT), i0.inject(i3.Router)); }, token: FuseSplashScreenService, providedIn: "root" });
    return FuseSplashScreenService;
}());
export { FuseSplashScreenService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BsYXNoLXNjcmVlbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL3NlcnZpY2VzL3NwbGFzaC1zY3JlZW4uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBbUIsS0FBSyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDeEYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUV4RCxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7OztBQUU5QztJQVFJOzs7Ozs7T0FNRztJQUNILGlDQUNZLGlCQUFtQyxFQUNqQixTQUFjLEVBQ2hDLE9BQWU7UUFGZixzQkFBaUIsR0FBakIsaUJBQWlCLENBQWtCO1FBQ2pCLGNBQVMsR0FBVCxTQUFTLENBQUs7UUFDaEMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUd2QixhQUFhO1FBQ2IsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0ssdUNBQUssR0FBYjtRQUFBLGlCQW9CQztRQWxCRyxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUUvRSx5Q0FBeUM7UUFDekMsSUFBSyxJQUFJLENBQUMsY0FBYyxFQUN4QjtZQUNJLDJDQUEyQztZQUMzQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07aUJBQ2QsSUFBSSxDQUNELE1BQU0sQ0FBQyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSyxZQUFZLGFBQWEsRUFBOUIsQ0FBOEIsQ0FBQyxDQUFDLEVBQ2pELElBQUksQ0FBQyxDQUFDLENBQUMsQ0FDVjtpQkFDQSxTQUFTLENBQUM7Z0JBQ1AsVUFBVSxDQUFDO29CQUNQLEtBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFDaEIsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztTQUNWO0lBQ0wsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsc0NBQUksR0FBSjtRQUFBLGlCQWVDO1FBYkcsSUFBSSxDQUFDLE1BQU07WUFDUCxJQUFJLENBQUMsaUJBQWlCO2lCQUNqQixLQUFLLENBQUM7Z0JBQ0gsS0FBSyxDQUFDO29CQUNGLE9BQU8sRUFBRSxHQUFHO29CQUNaLE1BQU0sRUFBRyxPQUFPO2lCQUNuQixDQUFDO2dCQUNGLE9BQU8sQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7YUFDL0MsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFdkMsVUFBVSxDQUFDO1lBQ1AsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDVixDQUFDO0lBRUQ7O09BRUc7SUFDSCxzQ0FBSSxHQUFKO1FBQUEsaUJBZUM7UUFiRyxJQUFJLENBQUMsTUFBTTtZQUNQLElBQUksQ0FBQyxpQkFBaUI7aUJBQ2pCLEtBQUssQ0FBQztnQkFDSCxLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsR0FBRyxFQUFDLENBQUM7Z0JBQ3JCLE9BQU8sQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDO29CQUN4QixPQUFPLEVBQUUsR0FBRztvQkFDWixNQUFNLEVBQUcsS0FBSztpQkFDakIsQ0FBQyxDQUFDO2FBQ04sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFFdkMsVUFBVSxDQUFDO1lBQ1AsS0FBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN2QixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDVixDQUFDOztnQkFsR0osVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFQaUIsZ0JBQWdCO2dEQXNCekIsTUFBTSxTQUFDLFFBQVE7Z0JBckJBLE1BQU07OztrQ0FIOUI7Q0EwR0MsQUFuR0QsSUFtR0M7U0FoR1ksdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERPQ1VNRU5UIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgYW5pbWF0ZSwgQW5pbWF0aW9uQnVpbGRlciwgQW5pbWF0aW9uUGxheWVyLCBzdHlsZSB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uRW5kLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgZmlsdGVyLCB0YWtlIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlU3BsYXNoU2NyZWVuU2VydmljZVxyXG57XHJcbiAgICBzcGxhc2hTY3JlZW5FbDogYW55O1xyXG4gICAgcGxheWVyOiBBbmltYXRpb25QbGF5ZXI7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7QW5pbWF0aW9uQnVpbGRlcn0gX2FuaW1hdGlvbkJ1aWxkZXJcclxuICAgICAqIEBwYXJhbSBfZG9jdW1lbnRcclxuICAgICAqIEBwYXJhbSB7Um91dGVyfSBfcm91dGVyXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX2FuaW1hdGlvbkJ1aWxkZXI6IEFuaW1hdGlvbkJ1aWxkZXIsXHJcbiAgICAgICAgQEluamVjdChET0NVTUVOVCkgcHJpdmF0ZSBfZG9jdW1lbnQ6IGFueSxcclxuICAgICAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlclxyXG4gICAgKVxyXG4gICAge1xyXG4gICAgICAgIC8vIEluaXRpYWxpemVcclxuICAgICAgICB0aGlzLl9pbml0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgUHJpdmF0ZSBtZXRob2RzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW5pdGlhbGl6ZVxyXG4gICAgICpcclxuICAgICAqIEBwcml2YXRlXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgX2luaXQoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIEdldCB0aGUgc3BsYXNoIHNjcmVlbiBlbGVtZW50XHJcbiAgICAgICAgdGhpcy5zcGxhc2hTY3JlZW5FbCA9IHRoaXMuX2RvY3VtZW50LmJvZHkucXVlcnlTZWxlY3RvcignI2Z1c2Utc3BsYXNoLXNjcmVlbicpO1xyXG5cclxuICAgICAgICAvLyBJZiB0aGUgc3BsYXNoIHNjcmVlbiBlbGVtZW50IGV4aXN0cy4uLlxyXG4gICAgICAgIGlmICggdGhpcy5zcGxhc2hTY3JlZW5FbCApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICAvLyBIaWRlIGl0IG9uIHRoZSBmaXJzdCBOYXZpZ2F0aW9uRW5kIGV2ZW50XHJcbiAgICAgICAgICAgIHRoaXMuX3JvdXRlci5ldmVudHNcclxuICAgICAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbHRlcigoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSksXHJcbiAgICAgICAgICAgICAgICAgICAgdGFrZSgxKVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGlkZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2hvdyB0aGUgc3BsYXNoIHNjcmVlblxyXG4gICAgICovXHJcbiAgICBzaG93KCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLnBsYXllciA9XHJcbiAgICAgICAgICAgIHRoaXMuX2FuaW1hdGlvbkJ1aWxkZXJcclxuICAgICAgICAgICAgICAgIC5idWlsZChbXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAnMCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHpJbmRleCA6ICc5OTk5OSdcclxuICAgICAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgICAgICBhbmltYXRlKCc0MDBtcyBlYXNlJywgc3R5bGUoe29wYWNpdHk6ICcxJ30pKVxyXG4gICAgICAgICAgICAgICAgXSkuY3JlYXRlKHRoaXMuc3BsYXNoU2NyZWVuRWwpO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5wbGF5ZXIucGxheSgpO1xyXG4gICAgICAgIH0sIDApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSGlkZSB0aGUgc3BsYXNoIHNjcmVlblxyXG4gICAgICovXHJcbiAgICBoaWRlKCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLnBsYXllciA9XHJcbiAgICAgICAgICAgIHRoaXMuX2FuaW1hdGlvbkJ1aWxkZXJcclxuICAgICAgICAgICAgICAgIC5idWlsZChbXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGUoe29wYWNpdHk6ICcxJ30pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGUoJzQwMG1zIGVhc2UnLCBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6ICcwJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgekluZGV4IDogJy0xMCdcclxuICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgICAgIF0pLmNyZWF0ZSh0aGlzLnNwbGFzaFNjcmVlbkVsKTtcclxuXHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMucGxheWVyLnBsYXkoKTtcclxuICAgICAgICB9LCAwKTtcclxuICAgIH1cclxufVxyXG4iXX0=