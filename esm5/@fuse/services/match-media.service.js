import { ObservableMedia } from "@angular/flex-layout";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout";
var FuseMatchMediaService = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {ObservableMedia} _observableMedia
     */
    function FuseMatchMediaService(_observableMedia) {
        this._observableMedia = _observableMedia;
        this.onMediaChange = new BehaviorSubject("");
        // Set the defaults
        this.activeMediaQuery = "";
        // Initialize
        this._init();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    FuseMatchMediaService.prototype._init = function () {
        var _this = this;
        this._observableMedia
            .asObservable()
            .pipe(debounceTime(500), distinctUntilChanged())
            .subscribe(function (change) {
            if (_this.activeMediaQuery !== change.mqAlias) {
                _this.activeMediaQuery = change.mqAlias;
                _this.onMediaChange.next(change.mqAlias);
            }
        });
    };
    FuseMatchMediaService.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    FuseMatchMediaService.ctorParameters = function () { return [
        { type: ObservableMedia }
    ]; };
    FuseMatchMediaService.ngInjectableDef = i0.defineInjectable({ factory: function FuseMatchMediaService_Factory() { return new FuseMatchMediaService(i0.inject(i1.ObservableMedia)); }, token: FuseMatchMediaService, providedIn: "root" });
    return FuseMatchMediaService;
}());
export { FuseMatchMediaService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0Y2gtbWVkaWEuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9zZXJ2aWNlcy9tYXRjaC1tZWRpYS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBZSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdkMsT0FBTyxFQUFFLFlBQVksRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7QUFFcEU7SUFPRTs7OztPQUlHO0lBQ0gsK0JBQW9CLGdCQUFpQztRQUFqQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWlCO1FBUHJELGtCQUFhLEdBQTRCLElBQUksZUFBZSxDQUFTLEVBQUUsQ0FBQyxDQUFDO1FBUXZFLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRTNCLGFBQWE7UUFDYixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDZixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNLLHFDQUFLLEdBQWI7UUFBQSxpQkFVQztRQVRDLElBQUksQ0FBQyxnQkFBZ0I7YUFDbEIsWUFBWSxFQUFFO2FBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsRUFBRSxvQkFBb0IsRUFBRSxDQUFDO2FBQy9DLFNBQVMsQ0FBQyxVQUFDLE1BQW1CO1lBQzdCLElBQUksS0FBSSxDQUFDLGdCQUFnQixLQUFLLE1BQU0sQ0FBQyxPQUFPLEVBQUU7Z0JBQzVDLEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUN2QyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDekM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQXZDRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQVBxQixlQUFlOzs7Z0NBQXJDO0NBNkNDLEFBeENELElBd0NDO1NBckNZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE1lZGlhQ2hhbmdlLCBPYnNlcnZhYmxlTWVkaWEgfSBmcm9tIFwiQGFuZ3VsYXIvZmxleC1sYXlvdXRcIjtcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCB7IGRlYm91bmNlVGltZSwgZGlzdGluY3RVbnRpbENoYW5nZWQgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiBcInJvb3RcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU1hdGNoTWVkaWFTZXJ2aWNlIHtcclxuICBhY3RpdmVNZWRpYVF1ZXJ5OiBzdHJpbmc7XHJcbiAgb25NZWRpYUNoYW5nZTogQmVoYXZpb3JTdWJqZWN0PHN0cmluZz4gPSBuZXcgQmVoYXZpb3JTdWJqZWN0PHN0cmluZz4oXCJcIik7XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge09ic2VydmFibGVNZWRpYX0gX29ic2VydmFibGVNZWRpYVxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgX29ic2VydmFibGVNZWRpYTogT2JzZXJ2YWJsZU1lZGlhKSB7XHJcbiAgICAvLyBTZXQgdGhlIGRlZmF1bHRzXHJcbiAgICB0aGlzLmFjdGl2ZU1lZGlhUXVlcnkgPSBcIlwiO1xyXG5cclxuICAgIC8vIEluaXRpYWxpemVcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQcml2YXRlIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBJbml0aWFsaXplXHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX2luaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9vYnNlcnZhYmxlTWVkaWFcclxuICAgICAgLmFzT2JzZXJ2YWJsZSgpXHJcbiAgICAgIC5waXBlKGRlYm91bmNlVGltZSg1MDApLCBkaXN0aW5jdFVudGlsQ2hhbmdlZCgpKVxyXG4gICAgICAuc3Vic2NyaWJlKChjaGFuZ2U6IE1lZGlhQ2hhbmdlKSA9PiB7XHJcbiAgICAgICAgaWYgKHRoaXMuYWN0aXZlTWVkaWFRdWVyeSAhPT0gY2hhbmdlLm1xQWxpYXMpIHtcclxuICAgICAgICAgIHRoaXMuYWN0aXZlTWVkaWFRdWVyeSA9IGNoYW5nZS5tcUFsaWFzO1xyXG4gICAgICAgICAgdGhpcy5vbk1lZGlhQ2hhbmdlLm5leHQoY2hhbmdlLm1xQWxpYXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcbiJdfQ==