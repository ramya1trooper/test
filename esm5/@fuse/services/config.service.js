import { Injectable } from "@angular/core";
import { Router, RoutesRecognized } from "@angular/router";
import { Platform } from "@angular/cdk/platform";
import { BehaviorSubject } from "rxjs";
import { filter } from "rxjs/operators";
import * as _ from "lodash";
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/platform";
import * as i2 from "@angular/router";
// Create the injection token for the custom settings
// export const FUSE_CONFIG = new InjectionToken('fuseCustomConfig');
var FuseConfigService = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {Platform} _platform
     * @param {Router} _router
     * @param _config
     */
    function FuseConfigService(_platform, _router) {
        this._platform = _platform;
        this._router = _router;
        // Set the default config from the user provided config (from forRoot)
        this._defaultConfig = {
            // Color themes can be defined in src/app/app.theme.scss
            colorTheme: "theme-default",
            customScrollbars: true,
            layout: {
                style: "vertical-layout-1",
                width: "fullwidth",
                navbar: {
                    primaryBackground: "fuse-navy-700",
                    secondaryBackground: "fuse-navy-900",
                    folded: false,
                    hidden: false,
                    position: "left",
                    variant: "vertical-style-1"
                },
                toolbar: {
                    customBackgroundColor: false,
                    background: "fuse-white-500",
                    hidden: false,
                    position: "below-static"
                },
                footer: {
                    customBackgroundColor: true,
                    background: "fuse-navy-900",
                    hidden: false,
                    position: "below-fixed"
                },
                sidepanel: {
                    hidden: false,
                    position: "right"
                }
            }
        };
        // Initialize the service
        this._init();
    }
    Object.defineProperty(FuseConfigService.prototype, "config", {
        get: function () {
            return this._configSubject.asObservable();
        },
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        /**
         * Set and get the config
         */
        set: function (value) {
            // Get the value from the behavior subject
            var config = this._configSubject.getValue();
            // Merge the new config
            config = _.merge({}, config, value);
            // Notify the observers
            this._configSubject.next(config);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseConfigService.prototype, "defaultConfig", {
        /**
         * Get default config
         *
         * @returns {any}
         */
        get: function () {
            return this._defaultConfig;
        },
        enumerable: true,
        configurable: true
    });
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    FuseConfigService.prototype._init = function () {
        var _this = this;
        /**
         * Disable custom scrollbars if browser is mobile
         */
        if (this._platform.ANDROID || this._platform.IOS) {
            this._defaultConfig.customScrollbars = false;
        }
        // Set the config from the default config
        this._configSubject = new BehaviorSubject(_.cloneDeep(this._defaultConfig));
        // Reload the default layout config on every RoutesRecognized event
        // if the current layout config is different from the default one
        this._router.events
            .pipe(filter(function (event) { return event instanceof RoutesRecognized; }))
            .subscribe(function () {
            if (!_.isEqual(_this._configSubject.getValue().layout, _this._defaultConfig.layout)) {
                // Clone the current config
                var config = _.cloneDeep(_this._configSubject.getValue());
                // Reset the layout from the default config
                config.layout = _.cloneDeep(_this._defaultConfig.layout);
                // Set the config
                _this._configSubject.next(config);
            }
        });
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Set config
     *
     * @param value
     * @param {{emitEvent: boolean}} opts
     */
    FuseConfigService.prototype.setConfig = function (value, opts) {
        if (opts === void 0) { opts = { emitEvent: true }; }
        // Get the value from the behavior subject
        var config = this._configSubject.getValue();
        // Merge the new config
        config = _.merge({}, config, value);
        // If emitEvent option is true...
        if (opts.emitEvent === true) {
            // Notify the observers
            this._configSubject.next(config);
        }
    };
    /**
     * Get config
     *
     * @returns {Observable<any>}
     */
    FuseConfigService.prototype.getConfig = function () {
        return this._configSubject.asObservable();
    };
    /**
     * Reset to the default config
     */
    FuseConfigService.prototype.resetToDefaults = function () {
        // Set the config from the default config
        this._configSubject.next(_.cloneDeep(this._defaultConfig));
    };
    FuseConfigService.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    FuseConfigService.ctorParameters = function () { return [
        { type: Platform },
        { type: Router }
    ]; };
    FuseConfigService.ngInjectableDef = i0.defineInjectable({ factory: function FuseConfigService_Factory() { return new FuseConfigService(i0.inject(i1.Platform), i0.inject(i2.Router)); }, token: FuseConfigService, providedIn: "root" });
    return FuseConfigService;
}());
export { FuseConfigService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2Uvc2VydmljZXMvY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFVLFVBQVUsRUFBa0IsTUFBTSxlQUFlLENBQUM7QUFDbkUsT0FBTyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4QyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQzs7OztBQUU1QixxREFBcUQ7QUFDckQscUVBQXFFO0FBRXJFO0lBUUU7Ozs7OztPQU1HO0lBQ0gsMkJBQ1UsU0FBbUIsRUFDbkIsT0FBZTtRQURmLGNBQVMsR0FBVCxTQUFTLENBQVU7UUFDbkIsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUd2QixzRUFBc0U7UUFDdEUsSUFBSSxDQUFDLGNBQWMsR0FBRztZQUNwQix3REFBd0Q7WUFDeEQsVUFBVSxFQUFFLGVBQWU7WUFDM0IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixNQUFNLEVBQUU7Z0JBQ04sS0FBSyxFQUFFLG1CQUFtQjtnQkFDMUIsS0FBSyxFQUFFLFdBQVc7Z0JBQ2xCLE1BQU0sRUFBRTtvQkFDTixpQkFBaUIsRUFBRSxlQUFlO29CQUNsQyxtQkFBbUIsRUFBRSxlQUFlO29CQUNwQyxNQUFNLEVBQUUsS0FBSztvQkFDYixNQUFNLEVBQUUsS0FBSztvQkFDYixRQUFRLEVBQUUsTUFBTTtvQkFDaEIsT0FBTyxFQUFFLGtCQUFrQjtpQkFDNUI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNQLHFCQUFxQixFQUFFLEtBQUs7b0JBQzVCLFVBQVUsRUFBRSxnQkFBZ0I7b0JBQzVCLE1BQU0sRUFBRSxLQUFLO29CQUNiLFFBQVEsRUFBRSxjQUFjO2lCQUN6QjtnQkFDRCxNQUFNLEVBQUU7b0JBQ04scUJBQXFCLEVBQUUsSUFBSTtvQkFDM0IsVUFBVSxFQUFFLGVBQWU7b0JBQzNCLE1BQU0sRUFBRSxLQUFLO29CQUNiLFFBQVEsRUFBRSxhQUFhO2lCQUN4QjtnQkFDRCxTQUFTLEVBQUU7b0JBQ1QsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsUUFBUSxFQUFFLE9BQU87aUJBQ2xCO2FBQ0Y7U0FDRixDQUFDO1FBRUYseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFTRCxzQkFBSSxxQ0FBTTthQVdWO1lBQ0UsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzVDLENBQUM7UUFwQkQsd0dBQXdHO1FBQ3hHLGNBQWM7UUFDZCx3R0FBd0c7UUFFeEc7O1dBRUc7YUFDSCxVQUFXLEtBQUs7WUFDZCwwQ0FBMEM7WUFDMUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUU1Qyx1QkFBdUI7WUFDdkIsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUVwQyx1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsQ0FBQzs7O09BQUE7SUFXRCxzQkFBSSw0Q0FBYTtRQUxqQjs7OztXQUlHO2FBQ0g7WUFDRSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7UUFDN0IsQ0FBQzs7O09BQUE7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0ssaUNBQUssR0FBYjtRQUFBLGlCQWdDQztRQS9CQzs7V0FFRztRQUNILElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDaEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7U0FDOUM7UUFFRCx5Q0FBeUM7UUFDekMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBRTVFLG1FQUFtRTtRQUNuRSxpRUFBaUU7UUFDakUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO2FBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLFlBQVksZ0JBQWdCLEVBQWpDLENBQWlDLENBQUMsQ0FBQzthQUN4RCxTQUFTLENBQUM7WUFDVCxJQUNFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FDUixLQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sRUFDckMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQzNCLEVBQ0Q7Z0JBQ0EsMkJBQTJCO2dCQUMzQixJQUFNLE1BQU0sR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztnQkFFM0QsMkNBQTJDO2dCQUMzQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFFeEQsaUJBQWlCO2dCQUNqQixLQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNsQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOzs7OztPQUtHO0lBQ0gscUNBQVMsR0FBVCxVQUFVLEtBQUssRUFBRSxJQUEwQjtRQUExQixxQkFBQSxFQUFBLFNBQVMsU0FBUyxFQUFFLElBQUksRUFBRTtRQUN6QywwQ0FBMEM7UUFDMUMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUU1Qyx1QkFBdUI7UUFDdkIsTUFBTSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUVwQyxpQ0FBaUM7UUFDakMsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksRUFBRTtZQUMzQix1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbEM7SUFDSCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILHFDQUFTLEdBQVQ7UUFDRSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsMkNBQWUsR0FBZjtRQUNFLHlDQUF5QztRQUN6QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO0lBQzdELENBQUM7O2dCQTVLRixVQUFVLFNBQUM7b0JBQ1YsVUFBVSxFQUFFLE1BQU07aUJBQ25COzs7O2dCQVZRLFFBQVE7Z0JBRFIsTUFBTTs7OzRCQURmO0NBdUxDLEFBN0tELElBNktDO1NBMUtZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4gfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIFJvdXRlc1JlY29nbml6ZWQgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFBsYXRmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2Nkay9wbGF0Zm9ybVwiO1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBmaWx0ZXIgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcblxyXG4vLyBDcmVhdGUgdGhlIGluamVjdGlvbiB0b2tlbiBmb3IgdGhlIGN1c3RvbSBzZXR0aW5nc1xyXG4vLyBleHBvcnQgY29uc3QgRlVTRV9DT05GSUcgPSBuZXcgSW5qZWN0aW9uVG9rZW4oJ2Z1c2VDdXN0b21Db25maWcnKTtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiBcInJvb3RcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZUNvbmZpZ1NlcnZpY2Uge1xyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF9jb25maWdTdWJqZWN0OiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICBwcml2YXRlIHJlYWRvbmx5IF9kZWZhdWx0Q29uZmlnOiBhbnk7XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge1BsYXRmb3JtfSBfcGxhdGZvcm1cclxuICAgKiBAcGFyYW0ge1JvdXRlcn0gX3JvdXRlclxyXG4gICAqIEBwYXJhbSBfY29uZmlnXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9wbGF0Zm9ybTogUGxhdGZvcm0sXHJcbiAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlclxyXG4gICkgLy8gQEluamVjdChGVVNFX0NPTkZJRykgcHJpdmF0ZSBfY29uZmlnXHJcbiAge1xyXG4gICAgLy8gU2V0IHRoZSBkZWZhdWx0IGNvbmZpZyBmcm9tIHRoZSB1c2VyIHByb3ZpZGVkIGNvbmZpZyAoZnJvbSBmb3JSb290KVxyXG4gICAgdGhpcy5fZGVmYXVsdENvbmZpZyA9IHtcclxuICAgICAgLy8gQ29sb3IgdGhlbWVzIGNhbiBiZSBkZWZpbmVkIGluIHNyYy9hcHAvYXBwLnRoZW1lLnNjc3NcclxuICAgICAgY29sb3JUaGVtZTogXCJ0aGVtZS1kZWZhdWx0XCIsXHJcbiAgICAgIGN1c3RvbVNjcm9sbGJhcnM6IHRydWUsXHJcbiAgICAgIGxheW91dDoge1xyXG4gICAgICAgIHN0eWxlOiBcInZlcnRpY2FsLWxheW91dC0xXCIsXHJcbiAgICAgICAgd2lkdGg6IFwiZnVsbHdpZHRoXCIsXHJcbiAgICAgICAgbmF2YmFyOiB7XHJcbiAgICAgICAgICBwcmltYXJ5QmFja2dyb3VuZDogXCJmdXNlLW5hdnktNzAwXCIsXHJcbiAgICAgICAgICBzZWNvbmRhcnlCYWNrZ3JvdW5kOiBcImZ1c2UtbmF2eS05MDBcIixcclxuICAgICAgICAgIGZvbGRlZDogZmFsc2UsXHJcbiAgICAgICAgICBoaWRkZW46IGZhbHNlLFxyXG4gICAgICAgICAgcG9zaXRpb246IFwibGVmdFwiLFxyXG4gICAgICAgICAgdmFyaWFudDogXCJ2ZXJ0aWNhbC1zdHlsZS0xXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIHRvb2xiYXI6IHtcclxuICAgICAgICAgIGN1c3RvbUJhY2tncm91bmRDb2xvcjogZmFsc2UsXHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBcImZ1c2Utd2hpdGUtNTAwXCIsXHJcbiAgICAgICAgICBoaWRkZW46IGZhbHNlLFxyXG4gICAgICAgICAgcG9zaXRpb246IFwiYmVsb3ctc3RhdGljXCJcclxuICAgICAgICB9LFxyXG4gICAgICAgIGZvb3Rlcjoge1xyXG4gICAgICAgICAgY3VzdG9tQmFja2dyb3VuZENvbG9yOiB0cnVlLFxyXG4gICAgICAgICAgYmFja2dyb3VuZDogXCJmdXNlLW5hdnktOTAwXCIsXHJcbiAgICAgICAgICBoaWRkZW46IGZhbHNlLFxyXG4gICAgICAgICAgcG9zaXRpb246IFwiYmVsb3ctZml4ZWRcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2lkZXBhbmVsOiB7XHJcbiAgICAgICAgICBoaWRkZW46IGZhbHNlLFxyXG4gICAgICAgICAgcG9zaXRpb246IFwicmlnaHRcIlxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICAvLyBJbml0aWFsaXplIHRoZSBzZXJ2aWNlXHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgQWNjZXNzb3JzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0IGFuZCBnZXQgdGhlIGNvbmZpZ1xyXG4gICAqL1xyXG4gIHNldCBjb25maWcodmFsdWUpIHtcclxuICAgIC8vIEdldCB0aGUgdmFsdWUgZnJvbSB0aGUgYmVoYXZpb3Igc3ViamVjdFxyXG4gICAgbGV0IGNvbmZpZyA9IHRoaXMuX2NvbmZpZ1N1YmplY3QuZ2V0VmFsdWUoKTtcclxuXHJcbiAgICAvLyBNZXJnZSB0aGUgbmV3IGNvbmZpZ1xyXG4gICAgY29uZmlnID0gXy5tZXJnZSh7fSwgY29uZmlnLCB2YWx1ZSk7XHJcblxyXG4gICAgLy8gTm90aWZ5IHRoZSBvYnNlcnZlcnNcclxuICAgIHRoaXMuX2NvbmZpZ1N1YmplY3QubmV4dChjb25maWcpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGNvbmZpZygpOiBhbnkgfCBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2NvbmZpZ1N1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgZGVmYXVsdCBjb25maWdcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgZ2V0IGRlZmF1bHRDb25maWcoKTogYW55IHtcclxuICAgIHJldHVybiB0aGlzLl9kZWZhdWx0Q29uZmlnO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIFByaXZhdGUgbWV0aG9kc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIEluaXRpYWxpemVcclxuICAgKlxyXG4gICAqIEBwcml2YXRlXHJcbiAgICovXHJcbiAgcHJpdmF0ZSBfaW5pdCgpOiB2b2lkIHtcclxuICAgIC8qKlxyXG4gICAgICogRGlzYWJsZSBjdXN0b20gc2Nyb2xsYmFycyBpZiBicm93c2VyIGlzIG1vYmlsZVxyXG4gICAgICovXHJcbiAgICBpZiAodGhpcy5fcGxhdGZvcm0uQU5EUk9JRCB8fCB0aGlzLl9wbGF0Zm9ybS5JT1MpIHtcclxuICAgICAgdGhpcy5fZGVmYXVsdENvbmZpZy5jdXN0b21TY3JvbGxiYXJzID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2V0IHRoZSBjb25maWcgZnJvbSB0aGUgZGVmYXVsdCBjb25maWdcclxuICAgIHRoaXMuX2NvbmZpZ1N1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KF8uY2xvbmVEZWVwKHRoaXMuX2RlZmF1bHRDb25maWcpKTtcclxuXHJcbiAgICAvLyBSZWxvYWQgdGhlIGRlZmF1bHQgbGF5b3V0IGNvbmZpZyBvbiBldmVyeSBSb3V0ZXNSZWNvZ25pemVkIGV2ZW50XHJcbiAgICAvLyBpZiB0aGUgY3VycmVudCBsYXlvdXQgY29uZmlnIGlzIGRpZmZlcmVudCBmcm9tIHRoZSBkZWZhdWx0IG9uZVxyXG4gICAgdGhpcy5fcm91dGVyLmV2ZW50c1xyXG4gICAgICAucGlwZShmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBSb3V0ZXNSZWNvZ25pemVkKSlcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgaWYgKFxyXG4gICAgICAgICAgIV8uaXNFcXVhbChcclxuICAgICAgICAgICAgdGhpcy5fY29uZmlnU3ViamVjdC5nZXRWYWx1ZSgpLmxheW91dCxcclxuICAgICAgICAgICAgdGhpcy5fZGVmYXVsdENvbmZpZy5sYXlvdXRcclxuICAgICAgICAgIClcclxuICAgICAgICApIHtcclxuICAgICAgICAgIC8vIENsb25lIHRoZSBjdXJyZW50IGNvbmZpZ1xyXG4gICAgICAgICAgY29uc3QgY29uZmlnID0gXy5jbG9uZURlZXAodGhpcy5fY29uZmlnU3ViamVjdC5nZXRWYWx1ZSgpKTtcclxuXHJcbiAgICAgICAgICAvLyBSZXNldCB0aGUgbGF5b3V0IGZyb20gdGhlIGRlZmF1bHQgY29uZmlnXHJcbiAgICAgICAgICBjb25maWcubGF5b3V0ID0gXy5jbG9uZURlZXAodGhpcy5fZGVmYXVsdENvbmZpZy5sYXlvdXQpO1xyXG5cclxuICAgICAgICAgIC8vIFNldCB0aGUgY29uZmlnXHJcbiAgICAgICAgICB0aGlzLl9jb25maWdTdWJqZWN0Lm5leHQoY29uZmlnKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0IGNvbmZpZ1xyXG4gICAqXHJcbiAgICogQHBhcmFtIHZhbHVlXHJcbiAgICogQHBhcmFtIHt7ZW1pdEV2ZW50OiBib29sZWFufX0gb3B0c1xyXG4gICAqL1xyXG4gIHNldENvbmZpZyh2YWx1ZSwgb3B0cyA9IHsgZW1pdEV2ZW50OiB0cnVlIH0pOiB2b2lkIHtcclxuICAgIC8vIEdldCB0aGUgdmFsdWUgZnJvbSB0aGUgYmVoYXZpb3Igc3ViamVjdFxyXG4gICAgbGV0IGNvbmZpZyA9IHRoaXMuX2NvbmZpZ1N1YmplY3QuZ2V0VmFsdWUoKTtcclxuXHJcbiAgICAvLyBNZXJnZSB0aGUgbmV3IGNvbmZpZ1xyXG4gICAgY29uZmlnID0gXy5tZXJnZSh7fSwgY29uZmlnLCB2YWx1ZSk7XHJcblxyXG4gICAgLy8gSWYgZW1pdEV2ZW50IG9wdGlvbiBpcyB0cnVlLi4uXHJcbiAgICBpZiAob3B0cy5lbWl0RXZlbnQgPT09IHRydWUpIHtcclxuICAgICAgLy8gTm90aWZ5IHRoZSBvYnNlcnZlcnNcclxuICAgICAgdGhpcy5fY29uZmlnU3ViamVjdC5uZXh0KGNvbmZpZyk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgY29uZmlnXHJcbiAgICpcclxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAqL1xyXG4gIGdldENvbmZpZygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2NvbmZpZ1N1YmplY3QuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBSZXNldCB0byB0aGUgZGVmYXVsdCBjb25maWdcclxuICAgKi9cclxuICByZXNldFRvRGVmYXVsdHMoKTogdm9pZCB7XHJcbiAgICAvLyBTZXQgdGhlIGNvbmZpZyBmcm9tIHRoZSBkZWZhdWx0IGNvbmZpZ1xyXG4gICAgdGhpcy5fY29uZmlnU3ViamVjdC5uZXh0KF8uY2xvbmVEZWVwKHRoaXMuX2RlZmF1bHRDb25maWcpKTtcclxuICB9XHJcbn1cclxuIl19