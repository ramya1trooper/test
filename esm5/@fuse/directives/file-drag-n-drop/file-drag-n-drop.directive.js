import { Directive, HostListener, Output, EventEmitter } from '@angular/core';
var FileDragNDropDirective = /** @class */ (function () {
    // @HostBinding('style.background') public background = '#eee';
    // @HostBinding('style.border') public borderStyle = '2px dashed';
    // @HostBinding('style.border-color') public borderColor = '#696D7D';
    // @HostBinding('style.border-radius') public borderRadius = '5px';
    function FileDragNDropDirective() {
        //@Input() private allowed_extensions : Array<string> = ['png', 'jpg', 'bmp'];
        //@Output() public filesChangeEmiter : EventEmitter<File[]> = new EventEmitter();
        //@Output() private filesInvalidEmiter : EventEmitter<File[]> = new EventEmitter();
        this.onFileDropped = new EventEmitter();
    }
    FileDragNDropDirective.prototype.onDragOver = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = 'lightgray';
        // this.borderColor = 'cadetblue';
        // this.borderStyle = '3px solid';
    };
    FileDragNDropDirective.prototype.onDragLeave = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#eee';
        // this.borderColor = '#696D7D';
        // this.borderStyle = '2px dashed';
    };
    FileDragNDropDirective.prototype.onDrop = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#eee';
        // this.borderColor = '#696D7D';
        // this.borderStyle = '2px dashed';
        var files = evt.dataTransfer.files;
        // let valid_files : Array<File> = files;
        // this.filesChangeEmiter.emit(valid_files);
        if (files.length > 0) {
            this.onFileDropped.emit(files);
        }
    };
    FileDragNDropDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[fileDragDrop]'
                },] }
    ];
    /** @nocollapse */
    FileDragNDropDirective.ctorParameters = function () { return []; };
    FileDragNDropDirective.propDecorators = {
        onFileDropped: [{ type: Output }],
        onDragOver: [{ type: HostListener, args: ['dragover', ['$event'],] }],
        onDragLeave: [{ type: HostListener, args: ['dragleave', ['$event'],] }],
        onDrop: [{ type: HostListener, args: ['drop', ['$event'],] }]
    };
    return FileDragNDropDirective;
}());
export { FileDragNDropDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS1kcmFnLW4tZHJvcC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvZGlyZWN0aXZlcy9maWxlLWRyYWctbi1kcm9wL2ZpbGUtZHJhZy1uLWRyb3AuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFlLE1BQU0sRUFBRSxZQUFZLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFFbEc7SUFVQywrREFBK0Q7SUFDL0Qsa0VBQWtFO0lBQ2xFLHFFQUFxRTtJQUNyRSxtRUFBbUU7SUFFbEU7UUFWQSw4RUFBOEU7UUFDNUUsaUZBQWlGO1FBQ25GLG1GQUFtRjtRQUN6RSxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFPbEMsQ0FBQztJQUU0QiwyQ0FBVSxHQUF2RCxVQUF3RCxHQUFHO1FBQ3pELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsaUNBQWlDO1FBQ2pDLGtDQUFrQztRQUNsQyxrQ0FBa0M7SUFDcEMsQ0FBQztJQUU2Qyw0Q0FBVyxHQUF6RCxVQUEwRCxHQUFHO1FBQzNELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsNEJBQTRCO1FBQzVCLGdDQUFnQztRQUNoQyxtQ0FBbUM7SUFDckMsQ0FBQztJQUV3Qyx1Q0FBTSxHQUEvQyxVQUFnRCxHQUFHO1FBQ2pELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsNEJBQTRCO1FBQzVCLGdDQUFnQztRQUNoQyxtQ0FBbUM7UUFDbkMsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFDbkMseUNBQXlDO1FBQ3pDLDRDQUE0QztRQUM1QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQy9CO0lBQ0gsQ0FBQzs7Z0JBN0NGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2lCQUMzQjs7Ozs7Z0NBTUUsTUFBTTs2QkFTTixZQUFZLFNBQUMsVUFBVSxFQUFFLENBQUMsUUFBUSxDQUFDOzhCQVFuQyxZQUFZLFNBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDO3lCQVFwQyxZQUFZLFNBQUMsTUFBTSxFQUFFLENBQUMsUUFBUSxDQUFDOztJQWFsQyw2QkFBQztDQUFBLEFBOUNELElBOENDO1NBMUNZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBIb3N0QmluZGluZywgT3V0cHV0LCBFdmVudEVtaXR0ZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICBzZWxlY3RvcjogJ1tmaWxlRHJhZ0Ryb3BdJ1xyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEZpbGVEcmFnTkRyb3BEaXJlY3RpdmUge1xyXG4gIC8vQElucHV0KCkgcHJpdmF0ZSBhbGxvd2VkX2V4dGVuc2lvbnMgOiBBcnJheTxzdHJpbmc+ID0gWydwbmcnLCAnanBnJywgJ2JtcCddO1xyXG4gICAgLy9AT3V0cHV0KCkgcHVibGljIGZpbGVzQ2hhbmdlRW1pdGVyIDogRXZlbnRFbWl0dGVyPEZpbGVbXT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgLy9AT3V0cHV0KCkgcHJpdmF0ZSBmaWxlc0ludmFsaWRFbWl0ZXIgOiBFdmVudEVtaXR0ZXI8RmlsZVtdPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuICBAT3V0cHV0KCkgb25GaWxlRHJvcHBlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuIC8vIEBIb3N0QmluZGluZygnc3R5bGUuYmFja2dyb3VuZCcpIHB1YmxpYyBiYWNrZ3JvdW5kID0gJyNlZWUnO1xyXG4gLy8gQEhvc3RCaW5kaW5nKCdzdHlsZS5ib3JkZXInKSBwdWJsaWMgYm9yZGVyU3R5bGUgPSAnMnB4IGRhc2hlZCc7XHJcbiAvLyBASG9zdEJpbmRpbmcoJ3N0eWxlLmJvcmRlci1jb2xvcicpIHB1YmxpYyBib3JkZXJDb2xvciA9ICcjNjk2RDdEJztcclxuIC8vIEBIb3N0QmluZGluZygnc3R5bGUuYm9yZGVyLXJhZGl1cycpIHB1YmxpYyBib3JkZXJSYWRpdXMgPSAnNXB4JztcclxuXHJcbiAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgQEhvc3RMaXN0ZW5lcignZHJhZ292ZXInLCBbJyRldmVudCddKSBwdWJsaWMgb25EcmFnT3ZlcihldnQpe1xyXG4gICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAvLyB0aGlzLmJhY2tncm91bmQgPSAnbGlnaHRncmF5JztcclxuICAgIC8vIHRoaXMuYm9yZGVyQ29sb3IgPSAnY2FkZXRibHVlJztcclxuICAgIC8vIHRoaXMuYm9yZGVyU3R5bGUgPSAnM3B4IHNvbGlkJztcclxuICB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RyYWdsZWF2ZScsIFsnJGV2ZW50J10pIHB1YmxpYyBvbkRyYWdMZWF2ZShldnQpe1xyXG4gICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAvLyB0aGlzLmJhY2tncm91bmQgPSAnI2VlZSc7XHJcbiAgICAvLyB0aGlzLmJvcmRlckNvbG9yID0gJyM2OTZEN0QnO1xyXG4gICAgLy8gdGhpcy5ib3JkZXJTdHlsZSA9ICcycHggZGFzaGVkJztcclxuICB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2Ryb3AnLCBbJyRldmVudCddKSBwdWJsaWMgb25Ecm9wKGV2dCl7XHJcbiAgICBldnQucHJldmVudERlZmF1bHQoKTtcclxuICAgIGV2dC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgIC8vIHRoaXMuYmFja2dyb3VuZCA9ICcjZWVlJztcclxuICAgIC8vIHRoaXMuYm9yZGVyQ29sb3IgPSAnIzY5NkQ3RCc7XHJcbiAgICAvLyB0aGlzLmJvcmRlclN0eWxlID0gJzJweCBkYXNoZWQnO1xyXG4gICAgbGV0IGZpbGVzID0gZXZ0LmRhdGFUcmFuc2Zlci5maWxlcztcclxuICAgIC8vIGxldCB2YWxpZF9maWxlcyA6IEFycmF5PEZpbGU+ID0gZmlsZXM7XHJcbiAgICAvLyB0aGlzLmZpbGVzQ2hhbmdlRW1pdGVyLmVtaXQodmFsaWRfZmlsZXMpO1xyXG4gICAgaWYgKGZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5vbkZpbGVEcm9wcGVkLmVtaXQoZmlsZXMpXHJcbiAgICB9XHJcbiAgfVxyXG59Il19