import { Directive, Input, HostListener, HostBinding } from "@angular/core";
import { MatSidenav } from "@angular/material";
import { ObservableMedia } from "@angular/flex-layout";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { FuseMatchMediaService } from "../../../@fuse/services/match-media.service";
import { FuseMatSidenavHelperService } from "../../../@fuse/directives/fuse-mat-sidenav/fuse-mat-sidenav.service";
var FuseMatSidenavHelperDirective = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {FuseMatSidenavHelperService} _fuseMatSidenavHelperService
     * @param {MatSidenav} _matSidenav
     * @param {ObservableMedia} _observableMedia
     */
    function FuseMatSidenavHelperDirective(_fuseMatchMediaService, _fuseMatSidenavHelperService, _matSidenav, _observableMedia) {
        this._fuseMatchMediaService = _fuseMatchMediaService;
        this._fuseMatSidenavHelperService = _fuseMatSidenavHelperService;
        this._matSidenav = _matSidenav;
        this._observableMedia = _observableMedia;
        // Set the defaults
        this.isLockedOpen = true;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    FuseMatSidenavHelperDirective.prototype.ngOnInit = function () {
        var _this = this;
        // Register the sidenav to the service
        this._fuseMatSidenavHelperService.setSidenav(this.fuseMatSidenavHelper, this._matSidenav);
        if (this._observableMedia.isActive(this.matIsLockedOpen)) {
            this.isLockedOpen = true;
            this._matSidenav.mode = "side";
            this._matSidenav.toggle(true);
        }
        else {
            this.isLockedOpen = false;
            this._matSidenav.mode = "over";
            this._matSidenav.toggle(false);
        }
        this._fuseMatchMediaService.onMediaChange
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            if (_this._observableMedia.isActive(_this.matIsLockedOpen)) {
                _this.isLockedOpen = true;
                _this._matSidenav.mode = "side";
                _this._matSidenav.toggle(true);
            }
            else {
                _this.isLockedOpen = false;
                _this._matSidenav.mode = "over";
                _this._matSidenav.toggle(false);
            }
        });
    };
    /**
     * On destroy
     */
    FuseMatSidenavHelperDirective.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    FuseMatSidenavHelperDirective.decorators = [
        { type: Directive, args: [{
                    selector: "[fuseMatSidenavHelper]"
                },] }
    ];
    /** @nocollapse */
    FuseMatSidenavHelperDirective.ctorParameters = function () { return [
        { type: FuseMatchMediaService },
        { type: FuseMatSidenavHelperService },
        { type: MatSidenav },
        { type: ObservableMedia }
    ]; };
    FuseMatSidenavHelperDirective.propDecorators = {
        isLockedOpen: [{ type: HostBinding, args: ["class.mat-is-locked-open",] }],
        fuseMatSidenavHelper: [{ type: Input }],
        matIsLockedOpen: [{ type: Input }]
    };
    return FuseMatSidenavHelperDirective;
}());
export { FuseMatSidenavHelperDirective };
var FuseMatSidenavTogglerDirective = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {FuseMatSidenavHelperService} _fuseMatSidenavHelperService
     */
    function FuseMatSidenavTogglerDirective(_fuseMatSidenavHelperService) {
        this._fuseMatSidenavHelperService = _fuseMatSidenavHelperService;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * On click
     */
    FuseMatSidenavTogglerDirective.prototype.onClick = function () {
        this._fuseMatSidenavHelperService
            .getSidenav(this.fuseMatSidenavToggler)
            .toggle();
    };
    FuseMatSidenavTogglerDirective.decorators = [
        { type: Directive, args: [{
                    selector: "[fuseMatSidenavToggler]"
                },] }
    ];
    /** @nocollapse */
    FuseMatSidenavTogglerDirective.ctorParameters = function () { return [
        { type: FuseMatSidenavHelperService }
    ]; };
    FuseMatSidenavTogglerDirective.propDecorators = {
        fuseMatSidenavToggler: [{ type: Input }],
        onClick: [{ type: HostListener, args: ["click",] }]
    };
    return FuseMatSidenavTogglerDirective;
}());
export { FuseMatSidenavTogglerDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVzZS1tYXQtc2lkZW5hdi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvZGlyZWN0aXZlcy9mdXNlLW1hdC1zaWRlbmF2L2Z1c2UtbWF0LXNpZGVuYXYuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBQ1QsS0FBSyxFQUVMLFlBQVksRUFFWixXQUFXLEVBQ1osTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNwRixPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxxRUFBcUUsQ0FBQztBQUVsSDtJQWdCRTs7Ozs7OztPQU9HO0lBQ0gsdUNBQ1Usc0JBQTZDLEVBQzdDLDRCQUF5RCxFQUN6RCxXQUF1QixFQUN2QixnQkFBaUM7UUFIakMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF1QjtRQUM3QyxpQ0FBNEIsR0FBNUIsNEJBQTRCLENBQTZCO1FBQ3pELGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFFekMsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1FBRXpCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsZ0RBQVEsR0FBUjtRQUFBLGlCQThCQztRQTdCQyxzQ0FBc0M7UUFDdEMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLFVBQVUsQ0FDMUMsSUFBSSxDQUFDLG9CQUFvQixFQUN6QixJQUFJLENBQUMsV0FBVyxDQUNqQixDQUFDO1FBRUYsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUN4RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7WUFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDL0I7YUFBTTtZQUNMLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztZQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNoQztRQUVELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhO2FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQztZQUNULElBQUksS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxLQUFJLENBQUMsZUFBZSxDQUFDLEVBQUU7Z0JBQ3hELEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2dCQUN6QixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7Z0JBQy9CLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQy9CO2lCQUFNO2dCQUNMLEtBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO2dCQUMxQixLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7Z0JBQy9CLEtBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxtREFBVyxHQUFYO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDOztnQkFuRkYsU0FBUyxTQUFDO29CQUNULFFBQVEsRUFBRSx3QkFBd0I7aUJBQ25DOzs7O2dCQUxRLHFCQUFxQjtnQkFDckIsMkJBQTJCO2dCQU4zQixVQUFVO2dCQUNWLGVBQWU7OzsrQkFXckIsV0FBVyxTQUFDLDBCQUEwQjt1Q0FHdEMsS0FBSztrQ0FHTCxLQUFLOztJQTBFUixvQ0FBQztDQUFBLEFBcEZELElBb0ZDO1NBakZZLDZCQUE2QjtBQW1GMUM7SUFPRTs7OztPQUlHO0lBQ0gsd0NBQ1UsNEJBQXlEO1FBQXpELGlDQUE0QixHQUE1Qiw0QkFBNEIsQ0FBNkI7SUFDaEUsQ0FBQztJQUVKLHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOztPQUVHO0lBRUgsZ0RBQU8sR0FEUDtRQUVFLElBQUksQ0FBQyw0QkFBNEI7YUFDOUIsVUFBVSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQzthQUN0QyxNQUFNLEVBQUUsQ0FBQztJQUNkLENBQUM7O2dCQTVCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHlCQUF5QjtpQkFDcEM7Ozs7Z0JBMUZRLDJCQUEyQjs7O3dDQTRGakMsS0FBSzswQkFtQkwsWUFBWSxTQUFDLE9BQU87O0lBTXZCLHFDQUFDO0NBQUEsQUE3QkQsSUE2QkM7U0ExQlksOEJBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBEaXJlY3RpdmUsXHJcbiAgSW5wdXQsXHJcbiAgT25Jbml0LFxyXG4gIEhvc3RMaXN0ZW5lcixcclxuICBPbkRlc3Ryb3ksXHJcbiAgSG9zdEJpbmRpbmdcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBNYXRTaWRlbmF2IH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsXCI7XHJcbmltcG9ydCB7IE9ic2VydmFibGVNZWRpYSB9IGZyb20gXCJAYW5ndWxhci9mbGV4LWxheW91dFwiO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlTWF0Y2hNZWRpYVNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2Uvc2VydmljZXMvbWF0Y2gtbWVkaWEuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBGdXNlTWF0U2lkZW5hdkhlbHBlclNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2UvZGlyZWN0aXZlcy9mdXNlLW1hdC1zaWRlbmF2L2Z1c2UtbWF0LXNpZGVuYXYuc2VydmljZVwiO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6IFwiW2Z1c2VNYXRTaWRlbmF2SGVscGVyXVwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlTWF0U2lkZW5hdkhlbHBlckRpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICBASG9zdEJpbmRpbmcoXCJjbGFzcy5tYXQtaXMtbG9ja2VkLW9wZW5cIilcclxuICBpc0xvY2tlZE9wZW46IGJvb2xlYW47XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgZnVzZU1hdFNpZGVuYXZIZWxwZXI6IHN0cmluZztcclxuXHJcbiAgQElucHV0KClcclxuICBtYXRJc0xvY2tlZE9wZW46IHN0cmluZztcclxuXHJcbiAgLy8gUHJpdmF0ZVxyXG4gIHByaXZhdGUgX3Vuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge0Z1c2VNYXRjaE1lZGlhU2VydmljZX0gX2Z1c2VNYXRjaE1lZGlhU2VydmljZVxyXG4gICAqIEBwYXJhbSB7RnVzZU1hdFNpZGVuYXZIZWxwZXJTZXJ2aWNlfSBfZnVzZU1hdFNpZGVuYXZIZWxwZXJTZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtNYXRTaWRlbmF2fSBfbWF0U2lkZW5hdlxyXG4gICAqIEBwYXJhbSB7T2JzZXJ2YWJsZU1lZGlhfSBfb2JzZXJ2YWJsZU1lZGlhXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9mdXNlTWF0Y2hNZWRpYVNlcnZpY2U6IEZ1c2VNYXRjaE1lZGlhU2VydmljZSxcclxuICAgIHByaXZhdGUgX2Z1c2VNYXRTaWRlbmF2SGVscGVyU2VydmljZTogRnVzZU1hdFNpZGVuYXZIZWxwZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfbWF0U2lkZW5hdjogTWF0U2lkZW5hdixcclxuICAgIHByaXZhdGUgX29ic2VydmFibGVNZWRpYTogT2JzZXJ2YWJsZU1lZGlhXHJcbiAgKSB7XHJcbiAgICAvLyBTZXQgdGhlIGRlZmF1bHRzXHJcbiAgICB0aGlzLmlzTG9ja2VkT3BlbiA9IHRydWU7XHJcblxyXG4gICAgLy8gU2V0IHRoZSBwcml2YXRlIGRlZmF1bHRzXHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogT24gaW5pdFxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgLy8gUmVnaXN0ZXIgdGhlIHNpZGVuYXYgdG8gdGhlIHNlcnZpY2VcclxuICAgIHRoaXMuX2Z1c2VNYXRTaWRlbmF2SGVscGVyU2VydmljZS5zZXRTaWRlbmF2KFxyXG4gICAgICB0aGlzLmZ1c2VNYXRTaWRlbmF2SGVscGVyLFxyXG4gICAgICB0aGlzLl9tYXRTaWRlbmF2XHJcbiAgICApO1xyXG5cclxuICAgIGlmICh0aGlzLl9vYnNlcnZhYmxlTWVkaWEuaXNBY3RpdmUodGhpcy5tYXRJc0xvY2tlZE9wZW4pKSB7XHJcbiAgICAgIHRoaXMuaXNMb2NrZWRPcGVuID0gdHJ1ZTtcclxuICAgICAgdGhpcy5fbWF0U2lkZW5hdi5tb2RlID0gXCJzaWRlXCI7XHJcbiAgICAgIHRoaXMuX21hdFNpZGVuYXYudG9nZ2xlKHRydWUpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5pc0xvY2tlZE9wZW4gPSBmYWxzZTtcclxuICAgICAgdGhpcy5fbWF0U2lkZW5hdi5tb2RlID0gXCJvdmVyXCI7XHJcbiAgICAgIHRoaXMuX21hdFNpZGVuYXYudG9nZ2xlKGZhbHNlKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLl9mdXNlTWF0Y2hNZWRpYVNlcnZpY2Uub25NZWRpYUNoYW5nZVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5fb2JzZXJ2YWJsZU1lZGlhLmlzQWN0aXZlKHRoaXMubWF0SXNMb2NrZWRPcGVuKSkge1xyXG4gICAgICAgICAgdGhpcy5pc0xvY2tlZE9wZW4gPSB0cnVlO1xyXG4gICAgICAgICAgdGhpcy5fbWF0U2lkZW5hdi5tb2RlID0gXCJzaWRlXCI7XHJcbiAgICAgICAgICB0aGlzLl9tYXRTaWRlbmF2LnRvZ2dsZSh0cnVlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5pc0xvY2tlZE9wZW4gPSBmYWxzZTtcclxuICAgICAgICAgIHRoaXMuX21hdFNpZGVuYXYubW9kZSA9IFwib3ZlclwiO1xyXG4gICAgICAgICAgdGhpcy5fbWF0U2lkZW5hdi50b2dnbGUoZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBPbiBkZXN0cm95XHJcbiAgICovXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAvLyBVbnN1YnNjcmliZSBmcm9tIGFsbCBzdWJzY3JpcHRpb25zXHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5uZXh0KCk7XHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5jb21wbGV0ZSgpO1xyXG4gIH1cclxufVxyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6IFwiW2Z1c2VNYXRTaWRlbmF2VG9nZ2xlcl1cIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU1hdFNpZGVuYXZUb2dnbGVyRGlyZWN0aXZlIHtcclxuICBASW5wdXQoKVxyXG4gIGZ1c2VNYXRTaWRlbmF2VG9nZ2xlcjogc3RyaW5nO1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtGdXNlTWF0U2lkZW5hdkhlbHBlclNlcnZpY2V9IF9mdXNlTWF0U2lkZW5hdkhlbHBlclNlcnZpY2VcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VNYXRTaWRlbmF2SGVscGVyU2VydmljZTogRnVzZU1hdFNpZGVuYXZIZWxwZXJTZXJ2aWNlXHJcbiAgKSB7fVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBPbiBjbGlja1xyXG4gICAqL1xyXG4gIEBIb3N0TGlzdGVuZXIoXCJjbGlja1wiKVxyXG4gIG9uQ2xpY2soKTogdm9pZCB7XHJcbiAgICB0aGlzLl9mdXNlTWF0U2lkZW5hdkhlbHBlclNlcnZpY2VcclxuICAgICAgLmdldFNpZGVuYXYodGhpcy5mdXNlTWF0U2lkZW5hdlRvZ2dsZXIpXHJcbiAgICAgIC50b2dnbGUoKTtcclxuICB9XHJcbn1cclxuIl19