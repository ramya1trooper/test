import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var FuseMatSidenavHelperService = /** @class */ (function () {
    /**
     * Constructor
     */
    function FuseMatSidenavHelperService() {
        this.sidenavInstances = [];
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Set sidenav
     *
     * @param id
     * @param instance
     */
    FuseMatSidenavHelperService.prototype.setSidenav = function (id, instance) {
        this.sidenavInstances[id] = instance;
    };
    /**
     * Get sidenav
     *
     * @param id
     * @returns {any}
     */
    FuseMatSidenavHelperService.prototype.getSidenav = function (id) {
        return this.sidenavInstances[id];
    };
    FuseMatSidenavHelperService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    FuseMatSidenavHelperService.ctorParameters = function () { return []; };
    FuseMatSidenavHelperService.ngInjectableDef = i0.defineInjectable({ factory: function FuseMatSidenavHelperService_Factory() { return new FuseMatSidenavHelperService(); }, token: FuseMatSidenavHelperService, providedIn: "root" });
    return FuseMatSidenavHelperService;
}());
export { FuseMatSidenavHelperService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVzZS1tYXQtc2lkZW5hdi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2RpcmVjdGl2ZXMvZnVzZS1tYXQtc2lkZW5hdi9mdXNlLW1hdC1zaWRlbmF2LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFHM0M7SUFPSTs7T0FFRztJQUNIO1FBRUksSUFBSSxDQUFDLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLGNBQWM7SUFDZCx3R0FBd0c7SUFFeEc7Ozs7O09BS0c7SUFDSCxnREFBVSxHQUFWLFVBQVcsRUFBRSxFQUFFLFFBQVE7UUFFbkIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxHQUFHLFFBQVEsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxnREFBVSxHQUFWLFVBQVcsRUFBRTtRQUVULE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7O2dCQXZDSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7OztzQ0FMRDtDQTJDQyxBQXhDRCxJQXdDQztTQXJDWSwyQkFBMkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFNpZGVuYXYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VNYXRTaWRlbmF2SGVscGVyU2VydmljZVxyXG57XHJcbiAgICBzaWRlbmF2SW5zdGFuY2VzOiBNYXRTaWRlbmF2W107XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcigpXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5zaWRlbmF2SW5zdGFuY2VzID0gW107XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgQWNjZXNzb3JzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0IHNpZGVuYXZcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gaWRcclxuICAgICAqIEBwYXJhbSBpbnN0YW5jZVxyXG4gICAgICovXHJcbiAgICBzZXRTaWRlbmF2KGlkLCBpbnN0YW5jZSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLnNpZGVuYXZJbnN0YW5jZXNbaWRdID0gaW5zdGFuY2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgc2lkZW5hdlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBpZFxyXG4gICAgICogQHJldHVybnMge2FueX1cclxuICAgICAqL1xyXG4gICAgZ2V0U2lkZW5hdihpZCk6IGFueVxyXG4gICAge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNpZGVuYXZJbnN0YW5jZXNbaWRdO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==