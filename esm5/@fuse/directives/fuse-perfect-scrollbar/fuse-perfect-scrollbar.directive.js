import * as tslib_1 from "tslib";
import { Directive, ElementRef, HostListener, Input } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { Platform } from "@angular/cdk/platform";
import { Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import PerfectScrollbar from "perfect-scrollbar";
import * as _ from "lodash";
import { FuseConfigService } from "../../../@fuse/services/config.service";
var FusePerfectScrollbarDirective = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {ElementRef} elementRef
     * @param {FuseConfigService} _fuseConfigService
     * @param {Platform} _platform
     * @param {Router} _router
     */
    function FusePerfectScrollbarDirective(elementRef, _fuseConfigService, _platform, _router) {
        this.elementRef = elementRef;
        this._fuseConfigService = _fuseConfigService;
        this._platform = _platform;
        this._router = _router;
        // Set the defaults
        this.isInitialized = false;
        this.isMobile = false;
        // Set the private defaults
        this._enabled = false;
        this._debouncedUpdate = _.debounce(this.update, 150);
        this._options = {
            updateOnRouteChange: false
        };
        this._unsubscribeAll = new Subject();
    }
    Object.defineProperty(FusePerfectScrollbarDirective.prototype, "fusePerfectScrollbarOptions", {
        get: function () {
            // Return the options
            return this._options;
        },
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        /**
         * Perfect Scrollbar options
         *
         * @param value
         */
        set: function (value) {
            // Merge the options
            this._options = _.merge({}, this._options, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FusePerfectScrollbarDirective.prototype, "enabled", {
        get: function () {
            // Return the enabled status
            return this._enabled;
        },
        /**
         * Is enabled
         *
         * @param {boolean | ""} value
         */
        set: function (value) {
            // If nothing is provided with the directive (empty string),
            // we will take that as a true
            if (value === "") {
                value = true;
            }
            // Return, if both values are the same
            if (this.enabled === value) {
                return;
            }
            // Store the value
            this._enabled = value;
            // If enabled...
            if (this.enabled) {
                // Init the directive
                this._init();
            }
            else {
                // Otherwise destroy it
                this._destroy();
            }
        },
        enumerable: true,
        configurable: true
    });
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * After view init
     */
    FusePerfectScrollbarDirective.prototype.ngAfterViewInit = function () {
        var _this = this;
        // Check if scrollbars enabled or not from the main config
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function (settings) {
            _this.enabled = settings.customScrollbars;
        });
        // Scroll to the top on every route change
        if (this.fusePerfectScrollbarOptions.updateOnRouteChange) {
            this._router.events
                .pipe(takeUntil(this._unsubscribeAll), filter(function (event) { return event instanceof NavigationEnd; }))
                .subscribe(function () {
                setTimeout(function () {
                    _this.scrollToTop();
                    _this.update();
                }, 0);
            });
        }
    };
    /**
     * On destroy
     */
    FusePerfectScrollbarDirective.prototype.ngOnDestroy = function () {
        this._destroy();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    FusePerfectScrollbarDirective.prototype._init = function () {
        // Return, if already initialized
        if (this.isInitialized) {
            return;
        }
        // Check if is mobile
        if (this._platform.ANDROID || this._platform.IOS) {
            this.isMobile = true;
        }
        // Return if it's mobile
        if (this.isMobile) {
            // Return...
            return;
        }
        // Set as initialized
        this.isInitialized = true;
        // Initialize the perfect-scrollbar
        this.ps = new PerfectScrollbar(this.elementRef.nativeElement, tslib_1.__assign({}, this.fusePerfectScrollbarOptions));
        // Unbind 'keydown' events of PerfectScrollbar since it causes an extremely
        // high CPU usage on Angular Material inputs.
        // Loop through all the event elements of this PerfectScrollbar instance
        this.ps.event.eventElements.forEach(function (eventElement) {
            // If we hit to the element with a 'keydown' event...
            if (typeof eventElement.handlers["keydown"] !== "undefined") {
                // Unbind it
                eventElement.element.removeEventListener("keydown", eventElement.handlers["keydown"][0]);
            }
        });
    };
    /**
     * Destroy
     *
     * @private
     */
    FusePerfectScrollbarDirective.prototype._destroy = function () {
        if (!this.isInitialized || !this.ps) {
            return;
        }
        // Destroy the perfect-scrollbar
        this.ps.destroy();
        // Clean up
        this.ps = null;
        this.isInitialized = false;
    };
    /**
     * Update scrollbars on window resize
     *
     * @private
     */
    FusePerfectScrollbarDirective.prototype._updateOnResize = function () {
        this._debouncedUpdate();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Document click
     *
     * @param {Event} event
     */
    FusePerfectScrollbarDirective.prototype.documentClick = function (event) {
        if (!this.isInitialized || !this.ps) {
            return;
        }
        // Update the scrollbar on document click..
        // This isn't the most elegant solution but there is no other way
        // of knowing when the contents of the scrollable container changes.
        // Therefore, we update scrollbars on every document click.
        this.ps.update();
    };
    /**
     * Update the scrollbar
     */
    FusePerfectScrollbarDirective.prototype.update = function () {
        if (!this.isInitialized) {
            return;
        }
        // Update the perfect-scrollbar
        this.ps.update();
    };
    /**
     * Destroy the scrollbar
     */
    FusePerfectScrollbarDirective.prototype.destroy = function () {
        this.ngOnDestroy();
    };
    /**
     * Scroll to X
     *
     * @param {number} x
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.scrollToX = function (x, speed) {
        this.animateScrolling("scrollLeft", x, speed);
    };
    /**
     * Scroll to Y
     *
     * @param {number} y
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.scrollToY = function (y, speed) {
        this.animateScrolling("scrollTop", y, speed);
    };
    /**
     * Scroll to top
     *
     * @param {number} offset
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.scrollToTop = function (offset, speed) {
        this.animateScrolling("scrollTop", offset || 0, speed);
    };
    /**
     * Scroll to left
     *
     * @param {number} offset
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.scrollToLeft = function (offset, speed) {
        this.animateScrolling("scrollLeft", offset || 0, speed);
    };
    /**
     * Scroll to right
     *
     * @param {number} offset
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.scrollToRight = function (offset, speed) {
        var width = this.elementRef.nativeElement.scrollWidth;
        this.animateScrolling("scrollLeft", width - (offset || 0), speed);
    };
    /**
     * Scroll to bottom
     *
     * @param {number} offset
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.scrollToBottom = function (offset, speed) {
        var height = this.elementRef.nativeElement.scrollHeight;
        this.animateScrolling("scrollTop", height - (offset || 0), speed);
    };
    /**
     * Animate scrolling
     *
     * @param {string} target
     * @param {number} value
     * @param {number} speed
     */
    FusePerfectScrollbarDirective.prototype.animateScrolling = function (target, value, speed) {
        var _this = this;
        if (!speed) {
            this.elementRef.nativeElement[target] = value;
            // PS has weird event sending order, this is a workaround for that
            this.update();
            this.update();
        }
        else if (value !== this.elementRef.nativeElement[target]) {
            var newValue_1 = 0;
            var scrollCount_1 = 0;
            var oldTimestamp_1 = performance.now();
            var oldValue_1 = this.elementRef.nativeElement[target];
            var cosParameter_1 = (oldValue_1 - value) / 2;
            var step_1 = function (newTimestamp) {
                scrollCount_1 += Math.PI / (speed / (newTimestamp - oldTimestamp_1));
                newValue_1 = Math.round(value + cosParameter_1 + cosParameter_1 * Math.cos(scrollCount_1));
                // Only continue animation if scroll position has not changed
                if (_this.elementRef.nativeElement[target] === oldValue_1) {
                    if (scrollCount_1 >= Math.PI) {
                        _this.elementRef.nativeElement[target] = value;
                        // PS has weird event sending order, this is a workaround for that
                        _this.update();
                        _this.update();
                    }
                    else {
                        _this.elementRef.nativeElement[target] = oldValue_1 = newValue_1;
                        oldTimestamp_1 = newTimestamp;
                        window.requestAnimationFrame(step_1);
                    }
                }
            };
            window.requestAnimationFrame(step_1);
        }
    };
    FusePerfectScrollbarDirective.decorators = [
        { type: Directive, args: [{
                    selector: "[fusePerfectScrollbar]"
                },] }
    ];
    /** @nocollapse */
    FusePerfectScrollbarDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: FuseConfigService },
        { type: Platform },
        { type: Router }
    ]; };
    FusePerfectScrollbarDirective.propDecorators = {
        fusePerfectScrollbarOptions: [{ type: Input }],
        enabled: [{ type: Input, args: ["fusePerfectScrollbar",] }],
        _updateOnResize: [{ type: HostListener, args: ["window:resize",] }],
        documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }]
    };
    return FusePerfectScrollbarDirective;
}());
export { FusePerfectScrollbarDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVzZS1wZXJmZWN0LXNjcm9sbGJhci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvZGlyZWN0aXZlcy9mdXNlLXBlcmZlY3Qtc2Nyb2xsYmFyL2Z1c2UtcGVyZmVjdC1zY3JvbGxiYXIuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBRUwsU0FBUyxFQUNULFVBQVUsRUFDVixZQUFZLEVBQ1osS0FBSyxFQUVOLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ2pELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNuRCxPQUFPLGdCQUFnQixNQUFNLG1CQUFtQixDQUFDO0FBQ2pELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRTNFO0lBY0U7Ozs7Ozs7T0FPRztJQUNILHVDQUNTLFVBQXNCLEVBQ3JCLGtCQUFxQyxFQUNyQyxTQUFtQixFQUNuQixPQUFlO1FBSGhCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDckIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQyxjQUFTLEdBQVQsU0FBUyxDQUFVO1FBQ25CLFlBQU8sR0FBUCxPQUFPLENBQVE7UUFFdkIsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBRXRCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxRQUFRLEdBQUc7WUFDZCxtQkFBbUIsRUFBRSxLQUFLO1NBQzNCLENBQUM7UUFDRixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQVdELHNCQUNJLHNFQUEyQjthQUsvQjtZQUNFLHFCQUFxQjtZQUNyQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDdkIsQ0FBQztRQWxCRCx3R0FBd0c7UUFDeEcsY0FBYztRQUNkLHdHQUF3RztRQUV4Rzs7OztXQUlHO2FBQ0gsVUFDZ0MsS0FBSztZQUNuQyxvQkFBb0I7WUFDcEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3BELENBQUM7OztPQUFBO0lBWUQsc0JBQ0ksa0RBQU87YUF5Qlg7WUFDRSw0QkFBNEI7WUFDNUIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3ZCLENBQUM7UUFsQ0Q7Ozs7V0FJRzthQUNILFVBQ1ksS0FBbUI7WUFDN0IsNERBQTREO1lBQzVELDhCQUE4QjtZQUM5QixJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7Z0JBQ2hCLEtBQUssR0FBRyxJQUFJLENBQUM7YUFDZDtZQUVELHNDQUFzQztZQUN0QyxJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUMxQixPQUFPO2FBQ1I7WUFFRCxrQkFBa0I7WUFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7WUFFdEIsZ0JBQWdCO1lBQ2hCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDaEIscUJBQXFCO2dCQUNyQixJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDZDtpQkFBTTtnQkFDTCx1QkFBdUI7Z0JBQ3ZCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUNqQjtRQUNILENBQUM7OztPQUFBO0lBT0Qsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCx1REFBZSxHQUFmO1FBQUEsaUJBc0JDO1FBckJDLDBEQUEwRDtRQUMxRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTTthQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNyQyxTQUFTLENBQUMsVUFBQSxRQUFRO1lBQ2pCLEtBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDLGdCQUFnQixDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO1FBRUwsMENBQTBDO1FBQzFDLElBQUksSUFBSSxDQUFDLDJCQUEyQixDQUFDLG1CQUFtQixFQUFFO1lBQ3hELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTtpQkFDaEIsSUFBSSxDQUNILFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQy9CLE1BQU0sQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssWUFBWSxhQUFhLEVBQTlCLENBQThCLENBQUMsQ0FDaEQ7aUJBQ0EsU0FBUyxDQUFDO2dCQUNULFVBQVUsQ0FBQztvQkFDVCxLQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ25CLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDaEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNILG1EQUFXLEdBQVg7UUFDRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILDZDQUFLLEdBQUw7UUFDRSxpQ0FBaUM7UUFDakMsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLE9BQU87U0FDUjtRQUVELHFCQUFxQjtRQUNyQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO1FBRUQsd0JBQXdCO1FBQ3hCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixZQUFZO1lBQ1osT0FBTztTQUNSO1FBRUQscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBRTFCLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLHVCQUN2RCxJQUFJLENBQUMsMkJBQTJCLEVBQ25DLENBQUM7UUFFSCwyRUFBMkU7UUFDM0UsNkNBQTZDO1FBQzdDLHdFQUF3RTtRQUN4RSxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQUEsWUFBWTtZQUM5QyxxREFBcUQ7WUFDckQsSUFBSSxPQUFPLFlBQVksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssV0FBVyxFQUFFO2dCQUMzRCxZQUFZO2dCQUNaLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQ3RDLFNBQVMsRUFDVCxZQUFZLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUNwQyxDQUFDO2FBQ0g7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsZ0RBQVEsR0FBUjtRQUNFLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxPQUFPO1NBQ1I7UUFFRCxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUUsQ0FBQztRQUVsQixXQUFXO1FBQ1gsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUM7UUFDZixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBRUQ7Ozs7T0FJRztJQUVILHVEQUFlLEdBRGY7UUFFRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUVILHFEQUFhLEdBRGIsVUFDYyxLQUFZO1FBQ3hCLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRTtZQUNuQyxPQUFPO1NBQ1I7UUFFRCwyQ0FBMkM7UUFDM0MsaUVBQWlFO1FBQ2pFLG9FQUFvRTtRQUNwRSwyREFBMkQ7UUFDM0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQ7O09BRUc7SUFDSCw4Q0FBTSxHQUFOO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDdkIsT0FBTztTQUNSO1FBRUQsK0JBQStCO1FBQy9CLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVEOztPQUVHO0lBQ0gsK0NBQU8sR0FBUDtRQUNFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNyQixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxpREFBUyxHQUFULFVBQVUsQ0FBUyxFQUFFLEtBQWM7UUFDakMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsaURBQVMsR0FBVCxVQUFVLENBQVMsRUFBRSxLQUFjO1FBQ2pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILG1EQUFXLEdBQVgsVUFBWSxNQUFlLEVBQUUsS0FBYztRQUN6QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLE1BQU0sSUFBSSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDekQsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsb0RBQVksR0FBWixVQUFhLE1BQWUsRUFBRSxLQUFjO1FBQzFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsTUFBTSxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxxREFBYSxHQUFiLFVBQWMsTUFBZSxFQUFFLEtBQWM7UUFDM0MsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO1FBRXhELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsS0FBSyxHQUFHLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILHNEQUFjLEdBQWQsVUFBZSxNQUFlLEVBQUUsS0FBYztRQUM1QyxJQUFNLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFFMUQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxNQUFNLEdBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILHdEQUFnQixHQUFoQixVQUFpQixNQUFjLEVBQUUsS0FBYSxFQUFFLEtBQWM7UUFBOUQsaUJBNENDO1FBM0NDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDVixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUM7WUFFOUMsa0VBQWtFO1lBQ2xFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNkLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNmO2FBQU0sSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDMUQsSUFBSSxVQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLElBQUksYUFBVyxHQUFHLENBQUMsQ0FBQztZQUVwQixJQUFJLGNBQVksR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDckMsSUFBSSxVQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFckQsSUFBTSxjQUFZLEdBQUcsQ0FBQyxVQUFRLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTVDLElBQU0sTUFBSSxHQUFHLFVBQUEsWUFBWTtnQkFDdkIsYUFBVyxJQUFJLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxZQUFZLEdBQUcsY0FBWSxDQUFDLENBQUMsQ0FBQztnQkFFakUsVUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQ25CLEtBQUssR0FBRyxjQUFZLEdBQUcsY0FBWSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBVyxDQUFDLENBQzVELENBQUM7Z0JBRUYsNkRBQTZEO2dCQUM3RCxJQUFJLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxLQUFLLFVBQVEsRUFBRTtvQkFDdEQsSUFBSSxhQUFXLElBQUksSUFBSSxDQUFDLEVBQUUsRUFBRTt3QkFDMUIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxDQUFDO3dCQUU5QyxrRUFBa0U7d0JBQ2xFLEtBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQzt3QkFFZCxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7cUJBQ2Y7eUJBQU07d0JBQ0wsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsVUFBUSxHQUFHLFVBQVEsQ0FBQzt3QkFFNUQsY0FBWSxHQUFHLFlBQVksQ0FBQzt3QkFFNUIsTUFBTSxDQUFDLHFCQUFxQixDQUFDLE1BQUksQ0FBQyxDQUFDO3FCQUNwQztpQkFDRjtZQUNILENBQUMsQ0FBQztZQUVGLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxNQUFJLENBQUMsQ0FBQztTQUNwQztJQUNILENBQUM7O2dCQXBYRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLHdCQUF3QjtpQkFDbkM7Ozs7Z0JBaEJDLFVBQVU7Z0JBWUgsaUJBQWlCO2dCQU5qQixRQUFRO2dCQURPLE1BQU07Ozs4Q0EyRDNCLEtBQUs7MEJBZ0JMLEtBQUssU0FBQyxzQkFBc0I7a0NBaUo1QixZQUFZLFNBQUMsZUFBZTtnQ0FjNUIsWUFBWSxTQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDOztJQW9KNUMsb0NBQUM7Q0FBQSxBQXJYRCxJQXFYQztTQWxYWSw2QkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIEFmdGVyVmlld0luaXQsXHJcbiAgRGlyZWN0aXZlLFxyXG4gIEVsZW1lbnRSZWYsXHJcbiAgSG9zdExpc3RlbmVyLFxyXG4gIElucHV0LFxyXG4gIE9uRGVzdHJveVxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUGxhdGZvcm0gfSBmcm9tIFwiQGFuZ3VsYXIvY2RrL3BsYXRmb3JtXCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBmaWx0ZXIsIHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5pbXBvcnQgUGVyZmVjdFNjcm9sbGJhciBmcm9tIFwicGVyZmVjdC1zY3JvbGxiYXJcIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlQ29uZmlnU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9zZXJ2aWNlcy9jb25maWcuc2VydmljZVwiO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6IFwiW2Z1c2VQZXJmZWN0U2Nyb2xsYmFyXVwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlUGVyZmVjdFNjcm9sbGJhckRpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XHJcbiAgaXNJbml0aWFsaXplZDogYm9vbGVhbjtcclxuICBpc01vYmlsZTogYm9vbGVhbjtcclxuICBwczogUGVyZmVjdFNjcm9sbGJhciB8IGFueTtcclxuXHJcbiAgLy8gUHJpdmF0ZVxyXG4gIHByaXZhdGUgX2VuYWJsZWQ6IGJvb2xlYW4gfCBcIlwiO1xyXG4gIHByaXZhdGUgX2RlYm91bmNlZFVwZGF0ZTogYW55O1xyXG4gIHByaXZhdGUgX29wdGlvbnM6IGFueTtcclxuICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtFbGVtZW50UmVmfSBlbGVtZW50UmVmXHJcbiAgICogQHBhcmFtIHtGdXNlQ29uZmlnU2VydmljZX0gX2Z1c2VDb25maWdTZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtQbGF0Zm9ybX0gX3BsYXRmb3JtXHJcbiAgICogQHBhcmFtIHtSb3V0ZXJ9IF9yb3V0ZXJcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHB1YmxpYyBlbGVtZW50UmVmOiBFbGVtZW50UmVmLFxyXG4gICAgcHJpdmF0ZSBfZnVzZUNvbmZpZ1NlcnZpY2U6IEZ1c2VDb25maWdTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfcGxhdGZvcm06IFBsYXRmb3JtLFxyXG4gICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXJcclxuICApIHtcclxuICAgIC8vIFNldCB0aGUgZGVmYXVsdHNcclxuICAgIHRoaXMuaXNJbml0aWFsaXplZCA9IGZhbHNlO1xyXG4gICAgdGhpcy5pc01vYmlsZSA9IGZhbHNlO1xyXG5cclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fZW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgdGhpcy5fZGVib3VuY2VkVXBkYXRlID0gXy5kZWJvdW5jZSh0aGlzLnVwZGF0ZSwgMTUwKTtcclxuICAgIHRoaXMuX29wdGlvbnMgPSB7XHJcbiAgICAgIHVwZGF0ZU9uUm91dGVDaGFuZ2U6IGZhbHNlXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIEFjY2Vzc29yc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIFBlcmZlY3QgU2Nyb2xsYmFyIG9wdGlvbnNcclxuICAgKlxyXG4gICAqIEBwYXJhbSB2YWx1ZVxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IGZ1c2VQZXJmZWN0U2Nyb2xsYmFyT3B0aW9ucyh2YWx1ZSkge1xyXG4gICAgLy8gTWVyZ2UgdGhlIG9wdGlvbnNcclxuICAgIHRoaXMuX29wdGlvbnMgPSBfLm1lcmdlKHt9LCB0aGlzLl9vcHRpb25zLCB2YWx1ZSk7XHJcbiAgfVxyXG5cclxuICBnZXQgZnVzZVBlcmZlY3RTY3JvbGxiYXJPcHRpb25zKCk6IGFueSB7XHJcbiAgICAvLyBSZXR1cm4gdGhlIG9wdGlvbnNcclxuICAgIHJldHVybiB0aGlzLl9vcHRpb25zO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogSXMgZW5hYmxlZFxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtib29sZWFuIHwgXCJcIn0gdmFsdWVcclxuICAgKi9cclxuICBASW5wdXQoXCJmdXNlUGVyZmVjdFNjcm9sbGJhclwiKVxyXG4gIHNldCBlbmFibGVkKHZhbHVlOiBib29sZWFuIHwgXCJcIikge1xyXG4gICAgLy8gSWYgbm90aGluZyBpcyBwcm92aWRlZCB3aXRoIHRoZSBkaXJlY3RpdmUgKGVtcHR5IHN0cmluZyksXHJcbiAgICAvLyB3ZSB3aWxsIHRha2UgdGhhdCBhcyBhIHRydWVcclxuICAgIGlmICh2YWx1ZSA9PT0gXCJcIikge1xyXG4gICAgICB2YWx1ZSA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUmV0dXJuLCBpZiBib3RoIHZhbHVlcyBhcmUgdGhlIHNhbWVcclxuICAgIGlmICh0aGlzLmVuYWJsZWQgPT09IHZhbHVlKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTdG9yZSB0aGUgdmFsdWVcclxuICAgIHRoaXMuX2VuYWJsZWQgPSB2YWx1ZTtcclxuXHJcbiAgICAvLyBJZiBlbmFibGVkLi4uXHJcbiAgICBpZiAodGhpcy5lbmFibGVkKSB7XHJcbiAgICAgIC8vIEluaXQgdGhlIGRpcmVjdGl2ZVxyXG4gICAgICB0aGlzLl9pbml0KCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBPdGhlcndpc2UgZGVzdHJveSBpdFxyXG4gICAgICB0aGlzLl9kZXN0cm95KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXQgZW5hYmxlZCgpOiBib29sZWFuIHwgXCJcIiB7XHJcbiAgICAvLyBSZXR1cm4gdGhlIGVuYWJsZWQgc3RhdHVzXHJcbiAgICByZXR1cm4gdGhpcy5fZW5hYmxlZDtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBBZnRlciB2aWV3IGluaXRcclxuICAgKi9cclxuICBuZ0FmdGVyVmlld0luaXQoKTogdm9pZCB7XHJcbiAgICAvLyBDaGVjayBpZiBzY3JvbGxiYXJzIGVuYWJsZWQgb3Igbm90IGZyb20gdGhlIG1haW4gY29uZmlnXHJcbiAgICB0aGlzLl9mdXNlQ29uZmlnU2VydmljZS5jb25maWdcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgLnN1YnNjcmliZShzZXR0aW5ncyA9PiB7XHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gc2V0dGluZ3MuY3VzdG9tU2Nyb2xsYmFycztcclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gU2Nyb2xsIHRvIHRoZSB0b3Agb24gZXZlcnkgcm91dGUgY2hhbmdlXHJcbiAgICBpZiAodGhpcy5mdXNlUGVyZmVjdFNjcm9sbGJhck9wdGlvbnMudXBkYXRlT25Sb3V0ZUNoYW5nZSkge1xyXG4gICAgICB0aGlzLl9yb3V0ZXIuZXZlbnRzXHJcbiAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICB0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpLFxyXG4gICAgICAgICAgZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZClcclxuICAgICAgICApXHJcbiAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zY3JvbGxUb1RvcCgpO1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgICAgICAgfSwgMCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBPbiBkZXN0cm95XHJcbiAgICovXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLl9kZXN0cm95KCk7XHJcblxyXG4gICAgLy8gVW5zdWJzY3JpYmUgZnJvbSBhbGwgc3Vic2NyaXB0aW9uc1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwubmV4dCgpO1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQcml2YXRlIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBJbml0aWFsaXplXHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIF9pbml0KCk6IHZvaWQge1xyXG4gICAgLy8gUmV0dXJuLCBpZiBhbHJlYWR5IGluaXRpYWxpemVkXHJcbiAgICBpZiAodGhpcy5pc0luaXRpYWxpemVkKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBDaGVjayBpZiBpcyBtb2JpbGVcclxuICAgIGlmICh0aGlzLl9wbGF0Zm9ybS5BTkRST0lEIHx8IHRoaXMuX3BsYXRmb3JtLklPUykge1xyXG4gICAgICB0aGlzLmlzTW9iaWxlID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBSZXR1cm4gaWYgaXQncyBtb2JpbGVcclxuICAgIGlmICh0aGlzLmlzTW9iaWxlKSB7XHJcbiAgICAgIC8vIFJldHVybi4uLlxyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2V0IGFzIGluaXRpYWxpemVkXHJcbiAgICB0aGlzLmlzSW5pdGlhbGl6ZWQgPSB0cnVlO1xyXG5cclxuICAgIC8vIEluaXRpYWxpemUgdGhlIHBlcmZlY3Qtc2Nyb2xsYmFyXHJcbiAgICB0aGlzLnBzID0gbmV3IFBlcmZlY3RTY3JvbGxiYXIodGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIHtcclxuICAgICAgLi4udGhpcy5mdXNlUGVyZmVjdFNjcm9sbGJhck9wdGlvbnNcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIFVuYmluZCAna2V5ZG93bicgZXZlbnRzIG9mIFBlcmZlY3RTY3JvbGxiYXIgc2luY2UgaXQgY2F1c2VzIGFuIGV4dHJlbWVseVxyXG4gICAgLy8gaGlnaCBDUFUgdXNhZ2Ugb24gQW5ndWxhciBNYXRlcmlhbCBpbnB1dHMuXHJcbiAgICAvLyBMb29wIHRocm91Z2ggYWxsIHRoZSBldmVudCBlbGVtZW50cyBvZiB0aGlzIFBlcmZlY3RTY3JvbGxiYXIgaW5zdGFuY2VcclxuICAgIHRoaXMucHMuZXZlbnQuZXZlbnRFbGVtZW50cy5mb3JFYWNoKGV2ZW50RWxlbWVudCA9PiB7XHJcbiAgICAgIC8vIElmIHdlIGhpdCB0byB0aGUgZWxlbWVudCB3aXRoIGEgJ2tleWRvd24nIGV2ZW50Li4uXHJcbiAgICAgIGlmICh0eXBlb2YgZXZlbnRFbGVtZW50LmhhbmRsZXJzW1wia2V5ZG93blwiXSAhPT0gXCJ1bmRlZmluZWRcIikge1xyXG4gICAgICAgIC8vIFVuYmluZCBpdFxyXG4gICAgICAgIGV2ZW50RWxlbWVudC5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoXHJcbiAgICAgICAgICBcImtleWRvd25cIixcclxuICAgICAgICAgIGV2ZW50RWxlbWVudC5oYW5kbGVyc1tcImtleWRvd25cIl1bMF1cclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIERlc3Ryb3lcclxuICAgKlxyXG4gICAqIEBwcml2YXRlXHJcbiAgICovXHJcbiAgX2Rlc3Ryb3koKTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMuaXNJbml0aWFsaXplZCB8fCAhdGhpcy5wcykge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRGVzdHJveSB0aGUgcGVyZmVjdC1zY3JvbGxiYXJcclxuICAgIHRoaXMucHMuZGVzdHJveSgpO1xyXG5cclxuICAgIC8vIENsZWFuIHVwXHJcbiAgICB0aGlzLnBzID0gbnVsbDtcclxuICAgIHRoaXMuaXNJbml0aWFsaXplZCA9IGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVXBkYXRlIHNjcm9sbGJhcnMgb24gd2luZG93IHJlc2l6ZVxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBASG9zdExpc3RlbmVyKFwid2luZG93OnJlc2l6ZVwiKVxyXG4gIF91cGRhdGVPblJlc2l6ZSgpOiB2b2lkIHtcclxuICAgIHRoaXMuX2RlYm91bmNlZFVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogRG9jdW1lbnQgY2xpY2tcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7RXZlbnR9IGV2ZW50XHJcbiAgICovXHJcbiAgQEhvc3RMaXN0ZW5lcihcImRvY3VtZW50OmNsaWNrXCIsIFtcIiRldmVudFwiXSlcclxuICBkb2N1bWVudENsaWNrKGV2ZW50OiBFdmVudCk6IHZvaWQge1xyXG4gICAgaWYgKCF0aGlzLmlzSW5pdGlhbGl6ZWQgfHwgIXRoaXMucHMpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFVwZGF0ZSB0aGUgc2Nyb2xsYmFyIG9uIGRvY3VtZW50IGNsaWNrLi5cclxuICAgIC8vIFRoaXMgaXNuJ3QgdGhlIG1vc3QgZWxlZ2FudCBzb2x1dGlvbiBidXQgdGhlcmUgaXMgbm8gb3RoZXIgd2F5XHJcbiAgICAvLyBvZiBrbm93aW5nIHdoZW4gdGhlIGNvbnRlbnRzIG9mIHRoZSBzY3JvbGxhYmxlIGNvbnRhaW5lciBjaGFuZ2VzLlxyXG4gICAgLy8gVGhlcmVmb3JlLCB3ZSB1cGRhdGUgc2Nyb2xsYmFycyBvbiBldmVyeSBkb2N1bWVudCBjbGljay5cclxuICAgIHRoaXMucHMudXBkYXRlKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBVcGRhdGUgdGhlIHNjcm9sbGJhclxyXG4gICAqL1xyXG4gIHVwZGF0ZSgpOiB2b2lkIHtcclxuICAgIGlmICghdGhpcy5pc0luaXRpYWxpemVkKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBVcGRhdGUgdGhlIHBlcmZlY3Qtc2Nyb2xsYmFyXHJcbiAgICB0aGlzLnBzLnVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRGVzdHJveSB0aGUgc2Nyb2xsYmFyXHJcbiAgICovXHJcbiAgZGVzdHJveSgpOiB2b2lkIHtcclxuICAgIHRoaXMubmdPbkRlc3Ryb3koKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNjcm9sbCB0byBYXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge251bWJlcn0geFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzcGVlZFxyXG4gICAqL1xyXG4gIHNjcm9sbFRvWCh4OiBudW1iZXIsIHNwZWVkPzogbnVtYmVyKTogdm9pZCB7XHJcbiAgICB0aGlzLmFuaW1hdGVTY3JvbGxpbmcoXCJzY3JvbGxMZWZ0XCIsIHgsIHNwZWVkKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNjcm9sbCB0byBZXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge251bWJlcn0geVxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzcGVlZFxyXG4gICAqL1xyXG4gIHNjcm9sbFRvWSh5OiBudW1iZXIsIHNwZWVkPzogbnVtYmVyKTogdm9pZCB7XHJcbiAgICB0aGlzLmFuaW1hdGVTY3JvbGxpbmcoXCJzY3JvbGxUb3BcIiwgeSwgc3BlZWQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2Nyb2xsIHRvIHRvcFxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IG9mZnNldFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzcGVlZFxyXG4gICAqL1xyXG4gIHNjcm9sbFRvVG9wKG9mZnNldD86IG51bWJlciwgc3BlZWQ/OiBudW1iZXIpOiB2b2lkIHtcclxuICAgIHRoaXMuYW5pbWF0ZVNjcm9sbGluZyhcInNjcm9sbFRvcFwiLCBvZmZzZXQgfHwgMCwgc3BlZWQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2Nyb2xsIHRvIGxlZnRcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBvZmZzZXRcclxuICAgKiBAcGFyYW0ge251bWJlcn0gc3BlZWRcclxuICAgKi9cclxuICBzY3JvbGxUb0xlZnQob2Zmc2V0PzogbnVtYmVyLCBzcGVlZD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgdGhpcy5hbmltYXRlU2Nyb2xsaW5nKFwic2Nyb2xsTGVmdFwiLCBvZmZzZXQgfHwgMCwgc3BlZWQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2Nyb2xsIHRvIHJpZ2h0XHJcbiAgICpcclxuICAgKiBAcGFyYW0ge251bWJlcn0gb2Zmc2V0XHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IHNwZWVkXHJcbiAgICovXHJcbiAgc2Nyb2xsVG9SaWdodChvZmZzZXQ/OiBudW1iZXIsIHNwZWVkPzogbnVtYmVyKTogdm9pZCB7XHJcbiAgICBjb25zdCB3aWR0aCA9IHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnNjcm9sbFdpZHRoO1xyXG5cclxuICAgIHRoaXMuYW5pbWF0ZVNjcm9sbGluZyhcInNjcm9sbExlZnRcIiwgd2lkdGggLSAob2Zmc2V0IHx8IDApLCBzcGVlZCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTY3JvbGwgdG8gYm90dG9tXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge251bWJlcn0gb2Zmc2V0XHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IHNwZWVkXHJcbiAgICovXHJcbiAgc2Nyb2xsVG9Cb3R0b20ob2Zmc2V0PzogbnVtYmVyLCBzcGVlZD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgY29uc3QgaGVpZ2h0ID0gdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuc2Nyb2xsSGVpZ2h0O1xyXG5cclxuICAgIHRoaXMuYW5pbWF0ZVNjcm9sbGluZyhcInNjcm9sbFRvcFwiLCBoZWlnaHQgLSAob2Zmc2V0IHx8IDApLCBzcGVlZCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBBbmltYXRlIHNjcm9sbGluZ1xyXG4gICAqXHJcbiAgICogQHBhcmFtIHtzdHJpbmd9IHRhcmdldFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSB2YWx1ZVxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzcGVlZFxyXG4gICAqL1xyXG4gIGFuaW1hdGVTY3JvbGxpbmcodGFyZ2V0OiBzdHJpbmcsIHZhbHVlOiBudW1iZXIsIHNwZWVkPzogbnVtYmVyKTogdm9pZCB7XHJcbiAgICBpZiAoIXNwZWVkKSB7XHJcbiAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50W3RhcmdldF0gPSB2YWx1ZTtcclxuXHJcbiAgICAgIC8vIFBTIGhhcyB3ZWlyZCBldmVudCBzZW5kaW5nIG9yZGVyLCB0aGlzIGlzIGEgd29ya2Fyb3VuZCBmb3IgdGhhdFxyXG4gICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgfSBlbHNlIGlmICh2YWx1ZSAhPT0gdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRbdGFyZ2V0XSkge1xyXG4gICAgICBsZXQgbmV3VmFsdWUgPSAwO1xyXG4gICAgICBsZXQgc2Nyb2xsQ291bnQgPSAwO1xyXG5cclxuICAgICAgbGV0IG9sZFRpbWVzdGFtcCA9IHBlcmZvcm1hbmNlLm5vdygpO1xyXG4gICAgICBsZXQgb2xkVmFsdWUgPSB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudFt0YXJnZXRdO1xyXG5cclxuICAgICAgY29uc3QgY29zUGFyYW1ldGVyID0gKG9sZFZhbHVlIC0gdmFsdWUpIC8gMjtcclxuXHJcbiAgICAgIGNvbnN0IHN0ZXAgPSBuZXdUaW1lc3RhbXAgPT4ge1xyXG4gICAgICAgIHNjcm9sbENvdW50ICs9IE1hdGguUEkgLyAoc3BlZWQgLyAobmV3VGltZXN0YW1wIC0gb2xkVGltZXN0YW1wKSk7XHJcblxyXG4gICAgICAgIG5ld1ZhbHVlID0gTWF0aC5yb3VuZChcclxuICAgICAgICAgIHZhbHVlICsgY29zUGFyYW1ldGVyICsgY29zUGFyYW1ldGVyICogTWF0aC5jb3Moc2Nyb2xsQ291bnQpXHJcbiAgICAgICAgKTtcclxuXHJcbiAgICAgICAgLy8gT25seSBjb250aW51ZSBhbmltYXRpb24gaWYgc2Nyb2xsIHBvc2l0aW9uIGhhcyBub3QgY2hhbmdlZFxyXG4gICAgICAgIGlmICh0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudFt0YXJnZXRdID09PSBvbGRWYWx1ZSkge1xyXG4gICAgICAgICAgaWYgKHNjcm9sbENvdW50ID49IE1hdGguUEkpIHtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRbdGFyZ2V0XSA9IHZhbHVlO1xyXG5cclxuICAgICAgICAgICAgLy8gUFMgaGFzIHdlaXJkIGV2ZW50IHNlbmRpbmcgb3JkZXIsIHRoaXMgaXMgYSB3b3JrYXJvdW5kIGZvciB0aGF0XHJcbiAgICAgICAgICAgIHRoaXMudXBkYXRlKCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRbdGFyZ2V0XSA9IG9sZFZhbHVlID0gbmV3VmFsdWU7XHJcblxyXG4gICAgICAgICAgICBvbGRUaW1lc3RhbXAgPSBuZXdUaW1lc3RhbXA7XHJcblxyXG4gICAgICAgICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHN0ZXApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuXHJcbiAgICAgIHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoc3RlcCk7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcbiJdfQ==