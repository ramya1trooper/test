import { Component, ElementRef, Input, Renderer2, ViewChild } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseMatchMediaService } from '../../../@fuse/services/match-media.service';
import { FuseNavigationService } from '../../../@fuse/components/navigation/navigation.service';
var FuseShortcutsComponent = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {Renderer2} _renderer
     * @param {CookieService} _cookieService
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {ObservableMedia} _observableMedia
     */
    function FuseShortcutsComponent(_cookieService, _fuseMatchMediaService, _fuseNavigationService, _observableMedia, _renderer) {
        this._cookieService = _cookieService;
        this._fuseMatchMediaService = _fuseMatchMediaService;
        this._fuseNavigationService = _fuseNavigationService;
        this._observableMedia = _observableMedia;
        this._renderer = _renderer;
        // Set the defaults
        this.shortcutItems = [];
        this.searching = false;
        this.mobileShortcutsPanelActive = false;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    FuseShortcutsComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Get the navigation items and flatten them
        this.filteredNavigationItems = this.navigationItems = this._fuseNavigationService.getFlatNavigation(this.navigation);
        if (this._cookieService.check('FUSE2.shortcuts')) {
            this.shortcutItems = JSON.parse(this._cookieService.get('FUSE2.shortcuts'));
        }
        else {
            // User's shortcut items
            this.shortcutItems = [];
        }
        // Subscribe to media changes
        this._fuseMatchMediaService.onMediaChange
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            if (_this._observableMedia.isActive('gt-sm')) {
                _this.hideMobileShortcutsPanel();
            }
        });
    };
    /**
     * On destroy
     */
    FuseShortcutsComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Search
     *
     * @param event
     */
    FuseShortcutsComponent.prototype.search = function (event) {
        var value = event.target.value.toLowerCase();
        if (value === '') {
            this.searching = false;
            this.filteredNavigationItems = this.navigationItems;
            return;
        }
        this.searching = true;
        this.filteredNavigationItems = this.navigationItems.filter(function (navigationItem) {
            return navigationItem.title.toLowerCase().includes(value);
        });
    };
    /**
     * Toggle shortcut
     *
     * @param event
     * @param itemToToggle
     */
    FuseShortcutsComponent.prototype.toggleShortcut = function (event, itemToToggle) {
        event.stopPropagation();
        for (var i = 0; i < this.shortcutItems.length; i++) {
            if (this.shortcutItems[i].url === itemToToggle.url) {
                this.shortcutItems.splice(i, 1);
                // Save to the cookies
                this._cookieService.set('FUSE2.shortcuts', JSON.stringify(this.shortcutItems));
                return;
            }
        }
        this.shortcutItems.push(itemToToggle);
        // Save to the cookies
        this._cookieService.set('FUSE2.shortcuts', JSON.stringify(this.shortcutItems));
    };
    /**
     * Is in shortcuts?
     *
     * @param navigationItem
     * @returns {any}
     */
    FuseShortcutsComponent.prototype.isInShortcuts = function (navigationItem) {
        return this.shortcutItems.find(function (item) {
            return item.url === navigationItem.url;
        });
    };
    /**
     * On menu open
     */
    FuseShortcutsComponent.prototype.onMenuOpen = function () {
        var _this = this;
        setTimeout(function () {
            _this.searchInputField.nativeElement.focus();
        });
    };
    /**
     * Show mobile shortcuts
     */
    FuseShortcutsComponent.prototype.showMobileShortcutsPanel = function () {
        this.mobileShortcutsPanelActive = true;
        this._renderer.addClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
    };
    /**
     * Hide mobile shortcuts
     */
    FuseShortcutsComponent.prototype.hideMobileShortcutsPanel = function () {
        this.mobileShortcutsPanelActive = false;
        this._renderer.removeClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
    };
    FuseShortcutsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuse-shortcuts',
                    template: "<div id=\"fuse-shortcuts\" #shortcuts>\r\n\r\n    <div class=\"shortcuts-mobile-toggle\" *ngIf=\"!mobileShortcutsPanelActive\" fxLayout=\"row\" fxLayoutAlign=\"start center\"\r\n         fxHide fxShow.lt-md>\r\n        <button mat-icon-button (click)=\"showMobileShortcutsPanel()\">\r\n            <mat-icon class=\"amber-600-fg\">star</mat-icon>\r\n        </button>\r\n    </div>\r\n\r\n    <div class=\"shortcuts\" fxLayout=\"row\" fxHide fxShow.gt-sm>\r\n\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\" fxFlex=\"0 1 auto\">\r\n\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                <div class=\"w-40 h-40 p-4\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n                     *ngFor=\"let shortcutItem of shortcutItems\">\r\n\r\n                    <a mat-icon-button matTooltip=\"{{shortcutItem.title}}\" [routerLink]=\"shortcutItem.url\">\r\n                        <mat-icon class=\"secondary-text\" *ngIf=\"shortcutItem.icon\">{{shortcutItem.icon}}</mat-icon>\r\n                        <span *ngIf=\"!shortcutItem.icon\" class=\"h2 secondary-text text-bold\">\r\n                            {{shortcutItem.title.substr(0, 1).toUpperCase()}}\r\n                        </span>\r\n                    </a>\r\n\r\n                </div>\r\n\r\n                <button mat-icon-button [matMenuTriggerFor]=\"addMenu\" matTooltip=\"Click to add/remove shortcut\"\r\n                        (menuOpened)=\"onMenuOpen()\">\r\n                    <mat-icon class=\"amber-600-fg\">star</mat-icon>\r\n                </button>\r\n\r\n            </div>\r\n\r\n            <div class=\"shortcuts-mobile-close\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxHide fxShow.lt-md>\r\n                <button mat-icon-button (click)=\"hideMobileShortcutsPanel()\">\r\n                    <mat-icon>close</mat-icon>\r\n                </button>\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <mat-menu #addMenu=\"matMenu\" class=\"w-240\">\r\n\r\n            <mat-form-field class=\"px-16 w-100-p\" (click)=\"$event.stopPropagation()\" floatLabel=\"never\">\r\n                <input #searchInput matInput placeholder=\"Search for an app or a page\" (input)=\"search($event)\">\r\n            </mat-form-field>\r\n\r\n            <mat-divider></mat-divider>\r\n\r\n            <mat-nav-list *ngIf=\"!searching\" style=\"max-height: 312px; overflow: auto\" fusePerfectScrollbar>\r\n\r\n                <mat-list-item *ngFor=\"let shortcutItem of shortcutItems\"\r\n                               (click)=\"toggleShortcut($event, shortcutItem)\">\r\n\r\n                    <div class=\"w-100-p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                        <mat-icon mat-list-icon class=\"mr-8 secondary-text\" *ngIf=\"shortcutItem.icon\">\r\n                            {{shortcutItem.icon}}\r\n                        </mat-icon>\r\n\r\n                        <span class=\"h2 w-32 h-32 p-4 mr-8 secondary-text text-bold\" fxLayout=\"row\"\r\n                              fxLayoutAlign=\"center center\" *ngIf=\"!shortcutItem.icon\">\r\n                            {{shortcutItem.title.substr(0, 1).toUpperCase()}}\r\n                        </span>\r\n\r\n                        <p matLine fxFlex>{{shortcutItem.title}}</p>\r\n\r\n                        <mat-icon class=\"ml-8 amber-fg\">star</mat-icon>\r\n\r\n                    </div>\r\n\r\n                </mat-list-item>\r\n\r\n                <mat-list-item *ngIf=\"shortcutItems.length === 0\">\r\n                    <p>\r\n                        <small>No shortcuts yet!</small>\r\n                    </p>\r\n                </mat-list-item>\r\n\r\n            </mat-nav-list>\r\n\r\n            <mat-nav-list *ngIf=\"searching\" style=\"max-height: 312px; overflow: auto\" fusePerfectScrollbar>\r\n\r\n                <mat-list-item *ngFor=\"let navigationItem of filteredNavigationItems\"\r\n                               (click)=\"toggleShortcut($event, navigationItem)\">\r\n\r\n                    <div class=\"w-100-p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                        <mat-icon mat-list-icon class=\"mr-8 secondary-text\" *ngIf=\"navigationItem.icon\">\r\n                            {{navigationItem.icon}}\r\n                        </mat-icon>\r\n\r\n                        <span class=\"h2 w-32 h-32 p-4 mr-8 secondary-text text-bold\" fxLayout=\"row\"\r\n                              fxLayoutAlign=\"center center\" *ngIf=\"!navigationItem.icon\">\r\n                            {{navigationItem.title.substr(0, 1).toUpperCase()}}\r\n                        </span>\r\n\r\n                        <p matLine fxFlex>{{navigationItem.title}}</p>\r\n\r\n                        <mat-icon class=\"ml-8 amber-fg\" *ngIf=\"isInShortcuts(navigationItem)\">star</mat-icon>\r\n\r\n                    </div>\r\n\r\n                </mat-list-item>\r\n\r\n            </mat-nav-list>\r\n\r\n        </mat-menu>\r\n\r\n    </div>\r\n\r\n</div>\r\n",
                    styles: ["@media screen and (max-width:959px){:host #fuse-shortcuts.show-mobile-panel{position:absolute;top:0;right:0;bottom:0;left:0;z-index:99;padding:0 8px}:host #fuse-shortcuts.show-mobile-panel .shortcuts{display:-webkit-box!important;display:flex!important;-webkit-box-flex:1;flex:1;height:100%}:host #fuse-shortcuts.show-mobile-panel .shortcuts>div{-webkit-box-flex:1!important;flex:1 1 auto!important}}"]
                }] }
    ];
    /** @nocollapse */
    FuseShortcutsComponent.ctorParameters = function () { return [
        { type: CookieService },
        { type: FuseMatchMediaService },
        { type: FuseNavigationService },
        { type: ObservableMedia },
        { type: Renderer2 }
    ]; };
    FuseShortcutsComponent.propDecorators = {
        navigation: [{ type: Input }],
        searchInputField: [{ type: ViewChild, args: ['searchInput',] }],
        shortcutsEl: [{ type: ViewChild, args: ['shortcuts',] }]
    };
    return FuseShortcutsComponent;
}());
export { FuseShortcutsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcnRjdXRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3Nob3J0Y3V0cy9zaG9ydGN1dHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBRWhHO0lBeUJJOzs7Ozs7OztPQVFHO0lBQ0gsZ0NBQ1ksY0FBNkIsRUFDN0Isc0JBQTZDLEVBQzdDLHNCQUE2QyxFQUM3QyxnQkFBaUMsRUFDakMsU0FBb0I7UUFKcEIsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0IsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF1QjtRQUM3QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXVCO1FBQzdDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFDakMsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUc1QixtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdkIsSUFBSSxDQUFDLDBCQUEwQixHQUFHLEtBQUssQ0FBQztRQUV4QywyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7T0FFRztJQUNILHlDQUFRLEdBQVI7UUFBQSxpQkF3QkM7UUF0QkcsNENBQTRDO1FBQzVDLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFckgsSUFBSyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxFQUNqRDtZQUNJLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUM7U0FDL0U7YUFFRDtZQUNJLHdCQUF3QjtZQUN4QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztTQUMzQjtRQUVELDZCQUE2QjtRQUM3QixJQUFJLENBQUMsc0JBQXNCLENBQUMsYUFBYTthQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNyQyxTQUFTLENBQUM7WUFDUCxJQUFLLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEVBQzVDO2dCQUNJLEtBQUksQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO2FBQ25DO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDWCxDQUFDO0lBRUQ7O09BRUc7SUFDSCw0Q0FBVyxHQUFYO1FBRUkscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILHVDQUFNLEdBQU4sVUFBTyxLQUFLO1FBRVIsSUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUM7UUFFL0MsSUFBSyxLQUFLLEtBQUssRUFBRSxFQUNqQjtZQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQ3ZCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBRXBELE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBRXRCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxVQUFDLGNBQWM7WUFDdEUsT0FBTyxjQUFjLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILCtDQUFjLEdBQWQsVUFBZSxLQUFLLEVBQUUsWUFBWTtRQUU5QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFeEIsS0FBTSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUNuRDtZQUNJLElBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssWUFBWSxDQUFDLEdBQUcsRUFDbkQ7Z0JBQ0ksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUVoQyxzQkFBc0I7Z0JBQ3RCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBRS9FLE9BQU87YUFDVjtTQUNKO1FBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFdEMsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDbkYsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsOENBQWEsR0FBYixVQUFjLGNBQWM7UUFFeEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFBLElBQUk7WUFDL0IsT0FBTyxJQUFJLENBQUMsR0FBRyxLQUFLLGNBQWMsQ0FBQyxHQUFHLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSCwyQ0FBVSxHQUFWO1FBQUEsaUJBS0M7UUFIRyxVQUFVLENBQUM7WUFDUCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hELENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOztPQUVHO0lBQ0gseURBQXdCLEdBQXhCO1FBRUksSUFBSSxDQUFDLDBCQUEwQixHQUFHLElBQUksQ0FBQztRQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7SUFFRDs7T0FFRztJQUNILHlEQUF3QixHQUF4QjtRQUVJLElBQUksQ0FBQywwQkFBMEIsR0FBRyxLQUFLLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztJQUNwRixDQUFDOztnQkE5TEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBSyxnQkFBZ0I7b0JBQzdCLG04SkFBeUM7O2lCQUU1Qzs7OztnQkFYUSxhQUFhO2dCQUliLHFCQUFxQjtnQkFDckIscUJBQXFCO2dCQU5yQixlQUFlO2dCQURrQyxTQUFTOzs7NkJBc0I5RCxLQUFLO21DQUdMLFNBQVMsU0FBQyxhQUFhOzhCQUd2QixTQUFTLFNBQUMsV0FBVzs7SUE0SzFCLDZCQUFDO0NBQUEsQUEvTEQsSUErTEM7U0ExTFksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQsIFJlbmRlcmVyMiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGVNZWRpYSB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuaW1wb3J0IHsgQ29va2llU2VydmljZSB9IGZyb20gJ25neC1jb29raWUtc2VydmljZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgRnVzZU1hdGNoTWVkaWFTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vQGZ1c2Uvc2VydmljZXMvbWF0Y2gtbWVkaWEuc2VydmljZSc7XHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvciAgIDogJ2Z1c2Utc2hvcnRjdXRzJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zaG9ydGN1dHMuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzICA6IFsnLi9zaG9ydGN1dHMuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZVNob3J0Y3V0c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95XHJcbntcclxuICAgIHNob3J0Y3V0SXRlbXM6IGFueVtdO1xyXG4gICAgbmF2aWdhdGlvbkl0ZW1zOiBhbnlbXTtcclxuICAgIGZpbHRlcmVkTmF2aWdhdGlvbkl0ZW1zOiBhbnlbXTtcclxuICAgIHNlYXJjaGluZzogYm9vbGVhbjtcclxuICAgIG1vYmlsZVNob3J0Y3V0c1BhbmVsQWN0aXZlOiBib29sZWFuO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBuYXZpZ2F0aW9uOiBhbnk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnc2VhcmNoSW5wdXQnKVxyXG4gICAgc2VhcmNoSW5wdXRGaWVsZDtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdzaG9ydGN1dHMnKVxyXG4gICAgc2hvcnRjdXRzRWw6IEVsZW1lbnRSZWY7XHJcblxyXG4gICAgLy8gUHJpdmF0ZVxyXG4gICAgcHJpdmF0ZSBfdW5zdWJzY3JpYmVBbGw6IFN1YmplY3Q8YW55PjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnN0cnVjdG9yXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtSZW5kZXJlcjJ9IF9yZW5kZXJlclxyXG4gICAgICogQHBhcmFtIHtDb29raWVTZXJ2aWNlfSBfY29va2llU2VydmljZVxyXG4gICAgICogQHBhcmFtIHtGdXNlTWF0Y2hNZWRpYVNlcnZpY2V9IF9mdXNlTWF0Y2hNZWRpYVNlcnZpY2VcclxuICAgICAqIEBwYXJhbSB7RnVzZU5hdmlnYXRpb25TZXJ2aWNlfSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlXHJcbiAgICAgKiBAcGFyYW0ge09ic2VydmFibGVNZWRpYX0gX29ic2VydmFibGVNZWRpYVxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9jb29raWVTZXJ2aWNlOiBDb29raWVTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX2Z1c2VNYXRjaE1lZGlhU2VydmljZTogRnVzZU1hdGNoTWVkaWFTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX2Z1c2VOYXZpZ2F0aW9uU2VydmljZTogRnVzZU5hdmlnYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgX29ic2VydmFibGVNZWRpYTogT2JzZXJ2YWJsZU1lZGlhLFxyXG4gICAgICAgIHByaXZhdGUgX3JlbmRlcmVyOiBSZW5kZXJlcjJcclxuICAgIClcclxuICAgIHtcclxuICAgICAgICAvLyBTZXQgdGhlIGRlZmF1bHRzXHJcbiAgICAgICAgdGhpcy5zaG9ydGN1dEl0ZW1zID0gW107XHJcbiAgICAgICAgdGhpcy5zZWFyY2hpbmcgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLm1vYmlsZVNob3J0Y3V0c1BhbmVsQWN0aXZlID0gZmFsc2U7XHJcblxyXG4gICAgICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPbiBpbml0XHJcbiAgICAgKi9cclxuICAgIG5nT25Jbml0KCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICAvLyBHZXQgdGhlIG5hdmlnYXRpb24gaXRlbXMgYW5kIGZsYXR0ZW4gdGhlbVxyXG4gICAgICAgIHRoaXMuZmlsdGVyZWROYXZpZ2F0aW9uSXRlbXMgPSB0aGlzLm5hdmlnYXRpb25JdGVtcyA9IHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5nZXRGbGF0TmF2aWdhdGlvbih0aGlzLm5hdmlnYXRpb24pO1xyXG5cclxuICAgICAgICBpZiAoIHRoaXMuX2Nvb2tpZVNlcnZpY2UuY2hlY2soJ0ZVU0UyLnNob3J0Y3V0cycpIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvcnRjdXRJdGVtcyA9IEpTT04ucGFyc2UodGhpcy5fY29va2llU2VydmljZS5nZXQoJ0ZVU0UyLnNob3J0Y3V0cycpKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgLy8gVXNlcidzIHNob3J0Y3V0IGl0ZW1zXHJcbiAgICAgICAgICAgIHRoaXMuc2hvcnRjdXRJdGVtcyA9IFtdO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gU3Vic2NyaWJlIHRvIG1lZGlhIGNoYW5nZXNcclxuICAgICAgICB0aGlzLl9mdXNlTWF0Y2hNZWRpYVNlcnZpY2Uub25NZWRpYUNoYW5nZVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICggdGhpcy5fb2JzZXJ2YWJsZU1lZGlhLmlzQWN0aXZlKCdndC1zbScpIClcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmhpZGVNb2JpbGVTaG9ydGN1dHNQYW5lbCgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIGRlc3Ryb3lcclxuICAgICAqL1xyXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFVuc3Vic2NyaWJlIGZyb20gYWxsIHN1YnNjcmlwdGlvbnNcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5uZXh0KCk7XHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlYXJjaFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICovXHJcbiAgICBzZWFyY2goZXZlbnQpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSBldmVudC50YXJnZXQudmFsdWUudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICAgICAgaWYgKCB2YWx1ZSA9PT0gJycgKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGhpcy5zZWFyY2hpbmcgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5maWx0ZXJlZE5hdmlnYXRpb25JdGVtcyA9IHRoaXMubmF2aWdhdGlvbkl0ZW1zO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zZWFyY2hpbmcgPSB0cnVlO1xyXG5cclxuICAgICAgICB0aGlzLmZpbHRlcmVkTmF2aWdhdGlvbkl0ZW1zID0gdGhpcy5uYXZpZ2F0aW9uSXRlbXMuZmlsdGVyKChuYXZpZ2F0aW9uSXRlbSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gbmF2aWdhdGlvbkl0ZW0udGl0bGUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyh2YWx1ZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUb2dnbGUgc2hvcnRjdXRcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEBwYXJhbSBpdGVtVG9Ub2dnbGVcclxuICAgICAqL1xyXG4gICAgdG9nZ2xlU2hvcnRjdXQoZXZlbnQsIGl0ZW1Ub1RvZ2dsZSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgZm9yICggbGV0IGkgPSAwOyBpIDwgdGhpcy5zaG9ydGN1dEl0ZW1zLmxlbmd0aDsgaSsrIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGlmICggdGhpcy5zaG9ydGN1dEl0ZW1zW2ldLnVybCA9PT0gaXRlbVRvVG9nZ2xlLnVybCApXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2hvcnRjdXRJdGVtcy5zcGxpY2UoaSwgMSk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gU2F2ZSB0byB0aGUgY29va2llc1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fY29va2llU2VydmljZS5zZXQoJ0ZVU0UyLnNob3J0Y3V0cycsIEpTT04uc3RyaW5naWZ5KHRoaXMuc2hvcnRjdXRJdGVtcykpO1xyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5zaG9ydGN1dEl0ZW1zLnB1c2goaXRlbVRvVG9nZ2xlKTtcclxuXHJcbiAgICAgICAgLy8gU2F2ZSB0byB0aGUgY29va2llc1xyXG4gICAgICAgIHRoaXMuX2Nvb2tpZVNlcnZpY2Uuc2V0KCdGVVNFMi5zaG9ydGN1dHMnLCBKU09OLnN0cmluZ2lmeSh0aGlzLnNob3J0Y3V0SXRlbXMpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIElzIGluIHNob3J0Y3V0cz9cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gbmF2aWdhdGlvbkl0ZW1cclxuICAgICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICAgKi9cclxuICAgIGlzSW5TaG9ydGN1dHMobmF2aWdhdGlvbkl0ZW0pOiBhbnlcclxuICAgIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaG9ydGN1dEl0ZW1zLmZpbmQoaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBpdGVtLnVybCA9PT0gbmF2aWdhdGlvbkl0ZW0udXJsO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gbWVudSBvcGVuXHJcbiAgICAgKi9cclxuICAgIG9uTWVudU9wZW4oKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaElucHV0RmllbGQubmF0aXZlRWxlbWVudC5mb2N1cygpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2hvdyBtb2JpbGUgc2hvcnRjdXRzXHJcbiAgICAgKi9cclxuICAgIHNob3dNb2JpbGVTaG9ydGN1dHNQYW5lbCgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5tb2JpbGVTaG9ydGN1dHNQYW5lbEFjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3ModGhpcy5zaG9ydGN1dHNFbC5uYXRpdmVFbGVtZW50LCAnc2hvdy1tb2JpbGUtcGFuZWwnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEhpZGUgbW9iaWxlIHNob3J0Y3V0c1xyXG4gICAgICovXHJcbiAgICBoaWRlTW9iaWxlU2hvcnRjdXRzUGFuZWwoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHRoaXMubW9iaWxlU2hvcnRjdXRzUGFuZWxBY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVDbGFzcyh0aGlzLnNob3J0Y3V0c0VsLm5hdGl2ZUVsZW1lbnQsICdzaG93LW1vYmlsZS1wYW5lbCcpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==