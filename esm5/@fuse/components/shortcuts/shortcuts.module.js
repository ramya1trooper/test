import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CookieService } from "ngx-cookie-service";
import { FuseShortcutsComponent } from "./shortcuts.component";
import { MaterialModule } from "../../../material.module";
var FuseShortcutsModule = /** @class */ (function () {
    function FuseShortcutsModule() {
    }
    FuseShortcutsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [FuseShortcutsComponent],
                    imports: [CommonModule, RouterModule, FlexLayoutModule, MaterialModule],
                    exports: [FuseShortcutsComponent],
                    providers: [CookieService]
                },] }
    ];
    return FuseShortcutsModule;
}());
export { FuseShortcutsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcnRjdXRzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3Nob3J0Y3V0cy9zaG9ydGN1dHMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFbkQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRTFEO0lBQUE7SUFNa0MsQ0FBQzs7Z0JBTmxDLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQztvQkFDdEMsT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLFlBQVksRUFBRSxnQkFBZ0IsRUFBRSxjQUFjLENBQUM7b0JBQ3ZFLE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO29CQUNqQyxTQUFTLEVBQUUsQ0FBQyxhQUFhLENBQUM7aUJBQzNCOztJQUNpQywwQkFBQztDQUFBLEFBTm5DLElBTW1DO1NBQXRCLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvZmxleC1sYXlvdXRcIjtcclxuaW1wb3J0IHsgQ29va2llU2VydmljZSB9IGZyb20gXCJuZ3gtY29va2llLXNlcnZpY2VcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VTaG9ydGN1dHNDb21wb25lbnQgfSBmcm9tIFwiLi9zaG9ydGN1dHMuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtGdXNlU2hvcnRjdXRzQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbQ29tbW9uTW9kdWxlLCBSb3V0ZXJNb2R1bGUsIEZsZXhMYXlvdXRNb2R1bGUsIE1hdGVyaWFsTW9kdWxlXSxcclxuICBleHBvcnRzOiBbRnVzZVNob3J0Y3V0c0NvbXBvbmVudF0sXHJcbiAgcHJvdmlkZXJzOiBbQ29va2llU2VydmljZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VTaG9ydGN1dHNNb2R1bGUge31cclxuIl19