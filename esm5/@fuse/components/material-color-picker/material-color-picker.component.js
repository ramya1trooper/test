import { Component, EventEmitter, forwardRef, Input, Output, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '../../../@fuse/animations';
import { MatColors } from '../../../@fuse/mat-colors';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export var FUSE_MATERIAL_COLOR_PICKER_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(function () { return FuseMaterialColorPickerComponent; }),
    multi: true
};
var FuseMaterialColorPickerComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function FuseMaterialColorPickerComponent() {
        // Set the defaults
        this.colorChanged = new EventEmitter();
        this.colors = MatColors.all;
        this.hues = ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'];
        this.selectedHue = '500';
        this.view = 'palettes';
        // Set the private defaults
        this._color = '';
        this._modelChange = function () {
        };
        this._modelTouched = function () {
        };
    }
    Object.defineProperty(FuseMaterialColorPickerComponent.prototype, "color", {
        get: function () {
            return this._color;
        },
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        /**
         * Selected class
         *
         * @param value
         */
        set: function (value) {
            if (!value || value === '' || this._color === value) {
                return;
            }
            // Split the color value (red-400, blue-500, fuse-navy-700 etc.)
            var colorParts = value.split('-');
            // Take the very last part as the selected hue value
            this.selectedHue = colorParts[colorParts.length - 1];
            // Remove the last part
            colorParts.pop();
            // Rejoin the remaining parts as the selected palette name
            this.selectedPalette = colorParts.join('-');
            // Store the color value
            this._color = value;
        },
        enumerable: true,
        configurable: true
    });
    // -----------------------------------------------------------------------------------------------------
    // @ Control Value Accessor implementation
    // -----------------------------------------------------------------------------------------------------
    /**
     * Register on change function
     *
     * @param fn
     */
    FuseMaterialColorPickerComponent.prototype.registerOnChange = function (fn) {
        this._modelChange = fn;
    };
    /**
     * Register on touched function
     *
     * @param fn
     */
    FuseMaterialColorPickerComponent.prototype.registerOnTouched = function (fn) {
        this._modelTouched = fn;
    };
    /**
     * Write value to the view from model
     *
     * @param color
     */
    FuseMaterialColorPickerComponent.prototype.writeValue = function (color) {
        // Return if null
        if (!color) {
            return;
        }
        // Set the color
        this.color = color;
        // Update the selected color
        this.updateSelectedColor();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Select palette
     *
     * @param event
     * @param palette
     */
    FuseMaterialColorPickerComponent.prototype.selectPalette = function (event, palette) {
        // Stop propagation
        event.stopPropagation();
        // Go to 'hues' view
        this.view = 'hues';
        // Update the selected palette
        this.selectedPalette = palette;
        // Update the selected color
        this.updateSelectedColor();
    };
    /**
     * Select hue
     *
     * @param event
     * @param hue
     */
    FuseMaterialColorPickerComponent.prototype.selectHue = function (event, hue) {
        // Stop propagation
        event.stopPropagation();
        // Update the selected huse
        this.selectedHue = hue;
        // Update the selected color
        this.updateSelectedColor();
    };
    /**
     * Remove color
     *
     * @param event
     */
    FuseMaterialColorPickerComponent.prototype.removeColor = function (event) {
        // Stop propagation
        event.stopPropagation();
        // Return to the 'palettes' view
        this.view = 'palettes';
        // Clear the selected palette and hue
        this.selectedPalette = '';
        this.selectedHue = '';
        // Update the selected color
        this.updateSelectedColor();
    };
    /**
     * Update selected color
     */
    FuseMaterialColorPickerComponent.prototype.updateSelectedColor = function () {
        if (this.selectedColor && this.selectedColor.palette === this.selectedPalette && this.selectedColor.hue === this.selectedHue) {
            return;
        }
        // Set the selected color object
        this.selectedColor = {
            palette: this.selectedPalette,
            hue: this.selectedHue,
            class: this.selectedPalette + '-' + this.selectedHue,
            bg: this.selectedPalette === '' ? '' : MatColors.getColor(this.selectedPalette)[this.selectedHue],
            fg: this.selectedPalette === '' ? '' : MatColors.getColor(this.selectedPalette).contrast[this.selectedHue]
        };
        // Emit the color changed event
        this.colorChanged.emit(this.selectedColor);
        // Mark the model as touched
        this._modelTouched(this.selectedColor.class);
        // Update the model
        this._modelChange(this.selectedColor.class);
    };
    /**
     * Go to palettes view
     *
     * @param event
     */
    FuseMaterialColorPickerComponent.prototype.goToPalettesView = function (event) {
        // Stop propagation
        event.stopPropagation();
        this.view = 'palettes';
    };
    /**
     * On menu open
     */
    FuseMaterialColorPickerComponent.prototype.onMenuOpen = function () {
        if (this.selectedPalette === '') {
            this.view = 'palettes';
        }
        else {
            this.view = 'hues';
        }
    };
    FuseMaterialColorPickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuse-material-color-picker',
                    template: "<button mat-icon-button class=\"mat-elevation-z1\" [matMenuTriggerFor]=\"colorMenu\" (menuOpened)=\"onMenuOpen()\"\r\n  [ngClass]=\"selectedPalette + '-' + selectedHue\">\r\n  <mat-icon>palette</mat-icon>\r\n</button>\r\n\r\n<mat-menu #colorMenu=\"matMenu\" class=\"fuse-material-color-picker-menu mat-elevation-z8\">\r\n\r\n  <header [ngClass]=\"selectedColor?.class || 'accent'\" class=\"mat-elevation-z4\" fxLayout=\"row\"\r\n    fxLayoutAlign=\"space-between center\">\r\n\r\n    <button mat-icon-button class=\"secondary-text\" [style.visibility]=\"view === 'hues' ? 'visible' : 'hidden'\"\r\n      (click)=\"goToPalettesView($event)\" aria-label=\"Palette\">\r\n      <mat-icon class=\"s-20\">arrow_back</mat-icon>\r\n    </button>\r\n\r\n    <span *ngIf=\"selectedColor?.palette\">\r\n      {{selectedColor.palette}} {{selectedColor.hue}}\r\n    </span>\r\n\r\n    <span *ngIf=\"!selectedColor?.palette\">\r\n      Select a Color\r\n    </span>\r\n\r\n    <button mat-icon-button class=\"remove-color-button secondary-text\" (click)=\"removeColor($event)\"\r\n      aria-label=\"Remove color\" matTooltip=\"Remove color\">\r\n      <mat-icon class=\"s-20\">delete</mat-icon>\r\n    </button>\r\n  </header>\r\n\r\n  <div [ngSwitch]=\"view\" class=\"views\">\r\n\r\n    <div class=\"view\" *ngSwitchCase=\"'palettes'\">\r\n\r\n      <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\" class=\"colors\" fusePerfectScrollbar>\r\n        <!-- <div class=\"color\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n                     *ngFor=\"let color of (colors | keys)\"\r\n                     [ngClass]=\"color.key\"\r\n                     [class.selected]=\"selectedPalette === color.key\"\r\n                     (click)=\"selectPalette($event, color.key)\">\r\n                </div> -->\r\n      </div>\r\n    </div>\r\n\r\n    <!-- <div class=\"view\" *ngSwitchCase=\"'hues'\">\r\n      <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\" class=\"colors\" fusePerfectScrollbar>\r\n        <div class=\"color\" fxLayout=\"row\" fxLayoutAlign=\"center center\" *ngFor=\"let hue of hues\"\r\n          [fxHide]=\"selectedPalette === 'fuse-white' && hue !== '500' || selectedPalette === 'fuse-black' && hue !== '500'\"\r\n          [ngClass]=\"selectedPalette + '-' + hue\" [class.selected]=\"selectedHue === hue\"\r\n          (click)=\"selectHue($event, hue)\">\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n\r\n  </div>\r\n</mat-menu>\r\n",
                    animations: fuseAnimations,
                    encapsulation: ViewEncapsulation.None,
                    providers: [FUSE_MATERIAL_COLOR_PICKER_VALUE_ACCESSOR],
                    styles: [".fuse-material-color-picker-menu{width:245px}.fuse-material-color-picker-menu .mat-menu-content{padding:0}.fuse-material-color-picker-menu .mat-menu-content .views{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-height:165px}.fuse-material-color-picker-menu .mat-menu-content .views .view{overflow:hidden}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors{padding:1px 0 0;margin-left:-1px}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors .color{width:40px;height:40px;margin:0 0 1px 1px;border-radius:0;cursor:pointer;-webkit-transition:border-radius .4s cubic-bezier(.25,.8,.25,1);transition:border-radius .4s cubic-bezier(.25,.8,.25,1)}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors .color:hover{border-radius:20%}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors .color.selected{border-radius:50%!important}"]
                }] }
    ];
    /** @nocollapse */
    FuseMaterialColorPickerComponent.ctorParameters = function () { return []; };
    FuseMaterialColorPickerComponent.propDecorators = {
        colorChanged: [{ type: Output }],
        color: [{ type: Input }]
    };
    return FuseMaterialColorPickerComponent;
}());
export { FuseMaterialColorPickerComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY29sb3ItcGlja2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL21hdGVyaWFsLWNvbG9yLXBpY2tlci9tYXRlcmlhbC1jb2xvci1waWNrZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXRHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDdEQsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXpFLE1BQU0sQ0FBQyxJQUFNLHlDQUF5QyxHQUFRO0lBQzFELE9BQU8sRUFBTSxpQkFBaUI7SUFDOUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxjQUFNLE9BQUEsZ0NBQWdDLEVBQWhDLENBQWdDLENBQUM7SUFDL0QsS0FBSyxFQUFRLElBQUk7Q0FDcEIsQ0FBQztBQUVGO0lBMEJJOztPQUVHO0lBQ0g7UUFFSSxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQztRQUM1QixJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ2xILElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1FBRXZCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztRQUNqQixJQUFJLENBQUMsWUFBWSxHQUFHO1FBQ3BCLENBQUMsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLEdBQUc7UUFDckIsQ0FBQyxDQUFDO0lBQ04sQ0FBQztJQVdELHNCQUNJLG1EQUFLO2FBdUJUO1lBRUksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7UUFwQ0Qsd0dBQXdHO1FBQ3hHLGNBQWM7UUFDZCx3R0FBd0c7UUFFeEc7Ozs7V0FJRzthQUNILFVBQ1UsS0FBSztZQUVYLElBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFDcEQ7Z0JBQ0ksT0FBTzthQUNWO1lBRUQsZ0VBQWdFO1lBQ2hFLElBQU0sVUFBVSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7WUFFcEMsb0RBQW9EO1lBQ3BELElBQUksQ0FBQyxXQUFXLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFFckQsdUJBQXVCO1lBQ3ZCLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUVqQiwwREFBMEQ7WUFDMUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTVDLHdCQUF3QjtZQUN4QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDOzs7T0FBQTtJQU9ELHdHQUF3RztJQUN4RywwQ0FBMEM7SUFDMUMsd0dBQXdHO0lBRXhHOzs7O09BSUc7SUFDSCwyREFBZ0IsR0FBaEIsVUFBaUIsRUFBTztRQUVwQixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILDREQUFpQixHQUFqQixVQUFrQixFQUFPO1FBRXJCLElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO0lBQzVCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gscURBQVUsR0FBVixVQUFXLEtBQVU7UUFFakIsaUJBQWlCO1FBQ2pCLElBQUssQ0FBQyxLQUFLLEVBQ1g7WUFDSSxPQUFPO1NBQ1Y7UUFFRCxnQkFBZ0I7UUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFFbkIsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsbUJBQW1CO0lBQ25CLHdHQUF3RztJQUV4Rzs7Ozs7T0FLRztJQUNILHdEQUFhLEdBQWIsVUFBYyxLQUFLLEVBQUUsT0FBTztRQUV4QixtQkFBbUI7UUFDbkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXhCLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUVuQiw4QkFBOEI7UUFDOUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7UUFFL0IsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILG9EQUFTLEdBQVQsVUFBVSxLQUFLLEVBQUUsR0FBRztRQUVoQixtQkFBbUI7UUFDbkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXhCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztRQUV2Qiw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxzREFBVyxHQUFYLFVBQVksS0FBSztRQUViLG1CQUFtQjtRQUNuQixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFeEIsZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1FBRXZCLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsZUFBZSxHQUFHLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUV0Qiw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVEOztPQUVHO0lBQ0gsOERBQW1CLEdBQW5CO1FBRUksSUFBSyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxLQUFLLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEtBQUssSUFBSSxDQUFDLFdBQVcsRUFDN0g7WUFDSSxPQUFPO1NBQ1Y7UUFFRCxnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLGFBQWEsR0FBRztZQUNqQixPQUFPLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDN0IsR0FBRyxFQUFNLElBQUksQ0FBQyxXQUFXO1lBQ3pCLEtBQUssRUFBSSxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVztZQUN0RCxFQUFFLEVBQU8sSUFBSSxDQUFDLGVBQWUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUN0RyxFQUFFLEVBQU8sSUFBSSxDQUFDLGVBQWUsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7U0FDbEgsQ0FBQztRQUVGLCtCQUErQjtRQUMvQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFM0MsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU3QyxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsMkRBQWdCLEdBQWhCLFVBQWlCLEtBQUs7UUFFbEIsbUJBQW1CO1FBQ25CLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxxREFBVSxHQUFWO1FBRUksSUFBSyxJQUFJLENBQUMsZUFBZSxLQUFLLEVBQUUsRUFDaEM7WUFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztTQUMxQjthQUVEO1lBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7U0FDdEI7SUFDTCxDQUFDOztnQkF2UEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBTyw0QkFBNEI7b0JBQzNDLHE3RUFBdUQ7b0JBRXZELFVBQVUsRUFBSyxjQUFjO29CQUM3QixhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsU0FBUyxFQUFNLENBQUMseUNBQXlDLENBQUM7O2lCQUM3RDs7Ozs7K0JBV0ksTUFBTTt3QkFxQ04sS0FBSzs7SUFpTVYsdUNBQUM7Q0FBQSxBQXhQRCxJQXdQQztTQWhQWSxnQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgZm9yd2FyZFJlZiwgSW5wdXQsIE91dHB1dCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IGZ1c2VBbmltYXRpb25zIH0gZnJvbSAnLi4vLi4vLi4vQGZ1c2UvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IE1hdENvbG9ycyB9IGZyb20gJy4uLy4uLy4uL0BmdXNlL21hdC1jb2xvcnMnO1xyXG5pbXBvcnQgeyBDb250cm9sVmFsdWVBY2Nlc3NvciwgTkdfVkFMVUVfQUNDRVNTT1IgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5leHBvcnQgY29uc3QgRlVTRV9NQVRFUklBTF9DT0xPUl9QSUNLRVJfVkFMVUVfQUNDRVNTT1I6IGFueSA9IHtcclxuICAgIHByb3ZpZGUgICAgOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IEZ1c2VNYXRlcmlhbENvbG9yUGlja2VyQ29tcG9uZW50KSxcclxuICAgIG11bHRpICAgICAgOiB0cnVlXHJcbn07XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yICAgICA6ICdmdXNlLW1hdGVyaWFsLWNvbG9yLXBpY2tlcicsXHJcbiAgICB0ZW1wbGF0ZVVybCAgOiAnLi9tYXRlcmlhbC1jb2xvci1waWNrZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzICAgIDogWycuL21hdGVyaWFsLWNvbG9yLXBpY2tlci5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgYW5pbWF0aW9ucyAgIDogZnVzZUFuaW1hdGlvbnMsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgcHJvdmlkZXJzICAgIDogW0ZVU0VfTUFURVJJQUxfQ09MT1JfUElDS0VSX1ZBTFVFX0FDQ0VTU09SXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU1hdGVyaWFsQ29sb3JQaWNrZXJDb21wb25lbnQgaW1wbGVtZW50cyBDb250cm9sVmFsdWVBY2Nlc3NvclxyXG57XHJcbiAgICBjb2xvcnM6IGFueTtcclxuICAgIGh1ZXM6IHN0cmluZ1tdO1xyXG4gICAgdmlldzogc3RyaW5nO1xyXG4gICAgc2VsZWN0ZWRDb2xvcjogYW55O1xyXG4gICAgc2VsZWN0ZWRQYWxldHRlOiBzdHJpbmc7XHJcbiAgICBzZWxlY3RlZEh1ZTogc3RyaW5nO1xyXG5cclxuICAgIC8vIENvbG9yIGNoYW5nZWRcclxuICAgIEBPdXRwdXQoKVxyXG4gICAgY29sb3JDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8YW55PjtcclxuXHJcbiAgICAvLyBQcml2YXRlXHJcbiAgICBwcml2YXRlIF9jb2xvcjogc3RyaW5nO1xyXG4gICAgcHJpdmF0ZSBfbW9kZWxDaGFuZ2U6ICh2YWx1ZTogYW55KSA9PiB2b2lkO1xyXG4gICAgcHJpdmF0ZSBfbW9kZWxUb3VjaGVkOiAodmFsdWU6IGFueSkgPT4gdm9pZDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKClcclxuICAgIHtcclxuICAgICAgICAvLyBTZXQgdGhlIGRlZmF1bHRzXHJcbiAgICAgICAgdGhpcy5jb2xvckNoYW5nZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgICAgICAgdGhpcy5jb2xvcnMgPSBNYXRDb2xvcnMuYWxsO1xyXG4gICAgICAgIHRoaXMuaHVlcyA9IFsnNTAnLCAnMTAwJywgJzIwMCcsICczMDAnLCAnNDAwJywgJzUwMCcsICc2MDAnLCAnNzAwJywgJzgwMCcsICc5MDAnLCAnQTEwMCcsICdBMjAwJywgJ0E0MDAnLCAnQTcwMCddO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRIdWUgPSAnNTAwJztcclxuICAgICAgICB0aGlzLnZpZXcgPSAncGFsZXR0ZXMnO1xyXG5cclxuICAgICAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgICAgICB0aGlzLl9jb2xvciA9ICcnO1xyXG4gICAgICAgIHRoaXMuX21vZGVsQ2hhbmdlID0gKCkgPT4ge1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fbW9kZWxUb3VjaGVkID0gKCkgPT4ge1xyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgQWNjZXNzb3JzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VsZWN0ZWQgY2xhc3NcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNldCBjb2xvcih2YWx1ZSlcclxuICAgIHtcclxuICAgICAgICBpZiAoICF2YWx1ZSB8fCB2YWx1ZSA9PT0gJycgfHwgdGhpcy5fY29sb3IgPT09IHZhbHVlIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFNwbGl0IHRoZSBjb2xvciB2YWx1ZSAocmVkLTQwMCwgYmx1ZS01MDAsIGZ1c2UtbmF2eS03MDAgZXRjLilcclxuICAgICAgICBjb25zdCBjb2xvclBhcnRzID0gdmFsdWUuc3BsaXQoJy0nKTtcclxuXHJcbiAgICAgICAgLy8gVGFrZSB0aGUgdmVyeSBsYXN0IHBhcnQgYXMgdGhlIHNlbGVjdGVkIGh1ZSB2YWx1ZVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRIdWUgPSBjb2xvclBhcnRzW2NvbG9yUGFydHMubGVuZ3RoIC0gMV07XHJcblxyXG4gICAgICAgIC8vIFJlbW92ZSB0aGUgbGFzdCBwYXJ0XHJcbiAgICAgICAgY29sb3JQYXJ0cy5wb3AoKTtcclxuXHJcbiAgICAgICAgLy8gUmVqb2luIHRoZSByZW1haW5pbmcgcGFydHMgYXMgdGhlIHNlbGVjdGVkIHBhbGV0dGUgbmFtZVxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRQYWxldHRlID0gY29sb3JQYXJ0cy5qb2luKCctJyk7XHJcblxyXG4gICAgICAgIC8vIFN0b3JlIHRoZSBjb2xvciB2YWx1ZVxyXG4gICAgICAgIHRoaXMuX2NvbG9yID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGNvbG9yKCk6IHN0cmluZ1xyXG4gICAge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9jb2xvcjtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBDb250cm9sIFZhbHVlIEFjY2Vzc29yIGltcGxlbWVudGF0aW9uXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVnaXN0ZXIgb24gY2hhbmdlIGZ1bmN0aW9uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGZuXHJcbiAgICAgKi9cclxuICAgIHJlZ2lzdGVyT25DaGFuZ2UoZm46IGFueSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLl9tb2RlbENoYW5nZSA9IGZuO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVnaXN0ZXIgb24gdG91Y2hlZCBmdW5jdGlvblxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBmblxyXG4gICAgICovXHJcbiAgICByZWdpc3Rlck9uVG91Y2hlZChmbjogYW55KTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHRoaXMuX21vZGVsVG91Y2hlZCA9IGZuO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogV3JpdGUgdmFsdWUgdG8gdGhlIHZpZXcgZnJvbSBtb2RlbFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBjb2xvclxyXG4gICAgICovXHJcbiAgICB3cml0ZVZhbHVlKGNvbG9yOiBhbnkpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gUmV0dXJuIGlmIG51bGxcclxuICAgICAgICBpZiAoICFjb2xvciApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTZXQgdGhlIGNvbG9yXHJcbiAgICAgICAgdGhpcy5jb2xvciA9IGNvbG9yO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgdGhlIHNlbGVjdGVkIGNvbG9yXHJcbiAgICAgICAgdGhpcy51cGRhdGVTZWxlY3RlZENvbG9yKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWxlY3QgcGFsZXR0ZVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICogQHBhcmFtIHBhbGV0dGVcclxuICAgICAqL1xyXG4gICAgc2VsZWN0UGFsZXR0ZShldmVudCwgcGFsZXR0ZSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICAvLyBTdG9wIHByb3BhZ2F0aW9uXHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgIC8vIEdvIHRvICdodWVzJyB2aWV3XHJcbiAgICAgICAgdGhpcy52aWV3ID0gJ2h1ZXMnO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgdGhlIHNlbGVjdGVkIHBhbGV0dGVcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUGFsZXR0ZSA9IHBhbGV0dGU7XHJcblxyXG4gICAgICAgIC8vIFVwZGF0ZSB0aGUgc2VsZWN0ZWQgY29sb3JcclxuICAgICAgICB0aGlzLnVwZGF0ZVNlbGVjdGVkQ29sb3IoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbGVjdCBodWVcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEBwYXJhbSBodWVcclxuICAgICAqL1xyXG4gICAgc2VsZWN0SHVlKGV2ZW50LCBodWUpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU3RvcCBwcm9wYWdhdGlvblxyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgdGhlIHNlbGVjdGVkIGh1c2VcclxuICAgICAgICB0aGlzLnNlbGVjdGVkSHVlID0gaHVlO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgdGhlIHNlbGVjdGVkIGNvbG9yXHJcbiAgICAgICAgdGhpcy51cGRhdGVTZWxlY3RlZENvbG9yKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZW1vdmUgY29sb3JcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgcmVtb3ZlQ29sb3IoZXZlbnQpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU3RvcCBwcm9wYWdhdGlvblxyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICAvLyBSZXR1cm4gdG8gdGhlICdwYWxldHRlcycgdmlld1xyXG4gICAgICAgIHRoaXMudmlldyA9ICdwYWxldHRlcyc7XHJcblxyXG4gICAgICAgIC8vIENsZWFyIHRoZSBzZWxlY3RlZCBwYWxldHRlIGFuZCBodWVcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUGFsZXR0ZSA9ICcnO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRIdWUgPSAnJztcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBzZWxlY3RlZCBjb2xvclxyXG4gICAgICAgIHRoaXMudXBkYXRlU2VsZWN0ZWRDb2xvcigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlIHNlbGVjdGVkIGNvbG9yXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZVNlbGVjdGVkQ29sb3IoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIGlmICggdGhpcy5zZWxlY3RlZENvbG9yICYmIHRoaXMuc2VsZWN0ZWRDb2xvci5wYWxldHRlID09PSB0aGlzLnNlbGVjdGVkUGFsZXR0ZSAmJiB0aGlzLnNlbGVjdGVkQ29sb3IuaHVlID09PSB0aGlzLnNlbGVjdGVkSHVlIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFNldCB0aGUgc2VsZWN0ZWQgY29sb3Igb2JqZWN0XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZENvbG9yID0ge1xyXG4gICAgICAgICAgICBwYWxldHRlOiB0aGlzLnNlbGVjdGVkUGFsZXR0ZSxcclxuICAgICAgICAgICAgaHVlICAgIDogdGhpcy5zZWxlY3RlZEh1ZSxcclxuICAgICAgICAgICAgY2xhc3MgIDogdGhpcy5zZWxlY3RlZFBhbGV0dGUgKyAnLScgKyB0aGlzLnNlbGVjdGVkSHVlLFxyXG4gICAgICAgICAgICBiZyAgICAgOiB0aGlzLnNlbGVjdGVkUGFsZXR0ZSA9PT0gJycgPyAnJyA6IE1hdENvbG9ycy5nZXRDb2xvcih0aGlzLnNlbGVjdGVkUGFsZXR0ZSlbdGhpcy5zZWxlY3RlZEh1ZV0sXHJcbiAgICAgICAgICAgIGZnICAgICA6IHRoaXMuc2VsZWN0ZWRQYWxldHRlID09PSAnJyA/ICcnIDogTWF0Q29sb3JzLmdldENvbG9yKHRoaXMuc2VsZWN0ZWRQYWxldHRlKS5jb250cmFzdFt0aGlzLnNlbGVjdGVkSHVlXVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIC8vIEVtaXQgdGhlIGNvbG9yIGNoYW5nZWQgZXZlbnRcclxuICAgICAgICB0aGlzLmNvbG9yQ2hhbmdlZC5lbWl0KHRoaXMuc2VsZWN0ZWRDb2xvcik7XHJcblxyXG4gICAgICAgIC8vIE1hcmsgdGhlIG1vZGVsIGFzIHRvdWNoZWRcclxuICAgICAgICB0aGlzLl9tb2RlbFRvdWNoZWQodGhpcy5zZWxlY3RlZENvbG9yLmNsYXNzKTtcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBtb2RlbFxyXG4gICAgICAgIHRoaXMuX21vZGVsQ2hhbmdlKHRoaXMuc2VsZWN0ZWRDb2xvci5jbGFzcyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHbyB0byBwYWxldHRlcyB2aWV3XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKi9cclxuICAgIGdvVG9QYWxldHRlc1ZpZXcoZXZlbnQpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU3RvcCBwcm9wYWdhdGlvblxyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICB0aGlzLnZpZXcgPSAncGFsZXR0ZXMnO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gbWVudSBvcGVuXHJcbiAgICAgKi9cclxuICAgIG9uTWVudU9wZW4oKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIGlmICggdGhpcy5zZWxlY3RlZFBhbGV0dGUgPT09ICcnIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMudmlldyA9ICdwYWxldHRlcyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2VcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMudmlldyA9ICdodWVzJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19