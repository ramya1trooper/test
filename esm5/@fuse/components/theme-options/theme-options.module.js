import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FuseDirectivesModule } from "../../../@fuse/directives/directives";
import { FuseMaterialColorPickerModule } from "../../../@fuse/components/material-color-picker/material-color-picker.module";
import { FuseSidebarModule } from "../../../@fuse/components/sidebar/sidebar.module";
import { FuseThemeOptionsComponent } from "../../../@fuse/components/theme-options/theme-options.component";
import { MaterialModule } from "../../../material.module";
var FuseThemeOptionsModule = /** @class */ (function () {
    function FuseThemeOptionsModule() {
    }
    FuseThemeOptionsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [FuseThemeOptionsComponent],
                    imports: [
                        CommonModule,
                        FormsModule,
                        ReactiveFormsModule,
                        FlexLayoutModule,
                        MaterialModule,
                        FuseDirectivesModule,
                        FuseMaterialColorPickerModule,
                        FuseSidebarModule
                    ],
                    exports: [FuseThemeOptionsComponent]
                },] }
    ];
    return FuseThemeOptionsModule;
}());
export { FuseThemeOptionsModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGhlbWUtb3B0aW9ucy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy90aGVtZS1vcHRpb25zL3RoZW1lLW9wdGlvbnMubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUV4RCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUM1RSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSw4RUFBOEUsQ0FBQztBQUM3SCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUVyRixPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxpRUFBaUUsQ0FBQztBQUM1RyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFMUQ7SUFBQTtJQWlCcUMsQ0FBQzs7Z0JBakJyQyxRQUFRLFNBQUM7b0JBQ1IsWUFBWSxFQUFFLENBQUMseUJBQXlCLENBQUM7b0JBQ3pDLE9BQU8sRUFBRTt3QkFDUCxZQUFZO3dCQUNaLFdBQVc7d0JBQ1gsbUJBQW1CO3dCQUVuQixnQkFBZ0I7d0JBRWhCLGNBQWM7d0JBRWQsb0JBQW9CO3dCQUNwQiw2QkFBNkI7d0JBQzdCLGlCQUFpQjtxQkFDbEI7b0JBQ0QsT0FBTyxFQUFFLENBQUMseUJBQXlCLENBQUM7aUJBQ3JDOztJQUNvQyw2QkFBQztDQUFBLEFBakJ0QyxJQWlCc0M7U0FBekIsc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uXCI7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvZmxleC1sYXlvdXRcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VEaXJlY3RpdmVzTW9kdWxlIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL2RpcmVjdGl2ZXMvZGlyZWN0aXZlc1wiO1xyXG5pbXBvcnQgeyBGdXNlTWF0ZXJpYWxDb2xvclBpY2tlck1vZHVsZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL21hdGVyaWFsLWNvbG9yLXBpY2tlci9tYXRlcmlhbC1jb2xvci1waWNrZXIubW9kdWxlXCI7XHJcbmltcG9ydCB7IEZ1c2VTaWRlYmFyTW9kdWxlIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLm1vZHVsZVwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVRoZW1lT3B0aW9uc0NvbXBvbmVudCB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL3RoZW1lLW9wdGlvbnMvdGhlbWUtb3B0aW9ucy5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vLi4vLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0Z1c2VUaGVtZU9wdGlvbnNDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIENvbW1vbk1vZHVsZSxcclxuICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuXHJcbiAgICBGbGV4TGF5b3V0TW9kdWxlLFxyXG5cclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG5cclxuICAgIEZ1c2VEaXJlY3RpdmVzTW9kdWxlLFxyXG4gICAgRnVzZU1hdGVyaWFsQ29sb3JQaWNrZXJNb2R1bGUsXHJcbiAgICBGdXNlU2lkZWJhck1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW0Z1c2VUaGVtZU9wdGlvbnNDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlVGhlbWVPcHRpb25zTW9kdWxlIHt9XHJcbiJdfQ==