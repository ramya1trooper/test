import { ChangeDetectorRef, Component, HostBinding, Input } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseNavigationService } from '../../../../../@fuse/components/navigation/navigation.service';
import { Compiler } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../../../../../_services/index';
var FuseNavVerticalItemComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    function FuseNavVerticalItemComponent(_changeDetectorRef, _fuseNavigationService, compiler, router, messageService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this.compiler = compiler;
        this.router = router;
        this.messageService = messageService;
        this.classes = 'nav-item';
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    FuseNavVerticalItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved).pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            // Mark for check
            _this._changeDetectorRef.markForCheck();
        });
    };
    /**
     * On destroy
     */
    FuseNavVerticalItemComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    FuseNavVerticalItemComponent.prototype.clickToGo = function (selectedItem) {
        this.messageService.sendRouting(selectedItem);
    };
    FuseNavVerticalItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuse-nav-vertical-item',
                    template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n    <!-- item.url -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"(item.url || item.id === 'dashboard') && !item.externalUrl && !item.function\"\r\n       [routerLink]=\"[item.url]\"  (click)=\"clickToGo(item)\" [routerLinkActive]=\"['active', 'accent']\"\r\n       [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\"\r\n       [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.externalUrl -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && !item.function\"\r\n       [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.function -->\r\n    <span class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && item.function\"\r\n          (click)=\"item.function()\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </span>\r\n\r\n    <!-- item.url && item.function -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && item.function\"\r\n       (click)=\"item.function()\"\r\n       [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n       [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\"\r\n       [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.externalUrl && item.function -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && item.function\"\r\n       (click)=\"item.function()\"\r\n       [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <ng-template #itemContent>\r\n        <mat-icon class=\"nav-link-icon\" *ngIf=\"item.icon\">{{item.icon}}</mat-icon>\r\n        <span class=\"nav-link-title\" [translate]=\"item.translate\">{{item.title}}</span>\r\n        <span class=\"nav-link-badge\" *ngIf=\"item.badge\" [translate]=\"item.badge.translate\"\r\n              [ngStyle]=\"{'background-color': item.badge.bg,'color': item.badge.fg}\">\r\n            {{item.badge.title}}\r\n        </span>\r\n    </ng-template>\r\n    \r\n</ng-container>",
                    styles: [".folded:not(.unfolded) :host .nav-link>.nav-link-badge,.folded:not(.unfolded) :host .nav-link>.nav-link-title{opacity:0;-webkit-transition:opacity .2s;transition:opacity .2s}"]
                }] }
    ];
    /** @nocollapse */
    FuseNavVerticalItemComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef },
        { type: FuseNavigationService },
        { type: Compiler },
        { type: Router },
        { type: MessageService }
    ]; };
    FuseNavVerticalItemComponent.propDecorators = {
        classes: [{ type: HostBinding, args: ['class',] }],
        item: [{ type: Input }]
    };
    return FuseNavVerticalItemComponent;
}());
export { FuseNavVerticalItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL3ZlcnRpY2FsL2l0ZW0vaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNwRyxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHM0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFDdEcsT0FBTyxFQUFhLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQWdCLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXZELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUdoRTtJQWdCSTs7T0FFRztJQUVIOzs7O09BSUc7SUFDSCxzQ0FDWSxrQkFBcUMsRUFDckMsc0JBQTZDLEVBQzdDLFFBQWtCLEVBQ2xCLE1BQWMsRUFDZCxjQUE4QjtRQUo5Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO1FBQ3JDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBdUI7UUFDN0MsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBdEIxQyxZQUFPLEdBQUcsVUFBVSxDQUFDO1FBMEJqQiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7T0FFRztJQUNILCtDQUFRLEdBQVI7UUFBQSxpQkFhQztRQVhHLCtCQUErQjtRQUMvQixLQUFLLENBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixFQUNqRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLEVBQ25ELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FDdEQsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNyQyxTQUFTLENBQUM7WUFFUCxpQkFBaUI7WUFDakIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVEOztPQUVHO0lBQ0gsa0RBQVcsR0FBWDtRQUVJLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVELGdEQUFTLEdBQVQsVUFBVSxZQUFZO1FBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xELENBQUM7O2dCQXhFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFLLHdCQUF3QjtvQkFDckMscStFQUFvQzs7aUJBRXZDOzs7O2dCQWhCUSxpQkFBaUI7Z0JBS2pCLHFCQUFxQjtnQkFDVixRQUFRO2dCQUNMLE1BQU07Z0JBRXBCLGNBQWM7OzswQkFVbEIsV0FBVyxTQUFDLE9BQU87dUJBR25CLEtBQUs7O0lBK0RWLG1DQUFDO0NBQUEsQUF6RUQsSUF5RUM7U0FwRVksNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0b3JSZWYsIENvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBtZXJnZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBGdXNlTmF2aWdhdGlvbkl0ZW0gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9AZnVzZS90eXBlcyc7XHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyAgTmdNb2R1bGUsIENvbXBpbGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSwgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vX3NlcnZpY2VzL2luZGV4JztcclxuXHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yICAgOiAnZnVzZS1uYXYtdmVydGljYWwtaXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vaXRlbS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHMgIDogWycuL2l0ZW0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU5hdlZlcnRpY2FsSXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95XHJcbntcclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MnKVxyXG4gICAgY2xhc3NlcyA9ICduYXYtaXRlbSc7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGl0ZW06IEZ1c2VOYXZpZ2F0aW9uSXRlbTtcclxuXHJcbiAgICAvLyBQcml2YXRlXHJcbiAgICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqL1xyXG5cclxuICAgIC8qKlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7Q2hhbmdlRGV0ZWN0b3JSZWZ9IF9jaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAgICogQHBhcmFtIHtGdXNlTmF2aWdhdGlvblNlcnZpY2V9IF9mdXNlTmF2aWdhdGlvblNlcnZpY2VcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgICAgIHByaXZhdGUgX2Z1c2VOYXZpZ2F0aW9uU2VydmljZTogRnVzZU5hdmlnYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgY29tcGlsZXI6IENvbXBpbGVyLFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2VcclxuXHJcbiAgICApXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU2V0IHRoZSBwcml2YXRlIGRlZmF1bHRzXHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIGluaXRcclxuICAgICAqL1xyXG4gICAgbmdPbkluaXQoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFN1YnNjcmliZSB0byBuYXZpZ2F0aW9uIGl0ZW1cclxuICAgICAgICBtZXJnZShcclxuICAgICAgICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1BZGRlZCxcclxuICAgICAgICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1VcGRhdGVkLFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbVJlbW92ZWRcclxuICAgICAgICApLnBpcGUodGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgICAgICAgICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIGRlc3Ryb3lcclxuICAgICAqL1xyXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFVuc3Vic2NyaWJlIGZyb20gYWxsIHN1YnNjcmlwdGlvbnNcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5uZXh0KCk7XHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBjbGlja1RvR28oc2VsZWN0ZWRJdGVtKSB7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhzZWxlY3RlZEl0ZW0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==