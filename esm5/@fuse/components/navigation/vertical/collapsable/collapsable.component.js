import * as tslib_1 from "tslib";
import { ChangeDetectorRef, Component, HostBinding, Input } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { merge, Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { fuseAnimations } from "../../../../../@fuse/animations";
import { FuseNavigationService } from "../../../../../@fuse/components/navigation/navigation.service";
var FuseNavVerticalCollapsableComponent = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {Router} _router
     */
    function FuseNavVerticalCollapsableComponent(_changeDetectorRef, _fuseNavigationService, _router) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this._router = _router;
        this.classes = "nav-collapsable nav-item";
        this.isOpen = false;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    FuseNavVerticalCollapsableComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Listen for router events
        this._router.events
            .pipe(filter(function (event) { return event instanceof NavigationEnd; }), takeUntil(this._unsubscribeAll))
            .subscribe(function (event) {
            // Check if the url can be found in
            // one of the children of this item
            if (_this.isUrlInChildren(_this.item, event.urlAfterRedirects)) {
                _this.expand();
            }
            else {
                _this.collapse();
            }
        });
        // Listen for collapsing of any navigation item
        this._fuseNavigationService.onItemCollapsed
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function (clickedItem) {
            if (clickedItem && clickedItem.children) {
                // Check if the clicked item is one
                // of the children of this item
                if (_this.isChildrenOf(_this.item, clickedItem)) {
                    return;
                }
                // Check if the url can be found in
                // one of the children of this item
                if (_this.isUrlInChildren(_this.item, _this._router.url)) {
                    return;
                }
                // If the clicked item is not this item, collapse...
                if (_this.item !== clickedItem) {
                    _this.collapse();
                }
            }
        });
        // Check if the url can be found in
        // one of the children of this item
        if (this.isUrlInChildren(this.item, this._router.url)) {
            this.expand();
        }
        else {
            this.collapse();
        }
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            // Mark for check
            _this._changeDetectorRef.markForCheck();
        });
    };
    /**
     * On destroy
     */
    FuseNavVerticalCollapsableComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle collapse
     *
     * @param ev
     */
    FuseNavVerticalCollapsableComponent.prototype.toggleOpen = function (ev) {
        ev.preventDefault();
        this.isOpen = !this.isOpen;
        // Navigation collapse toggled...
        this._fuseNavigationService.onItemCollapsed.next(this.item);
        this._fuseNavigationService.onItemCollapseToggled.next();
    };
    /**
     * Expand the collapsable navigation
     */
    FuseNavVerticalCollapsableComponent.prototype.expand = function () {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        // Mark for check
        this._changeDetectorRef.markForCheck();
        this._fuseNavigationService.onItemCollapseToggled.next();
    };
    /**
     * Collapse the collapsable navigation
     */
    FuseNavVerticalCollapsableComponent.prototype.collapse = function () {
        if (!this.isOpen) {
            return;
        }
        this.isOpen = false;
        // Mark for check
        this._changeDetectorRef.markForCheck();
        this._fuseNavigationService.onItemCollapseToggled.next();
    };
    /**
     * Check if the given parent has the
     * given item in one of its children
     *
     * @param parent
     * @param item
     * @returns {boolean}
     */
    FuseNavVerticalCollapsableComponent.prototype.isChildrenOf = function (parent, item) {
        var e_1, _a;
        if (!parent.children) {
            return false;
        }
        if (parent.children.indexOf(item) !== -1) {
            return true;
        }
        try {
            for (var _b = tslib_1.__values(parent.children), _c = _b.next(); !_c.done; _c = _b.next()) {
                var children = _c.value;
                if (children.children) {
                    return this.isChildrenOf(children, item);
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
    };
    /**
     * Check if the given url can be found
     * in one of the given parent's children
     *
     * @param parent
     * @param url
     * @returns {boolean}
     */
    FuseNavVerticalCollapsableComponent.prototype.isUrlInChildren = function (parent, url) {
        if (!parent.children) {
            return false;
        }
        for (var i = 0; i < parent.children.length; i++) {
            if (parent.children[i].children) {
                if (this.isUrlInChildren(parent.children[i], url)) {
                    return true;
                }
            }
            if (parent.children[i].url === url ||
                url.includes(parent.children[i].url)) {
                return true;
            }
        }
        return false;
    };
    FuseNavVerticalCollapsableComponent.decorators = [
        { type: Component, args: [{
                    selector: "fuse-nav-vertical-collapsable",
                    template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n  <!-- normal collapsable -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && !item.function\" (click)=\"toggleOpen($event)\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.url -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && !item.function\"\r\n    (click)=\"toggleOpen($event)\" [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n    [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.externalUrl -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && !item.function\"\r\n    (click)=\"toggleOpen($event)\" [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.function -->\r\n  <span class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && item.function\" (click)=\"toggleOpen($event);\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </span>\r\n\r\n  <!-- item.url && item.function -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && item.function\"\r\n    (click)=\"toggleOpen($event);\" [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n    [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.externalUrl && item.function -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && item.function\"\r\n    (click)=\"toggleOpen($event);\" [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <ng-template #itemContent>\r\n    <mat-icon class=\"nav-link-icon\" *ngIf=\"item.icon\">{{item.icon}}</mat-icon>\r\n    <span class=\"nav-link-title\" [translate]=\"item.translate\">{{item.title}}</span>\r\n    <span class=\"nav-link-badge\" *ngIf=\"item.badge\" [translate]=\"item.badge.translate\"\r\n      [ngStyle]=\"{'background-color': item.badge.bg,'color': item.badge.fg}\">\r\n      {{item.badge.title}}\r\n    </span>\r\n    <mat-icon class=\"collapsable-arrow\">keyboard_arrow_right</mat-icon>\r\n  </ng-template>\r\n\r\n  <div class=\"children\" [@slideInOut]=\"isOpen\">\r\n    <ng-container *ngFor=\"let item of item.children\">\r\n      <fuse-nav-vertical-item *ngIf=\"item.type=='item'\" [item]=\"item\"></fuse-nav-vertical-item>\r\n      <fuse-nav-vertical-collapsable *ngIf=\"item.type=='collapsable'\" [item]=\"item\"></fuse-nav-vertical-collapsable>\r\n      <fuse-nav-vertical-group *ngIf=\"item.type=='group'\" [item]=\"item\"></fuse-nav-vertical-group>\r\n    </ng-container>\r\n  </div>\r\n\r\n</ng-container>\r\n",
                    animations: fuseAnimations,
                    styles: [".folded:not(.unfolded) :host .nav-link>span{opacity:0;-webkit-transition:opacity .2s;transition:opacity .2s}.folded:not(.unfolded) :host.open .children{display:none!important}:host .nav-link .collapsable-arrow{-webkit-transition:opacity .25s ease-in-out .1s,-webkit-transform .3s ease-in-out;transition:transform .3s ease-in-out,opacity .25s ease-in-out .1s,-webkit-transform .3s ease-in-out;-webkit-transform:rotate(0);transform:rotate(0)}:host>.children{overflow:hidden}:host.open>.nav-link .collapsable-arrow{-webkit-transform:rotate(90deg);transform:rotate(90deg)}"]
                }] }
    ];
    /** @nocollapse */
    FuseNavVerticalCollapsableComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef },
        { type: FuseNavigationService },
        { type: Router }
    ]; };
    FuseNavVerticalCollapsableComponent.propDecorators = {
        item: [{ type: Input }],
        classes: [{ type: HostBinding, args: ["class",] }],
        isOpen: [{ type: HostBinding, args: ["class.open",] }]
    };
    return FuseNavVerticalCollapsableComponent;
}());
export { FuseNavVerticalCollapsableComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2FibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi92ZXJ0aWNhbC9jb2xsYXBzYWJsZS9jb2xsYXBzYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDTCxpQkFBaUIsRUFDakIsU0FBUyxFQUNULFdBQVcsRUFDWCxLQUFLLEVBR04sTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBR25ELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNqRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUV0RztJQW1CRTs7Ozs7O09BTUc7SUFDSCw2Q0FDVSxrQkFBcUMsRUFDckMsc0JBQTZDLEVBQzdDLE9BQWU7UUFGZix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO1FBQ3JDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBdUI7UUFDN0MsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQWxCekIsWUFBTyxHQUFHLDBCQUEwQixDQUFDO1FBRzlCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFpQnBCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsc0RBQVEsR0FBUjtRQUFBLGlCQTREQztRQTNEQywyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO2FBQ2hCLElBQUksQ0FDSCxNQUFNLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFLLFlBQVksYUFBYSxFQUE5QixDQUE4QixDQUFDLEVBQy9DLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQ2hDO2FBQ0EsU0FBUyxDQUFDLFVBQUMsS0FBb0I7WUFDOUIsbUNBQW1DO1lBQ25DLG1DQUFtQztZQUNuQyxJQUFJLEtBQUksQ0FBQyxlQUFlLENBQUMsS0FBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsRUFBRTtnQkFDNUQsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0wsS0FBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCwrQ0FBK0M7UUFDL0MsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWU7YUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLFVBQUEsV0FBVztZQUNwQixJQUFJLFdBQVcsSUFBSSxXQUFXLENBQUMsUUFBUSxFQUFFO2dCQUN2QyxtQ0FBbUM7Z0JBQ25DLCtCQUErQjtnQkFDL0IsSUFBSSxLQUFJLENBQUMsWUFBWSxDQUFDLEtBQUksQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLEVBQUU7b0JBQzdDLE9BQU87aUJBQ1I7Z0JBRUQsbUNBQW1DO2dCQUNuQyxtQ0FBbUM7Z0JBQ25DLElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxLQUFJLENBQUMsSUFBSSxFQUFFLEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUU7b0JBQ3JELE9BQU87aUJBQ1I7Z0JBRUQsb0RBQW9EO2dCQUNwRCxJQUFJLEtBQUksQ0FBQyxJQUFJLEtBQUssV0FBVyxFQUFFO29CQUM3QixLQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ2pCO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVMLG1DQUFtQztRQUNuQyxtQ0FBbUM7UUFDbkMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNyRCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDZjthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2pCO1FBRUQsK0JBQStCO1FBQy9CLEtBQUssQ0FDSCxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLEVBQ2pELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsRUFDbkQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUNwRDthQUNFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQztZQUNULGlCQUFpQjtZQUNqQixLQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSCx5REFBVyxHQUFYO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILHdEQUFVLEdBQVYsVUFBVyxFQUFFO1FBQ1gsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBRTNCLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNELENBQUM7SUFFRDs7T0FFRztJQUNILG9EQUFNLEdBQU47UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUVuQixpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXZDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxzREFBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFFcEIsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV2QyxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0QsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSCwwREFBWSxHQUFaLFVBQWEsTUFBTSxFQUFFLElBQUk7O1FBQ3ZCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ3BCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCxJQUFJLE1BQU0sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ3hDLE9BQU8sSUFBSSxDQUFDO1NBQ2I7O1lBRUQsS0FBdUIsSUFBQSxLQUFBLGlCQUFBLE1BQU0sQ0FBQyxRQUFRLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQW5DLElBQU0sUUFBUSxXQUFBO2dCQUNqQixJQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7b0JBQ3JCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7aUJBQzFDO2FBQ0Y7Ozs7Ozs7OztJQUNILENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ0gsNkRBQWUsR0FBZixVQUFnQixNQUFNLEVBQUUsR0FBRztRQUN6QixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUNwQixPQUFPLEtBQUssQ0FBQztTQUNkO1FBRUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQy9DLElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUU7Z0JBQy9CLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFO29CQUNqRCxPQUFPLElBQUksQ0FBQztpQkFDYjthQUNGO1lBRUQsSUFDRSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxHQUFHO2dCQUM5QixHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQ3BDO2dCQUNBLE9BQU8sSUFBSSxDQUFDO2FBQ2I7U0FDRjtRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2YsQ0FBQzs7Z0JBek5GLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsK0JBQStCO29CQUN6Qywya0dBQTJDO29CQUUzQyxVQUFVLEVBQUUsY0FBYzs7aUJBQzNCOzs7O2dCQXBCQyxpQkFBaUI7Z0JBYVYscUJBQXFCO2dCQU5OLE1BQU07Ozt1QkFlM0IsS0FBSzswQkFHTCxXQUFXLFNBQUMsT0FBTzt5QkFHbkIsV0FBVyxTQUFDLFlBQVk7O0lBNk0zQiwwQ0FBQztDQUFBLEFBMU5ELElBME5DO1NBcE5ZLG1DQUFtQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgQ29tcG9uZW50LFxyXG4gIEhvc3RCaW5kaW5nLFxyXG4gIElucHV0LFxyXG4gIE9uRGVzdHJveSxcclxuICBPbkluaXRcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uRW5kLCBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IG1lcmdlLCBTdWJqZWN0IH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgZmlsdGVyLCB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uSXRlbSB9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9AZnVzZS90eXBlc1wiO1xyXG5pbXBvcnQgeyBmdXNlQW5pbWF0aW9ucyB9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9AZnVzZS9hbmltYXRpb25zXCI7XHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJmdXNlLW5hdi12ZXJ0aWNhbC1jb2xsYXBzYWJsZVwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vY29sbGFwc2FibGUuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vY29sbGFwc2FibGUuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgYW5pbWF0aW9uczogZnVzZUFuaW1hdGlvbnNcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VOYXZWZXJ0aWNhbENvbGxhcHNhYmxlQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIEBJbnB1dCgpXHJcbiAgaXRlbTogRnVzZU5hdmlnYXRpb25JdGVtO1xyXG5cclxuICBASG9zdEJpbmRpbmcoXCJjbGFzc1wiKVxyXG4gIGNsYXNzZXMgPSBcIm5hdi1jb2xsYXBzYWJsZSBuYXYtaXRlbVwiO1xyXG5cclxuICBASG9zdEJpbmRpbmcoXCJjbGFzcy5vcGVuXCIpXHJcbiAgcHVibGljIGlzT3BlbiA9IGZhbHNlO1xyXG5cclxuICAvLyBQcml2YXRlXHJcbiAgcHJpdmF0ZSBfdW5zdWJzY3JpYmVBbGw6IFN1YmplY3Q8YW55PjtcclxuXHJcbiAgLyoqXHJcbiAgICogQ29uc3RydWN0b3JcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7Q2hhbmdlRGV0ZWN0b3JSZWZ9IF9jaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAqIEBwYXJhbSB7RnVzZU5hdmlnYXRpb25TZXJ2aWNlfSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtSb3V0ZXJ9IF9yb3V0ZXJcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2NoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIHByaXZhdGUgX2Z1c2VOYXZpZ2F0aW9uU2VydmljZTogRnVzZU5hdmlnYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfcm91dGVyOiBSb3V0ZXJcclxuICApIHtcclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIE9uIGluaXRcclxuICAgKi9cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIC8vIExpc3RlbiBmb3Igcm91dGVyIGV2ZW50c1xyXG4gICAgdGhpcy5fcm91dGVyLmV2ZW50c1xyXG4gICAgICAucGlwZShcclxuICAgICAgICBmaWx0ZXIoZXZlbnQgPT4gZXZlbnQgaW5zdGFuY2VvZiBOYXZpZ2F0aW9uRW5kKSxcclxuICAgICAgICB0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSgoZXZlbnQ6IE5hdmlnYXRpb25FbmQpID0+IHtcclxuICAgICAgICAvLyBDaGVjayBpZiB0aGUgdXJsIGNhbiBiZSBmb3VuZCBpblxyXG4gICAgICAgIC8vIG9uZSBvZiB0aGUgY2hpbGRyZW4gb2YgdGhpcyBpdGVtXHJcbiAgICAgICAgaWYgKHRoaXMuaXNVcmxJbkNoaWxkcmVuKHRoaXMuaXRlbSwgZXZlbnQudXJsQWZ0ZXJSZWRpcmVjdHMpKSB7XHJcbiAgICAgICAgICB0aGlzLmV4cGFuZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmNvbGxhcHNlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAvLyBMaXN0ZW4gZm9yIGNvbGxhcHNpbmcgb2YgYW55IG5hdmlnYXRpb24gaXRlbVxyXG4gICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uSXRlbUNvbGxhcHNlZFxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKGNsaWNrZWRJdGVtID0+IHtcclxuICAgICAgICBpZiAoY2xpY2tlZEl0ZW0gJiYgY2xpY2tlZEl0ZW0uY2hpbGRyZW4pIHtcclxuICAgICAgICAgIC8vIENoZWNrIGlmIHRoZSBjbGlja2VkIGl0ZW0gaXMgb25lXHJcbiAgICAgICAgICAvLyBvZiB0aGUgY2hpbGRyZW4gb2YgdGhpcyBpdGVtXHJcbiAgICAgICAgICBpZiAodGhpcy5pc0NoaWxkcmVuT2YodGhpcy5pdGVtLCBjbGlja2VkSXRlbSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC8vIENoZWNrIGlmIHRoZSB1cmwgY2FuIGJlIGZvdW5kIGluXHJcbiAgICAgICAgICAvLyBvbmUgb2YgdGhlIGNoaWxkcmVuIG9mIHRoaXMgaXRlbVxyXG4gICAgICAgICAgaWYgKHRoaXMuaXNVcmxJbkNoaWxkcmVuKHRoaXMuaXRlbSwgdGhpcy5fcm91dGVyLnVybCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIC8vIElmIHRoZSBjbGlja2VkIGl0ZW0gaXMgbm90IHRoaXMgaXRlbSwgY29sbGFwc2UuLi5cclxuICAgICAgICAgIGlmICh0aGlzLml0ZW0gIT09IGNsaWNrZWRJdGVtKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29sbGFwc2UoKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIENoZWNrIGlmIHRoZSB1cmwgY2FuIGJlIGZvdW5kIGluXHJcbiAgICAvLyBvbmUgb2YgdGhlIGNoaWxkcmVuIG9mIHRoaXMgaXRlbVxyXG4gICAgaWYgKHRoaXMuaXNVcmxJbkNoaWxkcmVuKHRoaXMuaXRlbSwgdGhpcy5fcm91dGVyLnVybCkpIHtcclxuICAgICAgdGhpcy5leHBhbmQoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuY29sbGFwc2UoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTdWJzY3JpYmUgdG8gbmF2aWdhdGlvbiBpdGVtXHJcbiAgICBtZXJnZShcclxuICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1BZGRlZCxcclxuICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1VcGRhdGVkLFxyXG4gICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbVJlbW92ZWRcclxuICAgIClcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgICAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBPbiBkZXN0cm95XHJcbiAgICovXHJcbiAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAvLyBVbnN1YnNjcmliZSBmcm9tIGFsbCBzdWJzY3JpcHRpb25zXHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5uZXh0KCk7XHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5jb21wbGV0ZSgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogVG9nZ2xlIGNvbGxhcHNlXHJcbiAgICpcclxuICAgKiBAcGFyYW0gZXZcclxuICAgKi9cclxuICB0b2dnbGVPcGVuKGV2KTogdm9pZCB7XHJcbiAgICBldi5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgIHRoaXMuaXNPcGVuID0gIXRoaXMuaXNPcGVuO1xyXG5cclxuICAgIC8vIE5hdmlnYXRpb24gY29sbGFwc2UgdG9nZ2xlZC4uLlxyXG4gICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uSXRlbUNvbGxhcHNlZC5uZXh0KHRoaXMuaXRlbSk7XHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25JdGVtQ29sbGFwc2VUb2dnbGVkLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEV4cGFuZCB0aGUgY29sbGFwc2FibGUgbmF2aWdhdGlvblxyXG4gICAqL1xyXG4gIGV4cGFuZCgpOiB2b2lkIHtcclxuICAgIGlmICh0aGlzLmlzT3Blbikge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5pc09wZW4gPSB0cnVlO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuXHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25JdGVtQ29sbGFwc2VUb2dnbGVkLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbGxhcHNlIHRoZSBjb2xsYXBzYWJsZSBuYXZpZ2F0aW9uXHJcbiAgICovXHJcbiAgY29sbGFwc2UoKTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMuaXNPcGVuKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmlzT3BlbiA9IGZhbHNlO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuXHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25JdGVtQ29sbGFwc2VUb2dnbGVkLm5leHQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENoZWNrIGlmIHRoZSBnaXZlbiBwYXJlbnQgaGFzIHRoZVxyXG4gICAqIGdpdmVuIGl0ZW0gaW4gb25lIG9mIGl0cyBjaGlsZHJlblxyXG4gICAqXHJcbiAgICogQHBhcmFtIHBhcmVudFxyXG4gICAqIEBwYXJhbSBpdGVtXHJcbiAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICovXHJcbiAgaXNDaGlsZHJlbk9mKHBhcmVudCwgaXRlbSk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKCFwYXJlbnQuY2hpbGRyZW4pIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlmIChwYXJlbnQuY2hpbGRyZW4uaW5kZXhPZihpdGVtKSAhPT0gLTEpIHtcclxuICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChjb25zdCBjaGlsZHJlbiBvZiBwYXJlbnQuY2hpbGRyZW4pIHtcclxuICAgICAgaWYgKGNoaWxkcmVuLmNoaWxkcmVuKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNDaGlsZHJlbk9mKGNoaWxkcmVuLCBpdGVtKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2hlY2sgaWYgdGhlIGdpdmVuIHVybCBjYW4gYmUgZm91bmRcclxuICAgKiBpbiBvbmUgb2YgdGhlIGdpdmVuIHBhcmVudCdzIGNoaWxkcmVuXHJcbiAgICpcclxuICAgKiBAcGFyYW0gcGFyZW50XHJcbiAgICogQHBhcmFtIHVybFxyXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxyXG4gICAqL1xyXG4gIGlzVXJsSW5DaGlsZHJlbihwYXJlbnQsIHVybCk6IGJvb2xlYW4ge1xyXG4gICAgaWYgKCFwYXJlbnQuY2hpbGRyZW4pIHtcclxuICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcGFyZW50LmNoaWxkcmVuLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGlmIChwYXJlbnQuY2hpbGRyZW5baV0uY2hpbGRyZW4pIHtcclxuICAgICAgICBpZiAodGhpcy5pc1VybEluQ2hpbGRyZW4ocGFyZW50LmNoaWxkcmVuW2ldLCB1cmwpKSB7XHJcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChcclxuICAgICAgICBwYXJlbnQuY2hpbGRyZW5baV0udXJsID09PSB1cmwgfHxcclxuICAgICAgICB1cmwuaW5jbHVkZXMocGFyZW50LmNoaWxkcmVuW2ldLnVybClcclxuICAgICAgKSB7XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfVxyXG59XHJcbiJdfQ==