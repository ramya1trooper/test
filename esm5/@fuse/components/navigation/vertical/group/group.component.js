import { ChangeDetectorRef, Component, HostBinding, Input } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseNavigationService } from '../../../../../@fuse/components/navigation/navigation.service';
var FuseNavVerticalGroupComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    function FuseNavVerticalGroupComponent(_changeDetectorRef, _fuseNavigationService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this.classes = 'nav-group nav-item';
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    FuseNavVerticalGroupComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved).pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            // Mark for check
            _this._changeDetectorRef.markForCheck();
        });
    };
    /**
     * On destroy
     */
    FuseNavVerticalGroupComponent.prototype.ngOnDestroy = function () {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    FuseNavVerticalGroupComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuse-nav-vertical-group',
                    template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n    <div class=\"group-title\" [ngClass]=\"item.classes\">\r\n        <span class=\"hint-text\" [translate]=\"item.translate\">{{ item.title }}</span>\r\n    </div>\r\n\r\n    <div class=\"group-items\">\r\n        <ng-container *ngFor=\"let item of item.children\">\r\n            <fuse-nav-vertical-group *ngIf=\"item.type=='group'\" [item]=\"item\"></fuse-nav-vertical-group>\r\n            <fuse-nav-vertical-collapsable *ngIf=\"item.type=='collapsable'\"\r\n                                           [item]=\"item\"></fuse-nav-vertical-collapsable>\r\n            <fuse-nav-vertical-item *ngIf=\"item.type=='item'\" [item]=\"item\"></fuse-nav-vertical-item>\r\n        </ng-container>\r\n    </div>\r\n\r\n</ng-container>",
                    styles: [".folded:not(.unfolded) :host>.group-title{-webkit-box-align:center;align-items:center}.folded:not(.unfolded) :host>.group-title>span{opacity:0;-webkit-transition:opacity .2s;transition:opacity .2s}.folded:not(.unfolded) :host>.group-title:before{content:'';display:block;position:absolute;min-width:1.6rem;border-top:2px solid;opacity:.2}"]
                }] }
    ];
    /** @nocollapse */
    FuseNavVerticalGroupComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef },
        { type: FuseNavigationService }
    ]; };
    FuseNavVerticalGroupComponent.propDecorators = {
        classes: [{ type: HostBinding, args: ['class',] }],
        item: [{ type: Input }]
    };
    return FuseNavVerticalGroupComponent;
}());
export { FuseNavVerticalGroupComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi92ZXJ0aWNhbC9ncm91cC9ncm91cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNwRyxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHM0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFFdEc7SUFnQkk7O09BRUc7SUFFSDs7OztPQUlHO0lBQ0gsdUNBQ1ksa0JBQXFDLEVBQ3JDLHNCQUE2QztRQUQ3Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO1FBQ3JDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBdUI7UUFuQnpELFlBQU8sR0FBRyxvQkFBb0IsQ0FBQztRQXNCM0IsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxnREFBUSxHQUFSO1FBQUEsaUJBYUM7UUFYRywrQkFBK0I7UUFDL0IsS0FBSyxDQUNELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsRUFDakQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixFQUNuRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQ3RELENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDO1lBRVAsaUJBQWlCO1lBQ2pCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMzQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFRDs7T0FFRztJQUNILG1EQUFXLEdBQVg7UUFFSSxxQ0FBcUM7UUFDckMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BDLENBQUM7O2dCQWhFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFLLHlCQUF5QjtvQkFDdEMsZ3hCQUFxQzs7aUJBRXhDOzs7O2dCQVhRLGlCQUFpQjtnQkFLakIscUJBQXFCOzs7MEJBU3pCLFdBQVcsU0FBQyxPQUFPO3VCQUduQixLQUFLOztJQXdEVixvQ0FBQztDQUFBLEFBbEVELElBa0VDO1NBN0RZLDZCQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENoYW5nZURldGVjdG9yUmVmLCBDb21wb25lbnQsIEhvc3RCaW5kaW5nLCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgbWVyZ2UsIFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25JdGVtIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vQGZ1c2UvdHlwZXMnO1xyXG5pbXBvcnQgeyBGdXNlTmF2aWdhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3IgICA6ICdmdXNlLW5hdi12ZXJ0aWNhbC1ncm91cCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZ3JvdXAuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzICA6IFsnLi9ncm91cC5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlTmF2VmVydGljYWxHcm91cENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95XHJcbntcclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MnKVxyXG4gICAgY2xhc3NlcyA9ICduYXYtZ3JvdXAgbmF2LWl0ZW0nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBpdGVtOiBGdXNlTmF2aWdhdGlvbkl0ZW07XHJcblxyXG4gICAgLy8gUHJpdmF0ZVxyXG4gICAgcHJpdmF0ZSBfdW5zdWJzY3JpYmVBbGw6IFN1YmplY3Q8YW55PjtcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnN0cnVjdG9yXHJcbiAgICAgKi9cclxuXHJcbiAgICAvKipcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge0NoYW5nZURldGVjdG9yUmVmfSBfY2hhbmdlRGV0ZWN0b3JSZWZcclxuICAgICAqIEBwYXJhbSB7RnVzZU5hdmlnYXRpb25TZXJ2aWNlfSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX2NoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgICAgICBwcml2YXRlIF9mdXNlTmF2aWdhdGlvblNlcnZpY2U6IEZ1c2VOYXZpZ2F0aW9uU2VydmljZVxyXG4gICAgKVxyXG4gICAge1xyXG4gICAgICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPbiBpbml0XHJcbiAgICAgKi9cclxuICAgIG5nT25Jbml0KCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICAvLyBTdWJzY3JpYmUgdG8gbmF2aWdhdGlvbiBpdGVtXHJcbiAgICAgICAgbWVyZ2UoXHJcbiAgICAgICAgICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbk5hdmlnYXRpb25JdGVtQWRkZWQsXHJcbiAgICAgICAgICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbk5hdmlnYXRpb25JdGVtVXBkYXRlZCxcclxuICAgICAgICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1SZW1vdmVkXHJcbiAgICAgICAgKS5waXBlKHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG5cclxuICAgICAgICAgICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICAgICAgICAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPbiBkZXN0cm95XHJcbiAgICAgKi9cclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICAvLyBVbnN1YnNjcmliZSBmcm9tIGFsbCBzdWJzY3JpcHRpb25zXHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwubmV4dCgpO1xyXG4gICAgICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==