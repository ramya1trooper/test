import { Component, HostBinding, Input } from '@angular/core';
var FuseNavHorizontalItemComponent = /** @class */ (function () {
    /**
     * Constructor
     */
    function FuseNavHorizontalItemComponent() {
        this.classes = 'nav-item';
    }
    FuseNavHorizontalItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuse-nav-horizontal-item',
                    template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n    <!-- item.url -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && !item.function\"\r\n       [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n       [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\"\r\n       [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.externalUrl -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && !item.function\"\r\n       [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.function -->\r\n    <span class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && item.function\"\r\n          (click)=\"item.function()\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </span>\r\n\r\n    <!-- item.url && item.function -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && item.function\"\r\n       (click)=\"item.function()\"\r\n       [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n       [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\"\r\n       [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.externalUrl && item.function -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && item.function\"\r\n       (click)=\"item.function()\"\r\n       [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <ng-template #itemContent>\r\n        <mat-icon class=\"nav-link-icon\" *ngIf=\"item.icon\">{{item.icon}}</mat-icon>\r\n        <span class=\"nav-link-title\" [translate]=\"item.translate\">{{item.title}}</span>\r\n        <span class=\"nav-link-badge\" *ngIf=\"item.badge\" [translate]=\"item.badge.translate\"\r\n              [ngStyle]=\"{'background-color': item.badge.bg,'color': item.badge.fg}\">\r\n            {{item.badge.title}}\r\n        </span>\r\n    </ng-template>\r\n\r\n</ng-container>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    FuseNavHorizontalItemComponent.ctorParameters = function () { return []; };
    FuseNavHorizontalItemComponent.propDecorators = {
        classes: [{ type: HostBinding, args: ['class',] }],
        item: [{ type: Input }]
    };
    return FuseNavHorizontalItemComponent;
}());
export { FuseNavHorizontalItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL2hvcml6b250YWwvaXRlbS9pdGVtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFdBQVcsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFOUQ7SUFhSTs7T0FFRztJQUNIO1FBUkEsWUFBTyxHQUFHLFVBQVUsQ0FBQztJQVdyQixDQUFDOztnQkFuQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBSywwQkFBMEI7b0JBQ3ZDLHU2RUFBb0M7O2lCQUV2Qzs7Ozs7MEJBR0ksV0FBVyxTQUFDLE9BQU87dUJBR25CLEtBQUs7O0lBVVYscUNBQUM7Q0FBQSxBQXBCRCxJQW9CQztTQWZZLDhCQUE4QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yICAgOiAnZnVzZS1uYXYtaG9yaXpvbnRhbC1pdGVtJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9pdGVtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJscyAgOiBbJy4vaXRlbS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlTmF2SG9yaXpvbnRhbEl0ZW1Db21wb25lbnRcclxue1xyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcycpXHJcbiAgICBjbGFzc2VzID0gJ25hdi1pdGVtJztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgaXRlbTogYW55O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoKVxyXG4gICAge1xyXG5cclxuICAgIH1cclxufVxyXG4iXX0=