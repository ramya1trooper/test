import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import * as _ from "lodash";
import * as i0 from "@angular/core";
var FuseNavigationService = /** @class */ (function () {
    /**
     * Constructor
     */
    function FuseNavigationService() {
        this._registry = {};
        // Set the defaults
        this.onItemCollapsed = new Subject();
        this.onItemCollapseToggled = new Subject();
        // Set the private defaults
        this._currentNavigationKey = null;
        this._onNavigationChanged = new BehaviorSubject(null);
        this._onNavigationRegistered = new BehaviorSubject(null);
        this._onNavigationUnregistered = new BehaviorSubject(null);
        this._onNavigationItemAdded = new BehaviorSubject(null);
        this._onNavigationItemUpdated = new BehaviorSubject(null);
        this._onNavigationItemRemoved = new BehaviorSubject(null);
    }
    Object.defineProperty(FuseNavigationService.prototype, "onNavigationChanged", {
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        /**
         * Get onNavigationChanged
         *
         * @returns {Observable<any>}
         */
        get: function () {
            return this._onNavigationChanged.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseNavigationService.prototype, "onNavigationRegistered", {
        /**
         * Get onNavigationRegistered
         *
         * @returns {Observable<any>}
         */
        get: function () {
            return this._onNavigationRegistered.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseNavigationService.prototype, "onNavigationUnregistered", {
        /**
         * Get onNavigationUnregistered
         *
         * @returns {Observable<any>}
         */
        get: function () {
            return this._onNavigationUnregistered.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseNavigationService.prototype, "onNavigationItemAdded", {
        /**
         * Get onNavigationItemAdded
         *
         * @returns {Observable<any>}
         */
        get: function () {
            return this._onNavigationItemAdded.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseNavigationService.prototype, "onNavigationItemUpdated", {
        /**
         * Get onNavigationItemUpdated
         *
         * @returns {Observable<any>}
         */
        get: function () {
            return this._onNavigationItemUpdated.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FuseNavigationService.prototype, "onNavigationItemRemoved", {
        /**
         * Get onNavigationItemRemoved
         *
         * @returns {Observable<any>}
         */
        get: function () {
            return this._onNavigationItemRemoved.asObservable();
        },
        enumerable: true,
        configurable: true
    });
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Register the given navigation
     * with the given key
     *
     * @param key
     * @param navigation
     */
    FuseNavigationService.prototype.register = function (key, navigation) {
        // Check if the key already being used
        if (this._registry[key]) {
            console.error("The navigation with the key '" + key + "' already exists. Either unregister it first or use a unique key.");
            return;
        }
        // Add to the registry
        this._registry[key] = navigation;
        // Notify the subject
        this._onNavigationRegistered.next([key, navigation]);
    };
    /**
     * Unregister the navigation from the registry
     * @param key
     */
    FuseNavigationService.prototype.unregister = function (key) {
        // Check if the navigation exists
        if (!this._registry[key]) {
            console.warn("The navigation with the key '" + key + "' doesn't exist in the registry.");
        }
        // Unregister the sidebar
        delete this._registry[key];
        // Notify the subject
        this._onNavigationUnregistered.next(key);
    };
    /**
     * Get navigation from registry by key
     *
     * @param key
     * @returns {any}
     */
    FuseNavigationService.prototype.getNavigation = function (key) {
        // Check if the navigation exists
        if (!this._registry[key]) {
            console.warn("The navigation with the key '" + key + "' doesn't exist in the registry.");
            return;
        }
        // Return the sidebar
        return this._registry[key];
    };
    /**
     * Get flattened navigation array
     *
     * @param navigation
     * @param flatNavigation
     * @returns {any[]}
     */
    FuseNavigationService.prototype.getFlatNavigation = function (navigation, flatNavigation) {
        if (flatNavigation === void 0) { flatNavigation = []; }
        var e_1, _a;
        try {
            for (var navigation_1 = tslib_1.__values(navigation), navigation_1_1 = navigation_1.next(); !navigation_1_1.done; navigation_1_1 = navigation_1.next()) {
                var item = navigation_1_1.value;
                if (item.type === "item") {
                    flatNavigation.push(item);
                    continue;
                }
                if (item.type === "collapsable" || item.type === "group") {
                    if (item.children) {
                        this.getFlatNavigation(item.children, flatNavigation);
                    }
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (navigation_1_1 && !navigation_1_1.done && (_a = navigation_1.return)) _a.call(navigation_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return flatNavigation;
    };
    /**
     * Get the current navigation
     *
     * @returns {any}
     */
    FuseNavigationService.prototype.getCurrentNavigation = function () {
        if (!this._currentNavigationKey) {
            console.warn("The current navigation is not set.");
            return;
        }
        return this.getNavigation(this._currentNavigationKey);
    };
    /**
     * Set the navigation with the key
     * as the current navigation
     *
     * @param key
     */
    FuseNavigationService.prototype.setCurrentNavigation = function (key) {
        // Check if the sidebar exists
        if (!this._registry[key]) {
            console.warn("The navigation with the key '" + key + "' doesn't exist in the registry.");
            return;
        }
        // Set the current navigation key
        this._currentNavigationKey = key;
        // Notify the subject
        this._onNavigationChanged.next(key);
    };
    /**
     * Get navigation item by id from the
     * current navigation
     *
     * @param id
     * @param {any} navigation
     * @returns {any | boolean}
     */
    FuseNavigationService.prototype.getNavigationItem = function (id, navigation) {
        if (navigation === void 0) { navigation = null; }
        var e_2, _a;
        if (!navigation) {
            navigation = this.getCurrentNavigation();
        }
        try {
            for (var navigation_2 = tslib_1.__values(navigation), navigation_2_1 = navigation_2.next(); !navigation_2_1.done; navigation_2_1 = navigation_2.next()) {
                var item = navigation_2_1.value;
                if (item.id === id) {
                    return item;
                }
                if (item.children) {
                    var childItem = this.getNavigationItem(id, item.children);
                    if (childItem) {
                        return childItem;
                    }
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (navigation_2_1 && !navigation_2_1.done && (_a = navigation_2.return)) _a.call(navigation_2);
            }
            finally { if (e_2) throw e_2.error; }
        }
        return false;
    };
    /**
     * Get the parent of the navigation item
     * with the id
     *
     * @param id
     * @param {any} navigation
     * @param parent
     */
    FuseNavigationService.prototype.getNavigationItemParent = function (id, navigation, parent) {
        if (navigation === void 0) { navigation = null; }
        if (parent === void 0) { parent = null; }
        var e_3, _a;
        if (!navigation) {
            navigation = this.getCurrentNavigation();
            parent = navigation;
        }
        try {
            for (var navigation_3 = tslib_1.__values(navigation), navigation_3_1 = navigation_3.next(); !navigation_3_1.done; navigation_3_1 = navigation_3.next()) {
                var item = navigation_3_1.value;
                if (item.id === id) {
                    return parent;
                }
                if (item.children) {
                    var childItem = this.getNavigationItemParent(id, item.children, item);
                    if (childItem) {
                        return childItem;
                    }
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (navigation_3_1 && !navigation_3_1.done && (_a = navigation_3.return)) _a.call(navigation_3);
            }
            finally { if (e_3) throw e_3.error; }
        }
        return false;
    };
    /**
     * Add a navigation item to the specified location
     *
     * @param item
     * @param id
     */
    FuseNavigationService.prototype.addNavigationItem = function (item, id) {
        // Get the current navigation
        var navigation = this.getCurrentNavigation();
        // Add to the end of the navigation
        if (id === "end") {
            navigation.push(item);
            // Trigger the observable
            this._onNavigationItemAdded.next(true);
            return;
        }
        // Add to the start of the navigation
        if (id === "start") {
            navigation.unshift(item);
            // Trigger the observable
            this._onNavigationItemAdded.next(true);
            return;
        }
        // Add it to a specific location
        var parent = this.getNavigationItem(id);
        if (parent) {
            // Check if parent has a children entry,
            // and add it if it doesn't
            if (!parent.children) {
                parent.children = [];
            }
            // Add the item
            parent.children.push(item);
        }
        // Trigger the observable
        this._onNavigationItemAdded.next(true);
    };
    /**
     * Update navigation item with the given id
     *
     * @param id
     * @param properties
     */
    FuseNavigationService.prototype.updateNavigationItem = function (id, properties) {
        // Get the navigation item
        var navigationItem = this.getNavigationItem(id);
        // If there is no navigation with the give id, return
        if (!navigationItem) {
            return;
        }
        // Merge the navigation properties
        _.merge(navigationItem, properties);
        // Trigger the observable
        this._onNavigationItemUpdated.next(true);
    };
    /**
     * Remove navigation item with the given id
     *
     * @param id
     */
    FuseNavigationService.prototype.removeNavigationItem = function (id) {
        var item = this.getNavigationItem(id);
        // Return, if there is not such an item
        if (!item) {
            return;
        }
        // Get the parent of the item
        var parent = this.getNavigationItemParent(id);
        // This check is required because of the first level
        // of the navigation, since the first level is not
        // inside the 'children' array
        parent = parent.children || parent;
        // Remove the item
        parent.splice(parent.indexOf(item), 1);
        // Trigger the observable
        this._onNavigationItemRemoved.next(true);
    };
    FuseNavigationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    FuseNavigationService.ctorParameters = function () { return []; };
    FuseNavigationService.ngInjectableDef = i0.defineInjectable({ factory: function FuseNavigationService_Factory() { return new FuseNavigationService(); }, token: FuseNavigationService, providedIn: "root" });
    return FuseNavigationService;
}());
export { FuseNavigationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGVBQWUsRUFBYyxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUQsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7O0FBSTVCO0lBa0JFOztPQUVHO0lBQ0g7UUFMUSxjQUFTLEdBQTJCLEVBQUUsQ0FBQztRQU03QyxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBRTNDLDJCQUEyQjtRQUMzQixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO1FBQ2xDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLHlCQUF5QixHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNELElBQUksQ0FBQyxzQkFBc0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDMUQsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFXRCxzQkFBSSxzREFBbUI7UUFUdkIsd0dBQXdHO1FBQ3hHLGNBQWM7UUFDZCx3R0FBd0c7UUFFeEc7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDbEQsQ0FBQzs7O09BQUE7SUFPRCxzQkFBSSx5REFBc0I7UUFMMUI7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDckQsQ0FBQzs7O09BQUE7SUFPRCxzQkFBSSwyREFBd0I7UUFMNUI7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMseUJBQXlCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdkQsQ0FBQzs7O09BQUE7SUFPRCxzQkFBSSx3REFBcUI7UUFMekI7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsc0JBQXNCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEQsQ0FBQzs7O09BQUE7SUFPRCxzQkFBSSwwREFBdUI7UUFMM0I7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdEQsQ0FBQzs7O09BQUE7SUFPRCxzQkFBSSwwREFBdUI7UUFMM0I7Ozs7V0FJRzthQUNIO1lBQ0UsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdEQsQ0FBQzs7O09BQUE7SUFFRCx3R0FBd0c7SUFDeEcsbUJBQW1CO0lBQ25CLHdHQUF3RztJQUV4Rzs7Ozs7O09BTUc7SUFDSCx3Q0FBUSxHQUFSLFVBQVMsR0FBRyxFQUFFLFVBQVU7UUFDdEIsc0NBQXNDO1FBQ3RDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN2QixPQUFPLENBQUMsS0FBSyxDQUNYLGtDQUFnQyxHQUFHLHNFQUFtRSxDQUN2RyxDQUFDO1lBRUYsT0FBTztTQUNSO1FBRUQsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsVUFBVSxDQUFDO1FBRWpDLHFCQUFxQjtRQUNyQixJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVEOzs7T0FHRztJQUNILDBDQUFVLEdBQVYsVUFBVyxHQUFHO1FBQ1osaUNBQWlDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQ1Ysa0NBQWdDLEdBQUcscUNBQWtDLENBQ3RFLENBQUM7U0FDSDtRQUVELHlCQUF5QjtRQUN6QixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFM0IscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDM0MsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsNkNBQWEsR0FBYixVQUFjLEdBQUc7UUFDZixpQ0FBaUM7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDeEIsT0FBTyxDQUFDLElBQUksQ0FDVixrQ0FBZ0MsR0FBRyxxQ0FBa0MsQ0FDdEUsQ0FBQztZQUVGLE9BQU87U0FDUjtRQUVELHFCQUFxQjtRQUNyQixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDN0IsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNILGlEQUFpQixHQUFqQixVQUNFLFVBQVUsRUFDVixjQUF5QztRQUF6QywrQkFBQSxFQUFBLG1CQUF5Qzs7O1lBRXpDLEtBQW1CLElBQUEsZUFBQSxpQkFBQSxVQUFVLENBQUEsc0NBQUEsOERBQUU7Z0JBQTFCLElBQU0sSUFBSSx1QkFBQTtnQkFDYixJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFO29CQUN4QixjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUUxQixTQUFTO2lCQUNWO2dCQUVELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxhQUFhLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLEVBQUU7b0JBQ3hELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTt3QkFDakIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7cUJBQ3ZEO2lCQUNGO2FBQ0Y7Ozs7Ozs7OztRQUVELE9BQU8sY0FBYyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsb0RBQW9CLEdBQXBCO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDLG9DQUFvQyxDQUFDLENBQUM7WUFFbkQsT0FBTztTQUNSO1FBRUQsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILG9EQUFvQixHQUFwQixVQUFxQixHQUFHO1FBQ3RCLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN4QixPQUFPLENBQUMsSUFBSSxDQUNWLGtDQUFnQyxHQUFHLHFDQUFrQyxDQUN0RSxDQUFDO1lBRUYsT0FBTztTQUNSO1FBRUQsaUNBQWlDO1FBQ2pDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUM7UUFFakMscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSCxpREFBaUIsR0FBakIsVUFBa0IsRUFBRSxFQUFFLFVBQWlCO1FBQWpCLDJCQUFBLEVBQUEsaUJBQWlCOztRQUNyQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsVUFBVSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBQzFDOztZQUVELEtBQW1CLElBQUEsZUFBQSxpQkFBQSxVQUFVLENBQUEsc0NBQUEsOERBQUU7Z0JBQTFCLElBQU0sSUFBSSx1QkFBQTtnQkFDYixJQUFJLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNsQixPQUFPLElBQUksQ0FBQztpQkFDYjtnQkFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ2pCLElBQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUU1RCxJQUFJLFNBQVMsRUFBRTt3QkFDYixPQUFPLFNBQVMsQ0FBQztxQkFDbEI7aUJBQ0Y7YUFDRjs7Ozs7Ozs7O1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNILHVEQUF1QixHQUF2QixVQUF3QixFQUFFLEVBQUUsVUFBaUIsRUFBRSxNQUFhO1FBQWhDLDJCQUFBLEVBQUEsaUJBQWlCO1FBQUUsdUJBQUEsRUFBQSxhQUFhOztRQUMxRCxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsVUFBVSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQ3pDLE1BQU0sR0FBRyxVQUFVLENBQUM7U0FDckI7O1lBRUQsS0FBbUIsSUFBQSxlQUFBLGlCQUFBLFVBQVUsQ0FBQSxzQ0FBQSw4REFBRTtnQkFBMUIsSUFBTSxJQUFJLHVCQUFBO2dCQUNiLElBQUksSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUU7b0JBQ2xCLE9BQU8sTUFBTSxDQUFDO2lCQUNmO2dCQUVELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDakIsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUV4RSxJQUFJLFNBQVMsRUFBRTt3QkFDYixPQUFPLFNBQVMsQ0FBQztxQkFDbEI7aUJBQ0Y7YUFDRjs7Ozs7Ozs7O1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxpREFBaUIsR0FBakIsVUFBa0IsSUFBSSxFQUFFLEVBQUU7UUFDeEIsNkJBQTZCO1FBQzdCLElBQU0sVUFBVSxHQUFVLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1FBRXRELG1DQUFtQztRQUNuQyxJQUFJLEVBQUUsS0FBSyxLQUFLLEVBQUU7WUFDaEIsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV0Qix5QkFBeUI7WUFDekIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV2QyxPQUFPO1NBQ1I7UUFFRCxxQ0FBcUM7UUFDckMsSUFBSSxFQUFFLEtBQUssT0FBTyxFQUFFO1lBQ2xCLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFekIseUJBQXlCO1lBQ3pCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFdkMsT0FBTztTQUNSO1FBRUQsZ0NBQWdDO1FBQ2hDLElBQU0sTUFBTSxHQUFRLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUUvQyxJQUFJLE1BQU0sRUFBRTtZQUNWLHdDQUF3QztZQUN4QywyQkFBMkI7WUFDM0IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQ3BCLE1BQU0sQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO2FBQ3RCO1lBRUQsZUFBZTtZQUNmLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzVCO1FBRUQseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsb0RBQW9CLEdBQXBCLFVBQXFCLEVBQUUsRUFBRSxVQUFVO1FBQ2pDLDBCQUEwQjtRQUMxQixJQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRSxDQUFDLENBQUM7UUFFbEQscURBQXFEO1FBQ3JELElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDbkIsT0FBTztTQUNSO1FBRUQsa0NBQWtDO1FBQ2xDLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBRXBDLHlCQUF5QjtRQUN6QixJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsb0RBQW9CLEdBQXBCLFVBQXFCLEVBQUU7UUFDckIsSUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRXhDLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsT0FBTztTQUNSO1FBRUQsNkJBQTZCO1FBQzdCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU5QyxvREFBb0Q7UUFDcEQsa0RBQWtEO1FBQ2xELDhCQUE4QjtRQUM5QixNQUFNLEdBQUcsTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUM7UUFFbkMsa0JBQWtCO1FBQ2xCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUV2Qyx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDOztnQkFoWUYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7Ozs7Z0NBUkQ7Q0F1WUMsQUFqWUQsSUFpWUM7U0E5WFkscUJBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25JdGVtIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL3R5cGVzXCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogXCJyb290XCJcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB7XHJcbiAgb25JdGVtQ29sbGFwc2VkOiBTdWJqZWN0PGFueT47XHJcbiAgb25JdGVtQ29sbGFwc2VUb2dnbGVkOiBTdWJqZWN0PGFueT47XHJcblxyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF9vbk5hdmlnYXRpb25DaGFuZ2VkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICBwcml2YXRlIF9vbk5hdmlnYXRpb25SZWdpc3RlcmVkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICBwcml2YXRlIF9vbk5hdmlnYXRpb25VbnJlZ2lzdGVyZWQ6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gIHByaXZhdGUgX29uTmF2aWdhdGlvbkl0ZW1BZGRlZDogQmVoYXZpb3JTdWJqZWN0PGFueT47XHJcbiAgcHJpdmF0ZSBfb25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQ6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gIHByaXZhdGUgX29uTmF2aWdhdGlvbkl0ZW1SZW1vdmVkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuXHJcbiAgcHJpdmF0ZSBfY3VycmVudE5hdmlnYXRpb25LZXk6IHN0cmluZztcclxuICBwcml2YXRlIF9yZWdpc3RyeTogeyBba2V5OiBzdHJpbmddOiBhbnkgfSA9IHt9O1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5vbkl0ZW1Db2xsYXBzZWQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgdGhpcy5vbkl0ZW1Db2xsYXBzZVRvZ2dsZWQgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fY3VycmVudE5hdmlnYXRpb25LZXkgPSBudWxsO1xyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uQ2hhbmdlZCA9IG5ldyBCZWhhdmlvclN1YmplY3QobnVsbCk7XHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25SZWdpc3RlcmVkID0gbmV3IEJlaGF2aW9yU3ViamVjdChudWxsKTtcclxuICAgIHRoaXMuX29uTmF2aWdhdGlvblVucmVnaXN0ZXJlZCA9IG5ldyBCZWhhdmlvclN1YmplY3QobnVsbCk7XHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtQWRkZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbVJlbW92ZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIEFjY2Vzc29yc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25DaGFuZ2VkXHJcbiAgICpcclxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAqL1xyXG4gIGdldCBvbk5hdmlnYXRpb25DaGFuZ2VkKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fb25OYXZpZ2F0aW9uQ2hhbmdlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25SZWdpc3RlcmVkXHJcbiAgICpcclxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAqL1xyXG4gIGdldCBvbk5hdmlnYXRpb25SZWdpc3RlcmVkKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fb25OYXZpZ2F0aW9uUmVnaXN0ZXJlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25VbnJlZ2lzdGVyZWRcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICovXHJcbiAgZ2V0IG9uTmF2aWdhdGlvblVucmVnaXN0ZXJlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX29uTmF2aWdhdGlvblVucmVnaXN0ZXJlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25JdGVtQWRkZWRcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICovXHJcbiAgZ2V0IG9uTmF2aWdhdGlvbkl0ZW1BZGRlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX29uTmF2aWdhdGlvbkl0ZW1BZGRlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25JdGVtVXBkYXRlZFxyXG4gICAqXHJcbiAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgKi9cclxuICBnZXQgb25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiB0aGlzLl9vbk5hdmlnYXRpb25JdGVtVXBkYXRlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25JdGVtUmVtb3ZlZFxyXG4gICAqXHJcbiAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgKi9cclxuICBnZXQgb25OYXZpZ2F0aW9uSXRlbVJlbW92ZWQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiB0aGlzLl9vbk5hdmlnYXRpb25JdGVtUmVtb3ZlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIFJlZ2lzdGVyIHRoZSBnaXZlbiBuYXZpZ2F0aW9uXHJcbiAgICogd2l0aCB0aGUgZ2l2ZW4ga2V5XHJcbiAgICpcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICogQHBhcmFtIG5hdmlnYXRpb25cclxuICAgKi9cclxuICByZWdpc3RlcihrZXksIG5hdmlnYXRpb24pOiB2b2lkIHtcclxuICAgIC8vIENoZWNrIGlmIHRoZSBrZXkgYWxyZWFkeSBiZWluZyB1c2VkXHJcbiAgICBpZiAodGhpcy5fcmVnaXN0cnlba2V5XSkge1xyXG4gICAgICBjb25zb2xlLmVycm9yKFxyXG4gICAgICAgIGBUaGUgbmF2aWdhdGlvbiB3aXRoIHRoZSBrZXkgJyR7a2V5fScgYWxyZWFkeSBleGlzdHMuIEVpdGhlciB1bnJlZ2lzdGVyIGl0IGZpcnN0IG9yIHVzZSBhIHVuaXF1ZSBrZXkuYFxyXG4gICAgICApO1xyXG5cclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFkZCB0byB0aGUgcmVnaXN0cnlcclxuICAgIHRoaXMuX3JlZ2lzdHJ5W2tleV0gPSBuYXZpZ2F0aW9uO1xyXG5cclxuICAgIC8vIE5vdGlmeSB0aGUgc3ViamVjdFxyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uUmVnaXN0ZXJlZC5uZXh0KFtrZXksIG5hdmlnYXRpb25dKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFVucmVnaXN0ZXIgdGhlIG5hdmlnYXRpb24gZnJvbSB0aGUgcmVnaXN0cnlcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICovXHJcbiAgdW5yZWdpc3RlcihrZXkpOiB2b2lkIHtcclxuICAgIC8vIENoZWNrIGlmIHRoZSBuYXZpZ2F0aW9uIGV4aXN0c1xyXG4gICAgaWYgKCF0aGlzLl9yZWdpc3RyeVtrZXldKSB7XHJcbiAgICAgIGNvbnNvbGUud2FybihcclxuICAgICAgICBgVGhlIG5hdmlnYXRpb24gd2l0aCB0aGUga2V5ICcke2tleX0nIGRvZXNuJ3QgZXhpc3QgaW4gdGhlIHJlZ2lzdHJ5LmBcclxuICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBVbnJlZ2lzdGVyIHRoZSBzaWRlYmFyXHJcbiAgICBkZWxldGUgdGhpcy5fcmVnaXN0cnlba2V5XTtcclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHN1YmplY3RcclxuICAgIHRoaXMuX29uTmF2aWdhdGlvblVucmVnaXN0ZXJlZC5uZXh0KGtleSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgbmF2aWdhdGlvbiBmcm9tIHJlZ2lzdHJ5IGJ5IGtleVxyXG4gICAqXHJcbiAgICogQHBhcmFtIGtleVxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgZ2V0TmF2aWdhdGlvbihrZXkpOiBhbnkge1xyXG4gICAgLy8gQ2hlY2sgaWYgdGhlIG5hdmlnYXRpb24gZXhpc3RzXHJcbiAgICBpZiAoIXRoaXMuX3JlZ2lzdHJ5W2tleV0pIHtcclxuICAgICAgY29uc29sZS53YXJuKFxyXG4gICAgICAgIGBUaGUgbmF2aWdhdGlvbiB3aXRoIHRoZSBrZXkgJyR7a2V5fScgZG9lc24ndCBleGlzdCBpbiB0aGUgcmVnaXN0cnkuYFxyXG4gICAgICApO1xyXG5cclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJldHVybiB0aGUgc2lkZWJhclxyXG4gICAgcmV0dXJuIHRoaXMuX3JlZ2lzdHJ5W2tleV07XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgZmxhdHRlbmVkIG5hdmlnYXRpb24gYXJyYXlcclxuICAgKlxyXG4gICAqIEBwYXJhbSBuYXZpZ2F0aW9uXHJcbiAgICogQHBhcmFtIGZsYXROYXZpZ2F0aW9uXHJcbiAgICogQHJldHVybnMge2FueVtdfVxyXG4gICAqL1xyXG4gIGdldEZsYXROYXZpZ2F0aW9uKFxyXG4gICAgbmF2aWdhdGlvbixcclxuICAgIGZsYXROYXZpZ2F0aW9uOiBGdXNlTmF2aWdhdGlvbkl0ZW1bXSA9IFtdXHJcbiAgKTogYW55IHtcclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBuYXZpZ2F0aW9uKSB7XHJcbiAgICAgIGlmIChpdGVtLnR5cGUgPT09IFwiaXRlbVwiKSB7XHJcbiAgICAgICAgZmxhdE5hdmlnYXRpb24ucHVzaChpdGVtKTtcclxuXHJcbiAgICAgICAgY29udGludWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLnR5cGUgPT09IFwiY29sbGFwc2FibGVcIiB8fCBpdGVtLnR5cGUgPT09IFwiZ3JvdXBcIikge1xyXG4gICAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XHJcbiAgICAgICAgICB0aGlzLmdldEZsYXROYXZpZ2F0aW9uKGl0ZW0uY2hpbGRyZW4sIGZsYXROYXZpZ2F0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZmxhdE5hdmlnYXRpb247XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgdGhlIGN1cnJlbnQgbmF2aWdhdGlvblxyXG4gICAqXHJcbiAgICogQHJldHVybnMge2FueX1cclxuICAgKi9cclxuICBnZXRDdXJyZW50TmF2aWdhdGlvbigpOiBhbnkge1xyXG4gICAgaWYgKCF0aGlzLl9jdXJyZW50TmF2aWdhdGlvbktleSkge1xyXG4gICAgICBjb25zb2xlLndhcm4oYFRoZSBjdXJyZW50IG5hdmlnYXRpb24gaXMgbm90IHNldC5gKTtcclxuXHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcy5nZXROYXZpZ2F0aW9uKHRoaXMuX2N1cnJlbnROYXZpZ2F0aW9uS2V5KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldCB0aGUgbmF2aWdhdGlvbiB3aXRoIHRoZSBrZXlcclxuICAgKiBhcyB0aGUgY3VycmVudCBuYXZpZ2F0aW9uXHJcbiAgICpcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICovXHJcbiAgc2V0Q3VycmVudE5hdmlnYXRpb24oa2V5KTogdm9pZCB7XHJcbiAgICAvLyBDaGVjayBpZiB0aGUgc2lkZWJhciBleGlzdHNcclxuICAgIGlmICghdGhpcy5fcmVnaXN0cnlba2V5XSkge1xyXG4gICAgICBjb25zb2xlLndhcm4oXHJcbiAgICAgICAgYFRoZSBuYXZpZ2F0aW9uIHdpdGggdGhlIGtleSAnJHtrZXl9JyBkb2Vzbid0IGV4aXN0IGluIHRoZSByZWdpc3RyeS5gXHJcbiAgICAgICk7XHJcblxyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2V0IHRoZSBjdXJyZW50IG5hdmlnYXRpb24ga2V5XHJcbiAgICB0aGlzLl9jdXJyZW50TmF2aWdhdGlvbktleSA9IGtleTtcclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHN1YmplY3RcclxuICAgIHRoaXMuX29uTmF2aWdhdGlvbkNoYW5nZWQubmV4dChrZXkpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0IG5hdmlnYXRpb24gaXRlbSBieSBpZCBmcm9tIHRoZVxyXG4gICAqIGN1cnJlbnQgbmF2aWdhdGlvblxyXG4gICAqXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICogQHBhcmFtIHthbnl9IG5hdmlnYXRpb25cclxuICAgKiBAcmV0dXJucyB7YW55IHwgYm9vbGVhbn1cclxuICAgKi9cclxuICBnZXROYXZpZ2F0aW9uSXRlbShpZCwgbmF2aWdhdGlvbiA9IG51bGwpOiBhbnkgfCBib29sZWFuIHtcclxuICAgIGlmICghbmF2aWdhdGlvbikge1xyXG4gICAgICBuYXZpZ2F0aW9uID0gdGhpcy5nZXRDdXJyZW50TmF2aWdhdGlvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBuYXZpZ2F0aW9uKSB7XHJcbiAgICAgIGlmIChpdGVtLmlkID09PSBpZCkge1xyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaXRlbS5jaGlsZHJlbikge1xyXG4gICAgICAgIGNvbnN0IGNoaWxkSXRlbSA9IHRoaXMuZ2V0TmF2aWdhdGlvbkl0ZW0oaWQsIGl0ZW0uY2hpbGRyZW4pO1xyXG5cclxuICAgICAgICBpZiAoY2hpbGRJdGVtKSB7XHJcbiAgICAgICAgICByZXR1cm4gY2hpbGRJdGVtO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCB0aGUgcGFyZW50IG9mIHRoZSBuYXZpZ2F0aW9uIGl0ZW1cclxuICAgKiB3aXRoIHRoZSBpZFxyXG4gICAqXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICogQHBhcmFtIHthbnl9IG5hdmlnYXRpb25cclxuICAgKiBAcGFyYW0gcGFyZW50XHJcbiAgICovXHJcbiAgZ2V0TmF2aWdhdGlvbkl0ZW1QYXJlbnQoaWQsIG5hdmlnYXRpb24gPSBudWxsLCBwYXJlbnQgPSBudWxsKTogYW55IHtcclxuICAgIGlmICghbmF2aWdhdGlvbikge1xyXG4gICAgICBuYXZpZ2F0aW9uID0gdGhpcy5nZXRDdXJyZW50TmF2aWdhdGlvbigpO1xyXG4gICAgICBwYXJlbnQgPSBuYXZpZ2F0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBuYXZpZ2F0aW9uKSB7XHJcbiAgICAgIGlmIChpdGVtLmlkID09PSBpZCkge1xyXG4gICAgICAgIHJldHVybiBwYXJlbnQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XHJcbiAgICAgICAgY29uc3QgY2hpbGRJdGVtID0gdGhpcy5nZXROYXZpZ2F0aW9uSXRlbVBhcmVudChpZCwgaXRlbS5jaGlsZHJlbiwgaXRlbSk7XHJcblxyXG4gICAgICAgIGlmIChjaGlsZEl0ZW0pIHtcclxuICAgICAgICAgIHJldHVybiBjaGlsZEl0ZW07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQWRkIGEgbmF2aWdhdGlvbiBpdGVtIHRvIHRoZSBzcGVjaWZpZWQgbG9jYXRpb25cclxuICAgKlxyXG4gICAqIEBwYXJhbSBpdGVtXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICovXHJcbiAgYWRkTmF2aWdhdGlvbkl0ZW0oaXRlbSwgaWQpOiB2b2lkIHtcclxuICAgIC8vIEdldCB0aGUgY3VycmVudCBuYXZpZ2F0aW9uXHJcbiAgICBjb25zdCBuYXZpZ2F0aW9uOiBhbnlbXSA9IHRoaXMuZ2V0Q3VycmVudE5hdmlnYXRpb24oKTtcclxuXHJcbiAgICAvLyBBZGQgdG8gdGhlIGVuZCBvZiB0aGUgbmF2aWdhdGlvblxyXG4gICAgaWYgKGlkID09PSBcImVuZFwiKSB7XHJcbiAgICAgIG5hdmlnYXRpb24ucHVzaChpdGVtKTtcclxuXHJcbiAgICAgIC8vIFRyaWdnZXIgdGhlIG9ic2VydmFibGVcclxuICAgICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbUFkZGVkLm5leHQodHJ1ZSk7XHJcblxyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQWRkIHRvIHRoZSBzdGFydCBvZiB0aGUgbmF2aWdhdGlvblxyXG4gICAgaWYgKGlkID09PSBcInN0YXJ0XCIpIHtcclxuICAgICAgbmF2aWdhdGlvbi51bnNoaWZ0KGl0ZW0pO1xyXG5cclxuICAgICAgLy8gVHJpZ2dlciB0aGUgb2JzZXJ2YWJsZVxyXG4gICAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtQWRkZWQubmV4dCh0cnVlKTtcclxuXHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBZGQgaXQgdG8gYSBzcGVjaWZpYyBsb2NhdGlvblxyXG4gICAgY29uc3QgcGFyZW50OiBhbnkgPSB0aGlzLmdldE5hdmlnYXRpb25JdGVtKGlkKTtcclxuXHJcbiAgICBpZiAocGFyZW50KSB7XHJcbiAgICAgIC8vIENoZWNrIGlmIHBhcmVudCBoYXMgYSBjaGlsZHJlbiBlbnRyeSxcclxuICAgICAgLy8gYW5kIGFkZCBpdCBpZiBpdCBkb2Vzbid0XHJcbiAgICAgIGlmICghcGFyZW50LmNoaWxkcmVuKSB7XHJcbiAgICAgICAgcGFyZW50LmNoaWxkcmVuID0gW107XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIEFkZCB0aGUgaXRlbVxyXG4gICAgICBwYXJlbnQuY2hpbGRyZW4ucHVzaChpdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUcmlnZ2VyIHRoZSBvYnNlcnZhYmxlXHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtQWRkZWQubmV4dCh0cnVlKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFVwZGF0ZSBuYXZpZ2F0aW9uIGl0ZW0gd2l0aCB0aGUgZ2l2ZW4gaWRcclxuICAgKlxyXG4gICAqIEBwYXJhbSBpZFxyXG4gICAqIEBwYXJhbSBwcm9wZXJ0aWVzXHJcbiAgICovXHJcbiAgdXBkYXRlTmF2aWdhdGlvbkl0ZW0oaWQsIHByb3BlcnRpZXMpOiB2b2lkIHtcclxuICAgIC8vIEdldCB0aGUgbmF2aWdhdGlvbiBpdGVtXHJcbiAgICBjb25zdCBuYXZpZ2F0aW9uSXRlbSA9IHRoaXMuZ2V0TmF2aWdhdGlvbkl0ZW0oaWQpO1xyXG5cclxuICAgIC8vIElmIHRoZXJlIGlzIG5vIG5hdmlnYXRpb24gd2l0aCB0aGUgZ2l2ZSBpZCwgcmV0dXJuXHJcbiAgICBpZiAoIW5hdmlnYXRpb25JdGVtKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBNZXJnZSB0aGUgbmF2aWdhdGlvbiBwcm9wZXJ0aWVzXHJcbiAgICBfLm1lcmdlKG5hdmlnYXRpb25JdGVtLCBwcm9wZXJ0aWVzKTtcclxuXHJcbiAgICAvLyBUcmlnZ2VyIHRoZSBvYnNlcnZhYmxlXHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtVXBkYXRlZC5uZXh0KHRydWUpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmVtb3ZlIG5hdmlnYXRpb24gaXRlbSB3aXRoIHRoZSBnaXZlbiBpZFxyXG4gICAqXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICovXHJcbiAgcmVtb3ZlTmF2aWdhdGlvbkl0ZW0oaWQpOiB2b2lkIHtcclxuICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmdldE5hdmlnYXRpb25JdGVtKGlkKTtcclxuXHJcbiAgICAvLyBSZXR1cm4sIGlmIHRoZXJlIGlzIG5vdCBzdWNoIGFuIGl0ZW1cclxuICAgIGlmICghaXRlbSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gR2V0IHRoZSBwYXJlbnQgb2YgdGhlIGl0ZW1cclxuICAgIGxldCBwYXJlbnQgPSB0aGlzLmdldE5hdmlnYXRpb25JdGVtUGFyZW50KGlkKTtcclxuXHJcbiAgICAvLyBUaGlzIGNoZWNrIGlzIHJlcXVpcmVkIGJlY2F1c2Ugb2YgdGhlIGZpcnN0IGxldmVsXHJcbiAgICAvLyBvZiB0aGUgbmF2aWdhdGlvbiwgc2luY2UgdGhlIGZpcnN0IGxldmVsIGlzIG5vdFxyXG4gICAgLy8gaW5zaWRlIHRoZSAnY2hpbGRyZW4nIGFycmF5XHJcbiAgICBwYXJlbnQgPSBwYXJlbnQuY2hpbGRyZW4gfHwgcGFyZW50O1xyXG5cclxuICAgIC8vIFJlbW92ZSB0aGUgaXRlbVxyXG4gICAgcGFyZW50LnNwbGljZShwYXJlbnQuaW5kZXhPZihpdGVtKSwgMSk7XHJcblxyXG4gICAgLy8gVHJpZ2dlciB0aGUgb2JzZXJ2YWJsZVxyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbVJlbW92ZWQubmV4dCh0cnVlKTtcclxuICB9XHJcbn1cclxuIl19