import { NgModule } from '@angular/core';
import { FuseSidebarComponent } from './sidebar.component';
var FuseSidebarModule = /** @class */ (function () {
    function FuseSidebarModule() {
    }
    FuseSidebarModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        FuseSidebarComponent
                    ],
                    exports: [
                        FuseSidebarComponent
                    ]
                },] }
    ];
    return FuseSidebarModule;
}());
export { FuseSidebarModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9zaWRlYmFyL3NpZGViYXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekMsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFM0Q7SUFBQTtJQVVBLENBQUM7O2dCQVZBLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUU7d0JBQ1Ysb0JBQW9CO3FCQUN2QjtvQkFDRCxPQUFPLEVBQU87d0JBQ1Ysb0JBQW9CO3FCQUN2QjtpQkFDSjs7SUFHRCx3QkFBQztDQUFBLEFBVkQsSUFVQztTQUZZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2lkZWJhckNvbXBvbmVudCB9IGZyb20gJy4vc2lkZWJhci5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEZ1c2VTaWRlYmFyQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgZXhwb3J0cyAgICAgOiBbXHJcbiAgICAgICAgRnVzZVNpZGViYXJDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VTaWRlYmFyTW9kdWxlXHJcbntcclxufVxyXG4iXX0=