import { ChangeDetectorRef, Component, ElementRef, EventEmitter, HostBinding, HostListener, Input, Output, Renderer2, ViewEncapsulation } from "@angular/core";
import { animate, AnimationBuilder, style } from "@angular/animations";
import { ObservableMedia } from "@angular/flex-layout";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { FuseSidebarService } from "./sidebar.service";
import { FuseMatchMediaService } from "../../../@fuse/services/match-media.service";
import { FuseConfigService } from "../../../@fuse/services/config.service";
var FuseSidebarComponent = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {AnimationBuilder} _animationBuilder
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {ElementRef} _elementRef
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {ObservableMedia} _observableMedia
     * @param {Renderer2} _renderer
     */
    function FuseSidebarComponent(_animationBuilder, _changeDetectorRef, _elementRef, _fuseConfigService, _fuseMatchMediaService, _fuseSidebarService, _observableMedia, _renderer) {
        this._animationBuilder = _animationBuilder;
        this._changeDetectorRef = _changeDetectorRef;
        this._elementRef = _elementRef;
        this._fuseConfigService = _fuseConfigService;
        this._fuseMatchMediaService = _fuseMatchMediaService;
        this._fuseSidebarService = _fuseSidebarService;
        this._observableMedia = _observableMedia;
        this._renderer = _renderer;
        this._backdrop = null;
        // Set the defaults
        this.foldedAutoTriggerOnHover = true;
        this.foldedWidth = 64;
        this.foldedChanged = new EventEmitter();
        this.openedChanged = new EventEmitter();
        this.opened = false;
        this.position = "left";
        this.invisibleOverlay = false;
        // Set the private defaults
        this._animationsEnabled = false;
        this._folded = false;
        this._unsubscribeAll = new Subject();
    }
    Object.defineProperty(FuseSidebarComponent.prototype, "folded", {
        get: function () {
            return this._folded;
        },
        // -----------------------------------------------------------------------------------------------------
        // @ Accessors
        // -----------------------------------------------------------------------------------------------------
        /**
         * Folded
         *
         * @param {boolean} value
         */
        set: function (value) {
            // Set the folded
            this._folded = value;
            // Return if the sidebar is closed
            if (!this.opened) {
                return;
            }
            // Programmatically add/remove padding to the element
            // that comes after or before based on the position
            var sibling, styleRule;
            var styleValue = this.foldedWidth + "px";
            // Get the sibling and set the style rule
            if (this.position === "left") {
                sibling = this._elementRef.nativeElement.nextElementSibling;
                styleRule = "padding-left";
            }
            else {
                sibling = this._elementRef.nativeElement.previousElementSibling;
                styleRule = "padding-right";
            }
            // If there is no sibling, return...
            if (!sibling) {
                return;
            }
            // If folded...
            if (value) {
                // Fold the sidebar
                this.fold();
                // Set the folded width
                this._renderer.setStyle(this._elementRef.nativeElement, "width", styleValue);
                this._renderer.setStyle(this._elementRef.nativeElement, "min-width", styleValue);
                this._renderer.setStyle(this._elementRef.nativeElement, "max-width", styleValue);
                // Set the style and class
                this._renderer.setStyle(sibling, styleRule, styleValue);
                this._renderer.addClass(this._elementRef.nativeElement, "folded");
            }
            // If unfolded...
            else {
                // Unfold the sidebar
                this.unfold();
                // Remove the folded width
                this._renderer.removeStyle(this._elementRef.nativeElement, "width");
                this._renderer.removeStyle(this._elementRef.nativeElement, "min-width");
                this._renderer.removeStyle(this._elementRef.nativeElement, "max-width");
                // Remove the style and class
                this._renderer.removeStyle(sibling, styleRule);
                this._renderer.removeClass(this._elementRef.nativeElement, "folded");
            }
            // Emit the 'foldedChanged' event
            this.foldedChanged.emit(this.folded);
        },
        enumerable: true,
        configurable: true
    });
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    FuseSidebarComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function (config) {
            _this._fuseConfig = config;
        });
        // Register the sidebar
        this._fuseSidebarService.register(this.name, this);
        // Setup visibility
        this._setupVisibility();
        // Setup position
        this._setupPosition();
        // Setup lockedOpen
        this._setupLockedOpen();
        // Setup folded
        this._setupFolded();
    };
    /**
     * On destroy
     */
    FuseSidebarComponent.prototype.ngOnDestroy = function () {
        // If the sidebar is folded, unfold it to revert modifications
        if (this.folded) {
            this.unfold();
        }
        // Unregister the sidebar
        this._fuseSidebarService.unregister(this.name);
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Setup the visibility of the sidebar
     *
     * @private
     */
    FuseSidebarComponent.prototype._setupVisibility = function () {
        // Remove the existing box-shadow
        this._renderer.setStyle(this._elementRef.nativeElement, "box-shadow", "none");
        // Make the sidebar invisible
        this._renderer.setStyle(this._elementRef.nativeElement, "visibility", "hidden");
    };
    /**
     * Setup the sidebar position
     *
     * @private
     */
    FuseSidebarComponent.prototype._setupPosition = function () {
        // Add the correct class name to the sidebar
        // element depending on the position attribute
        if (this.position === "right") {
            this._renderer.addClass(this._elementRef.nativeElement, "right-positioned");
        }
        else {
            this._renderer.addClass(this._elementRef.nativeElement, "left-positioned");
        }
    };
    /**
     * Setup the lockedOpen handler
     *
     * @private
     */
    FuseSidebarComponent.prototype._setupLockedOpen = function () {
        var _this = this;
        // Return if the lockedOpen wasn't set
        if (!this.lockedOpen) {
            // Return
            return;
        }
        // Set the wasActive for the first time
        this._wasActive = false;
        // Set the wasFolded
        this._wasFolded = this.folded;
        // Show the sidebar
        this._showSidebar();
        // Act on every media change
        this._fuseMatchMediaService.onMediaChange
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(function () {
            // Get the active status
            var isActive = _this._observableMedia.isActive(_this.lockedOpen);
            // If the both status are the same, don't act
            if (_this._wasActive === isActive) {
                return;
            }
            // Activate the lockedOpen
            if (isActive) {
                // Set the lockedOpen status
                _this.isLockedOpen = true;
                // Show the sidebar
                _this._showSidebar();
                // Force the the opened status to true
                _this.opened = true;
                // Emit the 'openedChanged' event
                _this.openedChanged.emit(_this.opened);
                // If the sidebar was folded, forcefully fold it again
                if (_this._wasFolded) {
                    // Enable the animations
                    _this._enableAnimations();
                    // Fold
                    _this.folded = true;
                    // Mark for check
                    _this._changeDetectorRef.markForCheck();
                }
                // Hide the backdrop if any exists
                _this._hideBackdrop();
            }
            // De-Activate the lockedOpen
            else {
                // Set the lockedOpen status
                _this.isLockedOpen = false;
                // Unfold the sidebar in case if it was folded
                _this.unfold();
                // Force the the opened status to close
                _this.opened = false;
                // Emit the 'openedChanged' event
                _this.openedChanged.emit(_this.opened);
                // Hide the sidebar
                _this._hideSidebar();
            }
            // Store the new active status
            _this._wasActive = isActive;
        });
    };
    /**
     * Setup the initial folded status
     *
     * @private
     */
    FuseSidebarComponent.prototype._setupFolded = function () {
        // Return, if sidebar is not folded
        if (!this.folded) {
            return;
        }
        // Return if the sidebar is closed
        if (!this.opened) {
            return;
        }
        // Programmatically add/remove padding to the element
        // that comes after or before based on the position
        var sibling, styleRule;
        var styleValue = this.foldedWidth + "px";
        // Get the sibling and set the style rule
        if (this.position === "left") {
            sibling = this._elementRef.nativeElement.nextElementSibling;
            styleRule = "padding-left";
        }
        else {
            sibling = this._elementRef.nativeElement.previousElementSibling;
            styleRule = "padding-right";
        }
        // If there is no sibling, return...
        if (!sibling) {
            return;
        }
        // Fold the sidebar
        this.fold();
        // Set the folded width
        this._renderer.setStyle(this._elementRef.nativeElement, "width", styleValue);
        this._renderer.setStyle(this._elementRef.nativeElement, "min-width", styleValue);
        this._renderer.setStyle(this._elementRef.nativeElement, "max-width", styleValue);
        // Set the style and class
        this._renderer.setStyle(sibling, styleRule, styleValue);
        this._renderer.addClass(this._elementRef.nativeElement, "folded");
    };
    /**
     * Show the backdrop
     *
     * @private
     */
    FuseSidebarComponent.prototype._showBackdrop = function () {
        var _this = this;
        // Create the backdrop element
        this._backdrop = this._renderer.createElement("div");
        // Add a class to the backdrop element
        this._backdrop.classList.add("fuse-sidebar-overlay");
        // Add a class depending on the invisibleOverlay option
        if (this.invisibleOverlay) {
            this._backdrop.classList.add("fuse-sidebar-overlay-invisible");
        }
        // Append the backdrop to the parent of the sidebar
        this._renderer.appendChild(this._elementRef.nativeElement.parentElement, this._backdrop);
        // Create the enter animation and attach it to the player
        this._player = this._animationBuilder
            .build([animate("300ms ease", style({ opacity: 1 }))])
            .create(this._backdrop);
        // Play the animation
        this._player.play();
        // Add an event listener to the overlay
        this._backdrop.addEventListener("click", function () {
            _this.close();
        });
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Hide the backdrop
     *
     * @private
     */
    FuseSidebarComponent.prototype._hideBackdrop = function () {
        var _this = this;
        if (!this._backdrop) {
            return;
        }
        // Create the leave animation and attach it to the player
        this._player = this._animationBuilder
            .build([animate("300ms ease", style({ opacity: 0 }))])
            .create(this._backdrop);
        // Play the animation
        this._player.play();
        // Once the animation is done...
        this._player.onDone(function () {
            // If the backdrop still exists...
            if (_this._backdrop) {
                // Remove the backdrop
                _this._backdrop.parentNode.removeChild(_this._backdrop);
                _this._backdrop = null;
            }
        });
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Change some properties of the sidebar
     * and make it visible
     *
     * @private
     */
    FuseSidebarComponent.prototype._showSidebar = function () {
        // Remove the box-shadow style
        this._renderer.removeStyle(this._elementRef.nativeElement, "box-shadow");
        // Make the sidebar invisible
        this._renderer.removeStyle(this._elementRef.nativeElement, "visibility");
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Change some properties of the sidebar
     * and make it invisible
     *
     * @private
     */
    FuseSidebarComponent.prototype._hideSidebar = function (delay) {
        var _this = this;
        if (delay === void 0) { delay = true; }
        var delayAmount = delay ? 300 : 0;
        // Add a delay so close animation can play
        setTimeout(function () {
            // Remove the box-shadow
            _this._renderer.setStyle(_this._elementRef.nativeElement, "box-shadow", "none");
            // Make the sidebar invisible
            _this._renderer.setStyle(_this._elementRef.nativeElement, "visibility", "hidden");
        }, delayAmount);
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Enable the animations
     *
     * @private
     */
    FuseSidebarComponent.prototype._enableAnimations = function () {
        // Return if animations already enabled
        if (this._animationsEnabled) {
            return;
        }
        // Enable the animations
        this._animationsEnabled = true;
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Open the sidebar
     */
    FuseSidebarComponent.prototype.open = function () {
        if (this.opened || this.isLockedOpen) {
            return;
        }
        // Enable the animations
        this._enableAnimations();
        // Show the sidebar
        this._showSidebar();
        // Show the backdrop
        this._showBackdrop();
        // Set the opened status
        this.opened = true;
        // Emit the 'openedChanged' event
        this.openedChanged.emit(this.opened);
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Close the sidebar
     */
    FuseSidebarComponent.prototype.close = function () {
        if (!this.opened || this.isLockedOpen) {
            return;
        }
        // Enable the animations
        this._enableAnimations();
        // Hide the backdrop
        this._hideBackdrop();
        // Set the opened status
        this.opened = false;
        // Emit the 'openedChanged' event
        this.openedChanged.emit(this.opened);
        // Hide the sidebar
        this._hideSidebar();
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Toggle open/close the sidebar
     */
    FuseSidebarComponent.prototype.toggleOpen = function () {
        if (this.opened) {
            this.close();
        }
        else {
            this.open();
        }
    };
    /**
     * Mouseenter
     */
    FuseSidebarComponent.prototype.onMouseEnter = function () {
        // Only work if the auto trigger is enabled
        if (!this.foldedAutoTriggerOnHover) {
            return;
        }
        this.unfoldTemporarily();
    };
    /**
     * Mouseleave
     */
    FuseSidebarComponent.prototype.onMouseLeave = function () {
        // Only work if the auto trigger is enabled
        if (!this.foldedAutoTriggerOnHover) {
            return;
        }
        this.foldTemporarily();
    };
    /**
     * Fold the sidebar permanently
     */
    FuseSidebarComponent.prototype.fold = function () {
        // Only work if the sidebar is not folded
        if (this.folded) {
            return;
        }
        // Enable the animations
        this._enableAnimations();
        // Fold
        this.folded = true;
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Unfold the sidebar permanently
     */
    FuseSidebarComponent.prototype.unfold = function () {
        // Only work if the sidebar is folded
        if (!this.folded) {
            return;
        }
        // Enable the animations
        this._enableAnimations();
        // Unfold
        this.folded = false;
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Toggle the sidebar fold/unfold permanently
     */
    FuseSidebarComponent.prototype.toggleFold = function () {
        if (this.folded) {
            this.unfold();
        }
        else {
            this.fold();
        }
    };
    /**
     * Fold the temporarily unfolded sidebar back
     */
    FuseSidebarComponent.prototype.foldTemporarily = function () {
        // Only work if the sidebar is folded
        if (!this.folded) {
            return;
        }
        // Enable the animations
        this._enableAnimations();
        // Fold the sidebar back
        this.unfolded = false;
        // Set the folded width
        var styleValue = this.foldedWidth + "px";
        this._renderer.setStyle(this._elementRef.nativeElement, "width", styleValue);
        this._renderer.setStyle(this._elementRef.nativeElement, "min-width", styleValue);
        this._renderer.setStyle(this._elementRef.nativeElement, "max-width", styleValue);
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    /**
     * Unfold the sidebar temporarily
     */
    FuseSidebarComponent.prototype.unfoldTemporarily = function () {
        // Only work if the sidebar is folded
        if (!this.folded) {
            return;
        }
        // Enable the animations
        this._enableAnimations();
        // Unfold the sidebar temporarily
        this.unfolded = true;
        // Remove the folded width
        this._renderer.removeStyle(this._elementRef.nativeElement, "width");
        this._renderer.removeStyle(this._elementRef.nativeElement, "min-width");
        this._renderer.removeStyle(this._elementRef.nativeElement, "max-width");
        // Mark for check
        this._changeDetectorRef.markForCheck();
    };
    FuseSidebarComponent.decorators = [
        { type: Component, args: [{
                    selector: "fuse-sidebar",
                    template: "<ng-content></ng-content>",
                    encapsulation: ViewEncapsulation.None,
                    styles: ["fuse-sidebar{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 0 auto;position:absolute;top:0;bottom:0;overflow-x:hidden;overflow-y:auto;-webkit-overflow-scrolling:touch;width:280px;min-width:280px;max-width:280px;z-index:1000;box-shadow:0 2px 8px 0 rgba(0,0,0,.35)}@media screen and (max-width:599px){fuse-sidebar{min-width:0!important;max-width:80vw!important;width:80vw!important}}fuse-sidebar.left-positioned{left:0;-webkit-transform:translateX(-100%);transform:translateX(-100%)}fuse-sidebar.right-positioned{right:0;-webkit-transform:translateX(100%);transform:translateX(100%)}fuse-sidebar.open{-webkit-transform:translateX(0);transform:translateX(0)}fuse-sidebar.locked-open{position:relative!important;-webkit-transform:translateX(0)!important;transform:translateX(0)!important}fuse-sidebar.folded{position:absolute!important;top:0;bottom:0}fuse-sidebar.animations-enabled{-webkit-transition-property:width,min-width,max-width,-webkit-transform;transition-property:transform,width,min-width,max-width,-webkit-transform;-webkit-transition-duration:150ms;transition-duration:150ms;-webkit-transition-timing-function:ease-in-out;transition-timing-function:ease-in-out}.fuse-sidebar-overlay{position:absolute;top:0;bottom:0;left:0;right:0;z-index:999;opacity:0}"]
                }] }
    ];
    /** @nocollapse */
    FuseSidebarComponent.ctorParameters = function () { return [
        { type: AnimationBuilder },
        { type: ChangeDetectorRef },
        { type: ElementRef },
        { type: FuseConfigService },
        { type: FuseMatchMediaService },
        { type: FuseSidebarService },
        { type: ObservableMedia },
        { type: Renderer2 }
    ]; };
    FuseSidebarComponent.propDecorators = {
        name: [{ type: Input }],
        key: [{ type: Input }],
        position: [{ type: Input }],
        opened: [{ type: HostBinding, args: ["class.open",] }],
        lockedOpen: [{ type: Input }],
        isLockedOpen: [{ type: HostBinding, args: ["class.locked-open",] }],
        foldedWidth: [{ type: Input }],
        foldedAutoTriggerOnHover: [{ type: Input }],
        unfolded: [{ type: HostBinding, args: ["class.unfolded",] }],
        invisibleOverlay: [{ type: Input }],
        foldedChanged: [{ type: Output }],
        openedChanged: [{ type: Output }],
        _animationsEnabled: [{ type: HostBinding, args: ["class.animations-enabled",] }],
        folded: [{ type: Input }],
        onMouseEnter: [{ type: HostListener, args: ["mouseenter",] }],
        onMouseLeave: [{ type: HostListener, args: ["mouseleave",] }]
    };
    return FuseSidebarComponent;
}());
export { FuseSidebarComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9zaWRlYmFyL3NpZGViYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxpQkFBaUIsRUFDakIsU0FBUyxFQUNULFVBQVUsRUFDVixZQUFZLEVBQ1osV0FBVyxFQUNYLFlBQVksRUFDWixLQUFLLEVBR0wsTUFBTSxFQUNOLFNBQVMsRUFDVCxpQkFBaUIsRUFDbEIsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUNMLE9BQU8sRUFDUCxnQkFBZ0IsRUFFaEIsS0FBSyxFQUNOLE1BQU0scUJBQXFCLENBQUM7QUFDN0IsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBRTNFO0lBbUVFOzs7Ozs7Ozs7OztPQVdHO0lBQ0gsOEJBQ1UsaUJBQW1DLEVBQ25DLGtCQUFxQyxFQUNyQyxXQUF1QixFQUN2QixrQkFBcUMsRUFDckMsc0JBQTZDLEVBQzdDLG1CQUF1QyxFQUN2QyxnQkFBaUMsRUFDakMsU0FBb0I7UUFQcEIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFrQjtRQUNuQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO1FBQ3JDLGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBbUI7UUFDckMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF1QjtRQUM3Qyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQW9CO1FBQ3ZDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFDakMsY0FBUyxHQUFULFNBQVMsQ0FBVztRQTNCdEIsY0FBUyxHQUF1QixJQUFJLENBQUM7UUE2QjNDLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7UUFDdkIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztRQUU5QiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztRQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQVdELHNCQUNJLHdDQUFNO2FBMEVWO1lBQ0UsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3RCLENBQUM7UUF0RkQsd0dBQXdHO1FBQ3hHLGNBQWM7UUFDZCx3R0FBd0c7UUFFeEc7Ozs7V0FJRzthQUNILFVBQ1csS0FBYztZQUN2QixpQkFBaUI7WUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFFckIsa0NBQWtDO1lBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNoQixPQUFPO2FBQ1I7WUFFRCxxREFBcUQ7WUFDckQsbURBQW1EO1lBQ25ELElBQUksT0FBTyxFQUFFLFNBQVMsQ0FBQztZQUV2QixJQUFNLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUUzQyx5Q0FBeUM7WUFDekMsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE1BQU0sRUFBRTtnQkFDNUIsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDO2dCQUM1RCxTQUFTLEdBQUcsY0FBYyxDQUFDO2FBQzVCO2lCQUFNO2dCQUNMLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQztnQkFDaEUsU0FBUyxHQUFHLGVBQWUsQ0FBQzthQUM3QjtZQUVELG9DQUFvQztZQUNwQyxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNaLE9BQU87YUFDUjtZQUVELGVBQWU7WUFDZixJQUFJLEtBQUssRUFBRTtnQkFDVCxtQkFBbUI7Z0JBQ25CLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQkFFWix1QkFBdUI7Z0JBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFDOUIsT0FBTyxFQUNQLFVBQVUsQ0FDWCxDQUFDO2dCQUNGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFDOUIsV0FBVyxFQUNYLFVBQVUsQ0FDWCxDQUFDO2dCQUNGLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFDOUIsV0FBVyxFQUNYLFVBQVUsQ0FDWCxDQUFDO2dCQUVGLDBCQUEwQjtnQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDbkU7WUFDRCxpQkFBaUI7aUJBQ1o7Z0JBQ0gscUJBQXFCO2dCQUNyQixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBRWQsMEJBQTBCO2dCQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDcEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3hFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUV4RSw2QkFBNkI7Z0JBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsUUFBUSxDQUFDLENBQUM7YUFDdEU7WUFFRCxpQ0FBaUM7WUFDakMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZDLENBQUM7OztPQUFBO0lBTUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCx1Q0FBUSxHQUFSO1FBQUEsaUJBc0JDO1FBckJDLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTTthQUMzQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNyQyxTQUFTLENBQUMsVUFBQSxNQUFNO1lBQ2YsS0FBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFFTCx1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBRW5ELG1CQUFtQjtRQUNuQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUV4QixpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXRCLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUV4QixlQUFlO1FBQ2YsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7T0FFRztJQUNILDBDQUFXLEdBQVg7UUFDRSw4REFBOEQ7UUFDOUQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2Y7UUFFRCx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFL0MscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNLLCtDQUFnQixHQUF4QjtRQUNFLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFlBQVksRUFDWixNQUFNLENBQ1AsQ0FBQztRQUVGLDZCQUE2QjtRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFlBQVksRUFDWixRQUFRLENBQ1QsQ0FBQztJQUNKLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ssNkNBQWMsR0FBdEI7UUFDRSw0Q0FBNEM7UUFDNUMsOENBQThDO1FBQzlDLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxPQUFPLEVBQUU7WUFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUM5QixrQkFBa0IsQ0FDbkIsQ0FBQztTQUNIO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLGlCQUFpQixDQUNsQixDQUFDO1NBQ0g7SUFDSCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLCtDQUFnQixHQUF4QjtRQUFBLGlCQThFQztRQTdFQyxzQ0FBc0M7UUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEIsU0FBUztZQUNULE9BQU87U0FDUjtRQUVELHVDQUF1QztRQUN2QyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUV4QixvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBRTlCLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFcEIsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxhQUFhO2FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQztZQUNULHdCQUF3QjtZQUN4QixJQUFNLFFBQVEsR0FBRyxLQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUVqRSw2Q0FBNkM7WUFDN0MsSUFBSSxLQUFJLENBQUMsVUFBVSxLQUFLLFFBQVEsRUFBRTtnQkFDaEMsT0FBTzthQUNSO1lBRUQsMEJBQTBCO1lBQzFCLElBQUksUUFBUSxFQUFFO2dCQUNaLDRCQUE0QjtnQkFDNUIsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBRXpCLG1CQUFtQjtnQkFDbkIsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUVwQixzQ0FBc0M7Z0JBQ3RDLEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO2dCQUVuQixpQ0FBaUM7Z0JBQ2pDLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFFckMsc0RBQXNEO2dCQUN0RCxJQUFJLEtBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ25CLHdCQUF3QjtvQkFDeEIsS0FBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7b0JBRXpCLE9BQU87b0JBQ1AsS0FBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7b0JBRW5CLGlCQUFpQjtvQkFDakIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO2lCQUN4QztnQkFFRCxrQ0FBa0M7Z0JBQ2xDLEtBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUN0QjtZQUNELDZCQUE2QjtpQkFDeEI7Z0JBQ0gsNEJBQTRCO2dCQUM1QixLQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztnQkFFMUIsOENBQThDO2dCQUM5QyxLQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBRWQsdUNBQXVDO2dCQUN2QyxLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztnQkFFcEIsaUNBQWlDO2dCQUNqQyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBRXJDLG1CQUFtQjtnQkFDbkIsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDO2FBQ3JCO1lBRUQsOEJBQThCO1lBQzlCLEtBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7O09BSUc7SUFDSywyQ0FBWSxHQUFwQjtRQUNFLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixPQUFPO1NBQ1I7UUFFRCxrQ0FBa0M7UUFDbEMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTztTQUNSO1FBRUQscURBQXFEO1FBQ3JELG1EQUFtRDtRQUNuRCxJQUFJLE9BQU8sRUFBRSxTQUFTLENBQUM7UUFFdkIsSUFBTSxVQUFVLEdBQUcsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFFM0MseUNBQXlDO1FBQ3pDLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxNQUFNLEVBQUU7WUFDNUIsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLGtCQUFrQixDQUFDO1lBQzVELFNBQVMsR0FBRyxjQUFjLENBQUM7U0FDNUI7YUFBTTtZQUNMLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQztZQUNoRSxTQUFTLEdBQUcsZUFBZSxDQUFDO1NBQzdCO1FBRUQsb0NBQW9DO1FBQ3BDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDWixPQUFPO1NBQ1I7UUFFRCxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBRVosdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFDOUIsT0FBTyxFQUNQLFVBQVUsQ0FDWCxDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUM5QixXQUFXLEVBQ1gsVUFBVSxDQUNYLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFdBQVcsRUFDWCxVQUFVLENBQ1gsQ0FBQztRQUVGLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ssNENBQWEsR0FBckI7UUFBQSxpQkFpQ0M7UUFoQ0MsOEJBQThCO1FBQzlCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFckQsc0NBQXNDO1FBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBRXJELHVEQUF1RDtRQUN2RCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLENBQUMsQ0FBQztTQUNoRTtRQUVELG1EQUFtRDtRQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUM1QyxJQUFJLENBQUMsU0FBUyxDQUNmLENBQUM7UUFFRix5REFBeUQ7UUFDekQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsaUJBQWlCO2FBQ2xDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3JELE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFMUIscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFFcEIsdUNBQXVDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxFQUFFO1lBQ3ZDLEtBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNmLENBQUMsQ0FBQyxDQUFDO1FBRUgsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNLLDRDQUFhLEdBQXJCO1FBQUEsaUJBeUJDO1FBeEJDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ25CLE9BQU87U0FDUjtRQUVELHlEQUF5RDtRQUN6RCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUI7YUFDbEMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDckQsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUUxQixxQkFBcUI7UUFDckIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUVwQixnQ0FBZ0M7UUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUM7WUFDbEIsa0NBQWtDO1lBQ2xDLElBQUksS0FBSSxDQUFDLFNBQVMsRUFBRTtnQkFDbEIsc0JBQXNCO2dCQUN0QixLQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsS0FBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN0RCxLQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUgsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSywyQ0FBWSxHQUFwQjtRQUNFLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUV6RSw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsWUFBWSxDQUFDLENBQUM7UUFFekUsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSywyQ0FBWSxHQUFwQixVQUFxQixLQUFZO1FBQWpDLGlCQXNCQztRQXRCb0Isc0JBQUEsRUFBQSxZQUFZO1FBQy9CLElBQU0sV0FBVyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFcEMsMENBQTBDO1FBQzFDLFVBQVUsQ0FBQztZQUNULHdCQUF3QjtZQUN4QixLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFlBQVksRUFDWixNQUFNLENBQ1AsQ0FBQztZQUVGLDZCQUE2QjtZQUM3QixLQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFlBQVksRUFDWixRQUFRLENBQ1QsQ0FBQztRQUNKLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUVoQixpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0ssZ0RBQWlCLEdBQXpCO1FBQ0UsdUNBQXVDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLE9BQU87U0FDUjtRQUVELHdCQUF3QjtRQUN4QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1FBRS9CLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsbUNBQUksR0FBSjtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQ3BDLE9BQU87U0FDUjtRQUVELHdCQUF3QjtRQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV6QixtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBCLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFckIsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBRW5CLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFckMsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxvQ0FBSyxHQUFMO1FBQ0UsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNyQyxPQUFPO1NBQ1I7UUFFRCx3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFekIsb0JBQW9CO1FBQ3BCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUVyQix3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFFcEIsaUNBQWlDO1FBQ2pDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUVyQyxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBCLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVEOztPQUVHO0lBQ0gseUNBQVUsR0FBVjtRQUNFLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNmLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNkO2FBQU07WUFDTCxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDYjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUVILDJDQUFZLEdBRFo7UUFFRSwyQ0FBMkM7UUFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyx3QkFBd0IsRUFBRTtZQUNsQyxPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFFSCwyQ0FBWSxHQURaO1FBRUUsMkNBQTJDO1FBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEVBQUU7WUFDbEMsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7T0FFRztJQUNILG1DQUFJLEdBQUo7UUFDRSx5Q0FBeUM7UUFDekMsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2YsT0FBTztTQUNSO1FBRUQsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLE9BQU87UUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUVuQixpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRDs7T0FFRztJQUNILHFDQUFNLEdBQU47UUFDRSxxQ0FBcUM7UUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTztTQUNSO1FBRUQsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1FBRXpCLFNBQVM7UUFDVCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUVwQixpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFRDs7T0FFRztJQUNILHlDQUFVLEdBQVY7UUFDRSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDZjthQUFNO1lBQ0wsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ2I7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCw4Q0FBZSxHQUFmO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU87U0FDUjtRQUVELHdCQUF3QjtRQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV6Qix3QkFBd0I7UUFDeEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFFdEIsdUJBQXVCO1FBQ3ZCLElBQU0sVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1FBRTNDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUNyQixJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFDOUIsT0FBTyxFQUNQLFVBQVUsQ0FDWCxDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQ3JCLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUM5QixXQUFXLEVBQ1gsVUFBVSxDQUNYLENBQUM7UUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FDckIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQzlCLFdBQVcsRUFDWCxVQUFVLENBQ1gsQ0FBQztRQUVGLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVEOztPQUVHO0lBQ0gsZ0RBQWlCLEdBQWpCO1FBQ0UscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLE9BQU87U0FDUjtRQUVELHdCQUF3QjtRQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUV6QixpQ0FBaUM7UUFDakMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFFckIsMEJBQTBCO1FBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBRXhFLGlCQUFpQjtRQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Z0JBcndCRixTQUFTLFNBQUM7b0JBQ1QsUUFBUSxFQUFFLGNBQWM7b0JBQ3hCLHFDQUF1QztvQkFFdkMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN0Qzs7OztnQkFqQkMsZ0JBQWdCO2dCQWZoQixpQkFBaUI7Z0JBRWpCLFVBQVU7Z0JBdUJILGlCQUFpQjtnQkFEakIscUJBQXFCO2dCQURyQixrQkFBa0I7Z0JBSmxCLGVBQWU7Z0JBVHRCLFNBQVM7Ozt1QkF5QlIsS0FBSztzQkFJTCxLQUFLOzJCQUlMLEtBQUs7eUJBSUwsV0FBVyxTQUFDLFlBQVk7NkJBSXhCLEtBQUs7K0JBSUwsV0FBVyxTQUFDLG1CQUFtQjs4QkFJL0IsS0FBSzsyQ0FJTCxLQUFLOzJCQUlMLFdBQVcsU0FBQyxnQkFBZ0I7bUNBSTVCLEtBQUs7Z0NBSUwsTUFBTTtnQ0FJTixNQUFNO3FDQVlOLFdBQVcsU0FBQywwQkFBMEI7eUJBaUR0QyxLQUFLOytCQWdoQkwsWUFBWSxTQUFDLFlBQVk7K0JBYXpCLFlBQVksU0FBQyxZQUFZOztJQXdINUIsMkJBQUM7Q0FBQSxBQXR3QkQsSUFzd0JDO1NBaHdCWSxvQkFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENoYW5nZURldGVjdG9yUmVmLFxyXG4gIENvbXBvbmVudCxcclxuICBFbGVtZW50UmVmLFxyXG4gIEV2ZW50RW1pdHRlcixcclxuICBIb3N0QmluZGluZyxcclxuICBIb3N0TGlzdGVuZXIsXHJcbiAgSW5wdXQsXHJcbiAgT25EZXN0cm95LFxyXG4gIE9uSW5pdCxcclxuICBPdXRwdXQsXHJcbiAgUmVuZGVyZXIyLFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uXHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtcclxuICBhbmltYXRlLFxyXG4gIEFuaW1hdGlvbkJ1aWxkZXIsXHJcbiAgQW5pbWF0aW9uUGxheWVyLFxyXG4gIHN0eWxlXHJcbn0gZnJvbSBcIkBhbmd1bGFyL2FuaW1hdGlvbnNcIjtcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZU1lZGlhIH0gZnJvbSBcIkBhbmd1bGFyL2ZsZXgtbGF5b3V0XCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VTaWRlYmFyU2VydmljZSB9IGZyb20gXCIuL3NpZGViYXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBGdXNlTWF0Y2hNZWRpYVNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2Uvc2VydmljZXMvbWF0Y2gtbWVkaWEuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBGdXNlQ29uZmlnU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9zZXJ2aWNlcy9jb25maWcuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiZnVzZS1zaWRlYmFyXCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9zaWRlYmFyLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3NpZGViYXIuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZVNpZGViYXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgLy8gTmFtZVxyXG4gIEBJbnB1dCgpXHJcbiAgbmFtZTogc3RyaW5nO1xyXG5cclxuICAvLyBLZXlcclxuICBASW5wdXQoKVxyXG4gIGtleTogc3RyaW5nO1xyXG5cclxuICAvLyBQb3NpdGlvblxyXG4gIEBJbnB1dCgpXHJcbiAgcG9zaXRpb246IFwibGVmdFwiIHwgXCJyaWdodFwiO1xyXG5cclxuICAvLyBPcGVuXHJcbiAgQEhvc3RCaW5kaW5nKFwiY2xhc3Mub3BlblwiKVxyXG4gIG9wZW5lZDogYm9vbGVhbjtcclxuXHJcbiAgLy8gTG9ja2VkIE9wZW5cclxuICBASW5wdXQoKVxyXG4gIGxvY2tlZE9wZW46IHN0cmluZztcclxuXHJcbiAgLy8gaXNMb2NrZWRPcGVuXHJcbiAgQEhvc3RCaW5kaW5nKFwiY2xhc3MubG9ja2VkLW9wZW5cIilcclxuICBpc0xvY2tlZE9wZW46IGJvb2xlYW47XHJcblxyXG4gIC8vIEZvbGRlZCB3aWR0aFxyXG4gIEBJbnB1dCgpXHJcbiAgZm9sZGVkV2lkdGg6IG51bWJlcjtcclxuXHJcbiAgLy8gRm9sZGVkIGF1dG8gdHJpZ2dlciBvbiBob3ZlclxyXG4gIEBJbnB1dCgpXHJcbiAgZm9sZGVkQXV0b1RyaWdnZXJPbkhvdmVyOiBib29sZWFuO1xyXG5cclxuICAvLyBGb2xkZWQgdW5mb2xkZWRcclxuICBASG9zdEJpbmRpbmcoXCJjbGFzcy51bmZvbGRlZFwiKVxyXG4gIHVuZm9sZGVkOiBib29sZWFuO1xyXG5cclxuICAvLyBJbnZpc2libGUgb3ZlcmxheVxyXG4gIEBJbnB1dCgpXHJcbiAgaW52aXNpYmxlT3ZlcmxheTogYm9vbGVhbjtcclxuXHJcbiAgLy8gRm9sZGVkIGNoYW5nZWRcclxuICBAT3V0cHV0KClcclxuICBmb2xkZWRDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8Ym9vbGVhbj47XHJcblxyXG4gIC8vIE9wZW5lZCBjaGFuZ2VkXHJcbiAgQE91dHB1dCgpXHJcbiAgb3BlbmVkQ2hhbmdlZDogRXZlbnRFbWl0dGVyPGJvb2xlYW4+O1xyXG5cclxuICAvLyBQcml2YXRlXHJcbiAgcHJpdmF0ZSBfZm9sZGVkOiBib29sZWFuO1xyXG4gIHByaXZhdGUgX2Z1c2VDb25maWc6IGFueTtcclxuICBwcml2YXRlIF93YXNBY3RpdmU6IGJvb2xlYW47XHJcbiAgcHJpdmF0ZSBfd2FzRm9sZGVkOiBib29sZWFuO1xyXG4gIHByaXZhdGUgX2JhY2tkcm9wOiBIVE1MRWxlbWVudCB8IG51bGwgPSBudWxsO1xyXG4gIHByaXZhdGUgX3BsYXllcjogQW5pbWF0aW9uUGxheWVyO1xyXG4gIHByaXZhdGUgX3Vuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gIEBIb3N0QmluZGluZyhcImNsYXNzLmFuaW1hdGlvbnMtZW5hYmxlZFwiKVxyXG4gIHB1YmxpYyBfYW5pbWF0aW9uc0VuYWJsZWQ6IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge0FuaW1hdGlvbkJ1aWxkZXJ9IF9hbmltYXRpb25CdWlsZGVyXHJcbiAgICogQHBhcmFtIHtDaGFuZ2VEZXRlY3RvclJlZn0gX2NoYW5nZURldGVjdG9yUmVmXHJcbiAgICogQHBhcmFtIHtFbGVtZW50UmVmfSBfZWxlbWVudFJlZlxyXG4gICAqIEBwYXJhbSB7RnVzZUNvbmZpZ1NlcnZpY2V9IF9mdXNlQ29uZmlnU2VydmljZVxyXG4gICAqIEBwYXJhbSB7RnVzZU1hdGNoTWVkaWFTZXJ2aWNlfSBfZnVzZU1hdGNoTWVkaWFTZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtGdXNlU2lkZWJhclNlcnZpY2V9IF9mdXNlU2lkZWJhclNlcnZpY2VcclxuICAgKiBAcGFyYW0ge09ic2VydmFibGVNZWRpYX0gX29ic2VydmFibGVNZWRpYVxyXG4gICAqIEBwYXJhbSB7UmVuZGVyZXIyfSBfcmVuZGVyZXJcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2FuaW1hdGlvbkJ1aWxkZXI6IEFuaW1hdGlvbkJ1aWxkZXIsXHJcbiAgICBwcml2YXRlIF9jaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBwcml2YXRlIF9lbGVtZW50UmVmOiBFbGVtZW50UmVmLFxyXG4gICAgcHJpdmF0ZSBfZnVzZUNvbmZpZ1NlcnZpY2U6IEZ1c2VDb25maWdTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfZnVzZU1hdGNoTWVkaWFTZXJ2aWNlOiBGdXNlTWF0Y2hNZWRpYVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9mdXNlU2lkZWJhclNlcnZpY2U6IEZ1c2VTaWRlYmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgX29ic2VydmFibGVNZWRpYTogT2JzZXJ2YWJsZU1lZGlhLFxyXG4gICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICkge1xyXG4gICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5mb2xkZWRBdXRvVHJpZ2dlck9uSG92ZXIgPSB0cnVlO1xyXG4gICAgdGhpcy5mb2xkZWRXaWR0aCA9IDY0O1xyXG4gICAgdGhpcy5mb2xkZWRDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgdGhpcy5vcGVuZWRDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgdGhpcy5vcGVuZWQgPSBmYWxzZTtcclxuICAgIHRoaXMucG9zaXRpb24gPSBcImxlZnRcIjtcclxuICAgIHRoaXMuaW52aXNpYmxlT3ZlcmxheSA9IGZhbHNlO1xyXG5cclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fYW5pbWF0aW9uc0VuYWJsZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuX2ZvbGRlZCA9IGZhbHNlO1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIEFjY2Vzc29yc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIEZvbGRlZFxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtib29sZWFufSB2YWx1ZVxyXG4gICAqL1xyXG4gIEBJbnB1dCgpXHJcbiAgc2V0IGZvbGRlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgLy8gU2V0IHRoZSBmb2xkZWRcclxuICAgIHRoaXMuX2ZvbGRlZCA9IHZhbHVlO1xyXG5cclxuICAgIC8vIFJldHVybiBpZiB0aGUgc2lkZWJhciBpcyBjbG9zZWRcclxuICAgIGlmICghdGhpcy5vcGVuZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2dyYW1tYXRpY2FsbHkgYWRkL3JlbW92ZSBwYWRkaW5nIHRvIHRoZSBlbGVtZW50XHJcbiAgICAvLyB0aGF0IGNvbWVzIGFmdGVyIG9yIGJlZm9yZSBiYXNlZCBvbiB0aGUgcG9zaXRpb25cclxuICAgIGxldCBzaWJsaW5nLCBzdHlsZVJ1bGU7XHJcblxyXG4gICAgY29uc3Qgc3R5bGVWYWx1ZSA9IHRoaXMuZm9sZGVkV2lkdGggKyBcInB4XCI7XHJcblxyXG4gICAgLy8gR2V0IHRoZSBzaWJsaW5nIGFuZCBzZXQgdGhlIHN0eWxlIHJ1bGVcclxuICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSBcImxlZnRcIikge1xyXG4gICAgICBzaWJsaW5nID0gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50Lm5leHRFbGVtZW50U2libGluZztcclxuICAgICAgc3R5bGVSdWxlID0gXCJwYWRkaW5nLWxlZnRcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNpYmxpbmcgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucHJldmlvdXNFbGVtZW50U2libGluZztcclxuICAgICAgc3R5bGVSdWxlID0gXCJwYWRkaW5nLXJpZ2h0XCI7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSWYgdGhlcmUgaXMgbm8gc2libGluZywgcmV0dXJuLi4uXHJcbiAgICBpZiAoIXNpYmxpbmcpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIElmIGZvbGRlZC4uLlxyXG4gICAgaWYgKHZhbHVlKSB7XHJcbiAgICAgIC8vIEZvbGQgdGhlIHNpZGViYXJcclxuICAgICAgdGhpcy5mb2xkKCk7XHJcblxyXG4gICAgICAvLyBTZXQgdGhlIGZvbGRlZCB3aWR0aFxyXG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgICB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXHJcbiAgICAgICAgXCJ3aWR0aFwiLFxyXG4gICAgICAgIHN0eWxlVmFsdWVcclxuICAgICAgKTtcclxuICAgICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUoXHJcbiAgICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFxyXG4gICAgICAgIFwibWluLXdpZHRoXCIsXHJcbiAgICAgICAgc3R5bGVWYWx1ZVxyXG4gICAgICApO1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgICB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXHJcbiAgICAgICAgXCJtYXgtd2lkdGhcIixcclxuICAgICAgICBzdHlsZVZhbHVlXHJcbiAgICAgICk7XHJcblxyXG4gICAgICAvLyBTZXQgdGhlIHN0eWxlIGFuZCBjbGFzc1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShzaWJsaW5nLCBzdHlsZVJ1bGUsIHN0eWxlVmFsdWUpO1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIFwiZm9sZGVkXCIpO1xyXG4gICAgfVxyXG4gICAgLy8gSWYgdW5mb2xkZWQuLi5cclxuICAgIGVsc2Uge1xyXG4gICAgICAvLyBVbmZvbGQgdGhlIHNpZGViYXJcclxuICAgICAgdGhpcy51bmZvbGQoKTtcclxuXHJcbiAgICAgIC8vIFJlbW92ZSB0aGUgZm9sZGVkIHdpZHRoXHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZVN0eWxlKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgXCJ3aWR0aFwiKTtcclxuICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlU3R5bGUodGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCBcIm1pbi13aWR0aFwiKTtcclxuICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlU3R5bGUodGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCBcIm1heC13aWR0aFwiKTtcclxuXHJcbiAgICAgIC8vIFJlbW92ZSB0aGUgc3R5bGUgYW5kIGNsYXNzXHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZVN0eWxlKHNpYmxpbmcsIHN0eWxlUnVsZSk7XHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZUNsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgXCJmb2xkZWRcIik7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRW1pdCB0aGUgJ2ZvbGRlZENoYW5nZWQnIGV2ZW50XHJcbiAgICB0aGlzLmZvbGRlZENoYW5nZWQuZW1pdCh0aGlzLmZvbGRlZCk7XHJcbiAgfVxyXG5cclxuICBnZXQgZm9sZGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX2ZvbGRlZDtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBPbiBpbml0XHJcbiAgICovXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAvLyBTdWJzY3JpYmUgdG8gY29uZmlnIGNoYW5nZXNcclxuICAgIHRoaXMuX2Z1c2VDb25maWdTZXJ2aWNlLmNvbmZpZ1xyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKGNvbmZpZyA9PiB7XHJcbiAgICAgICAgdGhpcy5fZnVzZUNvbmZpZyA9IGNvbmZpZztcclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gUmVnaXN0ZXIgdGhlIHNpZGViYXJcclxuICAgIHRoaXMuX2Z1c2VTaWRlYmFyU2VydmljZS5yZWdpc3Rlcih0aGlzLm5hbWUsIHRoaXMpO1xyXG5cclxuICAgIC8vIFNldHVwIHZpc2liaWxpdHlcclxuICAgIHRoaXMuX3NldHVwVmlzaWJpbGl0eSgpO1xyXG5cclxuICAgIC8vIFNldHVwIHBvc2l0aW9uXHJcbiAgICB0aGlzLl9zZXR1cFBvc2l0aW9uKCk7XHJcblxyXG4gICAgLy8gU2V0dXAgbG9ja2VkT3BlblxyXG4gICAgdGhpcy5fc2V0dXBMb2NrZWRPcGVuKCk7XHJcblxyXG4gICAgLy8gU2V0dXAgZm9sZGVkXHJcbiAgICB0aGlzLl9zZXR1cEZvbGRlZCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogT24gZGVzdHJveVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgLy8gSWYgdGhlIHNpZGViYXIgaXMgZm9sZGVkLCB1bmZvbGQgaXQgdG8gcmV2ZXJ0IG1vZGlmaWNhdGlvbnNcclxuICAgIGlmICh0aGlzLmZvbGRlZCkge1xyXG4gICAgICB0aGlzLnVuZm9sZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFVucmVnaXN0ZXIgdGhlIHNpZGViYXJcclxuICAgIHRoaXMuX2Z1c2VTaWRlYmFyU2VydmljZS51bnJlZ2lzdGVyKHRoaXMubmFtZSk7XHJcblxyXG4gICAgLy8gVW5zdWJzY3JpYmUgZnJvbSBhbGwgc3Vic2NyaXB0aW9uc1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwubmV4dCgpO1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQcml2YXRlIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBTZXR1cCB0aGUgdmlzaWJpbGl0eSBvZiB0aGUgc2lkZWJhclxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9zZXR1cFZpc2liaWxpdHkoKTogdm9pZCB7XHJcbiAgICAvLyBSZW1vdmUgdGhlIGV4aXN0aW5nIGJveC1zaGFkb3dcclxuICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKFxyXG4gICAgICB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXHJcbiAgICAgIFwiYm94LXNoYWRvd1wiLFxyXG4gICAgICBcIm5vbmVcIlxyXG4gICAgKTtcclxuXHJcbiAgICAvLyBNYWtlIHRoZSBzaWRlYmFyIGludmlzaWJsZVxyXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUoXHJcbiAgICAgIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCxcclxuICAgICAgXCJ2aXNpYmlsaXR5XCIsXHJcbiAgICAgIFwiaGlkZGVuXCJcclxuICAgICk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZXR1cCB0aGUgc2lkZWJhciBwb3NpdGlvblxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9zZXR1cFBvc2l0aW9uKCk6IHZvaWQge1xyXG4gICAgLy8gQWRkIHRoZSBjb3JyZWN0IGNsYXNzIG5hbWUgdG8gdGhlIHNpZGViYXJcclxuICAgIC8vIGVsZW1lbnQgZGVwZW5kaW5nIG9uIHRoZSBwb3NpdGlvbiBhdHRyaWJ1dGVcclxuICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSBcInJpZ2h0XCIpIHtcclxuICAgICAgdGhpcy5fcmVuZGVyZXIuYWRkQ2xhc3MoXHJcbiAgICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFxyXG4gICAgICAgIFwicmlnaHQtcG9zaXRpb25lZFwiXHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5hZGRDbGFzcyhcclxuICAgICAgICB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXHJcbiAgICAgICAgXCJsZWZ0LXBvc2l0aW9uZWRcIlxyXG4gICAgICApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2V0dXAgdGhlIGxvY2tlZE9wZW4gaGFuZGxlclxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9zZXR1cExvY2tlZE9wZW4oKTogdm9pZCB7XHJcbiAgICAvLyBSZXR1cm4gaWYgdGhlIGxvY2tlZE9wZW4gd2Fzbid0IHNldFxyXG4gICAgaWYgKCF0aGlzLmxvY2tlZE9wZW4pIHtcclxuICAgICAgLy8gUmV0dXJuXHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgdGhlIHdhc0FjdGl2ZSBmb3IgdGhlIGZpcnN0IHRpbWVcclxuICAgIHRoaXMuX3dhc0FjdGl2ZSA9IGZhbHNlO1xyXG5cclxuICAgIC8vIFNldCB0aGUgd2FzRm9sZGVkXHJcbiAgICB0aGlzLl93YXNGb2xkZWQgPSB0aGlzLmZvbGRlZDtcclxuXHJcbiAgICAvLyBTaG93IHRoZSBzaWRlYmFyXHJcbiAgICB0aGlzLl9zaG93U2lkZWJhcigpO1xyXG5cclxuICAgIC8vIEFjdCBvbiBldmVyeSBtZWRpYSBjaGFuZ2VcclxuICAgIHRoaXMuX2Z1c2VNYXRjaE1lZGlhU2VydmljZS5vbk1lZGlhQ2hhbmdlXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgIC8vIEdldCB0aGUgYWN0aXZlIHN0YXR1c1xyXG4gICAgICAgIGNvbnN0IGlzQWN0aXZlID0gdGhpcy5fb2JzZXJ2YWJsZU1lZGlhLmlzQWN0aXZlKHRoaXMubG9ja2VkT3Blbik7XHJcblxyXG4gICAgICAgIC8vIElmIHRoZSBib3RoIHN0YXR1cyBhcmUgdGhlIHNhbWUsIGRvbid0IGFjdFxyXG4gICAgICAgIGlmICh0aGlzLl93YXNBY3RpdmUgPT09IGlzQWN0aXZlKSB7XHJcbiAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBBY3RpdmF0ZSB0aGUgbG9ja2VkT3BlblxyXG4gICAgICAgIGlmIChpc0FjdGl2ZSkge1xyXG4gICAgICAgICAgLy8gU2V0IHRoZSBsb2NrZWRPcGVuIHN0YXR1c1xyXG4gICAgICAgICAgdGhpcy5pc0xvY2tlZE9wZW4gPSB0cnVlO1xyXG5cclxuICAgICAgICAgIC8vIFNob3cgdGhlIHNpZGViYXJcclxuICAgICAgICAgIHRoaXMuX3Nob3dTaWRlYmFyKCk7XHJcblxyXG4gICAgICAgICAgLy8gRm9yY2UgdGhlIHRoZSBvcGVuZWQgc3RhdHVzIHRvIHRydWVcclxuICAgICAgICAgIHRoaXMub3BlbmVkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAvLyBFbWl0IHRoZSAnb3BlbmVkQ2hhbmdlZCcgZXZlbnRcclxuICAgICAgICAgIHRoaXMub3BlbmVkQ2hhbmdlZC5lbWl0KHRoaXMub3BlbmVkKTtcclxuXHJcbiAgICAgICAgICAvLyBJZiB0aGUgc2lkZWJhciB3YXMgZm9sZGVkLCBmb3JjZWZ1bGx5IGZvbGQgaXQgYWdhaW5cclxuICAgICAgICAgIGlmICh0aGlzLl93YXNGb2xkZWQpIHtcclxuICAgICAgICAgICAgLy8gRW5hYmxlIHRoZSBhbmltYXRpb25zXHJcbiAgICAgICAgICAgIHRoaXMuX2VuYWJsZUFuaW1hdGlvbnMoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIEZvbGRcclxuICAgICAgICAgICAgdGhpcy5mb2xkZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICAgICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgICAgICAgICAgdGhpcy5fY2hhbmdlRGV0ZWN0b3JSZWYubWFya0ZvckNoZWNrKCk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLy8gSGlkZSB0aGUgYmFja2Ryb3AgaWYgYW55IGV4aXN0c1xyXG4gICAgICAgICAgdGhpcy5faGlkZUJhY2tkcm9wKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIERlLUFjdGl2YXRlIHRoZSBsb2NrZWRPcGVuXHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAvLyBTZXQgdGhlIGxvY2tlZE9wZW4gc3RhdHVzXHJcbiAgICAgICAgICB0aGlzLmlzTG9ja2VkT3BlbiA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgIC8vIFVuZm9sZCB0aGUgc2lkZWJhciBpbiBjYXNlIGlmIGl0IHdhcyBmb2xkZWRcclxuICAgICAgICAgIHRoaXMudW5mb2xkKCk7XHJcblxyXG4gICAgICAgICAgLy8gRm9yY2UgdGhlIHRoZSBvcGVuZWQgc3RhdHVzIHRvIGNsb3NlXHJcbiAgICAgICAgICB0aGlzLm9wZW5lZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgIC8vIEVtaXQgdGhlICdvcGVuZWRDaGFuZ2VkJyBldmVudFxyXG4gICAgICAgICAgdGhpcy5vcGVuZWRDaGFuZ2VkLmVtaXQodGhpcy5vcGVuZWQpO1xyXG5cclxuICAgICAgICAgIC8vIEhpZGUgdGhlIHNpZGViYXJcclxuICAgICAgICAgIHRoaXMuX2hpZGVTaWRlYmFyKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTdG9yZSB0aGUgbmV3IGFjdGl2ZSBzdGF0dXNcclxuICAgICAgICB0aGlzLl93YXNBY3RpdmUgPSBpc0FjdGl2ZTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZXR1cCB0aGUgaW5pdGlhbCBmb2xkZWQgc3RhdHVzXHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3NldHVwRm9sZGVkKCk6IHZvaWQge1xyXG4gICAgLy8gUmV0dXJuLCBpZiBzaWRlYmFyIGlzIG5vdCBmb2xkZWRcclxuICAgIGlmICghdGhpcy5mb2xkZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJldHVybiBpZiB0aGUgc2lkZWJhciBpcyBjbG9zZWRcclxuICAgIGlmICghdGhpcy5vcGVuZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFByb2dyYW1tYXRpY2FsbHkgYWRkL3JlbW92ZSBwYWRkaW5nIHRvIHRoZSBlbGVtZW50XHJcbiAgICAvLyB0aGF0IGNvbWVzIGFmdGVyIG9yIGJlZm9yZSBiYXNlZCBvbiB0aGUgcG9zaXRpb25cclxuICAgIGxldCBzaWJsaW5nLCBzdHlsZVJ1bGU7XHJcblxyXG4gICAgY29uc3Qgc3R5bGVWYWx1ZSA9IHRoaXMuZm9sZGVkV2lkdGggKyBcInB4XCI7XHJcblxyXG4gICAgLy8gR2V0IHRoZSBzaWJsaW5nIGFuZCBzZXQgdGhlIHN0eWxlIHJ1bGVcclxuICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSBcImxlZnRcIikge1xyXG4gICAgICBzaWJsaW5nID0gdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50Lm5leHRFbGVtZW50U2libGluZztcclxuICAgICAgc3R5bGVSdWxlID0gXCJwYWRkaW5nLWxlZnRcIjtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHNpYmxpbmcgPSB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucHJldmlvdXNFbGVtZW50U2libGluZztcclxuICAgICAgc3R5bGVSdWxlID0gXCJwYWRkaW5nLXJpZ2h0XCI7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gSWYgdGhlcmUgaXMgbm8gc2libGluZywgcmV0dXJuLi4uXHJcbiAgICBpZiAoIXNpYmxpbmcpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEZvbGQgdGhlIHNpZGViYXJcclxuICAgIHRoaXMuZm9sZCgpO1xyXG5cclxuICAgIC8vIFNldCB0aGUgZm9sZGVkIHdpZHRoXHJcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFxyXG4gICAgICBcIndpZHRoXCIsXHJcbiAgICAgIHN0eWxlVmFsdWVcclxuICAgICk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFxyXG4gICAgICBcIm1pbi13aWR0aFwiLFxyXG4gICAgICBzdHlsZVZhbHVlXHJcbiAgICApO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUoXHJcbiAgICAgIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCxcclxuICAgICAgXCJtYXgtd2lkdGhcIixcclxuICAgICAgc3R5bGVWYWx1ZVxyXG4gICAgKTtcclxuXHJcbiAgICAvLyBTZXQgdGhlIHN0eWxlIGFuZCBjbGFzc1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUoc2libGluZywgc3R5bGVSdWxlLCBzdHlsZVZhbHVlKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgXCJmb2xkZWRcIik7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTaG93IHRoZSBiYWNrZHJvcFxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9zaG93QmFja2Ryb3AoKTogdm9pZCB7XHJcbiAgICAvLyBDcmVhdGUgdGhlIGJhY2tkcm9wIGVsZW1lbnRcclxuICAgIHRoaXMuX2JhY2tkcm9wID0gdGhpcy5fcmVuZGVyZXIuY3JlYXRlRWxlbWVudChcImRpdlwiKTtcclxuXHJcbiAgICAvLyBBZGQgYSBjbGFzcyB0byB0aGUgYmFja2Ryb3AgZWxlbWVudFxyXG4gICAgdGhpcy5fYmFja2Ryb3AuY2xhc3NMaXN0LmFkZChcImZ1c2Utc2lkZWJhci1vdmVybGF5XCIpO1xyXG5cclxuICAgIC8vIEFkZCBhIGNsYXNzIGRlcGVuZGluZyBvbiB0aGUgaW52aXNpYmxlT3ZlcmxheSBvcHRpb25cclxuICAgIGlmICh0aGlzLmludmlzaWJsZU92ZXJsYXkpIHtcclxuICAgICAgdGhpcy5fYmFja2Ryb3AuY2xhc3NMaXN0LmFkZChcImZ1c2Utc2lkZWJhci1vdmVybGF5LWludmlzaWJsZVwiKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBcHBlbmQgdGhlIGJhY2tkcm9wIHRvIHRoZSBwYXJlbnQgb2YgdGhlIHNpZGViYXJcclxuICAgIHRoaXMuX3JlbmRlcmVyLmFwcGVuZENoaWxkKFxyXG4gICAgICB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQucGFyZW50RWxlbWVudCxcclxuICAgICAgdGhpcy5fYmFja2Ryb3BcclxuICAgICk7XHJcblxyXG4gICAgLy8gQ3JlYXRlIHRoZSBlbnRlciBhbmltYXRpb24gYW5kIGF0dGFjaCBpdCB0byB0aGUgcGxheWVyXHJcbiAgICB0aGlzLl9wbGF5ZXIgPSB0aGlzLl9hbmltYXRpb25CdWlsZGVyXHJcbiAgICAgIC5idWlsZChbYW5pbWF0ZShcIjMwMG1zIGVhc2VcIiwgc3R5bGUoeyBvcGFjaXR5OiAxIH0pKV0pXHJcbiAgICAgIC5jcmVhdGUodGhpcy5fYmFja2Ryb3ApO1xyXG5cclxuICAgIC8vIFBsYXkgdGhlIGFuaW1hdGlvblxyXG4gICAgdGhpcy5fcGxheWVyLnBsYXkoKTtcclxuXHJcbiAgICAvLyBBZGQgYW4gZXZlbnQgbGlzdGVuZXIgdG8gdGhlIG92ZXJsYXlcclxuICAgIHRoaXMuX2JhY2tkcm9wLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCAoKSA9PiB7XHJcbiAgICAgIHRoaXMuY2xvc2UoKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEhpZGUgdGhlIGJhY2tkcm9wXHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX2hpZGVCYWNrZHJvcCgpOiB2b2lkIHtcclxuICAgIGlmICghdGhpcy5fYmFja2Ryb3ApIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIENyZWF0ZSB0aGUgbGVhdmUgYW5pbWF0aW9uIGFuZCBhdHRhY2ggaXQgdG8gdGhlIHBsYXllclxyXG4gICAgdGhpcy5fcGxheWVyID0gdGhpcy5fYW5pbWF0aW9uQnVpbGRlclxyXG4gICAgICAuYnVpbGQoW2FuaW1hdGUoXCIzMDBtcyBlYXNlXCIsIHN0eWxlKHsgb3BhY2l0eTogMCB9KSldKVxyXG4gICAgICAuY3JlYXRlKHRoaXMuX2JhY2tkcm9wKTtcclxuXHJcbiAgICAvLyBQbGF5IHRoZSBhbmltYXRpb25cclxuICAgIHRoaXMuX3BsYXllci5wbGF5KCk7XHJcblxyXG4gICAgLy8gT25jZSB0aGUgYW5pbWF0aW9uIGlzIGRvbmUuLi5cclxuICAgIHRoaXMuX3BsYXllci5vbkRvbmUoKCkgPT4ge1xyXG4gICAgICAvLyBJZiB0aGUgYmFja2Ryb3Agc3RpbGwgZXhpc3RzLi4uXHJcbiAgICAgIGlmICh0aGlzLl9iYWNrZHJvcCkge1xyXG4gICAgICAgIC8vIFJlbW92ZSB0aGUgYmFja2Ryb3BcclxuICAgICAgICB0aGlzLl9iYWNrZHJvcC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKHRoaXMuX2JhY2tkcm9wKTtcclxuICAgICAgICB0aGlzLl9iYWNrZHJvcCA9IG51bGw7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENoYW5nZSBzb21lIHByb3BlcnRpZXMgb2YgdGhlIHNpZGViYXJcclxuICAgKiBhbmQgbWFrZSBpdCB2aXNpYmxlXHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX3Nob3dTaWRlYmFyKCk6IHZvaWQge1xyXG4gICAgLy8gUmVtb3ZlIHRoZSBib3gtc2hhZG93IHN0eWxlXHJcbiAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVTdHlsZSh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIFwiYm94LXNoYWRvd1wiKTtcclxuXHJcbiAgICAvLyBNYWtlIHRoZSBzaWRlYmFyIGludmlzaWJsZVxyXG4gICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlU3R5bGUodGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCBcInZpc2liaWxpdHlcIik7XHJcblxyXG4gICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2hhbmdlIHNvbWUgcHJvcGVydGllcyBvZiB0aGUgc2lkZWJhclxyXG4gICAqIGFuZCBtYWtlIGl0IGludmlzaWJsZVxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9oaWRlU2lkZWJhcihkZWxheSA9IHRydWUpOiB2b2lkIHtcclxuICAgIGNvbnN0IGRlbGF5QW1vdW50ID0gZGVsYXkgPyAzMDAgOiAwO1xyXG5cclxuICAgIC8vIEFkZCBhIGRlbGF5IHNvIGNsb3NlIGFuaW1hdGlvbiBjYW4gcGxheVxyXG4gICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgIC8vIFJlbW92ZSB0aGUgYm94LXNoYWRvd1xyXG4gICAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgICB0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsXHJcbiAgICAgICAgXCJib3gtc2hhZG93XCIsXHJcbiAgICAgICAgXCJub25lXCJcclxuICAgICAgKTtcclxuXHJcbiAgICAgIC8vIE1ha2UgdGhlIHNpZGViYXIgaW52aXNpYmxlXHJcbiAgICAgIHRoaXMuX3JlbmRlcmVyLnNldFN0eWxlKFxyXG4gICAgICAgIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCxcclxuICAgICAgICBcInZpc2liaWxpdHlcIixcclxuICAgICAgICBcImhpZGRlblwiXHJcbiAgICAgICk7XHJcbiAgICB9LCBkZWxheUFtb3VudCk7XHJcblxyXG4gICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRW5hYmxlIHRoZSBhbmltYXRpb25zXHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIHByaXZhdGUgX2VuYWJsZUFuaW1hdGlvbnMoKTogdm9pZCB7XHJcbiAgICAvLyBSZXR1cm4gaWYgYW5pbWF0aW9ucyBhbHJlYWR5IGVuYWJsZWRcclxuICAgIGlmICh0aGlzLl9hbmltYXRpb25zRW5hYmxlZCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRW5hYmxlIHRoZSBhbmltYXRpb25zXHJcbiAgICB0aGlzLl9hbmltYXRpb25zRW5hYmxlZCA9IHRydWU7XHJcblxyXG4gICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogT3BlbiB0aGUgc2lkZWJhclxyXG4gICAqL1xyXG4gIG9wZW4oKTogdm9pZCB7XHJcbiAgICBpZiAodGhpcy5vcGVuZWQgfHwgdGhpcy5pc0xvY2tlZE9wZW4pIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEVuYWJsZSB0aGUgYW5pbWF0aW9uc1xyXG4gICAgdGhpcy5fZW5hYmxlQW5pbWF0aW9ucygpO1xyXG5cclxuICAgIC8vIFNob3cgdGhlIHNpZGViYXJcclxuICAgIHRoaXMuX3Nob3dTaWRlYmFyKCk7XHJcblxyXG4gICAgLy8gU2hvdyB0aGUgYmFja2Ryb3BcclxuICAgIHRoaXMuX3Nob3dCYWNrZHJvcCgpO1xyXG5cclxuICAgIC8vIFNldCB0aGUgb3BlbmVkIHN0YXR1c1xyXG4gICAgdGhpcy5vcGVuZWQgPSB0cnVlO1xyXG5cclxuICAgIC8vIEVtaXQgdGhlICdvcGVuZWRDaGFuZ2VkJyBldmVudFxyXG4gICAgdGhpcy5vcGVuZWRDaGFuZ2VkLmVtaXQodGhpcy5vcGVuZWQpO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIENsb3NlIHRoZSBzaWRlYmFyXHJcbiAgICovXHJcbiAgY2xvc2UoKTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMub3BlbmVkIHx8IHRoaXMuaXNMb2NrZWRPcGVuKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBFbmFibGUgdGhlIGFuaW1hdGlvbnNcclxuICAgIHRoaXMuX2VuYWJsZUFuaW1hdGlvbnMoKTtcclxuXHJcbiAgICAvLyBIaWRlIHRoZSBiYWNrZHJvcFxyXG4gICAgdGhpcy5faGlkZUJhY2tkcm9wKCk7XHJcblxyXG4gICAgLy8gU2V0IHRoZSBvcGVuZWQgc3RhdHVzXHJcbiAgICB0aGlzLm9wZW5lZCA9IGZhbHNlO1xyXG5cclxuICAgIC8vIEVtaXQgdGhlICdvcGVuZWRDaGFuZ2VkJyBldmVudFxyXG4gICAgdGhpcy5vcGVuZWRDaGFuZ2VkLmVtaXQodGhpcy5vcGVuZWQpO1xyXG5cclxuICAgIC8vIEhpZGUgdGhlIHNpZGViYXJcclxuICAgIHRoaXMuX2hpZGVTaWRlYmFyKCk7XHJcblxyXG4gICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVG9nZ2xlIG9wZW4vY2xvc2UgdGhlIHNpZGViYXJcclxuICAgKi9cclxuICB0b2dnbGVPcGVuKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMub3BlbmVkKSB7XHJcbiAgICAgIHRoaXMuY2xvc2UoKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMub3BlbigpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogTW91c2VlbnRlclxyXG4gICAqL1xyXG4gIEBIb3N0TGlzdGVuZXIoXCJtb3VzZWVudGVyXCIpXHJcbiAgb25Nb3VzZUVudGVyKCk6IHZvaWQge1xyXG4gICAgLy8gT25seSB3b3JrIGlmIHRoZSBhdXRvIHRyaWdnZXIgaXMgZW5hYmxlZFxyXG4gICAgaWYgKCF0aGlzLmZvbGRlZEF1dG9UcmlnZ2VyT25Ib3Zlcikge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy51bmZvbGRUZW1wb3JhcmlseSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogTW91c2VsZWF2ZVxyXG4gICAqL1xyXG4gIEBIb3N0TGlzdGVuZXIoXCJtb3VzZWxlYXZlXCIpXHJcbiAgb25Nb3VzZUxlYXZlKCk6IHZvaWQge1xyXG4gICAgLy8gT25seSB3b3JrIGlmIHRoZSBhdXRvIHRyaWdnZXIgaXMgZW5hYmxlZFxyXG4gICAgaWYgKCF0aGlzLmZvbGRlZEF1dG9UcmlnZ2VyT25Ib3Zlcikge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5mb2xkVGVtcG9yYXJpbHkoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEZvbGQgdGhlIHNpZGViYXIgcGVybWFuZW50bHlcclxuICAgKi9cclxuICBmb2xkKCk6IHZvaWQge1xyXG4gICAgLy8gT25seSB3b3JrIGlmIHRoZSBzaWRlYmFyIGlzIG5vdCBmb2xkZWRcclxuICAgIGlmICh0aGlzLmZvbGRlZCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gRW5hYmxlIHRoZSBhbmltYXRpb25zXHJcbiAgICB0aGlzLl9lbmFibGVBbmltYXRpb25zKCk7XHJcblxyXG4gICAgLy8gRm9sZFxyXG4gICAgdGhpcy5mb2xkZWQgPSB0cnVlO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFVuZm9sZCB0aGUgc2lkZWJhciBwZXJtYW5lbnRseVxyXG4gICAqL1xyXG4gIHVuZm9sZCgpOiB2b2lkIHtcclxuICAgIC8vIE9ubHkgd29yayBpZiB0aGUgc2lkZWJhciBpcyBmb2xkZWRcclxuICAgIGlmICghdGhpcy5mb2xkZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEVuYWJsZSB0aGUgYW5pbWF0aW9uc1xyXG4gICAgdGhpcy5fZW5hYmxlQW5pbWF0aW9ucygpO1xyXG5cclxuICAgIC8vIFVuZm9sZFxyXG4gICAgdGhpcy5mb2xkZWQgPSBmYWxzZTtcclxuXHJcbiAgICAvLyBNYXJrIGZvciBjaGVja1xyXG4gICAgdGhpcy5fY2hhbmdlRGV0ZWN0b3JSZWYubWFya0ZvckNoZWNrKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBUb2dnbGUgdGhlIHNpZGViYXIgZm9sZC91bmZvbGQgcGVybWFuZW50bHlcclxuICAgKi9cclxuICB0b2dnbGVGb2xkKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuZm9sZGVkKSB7XHJcbiAgICAgIHRoaXMudW5mb2xkKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmZvbGQoKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEZvbGQgdGhlIHRlbXBvcmFyaWx5IHVuZm9sZGVkIHNpZGViYXIgYmFja1xyXG4gICAqL1xyXG4gIGZvbGRUZW1wb3JhcmlseSgpOiB2b2lkIHtcclxuICAgIC8vIE9ubHkgd29yayBpZiB0aGUgc2lkZWJhciBpcyBmb2xkZWRcclxuICAgIGlmICghdGhpcy5mb2xkZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEVuYWJsZSB0aGUgYW5pbWF0aW9uc1xyXG4gICAgdGhpcy5fZW5hYmxlQW5pbWF0aW9ucygpO1xyXG5cclxuICAgIC8vIEZvbGQgdGhlIHNpZGViYXIgYmFja1xyXG4gICAgdGhpcy51bmZvbGRlZCA9IGZhbHNlO1xyXG5cclxuICAgIC8vIFNldCB0aGUgZm9sZGVkIHdpZHRoXHJcbiAgICBjb25zdCBzdHlsZVZhbHVlID0gdGhpcy5mb2xkZWRXaWR0aCArIFwicHhcIjtcclxuXHJcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFxyXG4gICAgICBcIndpZHRoXCIsXHJcbiAgICAgIHN0eWxlVmFsdWVcclxuICAgICk7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5zZXRTdHlsZShcclxuICAgICAgdGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LFxyXG4gICAgICBcIm1pbi13aWR0aFwiLFxyXG4gICAgICBzdHlsZVZhbHVlXHJcbiAgICApO1xyXG4gICAgdGhpcy5fcmVuZGVyZXIuc2V0U3R5bGUoXHJcbiAgICAgIHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCxcclxuICAgICAgXCJtYXgtd2lkdGhcIixcclxuICAgICAgc3R5bGVWYWx1ZVxyXG4gICAgKTtcclxuXHJcbiAgICAvLyBNYXJrIGZvciBjaGVja1xyXG4gICAgdGhpcy5fY2hhbmdlRGV0ZWN0b3JSZWYubWFya0ZvckNoZWNrKCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBVbmZvbGQgdGhlIHNpZGViYXIgdGVtcG9yYXJpbHlcclxuICAgKi9cclxuICB1bmZvbGRUZW1wb3JhcmlseSgpOiB2b2lkIHtcclxuICAgIC8vIE9ubHkgd29yayBpZiB0aGUgc2lkZWJhciBpcyBmb2xkZWRcclxuICAgIGlmICghdGhpcy5mb2xkZWQpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEVuYWJsZSB0aGUgYW5pbWF0aW9uc1xyXG4gICAgdGhpcy5fZW5hYmxlQW5pbWF0aW9ucygpO1xyXG5cclxuICAgIC8vIFVuZm9sZCB0aGUgc2lkZWJhciB0ZW1wb3JhcmlseVxyXG4gICAgdGhpcy51bmZvbGRlZCA9IHRydWU7XHJcblxyXG4gICAgLy8gUmVtb3ZlIHRoZSBmb2xkZWQgd2lkdGhcclxuICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZVN0eWxlKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgXCJ3aWR0aFwiKTtcclxuICAgIHRoaXMuX3JlbmRlcmVyLnJlbW92ZVN0eWxlKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgXCJtaW4td2lkdGhcIik7XHJcbiAgICB0aGlzLl9yZW5kZXJlci5yZW1vdmVTdHlsZSh0aGlzLl9lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsIFwibWF4LXdpZHRoXCIpO1xyXG5cclxuICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICB0aGlzLl9jaGFuZ2VEZXRlY3RvclJlZi5tYXJrRm9yQ2hlY2soKTtcclxuICB9XHJcbn1cclxuIl19