import { NgModule } from '@angular/core';
import { FuseWidgetComponent } from './widget.component';
import { FuseWidgetToggleDirective } from './widget-toggle.directive';
var FuseWidgetModule = /** @class */ (function () {
    function FuseWidgetModule() {
    }
    FuseWidgetModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        FuseWidgetComponent,
                        FuseWidgetToggleDirective
                    ],
                    exports: [
                        FuseWidgetComponent,
                        FuseWidgetToggleDirective
                    ],
                },] }
    ];
    return FuseWidgetModule;
}());
export { FuseWidgetModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3dpZGdldC93aWRnZXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDekQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFdEU7SUFBQTtJQVlBLENBQUM7O2dCQVpBLFFBQVEsU0FBQztvQkFDTixZQUFZLEVBQUU7d0JBQ1YsbUJBQW1CO3dCQUNuQix5QkFBeUI7cUJBQzVCO29CQUNELE9BQU8sRUFBTzt3QkFDVixtQkFBbUI7d0JBQ25CLHlCQUF5QjtxQkFDNUI7aUJBQ0o7O0lBR0QsdUJBQUM7Q0FBQSxBQVpELElBWUM7U0FGWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgRnVzZVdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmUgfSBmcm9tICcuL3dpZGdldC10b2dnbGUuZGlyZWN0aXZlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBGdXNlV2lkZ2V0Q29tcG9uZW50LFxyXG4gICAgICAgIEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzICAgICA6IFtcclxuICAgICAgICBGdXNlV2lkZ2V0Q29tcG9uZW50LFxyXG4gICAgICAgIEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmVcclxuICAgIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlV2lkZ2V0TW9kdWxlXHJcbntcclxufVxyXG4iXX0=