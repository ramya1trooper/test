import { Directive, ElementRef } from '@angular/core';
var FuseWidgetToggleDirective = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {ElementRef} elementRef
     */
    function FuseWidgetToggleDirective(elementRef) {
        this.elementRef = elementRef;
    }
    FuseWidgetToggleDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[fuseWidgetToggle]'
                },] }
    ];
    /** @nocollapse */
    FuseWidgetToggleDirective.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    return FuseWidgetToggleDirective;
}());
export { FuseWidgetToggleDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LXRvZ2dsZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy93aWRnZXQvd2lkZ2V0LXRvZ2dsZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFdEQ7SUFLSTs7OztPQUlHO0lBQ0gsbUNBQ1csVUFBc0I7UUFBdEIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUdqQyxDQUFDOztnQkFkSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtpQkFDakM7Ozs7Z0JBSm1CLFVBQVU7O0lBaUI5QixnQ0FBQztDQUFBLEFBZkQsSUFlQztTQVpZLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1tmdXNlV2lkZ2V0VG9nZ2xlXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmVcclxue1xyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7RWxlbWVudFJlZn0gZWxlbWVudFJlZlxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgZWxlbWVudFJlZjogRWxlbWVudFJlZlxyXG4gICAgKVxyXG4gICAge1xyXG4gICAgfVxyXG59XHJcbiJdfQ==