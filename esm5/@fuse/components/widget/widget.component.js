import { Component, ContentChildren, ElementRef, HostBinding, QueryList, Renderer2, ViewEncapsulation } from '@angular/core';
import { FuseWidgetToggleDirective } from './widget-toggle.directive';
var FuseWidgetComponent = /** @class */ (function () {
    /**
     * Constructor
     *
     * @param {ElementRef} _elementRef
     * @param {Renderer2} _renderer
     */
    function FuseWidgetComponent(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.flipped = false;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * After content init
     */
    FuseWidgetComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        // Listen for the flip button click
        setTimeout(function () {
            _this.toggleButtons.forEach(function (flipButton) {
                _this._renderer.listen(flipButton.elementRef.nativeElement, 'click', function (event) {
                    event.preventDefault();
                    event.stopPropagation();
                    _this.toggle();
                });
            });
        });
    };
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle the flipped status
     */
    FuseWidgetComponent.prototype.toggle = function () {
        this.flipped = !this.flipped;
    };
    FuseWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'fuse-widget',
                    template: "<ng-content></ng-content>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: ["fuse-widget{display:block;position:relative;-webkit-perspective:3000px;perspective:3000px;padding:12px}fuse-widget>div{position:relative;-webkit-transform-style:preserve-3d;transform-style:preserve-3d;-webkit-transition:-webkit-transform 1s;transition:transform 1s;transition:transform 1s,-webkit-transform 1s}fuse-widget>.fuse-widget-front{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;position:relative;overflow:hidden;visibility:visible;width:100%;opacity:1;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(0);transform:rotateY(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}fuse-widget>.fuse-widget-back{display:block;position:absolute;top:12px;right:12px;bottom:12px;left:12px;overflow:hidden;visibility:hidden;opacity:0;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(180deg);transform:rotateY(180deg);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}fuse-widget>.fuse-widget-back [fuseWidgetToggle]{position:absolute;top:0;right:0}fuse-widget.flipped>.fuse-widget-front{visibility:hidden;opacity:0;-webkit-transform:rotateY(180deg);transform:rotateY(180deg)}fuse-widget.flipped>.fuse-widget-back{display:block;visibility:visible;opacity:1;-webkit-transform:rotateY(360deg);transform:rotateY(360deg)}fuse-widget .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper{padding:16px 0}fuse-widget .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper .mat-form-field-infix{border:none;padding:0}fuse-widget .mat-form-field.mat-form-field-type-mat-select .mat-form-field-underline{display:none}.fusewidgetClass{display:block;position:relative;-webkit-perspective:3000px;perspective:3000px;padding:0}.fusewidgetClass>div{position:relative;-webkit-transform-style:preserve-3d;transform-style:preserve-3d;-webkit-transition:-webkit-transform 1s;transition:transform 1s;transition:transform 1s,-webkit-transform 1s}.fusewidgetClass>.fuse-widget-front{border-color:#dbdbdb!important;background:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;position:relative;overflow:hidden;visibility:visible;width:100%;opacity:1;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(0);transform:rotateY(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}.fusewidgetClass>.fuse-widget-back{display:block;position:absolute;top:12px;right:12px;bottom:12px;left:12px;overflow:hidden;visibility:hidden;opacity:0;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(180deg);transform:rotateY(180deg);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}.fusewidgetClass>.fuse-widget-back [fuseWidgetToggle]{position:absolute;top:0;right:0}.fusewidgetClass.flipped>.fuse-widget-front{visibility:hidden;opacity:0;-webkit-transform:rotateY(180deg);transform:rotateY(180deg)}.fusewidgetClass.flipped>.fuse-widget-back{display:block;visibility:visible;opacity:1;-webkit-transform:rotateY(360deg);transform:rotateY(360deg)}.fusewidgetClass .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper{padding:16px 0}.fusewidgetClass .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper .mat-form-field-infix{border:none;padding:0}.fusewidgetClass .mat-form-field.mat-form-field-type-mat-select .mat-form-field-underline{display:none}"]
                }] }
    ];
    /** @nocollapse */
    FuseWidgetComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    FuseWidgetComponent.propDecorators = {
        flipped: [{ type: HostBinding, args: ['class.flipped',] }],
        toggleButtons: [{ type: ContentChildren, args: [FuseWidgetToggleDirective, { descendants: true },] }]
    };
    return FuseWidgetComponent;
}());
export { FuseWidgetComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3dpZGdldC93aWRnZXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBb0IsU0FBUyxFQUFFLGVBQWUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0ksT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFFdEU7SUFlSTs7Ozs7T0FLRztJQUNILDZCQUNZLFdBQXVCLEVBQ3ZCLFNBQW9CO1FBRHBCLGdCQUFXLEdBQVgsV0FBVyxDQUFZO1FBQ3ZCLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFiaEMsWUFBTyxHQUFHLEtBQUssQ0FBQztJQWdCaEIsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsZ0RBQWtCLEdBQWxCO1FBQUEsaUJBWUM7UUFWRyxtQ0FBbUM7UUFDbkMsVUFBVSxDQUFDO1lBQ1AsS0FBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsVUFBQSxVQUFVO2dCQUNqQyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsVUFBQyxLQUFLO29CQUN0RSxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7b0JBQ3ZCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztvQkFDeEIsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNsQixDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxvQ0FBTSxHQUFOO1FBRUksSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7SUFDakMsQ0FBQzs7Z0JBM0RKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQU8sYUFBYTtvQkFDNUIseUNBQXdDO29CQUV4QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVJzRCxVQUFVO2dCQUEwQixTQUFTOzs7MEJBWS9GLFdBQVcsU0FBQyxlQUFlO2dDQUczQixlQUFlLFNBQUMseUJBQXlCLEVBQUUsRUFBQyxXQUFXLEVBQUUsSUFBSSxFQUFDOztJQWlEbkUsMEJBQUM7Q0FBQSxBQTdERCxJQTZEQztTQXREWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBZnRlckNvbnRlbnRJbml0LCBDb21wb25lbnQsIENvbnRlbnRDaGlsZHJlbiwgRWxlbWVudFJlZiwgSG9zdEJpbmRpbmcsIFF1ZXJ5TGlzdCwgUmVuZGVyZXIyLCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGdXNlV2lkZ2V0VG9nZ2xlRGlyZWN0aXZlIH0gZnJvbSAnLi93aWRnZXQtdG9nZ2xlLmRpcmVjdGl2ZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yICAgICA6ICdmdXNlLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybCAgOiAnLi93aWRnZXQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzICAgIDogWycuL3dpZGdldC5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIEZ1c2VXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlckNvbnRlbnRJbml0XHJcbntcclxuICAgIEBIb3N0QmluZGluZygnY2xhc3MuZmxpcHBlZCcpXHJcbiAgICBmbGlwcGVkID0gZmFsc2U7XHJcblxyXG4gICAgQENvbnRlbnRDaGlsZHJlbihGdXNlV2lkZ2V0VG9nZ2xlRGlyZWN0aXZlLCB7ZGVzY2VuZGFudHM6IHRydWV9KVxyXG4gICAgdG9nZ2xlQnV0dG9uczogUXVlcnlMaXN0PEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmU+O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge0VsZW1lbnRSZWZ9IF9lbGVtZW50UmVmXHJcbiAgICAgKiBAcGFyYW0ge1JlbmRlcmVyMn0gX3JlbmRlcmVyXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICAgKVxyXG4gICAge1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFmdGVyIGNvbnRlbnQgaW5pdFxyXG4gICAgICovXHJcbiAgICBuZ0FmdGVyQ29udGVudEluaXQoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIExpc3RlbiBmb3IgdGhlIGZsaXAgYnV0dG9uIGNsaWNrXHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudG9nZ2xlQnV0dG9ucy5mb3JFYWNoKGZsaXBCdXR0b24gPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5fcmVuZGVyZXIubGlzdGVuKGZsaXBCdXR0b24uZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnY2xpY2snLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUb2dnbGUgdGhlIGZsaXBwZWQgc3RhdHVzXHJcbiAgICAgKi9cclxuICAgIHRvZ2dsZSgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5mbGlwcGVkID0gIXRoaXMuZmxpcHBlZDtcclxuICAgIH1cclxuXHJcbn1cclxuIl19