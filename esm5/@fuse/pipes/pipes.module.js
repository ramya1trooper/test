import { NgModule } from '@angular/core';
import { KeysPipe } from './keys.pipe';
import { GetByIdPipe } from './getById.pipe';
import { HtmlToPlaintextPipe } from './htmlToPlaintext.pipe';
import { FilterPipe } from './filter.pipe';
import { CamelCaseToDashPipe } from './camelCaseToDash.pipe';
var FusePipesModule = /** @class */ (function () {
    function FusePipesModule() {
    }
    FusePipesModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        KeysPipe,
                        GetByIdPipe,
                        HtmlToPlaintextPipe,
                        FilterPipe,
                        CamelCaseToDashPipe
                    ],
                    imports: [],
                    exports: [
                        KeysPipe,
                        GetByIdPipe,
                        HtmlToPlaintextPipe,
                        FilterPipe,
                        CamelCaseToDashPipe
                    ]
                },] }
    ];
    return FusePipesModule;
}());
export { FusePipesModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGlwZXMubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL3BpcGVzL3BpcGVzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXpDLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDdkMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzdELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFN0Q7SUFBQTtJQW1CQSxDQUFDOztnQkFuQkEsUUFBUSxTQUFDO29CQUNOLFlBQVksRUFBRTt3QkFDVixRQUFRO3dCQUNSLFdBQVc7d0JBQ1gsbUJBQW1CO3dCQUNuQixVQUFVO3dCQUNWLG1CQUFtQjtxQkFDdEI7b0JBQ0QsT0FBTyxFQUFPLEVBQUU7b0JBQ2hCLE9BQU8sRUFBTzt3QkFDVixRQUFRO3dCQUNSLFdBQVc7d0JBQ1gsbUJBQW1CO3dCQUNuQixVQUFVO3dCQUNWLG1CQUFtQjtxQkFDdEI7aUJBQ0o7O0lBR0Qsc0JBQUM7Q0FBQSxBQW5CRCxJQW1CQztTQUZZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgS2V5c1BpcGUgfSBmcm9tICcuL2tleXMucGlwZSc7XHJcbmltcG9ydCB7IEdldEJ5SWRQaXBlIH0gZnJvbSAnLi9nZXRCeUlkLnBpcGUnO1xyXG5pbXBvcnQgeyBIdG1sVG9QbGFpbnRleHRQaXBlIH0gZnJvbSAnLi9odG1sVG9QbGFpbnRleHQucGlwZSc7XHJcbmltcG9ydCB7IEZpbHRlclBpcGUgfSBmcm9tICcuL2ZpbHRlci5waXBlJztcclxuaW1wb3J0IHsgQ2FtZWxDYXNlVG9EYXNoUGlwZSB9IGZyb20gJy4vY2FtZWxDYXNlVG9EYXNoLnBpcGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEtleXNQaXBlLFxyXG4gICAgICAgIEdldEJ5SWRQaXBlLFxyXG4gICAgICAgIEh0bWxUb1BsYWludGV4dFBpcGUsXHJcbiAgICAgICAgRmlsdGVyUGlwZSxcclxuICAgICAgICBDYW1lbENhc2VUb0Rhc2hQaXBlXHJcbiAgICBdLFxyXG4gICAgaW1wb3J0cyAgICAgOiBbXSxcclxuICAgIGV4cG9ydHMgICAgIDogW1xyXG4gICAgICAgIEtleXNQaXBlLFxyXG4gICAgICAgIEdldEJ5SWRQaXBlLFxyXG4gICAgICAgIEh0bWxUb1BsYWludGV4dFBpcGUsXHJcbiAgICAgICAgRmlsdGVyUGlwZSxcclxuICAgICAgICBDYW1lbENhc2VUb0Rhc2hQaXBlXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlUGlwZXNNb2R1bGVcclxue1xyXG59XHJcbiJdfQ==