import { Pipe } from "@angular/core";
import { FuseUtils } from "../../@fuse/utils";
var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    /**
     * Transform
     *
     * @param {any[]} mainArr
     * @param {string} searchText
     * @param {string} property
     * @returns {any}
     */
    FilterPipe.prototype.transform = function (mainArr, searchText, property) {
        return FuseUtils.filterArrayByString(mainArr, searchText);
    };
    FilterPipe.decorators = [
        { type: Pipe, args: [{ name: "filter" },] }
    ];
    return FilterPipe;
}());
export { FilterPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvcGlwZXMvZmlsdGVyLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRTlDO0lBQUE7SUFhQSxDQUFDO0lBWEM7Ozs7Ozs7T0FPRztJQUNILDhCQUFTLEdBQVQsVUFBVSxPQUFjLEVBQUUsVUFBa0IsRUFBRSxRQUFnQjtRQUM1RCxPQUFPLFNBQVMsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDNUQsQ0FBQzs7Z0JBWkYsSUFBSSxTQUFDLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRTs7SUFheEIsaUJBQUM7Q0FBQSxBQWJELElBYUM7U0FaWSxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEZ1c2VVdGlscyB9IGZyb20gXCIuLi8uLi9AZnVzZS91dGlsc1wiO1xyXG5cclxuQFBpcGUoeyBuYW1lOiBcImZpbHRlclwiIH0pXHJcbmV4cG9ydCBjbGFzcyBGaWx0ZXJQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSB7XHJcbiAgLyoqXHJcbiAgICogVHJhbnNmb3JtXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge2FueVtdfSBtYWluQXJyXHJcbiAgICogQHBhcmFtIHtzdHJpbmd9IHNlYXJjaFRleHRcclxuICAgKiBAcGFyYW0ge3N0cmluZ30gcHJvcGVydHlcclxuICAgKiBAcmV0dXJucyB7YW55fVxyXG4gICAqL1xyXG4gIHRyYW5zZm9ybShtYWluQXJyOiBhbnlbXSwgc2VhcmNoVGV4dDogc3RyaW5nLCBwcm9wZXJ0eTogc3RyaW5nKTogYW55IHtcclxuICAgIHJldHVybiBGdXNlVXRpbHMuZmlsdGVyQXJyYXlCeVN0cmluZyhtYWluQXJyLCBzZWFyY2hUZXh0KTtcclxuICB9XHJcbn1cclxuIl19