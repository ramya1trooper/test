import { Pipe } from '@angular/core';
var KeysPipe = /** @class */ (function () {
    function KeysPipe() {
    }
    /**
     * Transform
     *
     * @param value
     * @param {string[]} args
     * @returns {any}
     */
    KeysPipe.prototype.transform = function (value, args) {
        var keys = [];
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                keys.push({
                    key: key,
                    value: value[key]
                });
            }
        }
        return keys;
    };
    KeysPipe.decorators = [
        { type: Pipe, args: [{ name: 'keys' },] }
    ];
    return KeysPipe;
}());
export { KeysPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5cy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL3BpcGVzL2tleXMucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBMkJBLENBQUM7SUF4Qkc7Ozs7OztPQU1HO0lBQ0gsNEJBQVMsR0FBVCxVQUFVLEtBQVUsRUFBRSxJQUFjO1FBRWhDLElBQU0sSUFBSSxHQUFVLEVBQUUsQ0FBQztRQUV2QixLQUFNLElBQU0sR0FBRyxJQUFJLEtBQUssRUFDeEI7WUFDSSxJQUFLLEtBQUssQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQzlCO2dCQUNJLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ04sR0FBRyxFQUFJLEdBQUc7b0JBQ1YsS0FBSyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUM7aUJBQ3BCLENBQUMsQ0FBQzthQUNOO1NBQ0o7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOztnQkExQkosSUFBSSxTQUFDLEVBQUMsSUFBSSxFQUFFLE1BQU0sRUFBQzs7SUEyQnBCLGVBQUM7Q0FBQSxBQTNCRCxJQTJCQztTQTFCWSxRQUFRIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQFBpcGUoe25hbWU6ICdrZXlzJ30pXHJcbmV4cG9ydCBjbGFzcyBLZXlzUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm1cclxue1xyXG4gICAgLyoqXHJcbiAgICAgKiBUcmFuc2Zvcm1cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nW119IGFyZ3NcclxuICAgICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICAgKi9cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBhcmdzOiBzdHJpbmdbXSk6IGFueVxyXG4gICAge1xyXG4gICAgICAgIGNvbnN0IGtleXM6IGFueVtdID0gW107XHJcblxyXG4gICAgICAgIGZvciAoIGNvbnN0IGtleSBpbiB2YWx1ZSApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZiAoIHZhbHVlLmhhc093blByb3BlcnR5KGtleSkgKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBrZXlzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGtleSAgOiBrZXksXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHZhbHVlW2tleV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4ga2V5cztcclxuICAgIH1cclxufVxyXG4iXX0=