import { Pipe } from '@angular/core';
var HtmlToPlaintextPipe = /** @class */ (function () {
    function HtmlToPlaintextPipe() {
    }
    /**
     * Transform
     *
     * @param {string} value
     * @param {any[]} args
     * @returns {string}
     */
    HtmlToPlaintextPipe.prototype.transform = function (value, args) {
        if (args === void 0) { args = []; }
        return value ? String(value).replace(/<[^>]+>/gm, '') : '';
    };
    HtmlToPlaintextPipe.decorators = [
        { type: Pipe, args: [{ name: 'htmlToPlaintext' },] }
    ];
    return HtmlToPlaintextPipe;
}());
export { HtmlToPlaintextPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbFRvUGxhaW50ZXh0LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvcGlwZXMvaHRtbFRvUGxhaW50ZXh0LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTtJQWNBLENBQUM7SUFYRzs7Ozs7O09BTUc7SUFDSCx1Q0FBUyxHQUFULFVBQVUsS0FBYSxFQUFFLElBQWdCO1FBQWhCLHFCQUFBLEVBQUEsU0FBZ0I7UUFFckMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDL0QsQ0FBQzs7Z0JBYkosSUFBSSxTQUFDLEVBQUMsSUFBSSxFQUFFLGlCQUFpQixFQUFDOztJQWMvQiwwQkFBQztDQUFBLEFBZEQsSUFjQztTQWJZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtuYW1lOiAnaHRtbFRvUGxhaW50ZXh0J30pXHJcbmV4cG9ydCBjbGFzcyBIdG1sVG9QbGFpbnRleHRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybVxyXG57XHJcbiAgICAvKipcclxuICAgICAqIFRyYW5zZm9ybVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG4gICAgICogQHBhcmFtIHthbnlbXX0gYXJnc1xyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBzdHJpbmcsIGFyZ3M6IGFueVtdID0gW10pOiBzdHJpbmdcclxuICAgIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUgPyBTdHJpbmcodmFsdWUpLnJlcGxhY2UoLzxbXj5dKz4vZ20sICcnKSA6ICcnO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==