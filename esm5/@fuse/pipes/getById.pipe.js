import { Pipe } from '@angular/core';
var GetByIdPipe = /** @class */ (function () {
    function GetByIdPipe() {
    }
    /**
     * Transform
     *
     * @param {any[]} value
     * @param {number} id
     * @param {string} property
     * @returns {any}
     */
    GetByIdPipe.prototype.transform = function (value, id, property) {
        var foundItem = value.find(function (item) {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (foundItem) {
            return foundItem[property];
        }
    };
    GetByIdPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'getById',
                    pure: false
                },] }
    ];
    return GetByIdPipe;
}());
export { GetByIdPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0QnlJZC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL3BpcGVzL2dldEJ5SWQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBOEJBLENBQUM7SUF4Qkc7Ozs7Ozs7T0FPRztJQUNILCtCQUFTLEdBQVQsVUFBVSxLQUFZLEVBQUUsRUFBVSxFQUFFLFFBQWdCO1FBRWhELElBQU0sU0FBUyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsVUFBQSxJQUFJO1lBQzdCLElBQUssSUFBSSxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQzFCO2dCQUNJLE9BQU8sSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUM7YUFDekI7WUFFRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztRQUVILElBQUssU0FBUyxFQUNkO1lBQ0ksT0FBTyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDOUI7SUFDTCxDQUFDOztnQkE3QkosSUFBSSxTQUFDO29CQUNGLElBQUksRUFBRSxTQUFTO29CQUNmLElBQUksRUFBRSxLQUFLO2lCQUNkOztJQTJCRCxrQkFBQztDQUFBLEFBOUJELElBOEJDO1NBMUJZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7XHJcbiAgICBuYW1lOiAnZ2V0QnlJZCcsXHJcbiAgICBwdXJlOiBmYWxzZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgR2V0QnlJZFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtXHJcbntcclxuICAgIC8qKlxyXG4gICAgICogVHJhbnNmb3JtXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHthbnlbXX0gdmFsdWVcclxuICAgICAqIEBwYXJhbSB7bnVtYmVyfSBpZFxyXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHByb3BlcnR5XHJcbiAgICAgKiBAcmV0dXJucyB7YW55fVxyXG4gICAgICovXHJcbiAgICB0cmFuc2Zvcm0odmFsdWU6IGFueVtdLCBpZDogbnVtYmVyLCBwcm9wZXJ0eTogc3RyaW5nKTogYW55XHJcbiAgICB7XHJcbiAgICAgICAgY29uc3QgZm91bmRJdGVtID0gdmFsdWUuZmluZChpdGVtID0+IHtcclxuICAgICAgICAgICAgaWYgKCBpdGVtLmlkICE9PSB1bmRlZmluZWQgKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaXRlbS5pZCA9PT0gaWQ7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKCBmb3VuZEl0ZW0gKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZvdW5kSXRlbVtwcm9wZXJ0eV07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==