import { Pipe } from '@angular/core';
var CamelCaseToDashPipe = /** @class */ (function () {
    function CamelCaseToDashPipe() {
    }
    /**
     * Transform
     *
     * @param {string} value
     * @param {any[]} args
     * @returns {string}
     */
    CamelCaseToDashPipe.prototype.transform = function (value, args) {
        if (args === void 0) { args = []; }
        return value ? String(value).replace(/([A-Z])/g, function (g) { return "-" + g[0].toLowerCase(); }) : '';
    };
    CamelCaseToDashPipe.decorators = [
        { type: Pipe, args: [{ name: 'camelCaseToDash' },] }
    ];
    return CamelCaseToDashPipe;
}());
export { CamelCaseToDashPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FtZWxDYXNlVG9EYXNoLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvcGlwZXMvY2FtZWxDYXNlVG9EYXNoLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTtJQWNBLENBQUM7SUFYRzs7Ozs7O09BTUc7SUFDSCx1Q0FBUyxHQUFULFVBQVUsS0FBYSxFQUFFLElBQWdCO1FBQWhCLHFCQUFBLEVBQUEsU0FBZ0I7UUFFckMsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLFVBQUMsQ0FBQyxJQUFLLE9BQUEsTUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFJLEVBQXhCLENBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzNGLENBQUM7O2dCQWJKLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxpQkFBaUIsRUFBQzs7SUFjL0IsMEJBQUM7Q0FBQSxBQWRELElBY0M7U0FiWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7bmFtZTogJ2NhbWVsQ2FzZVRvRGFzaCd9KVxyXG5leHBvcnQgY2xhc3MgQ2FtZWxDYXNlVG9EYXNoUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm1cclxue1xyXG4gICAgLyoqXHJcbiAgICAgKiBUcmFuc2Zvcm1cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuICAgICAqIEBwYXJhbSB7YW55W119IGFyZ3NcclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCBhcmdzOiBhbnlbXSA9IFtdKTogc3RyaW5nXHJcbiAgICB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlID8gU3RyaW5nKHZhbHVlKS5yZXBsYWNlKC8oW0EtWl0pL2csIChnKSA9PiBgLSR7Z1swXS50b0xvd2VyQ2FzZSgpfWApIDogJyc7XHJcbiAgICB9XHJcbn1cclxuIl19