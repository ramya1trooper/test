import { sequence, trigger, animate, style, group, query, transition, animateChild, state, animation, useAnimation, stagger } from '@angular/animations';
var customAnimation = animation([
    style({
        opacity: '{{opacity}}',
        transform: 'scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})'
    }),
    animate('{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)', style('*'))
], {
    params: {
        duration: '200ms',
        delay: '0ms',
        opacity: '0',
        scale: '1',
        x: '0',
        y: '0',
        z: '0'
    }
});
export var fuseAnimations = [
    trigger('animate', [transition('void => *', [useAnimation(customAnimation)])]),
    trigger('animateStagger', [
        state('50', style('*')),
        state('100', style('*')),
        state('200', style('*')),
        transition('void => 50', query('@*', [
            stagger('50ms', [
                animateChild()
            ])
        ], { optional: true })),
        transition('void => 100', query('@*', [
            stagger('100ms', [
                animateChild()
            ])
        ], { optional: true })),
        transition('void => 200', query('@*', [
            stagger('200ms', [
                animateChild()
            ])
        ], { optional: true }))
    ]),
    trigger('fadeInOut', [
        state('0', style({
            display: 'none',
            opacity: 0
        })),
        state('1', style({
            display: 'block',
            opacity: 1
        })),
        transition('1 => 0', animate('300ms ease-out')),
        transition('0 => 1', animate('300ms ease-in'))
    ]),
    trigger('slideInOut', [
        state('0', style({
            height: '0px',
            display: 'none'
        })),
        state('1', style({
            height: '*',
            display: 'block'
        })),
        transition('1 => 0', animate('300ms ease-out')),
        transition('0 => 1', animate('300ms ease-in'))
    ]),
    trigger('slideIn', [
        transition('void => left', [
            style({
                transform: 'translateX(100%)'
            }),
            animate('300ms ease-in', style({
                transform: 'translateX(0)'
            }))
        ]),
        transition('left => void', [
            style({
                transform: 'translateX(0)'
            }),
            animate('300ms ease-in', style({
                transform: 'translateX(-100%)'
            }))
        ]),
        transition('void => right', [
            style({
                transform: 'translateX(-100%)'
            }),
            animate('300ms ease-in', style({
                transform: 'translateX(0)'
            }))
        ]),
        transition('right => void', [
            style({
                transform: 'translateX(0)'
            }),
            animate('300ms ease-in', style({
                transform: 'translateX(100%)'
            }))
        ]),
    ]),
    trigger('slideInLeft', [
        state('void', style({
            transform: 'translateX(-100%)',
            display: 'none'
        })),
        state('*', style({
            transform: 'translateX(0)',
            display: 'flex'
        })),
        transition('void => *', animate('300ms')),
        transition('* => void', animate('300ms'))
    ]),
    trigger('slideInRight', [
        state('void', style({
            transform: 'translateX(100%)',
            display: 'none'
        })),
        state('*', style({
            transform: 'translateX(0)',
            display: 'flex'
        })),
        transition('void => *', animate('300ms')),
        transition('* => void', animate('300ms'))
    ]),
    trigger('slideInTop', [
        state('void', style({
            transform: 'translateY(-100%)',
            display: 'none'
        })),
        state('*', style({
            transform: 'translateY(0)',
            display: 'flex'
        })),
        transition('void => *', animate('300ms')),
        transition('* => void', animate('300ms'))
    ]),
    trigger('slideInBottom', [
        state('void', style({
            transform: 'translateY(100%)',
            display: 'none'
        })),
        state('*', style({
            transform: 'translateY(0)',
            display: 'flex'
        })),
        transition('void => *', animate('300ms')),
        transition('* => void', animate('300ms'))
    ]),
    trigger('expandCollapse', [
        state('void', style({
            height: '0px'
        })),
        state('*', style({
            height: '*'
        })),
        transition('void => *', animate('300ms ease-out')),
        transition('* => void', animate('300ms ease-in'))
    ]),
    // -----------------------------------------------------------------------------------------------------
    // @ Router animations
    // -----------------------------------------------------------------------------------------------------
    trigger('routerTransitionLeft', [
        transition('* => *', [
            query('content > :enter, content > :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0
                })
            ], { optional: true }),
            query('content > :enter', [
                style({
                    transform: 'translateX(100%)',
                    opacity: 0
                })
            ], { optional: true }),
            sequence([
                group([
                    query('content > :leave', [
                        style({
                            transform: 'translateX(0)',
                            opacity: 1
                        }),
                        animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                            transform: 'translateX(-100%)',
                            opacity: 0
                        }))
                    ], { optional: true }),
                    query('content > :enter', [
                        style({ transform: 'translateX(100%)' }),
                        animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                            transform: 'translateX(0%)',
                            opacity: 1
                        }))
                    ], { optional: true })
                ]),
                query('content > :leave', animateChild(), { optional: true }),
                query('content > :enter', animateChild(), { optional: true })
            ])
        ])
    ]),
    trigger('routerTransitionRight', [
        transition('* => *', [
            query('content > :enter, content > :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0
                })
            ], { optional: true }),
            query('content > :enter', [
                style({
                    transform: 'translateX(-100%)',
                    opacity: 0
                })
            ], { optional: true }),
            sequence([
                group([
                    query('content > :leave', [
                        style({
                            transform: 'translateX(0)',
                            opacity: 1
                        }),
                        animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                            transform: 'translateX(100%)',
                            opacity: 0
                        }))
                    ], { optional: true }),
                    query('content > :enter', [
                        style({ transform: 'translateX(-100%)' }),
                        animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                            transform: 'translateX(0%)',
                            opacity: 1
                        }))
                    ], { optional: true })
                ]),
                query('content > :leave', animateChild(), { optional: true }),
                query('content > :enter', animateChild(), { optional: true })
            ])
        ])
    ]),
    trigger('routerTransitionUp', [
        transition('* => *', [
            query('content > :enter, content > :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0
                })
            ], { optional: true }),
            query('content > :enter', [
                style({
                    transform: 'translateY(100%)',
                    opacity: 0
                })
            ], { optional: true }),
            group([
                query('content > :leave', [
                    style({
                        transform: 'translateY(0)',
                        opacity: 1
                    }),
                    animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                        transform: 'translateY(-100%)',
                        opacity: 0
                    }))
                ], { optional: true }),
                query('content > :enter', [
                    style({ transform: 'translateY(100%)' }),
                    animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                        transform: 'translateY(0%)',
                        opacity: 1
                    }))
                ], { optional: true })
            ]),
            query('content > :leave', animateChild(), { optional: true }),
            query('content > :enter', animateChild(), { optional: true })
        ])
    ]),
    trigger('routerTransitionDown', [
        transition('* => *', [
            query('content > :enter, content > :leave', [
                style({
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0
                })
            ], { optional: true }),
            query('content > :enter', [
                style({
                    transform: 'translateY(-100%)',
                    opacity: 0
                })
            ], { optional: true }),
            sequence([
                group([
                    query('content > :leave', [
                        style({
                            transform: 'translateY(0)',
                            opacity: 1
                        }),
                        animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                            transform: 'translateY(100%)',
                            opacity: 0
                        }))
                    ], { optional: true }),
                    query('content > :enter', [
                        style({ transform: 'translateY(-100%)' }),
                        animate('600ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                            transform: 'translateY(0%)',
                            opacity: 1
                        }))
                    ], { optional: true })
                ]),
                query('content > :leave', animateChild(), { optional: true }),
                query('content > :enter', animateChild(), { optional: true })
            ])
        ])
    ]),
    trigger('routerTransitionFade', [
        transition('* => *', group([
            query('content > :enter, content > :leave ', [
                style({
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: 0,
                    right: 0
                })
            ], { optional: true }),
            query('content > :enter', [
                style({
                    opacity: 0
                })
            ], { optional: true }),
            query('content > :leave', [
                style({
                    opacity: 1
                }),
                animate('300ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                    opacity: 0
                }))
            ], { optional: true }),
            query('content > :enter', [
                style({
                    opacity: 0
                }),
                animate('300ms cubic-bezier(0.0, 0.0, 0.2, 1)', style({
                    opacity: 1
                }))
            ], { optional: true }),
            query('content > :enter', animateChild(), { optional: true }),
            query('content > :leave', animateChild(), { optional: true })
        ]))
    ])
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvYW5pbWF0aW9ucy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUV6SixJQUFNLGVBQWUsR0FBRyxTQUFTLENBQUM7SUFDOUIsS0FBSyxDQUFDO1FBQ0YsT0FBTyxFQUFJLGFBQWE7UUFDeEIsU0FBUyxFQUFFLG1EQUFtRDtLQUNqRSxDQUFDO0lBQ0YsT0FBTyxDQUFDLHVEQUF1RCxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztDQUMvRSxFQUFFO0lBQ0MsTUFBTSxFQUFFO1FBQ0osUUFBUSxFQUFFLE9BQU87UUFDakIsS0FBSyxFQUFLLEtBQUs7UUFDZixPQUFPLEVBQUcsR0FBRztRQUNiLEtBQUssRUFBSyxHQUFHO1FBQ2IsQ0FBQyxFQUFTLEdBQUc7UUFDYixDQUFDLEVBQVMsR0FBRztRQUNiLENBQUMsRUFBUyxHQUFHO0tBQ2hCO0NBQ0osQ0FBQyxDQUFDO0FBRUgsTUFBTSxDQUFDLElBQU0sY0FBYyxHQUFHO0lBRTFCLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsWUFBWSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBRTlFLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRTtRQUN0QixLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN2QixLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN4QixLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUV4QixVQUFVLENBQUMsWUFBWSxFQUNuQixLQUFLLENBQUMsSUFBSSxFQUNOO1lBQ0ksT0FBTyxDQUFDLE1BQU0sRUFBRTtnQkFDWixZQUFZLEVBQUU7YUFDakIsQ0FBQztTQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztRQUM3QixVQUFVLENBQUMsYUFBYSxFQUNwQixLQUFLLENBQUMsSUFBSSxFQUNOO1lBQ0ksT0FBTyxDQUFDLE9BQU8sRUFBRTtnQkFDYixZQUFZLEVBQUU7YUFDakIsQ0FBQztTQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztRQUM3QixVQUFVLENBQUMsYUFBYSxFQUNwQixLQUFLLENBQUMsSUFBSSxFQUNOO1lBQ0ksT0FBTyxDQUFDLE9BQU8sRUFBRTtnQkFDYixZQUFZLEVBQUU7YUFDakIsQ0FBQztTQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztLQUNoQyxDQUFDO0lBRUYsT0FBTyxDQUFDLFdBQVcsRUFBRTtRQUNqQixLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztZQUNiLE9BQU8sRUFBRSxNQUFNO1lBQ2YsT0FBTyxFQUFFLENBQUM7U0FDYixDQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztZQUNiLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLE9BQU8sRUFBRSxDQUFDO1NBQ2IsQ0FBQyxDQUFDO1FBQ0gsVUFBVSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUMvQyxVQUFVLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQyxlQUFlLENBQUMsQ0FBQztLQUNqRCxDQUFDO0lBRUYsT0FBTyxDQUFDLFlBQVksRUFBRTtRQUNsQixLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztZQUNiLE1BQU0sRUFBRyxLQUFLO1lBQ2QsT0FBTyxFQUFFLE1BQU07U0FDbEIsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUM7WUFDYixNQUFNLEVBQUcsR0FBRztZQUNaLE9BQU8sRUFBRSxPQUFPO1NBQ25CLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDL0MsVUFBVSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7S0FDakQsQ0FBQztJQUVGLE9BQU8sQ0FBQyxTQUFTLEVBQUU7UUFDZixVQUFVLENBQUMsY0FBYyxFQUFFO1lBQ25CLEtBQUssQ0FBQztnQkFDRixTQUFTLEVBQUUsa0JBQWtCO2FBQ2hDLENBQUM7WUFDRixPQUFPLENBQUMsZUFBZSxFQUNuQixLQUFLLENBQUM7Z0JBQ0YsU0FBUyxFQUFFLGVBQWU7YUFDN0IsQ0FBQyxDQUNMO1NBQ0osQ0FDSjtRQUNELFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDbkIsS0FBSyxDQUFDO2dCQUNGLFNBQVMsRUFBRSxlQUFlO2FBQzdCLENBQUM7WUFDRixPQUFPLENBQUMsZUFBZSxFQUNuQixLQUFLLENBQUM7Z0JBQ0YsU0FBUyxFQUFFLG1CQUFtQjthQUNqQyxDQUFDLENBQ0w7U0FDSixDQUNKO1FBQ0QsVUFBVSxDQUFDLGVBQWUsRUFBRTtZQUNwQixLQUFLLENBQUM7Z0JBQ0YsU0FBUyxFQUFFLG1CQUFtQjthQUNqQyxDQUFDO1lBQ0YsT0FBTyxDQUFDLGVBQWUsRUFDbkIsS0FBSyxDQUFDO2dCQUNGLFNBQVMsRUFBRSxlQUFlO2FBQzdCLENBQUMsQ0FDTDtTQUNKLENBQ0o7UUFDRCxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3BCLEtBQUssQ0FBQztnQkFDRixTQUFTLEVBQUUsZUFBZTthQUM3QixDQUFDO1lBQ0YsT0FBTyxDQUFDLGVBQWUsRUFDbkIsS0FBSyxDQUFDO2dCQUNGLFNBQVMsRUFBRSxrQkFBa0I7YUFDaEMsQ0FBQyxDQUNMO1NBQ0osQ0FDSjtLQUNKLENBQUM7SUFFRixPQUFPLENBQUMsYUFBYSxFQUFFO1FBQ25CLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDO1lBQ2hCLFNBQVMsRUFBRSxtQkFBbUI7WUFDOUIsT0FBTyxFQUFJLE1BQU07U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUM7WUFDYixTQUFTLEVBQUUsZUFBZTtZQUMxQixPQUFPLEVBQUksTUFBTTtTQUNwQixDQUFDLENBQUM7UUFDSCxVQUFVLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxVQUFVLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUM1QyxDQUFDO0lBRUYsT0FBTyxDQUFDLGNBQWMsRUFBRTtRQUNwQixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQztZQUNoQixTQUFTLEVBQUUsa0JBQWtCO1lBQzdCLE9BQU8sRUFBSSxNQUFNO1NBQ3BCLENBQUMsQ0FBQztRQUNILEtBQUssQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDO1lBQ2IsU0FBUyxFQUFFLGVBQWU7WUFDMUIsT0FBTyxFQUFJLE1BQU07U0FDcEIsQ0FBQyxDQUFDO1FBQ0gsVUFBVSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekMsVUFBVSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7S0FDNUMsQ0FBQztJQUVGLE9BQU8sQ0FBQyxZQUFZLEVBQUU7UUFDbEIsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUM7WUFDaEIsU0FBUyxFQUFFLG1CQUFtQjtZQUM5QixPQUFPLEVBQUksTUFBTTtTQUNwQixDQUFDLENBQUM7UUFDSCxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztZQUNiLFNBQVMsRUFBRSxlQUFlO1lBQzFCLE9BQU8sRUFBSSxNQUFNO1NBQ3BCLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQzVDLENBQUM7SUFFRixPQUFPLENBQUMsZUFBZSxFQUFFO1FBQ3JCLEtBQUssQ0FBQyxNQUFNLEVBQ1IsS0FBSyxDQUFDO1lBQ0YsU0FBUyxFQUFFLGtCQUFrQjtZQUM3QixPQUFPLEVBQUksTUFBTTtTQUNwQixDQUFDLENBQUM7UUFDUCxLQUFLLENBQUMsR0FBRyxFQUFFLEtBQUssQ0FBQztZQUNiLFNBQVMsRUFBRSxlQUFlO1lBQzFCLE9BQU8sRUFBSSxNQUFNO1NBQ3BCLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0tBQzVDLENBQUM7SUFFRixPQUFPLENBQUMsZ0JBQWdCLEVBQUU7UUFDdEIsS0FBSyxDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUM7WUFDaEIsTUFBTSxFQUFFLEtBQUs7U0FDaEIsQ0FBQyxDQUFDO1FBQ0gsS0FBSyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUM7WUFDYixNQUFNLEVBQUUsR0FBRztTQUNkLENBQUMsQ0FBQztRQUNILFVBQVUsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDbEQsVUFBVSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsZUFBZSxDQUFDLENBQUM7S0FDcEQsQ0FBQztJQUVGLHdHQUF3RztJQUN4RyxzQkFBc0I7SUFDdEIsd0dBQXdHO0lBRXhHLE9BQU8sQ0FBQyxzQkFBc0IsRUFBRTtRQUU1QixVQUFVLENBQUMsUUFBUSxFQUFFO1lBQ2pCLEtBQUssQ0FBQyxvQ0FBb0MsRUFBRTtnQkFDeEMsS0FBSyxDQUFDO29CQUNGLFFBQVEsRUFBRSxVQUFVO29CQUNwQixHQUFHLEVBQU8sQ0FBQztvQkFDWCxNQUFNLEVBQUksQ0FBQztvQkFDWCxJQUFJLEVBQU0sQ0FBQztvQkFDWCxLQUFLLEVBQUssQ0FBQztpQkFDZCxDQUFDO2FBQ0wsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUNwQixLQUFLLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQztvQkFDRixTQUFTLEVBQUUsa0JBQWtCO29CQUM3QixPQUFPLEVBQUksQ0FBQztpQkFDZixDQUFDO2FBQ0wsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUNwQixRQUFRLENBQUM7Z0JBQ0wsS0FBSyxDQUFDO29CQUNGLEtBQUssQ0FBQyxrQkFBa0IsRUFBRTt3QkFDdEIsS0FBSyxDQUFDOzRCQUNGLFNBQVMsRUFBRSxlQUFlOzRCQUMxQixPQUFPLEVBQUksQ0FBQzt5QkFDZixDQUFDO3dCQUNGLE9BQU8sQ0FBQyxzQ0FBc0MsRUFDMUMsS0FBSyxDQUFDOzRCQUNGLFNBQVMsRUFBRSxtQkFBbUI7NEJBQzlCLE9BQU8sRUFBSSxDQUFDO3lCQUNmLENBQUMsQ0FBQztxQkFDVixFQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO29CQUNwQixLQUFLLENBQUMsa0JBQWtCLEVBQUU7d0JBQ3RCLEtBQUssQ0FBQyxFQUFDLFNBQVMsRUFBRSxrQkFBa0IsRUFBQyxDQUFDO3dCQUN0QyxPQUFPLENBQUMsc0NBQXNDLEVBQzFDLEtBQUssQ0FBQzs0QkFDRixTQUFTLEVBQUUsZ0JBQWdCOzRCQUMzQixPQUFPLEVBQUksQ0FBQzt5QkFDZixDQUFDLENBQUM7cUJBQ1YsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztpQkFDdkIsQ0FBQztnQkFDRixLQUFLLENBQUMsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7Z0JBQzNELEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQzthQUM5RCxDQUFDO1NBQ0wsQ0FBQztLQUNMLENBQUM7SUFFRixPQUFPLENBQUMsdUJBQXVCLEVBQUU7UUFFN0IsVUFBVSxDQUFDLFFBQVEsRUFBRTtZQUNqQixLQUFLLENBQUMsb0NBQW9DLEVBQUU7Z0JBQ3hDLEtBQUssQ0FBQztvQkFDRixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsR0FBRyxFQUFPLENBQUM7b0JBQ1gsTUFBTSxFQUFJLENBQUM7b0JBQ1gsSUFBSSxFQUFNLENBQUM7b0JBQ1gsS0FBSyxFQUFLLENBQUM7aUJBQ2QsQ0FBQzthQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7WUFDcEIsS0FBSyxDQUFDLGtCQUFrQixFQUFFO2dCQUN0QixLQUFLLENBQUM7b0JBQ0YsU0FBUyxFQUFFLG1CQUFtQjtvQkFDOUIsT0FBTyxFQUFJLENBQUM7aUJBQ2YsQ0FBQzthQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7WUFDcEIsUUFBUSxDQUFDO2dCQUNMLEtBQUssQ0FBQztvQkFDRixLQUFLLENBQUMsa0JBQWtCLEVBQUU7d0JBQ3RCLEtBQUssQ0FBQzs0QkFDRixTQUFTLEVBQUUsZUFBZTs0QkFDMUIsT0FBTyxFQUFJLENBQUM7eUJBQ2YsQ0FBQzt3QkFDRixPQUFPLENBQUMsc0NBQXNDLEVBQzFDLEtBQUssQ0FBQzs0QkFDRixTQUFTLEVBQUUsa0JBQWtCOzRCQUM3QixPQUFPLEVBQUksQ0FBQzt5QkFDZixDQUFDLENBQUM7cUJBQ1YsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztvQkFDcEIsS0FBSyxDQUFDLGtCQUFrQixFQUFFO3dCQUN0QixLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsbUJBQW1CLEVBQUMsQ0FBQzt3QkFDdkMsT0FBTyxDQUFDLHNDQUFzQyxFQUMxQyxLQUFLLENBQUM7NEJBQ0YsU0FBUyxFQUFFLGdCQUFnQjs0QkFDM0IsT0FBTyxFQUFJLENBQUM7eUJBQ2YsQ0FBQyxDQUFDO3FCQUNWLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7aUJBQ3ZCLENBQUM7Z0JBQ0YsS0FBSyxDQUFDLGtCQUFrQixFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO2dCQUMzRCxLQUFLLENBQUMsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7YUFDOUQsQ0FBQztTQUNMLENBQUM7S0FDTCxDQUFDO0lBRUYsT0FBTyxDQUFDLG9CQUFvQixFQUFFO1FBRTFCLFVBQVUsQ0FBQyxRQUFRLEVBQUU7WUFDakIsS0FBSyxDQUFDLG9DQUFvQyxFQUFFO2dCQUN4QyxLQUFLLENBQUM7b0JBQ0YsUUFBUSxFQUFFLFVBQVU7b0JBQ3BCLEdBQUcsRUFBTyxDQUFDO29CQUNYLE1BQU0sRUFBSSxDQUFDO29CQUNYLElBQUksRUFBTSxDQUFDO29CQUNYLEtBQUssRUFBSyxDQUFDO2lCQUNkLENBQUM7YUFDTCxFQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO1lBQ3BCLEtBQUssQ0FBQyxrQkFBa0IsRUFBRTtnQkFDdEIsS0FBSyxDQUFDO29CQUNGLFNBQVMsRUFBRSxrQkFBa0I7b0JBQzdCLE9BQU8sRUFBSSxDQUFDO2lCQUNmLENBQUM7YUFDTCxFQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO1lBQ3BCLEtBQUssQ0FBQztnQkFDRixLQUFLLENBQUMsa0JBQWtCLEVBQUU7b0JBQ3RCLEtBQUssQ0FBQzt3QkFDRixTQUFTLEVBQUUsZUFBZTt3QkFDMUIsT0FBTyxFQUFJLENBQUM7cUJBQ2YsQ0FBQztvQkFDRixPQUFPLENBQUMsc0NBQXNDLEVBQzFDLEtBQUssQ0FBQzt3QkFDRixTQUFTLEVBQUUsbUJBQW1CO3dCQUM5QixPQUFPLEVBQUksQ0FBQztxQkFDZixDQUFDLENBQUM7aUJBQ1YsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztnQkFDcEIsS0FBSyxDQUFDLGtCQUFrQixFQUFFO29CQUN0QixLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsa0JBQWtCLEVBQUMsQ0FBQztvQkFDdEMsT0FBTyxDQUFDLHNDQUFzQyxFQUMxQyxLQUFLLENBQUM7d0JBQ0YsU0FBUyxFQUFFLGdCQUFnQjt3QkFDM0IsT0FBTyxFQUFJLENBQUM7cUJBQ2YsQ0FBQyxDQUFDO2lCQUNWLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7YUFDdkIsQ0FBQztZQUNGLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUMzRCxLQUFLLENBQUMsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7U0FDOUQsQ0FBQztLQUNMLENBQUM7SUFFRixPQUFPLENBQUMsc0JBQXNCLEVBQUU7UUFFNUIsVUFBVSxDQUFDLFFBQVEsRUFBRTtZQUNqQixLQUFLLENBQUMsb0NBQW9DLEVBQUU7Z0JBQ3hDLEtBQUssQ0FBQztvQkFDRixRQUFRLEVBQUUsVUFBVTtvQkFDcEIsR0FBRyxFQUFPLENBQUM7b0JBQ1gsTUFBTSxFQUFJLENBQUM7b0JBQ1gsSUFBSSxFQUFNLENBQUM7b0JBQ1gsS0FBSyxFQUFLLENBQUM7aUJBQ2QsQ0FBQzthQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7WUFDcEIsS0FBSyxDQUFDLGtCQUFrQixFQUFFO2dCQUN0QixLQUFLLENBQUM7b0JBQ0YsU0FBUyxFQUFFLG1CQUFtQjtvQkFDOUIsT0FBTyxFQUFJLENBQUM7aUJBQ2YsQ0FBQzthQUNMLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7WUFDcEIsUUFBUSxDQUFDO2dCQUNMLEtBQUssQ0FBQztvQkFDRixLQUFLLENBQUMsa0JBQWtCLEVBQUU7d0JBQ3RCLEtBQUssQ0FBQzs0QkFDRixTQUFTLEVBQUUsZUFBZTs0QkFDMUIsT0FBTyxFQUFJLENBQUM7eUJBQ2YsQ0FBQzt3QkFDRixPQUFPLENBQUMsc0NBQXNDLEVBQzFDLEtBQUssQ0FBQzs0QkFDRixTQUFTLEVBQUUsa0JBQWtCOzRCQUM3QixPQUFPLEVBQUksQ0FBQzt5QkFDZixDQUFDLENBQUM7cUJBQ1YsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztvQkFDcEIsS0FBSyxDQUFDLGtCQUFrQixFQUFFO3dCQUN0QixLQUFLLENBQUMsRUFBQyxTQUFTLEVBQUUsbUJBQW1CLEVBQUMsQ0FBQzt3QkFDdkMsT0FBTyxDQUFDLHNDQUFzQyxFQUMxQyxLQUFLLENBQUM7NEJBQ0YsU0FBUyxFQUFFLGdCQUFnQjs0QkFDM0IsT0FBTyxFQUFJLENBQUM7eUJBQ2YsQ0FBQyxDQUFDO3FCQUNWLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7aUJBQ3ZCLENBQUM7Z0JBQ0YsS0FBSyxDQUFDLGtCQUFrQixFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO2dCQUMzRCxLQUFLLENBQUMsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7YUFDOUQsQ0FBQztTQUNMLENBQUM7S0FDTCxDQUFDO0lBRUYsT0FBTyxDQUFDLHNCQUFzQixFQUFFO1FBRTVCLFVBQVUsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDO1lBRXZCLEtBQUssQ0FBQyxxQ0FBcUMsRUFBRTtnQkFDekMsS0FBSyxDQUFDO29CQUNGLFFBQVEsRUFBRSxVQUFVO29CQUNwQixHQUFHLEVBQU8sQ0FBQztvQkFDWCxNQUFNLEVBQUksQ0FBQztvQkFDWCxJQUFJLEVBQU0sQ0FBQztvQkFDWCxLQUFLLEVBQUssQ0FBQztpQkFDZCxDQUFDO2FBQ0wsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUVwQixLQUFLLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQztvQkFDRixPQUFPLEVBQUUsQ0FBQztpQkFDYixDQUFDO2FBQ0wsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUNwQixLQUFLLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQztvQkFDRixPQUFPLEVBQUUsQ0FBQztpQkFDYixDQUFDO2dCQUNGLE9BQU8sQ0FBQyxzQ0FBc0MsRUFDMUMsS0FBSyxDQUFDO29CQUNGLE9BQU8sRUFBRSxDQUFDO2lCQUNiLENBQUMsQ0FBQzthQUNWLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7WUFDcEIsS0FBSyxDQUFDLGtCQUFrQixFQUFFO2dCQUN0QixLQUFLLENBQUM7b0JBQ0YsT0FBTyxFQUFFLENBQUM7aUJBQ2IsQ0FBQztnQkFDRixPQUFPLENBQUMsc0NBQXNDLEVBQzFDLEtBQUssQ0FBQztvQkFDRixPQUFPLEVBQUUsQ0FBQztpQkFDYixDQUFDLENBQUM7YUFDVixFQUFFLEVBQUMsUUFBUSxFQUFFLElBQUksRUFBQyxDQUFDO1lBQ3BCLEtBQUssQ0FBQyxrQkFBa0IsRUFBRSxZQUFZLEVBQUUsRUFBRSxFQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUMsQ0FBQztZQUMzRCxLQUFLLENBQUMsa0JBQWtCLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUM7U0FDOUQsQ0FBQyxDQUFDO0tBQ04sQ0FBQztDQUNMLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBzZXF1ZW5jZSwgdHJpZ2dlciwgYW5pbWF0ZSwgc3R5bGUsIGdyb3VwLCBxdWVyeSwgdHJhbnNpdGlvbiwgYW5pbWF0ZUNoaWxkLCBzdGF0ZSwgYW5pbWF0aW9uLCB1c2VBbmltYXRpb24sIHN0YWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuXHJcbmNvbnN0IGN1c3RvbUFuaW1hdGlvbiA9IGFuaW1hdGlvbihbXHJcbiAgICBzdHlsZSh7XHJcbiAgICAgICAgb3BhY2l0eSAgOiAne3tvcGFjaXR5fX0nLFxyXG4gICAgICAgIHRyYW5zZm9ybTogJ3NjYWxlKHt7c2NhbGV9fSkgdHJhbnNsYXRlM2Qoe3t4fX0sIHt7eX19LCB7e3p9fSknXHJcbiAgICB9KSxcclxuICAgIGFuaW1hdGUoJ3t7ZHVyYXRpb259fSB7e2RlbGF5fX0gY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJywgc3R5bGUoJyonKSlcclxuXSwge1xyXG4gICAgcGFyYW1zOiB7XHJcbiAgICAgICAgZHVyYXRpb246ICcyMDBtcycsXHJcbiAgICAgICAgZGVsYXkgICA6ICcwbXMnLFxyXG4gICAgICAgIG9wYWNpdHkgOiAnMCcsXHJcbiAgICAgICAgc2NhbGUgICA6ICcxJyxcclxuICAgICAgICB4ICAgICAgIDogJzAnLFxyXG4gICAgICAgIHkgICAgICAgOiAnMCcsXHJcbiAgICAgICAgeiAgICAgICA6ICcwJ1xyXG4gICAgfVxyXG59KTtcclxuXHJcbmV4cG9ydCBjb25zdCBmdXNlQW5pbWF0aW9ucyA9IFtcclxuXHJcbiAgICB0cmlnZ2VyKCdhbmltYXRlJywgW3RyYW5zaXRpb24oJ3ZvaWQgPT4gKicsIFt1c2VBbmltYXRpb24oY3VzdG9tQW5pbWF0aW9uKV0pXSksXHJcblxyXG4gICAgdHJpZ2dlcignYW5pbWF0ZVN0YWdnZXInLCBbXHJcbiAgICAgICAgc3RhdGUoJzUwJywgc3R5bGUoJyonKSksXHJcbiAgICAgICAgc3RhdGUoJzEwMCcsIHN0eWxlKCcqJykpLFxyXG4gICAgICAgIHN0YXRlKCcyMDAnLCBzdHlsZSgnKicpKSxcclxuXHJcbiAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiA1MCcsXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdAKicsXHJcbiAgICAgICAgICAgICAgICBbXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhZ2dlcignNTBtcycsIFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZUNoaWxkKClcclxuICAgICAgICAgICAgICAgICAgICBdKVxyXG4gICAgICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSkpLFxyXG4gICAgICAgIHRyYW5zaXRpb24oJ3ZvaWQgPT4gMTAwJyxcclxuICAgICAgICAgICAgcXVlcnkoJ0AqJyxcclxuICAgICAgICAgICAgICAgIFtcclxuICAgICAgICAgICAgICAgICAgICBzdGFnZ2VyKCcxMDBtcycsIFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZUNoaWxkKClcclxuICAgICAgICAgICAgICAgICAgICBdKVxyXG4gICAgICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSkpLFxyXG4gICAgICAgIHRyYW5zaXRpb24oJ3ZvaWQgPT4gMjAwJyxcclxuICAgICAgICAgICAgcXVlcnkoJ0AqJyxcclxuICAgICAgICAgICAgICAgIFtcclxuICAgICAgICAgICAgICAgICAgICBzdGFnZ2VyKCcyMDBtcycsIFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZUNoaWxkKClcclxuICAgICAgICAgICAgICAgICAgICBdKVxyXG4gICAgICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSkpXHJcbiAgICBdKSxcclxuXHJcbiAgICB0cmlnZ2VyKCdmYWRlSW5PdXQnLCBbXHJcbiAgICAgICAgc3RhdGUoJzAnLCBzdHlsZSh7XHJcbiAgICAgICAgICAgIGRpc3BsYXk6ICdub25lJyxcclxuICAgICAgICAgICAgb3BhY2l0eTogMFxyXG4gICAgICAgIH0pKSxcclxuICAgICAgICBzdGF0ZSgnMScsIHN0eWxlKHtcclxuICAgICAgICAgICAgZGlzcGxheTogJ2Jsb2NrJyxcclxuICAgICAgICAgICAgb3BhY2l0eTogMVxyXG4gICAgICAgIH0pKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCcxID0+IDAnLCBhbmltYXRlKCczMDBtcyBlYXNlLW91dCcpKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCcwID0+IDEnLCBhbmltYXRlKCczMDBtcyBlYXNlLWluJykpXHJcbiAgICBdKSxcclxuXHJcbiAgICB0cmlnZ2VyKCdzbGlkZUluT3V0JywgW1xyXG4gICAgICAgIHN0YXRlKCcwJywgc3R5bGUoe1xyXG4gICAgICAgICAgICBoZWlnaHQgOiAnMHB4JyxcclxuICAgICAgICAgICAgZGlzcGxheTogJ25vbmUnXHJcbiAgICAgICAgfSkpLFxyXG4gICAgICAgIHN0YXRlKCcxJywgc3R5bGUoe1xyXG4gICAgICAgICAgICBoZWlnaHQgOiAnKicsXHJcbiAgICAgICAgICAgIGRpc3BsYXk6ICdibG9jaydcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignMSA9PiAwJywgYW5pbWF0ZSgnMzAwbXMgZWFzZS1vdXQnKSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignMCA9PiAxJywgYW5pbWF0ZSgnMzAwbXMgZWFzZS1pbicpKVxyXG4gICAgXSksXHJcblxyXG4gICAgdHJpZ2dlcignc2xpZGVJbicsIFtcclxuICAgICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IGxlZnQnLCBbXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgxMDAlKSdcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgnMzAwbXMgZWFzZS1pbicsXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDApJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICApLFxyXG4gICAgICAgIHRyYW5zaXRpb24oJ2xlZnQgPT4gdm9pZCcsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDApJ1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBhbmltYXRlKCczMDBtcyBlYXNlLWluJyxcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLTEwMCUpJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICApLFxyXG4gICAgICAgIHRyYW5zaXRpb24oJ3ZvaWQgPT4gcmlnaHQnLCBbXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgtMTAwJSknXHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoJzMwMG1zIGVhc2UtaW4nLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgwKSdcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCdyaWdodCA9PiB2b2lkJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMCknXHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGFuaW1hdGUoJzMwMG1zIGVhc2UtaW4nLFxyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgxMDAlKSdcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgKSxcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ3NsaWRlSW5MZWZ0JywgW1xyXG4gICAgICAgIHN0YXRlKCd2b2lkJywgc3R5bGUoe1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKC0xMDAlKScsXHJcbiAgICAgICAgICAgIGRpc3BsYXkgIDogJ25vbmUnXHJcbiAgICAgICAgfSkpLFxyXG4gICAgICAgIHN0YXRlKCcqJywgc3R5bGUoe1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDApJyxcclxuICAgICAgICAgICAgZGlzcGxheSAgOiAnZmxleCdcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgYW5pbWF0ZSgnMzAwbXMnKSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgYW5pbWF0ZSgnMzAwbXMnKSlcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ3NsaWRlSW5SaWdodCcsIFtcclxuICAgICAgICBzdGF0ZSgndm9pZCcsIHN0eWxlKHtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWCgxMDAlKScsXHJcbiAgICAgICAgICAgIGRpc3BsYXkgIDogJ25vbmUnXHJcbiAgICAgICAgfSkpLFxyXG4gICAgICAgIHN0YXRlKCcqJywgc3R5bGUoe1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDApJyxcclxuICAgICAgICAgICAgZGlzcGxheSAgOiAnZmxleCdcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgYW5pbWF0ZSgnMzAwbXMnKSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgYW5pbWF0ZSgnMzAwbXMnKSlcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ3NsaWRlSW5Ub3AnLCBbXHJcbiAgICAgICAgc3RhdGUoJ3ZvaWQnLCBzdHlsZSh7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoLTEwMCUpJyxcclxuICAgICAgICAgICAgZGlzcGxheSAgOiAnbm9uZSdcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgc3RhdGUoJyonLCBzdHlsZSh7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMCknLFxyXG4gICAgICAgICAgICBkaXNwbGF5ICA6ICdmbGV4J1xyXG4gICAgICAgIH0pKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IConLCBhbmltYXRlKCczMDBtcycpKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCcqID0+IHZvaWQnLCBhbmltYXRlKCczMDBtcycpKVxyXG4gICAgXSksXHJcblxyXG4gICAgdHJpZ2dlcignc2xpZGVJbkJvdHRvbScsIFtcclxuICAgICAgICBzdGF0ZSgndm9pZCcsXHJcbiAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMTAwJSknLFxyXG4gICAgICAgICAgICAgICAgZGlzcGxheSAgOiAnbm9uZSdcclxuICAgICAgICAgICAgfSkpLFxyXG4gICAgICAgIHN0YXRlKCcqJywgc3R5bGUoe1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKDApJyxcclxuICAgICAgICAgICAgZGlzcGxheSAgOiAnZmxleCdcclxuICAgICAgICB9KSksXHJcbiAgICAgICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgYW5pbWF0ZSgnMzAwbXMnKSksXHJcbiAgICAgICAgdHJhbnNpdGlvbignKiA9PiB2b2lkJywgYW5pbWF0ZSgnMzAwbXMnKSlcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ2V4cGFuZENvbGxhcHNlJywgW1xyXG4gICAgICAgIHN0YXRlKCd2b2lkJywgc3R5bGUoe1xyXG4gICAgICAgICAgICBoZWlnaHQ6ICcwcHgnXHJcbiAgICAgICAgfSkpLFxyXG4gICAgICAgIHN0YXRlKCcqJywgc3R5bGUoe1xyXG4gICAgICAgICAgICBoZWlnaHQ6ICcqJ1xyXG4gICAgICAgIH0pKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IConLCBhbmltYXRlKCczMDBtcyBlYXNlLW91dCcpKSxcclxuICAgICAgICB0cmFuc2l0aW9uKCcqID0+IHZvaWQnLCBhbmltYXRlKCczMDBtcyBlYXNlLWluJykpXHJcbiAgICBdKSxcclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBSb3V0ZXIgYW5pbWF0aW9uc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIFxyXG4gICAgdHJpZ2dlcigncm91dGVyVHJhbnNpdGlvbkxlZnQnLCBbXHJcblxyXG4gICAgICAgIHRyYW5zaXRpb24oJyogPT4gKicsIFtcclxuICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6ZW50ZXIsIGNvbnRlbnQgPiA6bGVhdmUnLCBbXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgdG9wICAgICA6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYm90dG9tICA6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdCAgICA6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQgICA6IDBcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDplbnRlcicsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDEwMCUpJyxcclxuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5ICA6IDBcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICBzZXF1ZW5jZShbXHJcbiAgICAgICAgICAgICAgICBncm91cChbXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6bGVhdmUnLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMCknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbmltYXRlKCc2MDBtcyBjdWJpYy1iZXppZXIoMC4wLCAwLjAsIDAuMiwgMSknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLTEwMCUpJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5ICA6IDBcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdHlsZSh7dHJhbnNmb3JtOiAndHJhbnNsYXRlWCgxMDAlKSd9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZSgnNjAwbXMgY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDAlKScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgICAgICAgICBdLCB7b3B0aW9uYWw6IHRydWV9KVxyXG4gICAgICAgICAgICAgICAgXSksXHJcbiAgICAgICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDpsZWF2ZScsIGFuaW1hdGVDaGlsZCgpLCB7b3B0aW9uYWw6IHRydWV9KSxcclxuICAgICAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgYW5pbWF0ZUNoaWxkKCksIHtvcHRpb25hbDogdHJ1ZX0pXHJcbiAgICAgICAgICAgIF0pXHJcbiAgICAgICAgXSlcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ3JvdXRlclRyYW5zaXRpb25SaWdodCcsIFtcclxuXHJcbiAgICAgICAgdHJhbnNpdGlvbignKiA9PiAqJywgW1xyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDplbnRlciwgY29udGVudCA+IDpsZWF2ZScsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcclxuICAgICAgICAgICAgICAgICAgICB0b3AgICAgIDogMCxcclxuICAgICAgICAgICAgICAgICAgICBib3R0b20gIDogMCxcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0ICAgIDogMCxcclxuICAgICAgICAgICAgICAgICAgICByaWdodCAgIDogMFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoLTEwMCUpJyxcclxuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5ICA6IDBcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICBzZXF1ZW5jZShbXHJcbiAgICAgICAgICAgICAgICBncm91cChbXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6bGVhdmUnLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMCknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbmltYXRlKCc2MDBtcyBjdWJpYy1iZXppZXIoMC4wLCAwLjAsIDAuMiwgMSknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVgoMTAwJSknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgIDogMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6ZW50ZXInLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKC0xMDAlKSd9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZSgnNjAwbXMgY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVYKDAlKScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgICAgICAgICBdLCB7b3B0aW9uYWw6IHRydWV9KVxyXG4gICAgICAgICAgICAgICAgXSksXHJcbiAgICAgICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDpsZWF2ZScsIGFuaW1hdGVDaGlsZCgpLCB7b3B0aW9uYWw6IHRydWV9KSxcclxuICAgICAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgYW5pbWF0ZUNoaWxkKCksIHtvcHRpb25hbDogdHJ1ZX0pXHJcbiAgICAgICAgICAgIF0pXHJcbiAgICAgICAgXSlcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ3JvdXRlclRyYW5zaXRpb25VcCcsIFtcclxuXHJcbiAgICAgICAgdHJhbnNpdGlvbignKiA9PiAqJywgW1xyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDplbnRlciwgY29udGVudCA+IDpsZWF2ZScsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcclxuICAgICAgICAgICAgICAgICAgICB0b3AgICAgIDogMCxcclxuICAgICAgICAgICAgICAgICAgICBib3R0b20gIDogMCxcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0ICAgIDogMCxcclxuICAgICAgICAgICAgICAgICAgICByaWdodCAgIDogMFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMTAwJSknLFxyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgIDogMFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIGdyb3VwKFtcclxuICAgICAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmxlYXZlJywgW1xyXG4gICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgwKScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgIDogMVxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGUoJzYwMG1zIGN1YmljLWJlemllcigwLjAsIDAuMCwgMC4yLCAxKScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoLTEwMCUpJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgIDogMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6ZW50ZXInLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgc3R5bGUoe3RyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMTAwJSknfSksXHJcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZSgnNjAwbXMgY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgwJSknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSlcclxuICAgICAgICAgICAgXSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmxlYXZlJywgYW5pbWF0ZUNoaWxkKCksIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDplbnRlcicsIGFuaW1hdGVDaGlsZCgpLCB7b3B0aW9uYWw6IHRydWV9KVxyXG4gICAgICAgIF0pXHJcbiAgICBdKSxcclxuXHJcbiAgICB0cmlnZ2VyKCdyb3V0ZXJUcmFuc2l0aW9uRG93bicsIFtcclxuXHJcbiAgICAgICAgdHJhbnNpdGlvbignKiA9PiAqJywgW1xyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDplbnRlciwgY29udGVudCA+IDpsZWF2ZScsIFtcclxuICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICBwb3NpdGlvbjogJ2Fic29sdXRlJyxcclxuICAgICAgICAgICAgICAgICAgICB0b3AgICAgIDogMCxcclxuICAgICAgICAgICAgICAgICAgICBib3R0b20gIDogMCxcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0ICAgIDogMCxcclxuICAgICAgICAgICAgICAgICAgICByaWdodCAgIDogMFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoLTEwMCUpJyxcclxuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5ICA6IDBcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICBzZXF1ZW5jZShbXHJcbiAgICAgICAgICAgICAgICBncm91cChbXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6bGVhdmUnLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMCknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhbmltYXRlKCc2MDBtcyBjdWJpYy1iZXppZXIoMC4wLCAwLjAsIDAuMiwgMSknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoMTAwJSknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHkgIDogMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6ZW50ZXInLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHt0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKC0xMDAlKSd9KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZSgnNjAwbXMgY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0eWxlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGVZKDAlKScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eSAgOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgICAgICAgICBdLCB7b3B0aW9uYWw6IHRydWV9KVxyXG4gICAgICAgICAgICAgICAgXSksXHJcbiAgICAgICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDpsZWF2ZScsIGFuaW1hdGVDaGlsZCgpLCB7b3B0aW9uYWw6IHRydWV9KSxcclxuICAgICAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgYW5pbWF0ZUNoaWxkKCksIHtvcHRpb25hbDogdHJ1ZX0pXHJcbiAgICAgICAgICAgIF0pXHJcbiAgICAgICAgXSlcclxuICAgIF0pLFxyXG5cclxuICAgIHRyaWdnZXIoJ3JvdXRlclRyYW5zaXRpb25GYWRlJywgW1xyXG5cclxuICAgICAgICB0cmFuc2l0aW9uKCcqID0+IConLCBncm91cChbXHJcblxyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDplbnRlciwgY29udGVudCA+IDpsZWF2ZSAnLCBbXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246ICdhYnNvbHV0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgdG9wICAgICA6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgYm90dG9tICA6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdCAgICA6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQgICA6IDBcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIF0sIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG5cclxuICAgICAgICAgICAgcXVlcnkoJ2NvbnRlbnQgPiA6ZW50ZXInLCBbXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMFxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmxlYXZlJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDFcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgnMzAwbXMgY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJyxcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDBcclxuICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe1xyXG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDBcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgnMzAwbXMgY3ViaWMtYmV6aWVyKDAuMCwgMC4wLCAwLjIsIDEpJyxcclxuICAgICAgICAgICAgICAgICAgICBzdHlsZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDFcclxuICAgICAgICAgICAgICAgICAgICB9KSlcclxuICAgICAgICAgICAgXSwge29wdGlvbmFsOiB0cnVlfSksXHJcbiAgICAgICAgICAgIHF1ZXJ5KCdjb250ZW50ID4gOmVudGVyJywgYW5pbWF0ZUNoaWxkKCksIHtvcHRpb25hbDogdHJ1ZX0pLFxyXG4gICAgICAgICAgICBxdWVyeSgnY29udGVudCA+IDpsZWF2ZScsIGFuaW1hdGVDaGlsZCgpLCB7b3B0aW9uYWw6IHRydWV9KVxyXG4gICAgICAgIF0pKVxyXG4gICAgXSlcclxuXTtcclxuIl19