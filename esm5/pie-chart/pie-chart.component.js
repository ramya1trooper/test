import { Component, ViewEncapsulation, Input, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
var PieChartComponent = /** @class */ (function () {
    function PieChartComponent(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.showLegend = true;
        this.explodeSlices = false;
        this.doughnut = false;
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    PieChartComponent.prototype.ngOnInit = function () {
        this.chartData = this.result;
    };
    PieChartComponent.prototype.onLegendLabelClick = function (event) { };
    PieChartComponent.prototype.select = function (event) { };
    PieChartComponent.decorators = [
        { type: Component, args: [{
                    selector: "pie-chart",
                    template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\" style=\"align-self: center;\" class=\"h-400 p-10  mat-body-2\">\r\n    <div class=\"inner\" *ngIf=\"chartData.length > 0\">\r\n\r\n      <ngx-charts-pie-chart class=\"chart-container\" [view]=\"view\" [scheme]=\"scheme\" [results]=\"chartData\"\r\n        [animations]=\"animations\" [legend]=\"showLegend\" [legendTitle]=\"legendTitle\" [legendPosition]=\"legendPosition\"\r\n        [explodeSlices]=\"explodeSlices\" [doughnut]=\"doughnut\" [arcWidth]=\"arcWidth\"\r\n        (legendLabelClick)=\"onLegendLabelClick($event)\" [gradient]=\"gradient\" [tooltipText]=\"pieTooltipText\"\r\n        (select)=\"select($event)\">\r\n      </ngx-charts-pie-chart>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"chartData.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{label.nodata | translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                    encapsulation: ViewEncapsulation.None,
                    animations: fuseAnimations,
                    styles: [".piechartdiv{display:block!important}.piechartdiv .legend-label-text{text-align:left}.inner{display:block;width:100%;text-align:center;margin-left:0!important}.inner img{width:auto;max-width:100%}.piechartdiv .chart-legend{padding-top:10px}.piechartdiv .legend-label{padding-bottom:7px}.piechartdiv .graphdiv{width:auto!important;padding-top:4px}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}"]
                }] }
    ];
    /** @nocollapse */
    PieChartComponent.ctorParameters = function () { return [
        { type: FuseTranslationLoaderService },
        { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
    ]; };
    PieChartComponent.propDecorators = {
        result: [{ type: Input }],
        scheme: [{ type: Input }],
        view: [{ type: Input }],
        label: [{ type: Input }]
    };
    return PieChartComponent;
}());
export { PieChartComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGllLWNoYXJ0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJwaWUtY2hhcnQvcGllLWNoYXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULGlCQUFpQixFQUNqQixLQUFLLEVBQ0wsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVyRCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFFbEQ7SUF3QkUsMkJBQ1UsNkJBQTJELEVBQ3hDLE9BQU87UUFEMUIsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUE4QjtRQUN4QyxZQUFPLEdBQVAsT0FBTyxDQUFBO1FBbEJwQyxlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFrQmYsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQy9ELENBQUM7SUFFRCxvQ0FBUSxHQUFSO1FBQ0UsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO0lBQy9CLENBQUM7SUFDRCw4Q0FBa0IsR0FBbEIsVUFBbUIsS0FBSyxJQUFHLENBQUM7SUFDNUIsa0NBQU0sR0FBTixVQUFPLEtBQUssSUFBRyxDQUFDOztnQkFuQ2pCLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsV0FBVztvQkFDckIseTFDQUF5QztvQkFFekMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLFVBQVUsRUFBRSxjQUFjOztpQkFDM0I7Ozs7Z0JBVFEsNEJBQTRCO2dEQTZCaEMsTUFBTSxTQUFDLFNBQVM7Ozt5QkFmbEIsS0FBSzt5QkFDTCxLQUFLO3VCQUNMLEtBQUs7d0JBQ0wsS0FBSzs7SUFzQlIsd0JBQUM7Q0FBQSxBQXBDRCxJQW9DQztTQTdCWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgSW5wdXQsXHJcbiAgSW5qZWN0XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgZnVzZUFuaW1hdGlvbnMgfSBmcm9tIFwiLi4vQGZ1c2UvYW5pbWF0aW9uc1wiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJwaWUtY2hhcnRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL3BpZS1jaGFydC5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9waWUtY2hhcnQuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBhbmltYXRpb25zOiBmdXNlQW5pbWF0aW9uc1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUGllQ2hhcnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIHNob3dMZWdlbmQgPSB0cnVlO1xyXG4gIGV4cGxvZGVTbGljZXMgPSBmYWxzZTtcclxuICBkb3VnaG51dCA9IGZhbHNlO1xyXG4gIEBJbnB1dCgpIHJlc3VsdDogYW55O1xyXG4gIEBJbnB1dCgpIHNjaGVtZTogYW55O1xyXG4gIEBJbnB1dCgpIHZpZXc6IGFueTtcclxuICBASW5wdXQoKSBsYWJlbDogYW55O1xyXG5cclxuICBjaGFydERhdGE6IGFueTtcclxuICB0cmFuc2xhdGU6IGFueTtcclxuICBhbmltYXRpb25zO1xyXG4gIGxlZ2VuZFRpdGxlO1xyXG4gIGFyY1dpZHRoO1xyXG4gIGdyYWRpZW50O1xyXG4gIGxlZ2VuZFBvc2l0aW9uO1xyXG4gIHBpZVRvb2x0aXBUZXh0O1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZTogRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSxcclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHByaXZhdGUgZW5nbGlzaFxyXG4gICkge1xyXG4gICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLmNoYXJ0RGF0YSA9IHRoaXMucmVzdWx0O1xyXG4gIH1cclxuICBvbkxlZ2VuZExhYmVsQ2xpY2soZXZlbnQpIHt9XHJcbiAgc2VsZWN0KGV2ZW50KSB7fVxyXG59XHJcbiJdfQ==