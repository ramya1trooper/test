import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PieChartComponent } from "./pie-chart.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { ChartsModule } from "ng2-charts";
import { FuseProgressBarModule } from '../@fuse/components/progress-bar/progress-bar.module';
import { FuseThemeOptionsModule } from '../@fuse/components/theme-options/theme-options.module';
import { FuseWidgetModule } from "../@fuse/components/widget/widget.module";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
var PieChartlayoutModule = /** @class */ (function () {
    function PieChartlayoutModule() {
    }
    PieChartlayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [PieChartComponent],
                    imports: [
                        RouterModule,
                        NgxChartsModule,
                        ChartsModule,
                        FuseWidgetModule,
                        FuseSharedModule,
                        TranslateModule,
                        FuseProgressBarModule,
                        FuseThemeOptionsModule
                    ],
                    exports: [PieChartComponent]
                },] }
    ];
    return PieChartlayoutModule;
}());
export { PieChartlayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGllLWNoYXJ0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJwaWUtY2hhcnQvcGllLWNoYXJ0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDdkQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFlBQVksQ0FBQztBQUMxQyxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxzREFBc0QsQ0FBQztBQUMzRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSx3REFBd0QsQ0FBQTtBQUM3RixPQUFPLEVBQ0wsZ0JBQWdCLEVBQ2pCLE1BQU0sMENBQTBDLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRXREO0lBQUE7SUFjbUMsQ0FBQzs7Z0JBZG5DLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztvQkFDakMsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osZUFBZTt3QkFDZixZQUFZO3dCQUNaLGdCQUFnQjt3QkFDaEIsZ0JBQWdCO3dCQUNoQixlQUFlO3dCQUNmLHFCQUFxQjt3QkFDckIsc0JBQXNCO3FCQUN2QjtvQkFDRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztpQkFDN0I7O0lBQ2tDLDJCQUFDO0NBQUEsQUFkcEMsSUFjb0M7U0FBdkIsb0JBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBQaWVDaGFydENvbXBvbmVudCB9IGZyb20gXCIuL3BpZS1jaGFydC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgTmd4Q2hhcnRzTW9kdWxlIH0gZnJvbSBcIkBzd2ltbGFuZS9uZ3gtY2hhcnRzXCI7XHJcbmltcG9ydCB7IENoYXJ0c01vZHVsZSB9IGZyb20gXCJuZzItY2hhcnRzXCI7XHJcbmltcG9ydCB7RnVzZVByb2dyZXNzQmFyTW9kdWxlfSBmcm9tICcuLi9AZnVzZS9jb21wb25lbnRzL3Byb2dyZXNzLWJhci9wcm9ncmVzcy1iYXIubW9kdWxlJztcclxuaW1wb3J0IHtGdXNlVGhlbWVPcHRpb25zTW9kdWxlfSBmcm9tICcuLi9AZnVzZS9jb21wb25lbnRzL3RoZW1lLW9wdGlvbnMvdGhlbWUtb3B0aW9ucy5tb2R1bGUnXHJcbmltcG9ydCB7XHJcbiAgRnVzZVdpZGdldE1vZHVsZVxyXG59IGZyb20gXCIuLi9AZnVzZS9jb21wb25lbnRzL3dpZGdldC93aWRnZXQubW9kdWxlXCI7XHJcbmltcG9ydCB7IEZ1c2VTaGFyZWRNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2Uvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtQaWVDaGFydENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgUm91dGVyTW9kdWxlLFxyXG4gICAgTmd4Q2hhcnRzTW9kdWxlLFxyXG4gICAgQ2hhcnRzTW9kdWxlLFxyXG4gICAgRnVzZVdpZGdldE1vZHVsZSxcclxuICAgIEZ1c2VTaGFyZWRNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUsXHJcbiAgICBGdXNlUHJvZ3Jlc3NCYXJNb2R1bGUsXHJcbiAgICBGdXNlVGhlbWVPcHRpb25zTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbUGllQ2hhcnRDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQaWVDaGFydGxheW91dE1vZHVsZSB7fVxyXG4iXX0=