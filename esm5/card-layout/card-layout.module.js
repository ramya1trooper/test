import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CardLayoutComponent } from "./card-layout.component";
import { FuseSharedModule } from "../@fuse/shared.module";
import { FuseProgressBarModule } from '../@fuse/components/progress-bar/progress-bar.module';
import { FuseThemeOptionsModule } from '../@fuse/components/theme-options/theme-options.module';
import { FuseWidgetModule } from "../@fuse/components/widget/widget.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
var CardLayoutModule = /** @class */ (function () {
    function CardLayoutModule() {
    }
    CardLayoutModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [CardLayoutComponent],
                    imports: [
                        RouterModule,
                        MaterialModule,
                        FuseWidgetModule,
                        FuseSharedModule,
                        TranslateModule,
                        FuseProgressBarModule,
                        FuseThemeOptionsModule
                    ],
                    exports: [CardLayoutComponent],
                    providers: []
                },] }
    ];
    return CardLayoutModule;
}());
export { CardLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1sYXlvdXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNhcmQtbGF5b3V0L2NhcmQtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBSyxzREFBc0QsQ0FBQztBQUMxRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSx3REFBd0QsQ0FBQTtBQUM3RixPQUFPLEVBQUcsZ0JBQWdCLEVBQUMsTUFBTSwwQ0FBMEMsQ0FBQztBQUM1RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BEO0lBQUE7SUFnQkEsQ0FBQzs7Z0JBaEJBLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQztvQkFDbkMsT0FBTyxFQUFFO3dCQUNQLFlBQVk7d0JBQ1osY0FBYzt3QkFDZCxnQkFBZ0I7d0JBQ2hCLGdCQUFnQjt3QkFDaEIsZUFBZTt3QkFDZixxQkFBcUI7d0JBQ3JCLHNCQUFzQjtxQkFDdkI7b0JBQ0QsT0FBTyxFQUFFLENBQUMsbUJBQW1CLENBQUM7b0JBQzlCLFNBQVMsRUFBRyxFQUFFO2lCQUNmOztJQUdELHVCQUFDO0NBQUEsQUFoQkQsSUFnQkM7U0FGWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IENhcmRMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi9jYXJkLWxheW91dC5jb21wb25lbnRcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VTaGFyZWRNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2Uvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQge0Z1c2VQcm9ncmVzc0Jhck1vZHVsZX1mcm9tICcuLi9AZnVzZS9jb21wb25lbnRzL3Byb2dyZXNzLWJhci9wcm9ncmVzcy1iYXIubW9kdWxlJztcclxuaW1wb3J0IHtGdXNlVGhlbWVPcHRpb25zTW9kdWxlfSBmcm9tICcuLi9AZnVzZS9jb21wb25lbnRzL3RoZW1lLW9wdGlvbnMvdGhlbWUtb3B0aW9ucy5tb2R1bGUnXHJcbmltcG9ydCB7ICBGdXNlV2lkZ2V0TW9kdWxlfSBmcm9tIFwiLi4vQGZ1c2UvY29tcG9uZW50cy93aWRnZXQvd2lkZ2V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gXCIuLi9tYXRlcmlhbC5tb2R1bGVcIjtcclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtDYXJkTGF5b3V0Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIEZ1c2VXaWRnZXRNb2R1bGUsXHJcbiAgICBGdXNlU2hhcmVkTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG4gICAgRnVzZVByb2dyZXNzQmFyTW9kdWxlLFxyXG4gICAgRnVzZVRoZW1lT3B0aW9uc01vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW0NhcmRMYXlvdXRDb21wb25lbnRdLFxyXG4gIHByb3ZpZGVycyA6IFtdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkTGF5b3V0TW9kdWxlIHtcclxuXHJcbn1cclxuIl19