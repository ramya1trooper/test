import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
var SnackBarMessage = /** @class */ (function () {
    function SnackBarMessage() {
        this.action = null;
        this.config = null;
    }
    return SnackBarMessage;
}());
export { SnackBarMessage };
var SnackBarService = /** @class */ (function () {
    function SnackBarService(snackBar) {
        var _this = this;
        this.snackBar = snackBar;
        this.messageQueue = new Subject();
        this.subscription = this.messageQueue.subscribe(function (message) {
            _this.snackBarRef = _this.snackBar.open(message.message, message.action, message.config);
        });
    }
    SnackBarService.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    /**
     * Add a message
     * @param message The message to show in the snackbar.
     * @param action The label for the snackbar action.
     * @param config Additional configuration options for the snackbar.
     */
    SnackBarService.prototype.add = function (message, action, config) {
        if (!config) {
            config = new MatSnackBarConfig();
            config.duration = 3000;
            config.verticalPosition = 'top';
            config.horizontalPosition = 'right';
            config.panelClass = ['green-snackbar'];
        }
        var sbMessage = new SnackBarMessage();
        sbMessage.message = message;
        sbMessage.action = 'x';
        sbMessage.config = config;
        this.messageQueue.next(sbMessage);
    };
    SnackBarService.prototype.warning = function (message, action, config) {
        if (!config) {
            config = new MatSnackBarConfig();
            config.duration = 3000;
            config.verticalPosition = 'top';
            config.horizontalPosition = 'right';
            config.panelClass = ['red-snackbar'];
        }
        var sbMessage = new SnackBarMessage();
        sbMessage.message = message;
        sbMessage.action = 'x';
        sbMessage.config = config;
        this.messageQueue.next(sbMessage);
    };
    SnackBarService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    SnackBarService.ctorParameters = function () { return [
        { type: MatSnackBar }
    ]; };
    return SnackBarService;
}());
export { SnackBarService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic25hY2tiYXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzaGFyZWQvc25hY2tiYXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBVSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBRzdFO0lBQUE7UUFFQyxXQUFNLEdBQVcsSUFBSSxDQUFDO1FBQ3RCLFdBQU0sR0FBc0IsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUMsQUFKRCxJQUlDOztBQUVEO0lBT0MseUJBQW1CLFFBQXFCO1FBQXhDLGlCQUlDO1FBSmtCLGFBQVEsR0FBUixRQUFRLENBQWE7UUFMaEMsaUJBQVksR0FBNkIsSUFBSSxPQUFPLEVBQW1CLENBQUM7UUFNL0UsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE9BQU87WUFDdEQsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELHFDQUFXLEdBQVg7UUFDQyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRTs7Ozs7T0FLRztJQUNOLDZCQUFHLEdBQUgsVUFBSSxPQUFlLEVBQUUsTUFBZSxFQUFFLE1BQTBCO1FBRS9ELElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDWixNQUFNLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7WUFDaEMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUNwQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUN2QztRQUVELElBQUksU0FBUyxHQUFHLElBQUksZUFBZSxFQUFFLENBQUM7UUFDdEMsU0FBUyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDNUIsU0FBUyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDdkIsU0FBUyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUNELGlDQUFPLEdBQVAsVUFBUSxPQUFlLEVBQUUsTUFBZSxFQUFFLE1BQTBCO1FBQ25FLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDWixNQUFNLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7WUFDaEMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUNwQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDckM7UUFFRCxJQUFJLFNBQVMsR0FBRyxJQUFJLGVBQWUsRUFBRSxDQUFDO1FBQ3RDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQzVCLFNBQVMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ3ZCLFNBQVMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRTFCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7O2dCQXZERCxVQUFVOzs7O2dCQVRGLFdBQVc7O0lBaUVwQixzQkFBQztDQUFBLEFBeERELElBd0RDO1NBdkRZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzL1N1YnNjcmlwdGlvbic7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzL1N1YmplY3QnO1xyXG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0U25hY2tCYXJDb25maWcgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXInO1xyXG5pbXBvcnQgeyBNYXRTbmFja0JhclJlZiwgU2ltcGxlU25hY2tCYXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9zbmFjay1iYXInO1xyXG5cclxuZXhwb3J0IGNsYXNzIFNuYWNrQmFyTWVzc2FnZSB7XHJcblx0bWVzc2FnZTogc3RyaW5nO1xyXG5cdGFjdGlvbjogc3RyaW5nID0gbnVsbDtcclxuXHRjb25maWc6IE1hdFNuYWNrQmFyQ29uZmlnID0gbnVsbDtcclxufVxyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgU25hY2tCYXJTZXJ2aWNlIGltcGxlbWVudHMgT25EZXN0cm95IHtcclxuXHRwcml2YXRlIG1lc3NhZ2VRdWV1ZTogU3ViamVjdDxTbmFja0Jhck1lc3NhZ2U+ID0gbmV3IFN1YmplY3Q8U25hY2tCYXJNZXNzYWdlPigpO1xyXG5cdHByaXZhdGUgc3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcblx0cHJpdmF0ZSBzbmFja0JhclJlZjogTWF0U25hY2tCYXJSZWY8U2ltcGxlU25hY2tCYXI+O1xyXG5cclxuXHJcblx0Y29uc3RydWN0b3IocHVibGljIHNuYWNrQmFyOiBNYXRTbmFja0Jhcikge1xyXG5cdFx0dGhpcy5zdWJzY3JpcHRpb24gPSB0aGlzLm1lc3NhZ2VRdWV1ZS5zdWJzY3JpYmUobWVzc2FnZSA9PiB7XHJcblx0XHRcdHRoaXMuc25hY2tCYXJSZWYgPSB0aGlzLnNuYWNrQmFyLm9wZW4obWVzc2FnZS5tZXNzYWdlLCBtZXNzYWdlLmFjdGlvbiwgbWVzc2FnZS5jb25maWcpO1xyXG5cdFx0fSk7XHJcblx0fVxyXG5cclxuXHRuZ09uRGVzdHJveSgpIHtcclxuXHRcdHRoaXMuc3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcblx0fVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGEgbWVzc2FnZVxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgVGhlIG1lc3NhZ2UgdG8gc2hvdyBpbiB0aGUgc25hY2tiYXIuXHJcbiAgICAgKiBAcGFyYW0gYWN0aW9uIFRoZSBsYWJlbCBmb3IgdGhlIHNuYWNrYmFyIGFjdGlvbi5cclxuICAgICAqIEBwYXJhbSBjb25maWcgQWRkaXRpb25hbCBjb25maWd1cmF0aW9uIG9wdGlvbnMgZm9yIHRoZSBzbmFja2Jhci5cclxuICAgICAqL1xyXG5cdGFkZChtZXNzYWdlOiBzdHJpbmcsIGFjdGlvbj86IHN0cmluZywgY29uZmlnPzogTWF0U25hY2tCYXJDb25maWcpOiB2b2lkIHtcclxuXHJcblx0XHRpZiAoIWNvbmZpZykge1xyXG5cdFx0XHRjb25maWcgPSBuZXcgTWF0U25hY2tCYXJDb25maWcoKTtcclxuXHRcdFx0Y29uZmlnLmR1cmF0aW9uID0gMzAwMDtcclxuXHRcdFx0Y29uZmlnLnZlcnRpY2FsUG9zaXRpb24gPSAndG9wJztcclxuXHRcdFx0Y29uZmlnLmhvcml6b250YWxQb3NpdGlvbiA9ICdyaWdodCc7XHJcblx0XHRcdGNvbmZpZy5wYW5lbENsYXNzID0gWydncmVlbi1zbmFja2JhciddO1xyXG5cdFx0fVxyXG5cclxuXHRcdGxldCBzYk1lc3NhZ2UgPSBuZXcgU25hY2tCYXJNZXNzYWdlKCk7XHJcblx0XHRzYk1lc3NhZ2UubWVzc2FnZSA9IG1lc3NhZ2U7XHJcblx0XHRzYk1lc3NhZ2UuYWN0aW9uID0gJ3gnO1xyXG5cdFx0c2JNZXNzYWdlLmNvbmZpZyA9IGNvbmZpZztcclxuXHJcblx0XHR0aGlzLm1lc3NhZ2VRdWV1ZS5uZXh0KHNiTWVzc2FnZSk7XHJcblx0fVxyXG5cdHdhcm5pbmcobWVzc2FnZTogc3RyaW5nLCBhY3Rpb24/OiBzdHJpbmcsIGNvbmZpZz86IE1hdFNuYWNrQmFyQ29uZmlnKTogdm9pZCB7XHJcblx0XHRpZiAoIWNvbmZpZykge1xyXG5cdFx0XHRjb25maWcgPSBuZXcgTWF0U25hY2tCYXJDb25maWcoKTtcclxuXHRcdFx0Y29uZmlnLmR1cmF0aW9uID0gMzAwMDtcclxuXHRcdFx0Y29uZmlnLnZlcnRpY2FsUG9zaXRpb24gPSAndG9wJztcclxuXHRcdFx0Y29uZmlnLmhvcml6b250YWxQb3NpdGlvbiA9ICdyaWdodCc7XHJcblx0XHRcdGNvbmZpZy5wYW5lbENsYXNzID0gWydyZWQtc25hY2tiYXInXTtcclxuXHRcdH1cclxuXHJcblx0XHRsZXQgc2JNZXNzYWdlID0gbmV3IFNuYWNrQmFyTWVzc2FnZSgpO1xyXG5cdFx0c2JNZXNzYWdlLm1lc3NhZ2UgPSBtZXNzYWdlO1xyXG5cdFx0c2JNZXNzYWdlLmFjdGlvbiA9ICd4JztcclxuXHRcdHNiTWVzc2FnZS5jb25maWcgPSBjb25maWc7XHJcblxyXG5cdFx0dGhpcy5tZXNzYWdlUXVldWUubmV4dChzYk1lc3NhZ2UpO1xyXG5cdH1cclxufVxyXG4iXX0=