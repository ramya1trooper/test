import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpErrorResponse } from "@angular/common/http";
import { OAuthService } from "angular-oauth2-oidc";
import { LoaderService } from '../../loader.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "angular-oauth2-oidc";
import * as i3 from "../../loader.service";
var AuthExpiredInterceptor = /** @class */ (function () {
    function AuthExpiredInterceptor(router, oauthService, Loaderservice) {
        this.router = router;
        this.oauthService = oauthService;
        this.Loaderservice = Loaderservice;
    }
    AuthExpiredInterceptor.prototype.intercept = function (request, next) {
        var _this = this;
        return next.handle(request).do(function (event) { }, function (err) {
            if (err instanceof HttpErrorResponse) {
                _this.Loaderservice.stopLoader();
                if (err.status === 401) {
                    localStorage.clear();
                    _this.oauthService.initImplicitFlow();
                    return;
                }
                else if (err.status == 403) {
                    localStorage.clear();
                    _this.oauthService.logOut(false);
                    return;
                }
                else {
                    console.log("HttpErrorResponse", err);
                    return;
                }
            }
        });
    };
    AuthExpiredInterceptor.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    AuthExpiredInterceptor.ctorParameters = function () { return [
        { type: Router },
        { type: OAuthService },
        { type: LoaderService }
    ]; };
    AuthExpiredInterceptor.ngInjectableDef = i0.defineInjectable({ factory: function AuthExpiredInterceptor_Factory() { return new AuthExpiredInterceptor(i0.inject(i1.Router), i0.inject(i2.OAuthService), i0.inject(i3.LoaderService)); }, token: AuthExpiredInterceptor, providedIn: "root" });
    return AuthExpiredInterceptor;
}());
export { AuthExpiredInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1leHBpcmVkLmludGVyY2VwdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInNoYXJlZC9hdXRoL2F1dGgtZXhwaXJlZC5pbnRlcmNlcHRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBS0wsaUJBQWlCLEVBQ2xCLE1BQU0sc0JBQXNCLENBQUM7QUFFOUIsT0FBTyxFQUNMLFlBQVksRUFHYixNQUFNLHFCQUFxQixDQUFDO0FBQzdCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7Ozs7QUFFckQ7SUFJRSxnQ0FBb0IsTUFBYyxFQUFVLFlBQTBCLEVBQVMsYUFBNEI7UUFBdkYsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVMsa0JBQWEsR0FBYixhQUFhLENBQWU7SUFBRyxDQUFDO0lBRS9HLDBDQUFTLEdBQVQsVUFDRSxPQUF5QixFQUN6QixJQUFpQjtRQUZuQixpQkF3QkM7UUFwQkMsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FDNUIsVUFBQyxLQUFxQixJQUFNLENBQUMsRUFDN0IsVUFBQyxHQUFRO1lBQ1AsSUFBSSxHQUFHLFlBQVksaUJBQWlCLEVBQUU7Z0JBQ3BDLEtBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ2hDLElBQUksR0FBRyxDQUFDLE1BQU0sS0FBSyxHQUFHLEVBQUU7b0JBQ3RCLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUNyQyxPQUFPO2lCQUNSO3FCQUFNLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7b0JBQzVCLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDckIsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ2hDLE9BQU87aUJBQ1I7cUJBQU07b0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxHQUFHLENBQUMsQ0FBQztvQkFDdEMsT0FBTztpQkFDUjthQUNGO1FBQ0gsQ0FBQyxDQUNGLENBQUM7SUFDSixDQUFDOztnQkE5QkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFsQlEsTUFBTTtnQkFVYixZQUFZO2dCQUlMLGFBQWE7OztpQ0FoQnRCO0NBaURDLEFBL0JELElBK0JDO1NBNUJZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anMvT2JzZXJ2YWJsZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7XHJcbiAgSHR0cEludGVyY2VwdG9yLFxyXG4gIEh0dHBSZXF1ZXN0LFxyXG4gIEh0dHBIYW5kbGVyLFxyXG4gIEh0dHBFdmVudCxcclxuICBIdHRwRXJyb3JSZXNwb25zZVxyXG59IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5cclxuaW1wb3J0IHtcclxuICBPQXV0aFNlcnZpY2UsXHJcbiAgQXV0aENvbmZpZyxcclxuICBOdWxsVmFsaWRhdGlvbkhhbmRsZXJcclxufSBmcm9tIFwiYW5ndWxhci1vYXV0aDItb2lkY1wiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vbG9hZGVyLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46IFwicm9vdFwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBdXRoRXhwaXJlZEludGVyY2VwdG9yIGltcGxlbWVudHMgSHR0cEludGVyY2VwdG9yIHtcclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIG9hdXRoU2VydmljZTogT0F1dGhTZXJ2aWNlLHByaXZhdGUgTG9hZGVyc2VydmljZTogTG9hZGVyU2VydmljZSkge31cclxuXHJcbiAgaW50ZXJjZXB0KFxyXG4gICAgcmVxdWVzdDogSHR0cFJlcXVlc3Q8YW55PixcclxuICAgIG5leHQ6IEh0dHBIYW5kbGVyXHJcbiAgKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgcmV0dXJuIG5leHQuaGFuZGxlKHJlcXVlc3QpLmRvKFxyXG4gICAgICAoZXZlbnQ6IEh0dHBFdmVudDxhbnk+KSA9PiB7fSxcclxuICAgICAgKGVycjogYW55KSA9PiB7XHJcbiAgICAgICAgaWYgKGVyciBpbnN0YW5jZW9mIEh0dHBFcnJvclJlc3BvbnNlKSB7XHJcbiAgICAgICAgICB0aGlzLkxvYWRlcnNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgaWYgKGVyci5zdGF0dXMgPT09IDQwMSkge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcclxuICAgICAgICAgICAgdGhpcy5vYXV0aFNlcnZpY2UuaW5pdEltcGxpY2l0RmxvdygpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGVyci5zdGF0dXMgPT0gNDAzKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgICAgICAgICB0aGlzLm9hdXRoU2VydmljZS5sb2dPdXQoZmFsc2UpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkh0dHBFcnJvclJlc3BvbnNlXCIsIGVycik7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiJdfQ==