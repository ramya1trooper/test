import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Principal } from "./principal.service";
var UserRouteAccessService = /** @class */ (function () {
    function UserRouteAccessService(router, principal) {
        this.router = router;
        this.principal = principal;
        console.info("<<<<<<<UserRouteAccessService");
    }
    UserRouteAccessService.prototype.canActivate = function (route, state) {
        var authorities = route.data["authorities"];
        return this.checkLogin(authorities, state.url);
    };
    UserRouteAccessService.prototype.checkLogin = function (authorities, url) {
        var _this = this;
        var principal = this.principal;
        return Promise.resolve(principal.identity().then(function (account) {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account) {
                return principal.hasAnyAuthority(authorities).then(function (response) {
                    if (response) {
                        return true;
                    }
                    return false;
                });
            }
            _this.router.navigate(["login"]).then(function () {
                if (!account) {
                }
            });
            return false;
        }));
    };
    UserRouteAccessService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    UserRouteAccessService.ctorParameters = function () { return [
        { type: Router },
        { type: Principal }
    ]; };
    return UserRouteAccessService;
}());
export { UserRouteAccessService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1yb3V0ZS1hY2Nlc3Mtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzaGFyZWQvYXV0aC91c2VyLXJvdXRlLWFjY2Vzcy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUdMLE1BQU0sRUFFUCxNQUFNLGlCQUFpQixDQUFDO0FBRXpCLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVoRDtJQUVFLGdDQUFvQixNQUFjLEVBQVUsU0FBb0I7UUFBNUMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUFVLGNBQVMsR0FBVCxTQUFTLENBQVc7UUFDOUQsT0FBTyxDQUFDLElBQUksQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRCw0Q0FBVyxHQUFYLFVBQ0UsS0FBNkIsRUFDN0IsS0FBMEI7UUFFMUIsSUFBTSxXQUFXLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUM5QyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBRUQsMkNBQVUsR0FBVixVQUFXLFdBQXFCLEVBQUUsR0FBVztRQUE3QyxpQkF3QkM7UUF2QkMsSUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNqQyxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQ3BCLFNBQVMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO1lBQy9CLElBQUksQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQzVDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFFRCxJQUFJLE9BQU8sRUFBRTtnQkFDWCxPQUFPLFNBQVMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQUEsUUFBUTtvQkFDekQsSUFBSSxRQUFRLEVBQUU7d0JBQ1osT0FBTyxJQUFJLENBQUM7cUJBQ2I7b0JBQ0QsT0FBTyxLQUFLLENBQUM7Z0JBQ2YsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUVELEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ25DLElBQUksQ0FBQyxPQUFPLEVBQUU7aUJBQ2I7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxDQUFDLENBQ0gsQ0FBQztJQUNKLENBQUM7O2dCQXRDRixVQUFVOzs7O2dCQU5ULE1BQU07Z0JBSUMsU0FBUzs7SUF5Q2xCLDZCQUFDO0NBQUEsQUF2Q0QsSUF1Q0M7U0F0Q1ksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7XHJcbiAgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICBDYW5BY3RpdmF0ZSxcclxuICBSb3V0ZXIsXHJcbiAgUm91dGVyU3RhdGVTbmFwc2hvdFxyXG59IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IFByaW5jaXBhbCB9IGZyb20gXCIuL3ByaW5jaXBhbC5zZXJ2aWNlXCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBVc2VyUm91dGVBY2Nlc3NTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xyXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsIHByaXZhdGUgcHJpbmNpcGFsOiBQcmluY2lwYWwpIHtcclxuICAgIGNvbnNvbGUuaW5mbyhcIjw8PDw8PDxVc2VyUm91dGVBY2Nlc3NTZXJ2aWNlXCIpO1xyXG4gIH1cclxuXHJcbiAgY2FuQWN0aXZhdGUoXHJcbiAgICByb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbiAgKTogYm9vbGVhbiB8IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgY29uc3QgYXV0aG9yaXRpZXMgPSByb3V0ZS5kYXRhW1wiYXV0aG9yaXRpZXNcIl07XHJcbiAgICByZXR1cm4gdGhpcy5jaGVja0xvZ2luKGF1dGhvcml0aWVzLCBzdGF0ZS51cmwpO1xyXG4gIH1cclxuXHJcbiAgY2hlY2tMb2dpbihhdXRob3JpdGllczogc3RyaW5nW10sIHVybDogc3RyaW5nKTogUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICBjb25zdCBwcmluY2lwYWwgPSB0aGlzLnByaW5jaXBhbDtcclxuICAgIHJldHVybiBQcm9taXNlLnJlc29sdmUoXHJcbiAgICAgIHByaW5jaXBhbC5pZGVudGl0eSgpLnRoZW4oYWNjb3VudCA9PiB7XHJcbiAgICAgICAgaWYgKCFhdXRob3JpdGllcyB8fCBhdXRob3JpdGllcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFjY291bnQpIHtcclxuICAgICAgICAgIHJldHVybiBwcmluY2lwYWwuaGFzQW55QXV0aG9yaXR5KGF1dGhvcml0aWVzKS50aGVuKHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgaWYgKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbXCJsb2dpblwiXSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICBpZiAoIWFjY291bnQpIHtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgIH0pXHJcbiAgICApO1xyXG4gIH1cclxufVxyXG4iXX0=