import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs/Rx";
// import { environment } from "./../../environments/environment";
var AccountService = /** @class */ (function () {
    function AccountService(http, environment) {
        this.http = http;
        this.environment = environment;
        this.switchEvent = new Subject();
        this.baseURL = this.environment.baseUrl;
        this.idMgmtURL = this.environment.idMgmtBaseURL;
        this.issuesURL = this.environment.issuer;
        console.log(this.environment, "<<<<<<<");
    }
    AccountService.prototype.get = function () {
        console.info("*********");
        return this.http.get(this.baseURL + "/users/me");
    };
    // changePassword(): Observable<any> {
    // 	//return this.http.get(`${this.baseURL}/users/me`);
    // 	window.location.href = `${this.issuesURL}/change-password`;
    // 	return
    // }
    AccountService.prototype.changePassword = function (access_token) {
        window.location.href =
            this.issuesURL + "/change-password?access_token=" + access_token;
    };
    AccountService.prototype.emitSwitchEvent = function (event) {
        this.switchEvent.next(event);
    };
    AccountService.prototype.changeRealm = function (body) {
        console.log("body", body);
        return this.http.post(this.idMgmtURL + "/users/change-realm", body, {
            observe: "response"
        });
    };
    AccountService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AccountService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
    ]; };
    return AccountService;
}());
export { AccountService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInNoYXJlZC9hdXRoL2FjY291bnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFnQixNQUFNLHNCQUFzQixDQUFDO0FBRWhFLE9BQU8sRUFBYyxPQUFPLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUMsa0VBQWtFO0FBRWxFO0lBS0Usd0JBQ1UsSUFBZ0IsRUFDTyxXQUFXO1FBRGxDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDTyxnQkFBVyxHQUFYLFdBQVcsQ0FBQTtRQVE1QyxnQkFBVyxHQUFpQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBTnhDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBR0QsNEJBQUcsR0FBSDtRQUNFLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBSSxJQUFJLENBQUMsT0FBTyxjQUFXLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQsc0NBQXNDO0lBQ3RDLHVEQUF1RDtJQUN2RCwrREFBK0Q7SUFDL0QsVUFBVTtJQUNWLElBQUk7SUFFSix1Q0FBYyxHQUFkLFVBQWUsWUFBb0I7UUFDakMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJO1lBQ2YsSUFBSSxDQUFDLFNBQVMsbUNBQWdDLEdBQUcsWUFBWSxDQUFDO0lBQ3JFLENBQUM7SUFFRCx3Q0FBZSxHQUFmLFVBQWdCLEtBQVU7UUFDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQztJQUVELG9DQUFXLEdBQVgsVUFBWSxJQUFTO1FBQ25CLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQVMsSUFBSSxDQUFDLFNBQVMsd0JBQXFCLEVBQUUsSUFBSSxFQUFFO1lBQ3ZFLE9BQU8sRUFBRSxVQUFVO1NBQ3BCLENBQUMsQ0FBQztJQUNMLENBQUM7O2dCQXpDRixVQUFVOzs7O2dCQUxGLFVBQVU7Z0RBWWQsTUFBTSxTQUFDLGFBQWE7O0lBbUN6QixxQkFBQztDQUFBLEFBMUNELElBMENDO1NBekNZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUmVzcG9uc2UgfSBmcm9tIFwiQGFuZ3VsYXIvY29tbW9uL2h0dHBcIjtcclxuXHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QgfSBmcm9tIFwicnhqcy9SeFwiO1xyXG4vLyBpbXBvcnQgeyBlbnZpcm9ubWVudCB9IGZyb20gXCIuLy4uLy4uL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudFwiO1xyXG5cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQWNjb3VudFNlcnZpY2Uge1xyXG4gIGJhc2VVUkw6IHN0cmluZztcclxuICBpc3N1ZXNVUkw6IHN0cmluZztcclxuICBpZE1nbXRVUkw6IHN0cmluZztcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcclxuICAgIEBJbmplY3QoXCJlbnZpcm9ubWVudFwiKSBwcml2YXRlIGVudmlyb25tZW50XHJcbiAgKSB7XHJcbiAgICB0aGlzLmJhc2VVUkwgPSB0aGlzLmVudmlyb25tZW50LmJhc2VVcmw7XHJcbiAgICB0aGlzLmlkTWdtdFVSTCA9IHRoaXMuZW52aXJvbm1lbnQuaWRNZ210QmFzZVVSTDtcclxuICAgIHRoaXMuaXNzdWVzVVJMID0gdGhpcy5lbnZpcm9ubWVudC5pc3N1ZXI7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmVudmlyb25tZW50LCBcIjw8PDw8PDxcIik7XHJcbiAgfVxyXG5cclxuICBzd2l0Y2hFdmVudDogU3ViamVjdDxhbnk+ID0gbmV3IFN1YmplY3QoKTtcclxuICBnZXQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIGNvbnNvbGUuaW5mbyhcIioqKioqKioqKlwiKTtcclxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke3RoaXMuYmFzZVVSTH0vdXNlcnMvbWVgKTtcclxuICB9XHJcblxyXG4gIC8vIGNoYW5nZVBhc3N3b3JkKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgLy8gXHQvL3JldHVybiB0aGlzLmh0dHAuZ2V0KGAke3RoaXMuYmFzZVVSTH0vdXNlcnMvbWVgKTtcclxuICAvLyBcdHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gYCR7dGhpcy5pc3N1ZXNVUkx9L2NoYW5nZS1wYXNzd29yZGA7XHJcbiAgLy8gXHRyZXR1cm5cclxuICAvLyB9XHJcblxyXG4gIGNoYW5nZVBhc3N3b3JkKGFjY2Vzc190b2tlbjogc3RyaW5nKSB7XHJcbiAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9XHJcbiAgICAgIGAke3RoaXMuaXNzdWVzVVJMfS9jaGFuZ2UtcGFzc3dvcmQ/YWNjZXNzX3Rva2VuPWAgKyBhY2Nlc3NfdG9rZW47XHJcbiAgfVxyXG5cclxuICBlbWl0U3dpdGNoRXZlbnQoZXZlbnQ6IGFueSkge1xyXG4gICAgdGhpcy5zd2l0Y2hFdmVudC5uZXh0KGV2ZW50KTtcclxuICB9XHJcblxyXG4gIGNoYW5nZVJlYWxtKGJvZHk6IGFueSk6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueVtdPj4ge1xyXG4gICAgY29uc29sZS5sb2coXCJib2R5XCIsIGJvZHkpO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5wb3N0PGFueT4oYCR7dGhpcy5pZE1nbXRVUkx9L3VzZXJzL2NoYW5nZS1yZWFsbWAsIGJvZHksIHtcclxuICAgICAgb2JzZXJ2ZTogXCJyZXNwb25zZVwiXHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19