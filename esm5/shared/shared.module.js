import { NgModule, CUSTOM_ELEMENTS_SCHEMA, forwardRef } from "@angular/core";
import { SnackBarService } from "./snackbar.service";
import { HasAnyAuthorityDirective } from "./auth/has-any-authority.directive";
import { AccountService } from "./auth/account.service";
import { AwsS3UploadService } from "./aws-s3-upload.service";
import { Principal } from "./auth/principal.service";
// import {
//   AccountService,
//   Principal,
//   AwsS3UploadService,
//   HasAnyAuthorityDirective
//   // SnackBarService
// } from "./";
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule.forRoot = function (environment, english) {
        return {
            ngModule: SharedModule,
            providers: [
                AccountService,
                {
                    provide: "env",
                    useValue: environment
                },
                {
                    provide: "env",
                    useValue: english
                }
            ]
        };
    };
    SharedModule.decorators = [
        { type: NgModule, args: [{
                    imports: [],
                    declarations: [HasAnyAuthorityDirective],
                    providers: [
                        AccountService,
                        Principal,
                        SnackBarService,
                        forwardRef(function () { return AwsS3UploadService; })
                    ],
                    exports: [HasAnyAuthorityDirective],
                    schemas: [CUSTOM_ELEMENTS_SCHEMA]
                },] }
    ];
    return SharedModule;
}());
export { SharedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsUUFBUSxFQUNSLHNCQUFzQixFQUN0QixVQUFVLEVBRVgsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFckQsV0FBVztBQUNYLG9CQUFvQjtBQUNwQixlQUFlO0FBQ2Ysd0JBQXdCO0FBQ3hCLDZCQUE2QjtBQUM3Qix1QkFBdUI7QUFDdkIsZUFBZTtBQUVmO0lBQUE7SUE2QkEsQ0FBQztJQWhCZSxvQkFBTyxHQUFyQixVQUFzQixXQUFnQixFQUFFLE9BQVk7UUFDbEQsT0FBTztZQUNMLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLFNBQVMsRUFBRTtnQkFDVCxjQUFjO2dCQUNkO29CQUNFLE9BQU8sRUFBRSxLQUFLO29CQUNkLFFBQVEsRUFBRSxXQUFXO2lCQUN0QjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsS0FBSztvQkFDZCxRQUFRLEVBQUUsT0FBTztpQkFDbEI7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOztnQkE1QkYsUUFBUSxTQUFDO29CQUNSLE9BQU8sRUFBRSxFQUFFO29CQUNYLFlBQVksRUFBRSxDQUFDLHdCQUF3QixDQUFDO29CQUN4QyxTQUFTLEVBQUU7d0JBQ1QsY0FBYzt3QkFDZCxTQUFTO3dCQUNULGVBQWU7d0JBQ2YsVUFBVSxDQUFDLGNBQU0sT0FBQSxrQkFBa0IsRUFBbEIsQ0FBa0IsQ0FBQztxQkFDckM7b0JBQ0QsT0FBTyxFQUFFLENBQUMsd0JBQXdCLENBQUM7b0JBQ25DLE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2lCQUNsQzs7SUFrQkQsbUJBQUM7Q0FBQSxBQTdCRCxJQTZCQztTQWpCWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBOZ01vZHVsZSxcclxuICBDVVNUT01fRUxFTUVOVFNfU0NIRU1BLFxyXG4gIGZvcndhcmRSZWYsXHJcbiAgTW9kdWxlV2l0aFByb3ZpZGVyc1xyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBTbmFja0JhclNlcnZpY2UgfSBmcm9tIFwiLi9zbmFja2Jhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSB9IGZyb20gXCIuL2F1dGgvaGFzLWFueS1hdXRob3JpdHkuZGlyZWN0aXZlXCI7XHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSBcIi4vYXV0aC9hY2NvdW50LnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXdzUzNVcGxvYWRTZXJ2aWNlIH0gZnJvbSBcIi4vYXdzLXMzLXVwbG9hZC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFByaW5jaXBhbCB9IGZyb20gXCIuL2F1dGgvcHJpbmNpcGFsLnNlcnZpY2VcIjtcclxuXHJcbi8vIGltcG9ydCB7XHJcbi8vICAgQWNjb3VudFNlcnZpY2UsXHJcbi8vICAgUHJpbmNpcGFsLFxyXG4vLyAgIEF3c1MzVXBsb2FkU2VydmljZSxcclxuLy8gICBIYXNBbnlBdXRob3JpdHlEaXJlY3RpdmVcclxuLy8gICAvLyBTbmFja0JhclNlcnZpY2VcclxuLy8gfSBmcm9tIFwiLi9cIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW10sXHJcbiAgZGVjbGFyYXRpb25zOiBbSGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIEFjY291bnRTZXJ2aWNlLFxyXG4gICAgUHJpbmNpcGFsLFxyXG4gICAgU25hY2tCYXJTZXJ2aWNlLFxyXG4gICAgZm9yd2FyZFJlZigoKSA9PiBBd3NTM1VwbG9hZFNlcnZpY2UpXHJcbiAgXSxcclxuICBleHBvcnRzOiBbSGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlXSxcclxuICBzY2hlbWFzOiBbQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNoYXJlZE1vZHVsZSB7XHJcbiAgcHVibGljIHN0YXRpYyBmb3JSb290KGVudmlyb25tZW50OiBhbnksIGVuZ2xpc2g6IGFueSk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IFNoYXJlZE1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgQWNjb3VudFNlcnZpY2UsXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvdmlkZTogXCJlbnZcIiwgLy8geW91IGNhbiBhbHNvIHVzZSBJbmplY3Rpb25Ub2tlblxyXG4gICAgICAgICAgdXNlVmFsdWU6IGVudmlyb25tZW50XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBcImVudlwiLCAvLyB5b3UgY2FuIGFsc28gdXNlIEluamVjdGlvblRva2VuXHJcbiAgICAgICAgICB1c2VWYWx1ZTogZW5nbGlzaFxyXG4gICAgICAgIH1cclxuICAgICAgXVxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuIl19