import { Injectable, Inject } from "@angular/core";
import { NgxXml2jsonService } from "ngx-xml2json";
import { HttpClient, HttpParams } from "@angular/common/http";
import { RequestOptions, Headers, Http } from "@angular/http";
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "@angular/http";
import * as i3 from "ngx-xml2json";
// import { environment } from "./../environments/environment";
var AwsS3UploadService = /** @class */ (function () {
    function AwsS3UploadService(httpClient, http, xml2js, environment) {
        this.httpClient = httpClient;
        this.http = http;
        this.xml2js = xml2js;
        this.environment = environment;
        this.allQuery = {};
        this.resourceUrl = environment.baseUrl + "/s3Policy";
    }
    AwsS3UploadService.prototype.find = function (req) {
        this.allQuery = {};
        this.allQuery.type = req.type;
        this.allQuery.mimeType = req.mimeType;
        var params = new HttpParams().set("obj", this.allQuery);
        return this.httpClient
            .get(this.resourceUrl + "?mimeType=" + req.mimeType + "&type=" + req.type)
            .map(function (res) {
            return res;
        });
    };
    AwsS3UploadService.prototype.uploadS3 = function (file, s3credentials, data) {
        var _this = this;
        console.log("s3credentials", s3credentials, file);
        /*let filename = file.name.substr(0, file.name.lastIndexOf('.'));
            console.log("filename",filename);*/
        var headers = new Headers();
        //headers.append('X-Host', 's3.amazonaws.com');
        var options = new RequestOptions({ headers: headers });
        var timeStamp = Math.floor(Date.now());
        //var fileName = this.user.id + timeStamp;
        var formData = new FormData();
        if (data.s3RequestType) {
            formData.append("key", s3credentials.keyPath + timeStamp + file.name.substring(file.name.indexOf(".")));
        }
        else {
            formData.append("key", s3credentials.keyPath + timeStamp + "." + file.type.substring(12));
        }
        formData.append("acl", s3credentials.acl);
        formData.append("success_action_status", "201");
        formData.append("Policy", s3credentials.s3Policy);
        formData.append("X-Amz-Credential", s3credentials.credentials);
        formData.append("X-Amz-Algorithm", "AWS4-HMAC-SHA256");
        formData.append("X-Amz-Date", s3credentials.date);
        formData.append("X-Amz-Signature", s3credentials.s3Signature);
        formData.append("Content-Type", file.type);
        formData.append("file", file);
        return new Promise(function (resolve, reject) {
            _this.http
                .post("https://" + s3credentials.bucket + ".s3.amazonaws.com/", formData, options)
                .map(function (res) { return res.text(); })
                .subscribe(function (response) {
                // let awsRspObj = this.xml2js.xmlToJson(response);
                var parser = new DOMParser();
                var xml = parser.parseFromString(response, "text/xml");
                var obj = _this.xml2js.xmlToJson(xml);
                resolve(obj);
            }, function (x) {
                console.error(x);
                reject();
            });
        });
    };
    AwsS3UploadService.decorators = [
        { type: Injectable, args: [{
                    providedIn: "root"
                },] }
    ];
    /** @nocollapse */
    AwsS3UploadService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: Http },
        { type: NgxXml2jsonService },
        { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
    ]; };
    AwsS3UploadService.ngInjectableDef = i0.defineInjectable({ factory: function AwsS3UploadService_Factory() { return new AwsS3UploadService(i0.inject(i1.HttpClient), i0.inject(i2.Http), i0.inject(i3.NgxXml2jsonService), i0.inject("environment")); }, token: AwsS3UploadService, providedIn: "root" });
    return AwsS3UploadService;
}());
export { AwsS3UploadService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXdzLXMzLXVwbG9hZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInNoYXJlZC9hd3MtczMtdXBsb2FkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFbkQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7OztBQUM5RCwrREFBK0Q7QUFFL0Q7SUFPRSw0QkFDVSxVQUFzQixFQUN0QixJQUFVLEVBQ1YsTUFBMEIsRUFDSCxXQUFXO1FBSGxDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUNWLFdBQU0sR0FBTixNQUFNLENBQW9CO1FBQ0gsZ0JBQVcsR0FBWCxXQUFXLENBQUE7UUFONUMsYUFBUSxHQUFRLEVBQUUsQ0FBQztRQVFqQixJQUFJLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxpQ0FBSSxHQUFKLFVBQUssR0FBUztRQUNaLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ25CLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUN0QyxJQUFNLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzFELE9BQU8sSUFBSSxDQUFDLFVBQVU7YUFDbkIsR0FBRyxDQUFJLElBQUksQ0FBQyxXQUFXLGVBQVksR0FBRyxHQUFHLENBQUMsUUFBUSxHQUFHLFFBQVEsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDO2FBQ3pFLEdBQUcsQ0FBQyxVQUFBLEdBQUc7WUFDTixPQUFPLEdBQUcsQ0FBQztRQUNiLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHFDQUFRLEdBQVIsVUFBUyxJQUFTLEVBQUUsYUFBa0IsRUFBRSxJQUFTO1FBQWpELGlCQTBEQztRQXpEQyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbEQ7K0NBQ2lDO1FBQ2pDLElBQUksT0FBTyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDNUIsK0NBQStDO1FBQy9DLElBQUksT0FBTyxHQUFHLElBQUksY0FBYyxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDdkQsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUN2QywwQ0FBMEM7UUFFMUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUM5QixJQUFHLElBQUksQ0FBQyxhQUFhLEVBQUM7WUFDcEIsUUFBUSxDQUFDLE1BQU0sQ0FDYixLQUFLLEVBQ0wsYUFBYSxDQUFDLE9BQU8sR0FBRyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FDaEYsQ0FBQztTQUNIO2FBQUk7WUFDSCxRQUFRLENBQUMsTUFBTSxDQUNiLEtBQUssRUFDTCxhQUFhLENBQUMsT0FBTyxHQUFHLFNBQVMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQ2xFLENBQUM7U0FDSDtRQUVELFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUMxQyxRQUFRLENBQUMsTUFBTSxDQUFDLHVCQUF1QixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ2hELFFBQVEsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNsRCxRQUFRLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMvRCxRQUFRLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFFdkQsUUFBUSxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBRWxELFFBQVEsQ0FBQyxNQUFNLENBQUMsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRTlELFFBQVEsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUU5QixPQUFPLElBQUksT0FBTyxDQUFDLFVBQUMsT0FBTyxFQUFFLE1BQU07WUFDakMsS0FBSSxDQUFDLElBQUk7aUJBQ04sSUFBSSxDQUNILGFBQVcsYUFBYSxDQUFDLE1BQU0sdUJBQW9CLEVBQ25ELFFBQVEsRUFDUixPQUFPLENBQ1I7aUJBQ0EsR0FBRyxDQUFDLFVBQUEsR0FBRyxJQUFJLE9BQUEsR0FBRyxDQUFDLElBQUksRUFBRSxFQUFWLENBQVUsQ0FBQztpQkFDdEIsU0FBUyxDQUNSLFVBQUEsUUFBUTtnQkFDTixtREFBbUQ7Z0JBQ25ELElBQU0sTUFBTSxHQUFHLElBQUksU0FBUyxFQUFFLENBQUM7Z0JBQy9CLElBQU0sR0FBRyxHQUFHLE1BQU0sQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2dCQUN6RCxJQUFNLEdBQUcsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2YsQ0FBQyxFQUNELFVBQUEsQ0FBQztnQkFDQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQixNQUFNLEVBQUUsQ0FBQztZQUNYLENBQUMsQ0FDRixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOztnQkF0RkYsVUFBVSxTQUFDO29CQUNWLFVBQVUsRUFBRSxNQUFNO2lCQUNuQjs7OztnQkFOUSxVQUFVO2dCQUNlLElBQUk7Z0JBRjdCLGtCQUFrQjtnREFnQnRCLE1BQU0sU0FBQyxhQUFhOzs7NkJBbEJ6QjtDQThGQyxBQXZGRCxJQXVGQztTQXBGWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3QgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSBcInJ4anMvUnhcIjtcclxuaW1wb3J0IHsgTmd4WG1sMmpzb25TZXJ2aWNlIH0gZnJvbSBcIm5neC14bWwyanNvblwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IFJlcXVlc3RPcHRpb25zLCBIZWFkZXJzLCBIdHRwIH0gZnJvbSBcIkBhbmd1bGFyL2h0dHBcIjtcclxuLy8gaW1wb3J0IHsgZW52aXJvbm1lbnQgfSBmcm9tIFwiLi8uLi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnRcIjtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICBwcm92aWRlZEluOiBcInJvb3RcIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgQXdzUzNVcGxvYWRTZXJ2aWNlIHtcclxuICByZXNvdXJjZVVybDogc3RyaW5nO1xyXG4gIGFsbFF1ZXJ5OiBhbnkgPSB7fTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGh0dHBDbGllbnQ6IEh0dHBDbGllbnQsXHJcbiAgICBwcml2YXRlIGh0dHA6IEh0dHAsXHJcbiAgICBwcml2YXRlIHhtbDJqczogTmd4WG1sMmpzb25TZXJ2aWNlLFxyXG4gICAgQEluamVjdChcImVudmlyb25tZW50XCIpIHByaXZhdGUgZW52aXJvbm1lbnRcclxuICApIHtcclxuICAgIHRoaXMucmVzb3VyY2VVcmwgPSBlbnZpcm9ubWVudC5iYXNlVXJsICsgXCIvczNQb2xpY3lcIjtcclxuICB9XHJcblxyXG4gIGZpbmQocmVxPzogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHRoaXMuYWxsUXVlcnkgPSB7fTtcclxuICAgIHRoaXMuYWxsUXVlcnkudHlwZSA9IHJlcS50eXBlO1xyXG4gICAgdGhpcy5hbGxRdWVyeS5taW1lVHlwZSA9IHJlcS5taW1lVHlwZTtcclxuICAgIGNvbnN0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCkuc2V0KFwib2JqXCIsIHRoaXMuYWxsUXVlcnkpO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cENsaWVudFxyXG4gICAgICAuZ2V0KGAke3RoaXMucmVzb3VyY2VVcmx9P21pbWVUeXBlPWAgKyByZXEubWltZVR5cGUgKyBgJnR5cGU9YCArIHJlcS50eXBlKVxyXG4gICAgICAubWFwKHJlcyA9PiB7XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICB1cGxvYWRTMyhmaWxlOiBhbnksIHMzY3JlZGVudGlhbHM6IGFueSwgZGF0YTogYW55KTogUHJvbWlzZTxhbnk+IHtcclxuICAgIGNvbnNvbGUubG9nKFwiczNjcmVkZW50aWFsc1wiLCBzM2NyZWRlbnRpYWxzLCBmaWxlKTtcclxuICAgIC8qbGV0IGZpbGVuYW1lID0gZmlsZS5uYW1lLnN1YnN0cigwLCBmaWxlLm5hbWUubGFzdEluZGV4T2YoJy4nKSk7XHJcblx0XHRjb25zb2xlLmxvZyhcImZpbGVuYW1lXCIsZmlsZW5hbWUpOyovXHJcbiAgICBsZXQgaGVhZGVycyA9IG5ldyBIZWFkZXJzKCk7XHJcbiAgICAvL2hlYWRlcnMuYXBwZW5kKCdYLUhvc3QnLCAnczMuYW1hem9uYXdzLmNvbScpO1xyXG4gICAgbGV0IG9wdGlvbnMgPSBuZXcgUmVxdWVzdE9wdGlvbnMoeyBoZWFkZXJzOiBoZWFkZXJzIH0pO1xyXG4gICAgdmFyIHRpbWVTdGFtcCA9IE1hdGguZmxvb3IoRGF0ZS5ub3coKSk7XHJcbiAgICAvL3ZhciBmaWxlTmFtZSA9IHRoaXMudXNlci5pZCArIHRpbWVTdGFtcDtcclxuXHJcbiAgICBsZXQgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuICAgIGlmKGRhdGEuczNSZXF1ZXN0VHlwZSl7XHJcbiAgICAgIGZvcm1EYXRhLmFwcGVuZChcclxuICAgICAgICBcImtleVwiLFxyXG4gICAgICAgIHMzY3JlZGVudGlhbHMua2V5UGF0aCArIHRpbWVTdGFtcCArIGZpbGUubmFtZS5zdWJzdHJpbmcoZmlsZS5uYW1lLmluZGV4T2YoXCIuXCIpKSBcclxuICAgICAgKTtcclxuICAgIH1lbHNle1xyXG4gICAgICBmb3JtRGF0YS5hcHBlbmQoXHJcbiAgICAgICAgXCJrZXlcIixcclxuICAgICAgICBzM2NyZWRlbnRpYWxzLmtleVBhdGggKyB0aW1lU3RhbXAgKyBcIi5cIiArIGZpbGUudHlwZS5zdWJzdHJpbmcoMTIpXHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGZvcm1EYXRhLmFwcGVuZChcImFjbFwiLCBzM2NyZWRlbnRpYWxzLmFjbCk7XHJcbiAgICBmb3JtRGF0YS5hcHBlbmQoXCJzdWNjZXNzX2FjdGlvbl9zdGF0dXNcIiwgXCIyMDFcIik7XHJcbiAgICBmb3JtRGF0YS5hcHBlbmQoXCJQb2xpY3lcIiwgczNjcmVkZW50aWFscy5zM1BvbGljeSk7XHJcbiAgICBmb3JtRGF0YS5hcHBlbmQoXCJYLUFtei1DcmVkZW50aWFsXCIsIHMzY3JlZGVudGlhbHMuY3JlZGVudGlhbHMpO1xyXG4gICAgZm9ybURhdGEuYXBwZW5kKFwiWC1BbXotQWxnb3JpdGhtXCIsIFwiQVdTNC1ITUFDLVNIQTI1NlwiKTtcclxuXHJcbiAgICBmb3JtRGF0YS5hcHBlbmQoXCJYLUFtei1EYXRlXCIsIHMzY3JlZGVudGlhbHMuZGF0ZSk7XHJcblxyXG4gICAgZm9ybURhdGEuYXBwZW5kKFwiWC1BbXotU2lnbmF0dXJlXCIsIHMzY3JlZGVudGlhbHMuczNTaWduYXR1cmUpO1xyXG5cclxuICAgIGZvcm1EYXRhLmFwcGVuZChcIkNvbnRlbnQtVHlwZVwiLCBmaWxlLnR5cGUpO1xyXG4gICAgZm9ybURhdGEuYXBwZW5kKFwiZmlsZVwiLCBmaWxlKTtcclxuXHJcbiAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICB0aGlzLmh0dHBcclxuICAgICAgICAucG9zdChcclxuICAgICAgICAgIGBodHRwczovLyR7czNjcmVkZW50aWFscy5idWNrZXR9LnMzLmFtYXpvbmF3cy5jb20vYCxcclxuICAgICAgICAgIGZvcm1EYXRhLFxyXG4gICAgICAgICAgb3B0aW9uc1xyXG4gICAgICAgIClcclxuICAgICAgICAubWFwKHJlcyA9PiByZXMudGV4dCgpKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIC8vIGxldCBhd3NSc3BPYmogPSB0aGlzLnhtbDJqcy54bWxUb0pzb24ocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICBjb25zdCBwYXJzZXIgPSBuZXcgRE9NUGFyc2VyKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHhtbCA9IHBhcnNlci5wYXJzZUZyb21TdHJpbmcocmVzcG9uc2UsIFwidGV4dC94bWxcIik7XHJcbiAgICAgICAgICAgIGNvbnN0IG9iaiA9IHRoaXMueG1sMmpzLnhtbFRvSnNvbih4bWwpO1xyXG4gICAgICAgICAgICByZXNvbHZlKG9iaik7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgeCA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IoeCk7XHJcbiAgICAgICAgICAgIHJlamVjdCgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICB9KTtcclxuICB9XHJcbn1cclxuIl19