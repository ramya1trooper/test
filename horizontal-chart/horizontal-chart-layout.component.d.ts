import { OnInit } from "@angular/core";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
export declare class HorizontalChartLayoutComponent implements OnInit {
    private _fuseTranslationLoaderService;
    private english;
    result: any;
    color: any;
    view: any;
    label: any;
    gradient: boolean;
    showXAxis: boolean;
    showXAxisLabel: boolean;
    showYAxisLabel: boolean;
    translate: any;
    Users: any;
    Controls: any;
    constructor(_fuseTranslationLoaderService: FuseTranslationLoaderService, english: any);
    ngOnInit(): void;
}
