import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
import { FormBuilder, FormGroup, FormArray } from "@angular/forms";
import { MatDialogRef } from "@angular/material";
import { MessageService } from "../_services/message.service";
export declare class ModelLayoutComponent {
    private _fuseTranslationLoaderService;
    matDialogRef: MatDialogRef<ModelLayoutComponent>;
    data: any;
    private _formBuilder;
    private messageService;
    private english;
    formGroup: FormGroup;
    form: FormArray;
    currentConfigData: any;
    modelData: any;
    submitted: boolean;
    constructor(_fuseTranslationLoaderService: FuseTranslationLoaderService, matDialogRef: MatDialogRef<ModelLayoutComponent>, data: any, _formBuilder: FormBuilder, messageService: MessageService, english: any);
    ngOnInit(): void;
    init(): FormGroup;
    selectionChange(event: any): void;
    closeModel(): void;
}
