import { ObservableMedia } from "@angular/flex-layout";
import { BehaviorSubject } from "rxjs";
export declare class FuseMatchMediaService {
    private _observableMedia;
    activeMediaQuery: string;
    onMediaChange: BehaviorSubject<string>;
    /**
     * Constructor
     *
     * @param {ObservableMedia} _observableMedia
     */
    constructor(_observableMedia: ObservableMedia);
    /**
     * Initialize
     *
     * @private
     */
    private _init;
}
