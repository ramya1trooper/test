import { Router } from "@angular/router";
import { Platform } from "@angular/cdk/platform";
import { Observable } from "rxjs";
export declare class FuseConfigService {
    private _platform;
    private _router;
    private _configSubject;
    private readonly _defaultConfig;
    /**
     * Constructor
     *
     * @param {Platform} _platform
     * @param {Router} _router
     * @param _config
     */
    constructor(_platform: Platform, _router: Router);
    /**
     * Set and get the config
     */
    config: any | Observable<any>;
    /**
     * Get default config
     *
     * @returns {any}
     */
    readonly defaultConfig: any;
    /**
     * Initialize
     *
     * @private
     */
    private _init;
    /**
     * Set config
     *
     * @param value
     * @param {{emitEvent: boolean}} opts
     */
    setConfig(value: any, opts?: {
        emitEvent: boolean;
    }): void;
    /**
     * Get config
     *
     * @returns {Observable<any>}
     */
    getConfig(): Observable<any>;
    /**
     * Reset to the default config
     */
    resetToDefaults(): void;
}
