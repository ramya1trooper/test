import { PipeTransform } from '@angular/core';
export declare class KeysPipe implements PipeTransform {
    /**
     * Transform
     *
     * @param value
     * @param {string[]} args
     * @returns {any}
     */
    transform(value: any, args: string[]): any;
}
