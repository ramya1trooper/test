import { ModuleWithProviders } from "@angular/core";
export declare class FuseModule {
    constructor(parentModule: FuseModule);
    static forRoot(metaData: any, english: any): ModuleWithProviders;
}
