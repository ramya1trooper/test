import { OnInit, OnDestroy } from "@angular/core";
import { MatSidenav } from "@angular/material";
import { ObservableMedia } from "@angular/flex-layout";
import { FuseMatchMediaService } from "../../../@fuse/services/match-media.service";
import { FuseMatSidenavHelperService } from "../../../@fuse/directives/fuse-mat-sidenav/fuse-mat-sidenav.service";
export declare class FuseMatSidenavHelperDirective implements OnInit, OnDestroy {
    private _fuseMatchMediaService;
    private _fuseMatSidenavHelperService;
    private _matSidenav;
    private _observableMedia;
    isLockedOpen: boolean;
    fuseMatSidenavHelper: string;
    matIsLockedOpen: string;
    private _unsubscribeAll;
    /**
     * Constructor
     *
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {FuseMatSidenavHelperService} _fuseMatSidenavHelperService
     * @param {MatSidenav} _matSidenav
     * @param {ObservableMedia} _observableMedia
     */
    constructor(_fuseMatchMediaService: FuseMatchMediaService, _fuseMatSidenavHelperService: FuseMatSidenavHelperService, _matSidenav: MatSidenav, _observableMedia: ObservableMedia);
    /**
     * On init
     */
    ngOnInit(): void;
    /**
     * On destroy
     */
    ngOnDestroy(): void;
}
export declare class FuseMatSidenavTogglerDirective {
    private _fuseMatSidenavHelperService;
    fuseMatSidenavToggler: string;
    /**
     * Constructor
     *
     * @param {FuseMatSidenavHelperService} _fuseMatSidenavHelperService
     */
    constructor(_fuseMatSidenavHelperService: FuseMatSidenavHelperService);
    /**
     * On click
     */
    onClick(): void;
}
