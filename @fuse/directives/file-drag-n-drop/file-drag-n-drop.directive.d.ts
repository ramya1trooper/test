import { EventEmitter } from '@angular/core';
export declare class FileDragNDropDirective {
    onFileDropped: EventEmitter<any>;
    constructor();
    onDragOver(evt: any): void;
    onDragLeave(evt: any): void;
    onDrop(evt: any): void;
}
