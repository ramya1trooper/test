import { ChangeDetectorRef, OnDestroy, OnInit } from '@angular/core';
import { FuseNavigationItem } from '../../../../../@fuse/types';
import { FuseNavigationService } from '../../../../../@fuse/components/navigation/navigation.service';
import { Compiler } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../../../../../_services/index';
export declare class FuseNavVerticalItemComponent implements OnInit, OnDestroy {
    private _changeDetectorRef;
    private _fuseNavigationService;
    private compiler;
    private router;
    private messageService;
    classes: string;
    item: FuseNavigationItem;
    private _unsubscribeAll;
    /**
     * Constructor
     */
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    constructor(_changeDetectorRef: ChangeDetectorRef, _fuseNavigationService: FuseNavigationService, compiler: Compiler, router: Router, messageService: MessageService);
    /**
     * On init
     */
    ngOnInit(): void;
    /**
     * On destroy
     */
    ngOnDestroy(): void;
    clickToGo(selectedItem: any): void;
}
