import { AccountService } from "./../../../shared/auth/account.service";
import { ChangeDetectorRef, OnInit } from "@angular/core";
import { FuseNavigationService } from "../../../@fuse/components/navigation/navigation.service";
export declare class FuseNavigationComponent implements OnInit {
    private _changeDetectorRef;
    private _fuseNavigationService;
    private accountService;
    layout: string;
    currentUserRole: any;
    navigation: any;
    filteredMenus: any;
    private _unsubscribeAll;
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    constructor(_changeDetectorRef: ChangeDetectorRef, _fuseNavigationService: FuseNavigationService, accountService: AccountService);
    /**
     * On init
     */
    ngOnInit(): void;
    loadAll(): void;
}
