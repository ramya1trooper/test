import { ElementRef } from '@angular/core';
export declare class FuseWidgetToggleDirective {
    elementRef: ElementRef;
    /**
     * Constructor
     *
     * @param {ElementRef} elementRef
     */
    constructor(elementRef: ElementRef);
}
