import { OnInit } from "@angular/core";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
export declare class PieGridComponent implements OnInit {
    private _fuseTranslationLoaderService;
    private english;
    result: any;
    scheme: any;
    view: any;
    label: any;
    translate: any;
    ChartData: any;
    constructor(_fuseTranslationLoaderService: FuseTranslationLoaderService, english: any);
    ngOnInit(): void;
}
