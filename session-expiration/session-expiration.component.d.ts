import { OnDestroy, OnInit } from '@angular/core';
import { SessionService } from './session-expiration.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
export declare class SessionComponent implements OnDestroy, OnInit {
    private authService;
    dialog: MatDialog;
    private oauthService;
    minutesDisplay: number;
    secondsDisplay: number;
    endTime: number;
    unsubscribe$: Subject<void>;
    timerSubscription: Subscription;
    appParentMessage: string;
    subscription: Subscription;
    constructor(authService: SessionService, dialog: MatDialog, oauthService: OAuthService);
    resetTimerfn(): void;
    ngOnInit(): void;
    openDialog(): void;
    ngOnDestroy(): void;
    resetTimer(endTime?: number): void;
    private render;
    private getSeconds;
    private getMinutes;
    private pad;
}
