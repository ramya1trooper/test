import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
export declare class SessionService {
    private oauthService;
    private httpClient;
    baseURL: string;
    _userActionOccured: Subject<any>;
    constructor(oauthService: OAuthService, httpClient: HttpClient);
    userActionOccured(): Observable<any>;
    notifyUserAction(): void;
    loginUser(): void;
    logOutUser(): void;
    continueSession(): void;
    stopSession(): void;
}
