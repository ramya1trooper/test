import { SessionService } from './session-expiration.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material';
export declare class AuthModelDialog {
    private authService;
    dialog: MatDialog;
    matDialogRef: MatDialogRef<AuthModelDialog>;
    private data;
    private oauthService;
    minutesDisplay: any;
    secondsDisplay: any;
    appChildMessage: string;
    unsubscribe$: Subject<void>;
    timerSubscription: Subscription;
    constructor(authService: SessionService, dialog: MatDialog, matDialogRef: MatDialogRef<AuthModelDialog>, data: any, oauthService: OAuthService);
    ngOnInit(): void;
    logout(): void;
    onSubmit(): void;
    private render;
    private getSeconds;
    private getMinutes;
    private pad;
}
