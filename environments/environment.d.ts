export declare const environment: {
    production: boolean;
    hmr: boolean;
    roles: string[];
    baseUrl: string;
    idMgmtBaseURL: string;
    issuer: string;
    clientId: string;
    redirectUri: string;
    freshdeskBaseUrl: string;
    requireHttps: boolean;
};
