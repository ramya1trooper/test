import { ModuleWithProviders } from "@angular/core";
export declare class ToolbarModule {
    static forRoot(metaData: any, english: any): ModuleWithProviders;
}
