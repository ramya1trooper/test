import { MessageService } from "./../_services/message.service";
import { OnDestroy, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { FuseConfigService } from "../@fuse/services/config.service";
import { FuseSidebarService } from "../@fuse/components/sidebar/sidebar.service";
import { ContentService } from "../content/content.service";
import { AccountService } from "../shared/auth/account.service";
import { OAuthService } from "angular-oauth2-oidc";
import { CookieService } from 'ngx-cookie-service';
import { LoaderService } from '../loader.service';
import { FuseTranslationLoaderService } from '../@fuse/services/translation-loader.service';
export declare class ToolbarComponent implements OnInit, OnDestroy {
    private _fuseTranslationLoaderService;
    private _fuseConfigService;
    private _fuseSidebarService;
    private _translateService;
    private contentService;
    private accountService;
    private oauthService;
    private messageService;
    private _cookieService;
    private loaderService;
    private _metaData;
    english: any;
    horizontalNavbar: boolean;
    rightNavbar: boolean;
    hiddenNavbar: boolean;
    languages: any;
    navigation: any;
    selectedLanguage: any;
    userStatusOptions: any[];
    dataSource: any;
    notificationList: any[];
    selectedDatasourceId: any;
    defaultDatasourceName: any;
    defaultDatasource: any;
    defaultRealm: string;
    notificationData: any[];
    user: any;
    data: any;
    realm_data: any;
    cookieDS: any;
    count: number;
    private _unsubscribeAll;
    enableNotification: boolean;
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    constructor(_fuseTranslationLoaderService: FuseTranslationLoaderService, _fuseConfigService: FuseConfigService, _fuseSidebarService: FuseSidebarService, _translateService: TranslateService, contentService: ContentService, accountService: AccountService, oauthService: OAuthService, messageService: MessageService, _cookieService: CookieService, loaderService: LoaderService, _metaData: any, english: any);
    /**
     * On init
     */
    ngOnInit(): void;
    getId(event: any): void;
    getAllDatasource(): void;
    clearallNotification(notificationList: any): void;
    getAllNotification(): void;
    navigateToDownloads(msg: any): void;
    getMetaData(): any[];
    sendMessage(id: any, triggerFrom: any): void;
    updateDatasource(id: any): void;
    redirectToNotification(): void;
    ousideClick(e: any): void;
    Read(input: any): void;
    Remove(input: any): void;
    /**
     * On destroy
     */
    ngOnDestroy(): void;
    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key: any): void;
    /**
     * Search
     *
     * @param value
     */
    search(value: any): void;
    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang: any): void;
    logout(): void;
    changePwd(): void;
    openSideNodifiNav(e: any): void;
    closeSideNav(): void;
    close(): void;
}
