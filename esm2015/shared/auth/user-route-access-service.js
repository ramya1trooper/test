import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Principal } from "./principal.service";
export class UserRouteAccessService {
    constructor(router, principal) {
        this.router = router;
        this.principal = principal;
        console.info("<<<<<<<UserRouteAccessService");
    }
    canActivate(route, state) {
        const authorities = route.data["authorities"];
        return this.checkLogin(authorities, state.url);
    }
    checkLogin(authorities, url) {
        const principal = this.principal;
        return Promise.resolve(principal.identity().then(account => {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account) {
                return principal.hasAnyAuthority(authorities).then(response => {
                    if (response) {
                        return true;
                    }
                    return false;
                });
            }
            this.router.navigate(["login"]).then(() => {
                if (!account) {
                }
            });
            return false;
        }));
    }
}
UserRouteAccessService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
UserRouteAccessService.ctorParameters = () => [
    { type: Router },
    { type: Principal }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1yb3V0ZS1hY2Nlc3Mtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzaGFyZWQvYXV0aC91c2VyLXJvdXRlLWFjY2Vzcy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUdMLE1BQU0sRUFFUCxNQUFNLGlCQUFpQixDQUFDO0FBRXpCLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUdoRCxNQUFNLE9BQU8sc0JBQXNCO0lBQ2pDLFlBQW9CLE1BQWMsRUFBVSxTQUFvQjtRQUE1QyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQVUsY0FBUyxHQUFULFNBQVMsQ0FBVztRQUM5RCxPQUFPLENBQUMsSUFBSSxDQUFDLCtCQUErQixDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELFdBQVcsQ0FDVCxLQUE2QixFQUM3QixLQUEwQjtRQUUxQixNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxVQUFVLENBQUMsV0FBcUIsRUFBRSxHQUFXO1FBQzNDLE1BQU0sU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDakMsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUNwQixTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ2xDLElBQUksQ0FBQyxXQUFXLElBQUksV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQzVDLE9BQU8sSUFBSSxDQUFDO2FBQ2I7WUFFRCxJQUFJLE9BQU8sRUFBRTtnQkFDWCxPQUFPLFNBQVMsQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM1RCxJQUFJLFFBQVEsRUFBRTt3QkFDWixPQUFPLElBQUksQ0FBQztxQkFDYjtvQkFDRCxPQUFPLEtBQUssQ0FBQztnQkFDZixDQUFDLENBQUMsQ0FBQzthQUNKO1lBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUU7Z0JBQ3hDLElBQUksQ0FBQyxPQUFPLEVBQUU7aUJBQ2I7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sS0FBSyxDQUFDO1FBQ2YsQ0FBQyxDQUFDLENBQ0gsQ0FBQztJQUNKLENBQUM7OztZQXRDRixVQUFVOzs7O1lBTlQsTUFBTTtZQUlDLFNBQVMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtcclxuICBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gIENhbkFjdGl2YXRlLFxyXG4gIFJvdXRlcixcclxuICBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbn0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgUHJpbmNpcGFsIH0gZnJvbSBcIi4vcHJpbmNpcGFsLnNlcnZpY2VcIjtcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFVzZXJSb3V0ZUFjY2Vzc1NlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XHJcbiAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlciwgcHJpdmF0ZSBwcmluY2lwYWw6IFByaW5jaXBhbCkge1xyXG4gICAgY29uc29sZS5pbmZvKFwiPDw8PDw8PFVzZXJSb3V0ZUFjY2Vzc1NlcnZpY2VcIik7XHJcbiAgfVxyXG5cclxuICBjYW5BY3RpdmF0ZShcclxuICAgIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3RcclxuICApOiBib29sZWFuIHwgUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICBjb25zdCBhdXRob3JpdGllcyA9IHJvdXRlLmRhdGFbXCJhdXRob3JpdGllc1wiXTtcclxuICAgIHJldHVybiB0aGlzLmNoZWNrTG9naW4oYXV0aG9yaXRpZXMsIHN0YXRlLnVybCk7XHJcbiAgfVxyXG5cclxuICBjaGVja0xvZ2luKGF1dGhvcml0aWVzOiBzdHJpbmdbXSwgdXJsOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgIGNvbnN0IHByaW5jaXBhbCA9IHRoaXMucHJpbmNpcGFsO1xyXG4gICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZShcclxuICAgICAgcHJpbmNpcGFsLmlkZW50aXR5KCkudGhlbihhY2NvdW50ID0+IHtcclxuICAgICAgICBpZiAoIWF1dGhvcml0aWVzIHx8IGF1dGhvcml0aWVzLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoYWNjb3VudCkge1xyXG4gICAgICAgICAgcmV0dXJuIHByaW5jaXBhbC5oYXNBbnlBdXRob3JpdHkoYXV0aG9yaXRpZXMpLnRoZW4ocmVzcG9uc2UgPT4ge1xyXG4gICAgICAgICAgICBpZiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcImxvZ2luXCJdKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgIGlmICghYWNjb3VudCkge1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgfSlcclxuICAgICk7XHJcbiAgfVxyXG59XHJcbiJdfQ==