import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Subject } from "rxjs/Rx";
// import { environment } from "./../../environments/environment";
export class AccountService {
    constructor(http, environment) {
        this.http = http;
        this.environment = environment;
        this.switchEvent = new Subject();
        this.baseURL = this.environment.baseUrl;
        this.idMgmtURL = this.environment.idMgmtBaseURL;
        this.issuesURL = this.environment.issuer;
        console.log(this.environment, "<<<<<<<");
    }
    get() {
        console.info("*********");
        return this.http.get(`${this.baseURL}/users/me`);
    }
    // changePassword(): Observable<any> {
    // 	//return this.http.get(`${this.baseURL}/users/me`);
    // 	window.location.href = `${this.issuesURL}/change-password`;
    // 	return
    // }
    changePassword(access_token) {
        window.location.href =
            `${this.issuesURL}/change-password?access_token=` + access_token;
    }
    emitSwitchEvent(event) {
        this.switchEvent.next(event);
    }
    changeRealm(body) {
        console.log("body", body);
        return this.http.post(`${this.idMgmtURL}/users/change-realm`, body, {
            observe: "response"
        });
    }
}
AccountService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
AccountService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInNoYXJlZC9hdXRoL2FjY291bnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsVUFBVSxFQUFnQixNQUFNLHNCQUFzQixDQUFDO0FBRWhFLE9BQU8sRUFBYyxPQUFPLEVBQUUsTUFBTSxTQUFTLENBQUM7QUFDOUMsa0VBQWtFO0FBR2xFLE1BQU0sT0FBTyxjQUFjO0lBSXpCLFlBQ1UsSUFBZ0IsRUFDTyxXQUFXO1FBRGxDLFNBQUksR0FBSixJQUFJLENBQVk7UUFDTyxnQkFBVyxHQUFYLFdBQVcsQ0FBQTtRQVE1QyxnQkFBVyxHQUFpQixJQUFJLE9BQU8sRUFBRSxDQUFDO1FBTnhDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxTQUFTLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBR0QsR0FBRztRQUNELE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDMUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLFdBQVcsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxzQ0FBc0M7SUFDdEMsdURBQXVEO0lBQ3ZELCtEQUErRDtJQUMvRCxVQUFVO0lBQ1YsSUFBSTtJQUVKLGNBQWMsQ0FBQyxZQUFvQjtRQUNqQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUk7WUFDbEIsR0FBRyxJQUFJLENBQUMsU0FBUyxnQ0FBZ0MsR0FBRyxZQUFZLENBQUM7SUFDckUsQ0FBQztJQUVELGVBQWUsQ0FBQyxLQUFVO1FBQ3hCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCxXQUFXLENBQUMsSUFBUztRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMxQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMscUJBQXFCLEVBQUUsSUFBSSxFQUFFO1lBQ3ZFLE9BQU8sRUFBRSxVQUFVO1NBQ3BCLENBQUMsQ0FBQztJQUNMLENBQUM7OztZQXpDRixVQUFVOzs7O1lBTEYsVUFBVTs0Q0FZZCxNQUFNLFNBQUMsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUsIEluamVjdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBSZXNwb25zZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gXCJyeGpzL1J4XCI7XHJcbi8vIGltcG9ydCB7IGVudmlyb25tZW50IH0gZnJvbSBcIi4vLi4vLi4vZW52aXJvbm1lbnRzL2Vudmlyb25tZW50XCI7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBBY2NvdW50U2VydmljZSB7XHJcbiAgYmFzZVVSTDogc3RyaW5nO1xyXG4gIGlzc3Vlc1VSTDogc3RyaW5nO1xyXG4gIGlkTWdtdFVSTDogc3RyaW5nO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgQEluamVjdChcImVudmlyb25tZW50XCIpIHByaXZhdGUgZW52aXJvbm1lbnRcclxuICApIHtcclxuICAgIHRoaXMuYmFzZVVSTCA9IHRoaXMuZW52aXJvbm1lbnQuYmFzZVVybDtcclxuICAgIHRoaXMuaWRNZ210VVJMID0gdGhpcy5lbnZpcm9ubWVudC5pZE1nbXRCYXNlVVJMO1xyXG4gICAgdGhpcy5pc3N1ZXNVUkwgPSB0aGlzLmVudmlyb25tZW50Lmlzc3VlcjtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZW52aXJvbm1lbnQsIFwiPDw8PDw8PFwiKTtcclxuICB9XHJcblxyXG4gIHN3aXRjaEV2ZW50OiBTdWJqZWN0PGFueT4gPSBuZXcgU3ViamVjdCgpO1xyXG4gIGdldCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgY29uc29sZS5pbmZvKFwiKioqKioqKioqXCIpO1xyXG4gICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7dGhpcy5iYXNlVVJMfS91c2Vycy9tZWApO1xyXG4gIH1cclxuXHJcbiAgLy8gY2hhbmdlUGFzc3dvcmQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAvLyBcdC8vcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7dGhpcy5iYXNlVVJMfS91c2Vycy9tZWApO1xyXG4gIC8vIFx0d2luZG93LmxvY2F0aW9uLmhyZWYgPSBgJHt0aGlzLmlzc3Vlc1VSTH0vY2hhbmdlLXBhc3N3b3JkYDtcclxuICAvLyBcdHJldHVyblxyXG4gIC8vIH1cclxuXHJcbiAgY2hhbmdlUGFzc3dvcmQoYWNjZXNzX3Rva2VuOiBzdHJpbmcpIHtcclxuICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID1cclxuICAgICAgYCR7dGhpcy5pc3N1ZXNVUkx9L2NoYW5nZS1wYXNzd29yZD9hY2Nlc3NfdG9rZW49YCArIGFjY2Vzc190b2tlbjtcclxuICB9XHJcblxyXG4gIGVtaXRTd2l0Y2hFdmVudChldmVudDogYW55KSB7XHJcbiAgICB0aGlzLnN3aXRjaEV2ZW50Lm5leHQoZXZlbnQpO1xyXG4gIH1cclxuXHJcbiAgY2hhbmdlUmVhbG0oYm9keTogYW55KTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55W10+PiB7XHJcbiAgICBjb25zb2xlLmxvZyhcImJvZHlcIiwgYm9keSk7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwLnBvc3Q8YW55PihgJHt0aGlzLmlkTWdtdFVSTH0vdXNlcnMvY2hhbmdlLXJlYWxtYCwgYm9keSwge1xyXG4gICAgICBvYnNlcnZlOiBcInJlc3BvbnNlXCJcclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=