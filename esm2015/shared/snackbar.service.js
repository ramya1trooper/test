import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
export class SnackBarMessage {
    constructor() {
        this.action = null;
        this.config = null;
    }
}
export class SnackBarService {
    constructor(snackBar) {
        this.snackBar = snackBar;
        this.messageQueue = new Subject();
        this.subscription = this.messageQueue.subscribe(message => {
            this.snackBarRef = this.snackBar.open(message.message, message.action, message.config);
        });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    /**
     * Add a message
     * @param message The message to show in the snackbar.
     * @param action The label for the snackbar action.
     * @param config Additional configuration options for the snackbar.
     */
    add(message, action, config) {
        if (!config) {
            config = new MatSnackBarConfig();
            config.duration = 3000;
            config.verticalPosition = 'top';
            config.horizontalPosition = 'right';
            config.panelClass = ['green-snackbar'];
        }
        let sbMessage = new SnackBarMessage();
        sbMessage.message = message;
        sbMessage.action = 'x';
        sbMessage.config = config;
        this.messageQueue.next(sbMessage);
    }
    warning(message, action, config) {
        if (!config) {
            config = new MatSnackBarConfig();
            config.duration = 3000;
            config.verticalPosition = 'top';
            config.horizontalPosition = 'right';
            config.panelClass = ['red-snackbar'];
        }
        let sbMessage = new SnackBarMessage();
        sbMessage.message = message;
        sbMessage.action = 'x';
        sbMessage.config = config;
        this.messageQueue.next(sbMessage);
    }
}
SnackBarService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
SnackBarService.ctorParameters = () => [
    { type: MatSnackBar }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic25hY2tiYXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzaGFyZWQvc25hY2tiYXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBVSxVQUFVLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBRzdFLE1BQU0sT0FBTyxlQUFlO0lBQTVCO1FBRUMsV0FBTSxHQUFXLElBQUksQ0FBQztRQUN0QixXQUFNLEdBQXNCLElBQUksQ0FBQztJQUNsQyxDQUFDO0NBQUE7QUFHRCxNQUFNLE9BQU8sZUFBZTtJQU0zQixZQUFtQixRQUFxQjtRQUFyQixhQUFRLEdBQVIsUUFBUSxDQUFhO1FBTGhDLGlCQUFZLEdBQTZCLElBQUksT0FBTyxFQUFtQixDQUFDO1FBTS9FLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDekQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ3hGLENBQUMsQ0FBQyxDQUFDO0lBQ0osQ0FBQztJQUVELFdBQVc7UUFDVixJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRTs7Ozs7T0FLRztJQUNOLEdBQUcsQ0FBQyxPQUFlLEVBQUUsTUFBZSxFQUFFLE1BQTBCO1FBRS9ELElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDWixNQUFNLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7WUFDaEMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUNwQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUN2QztRQUVELElBQUksU0FBUyxHQUFHLElBQUksZUFBZSxFQUFFLENBQUM7UUFDdEMsU0FBUyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7UUFDNUIsU0FBUyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDdkIsU0FBUyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7UUFFMUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUNELE9BQU8sQ0FBQyxPQUFlLEVBQUUsTUFBZSxFQUFFLE1BQTBCO1FBQ25FLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDWixNQUFNLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1lBQ2pDLE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7WUFDaEMsTUFBTSxDQUFDLGtCQUFrQixHQUFHLE9BQU8sQ0FBQztZQUNwQyxNQUFNLENBQUMsVUFBVSxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDckM7UUFFRCxJQUFJLFNBQVMsR0FBRyxJQUFJLGVBQWUsRUFBRSxDQUFDO1FBQ3RDLFNBQVMsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1FBQzVCLFNBQVMsQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDO1FBQ3ZCLFNBQVMsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO1FBRTFCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7OztZQXZERCxVQUFVOzs7O1lBVEYsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMvU3Vic2NyaXB0aW9uJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMvU3ViamVjdCc7XHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFNuYWNrQmFyLCBNYXRTbmFja0JhckNvbmZpZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NuYWNrLWJhcic7XHJcbmltcG9ydCB7IE1hdFNuYWNrQmFyUmVmLCBTaW1wbGVTbmFja0JhciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL3NuYWNrLWJhcic7XHJcblxyXG5leHBvcnQgY2xhc3MgU25hY2tCYXJNZXNzYWdlIHtcclxuXHRtZXNzYWdlOiBzdHJpbmc7XHJcblx0YWN0aW9uOiBzdHJpbmcgPSBudWxsO1xyXG5cdGNvbmZpZzogTWF0U25hY2tCYXJDb25maWcgPSBudWxsO1xyXG59XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBTbmFja0JhclNlcnZpY2UgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xyXG5cdHByaXZhdGUgbWVzc2FnZVF1ZXVlOiBTdWJqZWN0PFNuYWNrQmFyTWVzc2FnZT4gPSBuZXcgU3ViamVjdDxTbmFja0Jhck1lc3NhZ2U+KCk7XHJcblx0cHJpdmF0ZSBzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHRwcml2YXRlIHNuYWNrQmFyUmVmOiBNYXRTbmFja0JhclJlZjxTaW1wbGVTbmFja0Jhcj47XHJcblxyXG5cclxuXHRjb25zdHJ1Y3RvcihwdWJsaWMgc25hY2tCYXI6IE1hdFNuYWNrQmFyKSB7XHJcblx0XHR0aGlzLnN1YnNjcmlwdGlvbiA9IHRoaXMubWVzc2FnZVF1ZXVlLnN1YnNjcmliZShtZXNzYWdlID0+IHtcclxuXHRcdFx0dGhpcy5zbmFja0JhclJlZiA9IHRoaXMuc25hY2tCYXIub3BlbihtZXNzYWdlLm1lc3NhZ2UsIG1lc3NhZ2UuYWN0aW9uLCBtZXNzYWdlLmNvbmZpZyk7XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG5cdG5nT25EZXN0cm95KCkge1xyXG5cdFx0dGhpcy5zdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKTtcclxuXHR9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGQgYSBtZXNzYWdlXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUaGUgbWVzc2FnZSB0byBzaG93IGluIHRoZSBzbmFja2Jhci5cclxuICAgICAqIEBwYXJhbSBhY3Rpb24gVGhlIGxhYmVsIGZvciB0aGUgc25hY2tiYXIgYWN0aW9uLlxyXG4gICAgICogQHBhcmFtIGNvbmZpZyBBZGRpdGlvbmFsIGNvbmZpZ3VyYXRpb24gb3B0aW9ucyBmb3IgdGhlIHNuYWNrYmFyLlxyXG4gICAgICovXHJcblx0YWRkKG1lc3NhZ2U6IHN0cmluZywgYWN0aW9uPzogc3RyaW5nLCBjb25maWc/OiBNYXRTbmFja0JhckNvbmZpZyk6IHZvaWQge1xyXG5cclxuXHRcdGlmICghY29uZmlnKSB7XHJcblx0XHRcdGNvbmZpZyA9IG5ldyBNYXRTbmFja0JhckNvbmZpZygpO1xyXG5cdFx0XHRjb25maWcuZHVyYXRpb24gPSAzMDAwO1xyXG5cdFx0XHRjb25maWcudmVydGljYWxQb3NpdGlvbiA9ICd0b3AnO1xyXG5cdFx0XHRjb25maWcuaG9yaXpvbnRhbFBvc2l0aW9uID0gJ3JpZ2h0JztcclxuXHRcdFx0Y29uZmlnLnBhbmVsQ2xhc3MgPSBbJ2dyZWVuLXNuYWNrYmFyJ107XHJcblx0XHR9XHJcblxyXG5cdFx0bGV0IHNiTWVzc2FnZSA9IG5ldyBTbmFja0Jhck1lc3NhZ2UoKTtcclxuXHRcdHNiTWVzc2FnZS5tZXNzYWdlID0gbWVzc2FnZTtcclxuXHRcdHNiTWVzc2FnZS5hY3Rpb24gPSAneCc7XHJcblx0XHRzYk1lc3NhZ2UuY29uZmlnID0gY29uZmlnO1xyXG5cclxuXHRcdHRoaXMubWVzc2FnZVF1ZXVlLm5leHQoc2JNZXNzYWdlKTtcclxuXHR9XHJcblx0d2FybmluZyhtZXNzYWdlOiBzdHJpbmcsIGFjdGlvbj86IHN0cmluZywgY29uZmlnPzogTWF0U25hY2tCYXJDb25maWcpOiB2b2lkIHtcclxuXHRcdGlmICghY29uZmlnKSB7XHJcblx0XHRcdGNvbmZpZyA9IG5ldyBNYXRTbmFja0JhckNvbmZpZygpO1xyXG5cdFx0XHRjb25maWcuZHVyYXRpb24gPSAzMDAwO1xyXG5cdFx0XHRjb25maWcudmVydGljYWxQb3NpdGlvbiA9ICd0b3AnO1xyXG5cdFx0XHRjb25maWcuaG9yaXpvbnRhbFBvc2l0aW9uID0gJ3JpZ2h0JztcclxuXHRcdFx0Y29uZmlnLnBhbmVsQ2xhc3MgPSBbJ3JlZC1zbmFja2JhciddO1xyXG5cdFx0fVxyXG5cclxuXHRcdGxldCBzYk1lc3NhZ2UgPSBuZXcgU25hY2tCYXJNZXNzYWdlKCk7XHJcblx0XHRzYk1lc3NhZ2UubWVzc2FnZSA9IG1lc3NhZ2U7XHJcblx0XHRzYk1lc3NhZ2UuYWN0aW9uID0gJ3gnO1xyXG5cdFx0c2JNZXNzYWdlLmNvbmZpZyA9IGNvbmZpZztcclxuXHJcblx0XHR0aGlzLm1lc3NhZ2VRdWV1ZS5uZXh0KHNiTWVzc2FnZSk7XHJcblx0fVxyXG59XHJcbiJdfQ==