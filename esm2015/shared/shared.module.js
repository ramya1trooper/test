import { NgModule, CUSTOM_ELEMENTS_SCHEMA, forwardRef } from "@angular/core";
import { SnackBarService } from "./snackbar.service";
import { HasAnyAuthorityDirective } from "./auth/has-any-authority.directive";
import { AccountService } from "./auth/account.service";
import { AwsS3UploadService } from "./aws-s3-upload.service";
import { Principal } from "./auth/principal.service";
// import {
//   AccountService,
//   Principal,
//   AwsS3UploadService,
//   HasAnyAuthorityDirective
//   // SnackBarService
// } from "./";
export class SharedModule {
    static forRoot(environment, english) {
        return {
            ngModule: SharedModule,
            providers: [
                AccountService,
                {
                    provide: "env",
                    useValue: environment
                },
                {
                    provide: "env",
                    useValue: english
                }
            ]
        };
    }
}
SharedModule.decorators = [
    { type: NgModule, args: [{
                imports: [],
                declarations: [HasAnyAuthorityDirective],
                providers: [
                    AccountService,
                    Principal,
                    SnackBarService,
                    forwardRef(() => AwsS3UploadService)
                ],
                exports: [HasAnyAuthorityDirective],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzaGFyZWQvc2hhcmVkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsUUFBUSxFQUNSLHNCQUFzQixFQUN0QixVQUFVLEVBRVgsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFckQsV0FBVztBQUNYLG9CQUFvQjtBQUNwQixlQUFlO0FBQ2Ysd0JBQXdCO0FBQ3hCLDZCQUE2QjtBQUM3Qix1QkFBdUI7QUFDdkIsZUFBZTtBQWNmLE1BQU0sT0FBTyxZQUFZO0lBQ2hCLE1BQU0sQ0FBQyxPQUFPLENBQUMsV0FBZ0IsRUFBRSxPQUFZO1FBQ2xELE9BQU87WUFDTCxRQUFRLEVBQUUsWUFBWTtZQUN0QixTQUFTLEVBQUU7Z0JBQ1QsY0FBYztnQkFDZDtvQkFDRSxPQUFPLEVBQUUsS0FBSztvQkFDZCxRQUFRLEVBQUUsV0FBVztpQkFDdEI7Z0JBQ0Q7b0JBQ0UsT0FBTyxFQUFFLEtBQUs7b0JBQ2QsUUFBUSxFQUFFLE9BQU87aUJBQ2xCO2FBQ0Y7U0FDRixDQUFDO0lBQ0osQ0FBQzs7O1lBNUJGLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUUsRUFBRTtnQkFDWCxZQUFZLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQztnQkFDeEMsU0FBUyxFQUFFO29CQUNULGNBQWM7b0JBQ2QsU0FBUztvQkFDVCxlQUFlO29CQUNmLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztpQkFDckM7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsd0JBQXdCLENBQUM7Z0JBQ25DLE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2FBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBOZ01vZHVsZSxcclxuICBDVVNUT01fRUxFTUVOVFNfU0NIRU1BLFxyXG4gIGZvcndhcmRSZWYsXHJcbiAgTW9kdWxlV2l0aFByb3ZpZGVyc1xyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vblwiO1xyXG5pbXBvcnQgeyBTbmFja0JhclNlcnZpY2UgfSBmcm9tIFwiLi9zbmFja2Jhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSB9IGZyb20gXCIuL2F1dGgvaGFzLWFueS1hdXRob3JpdHkuZGlyZWN0aXZlXCI7XHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSBcIi4vYXV0aC9hY2NvdW50LnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXdzUzNVcGxvYWRTZXJ2aWNlIH0gZnJvbSBcIi4vYXdzLXMzLXVwbG9hZC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IFByaW5jaXBhbCB9IGZyb20gXCIuL2F1dGgvcHJpbmNpcGFsLnNlcnZpY2VcIjtcclxuXHJcbi8vIGltcG9ydCB7XHJcbi8vICAgQWNjb3VudFNlcnZpY2UsXHJcbi8vICAgUHJpbmNpcGFsLFxyXG4vLyAgIEF3c1MzVXBsb2FkU2VydmljZSxcclxuLy8gICBIYXNBbnlBdXRob3JpdHlEaXJlY3RpdmVcclxuLy8gICAvLyBTbmFja0JhclNlcnZpY2VcclxuLy8gfSBmcm9tIFwiLi9cIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW10sXHJcbiAgZGVjbGFyYXRpb25zOiBbSGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlXSxcclxuICBwcm92aWRlcnM6IFtcclxuICAgIEFjY291bnRTZXJ2aWNlLFxyXG4gICAgUHJpbmNpcGFsLFxyXG4gICAgU25hY2tCYXJTZXJ2aWNlLFxyXG4gICAgZm9yd2FyZFJlZigoKSA9PiBBd3NTM1VwbG9hZFNlcnZpY2UpXHJcbiAgXSxcclxuICBleHBvcnRzOiBbSGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlXSxcclxuICBzY2hlbWFzOiBbQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNoYXJlZE1vZHVsZSB7XHJcbiAgcHVibGljIHN0YXRpYyBmb3JSb290KGVudmlyb25tZW50OiBhbnksIGVuZ2xpc2g6IGFueSk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IFNoYXJlZE1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgQWNjb3VudFNlcnZpY2UsXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvdmlkZTogXCJlbnZcIiwgLy8geW91IGNhbiBhbHNvIHVzZSBJbmplY3Rpb25Ub2tlblxyXG4gICAgICAgICAgdXNlVmFsdWU6IGVudmlyb25tZW50XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBcImVudlwiLCAvLyB5b3UgY2FuIGFsc28gdXNlIEluamVjdGlvblRva2VuXHJcbiAgICAgICAgICB1c2VWYWx1ZTogZW5nbGlzaFxyXG4gICAgICAgIH1cclxuICAgICAgXVxyXG4gICAgfTtcclxuICB9XHJcbn1cclxuIl19