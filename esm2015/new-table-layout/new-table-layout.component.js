import { Component, ViewChild, Input, Output, EventEmitter, Inject, ChangeDetectorRef } from "@angular/core";
import { MatPaginator, MatTableDataSource, MatDialog, MatCheckbox, MatTable } from "@angular/material";
import { SnackBarService } from "./../shared/snackbar.service";
import * as FileSaver from "file-saver";
import { FormControl } from "@angular/forms";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import * as _ from "lodash";
// require('jquery')
// require('kendoGrid')
// /<reference path="../jquery/index" />
// import $ from "../JQuery";
import { ContentService } from "../content/content.service";
import { MessageService } from "../_services/index";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from '../loader.service';
import { process } from '@progress/kendo-data-query';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
// declare  $: any;
// declare var $ : JQueryStatic;
export class NewTableLayoutComponent {
    constructor(_fuseTranslationLoaderService, contentService, messageService, _matDialog, snackBarService, changeDetectorRef, environment, english, loaderService) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.contentService = contentService;
        this.messageService = messageService;
        this._matDialog = _matDialog;
        this.snackBarService = snackBarService;
        this.changeDetectorRef = changeDetectorRef;
        this.environment = environment;
        this.english = english;
        this.loaderService = loaderService;
        this.disablePagination = false;
        this.checkClickEventMessage = new EventEmitter();
        this.actionClickEvent = new EventEmitter();
        this.enableSelectOption = false;
        this.enableSearch = false;
        this.tableData = [];
        this.enableAction = false;
        this.limit = 10;
        this.offset = 0;
        this.selectedData = [];
        this.inputData = {};
        this.unsubscribe = new Subject();
        this.unsubscribeMsg = new Subject();
        this.unsubscribeModel = new Subject();
        this.currentFilteredValue = {};
        this.enableOptionFilter = false;
        this.enableUIfilter = false;
        this.enableFilter = false;
        this.groupField = [];
        this.selectAllOption = "none";
        this.enableDelete = false;
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.notifyId = {};
        this.enableGrouping = false;
        this.selectAllItem = "unchecked";
        // selectOption: any[] = [{ Key: 'All', Name: 'All', Value: 'All', Type: 'String'},
        // { Key: 'Read', Name: 'Read', Value: 'read', Type: 'String'},
        // { Key: 'Unread', Name: 'Unread', Value: 'unread', Type: 'String'},
        // { Key: 'None', Name: 'None', Value: 'None', Type: 'String'}  ];
        this.selectOption = [{ "value": "all", "name": "All" },
            { "value": true, "name": "Read" },
            { "value": false, "name": "Unread" },
            { "value": "none", "name": "None" }];
        this.redirectUri = this.environment.redirectUri;
        this._fuseTranslationLoaderService.loadTranslations(english);
        this.messageService.modelCloseMessage
            .pipe(takeUntil(this.unsubscribeModel))
            .subscribe(data => {
            console.log(data, ">>>>data");
            this.selectedData = [];
            if (data != 0) {
                this.inputData["selectedData"] = [];
                this.inputData["selectedIdList"] = [];
                this.inputData["selectAll"] = false;
            }
            this.messageService.sendTriggerNotification({ data: "trigger" });
            this.messageService.sendDatasource("null");
            console.log(this.tableData);
            console.log(this.dataSource);
            // _.forEach(this.tableData, function(item) {
            //   item.checked = false;
            // });
            // this.dataSource.data.map(obj => {
            //   obj.checked = false;
            // });
            // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
            // // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            // this.checkClickEventMessage.emit("clicked");
            this.data = data;
            // if(this.data === 'listView'){
            if (this.data) {
                this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
                this.ngOnInit();
            }
            // }
        });
        this.messageService.getMessage().pipe(takeUntil(this.unsubscribeMsg)).subscribe(message => {
            console.log(message, ">>message");
            this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            console.log(this, ">>THIS");
            if (this.currentConfigData &&
                this.currentConfigData.listView &&
                this.currentConfigData.listView.enableNewTableLayout) {
                this.ngOnInit();
            }
        });
        this.messageService
            .getTableHeaderUpdate()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
            console.log(data, ">>>>>data");
            if (data.data == 'update') {
                this.updateTableView();
            }
            else {
                console.log("FILTERRRR");
                this.enableFilter = !this.enableFilter;
            }
        });
    }
    ngOnInit() {
        // this.changeDetectorRef.detectChanges();
        this.inputData["selectAll"] = false;
        this.selectAllValue = "";
        this.enableDelete = false;
        this.selectAllItem = 'unchecked';
        console.log(this, "......table this");
        this.defaultDatasource = localStorage.getItem("datasource");
        this.realm = localStorage.getItem("realm");
        this.queryParams = {
            offset: 0,
            limit: 10,
            datasource: this.defaultDatasource,
            realm: this.realm
        };
        this.currentTenant = {
            realm: this.realm,
            userId: localStorage.getItem("userId")
        };
        //  this.paginator["pageIndex"] = 0;
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        console.log(">>> this.data ", this.data);
        if (this.data == 'listView' || this.data == 'refreshPage' || this.data === 0) {
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.selectedData = [];
        }
        this.inputData["datasourceId"] = this.defaultDatasource;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        // this.changeDetectorRef.detectChanges();
        this.fromFieldValue = new FormControl([""]);
        this.toFieldValue = new FormControl([""]);
        this.onLoadData(null);
    }
    ngAfterViewInit() {
        console.log("ngAfterViewInit ");
        console.log(this.selectedPaginator, "selectedPag>>>>");
        if (this.dataSource && this.selectedPaginator) {
            this.dataSource.paginator = this.selectedPaginator;
        }
    }
    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribeMsg.next();
        this.unsubscribeModel.next();
    }
    groupChange(event) {
        console.log(event, ">>>>> GROUP EVENT");
        this.groupField = event;
        console.log(this, ">>>> THIS");
        let apiFunctionDetails = this.currentTableData.onGroupingFunction;
        let self = this;
        _.forEach(apiFunctionDetails.requestData, function (requestItem) {
            if (requestItem.fromCurrentData) {
                self.queryParams[requestItem.name] = self.currentData[requestItem.value];
            }
            else if (requestItem.directAssign) {
                self.queryParams[requestItem.name] = requestItem.convertToString
                    ? JSON.stringify(requestItem.value)
                    : requestItem.value;
            }
            else {
                self.queryParams[requestItem.name] = requestItem.convertToString
                    ? JSON.stringify(self.inputData[requestItem.value])
                    : self.inputData[requestItem.value];
            }
        });
        _.forEach(event, function (eventItem) {
            self.queryParams[eventItem.field] = true;
        });
        this.contentService
            .getAllReponse(this.queryParams, apiFunctionDetails.apiUrl)
            .subscribe(data => {
            // if ( currentTableValue.onLoadFunction.enableLoader) {
            this.loaderService.stopLoader();
            //}
            this.enableSearch = this.currentData.enableGlobalSearch;
            let selectedTableHeaders = localStorage.getItem("selectedTableHeaders"); // after global setting change
            this.columns = !_.isEmpty(selectedTableHeaders) ? JSON.parse(selectedTableHeaders) : this.columns;
            this.displayedColumns = this.columns
                .filter(function (val) {
                return val.isActive;
            });
            let tempArray = [];
            var self = this;
            console.log(self, ">>>>>>>>>>THISSSS");
            _.forEach(data.response[apiFunctionDetails.response], function (item, index) {
                if (typeof item !== "object") {
                    let tempObj = {};
                    // tempObj[keyToSet] = item;
                    tempArray.push(tempObj);
                }
                else {
                    let tempObj = {};
                    _.forEach(self.currentTableData.dataViewFormat, function (viewItem) {
                        if (viewItem.subkey) {
                            if (viewItem.assignFirstIndexval) {
                                let resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                tempObj[viewItem.name] = item[viewItem.value]
                                    ? resultValue
                                    : "";
                            }
                            else {
                                tempObj[viewItem.name] = item[viewItem.value]
                                    ? item[viewItem.value][viewItem.subkey]
                                    : "";
                            }
                        }
                        else if (viewItem.isCondition) {
                            tempObj[viewItem.name] = eval(viewItem.condition);
                        }
                        else if (viewItem.isAddDefaultValue) {
                            tempObj[viewItem.name] = viewItem.value;
                        }
                        else {
                            tempObj[viewItem.name] = item[viewItem.value];
                        }
                        if (self.currentTableData.loadLabelFromConfig) {
                            tempObj[viewItem.name] =
                                self.currentTableData.headerList[item[viewItem.value]];
                        }
                    });
                    let filterObj = {};
                    filterObj[self.currentTableData.filterKey] = item[self.currentTableData.dataFilterKey];
                    let selectedValue = _.find(self.inputData[self.currentTableData.selectedValuesKey], filterObj);
                    console.log(selectedValue, ">>>>>selectedValue");
                    if (selectedValue) {
                        item.checked = true;
                    }
                    tempObj = Object.assign({}, item, tempObj);
                    tempArray.push(tempObj);
                }
            });
            if (tempArray && tempArray.length) {
                this.tableData = tempArray;
            }
            else {
                this.tableData =
                    data && data.response && data.response[apiFunctionDetails.response]
                        ? (data.response[apiFunctionDetails.response])
                        : [];
            }
            if (data && data.response) {
                this.length = data.response.total;
            }
            this.constructGroupedData(data.response, apiFunctionDetails.response);
            // let concatData = this.tableData[0].data.concat(this.tableData[1].data)
            this.dataSource = new MatTableDataSource(this.tableData);
            this.dataSource.paginator = this.selectedPaginator; // newly added 
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            this.inputData["selectAll"] = false;
            var filteredKey = (this.currentFilteredValue.searchKey) ? this.currentFilteredValue.searchKey : '';
            _.forEach(self.columns, function (x) {
                if (filteredKey == x.value) {
                    self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                }
                else {
                    self.inputData[x.keyToSave] = null;
                }
            });
            // this.loadKendoGrid();
            // this.selectAllCheck({ checked: false });
            // this.selectAll = false;
            // console.log(this.selectAll, "SelectAll>>>>>>>");
            // this.inputData["selectedData"] = [];
            // this.inputData["selectedIdList"] = [];
            // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            // console.log(this.selectAll, "SelectAll>>>>>>>");
            // this.changeDetectorRef.detectChanges();
            this.checkClickEventMessage.emit("clicked");
        }, err => {
            this.loaderService.stopLoader();
            console.log(" Error ", err);
        });
    }
    constructGroupedData(response, responseName) {
        console.log(response, ">>response");
        console.log(">>>> curenttable Data ", this.currentTableData);
        this.tableData = [];
        let self = this;
        _.forEach(response[responseName], function (item) {
            _.forEach(item.data, function (dataItem, index) {
                let obj = dataItem;
                obj["controlName"] = response.accessControls[dataItem.accessControl].accessControlName;
                obj["riskRanking"] = response.accessControls[dataItem.accessControl].riskRanking;
                obj["controlType"] = response.accessControls[dataItem.accessControl].controlType;
                obj["controlDescription"] = response.accessControls[dataItem.accessControl].accessControlDescription;
                obj["entitlementName"] = response.accessGroups[dataItem.accessGroup].accessGroupName;
                obj["rulesetName"] = response.rulesets[response.accessControls[dataItem.accessControl].rulesetId].name;
                obj["datasourceName"] = response.datasources[dataItem.datasource].name;
                obj["statusClass"] = {
                    "Open": "label bg-intiated",
                    "Closed": "label bg-failed",
                    "Remediated": "label bg-failed",
                    "Authorized": "label bg-success",
                    "Approved": "label bg-success"
                };
                self.tableData.push(obj);
                if (item.data.length == 10 && index == '9') {
                    console.log("LOAD MORE");
                    console.log(self.tableData.length, ">>>>self.tableData.length");
                    console.log(index, ">>>> INDEX");
                    let tempobj = {};
                    tempobj["entitlementName"] = response.accessGroups[dataItem.accessGroup].accessGroupName;
                    tempobj["userName"] = dataItem.userName;
                    tempobj["roleName"] = dataItem.roleName;
                    tempobj['accessGroup'] = dataItem.accessGroup;
                    tempobj['accessControl'] = dataItem.accessControl;
                    tempobj["controlName"] = response.accessControls[dataItem.accessControl].accessControlName;
                    tempobj["riskRanking"] = response.accessControls[dataItem.accessControl].riskRanking;
                    tempobj["controlType"] = response.accessControls[dataItem.accessControl].controlType;
                    tempobj["controlDescription"] = response.accessControls[dataItem.accessControl].accessControlDescription;
                    tempobj["_id"] = response.accessControls[dataItem.accessControl]._id;
                    tempobj["enableLoad"] = true;
                    tempobj["statusClass"] = {
                        "Open": "label bg-intiated",
                        "Closed": "label bg-failed",
                        "Remediated": "label bg-failed",
                        "Authorized": "label bg-success",
                        "Approved": "label bg-success"
                    };
                    _.forEach(self.currentTableData.productKeys, function (viewItem) {
                        tempobj[viewItem.name] = dataItem[viewItem.value];
                    });
                    console.log(">>> tempobj ", tempobj);
                    self.tableData.push(tempobj);
                }
            });
        });
        this.newTableData = process(this.tableData, { group: this.groupField });
        this.newTableData.total = response.total;
        // this.notifycount = this.newTableData.total;
        // _.forEach(this.newTableData.data,function(itemVal, index){
        //   itemVal.collapseGroup(index.toString())
        // } )
        //   for (let m = 0; m < 5; m = m + 1) {
        //     this.newTableData.collapseGroup(m.toString());
        // }
        //   this.newTableData.find(".k-grouping-row").each(function () {
        //     this.newTableData.collapseGroup(this);
        // });
        console.log(this, ">>> THIS");
        console.log(this.newTableData, ">>> TABLE DAT NEW");
    }
    onRoutingClick(selectedRow) {
        if (this.currentData && this.currentData.enableRouting) {
            this.inputData = Object.assign({}, this.inputData, selectedRow);
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            this.messageService.sendRoutingMessage({
                selectedRow: selectedRow,
                hideView: this.currentData.hideViewLayouts
            });
        }
    }
    collapseGroups(grid) {
        this.newTableData.data.forEach((gr, idx) => grid.collapseGroup(idx.toString()));
    }
    loadMoreClick(row) {
        console.log(row, ">>>>>>> ROW");
        let apiFunctionDetails = this.currentTableData.onGroupingFunction;
        console.log(this.queryParams, ">>> QUERY PARAMS");
        console.log(this, ">>> THIS");
        let indexVal = -1;
        let queryObj = this.queryParams;
        let groupingField = this.currentTableData.groupingDetail;
        if (this.groupField && this.groupField.length == 1) {
            let self = this;
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) >= 0) {
                    if (groupItem.isId) {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.value];
                    }
                    else {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.name];
                    }
                    indexVal = _.findIndex(self.newTableData.data, { value: row[groupItem.name] });
                }
            });
            queryObj.loadMoreOffset = (this.newTableData.data[indexVal].items.length > 10) ? this.newTableData.data[indexVal].items.length - 1 : 10;
            this.contentService
                .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
                .subscribe(data => {
                console.log(data, ">>>data");
                let responseData = data.response;
                let controlItemValue = this.newTableData.data[indexVal].items;
                controlItemValue.pop();
                if (responseData[apiFunctionDetails.response]) {
                    _.forEach(responseData[apiFunctionDetails.response][0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        let obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                        obj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        obj["statusClass"] = {
                            "Open": "label bg-intiated",
                            "Closed": "label bg-failed",
                            "Remediated": "label bg-failed",
                            "Authorized": "label bg-success",
                            "Approved": "label bg-success"
                        };
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            let tempobj = {};
                            tempobj['accessGroup'] = dataItem.accessGroup;
                            tempobj['accessControl'] = dataItem.accessControl;
                            tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                            tempobj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                            tempobj["roleName"] = dataItem.roleName;
                            tempobj["entitlementName"] = dataItem.entitlementName;
                            tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj["enableLoad"] = true;
                            tempobj["statusClass"] = {
                                "Open": "label bg-intiated",
                                "Closed": "label bg-failed",
                                "Remediated": "label bg-failed",
                                "Authorized": "label bg-success",
                                "Approved": "label bg-success"
                            };
                            _.forEach(self.currentTableData.productKeys, function (viewItem) {
                                tempobj[viewItem.name] = dataItem[viewItem.value];
                            });
                            controlItemValue.push(tempobj);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                this.newTableData.data[indexVal].items = controlItemValue;
            }, err => {
                this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
        else if (this.groupField && this.groupField.length == 2) {
            let self = this;
            let firstIndexVal = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) >= 0) {
                    if (groupItem.isId) {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.value];
                    }
                    else {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.name];
                    }
                }
            });
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) == 0) {
                    firstIndexVal = _.findIndex(self.newTableData.data, { value: row[groupItem.name] });
                }
            });
            let secondIndexVal = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) == 1) {
                    secondIndexVal = _.findIndex(self.newTableData.data[firstIndexVal].items, { value: row[groupItem.name] });
                }
            });
            console.log(firstIndexVal, ">>> firstIndexVal");
            console.log(secondIndexVal, ">>>>> secondIndexVal");
            queryObj.loadMoreOffset = (this.newTableData.data[firstIndexVal].items[secondIndexVal].items.length > 10) ? this.newTableData.data[firstIndexVal].items[secondIndexVal].items.length - 1 : 10;
            this.contentService
                .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
                .subscribe(data => {
                console.log(data, ">>>data");
                let responseData = data.response;
                let controlItemValue = this.newTableData.data[firstIndexVal].items[secondIndexVal].items;
                controlItemValue.pop();
                if (responseData[apiFunctionDetails.response]) {
                    _.forEach(responseData[apiFunctionDetails.response][0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        let obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                        obj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        obj["statusClass"] = {
                            "Open": "label bg-intiated",
                            "Closed": "label bg-failed",
                            "Remediated": "label bg-failed",
                            "Authorized": "label bg-success",
                            "Approved": "label bg-success"
                        };
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            let tempobj = {};
                            tempobj['accessGroup'] = dataItem.accessGroup;
                            tempobj['accessControl'] = dataItem.accessControl;
                            tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                            tempobj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                            tempobj["roleName"] = dataItem.roleName;
                            tempobj["userName"] = dataItem.userName;
                            tempobj["entitlementName"] = dataItem.entitlementName;
                            tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj["enableLoad"] = true;
                            _.forEach(self.currentTableData.productKeys, function (viewItem) {
                                tempobj[viewItem.name] = dataItem[viewItem.value];
                            });
                            tempobj["statusClass"] = {
                                "Open": "label bg-intiated",
                                "Closed": "label bg-failed",
                                "Remediated": "label bg-failed",
                                "Authorized": "label bg-success",
                                "Approved": "label bg-success"
                            };
                            controlItemValue.push(tempobj);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                this.newTableData.data[firstIndexVal].items[secondIndexVal].items = controlItemValue;
            }, err => {
                this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
        else if (this.groupField && this.groupField.length == 3) {
            let self = this;
            let firstIndexVal = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) >= 0) {
                    if (groupItem.isId) {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.value];
                    }
                    else {
                        queryObj['loadMore' + groupItem.name] = row[groupItem.name];
                    }
                }
            });
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) == 0) {
                    firstIndexVal = _.findIndex(self.newTableData.data, { value: row[groupItem.name] });
                }
            });
            let secondIndexVal = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) == 1) {
                    secondIndexVal = _.findIndex(self.newTableData.data[firstIndexVal].items, { value: row[groupItem.name] });
                }
            });
            let thirdIndexVal = -1;
            _.forEach(groupingField, function (groupItem) {
                if (self.groupField && _.findIndex(self.groupField, { field: groupItem.name }) == 2) {
                    thirdIndexVal = _.findIndex(self.newTableData.data[firstIndexVal].items[secondIndexVal].items, { value: row[groupItem.name] });
                }
            });
            console.log(firstIndexVal, ">>> firstIndexVal");
            console.log(secondIndexVal, ">>>>> secondIndexVal");
            console.log(thirdIndexVal, ">>>>> thirdIndexVal");
            queryObj.loadMoreOffset = (this.newTableData.data[firstIndexVal].items[secondIndexVal].items[thirdIndexVal].items.length > 10) ? this.newTableData.data[firstIndexVal].items[secondIndexVal].items[thirdIndexVal].items.length - 1 : 10;
            this.contentService
                .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
                .subscribe(data => {
                console.log(data, ">>>data");
                let responseData = data.response;
                let controlItemValue = this.newTableData.data[firstIndexVal].items[secondIndexVal].items[thirdIndexVal].items;
                controlItemValue.pop();
                if (responseData[apiFunctionDetails.response]) {
                    _.forEach(responseData[apiFunctionDetails.response][0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        let obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                        obj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        obj["statusClass"] = {
                            "Open": "label bg-intiated",
                            "Closed": "label bg-failed",
                            "Remediated": "label bg-failed",
                            "Authorized": "label bg-success",
                            "Approved": "label bg-success"
                        };
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            let tempobj = {};
                            tempobj['accessGroup'] = dataItem.accessGroup;
                            tempobj['accessControl'] = dataItem.accessControl;
                            tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj["controlType"] = responseData.accessControls[dataItem.accessControl].controlType;
                            tempobj["controlDescription"] = responseData.accessControls[dataItem.accessControl].accessControlDescription;
                            tempobj["roleName"] = dataItem.roleName;
                            tempobj["userName"] = dataItem.userName;
                            tempobj["entitlementName"] = dataItem.entitlementName;
                            tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj["enableLoad"] = true;
                            _.forEach(self.currentTableData.productKeys, function (viewItem) {
                                tempobj[viewItem.name] = dataItem[viewItem.value];
                            });
                            tempobj["statusClass"] = {
                                "Open": "label bg-intiated",
                                "Closed": "label bg-failed",
                                "Remediated": "label bg-failed",
                                "Authorized": "label bg-success",
                                "Approved": "label bg-success"
                            };
                            controlItemValue.push(tempobj);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                this.newTableData.data[firstIndexVal].items[secondIndexVal].items[thirdIndexVal].items = controlItemValue;
            }, err => {
                this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
    }
    loadMoreClick_backup(row) {
        console.log(row, ">>>>>>> ROW");
        let apiFunctionDetails = this.currentTableData.onGroupingFunction;
        console.log(this.queryParams, ">>> QUERY PARAMS");
        console.log(this, ">>> THIS");
        let indexVal = -1;
        let queryObj = this.queryParams;
        if (this.groupField && _.findIndex(this.groupField, { field: "controlName" }) >= 0) {
            queryObj.loadMoreControl = row.controlName;
            indexVal = _.findIndex(this.newTableData.data, { value: row.controlName });
        }
        if (this.groupField && _.findIndex(this.groupField, { field: "userName" }) >= 0) {
            queryObj.loadMoreUserName = row.userName;
            indexVal = _.findIndex(this.newTableData.data, { value: row.userName });
        }
        if (this.groupField && _.findIndex(this.groupField, { field: "entitlementName" }) >= 0) {
            queryObj.loadMoreGroupName = row.accessGroup;
            indexVal = _.findIndex(this.newTableData.data, { value: row.entitlementName });
        }
        if (this.groupField && _.findIndex(this.groupField, { field: "roleName" }) >= 0) {
            queryObj.loadMoreRespName = row.roleName;
            indexVal = _.findIndex(this.newTableData.data, { value: row.roleName });
        }
        console.log(indexVal, ">>>>> indexVal");
        queryObj.loadMoreOffset = (this.newTableData.data[indexVal].items.length > 10) ? this.newTableData.data[indexVal].items.length - 1 : 10;
        console.log(queryObj, ">>>> queery Obj");
        this.contentService
            .getAllReponse(queryObj, apiFunctionDetails.apiUrl)
            .subscribe(data => {
            console.log(data, ">>>data");
            let responseData = data.response;
            // if(_.findIndex(this.groupField,{field : 'userName'}) >= 0){
            // }else{
            // }
            if (this.groupField && this.groupField.length == 1) {
                let controlItemValue = this.newTableData.data[indexVal].items;
                controlItemValue.pop();
                if (responseData.userConflicts) {
                    _.forEach(responseData.userConflicts[0].data, function (dataItem, index) {
                        console.log(index, ">>>index");
                        let obj = dataItem;
                        obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                        obj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                        obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                        obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                        obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                        controlItemValue.push(obj);
                        console.log(controlItemValue.length, ">>>> controlItemValue.length");
                        if (controlItemValue.length % 10 == 0 && index == '9') {
                            console.log("LOAD MORE");
                            let tempobj = {};
                            tempobj['accessGroup'] = dataItem.accessGroup;
                            tempobj['accessControl'] = dataItem.accessControl;
                            tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                            tempobj["riskRanking"] = responseData.accessControls[dataItem.accessControl].riskRanking;
                            tempobj["roleName"] = dataItem.roleName;
                            tempobj["entitlementName"] = dataItem.entitlementName;
                            tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                            tempobj["enableLoad"] = true;
                            controlItemValue.push(tempobj);
                        }
                    });
                }
                console.log(controlItemValue, ">>controlItemValue");
                this.newTableData.data[indexVal].items = controlItemValue;
            }
            else if (this.groupField && this.groupField.length == 2) {
                console.log(this, ">>>> THIS");
                console.log(responseData, ">>>>responseData");
                let controlItemValue = this.newTableData.data[indexVal].items;
                let secondValueIndex = _.findIndex(controlItemValue, { value: row.userName });
                let secondGroupVal = controlItemValue[secondValueIndex].items;
                // console.log(secondGroupVal,">>> GROUP VAL")
                secondGroupVal.pop();
                // if(responseData.userConflicts){
                //   _.forEach(responseData.userConflicts[0].data, function(dataItem, index){
                //     console.log(index,">>>index")
                //     let obj = dataItem;
                //       obj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                //       obj["entitlementName"] = responseData.accessGroups[dataItem.accessGroup].accessGroupName;
                //       obj["rulesetName"] = responseData.rulesets[responseData.accessControls[dataItem.accessControl].rulesetId].name;
                //       obj["datasourceName"] = responseData.datasources[dataItem.datasource].name;
                //       controlItemValue.push(obj);
                //       console.log(controlItemValue.length,">>>> controlItemValue.length")
                //       if(controlItemValue.length % 10 == 0 && index == '9'){
                //         console.log("LOAD MORE")
                //         let tempobj={};
                //         tempobj["controlName"] = responseData.accessControls[dataItem.accessControl].accessControlName;
                //         tempobj["roleName"] = dataItem.roleName;
                //         tempobj["entitlementName"] = dataItem.entitlementName;
                //         tempobj["_id"] = responseData.accessControls[dataItem.accessControl]._id;
                //         tempobj["enableLoad"] = true;
                //         controlItemValue.push(tempobj);
                //   }
                //   });
                // }
                // console.log(controlItemValue,">>controlItemValue");
                // this.newTableData.data[indexVal].items = controlItemValue;
            }
            //   // constructGroupedDa/ta(data, )
        }, err => {
            this.loaderService.stopLoader();
            console.log(" Error ", err);
        });
    }
    groupCollapse(event) {
        console.log(event, ">>> EVNT VAL");
    }
    updateTableView() {
        if (this.currentTableData && this.currentTableData.tableHeader) {
            let currentTableHeader = JSON.parse(localStorage.getItem("selectedTableHeaders"));
            let tableHeaderLength = this.currentTableData.tableHeader.length;
            // this.columns = currentTableHeader;
            this.displayedColumns = [];
            if (_.findIndex(this.currentTableData.tableHeader, { value: "select" }) > -1) {
                this.displayedColumns.push(this.currentTableData.tableHeader[0]);
            }
            let filterArray = currentTableHeader
                .filter(function (val) {
                return val.isActive;
            });
            this.displayedColumns = _.concat(this.displayedColumns, filterArray);
            if (_.findIndex(this.currentTableData.tableHeader, { value: "action" }) > -1) {
                this.displayedColumns.push(this.currentTableData.tableHeader[tableHeaderLength - 1]);
            }
        }
    }
    onLoadData(enableNext) {
        if (this.data == 'refreshPage') {
            this.queryParams["offset"] = 0;
            this.queryParams["limit"] = 10;
            this.offset = 0;
        }
        this.currentFilteredValue = {};
        this.columns = [];
        this.displayedColumns = [];
        let tableArray = this.onLoad
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        let responseKey = this.onLoad && this.onLoad.responseKey ? this.onLoad.responseKey : null;
        let index = _.findIndex(tableArray, { tableId: this.tableId });
        if (index > -1)
            this.currentTableData = tableArray[index];
        console.log(tableArray, ".....tableArray");
        console.log(this.currentTableData, ".....currentTableData");
        let checkOptionFilter = (this.currentTableData && this.currentTableData.enableOptionsFilter) ? true : false;
        if (this.currentTableData && this.currentTableData.enableMultiSelect) {
            this.enableOptionFilter = true;
        }
        if (checkOptionFilter) {
            var self = this;
            this.enableOptionFilter = true;
            let Optiondata = this.currentTableData.filterOptionLoadFunction;
            this.optionFilterFields = this.currentTableData.optionFields;
            var apiUrl = Optiondata.apiUrl;
            var responseName = Optiondata.response;
            var self = this;
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(res => {
                let tempArray = (res.response[responseName]) ? res.response[responseName] : [];
                self.optionFilterdata = tempArray;
            });
        }
        if (this.currentTableData && this.currentTableData.enableUifilter) {
            let filterKey = this.currentTableData.uiFilterKey;
            this.enableUIfilter = true;
            // this.dataSource.filterPredicate = (data: Element, filter: string) => {
            //   return data[filterKey] == filter;
            //  };
        }
        if (this.onLoad && this.onLoad.response && !enableNext) {
            this.getLoadData(this.currentTableData, this.onLoad.response, this.onLoad.total, responseKey);
        }
        else {
            if (this.currentTableData)
                this.getData(this.currentTableData, null);
        }
        if (!enableNext) {
            // this.paginator.firstPage();
        }
    }
    getData(currentTableValue, enableTableSearch) {
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.columns = currentTableValue.tableHeader;
        if (currentTableValue.onLoadFunction) {
            var apiUrl = currentTableValue.onLoadFunction.apiUrl;
            var responseName = currentTableValue.onLoadFunction.response;
            let keyToSet = currentTableValue.onLoadFunction.keyForStringResponse;
            this.currentData = this.currentConfigData["listView"];
            var includeRequest = currentTableValue.onLoadFunction.includerequestData ? true : false;
            //if ( currentTableValue.onLoadFunction.enableLoader) {
            this.loaderService.startLoader();
            //}
            let dynamicLoad = currentTableValue.onLoadFunction.dynamicReportLoad; // condition checked when implement additional report view
            let requestedQuery = currentTableValue.onLoadFunction.requestData;
            var self = this;
            var requestQueryParam = {};
            _.forEach(this.groupField, function (eventItem) {
                self.queryParams[eventItem.field] = true;
            });
            if (dynamicLoad) {
                let tempObj = localStorage.getItem("CurrentReportData");
                let reportITem = JSON.parse(tempObj);
                _.forEach(requestedQuery, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else {
                        requestQueryParam[requestItem.name] = (reportITem[requestItem.value]) ? reportITem[requestItem.value] : requestItem.value;
                    }
                });
            }
            else {
                if (!enableTableSearch) {
                    _.forEach(requestedQuery, function (requestItem) {
                        //  requestData[item.name] = item.subKey
                        //  ? details[item.value][item.subKey]
                        //  : details[item.value];
                        if (requestItem.fromCurrentData) {
                            requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                        }
                        else if (requestItem.directAssign) {
                            self.queryParams[requestItem.name] = requestItem.convertToString
                                ? JSON.stringify(requestItem.value)
                                : requestItem.value;
                        }
                        else {
                            let existCheck = _.has(self.inputData, requestItem.value);
                            //  tempObj[viewItem.name] = item[viewItem.value] ? item[viewItem.value][viewItem.subkey] : "";
                            if (existCheck) {
                                let tempData = requestItem.subKey
                                    ? self.inputData[requestItem.value][requestItem.subKey]
                                    : self.inputData[requestItem.value];
                                self.queryParams[requestItem.name] = requestItem.convertToString
                                    ? JSON.stringify(tempData)
                                    : tempData;
                            }
                        }
                    });
                }
            }
            this.queryParams["disableGroup"] = (this.groupField && this.groupField.length) ? false : true;
            this.queryParams = Object.assign({}, this.queryParams, requestQueryParam);
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(data => {
                // if ( currentTableValue.onLoadFunction.enableLoader) {
                this.loaderService.stopLoader();
                //}
                let selectedTableHeaders = localStorage.getItem("selectedTableHeaders"); // after global setting change
                this.columns = !_.isEmpty(selectedTableHeaders) ? JSON.parse(selectedTableHeaders) : this.columns;
                this.enableSearch = this.currentData.enableGlobalSearch;
                this.displayedColumns = this.columns
                    .filter(function (val) {
                    return val.isActive;
                });
                console.log(">>  this.displayedColumn ", this.displayedColumns);
                let tempArray = [];
                var self = this;
                console.log(self, ">>>>>>>>>>THISSSS");
                console.log("Data Response *****", data.response);
                console.log("Data Response Name *****", responseName);
                console.log(currentTableValue, "Curr Table data >>>>.");
                _.forEach(data.response[responseName], function (item, index) {
                    console.log("Item Data >>>>>>", item);
                    if (typeof item !== "object") {
                        let tempObj = {};
                        tempObj[keyToSet] = item;
                        tempArray.push(tempObj);
                    }
                    else {
                        // if (self.tableId == "notifications") {
                        //   item.feature = self._fuseTranslationLoaderService.instant(
                        //     "NOTIFICATION_FORMATS.FEATURE." + item.messageObj.feature);
                        //   let operationStatus = item.messageObj.action + "  " + item.status;
                        //   console.log("Operation Data", operationStatus);
                        //   item.status_action = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION_STATUS." + operationStatus);
                        //   item.msg = item.feature + "   " + item.status_action;
                        //   console.log("Entry of Condition item");
                        // }
                        let tempObj = {};
                        console.log("********** Item Data **************", item);
                        _.forEach(currentTableValue.dataViewFormat, function (viewItem) {
                            console.log("View Item of Dataview Format >>>>", viewItem);
                            if (viewItem.subkey) {
                                if (viewItem.assignFirstIndexval) {
                                    let resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                    tempObj[viewItem.name] = item[viewItem.value]
                                        ? resultValue
                                        : "";
                                }
                                else {
                                    tempObj[viewItem.name] = item[viewItem.value]
                                        ? item[viewItem.value][viewItem.subkey]
                                        : "";
                                }
                            }
                            else if (viewItem.isCondition) {
                                tempObj[viewItem.name] = eval(viewItem.condition);
                            }
                            else if (viewItem.isAddDefaultValue) {
                                tempObj[viewItem.name] = viewItem.value;
                            }
                            else {
                                tempObj[viewItem.name] = item[viewItem.value];
                            }
                            if (viewItem.isNotify) {
                                // tempObj[viewItem.name] = eval(viewItem.condition);
                                let feat = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.FEATURE." + (item[viewItem.value].feature));
                                let operationStatus = item[viewItem.value].action + "_" + item[viewItem.value].status;
                                // tslint:disable-next-line:max-line-length
                                let status_action = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION_STATUS." + operationStatus);
                                let message = feat + " " + status_action;
                                tempObj[viewItem.name] = message;
                                tempObj["featureName"] = feat;
                                tempObj["operationName"] = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION." + item[viewItem.value].action);
                                tempObj["typeName"] = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.TYPE." + item[viewItem.value].type);
                            }
                            if (currentTableValue.loadLabelFromConfig) {
                                tempObj[viewItem.name] =
                                    currentTableValue.headerList[item[viewItem.value]];
                            }
                        });
                        let filterObj = {};
                        filterObj[currentTableValue.filterKey] = item[currentTableValue.dataFilterKey];
                        let selectedValue = _.find(self.inputData[currentTableValue.selectedValuesKey], filterObj);
                        console.log(selectedValue, ">>>>>selectedValue");
                        if (selectedValue) {
                            item.checked = true;
                        }
                        tempObj = Object.assign({}, item, tempObj);
                        tempArray.push(tempObj);
                    }
                });
                console.log("", tempArray);
                if (tempArray && tempArray.length) {
                    this.tableData = tempArray;
                }
                else {
                    this.tableData =
                        data && data.response && data.response[responseName]
                            ? (data.response[responseName])
                            : [];
                }
                if (this.groupField && this.groupField.length) {
                    this.constructGroupedData(data.response, responseName);
                }
                if (data && data.response) {
                    this.length = data.response.total;
                }
                console.log(">>>(this.tableData ", this.tableData);
                this.inputData["selectedData"] = (this.inputData["selectedData"] && this.inputData["selectedData"].length > 0) ? (this.inputData["selectedData"]) : [];
                this.inputData["selectedIdList"] = (this.inputData["selectedIdList"] && this.inputData["selectedIdList"].length > 0) ? (this.inputData["selectedIdList"]) : [];
                this.selectedData = this.inputData["selectedIdList"];
                this.inputData["selectAll"] = (this.selectedData.length >= this.limit) ? true : false;
                this.newTableData = process(this.tableData, { group: this.groupField });
                this.newTableData.total = this.length;
                this.dataSource = new MatTableDataSource(this.tableData);
                this.dataSource.paginator = this.selectedPaginator; // newly added 
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                // this.inputData["selectAll"] = false;
                var filteredKey = (this.currentFilteredValue.searchKey) ? this.currentFilteredValue.searchKey : '';
                _.forEach(self.columns, function (x) {
                    if (filteredKey == x.value) {
                        self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                    }
                    else {
                        self.inputData[x.keyToSave] = null;
                    }
                });
                // this.loadKendoGrid();
                // this.selectAllCheck({ checked: false });
                // this.selectAll = false;
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.inputData["selectedData"] = [];
                // this.inputData["selectedIdList"] = [];
                // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.changeDetectorRef.detectChanges();
                this.checkClickEventMessage.emit("clicked");
            }, err => {
                this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
    }
    //   loadKendoGrid(){
    //     $(document).ready(function() {
    //       (<any>$("#grid")).kendoGrid({
    //           dataSource: {
    //               pageSize: 10,
    //               transport: {
    //                   read: {
    //                       url: "https://demos.telerik.com/kendo-ui/service/Products",
    //                       dataType: "jsonp"
    //                   }
    //               },
    //               schema: {
    //                   model: {
    //                       id: "ProductID"
    //                   }
    //               }
    //           },
    //           pageable: true,
    //           scrollable: false,
    //           persistSelection: true,
    //           sortable: true,
    //           // change: onChange,
    //           columns: [{
    //                   selectable: true,
    //                   width: "50px"
    //               },
    //               {
    //                   field: "ProductName",
    //                   title: "Product Name"
    //               },
    //               {
    //                   field: "UnitPrice",
    //                   title: "Unit Price",
    //                   format: "{0:c}"
    //               },
    //               {
    //                   field: "UnitsInStock",
    //                   title: "Units In Stock"
    //               },
    //               {
    //                   field: "Discontinued"
    //               }
    //           ]
    //       });
    //       var grid = $("#grid").data("kendoGrid");
    //       // grid.thead.on("click", ".k-checkbox", onClick);
    //   });
    //       // //bind click event to the checkbox
    //       // grid.table.on("click", ".k-checkbox" , selectRow);
    //       // $('#header-chb').change(function(ev){
    //       //   var checked = ev.target.checked;
    //       //   $('.k-checkbox').each(function(idx, item){
    //       //     if(checked){
    //       //       if(!($(item).closest('tr').is('.k-state-selected'))){
    //       //         $(item).click();
    //       //       }
    //       //     } else {
    //       //       if($(item).closest('tr').is('.k-state-selected')){
    //       //         $(item).click();
    //       //       }
    //       //     }
    //       //   });
    //       // });
    //       // $("#showSelection").bind("click", function () {
    //       //   var checked = [];
    //       //   for(var i in checkedIds){
    //       //     if(checkedIds[i]){
    //       //       checked.push(i);
    //       //     }
    //       //   }
    //         // alert(checked);
    //       // });
    //     // });
    // }
    getLoadData(currentValue, responseValue, totalLength, responseKey) {
        console.log(currentValue, "....currentValue");
        let tempArray = [];
        // if (responseValue && responseValue[responseKey]) {
        responseValue = responseKey ? responseValue[responseKey] : responseValue;
        if (responseValue && responseValue.length) {
            _.forEach(responseValue, function (item, index) {
                if (typeof item !== "object") {
                    let tempObj = {};
                    tempObj[currentValue.keyToSet] = item;
                    tempArray.push(tempObj);
                }
                else {
                    let tempObj = {};
                    _.forEach(currentValue.dataViewFormat, function (viewItem) {
                        if (viewItem.subkey) {
                            if (viewItem.assignFirstIndexval) {
                                let resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                tempObj[viewItem.name] = item[viewItem.value]
                                    ? resultValue
                                    : "";
                            }
                            else {
                                tempObj[viewItem.name] = item[viewItem.value]
                                    ? item[viewItem.value][viewItem.subkey]
                                    : "";
                            }
                        }
                        else {
                            tempObj[viewItem.name] = item[viewItem.value];
                        }
                    });
                    tempObj = Object.assign({}, item, tempObj);
                    tempArray.push(tempObj);
                }
            });
        }
        this.columns = currentValue.tableHeader;
        responseValue = tempArray.length ? tempArray : responseValue;
        console.log(this.inputData, "...INPUTDATA");
        if (currentValue && currentValue.mapDataFunction) {
            let self = this;
            _.forEach(currentValue.mapDataFunction, function (mapItem) {
                _.forEach(mapItem.requestData, function (requestItem) {
                    self.queryParams[requestItem.name] = requestItem.convertToString
                        ? JSON.stringify(self.inputData[requestItem.value])
                        : self.inputData[requestItem.value];
                });
                self.contentService
                    .getAllReponse(self.queryParams, mapItem.apiUrl)
                    .subscribe(data => {
                    console.log(mapItem, ">>>>>>> MAP ITEM");
                    console.log(data, ".....DATAAAA");
                    let tempObj = _.keyBy(data.response[mapItem.response], "_id");
                    console.log(tempObj, "....tempObj");
                    _.forEach(responseValue, function (responseItem) {
                        if (data.response &&
                            data.response.keyForCheck == mapItem.keyForCheck &&
                            responseItem[mapItem.keyForCheck]) {
                            responseItem[mapItem.showKey] =
                                tempObj && responseItem[mapItem.keyForCheck]
                                    ? tempObj[responseItem[mapItem.keyForCheck]].objectTypes
                                    : [];
                            responseItem[mapItem.modelKey] = responseItem[mapItem.showKey];
                        }
                        else
                            console.log(responseItem, ".....responseItem");
                    });
                    console.log(responseValue, ">>>>>responseValue");
                });
            });
        }
        else {
            this.dataSource = new MatTableDataSource(responseValue);
            this.length = totalLength ? totalLength : responseValue.length;
        }
        this.tableData = tempArray;
        this.dataSource = new MatTableDataSource(responseValue);
        this.length = totalLength ? totalLength : responseValue.length;
        this.newTableData = process(this.tableData, { group: this.groupField });
        this.newTableData.total = this.length;
        console.log(this.columns, ">>>>>COLUMNSS");
        // this.displayedColumns = _.map(this.columns, "value");
        let selectedTableHeaders = localStorage.getItem("selectedTableHeaders"); // after global setting change
        this.columns = !_.isEmpty(selectedTableHeaders) ? JSON.parse(selectedTableHeaders) : this.columns;
        this.displayedColumns = this.columns
            .filter(function (val) {
            return val.isActive;
        });
        this.dataSource.paginator = this.selectedPaginator;
        console.log(this.selectedPaginator, ".................... selectedPaginator");
        console.log(this.dataSource, ".........DATASOURCEEEE");
        // this.dataSource = responseKey
        //   ? new MatTableDataSource(responseValue[responseKey])
        //   : new MatTableDataSource(responseValue);
    }
    updateCheckClick(selected, event) {
        // selected.type = this.currentTableData.tableType
        //   ? this.currentTableData.tableType
        //   : "";
        if (this.currentTableData && this.currentTableData.tableType) {
            selected.type = this.currentTableData.tableType;
        }
        // selected.typeId = this.currentTableData.typeId
        //   ? this.currentTableData.typeId
        //   : "";
        let securityTypeLength = this.currentTableData.securityTypeLength ? Number(this.currentTableData.securityTypeLength) : 3;
        console.log(">>> securityTypeLength ", securityTypeLength);
        // if (selected && selected.typeId) {
        //   if (selected.typeId == "2") {
        //     selected["security_type"] = [4];
        //     selected["level"] = [4];
        //   } else {
        //     selected["security_type"] = [1, 2, 3];
        //     selected["level"] = [1, 2, 3];
        //    }
        let tempArray = [];
        let predefinedArray = this.currentTableData.securityArrayValue ? JSON.parse("[" + this.currentTableData.securityArrayValue + "]") : tempArray;
        console.log(">>> predefinedArray ", predefinedArray);
        if (securityTypeLength && securityTypeLength > 0) {
            if (predefinedArray && predefinedArray.length) {
                tempArray = predefinedArray;
            }
            else {
                for (let i = 1; i <= securityTypeLength; i++) {
                    tempArray.push(i);
                }
            }
            selected["security_type"] = tempArray;
        }
        else {
            if (securityTypeLength == 0) {
                selected["security_type"] = tempArray;
            }
            else {
                selected["security_type"] = [1, 2, 3];
            }
        }
        //}
        console.log(">>>  selected security_type ", selected["security_type"]);
        if (this.currentTableData && this.currentTableData.dataFormatToSave) {
            _.forEach(this.currentTableData.dataFormatToSave, function (item) {
                selected[item.name] = selected[item.value];
            });
        }
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        let savedSelectedData = this.inputData.selectedData && this.inputData.selectedData.length
            ? this.inputData.selectedData
            : [];
        this.selectedData = _.merge(savedSelectedData, this.selectedData);
        if (event.checked) {
            this.selectedData.push(selected);
        }
        else {
            let index = this.selectedData.findIndex(obj => obj.object_name === selected.object_name);
            this.selectedData.splice(index, 1);
        }
        this.inputData["selectedData"] = this.selectedData;
        if (this.currentTableData && this.currentTableData.saveInArray) {
            let arrayObj = this.currentTableData.saveInArray;
            if (this.currentTableData.tableId === arrayObj.key) {
                let tempArray = this.selectedData || this.selectedData.length
                    ? _.map(this.selectedData, arrayObj.valueToMap)
                    : [];
                tempArray = tempArray.filter(function (element) {
                    return element != null;
                });
                this.inputData[arrayObj.key] = tempArray;
            }
        }
        this.inputData["selectedIdList"] = _.map(this.selectedData, "_id");
        console.log("Selected Id Data List >>>>>>>", this.inputData["selectedIdList"]);
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    }
    onSelectedKeysChange(e) {
        console.log(">>>> onSelectedKeysChange ", e);
        let len = this.selectedData.length;
        this.selectCount = this.selectedData.length;
        console.log(">>> len ", len, "total records ", this.newTableData.data.total, " this.length ", this.length, " this.limit ", this.limit, " >>> selectCount ", this.selectCount);
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        let selectedIds = e;
        if (len === 0) {
            this.selectAllItem = "unchecked";
            this.enableDelete = false;
            this.inputData["selectAll"] = false;
        }
        else if (len > 0 && len < this.length) {
            this.selectAllItem = "indeterminate";
            this.enableDelete = true;
            // this.inputData["selectAll"] = (this.newTableData.data.length === this.limit)?true:false;
            this.inputData["selectAll"] = (len >= this.limit) ? true : false;
        }
        else {
            this.selectAllItem = "checked";
            this.enableDelete = true;
            this.inputData["selectAll"] = true;
        }
        if (this.selectedData.length > 0) {
            // current selection
            let tempData = [];
            var self = this;
            _.forEach(selectedIds, function (tableItem) {
                let indexs = self.newTableData.data.filter(obj => obj["_id"] == tableItem);
                console.log(">>>> ndexs.length ", indexs.length);
                if (indexs.length > 0) {
                    tempData.push(indexs[0]);
                }
            });
            this.selectCount = tempData.length;
            this.inputData["selectedData"] = _.concat(this.inputData["selectedData"], tempData);
            this.inputData["selectedData"] = _.uniqBy(this.inputData["selectedData"], "_id");
            selectedIds = _.concat(this.selectedData, selectedIds); // total selected ids
        }
        this.selectedData = _.uniq(selectedIds);
        this.inputData["selectedIdList"] = this.selectedData;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    }
    onSelectAllChange(checkedState) {
        console.log("selectAll -> ", checkedState);
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        if (checkedState === 'checked') {
            this.newTableData.data.map(obj => {
                obj.checked = true;
            });
            this.inputData["selectedData"] = this.newTableData.data;
            this.inputData["selectedIdList"] = _.map(this.newTableData.data, "_id");
            this.inputData["selectAll"] = true;
            this.selectAllItem = 'checked';
            this.enableDelete = true;
        }
        else {
            this.selectedData = [];
            this.newTableData.data.map(obj => {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.inputData["selectAll"] = false;
            this.selectAllItem = 'unchecked';
            this.enableDelete = false;
        }
        let selectedIds = this.inputData["selectedIdList"];
        if (this.selectedData.length > 0) {
            selectedIds = _.concat(this.selectedData, selectedIds);
        }
        this.selectedData = _.uniq(selectedIds);
        this.selectCount = this.selectedData.length;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    }
    selectAllCheck(event) {
        console.log(event);
        if (event.checked === true) {
            this.dataSource.data.map(obj => {
                obj.checked = true;
            });
            this.inputData["selectedData"] = this.dataSource.data;
            this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
            console.log("Check Event All Id List >>>>>", this.inputData["selectedIdList"]);
            this.enableDelete = true;
            this.selectCount = this.inputData["selectedIdList"].length;
            this.selectedData.push(this.inputData["selectedIdList"]);
        }
        else {
            this.dataSource.data.map(obj => {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.selectedData = [];
            this.enableDelete = false;
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    }
    addFieldClick(item, objectList) {
        console.log(item, "..........ITEM");
        console.log(this.inputData, ".......INPUT DAta");
        console.log(objectList, "......ObjectList");
    }
    selectAllData() {
        console.log("Select all Data >>>>>");
        this.selectAllItem = 'checked';
        let requestDetails = this.currentData.selectAllRequest;
        let query = {};
        let self = this;
        _.forEach(requestDetails.requestData, function (item) {
            query[item.name] = self.inputData[item.value];
        });
        this.contentService
            .getAllReponse(query, requestDetails.apiUrl)
            .subscribe(data => {
            this.inputData["selectedIdList"] =
                data.response[requestDetails.responseName];
            this.selectedData = this.inputData["selectedIdList"];
            console.log("Selected Id list @@@@@@@@@", this.selectedData);
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        });
    }
    clearSelectAllData() {
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.inputData["selectedIdList"] = [];
        this.inputData["selectedData"] = [];
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.enableDelete = false;
        this.ngOnInit();
    }
    applyFilter(event) {
        console.log(">>>>>>>>>> event ", event);
        if (event && event.filters) {
            let filterValue = event.filters ? event.filters[0] : {};
            this.currentFilteredValue = {
                searchKey: filterValue.field,
                searchValue: filterValue.value
            };
            this.inputData[filterValue.field] = filterValue.value;
            console.log(this.inputData, ">>>>> INPUT DATA");
            let tableArray = this.onLoad && this.onLoad.tableData
                ? this.onLoad.tableData
                : this.currentConfigData["listView"].tableData;
            let index = _.findIndex(tableArray, { tableId: this.tableId });
            let searchRequest = tableArray[index].onTableSearch;
            let request = searchRequest.requestData;
            let isUiserach = (searchRequest.uiSearch) ? true : false;
            if (isUiserach) {
                this.dataSource.filter = event;
            }
            else {
                this.currentTableData = tableArray[index];
                let query = {
                    offset: 0,
                    limit: 10,
                    datasource: this.defaultDatasource
                };
                if (searchRequest && searchRequest.isSinglerequest) {
                    query[searchRequest.singleRequestKey] = event;
                }
                else {
                    var self = this;
                    _.forEach(request, function (item) {
                        let tempData = item.subKey
                            ? self.inputData[item.value][item.subKey]
                            : self.inputData[item.value];
                        if (item.directAssign || item.isDefault) {
                            self.queryParams[item.name] = item.convertToString
                                ? JSON.stringify(item.value)
                                : item.value;
                        }
                        else if (tempData) {
                            self.queryParams[item.name] = item.convertToString
                                ? JSON.stringify(tempData)
                                : tempData;
                        }
                    });
                }
                // this.queryParams = query;
                this.getData(this.currentTableData, "tableSearch");
            }
        }
    }
    filterReset(field) {
        delete this.queryParams[field.requestKey];
        this.ngOnInit();
    }
    onMultiSelect(event, rowVal) {
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        let selectedData = this.inputData.selectedData;
        let index = _.findIndex(selectedData, { obj_name: rowVal.obj_name });
        if (index > -1 && selectedData && selectedData.length) {
            this.inputData.selectedData[index].security_type = event.value;
            this.inputData.selectedData[index].level = event.value;
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    }
    changePage(event) {
        console.log(event, ">>> EVENT");
        // if (event.pageSize != this.limit) {
        //   this.queryParams["limit"] = event.pageSize;
        //   this.queryParams["offset"] = 0;
        // } else {
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
        // }
        this.onLoadData("next");
    }
    viewData(element, col) {
        console.log(col, "viewData >>>>>>>>> isRedirect ", col.isRedirect, this.environment);
        if (col.isRedirect) {
            let testStr = "/control-management/access-control";
            window.location.href = `${this.redirectUri}` + testStr;
            console.log("Redirected ********************");
            // this.route.navigateByUrl("/control-management/access-control");
        }
        else {
            this.actionClick(element, "view");
        }
    }
    addButtonClick(element, action) {
        console.log("addButtonClick ");
        console.log(" element ", element);
        console.log(" action ", action);
    }
    actionClick(element, action) {
        console.log(element, ">>>>element");
        if (action == "edit" || action == "view" || action == "visibility" || action == "info") {
            action = (action == 'visibility' || action == "info") ? 'view' : action;
            this.openDialog(element, action);
        }
        else if (action == "delete") {
            console.log(this, ">>>>>>>this");
            if (this.onLoad && this.onLoad.inputKey) {
                let inputKey = this.onLoad.inputKey;
                if (this.inputData && this.inputData[inputKey]) {
                    let selectedData = this.inputData[inputKey];
                    let index = selectedData.findIndex(obj => obj == element);
                    selectedData.splice(index, 1);
                    this.inputData[inputKey] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    // this.actionClickEvent.emit("clicked");
                }
            }
            else {
                if (this.inputData && this.inputData.selectedData) {
                    let selectedData = this.inputData.selectedData;
                    let index = selectedData.findIndex(obj => obj.object_name == element.object_name);
                    selectedData.splice(index, 1);
                    this.inputData["selectedData"] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    // this.actionClickEvent.emit("clicked");
                }
            }
        }
        else if (action == "remove_red_eye") {
            this.openDialog(element, "view");
        }
        else if (action == "restore_page") {
            this.openDialog(element, action);
        }
        else if (action == 'askdefaultbutton') {
            let dsid = element._id;
            let dsname = element.name;
            console.log(" dsid ", dsid, " dsname ", dsname);
            var self = this;
            let requestDetails = this.currentTableData;
            let queryObj = {
                defaultDatasource: true
            };
            let toastMessageDetails = requestDetails.setDefault.toastMessage;
            self.contentService
                .updateRequest(queryObj, requestDetails.setDefault.apiUrl, dsid)
                .subscribe(res => {
                this.messageService.sendDatasource(dsid);
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                this.ngOnInit();
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
            // this.messageService.sendDatasource(dsid);
            // this.snackBarService.add(
            //   this._fuseTranslationLoaderService.instant(
            //     "Datasource Default Updated Successfully!"
            //   )
            // );
            this.inputData.datasourceId = dsid;
        }
        else if (action == "save_alt") {
            let requestDetails = this.currentData.downloadRequest;
            let query = {
                reportId: element._id
            };
            this.contentService
                .getAllReponse(query, requestDetails.apiUrl)
                .subscribe(data => {
                const ab = new ArrayBuffer(data.data.length);
                const view = new Uint8Array(ab);
                for (let i = 0; i < data.data.length; i++) {
                    view[i] = data.data[i];
                }
                let downloadType = 'application/zip';
                const file = new Blob([ab], { type: downloadType });
                FileSaver.saveAs(file, element.fileName + '.zip');
            });
        }
        else if (action == "speaker_notes" || action == "speaker_notes_off") {
            const requestDetails = this.currentData.notificationRead;
            let idsList = [];
            idsList.push(element._id);
            // if (action == "notdelete") {
            // queryObj["delete"] = true;
            //  let queryObj = {
            //     isread: true,
            //     feature: "delete",
            //     idList: idsList
            //   }
            // }
            let queryObj = {
                isread: (action == "speaker_notes") ? true : false,
                feature: "isread",
                idList: idsList
            };
            var self = this;
            let toastMessageDetails = requestDetails.toastMessage;
            this.contentService.createRequest(queryObj, requestDetails.apiUrl)
                .subscribe(res => {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
            this.ngOnInit();
        }
        else if (action == "notdelete") {
            const requestDetails = this.currentData.notificationRead;
            let idsList = [];
            idsList.push(element._id);
            let queryObj = {
                feature: "delete",
                idList: idsList,
                isread: true
            };
            var self = this;
            let toastMessageDetails = requestDetails.toastMessage;
            this.contentService.createRequest(queryObj, requestDetails.apiUrl)
                .subscribe(res => {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
            this.ngOnInit();
        }
    }
    updateStatus(actionName, selectedId) {
        let query = {};
        if (this.currentTableData && this.currentTableData.statusUpdate) {
            var currentRequestDetails = this.currentTableData.statusUpdate;
            let toastMessageDetails = currentRequestDetails.toastMessage;
            query["updateId"] = selectedId._id;
            query["status"] = actionName;
            this.contentService
                .updateRequest(query, currentRequestDetails.apiUrl, selectedId._id)
                .subscribe(res => {
                this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                this.ngOnInit();
            }, error => {
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
        }
    }
    actionRedirect(element, columnData) {
        console.log("actionRedirect >>>>>> ", element);
        var self = this;
        let requestDetails = this.currentTableData;
        let queryObj = {};
        let toastMessageDetails = requestDetails.onTableUpdate.toastMessage;
        console.log("requestDetails >>>>>> ", requestDetails);
        if (columnData.rulesetCheck) {
            _.forEach(requestDetails.onTableUpdate.requestData, function (item) {
                queryObj[item.name] = item.subkey
                    ? element[item.value][item.subkey]
                    : element[item.value];
            });
        }
        else {
            queryObj = {
                defaultDatasource: true
            };
            console.log("data source >>>>");
        }
        self.contentService
            .updateRequest(queryObj, requestDetails.onTableUpdate.apiUrl, element._id)
            .subscribe(res => {
            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            if (!columnData.rulesetCheck) {
                this.messageService.sendDatasource(element._id);
            }
            this.ngOnInit();
        }, error => {
            self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
        });
        console.log('actionRedirect >>>>>>>>>>>>>>> queryObj ', queryObj);
        console.log("Query Obj >>>", queryObj);
        console.log("Update URL >>>", requestDetails.onTableUpdate.apiUrl);
    }
    onToggleChange(event, element) {
        console.log(">>>>>>>>>>>> event ", event, " :element ", element);
        element.status = event.checked ? "ACTIVE" : "INACTIVE";
        let requestDetails = this.currentTableData
            ? this.currentTableData.onToggleChange
            : "";
        if (requestDetails) {
            let toastMessageDetails = requestDetails.toastMessage;
            var self = this;
            let queryObj = {};
            _.forEach(requestDetails.requestData, function (item) {
                // queryObj[item.name] = item.subkey
                //   ? element[item.value][item.subkey]
                //   : element[item.value];
                if (item.subkey) {
                    queryObj[item.name] = element[item.value][item.subkey];
                }
                else if (item.fromTenantData) {
                    queryObj[item.name] = self.currentTenant[item.value];
                }
                else {
                    queryObj[item.name] = element[item.value];
                }
            });
            console.log('Toggle URL >>>', requestDetails.apiUrl);
            self.contentService
                .updateRequest(queryObj, requestDetails.apiUrl, element._id)
                .subscribe(res => {
                if (element.status == 'ACTIVE' && toastMessageDetails.enable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.enable));
                }
                else if (element.status == 'INACTIVE' && toastMessageDetails.disable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.disable));
                }
                else {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                }
                if (requestDetails.isPageRefresh) {
                    setTimeout(() => {
                        self.onLoadData(null);
                    }, 100);
                }
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
        }
        else if (this.currentTableData && this.currentTableData.saveselectedToggle) {
            this.inputData[this.currentTableData["keyTosave"]] = this.dataSource.data;
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    }
    checkConfirm(element, action, column) {
        this.openConfirmDialog(element, action, column);
    }
    openConfirmDialog(element, action, column) {
        console.log(" openConfirmDialog Dialog ********* element: ", element, " **** action ", action, ">>> column ", column);
        let modelWidth = this.currentConfigData[action].modelData.size;
        let modelData = this.currentConfigData[action].modelData;
        console.log(">>> modelWidth ", modelWidth);
        this.dialogRef = this._matDialog
            .open(ConfirmDialogComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                savedData: element,
                modelData: modelData
            }
        })
            .afterClosed()
            .subscribe(response => {
            console.log(">>>> Confirm modal response ", response);
            if (response && column.redirectAction) {
                this.openDialog(element, column.redirectAction);
            }
            else {
                localStorage.removeItem("currentInput");
                this.messageService.sendModelCloseEvent("listView");
            }
        });
    }
    openDialog(element, action) {
        console.log(" Open Dialog ********* element: ", element);
        let modelWidth = this.currentConfigData[action].modelData.size;
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                savedData: element
            }
        })
            .afterClosed()
            .subscribe(response => {
            localStorage.removeItem("currentInput");
            this.messageService.sendModelCloseEvent("listView");
        });
    }
    onSelectOptionbackup(item) {
        let self = this;
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        if (item.value == 'none') {
            _.forEach(self.newTableData.data, function (tableItem) {
                tableItem.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.inputData["selectAll"] = false;
            this.enableDelete = false;
        }
        else {
            if (item.value != 'all') {
                self.queryParams[item.name] = item.value;
                console.log("Select Value >>>>>>>>>", item);
                if (item.keyToShow == "Read") {
                    self.currentData.isRead = true;
                }
                else {
                    self.currentData.isRead = false;
                }
            }
            this.inputData["selectAll"] = true;
            this.enableDelete = true;
            this.getData(this.currentTableData, null);
        }
        console.log(">>> this.inputData ", this.inputData);
    }
    onSelectOption(item) {
        console.log("Select Value >>>>>>>>>", item);
        let existCheck = _.has(this.queryParams, item.name);
        if (item.alternativeKey == 'unread') {
            delete this.queryParams["unread"];
            // _.omit(this.queryParams, this.queryParams["unread"]);
        }
        else {
            delete this.queryParams["isread"];
            // _.omit(this.queryParams, this.queryParams["isread"]);
        }
        console.log("Query Params >>", this.queryParams);
        if (this.queryParams["operation"] != item.name) {
            this.selectedData = [];
            this.newTableData.data.map(obj => {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
            this.inputData["selectAll"] = false;
            this.selectAllItem = 'unchecked';
            this.enableDelete = false;
        }
        this.queryParams["operation"] = item.name;
        this.queryParams[item.name] = item.value;
        this.enableDelete = false;
        this.getData(this.currentTableData, null);
    }
    DeleteOption() {
        var self = this;
        let requestDetails = this.currentData.notificationRead;
        let toastMessageDetails = requestDetails.toastMessage;
        // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        const input = this.inputData["selectedIdList"];
        console.log("Input data >>>>", input);
        let queryObj = {
            feature: "delete",
            idList: input
        };
        if (input.length > 0) {
            this.contentService.createRequest(queryObj, requestDetails.apiUrl)
                .subscribe(res => {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                this.ngOnInit();
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                this.ngOnInit();
            });
        }
    }
    Read() {
        var self = this;
        // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        const requestDetails = this.currentData.notificationRead;
        const input = this.inputData["selectedIdList"];
        console.log("Input data >>>>", input);
        let queryObj = {
            isread: true,
            feature: "isread",
            idList: input
        };
        let toastMessageDetails = requestDetails.toastMessage;
        this.contentService.createRequest(queryObj, requestDetails.apiUrl)
            .subscribe(res => {
            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            this.ngOnInit();
        }, error => {
            self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            this.ngOnInit();
        });
    }
    Unread() {
        var self = this;
        // this.inputData["selectedIdList"]
        // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        const requestDetails = this.currentData.notificationRead;
        const input = this.inputData["selectedIdList"];
        console.log("Selected ID List of Input >>>", input);
        let toastMessageDetails = requestDetails.toastMessage;
        let queryObj = {
            isread: false,
            feature: "isread",
            idList: input
        };
        this.contentService.createRequest(queryObj, requestDetails.apiUrl)
            .subscribe(res => {
            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
            this.ngOnInit();
        }, error => {
            self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            this.ngOnInit();
        });
    }
}
NewTableLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "new-table-layout",
                template: "<div class=\"mat-elevation-z8 sen-margin-10\" [ngClass]=\"currentConfigData.listView.inTable\">\r\n  <!-- <button class=\"k-button\" (click)=\"collapseGroups(grid)\">Collapse All</button>\r\n      <br /><br /> -->\r\n  <!-- <ng-container *ngIf=\"currentData.enableMultiSelect\">\r\n      <div class=\"col-12 sent-lib-border-btm\">\r\n        <div class=\"row\">\r\n          <div class=\"col-4\">\r\n            <div class=\"btn-group\" dropdown >\r\n            <mat-checkbox class=\"sen-select-box\" (change)=\"selectAllCheck($event)\" >\r\n          </mat-checkbox>\r\n            <mat-form-field  class=\"btn btn-default\">  \r\n                <mat-select (selectionChange)=\"onSelectOption($event)\"\r\n                [(ngModel)]=\"selectAllValue\">\r\n                <mat-option *ngFor=\"let select of selectOption;\" [value]=\"select.value\">{{select.name}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </div>\r\n          <ng-container *ngIf=\"enableDelete == true\">\r\n            <span class=\"material-icons sen-lib-delete\" (click)=\"DeleteOption()\" matTooltip=\"Delete\"> delete </span>\r\n            <mat-icon class=\"material-icons sen-lib-delete\" (click)=\"Read()\" matTooltip=\"Mark as Read\">mark_chat_read\r\n            </mat-icon>\r\n            <mat-icon class=\"material-icons sen-lib-delete\" (click)=\"Unread()\" matTooltip=\"Mark as Unread\">\r\n              mark_chat_unread\r\n            </mat-icon>\r\n\r\n          </ng-container>\r\n\r\n        </div>\r\n        <div class=\"col-6\" *ngIf=\"enableDelete == true\">\r\n          <span class=\"sen-lib-iconslist-content\">\r\n            All {{selectCount}} notifications on this page are selected.\r\n          </span>\r\n          <span class=\"sen-conversation sen-lib-iconslist-content\" (change)=\"selectAllCheck($event)\"> Select all\r\n            {{length}} notifications</span>\r\n\r\n        </div>\r\n\r\n      </div>\r\n  </ng-container> \r\n\r\n\r\n-->\r\n  <!-- Started on 12-11-2020 optionfilter-->\r\n  <ng-container *ngIf=\"enableOptionFilter\">\r\n    <div class=\"col-12 sent-lib-border-btm\">\r\n      <div class=\"row\">\r\n        <div class=\"col-12\">\r\n          <div class=\"text-right\" style=\"margin-top:-5px;\">\r\n            <mat-form-field appearance=\"outline\" style=\"width: 18%;margin-top:12px;\">\r\n              <mat-label>Select Option</mat-label>\r\n              <mat-select [(ngModel)]=\"selectAllValue\">\r\n                <mat-option *ngFor=\"let item of currentData.selectOption\" (click)=\"onSelectOption(item)\"\r\n                  value=\"{{item.value | translate}}\">\r\n                  {{item.keyToShow | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <ng-container *ngIf=\"enableDelete == true\">\r\n              <!-- <span class=\"material-icons sen-lib-delete sen-lib-cursor\" (click)=\"DeleteOption()\" matTooltip=\"Delete\">\r\n                delete </span>\r\n              <span class=\"material-icons  sen-lib-chat-read sen-lib-cursor\" (click)=\"Read()\" matTooltip=\"Mark as Read\">\r\n                speaker_notes\r\n              </span>\r\n              <span class=\"material-icons  sen-lib-chat-unread sen-lib-cursor\" (click)=\"Unread()\"\r\n                matTooltip=\"Mark as Unread\">\r\n                speaker_notes_off </span> -->\r\n              <div class=\"btn-group sen-lib-btn-bg-group\" role=\"group\" aria-label=\"Basic example\">\r\n                <button type=\"button\" class=\"btn btn-border-right\" (click)=\"DeleteOption()\">\r\n                  <span class=\"material-icons sen-lib-delete sen-lib-cursor\">\r\n                    delete </span> <span class=\"sen-lib-grp-btn-font\">Delete</span>\r\n                </button>\r\n                <!-- *ngIf=\"(currentData.isRead == false)\" -->\r\n                <button type=\"button\" class=\"btn  \" (click)=\"Read()\">\r\n                  <span class=\"material-icons  sen-lib-chat-read sen-lib-cursor\">\r\n                    speaker_notes\r\n                  </span>\r\n                  <span class=\"sen-lib-grp-btn-font\"> Mark as Read</span>\r\n                </button>\r\n                <button type=\"button\" class=\"btn \" (click)=\"Unread()\">\r\n                  <span class=\"material-icons  sen-lib-chat-unread sen-lib-cursor\">\r\n                    speaker_notes_off </span> <span class=\"sen-lib-grp-btn-font\">Mark as Unread</span>\r\n                </button>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n  <!-- <p>Text: {{currentData.selectAllTex}} - selectAll: {{inputData.selectAll}} - length: {{length}} -idlistLength : {{inputData.selectedIdList.length}} - selectedData len :{{selectedData.length}}- {{selectCount}}</p>  -->\r\n  <ng-container\r\n    *ngIf=\"currentData && currentData.selectAllText && inputData.selectAll && selectedData.length > 9 && length > 10\">\r\n    <div class=\"row\">\r\n      <div class=\"col-12 sent-lib-border-btm\" style=\"text-align: center;\">\r\n        <ng-container>\r\n          <div *ngIf=\"selectedData.length != length\">\r\n            <span class=\"m-0\">{{selectedData.length}} {{currentData.afterSelectAllText | translate}}\r\n              <button mat-button color=\"accent\" (click)=\"selectAllData()\">Select All {{length}}\r\n                {{currentData.selectFileText}}</button>\r\n            </span>\r\n          </div>\r\n          <div *ngIf=\"selectedData.length == length\" class=\"sen-conversation\">\r\n            <span class=\"m-0\">\r\n              All {{length}} {{currentData.afterSelectAllText}}\r\n              <button mat-button color=\"accent\" (click)=\"clearSelectAllData()\">Clear Selection</button></span>\r\n          </div>\r\n        </ng-container>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n\r\n  <!-- Ended option filter -->\r\n  <kendo-grid #grid [data]=\"newTableData\" [pageSize]=\"limit\" [skip]=\"offset\" [pageable]=\"true\" [pageable]=\"{\r\n    buttonCount: 10,\r\n    info: info,\r\n    type: type,\r\n    pageSizes: pageSizes,\r\n    previousNext: previousNext\r\n  }\" [filterable]=\"enableFilter\" [sortable]=\"true\" [resizable]=\"true\" [group]=\"groupField\"\r\n    [selectable]=\"{  checkboxOnly: true }\" [kendoGridSelectBy]=\"'_id'\" [selectedKeys]=\"selectedData\"\r\n    (selectedKeysChange)=\"onSelectedKeysChange($event)\"\r\n    [groupable]=\"(currentTableData && currentTableData.enableGrouping) ? true : false\"\r\n    (filterChange)=\"applyFilter($event)\" (pageChange)=\"changePage($event)\" (groupChange)=\"groupChange($event)\"\r\n    (groupCollapse)=\"groupCollapse($event)\">\r\n    <kendo-grid-checkbox-column *ngIf=\"currentData && currentData.selectAllText\"\r\n      [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\" [width]=\"40\" title=\"Select\">\r\n      <ng-template kendoGridHeaderTemplate>\r\n        <span class=\"text-center sen-lib-bx\"> <input class=\" k-checkbox \" id=\"selectAllCheckboxId\"\r\n            kendoGridSelectAllCheckbox [state]=\"selectAllItem\" (selectAllChange)=\"onSelectAllChange($event)\" /></span>\r\n        <label class=\"k-checkbox-label\" for=\"selectAllCheckboxId\"></label>\r\n      </ng-template>\r\n      <ng-template kendoGridCellTemplate let-dataItem let-rowIndex=\"rowIndex\" >\r\n<div class=\"{{dataItem.status === 'INACTIVE' ? 'k-disabled' : ''}}\">\r\n  <input class=\"ui-lib-checkbox-align\" [kendoGridSelectionCheckbox]=\"rowIndex\" />\r\n</div>\r\n</ng-template>\r\n      <!-- <ng-template kendoGridCellTemplate let-dataItem>\r\n        {{dataItem | json}}\r\n      </ng-template> -->\r\n    </kendo-grid-checkbox-column>\r\n\r\n    <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n      <!-- <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'select' && enableOptionFilter\" title=\"{{column.name | translate}}\" [width]=\"column.width ? column.width:30\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <div class=\"text-center\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"updateCheckClick(dataItem, $event)\"\r\n              [checked]=\"dataItem.checked\">\r\n            </mat-checkbox>\r\n          </div>\r\n        </ng-template>  \r\n      </kendo-grid-column>  -->\r\n      <!-- <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'select'\" title=\"{{column.name | translate}}\" width=\"30\">\r\n      <span *ngIf=\"column.value == 'select'\">\r\n        <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"updateCheckClick(row, $event)\"\r\n          [checked]=\"row.checked\">\r\n        </mat-checkbox>\r\n      </span>\r\n      </kendo-grid-column> -->\r\n\r\n      <!-- <span class=\"view-mode\" (click)=\"viewData(dataItem,column)\">{{dataItem[column.value]}}</span> -->\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.type == 'date'\" field=\"{{column.value}}\" title=\"{{column.name | translate}}\" width=\"130\"\r\n        filter=\"date\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          {{dataItem[column.value] | date: column.dateFormat}}\r\n          <!-- {{dataItem[column.value]}} -->\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.mapFromConfig\" title=\"{{column.name | translate}}\" width=\"100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span>\r\n            {{column.mapData[dataItem[column.value]] | translate}}</span>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'action' || column.value == 'viewInfo'\" title=\"{{column.name | translate}}\" width=\"50\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <ng-container *ngFor=\"let button of column.buttons\">\r\n            <span style=\"cursor: pointer;\" (click)=\"actionClick(dataItem, button)\">\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button=='save_alt' && (dataItem.status=='Completed')\">{{button}}\r\n              </span>\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button=='speaker_notes_off' && (dataItem.isread == true)\">{{button}}\r\n              </span>\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button=='speaker_notes' && (dataItem.isread  ==  false)\">{{button}}\r\n              </span>\r\n              <span class=\"material-icons sen-lib-icon-size\"\r\n                *ngIf=\"button!='save_alt' && button !='speaker_notes_off' && button !='speaker_notes'\">{{button}}\r\n              </span>\r\n            </span>\r\n          </ng-container>\r\n\r\n          <!-- condition based button start 10-12-20 --->\r\n          <ng-container *ngIf=\"column.isCondition\">\r\n            <button style=\"width: 45% !important;\" *ngFor=\"let button of column.buttonsData\" mat-icon-button\r\n              color=\"accent\" (click)=\"actionClick(dataItem, button.value)\"\r\n              [disabled]=\"button.disableCheck && dataItem[button.disableName]\">\r\n              <span class=\"material-icons sen-lib-icon-size\">{{button.value | translate}}</span>\r\n            </button>\r\n          </ng-container>\r\n          <!-- condition based button end --->\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n\r\n\r\n      <!-- <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n          *ngIf=\"column.value == 'action'\" title=\"{{column.name | translate}}\" width=\"50\">\r\n          <ng-template kendoGridCellTemplate let-dataItem>\r\n            <button style=\"width: 45% !important;\" *ngFor=\"let button of column.buttons\" mat-icon-button color=\"accent\"\r\n              (click)=\"actionClick(dataItem, button)\">\r\n              <mat-icon *ngIf=\"dataItem.isread=='true'\">{{button}}\r\n              </mat-icon>\r\n              <mat-icon *ngIf=\"!dataItem.isread\">{{button}}</mat-icon>\r\n            </button>\r\n          </ng-template>\r\n        </kendo-grid-column> -->\r\n\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'reimport'\" title=\"{{column.name | translate}}\" width=\"100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span class=\"label bg-success ng-star-inserted reimport\"\r\n            *ngIf=\"!column.isCondition ||(column.isCondition && dataItem[column.value]) \"\r\n            (click)=\"actionClick(dataItem,'edit' )\">\r\n            Re-import\r\n          </span>\r\n\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value == 'confirmDialog'\" title=\"{{column.name | translate}}\" width=\"100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span class=\"label bg-success ng-star-inserted reimport\"\r\n            (click)=\"checkConfirm(dataItem,'confirmView',column )\">\r\n            {{column.keyToShow | translate}}\r\n          </span>\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n      <kendo-grid-column [headerStyle]=\"{'background-color': '#223664','color': '#fff','line-height': ''}\"\r\n        *ngIf=\"column.value != 'action' && !column.type && column.value != 'select' && column.value != 'reimport' &&  column.value != 'confirmDialog' && !column.mapFromConfig && !column.mapFromConfig && column.value != 'viewInfo'\"\r\n        field=\"{{column.value}}\" title=\"{{column.name | translate}}\" [width]=\"column.width ? column.width:100\">\r\n        <ng-template kendoGridCellTemplate let-dataItem>\r\n          <span\r\n            *ngIf=\"!dataItem.enableLoad && !column.showTooltip && !column.showStatusToggle && !column.isArryObj && !column.hyperLink\"\r\n            [ngClass]=\"(dataItem.statusClass && dataItem.statusClass[dataItem[column.value]] )? dataItem.statusClass[dataItem[column.value]] : (dataItem.isread == false) ? 'sen-lib-bold' : ''\">\r\n            {{dataItem[column.value]}}\r\n            <button *ngIf=\"column.isViewInfoIcon && dataItem.controlName == 'Multiple Controls'\"\r\n              style=\"width: 0% !important;\" mat-icon-button color=\"accent\" (click)=\"actionClick(dataItem, 'info')\">\r\n              <mat-icon> info\r\n              </mat-icon>\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.isButtonArray\">\r\n            <button *ngFor=\"let button of column.buttonArray\" [ngClass]=\"button.class ? button.class : ''\"\r\n              style=\"margin : 3px;\" (click)=\"updateStatus(button.name, dataItem)\">\r\n              {{button.name}}\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.showTooltip\">\r\n            <button style=\"width: 45% !important;\" [matTooltip]=\"dataItem[column.value]\" mat-icon-button color=\"accent\">\r\n              <mat-icon>{{column.iconName}} </mat-icon>\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.hyperLink\">\r\n            <a style=\"color: #039be5; cursor: pointer;\" (click)=\"onRoutingClick(dataItem)\">\r\n              {{column.hyperLinkLable |translate}}</a>\r\n          </span>\r\n          <!-- <span *ngIf=\"!dataItem.enableLoad && !column.showStatusToggle && !column.isArryObj\"\r\n          [ngClass]=\"(dataItem.statusClass && dataItem.statusClass[dataItem[column.value]] )? dataItem.statusClass[dataItem[column.value]] : ''\">\r\n          {{dataItem[column.value]}} -->\r\n          <!-- </span> -->\r\n          <span *ngIf=\"column.isArryObj\">\r\n            <ng-container *ngFor=\"let data of dataItem[column.keyToShow]; let i=index\">\r\n              {{data[column.subKey] | translate }}\r\n              <span *ngIf=\"dataItem[column.keyToShow].length-1>i\">\r\n                {{column[dataItem[column.keyToCheck]] || \",\"}}\r\n              </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"dataItem.enableLoad && dataItem[column.value] && column.enableLoad\">\r\n            <button mat-raised-button color=\"primary\" (click)=\"loadMoreClick(dataItem)\">Load More</button>\r\n          </span>\r\n          <span *ngIf=\"column.showStatusToggle\">\r\n            <!-- <mat-slide-toggle fxFlex class=\"mat-accent\" aria-label=\"Cloud\" (change)=\"onToggleChange($event, dataItem)\"\r\n              [checked]=\"(dataItem[column.value] == 'ACTIVE' || dataItem[column.value] === true)? true : false\">\r\n            </mat-slide-toggle> -->\r\n            <mat-slide-toggle fxFlex class=\"mat-accent\" aria-label=\"Cloud\" (change)=\"onToggleChange($event, dataItem)\"\r\n              [disabled]=\"column.disableCheck && dataItem[column.disableName]\"\r\n              [checked]=\"(dataItem[column.value] == 'ACTIVE' || dataItem[column.value] === true)? true : false\"\r\n              *ngIf=\"!column.isCondition || (column.isCondition && dataItem[column.conditionName])\">\r\n            </mat-slide-toggle>\r\n          </span>\r\n          <!-- Default Data Source API Update rewrite- 11-12-2020 -->\r\n          <ng-container\r\n            *ngIf=\"column.value == 'status'&& dataItem.type !='AVM' && (column.isCondition && dataItem[column.setasdefaultConName])\">\r\n            &nbsp;&nbsp; <span class=\"label bg-intiated ng-star-inserted reimport \"\r\n              (click)=\"actionRedirect(dataItem,column)\">\r\n              Set as Default\r\n            </span>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"column.value == 'status' && column.isCondition && dataItem[column.defaultConName]\">\r\n            &nbsp;&nbsp; <span class=\"label bg-success ng-star-inserted reimport \">\r\n              Default\r\n            </span>\r\n          </ng-container>\r\n          <!-- Default Data source API  End -->\r\n        </ng-template>\r\n      </kendo-grid-column>\r\n    </ng-container>\r\n  </kendo-grid>\r\n</div>\r\n",
                styles: [".custom-mat-form{display:inline!important}.tabheade{padding-top:3px}.heading-label{font-size:14px;padding-top:10px}tr.example-detail-row{height:0}tr.example-element-row:not(.example-expanded-row):hover{background:#f5f5f5}tr.example-element-row:not(.example-expanded-row):active{background:#efefef}.res-table{height:100%;overflow-x:auto}.mat-table-sticky{background:#59abfd;opacity:1}.mat-cell,.mat-footer-cell,.mat-header-cell{min-width:80px;box-sizing:border-box}.mat-footer-row,.mat-header-row,.mat-row{min-width:1920px}table{width:100%;padding-top:10px}.mat-form-field-wrapper{padding-bottom:10px}.mat-header-cell{font-weight:700!important;color:#000!important}.selectall-export{text-align:center;margin:0 1%;padding:4px;font-size:14px;border-radius:4px;background:#ececeaab}.multi-select-dropdown{min-width:0;width:180px!important}.selectBoxStyle{font-size:50px!important}.view-mode{color:#039be5;font-size:14px;font-weight:600;cursor:pointer}.selectTH{position:absolute;top:31px}.noRecords{padding:25px 0;color:gray}.reimport{cursor:pointer}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.check_message{display:block;background-color:#ffc;color:#222;padding:3px 8px;font-size:90%;text-align:center;font-family:arial,sans-serif}.sen-select-box{margin-top:22px}.sen-conversation{font-style:initial;color:#039be5;font-weight:700}.sen-lib-delete{font-size:22px;position:relative;color:#fff}.sent-lib-border-btm{box-shadow:0 1px 2px rgba(0,0,0,.1);margin-bottom:10px}.sen-lib-iconslist-content{position:relative;top:22px}.sen-lib-bold{font-weight:700!important}.sen-in-table{margin-top:15px;margin-left:28px!important;margin-right:28px!important}.sen-lib-icon-size{font-size:21px}.sen-lib-chat-read,.sen-lib-chat-unread{font-size:22px;position:relative;color:#fff!important}.sen-lib-cursor{cursor:pointer!important}.sen-lib-grp-btn-font{position:relative;bottom:4px;font-size:13px;color:#fff}.btn-border-right{border-right:2px solid #fff!important}.sen-lib-btn-bg-group{border-radius:29px;margin-left:10px;box-shadow:3px 3px 5px 0 #b3b3b3;background:linear-gradient(135deg,#a2b6df 0,#33569b 100%)}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}"]
            }] }
];
/** @nocollapse */
NewTableLayoutComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: ContentService },
    { type: MessageService },
    { type: MatDialog },
    { type: SnackBarService },
    { type: ChangeDetectorRef },
    { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] },
    { type: LoaderService }
];
NewTableLayoutComponent.propDecorators = {
    viewFrom: [{ type: Input }],
    onLoad: [{ type: Input }],
    tableId: [{ type: Input }],
    disablePagination: [{ type: Input }],
    selectAllBox: [{ type: ViewChild, args: ["selectAllBox",] }],
    table: [{ type: ViewChild, args: ["MatTable",] }],
    checkClickEventMessage: [{ type: Output }],
    actionClickEvent: [{ type: Output }],
    paginator: [{ type: ViewChild, args: ["paginator",] }],
    selectedPaginator: [{ type: ViewChild, args: ["selectedPaginator",] }]
};
export function base64ToArrayBuffer(base64) {
    const binaryString = window.atob(base64); // Comment this if not using base64
    const bytes = new Uint8Array(binaryString.length);
    return bytes.map((byte, i) => binaryString.charCodeAt(i));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3LXRhYmxlLWxheW91dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibmV3LXRhYmxlLWxheW91dC9uZXctdGFibGUtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFNBQVMsRUFDVCxLQUFLLEVBQ0wsTUFBTSxFQUNOLFlBQVksRUFDWixNQUFNLEVBRU4saUJBQWlCLEVBQ2xCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFDTCxZQUFZLEVBQ1osa0JBQWtCLEVBQ2xCLFNBQVMsRUFDVCxXQUFXLEVBQ1gsUUFBUSxFQUVULE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBRS9ELE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBQ3hDLE9BQU8sRUFFTCxXQUFXLEVBSVosTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QixPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFDbEQsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsb0JBQW9CO0FBQ3BCLHVCQUF1QjtBQUV2Qix3Q0FBd0M7QUFFeEMsNkJBQTZCO0FBRTdCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM1RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDOUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN2QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFM0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRWxELE9BQU8sRUFBK0IsT0FBTyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFJbEYsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFHcEYsbUJBQW1CO0FBR25CLGdDQUFnQztBQU1oQyxNQUFNLE9BQU8sdUJBQXVCO0lBb0VsQyxZQUNVLDZCQUEyRCxFQUMzRCxjQUE4QixFQUM5QixjQUE4QixFQUM5QixVQUFxQixFQUNyQixlQUFnQyxFQUNoQyxpQkFBb0MsRUFDYixXQUFXLEVBRWYsT0FBTyxFQUMxQixhQUE0QjtRQVQ1QixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQzNELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNiLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBRWYsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQTFFN0Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBR2xDLDJCQUFzQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDcEQscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUN4RCx1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsY0FBUyxHQUFVLEVBQUUsQ0FBQztRQUV0QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQVM5QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFFbkIsaUJBQVksR0FBUSxFQUFFLENBQUM7UUFDdkIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQU1aLGdCQUFXLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUNsQyxtQkFBYyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFDckMscUJBQWdCLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUkvQyx5QkFBb0IsR0FBUSxFQUFFLENBQUM7UUFFL0IsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBRXBDLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRXZCLGVBQVUsR0FBc0IsRUFBRSxDQUFDO1FBQzFDLG9CQUFlLEdBQVEsTUFBTSxDQUFDO1FBRzlCLGlCQUFZLEdBQUcsS0FBSyxDQUFDO1FBSWQsU0FBSSxHQUFHLElBQUksQ0FBQztRQUNaLFNBQUksR0FBd0IsU0FBUyxDQUFDO1FBQ3RDLGNBQVMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNuSCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUMzQixhQUFRLEdBQVEsRUFBRSxDQUFDO1FBQ25CLG1CQUFjLEdBQWEsS0FBSyxDQUFDO1FBQzFCLGtCQUFhLEdBQTJCLFdBQVcsQ0FBQztRQUMzRCxtRkFBbUY7UUFDbkYsK0RBQStEO1FBQy9ELHFFQUFxRTtRQUNyRSxrRUFBa0U7UUFDbEUsaUJBQVksR0FBRyxDQUFDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFO1lBQ2pELEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO1lBQ2pDLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFO1lBQ3BDLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztRQWNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDO1FBQ2hELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUM3RCxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQjthQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3RDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQTtZQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztZQUN2QixJQUFJLElBQUksSUFBSSxDQUFDLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQ3JDO1lBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxFQUFDLElBQUksRUFBRyxTQUFTLEVBQUMsQ0FBQyxDQUFBO1lBQy9ELElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzdCLDZDQUE2QztZQUM3QywwQkFBMEI7WUFDMUIsTUFBTTtZQUVOLG9DQUFvQztZQUNwQyx5QkFBeUI7WUFDekIsTUFBTTtZQUNOLHlFQUF5RTtZQUN6RSwyRUFBMkU7WUFDM0UsK0NBQStDO1lBQy9DLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLGdDQUFnQztZQUNoQyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztnQkFDRixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7WUFDRCxJQUFJO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ3hGLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFDLFdBQVcsQ0FBQyxDQUFBO1lBQ2hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQzFDLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxRQUFRLENBQUMsQ0FBQTtZQUMxQixJQUNFLElBQUksQ0FBQyxpQkFBaUI7Z0JBQ3RCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxRQUFRO2dCQUMvQixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLG9CQUFvQixFQUNwRDtnQkFDQSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUVILElBQUksQ0FBQyxjQUFjO2FBQ2hCLG9CQUFvQixFQUFFO2FBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ2pDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQTtZQUM5QixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksUUFBUSxFQUFFO2dCQUN6QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7YUFDeEI7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7YUFDeEM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFHRCxRQUFRO1FBQ04sMENBQTBDO1FBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsV0FBVyxDQUFDO1FBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLEtBQUssR0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO1lBQ2xDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztTQUNsQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGFBQWEsR0FBQztZQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7WUFDakIsTUFBTSxFQUFFLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1NBQ3ZDLENBQUE7UUFDRCxvQ0FBb0M7UUFDcEMsSUFBSSxJQUFJLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQzFDLENBQUM7UUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QyxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksVUFBVSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksYUFBYSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUcsQ0FBQyxFQUFFO1lBQzFFLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDdEMsSUFBSSxDQUFDLFlBQVksR0FBQyxFQUFFLENBQUM7U0FDdEI7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUN4RCxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLDBDQUEwQztRQUMxQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM1QyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksV0FBVyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBRXhCLENBQUM7SUFDRCxlQUFlO1FBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDdkQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7U0FDcEQ7SUFFSCxDQUFDO0lBQ0QsV0FBVztRQUNULElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUMzQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELFdBQVcsQ0FBQyxLQUFLO1FBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUN4QyxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMvQixJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQztRQUNsRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO1lBQzdELElBQUksV0FBVyxDQUFDLGVBQWUsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7YUFDekU7aUJBQU0sSUFBSSxXQUFXLENBQUMsWUFBWSxFQUFFO2dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTtvQkFDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztvQkFDbkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGVBQWU7b0JBQzlELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdkM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLFVBQVUsU0FBUztZQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUM7UUFDM0MsQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsY0FBYzthQUNoQixhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7YUFDMUQsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2hCLHdEQUF3RDtZQUN4RCxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLEdBQUc7WUFDSCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUM7WUFDeEQsSUFBSSxvQkFBb0IsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBRSw4QkFBOEI7WUFDeEcsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQ2xHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTztpQkFDakMsTUFBTSxDQUFDLFVBQVUsR0FBRztnQkFDbkIsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0wsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ25CLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1lBQ3ZDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxLQUFLO2dCQUN6RSxJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtvQkFDNUIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNqQiw0QkFBNEI7b0JBQzVCLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pCO3FCQUFNO29CQUNMLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLFVBQVUsUUFBUTt3QkFDaEUsSUFBSSxRQUFRLENBQUMsTUFBTSxFQUFFOzRCQUNuQixJQUFJLFFBQVEsQ0FBQyxtQkFBbUIsRUFBRTtnQ0FDaEMsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dDQUNuSixPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29DQUMzQyxDQUFDLENBQUMsV0FBVztvQ0FDYixDQUFDLENBQUMsRUFBRSxDQUFDOzZCQUNSO2lDQUFNO2dDQUNMLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7b0NBQ3ZDLENBQUMsQ0FBQyxFQUFFLENBQUM7NkJBQ1I7eUJBQ0Y7NkJBQU0sSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFOzRCQUMvQixPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUM7eUJBQ25EOzZCQUFNLElBQUksUUFBUSxDQUFDLGlCQUFpQixFQUFFOzRCQUNyQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7eUJBQ3pDOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDL0M7d0JBRUQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUU7NEJBQzdDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO2dDQUNwQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt5QkFDMUQ7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO29CQUNuQixTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ3ZGLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQztvQkFDL0YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQTtvQkFDaEQsSUFBSSxhQUFhLEVBQUU7d0JBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO3FCQUNyQjtvQkFDRCxPQUFPLHFCQUFRLElBQUksRUFBSyxPQUFPLENBQUUsQ0FBQztvQkFDbEMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDekI7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2FBQzVCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxTQUFTO29CQUNaLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDO3dCQUNqRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUM5QyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ1Y7WUFDRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2FBQ25DO1lBQ0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUE7WUFDckUseUVBQXlFO1lBRXpFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDekQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsZUFBZTtZQUNuRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDbkcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQztnQkFDakMsSUFBSSxXQUFXLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRTtvQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsQ0FBQztpQkFDckU7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsd0JBQXdCO1lBQ3hCLDJDQUEyQztZQUMzQywwQkFBMEI7WUFDMUIsbURBQW1EO1lBQ25ELHVDQUF1QztZQUN2Qyx5Q0FBeUM7WUFDekMsd0VBQXdFO1lBQ3hFLG1EQUFtRDtZQUNuRCwwQ0FBMEM7WUFFMUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM5QyxDQUFDLEVBQ0MsR0FBRyxDQUFDLEVBQUU7WUFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLENBQUMsQ0FBQyxDQUFDO0lBQ1QsQ0FBQztJQUVELG9CQUFvQixDQUFDLFFBQVEsRUFBRSxZQUFZO1FBQ3pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDN0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLFVBQVUsSUFBSTtZQUM5QyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxRQUFRLEVBQUUsS0FBSztnQkFDNUMsSUFBSSxHQUFHLEdBQUcsUUFBUSxDQUFDO2dCQUNuQixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsaUJBQWlCLENBQUM7Z0JBQ3ZGLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUM7Z0JBQ2pGLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUM7Z0JBQ2pGLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLHdCQUF3QixDQUFDO2dCQUNyRyxHQUFHLENBQUMsaUJBQWlCLENBQUMsR0FBRyxRQUFRLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsQ0FBQyxlQUFlLENBQUM7Z0JBQ3JGLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdkcsR0FBRyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2RSxHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUc7b0JBQ25CLE1BQU0sRUFBRSxtQkFBbUI7b0JBQzNCLFFBQVEsRUFBRSxpQkFBaUI7b0JBQzNCLFlBQVksRUFBRyxpQkFBaUI7b0JBQ2hDLFlBQVksRUFBRSxrQkFBa0I7b0JBQ2hDLFVBQVUsRUFBRSxrQkFBa0I7aUJBQ2pDLENBQUM7Z0JBQ0EsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3pCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksRUFBRSxJQUFJLEtBQUssSUFBSSxHQUFHLEVBQUU7b0JBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsMkJBQTJCLENBQUMsQ0FBQztvQkFDaEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsWUFBWSxDQUFDLENBQUM7b0JBQ2pDLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDakIsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsUUFBUSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsZUFBZSxDQUFDO29CQUN6RixPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQztvQkFDeEMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7b0JBQ3hDLE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDO29CQUM5QyxPQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQztvQkFDbEQsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO29CQUMzRixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO29CQUNyRixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO29CQUNyRixPQUFPLENBQUMsb0JBQW9CLENBQUMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQztvQkFDekcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsQ0FBQztvQkFDckUsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQztvQkFDN0IsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHO3dCQUN2QixNQUFNLEVBQUUsbUJBQW1CO3dCQUMzQixRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixZQUFZLEVBQUcsaUJBQWlCO3dCQUNoQyxZQUFZLEVBQUUsa0JBQWtCO3dCQUNoQyxVQUFVLEVBQUUsa0JBQWtCO3FCQUNqQyxDQUFDO29CQUNBLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxVQUFVLFFBQVE7d0JBQzdELE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDcEQsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUU5QjtZQUNILENBQUMsQ0FBQyxDQUFBO1FBRUosQ0FBQyxDQUFDLENBQUE7UUFDRixJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDekMsOENBQThDO1FBQzlDLDZEQUE2RDtRQUM3RCw0Q0FBNEM7UUFDNUMsTUFBTTtRQUNOLHdDQUF3QztRQUN4QyxxREFBcUQ7UUFDckQsSUFBSTtRQUNKLGlFQUFpRTtRQUNqRSw2Q0FBNkM7UUFDN0MsTUFBTTtRQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1FBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxtQkFBbUIsQ0FBQyxDQUFBO0lBQ3JELENBQUM7SUFFRCxjQUFjLENBQUMsV0FBVztRQUN4QixJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUU7WUFDdEQsSUFBSSxDQUFDLFNBQVMscUJBQVEsSUFBSSxDQUFDLFNBQVMsRUFBSyxXQUFXLENBQUUsQ0FBQztZQUN2RCxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUM7Z0JBQ3JDLFdBQVcsRUFBRSxXQUFXO2dCQUN4QixRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlO2FBQzNDLENBQUMsQ0FBQTtTQUNIO0lBQ0gsQ0FBQztJQUVELGNBQWMsQ0FBQyxJQUFJO1FBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNsRixDQUFDO0lBRUQsYUFBYSxDQUFDLEdBQUc7UUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxhQUFhLENBQUMsQ0FBQTtRQUMvQixJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQztRQUNsRSxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsa0JBQWtCLENBQUMsQ0FBQTtRQUNqRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQTtRQUM3QixJQUFJLFFBQVEsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNsQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQ2hDLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7UUFFekQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUNsRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO3dCQUNsQixRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM5RDt5QkFBTTt3QkFDTCxRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM3RDtvQkFDRCxRQUFRLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDaEY7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUNGLFFBQVEsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3hJLElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsUUFBUSxFQUFFLGtCQUFrQixDQUFDLE1BQU0sQ0FBQztpQkFDbEQsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFDN0IsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDakMsSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzlELGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN2QixJQUFJLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDN0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLFVBQVUsUUFBUSxFQUFFLEtBQUs7d0JBQ3BGLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFBO3dCQUM5QixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUM7d0JBQ25CLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDM0YsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7d0JBQ3pHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGVBQWUsQ0FBQzt3QkFDekYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUMvRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQzNFLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRzs0QkFDbkIsTUFBTSxFQUFFLG1CQUFtQjs0QkFDM0IsUUFBUSxFQUFFLGlCQUFpQjs0QkFDM0IsWUFBWSxFQUFHLGlCQUFpQjs0QkFDaEMsWUFBWSxFQUFFLGtCQUFrQjs0QkFDaEMsVUFBVSxFQUFFLGtCQUFrQjt5QkFDakMsQ0FBQzt3QkFDQSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLDhCQUE4QixDQUFDLENBQUE7d0JBQ3BFLElBQUksZ0JBQWdCLENBQUMsTUFBTSxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsRUFBRTs0QkFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs0QkFDeEIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDOzRCQUNqQixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQzs0QkFDOUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUM7NEJBQ2xELE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzs0QkFDL0YsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDekYsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDekYsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7NEJBQzdHLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QyxPQUFPLENBQUMsaUJBQWlCLENBQUMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDOzRCQUN0RCxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDOzRCQUN6RSxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDOzRCQUM3QixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUc7Z0NBQ3ZCLE1BQU0sRUFBRSxtQkFBbUI7Z0NBQzNCLFFBQVEsRUFBRSxpQkFBaUI7Z0NBQzNCLFlBQVksRUFBRyxpQkFBaUI7Z0NBQ2hDLFlBQVksRUFBRSxrQkFBa0I7Z0NBQ2hDLFVBQVUsRUFBRSxrQkFBa0I7NkJBQ2pDLENBQUM7NEJBQ0EsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFVBQVUsUUFBUTtnQ0FDN0QsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUNwRCxDQUFDLENBQUMsQ0FBQzs0QkFDSCxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ2hDO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDO1lBQzVELENBQUMsRUFDQyxHQUFHLENBQUMsRUFBRTtnQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNSO2FBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUN6RCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFO3dCQUNsQixRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUM5RDt5QkFBTTt3QkFDTCxRQUFRLENBQUMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM3RDtpQkFDRjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsYUFBYSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQ3JGO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN4QixDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxVQUFVLFNBQVM7Z0JBQzFDLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNuRixjQUFjLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzNHO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1lBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLHNCQUFzQixDQUFDLENBQUM7WUFDcEQsUUFBUSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUM5TCxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7aUJBQ2xELFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBQzdCLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ2pDLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDekYsZ0JBQWdCLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksWUFBWSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM3QyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsVUFBVSxRQUFRLEVBQUUsS0FBSzt3QkFDcEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUE7d0JBQzlCLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQzt3QkFDbkIsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDO3dCQUMzRixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO3dCQUNyRixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDO3dCQUNyRixHQUFHLENBQUMsb0JBQW9CLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQzt3QkFDekcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsZUFBZSxDQUFDO3dCQUN6RixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQy9HLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDM0UsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHOzRCQUNuQixNQUFNLEVBQUUsbUJBQW1COzRCQUMzQixRQUFRLEVBQUUsaUJBQWlCOzRCQUMzQixZQUFZLEVBQUcsaUJBQWlCOzRCQUNoQyxZQUFZLEVBQUUsa0JBQWtCOzRCQUNoQyxVQUFVLEVBQUUsa0JBQWtCO3lCQUNqQyxDQUFDO3dCQUNBLGdCQUFnQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsOEJBQThCLENBQUMsQ0FBQTt3QkFDcEUsSUFBSSxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksR0FBRyxFQUFFOzRCQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFBOzRCQUN4QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7NEJBQ2pCLE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDOzRCQUM5QyxPQUFPLENBQUMsZUFBZSxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQzs0QkFDbEQsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLGlCQUFpQixDQUFDOzRCQUMvRixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsV0FBVyxDQUFDOzRCQUN6RixPQUFPLENBQUMsb0JBQW9CLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyx3QkFBd0IsQ0FBQzs0QkFDN0csT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7NEJBQ3hDLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QyxPQUFPLENBQUMsaUJBQWlCLENBQUMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDOzRCQUN0RCxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDOzRCQUN6RSxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDOzRCQUM3QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsVUFBVSxRQUFRO2dDQUM3RCxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3BELENBQUMsQ0FBQyxDQUFDOzRCQUNILE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRztnQ0FDdkIsTUFBTSxFQUFFLG1CQUFtQjtnQ0FDM0IsUUFBUSxFQUFFLGlCQUFpQjtnQ0FDM0IsWUFBWSxFQUFHLGlCQUFpQjtnQ0FDaEMsWUFBWSxFQUFFLGtCQUFrQjtnQ0FDaEMsVUFBVSxFQUFFLGtCQUFrQjs2QkFDakMsQ0FBQzs0QkFDQSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ2hDO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssR0FBRyxnQkFBZ0IsQ0FBQztZQUN2RixDQUFDLEVBQ0MsR0FBRyxDQUFDLEVBQUU7Z0JBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUM7U0FDUjthQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7WUFDekQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ3ZCLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsU0FBUztnQkFDMUMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ25GLElBQUksU0FBUyxDQUFDLElBQUksRUFBRTt3QkFDbEIsUUFBUSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDOUQ7eUJBQU07d0JBQ0wsUUFBUSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDN0Q7aUJBQ0Y7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsU0FBUztnQkFDMUMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLEtBQUssRUFBRSxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ25GLGFBQWEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNyRjtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDeEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsY0FBYyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUMzRztZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDdkIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsVUFBVSxTQUFTO2dCQUMxQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDbkYsYUFBYSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztpQkFDaEk7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLG1CQUFtQixDQUFDLENBQUM7WUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztZQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2xELFFBQVEsQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLENBQUMsY0FBYyxDQUFDLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUN4TyxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLFFBQVEsRUFBRSxrQkFBa0IsQ0FBQyxNQUFNLENBQUM7aUJBQ2xELFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7Z0JBQzdCLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQ2pDLElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQzlHLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN2QixJQUFJLFlBQVksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDN0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLFVBQVUsUUFBUSxFQUFFLEtBQUs7d0JBQ3BGLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFBO3dCQUM5QixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUM7d0JBQ25CLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDM0YsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7d0JBRXpHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLGVBQWUsQ0FBQzt3QkFDekYsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUMvRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxZQUFZLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQzNFLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRzs0QkFDbkIsTUFBTSxFQUFFLG1CQUFtQjs0QkFDM0IsUUFBUSxFQUFFLGlCQUFpQjs0QkFDM0IsWUFBWSxFQUFHLGlCQUFpQjs0QkFDaEMsWUFBWSxFQUFFLGtCQUFrQjs0QkFDaEMsVUFBVSxFQUFFLGtCQUFrQjt5QkFDakMsQ0FBQzt3QkFDQSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7d0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLDhCQUE4QixDQUFDLENBQUE7d0JBQ3BFLElBQUksZ0JBQWdCLENBQUMsTUFBTSxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsRUFBRTs0QkFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQTs0QkFDeEIsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDOzRCQUNqQixPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsUUFBUSxDQUFDLFdBQVcsQ0FBQzs0QkFDOUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUM7NEJBQ2xELE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzs0QkFDL0YsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDekYsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzs0QkFDekYsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsd0JBQXdCLENBQUM7NEJBQzdHLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QyxPQUFPLENBQUMsVUFBVSxDQUFDLEdBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQzs0QkFDeEMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQzs0QkFDdEQsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsQ0FBQzs0QkFDekUsT0FBTyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQzs0QkFDN0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFVBQVUsUUFBUTtnQ0FDN0QsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDOzRCQUNwRCxDQUFDLENBQUMsQ0FBQzs0QkFDSCxPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUc7Z0NBQ3ZCLE1BQU0sRUFBRSxtQkFBbUI7Z0NBQzNCLFFBQVEsRUFBRSxpQkFBaUI7Z0NBQzNCLFlBQVksRUFBRyxpQkFBaUI7Z0NBQ2hDLFlBQVksRUFBRSxrQkFBa0I7Z0NBQ2hDLFVBQVUsRUFBRSxrQkFBa0I7NkJBQ2pDLENBQUM7NEJBQ0EsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3lCQUNoQztvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLG9CQUFvQixDQUFDLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxjQUFjLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDO1lBQzVHLENBQUMsRUFDQyxHQUFHLENBQUMsRUFBRTtnQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNSO0lBQ0gsQ0FBQztJQUVELG9CQUFvQixDQUFDLEdBQUc7UUFDdEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsYUFBYSxDQUFDLENBQUE7UUFDL0IsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUM7UUFDbEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLGtCQUFrQixDQUFDLENBQUE7UUFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUE7UUFDN0IsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDbEIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUVoQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLGFBQWEsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2xGLFFBQVEsQ0FBQyxlQUFlLEdBQUcsR0FBRyxDQUFDLFdBQVcsQ0FBQztZQUMzQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxHQUFHLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztTQUM1RTtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDL0UsUUFBUSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUM7WUFDekMsUUFBUSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDekU7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLGlCQUFpQixFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDdEYsUUFBUSxDQUFDLGlCQUFpQixHQUFHLEdBQUcsQ0FBQyxXQUFXLENBQUM7WUFDN0MsUUFBUSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsR0FBRyxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7U0FDaEY7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9FLFFBQVEsQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDO1lBQ3pDLFFBQVEsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1NBQ3pFO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQTtRQUV2QyxRQUFRLENBQUMsY0FBYyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUV4SSxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFBO1FBQ3hDLElBQUksQ0FBQyxjQUFjO2FBQ2hCLGFBQWEsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsTUFBTSxDQUFDO2FBQ2xELFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FBQztZQUM3QixJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ2pDLDhEQUE4RDtZQUU5RCxTQUFTO1lBRVQsSUFBSTtZQUNKLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ2xELElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUM5RCxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztnQkFDdkIsSUFBSSxZQUFZLENBQUMsYUFBYSxFQUFFO29CQUM5QixDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFFLFVBQVUsUUFBUSxFQUFFLEtBQUs7d0JBQ3JFLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFBO3dCQUM5QixJQUFJLEdBQUcsR0FBRyxRQUFRLENBQUM7d0JBQ25CLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQzt3QkFDM0YsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxDQUFDLFdBQVcsQ0FBQzt3QkFDckYsR0FBRyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsWUFBWSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsZUFBZSxDQUFDO3dCQUN6RixHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQy9HLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLFlBQVksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQzt3QkFDM0UsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSw4QkFBOEIsQ0FBQyxDQUFBO3dCQUNwRSxJQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxFQUFFLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxHQUFHLEVBQUU7NEJBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUE7NEJBQ3hCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQzs0QkFDakIsT0FBTyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUM7NEJBQzlDLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDOzRCQUNsRCxPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsaUJBQWlCLENBQUM7NEJBQy9GLE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxZQUFZLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxXQUFXLENBQUM7NEJBQ3pGLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDOzRCQUN4QyxPQUFPLENBQUMsaUJBQWlCLENBQUMsR0FBRyxRQUFRLENBQUMsZUFBZSxDQUFDOzRCQUN0RCxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsWUFBWSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLENBQUMsR0FBRyxDQUFDOzRCQUN6RSxPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDOzRCQUM3QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ2hDO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO2dCQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztnQkFDcEQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsS0FBSyxHQUFHLGdCQUFnQixDQUFDO2FBQzNEO2lCQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUMvQixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxrQkFBa0IsQ0FBQyxDQUFBO2dCQUM3QyxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDOUQsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsS0FBSyxFQUFFLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFBO2dCQUM3RSxJQUFJLGNBQWMsR0FBRyxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDOUQsOENBQThDO2dCQUM5QyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7Z0JBQ3JCLGtDQUFrQztnQkFDbEMsNkVBQTZFO2dCQUM3RSxvQ0FBb0M7Z0JBQ3BDLDBCQUEwQjtnQkFDMUIsb0dBQW9HO2dCQUNwRyxrR0FBa0c7Z0JBQ2xHLHdIQUF3SDtnQkFDeEgsb0ZBQW9GO2dCQUNwRixvQ0FBb0M7Z0JBQ3BDLDRFQUE0RTtnQkFDNUUsK0RBQStEO2dCQUMvRCxtQ0FBbUM7Z0JBQ25DLDBCQUEwQjtnQkFDMUIsMEdBQTBHO2dCQUMxRyxtREFBbUQ7Z0JBQ25ELGlFQUFpRTtnQkFDakUsb0ZBQW9GO2dCQUNwRix3Q0FBd0M7Z0JBQ3hDLDBDQUEwQztnQkFDMUMsTUFBTTtnQkFDTixRQUFRO2dCQUNSLElBQUk7Z0JBRUosc0RBQXNEO2dCQUN0RCw2REFBNkQ7YUFDOUQ7WUFFRCxxQ0FBcUM7UUFDdkMsQ0FBQyxFQUNDLEdBQUcsQ0FBQyxFQUFFO1lBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUM5QixDQUFDLENBQUMsQ0FBQztJQUNULENBQUM7SUFDRCxhQUFhLENBQUMsS0FBSztRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxjQUFjLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBQ0QsZUFBZTtRQUNiLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7WUFDOUQsSUFBSSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQzdDLENBQUM7WUFDRixJQUFJLGlCQUFpQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ2pFLHFDQUFxQztZQUNyQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQzVFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ2xFO1lBRUQsSUFBSSxXQUFXLEdBQUcsa0JBQWtCO2lCQUNqQyxNQUFNLENBQUMsVUFBVSxHQUFHO2dCQUNuQixPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7WUFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDNUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLGlCQUFpQixHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDdEY7U0FDRjtJQUVILENBQUM7SUFDRCxVQUFVLENBQUMsVUFBVTtRQUVuQixJQUFHLElBQUksQ0FBQyxJQUFJLElBQUUsYUFBYSxFQUFDO1lBQzFCLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQy9CLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1NBQ2pCO1FBQ0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztRQUNsQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxNQUFNO1lBQzFCLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDakQsSUFBSSxXQUFXLEdBQ2IsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUMxRSxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUM7WUFDWixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztRQUM1RCxJQUFJLGlCQUFpQixHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQTtRQUMzRyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLEVBQUU7WUFDcEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQztTQUNoQztRQUNELElBQUksaUJBQWlCLEVBQUU7WUFDckIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDL0IsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHdCQUF3QixDQUFDO1lBQ2hFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDO1lBQzdELElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxNQUFNLENBQUM7WUFDL0IsSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUN2QyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQztpQkFDdkMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNmLElBQUksU0FBUyxHQUFHLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBRS9FLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7WUFDcEMsQ0FBQyxDQUFDLENBQUM7U0FDTjtRQUNELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUU7WUFDakUsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztZQUNsRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQix5RUFBeUU7WUFDekUsc0NBQXNDO1lBQ3RDLE1BQU07U0FDUDtRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUN0RCxJQUFJLENBQUMsV0FBVyxDQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUNqQixXQUFXLENBQ1osQ0FBQztTQUNIO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxnQkFBZ0I7Z0JBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDdEU7UUFDRCxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsOEJBQThCO1NBQy9CO0lBQ0gsQ0FBQztJQUNELE9BQU8sQ0FBQyxpQkFBaUIsRUFBRSxpQkFBaUI7UUFDMUMsSUFBSSxJQUFJLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFELElBQUksQ0FBQyxPQUFPLEdBQUcsaUJBQWlCLENBQUMsV0FBVyxDQUFDO1FBQzdDLElBQUksaUJBQWlCLENBQUMsY0FBYyxFQUFFO1lBQ3BDLElBQUksTUFBTSxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDckQsSUFBSSxZQUFZLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztZQUM3RCxJQUFJLFFBQVEsR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUM7WUFDckUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdEQsSUFBSSxjQUFjLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN4Rix1REFBdUQ7WUFDdkQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNqQyxHQUFHO1lBQ0gsSUFBSSxXQUFXLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLENBQUUsMERBQTBEO1lBQ2pJLElBQUksY0FBYyxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUM7WUFDbEUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1lBQzNCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxVQUFVLFNBQVM7Z0JBQzVDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQztZQUMzQyxDQUFDLENBQUMsQ0FBQTtZQUNGLElBQUksV0FBVyxFQUFFO2dCQUNmLElBQUksT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQztnQkFDeEQsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDckMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxXQUFXO29CQUM3QyxJQUFJLFdBQVcsQ0FBQyxlQUFlLEVBQUU7d0JBQy9CLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtxQkFDMUU7eUJBQU07d0JBQ0wsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO3FCQUMzSDtnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNO2dCQUNMLElBQUcsQ0FBQyxpQkFBaUIsRUFBRTtvQkFDckIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxXQUFXO3dCQUM3Qyx3Q0FBd0M7d0JBQ3hDLHNDQUFzQzt3QkFDdEMsMEJBQTBCO3dCQUMxQixJQUFJLFdBQVcsQ0FBQyxlQUFlLEVBQUU7NEJBQy9CLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTt5QkFDMUU7NkJBQU0sSUFBSSxXQUFXLENBQUMsWUFBWSxFQUFFOzRCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTtnQ0FDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztnQ0FDbkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7eUJBQ3ZCOzZCQUFLOzRCQUNKLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQzVELCtGQUErRjs0QkFDN0YsSUFBRyxVQUFVLEVBQUM7Z0NBQ2IsSUFBSyxRQUFRLEdBQUcsV0FBVyxDQUFDLE1BQU07b0NBQ2pDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO29DQUN2RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBRXRDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxlQUFlO29DQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7b0NBQzFCLENBQUMsQ0FBQyxRQUFRLENBQUM7NkJBQ1o7eUJBQ0Y7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7YUFFRjtZQUNELElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQzlGLElBQUksQ0FBQyxXQUFXLHFCQUFRLElBQUksQ0FBQyxXQUFXLEVBQUssaUJBQWlCLENBQUUsQ0FBQztZQUNqRSxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN2QyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2hCLHdEQUF3RDtnQkFDeEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDaEMsR0FBRztnQkFDSCxJQUFJLG9CQUFvQixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFFLDhCQUE4QjtnQkFDeEcsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2dCQUNsRyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCLENBQUM7Z0JBQ3hELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTztxQkFDakMsTUFBTSxDQUFDLFVBQVUsR0FBRztvQkFDbkIsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDO2dCQUN0QixDQUFDLENBQUMsQ0FBQztnQkFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLDJCQUEyQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUNoRSxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ25CLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztnQkFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsWUFBWSxDQUFDLENBQUM7Z0JBQ3RELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztnQkFHeEQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFLFVBQVUsSUFBSSxFQUFFLEtBQUs7b0JBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBRXRDLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO3dCQUM1QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7d0JBQ2pCLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxJQUFJLENBQUM7d0JBQ3pCLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ3pCO3lCQUFNO3dCQUNMLHlDQUF5Qzt3QkFDekMsK0RBQStEO3dCQUMvRCxrRUFBa0U7d0JBQ2xFLHVFQUF1RTt3QkFDdkUsb0RBQW9EO3dCQUVwRCw4SEFBOEg7d0JBQzlILDBEQUEwRDt3QkFDMUQsNENBQTRDO3dCQUM1QyxJQUFJO3dCQUNKLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDekQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLEVBQUUsVUFBVSxRQUFROzRCQUM1RCxPQUFPLENBQUMsR0FBRyxDQUFDLG1DQUFtQyxFQUFFLFFBQVEsQ0FBQyxDQUFDOzRCQUUzRCxJQUFJLFFBQVEsQ0FBQyxNQUFNLEVBQUU7Z0NBQ25CLElBQUksUUFBUSxDQUFDLG1CQUFtQixFQUFFO29DQUNoQyxJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0NBQ25KLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7d0NBQzNDLENBQUMsQ0FBQyxXQUFXO3dDQUNiLENBQUMsQ0FBQyxFQUFFLENBQUM7aUNBQ1I7cUNBQ0k7b0NBQ0gsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzt3Q0FDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQzt3Q0FDdkMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDUjs2QkFDRjtpQ0FBTSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUU7Z0NBQy9CLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQzs2QkFDbkQ7aUNBQ0ksSUFBSSxRQUFRLENBQUMsaUJBQWlCLEVBQUU7Z0NBQ25DLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQzs2QkFDekM7aUNBQU07Z0NBQ0wsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDOzZCQUMvQzs0QkFDRCxJQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7Z0NBQ3JCLHFEQUFxRDtnQ0FFckQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDbkQsK0JBQStCLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0NBRXBFLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQztnQ0FDdEYsMkNBQTJDO2dDQUMzQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLHFDQUFxQyxHQUFHLGVBQWUsQ0FBQyxDQUFDO2dDQUV4SCxJQUFJLE9BQU8sR0FBRyxJQUFJLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQztnQ0FDekMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUM7Z0NBQ2pDLE9BQU8sQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUM7Z0NBQzlCLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLDhCQUE4QixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7Z0NBQ3BJLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLDRCQUE0QixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUM7NkJBRTVIOzRCQUNELElBQUksaUJBQWlCLENBQUMsbUJBQW1CLEVBQUU7Z0NBQ3pDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29DQUNwQixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzZCQUN0RDt3QkFDSCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7d0JBQ25CLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQy9FLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3dCQUMzRixPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFBO3dCQUNoRCxJQUFJLGFBQWEsRUFBRTs0QkFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7eUJBQ3JCO3dCQUNELE9BQU8scUJBQVEsSUFBSSxFQUFLLE9BQU8sQ0FBRSxDQUFDO3dCQUNsQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUN6QjtnQkFDSCxDQUFDLENBQUMsQ0FBQztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLEVBQUUsRUFBRSxTQUFTLENBQUMsQ0FBQztnQkFFM0IsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sRUFBRTtvQkFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7aUJBQzVCO3FCQUFNO29CQUNMLElBQUksQ0FBQyxTQUFTO3dCQUNaLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDOzRCQUNsRCxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDOzRCQUMvQixDQUFDLENBQUMsRUFBRSxDQUFDO2lCQUNWO2dCQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRTtvQkFDN0MsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUE7aUJBQ3ZEO2dCQUNELElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7aUJBQ25DO2dCQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDdkosSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDL0osSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7Z0JBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sSUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDO2dCQUNoRixJQUFJLENBQUMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO2dCQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6RCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxlQUFlO2dCQUNuRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUN0RSx1Q0FBdUM7Z0JBQ3RDLElBQUksV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ25HLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUM7b0JBQ2pDLElBQUksV0FBVyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUU7d0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7cUJBQ3JFO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQztxQkFDcEM7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsd0JBQXdCO2dCQUN4QiwyQ0FBMkM7Z0JBQzNDLDBCQUEwQjtnQkFDMUIsbURBQW1EO2dCQUNuRCx1Q0FBdUM7Z0JBQ3ZDLHlDQUF5QztnQkFDekMsd0VBQXdFO2dCQUN4RSxtREFBbUQ7Z0JBQ25ELDBDQUEwQztnQkFFMUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM5QyxDQUFDLEVBQ0MsR0FBRyxDQUFDLEVBQUU7Z0JBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUFDLENBQUM7U0FDUjtJQUNILENBQUM7SUFDRCxxQkFBcUI7SUFDckIscUNBQXFDO0lBQ3JDLHNDQUFzQztJQUN0QywwQkFBMEI7SUFDMUIsOEJBQThCO0lBQzlCLDZCQUE2QjtJQUM3Qiw0QkFBNEI7SUFDNUIsb0ZBQW9GO0lBQ3BGLDBDQUEwQztJQUMxQyxzQkFBc0I7SUFDdEIsbUJBQW1CO0lBQ25CLDBCQUEwQjtJQUMxQiw2QkFBNkI7SUFDN0Isd0NBQXdDO0lBQ3hDLHNCQUFzQjtJQUN0QixrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLDRCQUE0QjtJQUM1QiwrQkFBK0I7SUFDL0Isb0NBQW9DO0lBQ3BDLDRCQUE0QjtJQUM1QixpQ0FBaUM7SUFDakMsd0JBQXdCO0lBQ3hCLHNDQUFzQztJQUN0QyxrQ0FBa0M7SUFDbEMsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQiwwQ0FBMEM7SUFDMUMsMENBQTBDO0lBQzFDLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsd0NBQXdDO0lBQ3hDLHlDQUF5QztJQUN6QyxvQ0FBb0M7SUFDcEMsbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQiwyQ0FBMkM7SUFDM0MsNENBQTRDO0lBQzVDLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsMENBQTBDO0lBQzFDLGtCQUFrQjtJQUNsQixjQUFjO0lBQ2QsWUFBWTtJQUVaLGlEQUFpRDtJQUVqRCwyREFBMkQ7SUFDM0QsUUFBUTtJQUVSLDhDQUE4QztJQUM5Qyw4REFBOEQ7SUFDOUQsaURBQWlEO0lBQ2pELDhDQUE4QztJQUM5Qyx3REFBd0Q7SUFDeEQsNEJBQTRCO0lBQzVCLHVFQUF1RTtJQUN2RSxvQ0FBb0M7SUFDcEMsbUJBQW1CO0lBQ25CLHdCQUF3QjtJQUN4QixvRUFBb0U7SUFDcEUsb0NBQW9DO0lBQ3BDLG1CQUFtQjtJQUNuQixpQkFBaUI7SUFDakIsaUJBQWlCO0lBRWpCLGVBQWU7SUFFZiwyREFBMkQ7SUFDM0QsK0JBQStCO0lBQy9CLHVDQUF1QztJQUN2QyxrQ0FBa0M7SUFDbEMsa0NBQWtDO0lBQ2xDLGlCQUFpQjtJQUNqQixlQUFlO0lBRWYsNkJBQTZCO0lBQzdCLGVBQWU7SUFDZixhQUFhO0lBQ2IsSUFBSTtJQUNKLFdBQVcsQ0FBQyxZQUFZLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxXQUFXO1FBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDOUMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ25CLHFEQUFxRDtRQUNyRCxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUN6RSxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQ3pDLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsSUFBSSxFQUFFLEtBQUs7Z0JBQzVDLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO29CQUM1QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ2pCLE9BQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUN0QyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN6QjtxQkFBTTtvQkFDTCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ2pCLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsRUFBRSxVQUFVLFFBQVE7d0JBQ3ZELElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTs0QkFDbkIsSUFBSSxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0NBQ2hDLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQ0FDbkosT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQ0FDM0MsQ0FBQyxDQUFDLFdBQVc7b0NBQ2IsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs2QkFDUjtpQ0FBTTtnQ0FDTCxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29DQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO29DQUN2QyxDQUFDLENBQUMsRUFBRSxDQUFDOzZCQUNSO3lCQUNGOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDL0M7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxxQkFBUSxJQUFJLEVBQUssT0FBTyxDQUFFLENBQUM7b0JBQ2xDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLFdBQVcsQ0FBQztRQUN4QyxhQUFhLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDN0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQzVDLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxlQUFlLEVBQUU7WUFDaEQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxVQUFVLE9BQU87Z0JBQ3ZELENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxVQUFVLFdBQVc7b0JBQ2xELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxlQUFlO3dCQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN4QyxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsY0FBYztxQkFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQztxQkFDL0MsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO29CQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztvQkFDbEMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7b0JBQ3BDLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsWUFBWTt3QkFDN0MsSUFDRSxJQUFJLENBQUMsUUFBUTs0QkFDYixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVzs0QkFDaEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFDakM7NEJBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0NBQzNCLE9BQU8sSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztvQ0FDMUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVztvQ0FDeEQsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDVCxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ2hFOzs0QkFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO29CQUNuRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO2dCQUNuRCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7UUFDL0QsSUFBSSxDQUFDLFlBQVksR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQztRQUN4RSxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxlQUFlLENBQUMsQ0FBQztRQUMzQyx3REFBd0Q7UUFDeEQsSUFBSSxvQkFBb0IsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLHNCQUFzQixDQUFDLENBQUMsQ0FBRSw4QkFBOEI7UUFDeEcsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ2xHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsT0FBTzthQUNqQyxNQUFNLENBQUMsVUFBVSxHQUFHO1lBQ25CLE9BQU8sR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FBQztRQUNMLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztRQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSx3Q0FBd0MsQ0FBQyxDQUFDO1FBQzlFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO1FBQ3ZELGdDQUFnQztRQUNoQyx5REFBeUQ7UUFDekQsNkNBQTZDO0lBQy9DLENBQUM7SUFDRCxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsS0FBSztRQUM5QixrREFBa0Q7UUFDbEQsc0NBQXNDO1FBQ3RDLFVBQVU7UUFDVixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFO1lBQzVELFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztTQUNqRDtRQUVELGlEQUFpRDtRQUNqRCxtQ0FBbUM7UUFDbkMsVUFBVTtRQUNWLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUN6SCxPQUFPLENBQUMsR0FBRyxDQUFDLHlCQUF5QixFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDM0QscUNBQXFDO1FBQ3JDLGtDQUFrQztRQUNsQyx1Q0FBdUM7UUFDdkMsK0JBQStCO1FBQy9CLGFBQWE7UUFDYiw2Q0FBNkM7UUFDN0MscUNBQXFDO1FBQ3JDLE9BQU87UUFDUCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDbkIsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUM5SSxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixFQUFFLGVBQWUsQ0FBQyxDQUFDO1FBQ3JELElBQUksa0JBQWtCLElBQUksa0JBQWtCLEdBQUcsQ0FBQyxFQUFFO1lBQ2hELElBQUksZUFBZSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEVBQUU7Z0JBQzdDLFNBQVMsR0FBRyxlQUFlLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0wsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLGtCQUFrQixFQUFFLENBQUMsRUFBRSxFQUFFO29CQUM1QyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUNuQjthQUNGO1lBQ0QsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLFNBQVMsQ0FBQztTQUN2QzthQUFNO1lBQ0wsSUFBSSxrQkFBa0IsSUFBSSxDQUFDLEVBQUU7Z0JBQzNCLFFBQVEsQ0FBQyxlQUFlLENBQUMsR0FBRyxTQUFTLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0wsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUd2QztTQUNGO1FBQ0QsR0FBRztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLEVBQUUsUUFBUSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7UUFDdkUsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO1lBQ25FLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFLFVBQVUsSUFBSTtnQkFDOUQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxpQkFBaUIsR0FDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTTtZQUMvRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO1lBQzdCLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDVCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2xFLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBQ0wsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQ3JDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFdBQVcsS0FBSyxRQUFRLENBQUMsV0FBVyxDQUNoRCxDQUFDO1lBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ25ELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7WUFDOUQsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztZQUNqRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLEdBQUcsRUFBRTtnQkFDbEQsSUFBSSxTQUFTLEdBQ1gsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU07b0JBQzNDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLFVBQVUsQ0FBQztvQkFDL0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDVCxTQUFTLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLE9BQU87b0JBQzVDLE9BQU8sT0FBTyxJQUFJLElBQUksQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsU0FBUyxDQUFDO2FBQzFDO1NBQ0Y7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ25FLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUE7UUFFOUUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxvQkFBb0IsQ0FBQyxDQUFDO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDN0MsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7UUFDbkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQztRQUM1QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBQyxHQUFHLEVBQUUsZ0JBQWdCLEVBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFDLGVBQWUsRUFBQyxJQUFJLENBQUMsTUFBTSxFQUFDLGNBQWMsRUFBQyxJQUFJLENBQUMsS0FBSyxFQUFDLG1CQUFtQixFQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUN0SyxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxXQUFXLEdBQUUsQ0FBQyxDQUFDO1FBQ25CLElBQUksR0FBRyxLQUFLLENBQUMsRUFBRTtZQUNiLElBQUksQ0FBQyxhQUFhLEdBQUcsV0FBVyxDQUFDO1lBQ2pDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1NBQ3JDO2FBQU0sSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLEdBQUcsZUFBZSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQzFCLDJGQUEyRjtZQUMxRixJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFFLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQSxDQUFDLENBQUEsSUFBSSxDQUFBLENBQUMsQ0FBQSxLQUFLLENBQUM7U0FDNUQ7YUFBTTtZQUNMLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1lBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDO1NBQ3BDO1FBRUQsSUFBRyxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sR0FBQyxDQUFDLEVBQzdCO1lBQ0Usb0JBQW9CO1lBQ3BCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxTQUFTO2dCQUN4QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksU0FBUyxDQUFDLENBQUM7Z0JBQzNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNoRCxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNyQixRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQjtZQUNILENBQUMsQ0FBQyxDQUFBO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ2pGLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxFQUFDLEtBQUssQ0FBQyxDQUFDO1lBQy9FLFdBQVcsR0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxxQkFBcUI7U0FDNUU7UUFDRCxJQUFJLENBQUMsWUFBWSxHQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxZQUFvQztRQUNwRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxZQUFZLENBQUMsQ0FBQztRQUMzQyxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxZQUFZLEtBQUssU0FBUyxFQUFFO1lBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDL0IsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1lBQ3hELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3hFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ25DLElBQUksQ0FBQyxhQUFhLEdBQUcsU0FBUyxDQUFDO1lBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQzVCO2FBQU07WUFDTCxJQUFJLENBQUMsWUFBWSxHQUFDLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQy9CLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQztZQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUMzQjtRQUNELElBQUksV0FBVyxHQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUNsRCxJQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFDLENBQUMsRUFDN0I7WUFDRSxXQUFXLEdBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsWUFBWSxFQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxDQUFDLFlBQVksR0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUM7UUFDNUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUNyRSxDQUFDO0lBQ0QsY0FBYyxDQUFDLEtBQUs7UUFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssSUFBSSxFQUFFO1lBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDN0IsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3RELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RFLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDL0UsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUMsTUFBTSxDQUFDO1lBQzNELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1NBRTFEO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0QyxJQUFJLENBQUMsWUFBWSxHQUFDLEVBQUUsQ0FBQTtZQUNwQixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUMzQjtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQ0QsYUFBYSxDQUFDLElBQUksRUFBRSxVQUFVO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLG1CQUFtQixDQUFDLENBQUE7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsYUFBYTtRQUNYLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNyQyxJQUFJLENBQUMsYUFBYSxHQUFHLFNBQVMsQ0FBQztRQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDO1FBQ3ZELElBQUksS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUNmLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsVUFBVSxJQUFJO1lBQ2xELEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEQsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsY0FBYzthQUNoQixhQUFhLENBQUMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7YUFDM0MsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxZQUFZLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1lBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBRTdELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDdkUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2hCLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUMsRUFBRSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUMsRUFBRSxDQUFDO1FBQ2xDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFDMUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztRQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDeEMsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUMxQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDeEQsSUFBSSxDQUFDLG9CQUFvQixHQUFHO2dCQUMxQixTQUFTLEVBQUUsV0FBVyxDQUFDLEtBQUs7Z0JBQzVCLFdBQVcsRUFBRSxXQUFXLENBQUMsS0FBSzthQUMvQixDQUFDO1lBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN0RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUMvQyxJQUFJLFVBQVUsR0FDWixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztnQkFDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztnQkFDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7WUFDbkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7WUFDL0QsSUFBSSxhQUFhLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLGFBQWEsQ0FBQztZQUNwRCxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO1lBQ3hDLElBQUksVUFBVSxHQUFHLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN6RCxJQUFJLFVBQVUsRUFBRTtnQkFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7YUFDaEM7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxLQUFLLEdBQUc7b0JBQ1YsTUFBTSxFQUFFLENBQUM7b0JBQ1QsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsVUFBVSxFQUFFLElBQUksQ0FBQyxpQkFBaUI7aUJBQ25DLENBQUM7Z0JBQ0YsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLGVBQWUsRUFBRTtvQkFDbEQsS0FBSyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEtBQUssQ0FBQztpQkFDL0M7cUJBQU07b0JBQ0wsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUk7d0JBQy9CLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNOzRCQUN4QixDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzs0QkFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUMvQixJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTs0QkFDdkMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7Z0NBQ2hELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0NBQzVCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDO3lCQUNoQjs2QkFBTSxJQUFJLFFBQVEsRUFBRTs0QkFDbkIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7Z0NBQ2hELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztnQ0FDMUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQzt5QkFDZDtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCw0QkFBNEI7Z0JBQzVCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxDQUFDO2FBQ3BEO1NBQ0Y7SUFHSCxDQUFDO0lBQ0QsV0FBVyxDQUFDLEtBQUs7UUFDZixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQixDQUFDO0lBQ0QsYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNO1FBQ3pCLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztRQUMvQyxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRSxFQUFFLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztRQUNyRSxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUNyRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUMvRCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztTQUN4RDtRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFDL0Isc0NBQXNDO1FBQ3RDLGdEQUFnRDtRQUNoRCxvQ0FBb0M7UUFDcEMsV0FBVztRQUNYLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUk7UUFDSixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFDRCxRQUFRLENBQUMsT0FBTyxFQUFFLEdBQUc7UUFDbkIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsZ0NBQWdDLEVBQUUsR0FBRyxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDckYsSUFBSSxHQUFHLENBQUMsVUFBVSxFQUFFO1lBQ2xCLElBQUksT0FBTyxHQUFHLG9DQUFvQyxDQUFBO1lBQ2xELE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxHQUFHLE9BQU8sQ0FBQztZQUV2RCxPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxDQUFDLENBQUE7WUFDOUMsa0VBQWtFO1NBRW5FO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNuQztJQUNILENBQUM7SUFDRCxjQUFjLENBQUMsT0FBTyxFQUFFLE1BQU07UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxXQUFXLENBQUMsT0FBTyxFQUFFLE1BQU07UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUE7UUFDbkMsSUFBSSxNQUFNLElBQUksTUFBTSxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksTUFBTSxJQUFJLFlBQVksSUFBSSxNQUFNLElBQUksTUFBTSxFQUFFO1lBQ3RGLE1BQU0sR0FBRyxDQUFDLE1BQU0sSUFBSSxZQUFZLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUN4RSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLFFBQVEsRUFBRTtZQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztZQUNqQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQ3ZDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtvQkFDOUMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxLQUFLLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FDaEMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksT0FBTyxDQUN0QixDQUFDO29CQUNGLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFlBQVksQ0FBQztvQkFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUseUNBQXlDO2lCQUMxQzthQUNGO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRTtvQkFDakQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUM7b0JBQy9DLElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQ2hDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUM5QyxDQUFDO29CQUNGLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLFlBQVksQ0FBQztvQkFDOUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUM5RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxNQUFNLEdBQUcsWUFBWSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUseUNBQXlDO2lCQUMxQzthQUNGO1NBQ0Y7YUFBTSxJQUFJLE1BQU0sSUFBSSxnQkFBZ0IsRUFBRTtZQUNyQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLGNBQWMsRUFBRTtZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNsQzthQUFNLElBQUksTUFBTSxJQUFJLGtCQUFrQixFQUFFO1lBQ3ZDLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDdkIsSUFBSSxNQUFNLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztZQUMxQixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1lBQ2hELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDM0MsSUFBSSxRQUFRLEdBQUc7Z0JBQ2IsaUJBQWlCLEVBQUcsSUFBSTthQUN6QixDQUFDO1lBQ0YsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQztZQUNqRSxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUM7aUJBQy9ELFNBQVMsQ0FDUixHQUFHLENBQUMsRUFBRTtnQkFDSixJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUNsQixDQUFDLEVBQ0QsS0FBSyxDQUFDLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO1lBQ0osQ0FBQyxDQUNGLENBQUM7WUFFSiw0Q0FBNEM7WUFDNUMsNEJBQTRCO1lBQzVCLGdEQUFnRDtZQUNoRCxpREFBaUQ7WUFDakQsTUFBTTtZQUNOLEtBQUs7WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7U0FDcEM7YUFBTSxJQUFJLE1BQU0sSUFBSSxVQUFVLEVBQUU7WUFDL0IsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUM7WUFDdEQsSUFBSSxLQUFLLEdBQUc7Z0JBQ1YsUUFBUSxFQUFFLE9BQU8sQ0FBQyxHQUFHO2FBQ3RCLENBQUE7WUFDRCxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLEtBQUssRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDO2lCQUMzQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2hCLE1BQU0sRUFBRSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzdDLE1BQU0sSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3pDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN4QjtnQkFDRCxJQUFJLFlBQVksR0FBRyxpQkFBaUIsQ0FBQztnQkFDckMsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO2dCQUNwRCxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxDQUFDO1lBQ3BELENBQUMsQ0FBQyxDQUFDO1NBQ047YUFBTSxJQUFJLE1BQU0sSUFBSSxlQUFlLElBQUksTUFBTSxJQUFJLG1CQUFtQixFQUFFO1lBQ3JFLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7WUFDekQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ2pCLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFCLCtCQUErQjtZQUM3Qiw2QkFBNkI7WUFDL0Isb0JBQW9CO1lBQ3BCLG9CQUFvQjtZQUNwQix5QkFBeUI7WUFDekIsc0JBQXNCO1lBQ3RCLE1BQU07WUFDTixJQUFJO1lBQ0osSUFBSSxRQUFRLEdBQUc7Z0JBQ2IsTUFBTSxFQUFFLENBQUMsTUFBTSxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUs7Z0JBQ2xELE9BQU8sRUFBRSxRQUFRO2dCQUNqQixNQUFNLEVBQUUsT0FBTzthQUNoQixDQUFBO1lBQ0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksbUJBQW1CLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQztpQkFDL0QsU0FBUyxDQUNSLEdBQUcsQ0FBQyxFQUFFO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztZQUNKLENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtnQkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztZQUNKLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjthQUFNLElBQUksTUFBTSxJQUFJLFdBQVcsRUFBRTtZQUNoQyxNQUFNLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixDQUFDO1lBQ3pELElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUNqQixPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMxQixJQUFJLFFBQVEsR0FBRztnQkFDYixPQUFPLEVBQUUsUUFBUTtnQkFDakIsTUFBTSxFQUFFLE9BQU87Z0JBQ2YsTUFBTSxFQUFFLElBQUk7YUFDYixDQUFBO1lBQ0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksbUJBQW1CLEdBQUcsY0FBYyxDQUFDLFlBQVksQ0FBQztZQUN0RCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQztpQkFDL0QsU0FBUyxDQUNSLEdBQUcsQ0FBQyxFQUFFO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztZQUNKLENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtnQkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztZQUNKLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtJQUNILENBQUM7SUFFRCxZQUFZLENBQUMsVUFBVSxFQUFFLFVBQVU7UUFDakMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBRyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBQztZQUM3RCxJQUFJLHFCQUFxQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLENBQUM7WUFDL0QsSUFBSSxtQkFBbUIsR0FBRyxxQkFBcUIsQ0FBQyxZQUFZLENBQUM7WUFDN0QsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFVBQVUsQ0FBQyxHQUFHLENBQUM7WUFDbkMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFVBQVUsQ0FBQztZQUM3QixJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLEtBQUssRUFBRSxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQztpQkFDbEUsU0FBUyxDQUNSLEdBQUcsQ0FBQyxFQUFFO2dCQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztnQkFDRixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDbEIsQ0FBQyxFQUNELEtBQUssQ0FBQyxFQUFFO2dCQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FDRixDQUFDO1NBQ0w7SUFDSCxDQUFDO0lBRUQsY0FBYyxDQUFDLE9BQU8sRUFBRSxVQUFVO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDL0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQTtRQUMxQyxJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDbEIsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUVwRSxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQ3RELElBQUksVUFBVSxDQUFDLFlBQVksRUFBRTtZQUMzQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtnQkFDaEUsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTTtvQkFDL0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsUUFBUSxHQUFHO2dCQUNULGlCQUFpQixFQUFHLElBQUk7YUFDekIsQ0FBQztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUNqQztRQUVELElBQUksQ0FBQyxjQUFjO2FBQ2xCLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLEdBQUcsQ0FBQzthQUN6RSxTQUFTLENBQ1IsR0FBRyxDQUFDLEVBQUU7WUFDSixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7WUFDRixJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRTtnQkFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2FBQ2pEO1lBQ0QsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtZQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztRQUNKLENBQUMsQ0FDRixDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsRUFBRSxRQUFRLENBQUMsQ0FBQTtRQUNqRSxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLGNBQWMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7SUFHckUsQ0FBQztJQUNELGNBQWMsQ0FBQyxLQUFLLEVBQUUsT0FBTztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakUsT0FBTyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCO1lBQ3hDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYztZQUN0QyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1AsSUFBSSxjQUFjLEVBQUU7WUFDbEIsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsb0NBQW9DO2dCQUNwQyx1Q0FBdUM7Z0JBQ3ZDLDJCQUEyQjtnQkFDekIsSUFBRyxJQUFJLENBQUMsTUFBTSxFQUNkO29CQUNFLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7aUJBQ3REO3FCQUFLLElBQUcsSUFBSSxDQUFDLGNBQWMsRUFBQztvQkFDM0IsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDcEQ7cUJBQ0c7b0JBQ0YsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN6QztZQUNMLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFckQsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsR0FBRyxDQUFDO2lCQUMzRCxTQUFTLENBQ1IsR0FBRyxDQUFDLEVBQUU7Z0JBQ0osSUFBRyxPQUFPLENBQUMsTUFBTSxJQUFFLFFBQVEsSUFBSyxtQkFBbUIsQ0FBQyxNQUFNLEVBQzFEO29CQUNDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxNQUFNLENBQzNCLENBQ0YsQ0FBQztpQkFDRjtxQkFBSyxJQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUUsVUFBVSxJQUFLLG1CQUFtQixDQUFDLE9BQU8sRUFDbkU7b0JBQ0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2lCQUNGO3FCQUNEO29CQUNDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztpQkFDRjtnQkFDQSxJQUFHLGNBQWMsQ0FBQyxhQUFhLEVBQUM7b0JBQy9CLFVBQVUsQ0FBQyxHQUFHLEVBQUU7d0JBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDeEIsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUVSO1lBQ0osQ0FBQyxFQUNELEtBQUssQ0FBQyxFQUFFO2dCQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FDRixDQUFDO1NBQ0w7YUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUU7WUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUMxRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1NBQ3RFO0lBQ0gsQ0FBQztJQUNILFlBQVksQ0FBQyxPQUFPLEVBQUMsTUFBTSxFQUFDLE1BQU07UUFDOUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sRUFBQyxNQUFNLEVBQUMsTUFBTSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUNGLGlCQUFpQixDQUFDLE9BQU8sRUFBQyxNQUFNLEVBQUMsTUFBTTtRQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLCtDQUErQyxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUMsTUFBTSxFQUFDLGFBQWEsRUFBQyxNQUFNLENBQUMsQ0FBQztRQUNuSCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztRQUMvRCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDO1FBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUMsVUFBVSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTthQUMvQixJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDNUIsWUFBWSxFQUFFLElBQUk7WUFDbEIsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLHFCQUFxQjtZQUNqQyxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLE1BQU07Z0JBQ2QsU0FBUyxFQUFFLE9BQU87Z0JBQ2xCLFNBQVMsRUFBRSxTQUFTO2FBQ3JCO1NBQ0YsQ0FBQzthQUNELFdBQVcsRUFBRTthQUNiLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixFQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3JELElBQUcsUUFBUSxJQUFJLE1BQU0sQ0FBQyxjQUFjLEVBQUM7Z0JBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQTthQUMvQztpQkFBTTtnQkFDTixZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUN4QyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ3BEO1FBQ0osQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0MsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNO1FBQ3hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0NBQWtDLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDekQsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFDL0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTthQUM3QixJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDMUIsWUFBWSxFQUFFLElBQUk7WUFDbEIsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLHFCQUFxQjtZQUNqQyxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLE1BQU07Z0JBQ2QsU0FBUyxFQUFFLE9BQU87YUFDbkI7U0FDRixDQUFDO2FBQ0QsV0FBVyxFQUFFO2FBQ2IsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3BCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxvQkFBb0IsQ0FBQyxJQUFJO1FBQ3ZCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLE1BQU0sRUFBRTtZQUN4QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLFVBQVUsU0FBUztnQkFDbkQsU0FBUyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7WUFDNUIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1NBQzNCO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxFQUFFO2dCQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2dCQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksTUFBTSxFQUFFO29CQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7aUJBQ2hDO3FCQUFNO29CQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztpQkFDakM7YUFDRjtZQUVELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ25DLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUNELGNBQWMsQ0FBQyxJQUFJO1FBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDNUMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwRCxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksUUFBUSxFQUFFO1lBQ25DLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsQyx3REFBd0Q7U0FDekQ7YUFBTTtZQUNMLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNsQyx3REFBd0Q7U0FDekQ7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNqRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNoRCxJQUFJLENBQUMsWUFBWSxHQUFDLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQy9CLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN0QyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEtBQUssQ0FBQztZQUNwQyxJQUFJLENBQUMsYUFBYSxHQUFHLFdBQVcsQ0FBQztZQUNqQyxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztTQUN6QjtRQUNELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMxQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFDRCxZQUFZO1FBQ1YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDdkQsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1FBQ3RELHlFQUF5RTtRQUN6RSxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUV0QyxJQUFJLFFBQVEsR0FBRztZQUNiLE9BQU8sRUFBRSxRQUFRO1lBQ2pCLE1BQU0sRUFBRSxLQUFLO1NBQ2QsQ0FBQTtRQUNELElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7aUJBQ2pFLFNBQVMsQ0FDUixHQUFHLENBQUMsRUFBRTtnQkFDSixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xCLENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtnQkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xCLENBQUMsQ0FDRixDQUFDO1NBQ0g7SUFFSCxDQUFDO0lBQ0QsSUFBSTtRQUNGLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQix5RUFBeUU7UUFDekUsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQztRQUN6RCxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN0QyxJQUFJLFFBQVEsR0FBRztZQUNiLE1BQU0sRUFBRSxJQUFJO1lBQ1osT0FBTyxFQUFFLFFBQVE7WUFDakIsTUFBTSxFQUFFLEtBQUs7U0FDZCxDQUFBO1FBQ0QsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1FBRXRELElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsTUFBTSxDQUFDO2FBQy9ELFNBQVMsQ0FDUixHQUFHLENBQUMsRUFBRTtZQUNKLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztZQUNGLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNsQixDQUFDLEVBQ0QsS0FBSyxDQUFDLEVBQUU7WUFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDRixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxDQUNGLENBQUM7SUFDTixDQUFDO0lBRUQsTUFBTTtRQUNKLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixtQ0FBbUM7UUFDbkMseUVBQXlFO1FBQ3pFLE1BQU0sY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDekQsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsK0JBQStCLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFcEQsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1FBQ3RELElBQUksUUFBUSxHQUFHO1lBQ2IsTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUUsUUFBUTtZQUNqQixNQUFNLEVBQUUsS0FBSztTQUNkLENBQUE7UUFFRCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQzthQUMvRCxTQUFTLENBQ1IsR0FBRyxDQUFDLEVBQUU7WUFDSixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7WUFDRixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxFQUNELEtBQUssQ0FBQyxFQUFFO1lBQ04sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FDRixDQUFDO0lBQ04sQ0FBQzs7O1lBM2tFRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsdTJrQkFBZ0Q7O2FBRWpEOzs7O1lBbENRLDRCQUE0QjtZQVc1QixjQUFjO1lBQ2QsY0FBYztZQTVCckIsU0FBUztZQUtGLGVBQWU7WUFWdEIsaUJBQWlCOzRDQW1JZCxNQUFNLFNBQUMsYUFBYTs0Q0FFcEIsTUFBTSxTQUFDLFNBQVM7WUEvRlosYUFBYTs7O3VCQW1CbkIsS0FBSztxQkFDTCxLQUFLO3NCQUNMLEtBQUs7Z0NBQ0wsS0FBSzsyQkFDTCxTQUFTLFNBQUMsY0FBYztvQkFDeEIsU0FBUyxTQUFDLFVBQVU7cUNBQ3BCLE1BQU07K0JBQ04sTUFBTTt3QkF5SU4sU0FBUyxTQUFDLFdBQVc7Z0NBQ3JCLFNBQVMsU0FBQyxtQkFBbUI7O0FBeTdEaEMsTUFBTSxVQUFVLG1CQUFtQixDQUFDLE1BQWM7SUFDaEQsTUFBTSxZQUFZLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLG1DQUFtQztJQUM3RSxNQUFNLEtBQUssR0FBRyxJQUFJLFVBQVUsQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEQsT0FBTyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQzVELENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBWaWV3Q2hpbGQsXHJcbiAgSW5wdXQsXHJcbiAgT3V0cHV0LFxyXG4gIEV2ZW50RW1pdHRlcixcclxuICBJbmplY3QsXHJcbiAgT25Jbml0LFxyXG4gIENoYW5nZURldGVjdG9yUmVmXHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtcclxuICBNYXRQYWdpbmF0b3IsXHJcbiAgTWF0VGFibGVEYXRhU291cmNlLFxyXG4gIE1hdERpYWxvZyxcclxuICBNYXRDaGVja2JveCxcclxuICBNYXRUYWJsZSxcclxuICBNYXRUb29sdGlwTW9kdWxlXHJcbn0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsXCI7XHJcbmltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9zbmFja2Jhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgKiBhcyBGaWxlU2F2ZXIgZnJvbSBcImZpbGUtc2F2ZXJcIjtcclxuaW1wb3J0IHtcclxuICBGb3JtQnVpbGRlcixcclxuICBGb3JtQ29udHJvbCxcclxuICBGb3JtR3JvdXAsXHJcbiAgVmFsaWRhdG9ycyxcclxuICBGb3JtQXJyYXlcclxufSBmcm9tIFwiQGFuZ3VsYXIvZm9ybXNcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UgfSBmcm9tIFwiLi4vQGZ1c2Uvc2VydmljZXMvdHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2VcIjtcclxuLy8gaW1wb3J0IHsgbG9jYWxlIGFzIGVuZ2xpc2ggfSBmcm9tIFwiLi4vaTE4bi9lblwiO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gXCJsb2Rhc2hcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcbi8vIHJlcXVpcmUoJ2pxdWVyeScpXHJcbi8vIHJlcXVpcmUoJ2tlbmRvR3JpZCcpXHJcblxyXG4vLyAvPHJlZmVyZW5jZSBwYXRoPVwiLi4vanF1ZXJ5L2luZGV4XCIgLz5cclxuXHJcbi8vIGltcG9ydCAkIGZyb20gXCIuLi9KUXVlcnlcIjtcclxuXHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSBcIi4uL2NvbnRlbnQvY29udGVudC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL19zZXJ2aWNlcy9pbmRleFwiO1xyXG5pbXBvcnQgeyBNb2RlbExheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi9tb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSBcInJ4anMvU3ViamVjdFwiO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0IHsgU2VsZWN0aW9uTW9kZWwgfSBmcm9tIFwiQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zXCI7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IEdyaWREYXRhUmVzdWx0LCBQYWdlQ2hhbmdlRXZlbnQsU2VsZWN0QWxsQ2hlY2tib3hTdGF0ZSB9IGZyb20gJ0Bwcm9ncmVzcy9rZW5kby1hbmd1bGFyLWdyaWQnO1xyXG5pbXBvcnQgeyBHcm91cERlc2NyaXB0b3IsIERhdGFSZXN1bHQsIHByb2Nlc3MgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tZGF0YS1xdWVyeSc7XHJcbmltcG9ydCB7IGV2ZW50TmFtZXMgfSBmcm9tICdwcm9jZXNzJztcclxuaW1wb3J0IHsgZGF0YSB9IGZyb20gJ2pxdWVyeSc7XHJcbmltcG9ydCB7IGVsZW1lbnQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3NyYy9yZW5kZXIzJztcclxuaW1wb3J0IHsgQ29uZmlybURpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4uL2NvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLmNvbXBvbmVudCc7XHJcblxyXG5cclxuLy8gZGVjbGFyZSAgJDogYW55O1xyXG5cclxuXHJcbi8vIGRlY2xhcmUgdmFyICQgOiBKUXVlcnlTdGF0aWM7XHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcIm5ldy10YWJsZS1sYXlvdXRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL25ldy10YWJsZS1sYXlvdXQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vbmV3LXRhYmxlLWxheW91dC5jb21wb25lbnQuc2Nzc1wiXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTmV3VGFibGVMYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIHZpZXdGcm9tOiBhbnk7XHJcbiAgQElucHV0KCkgb25Mb2FkOiBhbnk7XHJcbiAgQElucHV0KCkgdGFibGVJZDogYW55O1xyXG4gIEBJbnB1dCgpIGRpc2FibGVQYWdpbmF0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgQFZpZXdDaGlsZChcInNlbGVjdEFsbEJveFwiKSBzZWxlY3RBbGxCb3g6IE1hdENoZWNrYm94O1xyXG4gIEBWaWV3Q2hpbGQoXCJNYXRUYWJsZVwiKSB0YWJsZTogTWF0VGFibGU8YW55PjtcclxuICBAT3V0cHV0KCkgY2hlY2tDbGlja0V2ZW50TWVzc2FnZSA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xyXG4gIEBPdXRwdXQoKSBhY3Rpb25DbGlja0V2ZW50ID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XHJcbiAgZW5hYmxlU2VsZWN0T3B0aW9uOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZW5hYmxlU2VhcmNoOiBib29sZWFuID0gZmFsc2U7XHJcbiAgdGFibGVEYXRhOiBhbnlbXSA9IFtdO1xyXG4gIG5ld1RhYmxlRGF0YTogYW55O1xyXG4gIGVuYWJsZUFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGRpc3BsYXllZENvbHVtbnM6IGFueTtcclxuICBjb2x1bW5zOiBhbnk7XHJcbiAgdGFibGVMaXN0OiBhbnk7XHJcbiAgY3VycmVudENvbmZpZ0RhdGE6IGFueTtcclxuICBjdXJyZW50RGF0YTogYW55O1xyXG4gIGV4cGFuZGVkRWxlbWVudDogYW55O1xyXG4gIC8vIEBWaWV3Q2hpbGQoJ3ZpZXdNZScsIHsgc3RhdGljOiBmYWxzZSB9KVxyXG4gIGRhdGFTb3VyY2U6IGFueTtcclxuICBsaW1pdDogbnVtYmVyID0gMTA7XHJcbiAgb2Zmc2V0OiBudW1iZXIgPSAwO1xyXG4gIGxlbmd0aDogbnVtYmVyO1xyXG4gIHNlbGVjdGVkRGF0YTogYW55ID0gW107XHJcbiAgaW5wdXREYXRhOiBhbnkgPSB7fTtcclxuICBxdWVyeVBhcmFtczogYW55O1xyXG4gIGRlZmF1bHREYXRhc291cmNlOiBhbnk7XHJcbiAgZGlhbG9nUmVmOiBhbnk7XHJcbiAgY3VycmVudFRhYmxlRGF0YTogYW55O1xyXG4gIGRhdGE6IGFueTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlTXNnID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlTW9kZWwgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xyXG4gIGlucHV0VmFsdWU6IGFueTtcclxuICBmcm9tRmllbGRWYWx1ZTogYW55O1xyXG4gIHRvRmllbGRWYWx1ZTogYW55O1xyXG4gIGN1cnJlbnRGaWx0ZXJlZFZhbHVlOiBhbnkgPSB7fTtcclxuICBvcHRpb25GaWx0ZXJkYXRhOiBhbnk7XHJcbiAgZW5hYmxlT3B0aW9uRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgb3B0aW9uRmlsdGVyRmllbGRzOiBhbnk7XHJcbiAgZW5hYmxlVUlmaWx0ZXI6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBlbmFibGVGaWx0ZXI6IGJvb2xlYW4gPSBmYWxzZTtcclxuICByZWRpcmVjdFVyaTogc3RyaW5nO1xyXG4gIHB1YmxpYyBncm91cEZpZWxkOiBHcm91cERlc2NyaXB0b3JbXSA9IFtdO1xyXG4gIHNlbGVjdEFsbE9wdGlvbjogYW55ID0gXCJub25lXCI7XHJcbiAgbm90aWZ5Y291bnQ6IG51bWJlcjtcclxuICBzZWxlY3RBbGxWYWx1ZTogYW55O1xyXG4gIGVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gIHNlbGVjdENvdW50OiBudW1iZXI7XHJcbiAgcmVhbG06IGFueTtcclxuICBjdXJyZW50VGVuYW50OiBhbnk7XHJcbiAgcHVibGljIGluZm8gPSB0cnVlO1xyXG4gIHB1YmxpYyB0eXBlOiBcIm51bWVyaWNcIiB8IFwiaW5wdXRcIiA9IFwibnVtZXJpY1wiO1xyXG4gIHB1YmxpYyBwYWdlU2l6ZXMgPSBbeyB0ZXh0OiAxMCwgdmFsdWU6IDEwIH0sIHsgdGV4dDogMjUsIHZhbHVlOiAyNSB9LCB7IHRleHQ6IDUwLCB2YWx1ZTogNTAgfSwgeyB0ZXh0OiAxMDAsIHZhbHVlOiAxMDAgfV07XHJcbiAgcHVibGljIHByZXZpb3VzTmV4dCA9IHRydWU7XHJcbiAgbm90aWZ5SWQ6IGFueSA9IHt9O1xyXG4gIGVuYWJsZUdyb3VwaW5nIDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHB1YmxpYyBzZWxlY3RBbGxJdGVtOiBTZWxlY3RBbGxDaGVja2JveFN0YXRlID0gXCJ1bmNoZWNrZWRcIjtcclxuICAvLyBzZWxlY3RPcHRpb246IGFueVtdID0gW3sgS2V5OiAnQWxsJywgTmFtZTogJ0FsbCcsIFZhbHVlOiAnQWxsJywgVHlwZTogJ1N0cmluZyd9LFxyXG4gIC8vIHsgS2V5OiAnUmVhZCcsIE5hbWU6ICdSZWFkJywgVmFsdWU6ICdyZWFkJywgVHlwZTogJ1N0cmluZyd9LFxyXG4gIC8vIHsgS2V5OiAnVW5yZWFkJywgTmFtZTogJ1VucmVhZCcsIFZhbHVlOiAndW5yZWFkJywgVHlwZTogJ1N0cmluZyd9LFxyXG4gIC8vIHsgS2V5OiAnTm9uZScsIE5hbWU6ICdOb25lJywgVmFsdWU6ICdOb25lJywgVHlwZTogJ1N0cmluZyd9ICBdO1xyXG4gIHNlbGVjdE9wdGlvbiA9IFt7IFwidmFsdWVcIjogXCJhbGxcIiwgXCJuYW1lXCI6IFwiQWxsXCIgfSxcclxuICB7IFwidmFsdWVcIjogdHJ1ZSwgXCJuYW1lXCI6IFwiUmVhZFwiIH0sXHJcbiAgeyBcInZhbHVlXCI6IGZhbHNlLCBcIm5hbWVcIjogXCJVbnJlYWRcIiB9LFxyXG4gIHsgXCJ2YWx1ZVwiOiBcIm5vbmVcIiwgXCJuYW1lXCI6IFwiTm9uZVwiIH1dO1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZTogRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgY29udGVudFNlcnZpY2U6IENvbnRlbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9tYXREaWFsb2c6IE1hdERpYWxvZyxcclxuICAgIHByaXZhdGUgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNoYW5nZURldGVjdG9yUmVmOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIEBJbmplY3QoXCJlbnZpcm9ubWVudFwiKSBwcml2YXRlIGVudmlyb25tZW50LFxyXG5cclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHByaXZhdGUgZW5nbGlzaCxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZVxyXG4gICkge1xyXG5cclxuICAgIHRoaXMucmVkaXJlY3RVcmkgPSB0aGlzLmVudmlyb25tZW50LnJlZGlyZWN0VXJpO1xyXG4gICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5tb2RlbENsb3NlTWVzc2FnZVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZU1vZGVsKSlcclxuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIj4+Pj5kYXRhXCIpXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERhdGEgPSBbXTtcclxuICAgICAgICBpZiAoZGF0YSAhPSAwKSB7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kVHJpZ2dlck5vdGlmaWNhdGlvbih7ZGF0YSA6IFwidHJpZ2dlclwifSlcclxuICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmREYXRhc291cmNlKFwibnVsbFwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnRhYmxlRGF0YSk7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5kYXRhU291cmNlKTtcclxuICAgICAgICAvLyBfLmZvckVhY2godGhpcy50YWJsZURhdGEsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAvLyAgIGl0ZW0uY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIH0pO1xyXG5cclxuICAgICAgICAvLyB0aGlzLmRhdGFTb3VyY2UuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICAvLyAgIG9iai5jaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuZGF0YVNvdXJjZS5kYXRhLCBcIl9pZFwiKTtcclxuICAgICAgICAvLyAvLyBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIC8vIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICAgICAgICB0aGlzLmRhdGEgPSBkYXRhO1xyXG4gICAgICAgIC8vIGlmKHRoaXMuZGF0YSA9PT0gJ2xpc3RWaWV3Jyl7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YSkge1xyXG4gICAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgfSk7XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLmdldE1lc3NhZ2UoKS5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlTXNnKSkuc3Vic2NyaWJlKG1lc3NhZ2UgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhtZXNzYWdlLFwiPj5tZXNzYWdlXCIpXHJcbiAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgKTtcclxuICAgICAgY29uc29sZS5sb2codGhpcyxcIj4+VEhJU1wiKVxyXG4gICAgICBpZiAoXHJcbiAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSAmJlxyXG4gICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEubGlzdFZpZXcgJiZcclxuICAgICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhLmxpc3RWaWV3LmVuYWJsZU5ld1RhYmxlTGF5b3V0XHJcbiAgICAgICkge1xyXG4gICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZVxyXG4gICAgICAuZ2V0VGFibGVIZWFkZXJVcGRhdGUoKVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZSkpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj4+PmRhdGFcIilcclxuICAgICAgICBpZiAoZGF0YS5kYXRhID09ICd1cGRhdGUnKSB7XHJcbiAgICAgICAgICB0aGlzLnVwZGF0ZVRhYmxlVmlldygpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkZJTFRFUlJSUlwiKTtcclxuICAgICAgICAgIHRoaXMuZW5hYmxlRmlsdGVyID0gIXRoaXMuZW5hYmxlRmlsdGVyO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG4gIEBWaWV3Q2hpbGQoXCJwYWdpbmF0b3JcIikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgQFZpZXdDaGlsZChcInNlbGVjdGVkUGFnaW5hdG9yXCIpIHNlbGVjdGVkUGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICAvLyB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlbGVjdEFsbFZhbHVlID0gXCJcIjtcclxuICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlbGVjdEFsbEl0ZW0gPSAndW5jaGVja2VkJztcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiLi4uLi4udGFibGUgdGhpc1wiKTtcclxuICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VcIik7XHJcbiAgICB0aGlzLnJlYWxtPWxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmVhbG1cIik7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICBvZmZzZXQ6IDAsXHJcbiAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSxcclxuICAgICAgcmVhbG06IHRoaXMucmVhbG1cclxuICAgIH07XHJcbiAgICB0aGlzLmN1cnJlbnRUZW5hbnQ9e1xyXG4gICAgICByZWFsbTogdGhpcy5yZWFsbSxcclxuICAgICAgdXNlcklkOiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcInVzZXJJZFwiKVxyXG4gICAgfVxyXG4gICAgLy8gIHRoaXMucGFnaW5hdG9yW1wicGFnZUluZGV4XCJdID0gMDtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgKTtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHRoaXMuZGF0YSBcIix0aGlzLmRhdGEpO1xyXG4gICAgaWYgKHRoaXMuZGF0YSA9PSAnbGlzdFZpZXcnIHx8IHRoaXMuZGF0YSA9PSAncmVmcmVzaFBhZ2UnIHx8IHRoaXMuZGF0YT09PTApIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0YT1bXTtcclxuICAgIH1cclxuICAgIHRoaXMuaW5wdXREYXRhW1wiZGF0YXNvdXJjZUlkXCJdID0gdGhpcy5kZWZhdWx0RGF0YXNvdXJjZTtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAvLyB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuICAgIHRoaXMuZnJvbUZpZWxkVmFsdWUgPSBuZXcgRm9ybUNvbnRyb2woW1wiXCJdKTtcclxuICAgIHRoaXMudG9GaWVsZFZhbHVlID0gbmV3IEZvcm1Db250cm9sKFtcIlwiXSk7XHJcbiAgICB0aGlzLm9uTG9hZERhdGEobnVsbCk7XHJcblxyXG4gIH1cclxuICBuZ0FmdGVyVmlld0luaXQoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIm5nQWZ0ZXJWaWV3SW5pdCBcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdGVkUGFnaW5hdG9yLCBcInNlbGVjdGVkUGFnPj4+PlwiKTtcclxuICAgIGlmICh0aGlzLmRhdGFTb3VyY2UgJiYgdGhpcy5zZWxlY3RlZFBhZ2luYXRvcikge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5zZWxlY3RlZFBhZ2luYXRvcjtcclxuICAgIH1cclxuXHJcbiAgfVxyXG4gIG5nT25EZXN0cm95KCkge1xyXG4gICAgdGhpcy51bnN1YnNjcmliZS5uZXh0KCk7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlTXNnLm5leHQoKTtcclxuICAgIHRoaXMudW5zdWJzY3JpYmVNb2RlbC5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICBncm91cENoYW5nZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiPj4+Pj4gR1JPVVAgRVZFTlRcIik7XHJcbiAgICB0aGlzLmdyb3VwRmllbGQgPSBldmVudDtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiPj4+PiBUSElTXCIpO1xyXG4gICAgbGV0IGFwaUZ1bmN0aW9uRGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5vbkdyb3VwaW5nRnVuY3Rpb247XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICBfLmZvckVhY2goYXBpRnVuY3Rpb25EZXRhaWxzLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgaWYgKHJlcXVlc3RJdGVtLmZyb21DdXJyZW50RGF0YSkge1xyXG4gICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmN1cnJlbnREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVxyXG4gICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLmRpcmVjdEFzc2lnbikge1xyXG4gICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkocmVxdWVzdEl0ZW0udmFsdWUpXHJcbiAgICAgICAgICA6IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkoc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdKVxyXG4gICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgXy5mb3JFYWNoKGV2ZW50LCBmdW5jdGlvbiAoZXZlbnRJdGVtKSB7XHJcbiAgICAgIHNlbGYucXVlcnlQYXJhbXNbZXZlbnRJdGVtLmZpZWxkXSA9IHRydWU7XHJcbiAgICB9KVxyXG4gICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAuZ2V0QWxsUmVwb25zZSh0aGlzLnF1ZXJ5UGFyYW1zLCBhcGlGdW5jdGlvbkRldGFpbHMuYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIC8vIGlmICggY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uZW5hYmxlTG9hZGVyKSB7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAvL31cclxuICAgICAgICB0aGlzLmVuYWJsZVNlYXJjaCA9IHRoaXMuY3VycmVudERhdGEuZW5hYmxlR2xvYmFsU2VhcmNoO1xyXG4gICAgICAgIGxldCBzZWxlY3RlZFRhYmxlSGVhZGVycyA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwic2VsZWN0ZWRUYWJsZUhlYWRlcnNcIik7ICAvLyBhZnRlciBnbG9iYWwgc2V0dGluZyBjaGFuZ2VcclxuICAgICAgICB0aGlzLmNvbHVtbnMgPSAhXy5pc0VtcHR5KHNlbGVjdGVkVGFibGVIZWFkZXJzKSA/IEpTT04ucGFyc2Uoc2VsZWN0ZWRUYWJsZUhlYWRlcnMpIDogdGhpcy5jb2x1bW5zO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IHRoaXMuY29sdW1uc1xyXG4gICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB2YWwuaXNBY3RpdmU7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICBsZXQgdGVtcEFycmF5ID0gW107XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHNlbGYsIFwiPj4+Pj4+Pj4+PlRISVNTU1NcIik7XHJcbiAgICAgICAgXy5mb3JFYWNoKGRhdGEucmVzcG9uc2VbYXBpRnVuY3Rpb25EZXRhaWxzLnJlc3BvbnNlXSwgZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIGl0ZW0gIT09IFwib2JqZWN0XCIpIHtcclxuICAgICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgICAgLy8gdGVtcE9ialtrZXlUb1NldF0gPSBpdGVtO1xyXG4gICAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChzZWxmLmN1cnJlbnRUYWJsZURhdGEuZGF0YVZpZXdGb3JtYXQsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICAgIGlmICh2aWV3SXRlbS5zdWJrZXkpIHtcclxuICAgICAgICAgICAgICAgIGlmICh2aWV3SXRlbS5hc3NpZ25GaXJzdEluZGV4dmFsKSB7XHJcbiAgICAgICAgICAgICAgICAgIGxldCByZXN1bHRWYWx1ZSA9ICghXy5pc0VtcHR5KGl0ZW1bdmlld0l0ZW0udmFsdWVdKSAmJiBpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA/IChpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICAgID8gcmVzdWx0VmFsdWVcclxuICAgICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gaXRlbVt2aWV3SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgICA/IGl0ZW1bdmlld0l0ZW0udmFsdWVdW3ZpZXdJdGVtLnN1YmtleV1cclxuICAgICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmICh2aWV3SXRlbS5pc0NvbmRpdGlvbikge1xyXG4gICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGV2YWwodmlld0l0ZW0uY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZpZXdJdGVtLmlzQWRkRGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gdmlld0l0ZW0udmFsdWU7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgIGlmIChzZWxmLmN1cnJlbnRUYWJsZURhdGEubG9hZExhYmVsRnJvbUNvbmZpZykge1xyXG4gICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9XHJcbiAgICAgICAgICAgICAgICAgIHNlbGYuY3VycmVudFRhYmxlRGF0YS5oZWFkZXJMaXN0W2l0ZW1bdmlld0l0ZW0udmFsdWVdXTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBsZXQgZmlsdGVyT2JqID0ge307XHJcbiAgICAgICAgICAgIGZpbHRlck9ialtzZWxmLmN1cnJlbnRUYWJsZURhdGEuZmlsdGVyS2V5XSA9IGl0ZW1bc2VsZi5jdXJyZW50VGFibGVEYXRhLmRhdGFGaWx0ZXJLZXldO1xyXG4gICAgICAgICAgICBsZXQgc2VsZWN0ZWRWYWx1ZSA9IF8uZmluZChzZWxmLmlucHV0RGF0YVtzZWxmLmN1cnJlbnRUYWJsZURhdGEuc2VsZWN0ZWRWYWx1ZXNLZXldLCBmaWx0ZXJPYmopO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZFZhbHVlLCBcIj4+Pj4+c2VsZWN0ZWRWYWx1ZVwiKVxyXG4gICAgICAgICAgICBpZiAoc2VsZWN0ZWRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgIGl0ZW0uY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGVtcE9iaiA9IHsgLi4uaXRlbSwgLi4udGVtcE9iaiB9O1xyXG4gICAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAodGVtcEFycmF5ICYmIHRlbXBBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAgIHRoaXMudGFibGVEYXRhID0gdGVtcEFycmF5O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnRhYmxlRGF0YSA9XHJcbiAgICAgICAgICAgIGRhdGEgJiYgZGF0YS5yZXNwb25zZSAmJiBkYXRhLnJlc3BvbnNlW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV1cclxuICAgICAgICAgICAgICA/IChkYXRhLnJlc3BvbnNlW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV0pXHJcbiAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5yZXNwb25zZSkge1xyXG4gICAgICAgICAgdGhpcy5sZW5ndGggPSBkYXRhLnJlc3BvbnNlLnRvdGFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNvbnN0cnVjdEdyb3VwZWREYXRhKGRhdGEucmVzcG9uc2UsIGFwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZSlcclxuICAgICAgICAvLyBsZXQgY29uY2F0RGF0YSA9IHRoaXMudGFibGVEYXRhWzBdLmRhdGEuY29uY2F0KHRoaXMudGFibGVEYXRhWzFdLmRhdGEpXHJcblxyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy50YWJsZURhdGEpO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnNlbGVjdGVkUGFnaW5hdG9yOyAvLyBuZXdseSBhZGRlZCBcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gZmFsc2U7XHJcbiAgICAgICAgdmFyIGZpbHRlcmVkS2V5ID0gKHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoS2V5KSA/IHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoS2V5IDogJyc7XHJcbiAgICAgICAgXy5mb3JFYWNoKHNlbGYuY29sdW1ucywgZnVuY3Rpb24gKHgpIHtcclxuICAgICAgICAgIGlmIChmaWx0ZXJlZEtleSA9PSB4LnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3gua2V5VG9TYXZlXSA9IHNlbGYuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YVt4LmtleVRvU2F2ZV0gPSBudWxsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIC8vIHRoaXMubG9hZEtlbmRvR3JpZCgpO1xyXG4gICAgICAgIC8vIHRoaXMuc2VsZWN0QWxsQ2hlY2soeyBjaGVja2VkOiBmYWxzZSB9KTtcclxuICAgICAgICAvLyB0aGlzLnNlbGVjdEFsbCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0QWxsLCBcIlNlbGVjdEFsbD4+Pj4+Pj5cIik7XHJcbiAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgICAvLyB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gW107XHJcbiAgICAgICAgLy8gbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdEFsbCwgXCJTZWxlY3RBbGw+Pj4+Pj4+XCIpO1xyXG4gICAgICAgIC8vIHRoaXMuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xyXG5cclxuICAgICAgICB0aGlzLmNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgICAgIH0sXHJcbiAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIiBFcnJvciBcIiwgZXJyKTtcclxuICAgICAgICB9KTtcclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdEdyb3VwZWREYXRhKHJlc3BvbnNlLCByZXNwb25zZU5hbWUpIHtcclxuICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLCBcIj4+cmVzcG9uc2VcIik7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4gY3VyZW50dGFibGUgRGF0YSBcIiwgdGhpcy5jdXJyZW50VGFibGVEYXRhKTtcclxuICAgIHRoaXMudGFibGVEYXRhID0gW107XHJcbiAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICBfLmZvckVhY2gocmVzcG9uc2VbcmVzcG9uc2VOYW1lXSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgXy5mb3JFYWNoKGl0ZW0uZGF0YSwgZnVuY3Rpb24gKGRhdGFJdGVtLCBpbmRleCkge1xyXG4gICAgICAgIGxldCBvYmogPSBkYXRhSXRlbTtcclxuICAgICAgICBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgIG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucmlza1Jhbmtpbmc7XHJcbiAgICAgICAgb2JqW1wiY29udHJvbFR5cGVcIl0gPSByZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5jb250cm9sVHlwZTtcclxuICAgICAgICBvYmpbXCJjb250cm9sRGVzY3JpcHRpb25cIl0gPSByZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sRGVzY3JpcHRpb247XHJcbiAgICAgICAgb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gcmVzcG9uc2UuYWNjZXNzR3JvdXBzW2RhdGFJdGVtLmFjY2Vzc0dyb3VwXS5hY2Nlc3NHcm91cE5hbWU7XHJcbiAgICAgICAgb2JqW1wicnVsZXNldE5hbWVcIl0gPSByZXNwb25zZS5ydWxlc2V0c1tyZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5ydWxlc2V0SWRdLm5hbWU7XHJcbiAgICAgICAgb2JqW1wiZGF0YXNvdXJjZU5hbWVcIl0gPSByZXNwb25zZS5kYXRhc291cmNlc1tkYXRhSXRlbS5kYXRhc291cmNlXS5uYW1lO1xyXG4gICAgICAgIG9ialtcInN0YXR1c0NsYXNzXCJdID0ge1xyXG4gICAgICAgICAgXCJPcGVuXCI6IFwibGFiZWwgYmctaW50aWF0ZWRcIixcclxuICAgICAgICAgIFwiQ2xvc2VkXCI6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICBcIlJlbWVkaWF0ZWRcIiA6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICBcIkF1dGhvcml6ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCIsXHJcbiAgICAgICAgICBcIkFwcHJvdmVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiXHJcbiAgICAgIH07XHJcbiAgICAgICAgc2VsZi50YWJsZURhdGEucHVzaChvYmopO1xyXG4gICAgICAgIGlmIChpdGVtLmRhdGEubGVuZ3RoID09IDEwICYmIGluZGV4ID09ICc5Jykge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJMT0FEIE1PUkVcIik7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhzZWxmLnRhYmxlRGF0YS5sZW5ndGgsIFwiPj4+PnNlbGYudGFibGVEYXRhLmxlbmd0aFwiKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+Pj4gSU5ERVhcIik7XHJcbiAgICAgICAgICBsZXQgdGVtcG9iaiA9IHt9O1xyXG4gICAgICAgICAgdGVtcG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IHJlc3BvbnNlLmFjY2Vzc0dyb3Vwc1tkYXRhSXRlbS5hY2Nlc3NHcm91cF0uYWNjZXNzR3JvdXBOYW1lO1xyXG4gICAgICAgICAgdGVtcG9ialtcInVzZXJOYW1lXCJdID0gZGF0YUl0ZW0udXNlck5hbWU7XHJcbiAgICAgICAgICB0ZW1wb2JqW1wicm9sZU5hbWVcIl0gPSBkYXRhSXRlbS5yb2xlTmFtZTtcclxuICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0dyb3VwJ10gPSBkYXRhSXRlbS5hY2Nlc3NHcm91cDtcclxuICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0NvbnRyb2wnXSA9IGRhdGFJdGVtLmFjY2Vzc0NvbnRyb2w7XHJcbiAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgIHRlbXBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uY29udHJvbFR5cGU7XHJcbiAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbERlc2NyaXB0aW9uXCJdID0gcmVzcG9uc2UuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbERlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgdGVtcG9ialtcIl9pZFwiXSA9IHJlc3BvbnNlLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLl9pZDtcclxuICAgICAgICAgIHRlbXBvYmpbXCJlbmFibGVMb2FkXCJdID0gdHJ1ZTtcclxuICAgICAgICAgIHRlbXBvYmpbXCJzdGF0dXNDbGFzc1wiXSA9IHtcclxuICAgICAgICAgICAgXCJPcGVuXCI6IFwibGFiZWwgYmctaW50aWF0ZWRcIixcclxuICAgICAgICAgICAgXCJDbG9zZWRcIjogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgXCJSZW1lZGlhdGVkXCIgOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICBcIkF1dGhvcml6ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCIsXHJcbiAgICAgICAgICAgIFwiQXBwcm92ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCJcclxuICAgICAgICB9O1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHNlbGYuY3VycmVudFRhYmxlRGF0YS5wcm9kdWN0S2V5cywgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgIHRlbXBvYmpbdmlld0l0ZW0ubmFtZV0gPSBkYXRhSXRlbVt2aWV3SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+IHRlbXBvYmogXCIsIHRlbXBvYmopO1xyXG4gICAgICAgICAgc2VsZi50YWJsZURhdGEucHVzaCh0ZW1wb2JqKTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG5cclxuICAgIH0pXHJcbiAgICB0aGlzLm5ld1RhYmxlRGF0YSA9IHByb2Nlc3ModGhpcy50YWJsZURhdGEsIHsgZ3JvdXA6IHRoaXMuZ3JvdXBGaWVsZCB9KTtcclxuICAgIHRoaXMubmV3VGFibGVEYXRhLnRvdGFsID0gcmVzcG9uc2UudG90YWw7XHJcbiAgICAvLyB0aGlzLm5vdGlmeWNvdW50ID0gdGhpcy5uZXdUYWJsZURhdGEudG90YWw7XHJcbiAgICAvLyBfLmZvckVhY2godGhpcy5uZXdUYWJsZURhdGEuZGF0YSxmdW5jdGlvbihpdGVtVmFsLCBpbmRleCl7XHJcbiAgICAvLyAgIGl0ZW1WYWwuY29sbGFwc2VHcm91cChpbmRleC50b1N0cmluZygpKVxyXG4gICAgLy8gfSApXHJcbiAgICAvLyAgIGZvciAobGV0IG0gPSAwOyBtIDwgNTsgbSA9IG0gKyAxKSB7XHJcbiAgICAvLyAgICAgdGhpcy5uZXdUYWJsZURhdGEuY29sbGFwc2VHcm91cChtLnRvU3RyaW5nKCkpO1xyXG4gICAgLy8gfVxyXG4gICAgLy8gICB0aGlzLm5ld1RhYmxlRGF0YS5maW5kKFwiLmstZ3JvdXBpbmctcm93XCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG4gICAgLy8gICAgIHRoaXMubmV3VGFibGVEYXRhLmNvbGxhcHNlR3JvdXAodGhpcyk7XHJcbiAgICAvLyB9KTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiPj4+IFRISVNcIilcclxuICAgIGNvbnNvbGUubG9nKHRoaXMubmV3VGFibGVEYXRhLCBcIj4+PiBUQUJMRSBEQVQgTkVXXCIpXHJcbiAgfVxyXG5cclxuICBvblJvdXRpbmdDbGljayhzZWxlY3RlZFJvdykge1xyXG4gICAgaWYgKHRoaXMuY3VycmVudERhdGEgJiYgdGhpcy5jdXJyZW50RGF0YS5lbmFibGVSb3V0aW5nKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhID0geyAuLi50aGlzLmlucHV0RGF0YSwgLi4uc2VsZWN0ZWRSb3cgfTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZ01lc3NhZ2Uoe1xyXG4gICAgICAgIHNlbGVjdGVkUm93OiBzZWxlY3RlZFJvdyxcclxuICAgICAgICBoaWRlVmlldzogdGhpcy5jdXJyZW50RGF0YS5oaWRlVmlld0xheW91dHNcclxuICAgICAgfSlcclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNvbGxhcHNlR3JvdXBzKGdyaWQpIHtcclxuICAgIHRoaXMubmV3VGFibGVEYXRhLmRhdGEuZm9yRWFjaCgoZ3IsIGlkeCkgPT4gZ3JpZC5jb2xsYXBzZUdyb3VwKGlkeC50b1N0cmluZygpKSk7XHJcbiAgfVxyXG5cclxuICBsb2FkTW9yZUNsaWNrKHJvdykge1xyXG4gICAgY29uc29sZS5sb2cocm93LCBcIj4+Pj4+Pj4gUk9XXCIpXHJcbiAgICBsZXQgYXBpRnVuY3Rpb25EZXRhaWxzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLm9uR3JvdXBpbmdGdW5jdGlvbjtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMucXVlcnlQYXJhbXMsIFwiPj4+IFFVRVJZIFBBUkFNU1wiKVxyXG4gICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4gVEhJU1wiKVxyXG4gICAgbGV0IGluZGV4VmFsID0gLTE7XHJcbiAgICBsZXQgcXVlcnlPYmogPSB0aGlzLnF1ZXJ5UGFyYW1zO1xyXG4gICAgbGV0IGdyb3VwaW5nRmllbGQgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuZ3JvdXBpbmdEZXRhaWw7XHJcblxyXG4gICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiB0aGlzLmdyb3VwRmllbGQubGVuZ3RoID09IDEpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2goZ3JvdXBpbmdGaWVsZCwgZnVuY3Rpb24gKGdyb3VwSXRlbSkge1xyXG4gICAgICAgIGlmIChzZWxmLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgoc2VsZi5ncm91cEZpZWxkLCB7IGZpZWxkOiBncm91cEl0ZW0ubmFtZSB9KSA+PSAwKSB7XHJcbiAgICAgICAgICBpZiAoZ3JvdXBJdGVtLmlzSWQpIHtcclxuICAgICAgICAgICAgcXVlcnlPYmpbJ2xvYWRNb3JlJyArIGdyb3VwSXRlbS5uYW1lXSA9IHJvd1tncm91cEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcXVlcnlPYmpbJ2xvYWRNb3JlJyArIGdyb3VwSXRlbS5uYW1lXSA9IHJvd1tncm91cEl0ZW0ubmFtZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpbmRleFZhbCA9IF8uZmluZEluZGV4KHNlbGYubmV3VGFibGVEYXRhLmRhdGEsIHsgdmFsdWU6IHJvd1tncm91cEl0ZW0ubmFtZV0gfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgICBxdWVyeU9iai5sb2FkTW9yZU9mZnNldCA9ICh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcy5sZW5ndGggPiAxMCkgPyB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcy5sZW5ndGggLSAxIDogMTA7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0QWxsUmVwb25zZShxdWVyeU9iaiwgYXBpRnVuY3Rpb25EZXRhaWxzLmFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj5kYXRhXCIpO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlRGF0YSA9IGRhdGEucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgY29udHJvbEl0ZW1WYWx1ZSA9IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zO1xyXG4gICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wb3AoKTtcclxuICAgICAgICAgIGlmIChyZXNwb25zZURhdGFbYXBpRnVuY3Rpb25EZXRhaWxzLnJlc3BvbnNlXSkge1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVzcG9uc2VEYXRhW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV1bMF0uZGF0YSwgZnVuY3Rpb24gKGRhdGFJdGVtLCBpbmRleCkge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+PmluZGV4XCIpXHJcbiAgICAgICAgICAgICAgbGV0IG9iaiA9IGRhdGFJdGVtO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmNvbnRyb2xUeXBlO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xEZXNjcmlwdGlvblwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sRGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgICAgb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0dyb3Vwc1tkYXRhSXRlbS5hY2Nlc3NHcm91cF0uYWNjZXNzR3JvdXBOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJ1bGVzZXROYW1lXCJdID0gcmVzcG9uc2VEYXRhLnJ1bGVzZXRzW3Jlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5ydWxlc2V0SWRdLm5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wiZGF0YXNvdXJjZU5hbWVcIl0gPSByZXNwb25zZURhdGEuZGF0YXNvdXJjZXNbZGF0YUl0ZW0uZGF0YXNvdXJjZV0ubmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJzdGF0dXNDbGFzc1wiXSA9IHtcclxuICAgICAgICAgICAgICAgIFwiT3BlblwiOiBcImxhYmVsIGJnLWludGlhdGVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIkNsb3NlZFwiOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJSZW1lZGlhdGVkXCIgOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJBdXRob3JpemVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiLFxyXG4gICAgICAgICAgICAgICAgXCJBcHByb3ZlZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaChvYmopO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoLCBcIj4+Pj4gY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGhcIilcclxuICAgICAgICAgICAgICBpZiAoY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGggJSAxMCA9PSAwICYmIGluZGV4ID09ICc5Jykge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJMT0FEIE1PUkVcIilcclxuICAgICAgICAgICAgICAgIGxldCB0ZW1wb2JqID0ge307XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqWydhY2Nlc3NHcm91cCddID0gZGF0YUl0ZW0uYWNjZXNzR3JvdXA7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqWydhY2Nlc3NDb250cm9sJ10gPSBkYXRhSXRlbS5hY2Nlc3NDb250cm9sO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmNvbnRyb2xUeXBlO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImNvbnRyb2xEZXNjcmlwdGlvblwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sRGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wicm9sZU5hbWVcIl0gPSBkYXRhSXRlbS5yb2xlTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSBkYXRhSXRlbS5lbnRpdGxlbWVudE5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiX2lkXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLl9pZDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJlbmFibGVMb2FkXCJdID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJzdGF0dXNDbGFzc1wiXSA9IHtcclxuICAgICAgICAgICAgICAgICAgXCJPcGVuXCI6IFwibGFiZWwgYmctaW50aWF0ZWRcIixcclxuICAgICAgICAgICAgICAgICAgXCJDbG9zZWRcIjogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgICAgXCJSZW1lZGlhdGVkXCIgOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgICBcIkF1dGhvcml6ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiQXBwcm92ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCJcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgXy5mb3JFYWNoKHNlbGYuY3VycmVudFRhYmxlRGF0YS5wcm9kdWN0S2V5cywgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBvYmpbdmlld0l0ZW0ubmFtZV0gPSBkYXRhSXRlbVt2aWV3SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaCh0ZW1wb2JqKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGNvbnNvbGUubG9nKGNvbnRyb2xJdGVtVmFsdWUsIFwiPj5jb250cm9sSXRlbVZhbHVlXCIpO1xyXG4gICAgICAgICAgdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtpbmRleFZhbF0uaXRlbXMgPSBjb250cm9sSXRlbVZhbHVlO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiBFcnJvciBcIiwgZXJyKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmdyb3VwRmllbGQgJiYgdGhpcy5ncm91cEZpZWxkLmxlbmd0aCA9PSAyKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgbGV0IGZpcnN0SW5kZXhWYWwgPSAtMTtcclxuICAgICAgXy5mb3JFYWNoKGdyb3VwaW5nRmllbGQsIGZ1bmN0aW9uIChncm91cEl0ZW0pIHtcclxuICAgICAgICBpZiAoc2VsZi5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHNlbGYuZ3JvdXBGaWVsZCwgeyBmaWVsZDogZ3JvdXBJdGVtLm5hbWUgfSkgPj0gMCkge1xyXG4gICAgICAgICAgaWYgKGdyb3VwSXRlbS5pc0lkKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5T2JqWydsb2FkTW9yZScgKyBncm91cEl0ZW0ubmFtZV0gPSByb3dbZ3JvdXBJdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5T2JqWydsb2FkTW9yZScgKyBncm91cEl0ZW0ubmFtZV0gPSByb3dbZ3JvdXBJdGVtLm5hbWVdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIF8uZm9yRWFjaChncm91cGluZ0ZpZWxkLCBmdW5jdGlvbiAoZ3JvdXBJdGVtKSB7XHJcbiAgICAgICAgaWYgKHNlbGYuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleChzZWxmLmdyb3VwRmllbGQsIHsgZmllbGQ6IGdyb3VwSXRlbS5uYW1lIH0pID09IDApIHtcclxuICAgICAgICAgIGZpcnN0SW5kZXhWYWwgPSBfLmZpbmRJbmRleChzZWxmLm5ld1RhYmxlRGF0YS5kYXRhLCB7IHZhbHVlOiByb3dbZ3JvdXBJdGVtLm5hbWVdIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGxldCBzZWNvbmRJbmRleFZhbCA9IC0xO1xyXG4gICAgICBfLmZvckVhY2goZ3JvdXBpbmdGaWVsZCwgZnVuY3Rpb24gKGdyb3VwSXRlbSkge1xyXG4gICAgICAgIGlmIChzZWxmLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgoc2VsZi5ncm91cEZpZWxkLCB7IGZpZWxkOiBncm91cEl0ZW0ubmFtZSB9KSA9PSAxKSB7XHJcbiAgICAgICAgICBzZWNvbmRJbmRleFZhbCA9IF8uZmluZEluZGV4KHNlbGYubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXMsIHsgdmFsdWU6IHJvd1tncm91cEl0ZW0ubmFtZV0gfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2coZmlyc3RJbmRleFZhbCwgXCI+Pj4gZmlyc3RJbmRleFZhbFwiKTtcclxuICAgICAgY29uc29sZS5sb2coc2Vjb25kSW5kZXhWYWwsIFwiPj4+Pj4gc2Vjb25kSW5kZXhWYWxcIik7XHJcbiAgICAgIHF1ZXJ5T2JqLmxvYWRNb3JlT2Zmc2V0ID0gKHRoaXMubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXNbc2Vjb25kSW5kZXhWYWxdLml0ZW1zLmxlbmd0aCA+IDEwKSA/IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXNbc2Vjb25kSW5kZXhWYWxdLml0ZW1zLmxlbmd0aCAtIDEgOiAxMDtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5T2JqLCBhcGlGdW5jdGlvbkRldGFpbHMuYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIj4+PmRhdGFcIik7XHJcbiAgICAgICAgICBsZXQgcmVzcG9uc2VEYXRhID0gZGF0YS5yZXNwb25zZTtcclxuICAgICAgICAgIGxldCBjb250cm9sSXRlbVZhbHVlID0gdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtmaXJzdEluZGV4VmFsXS5pdGVtc1tzZWNvbmRJbmRleFZhbF0uaXRlbXM7XHJcbiAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnBvcCgpO1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlRGF0YVthcGlGdW5jdGlvbkRldGFpbHMucmVzcG9uc2VdKSB7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChyZXNwb25zZURhdGFbYXBpRnVuY3Rpb25EZXRhaWxzLnJlc3BvbnNlXVswXS5kYXRhLCBmdW5jdGlvbiAoZGF0YUl0ZW0sIGluZGV4KSB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coaW5kZXgsIFwiPj4+aW5kZXhcIilcclxuICAgICAgICAgICAgICBsZXQgb2JqID0gZGF0YUl0ZW07XHJcbiAgICAgICAgICAgICAgb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wicmlza1JhbmtpbmdcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucmlza1Jhbmtpbmc7XHJcbiAgICAgICAgICAgICAgb2JqW1wiY29udHJvbFR5cGVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uY29udHJvbFR5cGU7XHJcbiAgICAgICAgICAgICAgb2JqW1wiY29udHJvbERlc2NyaXB0aW9uXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xEZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgICBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzR3JvdXBzW2RhdGFJdGVtLmFjY2Vzc0dyb3VwXS5hY2Nlc3NHcm91cE5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wicnVsZXNldE5hbWVcIl0gPSByZXNwb25zZURhdGEucnVsZXNldHNbcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJ1bGVzZXRJZF0ubmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJkYXRhc291cmNlTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5kYXRhc291cmNlc1tkYXRhSXRlbS5kYXRhc291cmNlXS5uYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInN0YXR1c0NsYXNzXCJdID0ge1xyXG4gICAgICAgICAgICAgICAgXCJPcGVuXCI6IFwibGFiZWwgYmctaW50aWF0ZWRcIixcclxuICAgICAgICAgICAgICAgIFwiQ2xvc2VkXCI6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIlJlbWVkaWF0ZWRcIiA6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICBcIkF1dGhvcml6ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCIsXHJcbiAgICAgICAgICAgICAgICBcIkFwcHJvdmVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wdXNoKG9iaik7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGgsIFwiPj4+PiBjb250cm9sSXRlbVZhbHVlLmxlbmd0aFwiKVxyXG4gICAgICAgICAgICAgIGlmIChjb250cm9sSXRlbVZhbHVlLmxlbmd0aCAlIDEwID09IDAgJiYgaW5kZXggPT0gJzknKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkxPQUQgTU9SRVwiKVxyXG4gICAgICAgICAgICAgICAgbGV0IHRlbXBvYmogPSB7fTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0dyb3VwJ10gPSBkYXRhSXRlbS5hY2Nlc3NHcm91cDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbJ2FjY2Vzc0NvbnRyb2wnXSA9IGRhdGFJdGVtLmFjY2Vzc0NvbnRyb2w7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wicmlza1JhbmtpbmdcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucmlza1Jhbmtpbmc7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbFR5cGVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uY29udHJvbFR5cGU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiY29udHJvbERlc2NyaXB0aW9uXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xEZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJyb2xlTmFtZVwiXSA9IGRhdGFJdGVtLnJvbGVOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInVzZXJOYW1lXCJdID0gZGF0YUl0ZW0udXNlck5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gZGF0YUl0ZW0uZW50aXRsZW1lbnROYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcIl9pZFwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5faWQ7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiZW5hYmxlTG9hZFwiXSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goc2VsZi5jdXJyZW50VGFibGVEYXRhLnByb2R1Y3RLZXlzLCBmdW5jdGlvbiAodmlld0l0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcG9ialt2aWV3SXRlbS5uYW1lXSA9IGRhdGFJdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInN0YXR1c0NsYXNzXCJdID0ge1xyXG4gICAgICAgICAgICAgICAgICBcIk9wZW5cIjogXCJsYWJlbCBiZy1pbnRpYXRlZFwiLFxyXG4gICAgICAgICAgICAgICAgICBcIkNsb3NlZFwiOiBcImxhYmVsIGJnLWZhaWxlZFwiLFxyXG4gICAgICAgICAgICAgICAgICBcIlJlbWVkaWF0ZWRcIiA6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiQXV0aG9yaXplZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgICAgXCJBcHByb3ZlZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIlxyXG4gICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2godGVtcG9iaik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLCBcIj4+Y29udHJvbEl0ZW1WYWx1ZVwiKTtcclxuICAgICAgICAgIHRoaXMubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXNbc2Vjb25kSW5kZXhWYWxdLml0ZW1zID0gY29udHJvbEl0ZW1WYWx1ZTtcclxuICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgICB9KTtcclxuICAgIH0gZWxzZSBpZiAodGhpcy5ncm91cEZpZWxkICYmIHRoaXMuZ3JvdXBGaWVsZC5sZW5ndGggPT0gMykge1xyXG4gICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCBmaXJzdEluZGV4VmFsID0gLTE7XHJcbiAgICAgIF8uZm9yRWFjaChncm91cGluZ0ZpZWxkLCBmdW5jdGlvbiAoZ3JvdXBJdGVtKSB7XHJcbiAgICAgICAgaWYgKHNlbGYuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleChzZWxmLmdyb3VwRmllbGQsIHsgZmllbGQ6IGdyb3VwSXRlbS5uYW1lIH0pID49IDApIHtcclxuICAgICAgICAgIGlmIChncm91cEl0ZW0uaXNJZCkge1xyXG4gICAgICAgICAgICBxdWVyeU9ialsnbG9hZE1vcmUnICsgZ3JvdXBJdGVtLm5hbWVdID0gcm93W2dyb3VwSXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBxdWVyeU9ialsnbG9hZE1vcmUnICsgZ3JvdXBJdGVtLm5hbWVdID0gcm93W2dyb3VwSXRlbS5uYW1lXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBfLmZvckVhY2goZ3JvdXBpbmdGaWVsZCwgZnVuY3Rpb24gKGdyb3VwSXRlbSkge1xyXG4gICAgICAgIGlmIChzZWxmLmdyb3VwRmllbGQgJiYgXy5maW5kSW5kZXgoc2VsZi5ncm91cEZpZWxkLCB7IGZpZWxkOiBncm91cEl0ZW0ubmFtZSB9KSA9PSAwKSB7XHJcbiAgICAgICAgICBmaXJzdEluZGV4VmFsID0gXy5maW5kSW5kZXgoc2VsZi5uZXdUYWJsZURhdGEuZGF0YSwgeyB2YWx1ZTogcm93W2dyb3VwSXRlbS5uYW1lXSB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBsZXQgc2Vjb25kSW5kZXhWYWwgPSAtMTtcclxuICAgICAgXy5mb3JFYWNoKGdyb3VwaW5nRmllbGQsIGZ1bmN0aW9uIChncm91cEl0ZW0pIHtcclxuICAgICAgICBpZiAoc2VsZi5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHNlbGYuZ3JvdXBGaWVsZCwgeyBmaWVsZDogZ3JvdXBJdGVtLm5hbWUgfSkgPT0gMSkge1xyXG4gICAgICAgICAgc2Vjb25kSW5kZXhWYWwgPSBfLmZpbmRJbmRleChzZWxmLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zLCB7IHZhbHVlOiByb3dbZ3JvdXBJdGVtLm5hbWVdIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGxldCB0aGlyZEluZGV4VmFsID0gLTE7XHJcbiAgICAgIF8uZm9yRWFjaChncm91cGluZ0ZpZWxkLCBmdW5jdGlvbiAoZ3JvdXBJdGVtKSB7XHJcbiAgICAgICAgaWYgKHNlbGYuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleChzZWxmLmdyb3VwRmllbGQsIHsgZmllbGQ6IGdyb3VwSXRlbS5uYW1lIH0pID09IDIpIHtcclxuICAgICAgICAgIHRoaXJkSW5kZXhWYWwgPSBfLmZpbmRJbmRleChzZWxmLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtcywgeyB2YWx1ZTogcm93W2dyb3VwSXRlbS5uYW1lXSB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBjb25zb2xlLmxvZyhmaXJzdEluZGV4VmFsLCBcIj4+PiBmaXJzdEluZGV4VmFsXCIpO1xyXG4gICAgICBjb25zb2xlLmxvZyhzZWNvbmRJbmRleFZhbCwgXCI+Pj4+PiBzZWNvbmRJbmRleFZhbFwiKTtcclxuICAgICAgY29uc29sZS5sb2codGhpcmRJbmRleFZhbCwgXCI+Pj4+PiB0aGlyZEluZGV4VmFsXCIpO1xyXG4gICAgICBxdWVyeU9iai5sb2FkTW9yZU9mZnNldCA9ICh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtc1t0aGlyZEluZGV4VmFsXS5pdGVtcy5sZW5ndGggPiAxMCkgPyB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2ZpcnN0SW5kZXhWYWxdLml0ZW1zW3NlY29uZEluZGV4VmFsXS5pdGVtc1t0aGlyZEluZGV4VmFsXS5pdGVtcy5sZW5ndGggLSAxIDogMTA7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0QWxsUmVwb25zZShxdWVyeU9iaiwgYXBpRnVuY3Rpb25EZXRhaWxzLmFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj5kYXRhXCIpO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlRGF0YSA9IGRhdGEucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgY29udHJvbEl0ZW1WYWx1ZSA9IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbZmlyc3RJbmRleFZhbF0uaXRlbXNbc2Vjb25kSW5kZXhWYWxdLml0ZW1zW3RoaXJkSW5kZXhWYWxdLml0ZW1zO1xyXG4gICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wb3AoKTtcclxuICAgICAgICAgIGlmIChyZXNwb25zZURhdGFbYXBpRnVuY3Rpb25EZXRhaWxzLnJlc3BvbnNlXSkge1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVzcG9uc2VEYXRhW2FwaUZ1bmN0aW9uRGV0YWlscy5yZXNwb25zZV1bMF0uZGF0YSwgZnVuY3Rpb24gKGRhdGFJdGVtLCBpbmRleCkge1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKGluZGV4LCBcIj4+PmluZGV4XCIpXHJcbiAgICAgICAgICAgICAgbGV0IG9iaiA9IGRhdGFJdGVtO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmFjY2Vzc0NvbnRyb2xOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJpc2tSYW5raW5nXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLnJpc2tSYW5raW5nO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xUeXBlXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLmNvbnRyb2xUeXBlO1xyXG4gICAgICAgICAgICAgIG9ialtcImNvbnRyb2xEZXNjcmlwdGlvblwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sRGVzY3JpcHRpb247XHJcblxyXG4gICAgICAgICAgICAgIG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NHcm91cHNbZGF0YUl0ZW0uYWNjZXNzR3JvdXBdLmFjY2Vzc0dyb3VwTmFtZTtcclxuICAgICAgICAgICAgICBvYmpbXCJydWxlc2V0TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5ydWxlc2V0c1tyZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucnVsZXNldElkXS5uYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcImRhdGFzb3VyY2VOYW1lXCJdID0gcmVzcG9uc2VEYXRhLmRhdGFzb3VyY2VzW2RhdGFJdGVtLmRhdGFzb3VyY2VdLm5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wic3RhdHVzQ2xhc3NcIl0gPSB7XHJcbiAgICAgICAgICAgICAgICBcIk9wZW5cIjogXCJsYWJlbCBiZy1pbnRpYXRlZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJDbG9zZWRcIjogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgIFwiUmVtZWRpYXRlZFwiIDogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgIFwiQXV0aG9yaXplZFwiOiBcImxhYmVsIGJnLXN1Y2Nlc3NcIixcclxuICAgICAgICAgICAgICAgIFwiQXBwcm92ZWRcIjogXCJsYWJlbCBiZy1zdWNjZXNzXCJcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2gob2JqKTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLmxlbmd0aCwgXCI+Pj4+IGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoXCIpXHJcbiAgICAgICAgICAgICAgaWYgKGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoICUgMTAgPT0gMCAmJiBpbmRleCA9PSAnOScpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTE9BRCBNT1JFXCIpXHJcbiAgICAgICAgICAgICAgICBsZXQgdGVtcG9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialsnYWNjZXNzR3JvdXAnXSA9IGRhdGFJdGVtLmFjY2Vzc0dyb3VwO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialsnYWNjZXNzQ29udHJvbCddID0gZGF0YUl0ZW0uYWNjZXNzQ29udHJvbDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5yaXNrUmFua2luZztcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sVHlwZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5jb250cm9sVHlwZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sRGVzY3JpcHRpb25cIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbERlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcInJvbGVOYW1lXCJdID0gZGF0YUl0ZW0ucm9sZU5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1widXNlck5hbWVcIl0gPSBkYXRhSXRlbS51c2VyTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSBkYXRhSXRlbS5lbnRpdGxlbWVudE5hbWU7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wiX2lkXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0NvbnRyb2xzW2RhdGFJdGVtLmFjY2Vzc0NvbnRyb2xdLl9pZDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJlbmFibGVMb2FkXCJdID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIF8uZm9yRWFjaChzZWxmLmN1cnJlbnRUYWJsZURhdGEucHJvZHVjdEtleXMsIGZ1bmN0aW9uICh2aWV3SXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wb2JqW3ZpZXdJdGVtLm5hbWVdID0gZGF0YUl0ZW1bdmlld0l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB0ZW1wb2JqW1wic3RhdHVzQ2xhc3NcIl0gPSB7XHJcbiAgICAgICAgICAgICAgICAgIFwiT3BlblwiOiBcImxhYmVsIGJnLWludGlhdGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiQ2xvc2VkXCI6IFwibGFiZWwgYmctZmFpbGVkXCIsXHJcbiAgICAgICAgICAgICAgICAgIFwiUmVtZWRpYXRlZFwiIDogXCJsYWJlbCBiZy1mYWlsZWRcIixcclxuICAgICAgICAgICAgICAgICAgXCJBdXRob3JpemVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiLFxyXG4gICAgICAgICAgICAgICAgICBcIkFwcHJvdmVkXCI6IFwibGFiZWwgYmctc3VjY2Vzc1wiXHJcbiAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaCh0ZW1wb2JqKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIGNvbnNvbGUubG9nKGNvbnRyb2xJdGVtVmFsdWUsIFwiPj5jb250cm9sSXRlbVZhbHVlXCIpO1xyXG4gICAgICAgICAgdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtmaXJzdEluZGV4VmFsXS5pdGVtc1tzZWNvbmRJbmRleFZhbF0uaXRlbXNbdGhpcmRJbmRleFZhbF0uaXRlbXMgPSBjb250cm9sSXRlbVZhbHVlO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiBFcnJvciBcIiwgZXJyKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgbG9hZE1vcmVDbGlja19iYWNrdXAocm93KSB7XHJcbiAgICBjb25zb2xlLmxvZyhyb3csIFwiPj4+Pj4+PiBST1dcIilcclxuICAgIGxldCBhcGlGdW5jdGlvbkRldGFpbHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEub25Hcm91cGluZ0Z1bmN0aW9uO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5xdWVyeVBhcmFtcywgXCI+Pj4gUVVFUlkgUEFSQU1TXCIpXHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLCBcIj4+PiBUSElTXCIpXHJcbiAgICBsZXQgaW5kZXhWYWwgPSAtMTtcclxuICAgIGxldCBxdWVyeU9iaiA9IHRoaXMucXVlcnlQYXJhbXM7XHJcblxyXG4gICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiBfLmZpbmRJbmRleCh0aGlzLmdyb3VwRmllbGQsIHsgZmllbGQ6IFwiY29udHJvbE5hbWVcIiB9KSA+PSAwKSB7XHJcbiAgICAgIHF1ZXJ5T2JqLmxvYWRNb3JlQ29udHJvbCA9IHJvdy5jb250cm9sTmFtZTtcclxuICAgICAgaW5kZXhWYWwgPSBfLmZpbmRJbmRleCh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLCB7IHZhbHVlOiByb3cuY29udHJvbE5hbWUgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHRoaXMuZ3JvdXBGaWVsZCwgeyBmaWVsZDogXCJ1c2VyTmFtZVwiIH0pID49IDApIHtcclxuICAgICAgcXVlcnlPYmoubG9hZE1vcmVVc2VyTmFtZSA9IHJvdy51c2VyTmFtZTtcclxuICAgICAgaW5kZXhWYWwgPSBfLmZpbmRJbmRleCh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLCB7IHZhbHVlOiByb3cudXNlck5hbWUgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHRoaXMuZ3JvdXBGaWVsZCwgeyBmaWVsZDogXCJlbnRpdGxlbWVudE5hbWVcIiB9KSA+PSAwKSB7XHJcbiAgICAgIHF1ZXJ5T2JqLmxvYWRNb3JlR3JvdXBOYW1lID0gcm93LmFjY2Vzc0dyb3VwO1xyXG4gICAgICBpbmRleFZhbCA9IF8uZmluZEluZGV4KHRoaXMubmV3VGFibGVEYXRhLmRhdGEsIHsgdmFsdWU6IHJvdy5lbnRpdGxlbWVudE5hbWUgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5ncm91cEZpZWxkICYmIF8uZmluZEluZGV4KHRoaXMuZ3JvdXBGaWVsZCwgeyBmaWVsZDogXCJyb2xlTmFtZVwiIH0pID49IDApIHtcclxuICAgICAgcXVlcnlPYmoubG9hZE1vcmVSZXNwTmFtZSA9IHJvdy5yb2xlTmFtZTtcclxuICAgICAgaW5kZXhWYWwgPSBfLmZpbmRJbmRleCh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLCB7IHZhbHVlOiByb3cucm9sZU5hbWUgfSk7XHJcbiAgICB9XHJcbiAgICBjb25zb2xlLmxvZyhpbmRleFZhbCwgXCI+Pj4+PiBpbmRleFZhbFwiKVxyXG5cclxuICAgIHF1ZXJ5T2JqLmxvYWRNb3JlT2Zmc2V0ID0gKHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zLmxlbmd0aCA+IDEwKSA/IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zLmxlbmd0aCAtIDEgOiAxMDtcclxuXHJcbiAgICBjb25zb2xlLmxvZyhxdWVyeU9iaiwgXCI+Pj4+IHF1ZWVyeSBPYmpcIilcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnlPYmosIGFwaUZ1bmN0aW9uRGV0YWlscy5hcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCI+Pj5kYXRhXCIpO1xyXG4gICAgICAgIGxldCByZXNwb25zZURhdGEgPSBkYXRhLnJlc3BvbnNlO1xyXG4gICAgICAgIC8vIGlmKF8uZmluZEluZGV4KHRoaXMuZ3JvdXBGaWVsZCx7ZmllbGQgOiAndXNlck5hbWUnfSkgPj0gMCl7XHJcblxyXG4gICAgICAgIC8vIH1lbHNle1xyXG5cclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgaWYgKHRoaXMuZ3JvdXBGaWVsZCAmJiB0aGlzLmdyb3VwRmllbGQubGVuZ3RoID09IDEpIHtcclxuICAgICAgICAgIGxldCBjb250cm9sSXRlbVZhbHVlID0gdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtpbmRleFZhbF0uaXRlbXM7XHJcbiAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnBvcCgpO1xyXG4gICAgICAgICAgaWYgKHJlc3BvbnNlRGF0YS51c2VyQ29uZmxpY3RzKSB7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChyZXNwb25zZURhdGEudXNlckNvbmZsaWN0c1swXS5kYXRhLCBmdW5jdGlvbiAoZGF0YUl0ZW0sIGluZGV4KSB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coaW5kZXgsIFwiPj4+aW5kZXhcIilcclxuICAgICAgICAgICAgICBsZXQgb2JqID0gZGF0YUl0ZW07XHJcbiAgICAgICAgICAgICAgb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wicmlza1JhbmtpbmdcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucmlza1Jhbmtpbmc7XHJcbiAgICAgICAgICAgICAgb2JqW1wiZW50aXRsZW1lbnROYW1lXCJdID0gcmVzcG9uc2VEYXRhLmFjY2Vzc0dyb3Vwc1tkYXRhSXRlbS5hY2Nlc3NHcm91cF0uYWNjZXNzR3JvdXBOYW1lO1xyXG4gICAgICAgICAgICAgIG9ialtcInJ1bGVzZXROYW1lXCJdID0gcmVzcG9uc2VEYXRhLnJ1bGVzZXRzW3Jlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5ydWxlc2V0SWRdLm5hbWU7XHJcbiAgICAgICAgICAgICAgb2JqW1wiZGF0YXNvdXJjZU5hbWVcIl0gPSByZXNwb25zZURhdGEuZGF0YXNvdXJjZXNbZGF0YUl0ZW0uZGF0YXNvdXJjZV0ubmFtZTtcclxuICAgICAgICAgICAgICBjb250cm9sSXRlbVZhbHVlLnB1c2gob2JqKTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLmxlbmd0aCwgXCI+Pj4+IGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoXCIpXHJcbiAgICAgICAgICAgICAgaWYgKGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoICUgMTAgPT0gMCAmJiBpbmRleCA9PSAnOScpIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTE9BRCBNT1JFXCIpXHJcbiAgICAgICAgICAgICAgICBsZXQgdGVtcG9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialsnYWNjZXNzR3JvdXAnXSA9IGRhdGFJdGVtLmFjY2Vzc0dyb3VwO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialsnYWNjZXNzQ29udHJvbCddID0gZGF0YUl0ZW0uYWNjZXNzQ29udHJvbDtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJyaXNrUmFua2luZ1wiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5yaXNrUmFua2luZztcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJyb2xlTmFtZVwiXSA9IGRhdGFJdGVtLnJvbGVOYW1lO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImVudGl0bGVtZW50TmFtZVwiXSA9IGRhdGFJdGVtLmVudGl0bGVtZW50TmFtZTtcclxuICAgICAgICAgICAgICAgIHRlbXBvYmpbXCJfaWRcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uX2lkO1xyXG4gICAgICAgICAgICAgICAgdGVtcG9ialtcImVuYWJsZUxvYWRcIl0gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY29udHJvbEl0ZW1WYWx1ZS5wdXNoKHRlbXBvYmopO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZSwgXCI+PmNvbnRyb2xJdGVtVmFsdWVcIik7XHJcbiAgICAgICAgICB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhW2luZGV4VmFsXS5pdGVtcyA9IGNvbnRyb2xJdGVtVmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmdyb3VwRmllbGQgJiYgdGhpcy5ncm91cEZpZWxkLmxlbmd0aCA9PSAyKSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLCBcIj4+Pj4gVEhJU1wiKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlRGF0YSwgXCI+Pj4+cmVzcG9uc2VEYXRhXCIpXHJcbiAgICAgICAgICBsZXQgY29udHJvbEl0ZW1WYWx1ZSA9IHRoaXMubmV3VGFibGVEYXRhLmRhdGFbaW5kZXhWYWxdLml0ZW1zO1xyXG4gICAgICAgICAgbGV0IHNlY29uZFZhbHVlSW5kZXggPSBfLmZpbmRJbmRleChjb250cm9sSXRlbVZhbHVlLCB7IHZhbHVlOiByb3cudXNlck5hbWUgfSlcclxuICAgICAgICAgIGxldCBzZWNvbmRHcm91cFZhbCA9IGNvbnRyb2xJdGVtVmFsdWVbc2Vjb25kVmFsdWVJbmRleF0uaXRlbXM7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyhzZWNvbmRHcm91cFZhbCxcIj4+PiBHUk9VUCBWQUxcIilcclxuICAgICAgICAgIHNlY29uZEdyb3VwVmFsLnBvcCgpO1xyXG4gICAgICAgICAgLy8gaWYocmVzcG9uc2VEYXRhLnVzZXJDb25mbGljdHMpe1xyXG4gICAgICAgICAgLy8gICBfLmZvckVhY2gocmVzcG9uc2VEYXRhLnVzZXJDb25mbGljdHNbMF0uZGF0YSwgZnVuY3Rpb24oZGF0YUl0ZW0sIGluZGV4KXtcclxuICAgICAgICAgIC8vICAgICBjb25zb2xlLmxvZyhpbmRleCxcIj4+PmluZGV4XCIpXHJcbiAgICAgICAgICAvLyAgICAgbGV0IG9iaiA9IGRhdGFJdGVtO1xyXG4gICAgICAgICAgLy8gICAgICAgb2JqW1wiY29udHJvbE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uYWNjZXNzQ29udHJvbE5hbWU7XHJcbiAgICAgICAgICAvLyAgICAgICBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzR3JvdXBzW2RhdGFJdGVtLmFjY2Vzc0dyb3VwXS5hY2Nlc3NHcm91cE5hbWU7XHJcbiAgICAgICAgICAvLyAgICAgICBvYmpbXCJydWxlc2V0TmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5ydWxlc2V0c1tyZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0ucnVsZXNldElkXS5uYW1lO1xyXG4gICAgICAgICAgLy8gICAgICAgb2JqW1wiZGF0YXNvdXJjZU5hbWVcIl0gPSByZXNwb25zZURhdGEuZGF0YXNvdXJjZXNbZGF0YUl0ZW0uZGF0YXNvdXJjZV0ubmFtZTtcclxuICAgICAgICAgIC8vICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaChvYmopO1xyXG4gICAgICAgICAgLy8gICAgICAgY29uc29sZS5sb2coY29udHJvbEl0ZW1WYWx1ZS5sZW5ndGgsXCI+Pj4+IGNvbnRyb2xJdGVtVmFsdWUubGVuZ3RoXCIpXHJcbiAgICAgICAgICAvLyAgICAgICBpZihjb250cm9sSXRlbVZhbHVlLmxlbmd0aCAlIDEwID09IDAgJiYgaW5kZXggPT0gJzknKXtcclxuICAgICAgICAgIC8vICAgICAgICAgY29uc29sZS5sb2coXCJMT0FEIE1PUkVcIilcclxuICAgICAgICAgIC8vICAgICAgICAgbGV0IHRlbXBvYmo9e307XHJcbiAgICAgICAgICAvLyAgICAgICAgIHRlbXBvYmpbXCJjb250cm9sTmFtZVwiXSA9IHJlc3BvbnNlRGF0YS5hY2Nlc3NDb250cm9sc1tkYXRhSXRlbS5hY2Nlc3NDb250cm9sXS5hY2Nlc3NDb250cm9sTmFtZTtcclxuICAgICAgICAgIC8vICAgICAgICAgdGVtcG9ialtcInJvbGVOYW1lXCJdID0gZGF0YUl0ZW0ucm9sZU5hbWU7XHJcbiAgICAgICAgICAvLyAgICAgICAgIHRlbXBvYmpbXCJlbnRpdGxlbWVudE5hbWVcIl0gPSBkYXRhSXRlbS5lbnRpdGxlbWVudE5hbWU7XHJcbiAgICAgICAgICAvLyAgICAgICAgIHRlbXBvYmpbXCJfaWRcIl0gPSByZXNwb25zZURhdGEuYWNjZXNzQ29udHJvbHNbZGF0YUl0ZW0uYWNjZXNzQ29udHJvbF0uX2lkO1xyXG4gICAgICAgICAgLy8gICAgICAgICB0ZW1wb2JqW1wiZW5hYmxlTG9hZFwiXSA9IHRydWU7XHJcbiAgICAgICAgICAvLyAgICAgICAgIGNvbnRyb2xJdGVtVmFsdWUucHVzaCh0ZW1wb2JqKTtcclxuICAgICAgICAgIC8vICAgfVxyXG4gICAgICAgICAgLy8gICB9KTtcclxuICAgICAgICAgIC8vIH1cclxuXHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyhjb250cm9sSXRlbVZhbHVlLFwiPj5jb250cm9sSXRlbVZhbHVlXCIpO1xyXG4gICAgICAgICAgLy8gdGhpcy5uZXdUYWJsZURhdGEuZGF0YVtpbmRleFZhbF0uaXRlbXMgPSBjb250cm9sSXRlbVZhbHVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gICAvLyBjb25zdHJ1Y3RHcm91cGVkRGEvdGEoZGF0YSwgKVxyXG4gICAgICB9LFxyXG4gICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgfSk7XHJcbiAgfVxyXG4gIGdyb3VwQ29sbGFwc2UoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcIj4+PiBFVk5UIFZBTFwiKTtcclxuICB9XHJcbiAgdXBkYXRlVGFibGVWaWV3KCkge1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVIZWFkZXIpIHtcclxuICAgICAgbGV0IGN1cnJlbnRUYWJsZUhlYWRlciA9IEpTT04ucGFyc2UoXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKVxyXG4gICAgICApO1xyXG4gICAgICBsZXQgdGFibGVIZWFkZXJMZW5ndGggPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVIZWFkZXIubGVuZ3RoO1xyXG4gICAgICAvLyB0aGlzLmNvbHVtbnMgPSBjdXJyZW50VGFibGVIZWFkZXI7XHJcbiAgICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IFtdO1xyXG4gICAgICBpZiAoXy5maW5kSW5kZXgodGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlSGVhZGVyLCB7IHZhbHVlOiBcInNlbGVjdFwiIH0pID4gLTEpIHtcclxuICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMucHVzaCh0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVIZWFkZXJbMF0pO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBsZXQgZmlsdGVyQXJyYXkgPSBjdXJyZW50VGFibGVIZWFkZXJcclxuICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICAgIHJldHVybiB2YWwuaXNBY3RpdmU7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IF8uY29uY2F0KHRoaXMuZGlzcGxheWVkQ29sdW1ucywgZmlsdGVyQXJyYXkpO1xyXG4gICAgICBpZiAoXy5maW5kSW5kZXgodGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlSGVhZGVyLCB7IHZhbHVlOiBcImFjdGlvblwiIH0pID4gLTEpIHtcclxuICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMucHVzaCh0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVIZWFkZXJbdGFibGVIZWFkZXJMZW5ndGggLSAxXSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgfVxyXG4gIG9uTG9hZERhdGEoZW5hYmxlTmV4dCkge1xyXG5cclxuICAgIGlmKHRoaXMuZGF0YT09J3JlZnJlc2hQYWdlJyl7XHJcbiAgICAgIHRoaXMucXVlcnlQYXJhbXNbXCJvZmZzZXRcIl0gPSAwO1xyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wibGltaXRcIl0gPSAxMDtcclxuICAgICAgdGhpcy5vZmZzZXQgPSAwO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSA9IHt9O1xyXG4gICAgdGhpcy5jb2x1bW5zID0gW107XHJcbiAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSBbXTtcclxuICAgIGxldCB0YWJsZUFycmF5ID0gdGhpcy5vbkxvYWRcclxuICAgICAgPyB0aGlzLm9uTG9hZC50YWJsZURhdGFcclxuICAgICAgOiB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl0udGFibGVEYXRhO1xyXG4gICAgbGV0IHJlc3BvbnNlS2V5ID1cclxuICAgICAgdGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQucmVzcG9uc2VLZXkgPyB0aGlzLm9uTG9hZC5yZXNwb25zZUtleSA6IG51bGw7XHJcbiAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleCh0YWJsZUFycmF5LCB7IHRhYmxlSWQ6IHRoaXMudGFibGVJZCB9KTtcclxuICAgIGlmIChpbmRleCA+IC0xKVxyXG4gICAgICB0aGlzLmN1cnJlbnRUYWJsZURhdGEgPSB0YWJsZUFycmF5W2luZGV4XTtcclxuICAgIGNvbnNvbGUubG9nKHRhYmxlQXJyYXksIFwiLi4uLi50YWJsZUFycmF5XCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5jdXJyZW50VGFibGVEYXRhLCBcIi4uLi4uY3VycmVudFRhYmxlRGF0YVwiKTtcclxuICAgIGxldCBjaGVja09wdGlvbkZpbHRlciA9ICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLmVuYWJsZU9wdGlvbnNGaWx0ZXIpID8gdHJ1ZSA6IGZhbHNlXHJcbiAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5lbmFibGVNdWx0aVNlbGVjdCkge1xyXG4gICAgICB0aGlzLmVuYWJsZU9wdGlvbkZpbHRlciA9IHRydWU7XHJcbiAgICB9XHJcbiAgICBpZiAoY2hlY2tPcHRpb25GaWx0ZXIpIHtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB0aGlzLmVuYWJsZU9wdGlvbkZpbHRlciA9IHRydWU7XHJcbiAgICAgIGxldCBPcHRpb25kYXRhID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLmZpbHRlck9wdGlvbkxvYWRGdW5jdGlvbjtcclxuICAgICAgdGhpcy5vcHRpb25GaWx0ZXJGaWVsZHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEub3B0aW9uRmllbGRzO1xyXG4gICAgICB2YXIgYXBpVXJsID0gT3B0aW9uZGF0YS5hcGlVcmw7XHJcbiAgICAgIHZhciByZXNwb25zZU5hbWUgPSBPcHRpb25kYXRhLnJlc3BvbnNlO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0QWxsUmVwb25zZSh0aGlzLnF1ZXJ5UGFyYW1zLCBhcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgbGV0IHRlbXBBcnJheSA9IChyZXMucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSkgPyByZXMucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSA6IFtdO1xyXG5cclxuICAgICAgICAgIHNlbGYub3B0aW9uRmlsdGVyZGF0YSA9IHRlbXBBcnJheTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLmVuYWJsZVVpZmlsdGVyKSB7XHJcbiAgICAgIGxldCBmaWx0ZXJLZXkgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudWlGaWx0ZXJLZXk7XHJcbiAgICAgIHRoaXMuZW5hYmxlVUlmaWx0ZXIgPSB0cnVlO1xyXG4gICAgICAvLyB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyUHJlZGljYXRlID0gKGRhdGE6IEVsZW1lbnQsIGZpbHRlcjogc3RyaW5nKSA9PiB7XHJcbiAgICAgIC8vICAgcmV0dXJuIGRhdGFbZmlsdGVyS2V5XSA9PSBmaWx0ZXI7XHJcbiAgICAgIC8vICB9O1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLnJlc3BvbnNlICYmICFlbmFibGVOZXh0KSB7XHJcbiAgICAgIHRoaXMuZ2V0TG9hZERhdGEoXHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVEYXRhLFxyXG4gICAgICAgIHRoaXMub25Mb2FkLnJlc3BvbnNlLFxyXG4gICAgICAgIHRoaXMub25Mb2FkLnRvdGFsLFxyXG4gICAgICAgIHJlc3BvbnNlS2V5XHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhKSB0aGlzLmdldERhdGEodGhpcy5jdXJyZW50VGFibGVEYXRhLCBudWxsKTtcclxuICAgIH1cclxuICAgIGlmICghZW5hYmxlTmV4dCkge1xyXG4gICAgICAvLyB0aGlzLnBhZ2luYXRvci5maXJzdFBhZ2UoKTtcclxuICAgIH1cclxuICB9XHJcbiAgZ2V0RGF0YShjdXJyZW50VGFibGVWYWx1ZSwgZW5hYmxlVGFibGVTZWFyY2gpIHtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICB0aGlzLmNvbHVtbnMgPSBjdXJyZW50VGFibGVWYWx1ZS50YWJsZUhlYWRlcjtcclxuICAgIGlmIChjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICB2YXIgYXBpVXJsID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICB2YXIgcmVzcG9uc2VOYW1lID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgIGxldCBrZXlUb1NldCA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmtleUZvclN0cmluZ1Jlc3BvbnNlO1xyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVtcImxpc3RWaWV3XCJdO1xyXG4gICAgICB2YXIgaW5jbHVkZVJlcXVlc3QgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5pbmNsdWRlcmVxdWVzdERhdGEgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIC8vaWYgKCBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5lbmFibGVMb2FkZXIpIHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0YXJ0TG9hZGVyKCk7XHJcbiAgICAgIC8vfVxyXG4gICAgICBsZXQgZHluYW1pY0xvYWQgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5keW5hbWljUmVwb3J0TG9hZDsgIC8vIGNvbmRpdGlvbiBjaGVja2VkIHdoZW4gaW1wbGVtZW50IGFkZGl0aW9uYWwgcmVwb3J0IHZpZXdcclxuICAgICAgbGV0IHJlcXVlc3RlZFF1ZXJ5ID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24ucmVxdWVzdERhdGE7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgdmFyIHJlcXVlc3RRdWVyeVBhcmFtID0ge307XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmdyb3VwRmllbGQsIGZ1bmN0aW9uIChldmVudEl0ZW0pIHtcclxuICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW2V2ZW50SXRlbS5maWVsZF0gPSB0cnVlO1xyXG4gICAgICB9KVxyXG4gICAgICBpZiAoZHluYW1pY0xvYWQpIHtcclxuICAgICAgICBsZXQgdGVtcE9iaiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiQ3VycmVudFJlcG9ydERhdGFcIik7XHJcbiAgICAgICAgbGV0IHJlcG9ydElUZW0gPSBKU09OLnBhcnNlKHRlbXBPYmopO1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0ZWRRdWVyeSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uZnJvbUN1cnJlbnREYXRhKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RRdWVyeVBhcmFtW3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5jdXJyZW50RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RRdWVyeVBhcmFtW3JlcXVlc3RJdGVtLm5hbWVdID0gKHJlcG9ydElUZW1bcmVxdWVzdEl0ZW0udmFsdWVdKSA/IHJlcG9ydElUZW1bcmVxdWVzdEl0ZW0udmFsdWVdIDogcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7IFxyXG4gICAgICAgIGlmKCFlbmFibGVUYWJsZVNlYXJjaCkge1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3RlZFF1ZXJ5LCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgICAgLy8gIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnN1YktleVxyXG4gICAgICAgICAgICAvLyAgPyBkZXRhaWxzW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAvLyAgOiBkZXRhaWxzW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uZnJvbUN1cnJlbnREYXRhKSB7XHJcbiAgICAgICAgICAgICAgcmVxdWVzdFF1ZXJ5UGFyYW1bcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmN1cnJlbnREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLmRpcmVjdEFzc2lnbikge1xyXG4gICAgICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkocmVxdWVzdEl0ZW0udmFsdWUpXHJcbiAgICAgICAgICAgICAgICA6IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2V7IFxyXG4gICAgICAgICAgICAgIGxldCBleGlzdENoZWNrID0gXy5oYXMoc2VsZi5pbnB1dERhdGEsIHJlcXVlc3RJdGVtLnZhbHVlKTtcclxuICAgICAgICAgICAgLy8gIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXSA/IGl0ZW1bdmlld0l0ZW0udmFsdWVdW3ZpZXdJdGVtLnN1YmtleV0gOiBcIlwiO1xyXG4gICAgICAgICAgICAgIGlmKGV4aXN0Q2hlY2spe1xyXG4gICAgICAgICAgICAgICBsZXQgIHRlbXBEYXRhID0gcmVxdWVzdEl0ZW0uc3ViS2V5XHJcbiAgICAgICAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgICA6IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuXHJcbiAgICAgICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSlcclxuICAgICAgICAgICAgICAgIDogdGVtcERhdGE7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMucXVlcnlQYXJhbXNbXCJkaXNhYmxlR3JvdXBcIl0gPSAodGhpcy5ncm91cEZpZWxkICYmIHRoaXMuZ3JvdXBGaWVsZC5sZW5ndGgpID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zID0geyAuLi50aGlzLnF1ZXJ5UGFyYW1zLCAuLi5yZXF1ZXN0UXVlcnlQYXJhbSB9O1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UodGhpcy5xdWVyeVBhcmFtcywgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAvLyBpZiAoIGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmVuYWJsZUxvYWRlcikge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgIC8vfVxyXG4gICAgICAgICAgbGV0IHNlbGVjdGVkVGFibGVIZWFkZXJzID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKTsgIC8vIGFmdGVyIGdsb2JhbCBzZXR0aW5nIGNoYW5nZVxyXG4gICAgICAgICAgdGhpcy5jb2x1bW5zID0gIV8uaXNFbXB0eShzZWxlY3RlZFRhYmxlSGVhZGVycykgPyBKU09OLnBhcnNlKHNlbGVjdGVkVGFibGVIZWFkZXJzKSA6IHRoaXMuY29sdW1ucztcclxuICAgICAgICAgIHRoaXMuZW5hYmxlU2VhcmNoID0gdGhpcy5jdXJyZW50RGF0YS5lbmFibGVHbG9iYWxTZWFyY2g7XHJcbiAgICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMgPSB0aGlzLmNvbHVtbnNcclxuICAgICAgICAgICAgLmZpbHRlcihmdW5jdGlvbiAodmFsKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHZhbC5pc0FjdGl2ZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+ICB0aGlzLmRpc3BsYXllZENvbHVtbiBcIiwgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zKTtcclxuICAgICAgICAgIGxldCB0ZW1wQXJyYXkgPSBbXTtcclxuICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICAgIGNvbnNvbGUubG9nKHNlbGYsIFwiPj4+Pj4+Pj4+PlRISVNTU1NcIik7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkRhdGEgUmVzcG9uc2UgKioqKipcIiwgZGF0YS5yZXNwb25zZSk7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIkRhdGEgUmVzcG9uc2UgTmFtZSAqKioqKlwiLCByZXNwb25zZU5hbWUpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coY3VycmVudFRhYmxlVmFsdWUsIFwiQ3VyciBUYWJsZSBkYXRhID4+Pj4uXCIpO1xyXG5cclxuXHJcbiAgICAgICAgICBfLmZvckVhY2goZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdLCBmdW5jdGlvbiAoaXRlbSwgaW5kZXgpIHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCJJdGVtIERhdGEgPj4+Pj4+XCIsIGl0ZW0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBpdGVtICE9PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgICAgICB0ZW1wT2JqW2tleVRvU2V0XSA9IGl0ZW07XHJcbiAgICAgICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgLy8gaWYgKHNlbGYudGFibGVJZCA9PSBcIm5vdGlmaWNhdGlvbnNcIikge1xyXG4gICAgICAgICAgICAgIC8vICAgaXRlbS5mZWF0dXJlID0gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIC8vICAgICBcIk5PVElGSUNBVElPTl9GT1JNQVRTLkZFQVRVUkUuXCIgKyBpdGVtLm1lc3NhZ2VPYmouZmVhdHVyZSk7XHJcbiAgICAgICAgICAgICAgLy8gICBsZXQgb3BlcmF0aW9uU3RhdHVzID0gaXRlbS5tZXNzYWdlT2JqLmFjdGlvbiArIFwiICBcIiArIGl0ZW0uc3RhdHVzO1xyXG4gICAgICAgICAgICAgIC8vICAgY29uc29sZS5sb2coXCJPcGVyYXRpb24gRGF0YVwiLCBvcGVyYXRpb25TdGF0dXMpO1xyXG5cclxuICAgICAgICAgICAgICAvLyAgIGl0ZW0uc3RhdHVzX2FjdGlvbiA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcIk5PVElGSUNBVElPTl9GT1JNQVRTLkFDVElPTl9TVEFUVVMuXCIgKyBvcGVyYXRpb25TdGF0dXMpO1xyXG4gICAgICAgICAgICAgIC8vICAgaXRlbS5tc2cgPSBpdGVtLmZlYXR1cmUgKyBcIiAgIFwiICsgaXRlbS5zdGF0dXNfYWN0aW9uO1xyXG4gICAgICAgICAgICAgIC8vICAgY29uc29sZS5sb2coXCJFbnRyeSBvZiBDb25kaXRpb24gaXRlbVwiKTtcclxuICAgICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIioqKioqKioqKiogSXRlbSBEYXRhICoqKioqKioqKioqKioqXCIsIGl0ZW0pO1xyXG4gICAgICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50VGFibGVWYWx1ZS5kYXRhVmlld0Zvcm1hdCwgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlZpZXcgSXRlbSBvZiBEYXRhdmlldyBGb3JtYXQgPj4+PlwiLCB2aWV3SXRlbSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHZpZXdJdGVtLnN1YmtleSkge1xyXG4gICAgICAgICAgICAgICAgICBpZiAodmlld0l0ZW0uYXNzaWduRmlyc3RJbmRleHZhbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByZXN1bHRWYWx1ZSA9ICghXy5pc0VtcHR5KGl0ZW1bdmlld0l0ZW0udmFsdWVdKSAmJiBpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA/IChpdGVtW3ZpZXdJdGVtLnZhbHVlXVswXVt2aWV3SXRlbS5zdWJrZXldKSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgICAgICA/IHJlc3VsdFZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgICAgICA/IGl0ZW1bdmlld0l0ZW0udmFsdWVdW3ZpZXdJdGVtLnN1YmtleV1cclxuICAgICAgICAgICAgICAgICAgICAgIDogXCJcIjtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh2aWV3SXRlbS5pc0NvbmRpdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID0gZXZhbCh2aWV3SXRlbS5jb25kaXRpb24pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgZWxzZSBpZiAodmlld0l0ZW0uaXNBZGREZWZhdWx0VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IHZpZXdJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHZpZXdJdGVtLmlzTm90aWZ5KSB7XHJcbiAgICAgICAgICAgICAgICAgIC8vIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBldmFsKHZpZXdJdGVtLmNvbmRpdGlvbik7XHJcblxyXG4gICAgICAgICAgICAgICAgICBsZXQgZmVhdCA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgICBcIk5PVElGSUNBVElPTl9GT1JNQVRTLkZFQVRVUkUuXCIgKyAoaXRlbVt2aWV3SXRlbS52YWx1ZV0uZmVhdHVyZSkpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgbGV0IG9wZXJhdGlvblN0YXR1cyA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdLmFjdGlvbiArIFwiX1wiICsgaXRlbVt2aWV3SXRlbS52YWx1ZV0uc3RhdHVzO1xyXG4gICAgICAgICAgICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bWF4LWxpbmUtbGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgIGxldCBzdGF0dXNfYWN0aW9uID0gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFwiTk9USUZJQ0FUSU9OX0ZPUk1BVFMuQUNUSU9OX1NUQVRVUy5cIiArIG9wZXJhdGlvblN0YXR1cyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICBsZXQgbWVzc2FnZSA9IGZlYXQgKyBcIiBcIiArIHN0YXR1c19hY3Rpb247XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBtZXNzYWdlO1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW1wiZmVhdHVyZU5hbWVcIl0gPSBmZWF0O1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW1wib3BlcmF0aW9uTmFtZVwiXSA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcIk5PVElGSUNBVElPTl9GT1JNQVRTLkFDVElPTi5cIiArIGl0ZW1bdmlld0l0ZW0udmFsdWVdLmFjdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbXCJ0eXBlTmFtZVwiXSA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcIk5PVElGSUNBVElPTl9GT1JNQVRTLlRZUEUuXCIgKyBpdGVtW3ZpZXdJdGVtLnZhbHVlXS50eXBlKTtcclxuICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoY3VycmVudFRhYmxlVmFsdWUubG9hZExhYmVsRnJvbUNvbmZpZykge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wT2JqW3ZpZXdJdGVtLm5hbWVdID1cclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50VGFibGVWYWx1ZS5oZWFkZXJMaXN0W2l0ZW1bdmlld0l0ZW0udmFsdWVdXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICBsZXQgZmlsdGVyT2JqID0ge307XHJcbiAgICAgICAgICAgICAgZmlsdGVyT2JqW2N1cnJlbnRUYWJsZVZhbHVlLmZpbHRlcktleV0gPSBpdGVtW2N1cnJlbnRUYWJsZVZhbHVlLmRhdGFGaWx0ZXJLZXldO1xyXG4gICAgICAgICAgICAgIGxldCBzZWxlY3RlZFZhbHVlID0gXy5maW5kKHNlbGYuaW5wdXREYXRhW2N1cnJlbnRUYWJsZVZhbHVlLnNlbGVjdGVkVmFsdWVzS2V5XSwgZmlsdGVyT2JqKTtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZFZhbHVlLCBcIj4+Pj4+c2VsZWN0ZWRWYWx1ZVwiKVxyXG4gICAgICAgICAgICAgIGlmIChzZWxlY3RlZFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICBpdGVtLmNoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB0ZW1wT2JqID0geyAuLi5pdGVtLCAuLi50ZW1wT2JqIH07XHJcbiAgICAgICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJcIiwgdGVtcEFycmF5KTtcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgaWYgKHRlbXBBcnJheSAmJiB0ZW1wQXJyYXkubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFibGVEYXRhID0gdGVtcEFycmF5O1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy50YWJsZURhdGEgPVxyXG4gICAgICAgICAgICAgIGRhdGEgJiYgZGF0YS5yZXNwb25zZSAmJiBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV1cclxuICAgICAgICAgICAgICAgID8gKGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSlcclxuICAgICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAodGhpcy5ncm91cEZpZWxkICYmIHRoaXMuZ3JvdXBGaWVsZC5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5jb25zdHJ1Y3RHcm91cGVkRGF0YShkYXRhLnJlc3BvbnNlLCByZXNwb25zZU5hbWUpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLnJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMubGVuZ3RoID0gZGF0YS5yZXNwb25zZS50b3RhbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+KHRoaXMudGFibGVEYXRhIFwiLHRoaXMudGFibGVEYXRhKTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gKHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdICYmIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdLmxlbmd0aCA+IDApID8gKHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdKSA6IFtdO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9ICh0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdICYmIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0ubGVuZ3RoID4gMCkgPyAodGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSkgOiBbXTtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhPSAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gKHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aD49dGhpcy5saW1pdCk/dHJ1ZTpmYWxzZTsgIFxyXG4gICAgICAgICAgdGhpcy5uZXdUYWJsZURhdGEgPSBwcm9jZXNzKHRoaXMudGFibGVEYXRhLCB7IGdyb3VwOiB0aGlzLmdyb3VwRmllbGQgfSk7XHJcbiAgICAgICAgICB0aGlzLm5ld1RhYmxlRGF0YS50b3RhbCA9IHRoaXMubGVuZ3RoO1xyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLnRhYmxlRGF0YSk7XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5zZWxlY3RlZFBhZ2luYXRvcjsgLy8gbmV3bHkgYWRkZWQgXHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAvLyB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgICAgICAgdmFyIGZpbHRlcmVkS2V5ID0gKHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoS2V5KSA/IHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoS2V5IDogJyc7XHJcbiAgICAgICAgICBfLmZvckVhY2goc2VsZi5jb2x1bW5zLCBmdW5jdGlvbiAoeCkge1xyXG4gICAgICAgICAgICBpZiAoZmlsdGVyZWRLZXkgPT0geC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3gua2V5VG9TYXZlXSA9IHNlbGYuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbeC5rZXlUb1NhdmVdID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICAvLyB0aGlzLmxvYWRLZW5kb0dyaWQoKTtcclxuICAgICAgICAgIC8vIHRoaXMuc2VsZWN0QWxsQ2hlY2soeyBjaGVja2VkOiBmYWxzZSB9KTtcclxuICAgICAgICAgIC8vIHRoaXMuc2VsZWN0QWxsID0gZmFsc2U7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdEFsbCwgXCJTZWxlY3RBbGw+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgICAgIC8vIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgICAgICAgIC8vIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdEFsbCwgXCJTZWxlY3RBbGw+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgLy8gdGhpcy5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcblxyXG4gICAgICAgICAgdGhpcy5jaGVja0NsaWNrRXZlbnRNZXNzYWdlLmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgICBlcnIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIiBFcnJvciBcIiwgZXJyKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuICAvLyAgIGxvYWRLZW5kb0dyaWQoKXtcclxuICAvLyAgICAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XHJcbiAgLy8gICAgICAgKDxhbnk+JChcIiNncmlkXCIpKS5rZW5kb0dyaWQoe1xyXG4gIC8vICAgICAgICAgICBkYXRhU291cmNlOiB7XHJcbiAgLy8gICAgICAgICAgICAgICBwYWdlU2l6ZTogMTAsXHJcbiAgLy8gICAgICAgICAgICAgICB0cmFuc3BvcnQ6IHtcclxuICAvLyAgICAgICAgICAgICAgICAgICByZWFkOiB7XHJcbiAgLy8gICAgICAgICAgICAgICAgICAgICAgIHVybDogXCJodHRwczovL2RlbW9zLnRlbGVyaWsuY29tL2tlbmRvLXVpL3NlcnZpY2UvUHJvZHVjdHNcIixcclxuICAvLyAgICAgICAgICAgICAgICAgICAgICAgZGF0YVR5cGU6IFwianNvbnBcIlxyXG4gIC8vICAgICAgICAgICAgICAgICAgIH1cclxuICAvLyAgICAgICAgICAgICAgIH0sXHJcbiAgLy8gICAgICAgICAgICAgICBzY2hlbWE6IHtcclxuICAvLyAgICAgICAgICAgICAgICAgICBtb2RlbDoge1xyXG4gIC8vICAgICAgICAgICAgICAgICAgICAgICBpZDogXCJQcm9kdWN0SURcIlxyXG4gIC8vICAgICAgICAgICAgICAgICAgIH1cclxuICAvLyAgICAgICAgICAgICAgIH1cclxuICAvLyAgICAgICAgICAgfSxcclxuICAvLyAgICAgICAgICAgcGFnZWFibGU6IHRydWUsXHJcbiAgLy8gICAgICAgICAgIHNjcm9sbGFibGU6IGZhbHNlLFxyXG4gIC8vICAgICAgICAgICBwZXJzaXN0U2VsZWN0aW9uOiB0cnVlLFxyXG4gIC8vICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZSxcclxuICAvLyAgICAgICAgICAgLy8gY2hhbmdlOiBvbkNoYW5nZSxcclxuICAvLyAgICAgICAgICAgY29sdW1uczogW3tcclxuICAvLyAgICAgICAgICAgICAgICAgICBzZWxlY3RhYmxlOiB0cnVlLFxyXG4gIC8vICAgICAgICAgICAgICAgICAgIHdpZHRoOiBcIjUwcHhcIlxyXG4gIC8vICAgICAgICAgICAgICAgfSxcclxuICAvLyAgICAgICAgICAgICAgIHtcclxuICAvLyAgICAgICAgICAgICAgICAgICBmaWVsZDogXCJQcm9kdWN0TmFtZVwiLFxyXG4gIC8vICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlByb2R1Y3QgTmFtZVwiXHJcbiAgLy8gICAgICAgICAgICAgICB9LFxyXG4gIC8vICAgICAgICAgICAgICAge1xyXG4gIC8vICAgICAgICAgICAgICAgICAgIGZpZWxkOiBcIlVuaXRQcmljZVwiLFxyXG4gIC8vICAgICAgICAgICAgICAgICAgIHRpdGxlOiBcIlVuaXQgUHJpY2VcIixcclxuICAvLyAgICAgICAgICAgICAgICAgICBmb3JtYXQ6IFwiezA6Y31cIlxyXG4gIC8vICAgICAgICAgICAgICAgfSxcclxuICAvLyAgICAgICAgICAgICAgIHtcclxuICAvLyAgICAgICAgICAgICAgICAgICBmaWVsZDogXCJVbml0c0luU3RvY2tcIixcclxuICAvLyAgICAgICAgICAgICAgICAgICB0aXRsZTogXCJVbml0cyBJbiBTdG9ja1wiXHJcbiAgLy8gICAgICAgICAgICAgICB9LFxyXG4gIC8vICAgICAgICAgICAgICAge1xyXG4gIC8vICAgICAgICAgICAgICAgICAgIGZpZWxkOiBcIkRpc2NvbnRpbnVlZFwiXHJcbiAgLy8gICAgICAgICAgICAgICB9XHJcbiAgLy8gICAgICAgICAgIF1cclxuICAvLyAgICAgICB9KTtcclxuXHJcbiAgLy8gICAgICAgdmFyIGdyaWQgPSAkKFwiI2dyaWRcIikuZGF0YShcImtlbmRvR3JpZFwiKTtcclxuXHJcbiAgLy8gICAgICAgLy8gZ3JpZC50aGVhZC5vbihcImNsaWNrXCIsIFwiLmstY2hlY2tib3hcIiwgb25DbGljayk7XHJcbiAgLy8gICB9KTtcclxuXHJcbiAgLy8gICAgICAgLy8gLy9iaW5kIGNsaWNrIGV2ZW50IHRvIHRoZSBjaGVja2JveFxyXG4gIC8vICAgICAgIC8vIGdyaWQudGFibGUub24oXCJjbGlja1wiLCBcIi5rLWNoZWNrYm94XCIgLCBzZWxlY3RSb3cpO1xyXG4gIC8vICAgICAgIC8vICQoJyNoZWFkZXItY2hiJykuY2hhbmdlKGZ1bmN0aW9uKGV2KXtcclxuICAvLyAgICAgICAvLyAgIHZhciBjaGVja2VkID0gZXYudGFyZ2V0LmNoZWNrZWQ7XHJcbiAgLy8gICAgICAgLy8gICAkKCcuay1jaGVja2JveCcpLmVhY2goZnVuY3Rpb24oaWR4LCBpdGVtKXtcclxuICAvLyAgICAgICAvLyAgICAgaWYoY2hlY2tlZCl7XHJcbiAgLy8gICAgICAgLy8gICAgICAgaWYoISgkKGl0ZW0pLmNsb3Nlc3QoJ3RyJykuaXMoJy5rLXN0YXRlLXNlbGVjdGVkJykpKXtcclxuICAvLyAgICAgICAvLyAgICAgICAgICQoaXRlbSkuY2xpY2soKTtcclxuICAvLyAgICAgICAvLyAgICAgICB9XHJcbiAgLy8gICAgICAgLy8gICAgIH0gZWxzZSB7XHJcbiAgLy8gICAgICAgLy8gICAgICAgaWYoJChpdGVtKS5jbG9zZXN0KCd0cicpLmlzKCcuay1zdGF0ZS1zZWxlY3RlZCcpKXtcclxuICAvLyAgICAgICAvLyAgICAgICAgICQoaXRlbSkuY2xpY2soKTtcclxuICAvLyAgICAgICAvLyAgICAgICB9XHJcbiAgLy8gICAgICAgLy8gICAgIH1cclxuICAvLyAgICAgICAvLyAgIH0pO1xyXG5cclxuICAvLyAgICAgICAvLyB9KTtcclxuXHJcbiAgLy8gICAgICAgLy8gJChcIiNzaG93U2VsZWN0aW9uXCIpLmJpbmQoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgLy8gICAgICAgLy8gICB2YXIgY2hlY2tlZCA9IFtdO1xyXG4gIC8vICAgICAgIC8vICAgZm9yKHZhciBpIGluIGNoZWNrZWRJZHMpe1xyXG4gIC8vICAgICAgIC8vICAgICBpZihjaGVja2VkSWRzW2ldKXtcclxuICAvLyAgICAgICAvLyAgICAgICBjaGVja2VkLnB1c2goaSk7XHJcbiAgLy8gICAgICAgLy8gICAgIH1cclxuICAvLyAgICAgICAvLyAgIH1cclxuXHJcbiAgLy8gICAgICAgICAvLyBhbGVydChjaGVja2VkKTtcclxuICAvLyAgICAgICAvLyB9KTtcclxuICAvLyAgICAgLy8gfSk7XHJcbiAgLy8gfVxyXG4gIGdldExvYWREYXRhKGN1cnJlbnRWYWx1ZSwgcmVzcG9uc2VWYWx1ZSwgdG90YWxMZW5ndGgsIHJlc3BvbnNlS2V5KSB7XHJcbiAgICBjb25zb2xlLmxvZyhjdXJyZW50VmFsdWUsIFwiLi4uLmN1cnJlbnRWYWx1ZVwiKTtcclxuICAgIGxldCB0ZW1wQXJyYXkgPSBbXTtcclxuICAgIC8vIGlmIChyZXNwb25zZVZhbHVlICYmIHJlc3BvbnNlVmFsdWVbcmVzcG9uc2VLZXldKSB7XHJcbiAgICByZXNwb25zZVZhbHVlID0gcmVzcG9uc2VLZXkgPyByZXNwb25zZVZhbHVlW3Jlc3BvbnNlS2V5XSA6IHJlc3BvbnNlVmFsdWU7XHJcbiAgICBpZiAocmVzcG9uc2VWYWx1ZSAmJiByZXNwb25zZVZhbHVlLmxlbmd0aCkge1xyXG4gICAgICBfLmZvckVhY2gocmVzcG9uc2VWYWx1ZSwgZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBpdGVtICE9PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgdGVtcE9ialtjdXJyZW50VmFsdWUua2V5VG9TZXRdID0gaXRlbTtcclxuICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgXy5mb3JFYWNoKGN1cnJlbnRWYWx1ZS5kYXRhVmlld0Zvcm1hdCwgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgIGlmICh2aWV3SXRlbS5zdWJrZXkpIHtcclxuICAgICAgICAgICAgICBpZiAodmlld0l0ZW0uYXNzaWduRmlyc3RJbmRleHZhbCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlc3VsdFZhbHVlID0gKCFfLmlzRW1wdHkoaXRlbVt2aWV3SXRlbS52YWx1ZV0pICYmIGl0ZW1bdmlld0l0ZW0udmFsdWVdWzBdW3ZpZXdJdGVtLnN1YmtleV0pID8gKGl0ZW1bdmlld0l0ZW0udmFsdWVdWzBdW3ZpZXdJdGVtLnN1YmtleV0pIDogXCJcIjtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICA/IHJlc3VsdFZhbHVlXHJcbiAgICAgICAgICAgICAgICAgIDogXCJcIjtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgID8gaXRlbVt2aWV3SXRlbS52YWx1ZV1bdmlld0l0ZW0uc3Via2V5XVxyXG4gICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0ZW1wT2JqID0geyAuLi5pdGVtLCAuLi50ZW1wT2JqIH07XHJcbiAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jb2x1bW5zID0gY3VycmVudFZhbHVlLnRhYmxlSGVhZGVyO1xyXG4gICAgcmVzcG9uc2VWYWx1ZSA9IHRlbXBBcnJheS5sZW5ndGggPyB0ZW1wQXJyYXkgOiByZXNwb25zZVZhbHVlO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiLi4uSU5QVVREQVRBXCIpO1xyXG4gICAgaWYgKGN1cnJlbnRWYWx1ZSAmJiBjdXJyZW50VmFsdWUubWFwRGF0YUZ1bmN0aW9uKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKGN1cnJlbnRWYWx1ZS5tYXBEYXRhRnVuY3Rpb24sIGZ1bmN0aW9uIChtYXBJdGVtKSB7XHJcbiAgICAgICAgXy5mb3JFYWNoKG1hcEl0ZW0ucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSlcclxuICAgICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgc2VsZi5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgLmdldEFsbFJlcG9uc2Uoc2VsZi5xdWVyeVBhcmFtcywgbWFwSXRlbS5hcGlVcmwpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhtYXBJdGVtLCBcIj4+Pj4+Pj4gTUFQIElURU1cIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiLi4uLi5EQVRBQUFBXCIpO1xyXG4gICAgICAgICAgICBsZXQgdGVtcE9iaiA9IF8ua2V5QnkoZGF0YS5yZXNwb25zZVttYXBJdGVtLnJlc3BvbnNlXSwgXCJfaWRcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHRlbXBPYmosIFwiLi4uLnRlbXBPYmpcIik7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChyZXNwb25zZVZhbHVlLCBmdW5jdGlvbiAocmVzcG9uc2VJdGVtKSB7XHJcbiAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgZGF0YS5yZXNwb25zZSAmJlxyXG4gICAgICAgICAgICAgICAgZGF0YS5yZXNwb25zZS5rZXlGb3JDaGVjayA9PSBtYXBJdGVtLmtleUZvckNoZWNrICYmXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZUl0ZW1bbWFwSXRlbS5rZXlGb3JDaGVja11cclxuICAgICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlSXRlbVttYXBJdGVtLnNob3dLZXldID1cclxuICAgICAgICAgICAgICAgICAgdGVtcE9iaiAmJiByZXNwb25zZUl0ZW1bbWFwSXRlbS5rZXlGb3JDaGVja11cclxuICAgICAgICAgICAgICAgICAgICA/IHRlbXBPYmpbcmVzcG9uc2VJdGVtW21hcEl0ZW0ua2V5Rm9yQ2hlY2tdXS5vYmplY3RUeXBlc1xyXG4gICAgICAgICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZUl0ZW1bbWFwSXRlbS5tb2RlbEtleV0gPSByZXNwb25zZUl0ZW1bbWFwSXRlbS5zaG93S2V5XTtcclxuICAgICAgICAgICAgICB9IGVsc2VcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSXRlbSwgXCIuLi4uLnJlc3BvbnNlSXRlbVwiKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlVmFsdWUsIFwiPj4+Pj5yZXNwb25zZVZhbHVlXCIpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShyZXNwb25zZVZhbHVlKTtcclxuICAgICAgdGhpcy5sZW5ndGggPSB0b3RhbExlbmd0aCA/IHRvdGFsTGVuZ3RoIDogcmVzcG9uc2VWYWx1ZS5sZW5ndGg7XHJcbiAgICB9XHJcbiAgICB0aGlzLnRhYmxlRGF0YSA9IHRlbXBBcnJheTtcclxuICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZSk7XHJcbiAgICB0aGlzLmxlbmd0aCA9IHRvdGFsTGVuZ3RoID8gdG90YWxMZW5ndGggOiByZXNwb25zZVZhbHVlLmxlbmd0aDtcclxuICAgIHRoaXMubmV3VGFibGVEYXRhID0gcHJvY2Vzcyh0aGlzLnRhYmxlRGF0YSwgeyBncm91cDogdGhpcy5ncm91cEZpZWxkIH0pO1xyXG4gICAgdGhpcy5uZXdUYWJsZURhdGEudG90YWwgPSB0aGlzLmxlbmd0aDtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY29sdW1ucywgXCI+Pj4+PkNPTFVNTlNTXCIpO1xyXG4gICAgLy8gdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gXy5tYXAodGhpcy5jb2x1bW5zLCBcInZhbHVlXCIpO1xyXG4gICAgbGV0IHNlbGVjdGVkVGFibGVIZWFkZXJzID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKTsgIC8vIGFmdGVyIGdsb2JhbCBzZXR0aW5nIGNoYW5nZVxyXG4gICAgdGhpcy5jb2x1bW5zID0gIV8uaXNFbXB0eShzZWxlY3RlZFRhYmxlSGVhZGVycykgPyBKU09OLnBhcnNlKHNlbGVjdGVkVGFibGVIZWFkZXJzKSA6IHRoaXMuY29sdW1ucztcclxuICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IHRoaXMuY29sdW1uc1xyXG4gICAgICAuZmlsdGVyKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsLmlzQWN0aXZlO1xyXG4gICAgICB9KTtcclxuICAgIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnNlbGVjdGVkUGFnaW5hdG9yO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZFBhZ2luYXRvciwgXCIuLi4uLi4uLi4uLi4uLi4uLi4uLiBzZWxlY3RlZFBhZ2luYXRvclwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZGF0YVNvdXJjZSwgXCIuLi4uLi4uLi5EQVRBU09VUkNFRUVFXCIpO1xyXG4gICAgLy8gdGhpcy5kYXRhU291cmNlID0gcmVzcG9uc2VLZXlcclxuICAgIC8vICAgPyBuZXcgTWF0VGFibGVEYXRhU291cmNlKHJlc3BvbnNlVmFsdWVbcmVzcG9uc2VLZXldKVxyXG4gICAgLy8gICA6IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZSk7XHJcbiAgfVxyXG4gIHVwZGF0ZUNoZWNrQ2xpY2soc2VsZWN0ZWQsIGV2ZW50KSB7XHJcbiAgICAvLyBzZWxlY3RlZC50eXBlID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlVHlwZVxyXG4gICAgLy8gICA/IHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZVR5cGVcclxuICAgIC8vICAgOiBcIlwiO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVUeXBlKSB7XHJcbiAgICAgIHNlbGVjdGVkLnR5cGUgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVUeXBlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIHNlbGVjdGVkLnR5cGVJZCA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS50eXBlSWRcclxuICAgIC8vICAgPyB0aGlzLmN1cnJlbnRUYWJsZURhdGEudHlwZUlkXHJcbiAgICAvLyAgIDogXCJcIjtcclxuICAgIGxldCBzZWN1cml0eVR5cGVMZW5ndGggPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlUeXBlTGVuZ3RoID8gTnVtYmVyKHRoaXMuY3VycmVudFRhYmxlRGF0YS5zZWN1cml0eVR5cGVMZW5ndGgpIDogMztcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHNlY3VyaXR5VHlwZUxlbmd0aCBcIiwgc2VjdXJpdHlUeXBlTGVuZ3RoKTtcclxuICAgIC8vIGlmIChzZWxlY3RlZCAmJiBzZWxlY3RlZC50eXBlSWQpIHtcclxuICAgIC8vICAgaWYgKHNlbGVjdGVkLnR5cGVJZCA9PSBcIjJcIikge1xyXG4gICAgLy8gICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9IFs0XTtcclxuICAgIC8vICAgICBzZWxlY3RlZFtcImxldmVsXCJdID0gWzRdO1xyXG4gICAgLy8gICB9IGVsc2Uge1xyXG4gICAgLy8gICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9IFsxLCAyLCAzXTtcclxuICAgIC8vICAgICBzZWxlY3RlZFtcImxldmVsXCJdID0gWzEsIDIsIDNdO1xyXG4gICAgLy8gICAgfVxyXG4gICAgbGV0IHRlbXBBcnJheSA9IFtdO1xyXG4gICAgbGV0IHByZWRlZmluZWRBcnJheSA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5zZWN1cml0eUFycmF5VmFsdWUgPyBKU09OLnBhcnNlKFwiW1wiICsgdGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5QXJyYXlWYWx1ZSArIFwiXVwiKSA6IHRlbXBBcnJheTtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHByZWRlZmluZWRBcnJheSBcIiwgcHJlZGVmaW5lZEFycmF5KTtcclxuICAgIGlmIChzZWN1cml0eVR5cGVMZW5ndGggJiYgc2VjdXJpdHlUeXBlTGVuZ3RoID4gMCkge1xyXG4gICAgICBpZiAocHJlZGVmaW5lZEFycmF5ICYmIHByZWRlZmluZWRBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICB0ZW1wQXJyYXkgPSBwcmVkZWZpbmVkQXJyYXk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPD0gc2VjdXJpdHlUeXBlTGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgIHRlbXBBcnJheS5wdXNoKGkpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0gPSB0ZW1wQXJyYXk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoc2VjdXJpdHlUeXBlTGVuZ3RoID09IDApIHtcclxuICAgICAgICBzZWxlY3RlZFtcInNlY3VyaXR5X3R5cGVcIl0gPSB0ZW1wQXJyYXk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2VsZWN0ZWRbXCJzZWN1cml0eV90eXBlXCJdID0gWzEsIDIsIDNdO1xyXG5cclxuXHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIC8vfVxyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gIHNlbGVjdGVkIHNlY3VyaXR5X3R5cGUgXCIsIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSk7XHJcbiAgICBpZiAodGhpcy5jdXJyZW50VGFibGVEYXRhICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5kYXRhRm9ybWF0VG9TYXZlKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmN1cnJlbnRUYWJsZURhdGEuZGF0YUZvcm1hdFRvU2F2ZSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBzZWxlY3RlZFtpdGVtLm5hbWVdID0gc2VsZWN0ZWRbaXRlbS52YWx1ZV07XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wKSA/IEpTT04ucGFyc2UodGVtcCkgOiB7fTtcclxuICAgIGxldCBzYXZlZFNlbGVjdGVkRGF0YSA9XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YSAmJiB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEubGVuZ3RoXHJcbiAgICAgICAgPyB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFcclxuICAgICAgICA6IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZERhdGEgPSBfLm1lcmdlKHNhdmVkU2VsZWN0ZWREYXRhLCB0aGlzLnNlbGVjdGVkRGF0YSk7XHJcbiAgICBpZiAoZXZlbnQuY2hlY2tlZCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0YS5wdXNoKHNlbGVjdGVkKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGxldCBpbmRleCA9IHRoaXMuc2VsZWN0ZWREYXRhLmZpbmRJbmRleChcclxuICAgICAgICBvYmogPT4gb2JqLm9iamVjdF9uYW1lID09PSBzZWxlY3RlZC5vYmplY3RfbmFtZVxyXG4gICAgICApO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSB0aGlzLnNlbGVjdGVkRGF0YTtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnNhdmVJbkFycmF5KSB7XHJcbiAgICAgIGxldCBhcnJheU9iaiA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5zYXZlSW5BcnJheTtcclxuICAgICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZUlkID09PSBhcnJheU9iai5rZXkpIHtcclxuICAgICAgICBsZXQgdGVtcEFycmF5ID1cclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhIHx8IHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aFxyXG4gICAgICAgICAgICA/IF8ubWFwKHRoaXMuc2VsZWN0ZWREYXRhLCBhcnJheU9iai52YWx1ZVRvTWFwKVxyXG4gICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgIHRlbXBBcnJheSA9IHRlbXBBcnJheS5maWx0ZXIoZnVuY3Rpb24gKGVsZW1lbnQpIHtcclxuICAgICAgICAgIHJldHVybiBlbGVtZW50ICE9IG51bGw7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbYXJyYXlPYmoua2V5XSA9IHRlbXBBcnJheTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuc2VsZWN0ZWREYXRhLCBcIl9pZFwiKTtcclxuICAgIGNvbnNvbGUubG9nKFwiU2VsZWN0ZWQgSWQgRGF0YSBMaXN0ID4+Pj4+Pj5cIiwgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSlcclxuXHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgdGhpcy5jaGVja0NsaWNrRXZlbnRNZXNzYWdlLmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gIH1cclxuXHJcbiAgb25TZWxlY3RlZEtleXNDaGFuZ2UoZSkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+IG9uU2VsZWN0ZWRLZXlzQ2hhbmdlIFwiLCBlKTtcclxuICAgIGxldCBsZW4gPSB0aGlzLnNlbGVjdGVkRGF0YS5sZW5ndGg7XHJcbiAgICB0aGlzLnNlbGVjdENvdW50ID0gdGhpcy5zZWxlY3RlZERhdGEubGVuZ3RoO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gbGVuIFwiLGxlbiwgXCJ0b3RhbCByZWNvcmRzIFwiLHRoaXMubmV3VGFibGVEYXRhLmRhdGEudG90YWwsXCIgdGhpcy5sZW5ndGggXCIsdGhpcy5sZW5ndGgsXCIgdGhpcy5saW1pdCBcIix0aGlzLmxpbWl0LFwiID4+PiBzZWxlY3RDb3VudCBcIix0aGlzLnNlbGVjdENvdW50KTtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBsZXQgc2VsZWN0ZWRJZHM9IGU7XHJcbiAgICBpZiAobGVuID09PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0QWxsSXRlbSA9IFwidW5jaGVja2VkXCI7XHJcbiAgICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gZmFsc2U7XHJcbiAgICB9IGVsc2UgaWYgKGxlbiA+IDAgJiYgbGVuIDwgdGhpcy5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5zZWxlY3RBbGxJdGVtID0gXCJpbmRldGVybWluYXRlXCI7XHJcbiAgICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gdHJ1ZTtcclxuICAgICAvLyB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9ICh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLmxlbmd0aCA9PT0gdGhpcy5saW1pdCk/dHJ1ZTpmYWxzZTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSAobGVuPj10aGlzLmxpbWl0KT90cnVlOmZhbHNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5zZWxlY3RBbGxJdGVtID0gXCJjaGVja2VkXCI7XHJcbiAgICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gdHJ1ZTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIGlmKHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aD4wKVxyXG4gICAge1xyXG4gICAgICAvLyBjdXJyZW50IHNlbGVjdGlvblxyXG4gICAgICBsZXQgdGVtcERhdGEgPSBbXTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2goc2VsZWN0ZWRJZHMsIGZ1bmN0aW9uICh0YWJsZUl0ZW0pIHtcclxuICAgICAgICBsZXQgaW5kZXhzID0gc2VsZi5uZXdUYWJsZURhdGEuZGF0YS5maWx0ZXIob2JqID0+IG9ialtcIl9pZFwiXSA9PSB0YWJsZUl0ZW0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiPj4+PiBuZGV4cy5sZW5ndGggXCIsaW5kZXhzLmxlbmd0aCk7XHJcbiAgICAgICAgaWYgKGluZGV4cy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICB0ZW1wRGF0YS5wdXNoKGluZGV4c1swXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgICAgdGhpcy5zZWxlY3RDb3VudD10ZW1wRGF0YS5sZW5ndGg7XHJcbiAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXT1fLmNvbmNhdCh0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSx0ZW1wRGF0YSk7XHJcbiAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9Xy51bmlxQnkodGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0sXCJfaWRcIik7XHJcbiAgICAgICBzZWxlY3RlZElkcz1fLmNvbmNhdCh0aGlzLnNlbGVjdGVkRGF0YSxzZWxlY3RlZElkcyk7IC8vIHRvdGFsIHNlbGVjdGVkIGlkc1xyXG4gICAgfVxyXG4gICAgdGhpcy5zZWxlY3RlZERhdGE9Xy51bmlxKHNlbGVjdGVkSWRzKTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl09dGhpcy5zZWxlY3RlZERhdGE7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgdGhpcy5jaGVja0NsaWNrRXZlbnRNZXNzYWdlLmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gIH1cclxuXHJcbiAgb25TZWxlY3RBbGxDaGFuZ2UoY2hlY2tlZFN0YXRlOiBTZWxlY3RBbGxDaGVja2JveFN0YXRlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcInNlbGVjdEFsbCAtPiBcIiwgY2hlY2tlZFN0YXRlKTtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBpZiAoY2hlY2tlZFN0YXRlID09PSAnY2hlY2tlZCcpIHtcclxuICAgICAgdGhpcy5uZXdUYWJsZURhdGEuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICBvYmouY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IHRoaXMubmV3VGFibGVEYXRhLmRhdGE7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLCBcIl9pZFwiKTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSB0cnVlO1xyXG4gICAgICB0aGlzLnNlbGVjdEFsbEl0ZW0gPSAnY2hlY2tlZCc7XHJcbiAgICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gdHJ1ZTtcclxuICB9IGVsc2Uge1xyXG4gICAgdGhpcy5zZWxlY3RlZERhdGE9W107XHJcbiAgICB0aGlzLm5ld1RhYmxlRGF0YS5kYXRhLm1hcChvYmogPT4ge1xyXG4gICAgICBvYmouY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IFtdO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RBbGxcIl0gPSBmYWxzZTtcclxuICAgIHRoaXMuc2VsZWN0QWxsSXRlbSA9ICd1bmNoZWNrZWQnO1xyXG4gICAgdGhpcy5lbmFibGVEZWxldGUgPSBmYWxzZTtcclxuICB9XHJcbiAgbGV0IHNlbGVjdGVkSWRzPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdO1xyXG4gIGlmKHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aD4wKVxyXG4gIHtcclxuICAgIHNlbGVjdGVkSWRzPV8uY29uY2F0KHRoaXMuc2VsZWN0ZWREYXRhLHNlbGVjdGVkSWRzKTtcclxuICB9XHJcbiAgdGhpcy5zZWxlY3RlZERhdGE9Xy51bmlxKHNlbGVjdGVkSWRzKTtcclxuICB0aGlzLnNlbGVjdENvdW50ID0gdGhpcy5zZWxlY3RlZERhdGEubGVuZ3RoO1xyXG4gIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgfVxyXG4gIHNlbGVjdEFsbENoZWNrKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCk7XHJcbiAgICBpZiAoZXZlbnQuY2hlY2tlZCA9PT0gdHJ1ZSkge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICBvYmouY2hlY2tlZCA9IHRydWU7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IHRoaXMuZGF0YVNvdXJjZS5kYXRhO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAodGhpcy5kYXRhU291cmNlLmRhdGEsIFwiX2lkXCIpO1xyXG4gICAgICBjb25zb2xlLmxvZyhcIkNoZWNrIEV2ZW50IEFsbCBJZCBMaXN0ID4+Pj4+XCIsIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0pO1xyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IHRydWU7XHJcbiAgICAgIHRoaXMuc2VsZWN0Q291bnQgPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdLmxlbmd0aDtcclxuICAgICAgdGhpcy5zZWxlY3RlZERhdGEucHVzaCh0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdKTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICBvYmouY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0YT1bXVxyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICB9XHJcbiAgYWRkRmllbGRDbGljayhpdGVtLCBvYmplY3RMaXN0KSB7XHJcbiAgICBjb25zb2xlLmxvZyhpdGVtLCBcIi4uLi4uLi4uLi5JVEVNXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiLi4uLi4uLklOUFVUIERBdGFcIilcclxuICAgIGNvbnNvbGUubG9nKG9iamVjdExpc3QsIFwiLi4uLi4uT2JqZWN0TGlzdFwiKTtcclxuICB9XHJcblxyXG4gIHNlbGVjdEFsbERhdGEoKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIlNlbGVjdCBhbGwgRGF0YSA+Pj4+PlwiKTtcclxuICAgIHRoaXMuc2VsZWN0QWxsSXRlbSA9ICdjaGVja2VkJztcclxuICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEuc2VsZWN0QWxsUmVxdWVzdDtcclxuICAgIGxldCBxdWVyeSA9IHt9O1xyXG4gICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBxdWVyeVtpdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICB9KTtcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnksIHJlcXVlc3REZXRhaWxzLmFwaVVybClcclxuICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID1cclxuICAgICAgICBkYXRhLnJlc3BvbnNlW3JlcXVlc3REZXRhaWxzLnJlc3BvbnNlTmFtZV07XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERhdGE9dGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlNlbGVjdGVkIElkIGxpc3QgQEBAQEBAQEBAXCIsIHRoaXMuc2VsZWN0ZWREYXRhKTtcclxuICAgICAgICBcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGNsZWFyU2VsZWN0QWxsRGF0YSgpIHtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdPVtdO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl09W107XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgdGhpcy5lbmFibGVEZWxldGUgPSBmYWxzZTtcclxuICAgIHRoaXMubmdPbkluaXQoKTtcclxuICB9XHJcblxyXG4gIGFwcGx5RmlsdGVyKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4gZXZlbnQgXCIsIGV2ZW50KTtcclxuICAgIGlmIChldmVudCAmJiBldmVudC5maWx0ZXJzKSB7XHJcbiAgICAgIGxldCBmaWx0ZXJWYWx1ZSA9IGV2ZW50LmZpbHRlcnMgPyBldmVudC5maWx0ZXJzWzBdIDoge307XHJcbiAgICAgIHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUgPSB7XHJcbiAgICAgICAgc2VhcmNoS2V5OiBmaWx0ZXJWYWx1ZS5maWVsZCxcclxuICAgICAgICBzZWFyY2hWYWx1ZTogZmlsdGVyVmFsdWUudmFsdWVcclxuICAgICAgfTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbZmlsdGVyVmFsdWUuZmllbGRdID0gZmlsdGVyVmFsdWUudmFsdWU7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLFwiPj4+Pj4gSU5QVVQgREFUQVwiKTtcclxuICAgICAgbGV0IHRhYmxlQXJyYXkgPVxyXG4gICAgICAgIHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLnRhYmxlRGF0YVxyXG4gICAgICAgICAgPyB0aGlzLm9uTG9hZC50YWJsZURhdGFcclxuICAgICAgICAgIDogdGhpcy5jdXJyZW50Q29uZmlnRGF0YVtcImxpc3RWaWV3XCJdLnRhYmxlRGF0YTtcclxuICAgICAgbGV0IGluZGV4ID0gXy5maW5kSW5kZXgodGFibGVBcnJheSwgeyB0YWJsZUlkOiB0aGlzLnRhYmxlSWQgfSk7XHJcbiAgICAgIGxldCBzZWFyY2hSZXF1ZXN0ID0gdGFibGVBcnJheVtpbmRleF0ub25UYWJsZVNlYXJjaDtcclxuICAgICAgbGV0IHJlcXVlc3QgPSBzZWFyY2hSZXF1ZXN0LnJlcXVlc3REYXRhO1xyXG4gICAgICBsZXQgaXNVaXNlcmFjaCA9IChzZWFyY2hSZXF1ZXN0LnVpU2VhcmNoKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgaWYgKGlzVWlzZXJhY2gpIHtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyID0gZXZlbnQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVEYXRhID0gdGFibGVBcnJheVtpbmRleF07XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgICAgICAgbGltaXQ6IDEwLFxyXG4gICAgICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKHNlYXJjaFJlcXVlc3QgJiYgc2VhcmNoUmVxdWVzdC5pc1NpbmdsZXJlcXVlc3QpIHtcclxuICAgICAgICAgIHF1ZXJ5W3NlYXJjaFJlcXVlc3Quc2luZ2xlUmVxdWVzdEtleV0gPSBldmVudDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3QsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgIGxldCB0ZW1wRGF0YSA9IGl0ZW0uc3ViS2V5XHJcbiAgICAgICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgICAgICA6IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICBpZiAoaXRlbS5kaXJlY3RBc3NpZ24gfHwgaXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShpdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICAgICAgOiBpdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRlbXBEYXRhKSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkodGVtcERhdGEpXHJcbiAgICAgICAgICAgICAgICA6IHRlbXBEYXRhO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gdGhpcy5xdWVyeVBhcmFtcyA9IHF1ZXJ5O1xyXG4gICAgICAgIHRoaXMuZ2V0RGF0YSh0aGlzLmN1cnJlbnRUYWJsZURhdGEsIFwidGFibGVTZWFyY2hcIik7XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcblxyXG4gIH1cclxuICBmaWx0ZXJSZXNldChmaWVsZCkge1xyXG4gICAgZGVsZXRlIHRoaXMucXVlcnlQYXJhbXNbZmllbGQucmVxdWVzdEtleV07XHJcbiAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgfVxyXG4gIG9uTXVsdGlTZWxlY3QoZXZlbnQsIHJvd1ZhbCkge1xyXG4gICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wKSA/IEpTT04ucGFyc2UodGVtcCkgOiB7fTtcclxuICAgIGxldCBzZWxlY3RlZERhdGEgPSB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGE7XHJcbiAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleChzZWxlY3RlZERhdGEsIHsgb2JqX25hbWU6IHJvd1ZhbC5vYmpfbmFtZSB9KTtcclxuICAgIGlmIChpbmRleCA+IC0xICYmIHNlbGVjdGVkRGF0YSAmJiBzZWxlY3RlZERhdGEubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YVtpbmRleF0uc2VjdXJpdHlfdHlwZSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFbaW5kZXhdLmxldmVsID0gZXZlbnQudmFsdWU7XHJcbiAgICB9XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gIH1cclxuXHJcbiAgY2hhbmdlUGFnZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiPj4+IEVWRU5UXCIpXHJcbiAgICAvLyBpZiAoZXZlbnQucGFnZVNpemUgIT0gdGhpcy5saW1pdCkge1xyXG4gICAgLy8gICB0aGlzLnF1ZXJ5UGFyYW1zW1wibGltaXRcIl0gPSBldmVudC5wYWdlU2l6ZTtcclxuICAgIC8vICAgdGhpcy5xdWVyeVBhcmFtc1tcIm9mZnNldFwiXSA9IDA7XHJcbiAgICAvLyB9IGVsc2Uge1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtc1tcIm9mZnNldFwiXSA9IGV2ZW50LnNraXA7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wibGltaXRcIl0gPSB0aGlzLmxpbWl0O1xyXG4gICAgdGhpcy5vZmZzZXQgPSBldmVudC5za2lwO1xyXG4gICAgLy8gfVxyXG4gICAgdGhpcy5vbkxvYWREYXRhKFwibmV4dFwiKTtcclxuICB9XHJcbiAgdmlld0RhdGEoZWxlbWVudCwgY29sKSB7XHJcbiAgICBjb25zb2xlLmxvZyhjb2wsIFwidmlld0RhdGEgPj4+Pj4+Pj4+IGlzUmVkaXJlY3QgXCIsIGNvbC5pc1JlZGlyZWN0LCB0aGlzLmVudmlyb25tZW50KTtcclxuICAgIGlmIChjb2wuaXNSZWRpcmVjdCkge1xyXG4gICAgICBsZXQgdGVzdFN0ciA9IFwiL2NvbnRyb2wtbWFuYWdlbWVudC9hY2Nlc3MtY29udHJvbFwiXHJcbiAgICAgIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gYCR7dGhpcy5yZWRpcmVjdFVyaX1gICsgdGVzdFN0cjtcclxuXHJcbiAgICAgIGNvbnNvbGUubG9nKFwiUmVkaXJlY3RlZCAqKioqKioqKioqKioqKioqKioqKlwiKVxyXG4gICAgICAvLyB0aGlzLnJvdXRlLm5hdmlnYXRlQnlVcmwoXCIvY29udHJvbC1tYW5hZ2VtZW50L2FjY2Vzcy1jb250cm9sXCIpO1xyXG5cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuYWN0aW9uQ2xpY2soZWxlbWVudCwgXCJ2aWV3XCIpO1xyXG4gICAgfVxyXG4gIH1cclxuICBhZGRCdXR0b25DbGljayhlbGVtZW50LCBhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKFwiYWRkQnV0dG9uQ2xpY2sgXCIpO1xyXG4gICAgY29uc29sZS5sb2coXCIgZWxlbWVudCBcIiwgZWxlbWVudCk7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBhY3Rpb24gXCIsIGFjdGlvbik7XHJcbiAgfVxyXG4gIGFjdGlvbkNsaWNrKGVsZW1lbnQsIGFjdGlvbikge1xyXG4gICAgY29uc29sZS5sb2coZWxlbWVudCwgXCI+Pj4+ZWxlbWVudFwiKVxyXG4gICAgaWYgKGFjdGlvbiA9PSBcImVkaXRcIiB8fCBhY3Rpb24gPT0gXCJ2aWV3XCIgfHwgYWN0aW9uID09IFwidmlzaWJpbGl0eVwiIHx8IGFjdGlvbiA9PSBcImluZm9cIikge1xyXG4gICAgICBhY3Rpb24gPSAoYWN0aW9uID09ICd2aXNpYmlsaXR5JyB8fCBhY3Rpb24gPT0gXCJpbmZvXCIpID8gJ3ZpZXcnIDogYWN0aW9uO1xyXG4gICAgICB0aGlzLm9wZW5EaWFsb2coZWxlbWVudCwgYWN0aW9uKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwiZGVsZXRlXCIpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4+Pj4+dGhpc1wiKTtcclxuICAgICAgaWYgKHRoaXMub25Mb2FkICYmIHRoaXMub25Mb2FkLmlucHV0S2V5KSB7XHJcbiAgICAgICAgbGV0IGlucHV0S2V5ID0gdGhpcy5vbkxvYWQuaW5wdXRLZXk7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5wdXREYXRhICYmIHRoaXMuaW5wdXREYXRhW2lucHV0S2V5XSkge1xyXG4gICAgICAgICAgbGV0IHNlbGVjdGVkRGF0YSA9IHRoaXMuaW5wdXREYXRhW2lucHV0S2V5XTtcclxuICAgICAgICAgIGxldCBpbmRleCA9IHNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIG9iaiA9PiBvYmogPT0gZWxlbWVudFxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNlbGVjdGVkRGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbaW5wdXRLZXldID0gc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHNlbGVjdGVkRGF0YSwgXCJfaWRcIik7XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHNlbGVjdGVkRGF0YSk7XHJcbiAgICAgICAgICB0aGlzLmxlbmd0aCA9IHNlbGVjdGVkRGF0YS5sZW5ndGg7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgLy8gdGhpcy5hY3Rpb25DbGlja0V2ZW50LmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAodGhpcy5pbnB1dERhdGEgJiYgdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhKSB7XHJcbiAgICAgICAgICBsZXQgc2VsZWN0ZWREYXRhID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgICAgbGV0IGluZGV4ID0gc2VsZWN0ZWREYXRhLmZpbmRJbmRleChcclxuICAgICAgICAgICAgb2JqID0+IG9iai5vYmplY3RfbmFtZSA9PSBlbGVtZW50Lm9iamVjdF9uYW1lXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgc2VsZWN0ZWREYXRhLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IHNlbGVjdGVkRGF0YTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcChzZWxlY3RlZERhdGEsIFwiX2lkXCIpO1xyXG4gICAgICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShzZWxlY3RlZERhdGEpO1xyXG4gICAgICAgICAgdGhpcy5sZW5ndGggPSBzZWxlY3RlZERhdGEubGVuZ3RoO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgIC8vIHRoaXMuYWN0aW9uQ2xpY2tFdmVudC5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwicmVtb3ZlX3JlZF9leWVcIikge1xyXG4gICAgICB0aGlzLm9wZW5EaWFsb2coZWxlbWVudCwgXCJ2aWV3XCIpO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gXCJyZXN0b3JlX3BhZ2VcIikge1xyXG4gICAgICB0aGlzLm9wZW5EaWFsb2coZWxlbWVudCwgYWN0aW9uKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09ICdhc2tkZWZhdWx0YnV0dG9uJykge1xyXG4gICAgICBsZXQgZHNpZCA9IGVsZW1lbnQuX2lkO1xyXG4gICAgICBsZXQgZHNuYW1lID0gZWxlbWVudC5uYW1lO1xyXG4gICAgICBjb25zb2xlLmxvZyhcIiBkc2lkIFwiLCBkc2lkLCBcIiBkc25hbWUgXCIsIGRzbmFtZSk7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgbGV0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhO1xyXG4gICAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgICAgZGVmYXVsdERhdGFzb3VyY2UgOiB0cnVlXHJcbiAgICAgIH07XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMuc2V0RGVmYXVsdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAudXBkYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMuc2V0RGVmYXVsdC5hcGlVcmwsIGRzaWQpXHJcbiAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZERhdGFzb3VyY2UoZHNpZCk7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG5cclxuICAgICAgLy8gdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kRGF0YXNvdXJjZShkc2lkKTtcclxuICAgICAgLy8gdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAvLyAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgLy8gICAgIFwiRGF0YXNvdXJjZSBEZWZhdWx0IFVwZGF0ZWQgU3VjY2Vzc2Z1bGx5IVwiXHJcbiAgICAgIC8vICAgKVxyXG4gICAgICAvLyApO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YS5kYXRhc291cmNlSWQgPSBkc2lkO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gXCJzYXZlX2FsdFwiKSB7XHJcbiAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEuZG93bmxvYWRSZXF1ZXN0O1xyXG4gICAgICBsZXQgcXVlcnkgPSB7XHJcbiAgICAgICAgcmVwb3J0SWQ6IGVsZW1lbnQuX2lkXHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgICAgIGNvbnN0IGFiID0gbmV3IEFycmF5QnVmZmVyKGRhdGEuZGF0YS5sZW5ndGgpO1xyXG4gICAgICAgICAgY29uc3QgdmlldyA9IG5ldyBVaW50OEFycmF5KGFiKTtcclxuICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZGF0YS5kYXRhLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIHZpZXdbaV0gPSBkYXRhLmRhdGFbaV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBsZXQgZG93bmxvYWRUeXBlID0gJ2FwcGxpY2F0aW9uL3ppcCc7XHJcbiAgICAgICAgICBjb25zdCBmaWxlID0gbmV3IEJsb2IoW2FiXSwgeyB0eXBlOiBkb3dubG9hZFR5cGUgfSk7XHJcbiAgICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGZpbGUsIGVsZW1lbnQuZmlsZU5hbWUgKyAnLnppcCcpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gXCJzcGVha2VyX25vdGVzXCIgfHwgYWN0aW9uID09IFwic3BlYWtlcl9ub3Rlc19vZmZcIikge1xyXG4gICAgICBjb25zdCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudERhdGEubm90aWZpY2F0aW9uUmVhZDtcclxuICAgICAgbGV0IGlkc0xpc3QgPSBbXTtcclxuICAgICAgaWRzTGlzdC5wdXNoKGVsZW1lbnQuX2lkKTtcclxuICAgICAgLy8gaWYgKGFjdGlvbiA9PSBcIm5vdGRlbGV0ZVwiKSB7XHJcbiAgICAgICAgLy8gcXVlcnlPYmpbXCJkZWxldGVcIl0gPSB0cnVlO1xyXG4gICAgICAvLyAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICAvLyAgICAgaXNyZWFkOiB0cnVlLFxyXG4gICAgICAvLyAgICAgZmVhdHVyZTogXCJkZWxldGVcIixcclxuICAgICAgLy8gICAgIGlkTGlzdDogaWRzTGlzdFxyXG4gICAgICAvLyAgIH1cclxuICAgICAgLy8gfVxyXG4gICAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgICAgaXNyZWFkOiAoYWN0aW9uID09IFwic3BlYWtlcl9ub3Rlc1wiKSA/IHRydWUgOiBmYWxzZSxcclxuICAgICAgICBmZWF0dXJlOiBcImlzcmVhZFwiLFxyXG4gICAgICAgIGlkTGlzdDogaWRzTGlzdFxyXG4gICAgICB9XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSByZXF1ZXN0RGV0YWlscy50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMuYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwibm90ZGVsZXRlXCIpIHtcclxuICAgICAgY29uc3QgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnREYXRhLm5vdGlmaWNhdGlvblJlYWQ7XHJcbiAgICAgIGxldCBpZHNMaXN0ID0gW107XHJcbiAgICAgIGlkc0xpc3QucHVzaChlbGVtZW50Ll9pZCk7XHJcbiAgICAgIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgICBmZWF0dXJlOiBcImRlbGV0ZVwiLFxyXG4gICAgICAgIGlkTGlzdDogaWRzTGlzdCxcclxuICAgICAgICBpc3JlYWQ6IHRydWVcclxuICAgICAgfVxyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLmFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB1cGRhdGVTdGF0dXMoYWN0aW9uTmFtZSwgc2VsZWN0ZWRJZCl7XHJcbiAgICBsZXQgcXVlcnkgPSB7fTtcclxuICAgIGlmKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc3RhdHVzVXBkYXRlKXtcclxuICAgICAgdmFyIGN1cnJlbnRSZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5zdGF0dXNVcGRhdGU7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gY3VycmVudFJlcXVlc3REZXRhaWxzLnRvYXN0TWVzc2FnZTtcclxuICAgICAgcXVlcnlbXCJ1cGRhdGVJZFwiXSA9IHNlbGVjdGVkSWQuX2lkO1xyXG4gICAgICBxdWVyeVtcInN0YXR1c1wiXSA9IGFjdGlvbk5hbWU7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAudXBkYXRlUmVxdWVzdChxdWVyeSwgY3VycmVudFJlcXVlc3REZXRhaWxzLmFwaVVybCwgc2VsZWN0ZWRJZC5faWQpXHJcbiAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgYWN0aW9uUmVkaXJlY3QoZWxlbWVudCwgY29sdW1uRGF0YSkge1xyXG4gICAgY29uc29sZS5sb2coXCJhY3Rpb25SZWRpcmVjdCA+Pj4+Pj4gXCIsIGVsZW1lbnQpO1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgbGV0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50VGFibGVEYXRhXHJcbiAgICBsZXQgcXVlcnlPYmogPSB7fTtcclxuICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS50b2FzdE1lc3NhZ2U7XHJcblxyXG4gICAgY29uc29sZS5sb2coXCJyZXF1ZXN0RGV0YWlscyA+Pj4+Pj4gXCIsIHJlcXVlc3REZXRhaWxzKTtcclxuICAgIGlmIChjb2x1bW5EYXRhLnJ1bGVzZXRDaGVjaykge1xyXG4gICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBxdWVyeU9ialtpdGVtLm5hbWVdID0gaXRlbS5zdWJrZXlcclxuICAgICAgICAgID8gZWxlbWVudFtpdGVtLnZhbHVlXVtpdGVtLnN1YmtleV1cclxuICAgICAgICAgIDogZWxlbWVudFtpdGVtLnZhbHVlXTtcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBxdWVyeU9iaiA9IHtcclxuICAgICAgICBkZWZhdWx0RGF0YXNvdXJjZSA6IHRydWVcclxuICAgICAgfTtcclxuICAgICAgY29uc29sZS5sb2coXCJkYXRhIHNvdXJjZSA+Pj4+XCIpO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgIC51cGRhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCByZXF1ZXN0RGV0YWlscy5vblRhYmxlVXBkYXRlLmFwaVVybCwgZWxlbWVudC5faWQpXHJcbiAgICAuc3Vic2NyaWJlKFxyXG4gICAgICByZXMgPT4ge1xyXG4gICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgICAgICBpZiAoIWNvbHVtbkRhdGEucnVsZXNldENoZWNrKSB7XHJcbiAgICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmREYXRhc291cmNlKGVsZW1lbnQuX2lkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICB9LFxyXG4gICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgICBjb25zb2xlLmxvZygnYWN0aW9uUmVkaXJlY3QgPj4+Pj4+Pj4+Pj4+Pj4+IHF1ZXJ5T2JqICcsIHF1ZXJ5T2JqKVxyXG4gICAgY29uc29sZS5sb2coXCJRdWVyeSBPYmogPj4+XCIsIHF1ZXJ5T2JqKTtcclxuICAgIGNvbnNvbGUubG9nKFwiVXBkYXRlIFVSTCA+Pj5cIiwgcmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS5hcGlVcmwpO1xyXG5cclxuXHJcbiAgfVxyXG4gIG9uVG9nZ2xlQ2hhbmdlKGV2ZW50LCBlbGVtZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+PiBldmVudCBcIiwgZXZlbnQsIFwiIDplbGVtZW50IFwiLCBlbGVtZW50KTtcclxuICAgIGVsZW1lbnQuc3RhdHVzID0gZXZlbnQuY2hlY2tlZCA/IFwiQUNUSVZFXCIgOiBcIklOQUNUSVZFXCI7XHJcbiAgICBsZXQgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGFcclxuICAgICAgPyB0aGlzLmN1cnJlbnRUYWJsZURhdGEub25Ub2dnbGVDaGFuZ2VcclxuICAgICAgOiBcIlwiO1xyXG4gICAgaWYgKHJlcXVlc3REZXRhaWxzKSB7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCBxdWVyeU9iaiA9IHt9O1xyXG4gICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgLy8gcXVlcnlPYmpbaXRlbS5uYW1lXSA9IGl0ZW0uc3Via2V5XHJcbiAgICAgICAgLy8gICA/IGVsZW1lbnRbaXRlbS52YWx1ZV1baXRlbS5zdWJrZXldXHJcbiAgICAgICAgLy8gICA6IGVsZW1lbnRbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgICBpZihpdGVtLnN1YmtleSlcclxuICAgICAgICAgIHtcclxuICAgICAgICAgICAgcXVlcnlPYmpbaXRlbS5uYW1lXT1lbGVtZW50W2l0ZW0udmFsdWVdW2l0ZW0uc3Via2V5XTtcclxuICAgICAgICAgIH1lbHNlIGlmKGl0ZW0uZnJvbVRlbmFudERhdGEpe1xyXG4gICAgICAgICAgICBxdWVyeU9ialtpdGVtLm5hbWVdPXNlbGYuY3VycmVudFRlbmFudFtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgIHF1ZXJ5T2JqW2l0ZW0ubmFtZV09ZWxlbWVudFtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKCdUb2dnbGUgVVJMID4+PicsIHJlcXVlc3REZXRhaWxzLmFwaVVybCk7XHJcblxyXG4gICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLnVwZGF0ZVJlcXVlc3QocXVlcnlPYmosIHJlcXVlc3REZXRhaWxzLmFwaVVybCwgZWxlbWVudC5faWQpXHJcbiAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICAgIGlmKGVsZW1lbnQuc3RhdHVzPT0nQUNUSVZFJyAmJiAgdG9hc3RNZXNzYWdlRGV0YWlscy5lbmFibGUgKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVuYWJsZVxyXG4gICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9ZWxzZSBpZihlbGVtZW50LnN0YXR1cz09J0lOQUNUSVZFJyAmJiAgdG9hc3RNZXNzYWdlRGV0YWlscy5kaXNhYmxlKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmRpc2FibGVcclxuICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgIGlmKHJlcXVlc3REZXRhaWxzLmlzUGFnZVJlZnJlc2gpe1xyXG4gICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2VsZi5vbkxvYWREYXRhKG51bGwpO1xyXG4gICAgICAgICAgICAgIH0sIDEwMCk7XHJcbiAgICAgICAgICAgICBcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfSBlbHNlIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnNhdmVzZWxlY3RlZFRvZ2dsZSkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVt0aGlzLmN1cnJlbnRUYWJsZURhdGFbXCJrZXlUb3NhdmVcIl1dID0gdGhpcy5kYXRhU291cmNlLmRhdGE7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB9XHJcbiAgfVxyXG5jaGVja0NvbmZpcm0oZWxlbWVudCxhY3Rpb24sY29sdW1uKXtcclxuICAgIHRoaXMub3BlbkNvbmZpcm1EaWFsb2coZWxlbWVudCxhY3Rpb24sY29sdW1uKTtcclxuIH1cclxub3BlbkNvbmZpcm1EaWFsb2coZWxlbWVudCxhY3Rpb24sY29sdW1uKXtcclxuICBjb25zb2xlLmxvZyhcIiBvcGVuQ29uZmlybURpYWxvZyBEaWFsb2cgKioqKioqKioqIGVsZW1lbnQ6IFwiLCBlbGVtZW50ICxcIiAqKioqIGFjdGlvbiBcIixhY3Rpb24sXCI+Pj4gY29sdW1uIFwiLGNvbHVtbik7XHJcbiAgbGV0IG1vZGVsV2lkdGggPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW2FjdGlvbl0ubW9kZWxEYXRhLnNpemU7XHJcbiAgbGV0IG1vZGVsRGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGE7XHJcbiAgY29uc29sZS5sb2coXCI+Pj4gbW9kZWxXaWR0aCBcIixtb2RlbFdpZHRoKTtcclxuICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZ1xyXG4gIC5vcGVuKENvbmZpcm1EaWFsb2dDb21wb25lbnQsIHtcclxuICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgcGFuZWxDbGFzczogXCJjb250YWN0LWZvcm0tZGlhbG9nXCIsXHJcbiAgICBkYXRhOiB7XHJcbiAgICAgIGFjdGlvbjogYWN0aW9uLFxyXG4gICAgICBzYXZlZERhdGE6IGVsZW1lbnQsXHJcbiAgICAgIG1vZGVsRGF0YTogbW9kZWxEYXRhXHJcbiAgICB9XHJcbiAgfSlcclxuICAuYWZ0ZXJDbG9zZWQoKVxyXG4gIC5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+IENvbmZpcm0gbW9kYWwgcmVzcG9uc2UgXCIscmVzcG9uc2UpO1xyXG4gICAgaWYocmVzcG9uc2UgJiYgY29sdW1uLnJlZGlyZWN0QWN0aW9uKXtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGVsZW1lbnQsIGNvbHVtbi5yZWRpcmVjdEFjdGlvbilcclxuICAgICB9IGVsc2Uge1xyXG4gICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgfVxyXG4gIH0pO1xyXG59XHJcbiAgb3BlbkRpYWxvZyhlbGVtZW50LCBhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKFwiIE9wZW4gRGlhbG9nICoqKioqKioqKiBlbGVtZW50OiBcIiwgZWxlbWVudCk7XHJcbiAgICBsZXQgbW9kZWxXaWR0aCA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGEuc2l6ZTtcclxuICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nXHJcbiAgICAgIC5vcGVuKE1vZGVsTGF5b3V0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIGFjdGlvbjogYWN0aW9uLFxyXG4gICAgICAgICAgc2F2ZWREYXRhOiBlbGVtZW50XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgICAuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG4gIG9uU2VsZWN0T3B0aW9uYmFja3VwKGl0ZW0pIHtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICBpZiAoaXRlbS52YWx1ZSA9PSAnbm9uZScpIHtcclxuICAgICAgXy5mb3JFYWNoKHNlbGYubmV3VGFibGVEYXRhLmRhdGEsIGZ1bmN0aW9uICh0YWJsZUl0ZW0pIHtcclxuICAgICAgICB0YWJsZUl0ZW0uY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmVuYWJsZURlbGV0ZSA9IGZhbHNlO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKGl0ZW0udmFsdWUgIT0gJ2FsbCcpIHtcclxuICAgICAgICBzZWxmLnF1ZXJ5UGFyYW1zW2l0ZW0ubmFtZV0gPSBpdGVtLnZhbHVlO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiU2VsZWN0IFZhbHVlID4+Pj4+Pj4+PlwiLCBpdGVtKTtcclxuICAgICAgICBpZiAoaXRlbS5rZXlUb1Nob3cgPT0gXCJSZWFkXCIpIHtcclxuICAgICAgICAgIHNlbGYuY3VycmVudERhdGEuaXNSZWFkID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgc2VsZi5jdXJyZW50RGF0YS5pc1JlYWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gdHJ1ZTtcclxuICAgICAgdGhpcy5lbmFibGVEZWxldGUgPSB0cnVlO1xyXG4gICAgICB0aGlzLmdldERhdGEodGhpcy5jdXJyZW50VGFibGVEYXRhLCBudWxsKTtcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHRoaXMuaW5wdXREYXRhIFwiLCB0aGlzLmlucHV0RGF0YSk7XHJcbiAgfVxyXG4gIG9uU2VsZWN0T3B0aW9uKGl0ZW0pIHtcclxuICAgIGNvbnNvbGUubG9nKFwiU2VsZWN0IFZhbHVlID4+Pj4+Pj4+PlwiLCBpdGVtKTtcclxuICAgIGxldCBleGlzdENoZWNrID0gXy5oYXModGhpcy5xdWVyeVBhcmFtcywgaXRlbS5uYW1lKTtcclxuICAgIGlmIChpdGVtLmFsdGVybmF0aXZlS2V5ID09ICd1bnJlYWQnKSB7XHJcbiAgICAgIGRlbGV0ZSB0aGlzLnF1ZXJ5UGFyYW1zW1widW5yZWFkXCJdO1xyXG4gICAgICAvLyBfLm9taXQodGhpcy5xdWVyeVBhcmFtcywgdGhpcy5xdWVyeVBhcmFtc1tcInVucmVhZFwiXSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBkZWxldGUgdGhpcy5xdWVyeVBhcmFtc1tcImlzcmVhZFwiXTtcclxuICAgICAgLy8gXy5vbWl0KHRoaXMucXVlcnlQYXJhbXMsIHRoaXMucXVlcnlQYXJhbXNbXCJpc3JlYWRcIl0pO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coXCJRdWVyeSBQYXJhbXMgPj5cIiwgdGhpcy5xdWVyeVBhcmFtcyk7XHJcbiAgICBpZiAodGhpcy5xdWVyeVBhcmFtc1tcIm9wZXJhdGlvblwiXSAhPSBpdGVtLm5hbWUpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWREYXRhPVtdO1xyXG4gICAgdGhpcy5uZXdUYWJsZURhdGEuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgb2JqLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgIH0pO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBbXTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0QWxsXCJdID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlbGVjdEFsbEl0ZW0gPSAndW5jaGVja2VkJztcclxuICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wib3BlcmF0aW9uXCJdID0gaXRlbS5uYW1lO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtc1tpdGVtLm5hbWVdID0gaXRlbS52YWx1ZTtcclxuICAgIHRoaXMuZW5hYmxlRGVsZXRlID0gZmFsc2U7XHJcbiAgICB0aGlzLmdldERhdGEodGhpcy5jdXJyZW50VGFibGVEYXRhLCBudWxsKTtcclxuICB9XHJcbiAgRGVsZXRlT3B0aW9uKCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgbGV0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50RGF0YS5ub3RpZmljYXRpb25SZWFkO1xyXG4gICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSByZXF1ZXN0RGV0YWlscy50b2FzdE1lc3NhZ2U7XHJcbiAgICAvLyB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAodGhpcy5kYXRhU291cmNlLmRhdGEsIFwiX2lkXCIpO1xyXG4gICAgY29uc3QgaW5wdXQgPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdO1xyXG4gICAgY29uc29sZS5sb2coXCJJbnB1dCBkYXRhID4+Pj5cIiwgaW5wdXQpO1xyXG5cclxuICAgIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgZmVhdHVyZTogXCJkZWxldGVcIixcclxuICAgICAgaWRMaXN0OiBpbnB1dFxyXG4gICAgfVxyXG4gICAgaWYgKGlucHV0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZS5jcmVhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gIH1cclxuICBSZWFkKCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuZGF0YVNvdXJjZS5kYXRhLCBcIl9pZFwiKTtcclxuICAgIGNvbnN0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50RGF0YS5ub3RpZmljYXRpb25SZWFkO1xyXG4gICAgY29uc3QgaW5wdXQgPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdO1xyXG4gICAgY29uc29sZS5sb2coXCJJbnB1dCBkYXRhID4+Pj5cIiwgaW5wdXQpO1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICBpc3JlYWQ6IHRydWUsXHJcbiAgICAgIGZlYXR1cmU6IFwiaXNyZWFkXCIsXHJcbiAgICAgIGlkTGlzdDogaW5wdXRcclxuICAgIH1cclxuICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICBcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMuYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gIH1cclxuXHJcbiAgVW5yZWFkKCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXVxyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuZGF0YVNvdXJjZS5kYXRhLCBcIl9pZFwiKTtcclxuICAgIGNvbnN0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50RGF0YS5ub3RpZmljYXRpb25SZWFkO1xyXG4gICAgY29uc3QgaW5wdXQgPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdO1xyXG4gICAgY29uc29sZS5sb2coXCJTZWxlY3RlZCBJRCBMaXN0IG9mIElucHV0ID4+PlwiLCBpbnB1dCk7XHJcbiAgICBcclxuICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICBpc3JlYWQ6IGZhbHNlLFxyXG4gICAgICBmZWF0dXJlOiBcImlzcmVhZFwiLFxyXG4gICAgICBpZExpc3Q6IGlucHV0XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5jb250ZW50U2VydmljZS5jcmVhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgfVxyXG5cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBiYXNlNjRUb0FycmF5QnVmZmVyKGJhc2U2NDogc3RyaW5nKSB7XHJcbiAgY29uc3QgYmluYXJ5U3RyaW5nID0gd2luZG93LmF0b2IoYmFzZTY0KTsgLy8gQ29tbWVudCB0aGlzIGlmIG5vdCB1c2luZyBiYXNlNjRcclxuICBjb25zdCBieXRlcyA9IG5ldyBVaW50OEFycmF5KGJpbmFyeVN0cmluZy5sZW5ndGgpO1xyXG4gIHJldHVybiBieXRlcy5tYXAoKGJ5dGUsIGkpID0+IGJpbmFyeVN0cmluZy5jaGFyQ29kZUF0KGkpKTtcclxufVxyXG5cclxuIl19