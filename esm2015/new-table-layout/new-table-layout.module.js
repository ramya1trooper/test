import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { NewTableLayoutComponent } from "../new-table-layout/new-table-layout.component";
import { MaterialModule } from "../material.module";
import { GridModule } from '@progress/kendo-angular-grid';
import { ConfirmDialogModule } from "../confirm-dialog/confirm-dialog.module";
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
export class NewTableLayoutModule {
}
NewTableLayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NewTableLayoutComponent],
                imports: [
                    RouterModule,
                    // Material
                    MaterialModule,
                    ConfirmDialogModule,
                    TranslateModule,
                    FuseSharedModule,
                    GridModule
                ],
                exports: [NewTableLayoutComponent],
                entryComponents: [ConfirmDialogComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmV3LXRhYmxlLWxheW91dC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibmV3LXRhYmxlLWxheW91dC9uZXctdGFibGUtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDekYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUM5RSxPQUFRLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQWlCckYsTUFBTSxPQUFPLG9CQUFvQjs7O1lBaEJoQyxRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsdUJBQXVCLENBQUM7Z0JBQ3ZDLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUVaLFdBQVc7b0JBQ1gsY0FBYztvQkFDZCxtQkFBbUI7b0JBQ25CLGVBQWU7b0JBRWYsZ0JBQWdCO29CQUNoQixVQUFVO2lCQUNYO2dCQUNELE9BQU8sRUFBRSxDQUFDLHVCQUF1QixDQUFDO2dCQUNqQyxlQUFlLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUMzQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9zaGFyZWQubW9kdWxlXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcblxyXG5pbXBvcnQgeyBOZXdUYWJsZUxheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi9uZXctdGFibGUtbGF5b3V0L25ldy10YWJsZS1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBHcmlkTW9kdWxlIH0gZnJvbSAnQHByb2dyZXNzL2tlbmRvLWFuZ3VsYXItZ3JpZCc7XHJcbmltcG9ydCB7IENvbmZpcm1EaWFsb2dNb2R1bGUgfSBmcm9tIFwiLi4vY29uZmlybS1kaWFsb2cvY29uZmlybS1kaWFsb2cubW9kdWxlXCI7XHJcbmltcG9ydCAgeyBDb25maXJtRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi4vY29uZmlybS1kaWFsb2cvY29uZmlybS1kaWFsb2cuY29tcG9uZW50JztcclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtOZXdUYWJsZUxheW91dENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgUm91dGVyTW9kdWxlLFxyXG5cclxuICAgIC8vIE1hdGVyaWFsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgIENvbmZpcm1EaWFsb2dNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUsXHJcblxyXG4gICAgRnVzZVNoYXJlZE1vZHVsZSxcclxuICAgIEdyaWRNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtOZXdUYWJsZUxheW91dENvbXBvbmVudF0sXHJcbiAgIGVudHJ5Q29tcG9uZW50czogW0NvbmZpcm1EaWFsb2dDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOZXdUYWJsZUxheW91dE1vZHVsZSB7fVxyXG4iXX0=