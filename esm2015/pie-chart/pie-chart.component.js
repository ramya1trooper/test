import { Component, ViewEncapsulation, Input, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
export class PieChartComponent {
    constructor(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.showLegend = true;
        this.explodeSlices = false;
        this.doughnut = false;
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    ngOnInit() {
        this.chartData = this.result;
    }
    onLegendLabelClick(event) { }
    select(event) { }
}
PieChartComponent.decorators = [
    { type: Component, args: [{
                selector: "pie-chart",
                template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\" style=\"align-self: center;\" class=\"h-400 p-10  mat-body-2\">\r\n    <div class=\"inner\" *ngIf=\"chartData.length > 0\">\r\n\r\n      <ngx-charts-pie-chart class=\"chart-container\" [view]=\"view\" [scheme]=\"scheme\" [results]=\"chartData\"\r\n        [animations]=\"animations\" [legend]=\"showLegend\" [legendTitle]=\"legendTitle\" [legendPosition]=\"legendPosition\"\r\n        [explodeSlices]=\"explodeSlices\" [doughnut]=\"doughnut\" [arcWidth]=\"arcWidth\"\r\n        (legendLabelClick)=\"onLegendLabelClick($event)\" [gradient]=\"gradient\" [tooltipText]=\"pieTooltipText\"\r\n        (select)=\"select($event)\">\r\n      </ngx-charts-pie-chart>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"chartData.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{label.nodata | translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                encapsulation: ViewEncapsulation.None,
                animations: fuseAnimations,
                styles: [".piechartdiv{display:block!important}.piechartdiv .legend-label-text{text-align:left}.inner{display:block;width:100%;text-align:center;margin-left:0!important}.inner img{width:auto;max-width:100%}.piechartdiv .chart-legend{padding-top:10px}.piechartdiv .legend-label{padding-bottom:7px}.piechartdiv .graphdiv{width:auto!important;padding-top:4px}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}"]
            }] }
];
/** @nocollapse */
PieChartComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
PieChartComponent.propDecorators = {
    result: [{ type: Input }],
    scheme: [{ type: Input }],
    view: [{ type: Input }],
    label: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGllLWNoYXJ0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJwaWUtY2hhcnQvcGllLWNoYXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULGlCQUFpQixFQUNqQixLQUFLLEVBQ0wsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVyRCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFTbEQsTUFBTSxPQUFPLGlCQUFpQjtJQWlCNUIsWUFDVSw2QkFBMkQsRUFDeEMsT0FBTztRQUQxQixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQ3hDLFlBQU8sR0FBUCxPQUFPLENBQUE7UUFsQnBDLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQWtCZixJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDL0IsQ0FBQztJQUNELGtCQUFrQixDQUFDLEtBQUssSUFBRyxDQUFDO0lBQzVCLE1BQU0sQ0FBQyxLQUFLLElBQUcsQ0FBQzs7O1lBbkNqQixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLFdBQVc7Z0JBQ3JCLHkxQ0FBeUM7Z0JBRXpDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxVQUFVLEVBQUUsY0FBYzs7YUFDM0I7Ozs7WUFUUSw0QkFBNEI7NENBNkJoQyxNQUFNLFNBQUMsU0FBUzs7O3FCQWZsQixLQUFLO3FCQUNMLEtBQUs7bUJBQ0wsS0FBSztvQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gIElucHV0LFxyXG4gIEluamVjdFxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IGZ1c2VBbmltYXRpb25zIH0gZnJvbSBcIi4uL0BmdXNlL2FuaW1hdGlvbnNcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcbmltcG9ydCB7IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UgfSBmcm9tIFwiLi4vQGZ1c2Uvc2VydmljZXMvdHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2VcIjtcclxuLy8gaW1wb3J0IHsgbG9jYWxlIGFzIGVuZ2xpc2ggfSBmcm9tIFwiLi4vaTE4bi9lblwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwicGllLWNoYXJ0XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9waWUtY2hhcnQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vcGllLWNoYXJ0LmNvbXBvbmVudC5zY3NzXCJdLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgYW5pbWF0aW9uczogZnVzZUFuaW1hdGlvbnNcclxufSlcclxuZXhwb3J0IGNsYXNzIFBpZUNoYXJ0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBzaG93TGVnZW5kID0gdHJ1ZTtcclxuICBleHBsb2RlU2xpY2VzID0gZmFsc2U7XHJcbiAgZG91Z2hudXQgPSBmYWxzZTtcclxuICBASW5wdXQoKSByZXN1bHQ6IGFueTtcclxuICBASW5wdXQoKSBzY2hlbWU6IGFueTtcclxuICBASW5wdXQoKSB2aWV3OiBhbnk7XHJcbiAgQElucHV0KCkgbGFiZWw6IGFueTtcclxuXHJcbiAgY2hhcnREYXRhOiBhbnk7XHJcbiAgdHJhbnNsYXRlOiBhbnk7XHJcbiAgYW5pbWF0aW9ucztcclxuICBsZWdlbmRUaXRsZTtcclxuICBhcmNXaWR0aDtcclxuICBncmFkaWVudDtcclxuICBsZWdlbmRQb3NpdGlvbjtcclxuICBwaWVUb29sdGlwVGV4dDtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2hcclxuICApIHtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5jaGFydERhdGEgPSB0aGlzLnJlc3VsdDtcclxuICB9XHJcbiAgb25MZWdlbmRMYWJlbENsaWNrKGV2ZW50KSB7fVxyXG4gIHNlbGVjdChldmVudCkge31cclxufVxyXG4iXX0=