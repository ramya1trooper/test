import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatInput } from '@angular/material';
import { MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ReportManagementService } from '../report-management.service';
import * as FileSaver from 'file-saver';
import { SetupAdministrationService } from '../setup-administration.service';
import { ReportHistoryComponent } from './report-management-history/report-management.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { SnackBarService } from './../../shared/snackbar.service';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { fuseAnimations } from '../../@fuse/animations';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
const moment = _moment;
import { MessageService } from '../../_services/message.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'; // Import NgxUiLoaderService
import { configMetaData } from './demoData';
export const MY_FORMATS = {
    parse: {
        dateInput: 'LL'
    },
    display: {
        dateInput: 'LL',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY'
    }
};
export class ConfigTrackerComponent {
    constructor(formBuilder, _matDialog, router, snackBarService, _reportManagementService, SetupAdministrationService, messageService, ngxService) {
        this.formBuilder = formBuilder;
        this._matDialog = _matDialog;
        this.router = router;
        this.snackBarService = snackBarService;
        this._reportManagementService = _reportManagementService;
        this.SetupAdministrationService = SetupAdministrationService;
        this.messageService = messageService;
        this.ngxService = ngxService;
        this.messages = [];
        this.submitted = false;
        this.typesss = [];
        this.final_selected_data = [];
        this.app_types = [];
        this.checkedControl = [];
        this.displayedColumns = ['select', 'object_name', 'object_code'];
        this.displayColumns = [
            'module_name',
            'name',
            'control_name',
            'control_type',
            'riskRank',
            'action'
        ];
        this.displayColumnsList = [
            'module_name',
            'name',
            'control_name',
            'control_type',
            'riskRank'
        ];
        this.displayObjects = [
            'select',
            'name',
            'control_name',
            'control_type',
            'riskRank'
        ];
        this.selectedColumns = [
            'application_name',
            'object_name',
            'object_code'
        ];
        this.expandElement = false;
        this.parentLimit = 10;
        this.dSource = [
            {
                module: 'GL',
                application_name: 'General Ledger',
                object_name: 'Accounting Calendar',
                object_code: ''
            }
            // {module: 'GL', application_name: 'General Ledger', object_name: 'Column set', control_name: '',control_type:''}
        ];
        this.profileForm = new FormGroup({
            selected: new FormControl()
        });
        this.dSrcReport = [
            {
                app_name: '',
                operation: '',
                object: '',
                change_date: '',
                change_by: '',
                control_name: '',
                existing_record: '',
                new_record: ''
            }
        ];
        this.controlTypes = [
            {
                value: 'SOX',
                name: 'SOX'
            },
            {
                value: 'HIPAA',
                name: 'HIPAA'
            },
            {
                value: 'FIDA',
                name: 'FIDA'
            }
        ];
        this.riskRanks = [
            {
                value: 'critical',
                name: 'Critical'
            },
            {
                value: 'high',
                name: 'High'
            },
            {
                value: 'medium',
                name: 'Medium'
            },
            {
                value: 'low',
                name: 'Low'
            }
        ];
        // displayedColumns: string[] = ['clientID', 'fusionUrl', 'userName','queryTypes', 'scheduleType','retainDays' ];
        this.displayedReport = [
            'app_name',
            'operation',
            'object',
            'change_date',
            'change_by',
            'control_name',
            'existing_record',
            'new_record'
        ];
        this.show_data = 'dashboard';
        this.columnsToDisplay = [];
        this.fromDate = new FormControl(moment());
        this.toDate = new FormControl(moment());
        this.moduleNames = configMetaData.moduleData;
        this.selectedTabHeader = [];
        this.historyData = {};
        this.selectedTableHeader = [];
        this.tableHeader = configMetaData.tableConfig;
        this.tableConfig = [];
        this.historyConfigData = [];
        this.step = 0;
        this.conftchartbymodule = [];
        this.ebs_m_chart = [];
        this.master_chart = [];
        this.week_by_user_chart = [];
        this.month_by_user_chart = [];
        // Bar
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.showYAxisLabel = true;
        this.xlabel = 'Modules';
        this.ylabel = 'Records';
        this.xlabelweek = 'Users';
        this.ylabelweek = 'Records';
        /*pie chart*/
        // options
        this.showPieLegend = false;
        this.legendPosition = 'right';
        this.view = [700, 400];
        this.weekview = [450, 350];
        this.pieview = [460, 400];
        this.legendTitle = 'Users';
        this.masterbyuserview = [600, 350];
        this.colorPieScheme = {
            domain: ['#28a3dd', '#8ec9e5', '#1c729a', '#186184', '#14516e', '#104158']
        };
        this.limit = 10;
        this.offset = 0;
        // pie
        this.showLabels = true;
        this.explodeSlices = false;
        this.configTrackerSetupDone = false;
        this.configQTSetupDone = false;
        this.doughnut = false;
        this.isChecked = false;
        // xAxisLabel=true;
        this.barPadding = 50;
        this.colorScheme = {
            domain: ['#28a3dd']
        };
        this.onSelectedModuleObjects = [];
        this.CreateForm = this.formBuilder.group({
            select: [''],
            module: [''],
            userName: [''],
            object_name: [''],
            control_type: [''],
            control_name: [''],
            riskRank: ['']
        });
    }
    ngOnInit() {
        this.formAction = 'add';
        this.types = [];
        this.ngxService.start();
        this.showedit();
        this.getallconfigs();
        // this.selectedTabHeader = [
        // 	{
        // 		'value':'transactiontype',
        // 		'name': 'AR_TransactionTypes'
        // 	},{
        // 		'value':'paymentterms',
        // 		'name': 'AR_PaymentTerms'
        // 	}
        // ];
        this.mode = 'view';
    }
    openHistory(element) {
        // var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
        console.log(element, '.......element');
        // let selectId =  element[selectedColumn[0].name]["name"]
        this.dialogRef = this._matDialog.open(ReportHistoryComponent, {
            disableClose: true,
            width: '75%',
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new',
                historyData: element.historyData,
                reportData: element,
                selectedHeader: this.selectedHeaderId,
                dateColumns: this.dateColumns
            }
        });
    }
    openNavigate() {
        let obj = {
            url: 'setup-administration/config-tracker-setup',
            id: "manageConfigTrackerSetup"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['setup-administration/config-tracker-setup']);
    }
    getallconfigs() {
        var userId = localStorage.getItem('userId');
        this.selected_modules = [];
        this.final_selected_data = [];
        this.getConfigTable = [];
        this.dSource.length = 0;
        this._reportManagementService.getAllConfigTracker(userId).subscribe((data) => {
            var data_new = data.body;
            this.dSource = data_new.response.configDetails;
            if (this.dSource.length != 0) {
                this.configTrackerSetupDone = true;
                this.selected_modules = this.dSource[0].module;
                this.getConfigTable = this.dSource[0].object_name;
                this.configTrackerID = this.dSource[0]._id;
                this.clientID = this.dSource[0].clientID;
                this.selected_data = new MatTableDataSource(this.getConfigTable);
                this.selected_data.paginator = this.spaginator;
                this.selected_datasource = this.getConfigTable;
                this.selected_data_length = this.getConfigTable.length;
                this.module_RM = this.dSource[0].module;
                console.log('Module-------', this.dSource[0].module);
                this.object_name_RM_byModule = _.groupBy(this.dSource[0].object_name, 'module_name');
                if (this.dSource[0].module.length != 0) {
                    this.formAction = 'edit';
                    this.configQTSetupDone = true;
                }
                else {
                    this.formAction = 'add';
                    this.show_data = 'config_tracker';
                }
                this.mode = 'view';
            }
            else {
                this.show_data = 'config_tracker';
            }
            this.Toggle('dashboard', null, null, null);
            this.ngxService.stop();
        }, (error) => {
            this.ngxService.stop();
            console.log('error:::' + error);
            this.snackBarService.warning('Oops Something went wrong!! Please try again after sometime.');
        });
    }
    showConfigSetup() {
        this.show_data = 'config_tracker';
    }
    loadTableColumn(selectedHeader, headerValues) {
        this.configTrackerHeader = [];
        var tempArray = [
            {
                name: 'History',
                value: 'history',
                isActive: true
            },
            {
                name: 'Status',
                value: 'status',
                isActive: true
            }
        ];
        if (headerValues) {
            _.forEach(headerValues, function (item) {
                var tempObj = {
                    name: item.uiAttrName,
                    value: item.dbAttrName,
                    isIdentifier: item.isIdentifier,
                    isActive: false
                };
                if (item.type) {
                    tempObj['type'] = item.type;
                }
                if (item.dbAttrName === 'START_DATE' ||
                    item.dbAttrName === 'CREATED_BY_USER_ID' ||
                    item.dbAttrName === 'CREATION_DATE' ||
                    item.dbAttrName === 'LAST_UPDATED_BY_USER' ||
                    item.dbAttrName === 'NAME') {
                    tempObj['isActive'] = true;
                }
                // 	tempObj['isActive'] = false;
                // }
                tempArray.push(tempObj);
            });
            this.tableConfig = tempArray;
        }
        for (let columns of this.tableConfig) {
            if (columns.isActive) {
                this.configTrackerHeader.push(columns.name);
                this.selectedTableHeader.push(columns.name);
            }
        }
    }
    updateTable(event) {
        let selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.name) >= 0) {
                item.isActive = true;
            }
            else {
                item.isActive = false;
            }
        });
        localStorage.removeItem('control');
        localStorage.setItem('control', JSON.stringify(this.tableConfig));
        this.loadTableColumn(this.selectedHeaderId, null);
    }
    onSelectModuleRM(event, isRedirect) {
        event = event && event.value ? event.value : event;
        var tempArray = [];
        let tempObj = this.object_name_RM_byModule;
        _.forEach(event, function (item) {
            if (tempObj[String(item)]) {
                if (tempArray && tempArray.length) {
                    tempArray = _.concat(tempArray, tempObj[String(item)]);
                }
                else {
                    tempArray = tempObj[String(item)];
                }
            }
        });
        this.select_modules_RM = event;
        this.objectName_RM = tempArray;
        this.statusFilter = [
            {
                value: 'changed',
                name: 'Changed'
            },
            {
                value: 'notChanged',
                name: 'Not Changed'
            },
            {
                value: 'created',
                name: 'Created'
            }
        ];
        if (isRedirect) {
            this.selectedTabHeader = tempArray;
            this.selectedStatus = this.statusFilter[0];
            // this.selectedTabHeader.push(tempArray[0])
            this.changeTab({ index: 0 });
        }
    }
    updateTab(event, fromTab) {
        console.log('updateTab >>>> ', event);
        if (event.value) {
            // this.selectobjheader = event.value;
            this.selectedHeaderId = this.selectedTabHeader[0].type;
            // this.selected_data.push(this.dSource[i].queryTypes[j].value);
        }
        else if (fromTab == 'dashboard') {
            var s_obj = [];
            this.selectedTabHeader = event;
            this.changeTab({ index: 0 });
            // this.reportmanagementObjectlist.forEach(elem =>{
            // 	let index = event.findIndex(
            // 		obj => obj.name === elem.name
            // 	);
            // 	if(index > -1){
            // 		elem.checked = true
            // 		s_obj.push(elem.name);
            // 	}
            // })
            // this.selectobjheader = s_obj;
            // console.log('this.selectobjheader',this.selectobjheader)
            // console.log("types...",this.types,"reportmanagementObjectlist---------",this.reportmanagementObjectlist)
        }
        this.getUserNameList('');
    }
    applyFilter() {
        // if(this.show_data == 'report_management')
        this.changeTab(null);
    }
    resetFilter() {
        this.select_modules_RM = [];
        this.selectedTabHeader = [];
        this.selectedUser = '';
        this.selectedStatus = '';
        this.selectedTableHeader = [];
        this.fromInput.value = '';
        this.toInput.value = '';
        this.configData = [];
        this.configTrackerHeader = [];
    }
    changeTab(event) {
        if (event) {
            event = event.index;
            this.selectedHeaderId = this.selectedTabHeader[event].type;
            this.selectedHeaderModulename = this.selectedTabHeader[event].module_name;
            this.selectedHeaderObjectName = this.selectedTabHeader[event].name;
        }
        else {
            // if(!this.selectedHeaderId){
            this.selectedHeaderId = this.selectedTabHeader[0].type;
            this.selectedHeaderModulename = this.selectedTabHeader[0].module_name;
            this.selectedHeaderObjectName = this.selectedTabHeader[0].name;
            // }
        }
        console.log(this.selectedHeaderId, 'SELECTED');
        console.log(this.selectedStatus, '.........STATUS');
        var headerOption = {
            type: this.selectedHeaderId
        };
        console.log(this.tableConfig, '----tableconfig');
        this._reportManagementService
            .getHeaderData(headerOption)
            .subscribe((headerDatas) => {
            var headerData;
            this.currentHeaderData = headerDatas.body.response;
            this.loadTableColumn(this.selectedHeaderId, this.currentHeaderData.params);
            var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
            selectedColumn = _.map(selectedColumn, 'name');
            console.log(selectedColumn, '......');
            this.dateColumns = _.filter(this.tableConfig, ['type', 'date']);
            console.log(this.dateColumns, '......Date');
            let dateColumns = this.dateColumns;
            this.uniqueID = selectedColumn;
            var options = {
                limit: this.limit,
                offset: this.offset,
                configTrackerId: this.configTrackerID,
                type: this.selectedHeaderId,
                uniqueID: this.uniqueID,
                selectedUserName: this.selectedUser ? this.selectedUser : '',
                selectedStatus: this.selectedStatus ? this.selectedStatus.value : '',
                fromDate: this.fromInput.value ? this.fromInput.value : '',
                toDate: this.toInput.value ? this.toInput.value : ''
            };
            this._reportManagementService
                .getRawData(options)
                .subscribe((data) => {
                // var demoConfig = demoData.lastModifiedData;
                if (data && data.body && data.body.response) {
                    this.configData = data.body.response.processDataResp
                        ? data.body.response.processDataResp
                        : [];
                    // this.historyData = data.body.response.historyData;
                    let processArray = data.body.response.processDataResp
                        ? data.body.response.processDataResp
                        : [];
                    _.forEach(processArray, function (item) {
                        console.log(item, '-----ITEM');
                        // let id = (item && item[String(selectedColumn[0].name)]) ? item[String(selectedColumn[0].name)]["name"] : '';
                        // let tempObj = data.body.response.historyData;
                        item['isChanged'] =
                            item && item.historyData && item.historyData.length
                                ? true
                                : false;
                        item['isAdded'] =
                            item && item.historyData && item.historyData.length
                                ? false
                                : item['isAdded'];
                        if (dateColumns && dateColumns.length) {
                            _.forEach(dateColumns, function (dateItem) {
                                let dateString = item[String(dateItem.name)]['name'];
                                var showDate = new Date(dateString);
                                var formatDate = moment(showDate).format('lll');
                                item[String(dateItem.name)]['name'] = formatDate;
                            });
                        }
                    });
                    this.length = data.body.response.total;
                    this.configData =
                        processArray && processArray.length ? processArray : [];
                    this.configData.paginator = this.reportpaginator;
                    console.log(this.configData, '.......ConfigDATAAAAA');
                }
            });
        });
    }
    changePage(event) {
        if (event.pageSize != this.limit) {
            this.limit = event.pageSize;
            this.offset = 0;
        }
        else {
            this.offset = event.pageIndex * this.limit;
        }
        this.changeTab(null);
    }
    setStep(rows) {
        // this.step = index;
        var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
        var queryParams = {
            // type : this.selectedHeaderId,
            // uniqueID : selectedColumn[0].name,
            selectId: rows[selectedColumn[0].name]['name']
        };
        this.getConfigHistory(queryParams);
    }
    getConfigHistory(options) {
        console.log(this.historyData, '....this.historyData');
        this.historyConfigData = this.historyData[options.selectId];
        // this._reportManagementService.getHistoryData(options).subscribe((dataHistory: any) => {
        // 	if(dataHistory && dataHistory.body && dataHistory.body.response){
        // 		this.historyConfigData = dataHistory.body.response;
        // 	}
        // });
    }
    showedit() {
        // alert('jbvc')
        this.mode = 'edit';
        this._reportManagementService.getAllModules().subscribe((data) => {
            this.modules = [];
            this.types = [];
            this.reportmanagementObjectlist = [];
            this.typesss = [];
            var res = data.body.response.Modules;
            this.modules = data.body.response.Modules;
            this.final_selected_data = this.getConfigTable;
            if (this.selected_modules.length != 0) {
                this.getAllObjectsNameList(function () { });
            }
        });
    }
    showHistory(type, rows) {
        console.log('type---------', type);
        if (type == 'true') {
            this.expandElement = true;
        }
        else if (type == 'false') {
            this.expandElement = false;
        }
        console.log('this.expandElement---------', this.expandElement);
        var selectedColumn = _.filter(this.tableConfig, ['isIdentifier', true]);
        var queryParams = {
            // type : this.selectedHeaderId,
            // uniqueID : selectedColumn[0].name,
            selectId: rows[selectedColumn[0].name]['name']
        };
        this.getConfigHistory(queryParams);
    }
    getAllObjectsNameList(callback) {
        var options = {
            module: this.selected_modules
        };
        this._reportManagementService.getObjects(options).subscribe((data) => {
            var res = data.body.response.Modules;
            res.forEach((item) => {
                var obj = {};
                obj['name'] = item.objectName[0].name;
                obj['value'] = item.objectName[0].name;
                obj['type'] = item.objectName[0].type;
                obj['module_name'] = item.objectName[0].module_name;
                let index = this.getConfigTable.findIndex((obj) => obj.name === item.objectName[0].name);
                if (index > -1) {
                    obj['checked'] = true;
                    obj['control_name'] = this.getConfigTable[index].control_name;
                    obj['control_type'] = this.getConfigTable[index].control_type;
                    obj['riskRank'] = this.getConfigTable[index].riskRank;
                }
                else {
                    obj['checked'] = false;
                    obj['control_name'] = '';
                    obj['control_type'] = '';
                    obj['riskRank'] = '';
                }
                this.types.push(obj);
                this.reportmanagementObjectlist.push(obj);
            });
            console.log('reportmanagementObjectlist----------->>...', this.reportmanagementObjectlist);
            this.object_list = new MatTableDataSource(this.types);
            this.object_list.paginator = this.paginator;
            this.object_list_length = this.types.length;
        });
        callback();
    }
    checkExits(selectedRow) {
        if (selectedRow == true) {
            return true;
        }
        else {
            return false;
        }
    }
    onSearchName(filterValue) {
        this.selected_data.filter = filterValue.trim().toLowerCase();
        this.selected_data.paginator = this.spaginator;
    }
    onSearchObjectName(filterValue) {
        this.object_list.filter = filterValue.trim().toLowerCase();
        this.object_list.paginator = this.paginator;
        this.object_list_length = this.types.length;
    }
    onSelectModule(event) {
        let keyByObjectNames = _.groupBy(this.getConfigTable, 'module_name');
        _.forEach(event.value, function (item) {
            if (this.onSelectedModuleObjects && this.onSelectedModuleObjects.length) {
                this.onSelectedModuleObjects = _.concat(this.onSelectedModuleObjects, keyByObjectNames[item]);
            }
            else {
                this.onSelectedModuleObjects = keyByObjectNames[item];
            }
        });
    }
    onChangeDatasourceId(id, type) {
        //this.ngxService.start();
        var modules = [];
        if (type == 'dashboard') {
            this.select_modules = id;
            modules = id;
        }
        else {
            modules = id.value;
        }
        this.types = [];
        this.selected_objects = [];
        var options = {
            module: modules
        };
        this._reportManagementService.getObjects(options).subscribe((data) => {
            var res = data.body.response.Modules;
            res.forEach((item) => {
                var obj = {};
                obj['name'] = item.objectName[0].name;
                obj['value'] = item.objectName[0].name;
                obj['type'] = item.objectName[0].type;
                obj['module_name'] = item.objectName[0].module_name;
                if (this.final_selected_data.length > 0) {
                    let index = this.final_selected_data.findIndex((obj) => obj.name === item.objectName[0].name);
                    if (index > -1) {
                        obj['checked'] = true;
                        obj['control_name'] = this.final_selected_data[index].control_name;
                        obj['control_type'] = this.final_selected_data[index].control_type;
                        obj['riskRank'] = this.final_selected_data[index].riskRank;
                    }
                    else {
                        obj['checked'] = false;
                        obj['control_name'] = '';
                        obj['control_type'] = '';
                        obj['riskRank'] = '';
                    }
                }
                else {
                    obj['checked'] = false;
                    obj['control_name'] = '';
                    obj['control_type'] = '';
                    obj['riskRank'] = '';
                }
                // if(obj["checked"] == true){
                // 	this.isChecked = true;
                // }
                // else if(obj["checked"] == false){
                // 	this.isChecked = false;
                // }
                this.types.push(obj);
                this.reportmanagementObjectlist.push(obj);
            });
            this.object_list = new MatTableDataSource(this.types);
            this.object_list.paginator = this.paginator;
            this.object_list_length = this.types.length;
            this.final_selected_data = _.filter(this.types, ['checked', true]);
            //this.ngxService.stop();
        });
    }
    onChangeType(element, event, type) {
        if (type == 'all') {
            if (event.checked == true) {
                this.isChecked = true;
                this.types.forEach((item) => {
                    item.checked = true;
                    var obj = {};
                    obj['name'] = item.name;
                    obj['value'] = item.value;
                    obj['type'] = item.type;
                    obj['module_name'] = item.module_name;
                    obj['control_name'] = item.control_name;
                    obj['control_type'] = item.control_type;
                    obj['riskRank'] = item.riskRank;
                    obj['checked'] = item.checked;
                    this.final_selected_data.push(obj);
                });
            }
            else if (event.checked == false) {
                this.isChecked = false;
                this.types.forEach((item) => {
                    item.checked = false;
                });
                this.final_selected_data = [];
            }
        }
        else if (type == 'single') {
            if (this.final_selected_data.findIndex((temp) => temp.name === element.name) < 0) {
                if (event.checked == true) {
                    this.final_selected_data.push(element);
                    let iindex = this.types.findIndex((objt) => objt.name === element.name);
                    this.types[iindex].checked = true;
                }
                else if (event.checked == false) {
                    let index = this.final_selected_data.findIndex((obj) => obj.name === element.name);
                    this.final_selected_data.splice(index, 1);
                    let iindex = this.types.findIndex((objt) => objt.name === element.name);
                    this.types[iindex].checked = false;
                }
            }
        }
    }
    ConfirmData() {
        console.log('ConfirmData----------->>>', this.final_selected_data);
        console.log('selectedModules----------->>>', this.selected_modules);
        this.final_selected_data.forEach((item, indexx) => {
            let index = this.selected_modules.findIndex((obj) => obj === item.module_name);
            if (index > -1) {
                console.log('Item-----------', item.module_name);
            }
            else {
                console.log('ItemELSE-----------', index);
                this.final_selected_data.splice(indexx, 1);
            }
        });
        console.log('ConfirmDataFinal----------->>>', this.final_selected_data);
        this.final_selected_table = new MatTableDataSource(this.final_selected_data);
        this.final_selected_table.paginator = this.fpaginator;
        this.final_selected_length = this.final_selected_data.length;
    }
    PreviousData() {
        this.object_list = new MatTableDataSource(this.types);
        this.object_list.paginator = this.paginator;
        this.object_list_length = this.types.length;
    }
    onChangeName(event, name) {
        let iindex = this.types.findIndex((obj) => obj.name === name);
        this.types[iindex].control_name = event;
        let i = this.final_selected_data.findIndex((objt) => objt.name === name);
        this.final_selected_data[i].control_name = event;
    }
    onChangeControlType(event, name) {
        let iindex = this.types.findIndex((obj) => obj.name === name);
        this.types[iindex].control_type = event;
        let i = this.final_selected_data.findIndex((objt) => objt.name === name);
        this.final_selected_data[i].control_type = event;
    }
    onChangeRisk(event, name) {
        let iindex = this.types.findIndex((obj) => obj.name === name);
        this.types[iindex].riskRank = event;
        let i = this.final_selected_data.findIndex((objt) => objt.name === name);
        this.final_selected_data[i].riskRank = event;
    }
    exportpdf() {
        //this.ngxService.start();
        this.loginUser = JSON.parse(localStorage.getItem('currentLoginUser'));
        console.log('this.loginUser.username >>>>> ', this.loginUser['username']);
        var queryParams = {
            type: this.selectedHeaderId,
            username: this.loginUser.username,
            module_name: this.selectedHeaderModulename,
            object_name: this.selectedHeaderObjectName,
            selectedUserName: this.selectedUser ? this.selectedUser : '',
            selectedStatus: this.selectedStatus ? this.selectedStatus.value : '',
            configTrackerId: this.configTrackerID,
            uniqueID: this.uniqueID,
            fromDate: this.fromInput.value ? this.fromInput.value : '',
            toDate: this.toInput.value ? this.toInput.value : ''
        };
        this._reportManagementService.getSchedulePDF(queryParams).subscribe((res) => {
            const blob = new Blob([res.body], { type: 'application/pdf' });
            FileSaver.saveAs(blob, 'ConfigTrackerReport.pdf');
            //this.ngxService.stop();
        }, (error) => {
            //this.ngxService.stop();
            console.log('error:::' + JSON.stringify(error));
        });
    }
    onSubmit(type) {
        if (type == 'formSubmit') {
            var s_module = [];
            var obj_name = [];
            s_module = this.CreateForm.controls['module'].value;
            obj_name = this.final_selected_data;
        }
        else if (type == 'formDelete') {
            var s_module = [];
            var obj_name = [];
            s_module = this.selected_modules;
            obj_name = this.selected_datasource;
        }
        this.submitted = true;
        var obj = {
            object_name: obj_name,
            module: s_module,
            userId: this.dSource[0].userId,
            _id: this.dSource[0]._id,
            clientID: this.dSource[0].clientID,
            fusionUrl: this.dSource[0].fusionUrl,
            userName: this.dSource[0].userName,
            password: this.dSource[0].password,
            scheduleType: this.dSource[0].scheduleType,
            scheduleTypeValue: this.dSource[0].scheduleTypeValue,
            retainDays: this.dSource[0].retainDays,
            __v: this.dSource[0].__v,
            queryTypes: _.map(obj_name, 'type'),
            deleted: false
        };
        this.SetupAdministrationService.updateConfigDetails(obj, this.dSource[0].userId).subscribe((res) => {
            if (res.status == 201) {
                this.snackBarService.add(res.body.meta.msg);
            }
            else {
                this.snackBarService.add(res.body.meta.msg);
            }
        }, (error) => {
            this.errorStatus = error.error.meta.status;
            if (this.errorStatus == '500' || this.errorStatus == '400') {
                this.errorMsg = error.error.meta.msg;
                this.snackBarService.warning(this.errorMsg);
                setTimeout(() => {
                    this.submitted = false;
                }, 3000);
            }
        });
        this.getallconfigs();
    }
    runData() {
        var userId = localStorage.getItem('userId');
        var queryParams = {
            clientID: this.clientID,
            userId: userId
        };
        this._reportManagementService
            .startDataPull(queryParams)
            .subscribe((data) => {
            this.snackBarService.add('Run/ Sync Data Started Successfully');
        });
    }
    deleteSelectedConfigsFinal(element) {
        let index = this.final_selected_data.findIndex((obj) => obj.name === element.name);
        this.final_selected_data.splice(index, 1);
        let iindex = this.types.findIndex((objt) => objt.name === element.name);
        this.types[iindex].checked = false;
        delete this.final_selected_table[element.name];
        this.final_selected_table = new MatTableDataSource(this.final_selected_data);
        this.object_list = new MatTableDataSource(this.types);
        this.final_selected_table.paginator = this.fpaginator;
        this.object_list.paginator = this.paginator;
        this.object_list_length = this.types.length;
        this.final_selected_length = this.final_selected_data.length;
        this.snackBarService.warning('Config Removed Successfully');
    }
    deleteSelectedConfigs(element) {
        let index = this.selected_datasource.findIndex((obj) => obj.name === element.name);
        this.selected_datasource.splice(index, 1);
        delete this.selected_datasource[element.name];
        this.selected_data = new MatTableDataSource(this.selected_datasource);
        this.selected_data.paginator = this.spaginator;
        this.onSubmit('formDelete');
        this.snackBarService.warning('Config Removed Successfully');
    }
    getUserNameList(tempvalues) {
        let objectLists = _.map(this.selectedTabHeader, 'type');
        var queryParams = {
            selectedObject: objectLists
        };
        this._reportManagementService
            .getUpdatedUserList(queryParams)
            .subscribe((data) => {
            if (data && data.body && data.body['response']) {
                this.userList = data.body['response'].length
                    ? data.body['response']
                    : [];
                if (tempvalues) {
                    this.selectedUser = tempvalues.name;
                }
            }
        });
    }
    Toggle(id, tempvalues, typeChart, chartNum) {
        if (this.configQTSetupDone && this.configTrackerSetupDone) {
            if (id == 'config_tracker') {
                this.show_data = 'config_tracker';
            }
            else if (id == 'dashboard') {
                this.show_data = 'dashboard';
                this.changesbymodule();
                this.weekbyuserChart();
                this.monthbyuserChart();
            }
            else {
                this.show_data = 'report_management';
                if (tempvalues && tempvalues != '') {
                    var moduleName = [];
                    if (typeChart === 'module') {
                        moduleName.push(tempvalues.name);
                    }
                    else {
                        moduleName = this.module_RM;
                    }
                    this.onChangeDatasourceId(moduleName, 'dashboard');
                    this.onSelectModuleRM(moduleName, 'redirect');
                    this.getAllObjectsNameList(function () {
                        console.log('GET');
                    });
                    if (typeChart === 'user') {
                        if (chartNum == 1) {
                            this.getUserNameList(tempvalues);
                        }
                        else if (chartNum == 2) {
                            let tempSplitName = tempvalues.name.split('(');
                            tempvalues.name = tempSplitName[0];
                            this.getUserNameList(tempvalues);
                        }
                    }
                }
                //console.log(this._reportManagementService.configTrackerDetails);
            }
        }
    }
    //Chart portions
    changesbymodule() {
        this.conftchartbymodule = [];
        let queryParams = {
            modules: this.selected_modules,
            configTrackerID: this.configTrackerID
        };
        this._reportManagementService
            .confTrackByModuleChart(queryParams)
            .subscribe((data) => {
            var result = _.chain(data.body.response)
                .groupBy('_id.moduleName')
                .toPairs()
                .map((objs, key) => {
                console.log(objs);
                return {
                    modulename: objs[0],
                    count: _.sumBy(objs[1], 'count')
                };
            })
                .value();
            console.log(result, '...R');
            result.forEach((element) => {
                const tempArray = {};
                tempArray.name = element.modulename;
                tempArray.value = element.count;
                this.conftchartbymodule.push(tempArray);
            });
            console.log(this.conftchartbymodule, '----conftchartbymodule');
            //console.log("Grouped", states);
            /*
            var res = data.response.userConflicts;
            res.forEach(element => {
                const tempArray: any = {};
                tempArray.name = element.username;
                tempArray.value = element.total;
    
                this.ebs_m_chart.push(tempArray);
            });
            */
        });
    }
    weekbyuserChart() {
        this.week_by_user_chart = [];
        let queryParams = {
            modules: this.selected_modules,
            configTrackerID: this.configTrackerID
        };
        this._reportManagementService
            .confTrackByUserByWeekChart(queryParams)
            .subscribe((data) => {
            //console.log("response",data.response);
            var result = _.chain(data.body.response)
                .groupBy('_id.user')
                .toPairs()
                .map((objs, key) => {
                //console.log(objs);
                return {
                    users: objs[0],
                    count: _.sumBy(objs[1], 'count')
                };
            })
                .value();
            //console.log(result);
            result.forEach((element) => {
                const tempArray = {};
                tempArray.name = element.users;
                tempArray.value = element.count;
                this.week_by_user_chart.push(tempArray);
            });
        });
    }
    monthbyuserChart() {
        this.month_by_user_chart = [];
        /*
        this.month_by_user_chart = [
            {
              "name": "SISMAIL",
              "value": 75
            },
            {
                "name": "Operation",
                "value": 25
            },
            {
                "name": "XYZ",
                "value": 15
            },
            {
                "name": "MNO",
                "value": 45
            }
          ];
        */
        let queryParams = {
            modules: this.selected_modules,
            configTrackerID: this.configTrackerID
        };
        this._reportManagementService
            .confTrackByUserByMonthChart(queryParams)
            .subscribe((data) => {
            var result = _.chain(data.body.response)
                .groupBy('_id.user')
                .toPairs()
                .map((objs, key) => {
                //console.log(objs);
                return {
                    users: objs[0],
                    count: _.sumBy(objs[1], 'count')
                };
            })
                .value();
            var sumOfCount = 0;
            result.forEach((element) => {
                sumOfCount = sumOfCount + element.count;
            });
            var totalusercount = data.body.response.length;
            //console.log("sum", sumOfCount);
            result.forEach((element) => {
                const tempArray = {};
                //console.log("count",element.count);
                //console.log("sum", sumOfCount);
                //console.log("total",totalusercount);
                // tempArray.value = ((element.count/sumOfCount)*totalusercount) ;
                tempArray.percentage =
                    parseFloat(((element.count * 100) / sumOfCount).toFixed(2)) + '%';
                tempArray.name = element.users + '(' + tempArray.percentage + ')';
                tempArray.value = element.count;
                this.month_by_user_chart.push(tempArray);
            });
        });
    }
    formatPercent(c) {
        console.log(c);
        return c.value + '%';
    }
    axisFormat(val) {
        if (val % 1 === 0) {
            return val.toLocaleString();
        }
        else {
            return '';
        }
    }
    onSelect(emitted, typeChart, chartNum) {
        console.log(emitted);
        //this.mname.valuevalue = this.modulename;
        this.userSelectedToggle = 'reportMgmtToggle';
        this.Toggle('report_management', emitted, typeChart, chartNum);
        this._reportManagementService.configTrackerDetails = emitted;
    }
    close() {
        this.types = [];
        this.showedit();
        this.getallconfigs();
        this.mode = 'view';
    }
}
ConfigTrackerComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-config-tracker',
                template: "<div class=\"page-layout blank p-12\" fusePerfectScrollbar>\r\n\t<mat-drawer-container class=\"example-container\" autosize fxFlex>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" style=\"background: white\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayout.sm=\"column\" fxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"m-0\">Config Tracker</h2>\r\n\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" style=\"font-size :initial\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t<span class=\"m-0\">Track the configurations from Cloud Application</span>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n\t\t\t\tstyle=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: flex-start;\">\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<mat-button-toggle-group name=\"fontStyle\" aria-label=\"Font Style\" value={{show_data}} class=\"mr-12\">\r\n\t\t\t\t\t\t<mat-button-toggle value=\"dashboard\" color=\"primary\" (click)=\"Toggle('dashboard','','', '')\">\r\n\t\t\t\t\t\t\tDashboard </mat-button-toggle>\r\n\t\t\t\t\t\t<mat-button-toggle value=\"config_tracker\" (click)=\"Toggle('config_tracker','','', '')\">\r\n\t\t\t\t\t\t\tConfiguration Tracker</mat-button-toggle>\r\n\t\t\t\t\t\t<mat-button-toggle value=\"report_management\" (click)=\"Toggle('report_management','','', '')\">\r\n\t\t\t\t\t\t\tReport\r\n\t\t\t\t\t\t\tManagement </mat-button-toggle>\r\n\r\n\t\t\t\t\t</mat-button-toggle-group>\r\n\t\t\t\t</div>\r\n\t\t\t\t<button *ngIf=\"show_data == 'report_management'\" style=\"padding: 0px 10px 0px 10px;\r\n\t\t\t\tmargin: 0px 4px 0px 0px;\" mat-raised-button class=\"mat-raised-button mat-warn common-btn\" (click)=\"runData()\">\r\n\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">refresh</mat-icon>Run / Sync Data\r\n\t\t\t\t</button>\r\n\t\t\t\t<button *ngIf=\"show_data == 'report_management'\" mat-raised-button\r\n\t\t\t\t\tclass=\"mat-raised-button mat-warn common-btn\" (click)=\"exportpdf()\">\r\n\t\t\t\t\t<span>Export</span>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\" style=\"padding: 10px 0px 0px 10px; background: white; margin: 1%\"\r\n\t\t\t*ngIf=\"show_data == 'report_management' \">\r\n\r\n\t\t\t<div fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\" >\r\n\t\t\t\t\t<mat-label>Select Module</mat-label>\r\n\t\t\t\t\t<mat-select multiple (selectionChange)=\"onSelectModuleRM($event, null)\"\r\n\t\t\t\t\t\t[(ngModel)]=\"select_modules_RM\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let module of module_RM; let i = index\" [value]=\"module\">{{module}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t</div>\r\n\t\t\t<div  fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" *ngIf=\"show_data == 'report_management'\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-label>Object Name</mat-label>\r\n\t\t\t\t\t<mat-select multiple [(ngModel)]=\"selectedTabHeader\" (selectionChange)=\"updateTab($event,'')\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let type of objectName_RM; let i = index\" [value]=\"type\">{{type.name}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t\t<div  fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-label>Select User</mat-label>\r\n\t\t\t\t\t<mat-select [(ngModel)]=\"selectedUser\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let user of userList; let i = index\" [value]=\"user\">{{user}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t\t<div  fxLayout=\"column\" fxFlex=\"50\" fxFlex.gt-sm=\"20\" fxLayoutAlign=\"center\"\r\n\t\t\t\tclass=\"custom-mat-form\" style=\"    padding: 10px;\r\n\t\t\t\tflex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tmax-width: 20%;\r\n\t\t\t\tplace-content: stretch center;\r\n\t\t\t\talign-items: stretch;\r\n\t\t\t\tflex: 1 1 100%;\">\r\n\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"25\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-label>Select Status</mat-label>\r\n\t\t\t\t\t<mat-select [(ngModel)]=\"selectedStatus\">\r\n\t\t\t\t\t\t<mat-option *ngFor=\"let status of statusFilter; let i = index\" [value]=\"status\">{{status.name}}\r\n\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t</mat-select>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"center\"\r\n\t\t\tstyle=\"padding: 10px 0px 0px 10px; background: white; margin: 1%\" *ngIf=\"show_data == 'report_management' \">\r\n\t\t\t<div fxFlex=\"80%\">\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"20%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t\t<input #fromInput matInput [matDatepicker]=\"dp\" placeholder=\"From Date\">\r\n\t\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n\t\t\t\t\t\t<mat-datepicker #dp></mat-datepicker>\r\n\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"20%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t\t<input #toInput matInput [matDatepicker]=\"dp1\" placeholder=\"To Date\">\r\n\t\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp1\"></mat-datepicker-toggle>\r\n\t\t\t\t\t\t<mat-datepicker #dp1></mat-datepicker>\r\n\t\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t</div>\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"10%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<button mat-raised-button class=\"mat-raised-button mat-warn common-btn \"\r\n\t\t\t\t\t\t[disabled]=\"!selectedTabHeader.length\" (click)=\"applyFilter()\">\r\n\t\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">search</mat-icon>Apply\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div style=\"padding: 10px;\" fxFlex=\"10%\" class=\"custom-mat-form\">\r\n\t\t\t\t\t<button mat-raised-button class=\"mat-raised-button mat-warn common-btn \" (click)=\"resetFilter()\">\r\n\t\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">refresh</mat-icon>Reset\r\n\t\t\t\t\t</button>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\t<div fxFlex=\"15%\">\r\n\t\t\t\t<div *ngIf=\"show_data == 'report_management'\" style=\"width: 140px;\">\r\n\t\t\t\t\t<div style=\"padding: 10px;\" class=\"custom-mat-form\"\r\n\t\t\t\t\t\t*ngIf=\"selectedTableHeader && selectedTableHeader.length && configData && configData.length\">\r\n\t\t\t\t\t\t<button mat-icon-button class=\"mat-warn\" matTooltip=\"Table Settings\" (click)=select.open()>\r\n\t\t\t\t\t\t\t<mat-icon style=\"font-size: 32px;\">settings</mat-icon>\r\n\t\t\t\t\t\t\t<mat-select #select multiple style=\"width: 100px; padding: 23%\"\r\n\t\t\t\t\t\t\t\t[(ngModel)]=\"selectedTableHeader\" (selectionChange)=\"updateTable($event)\">\r\n\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let columnSetting of tableConfig\" [value]=\"columnSetting['name'] \">\r\n\t\t\t\t\t\t\t\t\t{{columnSetting.name}}</mat-option>\r\n\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<!-- <div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between -webkit-left\" style=\"padding: 10px 0px 0px 10px; width: 50%;\"\r\n\t\t\t*ngIf=\"show_data == 'dashboard' \" >\r\n\t\t\t<div style=\"padding: 10px;\" fxLayout=\"column\" fxFlex=\"10\" fxFlex.gt-sm=\"15\" fxLayoutAlign=\"-webkit-left\"\r\n\t\t\t\tclass=\"custom-mat-form\">\r\n\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t<input matInput [matDatepicker]=\"dp\" placeholder=\"From Date\">\r\n\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp\"></mat-datepicker-toggle>\r\n\t\t\t\t\t<mat-datepicker #dp></mat-datepicker>\r\n\t\t\t\t</mat-form-field>\r\n\t\t\t</div>\r\n\t\t\t<div style=\"padding: 10px;\" fxLayout=\"column\" fxFlex=\"10\" fxFlex.gt-sm=\"15\" fxLayoutAlign=\"-webkit-left\"\r\n\t\t\t\tclass=\"custom-mat-form\" >\r\n\t\t\t\t<mat-form-field>\r\n\t\t\t\t\t<input matInput [matDatepicker]=\"dp1\" placeholder=\"To Date\">\r\n\t\t\t\t\t<mat-datepicker-toggle matSuffix [for]=\"dp1\"></mat-datepicker-toggle>\r\n\t\t\t\t\t<mat-datepicker #dp1></mat-datepicker>\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t</div>\r\n\t\t\t<div style=\"padding: 10px;\" fxLayout=\"column\" fxFlex=\"10\" fxFlex.gt-sm=\"35\" fxLayoutAlign=\"-webkit-left\"\r\n\t\t\t\tclass=\"custom-mat-form\" >\r\n\t\t\t\t<button mat-raised-button class=\"mat-raised-button mat-warn common-btn \" (click)=\"applyFilter()\">\r\n\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">search</mat-icon>Apply\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</div> -->\r\n\t\t<div class=\"toolbar p-6 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\tfont-size: initial;    margin: 30px;padding: 1%;\" *ngIf=\"!configTrackerSetupDone\">\r\n\t\t\tTo proceed,\r\n\t\t\t<button mat-button color=\"accent\" (click)=\"openNavigate()\">\r\n\t\t\t\tClick Here</button> to setup the Config Tracker\r\n\t\t</div>\r\n\t\t<div class=\"toolbar p-6 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\tfont-size: initial;    margin: 30px;padding: 1%;\" *ngIf=\"configTrackerSetupDone && !configQTSetupDone\">\r\n\t\t\tClick Add Button to New Module and Object Names.\r\n\t\t</div>\r\n\t\t<div class=\"content p-24\" fusePerfectScrollbar *ngIf=\"show_data == 'config_tracker' \">\r\n\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white\">\r\n\r\n\t\t\t\t<div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t<div></div>\r\n\t\t\t\t\t<div fxFlex class=\"h3\" style=\"padding: 10px;\">Configuration Details &nbsp;&nbsp;\r\n\t\t\t\t\t\t<button mat-raised-button color='accent' (click)=\"showedit()\"\r\n\t\t\t\t\t\t\t*ngIf=\"formAction == 'add' && configTrackerSetupDone\"\r\n\t\t\t\t\t\t\tstyle=\"float:right;font-size: 15px;margin-top: -1%;\">\r\n\t\t\t\t\t\t\t<mat-icon style=\"font-size: 20px;padding: 2px;\">add</mat-icon>Add\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t<button mat-raised-button color='accent' (click)=\"showedit()\" *ngIf=\"formAction == 'edit'\"\r\n\t\t\t\t\t\t\tstyle=\"float:right;font-size: 15px;margin-top: -1%;\">\r\n\t\t\t\t\t\t\t<mat-icon style=\"font-size: 20px;padding: 2px;\">edit</mat-icon>Edit\r\n\t\t\t\t\t\t</button>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t\t<mat-horizontal-stepper class=\"mat-elevation-z4\" [linear]=\"true\" *ngIf=\"mode == 'edit'\" style=\"\r\n\t\t\t\tpadding-left: 3%;padding-right: 3%;\">\r\n\t\t\t\t\t<mat-step [stepControl]=\"selectControl\" style=\"margin-left: 2%;\">\r\n\t\t\t\t\t\t<div mat-dialog-content class=\"p-14 pb-0 m-0\" fusePerfectScrollbar>\r\n\t\t\t\t\t\t\t<form style=\"padding-bottom: 22px;\" fxLayout=\"column\" [formGroup]=\"CreateForm\">\r\n\t\t\t\t\t\t\t\t<ng-template matStepLabel>Set Configuration</ng-template>\r\n\t\t\t\t\t\t\t\t<div layout=\"row\" md-content layout-padding>\r\n\t\t\t\t\t\t\t\t\t<mat-label style=\"width:25%;margin-top:1%\">Module Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" fxFlex=\"30\" class=\"mr-8\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-label>Select Module Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t<mat-select required placeholder=\"Select Module Name\" formControlName=\"module\"\r\n\t\t\t\t\t\t\t\t\t\t\t[(ngModel)]=\"selected_modules\" multiple\r\n\t\t\t\t\t\t\t\t\t\t\t(selectionChange)=\"onChangeDatasourceId($event,'')\"\r\n\t\t\t\t\t\t\t\t\t\t\t[(value)]=\"selected_modules\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let md of modules\" [value]=\"md._id\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{md._id}}\r\n\t\t\t\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t<p *ngIf=\"submitted && CreateForm.get('module').invalid\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-error class=\"error_margin\">\r\n\t\t\t\t\t\t\t\t\t\t\tModule Name is required!\r\n\t\t\t\t\t\t\t\t\t\t</mat-error>\r\n\t\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t<table mat-table [dataSource]=\"object_list\" class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"select\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"width:10%;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tSelect\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"padding-top: 9px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox (change)=\"onChangeType(null,$event,'all')\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"margin-top: 27%;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"width:2%;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-checkbox (change)=\"onChangeType(element,$event,'single')\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t[checked]=element.checked>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-checkbox>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"padding-top: 25px;width:35%;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tObject Name\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"padding-top: 9px;max-width: 65%;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Object Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<input matInput\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(input)=\"onSearchObjectName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:28%;font-size: 14px;    padding: 0px 0px 56px 0;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Name\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 10px 0px 0px 0px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Control Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input matInput formControlName=\"control_name\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t[(value)]=\"element.control_name\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t(change)=\"onChangeName($event.target.value,element.name)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_type\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:15%;font-size: 14px;    padding: 0px 0px 56px 0;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Type\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 10px 0px 0px 0px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Select Control Type</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-select [(value)]=element.control_type\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Select Control Type\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let ct of controlTypes; let i = index\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue=\"{{ct.name}}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"onChangeControlType(ct.name,element.name)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{ct.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"riskRank\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef\r\n\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:15%;font-size: 14px;    padding: 0px 0px 56px 0;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tRisk Ranking\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 10px 0px 0px 0px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-label>Select Risk Rank</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-select [(value)]=element.riskRank\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tplaceholder=\"Select Risk Rank\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let risk of riskRanks; let i = index\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tvalue=\"{{risk.name}}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"onChangeRisk(risk.name,element.name)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{risk.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-option>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-select>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayObjects\"></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayObjects;\"></tr>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t<mat-paginator #paginator [pageSizeOptions]=\"[5]\" [pageIndex]=0\r\n\t\t\t\t\t\t\t\t\t\t[length]=\"object_list_length\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\t\t\t\t\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"m-0\"></h2>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t<button class=\"mr-8\" mat-raised-button matStepperNext type=\"button\"\r\n\t\t\t\t\t\t\t\t\t\t\tcolor=\"accent\" (click)=\"ConfirmData()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-icon>navigate_next</mat-icon>Next\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" *ngIf=\"formAction == 'view'\" fxlayout=\"row\"\r\n\t\t\t\t\t\t\t\t\t\tfxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</form>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-step>\r\n\t\t\t\t\t<mat-step style=\"margin-right: 2%;\">\r\n\t\t\t\t\t\t<form>\r\n\t\t\t\t\t\t\t<ng-template matStepLabel>Confirmation</ng-template>\r\n\t\t\t\t\t\t\t<div class=\"content p-12\" fusePerfectScrollbar>\r\n\t\t\t\t\t\t\t\t<div class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t<table mat-table [dataSource]=\"final_selected_table\" class=\"mat-elevation-z8\">\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Name </th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.control_name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_type\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tControl Type</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.control_type}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"riskRank\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tRisk Ranking</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.riskRank}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"module_name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tModule Name </th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.module_name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"name\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\"> Object Name\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t{{element.name}}\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"action\">\r\n\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"font-size: 14px;\"> Action\r\n\t\t\t\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"deleteSelectedConfigsFinal(element)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<button mat-icon-button color='warn'>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon>delete</mat-icon>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayColumns\"></tr>\r\n\t\t\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayColumns;\"></tr>\r\n\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t<!-- <mat-paginator #fpaginator [pageSizeOptions]=\"[5]\" [length]=\"final_selected_length\" [pageIndex]=0\r\n\t\t\t\t\t\t\t\t showFirstLastButtons></mat-paginator>\t -->\r\n\t\t\t\t\t\t\t\t\t<mat-paginator #fpaginator [pageSizeOptions]=\"[5]\" [length]=\"final_selected_length\"\r\n\t\t\t\t\t\t\t\t\t\t[pageIndex]=0 [pageSize]=\"parentLimit\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\t\t\t\t\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t\t\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"m-0\"></h2>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<button class=\"mr-8\" mat-raised-button matStepperPrevious type=\"button\"\r\n\t\t\t\t\t\t\t\t\t\t\tcolor=\"accent\" (click)=\"PreviousData()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-icon>navigate_before</mat-icon>Previous\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t<button mat-raised-button class=\"mr-4\" color=\"warn\" style=\"float:right\"\r\n\t\t\t\t\t\t\t\t\t\t\t(click)=\"onSubmit('formSubmit')\">\r\n\t\t\t\t\t\t\t\t\t\t\tSubmit\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t<div class=\"toolbar\" *ngIf=\"formAction == 'view'\" fxlayout=\"row\"\r\n\t\t\t\t\t\t\t\t\t\tfxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t<button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\"\r\n\t\t\t\t\t\t\t\t\t\t\tng-reflect-type=\"button\" style=\"background-color: grey !important;\r\n\t\t\t\t\t\tcolor: white !important;border-radius: 25px !important;margin-right: 8px !important;\" (click)=\"close()\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"mat-button-wrapper\">Cancel</span>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-ripple mat-ripple\" matripple=\"\"\r\n\t\t\t\t\t\t\t\t\t\t\t\tng-reflect-centered=\"false\" ng-reflect-disabled=\"false\"></div>\r\n\t\t\t\t\t\t\t\t\t\t\t<div class=\"mat-button-focus-overlay\"></div>\r\n\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</form>\r\n\t\t\t\t\t</mat-step>\r\n\t\t\t\t</mat-horizontal-stepper>\r\n\t\t\t\t<div mat-dialog-content class=\"p-24 pb-0 m-0\" fusePerfectScrollbar *ngIf=\"mode == 'view'\">\r\n\t\t\t\t\t<div class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t<table mat-table [dataSource]=\"selected_data\" class=\"mat-elevation-z8\">\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_name\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tControl Name\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Control Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.control_name}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"control_type\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tControl Type\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Control Type</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.control_type}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"riskRank\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tRisk Ranking\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Risk Ranking</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.riskRank}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"module_name\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tModule Name\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Module Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.module_name}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<ng-container matColumnDef=\"name\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-top: 15px;font-size: 14px;\"> Object\r\n\t\t\t\t\t\t\t\t\tName\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\" style=\"margin-top: 5%;\">\r\n\t\t\t\t\t\t\t\t\t\t<mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n\t\t\t\t\t\t\t\t\t\t\t<mat-label>Search By Object Name</mat-label>\r\n\t\t\t\t\t\t\t\t\t\t\t<input matInput (keyup)=\"onSearchName($event.target.value)\">\r\n\t\t\t\t\t\t\t\t\t\t</mat-form-field>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\">\r\n\t\t\t\t\t\t\t\t\t{{element.name}}\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t<!-- <ng-container matColumnDef=\"action\">\r\n\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding-bottom: 5%;font-size: 14px;\">\r\n\t\t\t\t\t\t\t\t\tAction\r\n\t\t\t\t\t\t\t\t\t<div class=\"mr-8\">\r\n\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</th>\r\n\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" (click)=\"deleteSelectedConfigs(element)\">\r\n\t\t\t\t\t\t\t\t\t<button mat-icon-button color='warn'>\r\n\t\t\t\t\t\t\t\t\t\t<mat-icon>delete</mat-icon>\r\n\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t</ng-container> -->\r\n\r\n\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"displayColumnsList\"></tr>\r\n\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: displayColumnsList;\"></tr>\r\n\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t<mat-paginator #spaginator [pageSizeOptions]=\"[10]\" [length]=\"selected_data_length\"\r\n\t\t\t\t\t\t\t[pageIndex]=0 [pageSize]=\"parentLimit\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div class=\"content p-6\" fusePerfectScrollbar\r\n\t\t\t*ngIf=\"show_data == 'report_management' && configTrackerSetupDone && configQTSetupDone\">\r\n\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white\"\r\n\t\t\t\t*ngIf=\"selectedTabHeader && selectedTabHeader.length\">\r\n\t\t\t\t<mat-tab-group (selectedTabChange)=\"changeTab($event)\" *ngIf=\"expandElement == false\"\r\n\t\t\t\t\tstyle=\"overflow: hidden;\">\r\n\t\t\t\t\t<mat-tab *ngFor=\"let type of selectedTabHeader\" label=\"{{type.name}}\">\r\n\t\t\t\t\t\t<div *ngIf=\"configData && configData.length\">\r\n\t\t\t\t\t\t\t<table mat-table [dataSource]=\"configData\" multiTemplateDataRows class=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"{{column}}\" *ngFor=\"let column of configTrackerHeader\">\r\n\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef style=\"padding: 0px 0px 0px 20px;\">\r\n\t\t\t\t\t\t\t\t\t\t{{column | titlecase}} </th>\r\n\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" style=\"padding: 15px\">\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'History'\">\r\n\t\t\t\t\t\t\t\t\t\t\t<button mat-button style=\"padding-bottom: 10%\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t*ngIf=\"element !== expandedElement\" [disabled]=\"!element.isChanged\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"openHistory(element)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <mat-icon *ngIf= \"element.isChanged\" style=\"font-size: 35px;    margin: 0px 3px;color: #D35400\">history</mat-icon> -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <span  > -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<img *ngIf=\"element.isChanged\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tsrc=\"../../assets/icons/gif-icons/source-1.gif\" alt=\"HTML5 Icon\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:30px;height:30px;\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- </span> -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <mat-icon *ngIf= \"!element.isChanged\" style=\"font-size: 35px;    margin: 0px 3px;color:#a43dcc\">history</mat-icon> -->\r\n\t\t\t\t\t\t\t\t\t\t\t\t<img *ngIf=\"!element.isChanged\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tsrc=\"../../assets/icons/gif-icons/source-2.png\" alt=\"HTML5 Icon\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"width:30px;height:30px;\">\r\n\r\n\t\t\t\t\t\t\t\t\t\t\t</button>\r\n\t\t\t\t\t\t\t\t\t\t\t<!-- <button mat-button color=\"accent\" *ngIf=\"element === expandedElement\" (click)=\"showHistory('false')\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon style=\"font-size: 21px;    margin: 0px 3px;\">expand_less</mat-icon>\r\n\t\t\t\t\t\t\t\t\t\t\t</button> -->\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t<!-- <span *ngIf=\"column === 'Status' && historyConfigData && historyConfigData.length\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tChanged\r\n\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' && !historyConfigData\">\r\n\t\t\t\t\t\t\t\t\t\t\t\tNot Changed\r\n\t\t\t\t\t\t\t\t\t\t</span> -->\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' &&  element.isChanged && !element.isAdded\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"label bg-success\"> Changed </span>\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' && !element.isChanged && !element.isAdded\"\r\n\t\t\t\t\t\t\t\t\t\t\tclass=\"label bg-running\" style=\"background: #467696\"> Not Changed </span>\r\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column === 'Status' &&  element.isAdded\" class=\"label bg-success\"\r\n\t\t\t\t\t\t\t\t\t\t\tstyle=\"background: #d32c79\"> Created </span>\r\n\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t*ngIf=\"element[column] && element[column].name\">{{element[column].name}}</span>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\r\n\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"expandedDetail\">\r\n\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element\" [attr.colspan]=\"configTrackerHeader.length\">\r\n\t\t\t\t\t\t\t\t\t\t<div class=\"example-element-detail\"\r\n\t\t\t\t\t\t\t\t\t\t\t[@detailExpand]=\"element === expandedElement ? 'expanded' : 'collapsed'\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span class=\"example-element-description-attribution\" style=\"width: 100%;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\tpadding: 10px 10px 10px 50px; \">\r\n\t\t\t\t\t\t\t\t\t\t\t\t<!-- <mat-drawer-container class=\"example-container\" autosize fxFlex [hasBackdrop]=\"false\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"content p-12\" fusePerfectScrollbar style=\"background: #e2e2ed\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"m-0\" style=\"font-size: 16px;\">History of Changes</span>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<table mat-table [dataSource]=\"historyConfigData\" multiTemplateDataRows\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"mat-elevation-z8\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ng-container matColumnDef=\"{{column1}}\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t*ngFor=\"let column1 of configTrackerHeader\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef > \r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"column1 !== 'History' && column1 !== 'Status'\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t{{column1 | uppercase}}\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</span> </th>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element1\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t[ngClass]=\"(element1[column1] && element1[column1].isChanged)?'class3':'class1'\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tstyle=\"padding: 20px; text-align: -webkit-left\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t*ngIf=\"element1[column1] && element1[column1].name\">{{element1[column1].name}}</span>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"configTrackerHeader\" ></tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let element; columns: configTrackerHeader;\"\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tclass=\"example-element-row\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</mat-drawer-container> -->\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</td>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"configTrackerHeader\"></tr>\r\n\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let element; columns: configTrackerHeader;\"\r\n\t\t\t\t\t\t\t\t\tclass=\"example-element-row\"\r\n\t\t\t\t\t\t\t\t\t[class.example-expanded-row]=\"expandedElement === element\"\r\n\t\t\t\t\t\t\t\t\t(click)=\"setStep(element)\">\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t\t<tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\"\r\n\t\t\t\t\t\t\t\t\tclass=\"example-detail-row\">\r\n\t\t\t\t\t\t\t\t</tr>\r\n\t\t\t\t\t\t\t</table>\r\n\t\t\t\t\t\t\t<mat-paginator #reportpaginator [pageSizeOptions]=\"[10]\" [length]=\"length\" [pageIndex]=0\r\n\t\t\t\t\t\t\t\t[pageSize]=\"limit\" (page)=\"changePage($event)\" showFirstLastButtons></mat-paginator>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div *ngIf=\"configData && configData.length === 0\">\r\n\t\t\t\t\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\t\t\t\t\t\t\tfont-size: large;\">\r\n\t\t\t\t\t\t\t\tNo Reports Found!\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</mat-tab>\r\n\t\t\t\t</mat-tab-group>\r\n\t\t\t\t<!-- <mat-drawer-container class=\"example-container\" autosize fxFlex [hasBackdrop]=\"false\"\r\n\t\t\t\t*ngIf=\"expandElement == true\"> -->\r\n\t\t\t\t<!-- <div class=\"content p-12\" fusePerfectScrollbar style=\"background: #e2e2ed\"> -->\r\n\t\t\t\t<!-- <div class=\"header-top ctrl-create header p-24 mat-elevation-z8 bg-white\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\"> -->\r\n\t\t\t\t<!-- <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t\t\t<span class=\"m-0\" style=\"font-size: 16px;\">History of Changes</span>\r\n\t\t\t\t\t</div> -->\r\n\t\t\t\t<!-- </div> -->\r\n\t\t\t\t<!-- </div> -->\r\n\t\t\t\t<!-- <table mat-table [dataSource]=\"historyConfigData\" multiTemplateDataRows class=\"mat-elevation-z8\">\r\n\t\t\t\t\t<ng-container matColumnDef=\"{{column1}}\" *ngFor=\"let column1 of configTrackerHeader\">\r\n\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef>\r\n\t\t\t\t\t\t\t<span *ngIf=\"column1 !== 'History' && column1 !== 'Status'\">\r\n\t\t\t\t\t\t\t\t{{column1 | uppercase}}\r\n\t\t\t\t\t\t\t</span> </th>\r\n\t\t\t\t\t\t<td mat-cell *matCellDef=\"let element1\"\r\n\t\t\t\t\t\t\t[ngClass]=\"(element1[column1] && element1[column1].isChanged)?'class3':'class1'\"\r\n\t\t\t\t\t\t\tstyle=\"padding: 20px; text-align: -webkit-left\">\r\n\t\t\t\t\t\t\t<span\r\n\t\t\t\t\t\t\t\t*ngIf=\"element1[column1] && element1[column1].name\">{{element1[column1].name}}</span>\r\n\t\t\t\t\t\t</td>\r\n\t\t\t\t\t</ng-container>\r\n\r\n\r\n\t\t\t\t\t<tr mat-header-row *matHeaderRowDef=\"configTrackerHeader\"></tr>\r\n\t\t\t\t\t<tr mat-row *matRowDef=\"let element; columns: configTrackerHeader;\" class=\"example-element-row\">\r\n\t\t\t\t\t</tr>\r\n\t\t\t\t</table> -->\r\n\t\t\t\t<!-- </mat-drawer-container> -->\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n\t\t\tfont-style: italic;\r\n\t\t\tfont-size: large;\" *ngIf=\"selectedTabHeader && !selectedTabHeader.length\">\r\n\t\t\t\tSelect Module and Object Name\r\n\t\t\t</div>\r\n\t\t</div>\r\n\t\t<div *ngIf=\"show_data == 'dashboard' && configTrackerSetupDone && configQTSetupDone\" style=\"padding: 1%;\">\r\n\t\t\t<div style=\" width: 100%;\r\n\t\t\t\tflex-direction: row;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\">\r\n\t\t\t\t<fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" style=\"flex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tflex: 1 1 100%;\r\n\t\t\t\tmax-width: 33.3%;\">\r\n\t\t\t\t\t<div class=\"fuse-widget-front\">\r\n\t\t\t\t\t\t<div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" style=\"background: lightgrey;\"\r\n\t\t\t\t\t\t\tfxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t\t\t<div></div>\r\n\t\t\t\t\t\t\t<div fxFlex class=\"py-16 h3\">Changes Happened in Last 1 week by Modules</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"33\">\r\n\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"conftchartbymodule && conftchartbymodule.length > 0\">\r\n\t\t\t\t\t\t\t\t<ngx-charts-bar-vertical [view]=\"weekview\" [scheme]=\"colorScheme\"\r\n\t\t\t\t\t\t\t\t\t[results]=\"conftchartbymodule\" [gradient]=\"gradient\" [yAxis]=\"showYAxis\"\r\n\t\t\t\t\t\t\t\t\t[xAxis]=\"showXAxis\" [yAxisTickFormatting]=\"axisFormat\"\r\n\t\t\t\t\t\t\t\t\t[showXAxisLabel]=\"showXAxisLabel\" [showYAxisLabel]=\"showYAxisLabel\"\r\n\t\t\t\t\t\t\t\t\t[showGridLines]=\"true\" (select)=\"onSelect($event, 'module', 0)\"\r\n\t\t\t\t\t\t\t\t\t[xAxisLabel]=\"xlabel\" [yAxisLabel]=\"ylabel\">\r\n\t\t\t\t\t\t\t\t</ngx-charts-bar-vertical>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"conftchartbymodule.length == 0\">\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t<span class=\"mat-caption\">There is No Chart Data Found</span>\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</fuse-widget>\r\n\t\t\t\t<fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" style=\"flex-direction: column;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tflex: 1 1 100%;\r\n\t\t\t\tmax-width: 33.3%;\">\r\n\t\t\t\t\t<div class=\"fuse-widget-front\">\r\n\t\t\t\t\t\t<div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" style=\"background: lightgrey;\"\r\n\t\t\t\t\t\t\tfxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t\t\t<div></div>\r\n\t\t\t\t\t\t\t<div fxFlex class=\"py-16 h3\">Changes Happened in Last 1 Week by Users</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"33\">\r\n\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"week_by_user_chart.length > 0\">\r\n\t\t\t\t\t\t\t\t<ngx-charts-bar-vertical [view]=\"weekview\" [scheme]=\"colorScheme\"\r\n\t\t\t\t\t\t\t\t\t[results]=\"week_by_user_chart\" [gradient]=\"gradient\" [yAxis]=\"showYAxis\"\r\n\t\t\t\t\t\t\t\t\t[xAxis]=\"showXAxis\" [showXAxisLabel]=\"showXAxisLabel\"\r\n\t\t\t\t\t\t\t\t\t[yAxisTickFormatting]=\"axisFormat\" [showYAxisLabel]=\"showYAxisLabel\"\r\n\t\t\t\t\t\t\t\t\t[barPadding]=\"barPadding\" [showGridLines]=\"true\"\r\n\t\t\t\t\t\t\t\t\t(select)=\"onSelect($event, 'user', 1)\" [xAxisLabel]=\"xlabelweek\"\r\n\t\t\t\t\t\t\t\t\t[yAxisLabel]=\"ylabel\">\r\n\t\t\t\t\t\t\t\t</ngx-charts-bar-vertical>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"week_by_user_chart.length == 0\">\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t<span class=\"mat-caption\">There is No Chart Data Found</span>\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</fuse-widget>\r\n\r\n\t\t\t\t<fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" fxLayout=\"column\" class=\"widget\" style=\"flex-direction: row;\r\n\t\t\t\tbox-sizing: border-box;\r\n\t\t\t\tdisplay: flex;\r\n\t\t\t\tflex: 1 1 100%;\r\n\t\t\t\tmax-width: 34.3%;\">\r\n\t\t\t\t\t<div class=\"fuse-widget-front\">\r\n\t\t\t\t\t\t<div class=\"px-32 border-bottom\" fxLayout=\"row wrap\" style=\"background: lightgrey;\"\r\n\t\t\t\t\t\t\tfxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t\t\t<div></div>\r\n\t\t\t\t\t\t\t<div fxFlex class=\"py-16 h3\">Changes Happened in Last 1 month by Users</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"33\">\r\n\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"month_by_user_chart.length > 0\">\r\n\t\t\t\t\t\t\t\t<ngx-charts-pie-chart [view]=\"pieview\" [scheme]=\"colorPieScheme\"\r\n\t\t\t\t\t\t\t\t\t[results]=\"month_by_user_chart\" [legend]=\"showPieLegend\"\r\n\t\t\t\t\t\t\t\t\t[legendPosition]=\"legendPosition\" [legendTitle]=\"legendTitle\" [labels]=\"showLabels\"\r\n\t\t\t\t\t\t\t\t\t[doughnut]=\"doughnut\" [gradient]=\"gradient\" (select)=\"onSelect($event, 'user', 2)\">\r\n\t\t\t\t\t\t\t\t</ngx-charts-pie-chart>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t<div class=\"inner\" *ngIf=\"month_by_user_chart.length == 0\">\r\n\t\t\t\t\t\t\t\t<p>\r\n\t\t\t\t\t\t\t\t\t<span class=\"mat-caption\">There is No Chart Data Found</span>\r\n\t\t\t\t\t\t\t\t</p>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t</fuse-widget>\r\n\t\t\t</div>\r\n\t\t</div>\r\n\r\n\t</mat-drawer-container>\r\n</div>",
                animations: [
                    trigger('detailExpand', [
                        state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
                        state('expanded', style({ height: '*' })),
                        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
                    ]),
                    fuseAnimations
                ],
                providers: [
                    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
                    // application's root module. We provide it at the component level here, due to limitations of
                    // our example generation script.
                    {
                        provide: DateAdapter,
                        useClass: MomentDateAdapter,
                        deps: [MAT_DATE_LOCALE]
                    },
                    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }
                ],
                styles: [".chart-legend{display:inline-block!important;padding:0!important;width:auto!important;text-align:center!important}.col-4{flex-basis:33.333%}.parentBox{background:#fff;border-radius:10px}.child-border-bottom{border-bottom:1px solid rgba(0,0,0,.12)}.class1{background:#fff}.class2{background:#f0e149}.class3{background:#c7c2ba}table{width:100%}tr.example-detail-row{height:0}tr.example-element-row:not(.example-expanded-row):hover{background:#f5f5f5}tr.example-element-row:not(.example-expanded-row):active{background:#efefef}.example-element-row td{border-bottom-width:0}.example-element-detail{overflow:hidden;display:-webkit-box;display:flex}.example-element-diagram{min-width:80px;border:2px solid #000;padding:8px;font-weight:lighter;margin:8px 0;height:104px}.example-element-symbol{font-weight:700;font-size:40px;line-height:normal}.example-element-description{padding:16px}.example-element-description-attribution{opacity:.8}div.mat-horizontal-stepper-header-container{margin-right:2%!important;margin-left:2%!important}.inner{display:block;width:100%;text-align:center;margin-top:19px;min-height:372px}.inner img{width:auto;max-width:100%}ngx-charts-legend.chart-legend{width:auto;clear:both;display:block;margin:0 auto}ngx-charts-legend.chart-legend>div{display:block;width:auto!important}ngx-charts-legend.chart-legend .legend-wrap{min-width:200pxf;text-align:initial;margin:0 auto;float:none;padding-right:33px}ngx-charts-legend.chart-legend .legend-title{display:none}ngx-charts-legend.chart-legend .legend-labels{display:inline;text-align:center;margin:0 auto;background:0 0;float:none;padding:0;white-space:inherit}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important}.drawer_style{padding:3px;-webkit-transform:none;transform:none;visibility:visible;width:27%;height:100%;overflow-x:hidden}body.theme-default .mat-button-toggle-checked{background-color:blue-grey!important}.fuse-widget-front{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;position:relative;overflow:hidden;visibility:visible;width:100%;opacity:1;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(0);transform:rotateY(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}"]
            }] }
];
/** @nocollapse */
ConfigTrackerComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: MatDialog },
    { type: Router },
    { type: SnackBarService },
    { type: ReportManagementService },
    { type: SetupAdministrationService },
    { type: MessageService },
    { type: NgxUiLoaderService }
];
ConfigTrackerComponent.propDecorators = {
    paginator: [{ type: ViewChild, args: ['paginator',] }],
    spaginator: [{ type: ViewChild, args: ['spaginator',] }],
    fpaginator: [{ type: ViewChild, args: ['fpaginator',] }],
    reportpaginator: [{ type: ViewChild, args: ['reportpaginator',] }],
    fromInput: [{ type: ViewChild, args: ['fromInput', {
                    read: MatInput
                },] }],
    toInput: [{ type: ViewChild, args: ['toInput', {
                    read: MatInput
                },] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLXRyYWNrZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImF2bS9jb25maWctdHJhY2tlci9jb25maWctdHJhY2tlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0EsT0FBTyxFQUFFLFNBQVMsRUFBVSxTQUFTLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxZQUFZLEVBQUUsa0JBQWtCLEVBQUUsUUFBUSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDL0UsT0FBTyxFQUNMLFNBQVMsRUFJVixNQUFNLG1CQUFtQixDQUFDO0FBQzNCLE9BQU8sRUFDTCxXQUFXLEVBQ1gsU0FBUyxFQUVULFdBQVcsRUFDWixNQUFNLGdCQUFnQixDQUFDO0FBQ3hCLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQ3ZFLE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBRWpHLE9BQU8sRUFDTCxPQUFPLEVBQ1AsS0FBSyxFQUNMLEtBQUssRUFDTCxVQUFVLEVBQ1YsT0FBTyxFQUNSLE1BQU0scUJBQXFCLENBQUM7QUFDN0IsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFeEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckUsT0FBTyxFQUNMLFdBQVcsRUFDWCxnQkFBZ0IsRUFDaEIsZUFBZSxFQUNoQixNQUFNLHdCQUF3QixDQUFDO0FBQ2hDLE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDO0FBQ2xDLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUV2QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFFakUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sZUFBZSxDQUFDLENBQUMsNEJBQTRCO0FBQ2hGLE9BQU8sRUFBQyxjQUFjLEVBQUMsTUFBTSxZQUFZLENBQUM7QUFDMUMsTUFBTSxDQUFDLE1BQU0sVUFBVSxHQUFHO0lBQ3hCLEtBQUssRUFBRTtRQUNMLFNBQVMsRUFBRSxJQUFJO0tBQ2hCO0lBQ0QsT0FBTyxFQUFFO1FBQ1AsU0FBUyxFQUFFLElBQUk7UUFDZixjQUFjLEVBQUUsVUFBVTtRQUMxQixhQUFhLEVBQUUsSUFBSTtRQUNuQixrQkFBa0IsRUFBRSxXQUFXO0tBQ2hDO0NBQ0YsQ0FBQztBQTJDRixNQUFNLE9BQU8sc0JBQXNCO0lBdVBqQyxZQUNVLFdBQXdCLEVBQ3hCLFVBQXFCLEVBQ3JCLE1BQWMsRUFDZCxlQUFnQyxFQUNoQyx3QkFBaUQsRUFDakQsMEJBQXNELEVBRXRELGNBQThCLEVBQzlCLFVBQThCO1FBUjlCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLGVBQVUsR0FBVixVQUFVLENBQVc7UUFDckIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQXlCO1FBQ2pELCtCQUEwQixHQUExQiwwQkFBMEIsQ0FBNEI7UUFFdEQsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBL1B4QyxhQUFRLEdBQVUsRUFBRSxDQUFDO1FBR3JCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFJbEIsWUFBTyxHQUFHLEVBQUUsQ0FBQztRQUliLHdCQUFtQixHQUFHLEVBQUUsQ0FBQztRQUt6QixjQUFTLEdBQUcsRUFBRSxDQUFDO1FBWWYsbUJBQWMsR0FBYSxFQUFFLENBQUM7UUFDOUIscUJBQWdCLEdBQWEsQ0FBQyxRQUFRLEVBQUUsYUFBYSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1FBQ3RFLG1CQUFjLEdBQWE7WUFDekIsYUFBYTtZQUNiLE1BQU07WUFDTixjQUFjO1lBQ2QsY0FBYztZQUNkLFVBQVU7WUFDVixRQUFRO1NBQ1QsQ0FBQztRQUNGLHVCQUFrQixHQUFhO1lBQzdCLGFBQWE7WUFDYixNQUFNO1lBQ04sY0FBYztZQUNkLGNBQWM7WUFDZCxVQUFVO1NBQ1gsQ0FBQztRQUNGLG1CQUFjLEdBQWE7WUFDekIsUUFBUTtZQUNSLE1BQU07WUFDTixjQUFjO1lBQ2QsY0FBYztZQUNkLFVBQVU7U0FDWCxDQUFDO1FBQ0Ysb0JBQWUsR0FBYTtZQUMxQixrQkFBa0I7WUFDbEIsYUFBYTtZQUNiLGFBQWE7U0FDZCxDQUFDO1FBS0Ysa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFFL0IsZ0JBQVcsR0FBUyxFQUFFLENBQUM7UUFxQnZCLFlBQU8sR0FBYztZQUNuQjtnQkFDRSxNQUFNLEVBQUUsSUFBSTtnQkFDWixnQkFBZ0IsRUFBRSxnQkFBZ0I7Z0JBQ2xDLFdBQVcsRUFBRSxxQkFBcUI7Z0JBQ2xDLFdBQVcsRUFBRSxFQUFFO2FBQ2hCO1lBQ0Qsa0hBQWtIO1NBQ25ILENBQUM7UUFFRixnQkFBVyxHQUFHLElBQUksU0FBUyxDQUFDO1lBQzFCLFFBQVEsRUFBRSxJQUFJLFdBQVcsRUFBRTtTQUM1QixDQUFDLENBQUM7UUFLSCxlQUFVLEdBQUc7WUFDWDtnQkFDRSxRQUFRLEVBQUUsRUFBRTtnQkFDWixTQUFTLEVBQUUsRUFBRTtnQkFDYixNQUFNLEVBQUUsRUFBRTtnQkFDVixXQUFXLEVBQUUsRUFBRTtnQkFDZixTQUFTLEVBQUUsRUFBRTtnQkFDYixZQUFZLEVBQUUsRUFBRTtnQkFDaEIsZUFBZSxFQUFFLEVBQUU7Z0JBQ25CLFVBQVUsRUFBRSxFQUFFO2FBQ2Y7U0FDRixDQUFDO1FBRUYsaUJBQVksR0FBRztZQUNiO2dCQUNFLEtBQUssRUFBRSxLQUFLO2dCQUNaLElBQUksRUFBRSxLQUFLO2FBQ1o7WUFDRDtnQkFDRSxLQUFLLEVBQUUsT0FBTztnQkFDZCxJQUFJLEVBQUUsT0FBTzthQUNkO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLE1BQU07Z0JBQ2IsSUFBSSxFQUFFLE1BQU07YUFDYjtTQUNGLENBQUM7UUFDRixjQUFTLEdBQUc7WUFDVjtnQkFDRSxLQUFLLEVBQUUsVUFBVTtnQkFDakIsSUFBSSxFQUFFLFVBQVU7YUFDakI7WUFDRDtnQkFDRSxLQUFLLEVBQUUsTUFBTTtnQkFDYixJQUFJLEVBQUUsTUFBTTthQUNiO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFFBQVE7Z0JBQ2YsSUFBSSxFQUFFLFFBQVE7YUFDZjtZQUNEO2dCQUNFLEtBQUssRUFBRSxLQUFLO2dCQUNaLElBQUksRUFBRSxLQUFLO2FBQ1o7U0FDRixDQUFDO1FBUUYsaUhBQWlIO1FBQ2pILG9CQUFlLEdBQWE7WUFDMUIsVUFBVTtZQUNWLFdBQVc7WUFDWCxRQUFRO1lBQ1IsYUFBYTtZQUNiLFdBQVc7WUFDWCxjQUFjO1lBQ2QsaUJBQWlCO1lBQ2pCLFlBQVk7U0FDYixDQUFDO1FBRUYsY0FBUyxHQUFXLFdBQVcsQ0FBQztRQUNoQyxxQkFBZ0IsR0FBYSxFQUFFLENBQUM7UUFFaEMsYUFBUSxHQUFHLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDckMsV0FBTSxHQUFHLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7UUFDbkMsZ0JBQVcsR0FBUSxjQUFjLENBQUMsVUFBVSxDQUFDO1FBRTdDLHNCQUFpQixHQUFRLEVBQUUsQ0FBQztRQUM1QixnQkFBVyxHQUFRLEVBQUUsQ0FBQztRQUV0Qix3QkFBbUIsR0FBUSxFQUFFLENBQUM7UUFDOUIsZ0JBQVcsR0FBRyxjQUFjLENBQUMsV0FBVyxDQUFDO1FBQ3pDLGdCQUFXLEdBQVEsRUFBRSxDQUFDO1FBQ3RCLHNCQUFpQixHQUFRLEVBQUUsQ0FBQztRQUU1QixTQUFJLEdBQUcsQ0FBQyxDQUFDO1FBT1QsdUJBQWtCLEdBQWEsRUFBRSxDQUFDO1FBR2xDLGdCQUFXLEdBQVEsRUFBRSxDQUFDO1FBQ3RCLGlCQUFZLEdBQVEsRUFBRSxDQUFDO1FBQ3ZCLHVCQUFrQixHQUFRLEVBQUUsQ0FBQztRQUM3Qix3QkFBbUIsR0FBUSxFQUFFLENBQUM7UUFDOUIsTUFBTTtRQUNOLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixhQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ2pCLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFFdEIsV0FBTSxHQUFHLFNBQVMsQ0FBQztRQUNuQixXQUFNLEdBQUcsU0FBUyxDQUFDO1FBQ25CLGVBQVUsR0FBRyxPQUFPLENBQUM7UUFDckIsZUFBVSxHQUFHLFNBQVMsQ0FBQztRQUN2QixhQUFhO1FBQ2IsVUFBVTtRQUNWLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLG1CQUFjLEdBQUcsT0FBTyxDQUFDO1FBQ3pCLFNBQUksR0FBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN6QixhQUFRLEdBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7UUFDN0IsWUFBTyxHQUFVLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQzVCLGdCQUFXLEdBQUcsT0FBTyxDQUFDO1FBQ3RCLHFCQUFnQixHQUFVLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLG1CQUFjLEdBQUc7WUFDZixNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQztTQUMzRSxDQUFDO1FBQ0YsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUNuQixXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBRW5CLE1BQU07UUFDTixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLGtCQUFhLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLDJCQUFzQixHQUFHLEtBQUssQ0FBQztRQUMvQixzQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDMUIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLG1CQUFtQjtRQUNuQixlQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLGdCQUFXLEdBQUc7WUFDWixNQUFNLEVBQUUsQ0FBQyxTQUFTLENBQUM7U0FDcEIsQ0FBQztRQUtGLDRCQUF1QixHQUFRLEVBQUUsQ0FBQztRQW9CaEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUN2QyxNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDWixNQUFNLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDZCxXQUFXLEVBQUUsQ0FBQyxFQUFFLENBQUM7WUFDakIsWUFBWSxFQUFFLENBQUMsRUFBRSxDQUFDO1lBQ2xCLFlBQVksRUFBRSxDQUFDLEVBQUUsQ0FBQztZQUNsQixRQUFRLEVBQUUsQ0FBQyxFQUFFLENBQUM7U0FDZixDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQiw2QkFBNkI7UUFDN0IsS0FBSztRQUNMLCtCQUErQjtRQUMvQixrQ0FBa0M7UUFDbEMsT0FBTztRQUNQLDRCQUE0QjtRQUM1Qiw4QkFBOEI7UUFDOUIsS0FBSztRQUNMLEtBQUs7UUFDTCxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztJQUNyQixDQUFDO0lBQ0QsV0FBVyxDQUFDLE9BQU87UUFDakIsMkVBQTJFO1FBQzNFLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDdkMsMERBQTBEO1FBRTFELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDNUQsWUFBWSxFQUFFLElBQUk7WUFDbEIsS0FBSyxFQUFFLEtBQUs7WUFDWixVQUFVLEVBQUUscUJBQXFCO1lBQ2pDLElBQUksRUFBRTtnQkFDSixNQUFNLEVBQUUsS0FBSztnQkFDYixXQUFXLEVBQUUsT0FBTyxDQUFDLFdBQVc7Z0JBQ2hDLFVBQVUsRUFBRSxPQUFPO2dCQUNuQixjQUFjLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtnQkFDckMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXO2FBQzlCO1NBQ0YsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELFlBQVk7UUFDVixJQUFJLEdBQUcsR0FBRztZQUNSLEdBQUcsRUFBRywyQ0FBMkM7WUFDakQsRUFBRSxFQUFHLDBCQUEwQjtTQUNoQyxDQUFBO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDLENBQUM7SUFDdEUsQ0FBQztJQUNELGFBQWE7UUFDWCxJQUFJLE1BQU0sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsY0FBYyxHQUFHLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDakUsQ0FBQyxJQUFTLEVBQUUsRUFBRTtZQUNaLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQztZQUMvQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDNUIsSUFBSSxDQUFDLHNCQUFzQixHQUFHLElBQUksQ0FBQztnQkFDbkMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO2dCQUNsRCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDO2dCQUN6QyxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNqRSxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO2dCQUV2RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO2dCQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsdUJBQXVCLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQzNCLGFBQWEsQ0FDZCxDQUFDO2dCQUVGLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7aUJBQy9CO3FCQUFNO29CQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO2lCQUNuQztnQkFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQzthQUNwQjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxHQUFHLGdCQUFnQixDQUFDO2FBQ25DO1lBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMzQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pCLENBQUMsRUFDRCxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ1IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsOERBQThELENBQy9ELENBQUM7UUFDSixDQUFDLENBQ0YsQ0FBQztJQUNKLENBQUM7SUFDRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLFNBQVMsR0FBRyxnQkFBZ0IsQ0FBQztJQUNwQyxDQUFDO0lBRUQsZUFBZSxDQUFDLGNBQWMsRUFBRSxZQUFZO1FBQzFDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7UUFDOUIsSUFBSSxTQUFTLEdBQUc7WUFDZDtnQkFDRSxJQUFJLEVBQUUsU0FBUztnQkFDZixLQUFLLEVBQUUsU0FBUztnQkFDaEIsUUFBUSxFQUFFLElBQUk7YUFDZjtZQUNEO2dCQUNFLElBQUksRUFBRSxRQUFRO2dCQUNkLEtBQUssRUFBRSxRQUFRO2dCQUNmLFFBQVEsRUFBRSxJQUFJO2FBQ2Y7U0FDRixDQUFDO1FBQ0YsSUFBSSxZQUFZLEVBQUU7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJO2dCQUNwQyxJQUFJLE9BQU8sR0FBRztvQkFDWixJQUFJLEVBQUUsSUFBSSxDQUFDLFVBQVU7b0JBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVTtvQkFDdEIsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZO29CQUMvQixRQUFRLEVBQUUsS0FBSztpQkFDaEIsQ0FBQztnQkFDRixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7b0JBQ2IsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7aUJBQzdCO2dCQUNELElBQ0UsSUFBSSxDQUFDLFVBQVUsS0FBSyxZQUFZO29CQUNoQyxJQUFJLENBQUMsVUFBVSxLQUFLLG9CQUFvQjtvQkFDeEMsSUFBSSxDQUFDLFVBQVUsS0FBSyxlQUFlO29CQUNuQyxJQUFJLENBQUMsVUFBVSxLQUFLLHNCQUFzQjtvQkFDMUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLEVBQzFCO29CQUNBLE9BQU8sQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7aUJBQzVCO2dCQUNELGdDQUFnQztnQkFDaEMsSUFBSTtnQkFDSixTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzFCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUM7U0FDOUI7UUFDRCxLQUFLLElBQUksT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDcEMsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO2dCQUNwQixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDN0M7U0FDRjtJQUNILENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztRQUNmLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUk7WUFDckMsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxZQUFZLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLFlBQVksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDbEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELGdCQUFnQixDQUFDLEtBQUssRUFBRSxVQUFVO1FBQ2hDLEtBQUssR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ25ELElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLE9BQU8sR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7UUFDM0MsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsVUFBVSxJQUFJO1lBQzdCLElBQUksT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFO2dCQUN6QixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxFQUFFO29CQUNqQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hEO3FCQUFNO29CQUNMLFNBQVMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ25DO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxTQUFTLENBQUM7UUFDL0IsSUFBSSxDQUFDLFlBQVksR0FBRztZQUNsQjtnQkFDRSxLQUFLLEVBQUUsU0FBUztnQkFDaEIsSUFBSSxFQUFFLFNBQVM7YUFDaEI7WUFDRDtnQkFDRSxLQUFLLEVBQUUsWUFBWTtnQkFDbkIsSUFBSSxFQUFFLGFBQWE7YUFDcEI7WUFDRDtnQkFDRSxLQUFLLEVBQUUsU0FBUztnQkFDaEIsSUFBSSxFQUFFLFNBQVM7YUFDaEI7U0FDRixDQUFDO1FBQ0YsSUFBSSxVQUFVLEVBQUU7WUFDZCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDO1lBQ25DLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMzQyw0Q0FBNEM7WUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQzlCO0lBQ0gsQ0FBQztJQUVELFNBQVMsQ0FBQyxLQUFLLEVBQUUsT0FBTztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3RDLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNmLHNDQUFzQztZQUN0QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUN2RCxnRUFBZ0U7U0FDakU7YUFBTSxJQUFJLE9BQU8sSUFBSSxXQUFXLEVBQUU7WUFDakMsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQ2YsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEtBQUssQ0FBQztZQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0IsbURBQW1EO1lBQ25ELGdDQUFnQztZQUNoQyxrQ0FBa0M7WUFDbEMsTUFBTTtZQUNOLG1CQUFtQjtZQUNuQix3QkFBd0I7WUFDeEIsMkJBQTJCO1lBQzNCLEtBQUs7WUFFTCxLQUFLO1lBQ0wsZ0NBQWdDO1lBQ2hDLDJEQUEyRDtZQUMzRCwyR0FBMkc7U0FDNUc7UUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFRCxXQUFXO1FBQ1QsNENBQTRDO1FBQzVDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkIsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxFQUFFLENBQUM7SUFDaEMsQ0FBQztJQUVELFNBQVMsQ0FBQyxLQUFLO1FBQ2IsSUFBSSxLQUFLLEVBQUU7WUFDVCxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNwQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQztZQUMzRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUMxRSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQztTQUNwRTthQUFNO1lBQ0wsOEJBQThCO1lBQzlCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQ3ZELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQ3RFLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1lBQy9ELElBQUk7U0FDTDtRQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1FBRXBELElBQUksWUFBWSxHQUFHO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsZ0JBQWdCO1NBQzVCLENBQUM7UUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsd0JBQXdCO2FBQzFCLGFBQWEsQ0FBQyxZQUFZLENBQUM7YUFDM0IsU0FBUyxDQUFDLENBQUMsV0FBZ0IsRUFBRSxFQUFFO1lBQzlCLElBQUksVUFBVSxDQUFDO1lBRWYsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ25ELElBQUksQ0FBQyxlQUFlLENBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FDOUIsQ0FBQztZQUNGLElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLGNBQWMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxRQUFRLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2hFLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUMsQ0FBQztZQUM1QyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ25DLElBQUksQ0FBQyxRQUFRLEdBQUcsY0FBYyxDQUFDO1lBQy9CLElBQUksT0FBTyxHQUFHO2dCQUNaLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztnQkFDakIsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2dCQUNuQixlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7Z0JBQ3JDLElBQUksRUFBRSxJQUFJLENBQUMsZ0JBQWdCO2dCQUMzQixRQUFRLEVBQUUsSUFBSSxDQUFDLFFBQVE7Z0JBQ3ZCLGdCQUFnQixFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLEVBQUU7Z0JBQzVELGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDcEUsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtnQkFDMUQsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTthQUNyRCxDQUFDO1lBQ0YsSUFBSSxDQUFDLHdCQUF3QjtpQkFDMUIsVUFBVSxDQUFDLE9BQU8sQ0FBQztpQkFDbkIsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7Z0JBQ3ZCLDhDQUE4QztnQkFDOUMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDM0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlO3dCQUNsRCxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZTt3QkFDcEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDUCxxREFBcUQ7b0JBQ3JELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWU7d0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlO3dCQUNwQyxDQUFDLENBQUMsRUFBRSxDQUFDO29CQUVQLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLFVBQVUsSUFBSTt3QkFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7d0JBQy9CLCtHQUErRzt3QkFDL0csZ0RBQWdEO3dCQUNoRCxJQUFJLENBQUMsV0FBVyxDQUFDOzRCQUNmLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTtnQ0FDakQsQ0FBQyxDQUFDLElBQUk7Z0NBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQzt3QkFDWixJQUFJLENBQUMsU0FBUyxDQUFDOzRCQUNiLElBQUksSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTTtnQ0FDakQsQ0FBQyxDQUFDLEtBQUs7Z0NBQ1AsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDdEIsSUFBSSxXQUFXLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTs0QkFDckMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxRQUFRO2dDQUN2QyxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUNyRCxJQUFJLFFBQVEsR0FBRyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQ0FDcEMsSUFBSSxVQUFVLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztnQ0FDaEQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxVQUFVLENBQUM7NEJBQ25ELENBQUMsQ0FBQyxDQUFDO3lCQUNKO29CQUNILENBQUMsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29CQUN2QyxJQUFJLENBQUMsVUFBVTt3QkFDYixZQUFZLElBQUksWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7b0JBQzFELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7b0JBQ2pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2lCQUN2RDtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBR0QsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNoQyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDNUIsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7U0FDakI7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1NBQzVDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2QixDQUFDO0lBQ0QsT0FBTyxDQUFDLElBQUk7UUFDVixxQkFBcUI7UUFFckIsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxXQUFXLEdBQUc7WUFDaEIsZ0NBQWdDO1lBQ2hDLHFDQUFxQztZQUNyQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDL0MsQ0FBQztRQUNGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQsZ0JBQWdCLENBQUMsT0FBTztRQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsc0JBQXNCLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDNUQsMEZBQTBGO1FBQzFGLHFFQUFxRTtRQUNyRSx3REFBd0Q7UUFDeEQsS0FBSztRQUNMLE1BQU07SUFDUixDQUFDO0lBRUQsUUFBUTtRQUNOLGdCQUFnQjtRQUNoQixJQUFJLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUVuQixJQUFJLENBQUMsd0JBQXdCLENBQUMsYUFBYSxFQUFFLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDcEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDaEIsSUFBSSxDQUFDLDBCQUEwQixHQUFHLEVBQUUsQ0FBQztZQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDckMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDMUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDL0MsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWEsQ0FBQyxDQUFDLENBQUM7YUFDNUM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxXQUFXLENBQUMsSUFBSSxFQUFFLElBQUk7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDbkMsSUFBSSxJQUFJLElBQUksTUFBTSxFQUFFO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1NBQzNCO2FBQU0sSUFBSSxJQUFJLElBQUksT0FBTyxFQUFFO1lBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2QkFBNkIsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFL0QsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDeEUsSUFBSSxXQUFXLEdBQUc7WUFDaEIsZ0NBQWdDO1lBQ2hDLHFDQUFxQztZQUNyQyxRQUFRLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUM7U0FDL0MsQ0FBQztRQUNGLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUNyQyxDQUFDO0lBRUQscUJBQXFCLENBQUMsUUFBUTtRQUM1QixJQUFJLE9BQU8sR0FBRztZQUNaLE1BQU0sRUFBRSxJQUFJLENBQUMsZ0JBQWdCO1NBQzlCLENBQUM7UUFDRixJQUFJLENBQUMsd0JBQXdCLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQVMsRUFBRSxFQUFFO1lBQ3hFLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztZQUNyQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ25CLElBQUksR0FBRyxHQUFHLEVBQUUsQ0FBQztnQkFDYixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RDLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdkMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN0QyxHQUFHLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7Z0JBQ3BELElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUN2QyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FDOUMsQ0FBQztnQkFDRixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBRTtvQkFDZCxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUN0QixHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLENBQUM7b0JBQzlELEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLFlBQVksQ0FBQztvQkFDOUQsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUMsUUFBUSxDQUFDO2lCQUN2RDtxQkFBTTtvQkFDTCxHQUFHLENBQUMsU0FBUyxDQUFDLEdBQUcsS0FBSyxDQUFDO29CQUN2QixHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUN6QixHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUN6QixHQUFHLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO2lCQUN0QjtnQkFDRCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDckIsSUFBSSxDQUFDLDBCQUEwQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQ1QsNENBQTRDLEVBQzVDLElBQUksQ0FBQywwQkFBMEIsQ0FDaEMsQ0FBQztZQUNGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDOUMsQ0FBQyxDQUFDLENBQUM7UUFDSCxRQUFRLEVBQUUsQ0FBQztJQUNiLENBQUM7SUFFRCxVQUFVLENBQUMsV0FBVztRQUNwQixJQUFJLFdBQVcsSUFBSSxJQUFJLEVBQUU7WUFDdkIsT0FBTyxJQUFJLENBQUM7U0FDYjthQUFNO1lBQ0wsT0FBTyxLQUFLLENBQUM7U0FDZDtJQUNILENBQUM7SUFFRCxZQUFZLENBQUMsV0FBbUI7UUFDOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEdBQUcsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQzdELElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDakQsQ0FBQztJQUVELGtCQUFrQixDQUFDLFdBQW1CO1FBQ3BDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUMzRCxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1FBQzVDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQztJQUM5QyxDQUFDO0lBRUQsY0FBYyxDQUFDLEtBQUs7UUFDbEIsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDckUsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsSUFBSTtZQUNuQyxJQUFJLElBQUksQ0FBQyx1QkFBdUIsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxFQUFFO2dCQUN2RSxJQUFJLENBQUMsdUJBQXVCLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FDckMsSUFBSSxDQUFDLHVCQUF1QixFQUM1QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FDdkIsQ0FBQzthQUNIO2lCQUFNO2dCQUNMLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN2RDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELG9CQUFvQixDQUFDLEVBQUUsRUFBRSxJQUFJO1FBQzNCLDBCQUEwQjtRQUMxQixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxJQUFJLElBQUksV0FBVyxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsRUFBRSxDQUFDO1lBQ3pCLE9BQU8sR0FBRyxFQUFFLENBQUM7U0FDZDthQUFNO1lBQ0wsT0FBTyxHQUFHLEVBQUUsQ0FBQyxLQUFLLENBQUM7U0FDcEI7UUFFRCxJQUFJLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztRQUVoQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBRTNCLElBQUksT0FBTyxHQUFHO1lBQ1osTUFBTSxFQUFFLE9BQU87U0FDaEIsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDeEUsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO1lBQ3JDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDbkIsSUFBSSxHQUFHLEdBQUcsRUFBRSxDQUFDO2dCQUViLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN2QyxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQ3RDLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztnQkFDcEQsSUFBSSxJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDdkMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FDNUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQzlDLENBQUM7b0JBRUYsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7d0JBQ2QsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQzt3QkFDdEIsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxZQUFZLENBQUM7d0JBQ25FLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUMsWUFBWSxDQUFDO3dCQUNuRSxHQUFHLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDLFFBQVEsQ0FBQztxQkFDNUQ7eUJBQU07d0JBQ0wsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEtBQUssQ0FBQzt3QkFDdkIsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDekIsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQzt3QkFDekIsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztxQkFDdEI7aUJBQ0Y7cUJBQU07b0JBQ0wsR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztvQkFDekIsR0FBRyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztvQkFDekIsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQkFDdEI7Z0JBRUQsOEJBQThCO2dCQUM5QiwwQkFBMEI7Z0JBQzFCLElBQUk7Z0JBQ0osb0NBQW9DO2dCQUNwQywyQkFBMkI7Z0JBQzNCLElBQUk7Z0JBRUosSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3JCLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDNUMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1lBQzVDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNuRSx5QkFBeUI7UUFDM0IsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsWUFBWSxDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsSUFBSTtRQUMvQixJQUFJLElBQUksSUFBSSxLQUFLLEVBQUU7WUFDakIsSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksRUFBRTtnQkFDekIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUNwQixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7b0JBQ2IsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ3hCLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO29CQUMxQixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDeEIsR0FBRyxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7b0JBQ3RDLEdBQUcsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO29CQUN4QyxHQUFHLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztvQkFDeEMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7b0JBQ2hDLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUM5QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsQ0FBQzthQUNKO2lCQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLEVBQUU7Z0JBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUN2QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO29CQUMxQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQzthQUMvQjtTQUNGO2FBQU0sSUFBSSxJQUFJLElBQUksUUFBUSxFQUFFO1lBQzNCLElBQ0UsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FDaEMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDLElBQUksQ0FDckMsR0FBRyxDQUFDLEVBQ0w7Z0JBQ0EsSUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLElBQUksRUFBRTtvQkFDekIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDdkMsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQy9CLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxJQUFJLENBQ3JDLENBQUM7b0JBQ0YsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2lCQUNuQztxQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLElBQUksS0FBSyxFQUFFO29CQUNqQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUM1QyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsSUFBSSxDQUNuQyxDQUFDO29CQUNGLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FDL0IsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssT0FBTyxDQUFDLElBQUksQ0FDckMsQ0FBQztvQkFDRixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7aUJBQ3BDO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFFRCxXQUFXO1FBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsRUFBRSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNuRSxPQUFPLENBQUMsR0FBRyxDQUFDLCtCQUErQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDaEQsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FDekMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUNsQyxDQUFDO1lBRUYsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEVBQUU7Z0JBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDbEQ7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDNUM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksa0JBQWtCLENBQ2hELElBQUksQ0FBQyxtQkFBbUIsQ0FDekIsQ0FBQztRQUNGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUN0RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQztJQUMvRCxDQUFDO0lBRUQsWUFBWTtRQUNWLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7SUFDOUMsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSTtRQUN0QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7UUFFeEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztJQUNuRCxDQUFDO0lBRUQsbUJBQW1CLENBQUMsS0FBSyxFQUFFLElBQUk7UUFDN0IsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBRXhDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7SUFDbkQsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSTtRQUN0QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFFcEMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztJQUMvQyxDQUFDO0lBRUQsU0FBUztRQUNQLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7UUFDdEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQ0FBZ0MsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxXQUFXLEdBQUc7WUFDaEIsSUFBSSxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDM0IsUUFBUSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUTtZQUNqQyxXQUFXLEVBQUUsSUFBSSxDQUFDLHdCQUF3QjtZQUMxQyxXQUFXLEVBQUUsSUFBSSxDQUFDLHdCQUF3QjtZQUMxQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQzVELGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUNwRSxlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7WUFDckMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDMUQsTUFBTSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRTtTQUNyRCxDQUFDO1FBQ0YsSUFBSSxDQUFDLHdCQUF3QixDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQ2pFLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDTixNQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxDQUFDLENBQUM7WUFDL0QsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUseUJBQXlCLENBQUMsQ0FBQztZQUNsRCx5QkFBeUI7UUFDM0IsQ0FBQyxFQUNELENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDUix5QkFBeUI7WUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1FBQ2xELENBQUMsQ0FDRixDQUFDO0lBQ0osQ0FBQztJQUVELFFBQVEsQ0FBQyxJQUFJO1FBQ1gsSUFBSSxJQUFJLElBQUksWUFBWSxFQUFFO1lBQ3hCLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNwRCxRQUFRLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1NBQ3JDO2FBQU0sSUFBSSxJQUFJLElBQUksWUFBWSxFQUFFO1lBQy9CLElBQUksUUFBUSxHQUFHLEVBQUUsQ0FBQztZQUNsQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUNqQyxRQUFRLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1NBQ3JDO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxHQUFHLEdBQUc7WUFDUixXQUFXLEVBQUUsUUFBUTtZQUNyQixNQUFNLEVBQUUsUUFBUTtZQUNoQixNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNO1lBQzlCLEdBQUcsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUc7WUFDeEIsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTtZQUNsQyxTQUFTLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTO1lBQ3BDLFFBQVEsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVE7WUFDbEMsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUTtZQUNsQyxZQUFZLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxZQUFZO1lBQzFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsaUJBQWlCO1lBQ3BELFVBQVUsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVU7WUFDdEMsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRztZQUN4QixVQUFVLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDO1lBQ25DLE9BQU8sRUFBRSxLQUFLO1NBQ2YsQ0FBQztRQUNGLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxtQkFBbUIsQ0FDakQsR0FBRyxFQUNILElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUN2QixDQUFDLFNBQVMsQ0FDVCxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ04sSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTtnQkFDckIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDN0M7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDN0M7UUFDSCxDQUFDLEVBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNSLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzNDLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxLQUFLLEVBQUU7Z0JBQzFELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO2dCQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQzVDLFVBQVUsQ0FBQyxHQUFHLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7Z0JBQ3pCLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNWO1FBQ0gsQ0FBQyxDQUNGLENBQUM7UUFFRixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELE9BQU87UUFDTCxJQUFJLE1BQU0sR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzVDLElBQUksV0FBVyxHQUFHO1lBQ2hCLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUTtZQUN2QixNQUFNLEVBQUUsTUFBTTtTQUNmLENBQUM7UUFDRixJQUFJLENBQUMsd0JBQXdCO2FBQzFCLGFBQWEsQ0FBQyxXQUFXLENBQUM7YUFDMUIsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMscUNBQXFDLENBQUMsQ0FBQztRQUNsRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCwwQkFBMEIsQ0FBQyxPQUFPO1FBQ2hDLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQzVDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFLLE9BQU8sQ0FBQyxJQUFJLENBQ25DLENBQUM7UUFDRixJQUFJLENBQUMsbUJBQW1CLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUUxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBRW5DLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxrQkFBa0IsQ0FDaEQsSUFBSSxDQUFDLG1CQUFtQixDQUN6QixDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDdEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDNUMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUM7UUFDN0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQscUJBQXFCLENBQUMsT0FBTztRQUMzQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsU0FBUyxDQUM1QyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksS0FBSyxPQUFPLENBQUMsSUFBSSxDQUNuQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDMUMsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQy9DLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsNkJBQTZCLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsZUFBZSxDQUFDLFVBQVU7UUFDeEIsSUFBSSxXQUFXLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDeEQsSUFBSSxXQUFXLEdBQUc7WUFDaEIsY0FBYyxFQUFFLFdBQVc7U0FDNUIsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0I7YUFDMUIsa0JBQWtCLENBQUMsV0FBVyxDQUFDO2FBQy9CLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ2xCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRTtnQkFDOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLE1BQU07b0JBQzFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQztvQkFDdkIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDUCxJQUFJLFVBQVUsRUFBRTtvQkFDZCxJQUFJLENBQUMsWUFBWSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUM7aUJBQ3JDO2FBQ0Y7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxNQUFNLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsUUFBUTtRQUN4QyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDekQsSUFBSSxFQUFFLElBQUksZ0JBQWdCLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsZ0JBQWdCLENBQUM7YUFDbkM7aUJBQU0sSUFBSSxFQUFFLElBQUksV0FBVyxFQUFFO2dCQUM1QixJQUFJLENBQUMsU0FBUyxHQUFHLFdBQVcsQ0FBQztnQkFDN0IsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7Z0JBQ3ZCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO2FBQ3pCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUM7Z0JBQ3JDLElBQUksVUFBVSxJQUFJLFVBQVUsSUFBSSxFQUFFLEVBQUU7b0JBQ2xDLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztvQkFDcEIsSUFBSSxTQUFTLEtBQUssUUFBUSxFQUFFO3dCQUMxQixVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztxQkFDbEM7eUJBQU07d0JBQ0wsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7cUJBQzdCO29CQUNELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBQ25ELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLENBQUM7b0JBQzlDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQzt3QkFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDckIsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxTQUFTLEtBQUssTUFBTSxFQUFFO3dCQUN4QixJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUU7NEJBQ2pCLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUM7eUJBQ2xDOzZCQUFNLElBQUksUUFBUSxJQUFJLENBQUMsRUFBRTs0QkFDeEIsSUFBSSxhQUFhLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7NEJBQy9DLFVBQVUsQ0FBQyxJQUFJLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNuQyxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUNsQztxQkFDRjtpQkFDRjtnQkFDRCxrRUFBa0U7YUFDbkU7U0FDRjtJQUNILENBQUM7SUFFRCxnQkFBZ0I7SUFDaEIsZUFBZTtRQUNiLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBSSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDOUIsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO1NBQ3RDLENBQUM7UUFDRixJQUFJLENBQUMsd0JBQXdCO2FBQzFCLHNCQUFzQixDQUFDLFdBQVcsQ0FBQzthQUNuQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNsQixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUNyQyxPQUFPLENBQUMsZ0JBQWdCLENBQUM7aUJBQ3pCLE9BQU8sRUFBRTtpQkFDVCxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUU7Z0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLE9BQU87b0JBQ0wsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ25CLEtBQUssRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUM7aUJBQ2pDLENBQUM7WUFDSixDQUFDLENBQUM7aUJBQ0QsS0FBSyxFQUFFLENBQUM7WUFDWCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUU1QixNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ3pCLE1BQU0sU0FBUyxHQUFRLEVBQUUsQ0FBQztnQkFDMUIsU0FBUyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO2dCQUNwQyxTQUFTLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSx3QkFBd0IsQ0FBQyxDQUFDO1lBQy9ELGlDQUFpQztZQUNqQzs7Ozs7Ozs7O2NBU0o7UUFDRSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEVBQUUsQ0FBQztRQUM3QixJQUFJLFdBQVcsR0FBRztZQUNoQixPQUFPLEVBQUUsSUFBSSxDQUFDLGdCQUFnQjtZQUM5QixlQUFlLEVBQUUsSUFBSSxDQUFDLGVBQWU7U0FDdEMsQ0FBQztRQUNGLElBQUksQ0FBQyx3QkFBd0I7YUFDMUIsMEJBQTBCLENBQUMsV0FBVyxDQUFDO2FBQ3ZDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ2xCLHdDQUF3QztZQUN4QyxJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUNyQyxPQUFPLENBQUMsVUFBVSxDQUFDO2lCQUNuQixPQUFPLEVBQUU7aUJBQ1QsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUNqQixvQkFBb0I7Z0JBQ3BCLE9BQU87b0JBQ0wsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2QsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQztpQkFDakMsQ0FBQztZQUNKLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztZQUNYLHNCQUFzQjtZQUN0QixNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ3pCLE1BQU0sU0FBUyxHQUFRLEVBQUUsQ0FBQztnQkFDMUIsU0FBUyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUMvQixTQUFTLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxnQkFBZ0I7UUFDZCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQzlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O1VBbUJEO1FBQ0MsSUFBSSxXQUFXLEdBQUc7WUFDaEIsT0FBTyxFQUFFLElBQUksQ0FBQyxnQkFBZ0I7WUFDOUIsZUFBZSxFQUFFLElBQUksQ0FBQyxlQUFlO1NBQ3RDLENBQUM7UUFDRixJQUFJLENBQUMsd0JBQXdCO2FBQzFCLDJCQUEyQixDQUFDLFdBQVcsQ0FBQzthQUN4QyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNsQixJQUFJLE1BQU0sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUNyQyxPQUFPLENBQUMsVUFBVSxDQUFDO2lCQUNuQixPQUFPLEVBQUU7aUJBQ1QsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUNqQixvQkFBb0I7Z0JBQ3BCLE9BQU87b0JBQ0wsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQ2QsS0FBSyxFQUFFLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sQ0FBQztpQkFDakMsQ0FBQztZQUNKLENBQUMsQ0FBQztpQkFDRCxLQUFLLEVBQUUsQ0FBQztZQUNYLElBQUksVUFBVSxHQUFHLENBQUMsQ0FBQztZQUNuQixNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ3pCLFVBQVUsR0FBRyxVQUFVLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztZQUMxQyxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQztZQUMvQyxpQ0FBaUM7WUFDakMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUN6QixNQUFNLFNBQVMsR0FBUSxFQUFFLENBQUM7Z0JBRTFCLHFDQUFxQztnQkFDckMsaUNBQWlDO2dCQUNqQyxzQ0FBc0M7Z0JBQ3RDLGtFQUFrRTtnQkFDbEUsU0FBUyxDQUFDLFVBQVU7b0JBQ2xCLFVBQVUsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsR0FBRyxVQUFVLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7Z0JBQ3BFLFNBQVMsQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsU0FBUyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7Z0JBQ2xFLFNBQVMsQ0FBQyxLQUFLLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQztnQkFDaEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMzQyxDQUFDLENBQUMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGFBQWEsQ0FBQyxDQUFDO1FBQ2IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNmLE9BQU8sQ0FBQyxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7SUFDdkIsQ0FBQztJQUNELFVBQVUsQ0FBQyxHQUFHO1FBQ1osSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUNqQixPQUFPLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUM3QjthQUFNO1lBQ0wsT0FBTyxFQUFFLENBQUM7U0FDWDtJQUNILENBQUM7SUFFRCxRQUFRLENBQUMsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRO1FBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDckIsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztRQUM3QyxJQUFJLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFFL0QsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG9CQUFvQixHQUFHLE9BQU8sQ0FBQztJQUMvRCxDQUFDO0lBRUQsS0FBSztRQUNILElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUNoQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7SUFDckIsQ0FBQzs7O1lBL3dDRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsODk5Q0FBOEM7Z0JBRTlDLFVBQVUsRUFBRTtvQkFDVixPQUFPLENBQUMsY0FBYyxFQUFFO3dCQUN0QixLQUFLLENBQ0gsV0FBVyxFQUNYLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLEdBQUcsRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FDMUQ7d0JBQ0QsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDekMsVUFBVSxDQUNSLHdCQUF3QixFQUN4QixPQUFPLENBQUMsc0NBQXNDLENBQUMsQ0FDaEQ7cUJBQ0YsQ0FBQztvQkFDRixjQUFjO2lCQUNmO2dCQUNELFNBQVMsRUFBRTtvQkFDVCw0RkFBNEY7b0JBQzVGLDhGQUE4RjtvQkFDOUYsaUNBQWlDO29CQUNqQzt3QkFDRSxPQUFPLEVBQUUsV0FBVzt3QkFDcEIsUUFBUSxFQUFFLGlCQUFpQjt3QkFDM0IsSUFBSSxFQUFFLENBQUMsZUFBZSxDQUFDO3FCQUN4QjtvQkFFRCxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFO2lCQUNwRDs7YUFDRjs7OztZQXZGQyxXQUFXO1lBTlgsU0FBUztZQXdCRixNQUFNO1lBRE4sZUFBZTtZQVpmLHVCQUF1QjtZQUV2QiwwQkFBMEI7WUF3QjFCLGNBQWM7WUFFZCxrQkFBa0I7Ozt3QkE4SHhCLFNBQVMsU0FBQyxXQUFXO3lCQUNyQixTQUFTLFNBQUMsWUFBWTt5QkFDdEIsU0FBUyxTQUFDLFlBQVk7OEJBQ3RCLFNBQVMsU0FBQyxpQkFBaUI7d0JBQzNCLFNBQVMsU0FBQyxXQUFXLEVBQUU7b0JBQ3RCLElBQUksRUFBRSxRQUFRO2lCQUNmO3NCQUdBLFNBQVMsU0FBQyxTQUFTLEVBQUU7b0JBQ3BCLElBQUksRUFBRSxRQUFRO2lCQUNmIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU2VsZWN0aW9uTW9kZWwgfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFRhYmxlRGF0YVNvdXJjZSwgTWF0SW5wdXQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7XHJcbiAgTWF0RGlhbG9nLFxyXG4gIE1hdFNpZGVuYXYsXHJcbiAgTWF0RGlhbG9nUmVmLFxyXG4gIE1BVF9ESUFMT0dfREFUQVxyXG59IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHtcclxuICBGb3JtQnVpbGRlcixcclxuICBGb3JtR3JvdXAsXHJcbiAgVmFsaWRhdG9ycyxcclxuICBGb3JtQ29udHJvbFxyXG59IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUmVwb3J0TWFuYWdlbWVudFNlcnZpY2UgfSBmcm9tICcuLi9yZXBvcnQtbWFuYWdlbWVudC5zZXJ2aWNlJztcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gJ2ZpbGUtc2F2ZXInO1xyXG5pbXBvcnQgeyBTZXR1cEFkbWluaXN0cmF0aW9uU2VydmljZSB9IGZyb20gJy4uL3NldHVwLWFkbWluaXN0cmF0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBSZXBvcnRIaXN0b3J5Q29tcG9uZW50IH0gZnJvbSAnLi9yZXBvcnQtbWFuYWdlbWVudC1oaXN0b3J5L3JlcG9ydC1tYW5hZ2VtZW50LmNvbXBvbmVudCc7XHJcblxyXG5pbXBvcnQge1xyXG4gIGFuaW1hdGUsXHJcbiAgc3RhdGUsXHJcbiAgc3R5bGUsXHJcbiAgdHJhbnNpdGlvbixcclxuICB0cmlnZ2VyXHJcbn0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2hhcmVkL3NuYWNrYmFyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gJ2xvZGFzaCc7XHJcbmltcG9ydCB7IGZ1c2VBbmltYXRpb25zIH0gZnJvbSAnLi4vLi4vQGZ1c2UvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTW9tZW50RGF0ZUFkYXB0ZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC1tb21lbnQtYWRhcHRlcic7XHJcbmltcG9ydCB7XHJcbiAgRGF0ZUFkYXB0ZXIsXHJcbiAgTUFUX0RBVEVfRk9STUFUUyxcclxuICBNQVRfREFURV9MT0NBTEVcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9jb3JlJztcclxuaW1wb3J0ICogYXMgX21vbWVudCBmcm9tICdtb21lbnQnO1xyXG5jb25zdCBtb21lbnQgPSBfbW9tZW50O1xyXG5cclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9fc2VydmljZXMvbWVzc2FnZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE5neFVpTG9hZGVyU2VydmljZSB9IGZyb20gJ25neC11aS1sb2FkZXInOyAvLyBJbXBvcnQgTmd4VWlMb2FkZXJTZXJ2aWNlXHJcbmltcG9ydCB7Y29uZmlnTWV0YURhdGF9IGZyb20gJy4vZGVtb0RhdGEnO1xyXG5leHBvcnQgY29uc3QgTVlfRk9STUFUUyA9IHtcclxuICBwYXJzZToge1xyXG4gICAgZGF0ZUlucHV0OiAnTEwnXHJcbiAgfSxcclxuICBkaXNwbGF5OiB7XHJcbiAgICBkYXRlSW5wdXQ6ICdMTCcsXHJcbiAgICBtb250aFllYXJMYWJlbDogJ01NTSBZWVlZJyxcclxuICAgIGRhdGVBMTF5TGFiZWw6ICdMTCcsXHJcbiAgICBtb250aFllYXJBMTF5TGFiZWw6ICdNTU1NIFlZWVknXHJcbiAgfVxyXG59O1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBFbGVtZW50IHtcclxuICBbeDogc3RyaW5nXTogYW55O1xyXG4gIG1vZHVsZTogc3RyaW5nO1xyXG4gIGFwcGxpY2F0aW9uX25hbWU6IHN0cmluZztcclxuICBvYmplY3RfbmFtZTogc3RyaW5nO1xyXG4gIG9iamVjdF9jb2RlOiBzdHJpbmc7XHJcbiAgLy8gY29udHJvbF9uYW1lOiBzdHJpbmc7XHJcbiAgLy8gY29udHJvbF90eXBlOiBzdHJpbmc7XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiAnYXBwLWNvbmZpZy10cmFja2VyJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vY29uZmlnLXRyYWNrZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gIHN0eWxlVXJsczogWycuL2NvbmZpZy10cmFja2VyLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgYW5pbWF0aW9uczogW1xyXG4gICAgdHJpZ2dlcignZGV0YWlsRXhwYW5kJywgW1xyXG4gICAgICBzdGF0ZShcclxuICAgICAgICAnY29sbGFwc2VkJyxcclxuICAgICAgICBzdHlsZSh7IGhlaWdodDogJzBweCcsIG1pbkhlaWdodDogJzAnLCBkaXNwbGF5OiAnbm9uZScgfSlcclxuICAgICAgKSxcclxuICAgICAgc3RhdGUoJ2V4cGFuZGVkJywgc3R5bGUoeyBoZWlnaHQ6ICcqJyB9KSksXHJcbiAgICAgIHRyYW5zaXRpb24oXHJcbiAgICAgICAgJ2V4cGFuZGVkIDw9PiBjb2xsYXBzZWQnLFxyXG4gICAgICAgIGFuaW1hdGUoJzIyNW1zIGN1YmljLWJlemllcigwLjQsIDAuMCwgMC4yLCAxKScpXHJcbiAgICAgIClcclxuICAgIF0pLFxyXG4gICAgZnVzZUFuaW1hdGlvbnNcclxuICBdLFxyXG4gIHByb3ZpZGVyczogW1xyXG4gICAgLy8gYE1vbWVudERhdGVBZGFwdGVyYCBjYW4gYmUgYXV0b21hdGljYWxseSBwcm92aWRlZCBieSBpbXBvcnRpbmcgYE1vbWVudERhdGVNb2R1bGVgIGluIHlvdXJcclxuICAgIC8vIGFwcGxpY2F0aW9uJ3Mgcm9vdCBtb2R1bGUuIFdlIHByb3ZpZGUgaXQgYXQgdGhlIGNvbXBvbmVudCBsZXZlbCBoZXJlLCBkdWUgdG8gbGltaXRhdGlvbnMgb2ZcclxuICAgIC8vIG91ciBleGFtcGxlIGdlbmVyYXRpb24gc2NyaXB0LlxyXG4gICAge1xyXG4gICAgICBwcm92aWRlOiBEYXRlQWRhcHRlcixcclxuICAgICAgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyLFxyXG4gICAgICBkZXBzOiBbTUFUX0RBVEVfTE9DQUxFXVxyXG4gICAgfSxcclxuXHJcbiAgICB7IHByb3ZpZGU6IE1BVF9EQVRFX0ZPUk1BVFMsIHVzZVZhbHVlOiBNWV9GT1JNQVRTIH1cclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb25maWdUcmFja2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBtZXNzYWdlczogYW55W10gPSBbXTtcclxuICBzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuXHJcbiAgc3VibWl0dGVkID0gZmFsc2U7XHJcbiAgZXJyb3JTdGF0dXM6IGFueTtcclxuICBlcnJvck1zZzogYW55O1xyXG4gIHR5cGVzOiBhbnk7XHJcbiAgdHlwZXNzcyA9IFtdO1xyXG4gIHNlbGVjdGVkX21vZHVsZXM6IGFueTtcclxuICBtb2R1bGVzOiBhbnk7XHJcbiAgc2VsZWN0ZWRfZGF0YV9sZW5ndGg6IGFueTtcclxuICBmaW5hbF9zZWxlY3RlZF9kYXRhID0gW107XHJcbiAgZmluYWxfc2VsZWN0ZWRfdGFibGU6IGFueTtcclxuICBmaW5hbF9zZWxlY3RlZF9sZW5ndGg6IGFueTtcclxuICBnZXRDb25maWdUYWJsZTogYW55O1xyXG4gIG1vZGU6IGFueTtcclxuICBhcHBfdHlwZXMgPSBbXTtcclxuICBkYXRhc291cmNlOiBhbnk7XHJcbiAgZGF0YXNvdXJjZWU6IGFueTtcclxuICBzZWxlY3RlZF9kYXRhc291cmNlOiBhbnk7XHJcbiAgQ3JlYXRlRm9ybTogRm9ybUdyb3VwO1xyXG4gIGRhdGFTb3VyY2U6IGFueTtcclxuICBvYmplY3RfbGlzdDogYW55O1xyXG4gIG9iamVjdF9saXN0X2xlbmd0aDogYW55O1xyXG4gIHNlbGV0ZWREYXRhc291cmNlSWQ6IGFueTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZU5hbWU6IGFueTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIHNjaGVkdWxlVHlwZTogYW55O1xyXG4gIGNoZWNrZWRDb250cm9sOiBzdHJpbmdbXSA9IFtdO1xyXG4gIGRpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gWydzZWxlY3QnLCAnb2JqZWN0X25hbWUnLCAnb2JqZWN0X2NvZGUnXTtcclxuICBkaXNwbGF5Q29sdW1uczogc3RyaW5nW10gPSBbXHJcbiAgICAnbW9kdWxlX25hbWUnLFxyXG4gICAgJ25hbWUnLFxyXG4gICAgJ2NvbnRyb2xfbmFtZScsXHJcbiAgICAnY29udHJvbF90eXBlJyxcclxuICAgICdyaXNrUmFuaycsXHJcbiAgICAnYWN0aW9uJ1xyXG4gIF07XHJcbiAgZGlzcGxheUNvbHVtbnNMaXN0OiBzdHJpbmdbXSA9IFtcclxuICAgICdtb2R1bGVfbmFtZScsXHJcbiAgICAnbmFtZScsXHJcbiAgICAnY29udHJvbF9uYW1lJyxcclxuICAgICdjb250cm9sX3R5cGUnLFxyXG4gICAgJ3Jpc2tSYW5rJ1xyXG4gIF07XHJcbiAgZGlzcGxheU9iamVjdHM6IHN0cmluZ1tdID0gW1xyXG4gICAgJ3NlbGVjdCcsXHJcbiAgICAnbmFtZScsXHJcbiAgICAnY29udHJvbF9uYW1lJyxcclxuICAgICdjb250cm9sX3R5cGUnLFxyXG4gICAgJ3Jpc2tSYW5rJ1xyXG4gIF07XHJcbiAgc2VsZWN0ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFtcclxuICAgICdhcHBsaWNhdGlvbl9uYW1lJyxcclxuICAgICdvYmplY3RfbmFtZScsXHJcbiAgICAnb2JqZWN0X2NvZGUnXHJcbiAgXTtcclxuICBzZWxlY3RDb250cm9sIDogYW55O1xyXG4gIHNlbGVjdGVkX29iamVjdHM6IGFueTtcclxuICBzZWxlY3RfbW9kdWxlczogYW55O1xyXG4gIHJlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0OiBhbnk7XHJcbiAgZXhwYW5kRWxlbWVudDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGRhdGVDb2x1bW5zOiBhbnk7XHJcbiAgcGFyZW50TGltaXQgOiBhbnkgPSAxMDtcclxuICBleHBhbmRlZEVsZW1lbnQgOiBhbnk7XHJcbiAgXHJcbiAgLy8gQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIC8vIEBWaWV3Q2hpbGQoTWF0UGFnaW5hdG9yKSBzcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgLy8gQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIGZwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuXHJcbiAgQFZpZXdDaGlsZCgncGFnaW5hdG9yJykgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgQFZpZXdDaGlsZCgnc3BhZ2luYXRvcicpIHNwYWdpbmF0b3I6IE1hdFBhZ2luYXRvcjtcclxuICBAVmlld0NoaWxkKCdmcGFnaW5hdG9yJykgZnBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIEBWaWV3Q2hpbGQoJ3JlcG9ydHBhZ2luYXRvcicpIHJlcG9ydHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIEBWaWV3Q2hpbGQoJ2Zyb21JbnB1dCcsIHtcclxuICAgIHJlYWQ6IE1hdElucHV0XHJcbiAgfSlcclxuICBmcm9tSW5wdXQ6IE1hdElucHV0O1xyXG5cclxuICBAVmlld0NoaWxkKCd0b0lucHV0Jywge1xyXG4gICAgcmVhZDogTWF0SW5wdXRcclxuICB9KVxyXG4gIHRvSW5wdXQ6IE1hdElucHV0O1xyXG5cclxuICBkU291cmNlOiBFbGVtZW50W10gPSBbXHJcbiAgICB7XHJcbiAgICAgIG1vZHVsZTogJ0dMJyxcclxuICAgICAgYXBwbGljYXRpb25fbmFtZTogJ0dlbmVyYWwgTGVkZ2VyJyxcclxuICAgICAgb2JqZWN0X25hbWU6ICdBY2NvdW50aW5nIENhbGVuZGFyJyxcclxuICAgICAgb2JqZWN0X2NvZGU6ICcnXHJcbiAgICB9XHJcbiAgICAvLyB7bW9kdWxlOiAnR0wnLCBhcHBsaWNhdGlvbl9uYW1lOiAnR2VuZXJhbCBMZWRnZXInLCBvYmplY3RfbmFtZTogJ0NvbHVtbiBzZXQnLCBjb250cm9sX25hbWU6ICcnLGNvbnRyb2xfdHlwZTonJ31cclxuICBdO1xyXG5cclxuICBwcm9maWxlRm9ybSA9IG5ldyBGb3JtR3JvdXAoe1xyXG4gICAgc2VsZWN0ZWQ6IG5ldyBGb3JtQ29udHJvbCgpXHJcbiAgfSk7XHJcblxyXG4gIHNjaGVkdWxlb2JqOiBhbnk7XHJcbiAgbG9naW5Vc2VyOiBhbnk7XHJcblxyXG4gIGRTcmNSZXBvcnQgPSBbXHJcbiAgICB7XHJcbiAgICAgIGFwcF9uYW1lOiAnJyxcclxuICAgICAgb3BlcmF0aW9uOiAnJyxcclxuICAgICAgb2JqZWN0OiAnJyxcclxuICAgICAgY2hhbmdlX2RhdGU6ICcnLFxyXG4gICAgICBjaGFuZ2VfYnk6ICcnLFxyXG4gICAgICBjb250cm9sX25hbWU6ICcnLFxyXG4gICAgICBleGlzdGluZ19yZWNvcmQ6ICcnLFxyXG4gICAgICBuZXdfcmVjb3JkOiAnJ1xyXG4gICAgfVxyXG4gIF07XHJcblxyXG4gIGNvbnRyb2xUeXBlcyA9IFtcclxuICAgIHtcclxuICAgICAgdmFsdWU6ICdTT1gnLFxyXG4gICAgICBuYW1lOiAnU09YJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgdmFsdWU6ICdISVBBQScsXHJcbiAgICAgIG5hbWU6ICdISVBBQSdcclxuICAgIH0sXHJcbiAgICB7XHJcbiAgICAgIHZhbHVlOiAnRklEQScsXHJcbiAgICAgIG5hbWU6ICdGSURBJ1xyXG4gICAgfVxyXG4gIF07XHJcbiAgcmlza1JhbmtzID0gW1xyXG4gICAge1xyXG4gICAgICB2YWx1ZTogJ2NyaXRpY2FsJyxcclxuICAgICAgbmFtZTogJ0NyaXRpY2FsJ1xyXG4gICAgfSxcclxuICAgIHtcclxuICAgICAgdmFsdWU6ICdoaWdoJyxcclxuICAgICAgbmFtZTogJ0hpZ2gnXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICB2YWx1ZTogJ21lZGl1bScsXHJcbiAgICAgIG5hbWU6ICdNZWRpdW0nXHJcbiAgICB9LFxyXG4gICAge1xyXG4gICAgICB2YWx1ZTogJ2xvdycsXHJcbiAgICAgIG5hbWU6ICdMb3cnXHJcbiAgICB9XHJcbiAgXTtcclxuXHJcbiAgc3RhdHVzRmlsdGVyOiBhbnk7XHJcblxyXG4gIGNvbmZpZ3M6IGFueTtcclxuICBkaWFsb2dSZWY6IGFueTtcclxuICAvLyBkYXRhU291cmNlOmFueTtcclxuICBkYXRhU291cmNlUmVwb3J0OiBhbnk7XHJcbiAgLy8gZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbJ2NsaWVudElEJywgJ2Z1c2lvblVybCcsICd1c2VyTmFtZScsJ3F1ZXJ5VHlwZXMnLCAnc2NoZWR1bGVUeXBlJywncmV0YWluRGF5cycgXTtcclxuICBkaXNwbGF5ZWRSZXBvcnQ6IHN0cmluZ1tdID0gW1xyXG4gICAgJ2FwcF9uYW1lJyxcclxuICAgICdvcGVyYXRpb24nLFxyXG4gICAgJ29iamVjdCcsXHJcbiAgICAnY2hhbmdlX2RhdGUnLFxyXG4gICAgJ2NoYW5nZV9ieScsXHJcbiAgICAnY29udHJvbF9uYW1lJyxcclxuICAgICdleGlzdGluZ19yZWNvcmQnLFxyXG4gICAgJ25ld19yZWNvcmQnXHJcbiAgXTtcclxuICBzZWxlY3RlZF9kYXRhOiBhbnk7XHJcbiAgc2hvd19kYXRhOiBzdHJpbmcgPSAnZGFzaGJvYXJkJztcclxuICBjb2x1bW5zVG9EaXNwbGF5OiBzdHJpbmdbXSA9IFtdO1xyXG4gIGNvbmZpZ0RhdGE6IGFueTtcclxuICBmcm9tRGF0ZSA9IG5ldyBGb3JtQ29udHJvbChtb21lbnQoKSk7XHJcbiAgdG9EYXRlID0gbmV3IEZvcm1Db250cm9sKG1vbWVudCgpKTtcclxuICBtb2R1bGVOYW1lczogYW55ID0gY29uZmlnTWV0YURhdGEubW9kdWxlRGF0YTtcclxuICBjb25maWdUcmFja2VySGVhZGVyOiBhbnk7XHJcbiAgc2VsZWN0ZWRUYWJIZWFkZXI6IGFueSA9IFtdO1xyXG4gIGhpc3RvcnlEYXRhOiBhbnkgPSB7fTtcclxuXHJcbiAgc2VsZWN0ZWRUYWJsZUhlYWRlcjogYW55ID0gW107XHJcbiAgdGFibGVIZWFkZXIgPSBjb25maWdNZXRhRGF0YS50YWJsZUNvbmZpZztcclxuICB0YWJsZUNvbmZpZzogYW55ID0gW107XHJcbiAgaGlzdG9yeUNvbmZpZ0RhdGE6IGFueSA9IFtdO1xyXG5cclxuICBzdGVwID0gMDtcclxuICBzZWxlY3RlZEhlYWRlcklkOiBhbnk7XHJcbiAgc2VsZWN0ZWRIZWFkZXJNb2R1bGVuYW1lOiBhbnk7XHJcbiAgc2VsZWN0ZWRIZWFkZXJPYmplY3ROYW1lOiBhbnk7XHJcblxyXG4gIC8qIGNoYXJ0IGFzc2lnbm1lbnQgKi9cclxuICBtb2R1bGVPYmplY3ROYW1lOiBhbnk7XHJcbiAgY29uZnRjaGFydGJ5bW9kdWxlOiBzdHJpbmdbXSA9IFtdO1xyXG4gIHVzZXJTZWxlY3RlZFRvZ2dsZTogc3RyaW5nO1xyXG4gIHZhbDogYW55O1xyXG4gIGVic19tX2NoYXJ0OiBhbnkgPSBbXTtcclxuICBtYXN0ZXJfY2hhcnQ6IGFueSA9IFtdO1xyXG4gIHdlZWtfYnlfdXNlcl9jaGFydDogYW55ID0gW107XHJcbiAgbW9udGhfYnlfdXNlcl9jaGFydDogYW55ID0gW107XHJcbiAgLy8gQmFyXHJcbiAgc2hvd1hBeGlzID0gdHJ1ZTtcclxuICBzaG93WUF4aXMgPSB0cnVlO1xyXG4gIGdyYWRpZW50ID0gZmFsc2U7XHJcbiAgc2hvd0xlZ2VuZCA9IHRydWU7XHJcbiAgc2hvd1hBeGlzTGFiZWwgPSB0cnVlO1xyXG4gIHNob3dZQXhpc0xhYmVsID0gdHJ1ZTtcclxuXHJcbiAgeGxhYmVsID0gJ01vZHVsZXMnO1xyXG4gIHlsYWJlbCA9ICdSZWNvcmRzJztcclxuICB4bGFiZWx3ZWVrID0gJ1VzZXJzJztcclxuICB5bGFiZWx3ZWVrID0gJ1JlY29yZHMnO1xyXG4gIC8qcGllIGNoYXJ0Ki9cclxuICAvLyBvcHRpb25zXHJcbiAgc2hvd1BpZUxlZ2VuZCA9IGZhbHNlO1xyXG4gIGxlZ2VuZFBvc2l0aW9uID0gJ3JpZ2h0JztcclxuICB2aWV3OiBhbnlbXSA9IFs3MDAsIDQwMF07XHJcbiAgd2Vla3ZpZXc6IGFueVtdID0gWzQ1MCwgMzUwXTtcclxuICBwaWV2aWV3OiBhbnlbXSA9IFs0NjAsIDQwMF07XHJcbiAgbGVnZW5kVGl0bGUgPSAnVXNlcnMnO1xyXG4gIG1hc3RlcmJ5dXNlcnZpZXc6IGFueVtdID0gWzYwMCwgMzUwXTtcclxuICBjb2xvclBpZVNjaGVtZSA9IHtcclxuICAgIGRvbWFpbjogWycjMjhhM2RkJywgJyM4ZWM5ZTUnLCAnIzFjNzI5YScsICcjMTg2MTg0JywgJyMxNDUxNmUnLCAnIzEwNDE1OCddXHJcbiAgfTtcclxuICBsaW1pdDogbnVtYmVyID0gMTA7XHJcbiAgb2Zmc2V0OiBudW1iZXIgPSAwO1xyXG4gIGxlbmd0aDogYW55O1xyXG4gIC8vIHBpZVxyXG4gIHNob3dMYWJlbHMgPSB0cnVlO1xyXG4gIGV4cGxvZGVTbGljZXMgPSBmYWxzZTtcclxuICBjb25maWdUcmFja2VyU2V0dXBEb25lID0gZmFsc2U7XHJcbiAgY29uZmlnUVRTZXR1cERvbmUgPSBmYWxzZTtcclxuICBkb3VnaG51dCA9IGZhbHNlO1xyXG4gIGlzQ2hlY2tlZCA9IGZhbHNlO1xyXG4gIC8vIHhBeGlzTGFiZWw9dHJ1ZTtcclxuICBiYXJQYWRkaW5nID0gNTA7XHJcbiAgY29sb3JTY2hlbWUgPSB7XHJcbiAgICBkb21haW46IFsnIzI4YTNkZCddXHJcbiAgfTtcclxuICBmb3JtQWN0aW9uOiBhbnk7XHJcbiAgY3VycmVudEhlYWRlckRhdGE6IGFueTtcclxuICBzZWxlY3RvYmpoZWFkZXI6IGFueTtcclxuICBjb25maWdUcmFja2VySUQ6IGFueTtcclxuICBvblNlbGVjdGVkTW9kdWxlT2JqZWN0czogYW55ID0gW107XHJcbiAgdW5pcXVlSUQ6IGFueTtcclxuICBvYmplY3RfbmFtZV9STV9ieU1vZHVsZTogYW55O1xyXG4gIG1vZHVsZV9STTogYW55O1xyXG4gIHNlbGVjdF9tb2R1bGVzX1JNOiBhbnk7XHJcbiAgb2JqZWN0TmFtZV9STTogYW55O1xyXG4gIGNsaWVudElEOiBhbnk7XHJcbiAgdXNlckxpc3Q6IGFueTtcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgX21hdERpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9yZXBvcnRNYW5hZ2VtZW50U2VydmljZTogUmVwb3J0TWFuYWdlbWVudFNlcnZpY2UsXHJcbiAgICBwcml2YXRlIFNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlOiBTZXR1cEFkbWluaXN0cmF0aW9uU2VydmljZSxcclxuXHJcbiAgICBwcml2YXRlIG1lc3NhZ2VTZXJ2aWNlOiBNZXNzYWdlU2VydmljZSxcclxuICAgIHByaXZhdGUgbmd4U2VydmljZTogTmd4VWlMb2FkZXJTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICB0aGlzLkNyZWF0ZUZvcm0gPSB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgc2VsZWN0OiBbJyddLFxyXG4gICAgICBtb2R1bGU6IFsnJ10sXHJcbiAgICAgIHVzZXJOYW1lOiBbJyddLFxyXG4gICAgICBvYmplY3RfbmFtZTogWycnXSxcclxuICAgICAgY29udHJvbF90eXBlOiBbJyddLFxyXG4gICAgICBjb250cm9sX25hbWU6IFsnJ10sXHJcbiAgICAgIHJpc2tSYW5rOiBbJyddXHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5mb3JtQWN0aW9uID0gJ2FkZCc7XHJcbiAgICB0aGlzLnR5cGVzID0gW107XHJcbiAgICB0aGlzLm5neFNlcnZpY2Uuc3RhcnQoKTtcclxuICAgIHRoaXMuc2hvd2VkaXQoKTtcclxuICAgIHRoaXMuZ2V0YWxsY29uZmlncygpO1xyXG4gICAgLy8gdGhpcy5zZWxlY3RlZFRhYkhlYWRlciA9IFtcclxuICAgIC8vIFx0e1xyXG4gICAgLy8gXHRcdCd2YWx1ZSc6J3RyYW5zYWN0aW9udHlwZScsXHJcbiAgICAvLyBcdFx0J25hbWUnOiAnQVJfVHJhbnNhY3Rpb25UeXBlcydcclxuICAgIC8vIFx0fSx7XHJcbiAgICAvLyBcdFx0J3ZhbHVlJzoncGF5bWVudHRlcm1zJyxcclxuICAgIC8vIFx0XHQnbmFtZSc6ICdBUl9QYXltZW50VGVybXMnXHJcbiAgICAvLyBcdH1cclxuICAgIC8vIF07XHJcbiAgICB0aGlzLm1vZGUgPSAndmlldyc7XHJcbiAgfVxyXG4gIG9wZW5IaXN0b3J5KGVsZW1lbnQpOiB2b2lkIHtcclxuICAgIC8vIHZhciBzZWxlY3RlZENvbHVtbiA9IF8uZmlsdGVyKHRoaXMudGFibGVDb25maWcsIFsnaXNJZGVudGlmaWVyJywgdHJ1ZV0pO1xyXG4gICAgY29uc29sZS5sb2coZWxlbWVudCwgJy4uLi4uLi5lbGVtZW50Jyk7XHJcbiAgICAvLyBsZXQgc2VsZWN0SWQgPSAgZWxlbWVudFtzZWxlY3RlZENvbHVtblswXS5uYW1lXVtcIm5hbWVcIl1cclxuXHJcbiAgICB0aGlzLmRpYWxvZ1JlZiA9IHRoaXMuX21hdERpYWxvZy5vcGVuKFJlcG9ydEhpc3RvcnlDb21wb25lbnQsIHtcclxuICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICB3aWR0aDogJzc1JScsXHJcbiAgICAgIHBhbmVsQ2xhc3M6ICdjb250YWN0LWZvcm0tZGlhbG9nJyxcclxuICAgICAgZGF0YToge1xyXG4gICAgICAgIGFjdGlvbjogJ25ldycsXHJcbiAgICAgICAgaGlzdG9yeURhdGE6IGVsZW1lbnQuaGlzdG9yeURhdGEsXHJcbiAgICAgICAgcmVwb3J0RGF0YTogZWxlbWVudCxcclxuICAgICAgICBzZWxlY3RlZEhlYWRlcjogdGhpcy5zZWxlY3RlZEhlYWRlcklkLFxyXG4gICAgICAgIGRhdGVDb2x1bW5zOiB0aGlzLmRhdGVDb2x1bW5zXHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBvcGVuTmF2aWdhdGUoKSB7XHJcbiAgICBsZXQgb2JqID0ge1xyXG4gICAgICB1cmwgOiAnc2V0dXAtYWRtaW5pc3RyYXRpb24vY29uZmlnLXRyYWNrZXItc2V0dXAnLFxyXG4gICAgICBpZCA6IFwibWFuYWdlQ29uZmlnVHJhY2tlclNldHVwXCJcclxuICAgIH1cclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFJvdXRpbmcob2JqKTtcclxuICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnc2V0dXAtYWRtaW5pc3RyYXRpb24vY29uZmlnLXRyYWNrZXItc2V0dXAnXSk7XHJcbiAgfVxyXG4gIGdldGFsbGNvbmZpZ3MoKSB7XHJcbiAgICB2YXIgdXNlcklkID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oJ3VzZXJJZCcpO1xyXG4gICAgdGhpcy5zZWxlY3RlZF9tb2R1bGVzID0gW107XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEgPSBbXTtcclxuICAgIHRoaXMuZ2V0Q29uZmlnVGFibGUgPSBbXTtcclxuICAgIHRoaXMuZFNvdXJjZS5sZW5ndGggPSAwO1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0QWxsQ29uZmlnVHJhY2tlcih1c2VySWQpLnN1YnNjcmliZShcclxuICAgICAgKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgIHZhciBkYXRhX25ldyA9IGRhdGEuYm9keTtcclxuICAgICAgICB0aGlzLmRTb3VyY2UgPSBkYXRhX25ldy5yZXNwb25zZS5jb25maWdEZXRhaWxzO1xyXG4gICAgICAgIGlmICh0aGlzLmRTb3VyY2UubGVuZ3RoICE9IDApIHtcclxuICAgICAgICAgIHRoaXMuY29uZmlnVHJhY2tlclNldHVwRG9uZSA9IHRydWU7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkX21vZHVsZXMgPSB0aGlzLmRTb3VyY2VbMF0ubW9kdWxlO1xyXG4gICAgICAgICAgdGhpcy5nZXRDb25maWdUYWJsZSA9IHRoaXMuZFNvdXJjZVswXS5vYmplY3RfbmFtZTtcclxuICAgICAgICAgIHRoaXMuY29uZmlnVHJhY2tlcklEID0gdGhpcy5kU291cmNlWzBdLl9pZDtcclxuICAgICAgICAgIHRoaXMuY2xpZW50SUQgPSB0aGlzLmRTb3VyY2VbMF0uY2xpZW50SUQ7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkX2RhdGEgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMuZ2V0Q29uZmlnVGFibGUpO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZF9kYXRhLnBhZ2luYXRvciA9IHRoaXMuc3BhZ2luYXRvcjtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWRfZGF0YXNvdXJjZSA9IHRoaXMuZ2V0Q29uZmlnVGFibGU7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkX2RhdGFfbGVuZ3RoID0gdGhpcy5nZXRDb25maWdUYWJsZS5sZW5ndGg7XHJcblxyXG4gICAgICAgICAgdGhpcy5tb2R1bGVfUk0gPSB0aGlzLmRTb3VyY2VbMF0ubW9kdWxlO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coJ01vZHVsZS0tLS0tLS0nLCB0aGlzLmRTb3VyY2VbMF0ubW9kdWxlKTtcclxuICAgICAgICAgIHRoaXMub2JqZWN0X25hbWVfUk1fYnlNb2R1bGUgPSBfLmdyb3VwQnkoXHJcbiAgICAgICAgICAgIHRoaXMuZFNvdXJjZVswXS5vYmplY3RfbmFtZSxcclxuICAgICAgICAgICAgJ21vZHVsZV9uYW1lJ1xyXG4gICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICBpZiAodGhpcy5kU291cmNlWzBdLm1vZHVsZS5sZW5ndGggIT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1BY3Rpb24gPSAnZWRpdCc7XHJcbiAgICAgICAgICAgIHRoaXMuY29uZmlnUVRTZXR1cERvbmUgPSB0cnVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtQWN0aW9uID0gJ2FkZCc7XHJcbiAgICAgICAgICAgIHRoaXMuc2hvd19kYXRhID0gJ2NvbmZpZ190cmFja2VyJztcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMubW9kZSA9ICd2aWV3JztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5zaG93X2RhdGEgPSAnY29uZmlnX3RyYWNrZXInO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLlRvZ2dsZSgnZGFzaGJvYXJkJywgbnVsbCwgbnVsbCwgbnVsbCk7XHJcbiAgICAgICAgdGhpcy5uZ3hTZXJ2aWNlLnN0b3AoKTtcclxuICAgICAgfSxcclxuICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgdGhpcy5uZ3hTZXJ2aWNlLnN0b3AoKTtcclxuICAgICAgICBjb25zb2xlLmxvZygnZXJyb3I6OjonICsgZXJyb3IpO1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAnT29wcyBTb21ldGhpbmcgd2VudCB3cm9uZyEhIFBsZWFzZSB0cnkgYWdhaW4gYWZ0ZXIgc29tZXRpbWUuJ1xyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgfVxyXG4gIHNob3dDb25maWdTZXR1cCgpIHtcclxuICAgIHRoaXMuc2hvd19kYXRhID0gJ2NvbmZpZ190cmFja2VyJztcclxuICB9XHJcblxyXG4gIGxvYWRUYWJsZUNvbHVtbihzZWxlY3RlZEhlYWRlciwgaGVhZGVyVmFsdWVzKSB7XHJcbiAgICB0aGlzLmNvbmZpZ1RyYWNrZXJIZWFkZXIgPSBbXTtcclxuICAgIHZhciB0ZW1wQXJyYXkgPSBbXHJcbiAgICAgIHtcclxuICAgICAgICBuYW1lOiAnSGlzdG9yeScsXHJcbiAgICAgICAgdmFsdWU6ICdoaXN0b3J5JyxcclxuICAgICAgICBpc0FjdGl2ZTogdHJ1ZVxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgbmFtZTogJ1N0YXR1cycsXHJcbiAgICAgICAgdmFsdWU6ICdzdGF0dXMnLFxyXG4gICAgICAgIGlzQWN0aXZlOiB0cnVlXHJcbiAgICAgIH1cclxuICAgIF07XHJcbiAgICBpZiAoaGVhZGVyVmFsdWVzKSB7XHJcbiAgICAgIF8uZm9yRWFjaChoZWFkZXJWYWx1ZXMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgdmFyIHRlbXBPYmogPSB7XHJcbiAgICAgICAgICBuYW1lOiBpdGVtLnVpQXR0ck5hbWUsXHJcbiAgICAgICAgICB2YWx1ZTogaXRlbS5kYkF0dHJOYW1lLFxyXG4gICAgICAgICAgaXNJZGVudGlmaWVyOiBpdGVtLmlzSWRlbnRpZmllcixcclxuICAgICAgICAgIGlzQWN0aXZlOiBmYWxzZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgaWYgKGl0ZW0udHlwZSkge1xyXG4gICAgICAgICAgdGVtcE9ialsndHlwZSddID0gaXRlbS50eXBlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICBpdGVtLmRiQXR0ck5hbWUgPT09ICdTVEFSVF9EQVRFJyB8fFxyXG4gICAgICAgICAgaXRlbS5kYkF0dHJOYW1lID09PSAnQ1JFQVRFRF9CWV9VU0VSX0lEJyB8fFxyXG4gICAgICAgICAgaXRlbS5kYkF0dHJOYW1lID09PSAnQ1JFQVRJT05fREFURScgfHxcclxuICAgICAgICAgIGl0ZW0uZGJBdHRyTmFtZSA9PT0gJ0xBU1RfVVBEQVRFRF9CWV9VU0VSJyB8fFxyXG4gICAgICAgICAgaXRlbS5kYkF0dHJOYW1lID09PSAnTkFNRSdcclxuICAgICAgICApIHtcclxuICAgICAgICAgIHRlbXBPYmpbJ2lzQWN0aXZlJ10gPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBcdHRlbXBPYmpbJ2lzQWN0aXZlJ10gPSBmYWxzZTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgIH0pO1xyXG4gICAgICB0aGlzLnRhYmxlQ29uZmlnID0gdGVtcEFycmF5O1xyXG4gICAgfVxyXG4gICAgZm9yIChsZXQgY29sdW1ucyBvZiB0aGlzLnRhYmxlQ29uZmlnKSB7XHJcbiAgICAgIGlmIChjb2x1bW5zLmlzQWN0aXZlKSB7XHJcbiAgICAgICAgdGhpcy5jb25maWdUcmFja2VySGVhZGVyLnB1c2goY29sdW1ucy5uYW1lKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLm5hbWUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB1cGRhdGVUYWJsZShldmVudCkge1xyXG4gICAgbGV0IHNlbGVjdGVkSGVhZGVyID0gdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyO1xyXG4gICAgdGhpcy50YWJsZUNvbmZpZy5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmIChzZWxlY3RlZEhlYWRlci5pbmRleE9mKGl0ZW0ubmFtZSkgPj0gMCkge1xyXG4gICAgICAgIGl0ZW0uaXNBY3RpdmUgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGl0ZW0uaXNBY3RpdmUgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnY29udHJvbCcpO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2NvbnRyb2wnLCBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlQ29uZmlnKSk7XHJcbiAgICB0aGlzLmxvYWRUYWJsZUNvbHVtbih0aGlzLnNlbGVjdGVkSGVhZGVySWQsIG51bGwpO1xyXG4gIH1cclxuXHJcbiAgb25TZWxlY3RNb2R1bGVSTShldmVudCwgaXNSZWRpcmVjdCkge1xyXG4gICAgZXZlbnQgPSBldmVudCAmJiBldmVudC52YWx1ZSA/IGV2ZW50LnZhbHVlIDogZXZlbnQ7XHJcbiAgICB2YXIgdGVtcEFycmF5ID0gW107XHJcbiAgICBsZXQgdGVtcE9iaiA9IHRoaXMub2JqZWN0X25hbWVfUk1fYnlNb2R1bGU7XHJcbiAgICBfLmZvckVhY2goZXZlbnQsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmICh0ZW1wT2JqW1N0cmluZyhpdGVtKV0pIHtcclxuICAgICAgICBpZiAodGVtcEFycmF5ICYmIHRlbXBBcnJheS5sZW5ndGgpIHtcclxuICAgICAgICAgIHRlbXBBcnJheSA9IF8uY29uY2F0KHRlbXBBcnJheSwgdGVtcE9ialtTdHJpbmcoaXRlbSldKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGVtcEFycmF5ID0gdGVtcE9ialtTdHJpbmcoaXRlbSldO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICB0aGlzLnNlbGVjdF9tb2R1bGVzX1JNID0gZXZlbnQ7XHJcbiAgICB0aGlzLm9iamVjdE5hbWVfUk0gPSB0ZW1wQXJyYXk7XHJcbiAgICB0aGlzLnN0YXR1c0ZpbHRlciA9IFtcclxuICAgICAge1xyXG4gICAgICAgIHZhbHVlOiAnY2hhbmdlZCcsXHJcbiAgICAgICAgbmFtZTogJ0NoYW5nZWQnXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICB2YWx1ZTogJ25vdENoYW5nZWQnLFxyXG4gICAgICAgIG5hbWU6ICdOb3QgQ2hhbmdlZCdcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIHZhbHVlOiAnY3JlYXRlZCcsXHJcbiAgICAgICAgbmFtZTogJ0NyZWF0ZWQnXHJcbiAgICAgIH1cclxuICAgIF07XHJcbiAgICBpZiAoaXNSZWRpcmVjdCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkVGFiSGVhZGVyID0gdGVtcEFycmF5O1xyXG4gICAgICB0aGlzLnNlbGVjdGVkU3RhdHVzID0gdGhpcy5zdGF0dXNGaWx0ZXJbMF07XHJcbiAgICAgIC8vIHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXIucHVzaCh0ZW1wQXJyYXlbMF0pXHJcbiAgICAgIHRoaXMuY2hhbmdlVGFiKHsgaW5kZXg6IDAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICB1cGRhdGVUYWIoZXZlbnQsIGZyb21UYWIpIHtcclxuICAgIGNvbnNvbGUubG9nKCd1cGRhdGVUYWIgPj4+PiAnLCBldmVudCk7XHJcbiAgICBpZiAoZXZlbnQudmFsdWUpIHtcclxuICAgICAgLy8gdGhpcy5zZWxlY3RvYmpoZWFkZXIgPSBldmVudC52YWx1ZTtcclxuICAgICAgdGhpcy5zZWxlY3RlZEhlYWRlcklkID0gdGhpcy5zZWxlY3RlZFRhYkhlYWRlclswXS50eXBlO1xyXG4gICAgICAvLyB0aGlzLnNlbGVjdGVkX2RhdGEucHVzaCh0aGlzLmRTb3VyY2VbaV0ucXVlcnlUeXBlc1tqXS52YWx1ZSk7XHJcbiAgICB9IGVsc2UgaWYgKGZyb21UYWIgPT0gJ2Rhc2hib2FyZCcpIHtcclxuICAgICAgdmFyIHNfb2JqID0gW107XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXIgPSBldmVudDtcclxuICAgICAgdGhpcy5jaGFuZ2VUYWIoeyBpbmRleDogMCB9KTtcclxuICAgICAgLy8gdGhpcy5yZXBvcnRtYW5hZ2VtZW50T2JqZWN0bGlzdC5mb3JFYWNoKGVsZW0gPT57XHJcbiAgICAgIC8vIFx0bGV0IGluZGV4ID0gZXZlbnQuZmluZEluZGV4KFxyXG4gICAgICAvLyBcdFx0b2JqID0+IG9iai5uYW1lID09PSBlbGVtLm5hbWVcclxuICAgICAgLy8gXHQpO1xyXG4gICAgICAvLyBcdGlmKGluZGV4ID4gLTEpe1xyXG4gICAgICAvLyBcdFx0ZWxlbS5jaGVja2VkID0gdHJ1ZVxyXG4gICAgICAvLyBcdFx0c19vYmoucHVzaChlbGVtLm5hbWUpO1xyXG4gICAgICAvLyBcdH1cclxuXHJcbiAgICAgIC8vIH0pXHJcbiAgICAgIC8vIHRoaXMuc2VsZWN0b2JqaGVhZGVyID0gc19vYmo7XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKCd0aGlzLnNlbGVjdG9iamhlYWRlcicsdGhpcy5zZWxlY3RvYmpoZWFkZXIpXHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKFwidHlwZXMuLi5cIix0aGlzLnR5cGVzLFwicmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3QtLS0tLS0tLS1cIix0aGlzLnJlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0KVxyXG4gICAgfVxyXG4gICAgdGhpcy5nZXRVc2VyTmFtZUxpc3QoJycpO1xyXG4gIH1cclxuXHJcbiAgYXBwbHlGaWx0ZXIoKSB7XHJcbiAgICAvLyBpZih0aGlzLnNob3dfZGF0YSA9PSAncmVwb3J0X21hbmFnZW1lbnQnKVxyXG4gICAgdGhpcy5jaGFuZ2VUYWIobnVsbCk7XHJcbiAgfVxyXG5cclxuICByZXNldEZpbHRlcigpIHtcclxuICAgIHRoaXMuc2VsZWN0X21vZHVsZXNfUk0gPSBbXTtcclxuICAgIHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXIgPSBbXTtcclxuICAgIHRoaXMuc2VsZWN0ZWRVc2VyID0gJyc7XHJcbiAgICB0aGlzLnNlbGVjdGVkU3RhdHVzID0gJyc7XHJcbiAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIgPSBbXTtcclxuICAgIHRoaXMuZnJvbUlucHV0LnZhbHVlID0gJyc7XHJcbiAgICB0aGlzLnRvSW5wdXQudmFsdWUgPSAnJztcclxuICAgIHRoaXMuY29uZmlnRGF0YSA9IFtdO1xyXG4gICAgdGhpcy5jb25maWdUcmFja2VySGVhZGVyID0gW107XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VUYWIoZXZlbnQpIHtcclxuICAgIGlmIChldmVudCkge1xyXG4gICAgICBldmVudCA9IGV2ZW50LmluZGV4O1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSGVhZGVySWQgPSB0aGlzLnNlbGVjdGVkVGFiSGVhZGVyW2V2ZW50XS50eXBlO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSGVhZGVyTW9kdWxlbmFtZSA9IHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXJbZXZlbnRdLm1vZHVsZV9uYW1lO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSGVhZGVyT2JqZWN0TmFtZSA9IHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXJbZXZlbnRdLm5hbWU7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAvLyBpZighdGhpcy5zZWxlY3RlZEhlYWRlcklkKXtcclxuICAgICAgdGhpcy5zZWxlY3RlZEhlYWRlcklkID0gdGhpcy5zZWxlY3RlZFRhYkhlYWRlclswXS50eXBlO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkSGVhZGVyTW9kdWxlbmFtZSA9IHRoaXMuc2VsZWN0ZWRUYWJIZWFkZXJbMF0ubW9kdWxlX25hbWU7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRIZWFkZXJPYmplY3ROYW1lID0gdGhpcy5zZWxlY3RlZFRhYkhlYWRlclswXS5uYW1lO1xyXG4gICAgICAvLyB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZEhlYWRlcklkLCAnU0VMRUNURUQnKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRTdGF0dXMsICcuLi4uLi4uLi5TVEFUVVMnKTtcclxuXHJcbiAgICB2YXIgaGVhZGVyT3B0aW9uID0ge1xyXG4gICAgICB0eXBlOiB0aGlzLnNlbGVjdGVkSGVhZGVySWRcclxuICAgIH07XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnRhYmxlQ29uZmlnLCAnLS0tLXRhYmxlY29uZmlnJyk7XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZVxyXG4gICAgICAuZ2V0SGVhZGVyRGF0YShoZWFkZXJPcHRpb24pXHJcbiAgICAgIC5zdWJzY3JpYmUoKGhlYWRlckRhdGFzOiBhbnkpID0+IHtcclxuICAgICAgICB2YXIgaGVhZGVyRGF0YTtcclxuXHJcbiAgICAgICAgdGhpcy5jdXJyZW50SGVhZGVyRGF0YSA9IGhlYWRlckRhdGFzLmJvZHkucmVzcG9uc2U7XHJcbiAgICAgICAgdGhpcy5sb2FkVGFibGVDb2x1bW4oXHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkSGVhZGVySWQsXHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRIZWFkZXJEYXRhLnBhcmFtc1xyXG4gICAgICAgICk7XHJcbiAgICAgICAgdmFyIHNlbGVjdGVkQ29sdW1uID0gXy5maWx0ZXIodGhpcy50YWJsZUNvbmZpZywgWydpc0lkZW50aWZpZXInLCB0cnVlXSk7XHJcbiAgICAgICAgc2VsZWN0ZWRDb2x1bW4gPSBfLm1hcChzZWxlY3RlZENvbHVtbiwgJ25hbWUnKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZENvbHVtbiwgJy4uLi4uLicpO1xyXG4gICAgICAgIHRoaXMuZGF0ZUNvbHVtbnMgPSBfLmZpbHRlcih0aGlzLnRhYmxlQ29uZmlnLCBbJ3R5cGUnLCAnZGF0ZSddKTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmRhdGVDb2x1bW5zLCAnLi4uLi4uRGF0ZScpO1xyXG4gICAgICAgIGxldCBkYXRlQ29sdW1ucyA9IHRoaXMuZGF0ZUNvbHVtbnM7XHJcbiAgICAgICAgdGhpcy51bmlxdWVJRCA9IHNlbGVjdGVkQ29sdW1uO1xyXG4gICAgICAgIHZhciBvcHRpb25zID0ge1xyXG4gICAgICAgICAgbGltaXQ6IHRoaXMubGltaXQsXHJcbiAgICAgICAgICBvZmZzZXQ6IHRoaXMub2Zmc2V0LFxyXG4gICAgICAgICAgY29uZmlnVHJhY2tlcklkOiB0aGlzLmNvbmZpZ1RyYWNrZXJJRCxcclxuICAgICAgICAgIHR5cGU6IHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCxcclxuICAgICAgICAgIHVuaXF1ZUlEOiB0aGlzLnVuaXF1ZUlELFxyXG4gICAgICAgICAgc2VsZWN0ZWRVc2VyTmFtZTogdGhpcy5zZWxlY3RlZFVzZXIgPyB0aGlzLnNlbGVjdGVkVXNlciA6ICcnLFxyXG4gICAgICAgICAgc2VsZWN0ZWRTdGF0dXM6IHRoaXMuc2VsZWN0ZWRTdGF0dXMgPyB0aGlzLnNlbGVjdGVkU3RhdHVzLnZhbHVlIDogJycsXHJcbiAgICAgICAgICBmcm9tRGF0ZTogdGhpcy5mcm9tSW5wdXQudmFsdWUgPyB0aGlzLmZyb21JbnB1dC52YWx1ZSA6ICcnLFxyXG4gICAgICAgICAgdG9EYXRlOiB0aGlzLnRvSW5wdXQudmFsdWUgPyB0aGlzLnRvSW5wdXQudmFsdWUgOiAnJ1xyXG4gICAgICAgIH07XHJcbiAgICAgICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2VcclxuICAgICAgICAgIC5nZXRSYXdEYXRhKG9wdGlvbnMpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgLy8gdmFyIGRlbW9Db25maWcgPSBkZW1vRGF0YS5sYXN0TW9kaWZpZWREYXRhO1xyXG4gICAgICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmJvZHkgJiYgZGF0YS5ib2R5LnJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5jb25maWdEYXRhID0gZGF0YS5ib2R5LnJlc3BvbnNlLnByb2Nlc3NEYXRhUmVzcFxyXG4gICAgICAgICAgICAgICAgPyBkYXRhLmJvZHkucmVzcG9uc2UucHJvY2Vzc0RhdGFSZXNwXHJcbiAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgICAgIC8vIHRoaXMuaGlzdG9yeURhdGEgPSBkYXRhLmJvZHkucmVzcG9uc2UuaGlzdG9yeURhdGE7XHJcbiAgICAgICAgICAgICAgbGV0IHByb2Nlc3NBcnJheSA9IGRhdGEuYm9keS5yZXNwb25zZS5wcm9jZXNzRGF0YVJlc3BcclxuICAgICAgICAgICAgICAgID8gZGF0YS5ib2R5LnJlc3BvbnNlLnByb2Nlc3NEYXRhUmVzcFxyXG4gICAgICAgICAgICAgICAgOiBbXTtcclxuXHJcbiAgICAgICAgICAgICAgXy5mb3JFYWNoKHByb2Nlc3NBcnJheSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGl0ZW0sICctLS0tLUlURU0nKTtcclxuICAgICAgICAgICAgICAgIC8vIGxldCBpZCA9IChpdGVtICYmIGl0ZW1bU3RyaW5nKHNlbGVjdGVkQ29sdW1uWzBdLm5hbWUpXSkgPyBpdGVtW1N0cmluZyhzZWxlY3RlZENvbHVtblswXS5uYW1lKV1bXCJuYW1lXCJdIDogJyc7XHJcbiAgICAgICAgICAgICAgICAvLyBsZXQgdGVtcE9iaiA9IGRhdGEuYm9keS5yZXNwb25zZS5oaXN0b3J5RGF0YTtcclxuICAgICAgICAgICAgICAgIGl0ZW1bJ2lzQ2hhbmdlZCddID1cclxuICAgICAgICAgICAgICAgICAgaXRlbSAmJiBpdGVtLmhpc3RvcnlEYXRhICYmIGl0ZW0uaGlzdG9yeURhdGEubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGl0ZW1bJ2lzQWRkZWQnXSA9XHJcbiAgICAgICAgICAgICAgICAgIGl0ZW0gJiYgaXRlbS5oaXN0b3J5RGF0YSAmJiBpdGVtLmhpc3RvcnlEYXRhLmxlbmd0aFxyXG4gICAgICAgICAgICAgICAgICAgID8gZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICA6IGl0ZW1bJ2lzQWRkZWQnXTtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRlQ29sdW1ucyAmJiBkYXRlQ29sdW1ucy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgXy5mb3JFYWNoKGRhdGVDb2x1bW5zLCBmdW5jdGlvbiAoZGF0ZUl0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZGF0ZVN0cmluZyA9IGl0ZW1bU3RyaW5nKGRhdGVJdGVtLm5hbWUpXVsnbmFtZSddO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBzaG93RGF0ZSA9IG5ldyBEYXRlKGRhdGVTdHJpbmcpO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBmb3JtYXREYXRlID0gbW9tZW50KHNob3dEYXRlKS5mb3JtYXQoJ2xsbCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW1bU3RyaW5nKGRhdGVJdGVtLm5hbWUpXVsnbmFtZSddID0gZm9ybWF0RGF0ZTtcclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgdGhpcy5sZW5ndGggPSBkYXRhLmJvZHkucmVzcG9uc2UudG90YWw7XHJcbiAgICAgICAgICAgICAgdGhpcy5jb25maWdEYXRhID1cclxuICAgICAgICAgICAgICAgIHByb2Nlc3NBcnJheSAmJiBwcm9jZXNzQXJyYXkubGVuZ3RoID8gcHJvY2Vzc0FycmF5IDogW107XHJcbiAgICAgICAgICAgICAgdGhpcy5jb25maWdEYXRhLnBhZ2luYXRvciA9IHRoaXMucmVwb3J0cGFnaW5hdG9yO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuY29uZmlnRGF0YSwgJy4uLi4uLi5Db25maWdEQVRBQUFBQScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG4gIHNlbGVjdGVkU3RhdHVzOiBhbnk7XHJcbiAgc2VsZWN0ZWRVc2VyOiBhbnk7XHJcbiAgY2hhbmdlUGFnZShldmVudCkge1xyXG4gICAgaWYgKGV2ZW50LnBhZ2VTaXplICE9IHRoaXMubGltaXQpIHtcclxuICAgICAgdGhpcy5saW1pdCA9IGV2ZW50LnBhZ2VTaXplO1xyXG4gICAgICB0aGlzLm9mZnNldCA9IDA7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm9mZnNldCA9IGV2ZW50LnBhZ2VJbmRleCAqIHRoaXMubGltaXQ7XHJcbiAgICB9XHJcbiAgICB0aGlzLmNoYW5nZVRhYihudWxsKTtcclxuICB9XHJcbiAgc2V0U3RlcChyb3dzKSB7XHJcbiAgICAvLyB0aGlzLnN0ZXAgPSBpbmRleDtcclxuXHJcbiAgICB2YXIgc2VsZWN0ZWRDb2x1bW4gPSBfLmZpbHRlcih0aGlzLnRhYmxlQ29uZmlnLCBbJ2lzSWRlbnRpZmllcicsIHRydWVdKTtcclxuICAgIHZhciBxdWVyeVBhcmFtcyA9IHtcclxuICAgICAgLy8gdHlwZSA6IHRoaXMuc2VsZWN0ZWRIZWFkZXJJZCxcclxuICAgICAgLy8gdW5pcXVlSUQgOiBzZWxlY3RlZENvbHVtblswXS5uYW1lLFxyXG4gICAgICBzZWxlY3RJZDogcm93c1tzZWxlY3RlZENvbHVtblswXS5uYW1lXVsnbmFtZSddXHJcbiAgICB9O1xyXG4gICAgdGhpcy5nZXRDb25maWdIaXN0b3J5KHF1ZXJ5UGFyYW1zKTtcclxuICB9XHJcblxyXG4gIGdldENvbmZpZ0hpc3Rvcnkob3B0aW9ucykge1xyXG4gICAgY29uc29sZS5sb2codGhpcy5oaXN0b3J5RGF0YSwgJy4uLi50aGlzLmhpc3RvcnlEYXRhJyk7XHJcbiAgICB0aGlzLmhpc3RvcnlDb25maWdEYXRhID0gdGhpcy5oaXN0b3J5RGF0YVtvcHRpb25zLnNlbGVjdElkXTtcclxuICAgIC8vIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldEhpc3RvcnlEYXRhKG9wdGlvbnMpLnN1YnNjcmliZSgoZGF0YUhpc3Rvcnk6IGFueSkgPT4ge1xyXG4gICAgLy8gXHRpZihkYXRhSGlzdG9yeSAmJiBkYXRhSGlzdG9yeS5ib2R5ICYmIGRhdGFIaXN0b3J5LmJvZHkucmVzcG9uc2Upe1xyXG4gICAgLy8gXHRcdHRoaXMuaGlzdG9yeUNvbmZpZ0RhdGEgPSBkYXRhSGlzdG9yeS5ib2R5LnJlc3BvbnNlO1xyXG4gICAgLy8gXHR9XHJcbiAgICAvLyB9KTtcclxuICB9XHJcblxyXG4gIHNob3dlZGl0KCkge1xyXG4gICAgLy8gYWxlcnQoJ2pidmMnKVxyXG4gICAgdGhpcy5tb2RlID0gJ2VkaXQnO1xyXG5cclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldEFsbE1vZHVsZXMoKS5zdWJzY3JpYmUoKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICB0aGlzLm1vZHVsZXMgPSBbXTtcclxuICAgICAgdGhpcy50eXBlcyA9IFtdO1xyXG4gICAgICB0aGlzLnJlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0ID0gW107XHJcbiAgICAgIHRoaXMudHlwZXNzcyA9IFtdO1xyXG4gICAgICB2YXIgcmVzID0gZGF0YS5ib2R5LnJlc3BvbnNlLk1vZHVsZXM7XHJcbiAgICAgIHRoaXMubW9kdWxlcyA9IGRhdGEuYm9keS5yZXNwb25zZS5Nb2R1bGVzO1xyXG4gICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEgPSB0aGlzLmdldENvbmZpZ1RhYmxlO1xyXG4gICAgICBpZiAodGhpcy5zZWxlY3RlZF9tb2R1bGVzLmxlbmd0aCAhPSAwKSB7XHJcbiAgICAgICAgdGhpcy5nZXRBbGxPYmplY3RzTmFtZUxpc3QoZnVuY3Rpb24gKCkge30pO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIHNob3dIaXN0b3J5KHR5cGUsIHJvd3MpIHtcclxuICAgIGNvbnNvbGUubG9nKCd0eXBlLS0tLS0tLS0tJywgdHlwZSk7XHJcbiAgICBpZiAodHlwZSA9PSAndHJ1ZScpIHtcclxuICAgICAgdGhpcy5leHBhbmRFbGVtZW50ID0gdHJ1ZTtcclxuICAgIH0gZWxzZSBpZiAodHlwZSA9PSAnZmFsc2UnKSB7XHJcbiAgICAgIHRoaXMuZXhwYW5kRWxlbWVudCA9IGZhbHNlO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coJ3RoaXMuZXhwYW5kRWxlbWVudC0tLS0tLS0tLScsIHRoaXMuZXhwYW5kRWxlbWVudCk7XHJcblxyXG4gICAgdmFyIHNlbGVjdGVkQ29sdW1uID0gXy5maWx0ZXIodGhpcy50YWJsZUNvbmZpZywgWydpc0lkZW50aWZpZXInLCB0cnVlXSk7XHJcbiAgICB2YXIgcXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIC8vIHR5cGUgOiB0aGlzLnNlbGVjdGVkSGVhZGVySWQsXHJcbiAgICAgIC8vIHVuaXF1ZUlEIDogc2VsZWN0ZWRDb2x1bW5bMF0ubmFtZSxcclxuICAgICAgc2VsZWN0SWQ6IHJvd3Nbc2VsZWN0ZWRDb2x1bW5bMF0ubmFtZV1bJ25hbWUnXVxyXG4gICAgfTtcclxuICAgIHRoaXMuZ2V0Q29uZmlnSGlzdG9yeShxdWVyeVBhcmFtcyk7XHJcbiAgfVxyXG5cclxuICBnZXRBbGxPYmplY3RzTmFtZUxpc3QoY2FsbGJhY2spIHtcclxuICAgIHZhciBvcHRpb25zID0ge1xyXG4gICAgICBtb2R1bGU6IHRoaXMuc2VsZWN0ZWRfbW9kdWxlc1xyXG4gICAgfTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldE9iamVjdHMob3B0aW9ucykuc3Vic2NyaWJlKChkYXRhOiBhbnkpID0+IHtcclxuICAgICAgdmFyIHJlcyA9IGRhdGEuYm9keS5yZXNwb25zZS5Nb2R1bGVzO1xyXG4gICAgICByZXMuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICAgIHZhciBvYmogPSB7fTtcclxuICAgICAgICBvYmpbJ25hbWUnXSA9IGl0ZW0ub2JqZWN0TmFtZVswXS5uYW1lO1xyXG4gICAgICAgIG9ialsndmFsdWUnXSA9IGl0ZW0ub2JqZWN0TmFtZVswXS5uYW1lO1xyXG4gICAgICAgIG9ialsndHlwZSddID0gaXRlbS5vYmplY3ROYW1lWzBdLnR5cGU7XHJcbiAgICAgICAgb2JqWydtb2R1bGVfbmFtZSddID0gaXRlbS5vYmplY3ROYW1lWzBdLm1vZHVsZV9uYW1lO1xyXG4gICAgICAgIGxldCBpbmRleCA9IHRoaXMuZ2V0Q29uZmlnVGFibGUuZmluZEluZGV4KFxyXG4gICAgICAgICAgKG9iaikgPT4gb2JqLm5hbWUgPT09IGl0ZW0ub2JqZWN0TmFtZVswXS5uYW1lXHJcbiAgICAgICAgKTtcclxuICAgICAgICBpZiAoaW5kZXggPiAtMSkge1xyXG4gICAgICAgICAgb2JqWydjaGVja2VkJ10gPSB0cnVlO1xyXG4gICAgICAgICAgb2JqWydjb250cm9sX25hbWUnXSA9IHRoaXMuZ2V0Q29uZmlnVGFibGVbaW5kZXhdLmNvbnRyb2xfbmFtZTtcclxuICAgICAgICAgIG9ialsnY29udHJvbF90eXBlJ10gPSB0aGlzLmdldENvbmZpZ1RhYmxlW2luZGV4XS5jb250cm9sX3R5cGU7XHJcbiAgICAgICAgICBvYmpbJ3Jpc2tSYW5rJ10gPSB0aGlzLmdldENvbmZpZ1RhYmxlW2luZGV4XS5yaXNrUmFuaztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgb2JqWydjaGVja2VkJ10gPSBmYWxzZTtcclxuICAgICAgICAgIG9ialsnY29udHJvbF9uYW1lJ10gPSAnJztcclxuICAgICAgICAgIG9ialsnY29udHJvbF90eXBlJ10gPSAnJztcclxuICAgICAgICAgIG9ialsncmlza1JhbmsnXSA9ICcnO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnR5cGVzLnB1c2gob2JqKTtcclxuICAgICAgICB0aGlzLnJlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0LnB1c2gob2JqKTtcclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICdyZXBvcnRtYW5hZ2VtZW50T2JqZWN0bGlzdC0tLS0tLS0tLS0tPj4uLi4nLFxyXG4gICAgICAgIHRoaXMucmVwb3J0bWFuYWdlbWVudE9iamVjdGxpc3RcclxuICAgICAgKTtcclxuICAgICAgdGhpcy5vYmplY3RfbGlzdCA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UodGhpcy50eXBlcyk7XHJcbiAgICAgIHRoaXMub2JqZWN0X2xpc3QucGFnaW5hdG9yID0gdGhpcy5wYWdpbmF0b3I7XHJcbiAgICAgIHRoaXMub2JqZWN0X2xpc3RfbGVuZ3RoID0gdGhpcy50eXBlcy5sZW5ndGg7XHJcbiAgICB9KTtcclxuICAgIGNhbGxiYWNrKCk7XHJcbiAgfVxyXG5cclxuICBjaGVja0V4aXRzKHNlbGVjdGVkUm93KTogYm9vbGVhbiB7XHJcbiAgICBpZiAoc2VsZWN0ZWRSb3cgPT0gdHJ1ZSkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uU2VhcmNoTmFtZShmaWx0ZXJWYWx1ZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnNlbGVjdGVkX2RhdGEuZmlsdGVyID0gZmlsdGVyVmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICB0aGlzLnNlbGVjdGVkX2RhdGEucGFnaW5hdG9yID0gdGhpcy5zcGFnaW5hdG9yO1xyXG4gIH1cclxuXHJcbiAgb25TZWFyY2hPYmplY3ROYW1lKGZpbHRlclZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMub2JqZWN0X2xpc3QuZmlsdGVyID0gZmlsdGVyVmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICB0aGlzLm9iamVjdF9saXN0LnBhZ2luYXRvciA9IHRoaXMucGFnaW5hdG9yO1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdF9sZW5ndGggPSB0aGlzLnR5cGVzLmxlbmd0aDtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0TW9kdWxlKGV2ZW50KSB7XHJcbiAgICBsZXQga2V5QnlPYmplY3ROYW1lcyA9IF8uZ3JvdXBCeSh0aGlzLmdldENvbmZpZ1RhYmxlLCAnbW9kdWxlX25hbWUnKTtcclxuICAgIF8uZm9yRWFjaChldmVudC52YWx1ZSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKHRoaXMub25TZWxlY3RlZE1vZHVsZU9iamVjdHMgJiYgdGhpcy5vblNlbGVjdGVkTW9kdWxlT2JqZWN0cy5sZW5ndGgpIHtcclxuICAgICAgICB0aGlzLm9uU2VsZWN0ZWRNb2R1bGVPYmplY3RzID0gXy5jb25jYXQoXHJcbiAgICAgICAgICB0aGlzLm9uU2VsZWN0ZWRNb2R1bGVPYmplY3RzLFxyXG4gICAgICAgICAga2V5QnlPYmplY3ROYW1lc1tpdGVtXVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5vblNlbGVjdGVkTW9kdWxlT2JqZWN0cyA9IGtleUJ5T2JqZWN0TmFtZXNbaXRlbV07XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2VEYXRhc291cmNlSWQoaWQsIHR5cGUpIHtcclxuICAgIC8vdGhpcy5uZ3hTZXJ2aWNlLnN0YXJ0KCk7XHJcbiAgICB2YXIgbW9kdWxlcyA9IFtdO1xyXG4gICAgaWYgKHR5cGUgPT0gJ2Rhc2hib2FyZCcpIHtcclxuICAgICAgdGhpcy5zZWxlY3RfbW9kdWxlcyA9IGlkO1xyXG4gICAgICBtb2R1bGVzID0gaWQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBtb2R1bGVzID0gaWQudmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy50eXBlcyA9IFtdO1xyXG5cclxuICAgIHRoaXMuc2VsZWN0ZWRfb2JqZWN0cyA9IFtdO1xyXG5cclxuICAgIHZhciBvcHRpb25zID0ge1xyXG4gICAgICBtb2R1bGU6IG1vZHVsZXNcclxuICAgIH07XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5nZXRPYmplY3RzKG9wdGlvbnMpLnN1YnNjcmliZSgoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgIHZhciByZXMgPSBkYXRhLmJvZHkucmVzcG9uc2UuTW9kdWxlcztcclxuICAgICAgcmVzLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICB2YXIgb2JqID0ge307XHJcblxyXG4gICAgICAgIG9ialsnbmFtZSddID0gaXRlbS5vYmplY3ROYW1lWzBdLm5hbWU7XHJcbiAgICAgICAgb2JqWyd2YWx1ZSddID0gaXRlbS5vYmplY3ROYW1lWzBdLm5hbWU7XHJcbiAgICAgICAgb2JqWyd0eXBlJ10gPSBpdGVtLm9iamVjdE5hbWVbMF0udHlwZTtcclxuICAgICAgICBvYmpbJ21vZHVsZV9uYW1lJ10gPSBpdGVtLm9iamVjdE5hbWVbMF0ubW9kdWxlX25hbWU7XHJcbiAgICAgICAgaWYgKHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICBsZXQgaW5kZXggPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEuZmluZEluZGV4KFxyXG4gICAgICAgICAgICAob2JqKSA9PiBvYmoubmFtZSA9PT0gaXRlbS5vYmplY3ROYW1lWzBdLm5hbWVcclxuICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgaWYgKGluZGV4ID4gLTEpIHtcclxuICAgICAgICAgICAgb2JqWydjaGVja2VkJ10gPSB0cnVlO1xyXG4gICAgICAgICAgICBvYmpbJ2NvbnRyb2xfbmFtZSddID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhW2luZGV4XS5jb250cm9sX25hbWU7XHJcbiAgICAgICAgICAgIG9ialsnY29udHJvbF90eXBlJ10gPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFbaW5kZXhdLmNvbnRyb2xfdHlwZTtcclxuICAgICAgICAgICAgb2JqWydyaXNrUmFuayddID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhW2luZGV4XS5yaXNrUmFuaztcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIG9ialsnY2hlY2tlZCddID0gZmFsc2U7XHJcbiAgICAgICAgICAgIG9ialsnY29udHJvbF9uYW1lJ10gPSAnJztcclxuICAgICAgICAgICAgb2JqWydjb250cm9sX3R5cGUnXSA9ICcnO1xyXG4gICAgICAgICAgICBvYmpbJ3Jpc2tSYW5rJ10gPSAnJztcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgb2JqWydjaGVja2VkJ10gPSBmYWxzZTtcclxuICAgICAgICAgIG9ialsnY29udHJvbF9uYW1lJ10gPSAnJztcclxuICAgICAgICAgIG9ialsnY29udHJvbF90eXBlJ10gPSAnJztcclxuICAgICAgICAgIG9ialsncmlza1JhbmsnXSA9ICcnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gaWYob2JqW1wiY2hlY2tlZFwiXSA9PSB0cnVlKXtcclxuICAgICAgICAvLyBcdHRoaXMuaXNDaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgICAgLy8gZWxzZSBpZihvYmpbXCJjaGVja2VkXCJdID09IGZhbHNlKXtcclxuICAgICAgICAvLyBcdHRoaXMuaXNDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgLy8gfVxyXG5cclxuICAgICAgICB0aGlzLnR5cGVzLnB1c2gob2JqKTtcclxuICAgICAgICB0aGlzLnJlcG9ydG1hbmFnZW1lbnRPYmplY3RsaXN0LnB1c2gob2JqKTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMub2JqZWN0X2xpc3QgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMudHlwZXMpO1xyXG4gICAgICB0aGlzLm9iamVjdF9saXN0LnBhZ2luYXRvciA9IHRoaXMucGFnaW5hdG9yO1xyXG4gICAgICB0aGlzLm9iamVjdF9saXN0X2xlbmd0aCA9IHRoaXMudHlwZXMubGVuZ3RoO1xyXG4gICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEgPSBfLmZpbHRlcih0aGlzLnR5cGVzLCBbJ2NoZWNrZWQnLCB0cnVlXSk7XHJcbiAgICAgIC8vdGhpcy5uZ3hTZXJ2aWNlLnN0b3AoKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2VUeXBlKGVsZW1lbnQsIGV2ZW50LCB0eXBlKSB7XHJcbiAgICBpZiAodHlwZSA9PSAnYWxsJykge1xyXG4gICAgICBpZiAoZXZlbnQuY2hlY2tlZCA9PSB0cnVlKSB7XHJcbiAgICAgICAgdGhpcy5pc0NoZWNrZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMudHlwZXMuZm9yRWFjaCgoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgaXRlbS5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICAgIHZhciBvYmogPSB7fTtcclxuICAgICAgICAgIG9ialsnbmFtZSddID0gaXRlbS5uYW1lO1xyXG4gICAgICAgICAgb2JqWyd2YWx1ZSddID0gaXRlbS52YWx1ZTtcclxuICAgICAgICAgIG9ialsndHlwZSddID0gaXRlbS50eXBlO1xyXG4gICAgICAgICAgb2JqWydtb2R1bGVfbmFtZSddID0gaXRlbS5tb2R1bGVfbmFtZTtcclxuICAgICAgICAgIG9ialsnY29udHJvbF9uYW1lJ10gPSBpdGVtLmNvbnRyb2xfbmFtZTtcclxuICAgICAgICAgIG9ialsnY29udHJvbF90eXBlJ10gPSBpdGVtLmNvbnRyb2xfdHlwZTtcclxuICAgICAgICAgIG9ialsncmlza1JhbmsnXSA9IGl0ZW0ucmlza1Jhbms7XHJcbiAgICAgICAgICBvYmpbJ2NoZWNrZWQnXSA9IGl0ZW0uY2hlY2tlZDtcclxuICAgICAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5wdXNoKG9iaik7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSBpZiAoZXZlbnQuY2hlY2tlZCA9PSBmYWxzZSkge1xyXG4gICAgICAgIHRoaXMuaXNDaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy50eXBlcy5mb3JFYWNoKChpdGVtKSA9PiB7XHJcbiAgICAgICAgICBpdGVtLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEgPSBbXTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIGlmICh0eXBlID09ICdzaW5nbGUnKSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEuZmluZEluZGV4KFxyXG4gICAgICAgICAgKHRlbXApID0+IHRlbXAubmFtZSA9PT0gZWxlbWVudC5uYW1lXHJcbiAgICAgICAgKSA8IDBcclxuICAgICAgKSB7XHJcbiAgICAgICAgaWYgKGV2ZW50LmNoZWNrZWQgPT0gdHJ1ZSkge1xyXG4gICAgICAgICAgdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLnB1c2goZWxlbWVudCk7XHJcbiAgICAgICAgICBsZXQgaWluZGV4ID0gdGhpcy50eXBlcy5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIChvYmp0KSA9PiBvYmp0Lm5hbWUgPT09IGVsZW1lbnQubmFtZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMudHlwZXNbaWluZGV4XS5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKGV2ZW50LmNoZWNrZWQgPT0gZmFsc2UpIHtcclxuICAgICAgICAgIGxldCBpbmRleCA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoXHJcbiAgICAgICAgICAgIChvYmopID0+IG9iai5uYW1lID09PSBlbGVtZW50Lm5hbWVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICAgIGxldCBpaW5kZXggPSB0aGlzLnR5cGVzLmZpbmRJbmRleChcclxuICAgICAgICAgICAgKG9ianQpID0+IG9ianQubmFtZSA9PT0gZWxlbWVudC5uYW1lXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy50eXBlc1tpaW5kZXhdLmNoZWNrZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIENvbmZpcm1EYXRhKCkge1xyXG4gICAgY29uc29sZS5sb2coJ0NvbmZpcm1EYXRhLS0tLS0tLS0tLS0+Pj4nLCB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEpO1xyXG4gICAgY29uc29sZS5sb2coJ3NlbGVjdGVkTW9kdWxlcy0tLS0tLS0tLS0tPj4+JywgdGhpcy5zZWxlY3RlZF9tb2R1bGVzKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5mb3JFYWNoKChpdGVtLCBpbmRleHgpID0+IHtcclxuICAgICAgbGV0IGluZGV4ID0gdGhpcy5zZWxlY3RlZF9tb2R1bGVzLmZpbmRJbmRleChcclxuICAgICAgICAob2JqKSA9PiBvYmogPT09IGl0ZW0ubW9kdWxlX25hbWVcclxuICAgICAgKTtcclxuXHJcbiAgICAgIGlmIChpbmRleCA+IC0xKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ0l0ZW0tLS0tLS0tLS0tLScsIGl0ZW0ubW9kdWxlX25hbWUpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdJdGVtRUxTRS0tLS0tLS0tLS0tJywgaW5kZXgpO1xyXG4gICAgICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5zcGxpY2UoaW5kZXh4LCAxKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBjb25zb2xlLmxvZygnQ29uZmlybURhdGFGaW5hbC0tLS0tLS0tLS0tPj4+JywgdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfdGFibGUgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKFxyXG4gICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFcclxuICAgICk7XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX3RhYmxlLnBhZ2luYXRvciA9IHRoaXMuZnBhZ2luYXRvcjtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfbGVuZ3RoID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhLmxlbmd0aDtcclxuICB9XHJcblxyXG4gIFByZXZpb3VzRGF0YSgpIHtcclxuICAgIHRoaXMub2JqZWN0X2xpc3QgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMudHlwZXMpO1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdC5wYWdpbmF0b3IgPSB0aGlzLnBhZ2luYXRvcjtcclxuICAgIHRoaXMub2JqZWN0X2xpc3RfbGVuZ3RoID0gdGhpcy50eXBlcy5sZW5ndGg7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZU5hbWUoZXZlbnQsIG5hbWUpIHtcclxuICAgIGxldCBpaW5kZXggPSB0aGlzLnR5cGVzLmZpbmRJbmRleCgob2JqKSA9PiBvYmoubmFtZSA9PT0gbmFtZSk7XHJcbiAgICB0aGlzLnR5cGVzW2lpbmRleF0uY29udHJvbF9uYW1lID0gZXZlbnQ7XHJcblxyXG4gICAgbGV0IGkgPSB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEuZmluZEluZGV4KChvYmp0KSA9PiBvYmp0Lm5hbWUgPT09IG5hbWUpO1xyXG4gICAgdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhW2ldLmNvbnRyb2xfbmFtZSA9IGV2ZW50O1xyXG4gIH1cclxuXHJcbiAgb25DaGFuZ2VDb250cm9sVHlwZShldmVudCwgbmFtZSkge1xyXG4gICAgbGV0IGlpbmRleCA9IHRoaXMudHlwZXMuZmluZEluZGV4KChvYmopID0+IG9iai5uYW1lID09PSBuYW1lKTtcclxuICAgIHRoaXMudHlwZXNbaWluZGV4XS5jb250cm9sX3R5cGUgPSBldmVudDtcclxuXHJcbiAgICBsZXQgaSA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoKG9ianQpID0+IG9ianQubmFtZSA9PT0gbmFtZSk7XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFbaV0uY29udHJvbF90eXBlID0gZXZlbnQ7XHJcbiAgfVxyXG5cclxuICBvbkNoYW5nZVJpc2soZXZlbnQsIG5hbWUpIHtcclxuICAgIGxldCBpaW5kZXggPSB0aGlzLnR5cGVzLmZpbmRJbmRleCgob2JqKSA9PiBvYmoubmFtZSA9PT0gbmFtZSk7XHJcbiAgICB0aGlzLnR5cGVzW2lpbmRleF0ucmlza1JhbmsgPSBldmVudDtcclxuXHJcbiAgICBsZXQgaSA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoKG9ianQpID0+IG9ianQubmFtZSA9PT0gbmFtZSk7XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFbaV0ucmlza1JhbmsgPSBldmVudDtcclxuICB9XHJcblxyXG4gIGV4cG9ydHBkZigpIHtcclxuICAgIC8vdGhpcy5uZ3hTZXJ2aWNlLnN0YXJ0KCk7XHJcbiAgICB0aGlzLmxvZ2luVXNlciA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ2N1cnJlbnRMb2dpblVzZXInKSk7XHJcbiAgICBjb25zb2xlLmxvZygndGhpcy5sb2dpblVzZXIudXNlcm5hbWUgPj4+Pj4gJywgdGhpcy5sb2dpblVzZXJbJ3VzZXJuYW1lJ10pO1xyXG4gICAgdmFyIHF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICB0eXBlOiB0aGlzLnNlbGVjdGVkSGVhZGVySWQsXHJcbiAgICAgIHVzZXJuYW1lOiB0aGlzLmxvZ2luVXNlci51c2VybmFtZSxcclxuICAgICAgbW9kdWxlX25hbWU6IHRoaXMuc2VsZWN0ZWRIZWFkZXJNb2R1bGVuYW1lLFxyXG4gICAgICBvYmplY3RfbmFtZTogdGhpcy5zZWxlY3RlZEhlYWRlck9iamVjdE5hbWUsXHJcbiAgICAgIHNlbGVjdGVkVXNlck5hbWU6IHRoaXMuc2VsZWN0ZWRVc2VyID8gdGhpcy5zZWxlY3RlZFVzZXIgOiAnJyxcclxuICAgICAgc2VsZWN0ZWRTdGF0dXM6IHRoaXMuc2VsZWN0ZWRTdGF0dXMgPyB0aGlzLnNlbGVjdGVkU3RhdHVzLnZhbHVlIDogJycsXHJcbiAgICAgIGNvbmZpZ1RyYWNrZXJJZDogdGhpcy5jb25maWdUcmFja2VySUQsXHJcbiAgICAgIHVuaXF1ZUlEOiB0aGlzLnVuaXF1ZUlELFxyXG4gICAgICBmcm9tRGF0ZTogdGhpcy5mcm9tSW5wdXQudmFsdWUgPyB0aGlzLmZyb21JbnB1dC52YWx1ZSA6ICcnLFxyXG4gICAgICB0b0RhdGU6IHRoaXMudG9JbnB1dC52YWx1ZSA/IHRoaXMudG9JbnB1dC52YWx1ZSA6ICcnXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0U2NoZWR1bGVQREYocXVlcnlQYXJhbXMpLnN1YnNjcmliZShcclxuICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgIGNvbnN0IGJsb2IgPSBuZXcgQmxvYihbcmVzLmJvZHldLCB7IHR5cGU6ICdhcHBsaWNhdGlvbi9wZGYnIH0pO1xyXG4gICAgICAgIEZpbGVTYXZlci5zYXZlQXMoYmxvYiwgJ0NvbmZpZ1RyYWNrZXJSZXBvcnQucGRmJyk7XHJcbiAgICAgICAgLy90aGlzLm5neFNlcnZpY2Uuc3RvcCgpO1xyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAvL3RoaXMubmd4U2VydmljZS5zdG9wKCk7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ2Vycm9yOjo6JyArIEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBvblN1Ym1pdCh0eXBlKSB7XHJcbiAgICBpZiAodHlwZSA9PSAnZm9ybVN1Ym1pdCcpIHtcclxuICAgICAgdmFyIHNfbW9kdWxlID0gW107XHJcbiAgICAgIHZhciBvYmpfbmFtZSA9IFtdO1xyXG4gICAgICBzX21vZHVsZSA9IHRoaXMuQ3JlYXRlRm9ybS5jb250cm9sc1snbW9kdWxlJ10udmFsdWU7XHJcbiAgICAgIG9ial9uYW1lID0gdGhpcy5maW5hbF9zZWxlY3RlZF9kYXRhO1xyXG4gICAgfSBlbHNlIGlmICh0eXBlID09ICdmb3JtRGVsZXRlJykge1xyXG4gICAgICB2YXIgc19tb2R1bGUgPSBbXTtcclxuICAgICAgdmFyIG9ial9uYW1lID0gW107XHJcbiAgICAgIHNfbW9kdWxlID0gdGhpcy5zZWxlY3RlZF9tb2R1bGVzO1xyXG4gICAgICBvYmpfbmFtZSA9IHRoaXMuc2VsZWN0ZWRfZGF0YXNvdXJjZTtcclxuICAgIH1cclxuICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgIHZhciBvYmogPSB7XHJcbiAgICAgIG9iamVjdF9uYW1lOiBvYmpfbmFtZSxcclxuICAgICAgbW9kdWxlOiBzX21vZHVsZSxcclxuICAgICAgdXNlcklkOiB0aGlzLmRTb3VyY2VbMF0udXNlcklkLFxyXG4gICAgICBfaWQ6IHRoaXMuZFNvdXJjZVswXS5faWQsXHJcbiAgICAgIGNsaWVudElEOiB0aGlzLmRTb3VyY2VbMF0uY2xpZW50SUQsXHJcbiAgICAgIGZ1c2lvblVybDogdGhpcy5kU291cmNlWzBdLmZ1c2lvblVybCxcclxuICAgICAgdXNlck5hbWU6IHRoaXMuZFNvdXJjZVswXS51c2VyTmFtZSxcclxuICAgICAgcGFzc3dvcmQ6IHRoaXMuZFNvdXJjZVswXS5wYXNzd29yZCxcclxuICAgICAgc2NoZWR1bGVUeXBlOiB0aGlzLmRTb3VyY2VbMF0uc2NoZWR1bGVUeXBlLFxyXG4gICAgICBzY2hlZHVsZVR5cGVWYWx1ZTogdGhpcy5kU291cmNlWzBdLnNjaGVkdWxlVHlwZVZhbHVlLFxyXG4gICAgICByZXRhaW5EYXlzOiB0aGlzLmRTb3VyY2VbMF0ucmV0YWluRGF5cyxcclxuICAgICAgX192OiB0aGlzLmRTb3VyY2VbMF0uX192LFxyXG4gICAgICBxdWVyeVR5cGVzOiBfLm1hcChvYmpfbmFtZSwgJ3R5cGUnKSxcclxuICAgICAgZGVsZXRlZDogZmFsc2VcclxuICAgIH07XHJcbiAgICB0aGlzLlNldHVwQWRtaW5pc3RyYXRpb25TZXJ2aWNlLnVwZGF0ZUNvbmZpZ0RldGFpbHMoXHJcbiAgICAgIG9iaixcclxuICAgICAgdGhpcy5kU291cmNlWzBdLnVzZXJJZFxyXG4gICAgKS5zdWJzY3JpYmUoXHJcbiAgICAgIChyZXMpID0+IHtcclxuICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDEpIHtcclxuICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChyZXMuYm9keS5tZXRhLm1zZyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChyZXMuYm9keS5tZXRhLm1zZyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICB0aGlzLmVycm9yU3RhdHVzID0gZXJyb3IuZXJyb3IubWV0YS5zdGF0dXM7XHJcbiAgICAgICAgaWYgKHRoaXMuZXJyb3JTdGF0dXMgPT0gJzUwMCcgfHwgdGhpcy5lcnJvclN0YXR1cyA9PSAnNDAwJykge1xyXG4gICAgICAgICAgdGhpcy5lcnJvck1zZyA9IGVycm9yLmVycm9yLm1ldGEubXNnO1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyh0aGlzLmVycm9yTXNnKTtcclxuICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgfSwgMzAwMCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICApO1xyXG5cclxuICAgIHRoaXMuZ2V0YWxsY29uZmlncygpO1xyXG4gIH1cclxuXHJcbiAgcnVuRGF0YSgpIHtcclxuICAgIHZhciB1c2VySWQgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgndXNlcklkJyk7XHJcbiAgICB2YXIgcXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIGNsaWVudElEOiB0aGlzLmNsaWVudElELFxyXG4gICAgICB1c2VySWQ6IHVzZXJJZFxyXG4gICAgfTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlXHJcbiAgICAgIC5zdGFydERhdGFQdWxsKHF1ZXJ5UGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKCdSdW4vIFN5bmMgRGF0YSBTdGFydGVkIFN1Y2Nlc3NmdWxseScpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZVNlbGVjdGVkQ29uZmlnc0ZpbmFsKGVsZW1lbnQpIHtcclxuICAgIGxldCBpbmRleCA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5maW5kSW5kZXgoXHJcbiAgICAgIChvYmopID0+IG9iai5uYW1lID09PSBlbGVtZW50Lm5hbWVcclxuICAgICk7XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGEuc3BsaWNlKGluZGV4LCAxKTtcclxuXHJcbiAgICBsZXQgaWluZGV4ID0gdGhpcy50eXBlcy5maW5kSW5kZXgoKG9ianQpID0+IG9ianQubmFtZSA9PT0gZWxlbWVudC5uYW1lKTtcclxuICAgIHRoaXMudHlwZXNbaWluZGV4XS5jaGVja2VkID0gZmFsc2U7XHJcblxyXG4gICAgZGVsZXRlIHRoaXMuZmluYWxfc2VsZWN0ZWRfdGFibGVbZWxlbWVudC5uYW1lXTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfdGFibGUgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKFxyXG4gICAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2RhdGFcclxuICAgICk7XHJcbiAgICB0aGlzLm9iamVjdF9saXN0ID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLnR5cGVzKTtcclxuICAgIHRoaXMuZmluYWxfc2VsZWN0ZWRfdGFibGUucGFnaW5hdG9yID0gdGhpcy5mcGFnaW5hdG9yO1xyXG4gICAgdGhpcy5vYmplY3RfbGlzdC5wYWdpbmF0b3IgPSB0aGlzLnBhZ2luYXRvcjtcclxuICAgIHRoaXMub2JqZWN0X2xpc3RfbGVuZ3RoID0gdGhpcy50eXBlcy5sZW5ndGg7XHJcbiAgICB0aGlzLmZpbmFsX3NlbGVjdGVkX2xlbmd0aCA9IHRoaXMuZmluYWxfc2VsZWN0ZWRfZGF0YS5sZW5ndGg7XHJcbiAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKCdDb25maWcgUmVtb3ZlZCBTdWNjZXNzZnVsbHknKTtcclxuICB9XHJcblxyXG4gIGRlbGV0ZVNlbGVjdGVkQ29uZmlncyhlbGVtZW50KSB7XHJcbiAgICBsZXQgaW5kZXggPSB0aGlzLnNlbGVjdGVkX2RhdGFzb3VyY2UuZmluZEluZGV4KFxyXG4gICAgICAob2JqKSA9PiBvYmoubmFtZSA9PT0gZWxlbWVudC5uYW1lXHJcbiAgICApO1xyXG4gICAgdGhpcy5zZWxlY3RlZF9kYXRhc291cmNlLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICBkZWxldGUgdGhpcy5zZWxlY3RlZF9kYXRhc291cmNlW2VsZW1lbnQubmFtZV07XHJcbiAgICB0aGlzLnNlbGVjdGVkX2RhdGEgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMuc2VsZWN0ZWRfZGF0YXNvdXJjZSk7XHJcbiAgICB0aGlzLnNlbGVjdGVkX2RhdGEucGFnaW5hdG9yID0gdGhpcy5zcGFnaW5hdG9yO1xyXG4gICAgdGhpcy5vblN1Ym1pdCgnZm9ybURlbGV0ZScpO1xyXG4gICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZygnQ29uZmlnIFJlbW92ZWQgU3VjY2Vzc2Z1bGx5Jyk7XHJcbiAgfVxyXG5cclxuICBnZXRVc2VyTmFtZUxpc3QodGVtcHZhbHVlcykge1xyXG4gICAgbGV0IG9iamVjdExpc3RzID0gXy5tYXAodGhpcy5zZWxlY3RlZFRhYkhlYWRlciwgJ3R5cGUnKTtcclxuICAgIHZhciBxdWVyeVBhcmFtcyA9IHtcclxuICAgICAgc2VsZWN0ZWRPYmplY3Q6IG9iamVjdExpc3RzXHJcbiAgICB9O1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2VcclxuICAgICAgLmdldFVwZGF0ZWRVc2VyTGlzdChxdWVyeVBhcmFtcylcclxuICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgIGlmIChkYXRhICYmIGRhdGEuYm9keSAmJiBkYXRhLmJvZHlbJ3Jlc3BvbnNlJ10pIHtcclxuICAgICAgICAgIHRoaXMudXNlckxpc3QgPSBkYXRhLmJvZHlbJ3Jlc3BvbnNlJ10ubGVuZ3RoXHJcbiAgICAgICAgICAgID8gZGF0YS5ib2R5WydyZXNwb25zZSddXHJcbiAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICBpZiAodGVtcHZhbHVlcykge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkVXNlciA9IHRlbXB2YWx1ZXMubmFtZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgVG9nZ2xlKGlkLCB0ZW1wdmFsdWVzLCB0eXBlQ2hhcnQsIGNoYXJ0TnVtKSB7XHJcbiAgICBpZiAodGhpcy5jb25maWdRVFNldHVwRG9uZSAmJiB0aGlzLmNvbmZpZ1RyYWNrZXJTZXR1cERvbmUpIHtcclxuICAgICAgaWYgKGlkID09ICdjb25maWdfdHJhY2tlcicpIHtcclxuICAgICAgICB0aGlzLnNob3dfZGF0YSA9ICdjb25maWdfdHJhY2tlcic7XHJcbiAgICAgIH0gZWxzZSBpZiAoaWQgPT0gJ2Rhc2hib2FyZCcpIHtcclxuICAgICAgICB0aGlzLnNob3dfZGF0YSA9ICdkYXNoYm9hcmQnO1xyXG4gICAgICAgIHRoaXMuY2hhbmdlc2J5bW9kdWxlKCk7XHJcbiAgICAgICAgdGhpcy53ZWVrYnl1c2VyQ2hhcnQoKTtcclxuICAgICAgICB0aGlzLm1vbnRoYnl1c2VyQ2hhcnQoKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB0aGlzLnNob3dfZGF0YSA9ICdyZXBvcnRfbWFuYWdlbWVudCc7XHJcbiAgICAgICAgaWYgKHRlbXB2YWx1ZXMgJiYgdGVtcHZhbHVlcyAhPSAnJykge1xyXG4gICAgICAgICAgdmFyIG1vZHVsZU5hbWUgPSBbXTtcclxuICAgICAgICAgIGlmICh0eXBlQ2hhcnQgPT09ICdtb2R1bGUnKSB7XHJcbiAgICAgICAgICAgIG1vZHVsZU5hbWUucHVzaCh0ZW1wdmFsdWVzLm5hbWUpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbW9kdWxlTmFtZSA9IHRoaXMubW9kdWxlX1JNO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5vbkNoYW5nZURhdGFzb3VyY2VJZChtb2R1bGVOYW1lLCAnZGFzaGJvYXJkJyk7XHJcbiAgICAgICAgICB0aGlzLm9uU2VsZWN0TW9kdWxlUk0obW9kdWxlTmFtZSwgJ3JlZGlyZWN0Jyk7XHJcbiAgICAgICAgICB0aGlzLmdldEFsbE9iamVjdHNOYW1lTGlzdChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKCdHRVQnKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgaWYgKHR5cGVDaGFydCA9PT0gJ3VzZXInKSB7XHJcbiAgICAgICAgICAgIGlmIChjaGFydE51bSA9PSAxKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5nZXRVc2VyTmFtZUxpc3QodGVtcHZhbHVlcyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoY2hhcnROdW0gPT0gMikge1xyXG4gICAgICAgICAgICAgIGxldCB0ZW1wU3BsaXROYW1lID0gdGVtcHZhbHVlcy5uYW1lLnNwbGl0KCcoJyk7XHJcbiAgICAgICAgICAgICAgdGVtcHZhbHVlcy5uYW1lID0gdGVtcFNwbGl0TmFtZVswXTtcclxuICAgICAgICAgICAgICB0aGlzLmdldFVzZXJOYW1lTGlzdCh0ZW1wdmFsdWVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvL2NvbnNvbGUubG9nKHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmNvbmZpZ1RyYWNrZXJEZXRhaWxzKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLy9DaGFydCBwb3J0aW9uc1xyXG4gIGNoYW5nZXNieW1vZHVsZSgpIHtcclxuICAgIHRoaXMuY29uZnRjaGFydGJ5bW9kdWxlID0gW107XHJcbiAgICBsZXQgcXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIG1vZHVsZXM6IHRoaXMuc2VsZWN0ZWRfbW9kdWxlcyxcclxuICAgICAgY29uZmlnVHJhY2tlcklEOiB0aGlzLmNvbmZpZ1RyYWNrZXJJRFxyXG4gICAgfTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlXHJcbiAgICAgIC5jb25mVHJhY2tCeU1vZHVsZUNoYXJ0KHF1ZXJ5UGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgdmFyIHJlc3VsdCA9IF8uY2hhaW4oZGF0YS5ib2R5LnJlc3BvbnNlKVxyXG4gICAgICAgICAgLmdyb3VwQnkoJ19pZC5tb2R1bGVOYW1lJylcclxuICAgICAgICAgIC50b1BhaXJzKClcclxuICAgICAgICAgIC5tYXAoKG9ianMsIGtleSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhvYmpzKTtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICBtb2R1bGVuYW1lOiBvYmpzWzBdLFxyXG4gICAgICAgICAgICAgIGNvdW50OiBfLnN1bUJ5KG9ianNbMV0sICdjb3VudCcpXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLnZhbHVlKCk7XHJcbiAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LCAnLi4uUicpO1xyXG5cclxuICAgICAgICByZXN1bHQuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdGVtcEFycmF5OiBhbnkgPSB7fTtcclxuICAgICAgICAgIHRlbXBBcnJheS5uYW1lID0gZWxlbWVudC5tb2R1bGVuYW1lO1xyXG4gICAgICAgICAgdGVtcEFycmF5LnZhbHVlID0gZWxlbWVudC5jb3VudDtcclxuICAgICAgICAgIHRoaXMuY29uZnRjaGFydGJ5bW9kdWxlLnB1c2godGVtcEFycmF5KTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmNvbmZ0Y2hhcnRieW1vZHVsZSwgJy0tLS1jb25mdGNoYXJ0Ynltb2R1bGUnKTtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKFwiR3JvdXBlZFwiLCBzdGF0ZXMpO1xyXG4gICAgICAgIC8qXHJcblx0XHR2YXIgcmVzID0gZGF0YS5yZXNwb25zZS51c2VyQ29uZmxpY3RzO1xyXG5cdFx0cmVzLmZvckVhY2goZWxlbWVudCA9PiB7XHJcblx0XHRcdGNvbnN0IHRlbXBBcnJheTogYW55ID0ge307XHJcblx0XHRcdHRlbXBBcnJheS5uYW1lID0gZWxlbWVudC51c2VybmFtZTtcclxuXHRcdFx0dGVtcEFycmF5LnZhbHVlID0gZWxlbWVudC50b3RhbDtcclxuXHJcblx0XHRcdHRoaXMuZWJzX21fY2hhcnQucHVzaCh0ZW1wQXJyYXkpO1xyXG5cdFx0fSk7XHJcblx0XHQqL1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHdlZWtieXVzZXJDaGFydCgpIHtcclxuICAgIHRoaXMud2Vla19ieV91c2VyX2NoYXJ0ID0gW107XHJcbiAgICBsZXQgcXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIG1vZHVsZXM6IHRoaXMuc2VsZWN0ZWRfbW9kdWxlcyxcclxuICAgICAgY29uZmlnVHJhY2tlcklEOiB0aGlzLmNvbmZpZ1RyYWNrZXJJRFxyXG4gICAgfTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlXHJcbiAgICAgIC5jb25mVHJhY2tCeVVzZXJCeVdlZWtDaGFydChxdWVyeVBhcmFtcylcclxuICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgIC8vY29uc29sZS5sb2coXCJyZXNwb25zZVwiLGRhdGEucmVzcG9uc2UpO1xyXG4gICAgICAgIHZhciByZXN1bHQgPSBfLmNoYWluKGRhdGEuYm9keS5yZXNwb25zZSlcclxuICAgICAgICAgIC5ncm91cEJ5KCdfaWQudXNlcicpXHJcbiAgICAgICAgICAudG9QYWlycygpXHJcbiAgICAgICAgICAubWFwKChvYmpzLCBrZXkpID0+IHtcclxuICAgICAgICAgICAgLy9jb25zb2xlLmxvZyhvYmpzKTtcclxuICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICB1c2Vyczogb2Jqc1swXSxcclxuICAgICAgICAgICAgICBjb3VudDogXy5zdW1CeShvYmpzWzFdLCAnY291bnQnKVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC52YWx1ZSgpO1xyXG4gICAgICAgIC8vY29uc29sZS5sb2cocmVzdWx0KTtcclxuICAgICAgICByZXN1bHQuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgdGVtcEFycmF5OiBhbnkgPSB7fTtcclxuICAgICAgICAgIHRlbXBBcnJheS5uYW1lID0gZWxlbWVudC51c2VycztcclxuICAgICAgICAgIHRlbXBBcnJheS52YWx1ZSA9IGVsZW1lbnQuY291bnQ7XHJcbiAgICAgICAgICB0aGlzLndlZWtfYnlfdXNlcl9jaGFydC5wdXNoKHRlbXBBcnJheSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbW9udGhieXVzZXJDaGFydCgpIHtcclxuICAgIHRoaXMubW9udGhfYnlfdXNlcl9jaGFydCA9IFtdO1xyXG4gICAgLypcclxuXHR0aGlzLm1vbnRoX2J5X3VzZXJfY2hhcnQgPSBbXHJcblx0XHR7XHJcblx0XHQgIFwibmFtZVwiOiBcIlNJU01BSUxcIixcclxuXHRcdCAgXCJ2YWx1ZVwiOiA3NVxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0XCJuYW1lXCI6IFwiT3BlcmF0aW9uXCIsXHJcblx0XHRcdFwidmFsdWVcIjogMjVcclxuXHRcdH0sXHJcblx0XHR7XHJcblx0XHRcdFwibmFtZVwiOiBcIlhZWlwiLFxyXG5cdFx0XHRcInZhbHVlXCI6IDE1XHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRcIm5hbWVcIjogXCJNTk9cIixcclxuXHRcdFx0XCJ2YWx1ZVwiOiA0NVxyXG5cdFx0fVxyXG5cdCAgXTtcclxuXHQqL1xyXG4gICAgbGV0IHF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICBtb2R1bGVzOiB0aGlzLnNlbGVjdGVkX21vZHVsZXMsXHJcbiAgICAgIGNvbmZpZ1RyYWNrZXJJRDogdGhpcy5jb25maWdUcmFja2VySURcclxuICAgIH07XHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZVxyXG4gICAgICAuY29uZlRyYWNrQnlVc2VyQnlNb250aENoYXJ0KHF1ZXJ5UGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgdmFyIHJlc3VsdCA9IF8uY2hhaW4oZGF0YS5ib2R5LnJlc3BvbnNlKVxyXG4gICAgICAgICAgLmdyb3VwQnkoJ19pZC51c2VyJylcclxuICAgICAgICAgIC50b1BhaXJzKClcclxuICAgICAgICAgIC5tYXAoKG9ianMsIGtleSkgPT4ge1xyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKG9ianMpO1xyXG4gICAgICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICAgIHVzZXJzOiBvYmpzWzBdLFxyXG4gICAgICAgICAgICAgIGNvdW50OiBfLnN1bUJ5KG9ianNbMV0sICdjb3VudCcpXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLnZhbHVlKCk7XHJcbiAgICAgICAgdmFyIHN1bU9mQ291bnQgPSAwO1xyXG4gICAgICAgIHJlc3VsdC5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICBzdW1PZkNvdW50ID0gc3VtT2ZDb3VudCArIGVsZW1lbnQuY291bnQ7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdmFyIHRvdGFsdXNlcmNvdW50ID0gZGF0YS5ib2R5LnJlc3BvbnNlLmxlbmd0aDtcclxuICAgICAgICAvL2NvbnNvbGUubG9nKFwic3VtXCIsIHN1bU9mQ291bnQpO1xyXG4gICAgICAgIHJlc3VsdC5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICBjb25zdCB0ZW1wQXJyYXk6IGFueSA9IHt9O1xyXG5cclxuICAgICAgICAgIC8vY29uc29sZS5sb2coXCJjb3VudFwiLGVsZW1lbnQuY291bnQpO1xyXG4gICAgICAgICAgLy9jb25zb2xlLmxvZyhcInN1bVwiLCBzdW1PZkNvdW50KTtcclxuICAgICAgICAgIC8vY29uc29sZS5sb2coXCJ0b3RhbFwiLHRvdGFsdXNlcmNvdW50KTtcclxuICAgICAgICAgIC8vIHRlbXBBcnJheS52YWx1ZSA9ICgoZWxlbWVudC5jb3VudC9zdW1PZkNvdW50KSp0b3RhbHVzZXJjb3VudCkgO1xyXG4gICAgICAgICAgdGVtcEFycmF5LnBlcmNlbnRhZ2UgPVxyXG4gICAgICAgICAgICBwYXJzZUZsb2F0KCgoZWxlbWVudC5jb3VudCAqIDEwMCkgLyBzdW1PZkNvdW50KS50b0ZpeGVkKDIpKSArICclJztcclxuICAgICAgICAgIHRlbXBBcnJheS5uYW1lID0gZWxlbWVudC51c2VycyArICcoJyArIHRlbXBBcnJheS5wZXJjZW50YWdlICsgJyknO1xyXG4gICAgICAgICAgdGVtcEFycmF5LnZhbHVlID0gZWxlbWVudC5jb3VudDtcclxuICAgICAgICAgIHRoaXMubW9udGhfYnlfdXNlcl9jaGFydC5wdXNoKHRlbXBBcnJheSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgZm9ybWF0UGVyY2VudChjKTogc3RyaW5nIHtcclxuICAgIGNvbnNvbGUubG9nKGMpO1xyXG4gICAgcmV0dXJuIGMudmFsdWUgKyAnJSc7XHJcbiAgfVxyXG4gIGF4aXNGb3JtYXQodmFsKSB7XHJcbiAgICBpZiAodmFsICUgMSA9PT0gMCkge1xyXG4gICAgICByZXR1cm4gdmFsLnRvTG9jYWxlU3RyaW5nKCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBvblNlbGVjdChlbWl0dGVkLCB0eXBlQ2hhcnQsIGNoYXJ0TnVtKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlbWl0dGVkKTtcclxuICAgIC8vdGhpcy5tbmFtZS52YWx1ZXZhbHVlID0gdGhpcy5tb2R1bGVuYW1lO1xyXG4gICAgdGhpcy51c2VyU2VsZWN0ZWRUb2dnbGUgPSAncmVwb3J0TWdtdFRvZ2dsZSc7XHJcbiAgICB0aGlzLlRvZ2dsZSgncmVwb3J0X21hbmFnZW1lbnQnLCBlbWl0dGVkLCB0eXBlQ2hhcnQsIGNoYXJ0TnVtKTtcclxuXHJcbiAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5jb25maWdUcmFja2VyRGV0YWlscyA9IGVtaXR0ZWQ7XHJcbiAgfVxyXG5cclxuICBjbG9zZSgpIHtcclxuICAgIHRoaXMudHlwZXMgPSBbXTtcclxuICAgIHRoaXMuc2hvd2VkaXQoKTtcclxuICAgIHRoaXMuZ2V0YWxsY29uZmlncygpO1xyXG4gICAgdGhpcy5tb2RlID0gJ3ZpZXcnO1xyXG4gIH1cclxufVxyXG4iXX0=