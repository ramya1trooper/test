import { ManageAvComponent } from './manage-av/manage-av.component';
import { ManageAvDetailComponent } from './manage-av-detail/manage-av-detail.component';
import { ManageAvbyuserComponent } from './manage-av-byuser/manage-av-byuser.component';
import { ConfigTrackerComponent } from './config-tracker/config-tracker.component';
export const REPORT_MANAGEMENT_ROUTE = {
    path: 'report-management',
    children: [
        {
            path: 'manage-av',
            component: ManageAvComponent
        },
        {
            path: 'manage-av-detail/:datasourceId/:collectionDetail/:controlname',
            component: ManageAvDetailComponent
        },
        {
            path: 'manage-av-byuser',
            component: ManageAvbyuserComponent
        }, {
            path: 'config-tracker',
            component: ConfigTrackerComponent
        }
    ]
};
export const SETUP_ADMIN_ROUTE = {
    path: 'setup-administration',
    children: [
        {
            path: 'config-tracker-setup',
            component: ConfigTrackerComponent
        }
    ]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwb3J0LW1hbmFnZW1lbnQtcm91dGluZy5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYXZtL3JlcG9ydC1tYW5hZ2VtZW50LXJvdXRpbmcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3hGLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3hGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBRW5GLE1BQU0sQ0FBQyxNQUFNLHVCQUF1QixHQUFVO0lBQzdDLElBQUksRUFBRSxtQkFBbUI7SUFDekIsUUFBUSxFQUFFO1FBQ1Q7WUFDQyxJQUFJLEVBQUUsV0FBVztZQUNqQixTQUFTLEVBQUUsaUJBQWlCO1NBQzVCO1FBQ0Q7WUFDQyxJQUFJLEVBQUUsK0RBQStEO1lBQ3JFLFNBQVMsRUFBRSx1QkFBdUI7U0FDbEM7UUFDRDtZQUNDLElBQUksRUFBRSxrQkFBa0I7WUFDeEIsU0FBUyxFQUFFLHVCQUF1QjtTQUNsQyxFQUFDO1lBQ0QsSUFBSSxFQUFFLGdCQUFnQjtZQUN0QixTQUFTLEVBQUUsc0JBQXNCO1NBQ2pDO0tBQ0Q7Q0FDRCxDQUFDO0FBRUYsTUFBTSxDQUFDLE1BQU0saUJBQWlCLEdBQVc7SUFDeEMsSUFBSSxFQUFHLHNCQUFzQjtJQUM3QixRQUFRLEVBQUc7UUFDVjtZQUNDLElBQUksRUFBRyxzQkFBc0I7WUFDN0IsU0FBUyxFQUFHLHNCQUFzQjtTQUNsQztLQUNEO0NBQ0QsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFJvdXRlLCBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBNYW5hZ2VBdkNvbXBvbmVudCB9IGZyb20gJy4vbWFuYWdlLWF2L21hbmFnZS1hdi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYW5hZ2VBdkRldGFpbENvbXBvbmVudCB9IGZyb20gJy4vbWFuYWdlLWF2LWRldGFpbC9tYW5hZ2UtYXYtZGV0YWlsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hbmFnZUF2Ynl1c2VyQ29tcG9uZW50IH0gZnJvbSAnLi9tYW5hZ2UtYXYtYnl1c2VyL21hbmFnZS1hdi1ieXVzZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ29uZmlnVHJhY2tlckNvbXBvbmVudCB9IGZyb20gJy4vY29uZmlnLXRyYWNrZXIvY29uZmlnLXRyYWNrZXIuY29tcG9uZW50JztcclxuXHJcbmV4cG9ydCBjb25zdCBSRVBPUlRfTUFOQUdFTUVOVF9ST1VURTogUm91dGUgPSB7XHJcblx0cGF0aDogJ3JlcG9ydC1tYW5hZ2VtZW50JyxcclxuXHRjaGlsZHJlbjogW1xyXG5cdFx0e1xyXG5cdFx0XHRwYXRoOiAnbWFuYWdlLWF2JyxcclxuXHRcdFx0Y29tcG9uZW50OiBNYW5hZ2VBdkNvbXBvbmVudFxyXG5cdFx0fSxcclxuXHRcdHtcclxuXHRcdFx0cGF0aDogJ21hbmFnZS1hdi1kZXRhaWwvOmRhdGFzb3VyY2VJZC86Y29sbGVjdGlvbkRldGFpbC86Y29udHJvbG5hbWUnLFxyXG5cdFx0XHRjb21wb25lbnQ6IE1hbmFnZUF2RGV0YWlsQ29tcG9uZW50XHJcblx0XHR9LFxyXG5cdFx0e1xyXG5cdFx0XHRwYXRoOiAnbWFuYWdlLWF2LWJ5dXNlcicsXHJcblx0XHRcdGNvbXBvbmVudDogTWFuYWdlQXZieXVzZXJDb21wb25lbnRcclxuXHRcdH0se1xyXG5cdFx0XHRwYXRoOiAnY29uZmlnLXRyYWNrZXInLFxyXG5cdFx0XHRjb21wb25lbnQ6IENvbmZpZ1RyYWNrZXJDb21wb25lbnRcclxuXHRcdH1cclxuXHRdXHJcbn07XHJcblxyXG5leHBvcnQgY29uc3QgU0VUVVBfQURNSU5fUk9VVEUgOiBSb3V0ZSA9IHtcclxuXHRwYXRoIDogJ3NldHVwLWFkbWluaXN0cmF0aW9uJyxcclxuXHRjaGlsZHJlbiA6IFtcclxuXHRcdHtcclxuXHRcdFx0cGF0aCA6ICdjb25maWctdHJhY2tlci1zZXR1cCcsXHJcblx0XHRcdGNvbXBvbmVudCA6IENvbmZpZ1RyYWNrZXJDb21wb25lbnRcclxuXHRcdH1cclxuXHRdXHJcbn0iXX0=