import { Component, ViewChild, Output } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog } from '@angular/material';
import { Location } from '@angular/common';
import { SnackBarService } from './../../shared/snackbar.service';
import { Router, ActivatedRoute } from "@angular/router";
import { ReportManagementService } from '../report-management.service';
import { manageAvDetailTableConfig } from '../../table_config';
import { fuseAnimations } from '../../@fuse/animations';
import { EventEmitter } from '@angular/core';
import * as c3 from 'c3';
import { MessageService } from "../../_services/message.service";
import { process } from '@progress/kendo-data-query';
var MONTH = { JAN: 0, FEB: 1, MAR: 2, APR: 3, MAY: 4, JUN: 5, JUL: 6, AUG: 7, SEP: 8, OCT: 9, NOV: 10, DEC: 11 };
var MONTH2 = { jan: 0, feb: 1, mar: 2, apr: 3, may: 4, jun: 5, jul: 6, aug: 7, sep: 8, oct: 9, nov: 10, dec: 11 };
// declare var c3: any;
export class ManageAvDetailComponent {
    constructor(router, _matDialog, _reportManagementService, activeRoute, snackBarService, location, messageService) {
        this.router = router;
        this._matDialog = _matDialog;
        this._reportManagementService = _reportManagementService;
        this.activeRoute = activeRoute;
        this.snackBarService = snackBarService;
        this.location = location;
        this.messageService = messageService;
        this.myEvent = new EventEmitter();
        this.selectedTableHeader = [];
        this.displayedColumns = [
            'entitlement',
            'username',
            'firstname',
            'lastname',
            'responsibiltyname'
        ];
        this.selection = new SelectionModel(true, []);
        this.nav_position = 'end';
        this.queryParams = {};
        this.limit = 10;
        this.offset = 0;
        this.roleChartData = [];
        this.userChartData = [];
        // Bar
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = true;
        this.showXAxisLabel = true;
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Population';
        this.colorScheme = {
            domain: ['#28a3dd']
        };
        // pie
        this.showLabels = true;
        this.showPieLegend = false;
        this.explodeSlices = false;
        this.doughnut = false;
        this.colorPieScheme = {
            domain: ['#3a3f51', '#23b7e5', '#f05050', '#27c24c', '#e60050', '#2980b9', '#7266ba', '#2accdb']
        };
        this.pieChartView = [300, 250];
        this.enableFilter = false;
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.state = {};
        this.panelOpenState = false;
        // console.log("av detail URL Parms **************",this.activeRoute.snapshot.params)
        this.parmsObj = this.activeRoute.snapshot.params;
    }
    ngOnInit() {
        this.state = {
            skip: this.offset,
            take: this.limit
        };
        console.log(this.router.url, ">>>THIS");
        let urlString = decodeURIComponent(this.router.url);
        this.urlParams = urlString.split('/');
        this.displayNam = this.urlParams[5]; //this._reportManagementService.passValue['displayName'];
        this.getdatasource();
        this.loadTableColumn();
        this.queryParams.datasource2 = this.urlParams[3]; //this._reportManagementService.passValue.datasourceId;
        this.queryParams.collectionDetail = this.urlParams[4]; //this._reportManagementService.passValue.collectionDetail;
        setTimeout(() => {
            this.onLoadChart();
        }, 500);
    }
    dataStateChange(state) {
        this.state = state;
        this.applyTableState(this.state);
    }
    applyTableState(state) {
        this.kendoGridData = process(this._data, state);
    }
    changePage(event) {
        console.log(event, ">>> EVENT Change Page");
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
    }
    onClick(action) {
        if (action == 'refresh') {
            this.ngOnInit();
        }
    }
    enableFilterOptions() {
        this.enableFilter = !this.enableFilter;
    }
    backClicked() {
        this.location.back();
        let obj = {
            id: "manageAvm"
        };
        let self = this;
        setTimeout(function () {
            self.messageService.sendRouting(obj);
        }, 100);
    }
    tableSetting() {
        this.panelOpenState = !this.panelOpenState;
    }
    getdatasource() {
        this._reportManagementService.getAllDatasources().subscribe(res => {
            this.customdatasource = res.response.datasources;
            this.defaultdatasourceId = res.response.datasources[0]._id;
            this.onLoadmanageAvmDetail(this.defaultdatasourceId);
        });
    }
    updateTable(event) {
        console.log(event, "----event");
        let selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isactive = true;
            }
            else {
                item.isactive = false;
            }
        });
        localStorage.removeItem('manageAvsDetail');
        localStorage.setItem('manageAvsDetail', JSON.stringify(this.tableConfig));
        this.loadTableColumn();
        // var index = this.tableNamesearch(event.source.value, this.tableConfig);
        // if (index >= 0) {
        // 	if (event.checked) {
        // 		let active = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: true
        // 		};
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, active);
        // 			localStorage.removeItem("manageAvsDetail");
        // 			localStorage.setItem("manageAvsDetail", JSON.stringify(this.tableConfig));
        // 		}
        // 	} else {
        // 		let inactive = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: false
        // 		};
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, inactive);
        // 			localStorage.removeItem("manageAvsDetail");
        // 			localStorage.setItem("manageAvsDetail", JSON.stringify(this.tableConfig));
        // 		}
        // 	}
        // }
        // this.loadTableColumn();
    }
    tableNamesearch(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].value === nameKey) {
                return i;
            }
        }
    }
    loadTableColumn() {
        this.displayedColumns = [];
        this.tableConfig = JSON.parse(localStorage.getItem('manageAvsDetail'));
        if (this.tableConfig) {
            for (let columns of this.tableConfig) {
                if (columns.isactive) {
                    this.selectedTableHeader.push(columns.value);
                    // this.displayedColumns.push(columns.value);
                    this.displayedColumns.push(columns);
                }
            }
        }
        else {
            this.tableConfig = manageAvDetailTableConfig;
            for (let columns of this.tableConfig) {
                if (columns.isactive) {
                    this.selectedTableHeader.push(columns.value);
                    //this.displayedColumns.push(columns.value);
                    this.displayedColumns.push(columns);
                }
            }
        }
    }
    getDatasourceId(id) {
        this.onLoadmanageAvmDetail(id);
    }
    onLoadmanageAvmDetail(id) {
        this.defaultdatasourceId = id;
        this._reportManagementService.getallmanageAvDetail(this.queryParams).subscribe(response => {
            this.manageAvslist = response.data;
            this.dataSource2 = new MatTableDataSource(this.manageAvslist);
            this.length = response.total;
            this.dataSource2.paginator = this.paginator;
            this._data = this.manageAvslist;
            this.applyTableState(this.state);
        });
    }
    onLoadChart() {
        //Chart 1
        this._reportManagementService.getCategoryPercentage(this.queryParams).subscribe(response => {
            this.chartData = [];
            if (this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails' || this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails-ebs') {
                this.title1 = 'Invoice Type';
            }
            else if (this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails' || this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails-ebs') {
                this.title1 = 'Top 5 Vendors';
            }
            else {
                this.title1 = "Journals by Category";
            }
            for (let result of response.data) {
                var obj = result;
                var ids = [];
                ids.push(obj.name);
                ids.push(obj.percentage);
                this.chartData.push(ids);
            }
            //this.chartData = [['Adjustmentsss', 20],['Accrual', 10],['Reclass', 45],['Other', 25]];
            let chart1 = c3.generate({
                bindto: '#chart1',
                data: {
                    columns: this.chartData,
                    //json : this.chartData,
                    type: 'pie',
                },
                color: {
                    pattern: ['#396AB1', '#DA7C30', '#3E9651', '#CC2529', '#535154', '#6B4C9A', '#948B3D']
                },
                legend: {
                    show: true
                },
            });
        });
        //Chart 2
        this._reportManagementService.getTopAmount(this.queryParams).subscribe(response => {
            this.chartData2 = [];
            if (this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails' || this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails-ebs') {
                this.title2 = 'Top 5 users with amount';
            }
            else if (this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails' || this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails-ebs') {
                this.title2 = 'Top 5 Users with Amount Paid';
            }
            else {
                this.title2 = "Top 5 Journals";
            }
            var responsearray = response.data;
            responsearray.sort(function (a, b) {
                return a.year - b.year || MONTH[a.month] - MONTH[b.month];
            });
            /*
            0: {journalname: "Conversie Adj-02", month: "MAR", year: "2005", amount: 10000}
            1: {journalname: "ABN 04-FEB-05 B025", month: "FEB", year: "2005", amount: 9500}
            2: {journalname: "DRI INT EXP Correction: 12-MAY-05 14:43:25", month: "MAY", year: "2005", amount: 9000}
            3: {journalname: "APR0014", month: "MAY", year: "2005", amount: 8500}
            4: {journalname: "Purchase Invoices USD", month: "FEB", year: "2005", amount: 8000}
            */
            /*
            this.chartData2 = [
            ['month','2019-03-25', '2019-04-25'],
            ['Conversie Adj-02', 2000, 2500],
            ['KAS FEB-05', 3000, 3500],
            ['Conversie Aug-03 Conversion EUR', 4000, 4500],
            ['MEMO 31-MAR-05 M027', 4500,2500],
            ['Purchase Invoices USD', 5000, 1000]
            ];
            */
            var months = ['month'];
            var jnames = [];
            for (let result of responsearray) {
                var obj = result;
                var ids = [];
                //username array object
                months.push(obj.month);
                ids.push(obj.journalname);
                jnames.push(obj.journalname);
                ids.push(obj.month);
                ids.push(obj.year);
                ids.push(obj.amount);
                this.chartData2.push(ids);
            }
            let uniquemonths = months.filter((elem, i, arr) => {
                if (arr.indexOf(elem) === i) {
                    return elem;
                }
            });
            //unique jnames
            let uniquejnames = jnames.filter((elem, i, arr) => {
                if (arr.indexOf(elem) === i) {
                    return elem;
                }
            });
            var journalarraysets = [];
            for (let result of responsearray) {
                for (let eachmonth of uniquemonths) {
                    if (eachmonth != 'month') {
                        var journalnames = [result.journalname];
                        if (eachmonth == result.month) {
                            if (result.amount != '') {
                                journalnames.push(result.amount);
                                journalarraysets.push(journalnames);
                            }
                        }
                        else {
                            journalnames.push(0);
                            journalarraysets.push(journalnames);
                        }
                    }
                }
            }
            var finalarrayset = [];
            for (let eachjnames of uniquejnames) {
                var newjrnarrays = [eachjnames];
                for (let entry of journalarraysets) {
                    if (entry[0] === eachjnames) {
                        newjrnarrays.push(entry[1]);
                    }
                }
                finalarrayset.push(newjrnarrays);
            }
            var chart2array = [];
            //chart2array.push(uniquemonths);
            for (let eachrow of finalarrayset)
                chart2array.push(eachrow);
            var monthcategorized = [];
            for (let monthname of uniquemonths) {
                if (monthname !== 'month') {
                    monthcategorized.push(monthname);
                }
            }
            var chart2 = c3.generate({
                bindto: '#chart2',
                data: {
                    columns: chart2array,
                    type: 'bar'
                },
                color: {
                    pattern: ['#396AB1', '#DA7C30', '#3E9651', '#CC2529', '#535154', '#6B4C9A', '#948B3D']
                },
                bar: {
                    width: 25
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: monthcategorized
                    }
                }
            });
        });
        //Chart 3
        this._reportManagementService.getTotalNumberofJournal(this.queryParams).subscribe(response => {
            this.chartData3 = [];
            if (this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails' || this.parmsObj.collectionDetail == 'avmSupplierInvoicedetails-ebs') {
                this.title3 = 'Invoice amount by month';
            }
            else if (this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails' || this.parmsObj.collectionDetail == 'avmSuppleirPaymentdetails-ebs') {
                this.title3 = 'Amount paid by month';
            }
            else {
                this.title3 = "Journals by Period";
            }
            var responsearray = response.data;
            responsearray.sort(function (a, b) {
                return a.year - b.year || MONTH2[a.month.toLowerCase()] - MONTH2[b.month.toLowerCase()];
            });
            var months = ['month'];
            for (let result of responsearray) {
                var obj = result;
                var ids = [];
                //username array object
                months.push(obj.month);
                ids.push(obj.month);
                ids.push(obj.year);
                ids.push(obj.count);
                this.chartData3.push(ids);
            }
            let uniquemonths = months.filter((elem, i, arr) => {
                if (arr.indexOf(elem) === i) {
                    return elem;
                }
            });
            var journalarraysets = [];
            for (let result of responsearray) {
                for (let eachmonth of uniquemonths) {
                    if (eachmonth != 'month') {
                        var journalperiod = [result.month];
                        if (eachmonth == result.month) {
                            if (result.count != '') {
                                journalperiod.push(result.count);
                                journalarraysets.push(journalperiod);
                            }
                        }
                        else {
                            journalperiod.push(0);
                            journalarraysets.push(journalperiod);
                        }
                    }
                }
            }
            var finalarrayset = [];
            for (let eachjperiod of uniquemonths) {
                var newjrnarrays = [eachjperiod];
                for (let entry of journalarraysets) {
                    if (entry[0] === eachjperiod) {
                        newjrnarrays.push(entry[1]);
                    }
                }
                finalarrayset.push(newjrnarrays);
            }
            var chart3array = [];
            //chart2array.push(uniquemonths);
            for (let eachrow of finalarrayset)
                chart3array.push(eachrow);
            var monthcategorized = [];
            for (let monthname of uniquemonths) {
                if (monthname !== 'month') {
                    monthcategorized.push(monthname);
                }
            }
            let chart3 = c3.generate({
                bindto: '#chart3',
                data: {
                    columns: chart3array,
                    type: 'bar'
                },
                bar: {
                    width: 25
                },
                color: {
                    pattern: ['#396AB1', '#DA7C30', '#3E9651', '#CC2529', '#535154', '#6B4C9A', '#948B3D']
                },
                axis: {
                    x: {
                        type: 'category',
                        categories: monthcategorized
                    }
                }
            });
        });
    }
    showAvDetail() {
        //localStorage.removeItem("editUserId");
        //localStorage.setItem("editUserId", user.id.toString());
        let obj = {
            url: 'report-management/manage-av-byuser',
            id: "manageAvUser"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['/', 'report-management/manage-av-byuser']).then(nav => {
        }, err => {
            console.log(err); // when there's an error
        });
    }
    ;
    onSelect(evt, selectedVal) {
        let obj = {
            url: 'report-management/manage-av-byuser',
            id: "manageAvUser"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['report-management/manage-av-byuser']);
        // this.myEvent.emit({createdby:selectedItem.username,journalname:selectedItem.entitlement});
        this._reportManagementService.passValue['createdBy'] = selectedVal.username;
        this._reportManagementService.passValue['journalname'] = selectedVal.entitlement;
    }
}
ManageAvDetailComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-manage-av-detail',
                template: "<div class=\"page-layout blank\" style=\"padding: 0px !important;\" fusePerfectScrollbar >\r\n\t<div style=\"padding: 10px\">\r\n\t\t<button mat-button (click)=\"backClicked()\">\r\n\t\t\t<mat-icon>arrow_left</mat-icon>Back to AV\r\n\t\t</button>\r\n\t</div>\r\n\t<!-- <mat-drawer-container class=\"example-container sen-bg-container\" autosize fxFlex [hasBackdrop]=\"false\"> -->\r\n      <div>\r\n\t\t<!-- CONTENT HEADER -->\r\n\t\t<div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\"\r\n\t\t\tstyle=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: space-between;\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n\t\t\t\tstyle=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: flex-start;\">\r\n\t\t\t\t<h2 class=\"m-0 senlib-top-header\">\r\n\t\t\t\t\t<span class=\"material-icons\"\r\n                    style=\"margin-right: 7px;font-size: 21px;position: relative;top:2px\">beenhere</span>\r\n\t\t\t\t\t{{displayNam}}\r\n\t\t\t\t</h2>\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilterOptions()\">\r\n\t\t\t\t\t<span class=\"k-icon k-i-filter\"\r\n\t\t\t\t\t  style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n\t\t\t\t</button>\r\n\t\t\t\t<button  class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n\t\t\t\t(click)=\"select.open()\">\r\n\t\t\t\t<span class=\"material-icons\"\r\n\t\t\t\t  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n\t\t\t\t<mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n\t\t\t\t  (selectionChange)=\"updateTable($event)\">\r\n\t\t\t\t  <mat-option *ngFor=\"let columnSetting of tableConfig\" [checked]=\"columnSetting.isactive\"\r\n\t\t\t\t\t[value]=\"columnSetting.value\">{{ columnSetting.name}}</mat-option>\r\n\t\t\t\t</mat-select>\r\n\t\t\t\t</button>\r\n\t\t\t\t<button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"  (click)=\"onClick('refresh')\">\r\n\t\t\t\t   <span class=\"material-icons\"\r\n\t\t\t\t   style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n\t\t\t\t   autorenew</span>Refresh\r\n\t\t\t\t</button>\r\n\t\t   </div>\r\n\t\t</div>\r\n\t\t<!-- / CONTENT HEADER -->\r\n\t\t<!-- <div class=\"content\"> -->\r\n\t\t\t<div class=\"ui-lib-card\">\r\n\t\t\t\t<div class=\"ui-lib-card-body\">\r\n\t\t\t\t\t<div class=\"row\" style=\"margin-right:10px;margin-left:10px;\">\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<div class=\"ui-lib-chart-header\">{{title1}}</div>\r\n\t\t\t\t\t\t\t<div id=\"chart1\"></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<div class=\"ui-lib-chart-header\">{{title2}}</div>\r\n\t\t\t\t\t\t\t<div id=\"chart2\"></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t<div class=\"col-md-4\">\r\n\t\t\t\t\t\t\t<div class=\"ui-lib-chart-header\">{{title3}}</div>\r\n\t\t\t\t\t\t\t<div id=\"chart3\"></div>\r\n\t\t\t\t\t\t</div>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div class=\"row\" style=\"margin-right:10px;margin-left:10px;\">\r\n\t\t\t\t\t\t<!-- Kendo UI Start -->\r\n\t\t\t\t\t\t<div class=\"mat-elevation-z8 sen-margin-10\">\r\n\t\t\t\t\t\t\t<kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n\t\t\t\t\t\t\t\t[skip]=\"state.skip\"\r\n\t\t\t\t\t\t\t\t[sort]=\"state.sort\"\r\n\t\t\t\t\t\t\t\t[filter]=\"state.filter\"\r\n\t\t\t\t\t\t\t\t[sortable]=\"true\"\r\n\t\t\t\t\t\t\t\t[pageable]=\"true\" \r\n\t\t\t\t\t\t\t\t[resizable]=\"true\"\r\n\t\t\t\t\t\t\t\t[filterable]=\"enableFilter\"\r\n\t\t\t\t\t\t\t\t(dataStateChange)=\"dataStateChange($event)\"\r\n\t\t\t\t\t\t\t\t>\r\n\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n\t\t\t\t\t\t\t\t\t<kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\" *ngIf=\"!column.onSelect\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-template kendoGridCellTemplate let-dataItem>\r\n\t\t\t\t\t\t\t\t\t\t\t<span> \r\n\t\t\t\t\t\t\t\t\t\t\t\t{{dataItem[column.value]}}\r\n\t\t\t\t\t\t\t\t\t\t\t</span>\r\n\t\t\t\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t\t\t</kendo-grid-column>\r\n\r\n\t\t\t\t\t\t\t\t\t<kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n\t\t\t\t\t\t\t\t\t*ngIf=\"column.onSelect\" title=\"{{column.name}}\" field=\"{{column.value}}\" width=\"100\">\r\n\t\t\t\t\t\t\t\t\t\t<ng-template kendoGridCellTemplate let-dataItem>\r\n\t\t\t\t\t\t\t\t\t\t\t<a (click)=\"onSelect($event, dataItem)\">{{dataItem[column.value]}}</a>\r\n\t\t\t\t\t\t\t\t\t\t</ng-template>\r\n\t\t\t\t\t\t\t\t\t</kendo-grid-column>\r\n\t\t\t\t\t\t\t\t</ng-container>\r\n\t\t\t\t\t\t\t<ng-template kendoGridNoRecordsTemplate>\r\n\t\t\t\t\t\t\t\tNo Records Found\r\n\t\t\t\t\t\t\t</ng-template> \r\n\t\t\t\t\t\t\t</kendo-grid>\r\n\t\t\t\t\t\t</div> \r\n\t\t\t\t\t</div>\r\n\t\t\t\t</div>\r\n\t\t\t</div>\r\n\t\t\r\n\t\t<!-- Kendo UI End -->\r\n      </div>\r\n\t<!-- </mat-drawer-container> -->\r\n</div>",
                animations: fuseAnimations,
                styles: [".tick text{font-size:12px}.axis line,.axis path{fill:none;stroke:#4c5554;stroke-width:1}.x.axis .tick line{display:none}.domain{display:block!important;stroke:#4c5554!important;stroke-width:2!important}.legend{font-size:12px;font-family:sans-serif}.legend rect{stroke-width:2}.chart-row-box{width:100%;margin-bottom:5px;padding-bottom:5px}.chartonebox{background-color:#fff;float:left;padding:10px;margin-right:4px;margin-bottom:30px}.twenty{width:20%}.thirty{width:33%}.fourty{width:40%}.custom-mat-form{display:inline!important}table{width:100%}.mat-header-cell{font-weight:700!important;color:#000!important}td.mat-cell,td.mat-footer-cell,th.mat-header-cell{border-bottom-width:1px!important}.mat-form-field{font-size:12px;line-height:15px;font-weight:400}mat-label{font-size:12px;font-weight:700}app-chart-layout,app-pie-chart,app-pie-grid{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.charts-view{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;flex-basis:100%;width:33%!important;-webkit-box-flex:1;flex:1;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.mat-option{white-space:none!important}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.senlib-padding-10{padding:10px;padding-top:0!important}.senlib-m{margin-left:16px;margin-right:13px!important;margin-top:0;margin-bottom:-34px}content{position:relative;display:-webkit-box;display:flex;z-index:1;-webkit-box-flex:1;flex:1 0 auto}content>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto;width:100%;min-width:100%}.card-directive{background:#fff;border:1px solid #d3d3d3;margin:5px!important}chart-layout{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;-webkit-box-flex:1!important;flex:auto!important}.ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.sentri-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}.example-container{width:100%;height:100%}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}.ui-lib-card{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-width:0;word-wrap:break-word;background-clip:border-box;border:0 solid transparent;border-radius:.25rem;background-color:#fff;-webkit-transition:.5s ease-in-out;transition:.5s ease-in-out;position:relative;height:calc(100% - 30px);margin:10px}.ui-card-body{padding:1.25rem;-webkit-box-flex:1;flex:1 1 auto}.ui-lib-card-body{-webkit-box-flex:1;flex:1 1 auto;padding:1rem}.ui-lib-card-title{font-size:16px;margin-bottom:.5rem}.ui-lib-card-header{padding:.5rem 1rem;margin-bottom:0;background-color:rgba(0,0,0,.03);border-bottom:1px solid rgba(0,0,0,.125)}.ui-lib-chart-header{padding-bottom:17px;color:#06065d;font-size:16px;font-weight:600;text-align:center}"]
            }] }
];
/** @nocollapse */
ManageAvDetailComponent.ctorParameters = () => [
    { type: Router },
    { type: MatDialog },
    { type: ReportManagementService },
    { type: ActivatedRoute },
    { type: SnackBarService },
    { type: Location },
    { type: MessageService }
];
ManageAvDetailComponent.propDecorators = {
    myEvent: [{ type: Output }],
    paginator: [{ type: ViewChild, args: [MatPaginator,] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlLWF2LWRldGFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYXZtL21hbmFnZS1hdi1kZXRhaWwvbWFuYWdlLWF2LWRldGFpbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFNBQVMsRUFBVSxTQUFTLEVBQXFCLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN4RixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ3JFLE9BQU8sRUFBRSxTQUFTLEVBQWMsTUFBTSxtQkFBbUIsQ0FBQztBQUMxRCxPQUFPLEVBQUMsUUFBUSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFFekMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDdkUsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFL0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0MsT0FBTyxLQUFLLEVBQUUsTUFBTSxJQUFJLENBQUM7QUFDekIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2pFLE9BQU8sRUFBK0IsT0FBTyxFQUFRLE1BQU0sNEJBQTRCLENBQUM7QUFheEYsSUFBSSxLQUFLLEdBQUcsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLEVBQUUsRUFBRSxDQUFDO0FBQ2pILElBQUksTUFBTSxHQUFHLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQztBQUNsSCx1QkFBdUI7QUFTdkIsTUFBTSxPQUFPLHVCQUF1QjtJQTZFbEMsWUFBb0IsTUFBYyxFQUFVLFVBQXFCLEVBQ3ZELHdCQUFpRCxFQUNqRCxXQUEwQixFQUMxQixlQUFnQyxFQUFTLFFBQWtCLEVBQzNELGNBQThCO1FBSnBCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFBVSxlQUFVLEdBQVYsVUFBVSxDQUFXO1FBQ3ZELDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBeUI7UUFDakQsZ0JBQVcsR0FBWCxXQUFXLENBQWU7UUFDMUIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQVMsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUMzRCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUEvRTlCLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBTXZDLHdCQUFtQixHQUFDLEVBQUUsQ0FBQztRQUN2QixxQkFBZ0IsR0FBYTtZQUMzQixhQUFhO1lBQ2IsVUFBVTtZQUNWLFdBQVc7WUFDWCxVQUFVO1lBQ1YsbUJBQW1CO1NBQ3BCLENBQUM7UUFDRixjQUFTLEdBQUcsSUFBSSxjQUFjLENBQWtCLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUUxRCxpQkFBWSxHQUFXLEtBQUssQ0FBQztRQUM3QixnQkFBVyxHQUFRLEVBQUUsQ0FBQztRQUN0QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFpQm5CLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLE1BQU07UUFDTixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixlQUFVLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLG1CQUFjLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLGVBQVUsR0FBRyxZQUFZLENBQUM7UUFHMUIsZ0JBQVcsR0FBRztZQUNaLE1BQU0sRUFBRSxDQUFDLFNBQVMsQ0FBQztTQUNwQixDQUFDO1FBR0YsTUFBTTtRQUNOLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQUNqQixtQkFBYyxHQUFHO1lBQ2YsTUFBTSxFQUFFLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQztTQUNqRyxDQUFDO1FBRUYsaUJBQVksR0FBUyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUtoQyxpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUV2QixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ1osU0FBSSxHQUF3QixTQUFTLENBQUM7UUFDdEMsY0FBUyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ25ILGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzNCLFVBQUssR0FBVSxFQUFFLENBQUM7UUE4RGxCLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBdEQ5QixxRkFBcUY7UUFDckYsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7SUFDbkQsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsS0FBSyxHQUFDO1lBQ1QsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSztTQUNqQixDQUFBO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBQyxTQUFTLENBQUMsQ0FBQTtRQUN0QyxJQUFJLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsVUFBVSxHQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyx5REFBeUQ7UUFDNUYsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsdURBQXVEO1FBQ3pHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFBLDJEQUEyRDtRQUNqSCxVQUFVLENBQUMsR0FBRSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3hCLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUVQLENBQUM7SUFDRCxlQUFlLENBQUMsS0FBMkI7UUFDekMsSUFBSSxDQUFDLEtBQUssR0FBRSxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUNELGVBQWUsQ0FBQyxLQUFZO1FBQzFCLElBQUksQ0FBQyxhQUFhLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDbEQsQ0FBQztJQUNELFVBQVUsQ0FBQyxLQUFLO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsdUJBQXVCLENBQUMsQ0FBQTtRQUMzQyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztJQUMzQixDQUFDO0lBQ0QsT0FBTyxDQUFDLE1BQU07UUFDWixJQUFHLE1BQU0sSUFBRSxTQUFTLEVBQ3BCO1lBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2pCO0lBQ0gsQ0FBQztJQUNELG1CQUFtQjtRQUNqQixJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUN4QyxDQUFDO0lBQ0YsV0FBVztRQUNULElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDckIsSUFBSSxHQUFHLEdBQUc7WUFDUixFQUFFLEVBQUcsV0FBVztTQUNqQixDQUFDO1FBQ0YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNULElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZDLENBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQTtJQUNSLENBQUM7SUFHRixZQUFZO1FBQ1gsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUE7SUFDM0MsQ0FBQztJQUVBLGFBQWE7UUFDWCxJQUFJLENBQUMsd0JBQXdCLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDaEUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ2pELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7WUFDM0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3ZELENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUNELFdBQVcsQ0FBQyxLQUFLO1FBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLENBQUE7UUFDakMsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQzVDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLFVBQVMsSUFBSTtZQUNsQyxJQUFHLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDeEI7aUJBQUk7Z0JBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7YUFDekI7UUFDTCxDQUFDLENBQUMsQ0FBQTtRQUVFLFlBQVksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUMzQyxZQUFZLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7UUFDMUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRTdCLDBFQUEwRTtRQUMxRSxvQkFBb0I7UUFDcEIsd0JBQXdCO1FBQ3hCLG1CQUFtQjtRQUNuQixnQ0FBZ0M7UUFDaEMsOEJBQThCO1FBQzlCLG9CQUFvQjtRQUNwQixPQUFPO1FBQ1Asc0JBQXNCO1FBQ3RCLGdEQUFnRDtRQUNoRCxpREFBaUQ7UUFDakQsZ0ZBQWdGO1FBQ2hGLE1BQU07UUFDTixZQUFZO1FBQ1oscUJBQXFCO1FBQ3JCLGdDQUFnQztRQUNoQyw4QkFBOEI7UUFDOUIscUJBQXFCO1FBQ3JCLE9BQU87UUFDUCxzQkFBc0I7UUFDdEIsa0RBQWtEO1FBQ2xELGlEQUFpRDtRQUNqRCxnRkFBZ0Y7UUFDaEYsTUFBTTtRQUNOLEtBQUs7UUFDTCxJQUFJO1FBQ0osMEJBQTBCO0lBQzFCLENBQUM7SUFFRixlQUFlLENBQUMsT0FBTyxFQUFFLE9BQU87UUFDL0IsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxLQUFLLE9BQU8sRUFBRTtnQkFDakMsT0FBTyxDQUFDLENBQUM7YUFDVDtTQUNEO0lBQ0YsQ0FBQztJQUVBLGVBQWU7UUFDYixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQztRQUN2RSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDcEIsS0FBSyxJQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7b0JBQ3pCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN6Qyw2Q0FBNkM7b0JBQzdDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3BDO2FBQ0Y7U0FDRjthQUFNO1lBQ0wsSUFBSSxDQUFDLFdBQVcsR0FBRyx5QkFBeUIsQ0FBQztZQUM3QyxLQUFLLElBQUksT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ3BDLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTtvQkFDekIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ3hDLDRDQUE0QztvQkFDNUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDckM7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVELGVBQWUsQ0FBQyxFQUFFO1FBQ2hCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsQ0FBQTtJQUNoQyxDQUFDO0lBRUQscUJBQXFCLENBQUMsRUFBRTtRQUN0QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFBO1FBRTdCLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3hGLElBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztZQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQztZQUM3QixJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBRzVDLElBQUksQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUM5QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUVuQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFHRCxXQUFXO1FBRVQsU0FBUztRQUNULElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBRXpGLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBQ3BCLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBRSwyQkFBMkIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLCtCQUErQixFQUFDO2dCQUNoSSxJQUFJLENBQUMsTUFBTSxHQUFFLGNBQWMsQ0FBQTthQUM1QjtpQkFBSyxJQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsMkJBQTJCLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBRSwrQkFBK0IsRUFBQztnQkFDdEksSUFBSSxDQUFDLE1BQU0sR0FBRSxlQUFlLENBQUE7YUFDN0I7aUJBQUk7Z0JBQ0gsSUFBSSxDQUFDLE1BQU0sR0FBRyxzQkFBc0IsQ0FBQzthQUN0QztZQUdELEtBQUksSUFBSSxNQUFNLElBQUksUUFBUSxDQUFDLElBQUksRUFBQztnQkFDOUIsSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDO2dCQUNqQixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7Z0JBQ2IsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25CLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUMxQjtZQUVELHlGQUF5RjtZQUN6RixJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUN2QixNQUFNLEVBQUUsU0FBUztnQkFDakIsSUFBSSxFQUFFO29CQUNKLE9BQU8sRUFBRyxJQUFJLENBQUMsU0FBUztvQkFDeEIsd0JBQXdCO29CQUN4QixJQUFJLEVBQUcsS0FBSztpQkFDWjtnQkFFRCxLQUFLLEVBQUM7b0JBQ0osT0FBTyxFQUFDLENBQUMsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDO2lCQUN0RjtnQkFDSCxNQUFNLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLElBQUk7aUJBQ1g7YUFFSCxDQUFDLENBQUM7UUFFUCxDQUFDLENBQUMsQ0FBQztRQUdDLFNBQVM7UUFDVCxJQUFJLENBQUMsd0JBQXdCLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEVBQUU7WUFFaEYsSUFBSSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDckIsSUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLDJCQUEyQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsK0JBQStCLEVBQUU7Z0JBQ2pJLElBQUksQ0FBQyxNQUFNLEdBQUUseUJBQXlCLENBQUE7YUFDdkM7aUJBQUssSUFBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLDJCQUEyQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLElBQUUsK0JBQStCLEVBQUU7Z0JBQ3ZJLElBQUksQ0FBQyxNQUFNLEdBQUUsOEJBQThCLENBQUE7YUFDNUM7aUJBQUk7Z0JBQ0gsSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQzthQUNoQztZQUVELElBQUksYUFBYSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDbEMsYUFBYSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsRUFBRSxDQUFDO2dCQUM3QixPQUFPLENBQUMsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDOUQsQ0FBQyxDQUFDLENBQUM7WUFDSDs7Ozs7O2NBTUU7WUFFRjs7Ozs7Ozs7O2NBU0U7WUFFRixJQUFJLE1BQU0sR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3ZCLElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQztZQUNoQixLQUFJLElBQUksTUFBTSxJQUFJLGFBQWEsRUFBQztnQkFDOUIsSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDO2dCQUNqQixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7Z0JBQ2IsdUJBQXVCO2dCQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdkIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzFCLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUM3QixHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25CLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUMzQjtZQUNELElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUNoRCxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUMzQixPQUFPLElBQUksQ0FBQTtpQkFDWjtZQUNILENBQUMsQ0FBQyxDQUFBO1lBRUYsZUFBZTtZQUNmLElBQUksWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEdBQUcsRUFBRSxFQUFFO2dCQUNoRCxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUMzQixPQUFPLElBQUksQ0FBQTtpQkFDWjtZQUNILENBQUMsQ0FBQyxDQUFBO1lBRUYsSUFBSSxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7WUFDMUIsS0FBSSxJQUFJLE1BQU0sSUFBSSxhQUFhLEVBQUM7Z0JBQ2hDLEtBQUksSUFBSSxTQUFTLElBQUksWUFBWSxFQUFDO29CQUNoQyxJQUFHLFNBQVMsSUFBRSxPQUFPLEVBQUM7d0JBQ3RCLElBQUksWUFBWSxHQUFHLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUN0QyxJQUFHLFNBQVMsSUFBRSxNQUFNLENBQUMsS0FBSyxFQUFDOzRCQUN4QixJQUFHLE1BQU0sQ0FBQyxNQUFNLElBQUksRUFBRSxFQUFDO2dDQUN0QixZQUFZLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztnQ0FDakMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDOzZCQUNyQzt5QkFDRjs2QkFDRzs0QkFDRixZQUFZLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDOzRCQUNyQixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7eUJBQ3JDO3FCQUVGO2lCQUNGO2FBRUE7WUFHRCxJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7WUFDdkIsS0FBSSxJQUFJLFVBQVUsSUFBSSxZQUFZLEVBQUM7Z0JBQ2pDLElBQUksWUFBWSxHQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7Z0JBQy9CLEtBQUksSUFBSSxLQUFLLElBQUksZ0JBQWdCLEVBQUM7b0JBRTlCLElBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFHLFVBQVUsRUFBQzt3QkFDeEIsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDNUI7aUJBRUo7Z0JBQ0QsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNqQztZQUVELElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUNyQixpQ0FBaUM7WUFDakMsS0FBSSxJQUFJLE9BQU8sSUFBSSxhQUFhO2dCQUM5QixXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRzVCLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzFCLEtBQUksSUFBSSxTQUFTLElBQUksWUFBWSxFQUNqQztnQkFFRSxJQUFHLFNBQVMsS0FBSyxPQUFPLEVBQUM7b0JBQ3ZCLGdCQUFnQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDbEM7YUFFRjtZQUtELElBQUksTUFBTSxHQUFHLEVBQUUsQ0FBQyxRQUFRLENBQUM7Z0JBQ3ZCLE1BQU0sRUFBRSxTQUFTO2dCQUVqQixJQUFJLEVBQUU7b0JBRUYsT0FBTyxFQUFFLFdBQVc7b0JBRXBCLElBQUksRUFBRSxLQUFLO2lCQUNkO2dCQUNELEtBQUssRUFBQztvQkFDSixPQUFPLEVBQUMsQ0FBQyxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLENBQUM7aUJBQ3RGO2dCQUNELEdBQUcsRUFBRTtvQkFDSCxLQUFLLEVBQUUsRUFBRTtpQkFDVjtnQkFDRCxJQUFJLEVBQUU7b0JBQ0YsQ0FBQyxFQUFFO3dCQUNDLElBQUksRUFBRSxVQUFVO3dCQUNqQixVQUFVLEVBQUUsZ0JBQWdCO3FCQUM5QjtpQkFDSjthQUNGLENBQUMsQ0FBQztRQU1MLENBQUMsQ0FBQyxDQUFDO1FBSUwsU0FBUztRQUNULElBQUksQ0FBQyx3QkFBd0IsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBRXpGLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO1lBRXJCLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBRSwyQkFBMkIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLCtCQUErQixFQUFDO2dCQUNoSSxJQUFJLENBQUMsTUFBTSxHQUFFLHlCQUF5QixDQUFBO2FBQ3ZDO2lCQUFLLElBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsSUFBRSwyQkFBMkIsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixJQUFFLCtCQUErQixFQUFDO2dCQUN0SSxJQUFJLENBQUMsTUFBTSxHQUFFLHNCQUFzQixDQUFBO2FBQ3BDO2lCQUFJO2dCQUNILElBQUksQ0FBQyxNQUFNLEdBQUcsb0JBQW9CLENBQUM7YUFDcEM7WUFDRCxJQUFJLGFBQWEsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQ2xDLGFBQWEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQztnQkFDN0IsT0FBTyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLElBQUksTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQzVGLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxNQUFNLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUV2QixLQUFJLElBQUksTUFBTSxJQUFJLGFBQWEsRUFBQztnQkFDOUIsSUFBSSxHQUFHLEdBQUcsTUFBTSxDQUFDO2dCQUNqQixJQUFJLEdBQUcsR0FBRyxFQUFFLENBQUM7Z0JBQ2IsdUJBQXVCO2dCQUN2QixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDdkIsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQ3BCLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNuQixHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDM0I7WUFDRCxJQUFJLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsRUFBRTtnQkFDaEQsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDM0IsT0FBTyxJQUFJLENBQUE7aUJBQ1o7WUFDSCxDQUFDLENBQUMsQ0FBQTtZQUdGLElBQUksZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1lBQzFCLEtBQUksSUFBSSxNQUFNLElBQUksYUFBYSxFQUFDO2dCQUNoQyxLQUFJLElBQUksU0FBUyxJQUFJLFlBQVksRUFBQztvQkFDaEMsSUFBRyxTQUFTLElBQUUsT0FBTyxFQUFDO3dCQUN0QixJQUFJLGFBQWEsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDakMsSUFBRyxTQUFTLElBQUUsTUFBTSxDQUFDLEtBQUssRUFBQzs0QkFDeEIsSUFBRyxNQUFNLENBQUMsS0FBSyxJQUFJLEVBQUUsRUFBQztnQ0FDckIsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBQ2pDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQzs2QkFDdEM7eUJBQ0Y7NkJBQ0c7NEJBQ0YsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDdEIsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO3lCQUN0QztxQkFFRjtpQkFDRjthQUVBO1lBR0QsSUFBSSxhQUFhLEdBQUcsRUFBRSxDQUFDO1lBQ3ZCLEtBQUksSUFBSSxXQUFXLElBQUksWUFBWSxFQUFDO2dCQUNsQyxJQUFJLFlBQVksR0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUNoQyxLQUFJLElBQUksS0FBSyxJQUFJLGdCQUFnQixFQUFDO29CQUU5QixJQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBRyxXQUFXLEVBQUM7d0JBQ3pCLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7cUJBQzVCO2lCQUVKO2dCQUNELGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDakM7WUFFRCxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDckIsaUNBQWlDO1lBQ2pDLEtBQUksSUFBSSxPQUFPLElBQUksYUFBYTtnQkFDaEMsV0FBVyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUcxQixJQUFJLGdCQUFnQixHQUFHLEVBQUUsQ0FBQztZQUMxQixLQUFJLElBQUksU0FBUyxJQUFJLFlBQVksRUFDakM7Z0JBRUUsSUFBRyxTQUFTLEtBQUssT0FBTyxFQUFDO29CQUN2QixnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ2xDO2FBRUY7WUFFRCxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUMsUUFBUSxDQUFDO2dCQUNyQixNQUFNLEVBQUUsU0FBUztnQkFDakIsSUFBSSxFQUFFO29CQUVGLE9BQU8sRUFBRSxXQUFXO29CQUVwQixJQUFJLEVBQUUsS0FBSztpQkFDZDtnQkFDRCxHQUFHLEVBQUU7b0JBQ0gsS0FBSyxFQUFFLEVBQUU7aUJBQ1Y7Z0JBQ0QsS0FBSyxFQUFDO29CQUNKLE9BQU8sRUFBQyxDQUFDLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQztpQkFDdEY7Z0JBQ0QsSUFBSSxFQUFFO29CQUNGLENBQUMsRUFBRTt3QkFDQyxJQUFJLEVBQUUsVUFBVTt3QkFDakIsVUFBVSxFQUFFLGdCQUFnQjtxQkFDOUI7aUJBQ0o7YUFDSixDQUFDLENBQUM7UUFHUCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxZQUFZO1FBQ1Ysd0NBQXdDO1FBQ3hDLHlEQUF5RDtRQUN6RCxJQUFJLEdBQUcsR0FBRztZQUNSLEdBQUcsRUFBRyxvQ0FBb0M7WUFDMUMsRUFBRSxFQUFHLGNBQWM7U0FDcEIsQ0FBQTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsR0FBRyxFQUFFLG9DQUFvQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7UUFDN0UsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFO1lBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQSxDQUFDLHdCQUF3QjtRQUMzQyxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFBQSxDQUFDO0lBR0YsUUFBUSxDQUFDLEdBQUcsRUFBRSxXQUFXO1FBQ3ZCLElBQUksR0FBRyxHQUFHO1lBQ1IsR0FBRyxFQUFHLG9DQUFvQztZQUMxQyxFQUFFLEVBQUcsY0FBYztTQUNwQixDQUFBO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxvQ0FBb0MsQ0FBQyxDQUFDLENBQUM7UUFDN0QsNkZBQTZGO1FBQzdGLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQztRQUM1RSxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxXQUFXLENBQUM7SUFDbkYsQ0FBQzs7O1lBdGtCRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHNCQUFzQjtnQkFDaEMsNjNLQUFnRDtnQkFFaEQsVUFBVSxFQUFFLGNBQWM7O2FBQzNCOzs7O1lBN0JRLE1BQU07WUFKTixTQUFTO1lBS1QsdUJBQXVCO1lBRGYsY0FBYztZQUR0QixlQUFlO1lBRmhCLFFBQVE7WUFVUCxjQUFjOzs7c0JBMkJwQixNQUFNO3dCQWlDTixTQUFTLFNBQUMsWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uLCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU2VsZWN0aW9uTW9kZWwgfSBmcm9tICdAYW5ndWxhci9jZGsvY29sbGVjdGlvbnMnO1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nLCBNYXRTaWRlbmF2IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQge0xvY2F0aW9ufSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5cclxuaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFJlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlIH0gZnJvbSAnLi4vcmVwb3J0LW1hbmFnZW1lbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IG1hbmFnZUF2RGV0YWlsVGFibGVDb25maWcgfSBmcm9tICcuLi8uLi90YWJsZV9jb25maWcnO1xyXG5pbXBvcnQgeyBBdm1JbXBvcnRGb3JtQ29tcG9uZW50IH0gZnJvbSAnLi4vaW1wb3J0LWZvcm0vaW1wb3J0LWZvcm0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgZnVzZUFuaW1hdGlvbnMgfSBmcm9tICcuLi8uLi9AZnVzZS9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCAqIGFzIGMzIGZyb20gJ2MzJztcclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vX3NlcnZpY2VzL21lc3NhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBHcm91cERlc2NyaXB0b3IsIERhdGFSZXN1bHQsIHByb2Nlc3MsU3RhdGUgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tZGF0YS1xdWVyeSc7XHJcbmltcG9ydCB7IEdyaWREYXRhUmVzdWx0LCBQYWdlQ2hhbmdlRXZlbnQsU2VsZWN0QWxsQ2hlY2tib3hTdGF0ZSxEYXRhU3RhdGVDaGFuZ2VFdmVudCB9IGZyb20gJ0Bwcm9ncmVzcy9rZW5kby1hbmd1bGFyLWdyaWQnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBtYW5hZ2VBdnNEZXRhaWwge1xyXG4gIHNvZG5hbWU6IHN0cmluZztcclxuICBlbnRpdGxlbWVudDogc3RyaW5nO1xyXG4gIHVzZXJuYW1lOiBzdHJpbmc7XHJcbiAgZmlyc3RuYW1lOiBzdHJpbmc7XHJcbiAgbGFzdG5hbWU6IHN0cmluZztcclxuICByZXNwb25zaWJpbHR5bmFtZTogc3RyaW5nOyBcclxufVxyXG5cclxuXHJcbnZhciBNT05USCA9IHsgSkFOOiAwLCBGRUI6IDEsIE1BUjogMiwgQVBSOiAzLCBNQVk6IDQsIEpVTjogNSwgSlVMOiA2LCBBVUc6IDcsIFNFUDogOCwgT0NUOiA5LCBOT1Y6IDEwLCBERUM6IDExIH07XHJcbnZhciBNT05USDIgPSB7IGphbjogMCwgZmViOiAxLCBtYXI6IDIsIGFwcjogMywgbWF5OiA0LCBqdW46IDUsIGp1bDogNiwgYXVnOiA3LCBzZXA6IDgsIG9jdDogOSwgbm92OiAxMCwgZGVjOiAxMSB9O1xyXG4vLyBkZWNsYXJlIHZhciBjMzogYW55O1xyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ2FwcC1tYW5hZ2UtYXYtZGV0YWlsJyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbWFuYWdlLWF2LWRldGFpbC5jb21wb25lbnQuaHRtbCcsXHJcbiAgc3R5bGVVcmxzOiBbJy4vbWFuYWdlLWF2LWRldGFpbC5jb21wb25lbnQuc2NzcyddLFxyXG4gIGFuaW1hdGlvbnM6IGZ1c2VBbmltYXRpb25zXHJcbn0pXHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIE1hbmFnZUF2RGV0YWlsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgQE91dHB1dCgpIG15RXZlbnQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgbWFuYWdlQXZzbGlzdDogYW55O1xyXG4gIGRhdGFTb3VyY2UyOiBhbnk7XHJcbiAgY3VzdG9tZGF0YXNvdXJjZTogb2JqZWN0W107XHJcbiAgZGVmYXVsdGRhdGFzb3VyY2VJZDogc3RyaW5nO1xyXG4gIGRpc3BsYXlOYW06c3RyaW5nO1xyXG4gIHNlbGVjdGVkVGFibGVIZWFkZXI9W107XHJcbiAgZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbXHJcbiAgICAnZW50aXRsZW1lbnQnLFxyXG4gICAgJ3VzZXJuYW1lJyxcclxuICAgICdmaXJzdG5hbWUnLFxyXG4gICAgJ2xhc3RuYW1lJyxcclxuICAgICdyZXNwb25zaWJpbHR5bmFtZSdcclxuICBdO1xyXG4gIHNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxtYW5hZ2VBdnNEZXRhaWw+KHRydWUsIFtdKTtcclxuICBkaWFsb2dSZWY6IGFueTtcclxuICBuYXZfcG9zaXRpb246IHN0cmluZyA9ICdlbmQnO1xyXG4gIHF1ZXJ5UGFyYW1zOiBhbnkgPSB7fTtcclxuICBsaW1pdDogbnVtYmVyID0gMTA7XHJcbiAgb2Zmc2V0OiBudW1iZXIgPSAwO1xyXG4gIGxlbmd0aDogbnVtYmVyO1xyXG4gIHRhYmxlQ29uZmlnOiBhbnk7XHJcblxyXG4gXHJcbiAgY2hhcnREYXRhOiBhbnk7XHJcbiAgY2hhcnREYXRhMjogYW55O1xyXG4gIGNoYXJ0RGF0YTM6IGFueTtcclxuXHJcbiAgY2hhcmFjdGVyczogYW55O1xyXG4gIHNldHRpbmdzOiBhbnk7XHJcbiAgdGl0bGUxOiBzdHJpbmc7XHJcbiAgdGl0bGUyOiBzdHJpbmc7XHJcbiAgdGl0bGUzOiBzdHJpbmc7XHJcbiAgQFZpZXdDaGlsZChNYXRQYWdpbmF0b3IpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG5cclxuXHJcbiAgcm9sZUNoYXJ0RGF0YTogYW55ID0gW107XHJcbiAgdXNlckNoYXJ0RGF0YTogYW55ID0gW107XHJcbiAgLy8gQmFyXHJcbiAgc2hvd1hBeGlzID0gdHJ1ZTtcclxuICBzaG93WUF4aXMgPSB0cnVlO1xyXG4gIGdyYWRpZW50ID0gZmFsc2U7XHJcbiAgc2hvd0xlZ2VuZCA9IHRydWU7XHJcbiAgc2hvd1hBeGlzTGFiZWwgPSB0cnVlO1xyXG4gIHNob3dZQXhpc0xhYmVsID0gdHJ1ZTtcclxuICB5QXhpc0xhYmVsID0gJ1BvcHVsYXRpb24nO1xyXG4gIHBhcm1zT2JqOmFueTtcclxuXHJcbiAgY29sb3JTY2hlbWUgPSB7XHJcbiAgICBkb21haW46IFsnIzI4YTNkZCddXHJcbiAgfTtcclxuXHJcbiAgdXJsUGFyYW1zIDogYW55O1xyXG4gIC8vIHBpZVxyXG4gIHNob3dMYWJlbHMgPSB0cnVlO1xyXG4gIHNob3dQaWVMZWdlbmQgPSBmYWxzZTtcclxuICBleHBsb2RlU2xpY2VzID0gZmFsc2U7XHJcbiAgZG91Z2hudXQgPSBmYWxzZTtcclxuICBjb2xvclBpZVNjaGVtZSA9IHtcclxuICAgIGRvbWFpbjogWycjM2EzZjUxJywgJyMyM2I3ZTUnLCAnI2YwNTA1MCcsICcjMjdjMjRjJywgJyNlNjAwNTAnLCAnIzI5ODBiOScsICcjNzI2NmJhJywgJyMyYWNjZGInXVxyXG4gIH07XHJcbiAgY29sb3JDaGFydDEgOiBhbnk7XHJcbiAgcGllQ2hhcnRWaWV3IDogYW55ID0gWzMwMCwgMjUwXTtcclxuICBwaWVDaGFydExhYmVsOiB7IHRpdGxlOiBhbnk7IG5vZGF0YTogYW55OyB9O1xyXG4gIGNoYXJ0MkRhdGE6IGFueVtdO1xyXG4gIFxyXG4gIGtlbmRvR3JpZERhdGEgOiBHcmlkRGF0YVJlc3VsdDtcclxuICBlbmFibGVGaWx0ZXI6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwdWJsaWMgX2RhdGE6IGFueVtdO1xyXG4gIHB1YmxpYyBpbmZvID0gdHJ1ZTtcclxuICBwdWJsaWMgdHlwZTogXCJudW1lcmljXCIgfCBcImlucHV0XCIgPSBcIm51bWVyaWNcIjtcclxuICBwdWJsaWMgcGFnZVNpemVzID0gW3sgdGV4dDogMTAsIHZhbHVlOiAxMCB9LCB7IHRleHQ6IDI1LCB2YWx1ZTogMjUgfSwgeyB0ZXh0OiA1MCwgdmFsdWU6IDUwIH0sIHsgdGV4dDogMTAwLCB2YWx1ZTogMTAwIH1dO1xyXG4gIHB1YmxpYyBwcmV2aW91c05leHQgPSB0cnVlO1xyXG4gIHN0YXRlOiBTdGF0ZSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlcjogUm91dGVyLCBwcml2YXRlIF9tYXREaWFsb2c6IE1hdERpYWxvZyxcclxuICAgIHByaXZhdGUgX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlOiBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgYWN0aXZlUm91dGU6QWN0aXZhdGVkUm91dGUsXHJcbiAgICBwcml2YXRlIHNuYWNrQmFyU2VydmljZTogU25hY2tCYXJTZXJ2aWNlLHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZSA6TWVzc2FnZVNlcnZpY2VcclxuICApIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKFwiYXYgZGV0YWlsIFVSTCBQYXJtcyAqKioqKioqKioqKioqKlwiLHRoaXMuYWN0aXZlUm91dGUuc25hcHNob3QucGFyYW1zKVxyXG4gICAgdGhpcy5wYXJtc09iaiA9IHRoaXMuYWN0aXZlUm91dGUuc25hcHNob3QucGFyYW1zO1xyXG4gIH1cclxuXHJcbiAgbmdPbkluaXQoKSB7XHJcbiAgICB0aGlzLnN0YXRlPXtcclxuICAgICAgc2tpcDogdGhpcy5vZmZzZXQsXHJcbiAgICAgIHRha2U6IHRoaXMubGltaXRcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKHRoaXMucm91dGVyLnVybCxcIj4+PlRISVNcIilcclxuICAgIGxldCB1cmxTdHJpbmcgPSBkZWNvZGVVUklDb21wb25lbnQodGhpcy5yb3V0ZXIudXJsKTtcclxuICAgIHRoaXMudXJsUGFyYW1zID0gdXJsU3RyaW5nLnNwbGl0KCcvJyk7XHJcbiAgICB0aGlzLmRpc3BsYXlOYW09dGhpcy51cmxQYXJhbXNbNV07IC8vdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlWydkaXNwbGF5TmFtZSddO1xyXG4gICAgdGhpcy5nZXRkYXRhc291cmNlKCk7XHJcbiAgICB0aGlzLmxvYWRUYWJsZUNvbHVtbigpO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtcy5kYXRhc291cmNlMiA9IHRoaXMudXJsUGFyYW1zWzNdOyAvL3RoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5kYXRhc291cmNlSWQ7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zLmNvbGxlY3Rpb25EZXRhaWwgPSB0aGlzLnVybFBhcmFtc1s0XTsvL3RoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5jb2xsZWN0aW9uRGV0YWlsO1xyXG4gICAgc2V0VGltZW91dCgoKT0+eyAgICAvLzw8PC0tLSAgICB1c2luZyAoKT0+IHN5bnRheFxyXG4gICAgICB0aGlzLm9uTG9hZENoYXJ0KCk7XHJcbiB9LCA1MDApO1xyXG4gICAgXHJcbiAgfVxyXG4gIGRhdGFTdGF0ZUNoYW5nZShzdGF0ZTogRGF0YVN0YXRlQ2hhbmdlRXZlbnQpOiB2b2lkIHtcclxuICAgIHRoaXMuc3RhdGU9IHN0YXRlO1xyXG4gICAgdGhpcy5hcHBseVRhYmxlU3RhdGUodGhpcy5zdGF0ZSk7XHJcbiAgfVxyXG4gIGFwcGx5VGFibGVTdGF0ZShzdGF0ZTogU3RhdGUpOiB2b2lkIHtcclxuICAgIHRoaXMua2VuZG9HcmlkRGF0YSA9IHByb2Nlc3ModGhpcy5fZGF0YSwgc3RhdGUpO1xyXG4gIH1cclxuICBjaGFuZ2VQYWdlKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCI+Pj4gRVZFTlQgQ2hhbmdlIFBhZ2VcIilcclxuICAgIHRoaXMucXVlcnlQYXJhbXNbXCJvZmZzZXRcIl0gPSBldmVudC5za2lwO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtc1tcImxpbWl0XCJdID0gdGhpcy5saW1pdDtcclxuICAgIHRoaXMub2Zmc2V0ID0gZXZlbnQuc2tpcDtcclxuICB9XHJcbiAgb25DbGljayhhY3Rpb24pe1xyXG4gICAgaWYoYWN0aW9uPT0ncmVmcmVzaCcpXHJcbiAgICB7XHJcbiAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgIH1cclxuICB9XHJcbiAgZW5hYmxlRmlsdGVyT3B0aW9ucygpIHtcclxuICAgIHRoaXMuZW5hYmxlRmlsdGVyID0gIXRoaXMuZW5hYmxlRmlsdGVyOyAgIFxyXG4gICB9XHJcbiAgYmFja0NsaWNrZWQoKSB7XHJcbiAgICB0aGlzLmxvY2F0aW9uLmJhY2soKTtcclxuICAgIGxldCBvYmogPSB7XHJcbiAgICAgIGlkIDogXCJtYW5hZ2VBdm1cIlxyXG4gICAgfTtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgc2VsZi5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG4gICAgfSwxMDApXHJcbiAgfVxyXG4gIHBhbmVsT3BlblN0YXRlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG5cdHRhYmxlU2V0dGluZygpIHtcclxuXHRcdHRoaXMucGFuZWxPcGVuU3RhdGUgPSAhdGhpcy5wYW5lbE9wZW5TdGF0ZVxyXG5cdH1cclxuXHJcbiAgZ2V0ZGF0YXNvdXJjZSgpIHtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldEFsbERhdGFzb3VyY2VzKCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgIHRoaXMuY3VzdG9tZGF0YXNvdXJjZSA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlcztcclxuICAgICAgdGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLl9pZDtcclxuICAgICAgdGhpcy5vbkxvYWRtYW5hZ2VBdm1EZXRhaWwodGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkKTtcclxuICAgIH0pXHJcbiAgfVxyXG4gIHVwZGF0ZVRhYmxlKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCItLS0tZXZlbnRcIilcclxuXHRcdGxldCBzZWxlY3RlZEhlYWRlciA9IHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlcjtcclxuICAgIHRoaXMudGFibGVDb25maWcuZm9yRWFjaChmdW5jdGlvbihpdGVtKXtcclxuICAgICAgICBpZihzZWxlY3RlZEhlYWRlci5pbmRleE9mKGl0ZW0udmFsdWUpID49IDApIHtcclxuICAgICAgICAgICAgaXRlbS5pc2FjdGl2ZSA9IHRydWU7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIGl0ZW0uaXNhY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9KVxyXG5cclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnbWFuYWdlQXZzRGV0YWlsJyk7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ21hbmFnZUF2c0RldGFpbCcsIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVDb25maWcpKTtcclxuICAgICAgICB0aGlzLmxvYWRUYWJsZUNvbHVtbigpOyAgICBcclxuXHJcblx0XHQvLyB2YXIgaW5kZXggPSB0aGlzLnRhYmxlTmFtZXNlYXJjaChldmVudC5zb3VyY2UudmFsdWUsIHRoaXMudGFibGVDb25maWcpO1xyXG5cdFx0Ly8gaWYgKGluZGV4ID49IDApIHtcclxuXHRcdC8vIFx0aWYgKGV2ZW50LmNoZWNrZWQpIHtcclxuXHRcdC8vIFx0XHRsZXQgYWN0aXZlID0ge1xyXG5cdFx0Ly8gXHRcdFx0dmFsdWU6IGV2ZW50LnNvdXJjZS52YWx1ZSxcclxuXHRcdC8vIFx0XHRcdG5hbWU6IGV2ZW50LnNvdXJjZS5uYW1lLFxyXG5cdFx0Ly8gXHRcdFx0aXNhY3RpdmU6IHRydWVcclxuXHRcdC8vIFx0XHR9O1xyXG5cdFx0Ly8gXHRcdGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHQvLyBcdFx0XHR0aGlzLnRhYmxlQ29uZmlnLnNwbGljZShpbmRleCwgMSwgYWN0aXZlKTtcclxuXHRcdC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwibWFuYWdlQXZzRGV0YWlsXCIpO1xyXG5cdFx0Ly8gXHRcdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJtYW5hZ2VBdnNEZXRhaWxcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy50YWJsZUNvbmZpZykpO1xyXG5cdFx0Ly8gXHRcdH1cclxuXHRcdC8vIFx0fSBlbHNlIHtcclxuXHRcdC8vIFx0XHRsZXQgaW5hY3RpdmUgPSB7XHJcblx0XHQvLyBcdFx0XHR2YWx1ZTogZXZlbnQuc291cmNlLnZhbHVlLFxyXG5cdFx0Ly8gXHRcdFx0bmFtZTogZXZlbnQuc291cmNlLm5hbWUsXHJcblx0XHQvLyBcdFx0XHRpc2FjdGl2ZTogZmFsc2VcclxuXHRcdC8vIFx0XHR9O1xyXG5cdFx0Ly8gXHRcdGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHQvLyBcdFx0XHR0aGlzLnRhYmxlQ29uZmlnLnNwbGljZShpbmRleCwgMSwgaW5hY3RpdmUpO1xyXG5cdFx0Ly8gXHRcdFx0bG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJtYW5hZ2VBdnNEZXRhaWxcIik7XHJcblx0XHQvLyBcdFx0XHRsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcIm1hbmFnZUF2c0RldGFpbFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlQ29uZmlnKSk7XHJcblx0XHQvLyBcdFx0fVxyXG5cdFx0Ly8gXHR9XHJcblx0XHQvLyB9XHJcblx0XHQvLyB0aGlzLmxvYWRUYWJsZUNvbHVtbigpO1xyXG4gIH1cclxuICBcclxuXHR0YWJsZU5hbWVzZWFyY2gobmFtZUtleSwgbXlBcnJheSkge1xyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBteUFycmF5Lmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdGlmIChteUFycmF5W2ldLnZhbHVlID09PSBuYW1lS2V5KSB7XHJcblx0XHRcdFx0cmV0dXJuIGk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG4gIGxvYWRUYWJsZUNvbHVtbigpIHtcclxuICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IFtdO1xyXG4gICAgdGhpcy50YWJsZUNvbmZpZyA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oJ21hbmFnZUF2c0RldGFpbCcpKTtcclxuICAgIGlmICh0aGlzLnRhYmxlQ29uZmlnKSB7XHJcbiAgICAgIGZvciAobGV0IGNvbHVtbnMgb2YgdGhpcy50YWJsZUNvbmZpZykge1xyXG4gICAgICAgIGlmIChjb2x1bW5zLmlzYWN0aXZlKSB7XHJcblx0XHRcdFx0XHR0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuICAgICAgICAgLy8gdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcbiAgICAgICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucy5wdXNoKGNvbHVtbnMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy50YWJsZUNvbmZpZyA9IG1hbmFnZUF2RGV0YWlsVGFibGVDb25maWc7XHJcbiAgICAgIGZvciAobGV0IGNvbHVtbnMgb2YgdGhpcy50YWJsZUNvbmZpZykge1xyXG4gICAgICAgIGlmIChjb2x1bW5zLmlzYWN0aXZlKSB7XHJcblx0XHRcdFx0XHR0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuICAgICAgICAgIC8vdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcbiAgICAgICAgICB0aGlzLmRpc3BsYXllZENvbHVtbnMucHVzaChjb2x1bW5zKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGdldERhdGFzb3VyY2VJZChpZCkge1xyXG4gICAgdGhpcy5vbkxvYWRtYW5hZ2VBdm1EZXRhaWwoaWQpXHJcbiAgfVxyXG5cclxuICBvbkxvYWRtYW5hZ2VBdm1EZXRhaWwoaWQpIHtcclxuICAgIHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCA9IGlkXHJcbiAgICBcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldGFsbG1hbmFnZUF2RGV0YWlsKHRoaXMucXVlcnlQYXJhbXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIHRoaXMubWFuYWdlQXZzbGlzdCA9IHJlc3BvbnNlLmRhdGE7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZTIgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMubWFuYWdlQXZzbGlzdCk7XHJcbiAgICAgIHRoaXMubGVuZ3RoID0gcmVzcG9uc2UudG90YWw7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZTIucGFnaW5hdG9yID0gdGhpcy5wYWdpbmF0b3I7XHJcbiAgIFxyXG5cclxuICAgICAgdGhpcy5fZGF0YT10aGlzLm1hbmFnZUF2c2xpc3Q7XHJcbiAgICAgIHRoaXMuYXBwbHlUYWJsZVN0YXRlKHRoaXMuc3RhdGUpO1xyXG5cclxuICAgIH0pO1xyXG4gIH1cclxuXHJcblxyXG4gIG9uTG9hZENoYXJ0KCkge1xyXG4gICAgXHJcbiAgICAvL0NoYXJ0IDFcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldENhdGVnb3J5UGVyY2VudGFnZSh0aGlzLnF1ZXJ5UGFyYW1zKS5zdWJzY3JpYmUocmVzcG9uc2UgPT4ge1xyXG4gICAgICBcclxuICAgICAgdGhpcy5jaGFydERhdGEgPSBbXTtcclxuICAgICAgaWYodGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxpZXJJbnZvaWNlZGV0YWlscycgfHwgdGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxpZXJJbnZvaWNlZGV0YWlscy1lYnMnKXtcclxuICAgICAgICB0aGlzLnRpdGxlMSA9J0ludm9pY2UgVHlwZSdcclxuICAgICAgfWVsc2UgaWYodGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxlaXJQYXltZW50ZGV0YWlscycgfHwgdGhpcy5wYXJtc09iai5jb2xsZWN0aW9uRGV0YWlsPT0nYXZtU3VwcGxlaXJQYXltZW50ZGV0YWlscy1lYnMnKXtcclxuICAgICAgICB0aGlzLnRpdGxlMSA9J1RvcCA1IFZlbmRvcnMnXHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIHRoaXMudGl0bGUxID0gXCJKb3VybmFscyBieSBDYXRlZ29yeVwiO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICBcclxuICAgICAgZm9yKGxldCByZXN1bHQgb2YgcmVzcG9uc2UuZGF0YSl7XHJcbiAgICAgICAgdmFyIG9iaiA9IHJlc3VsdDtcclxuICAgICAgICB2YXIgaWRzID0gW107XHJcbiAgICAgICAgaWRzLnB1c2gob2JqLm5hbWUpO1xyXG4gICAgICAgIGlkcy5wdXNoKG9iai5wZXJjZW50YWdlKTtcclxuICAgICAgICB0aGlzLmNoYXJ0RGF0YS5wdXNoKGlkcyk7XHJcbiAgICAgIH1cclxuICAgICAgIFxyXG4gICAgICAvL3RoaXMuY2hhcnREYXRhID0gW1snQWRqdXN0bWVudHNzcycsIDIwXSxbJ0FjY3J1YWwnLCAxMF0sWydSZWNsYXNzJywgNDVdLFsnT3RoZXInLCAyNV1dO1xyXG4gICAgICBsZXQgY2hhcnQxID0gYzMuZ2VuZXJhdGUoe1xyXG4gICAgICAgIGJpbmR0bzogJyNjaGFydDEnLFx0XHJcbiAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgY29sdW1ucyA6IHRoaXMuY2hhcnREYXRhLFxyXG4gICAgICAgICAgLy9qc29uIDogdGhpcy5jaGFydERhdGEsXHJcbiAgICAgICAgICB0eXBlIDogJ3BpZScsXHJcbiAgICAgICAgIH0sXHJcbiAgICAgICBcclxuICAgICAgICAgY29sb3I6e1xyXG4gICAgICAgICAgIHBhdHRlcm46WycjMzk2QUIxJywgJyNEQTdDMzAnLCAnIzNFOTY1MScsICcjQ0MyNTI5JywgJyM1MzUxNTQnLCAnIzZCNEM5QScsICcjOTQ4QjNEJ11cclxuICAgICAgICAgfSxcclxuICAgICAgIGxlZ2VuZDoge1xyXG4gICAgICAgICAgICAgc2hvdzogdHJ1ZVxyXG4gICAgICAgICAgIH0sXHJcbiAgXHJcbiAgICAgICAgfSk7XHJcbiAgICAgIFxyXG4gICAgfSk7XHJcbiAgICBcclxuXHJcbiAgICAgICAgLy9DaGFydCAyXHJcbiAgICAgICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0VG9wQW1vdW50KHRoaXMucXVlcnlQYXJhbXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIFxyXG4gICAgICAgICAgdGhpcy5jaGFydERhdGEyID0gW107XHJcbiAgICAgICAgICBpZih0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGllckludm9pY2VkZXRhaWxzJyB8fCB0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGllckludm9pY2VkZXRhaWxzLWVicycgKXtcclxuICAgICAgICAgICAgdGhpcy50aXRsZTIgPSdUb3AgNSB1c2VycyB3aXRoIGFtb3VudCdcclxuICAgICAgICAgIH1lbHNlIGlmKHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsZWlyUGF5bWVudGRldGFpbHMnIHx8IHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsZWlyUGF5bWVudGRldGFpbHMtZWJzJyApe1xyXG4gICAgICAgICAgICB0aGlzLnRpdGxlMiA9J1RvcCA1IFVzZXJzIHdpdGggQW1vdW50IFBhaWQnXHJcbiAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgdGhpcy50aXRsZTIgPSBcIlRvcCA1IEpvdXJuYWxzXCI7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIHJlc3BvbnNlYXJyYXkgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgICAgcmVzcG9uc2VhcnJheS5zb3J0KGZ1bmN0aW9uIChhLCBiKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIGEueWVhciAtIGIueWVhciB8fCBNT05USFthLm1vbnRoXSAtIE1PTlRIW2IubW9udGhdO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICAvKlxyXG4gICAgICAgICAgMDoge2pvdXJuYWxuYW1lOiBcIkNvbnZlcnNpZSBBZGotMDJcIiwgbW9udGg6IFwiTUFSXCIsIHllYXI6IFwiMjAwNVwiLCBhbW91bnQ6IDEwMDAwfVxyXG4gICAgICAgICAgMToge2pvdXJuYWxuYW1lOiBcIkFCTiAwNC1GRUItMDUgQjAyNVwiLCBtb250aDogXCJGRUJcIiwgeWVhcjogXCIyMDA1XCIsIGFtb3VudDogOTUwMH1cclxuICAgICAgICAgIDI6IHtqb3VybmFsbmFtZTogXCJEUkkgSU5UIEVYUCBDb3JyZWN0aW9uOiAxMi1NQVktMDUgMTQ6NDM6MjVcIiwgbW9udGg6IFwiTUFZXCIsIHllYXI6IFwiMjAwNVwiLCBhbW91bnQ6IDkwMDB9XHJcbiAgICAgICAgICAzOiB7am91cm5hbG5hbWU6IFwiQVBSMDAxNFwiLCBtb250aDogXCJNQVlcIiwgeWVhcjogXCIyMDA1XCIsIGFtb3VudDogODUwMH1cclxuICAgICAgICAgIDQ6IHtqb3VybmFsbmFtZTogXCJQdXJjaGFzZSBJbnZvaWNlcyBVU0RcIiwgbW9udGg6IFwiRkVCXCIsIHllYXI6IFwiMjAwNVwiLCBhbW91bnQ6IDgwMDB9XHJcbiAgICAgICAgICAqL1xyXG5cclxuICAgICAgICAgIC8qXHJcbiAgICAgICAgICB0aGlzLmNoYXJ0RGF0YTIgPSBbXHJcbiAgICAgICAgICBbJ21vbnRoJywnMjAxOS0wMy0yNScsICcyMDE5LTA0LTI1J10sXHJcbiAgICAgICAgICBbJ0NvbnZlcnNpZSBBZGotMDInLCAyMDAwLCAyNTAwXSxcclxuICAgICAgICAgIFsnS0FTIEZFQi0wNScsIDMwMDAsIDM1MDBdLFxyXG4gICAgICAgICAgWydDb252ZXJzaWUgQXVnLTAzIENvbnZlcnNpb24gRVVSJywgNDAwMCwgNDUwMF0sXHJcbiAgICAgICAgICBbJ01FTU8gMzEtTUFSLTA1IE0wMjcnLCA0NTAwLDI1MDBdLFxyXG4gICAgICAgICAgWydQdXJjaGFzZSBJbnZvaWNlcyBVU0QnLCA1MDAwLCAxMDAwXVxyXG4gICAgICAgICAgXTtcclxuICAgICAgICAgICovXHJcbiAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIG1vbnRocyA9IFsnbW9udGgnXTtcclxuICAgICAgICAgIHZhciBqbmFtZXMgPSBbXTtcclxuICAgICAgICAgIGZvcihsZXQgcmVzdWx0IG9mIHJlc3BvbnNlYXJyYXkpe1xyXG4gICAgICAgICAgICB2YXIgb2JqID0gcmVzdWx0O1xyXG4gICAgICAgICAgICB2YXIgaWRzID0gW107XHJcbiAgICAgICAgICAgIC8vdXNlcm5hbWUgYXJyYXkgb2JqZWN0XHJcbiAgICAgICAgICAgIG1vbnRocy5wdXNoKG9iai5tb250aCk7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai5qb3VybmFsbmFtZSk7XHJcbiAgICAgICAgICAgIGpuYW1lcy5wdXNoKG9iai5qb3VybmFsbmFtZSk7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai5tb250aCk7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai55ZWFyKTtcclxuICAgICAgICAgICAgaWRzLnB1c2gob2JqLmFtb3VudCk7XHJcbiAgICAgICAgICAgIHRoaXMuY2hhcnREYXRhMi5wdXNoKGlkcyk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBsZXQgdW5pcXVlbW9udGhzID0gbW9udGhzLmZpbHRlcigoZWxlbSwgaSwgYXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcnIuaW5kZXhPZihlbGVtKSA9PT0gaSkge1xyXG4gICAgICAgICAgICAgIHJldHVybiBlbGVtXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcblxyXG4gICAgICAgICAgLy91bmlxdWUgam5hbWVzXHJcbiAgICAgICAgICBsZXQgdW5pcXVlam5hbWVzID0gam5hbWVzLmZpbHRlcigoZWxlbSwgaSwgYXJyKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChhcnIuaW5kZXhPZihlbGVtKSA9PT0gaSkge1xyXG4gICAgICAgICAgICAgIHJldHVybiBlbGVtXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICBcclxuICAgICAgICAgIHZhciBqb3VybmFsYXJyYXlzZXRzID0gW107XHJcbiAgICAgICAgICBmb3IobGV0IHJlc3VsdCBvZiByZXNwb25zZWFycmF5KXtcclxuICAgICAgICAgIGZvcihsZXQgZWFjaG1vbnRoIG9mIHVuaXF1ZW1vbnRocyl7XHJcbiAgICAgICAgICAgIGlmKGVhY2htb250aCE9J21vbnRoJyl7XHJcbiAgICAgICAgICAgIHZhciBqb3VybmFsbmFtZXMgPSBbcmVzdWx0LmpvdXJuYWxuYW1lXTsgIFxyXG4gICAgICAgICAgICAgIGlmKGVhY2htb250aD09cmVzdWx0Lm1vbnRoKXtcclxuICAgICAgICAgICAgICAgICBpZihyZXN1bHQuYW1vdW50ICE9ICcnKXtcclxuICAgICAgICAgICAgICAgICAgam91cm5hbG5hbWVzLnB1c2gocmVzdWx0LmFtb3VudCk7XHJcbiAgICAgICAgICAgICAgICAgIGpvdXJuYWxhcnJheXNldHMucHVzaChqb3VybmFsbmFtZXMpOyBcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgZWxzZXtcclxuICAgICAgICAgICAgICAgIGpvdXJuYWxuYW1lcy5wdXNoKDApO1xyXG4gICAgICAgICAgICAgICAgam91cm5hbGFycmF5c2V0cy5wdXNoKGpvdXJuYWxuYW1lcyk7IFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgXHJcbiAgICAgICAgICB2YXIgZmluYWxhcnJheXNldCA9IFtdO1xyXG4gICAgICAgICAgZm9yKGxldCBlYWNoam5hbWVzIG9mIHVuaXF1ZWpuYW1lcyl7XHJcbiAgICAgICAgICAgIHZhciBuZXdqcm5hcnJheXM9W2VhY2hqbmFtZXNdO1xyXG4gICAgICAgICAgIGZvcihsZXQgZW50cnkgb2Ygam91cm5hbGFycmF5c2V0cyl7XHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICBpZihlbnRyeVswXT09PWVhY2hqbmFtZXMpe1xyXG4gICAgICAgICAgICAgICAgbmV3anJuYXJyYXlzLnB1c2goZW50cnlbMV0pO1xyXG4gICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgICAgZmluYWxhcnJheXNldC5wdXNoKG5ld2pybmFycmF5cyk7IFxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHZhciBjaGFydDJhcnJheSA9IFtdO1xyXG4gICAgICAgICAgLy9jaGFydDJhcnJheS5wdXNoKHVuaXF1ZW1vbnRocyk7XHJcbiAgICAgICAgICBmb3IobGV0IGVhY2hyb3cgb2YgZmluYWxhcnJheXNldClcclxuICAgICAgICAgICAgY2hhcnQyYXJyYXkucHVzaChlYWNocm93KTtcclxuXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIG1vbnRoY2F0ZWdvcml6ZWQgPSBbXTtcclxuICAgICAgICAgIGZvcihsZXQgbW9udGhuYW1lIG9mIHVuaXF1ZW1vbnRocylcclxuICAgICAgICAgIHtcclxuXHJcbiAgICAgICAgICAgIGlmKG1vbnRobmFtZSAhPT0gJ21vbnRoJyl7XHJcbiAgICAgICAgICAgICAgbW9udGhjYXRlZ29yaXplZC5wdXNoKG1vbnRobmFtZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICB9XHJcblxyXG5cclxuXHJcbiAgICAgICAgIFxyXG4gICAgICAgICAgdmFyIGNoYXJ0MiA9IGMzLmdlbmVyYXRlKHtcclxuICAgICAgICAgICAgYmluZHRvOiAnI2NoYXJ0MicsXHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICBjb2x1bW5zOiBjaGFydDJhcnJheSxcclxuICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgdHlwZTogJ2JhcidcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgY29sb3I6e1xyXG4gICAgICAgICAgICAgIHBhdHRlcm46WycjMzk2QUIxJywgJyNEQTdDMzAnLCAnIzNFOTY1MScsICcjQ0MyNTI5JywgJyM1MzUxNTQnLCAnIzZCNEM5QScsICcjOTQ4QjNEJ11cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgYmFyOiB7XHJcbiAgICAgICAgICAgICAgd2lkdGg6IDI1XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGF4aXM6IHtcclxuICAgICAgICAgICAgICAgIHg6IHtcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY2F0ZWdvcnknICwvLyB0aGlzIG5lZWRlZCB0byBsb2FkIHN0cmluZyB4IHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICBjYXRlZ29yaWVzOiBtb250aGNhdGVnb3JpemVkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgXHJcblxyXG5cclxuXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgXHJcblxyXG5cclxuICAgICAgLy9DaGFydCAzXHJcbiAgICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldFRvdGFsTnVtYmVyb2ZKb3VybmFsKHRoaXMucXVlcnlQYXJhbXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIFxyXG4gICAgICAgICAgdGhpcy5jaGFydERhdGEzID0gW107XHJcbiAgICAgICAgICBcclxuICAgICAgICAgIGlmKHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsaWVySW52b2ljZWRldGFpbHMnIHx8IHRoaXMucGFybXNPYmouY29sbGVjdGlvbkRldGFpbD09J2F2bVN1cHBsaWVySW52b2ljZWRldGFpbHMtZWJzJyl7XHJcbiAgICAgICAgICAgIHRoaXMudGl0bGUzID0nSW52b2ljZSBhbW91bnQgYnkgbW9udGgnXHJcbiAgICAgICAgICB9ZWxzZSBpZih0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGVpclBheW1lbnRkZXRhaWxzJyB8fCB0aGlzLnBhcm1zT2JqLmNvbGxlY3Rpb25EZXRhaWw9PSdhdm1TdXBwbGVpclBheW1lbnRkZXRhaWxzLWVicycpe1xyXG4gICAgICAgICAgICB0aGlzLnRpdGxlMyA9J0Ftb3VudCBwYWlkIGJ5IG1vbnRoJ1xyXG4gICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgIHRoaXMudGl0bGUzID0gXCJKb3VybmFscyBieSBQZXJpb2RcIjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHZhciByZXNwb25zZWFycmF5ID0gcmVzcG9uc2UuZGF0YTtcclxuICAgICAgICAgIHJlc3BvbnNlYXJyYXkuc29ydChmdW5jdGlvbiAoYSwgYikge1xyXG4gICAgICAgICAgICAgIHJldHVybiBhLnllYXIgLSBiLnllYXIgfHwgTU9OVEgyW2EubW9udGgudG9Mb3dlckNhc2UoKV0gLSBNT05USDJbYi5tb250aC50b0xvd2VyQ2FzZSgpXTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdmFyIG1vbnRocyA9IFsnbW9udGgnXTtcclxuICAgICAgICAgICBcclxuICAgICAgICAgIGZvcihsZXQgcmVzdWx0IG9mIHJlc3BvbnNlYXJyYXkpe1xyXG4gICAgICAgICAgICB2YXIgb2JqID0gcmVzdWx0O1xyXG4gICAgICAgICAgICB2YXIgaWRzID0gW107XHJcbiAgICAgICAgICAgIC8vdXNlcm5hbWUgYXJyYXkgb2JqZWN0XHJcbiAgICAgICAgICAgIG1vbnRocy5wdXNoKG9iai5tb250aCk7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai5tb250aCk7XHJcbiAgICAgICAgICAgIGlkcy5wdXNoKG9iai55ZWFyKTtcclxuICAgICAgICAgICAgaWRzLnB1c2gob2JqLmNvdW50KTtcclxuICAgICAgICAgICAgdGhpcy5jaGFydERhdGEzLnB1c2goaWRzKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGxldCB1bmlxdWVtb250aHMgPSBtb250aHMuZmlsdGVyKChlbGVtLCBpLCBhcnIpID0+IHtcclxuICAgICAgICAgICAgaWYgKGFyci5pbmRleE9mKGVsZW0pID09PSBpKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIGVsZW1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSlcclxuXHJcbiAgICAgICAgICBcclxuICAgICAgICAgIHZhciBqb3VybmFsYXJyYXlzZXRzID0gW107XHJcbiAgICAgICAgICBmb3IobGV0IHJlc3VsdCBvZiByZXNwb25zZWFycmF5KXtcclxuICAgICAgICAgIGZvcihsZXQgZWFjaG1vbnRoIG9mIHVuaXF1ZW1vbnRocyl7XHJcbiAgICAgICAgICAgIGlmKGVhY2htb250aCE9J21vbnRoJyl7XHJcbiAgICAgICAgICAgIHZhciBqb3VybmFscGVyaW9kID0gW3Jlc3VsdC5tb250aF07ICBcclxuICAgICAgICAgICAgICBpZihlYWNobW9udGg9PXJlc3VsdC5tb250aCl7XHJcbiAgICAgICAgICAgICAgICAgaWYocmVzdWx0LmNvdW50ICE9ICcnKXtcclxuICAgICAgICAgICAgICAgICAgam91cm5hbHBlcmlvZC5wdXNoKHJlc3VsdC5jb3VudCk7XHJcbiAgICAgICAgICAgICAgICAgIGpvdXJuYWxhcnJheXNldHMucHVzaChqb3VybmFscGVyaW9kKTsgXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIGVsc2V7XHJcbiAgICAgICAgICAgICAgICBqb3VybmFscGVyaW9kLnB1c2goMCk7XHJcbiAgICAgICAgICAgICAgICBqb3VybmFsYXJyYXlzZXRzLnB1c2goam91cm5hbHBlcmlvZCk7IFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICBcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgXHJcbiAgICAgICAgICB2YXIgZmluYWxhcnJheXNldCA9IFtdO1xyXG4gICAgICAgICAgZm9yKGxldCBlYWNoanBlcmlvZCBvZiB1bmlxdWVtb250aHMpe1xyXG4gICAgICAgICAgICB2YXIgbmV3anJuYXJyYXlzPVtlYWNoanBlcmlvZF07XHJcbiAgICAgICAgICAgZm9yKGxldCBlbnRyeSBvZiBqb3VybmFsYXJyYXlzZXRzKXtcclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgIGlmKGVudHJ5WzBdPT09ZWFjaGpwZXJpb2Qpe1xyXG4gICAgICAgICAgICAgICAgbmV3anJuYXJyYXlzLnB1c2goZW50cnlbMV0pO1xyXG4gICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBcclxuICAgICAgICAgICB9XHJcbiAgICAgICAgICAgZmluYWxhcnJheXNldC5wdXNoKG5ld2pybmFycmF5cyk7IFxyXG4gICAgICAgICAgfVxyXG5cclxuICAgICAgICAgIHZhciBjaGFydDNhcnJheSA9IFtdO1xyXG4gICAgICAgICAgLy9jaGFydDJhcnJheS5wdXNoKHVuaXF1ZW1vbnRocyk7XHJcbiAgICAgICAgICBmb3IobGV0IGVhY2hyb3cgb2YgZmluYWxhcnJheXNldClcclxuICAgICAgICAgIGNoYXJ0M2FycmF5LnB1c2goZWFjaHJvdyk7XHJcblxyXG4gICAgICAgICAgICBcclxuICAgICAgICAgIHZhciBtb250aGNhdGVnb3JpemVkID0gW107XHJcbiAgICAgICAgICBmb3IobGV0IG1vbnRobmFtZSBvZiB1bmlxdWVtb250aHMpXHJcbiAgICAgICAgICB7XHJcblxyXG4gICAgICAgICAgICBpZihtb250aG5hbWUgIT09ICdtb250aCcpe1xyXG4gICAgICAgICAgICAgIG1vbnRoY2F0ZWdvcml6ZWQucHVzaChtb250aG5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICBcclxuICAgICAgICAgIGxldCBjaGFydDMgPSBjMy5nZW5lcmF0ZSh7XHJcbiAgICAgICAgICAgICAgYmluZHRvOiAnI2NoYXJ0MycsXHJcbiAgICAgICAgICAgICAgZGF0YToge1xyXG4gICAgICAgICAgICAgICAgIFxyXG4gICAgICAgICAgICAgICAgICBjb2x1bW5zOiBjaGFydDNhcnJheSxcclxuICAgICAgICAgICAgICAgICAgXHJcbiAgICAgICAgICAgICAgICAgIHR5cGU6ICdiYXInXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICBiYXI6IHtcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAyNVxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgY29sb3I6e1xyXG4gICAgICAgICAgICAgICAgcGF0dGVybjpbJyMzOTZBQjEnLCAnI0RBN0MzMCcsICcjM0U5NjUxJywgJyNDQzI1MjknLCAnIzUzNTE1NCcsICcjNkI0QzlBJywgJyM5NDhCM0QnXVxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgYXhpczoge1xyXG4gICAgICAgICAgICAgICAgICB4OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY2F0ZWdvcnknICwvLyB0aGlzIG5lZWRlZCB0byBsb2FkIHN0cmluZyB4IHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgIGNhdGVnb3JpZXM6IG1vbnRoY2F0ZWdvcml6ZWRcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pOyAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIHNob3dBdkRldGFpbCgpOiB2b2lkIHtcclxuICAgIC8vbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJlZGl0VXNlcklkXCIpO1xyXG4gICAgLy9sb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImVkaXRVc2VySWRcIiwgdXNlci5pZC50b1N0cmluZygpKTtcclxuICAgIGxldCBvYmogPSB7XHJcbiAgICAgIHVybCA6ICdyZXBvcnQtbWFuYWdlbWVudC9tYW5hZ2UtYXYtYnl1c2VyJyxcclxuICAgICAgaWQgOiBcIm1hbmFnZUF2VXNlclwiXHJcbiAgICB9XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKG9iaik7XHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nLCAncmVwb3J0LW1hbmFnZW1lbnQvbWFuYWdlLWF2LWJ5dXNlciddKS50aGVuKG5hdiA9PiB7XHJcbiAgICB9LCBlcnIgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhlcnIpIC8vIHdoZW4gdGhlcmUncyBhbiBlcnJvclxyXG4gICAgfSk7XHJcbiAgfTtcclxuXHJcblxyXG4gIG9uU2VsZWN0KGV2dCwgc2VsZWN0ZWRWYWwpIHtcclxuICAgIGxldCBvYmogPSB7XHJcbiAgICAgIHVybCA6ICdyZXBvcnQtbWFuYWdlbWVudC9tYW5hZ2UtYXYtYnl1c2VyJyxcclxuICAgICAgaWQgOiBcIm1hbmFnZUF2VXNlclwiXHJcbiAgICB9XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKG9iaik7XHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ3JlcG9ydC1tYW5hZ2VtZW50L21hbmFnZS1hdi1ieXVzZXInXSk7XHJcbiAgICAvLyB0aGlzLm15RXZlbnQuZW1pdCh7Y3JlYXRlZGJ5OnNlbGVjdGVkSXRlbS51c2VybmFtZSxqb3VybmFsbmFtZTpzZWxlY3RlZEl0ZW0uZW50aXRsZW1lbnR9KTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZVsnY3JlYXRlZEJ5J10gPSBzZWxlY3RlZFZhbC51c2VybmFtZTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZVsnam91cm5hbG5hbWUnXSA9IHNlbGVjdGVkVmFsLmVudGl0bGVtZW50O1xyXG4gIH1cclxuXHJcbn1cclxuXHJcblxyXG5cclxuIl19