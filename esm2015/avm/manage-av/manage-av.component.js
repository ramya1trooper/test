import { SelectionModel } from '@angular/cdk/collections';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatDialog, MatSidenav } from '@angular/material';
import { ReportManagementService } from '../report-management.service';
import { NgxUiLoaderService } from 'ngx-ui-loader'; // Import NgxUiLoaderService
import { SnackBarService } from './../../shared/snackbar.service';
import { Router } from '@angular/router';
import { manageAvTableConfig } from '../../table_config';
import { AvmImportFormComponent } from '../import-form/import-form.component';
import { MessageService } from "../../_services/message.service";
import { process } from '@progress/kendo-data-query';
import { Subject } from "rxjs/Subject";
export class ManageAvComponent {
    constructor(router, _matDialog, _reportManagementService, snackBarService, _router, ngxService, messageService) {
        this.router = router;
        this._matDialog = _matDialog;
        this._reportManagementService = _reportManagementService;
        this.snackBarService = snackBarService;
        this._router = _router;
        this.ngxService = ngxService;
        this.messageService = messageService;
        this.displayedColumns = [
            'controlname',
            'description',
            'usercount',
            'datasource',
            'controltype'
        ];
        //displayedColumns: string[] = ['select', 'controlname', 'description', 'usercounts', 'datasource', 'controltype','action'];
        this.selection = new SelectionModel(true, []);
        this.nav_position = 'end';
        this.queryParams = {};
        this.limit = 10;
        this.offset = 0;
        this.selectedTableHeader = [];
        this.state = {};
        this.enableFilter = false;
        this.menu = [];
        this.breadcrumbList = [];
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.groupField = [];
        this.unsubscribe = new Subject();
        this.unsubscribeModel = new Subject();
        this.panelOpenState = false;
    }
    ngOnInit() {
        this.state = {
            skip: this.offset,
            take: this.limit
        };
        this.realm = localStorage.getItem("realm");
        this.ngxService.start();
        this.getdatasource();
        this.loadTableColumn();
    }
    applyFilter(filterValue) {
        console.log(">>> filterValue ", filterValue);
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    dataStateChange(state) {
        this.state = state;
        this.applyTableState(this.state);
    }
    applyTableState(state) {
        this.kendoGridData = process(this._data, state);
    }
    changePage(event) {
        console.log(event, ">>> EVENT Change Page");
        // if (event.pageSize != this.limit) {
        //   this.queryParams["limit"] = event.pageSize;
        //   this.queryParams["offset"] = 0;
        // } else {
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
        // }
        // this.onLoadData("next");
    }
    onClick(action) {
        console.log(">>> onClick action ", action);
        if (action == 'refresh') {
            this.ngOnInit();
        }
    }
    enableFilterOptions() {
        this.enableFilter = !this.enableFilter;
    }
    // applyFilter(event) {
    //   console.log(">>>>>>>>>> event ", event);
    getdatasource_old() {
        this._reportManagementService.getAllAvmDatasources().subscribe((res) => {
            this.ngxService.stop();
            if (res.response && res.response.datasources && res.response.datasources.length) {
                this.customdatasource = res.response.datasources;
                this.defaultdatasourceId = res.response.datasources[0]._id;
                this.onLoadmanageAvs(this.defaultdatasourceId);
            }
        });
    }
    getdatasource() {
        let query = {
            realm: this.realm
        };
        let apiUrl = "/datasources/allAvm";
        console.log(">>> realm check ", this.realm, ">>> query ", query);
        this._reportManagementService.getAllReponse(query, apiUrl).subscribe(res => {
            this.ngxService.stop();
            this.customdatasource = res.response.datasources;
            this.defaultdatasourceId = res.response.datasources[0]._id;
            this.onLoadmanageAvs(this.defaultdatasourceId);
        });
    }
    onChangeDatasourceId(id) {
        this.defaultdatasourceId = id;
        this.onLoadmanageAvs(this.defaultdatasourceId);
    }
    updateTable(event) {
        console.log(event, '----event');
        let selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isactive = true;
            }
            else {
                item.isactive = false;
            }
        });
        // var index = this.tableNamesearch(event.source.value, this.tableConfig)
        // if (index >= 0) {
        // 	if (event.checked) {
        // 		let active = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: true
        // 		}
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, active);
        // 			localStorage.removeItem('control');
        // 			localStorage.setItem('control', JSON.stringify(this.tableConfig));
        // 		}
        // 	} else {
        // 		let inactive = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: false
        // 		}
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, inactive);
        // 			localStorage.removeItem('control');
        // 			localStorage.setItem('control', JSON.stringify(this.tableConfig));
        // 		}
        // 	}
        // }
        localStorage.removeItem('manageAv');
        localStorage.setItem('manageAv', JSON.stringify(this.tableConfig));
        this.loadTableColumn();
    }
    tableNamesearch(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].value === nameKey) {
                return i;
            }
        }
    }
    getDatasourceId(id) {
        this.onLoadmanageAvs(id);
    }
    loadTableColumn() {
        this.displayedColumns = [];
        this.tableConfig = JSON.parse(localStorage.getItem('manageAv'));
        if (this.tableConfig) {
            for (let columns of this.tableConfig) {
                if (columns.isactive) {
                    this.displayedColumns.push(columns);
                    this.selectedTableHeader.push(columns.value);
                }
            }
        }
        else {
            this.tableConfig = manageAvTableConfig;
            for (let columns of this.tableConfig) {
                if (columns.isactive) {
                    this.displayedColumns.push(columns);
                    this.selectedTableHeader.push(columns.value);
                }
            }
        }
    }
    onLoadmanageAvs(id) {
        this.ngxService.start();
        this.defaultdatasourceId = id;
        this.queryParams.datasource = this.defaultdatasourceId;
        this._reportManagementService
            .getallmanageAv(this.queryParams)
            .subscribe((response) => {
            this.ngxService.stop();
            this.manageAvs = response.data;
            console.log(response.type);
            //this.dataSource = new MatTableDataSource<manageAv>(ELEMENT_DATA);
            this.dataSource = new MatTableDataSource(this.manageAvs);
            this._data = this.manageAvs;
            // this.kendoGridData = process(this.manageAvs, this.state);
            // this.kendoGridData.total=this.manageAvs.length;
            // console.log(">>> this.kendoGridData ",this.kendoGridData);
            console.log(this.dataSource);
            this.length = response.data.length;
            this.dataSource.paginator = this.paginator;
            this.applyTableState(this.state);
        });
    }
    showAvDetail() {
        //localStorage.removeItem("editUserId");
        //localStorage.setItem("editUserId", user.id.toString());
        console.log('navingating');
        this.router.navigate(['/', 'report-management/manage-av-detail']).then((nav) => {
            console.log(nav); // true if navigation is successful
        }, (err) => {
            console.log(err); // when there's an error
        });
    }
    importAvm() {
        this.dialogRef = this._matDialog.open(AvmImportFormComponent, {
            width: '50%',
            panelClass: 'contact-form-dialog',
            data: {
                action: 'new'
            }
        });
        this._matDialog.afterAllClosed.subscribe(() => {
            // Do stuff after the dialog has closed
            this.onLoadmanageAvs(this.defaultdatasourceId);
        });
    }
    listenRouting() {
        let routerUrl, routerList, target;
        this._router.events.subscribe((router) => {
            routerUrl = router.urlAfterRedirects;
            if (routerUrl && typeof routerUrl === 'string') {
                // 初始化breadcrumb
                target = this.menu;
                this.breadcrumbList.length = 0;
                // 取得目前routing url用/區格, [0]=第一層, [1]=第二層 ...etc
                routerList = routerUrl.slice(1).split('/');
                routerList.forEach((router, index) => {
                    // 找到這一層在menu的路徑和目前routing相同的路徑
                    target = target.find((page) => page.path.slice(2) === router);
                    // 存到breadcrumbList到時後直接loop這個list就是麵包屑了
                    this.breadcrumbList.push({
                        name: target.name,
                        // 第二層開始路由要加上前一層的routing, 用相對位置會造成routing錯誤
                        path: index === 0
                            ? target.path
                            : `${this.breadcrumbList[index - 1].path}/${target.path.slice(2)}`
                    });
                    // 下一層要比對的目標是這一層指定的子頁面
                    if (index + 1 !== routerList.length) {
                        target = target.children;
                    }
                });
                console.log(this.breadcrumbList);
            }
        });
    }
    onSelect(evt, selectedVal) {
        console.log(">>>>> selectedVal ", selectedVal);
        // this.routerLink="/report-management/manage-av-detail"
        this.router.navigate([
            'report-management/manage-av-detail' +
                '/' +
                selectedVal.datasourceId +
                '/' +
                selectedVal.collectionDetail +
                '/' +
                selectedVal.controlname
        ]);
        let obj = {
            url: 'report-management/manage-av-detail' +
                '/' +
                selectedVal.datasourceId +
                '/' +
                selectedVal.collectionDetail +
                '/' +
                selectedVal.controlname,
            id: "manageAvDetail"
        };
        this.messageService.sendRouting(obj);
        this._reportManagementService.passValue['datasourceId'] =
            selectedVal.datasourceId;
        this._reportManagementService.passValue['collectionDetail'] =
            selectedVal.collectionDetail;
        this._reportManagementService.passValue['displayName'] =
            selectedVal.controlname;
        //console.log(evt, " >>Selected item Id: >>", selectedVal); // You get the Id of the selected item here
    }
    tableSetting() {
        this.panelOpenState = !this.panelOpenState;
    }
}
ManageAvComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-manage-av',
                template: "   <div class=\"page-layout blank\" style=\"padding: 0px !important;\" fusePerfectScrollbar >\r\n      <mat-drawer-container class=\"example-container sen-bg-container\" autosize fxFlex [hasBackdrop]=\"false\">\r\n        <div>\r\n         <!-- CONTENT HEADER -->\r\n         <div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n            fxlayoutalign=\"space-between center\"\r\n            style=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: space-between;\">\r\n            <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"width:40%;\">\r\n               <h2 class=\"m-0 senlib-top-header\">\r\n                  <span class=\"material-icons\"\r\n                    style=\"margin-right: 7px;font-size: 21px;position: relative;top:2px\">beenhere</span>\r\n                    Manage Access Violations - User Conflicts\r\n                </h2>\r\n                <span class=\"senlib-top-italic\">View list of transactions where users have violated anSoD Conflicts</span> \r\n            </div>\r\n\r\n            <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n               <button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilterOptions()\">\r\n                  <span class=\"k-icon k-i-filter\"\r\n                  style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n               </button>\r\n               <button  class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n               (click)=\"select.open()\">\r\n               <span class=\"material-icons\"\r\n                 style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n               <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n                 (selectionChange)=\"updateTable($event)\">\r\n                 <mat-option *ngFor=\"let columnSetting of tableConfig\" [checked]=\"columnSetting.isactive\"\r\n                   [value]=\"columnSetting.value\">{{ columnSetting.name}}</mat-option>\r\n               </mat-select>\r\n               </button>\r\n               <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"  (click)=\"onClick('refresh')\">\r\n                  <span class=\"material-icons\"\r\n                  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n                  autorenew</span>Refresh\r\n               </button>\r\n               <mat-form-field appearance=\"outline\" fxFlex=\"100\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n                  style=\"padding: 0px 0px 0px 5px;\">\r\n                  <mat-label>Select Data Source</mat-label>\r\n\r\n                  <mat-select [(ngModel)]=\"defaultdatasourceId\" required placeholder=\"Select Data Source\"\r\n                     (selectionChange)=\"onChangeDatasourceId($event.value)\">\r\n                     <mat-option *ngFor=\"let ds of customdatasource\" value=\"{{ ds._id }}\">\r\n                        {{ ds.name }}\r\n                     </mat-option>\r\n                  </mat-select>\r\n               </mat-form-field>\r\n            </div>\r\n         </div>\r\n         <div class=\"mat-elevation-z8 sen-margin-10\">\r\n         <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n            [skip]=\"state.skip\"\r\n            [sort]=\"state.sort\"\r\n            [filter]=\"state.filter\"\r\n            [sortable]=\"true\"\r\n            [pageable]=\"true\" \r\n            [resizable]=\"true\"\r\n            [filterable]=\"enableFilter\"\r\n            (dataStateChange)=\"dataStateChange($event)\"\r\n            >\r\n            <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n               <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\" *ngIf=\"!column.onSelect\">\r\n                  <ng-template kendoGridCellTemplate let-dataItem>\r\n                  <span> \r\n                     {{dataItem[column.value]}}\r\n                  </span>\r\n                  </ng-template>\r\n               </kendo-grid-column>\r\n\r\n               <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"\r\n               *ngIf=\"column.onSelect\" title=\"{{column.name}}\" field=\"{{column.value}}\" width=\"100\">\r\n                  <ng-template kendoGridCellTemplate let-dataItem>\r\n                     <a (click)=\"onSelect($event, dataItem)\">{{dataItem[column.value]}}</a>\r\n                  </ng-template>\r\n               </kendo-grid-column>\r\n            </ng-container>\r\n         <ng-template kendoGridNoRecordsTemplate>\r\n            No Records Found\r\n         </ng-template> \r\n      </kendo-grid>\r\n         </div> \r\n       </div>  \r\n    </mat-drawer-container>\r\n</div>",
                styles: ["content{position:relative;display:-webkit-box;display:flex;z-index:1;-webkit-box-flex:1;flex:1 0 auto}content>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto;width:100%;min-width:100%}.example-container{width:100%;height:100%}.card-directive{background:#fff;border:1px solid #d3d3d3;margin:5px!important}chart-layout{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;-webkit-box-flex:1!important;flex:auto!important}.ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.ui-common-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}"]
            }] }
];
/** @nocollapse */
ManageAvComponent.ctorParameters = () => [
    { type: Router },
    { type: MatDialog },
    { type: ReportManagementService },
    { type: SnackBarService },
    { type: Router },
    { type: NgxUiLoaderService },
    { type: MessageService }
];
ManageAvComponent.propDecorators = {
    paginator: [{ type: ViewChild, args: [MatPaginator,] }],
    drawer: [{ type: ViewChild, args: ['drawer',] }]
};
const ELEMENT_DATA = [
    {
        controlname: 'Create Sales Order & Enter Accounts Receivable Invoice',
        description: 'Create Sales Order & Enter Accounts Receivable Invoice',
        usercount: 25,
        datasource: 'DATA.FUSION',
        controltype: 'SOD'
    },
    {
        controlname: 'Enter Accounts Receivable Invoice & Enter Customer Receipt',
        description: 'Enter Accounts Receivable Invoice & Enter Customer Receipt',
        usercount: 45,
        datasource: 'DATA.FUSION',
        controltype: 'SOD'
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlLWF2LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJhdm0vbWFuYWdlLWF2L21hbmFnZS1hdi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxTQUFTLEVBQVUsU0FBUyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsWUFBWSxFQUFFLGtCQUFrQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDckUsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUUxRCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUN2RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxlQUFlLENBQUMsQ0FBQyw0QkFBNEI7QUFFaEYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2xFLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUN6RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUU5RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDakUsT0FBTyxFQUErQixPQUFPLEVBQVEsTUFBTSw0QkFBNEIsQ0FBQztBQUd4RixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBZXZDLE1BQU0sT0FBTyxpQkFBaUI7SUF3QzVCLFlBQ1UsTUFBYyxFQUNkLFVBQXFCLEVBQ3JCLHdCQUFpRCxFQUNqRCxlQUFnQyxFQUNoQyxPQUFlLEVBQ2YsVUFBOEIsRUFDOUIsY0FBOEI7UUFOOUIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLGVBQVUsR0FBVixVQUFVLENBQVc7UUFDckIsNkJBQXdCLEdBQXhCLHdCQUF3QixDQUF5QjtRQUNqRCxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNmLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQXhDeEMscUJBQWdCLEdBQWE7WUFDM0IsYUFBYTtZQUNiLGFBQWE7WUFDYixXQUFXO1lBQ1gsWUFBWTtZQUNaLGFBQWE7U0FDZCxDQUFDO1FBQ0YsNEhBQTRIO1FBQzVILGNBQVMsR0FBRyxJQUFJLGNBQWMsQ0FBVyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFbkQsaUJBQVksR0FBVyxLQUFLLENBQUM7UUFDN0IsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFDdEIsVUFBSyxHQUFXLEVBQUUsQ0FBQztRQUNuQixXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBR25CLHdCQUFtQixHQUFHLEVBQUUsQ0FBQztRQUN6QixVQUFLLEdBQVUsRUFBRSxDQUFDO1FBQ2xCLGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRzlCLFNBQUksR0FBZSxFQUFFLENBQUM7UUFDdEIsbUJBQWMsR0FBZSxFQUFFLENBQUM7UUFDekIsU0FBSSxHQUFHLElBQUksQ0FBQztRQUNaLFNBQUksR0FBd0IsU0FBUyxDQUFDO1FBQ3RDLGNBQVMsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQztRQUNuSCxpQkFBWSxHQUFHLElBQUksQ0FBQztRQUNwQixlQUFVLEdBQXNCLEVBQUUsQ0FBQztRQUNsQyxnQkFBVyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFDbEMscUJBQWdCLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQTBSL0MsbUJBQWMsR0FBWSxLQUFLLENBQUM7SUE5UTdCLENBQUM7SUFFSixRQUFRO1FBQ04sSUFBSSxDQUFDLEtBQUssR0FBQztZQUNULElBQUksRUFBRSxJQUFJLENBQUMsTUFBTTtZQUNqQixJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDakIsQ0FBQTtRQUNELElBQUksQ0FBQyxLQUFLLEdBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUNELFdBQVcsQ0FBQyxXQUFtQjtRQUU1QixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM3RCxDQUFDO0lBQ0QsZUFBZSxDQUFDLEtBQTJCO1FBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUUsS0FBSyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFDRCxlQUFlLENBQUMsS0FBWTtRQUMxQixJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFDRCxVQUFVLENBQUMsS0FBSztRQUNkLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLHVCQUF1QixDQUFDLENBQUE7UUFDM0Msc0NBQXNDO1FBQ3RDLGdEQUFnRDtRQUNoRCxvQ0FBb0M7UUFDcEMsV0FBVztRQUNYLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ3pCLElBQUk7UUFDTCwyQkFBMkI7SUFDNUIsQ0FBQztJQUNELE9BQU8sQ0FBQyxNQUFNO1FBQ1osT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBQyxNQUFNLENBQUMsQ0FBQztRQUMxQyxJQUFHLE1BQU0sSUFBRSxTQUFTLEVBQ3BCO1lBQ0UsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2pCO0lBQ0gsQ0FBQztJQUNELG1CQUFtQjtRQUNsQixJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUN4QyxDQUFDO0lBQ0QsdUJBQXVCO0lBQ3ZCLDZDQUE2QztJQUM3QyxpQkFBaUI7UUFDZixJQUFJLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUNyRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZCLElBQUcsR0FBRyxDQUFDLFFBQVEsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUM7Z0JBQzdFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQztnQkFDakQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztnQkFDM0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQzthQUNoRDtRQUVILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELGFBQWE7UUFHWCxJQUFJLEtBQUssR0FBRztZQUNWLEtBQUssRUFBRyxJQUFJLENBQUMsS0FBSztTQUNuQixDQUFDO1FBQ0YsSUFBSSxNQUFNLEdBQUcscUJBQXFCLENBQUM7UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsRUFBQyxJQUFJLENBQUMsS0FBSyxFQUFHLFlBQVksRUFBQyxLQUFLLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsd0JBQXdCLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDekUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDakQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUMzRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ2pELENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELG9CQUFvQixDQUFDLEVBQUU7UUFDckIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEVBQUUsQ0FBQztRQUM5QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxXQUFXLENBQUMsS0FBSztRQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQ2hDLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUM5QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLElBQUk7WUFDckMsSUFBSSxjQUFjLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNMLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO2FBQ3ZCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCx5RUFBeUU7UUFDekUsb0JBQW9CO1FBQ3BCLHdCQUF3QjtRQUN4QixtQkFBbUI7UUFDbkIsZ0NBQWdDO1FBQ2hDLDhCQUE4QjtRQUM5QixvQkFBb0I7UUFDcEIsTUFBTTtRQUNOLHNCQUFzQjtRQUN0QixnREFBZ0Q7UUFDaEQseUNBQXlDO1FBQ3pDLHdFQUF3RTtRQUN4RSxNQUFNO1FBQ04sWUFBWTtRQUNaLHFCQUFxQjtRQUNyQixnQ0FBZ0M7UUFDaEMsOEJBQThCO1FBQzlCLHFCQUFxQjtRQUNyQixNQUFNO1FBQ04sc0JBQXNCO1FBQ3RCLGtEQUFrRDtRQUNsRCx5Q0FBeUM7UUFDekMsd0VBQXdFO1FBQ3hFLE1BQU07UUFDTixLQUFLO1FBQ0wsSUFBSTtRQUNKLFlBQVksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDekIsQ0FBQztJQUNELGVBQWUsQ0FBQyxPQUFPLEVBQUUsT0FBTztRQUM5QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUN2QyxJQUFJLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssT0FBTyxFQUFFO2dCQUNoQyxPQUFPLENBQUMsQ0FBQzthQUNWO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsZUFBZSxDQUFDLEVBQUU7UUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztRQUNoRSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDcEIsS0FBSyxJQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7b0JBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM5QzthQUNGO1NBQ0Y7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLEdBQUcsbUJBQW1CLENBQUM7WUFDdkMsS0FBSyxJQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNwQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7b0JBQ3BCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM5QzthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsZUFBZSxDQUFDLEVBQUU7UUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUN2RCxJQUFJLENBQUMsd0JBQXdCO2FBQzFCLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO2FBQ2hDLFNBQVMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO1lBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzNCLG1FQUFtRTtZQUNuRSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3pELElBQUksQ0FBQyxLQUFLLEdBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUMxQiw0REFBNEQ7WUFDNUQsa0RBQWtEO1lBQ2xELDZEQUE2RDtZQUM3RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDM0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsWUFBWTtRQUNWLHdDQUF3QztRQUN4Qyx5REFBeUQ7UUFDekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUUzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxvQ0FBb0MsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNwRSxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLG1DQUFtQztRQUN2RCxDQUFDLEVBQ0QsQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyx3QkFBd0I7UUFDNUMsQ0FBQyxDQUNGLENBQUM7SUFDSixDQUFDO0lBRUQsU0FBUztRQUNQLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsc0JBQXNCLEVBQUU7WUFDNUQsS0FBSyxFQUFFLEtBQUs7WUFDWixVQUFVLEVBQUUscUJBQXFCO1lBQ2pDLElBQUksRUFBRTtnQkFDSixNQUFNLEVBQUUsS0FBSzthQUNkO1NBQ0YsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUM1Qyx1Q0FBdUM7WUFDdkMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNqRCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxhQUFhO1FBQ1gsSUFBSSxTQUFpQixFQUFFLFVBQXNCLEVBQUUsTUFBVyxDQUFDO1FBQzNELElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxDQUFDLE1BQVcsRUFBRSxFQUFFO1lBQzVDLFNBQVMsR0FBRyxNQUFNLENBQUMsaUJBQWlCLENBQUM7WUFDckMsSUFBSSxTQUFTLElBQUksT0FBTyxTQUFTLEtBQUssUUFBUSxFQUFFO2dCQUM5QyxnQkFBZ0I7Z0JBQ2hCLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNuQixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7Z0JBQy9CLCtDQUErQztnQkFDL0MsVUFBVSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUMzQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxFQUFFO29CQUNuQywrQkFBK0I7b0JBQy9CLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxNQUFNLENBQUMsQ0FBQztvQkFDOUQsd0NBQXdDO29CQUN4QyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQzt3QkFDdkIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJO3dCQUNqQiwyQ0FBMkM7d0JBQzNDLElBQUksRUFDRixLQUFLLEtBQUssQ0FBQzs0QkFDVCxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUk7NEJBQ2IsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUN6RCxDQUFDLENBQ0YsRUFBRTtxQkFDVixDQUFDLENBQUM7b0JBRUgsc0JBQXNCO29CQUN0QixJQUFJLEtBQUssR0FBRyxDQUFDLEtBQUssVUFBVSxDQUFDLE1BQU0sRUFBRTt3QkFDbkMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUM7cUJBQzFCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQ2xDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsUUFBUSxDQUFDLEdBQUcsRUFBRSxXQUFXO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLEVBQUMsV0FBVyxDQUFDLENBQUM7UUFDOUMsd0RBQXdEO1FBQ3hELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDO1lBQ25CLG9DQUFvQztnQkFDbEMsR0FBRztnQkFDSCxXQUFXLENBQUMsWUFBWTtnQkFDeEIsR0FBRztnQkFDSCxXQUFXLENBQUMsZ0JBQWdCO2dCQUM1QixHQUFHO2dCQUNILFdBQVcsQ0FBQyxXQUFXO1NBQzFCLENBQUMsQ0FBQztRQUNILElBQUksR0FBRyxHQUFHO1lBQ1IsR0FBRyxFQUFHLG9DQUFvQztnQkFDMUMsR0FBRztnQkFDSCxXQUFXLENBQUMsWUFBWTtnQkFDeEIsR0FBRztnQkFDSCxXQUFXLENBQUMsZ0JBQWdCO2dCQUM1QixHQUFHO2dCQUNILFdBQVcsQ0FBQyxXQUFXO1lBQ3ZCLEVBQUUsRUFBRyxnQkFBZ0I7U0FDdEIsQ0FBQTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3JDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDO1lBQ3JELFdBQVcsQ0FBQyxZQUFZLENBQUM7UUFDM0IsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQztZQUN6RCxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDL0IsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7WUFDcEQsV0FBVyxDQUFDLFdBQVcsQ0FBQztRQUMxQix1R0FBdUc7SUFDekcsQ0FBQztJQUdELFlBQVk7UUFDVixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUM3QyxDQUFDOzs7WUF2VUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxlQUFlO2dCQUN6QixnaEtBQXlDOzthQUUxQzs7OztZQXRCUSxNQUFNO1lBTk4sU0FBUztZQUVULHVCQUF1QjtZQUd2QixlQUFlO1lBQ2YsTUFBTTtZQUhOLGtCQUFrQjtZQU9sQixjQUFjOzs7d0JBd0RwQixTQUFTLFNBQUMsWUFBWTtxQkFDdEIsU0FBUyxTQUFDLFFBQVE7O0FBK1JyQixNQUFNLFlBQVksR0FBZTtJQUMvQjtRQUNFLFdBQVcsRUFBRSx3REFBd0Q7UUFDckUsV0FBVyxFQUFFLHdEQUF3RDtRQUNyRSxTQUFTLEVBQUUsRUFBRTtRQUNiLFVBQVUsRUFBRSxhQUFhO1FBQ3pCLFdBQVcsRUFBRSxLQUFLO0tBQ25CO0lBQ0Q7UUFDRSxXQUFXLEVBQUUsNERBQTREO1FBQ3pFLFdBQVcsRUFBRSw0REFBNEQ7UUFDekUsU0FBUyxFQUFFLEVBQUU7UUFDYixVQUFVLEVBQUUsYUFBYTtRQUN6QixXQUFXLEVBQUUsS0FBSztLQUNuQjtDQUNGLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBTZWxlY3Rpb25Nb2RlbCB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFBhZ2luYXRvciwgTWF0VGFibGVEYXRhU291cmNlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2csIE1hdFNpZGVuYXYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5pbXBvcnQgeyBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSB9IGZyb20gJy4uL3JlcG9ydC1tYW5hZ2VtZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBOZ3hVaUxvYWRlclNlcnZpY2UgfSBmcm9tICduZ3gtdWktbG9hZGVyJzsgLy8gSW1wb3J0IE5neFVpTG9hZGVyU2VydmljZVxyXG5cclxuaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IG1hbmFnZUF2VGFibGVDb25maWcgfSBmcm9tICcuLi8uLi90YWJsZV9jb25maWcnO1xyXG5pbXBvcnQgeyBBdm1JbXBvcnRGb3JtQ29tcG9uZW50IH0gZnJvbSAnLi4vaW1wb3J0LWZvcm0vaW1wb3J0LWZvcm0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgaWQgfSBmcm9tICdAc3dpbWxhbmUvbmd4LWNoYXJ0cy9yZWxlYXNlL3V0aWxzJztcclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vX3NlcnZpY2VzL21lc3NhZ2Uuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBHcm91cERlc2NyaXB0b3IsIERhdGFSZXN1bHQsIHByb2Nlc3MsU3RhdGUgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tZGF0YS1xdWVyeSc7XHJcbmltcG9ydCB7IEdyaWREYXRhUmVzdWx0LCBQYWdlQ2hhbmdlRXZlbnQsU2VsZWN0QWxsQ2hlY2tib3hTdGF0ZSxEYXRhU3RhdGVDaGFuZ2VFdmVudCB9IGZyb20gJ0Bwcm9ncmVzcy9rZW5kby1hbmd1bGFyLWdyaWQnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgbWFuYWdlQXYge1xyXG4gIGNvbnRyb2xuYW1lOiBzdHJpbmc7XHJcbiAgZGVzY3JpcHRpb246IHN0cmluZztcclxuICB1c2VyY291bnQ6IG51bWJlcjtcclxuICBkYXRhc291cmNlOiBzdHJpbmc7XHJcbiAgY29udHJvbHR5cGU6IHN0cmluZztcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6ICdhcHAtbWFuYWdlLWF2JyxcclxuICB0ZW1wbGF0ZVVybDogJy4vbWFuYWdlLWF2LmNvbXBvbmVudC5odG1sJyxcclxuICBzdHlsZVVybHM6IFsnLi9tYW5hZ2UtYXYuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTWFuYWdlQXZDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIG1hbmFnZUF2czogYW55O1xyXG4gIGRhdGFTb3VyY2U6IGFueTtcclxuICBrZW5kb0dyaWREYXRhIDogR3JpZERhdGFSZXN1bHQ7XHJcbiAgY3VzdG9tZGF0YXNvdXJjZTogb2JqZWN0W107XHJcbiAgZGVmYXVsdGRhdGFzb3VyY2VJZDogc3RyaW5nO1xyXG4gIHJlYWxtOiBhbnk7XHJcbiAgZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbXHJcbiAgICAnY29udHJvbG5hbWUnLFxyXG4gICAgJ2Rlc2NyaXB0aW9uJyxcclxuICAgICd1c2VyY291bnQnLFxyXG4gICAgJ2RhdGFzb3VyY2UnLFxyXG4gICAgJ2NvbnRyb2x0eXBlJ1xyXG4gIF07XHJcbiAgLy9kaXNwbGF5ZWRDb2x1bW5zOiBzdHJpbmdbXSA9IFsnc2VsZWN0JywgJ2NvbnRyb2xuYW1lJywgJ2Rlc2NyaXB0aW9uJywgJ3VzZXJjb3VudHMnLCAnZGF0YXNvdXJjZScsICdjb250cm9sdHlwZScsJ2FjdGlvbiddO1xyXG4gIHNlbGVjdGlvbiA9IG5ldyBTZWxlY3Rpb25Nb2RlbDxtYW5hZ2VBdj4odHJ1ZSwgW10pO1xyXG4gIGRpYWxvZ1JlZjogYW55O1xyXG4gIG5hdl9wb3NpdGlvbjogc3RyaW5nID0gJ2VuZCc7XHJcbiAgcXVlcnlQYXJhbXM6IGFueSA9IHt9O1xyXG4gIGxpbWl0OiBudW1iZXIgPSAxMDtcclxuICBvZmZzZXQ6IG51bWJlciA9IDA7XHJcbiAgbGVuZ3RoOiBudW1iZXI7XHJcbiAgdGFibGVDb25maWc6IGFueTtcclxuICBzZWxlY3RlZFRhYmxlSGVhZGVyID0gW107XHJcbiAgc3RhdGU6IFN0YXRlID0ge307XHJcbiAgZW5hYmxlRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIF9kYXRhOiBhbnlbXTtcclxuICBuYW1lOiBzdHJpbmc7XHJcbiAgbWVudTogQXJyYXk8YW55PiA9IFtdO1xyXG4gIGJyZWFkY3J1bWJMaXN0OiBBcnJheTxhbnk+ID0gW107XHJcbiAgcHVibGljIGluZm8gPSB0cnVlO1xyXG4gIHB1YmxpYyB0eXBlOiBcIm51bWVyaWNcIiB8IFwiaW5wdXRcIiA9IFwibnVtZXJpY1wiO1xyXG4gIHB1YmxpYyBwYWdlU2l6ZXMgPSBbeyB0ZXh0OiAxMCwgdmFsdWU6IDEwIH0sIHsgdGV4dDogMjUsIHZhbHVlOiAyNSB9LCB7IHRleHQ6IDUwLCB2YWx1ZTogNTAgfSwgeyB0ZXh0OiAxMDAsIHZhbHVlOiAxMDAgfV07XHJcbiAgcHVibGljIHByZXZpb3VzTmV4dCA9IHRydWU7XHJcbiAgcHVibGljIGdyb3VwRmllbGQ6IEdyb3VwRGVzY3JpcHRvcltdID0gW107XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZSA9IG5ldyBTdWJqZWN0PHZvaWQ+KCk7XHJcbiAgcHJpdmF0ZSB1bnN1YnNjcmliZU1vZGVsID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBAVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcbiAgQFZpZXdDaGlsZCgnZHJhd2VyJykgZHJhd2VyOiBNYXRTaWRlbmF2O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICBwcml2YXRlIF9tYXREaWFsb2c6IE1hdERpYWxvZyxcclxuICAgIHByaXZhdGUgX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlOiBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIHByaXZhdGUgbmd4U2VydmljZTogTmd4VWlMb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZSA6TWVzc2FnZVNlcnZpY2VcclxuICApIHt9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5zdGF0ZT17XHJcbiAgICAgIHNraXA6IHRoaXMub2Zmc2V0LFxyXG4gICAgICB0YWtlOiB0aGlzLmxpbWl0XHJcbiAgICB9XHJcbiAgICB0aGlzLnJlYWxtPWxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmVhbG1cIik7XHJcbiAgICB0aGlzLm5neFNlcnZpY2Uuc3RhcnQoKTtcclxuICAgIHRoaXMuZ2V0ZGF0YXNvdXJjZSgpO1xyXG4gICAgdGhpcy5sb2FkVGFibGVDb2x1bW4oKTtcclxuICB9XHJcbiAgYXBwbHlGaWx0ZXIoZmlsdGVyVmFsdWU6IHN0cmluZylcclxuICAge1xyXG4gICAgIGNvbnNvbGUubG9nKFwiPj4+IGZpbHRlclZhbHVlIFwiLGZpbHRlclZhbHVlKTtcclxuICAgICB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyID0gZmlsdGVyVmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCk7XHJcbiAgfVxyXG4gIGRhdGFTdGF0ZUNoYW5nZShzdGF0ZTogRGF0YVN0YXRlQ2hhbmdlRXZlbnQpOiB2b2lkIHtcclxuICAgIHRoaXMuc3RhdGU9IHN0YXRlO1xyXG4gICAgdGhpcy5hcHBseVRhYmxlU3RhdGUodGhpcy5zdGF0ZSk7XHJcbiAgfVxyXG4gIGFwcGx5VGFibGVTdGF0ZShzdGF0ZTogU3RhdGUpOiB2b2lkIHtcclxuICAgIHRoaXMua2VuZG9HcmlkRGF0YSA9IHByb2Nlc3ModGhpcy5fZGF0YSwgc3RhdGUpO1xyXG4gIH1cclxuICBjaGFuZ2VQYWdlKGV2ZW50KSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCI+Pj4gRVZFTlQgQ2hhbmdlIFBhZ2VcIilcclxuICAgIC8vIGlmIChldmVudC5wYWdlU2l6ZSAhPSB0aGlzLmxpbWl0KSB7XHJcbiAgICAvLyAgIHRoaXMucXVlcnlQYXJhbXNbXCJsaW1pdFwiXSA9IGV2ZW50LnBhZ2VTaXplO1xyXG4gICAgLy8gICB0aGlzLnF1ZXJ5UGFyYW1zW1wib2Zmc2V0XCJdID0gMDtcclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wib2Zmc2V0XCJdID0gZXZlbnQuc2tpcDtcclxuICAgIHRoaXMucXVlcnlQYXJhbXNbXCJsaW1pdFwiXSA9IHRoaXMubGltaXQ7XHJcbiAgICB0aGlzLm9mZnNldCA9IGV2ZW50LnNraXA7XHJcbiAgICAvLyB9XHJcbiAgIC8vIHRoaXMub25Mb2FkRGF0YShcIm5leHRcIik7XHJcbiAgfVxyXG4gIG9uQ2xpY2soYWN0aW9uKXtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IG9uQ2xpY2sgYWN0aW9uIFwiLGFjdGlvbik7XHJcbiAgICBpZihhY3Rpb249PSdyZWZyZXNoJylcclxuICAgIHtcclxuICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgfVxyXG4gIH1cclxuICBlbmFibGVGaWx0ZXJPcHRpb25zKCkge1xyXG4gICB0aGlzLmVuYWJsZUZpbHRlciA9ICF0aGlzLmVuYWJsZUZpbHRlcjsgICBcclxuICB9XHJcbiAgLy8gYXBwbHlGaWx0ZXIoZXZlbnQpIHtcclxuICAvLyAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+PiBldmVudCBcIiwgZXZlbnQpO1xyXG4gIGdldGRhdGFzb3VyY2Vfb2xkKCkge1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UuZ2V0QWxsQXZtRGF0YXNvdXJjZXMoKS5zdWJzY3JpYmUoKHJlcykgPT4ge1xyXG4gICAgICB0aGlzLm5neFNlcnZpY2Uuc3RvcCgpO1xyXG4gICAgICBpZihyZXMucmVzcG9uc2UgJiYgcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzICYmIHJlcy5yZXNwb25zZS5kYXRhc291cmNlcy5sZW5ndGgpe1xyXG4gICAgICAgIHRoaXMuY3VzdG9tZGF0YXNvdXJjZSA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlcztcclxuICAgICAgICB0aGlzLmRlZmF1bHRkYXRhc291cmNlSWQgPSByZXMucmVzcG9uc2UuZGF0YXNvdXJjZXNbMF0uX2lkO1xyXG4gICAgICAgIHRoaXMub25Mb2FkbWFuYWdlQXZzKHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZCk7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICB9KTtcclxuICB9XHJcbiAgZ2V0ZGF0YXNvdXJjZSgpXHJcbiAge1xyXG4gICBcclxuICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgcmVhbG0gOiB0aGlzLnJlYWxtXHJcbiAgICB9O1xyXG4gICAgbGV0IGFwaVVybCA9IFwiL2RhdGFzb3VyY2VzL2FsbEF2bVwiO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gcmVhbG0gY2hlY2sgXCIsdGhpcy5yZWFsbSAsIFwiPj4+IHF1ZXJ5IFwiLHF1ZXJ5KTtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgIHRoaXMubmd4U2VydmljZS5zdG9wKCk7XHJcbiAgICAgIHRoaXMuY3VzdG9tZGF0YXNvdXJjZSA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlcztcclxuICAgICAgdGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLl9pZDtcclxuICAgICAgdGhpcy5vbkxvYWRtYW5hZ2VBdnModGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkKTtcclxuICAgIH0pO1xyXG4gIH1cclxuICBvbkNoYW5nZURhdGFzb3VyY2VJZChpZCkge1xyXG4gICAgdGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkID0gaWQ7XHJcbiAgICB0aGlzLm9uTG9hZG1hbmFnZUF2cyh0aGlzLmRlZmF1bHRkYXRhc291cmNlSWQpO1xyXG4gIH1cclxuXHJcbiAgdXBkYXRlVGFibGUoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCAnLS0tLWV2ZW50Jyk7XHJcbiAgICBsZXQgc2VsZWN0ZWRIZWFkZXIgPSB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXI7XHJcbiAgICB0aGlzLnRhYmxlQ29uZmlnLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKHNlbGVjdGVkSGVhZGVyLmluZGV4T2YoaXRlbS52YWx1ZSkgPj0gMCkge1xyXG4gICAgICAgIGl0ZW0uaXNhY3RpdmUgPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGl0ZW0uaXNhY3RpdmUgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICAvLyB2YXIgaW5kZXggPSB0aGlzLnRhYmxlTmFtZXNlYXJjaChldmVudC5zb3VyY2UudmFsdWUsIHRoaXMudGFibGVDb25maWcpXHJcbiAgICAvLyBpZiAoaW5kZXggPj0gMCkge1xyXG4gICAgLy8gXHRpZiAoZXZlbnQuY2hlY2tlZCkge1xyXG4gICAgLy8gXHRcdGxldCBhY3RpdmUgPSB7XHJcbiAgICAvLyBcdFx0XHR2YWx1ZTogZXZlbnQuc291cmNlLnZhbHVlLFxyXG4gICAgLy8gXHRcdFx0bmFtZTogZXZlbnQuc291cmNlLm5hbWUsXHJcbiAgICAvLyBcdFx0XHRpc2FjdGl2ZTogdHJ1ZVxyXG4gICAgLy8gXHRcdH1cclxuICAgIC8vIFx0XHRpZiAoaW5kZXggPj0gMCkge1xyXG4gICAgLy8gXHRcdFx0dGhpcy50YWJsZUNvbmZpZy5zcGxpY2UoaW5kZXgsIDEsIGFjdGl2ZSk7XHJcbiAgICAvLyBcdFx0XHRsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnY29udHJvbCcpO1xyXG4gICAgLy8gXHRcdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2NvbnRyb2wnLCBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlQ29uZmlnKSk7XHJcbiAgICAvLyBcdFx0fVxyXG4gICAgLy8gXHR9IGVsc2Uge1xyXG4gICAgLy8gXHRcdGxldCBpbmFjdGl2ZSA9IHtcclxuICAgIC8vIFx0XHRcdHZhbHVlOiBldmVudC5zb3VyY2UudmFsdWUsXHJcbiAgICAvLyBcdFx0XHRuYW1lOiBldmVudC5zb3VyY2UubmFtZSxcclxuICAgIC8vIFx0XHRcdGlzYWN0aXZlOiBmYWxzZVxyXG4gICAgLy8gXHRcdH1cclxuICAgIC8vIFx0XHRpZiAoaW5kZXggPj0gMCkge1xyXG4gICAgLy8gXHRcdFx0dGhpcy50YWJsZUNvbmZpZy5zcGxpY2UoaW5kZXgsIDEsIGluYWN0aXZlKTtcclxuICAgIC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdjb250cm9sJyk7XHJcbiAgICAvLyBcdFx0XHRsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnY29udHJvbCcsIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVDb25maWcpKTtcclxuICAgIC8vIFx0XHR9XHJcbiAgICAvLyBcdH1cclxuICAgIC8vIH1cclxuICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKCdtYW5hZ2VBdicpO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oJ21hbmFnZUF2JywgSlNPTi5zdHJpbmdpZnkodGhpcy50YWJsZUNvbmZpZykpO1xyXG4gICAgdGhpcy5sb2FkVGFibGVDb2x1bW4oKTtcclxuICB9XHJcbiAgdGFibGVOYW1lc2VhcmNoKG5hbWVLZXksIG15QXJyYXkpIHtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgbXlBcnJheS5sZW5ndGg7IGkrKykge1xyXG4gICAgICBpZiAobXlBcnJheVtpXS52YWx1ZSA9PT0gbmFtZUtleSkge1xyXG4gICAgICAgIHJldHVybiBpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldERhdGFzb3VyY2VJZChpZCkge1xyXG4gICAgdGhpcy5vbkxvYWRtYW5hZ2VBdnMoaWQpO1xyXG4gIH1cclxuXHJcbiAgbG9hZFRhYmxlQ29sdW1uKCkge1xyXG4gICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gW107XHJcbiAgICB0aGlzLnRhYmxlQ29uZmlnID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbWFuYWdlQXYnKSk7XHJcbiAgICBpZiAodGhpcy50YWJsZUNvbmZpZykge1xyXG4gICAgICBmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuICAgICAgICBpZiAoY29sdW1ucy5pc2FjdGl2ZSkge1xyXG4gICAgICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucyk7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMudGFibGVDb25maWcgPSBtYW5hZ2VBdlRhYmxlQ29uZmlnO1xyXG4gICAgICBmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuICAgICAgICBpZiAoY29sdW1ucy5pc2FjdGl2ZSkge1xyXG4gICAgICAgICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucyk7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIG9uTG9hZG1hbmFnZUF2cyhpZCkge1xyXG4gICAgdGhpcy5uZ3hTZXJ2aWNlLnN0YXJ0KCk7XHJcbiAgICB0aGlzLmRlZmF1bHRkYXRhc291cmNlSWQgPSBpZDtcclxuICAgIHRoaXMucXVlcnlQYXJhbXMuZGF0YXNvdXJjZSA9IHRoaXMuZGVmYXVsdGRhdGFzb3VyY2VJZDtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRhbGxtYW5hZ2VBdih0aGlzLnF1ZXJ5UGFyYW1zKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIHRoaXMubmd4U2VydmljZS5zdG9wKCk7XHJcbiAgICAgICAgdGhpcy5tYW5hZ2VBdnMgPSByZXNwb25zZS5kYXRhO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlLnR5cGUpO1xyXG4gICAgICAgIC8vdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZTxtYW5hZ2VBdj4oRUxFTUVOVF9EQVRBKTtcclxuICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMubWFuYWdlQXZzKTtcclxuICAgICAgICB0aGlzLl9kYXRhPXRoaXMubWFuYWdlQXZzO1xyXG4gICAgICAgIC8vIHRoaXMua2VuZG9HcmlkRGF0YSA9IHByb2Nlc3ModGhpcy5tYW5hZ2VBdnMsIHRoaXMuc3RhdGUpO1xyXG4gICAgICAgIC8vIHRoaXMua2VuZG9HcmlkRGF0YS50b3RhbD10aGlzLm1hbmFnZUF2cy5sZW5ndGg7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCI+Pj4gdGhpcy5rZW5kb0dyaWREYXRhIFwiLHRoaXMua2VuZG9HcmlkRGF0YSk7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5kYXRhU291cmNlKTtcclxuICAgICAgICB0aGlzLmxlbmd0aCA9IHJlc3BvbnNlLmRhdGEubGVuZ3RoO1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnBhZ2luYXRvcjtcclxuICAgICAgICB0aGlzLmFwcGx5VGFibGVTdGF0ZSh0aGlzLnN0YXRlKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBzaG93QXZEZXRhaWwoKTogdm9pZCB7XHJcbiAgICAvL2xvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiZWRpdFVzZXJJZFwiKTtcclxuICAgIC8vbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJlZGl0VXNlcklkXCIsIHVzZXIuaWQudG9TdHJpbmcoKSk7XHJcbiAgICBjb25zb2xlLmxvZygnbmF2aW5nYXRpbmcnKTtcclxuXHJcbiAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nLCAncmVwb3J0LW1hbmFnZW1lbnQvbWFuYWdlLWF2LWRldGFpbCddKS50aGVuKFxyXG4gICAgICAobmF2KSA9PiB7XHJcbiAgICAgICAgY29uc29sZS5sb2cobmF2KTsgLy8gdHJ1ZSBpZiBuYXZpZ2F0aW9uIGlzIHN1Y2Nlc3NmdWxcclxuICAgICAgfSxcclxuICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGVycik7IC8vIHdoZW4gdGhlcmUncyBhbiBlcnJvclxyXG4gICAgICB9XHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgaW1wb3J0QXZtKCk6IHZvaWQge1xyXG4gICAgdGhpcy5kaWFsb2dSZWYgPSB0aGlzLl9tYXREaWFsb2cub3BlbihBdm1JbXBvcnRGb3JtQ29tcG9uZW50LCB7XHJcbiAgICAgIHdpZHRoOiAnNTAlJyxcclxuICAgICAgcGFuZWxDbGFzczogJ2NvbnRhY3QtZm9ybS1kaWFsb2cnLFxyXG4gICAgICBkYXRhOiB7XHJcbiAgICAgICAgYWN0aW9uOiAnbmV3J1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMuX21hdERpYWxvZy5hZnRlckFsbENsb3NlZC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAvLyBEbyBzdHVmZiBhZnRlciB0aGUgZGlhbG9nIGhhcyBjbG9zZWRcclxuICAgICAgdGhpcy5vbkxvYWRtYW5hZ2VBdnModGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbGlzdGVuUm91dGluZygpIHtcclxuICAgIGxldCByb3V0ZXJVcmw6IHN0cmluZywgcm91dGVyTGlzdDogQXJyYXk8YW55PiwgdGFyZ2V0OiBhbnk7XHJcbiAgICB0aGlzLl9yb3V0ZXIuZXZlbnRzLnN1YnNjcmliZSgocm91dGVyOiBhbnkpID0+IHtcclxuICAgICAgcm91dGVyVXJsID0gcm91dGVyLnVybEFmdGVyUmVkaXJlY3RzO1xyXG4gICAgICBpZiAocm91dGVyVXJsICYmIHR5cGVvZiByb3V0ZXJVcmwgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgLy8g5Yid5aeL5YyWYnJlYWRjcnVtYlxyXG4gICAgICAgIHRhcmdldCA9IHRoaXMubWVudTtcclxuICAgICAgICB0aGlzLmJyZWFkY3J1bWJMaXN0Lmxlbmd0aCA9IDA7XHJcbiAgICAgICAgLy8g5Y+W5b6X55uu5YmNcm91dGluZyB1cmznlKgv5Y2A5qC8LCBbMF0956ys5LiA5bGkLCBbMV0956ys5LqM5bGkIC4uLmV0Y1xyXG4gICAgICAgIHJvdXRlckxpc3QgPSByb3V0ZXJVcmwuc2xpY2UoMSkuc3BsaXQoJy8nKTtcclxuICAgICAgICByb3V0ZXJMaXN0LmZvckVhY2goKHJvdXRlciwgaW5kZXgpID0+IHtcclxuICAgICAgICAgIC8vIOaJvuWIsOmAmeS4gOWxpOWcqG1lbnXnmoTot6/lvpHlkoznm67liY1yb3V0aW5n55u45ZCM55qE6Lev5b6RXHJcbiAgICAgICAgICB0YXJnZXQgPSB0YXJnZXQuZmluZCgocGFnZSkgPT4gcGFnZS5wYXRoLnNsaWNlKDIpID09PSByb3V0ZXIpO1xyXG4gICAgICAgICAgLy8g5a2Y5YiwYnJlYWRjcnVtYkxpc3TliLDmmYLlvoznm7TmjqVsb29w6YCZ5YCLbGlzdOWwseaYr+m6teWMheWxkeS6hlxyXG4gICAgICAgICAgdGhpcy5icmVhZGNydW1iTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgbmFtZTogdGFyZ2V0Lm5hbWUsXHJcbiAgICAgICAgICAgIC8vIOesrOS6jOWxpOmWi+Wni+i3r+eUseimgeWKoOS4iuWJjeS4gOWxpOeahHJvdXRpbmcsIOeUqOebuOWwjeS9jee9ruacg+mAoOaIkHJvdXRpbmfpjK/oqqRcclxuICAgICAgICAgICAgcGF0aDpcclxuICAgICAgICAgICAgICBpbmRleCA9PT0gMFxyXG4gICAgICAgICAgICAgICAgPyB0YXJnZXQucGF0aFxyXG4gICAgICAgICAgICAgICAgOiBgJHt0aGlzLmJyZWFkY3J1bWJMaXN0W2luZGV4IC0gMV0ucGF0aH0vJHt0YXJnZXQucGF0aC5zbGljZShcclxuICAgICAgICAgICAgICAgICAgICAyXHJcbiAgICAgICAgICAgICAgICAgICl9YFxyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgLy8g5LiL5LiA5bGk6KaB5q+U5bCN55qE55uu5qiZ5piv6YCZ5LiA5bGk5oyH5a6a55qE5a2Q6aCB6Z2iXHJcbiAgICAgICAgICBpZiAoaW5kZXggKyAxICE9PSByb3V0ZXJMaXN0Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0YXJnZXQgPSB0YXJnZXQuY2hpbGRyZW47XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuYnJlYWRjcnVtYkxpc3QpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcblxyXG4gIG9uU2VsZWN0KGV2dCwgc2VsZWN0ZWRWYWwpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4gc2VsZWN0ZWRWYWwgXCIsc2VsZWN0ZWRWYWwpO1xyXG4gICAgLy8gdGhpcy5yb3V0ZXJMaW5rPVwiL3JlcG9ydC1tYW5hZ2VtZW50L21hbmFnZS1hdi1kZXRhaWxcIlxyXG4gICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1xyXG4gICAgICAncmVwb3J0LW1hbmFnZW1lbnQvbWFuYWdlLWF2LWRldGFpbCcgK1xyXG4gICAgICAgICcvJyArXHJcbiAgICAgICAgc2VsZWN0ZWRWYWwuZGF0YXNvdXJjZUlkICtcclxuICAgICAgICAnLycgK1xyXG4gICAgICAgIHNlbGVjdGVkVmFsLmNvbGxlY3Rpb25EZXRhaWwgK1xyXG4gICAgICAgICcvJyArXHJcbiAgICAgICAgc2VsZWN0ZWRWYWwuY29udHJvbG5hbWVcclxuICAgIF0pO1xyXG4gICAgbGV0IG9iaiA9IHtcclxuICAgICAgdXJsIDogJ3JlcG9ydC1tYW5hZ2VtZW50L21hbmFnZS1hdi1kZXRhaWwnICtcclxuICAgICAgJy8nICtcclxuICAgICAgc2VsZWN0ZWRWYWwuZGF0YXNvdXJjZUlkICtcclxuICAgICAgJy8nICtcclxuICAgICAgc2VsZWN0ZWRWYWwuY29sbGVjdGlvbkRldGFpbCArXHJcbiAgICAgICcvJyArXHJcbiAgICAgIHNlbGVjdGVkVmFsLmNvbnRyb2xuYW1lLFxyXG4gICAgICBpZCA6IFwibWFuYWdlQXZEZXRhaWxcIlxyXG4gICAgfVxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG4gICAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlWydkYXRhc291cmNlSWQnXSA9XHJcbiAgICAgIHNlbGVjdGVkVmFsLmRhdGFzb3VyY2VJZDtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZVsnY29sbGVjdGlvbkRldGFpbCddID1cclxuICAgICAgc2VsZWN0ZWRWYWwuY29sbGVjdGlvbkRldGFpbDtcclxuICAgIHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZVsnZGlzcGxheU5hbWUnXSA9XHJcbiAgICAgIHNlbGVjdGVkVmFsLmNvbnRyb2xuYW1lO1xyXG4gICAgLy9jb25zb2xlLmxvZyhldnQsIFwiID4+U2VsZWN0ZWQgaXRlbSBJZDogPj5cIiwgc2VsZWN0ZWRWYWwpOyAvLyBZb3UgZ2V0IHRoZSBJZCBvZiB0aGUgc2VsZWN0ZWQgaXRlbSBoZXJlXHJcbiAgfVxyXG4gIHBhbmVsT3BlblN0YXRlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gIHRhYmxlU2V0dGluZygpIHtcclxuICAgIHRoaXMucGFuZWxPcGVuU3RhdGUgPSAhdGhpcy5wYW5lbE9wZW5TdGF0ZTtcclxuICB9XHJcbn1cclxuXHJcbmNvbnN0IEVMRU1FTlRfREFUQTogbWFuYWdlQXZbXSA9IFtcclxuICB7XHJcbiAgICBjb250cm9sbmFtZTogJ0NyZWF0ZSBTYWxlcyBPcmRlciAmIEVudGVyIEFjY291bnRzIFJlY2VpdmFibGUgSW52b2ljZScsXHJcbiAgICBkZXNjcmlwdGlvbjogJ0NyZWF0ZSBTYWxlcyBPcmRlciAmIEVudGVyIEFjY291bnRzIFJlY2VpdmFibGUgSW52b2ljZScsXHJcbiAgICB1c2VyY291bnQ6IDI1LFxyXG4gICAgZGF0YXNvdXJjZTogJ0RBVEEuRlVTSU9OJyxcclxuICAgIGNvbnRyb2x0eXBlOiAnU09EJ1xyXG4gIH0sXHJcbiAge1xyXG4gICAgY29udHJvbG5hbWU6ICdFbnRlciBBY2NvdW50cyBSZWNlaXZhYmxlIEludm9pY2UgJiBFbnRlciBDdXN0b21lciBSZWNlaXB0JyxcclxuICAgIGRlc2NyaXB0aW9uOiAnRW50ZXIgQWNjb3VudHMgUmVjZWl2YWJsZSBJbnZvaWNlICYgRW50ZXIgQ3VzdG9tZXIgUmVjZWlwdCcsXHJcbiAgICB1c2VyY291bnQ6IDQ1LFxyXG4gICAgZGF0YXNvdXJjZTogJ0RBVEEuRlVTSU9OJyxcclxuICAgIGNvbnRyb2x0eXBlOiAnU09EJ1xyXG4gIH1cclxuXTtcclxuIl19