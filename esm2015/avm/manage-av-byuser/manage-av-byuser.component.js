import { SelectionModel } from '@angular/cdk/collections';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { MatSidenav } from '@angular/material';
import { Location } from '@angular/common';
import { ReportManagementService } from '../report-management.service';
import { SnackBarService } from './../../shared/snackbar.service';
import { manageAvbyLedgerUserTableConfig } from '../../table_config';
import { manageAvbyVendorUserTableConfig } from '../../table_config';
import { manageAvbyInvoiceUserTableConfig } from '../../table_config';
import 'rxjs/add/observable/of';
import * as FileSaver from 'file-saver';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from '../../_services/message.service';
import { process } from '@progress/kendo-data-query';
export class ManageAvbyuserComponent {
    constructor(_reportManagementService, activeRoute, snackBarService, location, messageService, router) {
        this._reportManagementService = _reportManagementService;
        this.activeRoute = activeRoute;
        this.snackBarService = snackBarService;
        this.location = location;
        this.messageService = messageService;
        this.router = router;
        //displayedColumns: string[] = ['ledger', 'journalname', 'journaldescription', 'journalsource', 'journalcategory','journalperiod','currencycode','status','createdby','postedby','amount','posteddate'];
        //displayedColumns: string[] = ['select', 'controlname', 'description', 'usercounts', 'datasource', 'controltype','action'];
        this.selection = new SelectionModel(true, []);
        this.nav_position = 'end';
        this.queryParams = {};
        this.limit = 10;
        this.offset = 0;
        this.isLedgerTable = false;
        this.isVendorTable = false;
        this.isInvoiceTable = false;
        this.columns = [];
        this.selectedTableHeader = [];
        this.enableFilter = false;
        this.info = true;
        this.type = "numeric";
        this.pageSizes = [{ text: 10, value: 10 }, { text: 25, value: 25 }, { text: 50, value: 50 }, { text: 100, value: 100 }];
        this.previousNext = true;
        this.state = {};
        this.viewData = {
            employees: []
        };
        this.panelOpenState = false;
        console.log("av detail URL Parms **************", this.activeRoute.snapshot.params);
    }
    getNotification() {
        console.log('Do something with the notification (evt) sent by the child!', this._reportManagementService.passValue);
        this.queryParams.createdby = this._reportManagementService.passValue.createdBy;
        this.queryParams.journalname = this._reportManagementService.passValue.journalname;
        this.queryParams.collectionDetail = this._reportManagementService.passValue.collectionDetail;
        this.queryParams.datasource2 = this._reportManagementService.passValue.datasourceId;
        console.log('this.queryParams', this.queryParams);
        this._reportManagementService.getallAvByuser(this.queryParams).subscribe(response => {
            this.manageAvsbyuser = response.data;
            console.log(response.type);
            var cols = Object.keys(response.data[0]);
            this.settings = { actions: false };
            this.settings["columns"] = {};
            //var arr=[id,name,amount]
            for (var i = 0; i <= cols.length; i++) {
                var colDisplayname = cols[i];
                if (colDisplayname && colDisplayname != undefined && colDisplayname != 'status') {
                    console.log(colDisplayname);
                    this.settings["columns"][cols[i]] = { title: colDisplayname[0].toUpperCase() + colDisplayname.substr(1).toLowerCase() };
                }
            }
            for (var i = 0; i <= cols.length; i++) {
                var col = cols[i];
                if (col && col != undefined) {
                    var objCols = {};
                    objCols['columnDef'] = col;
                    objCols['formCtl'] = col + 'Filter';
                    objCols['placeHoldeName'] = col.toLocaleUpperCase() + ' Filter';
                    objCols['header'] = col.toLocaleUpperCase();
                    objCols['title'] = col.toUpperCase() + col.substr(1).toLowerCase();
                    this.columns.push(objCols);
                }
            }
            if (response && response.data) {
                if (response.data[0].ledger) {
                    this.isLedgerTable = true;
                    this.tableConfig = manageAvbyLedgerUserTableConfig;
                }
                else if (response.data[0].vendorname) {
                    this.isVendorTable = true;
                    this.tableConfig = manageAvbyVendorUserTableConfig;
                }
                else {
                    this.isInvoiceTable = true;
                    this.tableConfig = manageAvbyInvoiceUserTableConfig;
                }
            }
            //this.displayedColumns = Object.keys(response.data[0]);
            this.loadTableColumn();
            this.dataSource = new MatTableDataSource(response.data);
            this._data = response.data;
            this.length = response.data.length;
            this.dataSource.paginator = this.paginator;
            console.log(">>>>>>>> response.data ", response.data);
            this.applyTableState(this.state);
        });
        /*this.settings = {
            columns: {
              id: {
                title: 'ID'
              },
              name: {
                title: 'Full Name'
              },
              username: {
                title: 'User Name'
              },
              email: {
                title: 'Email'
              }
            }
          };

          this.data = [
            {
              id: 1,
              name: "Leanne Graham",
              username: "Bret",
              email: "Sincere@april.biz"
            },
            {
              id: 2,
              name: "Ervin Howell",
              username: "Antonette",
              email: "Shanna@melissa.tv"
            },
            
            // ... list of items
            
            {
              id: 11,
              name: "Nicholas DuBuque",
              username: "Nicholas.Stanton",
              email: "Rey.Padberg@rosamond.biz"
            }
          ];*/
        // Do something with the notification (evt) sent by the child!
        console.log(this.viewData);
    }
    ngOnInit() {
        this.state = {
            skip: this.offset,
            take: this.limit
        };
        this.getdatasource();
        this.getNotification();
    }
    dataStateChange(state) {
        this.state = state;
        this.applyTableState(this.state);
    }
    applyTableState(state) {
        this.kendoGridData = process(this._data, state);
    }
    onClick(action) {
        if (action == 'refresh') {
            this.ngOnInit();
        }
    }
    changePage(event) {
        console.log(event, ">>> EVENT Change Page");
        this.queryParams["offset"] = event.skip;
        this.queryParams["limit"] = this.limit;
        this.offset = event.skip;
    }
    enableFilterOptions() {
        this.enableFilter = !this.enableFilter;
    }
    applyFilter(filterValue) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    backToAVDetail() {
        this.location.back();
        let obj = {
            id: "manageAvDetail"
        };
        let self = this;
        setTimeout(function () {
            self.messageService.sendRouting(obj);
        }, 100);
    }
    backToAV() {
        let obj = {
            id: "manageAvm"
        };
        this.messageService.sendRouting(obj);
        this.router.navigate(['report-management/manage-av']);
    }
    getdatasource() {
        this._reportManagementService.getAllDatasources().subscribe(res => {
            this.customdatasource = res.response.datasources;
            this.defaultdatasourceId = res.response.datasources[0]._id;
            this.onLoadmanageAvs(this.defaultdatasourceId);
        });
    }
    getDatasourceId(id) {
        this.onLoadmanageAvs(id);
    }
    updateTable(event) {
        console.log(event, "----event");
        let selectedHeader = this.selectedTableHeader;
        this.tableConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isactive = true;
            }
            else {
                item.isactive = false;
            }
        });
        // var index = this.tableNamesearch(event.source.value, this.tableConfig)
        // if (index >= 0) {
        // 	if (event.checked) {
        // 		let active = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: true
        // 		}
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, active);
        // 			localStorage.removeItem('control');
        // 			localStorage.setItem('control', JSON.stringify(this.tableConfig));
        // 		}
        // 	} else {
        // 		let inactive = {
        // 			value: event.source.value,
        // 			name: event.source.name,
        // 			isactive: false
        // 		}
        // 		if (index >= 0) {
        // 			this.tableConfig.splice(index, 1, inactive);
        // 			localStorage.removeItem('control');
        // 			localStorage.setItem('control', JSON.stringify(this.tableConfig));
        // 		}
        // 	}
        // }
        localStorage.removeItem('manageAvbyuser');
        localStorage.setItem('manageAvbyuser', JSON.stringify(this.tableConfig));
        this.loadTableColumn();
    }
    tableNamesearch(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].value === nameKey) {
                return i;
            }
        }
    }
    tableSetting() {
        this.panelOpenState = !this.panelOpenState;
    }
    loadTableColumn() {
        console.log(this.displayedColumns, "......displayedColumns");
        this.displayedColumns = [];
        this.tableConfig = JSON.parse(localStorage.getItem('manageAvbyuser'));
        if (this.tableConfig) {
            for (let columns of this.tableConfig) {
                if (columns.isactive) {
                    // this.displayedColumns.push(columns.value);
                    // this.selectedTableHeader.push(columns.value);
                    this.displayedColumns.push(columns);
                    this.selectedTableHeader.push(columns.value);
                }
            }
        }
        else {
            if (this.isLedgerTable) {
                this.tableConfig = manageAvbyLedgerUserTableConfig;
            }
            else if (this.isVendorTable) {
                this.tableConfig = manageAvbyVendorUserTableConfig;
            }
            else if (this.isInvoiceTable) {
                this.tableConfig = manageAvbyInvoiceUserTableConfig;
            }
            console.log(this.tableConfig, "...........tableconfig");
            for (let columns of this.tableConfig) {
                if (columns.isactive) {
                    // this.displayedColumns.push(columns.value);
                    // this.selectedTableHeader.push(columns.value);
                    this.displayedColumns.push(columns);
                    this.selectedTableHeader.push(columns.value);
                }
            }
            console.log(this.displayedColumns, "....this.displayedColumns");
            console.log(">>>> selectedTableHeader ", this.selectedTableHeader);
        }
    }
    onLoadmanageAvs(id) {
        this.defaultdatasourceId = id;
    }
    exportAvmUserdetail() {
        console.log('data source id', this._reportManagementService.passValue.datasourceId);
        var value = 1;
        this.avmExportObj = {};
        this.avmExportObj['datasource'] = this._reportManagementService.passValue.datasourceId;
        this.avmExportObj['report_type'] = 'CSV';
        this.avmExportObj['collectionDetail'] = this._reportManagementService.passValue.collectionDetail;
        this.avmExportObj['createdby'] = this._reportManagementService.passValue.createdBy;
        this.avmExportObj['journalname'] = this._reportManagementService.passValue.journalname;
        this._reportManagementService.exportAVMUSerdetails(this.avmExportObj).subscribe(resp => {
            if (value == 1) {
                const blob = new Blob([resp.body], { type: 'text/csv' });
                FileSaver.saveAs(blob, "AccessViolationByUser.csv");
            }
            else {
                const blob = new Blob([resp.body], { type: 'text/xlsx' });
                FileSaver.saveAs(blob, "UserConflict.xlsx");
            }
        }, error => {
            console.log("error:::" + JSON.stringify(error));
        });
    }
}
ManageAvbyuserComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-manage-av-byuser',
                template: "<div class=\"page-layout blank\" style=\"padding: 0px !important;\" fusePerfectScrollbar >\r\n   <div style=\"padding: 10px\">\r\n      <button mat-button (click)=\"backToAV()\"><mat-icon>arrow_left</mat-icon>Back to AV</button>\r\n      <button mat-button (click)=\"backToAVDetail()\"><mat-icon>arrow_left</mat-icon>Back to AV Detail</button>\r\n   </div>\r\n   <mat-drawer-container class=\"example-container sen-bg-container\" autosize fxFlex [hasBackdrop]=\"false\">\r\n      <div>\r\n      <div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\"\r\n         style=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: space-between;\">\r\n         <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n         style=\"flex-direction: row; box-sizing: border-box; display: flex; max-height: 100%; align-content: center; align-items: center; justify-content: flex-start;\">\r\n         </div>\r\n         <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n               <button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilterOptions()\">\r\n                  <span class=\"k-icon k-i-filter\"\r\n                  style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n               </button>\r\n               <button  class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n               (click)=\"select.open()\">\r\n               <span class=\"material-icons\"\r\n               style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n               <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n               (selectionChange)=\"updateTable($event)\">\r\n               <mat-option *ngFor=\"let columnSetting of tableConfig\" [checked]=\"columnSetting.isactive\"\r\n                  [value]=\"columnSetting.value\">{{ columnSetting.name}}</mat-option>\r\n               </mat-select>\r\n               </button>\r\n               <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"  (click)=\"onClick('refresh')\">\r\n                  <span class=\"material-icons\"\r\n                  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n                  autorenew</span>Refresh\r\n               </button>\r\n               <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\"   (click)=\"exportAvmUserdetail()\">\r\n                  <span class=\"material-icons\"\r\n                  style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n                  cloud_download</span>Export\r\n               </button>\r\n         </div>\r\n      </div>\r\n      <div style=\"overflow-x: auto;\">\r\n         <div *ngIf=\"isLedgerTable\">\r\n            <div class=\"mat-elevation-z8 sen-margin-10\">\r\n               <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n               [skip]=\"state.skip\"\r\n               [sort]=\"state.sort\"\r\n               [filter]=\"state.filter\"\r\n               [sortable]=\"true\"\r\n               [pageable]=\"true\" \r\n               [resizable]=\"true\"\r\n               [filterable]=\"enableFilter\"\r\n               (dataStateChange)=\"dataStateChange($event)\">\r\n               <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n                  <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\">\r\n                     <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <span> \r\n                           {{dataItem[column.value]}}\r\n                        </span>\r\n                     </ng-template>\r\n                  </kendo-grid-column>\r\n               </ng-container>\r\n            </kendo-grid>\r\n            </div>\r\n         </div>\r\n         <div *ngIf=\"isVendorTable\">\r\n            <div class=\"mat-elevation-z8 sen-margin-10\">\r\n               <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n               [skip]=\"state.skip\"\r\n               [sort]=\"state.sort\"\r\n               [filter]=\"state.filter\"\r\n               [sortable]=\"true\"\r\n               [pageable]=\"true\" \r\n               [resizable]=\"true\"\r\n               [filterable]=\"enableFilter\"\r\n               (dataStateChange)=\"dataStateChange($event)\">\r\n               <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n                  <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\">\r\n                     <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <span> \r\n                           {{dataItem[column.value]}}\r\n                        </span>\r\n                     </ng-template>\r\n                  </kendo-grid-column>\r\n               </ng-container>\r\n            </kendo-grid>\r\n            </div>\r\n         </div>\r\n         <div *ngIf=\"isInvoiceTable\">\r\n            <div class=\"mat-elevation-z8 sen-margin-10\">\r\n               <kendo-grid  [data]=\"kendoGridData\"  [pageSize]=\"state.take\"\r\n               [skip]=\"state.skip\"\r\n               [sort]=\"state.sort\"\r\n               [filter]=\"state.filter\"\r\n               [sortable]=\"true\"\r\n               [pageable]=\"true\" \r\n               [resizable]=\"true\"\r\n               [filterable]=\"enableFilter\"\r\n               (dataStateChange)=\"dataStateChange($event)\">\r\n               <ng-container *ngFor=\"let column of displayedColumns; let i = index\">\r\n                  <kendo-grid-column [headerStyle]=\"{'background-color': '#394E66','color': '#fff','line-height': ''}\"  field=\"{{column.value}}\" title=\"{{column.name}}\" [width]=\"100\">\r\n                     <ng-template kendoGridCellTemplate let-dataItem>\r\n                        <span> \r\n                           {{dataItem[column.value]}}\r\n                        </span>\r\n                     </ng-template>\r\n                  </kendo-grid-column>\r\n               </ng-container>\r\n            </kendo-grid>\r\n            </div>\r\n         </div>\r\n      </div>\r\n  </div>\r\n </mat-drawer-container>\r\n</div>\r\n",
                styles: ["content{position:relative;display:-webkit-box;display:flex;z-index:1;-webkit-box-flex:1;flex:1 0 auto}content>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto;width:100%;min-width:100%}.card-directive{background:#fff;border:1px solid #d3d3d3;margin:5px!important}chart-layout{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;-webkit-box-flex:1!important;flex:auto!important}.ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.ui-common-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}.example-container{width:100%;height:100%}.mat-elevation-z8{box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12)}.sen-margin-10{margin-left:22px;margin-right:22px}.k-pager-numbers .k-link.k-state-selected{color:#2d323e!important;background-color:#2d323e3d!important}.k-pager-numbers .k-link,span.k-icon.k-i-arrow-e{color:#2d323e!important}.k-grid td{border-width:0 0 0 1px;vertical-align:middle;color:#2c2d48!important;padding:7px!important;border-bottom:1px solid #e0e6ed!important}.k-grid th{padding:12px!important}.k-grid-header .k-header::before{content:\"\"}.k-filter-row>td,.k-filter-row>th,.k-grid td,.k-grid-content-locked,.k-grid-footer,.k-grid-footer-locked,.k-grid-footer-wrap,.k-grid-header,.k-grid-header-locked,.k-grid-header-wrap,.k-grouping-header,.k-grouping-header .k-group-indicator,.k-header{border-color:#2d323e40!important}.k-grid td.k-state-selected,.k-grid tr.k-state-selected>td{background-color:#2d323e24!important}.k-pager-info,.k-pager-input,.k-pager-sizes{margin-left:1em;margin-right:1em;display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-align:center;align-items:center;text-transform:capitalize!important}"]
            }] }
];
/** @nocollapse */
ManageAvbyuserComponent.ctorParameters = () => [
    { type: ReportManagementService },
    { type: ActivatedRoute },
    { type: SnackBarService },
    { type: Location },
    { type: MessageService },
    { type: Router }
];
ManageAvbyuserComponent.propDecorators = {
    paginator: [{ type: ViewChild, args: [MatPaginator,] }],
    drawer: [{ type: ViewChild, args: ['drawer',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWFuYWdlLWF2LWJ5dXNlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYXZtL21hbmFnZS1hdi1ieXVzZXIvbWFuYWdlLWF2LWJ5dXNlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELE9BQU8sRUFBRSxTQUFTLEVBQVUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBRSxZQUFZLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNyRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDL0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBRXZFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUUsK0JBQStCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRSxPQUFPLEVBQUUsK0JBQStCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUd0RSxPQUFPLHdCQUF3QixDQUFDO0FBQ2hDLE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBRWpFLE9BQU8sRUFBK0IsT0FBTyxFQUFRLE1BQU0sNEJBQTRCLENBQUM7QUF1QnhGLE1BQU0sT0FBTyx1QkFBdUI7SUE0SmpDLFlBQW9CLHdCQUFpRCxFQUM5RCxXQUEwQixFQUMxQixlQUFnQyxFQUFVLFFBQWtCLEVBQzdELGNBQStCLEVBQVMsTUFBYztRQUh4Qyw2QkFBd0IsR0FBeEIsd0JBQXdCLENBQXlCO1FBQzlELGdCQUFXLEdBQVgsV0FBVyxDQUFlO1FBQzFCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDN0QsbUJBQWMsR0FBZCxjQUFjLENBQWlCO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQTFKOUQsd01BQXdNO1FBQ3hNLDRIQUE0SDtRQUM1SCxjQUFTLEdBQUcsSUFBSSxjQUFjLENBQWlCLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUV6RCxpQkFBWSxHQUFXLEtBQUssQ0FBQztRQUM3QixnQkFBVyxHQUFRLEVBQUUsQ0FBQztRQUN0QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFJbkIsa0JBQWEsR0FBYSxLQUFLLENBQUM7UUFDaEMsa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFDL0IsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFHaEMsWUFBTyxHQUFHLEVBQUUsQ0FBQztRQU9aLHdCQUFtQixHQUFFLEVBQUUsQ0FBQztRQUl4QixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUV2QixTQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ1osU0FBSSxHQUF3QixTQUFTLENBQUM7UUFDdEMsY0FBUyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEtBQUssRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO1FBQ25ILGlCQUFZLEdBQUcsSUFBSSxDQUFDO1FBQzNCLFVBQUssR0FBVSxFQUFFLENBQUM7UUErR3BCLGFBQVEsR0FBRztZQUNQLFNBQVMsRUFBRyxFQUFFO1NBQ2pCLENBQUM7UUFnSUQsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFySC9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0NBQW9DLEVBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUE7SUFFbEYsQ0FBQztJQTVIRixlQUFlO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2REFBNkQsRUFBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLENBQUM7UUFFbkgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7UUFDL0UsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7UUFDbkYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDO1FBQzdGLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxHQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1FBQ3JGLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUVuRixJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDckMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7WUFHM0IsSUFBSSxJQUFJLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFFekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUMsQ0FBQztZQUVqQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxHQUFDLEVBQUUsQ0FBQTtZQUMzQiwwQkFBMEI7WUFDMUIsS0FBSSxJQUFJLENBQUMsR0FBQyxDQUFDLEVBQUMsQ0FBQyxJQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUM7Z0JBQy9CLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsSUFBRyxjQUFjLElBQUcsY0FBYyxJQUFFLFNBQVMsSUFBSSxjQUFjLElBQUUsUUFBUSxFQUFDO29CQUN6RSxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDO29CQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFDLEVBQUMsS0FBSyxFQUFHLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsR0FBRyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFDLENBQUE7aUJBQ3BIO2FBQ0Q7WUFFRCxLQUFJLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBQztnQkFDcEMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFBO2dCQUNqQixJQUFHLEdBQUcsSUFBRyxHQUFHLElBQUUsU0FBUyxFQUFDO29CQUN2QixJQUFJLE9BQU8sR0FBQyxFQUFFLENBQUM7b0JBQ2YsT0FBTyxDQUFDLFdBQVcsQ0FBQyxHQUFDLEdBQUcsQ0FBQztvQkFDekIsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFDLEdBQUcsR0FBQyxRQUFRLENBQUM7b0JBQ2hDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxHQUFDLFNBQVMsQ0FBQztvQkFDNUQsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO29CQUMxQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxHQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQy9ELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUczQjthQUVEO1lBQ0QsSUFBRyxRQUFRLElBQUksUUFBUSxDQUFDLElBQUksRUFBRTtnQkFDN0IsSUFBRyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sRUFBQztvQkFDMUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsK0JBQStCLENBQUM7aUJBQ25EO3FCQUFLLElBQUksUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUU7b0JBQ3RDLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO29CQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLCtCQUErQixDQUFDO2lCQUNuRDtxQkFBSztvQkFDTCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztvQkFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxnQ0FBZ0MsQ0FBQztpQkFDcEQ7YUFDRDtZQUNELHdEQUF3RDtZQUN4RCxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7WUFDM0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMseUJBQXlCLEVBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3JELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBRUg7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztjQXVDTTtRQUNOLDhEQUE4RDtRQUM5RCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtJQUM1QixDQUFDO0lBbUJFLFFBQVE7UUFDTixJQUFJLENBQUMsS0FBSyxHQUFDO1lBQ1YsSUFBSSxFQUFFLElBQUksQ0FBQyxNQUFNO1lBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSztTQUNqQixDQUFBO1FBQ0QsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUd4QixDQUFDO0lBQ0QsZUFBZSxDQUFDLEtBQTJCO1FBQzFDLElBQUksQ0FBQyxLQUFLLEdBQUUsS0FBSyxDQUFDO1FBQ2xCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hDLENBQUM7SUFDSCxlQUFlLENBQUMsS0FBWTtRQUMzQixJQUFJLENBQUMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUM7SUFDSCxPQUFPLENBQUMsTUFBTTtRQUNiLElBQUcsTUFBTSxJQUFFLFNBQVMsRUFDcEI7WUFDRSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDQSxDQUFDO0lBQ0gsVUFBVSxDQUFDLEtBQUs7UUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSx1QkFBdUIsQ0FBQyxDQUFBO1FBQzNDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztRQUN4QyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFDRCxtQkFBbUI7UUFDcEIsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDckMsQ0FBQztJQUNKLFdBQVcsQ0FBQyxXQUFtQjtRQUM1QixJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVBLGNBQWM7UUFDZixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3JCLElBQUksR0FBRyxHQUFHO1lBQ1QsRUFBRSxFQUFHLGdCQUFnQjtTQUNyQixDQUFDO1FBQ0YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNWLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsRUFBQyxHQUFHLENBQUMsQ0FBQTtJQUNMLENBQUM7SUFFRixRQUFRO1FBQ1AsSUFBSSxHQUFHLEdBQUc7WUFDVCxFQUFFLEVBQUcsV0FBVztTQUNoQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUM7UUFDckMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyw2QkFBNkIsQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQztJQUVBLGFBQWE7UUFDYixJQUFJLENBQUMsd0JBQXdCLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDakUsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDO1lBQ2pELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7WUFDM0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQTtJQUNILENBQUM7SUFFQSxlQUFlLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsZUFBZSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzFCLENBQUM7SUFDRCxXQUFXLENBQUMsS0FBSztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQTtRQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDeEMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsVUFBUyxJQUFJO1lBQ2xDLElBQUcsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUN4QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN4QjtpQkFBSTtnQkFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN6QjtRQUNMLENBQUMsQ0FBQyxDQUFBO1FBQ1IseUVBQXlFO1FBQ3pFLG9CQUFvQjtRQUNwQix3QkFBd0I7UUFDeEIsbUJBQW1CO1FBQ25CLGdDQUFnQztRQUNoQyw4QkFBOEI7UUFDOUIsb0JBQW9CO1FBQ3BCLE1BQU07UUFDTixzQkFBc0I7UUFDdEIsZ0RBQWdEO1FBQ2hELHlDQUF5QztRQUN6Qyx3RUFBd0U7UUFDeEUsTUFBTTtRQUNOLFlBQVk7UUFDWixxQkFBcUI7UUFDckIsZ0NBQWdDO1FBQ2hDLDhCQUE4QjtRQUM5QixxQkFBcUI7UUFDckIsTUFBTTtRQUNOLHNCQUFzQjtRQUN0QixrREFBa0Q7UUFDbEQseUNBQXlDO1FBQ3pDLHdFQUF3RTtRQUN4RSxNQUFNO1FBQ04sS0FBSztRQUNMLElBQUk7UUFDSixZQUFZLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDMUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQ3pFLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztJQUN4QixDQUFDO0lBQ0QsZUFBZSxDQUFDLE9BQU8sRUFBRSxPQUFPO1FBQy9CLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3hDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxPQUFPLEVBQUU7Z0JBQ2pDLE9BQU8sQ0FBQyxDQUFDO2FBQ1Q7U0FDRDtJQUNGLENBQUM7SUFHRCxZQUFZO1FBQ1gsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUE7SUFDM0MsQ0FBQztJQUNELGVBQWU7UUFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBQyx3QkFBd0IsQ0FBQyxDQUFBO1FBQzNELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3RFLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNyQixLQUFLLElBQUksT0FBTyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ3JDLElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTtvQkFDckIsNkNBQTZDO29CQUM3QyxnREFBZ0Q7b0JBQ2hELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM3QzthQUNEO1NBQ0Q7YUFBTTtZQUNOLElBQUcsSUFBSSxDQUFDLGFBQWEsRUFBQztnQkFDckIsSUFBSSxDQUFDLFdBQVcsR0FBRywrQkFBK0IsQ0FBQzthQUNuRDtpQkFBSyxJQUFHLElBQUksQ0FBQyxhQUFhLEVBQUM7Z0JBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsK0JBQStCLENBQUM7YUFDbkQ7aUJBQUssSUFBRyxJQUFJLENBQUMsY0FBYyxFQUFFO2dCQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLGdDQUFnQyxDQUFDO2FBQ3BEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFDLHdCQUF3QixDQUFDLENBQUE7WUFDdEQsS0FBSyxJQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNyQyxJQUFJLE9BQU8sQ0FBQyxRQUFRLEVBQUU7b0JBQ3JCLDZDQUE2QztvQkFDN0MsZ0RBQWdEO29CQUNoRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFFN0M7YUFDRDtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFDLDJCQUEyQixDQUFDLENBQUE7WUFDOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsRUFBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztTQUNsRTtJQUNGLENBQUM7SUFFQSxlQUFlLENBQUMsRUFBRTtRQUNsQixJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFBO0lBRTlCLENBQUM7SUFFRCxtQkFBbUI7UUFDWixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFDLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUE7UUFDbEYsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxDQUFDLFlBQVksR0FBQyxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLFlBQVksQ0FBQyxZQUFZLENBQUMsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztRQUN2RixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLGtCQUFrQixDQUFDLEdBQUUsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztRQUNoRyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDO1FBQ25GLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7UUFFdkYsSUFBSSxDQUFDLHdCQUF3QixDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFFbkYsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUNaLE1BQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUM7Z0JBQ3pELFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLDJCQUEyQixDQUFDLENBQUM7YUFDdkQ7aUJBQ0k7Z0JBQ0QsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztnQkFDMUQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsbUJBQW1CLENBQUMsQ0FBQzthQUMvQztRQUNMLENBQUMsRUFBRSxLQUFLLENBQUMsRUFBRTtZQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztRQUNwRCxDQUFDLENBQUMsQ0FBQztJQUdQLENBQUM7OztZQW5XSixTQUFTLFNBQUM7Z0JBQ1YsUUFBUSxFQUFFLHNCQUFzQjtnQkFDL0IsOGdOQUFnRDs7YUFFakQ7Ozs7WUFuQ1EsdUJBQXVCO1lBVWYsY0FBYztZQVJ0QixlQUFlO1lBSGYsUUFBUTtZQVlSLGNBQWM7WUFEZCxNQUFNOzs7d0JBa0xkLFNBQVMsU0FBQyxZQUFZO3FCQUNyQixTQUFTLFNBQUMsUUFBUSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2NvbGxlY3Rpb25zJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRQYWdpbmF0b3IsIE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTWF0U2lkZW5hdiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBSZXBvcnRNYW5hZ2VtZW50U2VydmljZSB9IGZyb20gJy4uL3JlcG9ydC1tYW5hZ2VtZW50LnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZSc7XHJcbmltcG9ydCB7IG1hbmFnZUF2YnlMZWRnZXJVc2VyVGFibGVDb25maWcgfSBmcm9tICcuLi8uLi90YWJsZV9jb25maWcnO1xyXG5pbXBvcnQgeyBtYW5hZ2VBdmJ5VmVuZG9yVXNlclRhYmxlQ29uZmlnIH0gZnJvbSAnLi4vLi4vdGFibGVfY29uZmlnJztcclxuaW1wb3J0IHsgbWFuYWdlQXZieUludm9pY2VVc2VyVGFibGVDb25maWcgfSBmcm9tICcuLi8uLi90YWJsZV9jb25maWcnO1xyXG5pbXBvcnQge0RhdGFTb3VyY2V9IGZyb20gJ0Bhbmd1bGFyL2Nkay9jb2xsZWN0aW9ucyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzL1J4J1xyXG5pbXBvcnQgJ3J4anMvYWRkL29ic2VydmFibGUvb2YnO1xyXG5pbXBvcnQgKiBhcyBGaWxlU2F2ZXIgZnJvbSAnZmlsZS1zYXZlcic7XHJcbmltcG9ydCB7IFJvdXRlciwgQWN0aXZhdGVkUm91dGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gJy4uLy4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHsgR3JvdXBEZXNjcmlwdG9yLCBEYXRhUmVzdWx0LCBwcm9jZXNzLFN0YXRlIH0gZnJvbSAnQHByb2dyZXNzL2tlbmRvLWRhdGEtcXVlcnknO1xyXG5pbXBvcnQgeyBHcmlkRGF0YVJlc3VsdCwgUGFnZUNoYW5nZUV2ZW50LFNlbGVjdEFsbENoZWNrYm94U3RhdGUsRGF0YVN0YXRlQ2hhbmdlRXZlbnQgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tYW5ndWxhci1ncmlkJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgbWFuYWdlQXZieXVzZXIge1xyXG4gIGxlZGdlcjogc3RyaW5nO1xyXG4gIGpvdXJuYWxuYW1lOiBzdHJpbmc7XHJcbiAgam91cm5hbGRlc2NyaXB0aW9uOiBzdHJpbmc7XHJcbiAgam91cm5hbHNvdXJjZTogc3RyaW5nO1xyXG5cdGpvdXJuYWxjYXRlZ29yeTogc3RyaW5nO1xyXG5cdGpvdXJuYWxwZXJpb2Q6IHN0cmluZztcclxuXHRjdXJyZW5jeWNvZGU6IHN0cmluZztcclxuXHRzdGF0dXM6IHN0cmluZztcclxuXHRjcmVhdGVkYnk6IHN0cmluZztcclxuXHRwb3N0ZWRieTogc3RyaW5nO1xyXG5cdGFtb3VudDogbnVtYmVyO1xyXG5cdHBvc3RlZGRhdGU6IERhdGU7XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG5cdHNlbGVjdG9yOiAnYXBwLW1hbmFnZS1hdi1ieXVzZXInLFxyXG4gIHRlbXBsYXRlVXJsOiAnLi9tYW5hZ2UtYXYtYnl1c2VyLmNvbXBvbmVudC5odG1sJyxcclxuXHRzdHlsZVVybHM6IFsnLi9tYW5hZ2UtYXYtYnl1c2VyLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIE1hbmFnZUF2Ynl1c2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRtYW5hZ2VBdnNieXVzZXI6YW55O1xyXG5cdC8vZGF0YVNvdXJjZTphbnk7XHJcblx0Y3VzdG9tZGF0YXNvdXJjZTpvYmplY3RbXTtcclxuXHRkZWZhdWx0ZGF0YXNvdXJjZUlkOnN0cmluZztcclxuXHQvL2Rpc3BsYXllZENvbHVtbnM6IHN0cmluZ1tdID0gWydsZWRnZXInLCAnam91cm5hbG5hbWUnLCAnam91cm5hbGRlc2NyaXB0aW9uJywgJ2pvdXJuYWxzb3VyY2UnLCAnam91cm5hbGNhdGVnb3J5Jywnam91cm5hbHBlcmlvZCcsJ2N1cnJlbmN5Y29kZScsJ3N0YXR1cycsJ2NyZWF0ZWRieScsJ3Bvc3RlZGJ5JywnYW1vdW50JywncG9zdGVkZGF0ZSddO1xyXG5cdC8vZGlzcGxheWVkQ29sdW1uczogc3RyaW5nW10gPSBbJ3NlbGVjdCcsICdjb250cm9sbmFtZScsICdkZXNjcmlwdGlvbicsICd1c2VyY291bnRzJywgJ2RhdGFzb3VyY2UnLCAnY29udHJvbHR5cGUnLCdhY3Rpb24nXTtcclxuXHRzZWxlY3Rpb24gPSBuZXcgU2VsZWN0aW9uTW9kZWw8bWFuYWdlQXZieXVzZXI+KHRydWUsIFtdKTtcclxuXHRkaWFsb2dSZWY6IGFueTtcclxuXHRuYXZfcG9zaXRpb246IHN0cmluZyA9ICdlbmQnO1xyXG5cdHF1ZXJ5UGFyYW1zOiBhbnkgPSB7fTtcclxuXHRsaW1pdDogbnVtYmVyID0gMTA7XHJcblx0b2Zmc2V0OiBudW1iZXIgPSAwO1xyXG5cdGxlbmd0aDogbnVtYmVyO1xyXG5cdHRhYmxlQ29uZmlnOiBhbnk7XHJcblx0YXZtRXhwb3J0T2JqOnt9O1xyXG5cdGlzTGVkZ2VyVGFibGUgOiBCb29sZWFuID0gZmFsc2U7XHJcblx0aXNWZW5kb3JUYWJsZTogQm9vbGVhbiA9IGZhbHNlO1xyXG5cdGlzSW52b2ljZVRhYmxlOiBCb29sZWFuID0gZmFsc2U7XHJcblx0XHJcblxyXG5cdGNvbHVtbnMgPSBbXTtcclxuXHRzZXR0aW5nczphbnk7XHJcblx0ZGF0YTphbnk7XHJcblx0dGVtcHNldHRpbmdzOm9iamVjdDtcclxuXHR0ZW1wQ29sczpvYmplY3Q7XHJcblx0ZGF0YVNvdXJjZSA6YW55O1xyXG5cdGRpc3BsYXllZENvbHVtbnM6YW55O1xyXG5cdFx0c2VsZWN0ZWRUYWJsZUhlYWRlcj0gW107XHJcblxyXG5cclxuICBrZW5kb0dyaWREYXRhIDogR3JpZERhdGFSZXN1bHQ7XHJcbiAgZW5hYmxlRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgcHVibGljIF9kYXRhOiBhbnlbXTtcclxuICBwdWJsaWMgaW5mbyA9IHRydWU7XHJcbiAgcHVibGljIHR5cGU6IFwibnVtZXJpY1wiIHwgXCJpbnB1dFwiID0gXCJudW1lcmljXCI7XHJcbiAgcHVibGljIHBhZ2VTaXplcyA9IFt7IHRleHQ6IDEwLCB2YWx1ZTogMTAgfSwgeyB0ZXh0OiAyNSwgdmFsdWU6IDI1IH0sIHsgdGV4dDogNTAsIHZhbHVlOiA1MCB9LCB7IHRleHQ6IDEwMCwgdmFsdWU6IDEwMCB9XTtcclxuICBwdWJsaWMgcHJldmlvdXNOZXh0ID0gdHJ1ZTtcclxuICBzdGF0ZTogU3RhdGUgPSB7fTtcclxuXHJcblx0Z2V0Tm90aWZpY2F0aW9uKCkge1xyXG5cdFx0Y29uc29sZS5sb2coJ0RvIHNvbWV0aGluZyB3aXRoIHRoZSBub3RpZmljYXRpb24gKGV2dCkgc2VudCBieSB0aGUgY2hpbGQhJyx0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUpO1xyXG5cclxuXHRcdHRoaXMucXVlcnlQYXJhbXMuY3JlYXRlZGJ5ID0gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmNyZWF0ZWRCeTtcclxuXHRcdHRoaXMucXVlcnlQYXJhbXMuam91cm5hbG5hbWUgPSB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUuam91cm5hbG5hbWU7XHJcblx0XHR0aGlzLnF1ZXJ5UGFyYW1zLmNvbGxlY3Rpb25EZXRhaWwgPSB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUuY29sbGVjdGlvbkRldGFpbDtcclxuXHRcdHRoaXMucXVlcnlQYXJhbXMuZGF0YXNvdXJjZTIgPSAgdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmRhdGFzb3VyY2VJZDtcclxuXHRcdGNvbnNvbGUubG9nKCd0aGlzLnF1ZXJ5UGFyYW1zJyx0aGlzLnF1ZXJ5UGFyYW1zKTtcclxuXHRcdHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldGFsbEF2Qnl1c2VyKHRoaXMucXVlcnlQYXJhbXMpLnN1YnNjcmliZShyZXNwb25zZSA9PiB7XHJcbiAgICAgIFxyXG5cdFx0XHR0aGlzLm1hbmFnZUF2c2J5dXNlciA9IHJlc3BvbnNlLmRhdGE7XHJcblx0XHRcdGNvbnNvbGUubG9nKHJlc3BvbnNlLnR5cGUpO1xyXG5cclxuXHRcdFxyXG5cdFx0XHR2YXIgY29scyA9IE9iamVjdC5rZXlzKHJlc3BvbnNlLmRhdGFbMF0pO1xyXG5cdFx0XHRcclxuXHRcdFx0dGhpcy5zZXR0aW5ncyA9IHthY3Rpb25zOiBmYWxzZX07XHJcblxyXG5cdFx0XHR0aGlzLnNldHRpbmdzW1wiY29sdW1uc1wiXT17fVxyXG5cdFx0XHQvL3ZhciBhcnI9W2lkLG5hbWUsYW1vdW50XVxyXG5cdFx0XHRmb3IodmFyIGk9MDtpPD1jb2xzLmxlbmd0aDsgaSsrKXtcclxuXHRcdFx0XHR2YXIgY29sRGlzcGxheW5hbWUgPSBjb2xzW2ldO1xyXG5cdFx0XHRcdGlmKGNvbERpc3BsYXluYW1lICYmY29sRGlzcGxheW5hbWUhPXVuZGVmaW5lZCAmJiBjb2xEaXNwbGF5bmFtZSE9J3N0YXR1cycpe1xyXG5cdFx0XHRcdFx0Y29uc29sZS5sb2coY29sRGlzcGxheW5hbWUpO1xyXG5cdFx0XHRcdFx0dGhpcy5zZXR0aW5nc1tcImNvbHVtbnNcIl1bY29sc1tpXV09e3RpdGxlOiAgY29sRGlzcGxheW5hbWVbMF0udG9VcHBlckNhc2UoKSArIGNvbERpc3BsYXluYW1lLnN1YnN0cigxKS50b0xvd2VyQ2FzZSgpfVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Zm9yKHZhciBpID0gMDsgaSA8PSBjb2xzLmxlbmd0aDsgaSsrKXtcclxuXHRcdFx0XHR2YXIgY29sID0gY29sc1tpXVxyXG5cdFx0XHRcdGlmKGNvbCAmJmNvbCE9dW5kZWZpbmVkKXtcclxuXHRcdFx0XHRcdHZhciBvYmpDb2xzPXt9O1xyXG5cdFx0XHRcdFx0b2JqQ29sc1snY29sdW1uRGVmJ109Y29sO1xyXG5cdFx0XHRcdFx0b2JqQ29sc1snZm9ybUN0bCddPWNvbCsnRmlsdGVyJztcclxuXHRcdFx0XHRcdG9iakNvbHNbJ3BsYWNlSG9sZGVOYW1lJ109Y29sLnRvTG9jYWxlVXBwZXJDYXNlKCkrJyBGaWx0ZXInO1xyXG5cdFx0XHRcdFx0b2JqQ29sc1snaGVhZGVyJ109Y29sLnRvTG9jYWxlVXBwZXJDYXNlKCk7XHJcblx0XHRcdFx0XHRvYmpDb2xzWyd0aXRsZSddPWNvbC50b1VwcGVyQ2FzZSgpK2NvbC5zdWJzdHIoMSkudG9Mb3dlckNhc2UoKTtcclxuXHRcdFx0XHRcdHRoaXMuY29sdW1ucy5wdXNoKG9iakNvbHMpO1xyXG5cdFx0XHRcdFx0XHJcblxyXG5cdFx0XHRcdH1cclxuXHRcdFx0XHRcdFxyXG5cdFx0IH1cclxuXHRcdFx0aWYocmVzcG9uc2UgJiYgcmVzcG9uc2UuZGF0YSkge1xyXG5cdFx0XHRcdGlmKHJlc3BvbnNlLmRhdGFbMF0ubGVkZ2VyKXtcclxuXHRcdFx0XHRcdHRoaXMuaXNMZWRnZXJUYWJsZSA9IHRydWU7XHJcblx0XHRcdFx0XHR0aGlzLnRhYmxlQ29uZmlnID0gbWFuYWdlQXZieUxlZGdlclVzZXJUYWJsZUNvbmZpZztcclxuXHRcdFx0XHR9ZWxzZSBpZiAocmVzcG9uc2UuZGF0YVswXS52ZW5kb3JuYW1lKSB7XHJcblx0XHRcdFx0XHR0aGlzLmlzVmVuZG9yVGFibGUgPSB0cnVlO1xyXG5cdFx0XHRcdFx0dGhpcy50YWJsZUNvbmZpZyA9IG1hbmFnZUF2YnlWZW5kb3JVc2VyVGFibGVDb25maWc7XHJcblx0XHRcdFx0fWVsc2Uge1xyXG5cdFx0XHRcdFx0dGhpcy5pc0ludm9pY2VUYWJsZSA9IHRydWU7XHJcblx0XHRcdFx0XHR0aGlzLnRhYmxlQ29uZmlnID0gbWFuYWdlQXZieUludm9pY2VVc2VyVGFibGVDb25maWc7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdC8vdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gT2JqZWN0LmtleXMocmVzcG9uc2UuZGF0YVswXSk7XHJcblx0XHRcdHRoaXMubG9hZFRhYmxlQ29sdW1uKCk7XHJcblx0XHRcdHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2UuZGF0YSk7XHJcblx0XHRcdHRoaXMuX2RhdGEgPSByZXNwb25zZS5kYXRhO1xyXG5cdFx0XHR0aGlzLmxlbmd0aCA9IHJlc3BvbnNlLmRhdGEubGVuZ3RoO1xyXG5cdFx0XHR0aGlzLmRhdGFTb3VyY2UucGFnaW5hdG9yID0gdGhpcy5wYWdpbmF0b3I7XHJcblx0XHRcdGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4gcmVzcG9uc2UuZGF0YSBcIixyZXNwb25zZS5kYXRhKTtcclxuXHRcdFx0dGhpcy5hcHBseVRhYmxlU3RhdGUodGhpcy5zdGF0ZSk7XHJcblx0XHR9KTtcclxuXHJcblx0XHQvKnRoaXMuc2V0dGluZ3MgPSB7XHJcblx0XHRcdGNvbHVtbnM6IHtcclxuXHRcdFx0ICBpZDoge1xyXG5cdFx0XHRcdHRpdGxlOiAnSUQnXHJcblx0XHRcdCAgfSxcclxuXHRcdFx0ICBuYW1lOiB7XHJcblx0XHRcdFx0dGl0bGU6ICdGdWxsIE5hbWUnXHJcblx0XHRcdCAgfSxcclxuXHRcdFx0ICB1c2VybmFtZToge1xyXG5cdFx0XHRcdHRpdGxlOiAnVXNlciBOYW1lJ1xyXG5cdFx0XHQgIH0sXHJcblx0XHRcdCAgZW1haWw6IHtcclxuXHRcdFx0XHR0aXRsZTogJ0VtYWlsJ1xyXG5cdFx0XHQgIH1cclxuXHRcdFx0fVxyXG5cdFx0ICB9O1xyXG5cclxuXHRcdCAgdGhpcy5kYXRhID0gW1xyXG5cdFx0XHR7XHJcblx0XHRcdCAgaWQ6IDEsXHJcblx0XHRcdCAgbmFtZTogXCJMZWFubmUgR3JhaGFtXCIsXHJcblx0XHRcdCAgdXNlcm5hbWU6IFwiQnJldFwiLFxyXG5cdFx0XHQgIGVtYWlsOiBcIlNpbmNlcmVAYXByaWwuYml6XCJcclxuXHRcdFx0fSxcclxuXHRcdFx0e1xyXG5cdFx0XHQgIGlkOiAyLFxyXG5cdFx0XHQgIG5hbWU6IFwiRXJ2aW4gSG93ZWxsXCIsXHJcblx0XHRcdCAgdXNlcm5hbWU6IFwiQW50b25ldHRlXCIsXHJcblx0XHRcdCAgZW1haWw6IFwiU2hhbm5hQG1lbGlzc2EudHZcIlxyXG5cdFx0XHR9LFxyXG5cdFx0XHRcclxuXHRcdFx0Ly8gLi4uIGxpc3Qgb2YgaXRlbXNcclxuXHRcdFx0XHJcblx0XHRcdHtcclxuXHRcdFx0ICBpZDogMTEsXHJcblx0XHRcdCAgbmFtZTogXCJOaWNob2xhcyBEdUJ1cXVlXCIsXHJcblx0XHRcdCAgdXNlcm5hbWU6IFwiTmljaG9sYXMuU3RhbnRvblwiLFxyXG5cdFx0XHQgIGVtYWlsOiBcIlJleS5QYWRiZXJnQHJvc2Ftb25kLmJpelwiXHJcblx0XHRcdH1cclxuXHRcdCAgXTsqL1xyXG5cdFx0Ly8gRG8gc29tZXRoaW5nIHdpdGggdGhlIG5vdGlmaWNhdGlvbiAoZXZ0KSBzZW50IGJ5IHRoZSBjaGlsZCFcclxuXHRcdGNvbnNvbGUubG9nKHRoaXMudmlld0RhdGEpXHJcbn1cclxuXHJcbnZpZXdEYXRhID0geyBcclxuICAgIGVtcGxveWVlcyA6IFtdIFxyXG59O1xyXG5AVmlld0NoaWxkKE1hdFBhZ2luYXRvcikgcGFnaW5hdG9yOiBNYXRQYWdpbmF0b3I7XHJcblx0QFZpZXdDaGlsZCgnZHJhd2VyJykgZHJhd2VyOiBNYXRTaWRlbmF2O1xyXG5cclxuXHQgXHJcblx0ICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9yZXBvcnRNYW5hZ2VtZW50U2VydmljZTogUmVwb3J0TWFuYWdlbWVudFNlcnZpY2UsXHJcblx0XHRwcml2YXRlIGFjdGl2ZVJvdXRlOkFjdGl2YXRlZFJvdXRlLFxyXG5cdFx0cHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSwgcHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sXHJcblx0cHJpdmF0ZSBtZXNzYWdlU2VydmljZSA6IE1lc3NhZ2VTZXJ2aWNlLHByaXZhdGUgcm91dGVyOiBSb3V0ZXJcclxuXHRcclxuICkgeyBcclxuXHQgY29uc29sZS5sb2coXCJhdiBkZXRhaWwgVVJMIFBhcm1zICoqKioqKioqKioqKioqXCIsdGhpcy5hY3RpdmVSb3V0ZS5zbmFwc2hvdC5wYXJhbXMpXHJcblx0XHJcbiAgfVxyXG4gIFxyXG4gICBuZ09uSW5pdCgpIHtcclxuICAgICB0aGlzLnN0YXRlPXtcclxuICAgICAgc2tpcDogdGhpcy5vZmZzZXQsXHJcbiAgICAgIHRha2U6IHRoaXMubGltaXRcclxuICAgIH1cclxuICAgIHRoaXMuZ2V0ZGF0YXNvdXJjZSgpO1xyXG5cdFx0dGhpcy5nZXROb3RpZmljYXRpb24oKTtcclxuXHRcdFxyXG5cclxuXHR9XHJcblx0ZGF0YVN0YXRlQ2hhbmdlKHN0YXRlOiBEYXRhU3RhdGVDaGFuZ2VFdmVudCk6IHZvaWQge1xyXG5cdFx0dGhpcy5zdGF0ZT0gc3RhdGU7XHJcblx0XHR0aGlzLmFwcGx5VGFibGVTdGF0ZSh0aGlzLnN0YXRlKTtcclxuXHQgIH1cclxuXHRhcHBseVRhYmxlU3RhdGUoc3RhdGU6IFN0YXRlKTogdm9pZCB7XHJcblx0XHR0aGlzLmtlbmRvR3JpZERhdGEgPSBwcm9jZXNzKHRoaXMuX2RhdGEsIHN0YXRlKTtcclxuXHQgIH1cclxuXHRvbkNsaWNrKGFjdGlvbil7XHJcblx0XHRpZihhY3Rpb249PSdyZWZyZXNoJylcclxuXHRcdHtcclxuXHRcdCAgdGhpcy5uZ09uSW5pdCgpO1xyXG5cdFx0fVxyXG5cdCAgfSAgXHJcblx0Y2hhbmdlUGFnZShldmVudCkge1xyXG5cdFx0Y29uc29sZS5sb2coZXZlbnQsIFwiPj4+IEVWRU5UIENoYW5nZSBQYWdlXCIpXHJcblx0XHR0aGlzLnF1ZXJ5UGFyYW1zW1wib2Zmc2V0XCJdID0gZXZlbnQuc2tpcDtcclxuXHRcdHRoaXMucXVlcnlQYXJhbXNbXCJsaW1pdFwiXSA9IHRoaXMubGltaXQ7XHJcblx0XHR0aGlzLm9mZnNldCA9IGV2ZW50LnNraXA7XHJcblx0ICB9XHJcblx0ICBlbmFibGVGaWx0ZXJPcHRpb25zKCkge1xyXG5cdFx0dGhpcy5lbmFibGVGaWx0ZXIgPSAhdGhpcy5lbmFibGVGaWx0ZXI7ICAgXHJcblx0ICAgfVxyXG5cdGFwcGx5RmlsdGVyKGZpbHRlclZhbHVlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuZGF0YVNvdXJjZS5maWx0ZXIgPSBmaWx0ZXJWYWx1ZS50cmltKCkudG9Mb3dlckNhc2UoKTtcclxuXHR9XHJcblx0XHJcbiAgYmFja1RvQVZEZXRhaWwoKSB7XHJcblx0dGhpcy5sb2NhdGlvbi5iYWNrKCk7XHJcblx0bGV0IG9iaiA9IHtcclxuXHRcdGlkIDogXCJtYW5hZ2VBdkRldGFpbFwiXHJcblx0fTtcclxuXHRsZXQgc2VsZiA9IHRoaXM7XHJcblx0c2V0VGltZW91dChmdW5jdGlvbigpe1xyXG5cdFx0c2VsZi5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyhvYmopO1xyXG5cdH0sMTAwKVxyXG4gIH1cclxuXHJcblx0YmFja1RvQVYoKSB7XHJcblx0XHRsZXQgb2JqID0ge1xyXG5cdFx0XHRpZCA6IFwibWFuYWdlQXZtXCJcclxuXHRcdH07XHJcblx0XHR0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKG9iaik7XHJcblx0XHR0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ3JlcG9ydC1tYW5hZ2VtZW50L21hbmFnZS1hdiddKTtcclxuXHR9XHJcblx0XHJcbiAgZ2V0ZGF0YXNvdXJjZSgpIHtcclxuXHRcdHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLmdldEFsbERhdGFzb3VyY2VzKCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcblx0XHRcdHRoaXMuY3VzdG9tZGF0YXNvdXJjZSA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlcztcclxuXHRcdFx0dGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLl9pZDtcclxuXHRcdFx0dGhpcy5vbkxvYWRtYW5hZ2VBdnModGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkKTtcclxuXHRcdH0pXHJcblx0fVxyXG5cclxuICBnZXREYXRhc291cmNlSWQoaWQpIHtcclxuXHRcdHRoaXMub25Mb2FkbWFuYWdlQXZzKGlkKTtcclxuXHR9XHJcblx0dXBkYXRlVGFibGUoZXZlbnQpIHtcclxuXHRcdGNvbnNvbGUubG9nKGV2ZW50LCBcIi0tLS1ldmVudFwiKVxyXG5cdFx0bGV0IHNlbGVjdGVkSGVhZGVyID0gdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyO1xyXG4gICAgICAgIHRoaXMudGFibGVDb25maWcuZm9yRWFjaChmdW5jdGlvbihpdGVtKXtcclxuICAgICAgICAgICAgaWYoc2VsZWN0ZWRIZWFkZXIuaW5kZXhPZihpdGVtLnZhbHVlKSA+PSAwKSB7XHJcbiAgICAgICAgICAgICAgICBpdGVtLmlzYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICBpdGVtLmlzYWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KVxyXG5cdFx0Ly8gdmFyIGluZGV4ID0gdGhpcy50YWJsZU5hbWVzZWFyY2goZXZlbnQuc291cmNlLnZhbHVlLCB0aGlzLnRhYmxlQ29uZmlnKVxyXG5cdFx0Ly8gaWYgKGluZGV4ID49IDApIHtcclxuXHRcdC8vIFx0aWYgKGV2ZW50LmNoZWNrZWQpIHtcclxuXHRcdC8vIFx0XHRsZXQgYWN0aXZlID0ge1xyXG5cdFx0Ly8gXHRcdFx0dmFsdWU6IGV2ZW50LnNvdXJjZS52YWx1ZSxcclxuXHRcdC8vIFx0XHRcdG5hbWU6IGV2ZW50LnNvdXJjZS5uYW1lLFxyXG5cdFx0Ly8gXHRcdFx0aXNhY3RpdmU6IHRydWVcclxuXHRcdC8vIFx0XHR9XHJcblx0XHQvLyBcdFx0aWYgKGluZGV4ID49IDApIHtcclxuXHRcdC8vIFx0XHRcdHRoaXMudGFibGVDb25maWcuc3BsaWNlKGluZGV4LCAxLCBhY3RpdmUpO1xyXG5cdFx0Ly8gXHRcdFx0bG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oJ2NvbnRyb2wnKTtcclxuXHRcdC8vIFx0XHRcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjb250cm9sJywgSlNPTi5zdHJpbmdpZnkodGhpcy50YWJsZUNvbmZpZykpO1xyXG5cdFx0Ly8gXHRcdH1cclxuXHRcdC8vIFx0fSBlbHNlIHtcclxuXHRcdC8vIFx0XHRsZXQgaW5hY3RpdmUgPSB7XHJcblx0XHQvLyBcdFx0XHR2YWx1ZTogZXZlbnQuc291cmNlLnZhbHVlLFxyXG5cdFx0Ly8gXHRcdFx0bmFtZTogZXZlbnQuc291cmNlLm5hbWUsXHJcblx0XHQvLyBcdFx0XHRpc2FjdGl2ZTogZmFsc2VcclxuXHRcdC8vIFx0XHR9XHJcblx0XHQvLyBcdFx0aWYgKGluZGV4ID49IDApIHtcclxuXHRcdC8vIFx0XHRcdHRoaXMudGFibGVDb25maWcuc3BsaWNlKGluZGV4LCAxLCBpbmFjdGl2ZSk7XHJcblx0XHQvLyBcdFx0XHRsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnY29udHJvbCcpO1xyXG5cdFx0Ly8gXHRcdFx0bG9jYWxTdG9yYWdlLnNldEl0ZW0oJ2NvbnRyb2wnLCBKU09OLnN0cmluZ2lmeSh0aGlzLnRhYmxlQ29uZmlnKSk7XHJcblx0XHQvLyBcdFx0fVxyXG5cdFx0Ly8gXHR9XHJcblx0XHQvLyB9XHJcblx0XHRsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSgnbWFuYWdlQXZieXVzZXInKTtcclxuXHRcdGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdtYW5hZ2VBdmJ5dXNlcicsIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVDb25maWcpKTtcclxuXHRcdHRoaXMubG9hZFRhYmxlQ29sdW1uKCk7XHJcblx0fVxyXG5cdHRhYmxlTmFtZXNlYXJjaChuYW1lS2V5LCBteUFycmF5KSB7XHJcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IG15QXJyYXkubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0aWYgKG15QXJyYXlbaV0udmFsdWUgPT09IG5hbWVLZXkpIHtcclxuXHRcdFx0XHRyZXR1cm4gaTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHRwYW5lbE9wZW5TdGF0ZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuXHR0YWJsZVNldHRpbmcoKSB7XHJcblx0XHR0aGlzLnBhbmVsT3BlblN0YXRlID0gIXRoaXMucGFuZWxPcGVuU3RhdGVcclxuXHR9XHJcblx0bG9hZFRhYmxlQ29sdW1uKCkge1xyXG5cdFx0Y29uc29sZS5sb2codGhpcy5kaXNwbGF5ZWRDb2x1bW5zLFwiLi4uLi4uZGlzcGxheWVkQ29sdW1uc1wiKVxyXG5cdFx0dGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gW107XHJcblx0XHR0aGlzLnRhYmxlQ29uZmlnID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbWFuYWdlQXZieXVzZXInKSk7XHJcblx0XHRpZiAodGhpcy50YWJsZUNvbmZpZykge1xyXG5cdFx0XHRmb3IgKGxldCBjb2x1bW5zIG9mIHRoaXMudGFibGVDb25maWcpIHtcclxuXHRcdFx0XHRpZiAoY29sdW1ucy5pc2FjdGl2ZSkge1xyXG5cdFx0XHRcdFx0Ly8gdGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcblx0XHRcdFx0XHQvLyB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuXHRcdFx0XHRcdHRoaXMuZGlzcGxheWVkQ29sdW1ucy5wdXNoKGNvbHVtbnMpO1xyXG5cdFx0XHRcdFx0dGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyLnB1c2goY29sdW1ucy52YWx1ZSk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9IGVsc2Uge1xyXG5cdFx0XHRpZih0aGlzLmlzTGVkZ2VyVGFibGUpe1xyXG5cdFx0XHRcdHRoaXMudGFibGVDb25maWcgPSBtYW5hZ2VBdmJ5TGVkZ2VyVXNlclRhYmxlQ29uZmlnO1xyXG5cdFx0XHR9ZWxzZSBpZih0aGlzLmlzVmVuZG9yVGFibGUpe1xyXG5cdFx0XHRcdHRoaXMudGFibGVDb25maWcgPSBtYW5hZ2VBdmJ5VmVuZG9yVXNlclRhYmxlQ29uZmlnO1xyXG5cdFx0XHR9ZWxzZSBpZih0aGlzLmlzSW52b2ljZVRhYmxlKSB7XHJcblx0XHRcdFx0dGhpcy50YWJsZUNvbmZpZyA9IG1hbmFnZUF2YnlJbnZvaWNlVXNlclRhYmxlQ29uZmlnO1xyXG5cdFx0XHR9XHJcblx0XHRcdGNvbnNvbGUubG9nKHRoaXMudGFibGVDb25maWcsXCIuLi4uLi4uLi4uLnRhYmxlY29uZmlnXCIpXHJcblx0XHRcdGZvciAobGV0IGNvbHVtbnMgb2YgdGhpcy50YWJsZUNvbmZpZykge1xyXG5cdFx0XHRcdGlmIChjb2x1bW5zLmlzYWN0aXZlKSB7XHJcblx0XHRcdFx0XHQvLyB0aGlzLmRpc3BsYXllZENvbHVtbnMucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuXHRcdFx0XHRcdC8vIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlci5wdXNoKGNvbHVtbnMudmFsdWUpO1xyXG5cdFx0XHRcdFx0dGhpcy5kaXNwbGF5ZWRDb2x1bW5zLnB1c2goY29sdW1ucyk7XHJcblx0XHRcdFx0XHR0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIucHVzaChjb2x1bW5zLnZhbHVlKTtcclxuXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGNvbnNvbGUubG9nKHRoaXMuZGlzcGxheWVkQ29sdW1ucyxcIi4uLi50aGlzLmRpc3BsYXllZENvbHVtbnNcIilcclxuXHRcdFx0Y29uc29sZS5sb2coXCI+Pj4+IHNlbGVjdGVkVGFibGVIZWFkZXIgXCIsdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG4gIG9uTG9hZG1hbmFnZUF2cyhpZCkge1xyXG5cdFx0dGhpcy5kZWZhdWx0ZGF0YXNvdXJjZUlkID0gaWRcclxuXHRcdFxyXG5cdH1cclxuXHJcblx0ZXhwb3J0QXZtVXNlcmRldGFpbCgpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdkYXRhIHNvdXJjZSBpZCcsdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmRhdGFzb3VyY2VJZClcclxuICAgICAgICB2YXIgdmFsdWUgPSAxO1xyXG4gICAgICAgIHRoaXMuYXZtRXhwb3J0T2JqPXt9O1xyXG4gICAgICAgIHRoaXMuYXZtRXhwb3J0T2JqWydkYXRhc291cmNlJ10gPSB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5wYXNzVmFsdWUuZGF0YXNvdXJjZUlkO1xyXG4gICAgICAgIHRoaXMuYXZtRXhwb3J0T2JqWydyZXBvcnRfdHlwZSddID0gJ0NTVic7XHJcbiAgICAgICAgdGhpcy5hdm1FeHBvcnRPYmpbJ2NvbGxlY3Rpb25EZXRhaWwnXT0gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmNvbGxlY3Rpb25EZXRhaWw7XHJcbiAgICAgICAgdGhpcy5hdm1FeHBvcnRPYmpbJ2NyZWF0ZWRieSddID0gdGhpcy5fcmVwb3J0TWFuYWdlbWVudFNlcnZpY2UucGFzc1ZhbHVlLmNyZWF0ZWRCeTtcclxuICAgICAgICB0aGlzLmF2bUV4cG9ydE9ialsnam91cm5hbG5hbWUnXSA9IHRoaXMuX3JlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLnBhc3NWYWx1ZS5qb3VybmFsbmFtZTtcclxuICAgICAgICBcclxuICAgICAgICB0aGlzLl9yZXBvcnRNYW5hZ2VtZW50U2VydmljZS5leHBvcnRBVk1VU2VyZGV0YWlscyh0aGlzLmF2bUV4cG9ydE9iaikuc3Vic2NyaWJlKHJlc3AgPT4ge1xyXG5cclxuICAgICAgICAgICAgaWYgKHZhbHVlID09IDEpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGJsb2IgPSBuZXcgQmxvYihbcmVzcC5ib2R5XSwgeyB0eXBlOiAndGV4dC9jc3YnIH0pO1xyXG4gICAgICAgICAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhibG9iLCBcIkFjY2Vzc1Zpb2xhdGlvbkJ5VXNlci5jc3ZcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBibG9iID0gbmV3IEJsb2IoW3Jlc3AuYm9keV0sIHsgdHlwZTogJ3RleHQveGxzeCcgfSk7XHJcbiAgICAgICAgICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIFwiVXNlckNvbmZsaWN0Lnhsc3hcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCBlcnJvciA9PiB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZXJyb3I6OjpcIiArIEpTT04uc3RyaW5naWZ5KGVycm9yKSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuICAgIH1cclxuXHJcblxyXG4gIH1cclxuIl19