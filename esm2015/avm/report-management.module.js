import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
// import { FuseSidebarModule } from '@fuse/components';
// import { FuseSharedModule } from '@fuse/shared.module';
// import { TodoMainSidebarComponent } from 'app/report-management/schedule-reports/sidebar/main/main-sidebar.component';
// import { TodoListComponent } from 'app/report-management/schedule-reports/schedule-list/schedule-list.component';
// import { TodoListItemComponent } from 'app/report-management/schedule-reports/schedule-list/schedule-list-item/schedule-list-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartsModule } from 'ng2-charts';
import { CdkTableModule } from '@angular/cdk/table';
// import { Ng2SmartTableModule } from 'ng2-smart-table';
import { AvmImportFormComponent } from './import-form/import-form.component';
import { ReportHistoryComponent } from './config-tracker/report-management-history/report-management.component';
// import { RunFormComponent } from './schedule-reports/run-form/run-form.component';
// import { ScheduleFormComponent } from './schedule-reports/schedule-form/schedule-form.component';
import { REPORT_MANAGEMENT_ROUTE, SETUP_ADMIN_ROUTE } from './report-management-routing.module';
import { ManageAvComponent } from './manage-av/manage-av.component';
import { ManageAvDetailComponent } from './manage-av-detail/manage-av-detail.component';
import { ManageAvbyuserComponent } from './manage-av-byuser/manage-av-byuser.component';
import { ReportManagementService } from './report-management.service';
import { PieChartlayoutModule } from "../pie-chart/pie-chart.module";
import { FuseWidgetModule } from "./../@fuse/components/widget/widget.module";
import { VerticalBarChartlayoutModule } from "../vertical-bar-chart/vertical-bar-chart.module";
import { ConfigTrackerComponent } from './config-tracker/config-tracker.component';
import { ConfigTrackerSetupComponent } from './config-tracker-setup/config-tracker-setup.component';
import { GridModule } from '@progress/kendo-angular-grid';
// import {
// 	// ReportManagementComponent,
// 	REPORT_MANAGEMENT_ROUTE,
// 	ReportManagementService,
// 	ManageConflictReportComponent,
// 	ExportFormComponent,
// 	ScheduleReportsComponent,
// 	ManageJobComponent,
// 	EditFormComponent,
// 	ManageSchedulingComponent,
// 	ConflictReportFilterComponent,
// 	ManageJobFilterComponent,
// 	ManageSchedulingFilterComponent,
// 	ManageAvComponent,
// 	ManageAvDetailComponent,
// 	ManageAvbyuserComponent,
// 	ConfigTrackerComponent,
// 	DownloadReportsComponent,
// 	DownloadReportsFilterComponent,
// 	ViewReportsComponent
// } from '../';
const routes = [
    {
        path: 'all',
    }
];
export class ReportManagementModule {
}
ReportManagementModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CdkTableModule,
                    RouterModule.forRoot([REPORT_MANAGEMENT_ROUTE, SETUP_ADMIN_ROUTE]),
                    MaterialModule,
                    FormsModule,
                    ReactiveFormsModule,
                    // FuseSidebarModule,
                    // FuseSharedModule,
                    NgxChartsModule,
                    ChartsModule,
                    PieChartlayoutModule,
                    FuseWidgetModule,
                    VerticalBarChartlayoutModule,
                    GridModule
                    // Ng2SmartTableModule
                ],
                exports: [
                    MaterialModule,
                    AvmImportFormComponent,
                    ManageAvComponent,
                    ManageAvDetailComponent,
                    ManageAvbyuserComponent,
                    ConfigTrackerComponent,
                    ConfigTrackerSetupComponent,
                    ReportHistoryComponent
                ],
                declarations: [
                    // ReportManagementComponent,
                    // ManageConflictReportComponent,
                    // ExportFormComponent,
                    // ScheduleReportsComponent,
                    // ManageJobComponent,
                    // EditFormComponent,
                    // ManageSchedulingComponent,
                    // TodoMainSidebarComponent,
                    // TodoListComponent,
                    // TodoListItemComponent,
                    // ConflictReportFilterComponent,
                    // ManageJobFilterComponent,
                    // ManageSchedulingFilterComponent,
                    AvmImportFormComponent,
                    ManageAvComponent,
                    ManageAvDetailComponent,
                    ManageAvbyuserComponent,
                    ConfigTrackerComponent,
                    ReportHistoryComponent,
                    ConfigTrackerSetupComponent
                    // RunFormComponent,
                    // ScheduleFormComponent,
                    // DownloadReportsComponent,
                    // DownloadReportsFilterComponent,
                    // ViewReportsComponent
                ],
                providers: [ReportManagementService],
                schemas: [CUSTOM_ELEMENTS_SCHEMA],
                entryComponents: [
                    // EditFormComponent,
                    // ExportFormComponent,
                    // ConflictReportFilterComponent,
                    // ManageJobFilterComponent,
                    AvmImportFormComponent,
                    // ManageSchedulingFilterComponent,
                    ReportHistoryComponent,
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVwb3J0LW1hbmFnZW1lbnQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImF2bS9yZXBvcnQtbWFuYWdlbWVudC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRSxPQUFPLEVBQUUsWUFBWSxFQUFTLE1BQU0saUJBQWlCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBQ25ELHdEQUF3RDtBQUN4RCwwREFBMEQ7QUFDMUQseUhBQXlIO0FBQ3pILG9IQUFvSDtBQUNwSCxnSkFBZ0o7QUFDaEosT0FBTyxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN2RCxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0sWUFBWSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCx5REFBeUQ7QUFDekQsT0FBTyxFQUFFLHNCQUFzQixFQUFDLE1BQU0scUNBQXFDLENBQUM7QUFDNUUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sd0VBQXdFLENBQUM7QUFDaEgscUZBQXFGO0FBQ3JGLG9HQUFvRztBQUNwRyxPQUFPLEVBQUMsdUJBQXVCLEVBQUUsaUJBQWlCLEVBQUMsTUFBTSxvQ0FBb0MsQ0FBQztBQUM5RixPQUFPLEVBQUMsaUJBQWlCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztBQUNsRSxPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSwrQ0FBK0MsQ0FBQztBQUN0RixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSwrQ0FBK0MsQ0FBQztBQUN0RixPQUFPLEVBQUMsdUJBQXVCLEVBQUMsTUFBTSw2QkFBNkIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUM5RSxPQUFPLEVBQUMsNEJBQTRCLEVBQUMsTUFBTSxpREFBaUQsQ0FBQztBQUM3RixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSwyQ0FBMkMsQ0FBQztBQUNqRixPQUFPLEVBQUMsMkJBQTJCLEVBQUMsTUFBTSx1REFBdUQsQ0FBQztBQUNsRyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDMUQsV0FBVztBQUNYLGlDQUFpQztBQUNqQyw0QkFBNEI7QUFDNUIsNEJBQTRCO0FBQzVCLGtDQUFrQztBQUNsQyx3QkFBd0I7QUFDeEIsNkJBQTZCO0FBQzdCLHVCQUF1QjtBQUN2QixzQkFBc0I7QUFDdEIsOEJBQThCO0FBQzlCLGtDQUFrQztBQUNsQyw2QkFBNkI7QUFDN0Isb0NBQW9DO0FBQ3BDLHNCQUFzQjtBQUN0Qiw0QkFBNEI7QUFDNUIsNEJBQTRCO0FBQzVCLDJCQUEyQjtBQUMzQiw2QkFBNkI7QUFDN0IsbUNBQW1DO0FBQ25DLHdCQUF3QjtBQUN4QixnQkFBZ0I7QUFFaEIsTUFBTSxNQUFNLEdBQVc7SUFDbkI7UUFDSSxJQUFJLEVBQU8sS0FBSztLQUtuQjtDQUVKLENBQUM7QUEyRUYsTUFBTSxPQUFPLHNCQUFzQjs7O1lBMUVsQyxRQUFRLFNBQUM7Z0JBQ1QsT0FBTyxFQUFFO29CQUNSLGNBQWM7b0JBQ2QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxDQUFDLHVCQUF1QixFQUFFLGlCQUFpQixDQUFDLENBQUM7b0JBQ2xFLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLHFCQUFxQjtvQkFDckIsb0JBQW9CO29CQUNwQixlQUFlO29CQUNmLFlBQVk7b0JBQ1osb0JBQW9CO29CQUNwQixnQkFBZ0I7b0JBQ2hCLDRCQUE0QjtvQkFDNUIsVUFBVTtvQkFDVixzQkFBc0I7aUJBSXRCO2dCQUNELE9BQU8sRUFBRTtvQkFDUixjQUFjO29CQUNkLHNCQUFzQjtvQkFDdEIsaUJBQWlCO29CQUNqQix1QkFBdUI7b0JBQ3ZCLHVCQUF1QjtvQkFDdkIsc0JBQXNCO29CQUN0QiwyQkFBMkI7b0JBQzNCLHNCQUFzQjtpQkFDdEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNiLDZCQUE2QjtvQkFDN0IsaUNBQWlDO29CQUNqQyx1QkFBdUI7b0JBQ3ZCLDRCQUE0QjtvQkFDNUIsc0JBQXNCO29CQUN0QixxQkFBcUI7b0JBQ3JCLDZCQUE2QjtvQkFDN0IsNEJBQTRCO29CQUM1QixxQkFBcUI7b0JBQ3JCLHlCQUF5QjtvQkFDekIsaUNBQWlDO29CQUNqQyw0QkFBNEI7b0JBQzVCLG1DQUFtQztvQkFDbkMsc0JBQXNCO29CQUN0QixpQkFBaUI7b0JBQ2pCLHVCQUF1QjtvQkFDdkIsdUJBQXVCO29CQUN2QixzQkFBc0I7b0JBQ3RCLHNCQUFzQjtvQkFDdEIsMkJBQTJCO29CQUMzQixvQkFBb0I7b0JBQ3BCLHlCQUF5QjtvQkFDekIsNEJBQTRCO29CQUM1QixrQ0FBa0M7b0JBQ2xDLHVCQUF1QjtpQkFDdkI7Z0JBQ0QsU0FBUyxFQUFFLENBQUMsdUJBQXVCLENBQUM7Z0JBQ3BDLE9BQU8sRUFBRSxDQUFDLHNCQUFzQixDQUFDO2dCQUNqQyxlQUFlLEVBQUU7b0JBQ2hCLHFCQUFxQjtvQkFDckIsdUJBQXVCO29CQUN2QixpQ0FBaUM7b0JBQ2pDLDRCQUE0QjtvQkFDNUIsc0JBQXNCO29CQUN0QixtQ0FBbUM7b0JBQ25DLHNCQUFzQjtpQkFNbkI7YUFDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBDVVNUT01fRUxFTUVOVFNfU0NIRU1BIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSxSb3V0ZXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZX0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuLy8gaW1wb3J0IHsgRnVzZVNpZGViYXJNb2R1bGUgfSBmcm9tICdAZnVzZS9jb21wb25lbnRzJztcclxuLy8gaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gJ0BmdXNlL3NoYXJlZC5tb2R1bGUnO1xyXG4vLyBpbXBvcnQgeyBUb2RvTWFpblNpZGViYXJDb21wb25lbnQgfSBmcm9tICdhcHAvcmVwb3J0LW1hbmFnZW1lbnQvc2NoZWR1bGUtcmVwb3J0cy9zaWRlYmFyL21haW4vbWFpbi1zaWRlYmFyLmNvbXBvbmVudCc7XHJcbi8vIGltcG9ydCB7IFRvZG9MaXN0Q29tcG9uZW50IH0gZnJvbSAnYXBwL3JlcG9ydC1tYW5hZ2VtZW50L3NjaGVkdWxlLXJlcG9ydHMvc2NoZWR1bGUtbGlzdC9zY2hlZHVsZS1saXN0LmNvbXBvbmVudCc7XHJcbi8vIGltcG9ydCB7IFRvZG9MaXN0SXRlbUNvbXBvbmVudCB9IGZyb20gJ2FwcC9yZXBvcnQtbWFuYWdlbWVudC9zY2hlZHVsZS1yZXBvcnRzL3NjaGVkdWxlLWxpc3Qvc2NoZWR1bGUtbGlzdC1pdGVtL3NjaGVkdWxlLWxpc3QtaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTmd4Q2hhcnRzTW9kdWxlIH0gZnJvbSAnQHN3aW1sYW5lL25neC1jaGFydHMnO1xyXG5pbXBvcnQge0NoYXJ0c01vZHVsZX0gZnJvbSAnbmcyLWNoYXJ0cyc7XHJcbmltcG9ydCB7IENka1RhYmxlTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3RhYmxlJztcclxuLy8gaW1wb3J0IHsgTmcyU21hcnRUYWJsZU1vZHVsZSB9IGZyb20gJ25nMi1zbWFydC10YWJsZSc7XHJcbmltcG9ydCB7IEF2bUltcG9ydEZvcm1Db21wb25lbnR9IGZyb20gJy4vaW1wb3J0LWZvcm0vaW1wb3J0LWZvcm0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgUmVwb3J0SGlzdG9yeUNvbXBvbmVudCB9IGZyb20gJy4vY29uZmlnLXRyYWNrZXIvcmVwb3J0LW1hbmFnZW1lbnQtaGlzdG9yeS9yZXBvcnQtbWFuYWdlbWVudC5jb21wb25lbnQnO1xyXG4vLyBpbXBvcnQgeyBSdW5Gb3JtQ29tcG9uZW50IH0gZnJvbSAnLi9zY2hlZHVsZS1yZXBvcnRzL3J1bi1mb3JtL3J1bi1mb3JtLmNvbXBvbmVudCc7XHJcbi8vIGltcG9ydCB7IFNjaGVkdWxlRm9ybUNvbXBvbmVudCB9IGZyb20gJy4vc2NoZWR1bGUtcmVwb3J0cy9zY2hlZHVsZS1mb3JtL3NjaGVkdWxlLWZvcm0uY29tcG9uZW50JztcclxuaW1wb3J0IHtSRVBPUlRfTUFOQUdFTUVOVF9ST1VURSwgU0VUVVBfQURNSU5fUk9VVEV9IGZyb20gJy4vcmVwb3J0LW1hbmFnZW1lbnQtcm91dGluZy5tb2R1bGUnO1xyXG5pbXBvcnQge01hbmFnZUF2Q29tcG9uZW50fSBmcm9tICcuL21hbmFnZS1hdi9tYW5hZ2UtYXYuY29tcG9uZW50JztcclxuaW1wb3J0IHtNYW5hZ2VBdkRldGFpbENvbXBvbmVudH0gZnJvbSAnLi9tYW5hZ2UtYXYtZGV0YWlsL21hbmFnZS1hdi1kZXRhaWwuY29tcG9uZW50JztcclxuaW1wb3J0IHtNYW5hZ2VBdmJ5dXNlckNvbXBvbmVudH0gZnJvbSAnLi9tYW5hZ2UtYXYtYnl1c2VyL21hbmFnZS1hdi1ieXVzZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHtSZXBvcnRNYW5hZ2VtZW50U2VydmljZX0gZnJvbSAnLi9yZXBvcnQtbWFuYWdlbWVudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUGllQ2hhcnRsYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vcGllLWNoYXJ0L3BpZS1jaGFydC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgRnVzZVdpZGdldE1vZHVsZSB9IGZyb20gXCIuLy4uL0BmdXNlL2NvbXBvbmVudHMvd2lkZ2V0L3dpZGdldC5tb2R1bGVcIjtcclxuaW1wb3J0IHtWZXJ0aWNhbEJhckNoYXJ0bGF5b3V0TW9kdWxlfSBmcm9tIFwiLi4vdmVydGljYWwtYmFyLWNoYXJ0L3ZlcnRpY2FsLWJhci1jaGFydC5tb2R1bGVcIjtcclxuaW1wb3J0IHtDb25maWdUcmFja2VyQ29tcG9uZW50fSBmcm9tICcuL2NvbmZpZy10cmFja2VyL2NvbmZpZy10cmFja2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7Q29uZmlnVHJhY2tlclNldHVwQ29tcG9uZW50fSBmcm9tICcuL2NvbmZpZy10cmFja2VyLXNldHVwL2NvbmZpZy10cmFja2VyLXNldHVwLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEdyaWRNb2R1bGUgfSBmcm9tICdAcHJvZ3Jlc3Mva2VuZG8tYW5ndWxhci1ncmlkJztcclxuLy8gaW1wb3J0IHtcclxuLy8gXHQvLyBSZXBvcnRNYW5hZ2VtZW50Q29tcG9uZW50LFxyXG4vLyBcdFJFUE9SVF9NQU5BR0VNRU5UX1JPVVRFLFxyXG4vLyBcdFJlcG9ydE1hbmFnZW1lbnRTZXJ2aWNlLFxyXG4vLyBcdE1hbmFnZUNvbmZsaWN0UmVwb3J0Q29tcG9uZW50LFxyXG4vLyBcdEV4cG9ydEZvcm1Db21wb25lbnQsXHJcbi8vIFx0U2NoZWR1bGVSZXBvcnRzQ29tcG9uZW50LFxyXG4vLyBcdE1hbmFnZUpvYkNvbXBvbmVudCxcclxuLy8gXHRFZGl0Rm9ybUNvbXBvbmVudCxcclxuLy8gXHRNYW5hZ2VTY2hlZHVsaW5nQ29tcG9uZW50LFxyXG4vLyBcdENvbmZsaWN0UmVwb3J0RmlsdGVyQ29tcG9uZW50LFxyXG4vLyBcdE1hbmFnZUpvYkZpbHRlckNvbXBvbmVudCxcclxuLy8gXHRNYW5hZ2VTY2hlZHVsaW5nRmlsdGVyQ29tcG9uZW50LFxyXG4vLyBcdE1hbmFnZUF2Q29tcG9uZW50LFxyXG4vLyBcdE1hbmFnZUF2RGV0YWlsQ29tcG9uZW50LFxyXG4vLyBcdE1hbmFnZUF2Ynl1c2VyQ29tcG9uZW50LFxyXG4vLyBcdENvbmZpZ1RyYWNrZXJDb21wb25lbnQsXHJcbi8vIFx0RG93bmxvYWRSZXBvcnRzQ29tcG9uZW50LFxyXG4vLyBcdERvd25sb2FkUmVwb3J0c0ZpbHRlckNvbXBvbmVudCxcclxuLy8gXHRWaWV3UmVwb3J0c0NvbXBvbmVudFxyXG4vLyB9IGZyb20gJy4uLyc7XHJcblxyXG5jb25zdCByb3V0ZXM6IFJvdXRlcyA9IFtcclxuICAgIHtcclxuICAgICAgICBwYXRoICAgICA6ICdhbGwnLFxyXG4gICAgICAgIC8vIGNvbXBvbmVudDogU2NoZWR1bGVSZXBvcnRzQ29tcG9uZW50LFxyXG4gICAgICAgIC8vIHJlc29sdmUgIDoge1xyXG4gICAgICAgIC8vICAgICB0b2RvOiBUb2RvU2VydmljZVxyXG4gICAgICAgIC8vIH1cclxuICAgIH1cclxuICAgXHJcbl07XHJcbkBOZ01vZHVsZSh7XHJcblx0aW1wb3J0czogW1xyXG5cdFx0Q2RrVGFibGVNb2R1bGUsXHJcblx0XHRSb3V0ZXJNb2R1bGUuZm9yUm9vdChbUkVQT1JUX01BTkFHRU1FTlRfUk9VVEUsIFNFVFVQX0FETUlOX1JPVVRFXSksXHJcblx0XHRNYXRlcmlhbE1vZHVsZSxcclxuXHRcdEZvcm1zTW9kdWxlLFxyXG5cdFx0UmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuXHRcdC8vIEZ1c2VTaWRlYmFyTW9kdWxlLFxyXG5cdFx0Ly8gRnVzZVNoYXJlZE1vZHVsZSxcclxuXHRcdE5neENoYXJ0c01vZHVsZSxcclxuXHRcdENoYXJ0c01vZHVsZSxcclxuXHRcdFBpZUNoYXJ0bGF5b3V0TW9kdWxlLFxyXG5cdFx0RnVzZVdpZGdldE1vZHVsZSxcclxuXHRcdFZlcnRpY2FsQmFyQ2hhcnRsYXlvdXRNb2R1bGUsXHJcblx0XHRHcmlkTW9kdWxlXHJcblx0XHQvLyBOZzJTbWFydFRhYmxlTW9kdWxlXHJcblxyXG5cdFx0XHJcblx0XHRcclxuXHRdLFxyXG5cdGV4cG9ydHM6IFtcclxuXHRcdE1hdGVyaWFsTW9kdWxlLFxyXG5cdFx0QXZtSW1wb3J0Rm9ybUNvbXBvbmVudCxcclxuXHRcdE1hbmFnZUF2Q29tcG9uZW50LFxyXG5cdFx0TWFuYWdlQXZEZXRhaWxDb21wb25lbnQsXHJcblx0XHRNYW5hZ2VBdmJ5dXNlckNvbXBvbmVudCxcclxuXHRcdENvbmZpZ1RyYWNrZXJDb21wb25lbnQsXHJcblx0XHRDb25maWdUcmFja2VyU2V0dXBDb21wb25lbnQsXHJcblx0XHRSZXBvcnRIaXN0b3J5Q29tcG9uZW50XHJcblx0XSxcclxuXHRkZWNsYXJhdGlvbnM6IFtcclxuXHRcdC8vIFJlcG9ydE1hbmFnZW1lbnRDb21wb25lbnQsXHJcblx0XHQvLyBNYW5hZ2VDb25mbGljdFJlcG9ydENvbXBvbmVudCxcclxuXHRcdC8vIEV4cG9ydEZvcm1Db21wb25lbnQsXHJcblx0XHQvLyBTY2hlZHVsZVJlcG9ydHNDb21wb25lbnQsXHJcblx0XHQvLyBNYW5hZ2VKb2JDb21wb25lbnQsXHJcblx0XHQvLyBFZGl0Rm9ybUNvbXBvbmVudCxcclxuXHRcdC8vIE1hbmFnZVNjaGVkdWxpbmdDb21wb25lbnQsXHJcblx0XHQvLyBUb2RvTWFpblNpZGViYXJDb21wb25lbnQsXHJcblx0XHQvLyBUb2RvTGlzdENvbXBvbmVudCxcclxuXHRcdC8vIFRvZG9MaXN0SXRlbUNvbXBvbmVudCxcclxuXHRcdC8vIENvbmZsaWN0UmVwb3J0RmlsdGVyQ29tcG9uZW50LFxyXG5cdFx0Ly8gTWFuYWdlSm9iRmlsdGVyQ29tcG9uZW50LFxyXG5cdFx0Ly8gTWFuYWdlU2NoZWR1bGluZ0ZpbHRlckNvbXBvbmVudCxcclxuXHRcdEF2bUltcG9ydEZvcm1Db21wb25lbnQsXHJcblx0XHRNYW5hZ2VBdkNvbXBvbmVudCxcclxuXHRcdE1hbmFnZUF2RGV0YWlsQ29tcG9uZW50LFxyXG5cdFx0TWFuYWdlQXZieXVzZXJDb21wb25lbnQsXHJcblx0XHRDb25maWdUcmFja2VyQ29tcG9uZW50LFxyXG5cdFx0UmVwb3J0SGlzdG9yeUNvbXBvbmVudCxcclxuXHRcdENvbmZpZ1RyYWNrZXJTZXR1cENvbXBvbmVudFxyXG5cdFx0Ly8gUnVuRm9ybUNvbXBvbmVudCxcclxuXHRcdC8vIFNjaGVkdWxlRm9ybUNvbXBvbmVudCxcclxuXHRcdC8vIERvd25sb2FkUmVwb3J0c0NvbXBvbmVudCxcclxuXHRcdC8vIERvd25sb2FkUmVwb3J0c0ZpbHRlckNvbXBvbmVudCxcclxuXHRcdC8vIFZpZXdSZXBvcnRzQ29tcG9uZW50XHJcblx0XSxcclxuXHRwcm92aWRlcnM6IFtSZXBvcnRNYW5hZ2VtZW50U2VydmljZV0sXHJcblx0c2NoZW1hczogW0NVU1RPTV9FTEVNRU5UU19TQ0hFTUFdLFxyXG5cdGVudHJ5Q29tcG9uZW50czogW1xyXG5cdFx0Ly8gRWRpdEZvcm1Db21wb25lbnQsXHJcblx0XHQvLyBFeHBvcnRGb3JtQ29tcG9uZW50LFxyXG5cdFx0Ly8gQ29uZmxpY3RSZXBvcnRGaWx0ZXJDb21wb25lbnQsXHJcblx0XHQvLyBNYW5hZ2VKb2JGaWx0ZXJDb21wb25lbnQsXHJcblx0XHRBdm1JbXBvcnRGb3JtQ29tcG9uZW50LFxyXG5cdFx0Ly8gTWFuYWdlU2NoZWR1bGluZ0ZpbHRlckNvbXBvbmVudCxcclxuXHRcdFJlcG9ydEhpc3RvcnlDb21wb25lbnQsXHJcblx0XHQvLyBSdW5Gb3JtQ29tcG9uZW50LFxyXG5cdFx0Ly8gRG93bmxvYWRSZXBvcnRzQ29tcG9uZW50LFxyXG5cdFx0Ly8gRG93bmxvYWRSZXBvcnRzRmlsdGVyQ29tcG9uZW50LFxyXG5cdFx0Ly8gVmlld1JlcG9ydHNDb21wb25lbnQsXHJcblx0XHQvLyBTY2hlZHVsZUZvcm1Db21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFJlcG9ydE1hbmFnZW1lbnRNb2R1bGUgeyB9XHJcbiJdfQ==