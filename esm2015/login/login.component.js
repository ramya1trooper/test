import { Component, ViewEncapsulation } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { FuseConfigService } from "../@fuse/services/config.service";
import { LoginService } from "./login.service";
import { OAuthService } from "angular-oauth2-oidc";
export class LoginComponent {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(_fuseConfigService, _formBuilder, loginService, router, oauthService) {
        this._fuseConfigService = _fuseConfigService;
        this._formBuilder = _formBuilder;
        this.loginService = loginService;
        this.router = router;
        this.oauthService = oauthService;
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        if (!localStorage.getItem("access_token")) {
            this.oauthService.initImplicitFlow();
        }
        else {
            this.router.navigate([""]);
        }
        this.loginForm = this._formBuilder.group({
            username: ["", Validators.required],
            password: ["", Validators.required]
        });
    }
    get f() {
        return this.loginForm.controls;
    }
    login() {
        var dataObj = {
            username: this.loginForm.controls["username"].value,
            password: this.loginForm.controls["password"].value
        };
        this.loginService.loginAuth(dataObj).subscribe(data => {
            if (this.router.url === "/login") {
                this.router.navigate([""]);
            }
        }, error => {
            this.errorStatus = error.error.meta.status;
            this.errorMsg = error.error.meta.msg;
        });
    }
}
LoginComponent.decorators = [
    { type: Component, args: [{
                selector: "login",
                template: "<div id=\"login\" fxLayout=\"column\">\r\n\r\n\t<div id=\"login-form-wrapper\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n\r\n\t\t<div id=\"login-form\" [@animate]=\"{value:'*',params:{duration:'300ms',y:'100px'}}\">\r\n\r\n\t\t\t<div class=\"logo\">\r\n\t\t\t\t<img src=\"assets/images/logos/2.png\">\r\n\t\t\t</div>\r\n\r\n\t\t\t<div class=\"title\">LOGIN TO YOUR ACCOUNT</div>\r\n\r\n\t\t\t<form name=\"loginForm\" [formGroup]=\"loginForm\" novalidate>\r\n\r\n\t\t\t\t<mat-form-field appearance=\"outline\">\r\n\t\t\t\t\t<mat-label>Username</mat-label>\r\n\t\t\t\t\t<input matInput formControlName=\"username\" style=\"font-size: 16px;\">\r\n\t\t\t\t\t<mat-icon matSuffix class=\"secondary-text\">person</mat-icon>\r\n\t\t\t\t\t<mat-error>\r\n\t\t\t\t\t\tUsername is required\r\n\t\t\t\t\t</mat-error>\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t<mat-form-field appearance=\"outline\">\r\n\t\t\t\t\t<mat-label>Password</mat-label>\r\n\t\t\t\t\t<input matInput type=\"password\" formControlName=\"password\" style=\"font-size: 16px;\">\r\n\t\t\t\t\t<mat-icon matSuffix class=\"secondary-text\">vpn_key</mat-icon>\r\n\t\t\t\t\t<mat-error>\r\n\t\t\t\t\t\tPassword is requiredaa\r\n\t\t\t\t\t</mat-error>\r\n\t\t\t\t</mat-form-field>\r\n\r\n\t\t\t\t<!-- <div class=\"remember-forgot-password\" fxLayout=\"row\" fxLayout.xs=\"column\" fxLayoutAlign=\"space-between center\">\r\n\t\t\t\t\t<mat-checkbox class=\"remember-me\" aria-label=\"Remember Me\">\r\n\t\t\t\t\t\tRemember Me\r\n\t\t\t\t\t</mat-checkbox>\r\n\r\n\t\t\t\t\t<a class=\"forgot-password\" [routerLink]=\"'/pages/auth/forgot-password'\">\r\n\t\t\t\t\t\tForgot Password?\r\n\t\t\t\t\t</a>\r\n\t\t\t\t</div> -->\r\n\r\n\t\t\t\t<div class=\"message-box error\" *ngIf=\"errorStatus==401\">\r\n\t\t\t\t\t<span >{{this.errorMsg}}</span>\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\t<button mat-raised-button color=\"accent\" class=\"submit-button\" (click)=login(); aria-label=\"LOG IN\" [disabled]=\"loginForm.invalid\">\r\n\t\t\t\t\tLOGIN\r\n\t\t\t\t</button>\r\n\r\n\r\n\t\t\t</form>\r\n\r\n\t\t\t<!-- <div class=\"separator\">\r\n                <span class=\"text\">OR</span>\r\n            </div>\r\n\r\n            <button mat-raised-button class=\"google\">\r\n                Log in with Google\r\n            </button>\r\n\r\n            <button mat-raised-button class=\"facebook\">\r\n                Log in with Facebook\r\n            </button>\r\n\r\n            <div class=\"register\" fxLayout=\"column\" fxLayoutAlign=\"center center\">\r\n                <span class=\"text\">Don't have an account?</span>\r\n                <a class=\"link\" [routerLink]=\"'/pages/auth/register'\">Create an account</a>\r\n            </div> -->\r\n\r\n\t\t</div>\r\n\r\n\t</div>\r\n\r\n</div>",
                encapsulation: ViewEncapsulation.None
                // animations: fuseAnimations
                ,
                styles: ["login #login{width:100%;background:url(/assets/images/backgrounds/dark-material-bg.jpg) 0 0/cover no-repeat}login #login #login-form-wrapper{-webkit-box-flex:1;flex:1 0 auto;padding:32px}login #login #login-form-wrapper #login-form{width:384px;max-width:384px;padding:32px;text-align:center;background-color:#fff;box-shadow:0 8px 10px -5px rgba(0,0,0,.2),0 16px 24px 2px rgba(0,0,0,.14),0 6px 30px 5px rgba(0,0,0,.12)}login #login #login-form-wrapper #login-form .logo{width:128px;margin:32px auto}login #login #login-form-wrapper #login-form .title{font-size:20px;margin:16px 0 32px}login #login #login-form-wrapper #login-form form{width:100%;text-align:left}login #login #login-form-wrapper #login-form form mat-form-field{width:100%}login #login #login-form-wrapper #login-form form mat-checkbox{margin:0}login #login #login-form-wrapper #login-form form .remember-forgot-password{font-size:13px;margin-top:8px}login #login #login-form-wrapper #login-form form .remember-forgot-password .remember-me{margin-bottom:16px}login #login #login-form-wrapper #login-form form .remember-forgot-password .forgot-password{font-size:13px;font-weight:600;margin-bottom:16px}login #login #login-form-wrapper #login-form form .submit-button{width:220px;margin:16px auto;display:block}@media screen and (max-width:599px){login #login #login-form-wrapper{padding:16px}login #login #login-form-wrapper #login-form{padding:24px;width:100%}login #login #login-form-wrapper #login-form form .submit-button{width:90%}login #login #login-form-wrapper #login-form button{width:80%}}login #login #login-form-wrapper #login-form .register{margin:32px auto 24px;font-weight:600}login #login #login-form-wrapper #login-form .register .text{margin-right:8px}login #login #login-form-wrapper #login-form .separator{font-size:15px;font-weight:600;margin:24px auto;position:relative;overflow:hidden;width:100px}login #login #login-form-wrapper #login-form .separator .text{display:-webkit-inline-box;display:inline-flex;position:relative;padding:0 8px;z-index:9999}login #login #login-form-wrapper #login-form .separator .text:after,login #login #login-form-wrapper #login-form .separator .text:before{content:'';display:block;width:30px;position:absolute;top:10px;border-top:1px solid}login #login #login-form-wrapper #login-form .separator .text:before{right:100%}login #login #login-form-wrapper #login-form .separator .text:after{left:100%}login #login #login-form-wrapper #login-form button.facebook,login #login #login-form-wrapper #login-form button.google{width:192px;text-transform:none;color:#fff;font-size:13px}login #login #login-form-wrapper #login-form button.google{background-color:#d73d32;margin-bottom:8px}login #login #login-form-wrapper #login-form button.facebook{background-color:#3f5c9a}"]
            }] }
];
/** @nocollapse */
LoginComponent.ctorParameters = () => [
    { type: FuseConfigService },
    { type: FormBuilder },
    { type: LoginService },
    { type: Router },
    { type: OAuthService }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImxvZ2luL2xvZ2luLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQWEsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDcEUsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXpDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQ0wsWUFBWSxFQUdiLE1BQU0scUJBQXFCLENBQUM7QUFTN0IsTUFBTSxPQUFPLGNBQWM7SUFTekI7Ozs7O09BS0c7SUFDSCxZQUNVLGtCQUFxQyxFQUNyQyxZQUF5QixFQUN6QixZQUEwQixFQUMxQixNQUFjLEVBQ2QsWUFBMEI7UUFKMUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQyxpQkFBWSxHQUFaLFlBQVksQ0FBYTtRQUN6QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsaUJBQVksR0FBWixZQUFZLENBQWM7UUFFbEMsdUJBQXVCO1FBQ3ZCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEdBQUc7WUFDL0IsTUFBTSxFQUFFO2dCQUNOLE1BQU0sRUFBRTtvQkFDTixNQUFNLEVBQUUsSUFBSTtpQkFDYjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AsTUFBTSxFQUFFLElBQUk7aUJBQ2I7Z0JBQ0QsTUFBTSxFQUFFO29CQUNOLE1BQU0sRUFBRSxJQUFJO2lCQUNiO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxNQUFNLEVBQUUsSUFBSTtpQkFDYjthQUNGO1NBQ0YsQ0FBQztJQUNKLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7T0FFRztJQUNILFFBQVE7UUFDTixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRTtZQUN6QyxJQUFJLENBQUMsWUFBWSxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDdEM7YUFBTTtZQUNMLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7WUFDdkMsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDbkMsUUFBUSxFQUFFLENBQUMsRUFBRSxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7U0FDcEMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELElBQUksQ0FBQztRQUNILE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7SUFDakMsQ0FBQztJQUVELEtBQUs7UUFDSCxJQUFJLE9BQU8sR0FBRztZQUNaLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLO1lBQ25ELFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLO1NBQ3BELENBQUM7UUFDRixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxTQUFTLENBQzVDLElBQUksQ0FBQyxFQUFFO1lBQ0wsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsS0FBSyxRQUFRLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM1QjtRQUNILENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtZQUNOLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBQzNDLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDO1FBQ3ZDLENBQUMsQ0FDRixDQUFDO0lBQ0osQ0FBQzs7O1lBeEZGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsT0FBTztnQkFDakIsOHNGQUFxQztnQkFFckMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLDZCQUE2Qjs7O2FBQzlCOzs7O1lBZFEsaUJBQWlCO1lBSGpCLFdBQVc7WUFJWCxZQUFZO1lBSFosTUFBTTtZQUtiLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgRm9ybUdyb3VwLCBWYWxpZGF0b3JzIH0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VDb25maWdTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IExvZ2luU2VydmljZSB9IGZyb20gXCIuL2xvZ2luLnNlcnZpY2VcIjtcclxuaW1wb3J0IHtcclxuICBPQXV0aFNlcnZpY2UsXHJcbiAgQXV0aENvbmZpZyxcclxuICBOdWxsVmFsaWRhdGlvbkhhbmRsZXJcclxufSBmcm9tIFwiYW5ndWxhci1vYXV0aDItb2lkY1wiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwibG9naW5cIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL2xvZ2luLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2xvZ2luLmNvbXBvbmVudC5zY3NzXCJdLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxuICAvLyBhbmltYXRpb25zOiBmdXNlQW5pbWF0aW9uc1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIGxvZ2luRm9ybTogRm9ybUdyb3VwO1xyXG5cclxuICBhdXRoZW50aWNhdGlvbkVycm9yOiBib29sZWFuO1xyXG4gIHVzZXJuYW1lOiBzdHJpbmc7XHJcbiAgcGFzc3dvcmQ6IHN0cmluZztcclxuICBlcnJvclN0YXR1czogYW55O1xyXG4gIGVycm9yTXNnOiBhbnk7XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge0Z1c2VDb25maWdTZXJ2aWNlfSBfZnVzZUNvbmZpZ1NlcnZpY2VcclxuICAgKiBAcGFyYW0ge0Zvcm1CdWlsZGVyfSBfZm9ybUJ1aWxkZXJcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VDb25maWdTZXJ2aWNlOiBGdXNlQ29uZmlnU2VydmljZSxcclxuICAgIHByaXZhdGUgX2Zvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgIHByaXZhdGUgbG9naW5TZXJ2aWNlOiBMb2dpblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgcHJpdmF0ZSBvYXV0aFNlcnZpY2U6IE9BdXRoU2VydmljZVxyXG4gICkge1xyXG4gICAgLy8gQ29uZmlndXJlIHRoZSBsYXlvdXRcclxuICAgIHRoaXMuX2Z1c2VDb25maWdTZXJ2aWNlLmNvbmZpZyA9IHtcclxuICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgbmF2YmFyOiB7XHJcbiAgICAgICAgICBoaWRkZW46IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHRvb2xiYXI6IHtcclxuICAgICAgICAgIGhpZGRlbjogdHJ1ZVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZm9vdGVyOiB7XHJcbiAgICAgICAgICBoaWRkZW46IHRydWVcclxuICAgICAgICB9LFxyXG4gICAgICAgIHNpZGVwYW5lbDoge1xyXG4gICAgICAgICAgaGlkZGVuOiB0cnVlXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIE9uIGluaXRcclxuICAgKi9cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIGlmICghbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJhY2Nlc3NfdG9rZW5cIikpIHtcclxuICAgICAgdGhpcy5vYXV0aFNlcnZpY2UuaW5pdEltcGxpY2l0RmxvdygpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW1wiXCJdKTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmxvZ2luRm9ybSA9IHRoaXMuX2Zvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgdXNlcm5hbWU6IFtcIlwiLCBWYWxpZGF0b3JzLnJlcXVpcmVkXSxcclxuICAgICAgcGFzc3dvcmQ6IFtcIlwiLCBWYWxpZGF0b3JzLnJlcXVpcmVkXVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXQgZigpIHtcclxuICAgIHJldHVybiB0aGlzLmxvZ2luRm9ybS5jb250cm9scztcclxuICB9XHJcblxyXG4gIGxvZ2luKCkge1xyXG4gICAgdmFyIGRhdGFPYmogPSB7XHJcbiAgICAgIHVzZXJuYW1lOiB0aGlzLmxvZ2luRm9ybS5jb250cm9sc1tcInVzZXJuYW1lXCJdLnZhbHVlLFxyXG4gICAgICBwYXNzd29yZDogdGhpcy5sb2dpbkZvcm0uY29udHJvbHNbXCJwYXNzd29yZFwiXS52YWx1ZVxyXG4gICAgfTtcclxuICAgIHRoaXMubG9naW5TZXJ2aWNlLmxvZ2luQXV0aChkYXRhT2JqKS5zdWJzY3JpYmUoXHJcbiAgICAgIGRhdGEgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnJvdXRlci51cmwgPT09IFwiL2xvZ2luXCIpIHtcclxuICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFtcIlwiXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBlcnJvciA9PiB7XHJcbiAgICAgICAgdGhpcy5lcnJvclN0YXR1cyA9IGVycm9yLmVycm9yLm1ldGEuc3RhdHVzO1xyXG4gICAgICAgIHRoaXMuZXJyb3JNc2cgPSBlcnJvci5lcnJvci5tZXRhLm1zZztcclxuICAgICAgfVxyXG4gICAgKTtcclxuICB9XHJcbn1cclxuIl19