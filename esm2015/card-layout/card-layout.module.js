import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { CardLayoutComponent } from "./card-layout.component";
import { FuseSharedModule } from "../@fuse/shared.module";
import { FuseProgressBarModule } from '../@fuse/components/progress-bar/progress-bar.module';
import { FuseThemeOptionsModule } from '../@fuse/components/theme-options/theme-options.module';
import { FuseWidgetModule } from "../@fuse/components/widget/widget.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
export class CardLayoutModule {
}
CardLayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [CardLayoutComponent],
                imports: [
                    RouterModule,
                    MaterialModule,
                    FuseWidgetModule,
                    FuseSharedModule,
                    TranslateModule,
                    FuseProgressBarModule,
                    FuseThemeOptionsModule
                ],
                exports: [CardLayoutComponent],
                providers: []
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1sYXlvdXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNhcmQtbGF5b3V0L2NhcmQtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBSyxzREFBc0QsQ0FBQztBQUMxRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSx3REFBd0QsQ0FBQTtBQUM3RixPQUFPLEVBQUcsZ0JBQWdCLEVBQUMsTUFBTSwwQ0FBMEMsQ0FBQztBQUM1RSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBZXBELE1BQU0sT0FBTyxnQkFBZ0I7OztZQWQ1QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsbUJBQW1CLENBQUM7Z0JBQ25DLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGNBQWM7b0JBQ2QsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixzQkFBc0I7aUJBQ3ZCO2dCQUNELE9BQU8sRUFBRSxDQUFDLG1CQUFtQixDQUFDO2dCQUM5QixTQUFTLEVBQUcsRUFBRTthQUNmIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBDYXJkTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4vY2FyZC1sYXlvdXQuY29tcG9uZW50XCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHtGdXNlUHJvZ3Jlc3NCYXJNb2R1bGV9ZnJvbSAnLi4vQGZ1c2UvY29tcG9uZW50cy9wcm9ncmVzcy1iYXIvcHJvZ3Jlc3MtYmFyLm1vZHVsZSc7XHJcbmltcG9ydCB7RnVzZVRoZW1lT3B0aW9uc01vZHVsZX0gZnJvbSAnLi4vQGZ1c2UvY29tcG9uZW50cy90aGVtZS1vcHRpb25zL3RoZW1lLW9wdGlvbnMubW9kdWxlJ1xyXG5pbXBvcnQgeyAgRnVzZVdpZGdldE1vZHVsZX0gZnJvbSBcIi4uL0BmdXNlL2NvbXBvbmVudHMvd2lkZ2V0L3dpZGdldC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbQ2FyZExheW91dENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgUm91dGVyTW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBGdXNlV2lkZ2V0TW9kdWxlLFxyXG4gICAgRnVzZVNoYXJlZE1vZHVsZSxcclxuICAgIFRyYW5zbGF0ZU1vZHVsZSxcclxuICAgIEZ1c2VQcm9ncmVzc0Jhck1vZHVsZSxcclxuICAgIEZ1c2VUaGVtZU9wdGlvbnNNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtDYXJkTGF5b3V0Q29tcG9uZW50XSxcclxuICBwcm92aWRlcnMgOiBbXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZExheW91dE1vZHVsZSB7XHJcblxyXG59XHJcbiJdfQ==