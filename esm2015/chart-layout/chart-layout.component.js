import { MessageService } from "./../_services/message.service";
import { Component } from "@angular/core";
import { ContentService } from "../content/content.service";
import * as _ from "lodash";
import { LoaderService } from '../loader.service';
export class ChartLayoutComponent {
    constructor(contentService, messageService, loaderService) {
        this.contentService = contentService;
        this.messageService = messageService;
        this.loaderService = loaderService;
        this.enableChart = false;
        this.enablePieChart = false;
        this.enablePieGrid = false;
        this.chartData = [];
        this.pieChartData = [];
        this.pieGridData = [];
        this.inputData = {};
        this.messageService.getMessage().subscribe(message => {
            this.ngOnInit();
        });
    }
    ngOnInit() {
        this.defaultDatasource = localStorage.getItem("datasource");
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.queryParams = {
            datasource: this.defaultDatasource
        };
        this.onLoad();
    }
    onLoad() {
        let currentData = this.currentConfigData["listView"].chartData;
        this.inputData["datasourceId"] = this.defaultDatasource;
        if (currentData) {
            this.colors = currentData.colors;
            this.view = currentData.View;
            var self = this;
            this.pieGridData = [];
            this.pieChartData = [];
            this.chartData = [];
            if (currentData.onLoadFunction && currentData.onLoadFunction.requestData) {
                _.forEach(currentData.onLoadFunction.requestData, function (requestItem) {
                    if (requestItem.isDefault) {
                        self.queryParams[requestItem.name] = requestItem.value;
                    }
                    else {
                        self.queryParams[requestItem.name] = self.inputData[requestItem.value];
                    }
                });
            }
            if (currentData.singleRequest) {
                let requestDetails = currentData.onLoadFunction;
                let apiUrl = requestDetails.apiUrl;
                this.contentService
                    .getAllReponse(this.queryParams, apiUrl)
                    .subscribe(resp => {
                    let response = resp.response[currentData.onLoadFunction.response];
                    let keyByResponse = _.keyBy(response, "countsourceName");
                    var self = this;
                    _.forEach(currentData.chartRequest, function (item) {
                        let data = (keyByResponse[item.chartId] && keyByResponse[item.chartId].countDetails) ? keyByResponse[item.chartId].countDetails : [];
                        let total = _.sumBy(data, "count");
                        self.constructData(data, total, item);
                    });
                });
            }
            else {
                _.forEach(currentData.chartRequest, function (item) {
                    let requestDetails = item.onLoadFunction;
                    let apiUrl = requestDetails.apiUrl;
                    let responseName = requestDetails.response;
                    // if (item.View) {
                    //   this.view = item["View"];
                    // }
                    self.contentService
                        .getAllReponse(self.queryParams, apiUrl)
                        .subscribe(resp => {
                        let data = resp.response[responseName];
                        let total = _.sumBy(data, "count");
                        self.constructData(data, total, item);
                    });
                });
            }
        }
    }
    constructData(data, total, chartResp) {
        console.log(chartResp, "...chartResp");
        console.log(data, "...data");
        if (chartResp.enableChart) {
            this.enableChart = true;
            this.chartView = chartResp.View
                ? chartResp.View
                : this.currentConfigData["listView"].chartData.View;
            this.chartLabel = {
                title: chartResp.label,
                nodata: chartResp.nodata
            };
        }
        if (chartResp.enablePieChart) {
            this.enablePieChart = true;
            this.pieChartView = chartResp.View ? chartResp.View : this.view;
            this.pieChartLabel = {
                title: chartResp.label,
                nodata: chartResp.nodata
            };
        }
        if (chartResp.enablePieGrid) {
            this.enablePieGrid = true;
            this.pieGridView = chartResp.View ? chartResp.View : this.view;
            this.pieGridLabel = {
                title: chartResp.label,
                nodata: chartResp.nodata
            };
        }
        var self = this;
        _.forEach(data, function (item) {
            let tempObj = {
                name: item.name,
                value: item.count
            };
            if (!chartResp.enablePieChart) {
                tempObj["percentage"] = Math.floor((item.count / total) * 100);
            }
            if (chartResp.enableChart) {
                self.chartData.push(tempObj);
            }
            if (chartResp.enablePieChart) {
                self.pieChartData.push(tempObj);
            }
            if (chartResp.enablePieGrid) {
                self.pieGridData.push(tempObj);
            }
        });
    }
}
ChartLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "chart-layout",
                template: "<div class=\"senlib-padding-10 senlib-m\">\r\n  <div class=\"page-layout \" fxLayoutAlign=\" stretch\" fxLayout=\"row wrap\" fxFlex=\"100\" *fuseIfOnDom>\r\n    <fuse-widget fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"33\" class=\"widget charts-view\" *ngIf=\"enableChart\">\r\n\r\n      <horizontal-chart-layout [result]=\"chartData\" [label]=\"chartLabel\" [color]=\"colors\" [view]=\"chartView\"\r\n        class=\"fusewidgetClass\">\r\n      </horizontal-chart-layout>\r\n\r\n    </fuse-widget>\r\n    <fuse-widget fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"33\" class=\"widget charts-view\" *ngIf=\"enablePieChart\">\r\n\r\n\r\n      <pie-chart [result]=\"pieChartData\" [label]=\"pieChartLabel\" [scheme]=\"colors\" [view]=\"pieChartView\"\r\n        class=\"fusewidgetClass\">\r\n      </pie-chart>\r\n\r\n    </fuse-widget>\r\n    <fuse-widget fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"33\" class=\"widget charts-view\" *ngIf=\"enablePieGrid\">\r\n\r\n\r\n      <pie-grid [result]=\"pieGridData\" [label]=\"pieGridLabel\" [scheme]=\"colors\" [view]=\"pieGridView\"\r\n        class=\"fusewidgetClass\">\r\n      </pie-grid>\r\n\r\n\r\n    </fuse-widget>\r\n\r\n    <!-- <div class=\"charts-view\">\r\n      <ng-container *ngIf=\"enablePieChart\">\r\n        <pie-chart [result]=\"pieChartData\" [label]=\"pieChartLabel\" [scheme]=\"colors\" [view]=\"view\">\r\n        </pie-chart>\r\n      </ng-container>\r\n    </div>\r\n    <div class=\"charts-view\">\r\n      <ng-container *ngIf=\"enablePieGrid\">\r\n        <pie-grid [result]=\"pieGridData\" [label]=\"pieGridLabel\" [scheme]=\"colors\" [view]=\"view\">\r\n        </pie-grid>\r\n      </ng-container>\r\n    </div> -->\r\n  </div>\r\n  <!-- \r\n    <div class=\"page-layout \" fxLayoutAlign=\" stretch\" fxLayout=\"row wrap\" fxFlex=\"100\" *fuseIfOnDom\r\n    [@animateStagger]=\"{value:'50'}\">\r\n    Annimate for widget [@animate]=\"{value:'*',params:{y:'100%'}}\" -->\r\n\r\n</div>\r\n",
                styles: ["app-chart-layout,app-pie-chart,app-pie-grid{height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.charts-view{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;flex-basis:100%;-webkit-box-flex:1;flex:1;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.page-layout{width:auto!important;position:relative;display:-webkit-box;display:flex;z-index:1;flex-wrap:wrap}.page-layout>:not(router-outlet){display:-webkit-box;display:flex;-webkit-box-flex:1!important;flex:auto!important}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.senlib-padding-10{padding:10px;padding-top:0!important}.senlib-m{margin-left:16px;margin-right:6px!important;margin-top:0;margin-bottom:12px}"]
            }] }
];
/** @nocollapse */
ChartLayoutComponent.ctorParameters = () => [
    { type: ContentService },
    { type: MessageService },
    { type: LoaderService }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhcnQtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJjaGFydC1sYXlvdXQvY2hhcnQtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFNBQVMsRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRTVELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBQzVCLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQU1sRCxNQUFNLE9BQU8sb0JBQW9CO0lBc0IvQixZQUNVLGNBQThCLEVBQzlCLGNBQThCLEVBQzlCLGFBQTRCO1FBRjVCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsa0JBQWEsR0FBYixhQUFhLENBQWU7UUF4QnRDLGdCQUFXLEdBQVksS0FBSyxDQUFDO1FBQzdCLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLGtCQUFhLEdBQVksS0FBSyxDQUFDO1FBWS9CLGNBQVMsR0FBUSxFQUFFLENBQUM7UUFDcEIsaUJBQVksR0FBUSxFQUFFLENBQUM7UUFDdkIsZ0JBQVcsR0FBUSxFQUFFLENBQUM7UUFHdEIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQU9sQixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNuRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDbEIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsUUFBUTtRQUNOLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQzFDLENBQUM7UUFDRixJQUFJLENBQUMsV0FBVyxHQUFHO1lBQ2pCLFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO1NBQ25DLENBQUM7UUFDRixJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDaEIsQ0FBQztJQUVELE1BQU07UUFDSixJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDO1FBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ3hELElBQUksV0FBVyxFQUFFO1lBQ2YsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxJQUFJLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQztZQUM3QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7WUFDcEIsSUFBRyxXQUFXLENBQUMsY0FBYyxJQUFJLFdBQVcsQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUN2RTtnQkFDRSxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQ2hELFdBQVc7b0JBRVgsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO3dCQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFBO3FCQUN2RDt5QkFBTTt3QkFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDeEU7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtZQUVELElBQUcsV0FBVyxDQUFDLGFBQWEsRUFBQztnQkFDM0IsSUFBSSxjQUFjLEdBQUcsV0FBVyxDQUFDLGNBQWMsQ0FBQztnQkFDOUMsSUFBSSxNQUFNLEdBQUcsY0FBYyxDQUFDLE1BQU0sQ0FBQztnQkFDckMsSUFBSSxDQUFDLGNBQWM7cUJBQ2hCLGFBQWEsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQztxQkFDdkMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNoQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ2xFLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLGlCQUFpQixDQUFDLENBQUM7b0JBQ3pELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsWUFBWSxFQUFFLFVBQVMsSUFBSTt3QkFDN0MsSUFBSSxJQUFJLEdBQUcsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsWUFBd0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO3dCQUNqSixJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzt3QkFDbkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUMxQyxDQUFDLENBQUMsQ0FBQztnQkFDTCxDQUFDLENBQUMsQ0FBQzthQUNOO2lCQUFJO2dCQUNILENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRSxVQUFTLElBQUk7b0JBQy9DLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7b0JBQ3pDLElBQUksTUFBTSxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ25DLElBQUksWUFBWSxHQUFHLGNBQWMsQ0FBQyxRQUFRLENBQUM7b0JBQzNDLG1CQUFtQjtvQkFDbkIsOEJBQThCO29CQUM5QixJQUFJO29CQUNKLElBQUksQ0FBQyxjQUFjO3lCQUNoQixhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUM7eUJBQ3ZDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDaEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQWEsQ0FBQzt3QkFFbkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLENBQUM7d0JBQ25DLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDeEMsQ0FBQyxDQUFDLENBQUM7Z0JBQ1AsQ0FBQyxDQUFDLENBQUM7YUFDSjtTQUNGO0lBQ0gsQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLFNBQVM7UUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDN0IsSUFBSSxTQUFTLENBQUMsV0FBVyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDLElBQUk7Z0JBQzdCLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSTtnQkFDaEIsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1lBQ3RELElBQUksQ0FBQyxVQUFVLEdBQUc7Z0JBQ2hCLEtBQUssRUFBRSxTQUFTLENBQUMsS0FBSztnQkFDdEIsTUFBTSxFQUFFLFNBQVMsQ0FBQyxNQUFNO2FBQ3pCLENBQUM7U0FDSDtRQUNELElBQUksU0FBUyxDQUFDLGNBQWMsRUFBRTtZQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsWUFBWSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFFaEUsSUFBSSxDQUFDLGFBQWEsR0FBRztnQkFDbkIsS0FBSyxFQUFFLFNBQVMsQ0FBQyxLQUFLO2dCQUN0QixNQUFNLEVBQUUsU0FBUyxDQUFDLE1BQU07YUFDekIsQ0FBQztTQUNIO1FBQ0QsSUFBSSxTQUFTLENBQUMsYUFBYSxFQUFFO1lBQzNCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1lBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztZQUUvRCxJQUFJLENBQUMsWUFBWSxHQUFHO2dCQUNsQixLQUFLLEVBQUUsU0FBUyxDQUFDLEtBQUs7Z0JBQ3RCLE1BQU0sRUFBRSxTQUFTLENBQUMsTUFBTTthQUN6QixDQUFDO1NBQ0g7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBUyxJQUFJO1lBQzNCLElBQUksT0FBTyxHQUFHO2dCQUNaLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtnQkFDZixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7YUFDbEIsQ0FBQztZQUNGLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFO2dCQUM3QixPQUFPLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUM7YUFDaEU7WUFDRCxJQUFJLFNBQVMsQ0FBQyxXQUFXLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQzlCO1lBQ0QsSUFBSSxTQUFTLENBQUMsY0FBYyxFQUFFO2dCQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzthQUNqQztZQUNELElBQUksU0FBUyxDQUFDLGFBQWEsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDaEM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7OztZQTVKRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLHM4REFBNEM7O2FBRTdDOzs7O1lBUlEsY0FBYztZQUZkLGNBQWM7WUFLZCxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi8uLi9fc2VydmljZXMvbWVzc2FnZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSBcIi4uL2NvbnRlbnQvY29udGVudC5zZXJ2aWNlXCI7XHJcblxyXG5pbXBvcnQgKiBhcyBfIGZyb20gXCJsb2Rhc2hcIjtcclxuaW1wb3J0IHsgTG9hZGVyU2VydmljZSB9IGZyb20gJy4uL2xvYWRlci5zZXJ2aWNlJztcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiY2hhcnQtbGF5b3V0XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9jaGFydC1sYXlvdXQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vY2hhcnQtbGF5b3V0LmNvbXBvbmVudC5zY3NzXCJdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDaGFydExheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgZW5hYmxlQ2hhcnQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBlbmFibGVQaWVDaGFydDogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGVuYWJsZVBpZUdyaWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgY3VycmVudENvbmZpZ0RhdGE6IGFueTtcclxuXHJcbiAgY2hhcnRMYWJlbDogYW55O1xyXG4gIHBpZUNoYXJ0TGFiZWw6IGFueTtcclxuICBwaWVHcmlkTGFiZWw6IGFueTtcclxuICBjaGFydFZpZXc6IGFueTtcclxuICBwaWVDaGFydFZpZXc6IGFueTtcclxuICBwaWVHcmlkVmlldzogYW55O1xyXG4gIHF1ZXJ5UGFyYW1zOiBhbnk7XHJcbiAgY29sb3JzOiBhbnk7XHJcbiAgY2hhcnREYXRhOiBhbnkgPSBbXTtcclxuICBwaWVDaGFydERhdGE6IGFueSA9IFtdO1xyXG4gIHBpZUdyaWREYXRhOiBhbnkgPSBbXTtcclxuICB2aWV3OiBhbnk7XHJcbiAgZGVmYXVsdERhdGFzb3VyY2U6IGFueTtcclxuICBpbnB1dERhdGE6IGFueSA9IHt9O1xyXG5cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgY29udGVudFNlcnZpY2U6IENvbnRlbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2UuZ2V0TWVzc2FnZSgpLnN1YnNjcmliZShtZXNzYWdlID0+IHtcclxuICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZVwiKTtcclxuICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICApO1xyXG4gICAgdGhpcy5xdWVyeVBhcmFtcyA9IHtcclxuICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZVxyXG4gICAgfTtcclxuICAgIHRoaXMub25Mb2FkKCk7XHJcbiAgfVxyXG5cclxuICBvbkxvYWQoKSB7XHJcbiAgICBsZXQgY3VycmVudERhdGEgPSB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl0uY2hhcnREYXRhO1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJkYXRhc291cmNlSWRcIl0gPSB0aGlzLmRlZmF1bHREYXRhc291cmNlO1xyXG4gICAgaWYgKGN1cnJlbnREYXRhKSB7XHJcbiAgICAgIHRoaXMuY29sb3JzID0gY3VycmVudERhdGEuY29sb3JzO1xyXG4gICAgICB0aGlzLnZpZXcgPSBjdXJyZW50RGF0YS5WaWV3O1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHRoaXMucGllR3JpZERhdGEgPSBbXTtcclxuICAgICAgdGhpcy5waWVDaGFydERhdGEgPSBbXTtcclxuICAgICAgdGhpcy5jaGFydERhdGEgPSBbXTtcclxuICAgICAgaWYoY3VycmVudERhdGEub25Mb2FkRnVuY3Rpb24gJiYgY3VycmVudERhdGEub25Mb2FkRnVuY3Rpb24ucmVxdWVzdERhdGEpXHJcbiAgICAgIHtcclxuICAgICAgICBfLmZvckVhY2goY3VycmVudERhdGEub25Mb2FkRnVuY3Rpb24ucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChcclxuICAgICAgICAgIHJlcXVlc3RJdGVtXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZVxyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgXHJcbiAgICAgIGlmKGN1cnJlbnREYXRhLnNpbmdsZVJlcXVlc3Qpe1xyXG4gICAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IGN1cnJlbnREYXRhLm9uTG9hZEZ1bmN0aW9uO1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IHJlcXVlc3REZXRhaWxzLmFwaVVybDtcclxuICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAuZ2V0QWxsUmVwb25zZSh0aGlzLnF1ZXJ5UGFyYW1zLCBhcGlVcmwpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKHJlc3AgPT4ge1xyXG4gICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSByZXNwLnJlc3BvbnNlW2N1cnJlbnREYXRhLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlXTtcclxuICAgICAgICAgICAgbGV0IGtleUJ5UmVzcG9uc2UgPSBfLmtleUJ5KHJlc3BvbnNlLCBcImNvdW50c291cmNlTmFtZVwiKTtcclxuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgICBfLmZvckVhY2goY3VycmVudERhdGEuY2hhcnRSZXF1ZXN0LCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgZGF0YSA9IChrZXlCeVJlc3BvbnNlW2l0ZW0uY2hhcnRJZF0gJiYga2V5QnlSZXNwb25zZVtpdGVtLmNoYXJ0SWRdLmNvdW50RGV0YWlscykgPyBrZXlCeVJlc3BvbnNlW2l0ZW0uY2hhcnRJZF0uY291bnREZXRhaWxzIGFzIG9iamVjdFtdIDogW107XHJcbiAgICAgICAgICAgICAgICBsZXQgdG90YWwgPSBfLnN1bUJ5KGRhdGEsIFwiY291bnRcIik7XHJcbiAgICAgICAgICAgICAgICBzZWxmLmNvbnN0cnVjdERhdGEoZGF0YSwgdG90YWwsIGl0ZW0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICBfLmZvckVhY2goY3VycmVudERhdGEuY2hhcnRSZXF1ZXN0LCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uO1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IHJlcXVlc3REZXRhaWxzLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSByZXF1ZXN0RGV0YWlscy5yZXNwb25zZTtcclxuICAgICAgICAgIC8vIGlmIChpdGVtLlZpZXcpIHtcclxuICAgICAgICAgIC8vICAgdGhpcy52aWV3ID0gaXRlbVtcIlZpZXdcIl07XHJcbiAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRBbGxSZXBvbnNlKHNlbGYucXVlcnlQYXJhbXMsIGFwaVVybClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShyZXNwID0+IHtcclxuICAgICAgICAgICAgICBsZXQgZGF0YSA9IHJlc3AucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSBhcyBvYmplY3RbXTtcclxuICBcclxuICAgICAgICAgICAgICBsZXQgdG90YWwgPSBfLnN1bUJ5KGRhdGEsIFwiY291bnRcIik7XHJcbiAgICAgICAgICAgICAgc2VsZi5jb25zdHJ1Y3REYXRhKGRhdGEsIHRvdGFsLCBpdGVtKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIGNvbnN0cnVjdERhdGEoZGF0YSwgdG90YWwsIGNoYXJ0UmVzcCkge1xyXG4gICAgY29uc29sZS5sb2coY2hhcnRSZXNwLCBcIi4uLmNoYXJ0UmVzcFwiKTtcclxuICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiLi4uZGF0YVwiKTtcclxuICAgIGlmIChjaGFydFJlc3AuZW5hYmxlQ2hhcnQpIHtcclxuICAgICAgdGhpcy5lbmFibGVDaGFydCA9IHRydWU7XHJcbiAgICAgIHRoaXMuY2hhcnRWaWV3ID0gY2hhcnRSZXNwLlZpZXdcclxuICAgICAgICA/IGNoYXJ0UmVzcC5WaWV3XHJcbiAgICAgICAgOiB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl0uY2hhcnREYXRhLlZpZXc7XHJcbiAgICAgIHRoaXMuY2hhcnRMYWJlbCA9IHtcclxuICAgICAgICB0aXRsZTogY2hhcnRSZXNwLmxhYmVsLFxyXG4gICAgICAgIG5vZGF0YTogY2hhcnRSZXNwLm5vZGF0YVxyXG4gICAgICB9O1xyXG4gICAgfVxyXG4gICAgaWYgKGNoYXJ0UmVzcC5lbmFibGVQaWVDaGFydCkge1xyXG4gICAgICB0aGlzLmVuYWJsZVBpZUNoYXJ0ID0gdHJ1ZTtcclxuICAgICAgdGhpcy5waWVDaGFydFZpZXcgPSBjaGFydFJlc3AuVmlldyA/IGNoYXJ0UmVzcC5WaWV3IDogdGhpcy52aWV3O1xyXG5cclxuICAgICAgdGhpcy5waWVDaGFydExhYmVsID0ge1xyXG4gICAgICAgIHRpdGxlOiBjaGFydFJlc3AubGFiZWwsXHJcbiAgICAgICAgbm9kYXRhOiBjaGFydFJlc3Aubm9kYXRhXHJcbiAgICAgIH07XHJcbiAgICB9XHJcbiAgICBpZiAoY2hhcnRSZXNwLmVuYWJsZVBpZUdyaWQpIHtcclxuICAgICAgdGhpcy5lbmFibGVQaWVHcmlkID0gdHJ1ZTtcclxuICAgICAgdGhpcy5waWVHcmlkVmlldyA9IGNoYXJ0UmVzcC5WaWV3ID8gY2hhcnRSZXNwLlZpZXcgOiB0aGlzLnZpZXc7XHJcblxyXG4gICAgICB0aGlzLnBpZUdyaWRMYWJlbCA9IHtcclxuICAgICAgICB0aXRsZTogY2hhcnRSZXNwLmxhYmVsLFxyXG4gICAgICAgIG5vZGF0YTogY2hhcnRSZXNwLm5vZGF0YVxyXG4gICAgICB9O1xyXG4gICAgfVxyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgXy5mb3JFYWNoKGRhdGEsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgbGV0IHRlbXBPYmogPSB7XHJcbiAgICAgICAgbmFtZTogaXRlbS5uYW1lLFxyXG4gICAgICAgIHZhbHVlOiBpdGVtLmNvdW50XHJcbiAgICAgIH07XHJcbiAgICAgIGlmICghY2hhcnRSZXNwLmVuYWJsZVBpZUNoYXJ0KSB7XHJcbiAgICAgICAgdGVtcE9ialtcInBlcmNlbnRhZ2VcIl0gPSBNYXRoLmZsb29yKChpdGVtLmNvdW50IC8gdG90YWwpICogMTAwKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoY2hhcnRSZXNwLmVuYWJsZUNoYXJ0KSB7XHJcbiAgICAgICAgc2VsZi5jaGFydERhdGEucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoY2hhcnRSZXNwLmVuYWJsZVBpZUNoYXJ0KSB7XHJcbiAgICAgICAgc2VsZi5waWVDaGFydERhdGEucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoY2hhcnRSZXNwLmVuYWJsZVBpZUdyaWQpIHtcclxuICAgICAgICBzZWxmLnBpZUdyaWREYXRhLnB1c2godGVtcE9iaik7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=