import { FuseWidgetModule } from "./../@fuse/components/widget/widget.module";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { HttpClientModule } from "@angular/common/http";
import { HorizontalChartlayoutModule } from "../horizontal-chart/horizontal-chart-layout.module";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { PieChartlayoutModule } from "../pie-chart/pie-chart.module";
import { ChartsModule } from "ng2-charts";
import { PieGridlayoutModule } from "../pie-grid/pie-grid.module";
import { ChartLayoutComponent } from "./chart-layout.component";
export class ChartLayoutModule {
}
ChartLayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ChartLayoutComponent],
                imports: [
                    RouterModule,
                    FuseSharedModule,
                    NgxChartsModule,
                    ChartsModule,
                    HorizontalChartlayoutModule,
                    HttpClientModule,
                    FuseWidgetModule,
                    PieChartlayoutModule,
                    PieGridlayoutModule,
                    TranslateModule.forRoot()
                ],
                exports: [ChartLayoutComponent, TranslateModule]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hhcnQtbGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJjaGFydC1sYXlvdXQvY2hhcnQtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUM5RSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUUxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFDakcsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ3JFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxZQUFZLENBQUM7QUFDMUMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDbEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFrQmhFLE1BQU0sT0FBTyxpQkFBaUI7OztZQWhCN0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLG9CQUFvQixDQUFDO2dCQUNwQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YsWUFBWTtvQkFDWiwyQkFBMkI7b0JBQzNCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixvQkFBb0I7b0JBQ3BCLG1CQUFtQjtvQkFDbkIsZUFBZSxDQUFDLE9BQU8sRUFBRTtpQkFDMUI7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsb0JBQW9CLEVBQUUsZUFBZSxDQUFDO2FBQ2pEIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRnVzZVdpZGdldE1vZHVsZSB9IGZyb20gXCIuLy4uL0BmdXNlL2NvbXBvbmVudHMvd2lkZ2V0L3dpZGdldC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcblxyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IEhvcml6b250YWxDaGFydGxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9ob3Jpem9udGFsLWNoYXJ0L2hvcml6b250YWwtY2hhcnQtbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBOZ3hDaGFydHNNb2R1bGUgfSBmcm9tIFwiQHN3aW1sYW5lL25neC1jaGFydHNcIjtcclxuaW1wb3J0IHsgUGllQ2hhcnRsYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vcGllLWNoYXJ0L3BpZS1jaGFydC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQ2hhcnRzTW9kdWxlIH0gZnJvbSBcIm5nMi1jaGFydHNcIjtcclxuaW1wb3J0IHsgUGllR3JpZGxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9waWUtZ3JpZC9waWUtZ3JpZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQ2hhcnRMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi9jaGFydC1sYXlvdXQuY29tcG9uZW50XCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0NoYXJ0TGF5b3V0Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICBGdXNlU2hhcmVkTW9kdWxlLFxyXG4gICAgTmd4Q2hhcnRzTW9kdWxlLFxyXG4gICAgQ2hhcnRzTW9kdWxlLFxyXG4gICAgSG9yaXpvbnRhbENoYXJ0bGF5b3V0TW9kdWxlLFxyXG4gICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgIEZ1c2VXaWRnZXRNb2R1bGUsXHJcbiAgICBQaWVDaGFydGxheW91dE1vZHVsZSxcclxuICAgIFBpZUdyaWRsYXlvdXRNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUuZm9yUm9vdCgpXHJcbiAgXSxcclxuICBleHBvcnRzOiBbQ2hhcnRMYXlvdXRDb21wb25lbnQsIFRyYW5zbGF0ZU1vZHVsZV1cclxufSlcclxuZXhwb3J0IGNsYXNzIENoYXJ0TGF5b3V0TW9kdWxlIHt9XHJcbiJdfQ==