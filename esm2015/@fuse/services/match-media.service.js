import { ObservableMedia } from "@angular/flex-layout";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import * as i0 from "@angular/core";
import * as i1 from "@angular/flex-layout";
export class FuseMatchMediaService {
    /**
     * Constructor
     *
     * @param {ObservableMedia} _observableMedia
     */
    constructor(_observableMedia) {
        this._observableMedia = _observableMedia;
        this.onMediaChange = new BehaviorSubject("");
        // Set the defaults
        this.activeMediaQuery = "";
        // Initialize
        this._init();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    _init() {
        this._observableMedia
            .asObservable()
            .pipe(debounceTime(500), distinctUntilChanged())
            .subscribe((change) => {
            if (this.activeMediaQuery !== change.mqAlias) {
                this.activeMediaQuery = change.mqAlias;
                this.onMediaChange.next(change.mqAlias);
            }
        });
    }
}
FuseMatchMediaService.decorators = [
    { type: Injectable, args: [{
                providedIn: "root"
            },] }
];
/** @nocollapse */
FuseMatchMediaService.ctorParameters = () => [
    { type: ObservableMedia }
];
FuseMatchMediaService.ngInjectableDef = i0.defineInjectable({ factory: function FuseMatchMediaService_Factory() { return new FuseMatchMediaService(i0.inject(i1.ObservableMedia)); }, token: FuseMatchMediaService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0Y2gtbWVkaWEuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9zZXJ2aWNlcy9tYXRjaC1tZWRpYS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBZSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdkMsT0FBTyxFQUFFLFlBQVksRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7QUFLcEUsTUFBTSxPQUFPLHFCQUFxQjtJQUloQzs7OztPQUlHO0lBQ0gsWUFBb0IsZ0JBQWlDO1FBQWpDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBaUI7UUFQckQsa0JBQWEsR0FBNEIsSUFBSSxlQUFlLENBQVMsRUFBRSxDQUFDLENBQUM7UUFRdkUsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFFM0IsYUFBYTtRQUNiLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNmLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0ssS0FBSztRQUNYLElBQUksQ0FBQyxnQkFBZ0I7YUFDbEIsWUFBWSxFQUFFO2FBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsRUFBRSxvQkFBb0IsRUFBRSxDQUFDO2FBQy9DLFNBQVMsQ0FBQyxDQUFDLE1BQW1CLEVBQUUsRUFBRTtZQUNqQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxNQUFNLENBQUMsT0FBTyxFQUFFO2dCQUM1QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQztnQkFDdkMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ3pDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7WUF2Q0YsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25COzs7O1lBUHFCLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNZWRpYUNoYW5nZSwgT2JzZXJ2YWJsZU1lZGlhIH0gZnJvbSBcIkBhbmd1bGFyL2ZsZXgtbGF5b3V0XCI7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBkZWJvdW5jZVRpbWUsIGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogXCJyb290XCJcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VNYXRjaE1lZGlhU2VydmljZSB7XHJcbiAgYWN0aXZlTWVkaWFRdWVyeTogc3RyaW5nO1xyXG4gIG9uTWVkaWFDaGFuZ2U6IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+ID0gbmV3IEJlaGF2aW9yU3ViamVjdDxzdHJpbmc+KFwiXCIpO1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtPYnNlcnZhYmxlTWVkaWF9IF9vYnNlcnZhYmxlTWVkaWFcclxuICAgKi9cclxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIF9vYnNlcnZhYmxlTWVkaWE6IE9ic2VydmFibGVNZWRpYSkge1xyXG4gICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5hY3RpdmVNZWRpYVF1ZXJ5ID0gXCJcIjtcclxuXHJcbiAgICAvLyBJbml0aWFsaXplXHJcbiAgICB0aGlzLl9pbml0KCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHJpdmF0ZSBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogSW5pdGlhbGl6ZVxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9pbml0KCk6IHZvaWQge1xyXG4gICAgdGhpcy5fb2JzZXJ2YWJsZU1lZGlhXHJcbiAgICAgIC5hc09ic2VydmFibGUoKVxyXG4gICAgICAucGlwZShkZWJvdW5jZVRpbWUoNTAwKSwgZGlzdGluY3RVbnRpbENoYW5nZWQoKSlcclxuICAgICAgLnN1YnNjcmliZSgoY2hhbmdlOiBNZWRpYUNoYW5nZSkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLmFjdGl2ZU1lZGlhUXVlcnkgIT09IGNoYW5nZS5tcUFsaWFzKSB7XHJcbiAgICAgICAgICB0aGlzLmFjdGl2ZU1lZGlhUXVlcnkgPSBjaGFuZ2UubXFBbGlhcztcclxuICAgICAgICAgIHRoaXMub25NZWRpYUNoYW5nZS5uZXh0KGNoYW5nZS5tcUFsaWFzKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxufVxyXG4iXX0=