import { Injectable } from "@angular/core";
import { Router, RoutesRecognized } from "@angular/router";
import { Platform } from "@angular/cdk/platform";
import { BehaviorSubject } from "rxjs";
import { filter } from "rxjs/operators";
import * as _ from "lodash";
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/platform";
import * as i2 from "@angular/router";
// Create the injection token for the custom settings
// export const FUSE_CONFIG = new InjectionToken('fuseCustomConfig');
export class FuseConfigService {
    /**
     * Constructor
     *
     * @param {Platform} _platform
     * @param {Router} _router
     * @param _config
     */
    constructor(_platform, _router) {
        this._platform = _platform;
        this._router = _router;
        // Set the default config from the user provided config (from forRoot)
        this._defaultConfig = {
            // Color themes can be defined in src/app/app.theme.scss
            colorTheme: "theme-default",
            customScrollbars: true,
            layout: {
                style: "vertical-layout-1",
                width: "fullwidth",
                navbar: {
                    primaryBackground: "fuse-navy-700",
                    secondaryBackground: "fuse-navy-900",
                    folded: false,
                    hidden: false,
                    position: "left",
                    variant: "vertical-style-1"
                },
                toolbar: {
                    customBackgroundColor: false,
                    background: "fuse-white-500",
                    hidden: false,
                    position: "below-static"
                },
                footer: {
                    customBackgroundColor: true,
                    background: "fuse-navy-900",
                    hidden: false,
                    position: "below-fixed"
                },
                sidepanel: {
                    hidden: false,
                    position: "right"
                }
            }
        };
        // Initialize the service
        this._init();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Set and get the config
     */
    set config(value) {
        // Get the value from the behavior subject
        let config = this._configSubject.getValue();
        // Merge the new config
        config = _.merge({}, config, value);
        // Notify the observers
        this._configSubject.next(config);
    }
    get config() {
        return this._configSubject.asObservable();
    }
    /**
     * Get default config
     *
     * @returns {any}
     */
    get defaultConfig() {
        return this._defaultConfig;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    _init() {
        /**
         * Disable custom scrollbars if browser is mobile
         */
        if (this._platform.ANDROID || this._platform.IOS) {
            this._defaultConfig.customScrollbars = false;
        }
        // Set the config from the default config
        this._configSubject = new BehaviorSubject(_.cloneDeep(this._defaultConfig));
        // Reload the default layout config on every RoutesRecognized event
        // if the current layout config is different from the default one
        this._router.events
            .pipe(filter(event => event instanceof RoutesRecognized))
            .subscribe(() => {
            if (!_.isEqual(this._configSubject.getValue().layout, this._defaultConfig.layout)) {
                // Clone the current config
                const config = _.cloneDeep(this._configSubject.getValue());
                // Reset the layout from the default config
                config.layout = _.cloneDeep(this._defaultConfig.layout);
                // Set the config
                this._configSubject.next(config);
            }
        });
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Set config
     *
     * @param value
     * @param {{emitEvent: boolean}} opts
     */
    setConfig(value, opts = { emitEvent: true }) {
        // Get the value from the behavior subject
        let config = this._configSubject.getValue();
        // Merge the new config
        config = _.merge({}, config, value);
        // If emitEvent option is true...
        if (opts.emitEvent === true) {
            // Notify the observers
            this._configSubject.next(config);
        }
    }
    /**
     * Get config
     *
     * @returns {Observable<any>}
     */
    getConfig() {
        return this._configSubject.asObservable();
    }
    /**
     * Reset to the default config
     */
    resetToDefaults() {
        // Set the config from the default config
        this._configSubject.next(_.cloneDeep(this._defaultConfig));
    }
}
FuseConfigService.decorators = [
    { type: Injectable, args: [{
                providedIn: "root"
            },] }
];
/** @nocollapse */
FuseConfigService.ctorParameters = () => [
    { type: Platform },
    { type: Router }
];
FuseConfigService.ngInjectableDef = i0.defineInjectable({ factory: function FuseConfigService_Factory() { return new FuseConfigService(i0.inject(i1.Platform), i0.inject(i2.Router)); }, token: FuseConfigService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlnLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2Uvc2VydmljZXMvY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFVLFVBQVUsRUFBa0IsTUFBTSxlQUFlLENBQUM7QUFDbkUsT0FBTyxFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNqRCxPQUFPLEVBQUUsZUFBZSxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN4QyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQzs7OztBQUU1QixxREFBcUQ7QUFDckQscUVBQXFFO0FBS3JFLE1BQU0sT0FBTyxpQkFBaUI7SUFLNUI7Ozs7OztPQU1HO0lBQ0gsWUFDVSxTQUFtQixFQUNuQixPQUFlO1FBRGYsY0FBUyxHQUFULFNBQVMsQ0FBVTtRQUNuQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBR3ZCLHNFQUFzRTtRQUN0RSxJQUFJLENBQUMsY0FBYyxHQUFHO1lBQ3BCLHdEQUF3RDtZQUN4RCxVQUFVLEVBQUUsZUFBZTtZQUMzQixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLE1BQU0sRUFBRTtnQkFDTixLQUFLLEVBQUUsbUJBQW1CO2dCQUMxQixLQUFLLEVBQUUsV0FBVztnQkFDbEIsTUFBTSxFQUFFO29CQUNOLGlCQUFpQixFQUFFLGVBQWU7b0JBQ2xDLG1CQUFtQixFQUFFLGVBQWU7b0JBQ3BDLE1BQU0sRUFBRSxLQUFLO29CQUNiLE1BQU0sRUFBRSxLQUFLO29CQUNiLFFBQVEsRUFBRSxNQUFNO29CQUNoQixPQUFPLEVBQUUsa0JBQWtCO2lCQUM1QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ1AscUJBQXFCLEVBQUUsS0FBSztvQkFDNUIsVUFBVSxFQUFFLGdCQUFnQjtvQkFDNUIsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsUUFBUSxFQUFFLGNBQWM7aUJBQ3pCO2dCQUNELE1BQU0sRUFBRTtvQkFDTixxQkFBcUIsRUFBRSxJQUFJO29CQUMzQixVQUFVLEVBQUUsZUFBZTtvQkFDM0IsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsUUFBUSxFQUFFLGFBQWE7aUJBQ3hCO2dCQUNELFNBQVMsRUFBRTtvQkFDVCxNQUFNLEVBQUUsS0FBSztvQkFDYixRQUFRLEVBQUUsT0FBTztpQkFDbEI7YUFDRjtTQUNGLENBQUM7UUFFRix5QkFBeUI7UUFDekIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2YsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxjQUFjO0lBQ2Qsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsSUFBSSxNQUFNLENBQUMsS0FBSztRQUNkLDBDQUEwQztRQUMxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRTVDLHVCQUF1QjtRQUN2QixNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXBDLHVCQUF1QjtRQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsSUFBSSxNQUFNO1FBQ1IsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsSUFBSSxhQUFhO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzdCLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsb0JBQW9CO0lBQ3BCLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0ssS0FBSztRQUNYOztXQUVHO1FBQ0gsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNoRCxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztTQUM5QztRQUVELHlDQUF5QztRQUN6QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksZUFBZSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFFNUUsbUVBQW1FO1FBQ25FLGlFQUFpRTtRQUNqRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07YUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssWUFBWSxnQkFBZ0IsQ0FBQyxDQUFDO2FBQ3hELFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFDZCxJQUNFLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FDUixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDLE1BQU0sRUFDckMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQzNCLEVBQ0Q7Z0JBQ0EsMkJBQTJCO2dCQUMzQixNQUFNLE1BQU0sR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQztnQkFFM0QsMkNBQTJDO2dCQUMzQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFFeEQsaUJBQWlCO2dCQUNqQixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUNsQztRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOzs7OztPQUtHO0lBQ0gsU0FBUyxDQUFDLEtBQUssRUFBRSxJQUFJLEdBQUcsRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFO1FBQ3pDLDBDQUEwQztRQUMxQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBRTVDLHVCQUF1QjtRQUN2QixNQUFNLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBRXBDLGlDQUFpQztRQUNqQyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxFQUFFO1lBQzNCLHVCQUF1QjtZQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNsQztJQUNILENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsU0FBUztRQUNQLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQ7O09BRUc7SUFDSCxlQUFlO1FBQ2IseUNBQXlDO1FBQ3pDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7SUFDN0QsQ0FBQzs7O1lBNUtGLFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQVZRLFFBQVE7WUFEUixNQUFNIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlciwgUm91dGVzUmVjb2duaXplZCB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgUGxhdGZvcm0gfSBmcm9tIFwiQGFuZ3VsYXIvY2RrL3BsYXRmb3JtXCI7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCB7IGZpbHRlciB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gXCJsb2Rhc2hcIjtcclxuXHJcbi8vIENyZWF0ZSB0aGUgaW5qZWN0aW9uIHRva2VuIGZvciB0aGUgY3VzdG9tIHNldHRpbmdzXHJcbi8vIGV4cG9ydCBjb25zdCBGVVNFX0NPTkZJRyA9IG5ldyBJbmplY3Rpb25Ub2tlbignZnVzZUN1c3RvbUNvbmZpZycpO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gIHByb3ZpZGVkSW46IFwicm9vdFwiXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlQ29uZmlnU2VydmljZSB7XHJcbiAgLy8gUHJpdmF0ZVxyXG4gIHByaXZhdGUgX2NvbmZpZ1N1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gIHByaXZhdGUgcmVhZG9ubHkgX2RlZmF1bHRDb25maWc6IGFueTtcclxuXHJcbiAgLyoqXHJcbiAgICogQ29uc3RydWN0b3JcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7UGxhdGZvcm19IF9wbGF0Zm9ybVxyXG4gICAqIEBwYXJhbSB7Um91dGVyfSBfcm91dGVyXHJcbiAgICogQHBhcmFtIF9jb25maWdcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX3BsYXRmb3JtOiBQbGF0Zm9ybSxcclxuICAgIHByaXZhdGUgX3JvdXRlcjogUm91dGVyXHJcbiAgKSAvLyBASW5qZWN0KEZVU0VfQ09ORklHKSBwcml2YXRlIF9jb25maWdcclxuICB7XHJcbiAgICAvLyBTZXQgdGhlIGRlZmF1bHQgY29uZmlnIGZyb20gdGhlIHVzZXIgcHJvdmlkZWQgY29uZmlnIChmcm9tIGZvclJvb3QpXHJcbiAgICB0aGlzLl9kZWZhdWx0Q29uZmlnID0ge1xyXG4gICAgICAvLyBDb2xvciB0aGVtZXMgY2FuIGJlIGRlZmluZWQgaW4gc3JjL2FwcC9hcHAudGhlbWUuc2Nzc1xyXG4gICAgICBjb2xvclRoZW1lOiBcInRoZW1lLWRlZmF1bHRcIixcclxuICAgICAgY3VzdG9tU2Nyb2xsYmFyczogdHJ1ZSxcclxuICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgc3R5bGU6IFwidmVydGljYWwtbGF5b3V0LTFcIixcclxuICAgICAgICB3aWR0aDogXCJmdWxsd2lkdGhcIixcclxuICAgICAgICBuYXZiYXI6IHtcclxuICAgICAgICAgIHByaW1hcnlCYWNrZ3JvdW5kOiBcImZ1c2UtbmF2eS03MDBcIixcclxuICAgICAgICAgIHNlY29uZGFyeUJhY2tncm91bmQ6IFwiZnVzZS1uYXZ5LTkwMFwiLFxyXG4gICAgICAgICAgZm9sZGVkOiBmYWxzZSxcclxuICAgICAgICAgIGhpZGRlbjogZmFsc2UsXHJcbiAgICAgICAgICBwb3NpdGlvbjogXCJsZWZ0XCIsXHJcbiAgICAgICAgICB2YXJpYW50OiBcInZlcnRpY2FsLXN0eWxlLTFcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdG9vbGJhcjoge1xyXG4gICAgICAgICAgY3VzdG9tQmFja2dyb3VuZENvbG9yOiBmYWxzZSxcclxuICAgICAgICAgIGJhY2tncm91bmQ6IFwiZnVzZS13aGl0ZS01MDBcIixcclxuICAgICAgICAgIGhpZGRlbjogZmFsc2UsXHJcbiAgICAgICAgICBwb3NpdGlvbjogXCJiZWxvdy1zdGF0aWNcIlxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZm9vdGVyOiB7XHJcbiAgICAgICAgICBjdXN0b21CYWNrZ3JvdW5kQ29sb3I6IHRydWUsXHJcbiAgICAgICAgICBiYWNrZ3JvdW5kOiBcImZ1c2UtbmF2eS05MDBcIixcclxuICAgICAgICAgIGhpZGRlbjogZmFsc2UsXHJcbiAgICAgICAgICBwb3NpdGlvbjogXCJiZWxvdy1maXhlZFwiXHJcbiAgICAgICAgfSxcclxuICAgICAgICBzaWRlcGFuZWw6IHtcclxuICAgICAgICAgIGhpZGRlbjogZmFsc2UsXHJcbiAgICAgICAgICBwb3NpdGlvbjogXCJyaWdodFwiXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIC8vIEluaXRpYWxpemUgdGhlIHNlcnZpY2VcclxuICAgIHRoaXMuX2luaXQoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBBY2Nlc3NvcnNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBTZXQgYW5kIGdldCB0aGUgY29uZmlnXHJcbiAgICovXHJcbiAgc2V0IGNvbmZpZyh2YWx1ZSkge1xyXG4gICAgLy8gR2V0IHRoZSB2YWx1ZSBmcm9tIHRoZSBiZWhhdmlvciBzdWJqZWN0XHJcbiAgICBsZXQgY29uZmlnID0gdGhpcy5fY29uZmlnU3ViamVjdC5nZXRWYWx1ZSgpO1xyXG5cclxuICAgIC8vIE1lcmdlIHRoZSBuZXcgY29uZmlnXHJcbiAgICBjb25maWcgPSBfLm1lcmdlKHt9LCBjb25maWcsIHZhbHVlKTtcclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIG9ic2VydmVyc1xyXG4gICAgdGhpcy5fY29uZmlnU3ViamVjdC5uZXh0KGNvbmZpZyk7XHJcbiAgfVxyXG5cclxuICBnZXQgY29uZmlnKCk6IGFueSB8IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fY29uZmlnU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBkZWZhdWx0IGNvbmZpZ1xyXG4gICAqXHJcbiAgICogQHJldHVybnMge2FueX1cclxuICAgKi9cclxuICBnZXQgZGVmYXVsdENvbmZpZygpOiBhbnkge1xyXG4gICAgcmV0dXJuIHRoaXMuX2RlZmF1bHRDb25maWc7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHJpdmF0ZSBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogSW5pdGlhbGl6ZVxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBwcml2YXRlIF9pbml0KCk6IHZvaWQge1xyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNhYmxlIGN1c3RvbSBzY3JvbGxiYXJzIGlmIGJyb3dzZXIgaXMgbW9iaWxlXHJcbiAgICAgKi9cclxuICAgIGlmICh0aGlzLl9wbGF0Zm9ybS5BTkRST0lEIHx8IHRoaXMuX3BsYXRmb3JtLklPUykge1xyXG4gICAgICB0aGlzLl9kZWZhdWx0Q29uZmlnLmN1c3RvbVNjcm9sbGJhcnMgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBTZXQgdGhlIGNvbmZpZyBmcm9tIHRoZSBkZWZhdWx0IGNvbmZpZ1xyXG4gICAgdGhpcy5fY29uZmlnU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3QoXy5jbG9uZURlZXAodGhpcy5fZGVmYXVsdENvbmZpZykpO1xyXG5cclxuICAgIC8vIFJlbG9hZCB0aGUgZGVmYXVsdCBsYXlvdXQgY29uZmlnIG9uIGV2ZXJ5IFJvdXRlc1JlY29nbml6ZWQgZXZlbnRcclxuICAgIC8vIGlmIHRoZSBjdXJyZW50IGxheW91dCBjb25maWcgaXMgZGlmZmVyZW50IGZyb20gdGhlIGRlZmF1bHQgb25lXHJcbiAgICB0aGlzLl9yb3V0ZXIuZXZlbnRzXHJcbiAgICAgIC5waXBlKGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIFJvdXRlc1JlY29nbml6ZWQpKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAhXy5pc0VxdWFsKFxyXG4gICAgICAgICAgICB0aGlzLl9jb25maWdTdWJqZWN0LmdldFZhbHVlKCkubGF5b3V0LFxyXG4gICAgICAgICAgICB0aGlzLl9kZWZhdWx0Q29uZmlnLmxheW91dFxyXG4gICAgICAgICAgKVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgLy8gQ2xvbmUgdGhlIGN1cnJlbnQgY29uZmlnXHJcbiAgICAgICAgICBjb25zdCBjb25maWcgPSBfLmNsb25lRGVlcCh0aGlzLl9jb25maWdTdWJqZWN0LmdldFZhbHVlKCkpO1xyXG5cclxuICAgICAgICAgIC8vIFJlc2V0IHRoZSBsYXlvdXQgZnJvbSB0aGUgZGVmYXVsdCBjb25maWdcclxuICAgICAgICAgIGNvbmZpZy5sYXlvdXQgPSBfLmNsb25lRGVlcCh0aGlzLl9kZWZhdWx0Q29uZmlnLmxheW91dCk7XHJcblxyXG4gICAgICAgICAgLy8gU2V0IHRoZSBjb25maWdcclxuICAgICAgICAgIHRoaXMuX2NvbmZpZ1N1YmplY3QubmV4dChjb25maWcpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBTZXQgY29uZmlnXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdmFsdWVcclxuICAgKiBAcGFyYW0ge3tlbWl0RXZlbnQ6IGJvb2xlYW59fSBvcHRzXHJcbiAgICovXHJcbiAgc2V0Q29uZmlnKHZhbHVlLCBvcHRzID0geyBlbWl0RXZlbnQ6IHRydWUgfSk6IHZvaWQge1xyXG4gICAgLy8gR2V0IHRoZSB2YWx1ZSBmcm9tIHRoZSBiZWhhdmlvciBzdWJqZWN0XHJcbiAgICBsZXQgY29uZmlnID0gdGhpcy5fY29uZmlnU3ViamVjdC5nZXRWYWx1ZSgpO1xyXG5cclxuICAgIC8vIE1lcmdlIHRoZSBuZXcgY29uZmlnXHJcbiAgICBjb25maWcgPSBfLm1lcmdlKHt9LCBjb25maWcsIHZhbHVlKTtcclxuXHJcbiAgICAvLyBJZiBlbWl0RXZlbnQgb3B0aW9uIGlzIHRydWUuLi5cclxuICAgIGlmIChvcHRzLmVtaXRFdmVudCA9PT0gdHJ1ZSkge1xyXG4gICAgICAvLyBOb3RpZnkgdGhlIG9ic2VydmVyc1xyXG4gICAgICB0aGlzLl9jb25maWdTdWJqZWN0Lm5leHQoY29uZmlnKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBjb25maWdcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICovXHJcbiAgZ2V0Q29uZmlnKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fY29uZmlnU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFJlc2V0IHRvIHRoZSBkZWZhdWx0IGNvbmZpZ1xyXG4gICAqL1xyXG4gIHJlc2V0VG9EZWZhdWx0cygpOiB2b2lkIHtcclxuICAgIC8vIFNldCB0aGUgY29uZmlnIGZyb20gdGhlIGRlZmF1bHQgY29uZmlnXHJcbiAgICB0aGlzLl9jb25maWdTdWJqZWN0Lm5leHQoXy5jbG9uZURlZXAodGhpcy5fZGVmYXVsdENvbmZpZykpO1xyXG4gIH1cclxufVxyXG4iXX0=