import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
export class FuseTranslationLoaderService {
    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     */
    constructor(_translateService) {
        this._translateService = _translateService;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Load translations
     *
     * @param {Locale} args
     */
    loadTranslations(...args) {
        const locales = [...args];
        locales.forEach((locale) => {
            // use setTranslation() with the third argument set to true
            // to append translations instead of replacing them
            this._translateService.setTranslation(locale.lang, locale.data, true);
        });
    }
    instant(value) {
        return this._translateService.instant(value);
    }
}
FuseTranslationLoaderService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
FuseTranslationLoaderService.ctorParameters = () => [
    { type: TranslateService }
];
FuseTranslationLoaderService.ngInjectableDef = i0.defineInjectable({ factory: function FuseTranslationLoaderService_Factory() { return new FuseTranslationLoaderService(i0.inject(i1.TranslateService)); }, token: FuseTranslationLoaderService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2Uvc2VydmljZXMvdHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQzs7O0FBV3ZELE1BQU0sT0FBTyw0QkFBNEI7SUFFckM7Ozs7T0FJRztJQUNILFlBQ1ksaUJBQW1DO1FBQW5DLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7SUFHL0MsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOzs7O09BSUc7SUFDSCxnQkFBZ0IsQ0FBQyxHQUFHLElBQWM7UUFFOUIsTUFBTSxPQUFPLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQzFCLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUN2QiwyREFBMkQ7WUFDM0QsbURBQW1EO1lBQ25ELElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFFLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELE9BQU8sQ0FBRSxLQUFLO1FBQ1YsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7OztZQXJDSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFWUSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgTG9jYWxlXHJcbntcclxuICAgIGxhbmc6IHN0cmluZztcclxuICAgIGRhdGE6IE9iamVjdDtcclxufVxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlXHJcbntcclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge1RyYW5zbGF0ZVNlcnZpY2V9IF90cmFuc2xhdGVTZXJ2aWNlXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX3RyYW5zbGF0ZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2VcclxuICAgIClcclxuICAgIHtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvYWQgdHJhbnNsYXRpb25zXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtMb2NhbGV9IGFyZ3NcclxuICAgICAqL1xyXG4gICAgbG9hZFRyYW5zbGF0aW9ucyguLi5hcmdzOiBMb2NhbGVbXSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICBjb25zdCBsb2NhbGVzID0gWy4uLmFyZ3NdO1xyXG4gICAgICAgIGxvY2FsZXMuZm9yRWFjaCgobG9jYWxlKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIHVzZSBzZXRUcmFuc2xhdGlvbigpIHdpdGggdGhlIHRoaXJkIGFyZ3VtZW50IHNldCB0byB0cnVlXHJcbiAgICAgICAgICAgIC8vIHRvIGFwcGVuZCB0cmFuc2xhdGlvbnMgaW5zdGVhZCBvZiByZXBsYWNpbmcgdGhlbVxyXG4gICAgICAgICAgICB0aGlzLl90cmFuc2xhdGVTZXJ2aWNlLnNldFRyYW5zbGF0aW9uKGxvY2FsZS5sYW5nLCBsb2NhbGUuZGF0YSwgdHJ1ZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5zdGFudCAodmFsdWUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdHJhbnNsYXRlU2VydmljZS5pbnN0YW50KHZhbHVlKTtcclxuICAgIH1cclxufVxyXG4iXX0=