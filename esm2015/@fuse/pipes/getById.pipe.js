import { Pipe } from '@angular/core';
export class GetByIdPipe {
    /**
     * Transform
     *
     * @param {any[]} value
     * @param {number} id
     * @param {string} property
     * @returns {any}
     */
    transform(value, id, property) {
        const foundItem = value.find(item => {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (foundItem) {
            return foundItem[property];
        }
    }
}
GetByIdPipe.decorators = [
    { type: Pipe, args: [{
                name: 'getById',
                pure: false
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ2V0QnlJZC5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL3BpcGVzL2dldEJ5SWQucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQU1wRCxNQUFNLE9BQU8sV0FBVztJQUVwQjs7Ozs7OztPQU9HO0lBQ0gsU0FBUyxDQUFDLEtBQVksRUFBRSxFQUFVLEVBQUUsUUFBZ0I7UUFFaEQsTUFBTSxTQUFTLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNoQyxJQUFLLElBQUksQ0FBQyxFQUFFLEtBQUssU0FBUyxFQUMxQjtnQkFDSSxPQUFPLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDO2FBQ3pCO1lBRUQsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFLLFNBQVMsRUFDZDtZQUNJLE9BQU8sU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzlCO0lBQ0wsQ0FBQzs7O1lBN0JKLElBQUksU0FBQztnQkFDRixJQUFJLEVBQUUsU0FBUztnQkFDZixJQUFJLEVBQUUsS0FBSzthQUNkIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ2dldEJ5SWQnLFxyXG4gICAgcHVyZTogZmFsc2VcclxufSlcclxuZXhwb3J0IGNsYXNzIEdldEJ5SWRQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybVxyXG57XHJcbiAgICAvKipcclxuICAgICAqIFRyYW5zZm9ybVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7YW55W119IHZhbHVlXHJcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gaWRcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBwcm9wZXJ0eVxyXG4gICAgICogQHJldHVybnMge2FueX1cclxuICAgICAqL1xyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBhbnlbXSwgaWQ6IG51bWJlciwgcHJvcGVydHk6IHN0cmluZyk6IGFueVxyXG4gICAge1xyXG4gICAgICAgIGNvbnN0IGZvdW5kSXRlbSA9IHZhbHVlLmZpbmQoaXRlbSA9PiB7XHJcbiAgICAgICAgICAgIGlmICggaXRlbS5pZCAhPT0gdW5kZWZpbmVkIClcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGl0ZW0uaWQgPT09IGlkO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICggZm91bmRJdGVtIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHJldHVybiBmb3VuZEl0ZW1bcHJvcGVydHldO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=