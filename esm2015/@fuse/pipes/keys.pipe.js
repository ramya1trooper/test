import { Pipe } from '@angular/core';
export class KeysPipe {
    /**
     * Transform
     *
     * @param value
     * @param {string[]} args
     * @returns {any}
     */
    transform(value, args) {
        const keys = [];
        for (const key in value) {
            if (value.hasOwnProperty(key)) {
                keys.push({
                    key: key,
                    value: value[key]
                });
            }
        }
        return keys;
    }
}
KeysPipe.decorators = [
    { type: Pipe, args: [{ name: 'keys' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoia2V5cy5waXBlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL3BpcGVzL2tleXMucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUdwRCxNQUFNLE9BQU8sUUFBUTtJQUVqQjs7Ozs7O09BTUc7SUFDSCxTQUFTLENBQUMsS0FBVSxFQUFFLElBQWM7UUFFaEMsTUFBTSxJQUFJLEdBQVUsRUFBRSxDQUFDO1FBRXZCLEtBQU0sTUFBTSxHQUFHLElBQUksS0FBSyxFQUN4QjtZQUNJLElBQUssS0FBSyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFDOUI7Z0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQztvQkFDTixHQUFHLEVBQUksR0FBRztvQkFDVixLQUFLLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQztpQkFDcEIsQ0FBQyxDQUFDO2FBQ047U0FDSjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7OztZQTFCSixJQUFJLFNBQUMsRUFBQyxJQUFJLEVBQUUsTUFBTSxFQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQFBpcGUoe25hbWU6ICdrZXlzJ30pXHJcbmV4cG9ydCBjbGFzcyBLZXlzUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm1cclxue1xyXG4gICAgLyoqXHJcbiAgICAgKiBUcmFuc2Zvcm1cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nW119IGFyZ3NcclxuICAgICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICAgKi9cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBhcmdzOiBzdHJpbmdbXSk6IGFueVxyXG4gICAge1xyXG4gICAgICAgIGNvbnN0IGtleXM6IGFueVtdID0gW107XHJcblxyXG4gICAgICAgIGZvciAoIGNvbnN0IGtleSBpbiB2YWx1ZSApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZiAoIHZhbHVlLmhhc093blByb3BlcnR5KGtleSkgKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBrZXlzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgIGtleSAgOiBrZXksXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHZhbHVlW2tleV1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4ga2V5cztcclxuICAgIH1cclxufVxyXG4iXX0=