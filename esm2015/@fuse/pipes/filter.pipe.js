import { Pipe } from "@angular/core";
import { FuseUtils } from "../../@fuse/utils";
export class FilterPipe {
    /**
     * Transform
     *
     * @param {any[]} mainArr
     * @param {string} searchText
     * @param {string} property
     * @returns {any}
     */
    transform(mainArr, searchText, property) {
        return FuseUtils.filterArrayByString(mainArr, searchText);
    }
}
FilterPipe.decorators = [
    { type: Pipe, args: [{ name: "filter" },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsdGVyLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvcGlwZXMvZmlsdGVyLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFDcEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRzlDLE1BQU0sT0FBTyxVQUFVO0lBQ3JCOzs7Ozs7O09BT0c7SUFDSCxTQUFTLENBQUMsT0FBYyxFQUFFLFVBQWtCLEVBQUUsUUFBZ0I7UUFDNUQsT0FBTyxTQUFTLENBQUMsbUJBQW1CLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQzVELENBQUM7OztZQVpGLElBQUksU0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgRnVzZVV0aWxzIH0gZnJvbSBcIi4uLy4uL0BmdXNlL3V0aWxzXCI7XHJcblxyXG5AUGlwZSh7IG5hbWU6IFwiZmlsdGVyXCIgfSlcclxuZXhwb3J0IGNsYXNzIEZpbHRlclBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuICAvKipcclxuICAgKiBUcmFuc2Zvcm1cclxuICAgKlxyXG4gICAqIEBwYXJhbSB7YW55W119IG1haW5BcnJcclxuICAgKiBAcGFyYW0ge3N0cmluZ30gc2VhcmNoVGV4dFxyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBwcm9wZXJ0eVxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgdHJhbnNmb3JtKG1haW5BcnI6IGFueVtdLCBzZWFyY2hUZXh0OiBzdHJpbmcsIHByb3BlcnR5OiBzdHJpbmcpOiBhbnkge1xyXG4gICAgcmV0dXJuIEZ1c2VVdGlscy5maWx0ZXJBcnJheUJ5U3RyaW5nKG1haW5BcnIsIHNlYXJjaFRleHQpO1xyXG4gIH1cclxufVxyXG4iXX0=