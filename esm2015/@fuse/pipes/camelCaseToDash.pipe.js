import { Pipe } from '@angular/core';
export class CamelCaseToDashPipe {
    /**
     * Transform
     *
     * @param {string} value
     * @param {any[]} args
     * @returns {string}
     */
    transform(value, args = []) {
        return value ? String(value).replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`) : '';
    }
}
CamelCaseToDashPipe.decorators = [
    { type: Pipe, args: [{ name: 'camelCaseToDash' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FtZWxDYXNlVG9EYXNoLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvcGlwZXMvY2FtZWxDYXNlVG9EYXNoLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFHcEQsTUFBTSxPQUFPLG1CQUFtQjtJQUU1Qjs7Ozs7O09BTUc7SUFDSCxTQUFTLENBQUMsS0FBYSxFQUFFLE9BQWMsRUFBRTtRQUVyQyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzNGLENBQUM7OztZQWJKLElBQUksU0FBQyxFQUFDLElBQUksRUFBRSxpQkFBaUIsRUFBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtuYW1lOiAnY2FtZWxDYXNlVG9EYXNoJ30pXHJcbmV4cG9ydCBjbGFzcyBDYW1lbENhc2VUb0Rhc2hQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybVxyXG57XHJcbiAgICAvKipcclxuICAgICAqIFRyYW5zZm9ybVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB2YWx1ZVxyXG4gICAgICogQHBhcmFtIHthbnlbXX0gYXJnc1xyXG4gICAgICogQHJldHVybnMge3N0cmluZ31cclxuICAgICAqL1xyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBzdHJpbmcsIGFyZ3M6IGFueVtdID0gW10pOiBzdHJpbmdcclxuICAgIHtcclxuICAgICAgICByZXR1cm4gdmFsdWUgPyBTdHJpbmcodmFsdWUpLnJlcGxhY2UoLyhbQS1aXSkvZywgKGcpID0+IGAtJHtnWzBdLnRvTG93ZXJDYXNlKCl9YCkgOiAnJztcclxuICAgIH1cclxufVxyXG4iXX0=