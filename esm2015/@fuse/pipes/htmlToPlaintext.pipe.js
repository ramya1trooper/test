import { Pipe } from '@angular/core';
export class HtmlToPlaintextPipe {
    /**
     * Transform
     *
     * @param {string} value
     * @param {any[]} args
     * @returns {string}
     */
    transform(value, args = []) {
        return value ? String(value).replace(/<[^>]+>/gm, '') : '';
    }
}
HtmlToPlaintextPipe.decorators = [
    { type: Pipe, args: [{ name: 'htmlToPlaintext' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHRtbFRvUGxhaW50ZXh0LnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvcGlwZXMvaHRtbFRvUGxhaW50ZXh0LnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFHcEQsTUFBTSxPQUFPLG1CQUFtQjtJQUU1Qjs7Ozs7O09BTUc7SUFDSCxTQUFTLENBQUMsS0FBYSxFQUFFLE9BQWMsRUFBRTtRQUVyQyxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUMvRCxDQUFDOzs7WUFiSixJQUFJLFNBQUMsRUFBQyxJQUFJLEVBQUUsaUJBQWlCLEVBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7bmFtZTogJ2h0bWxUb1BsYWludGV4dCd9KVxyXG5leHBvcnQgY2xhc3MgSHRtbFRvUGxhaW50ZXh0UGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm1cclxue1xyXG4gICAgLyoqXHJcbiAgICAgKiBUcmFuc2Zvcm1cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdmFsdWVcclxuICAgICAqIEBwYXJhbSB7YW55W119IGFyZ3NcclxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICAgKi9cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogc3RyaW5nLCBhcmdzOiBhbnlbXSA9IFtdKTogc3RyaW5nXHJcbiAgICB7XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlID8gU3RyaW5nKHZhbHVlKS5yZXBsYWNlKC88W14+XSs+L2dtLCAnJykgOiAnJztcclxuICAgIH1cclxufVxyXG4iXX0=