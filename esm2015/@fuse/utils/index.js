// @dynamic
export class FuseUtils {
    /**
     * Filter array by string
     *
     * @param mainArr
     * @param searchText
     * @returns {any}
     */
    static filterArrayByString(mainArr, searchText) {
        if (searchText === "") {
            return mainArr;
        }
        searchText = searchText.toLowerCase();
        return mainArr.filter(itemObj => {
            return this.searchInObj(itemObj, searchText);
        });
    }
    /**
     * Search in object
     *
     * @param itemObj
     * @param searchText
     * @returns {boolean}
     */
    static searchInObj(itemObj, searchText) {
        for (const prop in itemObj) {
            if (!itemObj.hasOwnProperty(prop)) {
                continue;
            }
            const value = itemObj[prop];
            if (typeof value === "string") {
                if (this.searchInString(value, searchText)) {
                    return true;
                }
            }
            else if (Array.isArray(value)) {
                if (this.searchInArray(value, searchText)) {
                    return true;
                }
            }
            if (typeof value === "object") {
                if (this.searchInObj(value, searchText)) {
                    return true;
                }
            }
        }
    }
    /**
     * Search in array
     *
     * @param arr
     * @param searchText
     * @returns {boolean}
     */
    static searchInArray(arr, searchText) {
        for (const value of arr) {
            if (typeof value === "string") {
                if (this.searchInString(value, searchText)) {
                    return true;
                }
            }
            if (typeof value === "object") {
                if (this.searchInObj(value, searchText)) {
                    return true;
                }
            }
        }
    }
    /**
     * Search in string
     *
     * @param value
     * @param searchText
     * @returns {any}
     */
    static searchInString(value, searchText) {
        return value.toLowerCase().includes(searchText);
    }
    /**
     * Generate a unique GUID
     *
     * @returns {string}
     */
    static generateGUID() {
        function S4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return S4() + S4();
    }
    /**
     * Toggle in array
     *
     * @param item
     * @param array
     */
    static toggleInArray(item, array) {
        if (array.indexOf(item) === -1) {
            array.push(item);
        }
        else {
            array.splice(array.indexOf(item), 1);
        }
    }
    /**
     * Handleize
     *
     * @param text
     * @returns {string}
     */
    static handleize(text) {
        return text
            .toString()
            .toLowerCase()
            .replace(/\s+/g, "-") // Replace spaces with -
            .replace(/[^\w\-]+/g, "") // Remove all non-word chars
            .replace(/\-\-+/g, "-") // Replace multiple - with single -
            .replace(/^-+/, "") // Trim - from start of text
            .replace(/-+$/, ""); // Trim - from end of text
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvdXRpbHMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsV0FBVztBQUNYLE1BQU0sT0FBTyxTQUFTO0lBQ3BCOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLEVBQUUsVUFBVTtRQUNuRCxJQUFJLFVBQVUsS0FBSyxFQUFFLEVBQUU7WUFDckIsT0FBTyxPQUFPLENBQUM7U0FDaEI7UUFFRCxVQUFVLEdBQUcsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBRXRDLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM5QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFVBQVU7UUFDM0MsS0FBSyxNQUFNLElBQUksSUFBSSxPQUFPLEVBQUU7WUFDMUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2pDLFNBQVM7YUFDVjtZQUVELE1BQU0sS0FBSyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUU1QixJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDN0IsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtvQkFDMUMsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtpQkFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQy9CLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLEVBQUU7b0JBQ3pDLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFFRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtvQkFDdkMsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLFVBQVU7UUFDekMsS0FBSyxNQUFNLEtBQUssSUFBSSxHQUFHLEVBQUU7WUFDdkIsSUFBSSxPQUFPLEtBQUssS0FBSyxRQUFRLEVBQUU7Z0JBQzdCLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLEVBQUU7b0JBQzFDLE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFFRCxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDN0IsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtvQkFDdkMsT0FBTyxJQUFJLENBQUM7aUJBQ2I7YUFDRjtTQUNGO0lBQ0gsQ0FBQztJQUVEOzs7Ozs7T0FNRztJQUNJLE1BQU0sQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLFVBQVU7UUFDNUMsT0FBTyxLQUFLLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2xELENBQUM7SUFFRDs7OztPQUlHO0lBQ0ksTUFBTSxDQUFDLFlBQVk7UUFDeEIsU0FBUyxFQUFFO1lBQ1QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQztpQkFDN0MsUUFBUSxDQUFDLEVBQUUsQ0FBQztpQkFDWixTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDbEIsQ0FBQztRQUVELE9BQU8sRUFBRSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsS0FBSztRQUNyQyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDOUIsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQjthQUFNO1lBQ0wsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0ksTUFBTSxDQUFDLFNBQVMsQ0FBQyxJQUFJO1FBQzFCLE9BQU8sSUFBSTthQUNSLFFBQVEsRUFBRTthQUNWLFdBQVcsRUFBRTthQUNiLE9BQU8sQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUMsd0JBQXdCO2FBQzdDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDLENBQUMsNEJBQTRCO2FBQ3JELE9BQU8sQ0FBQyxRQUFRLEVBQUUsR0FBRyxDQUFDLENBQUMsbUNBQW1DO2FBQzFELE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUMsNEJBQTRCO2FBQy9DLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQywwQkFBMEI7SUFDbkQsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQGR5bmFtaWNcclxuZXhwb3J0IGNsYXNzIEZ1c2VVdGlscyB7XHJcbiAgLyoqXHJcbiAgICogRmlsdGVyIGFycmF5IGJ5IHN0cmluZ1xyXG4gICAqXHJcbiAgICogQHBhcmFtIG1haW5BcnJcclxuICAgKiBAcGFyYW0gc2VhcmNoVGV4dFxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBmaWx0ZXJBcnJheUJ5U3RyaW5nKG1haW5BcnIsIHNlYXJjaFRleHQpOiBhbnkge1xyXG4gICAgaWYgKHNlYXJjaFRleHQgPT09IFwiXCIpIHtcclxuICAgICAgcmV0dXJuIG1haW5BcnI7XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoVGV4dCA9IHNlYXJjaFRleHQudG9Mb3dlckNhc2UoKTtcclxuXHJcbiAgICByZXR1cm4gbWFpbkFyci5maWx0ZXIoaXRlbU9iaiA9PiB7XHJcbiAgICAgIHJldHVybiB0aGlzLnNlYXJjaEluT2JqKGl0ZW1PYmosIHNlYXJjaFRleHQpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZWFyY2ggaW4gb2JqZWN0XHJcbiAgICpcclxuICAgKiBAcGFyYW0gaXRlbU9ialxyXG4gICAqIEBwYXJhbSBzZWFyY2hUZXh0XHJcbiAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBzZWFyY2hJbk9iaihpdGVtT2JqLCBzZWFyY2hUZXh0KTogYm9vbGVhbiB7XHJcbiAgICBmb3IgKGNvbnN0IHByb3AgaW4gaXRlbU9iaikge1xyXG4gICAgICBpZiAoIWl0ZW1PYmouaGFzT3duUHJvcGVydHkocHJvcCkpIHtcclxuICAgICAgICBjb250aW51ZTtcclxuICAgICAgfVxyXG5cclxuICAgICAgY29uc3QgdmFsdWUgPSBpdGVtT2JqW3Byb3BdO1xyXG5cclxuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJzdHJpbmdcIikge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEluU3RyaW5nKHZhbHVlLCBzZWFyY2hUZXh0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKEFycmF5LmlzQXJyYXkodmFsdWUpKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VhcmNoSW5BcnJheSh2YWx1ZSwgc2VhcmNoVGV4dCkpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEluT2JqKHZhbHVlLCBzZWFyY2hUZXh0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZWFyY2ggaW4gYXJyYXlcclxuICAgKlxyXG4gICAqIEBwYXJhbSBhcnJcclxuICAgKiBAcGFyYW0gc2VhcmNoVGV4dFxyXG4gICAqIEByZXR1cm5zIHtib29sZWFufVxyXG4gICAqL1xyXG4gIHB1YmxpYyBzdGF0aWMgc2VhcmNoSW5BcnJheShhcnIsIHNlYXJjaFRleHQpOiBib29sZWFuIHtcclxuICAgIGZvciAoY29uc3QgdmFsdWUgb2YgYXJyKSB7XHJcbiAgICAgIGlmICh0eXBlb2YgdmFsdWUgPT09IFwic3RyaW5nXCIpIHtcclxuICAgICAgICBpZiAodGhpcy5zZWFyY2hJblN0cmluZyh2YWx1ZSwgc2VhcmNoVGV4dCkpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKHR5cGVvZiB2YWx1ZSA9PT0gXCJvYmplY3RcIikge1xyXG4gICAgICAgIGlmICh0aGlzLnNlYXJjaEluT2JqKHZhbHVlLCBzZWFyY2hUZXh0KSkge1xyXG4gICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZWFyY2ggaW4gc3RyaW5nXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdmFsdWVcclxuICAgKiBAcGFyYW0gc2VhcmNoVGV4dFxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBzZWFyY2hJblN0cmluZyh2YWx1ZSwgc2VhcmNoVGV4dCk6IGFueSB7XHJcbiAgICByZXR1cm4gdmFsdWUudG9Mb3dlckNhc2UoKS5pbmNsdWRlcyhzZWFyY2hUZXh0KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdlbmVyYXRlIGEgdW5pcXVlIEdVSURcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBnZW5lcmF0ZUdVSUQoKTogc3RyaW5nIHtcclxuICAgIGZ1bmN0aW9uIFM0KCk6IHN0cmluZyB7XHJcbiAgICAgIHJldHVybiBNYXRoLmZsb29yKCgxICsgTWF0aC5yYW5kb20oKSkgKiAweDEwMDAwKVxyXG4gICAgICAgIC50b1N0cmluZygxNilcclxuICAgICAgICAuc3Vic3RyaW5nKDEpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBTNCgpICsgUzQoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRvZ2dsZSBpbiBhcnJheVxyXG4gICAqXHJcbiAgICogQHBhcmFtIGl0ZW1cclxuICAgKiBAcGFyYW0gYXJyYXlcclxuICAgKi9cclxuICBwdWJsaWMgc3RhdGljIHRvZ2dsZUluQXJyYXkoaXRlbSwgYXJyYXkpOiB2b2lkIHtcclxuICAgIGlmIChhcnJheS5pbmRleE9mKGl0ZW0pID09PSAtMSkge1xyXG4gICAgICBhcnJheS5wdXNoKGl0ZW0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgYXJyYXkuc3BsaWNlKGFycmF5LmluZGV4T2YoaXRlbSksIDEpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogSGFuZGxlaXplXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdGV4dFxyXG4gICAqIEByZXR1cm5zIHtzdHJpbmd9XHJcbiAgICovXHJcbiAgcHVibGljIHN0YXRpYyBoYW5kbGVpemUodGV4dCk6IHN0cmluZyB7XHJcbiAgICByZXR1cm4gdGV4dFxyXG4gICAgICAudG9TdHJpbmcoKVxyXG4gICAgICAudG9Mb3dlckNhc2UoKVxyXG4gICAgICAucmVwbGFjZSgvXFxzKy9nLCBcIi1cIikgLy8gUmVwbGFjZSBzcGFjZXMgd2l0aCAtXHJcbiAgICAgIC5yZXBsYWNlKC9bXlxcd1xcLV0rL2csIFwiXCIpIC8vIFJlbW92ZSBhbGwgbm9uLXdvcmQgY2hhcnNcclxuICAgICAgLnJlcGxhY2UoL1xcLVxcLSsvZywgXCItXCIpIC8vIFJlcGxhY2UgbXVsdGlwbGUgLSB3aXRoIHNpbmdsZSAtXHJcbiAgICAgIC5yZXBsYWNlKC9eLSsvLCBcIlwiKSAvLyBUcmltIC0gZnJvbSBzdGFydCBvZiB0ZXh0XHJcbiAgICAgIC5yZXBsYWNlKC8tKyQvLCBcIlwiKTsgLy8gVHJpbSAtIGZyb20gZW5kIG9mIHRleHRcclxuICB9XHJcbn1cclxuIl19