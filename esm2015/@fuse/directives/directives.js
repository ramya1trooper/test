import { NgModule } from "@angular/core";
import { FuseIfOnDomDirective } from "../../@fuse/directives/fuse-if-on-dom/fuse-if-on-dom.directive";
import { FuseInnerScrollDirective } from "../../@fuse/directives/fuse-inner-scroll/fuse-inner-scroll.directive";
import { FusePerfectScrollbarDirective } from "../../@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";
import { FuseMatSidenavHelperDirective } from "../directives/fuse-mat-sidenav/fuse-mat-sidenav.directive";
import { FuseMatSidenavTogglerDirective } from '../directives/fuse-mat-sidenav/fuse-mat-sidenav.directive';
import { FileDragNDropDirective } from "../directives/file-drag-n-drop/file-drag-n-drop.directive";
export class FuseDirectivesModule {
}
FuseDirectivesModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    FuseIfOnDomDirective,
                    FuseInnerScrollDirective,
                    FuseMatSidenavHelperDirective,
                    FuseMatSidenavTogglerDirective,
                    FusePerfectScrollbarDirective,
                    FileDragNDropDirective
                ],
                imports: [],
                exports: [
                    FuseIfOnDomDirective,
                    FuseInnerScrollDirective,
                    FuseMatSidenavHelperDirective,
                    FuseMatSidenavTogglerDirective,
                    FusePerfectScrollbarDirective,
                    FileDragNDropDirective
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlyZWN0aXZlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9kaXJlY3RpdmVzL2RpcmVjdGl2ZXMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnRUFBZ0UsQ0FBQztBQUN0RyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxzRUFBc0UsQ0FBQztBQUNoSCxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxnRkFBZ0YsQ0FBQztBQUMvSCxPQUFPLEVBQ0wsNkJBQTZCLEVBQzlCLE1BQU0sMkRBQTJELENBQUM7QUFDbkUsT0FBTyxFQUFDLDhCQUE4QixFQUFDLE1BQU0sMkRBQTJELENBQUE7QUFDeEcsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sMkRBQTJELENBQUE7QUFxQmxHLE1BQU0sT0FBTyxvQkFBb0I7OztZQW5CaEMsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRTtvQkFDWixvQkFBb0I7b0JBQ3BCLHdCQUF3QjtvQkFDeEIsNkJBQTZCO29CQUM3Qiw4QkFBOEI7b0JBQzlCLDZCQUE2QjtvQkFDN0Isc0JBQXNCO2lCQUN2QjtnQkFDRCxPQUFPLEVBQUUsRUFBRTtnQkFDWCxPQUFPLEVBQUU7b0JBQ1Asb0JBQW9CO29CQUNwQix3QkFBd0I7b0JBQ3hCLDZCQUE2QjtvQkFDN0IsOEJBQThCO29CQUM5Qiw2QkFBNkI7b0JBQzdCLHNCQUFzQjtpQkFDdkI7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VJZk9uRG9tRGlyZWN0aXZlIH0gZnJvbSBcIi4uLy4uL0BmdXNlL2RpcmVjdGl2ZXMvZnVzZS1pZi1vbi1kb20vZnVzZS1pZi1vbi1kb20uZGlyZWN0aXZlXCI7XHJcbmltcG9ydCB7IEZ1c2VJbm5lclNjcm9sbERpcmVjdGl2ZSB9IGZyb20gXCIuLi8uLi9AZnVzZS9kaXJlY3RpdmVzL2Z1c2UtaW5uZXItc2Nyb2xsL2Z1c2UtaW5uZXItc2Nyb2xsLmRpcmVjdGl2ZVwiO1xyXG5pbXBvcnQgeyBGdXNlUGVyZmVjdFNjcm9sbGJhckRpcmVjdGl2ZSB9IGZyb20gXCIuLi8uLi9AZnVzZS9kaXJlY3RpdmVzL2Z1c2UtcGVyZmVjdC1zY3JvbGxiYXIvZnVzZS1wZXJmZWN0LXNjcm9sbGJhci5kaXJlY3RpdmVcIjtcclxuaW1wb3J0IHtcclxuICBGdXNlTWF0U2lkZW5hdkhlbHBlckRpcmVjdGl2ZVxyXG59IGZyb20gXCIuLi9kaXJlY3RpdmVzL2Z1c2UtbWF0LXNpZGVuYXYvZnVzZS1tYXQtc2lkZW5hdi5kaXJlY3RpdmVcIjtcclxuaW1wb3J0IHtGdXNlTWF0U2lkZW5hdlRvZ2dsZXJEaXJlY3RpdmV9IGZyb20gJy4uL2RpcmVjdGl2ZXMvZnVzZS1tYXQtc2lkZW5hdi9mdXNlLW1hdC1zaWRlbmF2LmRpcmVjdGl2ZSdcclxuaW1wb3J0IHsgRmlsZURyYWdORHJvcERpcmVjdGl2ZSB9IGZyb20gXCIuLi9kaXJlY3RpdmVzL2ZpbGUtZHJhZy1uLWRyb3AvZmlsZS1kcmFnLW4tZHJvcC5kaXJlY3RpdmVcIlxyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEZ1c2VJZk9uRG9tRGlyZWN0aXZlLFxyXG4gICAgRnVzZUlubmVyU2Nyb2xsRGlyZWN0aXZlLFxyXG4gICAgRnVzZU1hdFNpZGVuYXZIZWxwZXJEaXJlY3RpdmUsXHJcbiAgICBGdXNlTWF0U2lkZW5hdlRvZ2dsZXJEaXJlY3RpdmUsXHJcbiAgICBGdXNlUGVyZmVjdFNjcm9sbGJhckRpcmVjdGl2ZSxcclxuICAgIEZpbGVEcmFnTkRyb3BEaXJlY3RpdmVcclxuICBdLFxyXG4gIGltcG9ydHM6IFtdLFxyXG4gIGV4cG9ydHM6IFtcclxuICAgIEZ1c2VJZk9uRG9tRGlyZWN0aXZlLFxyXG4gICAgRnVzZUlubmVyU2Nyb2xsRGlyZWN0aXZlLFxyXG4gICAgRnVzZU1hdFNpZGVuYXZIZWxwZXJEaXJlY3RpdmUsXHJcbiAgICBGdXNlTWF0U2lkZW5hdlRvZ2dsZXJEaXJlY3RpdmUsXHJcbiAgICBGdXNlUGVyZmVjdFNjcm9sbGJhckRpcmVjdGl2ZSxcclxuICAgIEZpbGVEcmFnTkRyb3BEaXJlY3RpdmVcclxuICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlRGlyZWN0aXZlc01vZHVsZSB7fVxyXG4iXX0=