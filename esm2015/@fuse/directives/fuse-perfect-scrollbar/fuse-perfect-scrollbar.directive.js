import { Directive, ElementRef, HostListener, Input } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { Platform } from "@angular/cdk/platform";
import { Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import PerfectScrollbar from "perfect-scrollbar";
import * as _ from "lodash";
import { FuseConfigService } from "../../../@fuse/services/config.service";
export class FusePerfectScrollbarDirective {
    /**
     * Constructor
     *
     * @param {ElementRef} elementRef
     * @param {FuseConfigService} _fuseConfigService
     * @param {Platform} _platform
     * @param {Router} _router
     */
    constructor(elementRef, _fuseConfigService, _platform, _router) {
        this.elementRef = elementRef;
        this._fuseConfigService = _fuseConfigService;
        this._platform = _platform;
        this._router = _router;
        // Set the defaults
        this.isInitialized = false;
        this.isMobile = false;
        // Set the private defaults
        this._enabled = false;
        this._debouncedUpdate = _.debounce(this.update, 150);
        this._options = {
            updateOnRouteChange: false
        };
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Perfect Scrollbar options
     *
     * @param value
     */
    set fusePerfectScrollbarOptions(value) {
        // Merge the options
        this._options = _.merge({}, this._options, value);
    }
    get fusePerfectScrollbarOptions() {
        // Return the options
        return this._options;
    }
    /**
     * Is enabled
     *
     * @param {boolean | ""} value
     */
    set enabled(value) {
        // If nothing is provided with the directive (empty string),
        // we will take that as a true
        if (value === "") {
            value = true;
        }
        // Return, if both values are the same
        if (this.enabled === value) {
            return;
        }
        // Store the value
        this._enabled = value;
        // If enabled...
        if (this.enabled) {
            // Init the directive
            this._init();
        }
        else {
            // Otherwise destroy it
            this._destroy();
        }
    }
    get enabled() {
        // Return the enabled status
        return this._enabled;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * After view init
     */
    ngAfterViewInit() {
        // Check if scrollbars enabled or not from the main config
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(settings => {
            this.enabled = settings.customScrollbars;
        });
        // Scroll to the top on every route change
        if (this.fusePerfectScrollbarOptions.updateOnRouteChange) {
            this._router.events
                .pipe(takeUntil(this._unsubscribeAll), filter(event => event instanceof NavigationEnd))
                .subscribe(() => {
                setTimeout(() => {
                    this.scrollToTop();
                    this.update();
                }, 0);
            });
        }
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        this._destroy();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Initialize
     *
     * @private
     */
    _init() {
        // Return, if already initialized
        if (this.isInitialized) {
            return;
        }
        // Check if is mobile
        if (this._platform.ANDROID || this._platform.IOS) {
            this.isMobile = true;
        }
        // Return if it's mobile
        if (this.isMobile) {
            // Return...
            return;
        }
        // Set as initialized
        this.isInitialized = true;
        // Initialize the perfect-scrollbar
        this.ps = new PerfectScrollbar(this.elementRef.nativeElement, Object.assign({}, this.fusePerfectScrollbarOptions));
        // Unbind 'keydown' events of PerfectScrollbar since it causes an extremely
        // high CPU usage on Angular Material inputs.
        // Loop through all the event elements of this PerfectScrollbar instance
        this.ps.event.eventElements.forEach(eventElement => {
            // If we hit to the element with a 'keydown' event...
            if (typeof eventElement.handlers["keydown"] !== "undefined") {
                // Unbind it
                eventElement.element.removeEventListener("keydown", eventElement.handlers["keydown"][0]);
            }
        });
    }
    /**
     * Destroy
     *
     * @private
     */
    _destroy() {
        if (!this.isInitialized || !this.ps) {
            return;
        }
        // Destroy the perfect-scrollbar
        this.ps.destroy();
        // Clean up
        this.ps = null;
        this.isInitialized = false;
    }
    /**
     * Update scrollbars on window resize
     *
     * @private
     */
    _updateOnResize() {
        this._debouncedUpdate();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Document click
     *
     * @param {Event} event
     */
    documentClick(event) {
        if (!this.isInitialized || !this.ps) {
            return;
        }
        // Update the scrollbar on document click..
        // This isn't the most elegant solution but there is no other way
        // of knowing when the contents of the scrollable container changes.
        // Therefore, we update scrollbars on every document click.
        this.ps.update();
    }
    /**
     * Update the scrollbar
     */
    update() {
        if (!this.isInitialized) {
            return;
        }
        // Update the perfect-scrollbar
        this.ps.update();
    }
    /**
     * Destroy the scrollbar
     */
    destroy() {
        this.ngOnDestroy();
    }
    /**
     * Scroll to X
     *
     * @param {number} x
     * @param {number} speed
     */
    scrollToX(x, speed) {
        this.animateScrolling("scrollLeft", x, speed);
    }
    /**
     * Scroll to Y
     *
     * @param {number} y
     * @param {number} speed
     */
    scrollToY(y, speed) {
        this.animateScrolling("scrollTop", y, speed);
    }
    /**
     * Scroll to top
     *
     * @param {number} offset
     * @param {number} speed
     */
    scrollToTop(offset, speed) {
        this.animateScrolling("scrollTop", offset || 0, speed);
    }
    /**
     * Scroll to left
     *
     * @param {number} offset
     * @param {number} speed
     */
    scrollToLeft(offset, speed) {
        this.animateScrolling("scrollLeft", offset || 0, speed);
    }
    /**
     * Scroll to right
     *
     * @param {number} offset
     * @param {number} speed
     */
    scrollToRight(offset, speed) {
        const width = this.elementRef.nativeElement.scrollWidth;
        this.animateScrolling("scrollLeft", width - (offset || 0), speed);
    }
    /**
     * Scroll to bottom
     *
     * @param {number} offset
     * @param {number} speed
     */
    scrollToBottom(offset, speed) {
        const height = this.elementRef.nativeElement.scrollHeight;
        this.animateScrolling("scrollTop", height - (offset || 0), speed);
    }
    /**
     * Animate scrolling
     *
     * @param {string} target
     * @param {number} value
     * @param {number} speed
     */
    animateScrolling(target, value, speed) {
        if (!speed) {
            this.elementRef.nativeElement[target] = value;
            // PS has weird event sending order, this is a workaround for that
            this.update();
            this.update();
        }
        else if (value !== this.elementRef.nativeElement[target]) {
            let newValue = 0;
            let scrollCount = 0;
            let oldTimestamp = performance.now();
            let oldValue = this.elementRef.nativeElement[target];
            const cosParameter = (oldValue - value) / 2;
            const step = newTimestamp => {
                scrollCount += Math.PI / (speed / (newTimestamp - oldTimestamp));
                newValue = Math.round(value + cosParameter + cosParameter * Math.cos(scrollCount));
                // Only continue animation if scroll position has not changed
                if (this.elementRef.nativeElement[target] === oldValue) {
                    if (scrollCount >= Math.PI) {
                        this.elementRef.nativeElement[target] = value;
                        // PS has weird event sending order, this is a workaround for that
                        this.update();
                        this.update();
                    }
                    else {
                        this.elementRef.nativeElement[target] = oldValue = newValue;
                        oldTimestamp = newTimestamp;
                        window.requestAnimationFrame(step);
                    }
                }
            };
            window.requestAnimationFrame(step);
        }
    }
}
FusePerfectScrollbarDirective.decorators = [
    { type: Directive, args: [{
                selector: "[fusePerfectScrollbar]"
            },] }
];
/** @nocollapse */
FusePerfectScrollbarDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: FuseConfigService },
    { type: Platform },
    { type: Router }
];
FusePerfectScrollbarDirective.propDecorators = {
    fusePerfectScrollbarOptions: [{ type: Input }],
    enabled: [{ type: Input, args: ["fusePerfectScrollbar",] }],
    _updateOnResize: [{ type: HostListener, args: ["window:resize",] }],
    documentClick: [{ type: HostListener, args: ["document:click", ["$event"],] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVzZS1wZXJmZWN0LXNjcm9sbGJhci5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvZGlyZWN0aXZlcy9mdXNlLXBlcmZlY3Qtc2Nyb2xsYmFyL2Z1c2UtcGVyZmVjdC1zY3JvbGxiYXIuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFFTCxTQUFTLEVBQ1QsVUFBVSxFQUNWLFlBQVksRUFDWixLQUFLLEVBRU4sTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDakQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEVBQUUsTUFBTSxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ25ELE9BQU8sZ0JBQWdCLE1BQU0sbUJBQW1CLENBQUM7QUFDakQsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFFNUIsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFLM0UsTUFBTSxPQUFPLDZCQUE2QjtJQVd4Qzs7Ozs7OztPQU9HO0lBQ0gsWUFDUyxVQUFzQixFQUNyQixrQkFBcUMsRUFDckMsU0FBbUIsRUFDbkIsT0FBZTtRQUhoQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3JCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBbUI7UUFDckMsY0FBUyxHQUFULFNBQVMsQ0FBVTtRQUNuQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBRXZCLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUV0QiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNyRCxJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ2QsbUJBQW1CLEVBQUUsS0FBSztTQUMzQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBQ3ZDLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsY0FBYztJQUNkLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0gsSUFDSSwyQkFBMkIsQ0FBQyxLQUFLO1FBQ25DLG9CQUFvQjtRQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEQsQ0FBQztJQUVELElBQUksMkJBQTJCO1FBQzdCLHFCQUFxQjtRQUNyQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxJQUNJLE9BQU8sQ0FBQyxLQUFtQjtRQUM3Qiw0REFBNEQ7UUFDNUQsOEJBQThCO1FBQzlCLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUNoQixLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQ2Q7UUFFRCxzQ0FBc0M7UUFDdEMsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLEtBQUssRUFBRTtZQUMxQixPQUFPO1NBQ1I7UUFFRCxrQkFBa0I7UUFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFFdEIsZ0JBQWdCO1FBQ2hCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQixxQkFBcUI7WUFDckIsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2Q7YUFBTTtZQUNMLHVCQUF1QjtZQUN2QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7SUFDSCxDQUFDO0lBRUQsSUFBSSxPQUFPO1FBQ1QsNEJBQTRCO1FBQzVCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN2QixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxlQUFlO1FBQ2IsMERBQTBEO1FBQzFELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNO2FBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztRQUMzQyxDQUFDLENBQUMsQ0FBQztRQUVMLDBDQUEwQztRQUMxQyxJQUFJLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxtQkFBbUIsRUFBRTtZQUN4RCxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07aUJBQ2hCLElBQUksQ0FDSCxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxFQUMvQixNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxDQUFDLENBQ2hEO2lCQUNBLFNBQVMsQ0FBQyxHQUFHLEVBQUU7Z0JBQ2QsVUFBVSxDQUFDLEdBQUcsRUFBRTtvQkFDZCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ25CLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDaEIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFFRDs7T0FFRztJQUNILFdBQVc7UUFDVCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFaEIscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILEtBQUs7UUFDSCxpQ0FBaUM7UUFDakMsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3RCLE9BQU87U0FDUjtRQUVELHFCQUFxQjtRQUNyQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2hELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO1FBRUQsd0JBQXdCO1FBQ3hCLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixZQUFZO1lBQ1osT0FBTztTQUNSO1FBRUQscUJBQXFCO1FBQ3JCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBRTFCLG1DQUFtQztRQUNuQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLG9CQUN2RCxJQUFJLENBQUMsMkJBQTJCLEVBQ25DLENBQUM7UUFFSCwyRUFBMkU7UUFDM0UsNkNBQTZDO1FBQzdDLHdFQUF3RTtRQUN4RSxJQUFJLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ2pELHFEQUFxRDtZQUNyRCxJQUFJLE9BQU8sWUFBWSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxXQUFXLEVBQUU7Z0JBQzNELFlBQVk7Z0JBQ1osWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FDdEMsU0FBUyxFQUNULFlBQVksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQ3BDLENBQUM7YUFDSDtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ25DLE9BQU87U0FDUjtRQUVELGdDQUFnQztRQUNoQyxJQUFJLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBRWxCLFdBQVc7UUFDWCxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQztRQUNmLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFFRDs7OztPQUlHO0lBRUgsZUFBZTtRQUNiLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsbUJBQW1CO0lBQ25CLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBRUgsYUFBYSxDQUFDLEtBQVk7UUFDeEIsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFO1lBQ25DLE9BQU87U0FDUjtRQUVELDJDQUEyQztRQUMzQyxpRUFBaUU7UUFDakUsb0VBQW9FO1FBQ3BFLDJEQUEyRDtRQUMzRCxJQUFJLENBQUMsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRDs7T0FFRztJQUNILE1BQU07UUFDSixJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN2QixPQUFPO1NBQ1I7UUFFRCwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxPQUFPO1FBQ0wsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILFNBQVMsQ0FBQyxDQUFTLEVBQUUsS0FBYztRQUNqQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxTQUFTLENBQUMsQ0FBUyxFQUFFLEtBQWM7UUFDakMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0MsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsV0FBVyxDQUFDLE1BQWUsRUFBRSxLQUFjO1FBQ3pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsTUFBTSxJQUFJLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxZQUFZLENBQUMsTUFBZSxFQUFFLEtBQWM7UUFDMUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxNQUFNLElBQUksQ0FBQyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFELENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILGFBQWEsQ0FBQyxNQUFlLEVBQUUsS0FBYztRQUMzQyxNQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUM7UUFFeEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxLQUFLLEdBQUcsQ0FBQyxNQUFNLElBQUksQ0FBQyxDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDcEUsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsY0FBYyxDQUFDLE1BQWUsRUFBRSxLQUFjO1FBQzVDLE1BQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUUxRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLE1BQU0sR0FBRyxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0gsZ0JBQWdCLENBQUMsTUFBYyxFQUFFLEtBQWEsRUFBRSxLQUFjO1FBQzVELElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDVixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUM7WUFFOUMsa0VBQWtFO1lBQ2xFLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNkLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNmO2FBQU0sSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEVBQUU7WUFDMUQsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQztZQUVwQixJQUFJLFlBQVksR0FBRyxXQUFXLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDckMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7WUFFckQsTUFBTSxZQUFZLEdBQUcsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBRTVDLE1BQU0sSUFBSSxHQUFHLFlBQVksQ0FBQyxFQUFFO2dCQUMxQixXQUFXLElBQUksSUFBSSxDQUFDLEVBQUUsR0FBRyxDQUFDLEtBQUssR0FBRyxDQUFDLFlBQVksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDO2dCQUVqRSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDbkIsS0FBSyxHQUFHLFlBQVksR0FBRyxZQUFZLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FDNUQsQ0FBQztnQkFFRiw2REFBNkQ7Z0JBQzdELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLEtBQUssUUFBUSxFQUFFO29CQUN0RCxJQUFJLFdBQVcsSUFBSSxJQUFJLENBQUMsRUFBRSxFQUFFO3dCQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLENBQUM7d0JBRTlDLGtFQUFrRTt3QkFDbEUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO3dCQUVkLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztxQkFDZjt5QkFBTTt3QkFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsR0FBRyxRQUFRLEdBQUcsUUFBUSxDQUFDO3dCQUU1RCxZQUFZLEdBQUcsWUFBWSxDQUFDO3dCQUU1QixNQUFNLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3BDO2lCQUNGO1lBQ0gsQ0FBQyxDQUFDO1lBRUYsTUFBTSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQzs7O1lBcFhGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsd0JBQXdCO2FBQ25DOzs7O1lBaEJDLFVBQVU7WUFZSCxpQkFBaUI7WUFOakIsUUFBUTtZQURPLE1BQU07OzswQ0EyRDNCLEtBQUs7c0JBZ0JMLEtBQUssU0FBQyxzQkFBc0I7OEJBaUo1QixZQUFZLFNBQUMsZUFBZTs0QkFjNUIsWUFBWSxTQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBBZnRlclZpZXdJbml0LFxyXG4gIERpcmVjdGl2ZSxcclxuICBFbGVtZW50UmVmLFxyXG4gIEhvc3RMaXN0ZW5lcixcclxuICBJbnB1dCxcclxuICBPbkRlc3Ryb3lcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uRW5kLCBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFBsYXRmb3JtIH0gZnJvbSBcIkBhbmd1bGFyL2Nkay9wbGF0Zm9ybVwiO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgZmlsdGVyLCB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0IFBlcmZlY3RTY3JvbGxiYXIgZnJvbSBcInBlcmZlY3Qtc2Nyb2xsYmFyXCI7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZUNvbmZpZ1NlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2Uvc2VydmljZXMvY29uZmlnLnNlcnZpY2VcIjtcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gIHNlbGVjdG9yOiBcIltmdXNlUGVyZmVjdFNjcm9sbGJhcl1cIlxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZVBlcmZlY3RTY3JvbGxiYXJEaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0LCBPbkRlc3Ryb3kge1xyXG4gIGlzSW5pdGlhbGl6ZWQ6IGJvb2xlYW47XHJcbiAgaXNNb2JpbGU6IGJvb2xlYW47XHJcbiAgcHM6IFBlcmZlY3RTY3JvbGxiYXIgfCBhbnk7XHJcblxyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF9lbmFibGVkOiBib29sZWFuIHwgXCJcIjtcclxuICBwcml2YXRlIF9kZWJvdW5jZWRVcGRhdGU6IGFueTtcclxuICBwcml2YXRlIF9vcHRpb25zOiBhbnk7XHJcbiAgcHJpdmF0ZSBfdW5zdWJzY3JpYmVBbGw6IFN1YmplY3Q8YW55PjtcclxuXHJcbiAgLyoqXHJcbiAgICogQ29uc3RydWN0b3JcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7RWxlbWVudFJlZn0gZWxlbWVudFJlZlxyXG4gICAqIEBwYXJhbSB7RnVzZUNvbmZpZ1NlcnZpY2V9IF9mdXNlQ29uZmlnU2VydmljZVxyXG4gICAqIEBwYXJhbSB7UGxhdGZvcm19IF9wbGF0Zm9ybVxyXG4gICAqIEBwYXJhbSB7Um91dGVyfSBfcm91dGVyXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwdWJsaWMgZWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgIHByaXZhdGUgX2Z1c2VDb25maWdTZXJ2aWNlOiBGdXNlQ29uZmlnU2VydmljZSxcclxuICAgIHByaXZhdGUgX3BsYXRmb3JtOiBQbGF0Zm9ybSxcclxuICAgIHByaXZhdGUgX3JvdXRlcjogUm91dGVyXHJcbiAgKSB7XHJcbiAgICAvLyBTZXQgdGhlIGRlZmF1bHRzXHJcbiAgICB0aGlzLmlzSW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuaXNNb2JpbGUgPSBmYWxzZTtcclxuXHJcbiAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgIHRoaXMuX2VuYWJsZWQgPSBmYWxzZTtcclxuICAgIHRoaXMuX2RlYm91bmNlZFVwZGF0ZSA9IF8uZGVib3VuY2UodGhpcy51cGRhdGUsIDE1MCk7XHJcbiAgICB0aGlzLl9vcHRpb25zID0ge1xyXG4gICAgICB1cGRhdGVPblJvdXRlQ2hhbmdlOiBmYWxzZVxyXG4gICAgfTtcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBBY2Nlc3NvcnNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBQZXJmZWN0IFNjcm9sbGJhciBvcHRpb25zXHJcbiAgICpcclxuICAgKiBAcGFyYW0gdmFsdWVcclxuICAgKi9cclxuICBASW5wdXQoKVxyXG4gIHNldCBmdXNlUGVyZmVjdFNjcm9sbGJhck9wdGlvbnModmFsdWUpIHtcclxuICAgIC8vIE1lcmdlIHRoZSBvcHRpb25zXHJcbiAgICB0aGlzLl9vcHRpb25zID0gXy5tZXJnZSh7fSwgdGhpcy5fb3B0aW9ucywgdmFsdWUpO1xyXG4gIH1cclxuXHJcbiAgZ2V0IGZ1c2VQZXJmZWN0U2Nyb2xsYmFyT3B0aW9ucygpOiBhbnkge1xyXG4gICAgLy8gUmV0dXJuIHRoZSBvcHRpb25zXHJcbiAgICByZXR1cm4gdGhpcy5fb3B0aW9ucztcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIElzIGVuYWJsZWRcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7Ym9vbGVhbiB8IFwiXCJ9IHZhbHVlXHJcbiAgICovXHJcbiAgQElucHV0KFwiZnVzZVBlcmZlY3RTY3JvbGxiYXJcIilcclxuICBzZXQgZW5hYmxlZCh2YWx1ZTogYm9vbGVhbiB8IFwiXCIpIHtcclxuICAgIC8vIElmIG5vdGhpbmcgaXMgcHJvdmlkZWQgd2l0aCB0aGUgZGlyZWN0aXZlIChlbXB0eSBzdHJpbmcpLFxyXG4gICAgLy8gd2Ugd2lsbCB0YWtlIHRoYXQgYXMgYSB0cnVlXHJcbiAgICBpZiAodmFsdWUgPT09IFwiXCIpIHtcclxuICAgICAgdmFsdWUgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJldHVybiwgaWYgYm90aCB2YWx1ZXMgYXJlIHRoZSBzYW1lXHJcbiAgICBpZiAodGhpcy5lbmFibGVkID09PSB2YWx1ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU3RvcmUgdGhlIHZhbHVlXHJcbiAgICB0aGlzLl9lbmFibGVkID0gdmFsdWU7XHJcblxyXG4gICAgLy8gSWYgZW5hYmxlZC4uLlxyXG4gICAgaWYgKHRoaXMuZW5hYmxlZCkge1xyXG4gICAgICAvLyBJbml0IHRoZSBkaXJlY3RpdmVcclxuICAgICAgdGhpcy5faW5pdCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgLy8gT3RoZXJ3aXNlIGRlc3Ryb3kgaXRcclxuICAgICAgdGhpcy5fZGVzdHJveSgpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0IGVuYWJsZWQoKTogYm9vbGVhbiB8IFwiXCIge1xyXG4gICAgLy8gUmV0dXJuIHRoZSBlbmFibGVkIHN0YXR1c1xyXG4gICAgcmV0dXJuIHRoaXMuX2VuYWJsZWQ7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogQWZ0ZXIgdmlldyBpbml0XHJcbiAgICovXHJcbiAgbmdBZnRlclZpZXdJbml0KCk6IHZvaWQge1xyXG4gICAgLy8gQ2hlY2sgaWYgc2Nyb2xsYmFycyBlbmFibGVkIG9yIG5vdCBmcm9tIHRoZSBtYWluIGNvbmZpZ1xyXG4gICAgdGhpcy5fZnVzZUNvbmZpZ1NlcnZpY2UuY29uZmlnXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoc2V0dGluZ3MgPT4ge1xyXG4gICAgICAgIHRoaXMuZW5hYmxlZCA9IHNldHRpbmdzLmN1c3RvbVNjcm9sbGJhcnM7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIFNjcm9sbCB0byB0aGUgdG9wIG9uIGV2ZXJ5IHJvdXRlIGNoYW5nZVxyXG4gICAgaWYgKHRoaXMuZnVzZVBlcmZlY3RTY3JvbGxiYXJPcHRpb25zLnVwZGF0ZU9uUm91dGVDaGFuZ2UpIHtcclxuICAgICAgdGhpcy5fcm91dGVyLmV2ZW50c1xyXG4gICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgdGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSxcclxuICAgICAgICAgIGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpXHJcbiAgICAgICAgKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsVG9Ub3AoKTtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGUoKTtcclxuICAgICAgICAgIH0sIDApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogT24gZGVzdHJveVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgdGhpcy5fZGVzdHJveSgpO1xyXG5cclxuICAgIC8vIFVuc3Vic2NyaWJlIGZyb20gYWxsIHN1YnNjcmlwdGlvbnNcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHJpdmF0ZSBtZXRob2RzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogSW5pdGlhbGl6ZVxyXG4gICAqXHJcbiAgICogQHByaXZhdGVcclxuICAgKi9cclxuICBfaW5pdCgpOiB2b2lkIHtcclxuICAgIC8vIFJldHVybiwgaWYgYWxyZWFkeSBpbml0aWFsaXplZFxyXG4gICAgaWYgKHRoaXMuaXNJbml0aWFsaXplZCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQ2hlY2sgaWYgaXMgbW9iaWxlXHJcbiAgICBpZiAodGhpcy5fcGxhdGZvcm0uQU5EUk9JRCB8fCB0aGlzLl9wbGF0Zm9ybS5JT1MpIHtcclxuICAgICAgdGhpcy5pc01vYmlsZSA9IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gUmV0dXJuIGlmIGl0J3MgbW9iaWxlXHJcbiAgICBpZiAodGhpcy5pc01vYmlsZSkge1xyXG4gICAgICAvLyBSZXR1cm4uLi5cclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFNldCBhcyBpbml0aWFsaXplZFxyXG4gICAgdGhpcy5pc0luaXRpYWxpemVkID0gdHJ1ZTtcclxuXHJcbiAgICAvLyBJbml0aWFsaXplIHRoZSBwZXJmZWN0LXNjcm9sbGJhclxyXG4gICAgdGhpcy5wcyA9IG5ldyBQZXJmZWN0U2Nyb2xsYmFyKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCB7XHJcbiAgICAgIC4uLnRoaXMuZnVzZVBlcmZlY3RTY3JvbGxiYXJPcHRpb25zXHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBVbmJpbmQgJ2tleWRvd24nIGV2ZW50cyBvZiBQZXJmZWN0U2Nyb2xsYmFyIHNpbmNlIGl0IGNhdXNlcyBhbiBleHRyZW1lbHlcclxuICAgIC8vIGhpZ2ggQ1BVIHVzYWdlIG9uIEFuZ3VsYXIgTWF0ZXJpYWwgaW5wdXRzLlxyXG4gICAgLy8gTG9vcCB0aHJvdWdoIGFsbCB0aGUgZXZlbnQgZWxlbWVudHMgb2YgdGhpcyBQZXJmZWN0U2Nyb2xsYmFyIGluc3RhbmNlXHJcbiAgICB0aGlzLnBzLmV2ZW50LmV2ZW50RWxlbWVudHMuZm9yRWFjaChldmVudEVsZW1lbnQgPT4ge1xyXG4gICAgICAvLyBJZiB3ZSBoaXQgdG8gdGhlIGVsZW1lbnQgd2l0aCBhICdrZXlkb3duJyBldmVudC4uLlxyXG4gICAgICBpZiAodHlwZW9mIGV2ZW50RWxlbWVudC5oYW5kbGVyc1tcImtleWRvd25cIl0gIT09IFwidW5kZWZpbmVkXCIpIHtcclxuICAgICAgICAvLyBVbmJpbmQgaXRcclxuICAgICAgICBldmVudEVsZW1lbnQuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKFxyXG4gICAgICAgICAgXCJrZXlkb3duXCIsXHJcbiAgICAgICAgICBldmVudEVsZW1lbnQuaGFuZGxlcnNbXCJrZXlkb3duXCJdWzBdXHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBEZXN0cm95XHJcbiAgICpcclxuICAgKiBAcHJpdmF0ZVxyXG4gICAqL1xyXG4gIF9kZXN0cm95KCk6IHZvaWQge1xyXG4gICAgaWYgKCF0aGlzLmlzSW5pdGlhbGl6ZWQgfHwgIXRoaXMucHMpIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIERlc3Ryb3kgdGhlIHBlcmZlY3Qtc2Nyb2xsYmFyXHJcbiAgICB0aGlzLnBzLmRlc3Ryb3koKTtcclxuXHJcbiAgICAvLyBDbGVhbiB1cFxyXG4gICAgdGhpcy5wcyA9IG51bGw7XHJcbiAgICB0aGlzLmlzSW5pdGlhbGl6ZWQgPSBmYWxzZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFVwZGF0ZSBzY3JvbGxiYXJzIG9uIHdpbmRvdyByZXNpemVcclxuICAgKlxyXG4gICAqIEBwcml2YXRlXHJcbiAgICovXHJcbiAgQEhvc3RMaXN0ZW5lcihcIndpbmRvdzpyZXNpemVcIilcclxuICBfdXBkYXRlT25SZXNpemUoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9kZWJvdW5jZWRVcGRhdGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIERvY3VtZW50IGNsaWNrXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge0V2ZW50fSBldmVudFxyXG4gICAqL1xyXG4gIEBIb3N0TGlzdGVuZXIoXCJkb2N1bWVudDpjbGlja1wiLCBbXCIkZXZlbnRcIl0pXHJcbiAgZG9jdW1lbnRDbGljayhldmVudDogRXZlbnQpOiB2b2lkIHtcclxuICAgIGlmICghdGhpcy5pc0luaXRpYWxpemVkIHx8ICF0aGlzLnBzKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBVcGRhdGUgdGhlIHNjcm9sbGJhciBvbiBkb2N1bWVudCBjbGljay4uXHJcbiAgICAvLyBUaGlzIGlzbid0IHRoZSBtb3N0IGVsZWdhbnQgc29sdXRpb24gYnV0IHRoZXJlIGlzIG5vIG90aGVyIHdheVxyXG4gICAgLy8gb2Yga25vd2luZyB3aGVuIHRoZSBjb250ZW50cyBvZiB0aGUgc2Nyb2xsYWJsZSBjb250YWluZXIgY2hhbmdlcy5cclxuICAgIC8vIFRoZXJlZm9yZSwgd2UgdXBkYXRlIHNjcm9sbGJhcnMgb24gZXZlcnkgZG9jdW1lbnQgY2xpY2suXHJcbiAgICB0aGlzLnBzLnVwZGF0ZSgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogVXBkYXRlIHRoZSBzY3JvbGxiYXJcclxuICAgKi9cclxuICB1cGRhdGUoKTogdm9pZCB7XHJcbiAgICBpZiAoIXRoaXMuaXNJbml0aWFsaXplZCkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVXBkYXRlIHRoZSBwZXJmZWN0LXNjcm9sbGJhclxyXG4gICAgdGhpcy5wcy51cGRhdGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIERlc3Ryb3kgdGhlIHNjcm9sbGJhclxyXG4gICAqL1xyXG4gIGRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICB0aGlzLm5nT25EZXN0cm95KCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTY3JvbGwgdG8gWFxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IHhcclxuICAgKiBAcGFyYW0ge251bWJlcn0gc3BlZWRcclxuICAgKi9cclxuICBzY3JvbGxUb1goeDogbnVtYmVyLCBzcGVlZD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgdGhpcy5hbmltYXRlU2Nyb2xsaW5nKFwic2Nyb2xsTGVmdFwiLCB4LCBzcGVlZCk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTY3JvbGwgdG8gWVxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IHlcclxuICAgKiBAcGFyYW0ge251bWJlcn0gc3BlZWRcclxuICAgKi9cclxuICBzY3JvbGxUb1koeTogbnVtYmVyLCBzcGVlZD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgdGhpcy5hbmltYXRlU2Nyb2xsaW5nKFwic2Nyb2xsVG9wXCIsIHksIHNwZWVkKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNjcm9sbCB0byB0b3BcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBvZmZzZXRcclxuICAgKiBAcGFyYW0ge251bWJlcn0gc3BlZWRcclxuICAgKi9cclxuICBzY3JvbGxUb1RvcChvZmZzZXQ/OiBudW1iZXIsIHNwZWVkPzogbnVtYmVyKTogdm9pZCB7XHJcbiAgICB0aGlzLmFuaW1hdGVTY3JvbGxpbmcoXCJzY3JvbGxUb3BcIiwgb2Zmc2V0IHx8IDAsIHNwZWVkKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNjcm9sbCB0byBsZWZ0XHJcbiAgICpcclxuICAgKiBAcGFyYW0ge251bWJlcn0gb2Zmc2V0XHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IHNwZWVkXHJcbiAgICovXHJcbiAgc2Nyb2xsVG9MZWZ0KG9mZnNldD86IG51bWJlciwgc3BlZWQ/OiBudW1iZXIpOiB2b2lkIHtcclxuICAgIHRoaXMuYW5pbWF0ZVNjcm9sbGluZyhcInNjcm9sbExlZnRcIiwgb2Zmc2V0IHx8IDAsIHNwZWVkKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNjcm9sbCB0byByaWdodFxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IG9mZnNldFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzcGVlZFxyXG4gICAqL1xyXG4gIHNjcm9sbFRvUmlnaHQob2Zmc2V0PzogbnVtYmVyLCBzcGVlZD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgY29uc3Qgd2lkdGggPSB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5zY3JvbGxXaWR0aDtcclxuXHJcbiAgICB0aGlzLmFuaW1hdGVTY3JvbGxpbmcoXCJzY3JvbGxMZWZ0XCIsIHdpZHRoIC0gKG9mZnNldCB8fCAwKSwgc3BlZWQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogU2Nyb2xsIHRvIGJvdHRvbVxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtudW1iZXJ9IG9mZnNldFxyXG4gICAqIEBwYXJhbSB7bnVtYmVyfSBzcGVlZFxyXG4gICAqL1xyXG4gIHNjcm9sbFRvQm90dG9tKG9mZnNldD86IG51bWJlciwgc3BlZWQ/OiBudW1iZXIpOiB2b2lkIHtcclxuICAgIGNvbnN0IGhlaWdodCA9IHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnNjcm9sbEhlaWdodDtcclxuXHJcbiAgICB0aGlzLmFuaW1hdGVTY3JvbGxpbmcoXCJzY3JvbGxUb3BcIiwgaGVpZ2h0IC0gKG9mZnNldCB8fCAwKSwgc3BlZWQpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQW5pbWF0ZSBzY3JvbGxpbmdcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB0YXJnZXRcclxuICAgKiBAcGFyYW0ge251bWJlcn0gdmFsdWVcclxuICAgKiBAcGFyYW0ge251bWJlcn0gc3BlZWRcclxuICAgKi9cclxuICBhbmltYXRlU2Nyb2xsaW5nKHRhcmdldDogc3RyaW5nLCB2YWx1ZTogbnVtYmVyLCBzcGVlZD86IG51bWJlcik6IHZvaWQge1xyXG4gICAgaWYgKCFzcGVlZCkge1xyXG4gICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudFt0YXJnZXRdID0gdmFsdWU7XHJcblxyXG4gICAgICAvLyBQUyBoYXMgd2VpcmQgZXZlbnQgc2VuZGluZyBvcmRlciwgdGhpcyBpcyBhIHdvcmthcm91bmQgZm9yIHRoYXRcclxuICAgICAgdGhpcy51cGRhdGUoKTtcclxuICAgICAgdGhpcy51cGRhdGUoKTtcclxuICAgIH0gZWxzZSBpZiAodmFsdWUgIT09IHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50W3RhcmdldF0pIHtcclxuICAgICAgbGV0IG5ld1ZhbHVlID0gMDtcclxuICAgICAgbGV0IHNjcm9sbENvdW50ID0gMDtcclxuXHJcbiAgICAgIGxldCBvbGRUaW1lc3RhbXAgPSBwZXJmb3JtYW5jZS5ub3coKTtcclxuICAgICAgbGV0IG9sZFZhbHVlID0gdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRbdGFyZ2V0XTtcclxuXHJcbiAgICAgIGNvbnN0IGNvc1BhcmFtZXRlciA9IChvbGRWYWx1ZSAtIHZhbHVlKSAvIDI7XHJcblxyXG4gICAgICBjb25zdCBzdGVwID0gbmV3VGltZXN0YW1wID0+IHtcclxuICAgICAgICBzY3JvbGxDb3VudCArPSBNYXRoLlBJIC8gKHNwZWVkIC8gKG5ld1RpbWVzdGFtcCAtIG9sZFRpbWVzdGFtcCkpO1xyXG5cclxuICAgICAgICBuZXdWYWx1ZSA9IE1hdGgucm91bmQoXHJcbiAgICAgICAgICB2YWx1ZSArIGNvc1BhcmFtZXRlciArIGNvc1BhcmFtZXRlciAqIE1hdGguY29zKHNjcm9sbENvdW50KVxyXG4gICAgICAgICk7XHJcblxyXG4gICAgICAgIC8vIE9ubHkgY29udGludWUgYW5pbWF0aW9uIGlmIHNjcm9sbCBwb3NpdGlvbiBoYXMgbm90IGNoYW5nZWRcclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnRbdGFyZ2V0XSA9PT0gb2xkVmFsdWUpIHtcclxuICAgICAgICAgIGlmIChzY3JvbGxDb3VudCA+PSBNYXRoLlBJKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50W3RhcmdldF0gPSB2YWx1ZTtcclxuXHJcbiAgICAgICAgICAgIC8vIFBTIGhhcyB3ZWlyZCBldmVudCBzZW5kaW5nIG9yZGVyLCB0aGlzIGlzIGEgd29ya2Fyb3VuZCBmb3IgdGhhdFxyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZSgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy51cGRhdGUoKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50W3RhcmdldF0gPSBvbGRWYWx1ZSA9IG5ld1ZhbHVlO1xyXG5cclxuICAgICAgICAgICAgb2xkVGltZXN0YW1wID0gbmV3VGltZXN0YW1wO1xyXG5cclxuICAgICAgICAgICAgd2luZG93LnJlcXVlc3RBbmltYXRpb25GcmFtZShzdGVwKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH07XHJcblxyXG4gICAgICB3aW5kb3cucmVxdWVzdEFuaW1hdGlvbkZyYW1lKHN0ZXApO1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG4iXX0=