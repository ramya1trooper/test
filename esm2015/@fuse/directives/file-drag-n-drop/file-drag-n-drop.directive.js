import { Directive, HostListener, Output, EventEmitter } from '@angular/core';
export class FileDragNDropDirective {
    // @HostBinding('style.background') public background = '#eee';
    // @HostBinding('style.border') public borderStyle = '2px dashed';
    // @HostBinding('style.border-color') public borderColor = '#696D7D';
    // @HostBinding('style.border-radius') public borderRadius = '5px';
    constructor() {
        //@Input() private allowed_extensions : Array<string> = ['png', 'jpg', 'bmp'];
        //@Output() public filesChangeEmiter : EventEmitter<File[]> = new EventEmitter();
        //@Output() private filesInvalidEmiter : EventEmitter<File[]> = new EventEmitter();
        this.onFileDropped = new EventEmitter();
    }
    onDragOver(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = 'lightgray';
        // this.borderColor = 'cadetblue';
        // this.borderStyle = '3px solid';
    }
    onDragLeave(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#eee';
        // this.borderColor = '#696D7D';
        // this.borderStyle = '2px dashed';
    }
    onDrop(evt) {
        evt.preventDefault();
        evt.stopPropagation();
        // this.background = '#eee';
        // this.borderColor = '#696D7D';
        // this.borderStyle = '2px dashed';
        let files = evt.dataTransfer.files;
        // let valid_files : Array<File> = files;
        // this.filesChangeEmiter.emit(valid_files);
        if (files.length > 0) {
            this.onFileDropped.emit(files);
        }
    }
}
FileDragNDropDirective.decorators = [
    { type: Directive, args: [{
                selector: '[fileDragDrop]'
            },] }
];
/** @nocollapse */
FileDragNDropDirective.ctorParameters = () => [];
FileDragNDropDirective.propDecorators = {
    onFileDropped: [{ type: Output }],
    onDragOver: [{ type: HostListener, args: ['dragover', ['$event'],] }],
    onDragLeave: [{ type: HostListener, args: ['dragleave', ['$event'],] }],
    onDrop: [{ type: HostListener, args: ['drop', ['$event'],] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS1kcmFnLW4tZHJvcC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvZGlyZWN0aXZlcy9maWxlLWRyYWctbi1kcm9wL2ZpbGUtZHJhZy1uLWRyb3AuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFlLE1BQU0sRUFBRSxZQUFZLEVBQVMsTUFBTSxlQUFlLENBQUM7QUFNbEcsTUFBTSxPQUFPLHNCQUFzQjtJQU1sQywrREFBK0Q7SUFDL0Qsa0VBQWtFO0lBQ2xFLHFFQUFxRTtJQUNyRSxtRUFBbUU7SUFFbEU7UUFWQSw4RUFBOEU7UUFDNUUsaUZBQWlGO1FBQ25GLG1GQUFtRjtRQUN6RSxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFPLENBQUM7SUFPbEMsQ0FBQztJQUU0QixVQUFVLENBQUMsR0FBRztRQUN6RCxHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDckIsR0FBRyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3RCLGlDQUFpQztRQUNqQyxrQ0FBa0M7UUFDbEMsa0NBQWtDO0lBQ3BDLENBQUM7SUFFNkMsV0FBVyxDQUFDLEdBQUc7UUFDM0QsR0FBRyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3JCLEdBQUcsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN0Qiw0QkFBNEI7UUFDNUIsZ0NBQWdDO1FBQ2hDLG1DQUFtQztJQUNyQyxDQUFDO0lBRXdDLE1BQU0sQ0FBQyxHQUFHO1FBQ2pELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUNyQixHQUFHLENBQUMsZUFBZSxFQUFFLENBQUM7UUFDdEIsNEJBQTRCO1FBQzVCLGdDQUFnQztRQUNoQyxtQ0FBbUM7UUFDbkMsSUFBSSxLQUFLLEdBQUcsR0FBRyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7UUFDbkMseUNBQXlDO1FBQ3pDLDRDQUE0QztRQUM1QyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQy9CO0lBQ0gsQ0FBQzs7O1lBN0NGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZ0JBQWdCO2FBQzNCOzs7Ozs0QkFNRSxNQUFNO3lCQVNOLFlBQVksU0FBQyxVQUFVLEVBQUUsQ0FBQyxRQUFRLENBQUM7MEJBUW5DLFlBQVksU0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUM7cUJBUXBDLFlBQVksU0FBQyxNQUFNLEVBQUUsQ0FBQyxRQUFRLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lciwgSG9zdEJpbmRpbmcsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgc2VsZWN0b3I6ICdbZmlsZURyYWdEcm9wXSdcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlRHJhZ05Ecm9wRGlyZWN0aXZlIHtcclxuICAvL0BJbnB1dCgpIHByaXZhdGUgYWxsb3dlZF9leHRlbnNpb25zIDogQXJyYXk8c3RyaW5nPiA9IFsncG5nJywgJ2pwZycsICdibXAnXTtcclxuICAgIC8vQE91dHB1dCgpIHB1YmxpYyBmaWxlc0NoYW5nZUVtaXRlciA6IEV2ZW50RW1pdHRlcjxGaWxlW10+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gIC8vQE91dHB1dCgpIHByaXZhdGUgZmlsZXNJbnZhbGlkRW1pdGVyIDogRXZlbnRFbWl0dGVyPEZpbGVbXT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgQE91dHB1dCgpIG9uRmlsZURyb3BwZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAvLyBASG9zdEJpbmRpbmcoJ3N0eWxlLmJhY2tncm91bmQnKSBwdWJsaWMgYmFja2dyb3VuZCA9ICcjZWVlJztcclxuIC8vIEBIb3N0QmluZGluZygnc3R5bGUuYm9yZGVyJykgcHVibGljIGJvcmRlclN0eWxlID0gJzJweCBkYXNoZWQnO1xyXG4gLy8gQEhvc3RCaW5kaW5nKCdzdHlsZS5ib3JkZXItY29sb3InKSBwdWJsaWMgYm9yZGVyQ29sb3IgPSAnIzY5NkQ3RCc7XHJcbiAvLyBASG9zdEJpbmRpbmcoJ3N0eWxlLmJvcmRlci1yYWRpdXMnKSBwdWJsaWMgYm9yZGVyUmFkaXVzID0gJzVweCc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKCkgeyB9XHJcblxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RyYWdvdmVyJywgWyckZXZlbnQnXSkgcHVibGljIG9uRHJhZ092ZXIoZXZ0KXtcclxuICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgLy8gdGhpcy5iYWNrZ3JvdW5kID0gJ2xpZ2h0Z3JheSc7XHJcbiAgICAvLyB0aGlzLmJvcmRlckNvbG9yID0gJ2NhZGV0Ymx1ZSc7XHJcbiAgICAvLyB0aGlzLmJvcmRlclN0eWxlID0gJzNweCBzb2xpZCc7XHJcbiAgfVxyXG5cclxuICBASG9zdExpc3RlbmVyKCdkcmFnbGVhdmUnLCBbJyRldmVudCddKSBwdWJsaWMgb25EcmFnTGVhdmUoZXZ0KXtcclxuICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgZXZ0LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgLy8gdGhpcy5iYWNrZ3JvdW5kID0gJyNlZWUnO1xyXG4gICAgLy8gdGhpcy5ib3JkZXJDb2xvciA9ICcjNjk2RDdEJztcclxuICAgIC8vIHRoaXMuYm9yZGVyU3R5bGUgPSAnMnB4IGRhc2hlZCc7XHJcbiAgfVxyXG5cclxuICBASG9zdExpc3RlbmVyKCdkcm9wJywgWyckZXZlbnQnXSkgcHVibGljIG9uRHJvcChldnQpe1xyXG4gICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICBldnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAvLyB0aGlzLmJhY2tncm91bmQgPSAnI2VlZSc7XHJcbiAgICAvLyB0aGlzLmJvcmRlckNvbG9yID0gJyM2OTZEN0QnO1xyXG4gICAgLy8gdGhpcy5ib3JkZXJTdHlsZSA9ICcycHggZGFzaGVkJztcclxuICAgIGxldCBmaWxlcyA9IGV2dC5kYXRhVHJhbnNmZXIuZmlsZXM7XHJcbiAgICAvLyBsZXQgdmFsaWRfZmlsZXMgOiBBcnJheTxGaWxlPiA9IGZpbGVzO1xyXG4gICAgLy8gdGhpcy5maWxlc0NoYW5nZUVtaXRlci5lbWl0KHZhbGlkX2ZpbGVzKTtcclxuICAgIGlmIChmaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgIHRoaXMub25GaWxlRHJvcHBlZC5lbWl0KGZpbGVzKVxyXG4gICAgfVxyXG4gIH1cclxufSJdfQ==