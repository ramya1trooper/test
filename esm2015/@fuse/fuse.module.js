import { AccountService } from "./../shared/auth/account.service";
import { NgModule, Optional, SkipSelf } from "@angular/core";
// import { FUSE_CONFIG } from "./services/config.service";
export class FuseModule {
    constructor(parentModule) {
        if (parentModule) {
            throw new Error("FuseModule is already loaded. Import it in the AppModule only!");
        }
    }
    static forRoot(metaData, english) {
        return {
            ngModule: FuseModule,
            providers: [
                {
                    provide: "metaData",
                    useValue: metaData
                },
                { provide: "english", useValue: english },
                AccountService
            ]
        };
    }
}
FuseModule.decorators = [
    { type: NgModule }
];
/** @nocollapse */
FuseModule.ctorParameters = () => [
    { type: FuseModule, decorators: [{ type: Optional }, { type: SkipSelf }] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVzZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvZnVzZS5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ2xFLE9BQU8sRUFFTCxRQUFRLEVBQ1IsUUFBUSxFQUNSLFFBQVEsRUFDVCxNQUFNLGVBQWUsQ0FBQztBQUV2QiwyREFBMkQ7QUFHM0QsTUFBTSxPQUFPLFVBQVU7SUFDckIsWUFBb0MsWUFBd0I7UUFDMUQsSUFBSSxZQUFZLEVBQUU7WUFDaEIsTUFBTSxJQUFJLEtBQUssQ0FDYixnRUFBZ0UsQ0FDakUsQ0FBQztTQUNIO0lBQ0gsQ0FBQztJQUVELE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLE9BQU87UUFDOUIsT0FBTztZQUNMLFFBQVEsRUFBRSxVQUFVO1lBQ3BCLFNBQVMsRUFBRTtnQkFDVDtvQkFDRSxPQUFPLEVBQUUsVUFBVTtvQkFDbkIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUNELEVBQUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFO2dCQUN6QyxjQUFjO2FBQ2Y7U0FDRixDQUFDO0lBQ0osQ0FBQzs7O1lBdEJGLFFBQVE7Ozs7WUFFMkMsVUFBVSx1QkFBL0MsUUFBUSxZQUFJLFFBQVEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9hdXRoL2FjY291bnQuc2VydmljZVwiO1xyXG5pbXBvcnQge1xyXG4gIE1vZHVsZVdpdGhQcm92aWRlcnMsXHJcbiAgTmdNb2R1bGUsXHJcbiAgT3B0aW9uYWwsXHJcbiAgU2tpcFNlbGZcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuLy8gaW1wb3J0IHsgRlVTRV9DT05GSUcgfSBmcm9tIFwiLi9zZXJ2aWNlcy9jb25maWcuc2VydmljZVwiO1xyXG5cclxuQE5nTW9kdWxlKClcclxuZXhwb3J0IGNsYXNzIEZ1c2VNb2R1bGUge1xyXG4gIGNvbnN0cnVjdG9yKEBPcHRpb25hbCgpIEBTa2lwU2VsZigpIHBhcmVudE1vZHVsZTogRnVzZU1vZHVsZSkge1xyXG4gICAgaWYgKHBhcmVudE1vZHVsZSkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXHJcbiAgICAgICAgXCJGdXNlTW9kdWxlIGlzIGFscmVhZHkgbG9hZGVkLiBJbXBvcnQgaXQgaW4gdGhlIEFwcE1vZHVsZSBvbmx5IVwiXHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBzdGF0aWMgZm9yUm9vdChtZXRhRGF0YSwgZW5nbGlzaCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IEZ1c2VNb2R1bGUsXHJcbiAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IFwibWV0YURhdGFcIixcclxuICAgICAgICAgIHVzZVZhbHVlOiBtZXRhRGF0YVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgeyBwcm92aWRlOiBcImVuZ2xpc2hcIiwgdXNlVmFsdWU6IGVuZ2xpc2ggfSxcclxuICAgICAgICBBY2NvdW50U2VydmljZVxyXG4gICAgICBdXHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=