import { Component, EventEmitter, forwardRef, Input, Output, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '../../../@fuse/animations';
import { MatColors } from '../../../@fuse/mat-colors';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
export const FUSE_MATERIAL_COLOR_PICKER_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => FuseMaterialColorPickerComponent),
    multi: true
};
export class FuseMaterialColorPickerComponent {
    /**
     * Constructor
     */
    constructor() {
        // Set the defaults
        this.colorChanged = new EventEmitter();
        this.colors = MatColors.all;
        this.hues = ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'];
        this.selectedHue = '500';
        this.view = 'palettes';
        // Set the private defaults
        this._color = '';
        this._modelChange = () => {
        };
        this._modelTouched = () => {
        };
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Selected class
     *
     * @param value
     */
    set color(value) {
        if (!value || value === '' || this._color === value) {
            return;
        }
        // Split the color value (red-400, blue-500, fuse-navy-700 etc.)
        const colorParts = value.split('-');
        // Take the very last part as the selected hue value
        this.selectedHue = colorParts[colorParts.length - 1];
        // Remove the last part
        colorParts.pop();
        // Rejoin the remaining parts as the selected palette name
        this.selectedPalette = colorParts.join('-');
        // Store the color value
        this._color = value;
    }
    get color() {
        return this._color;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Control Value Accessor implementation
    // -----------------------------------------------------------------------------------------------------
    /**
     * Register on change function
     *
     * @param fn
     */
    registerOnChange(fn) {
        this._modelChange = fn;
    }
    /**
     * Register on touched function
     *
     * @param fn
     */
    registerOnTouched(fn) {
        this._modelTouched = fn;
    }
    /**
     * Write value to the view from model
     *
     * @param color
     */
    writeValue(color) {
        // Return if null
        if (!color) {
            return;
        }
        // Set the color
        this.color = color;
        // Update the selected color
        this.updateSelectedColor();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Select palette
     *
     * @param event
     * @param palette
     */
    selectPalette(event, palette) {
        // Stop propagation
        event.stopPropagation();
        // Go to 'hues' view
        this.view = 'hues';
        // Update the selected palette
        this.selectedPalette = palette;
        // Update the selected color
        this.updateSelectedColor();
    }
    /**
     * Select hue
     *
     * @param event
     * @param hue
     */
    selectHue(event, hue) {
        // Stop propagation
        event.stopPropagation();
        // Update the selected huse
        this.selectedHue = hue;
        // Update the selected color
        this.updateSelectedColor();
    }
    /**
     * Remove color
     *
     * @param event
     */
    removeColor(event) {
        // Stop propagation
        event.stopPropagation();
        // Return to the 'palettes' view
        this.view = 'palettes';
        // Clear the selected palette and hue
        this.selectedPalette = '';
        this.selectedHue = '';
        // Update the selected color
        this.updateSelectedColor();
    }
    /**
     * Update selected color
     */
    updateSelectedColor() {
        if (this.selectedColor && this.selectedColor.palette === this.selectedPalette && this.selectedColor.hue === this.selectedHue) {
            return;
        }
        // Set the selected color object
        this.selectedColor = {
            palette: this.selectedPalette,
            hue: this.selectedHue,
            class: this.selectedPalette + '-' + this.selectedHue,
            bg: this.selectedPalette === '' ? '' : MatColors.getColor(this.selectedPalette)[this.selectedHue],
            fg: this.selectedPalette === '' ? '' : MatColors.getColor(this.selectedPalette).contrast[this.selectedHue]
        };
        // Emit the color changed event
        this.colorChanged.emit(this.selectedColor);
        // Mark the model as touched
        this._modelTouched(this.selectedColor.class);
        // Update the model
        this._modelChange(this.selectedColor.class);
    }
    /**
     * Go to palettes view
     *
     * @param event
     */
    goToPalettesView(event) {
        // Stop propagation
        event.stopPropagation();
        this.view = 'palettes';
    }
    /**
     * On menu open
     */
    onMenuOpen() {
        if (this.selectedPalette === '') {
            this.view = 'palettes';
        }
        else {
            this.view = 'hues';
        }
    }
}
FuseMaterialColorPickerComponent.decorators = [
    { type: Component, args: [{
                selector: 'fuse-material-color-picker',
                template: "<button mat-icon-button class=\"mat-elevation-z1\" [matMenuTriggerFor]=\"colorMenu\" (menuOpened)=\"onMenuOpen()\"\r\n  [ngClass]=\"selectedPalette + '-' + selectedHue\">\r\n  <mat-icon>palette</mat-icon>\r\n</button>\r\n\r\n<mat-menu #colorMenu=\"matMenu\" class=\"fuse-material-color-picker-menu mat-elevation-z8\">\r\n\r\n  <header [ngClass]=\"selectedColor?.class || 'accent'\" class=\"mat-elevation-z4\" fxLayout=\"row\"\r\n    fxLayoutAlign=\"space-between center\">\r\n\r\n    <button mat-icon-button class=\"secondary-text\" [style.visibility]=\"view === 'hues' ? 'visible' : 'hidden'\"\r\n      (click)=\"goToPalettesView($event)\" aria-label=\"Palette\">\r\n      <mat-icon class=\"s-20\">arrow_back</mat-icon>\r\n    </button>\r\n\r\n    <span *ngIf=\"selectedColor?.palette\">\r\n      {{selectedColor.palette}} {{selectedColor.hue}}\r\n    </span>\r\n\r\n    <span *ngIf=\"!selectedColor?.palette\">\r\n      Select a Color\r\n    </span>\r\n\r\n    <button mat-icon-button class=\"remove-color-button secondary-text\" (click)=\"removeColor($event)\"\r\n      aria-label=\"Remove color\" matTooltip=\"Remove color\">\r\n      <mat-icon class=\"s-20\">delete</mat-icon>\r\n    </button>\r\n  </header>\r\n\r\n  <div [ngSwitch]=\"view\" class=\"views\">\r\n\r\n    <div class=\"view\" *ngSwitchCase=\"'palettes'\">\r\n\r\n      <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\" class=\"colors\" fusePerfectScrollbar>\r\n        <!-- <div class=\"color\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n                     *ngFor=\"let color of (colors | keys)\"\r\n                     [ngClass]=\"color.key\"\r\n                     [class.selected]=\"selectedPalette === color.key\"\r\n                     (click)=\"selectPalette($event, color.key)\">\r\n                </div> -->\r\n      </div>\r\n    </div>\r\n\r\n    <!-- <div class=\"view\" *ngSwitchCase=\"'hues'\">\r\n      <div fxLayout=\"row wrap\" fxLayoutAlign=\"start start\" class=\"colors\" fusePerfectScrollbar>\r\n        <div class=\"color\" fxLayout=\"row\" fxLayoutAlign=\"center center\" *ngFor=\"let hue of hues\"\r\n          [fxHide]=\"selectedPalette === 'fuse-white' && hue !== '500' || selectedPalette === 'fuse-black' && hue !== '500'\"\r\n          [ngClass]=\"selectedPalette + '-' + hue\" [class.selected]=\"selectedHue === hue\"\r\n          (click)=\"selectHue($event, hue)\">\r\n        </div>\r\n      </div>\r\n    </div> -->\r\n\r\n  </div>\r\n</mat-menu>\r\n",
                animations: fuseAnimations,
                encapsulation: ViewEncapsulation.None,
                providers: [FUSE_MATERIAL_COLOR_PICKER_VALUE_ACCESSOR],
                styles: [".fuse-material-color-picker-menu{width:245px}.fuse-material-color-picker-menu .mat-menu-content{padding:0}.fuse-material-color-picker-menu .mat-menu-content .views{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;min-height:165px}.fuse-material-color-picker-menu .mat-menu-content .views .view{overflow:hidden}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors{padding:1px 0 0;margin-left:-1px}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors .color{width:40px;height:40px;margin:0 0 1px 1px;border-radius:0;cursor:pointer;-webkit-transition:border-radius .4s cubic-bezier(.25,.8,.25,1);transition:border-radius .4s cubic-bezier(.25,.8,.25,1)}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors .color:hover{border-radius:20%}.fuse-material-color-picker-menu .mat-menu-content .views .view .colors .color.selected{border-radius:50%!important}"]
            }] }
];
/** @nocollapse */
FuseMaterialColorPickerComponent.ctorParameters = () => [];
FuseMaterialColorPickerComponent.propDecorators = {
    colorChanged: [{ type: Output }],
    color: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY29sb3ItcGlja2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL21hdGVyaWFsLWNvbG9yLXBpY2tlci9tYXRlcmlhbC1jb2xvci1waWNrZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXRHLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDdEQsT0FBTyxFQUF3QixpQkFBaUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXpFLE1BQU0sQ0FBQyxNQUFNLHlDQUF5QyxHQUFRO0lBQzFELE9BQU8sRUFBTSxpQkFBaUI7SUFDOUIsV0FBVyxFQUFFLFVBQVUsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxnQ0FBZ0MsQ0FBQztJQUMvRCxLQUFLLEVBQVEsSUFBSTtDQUNwQixDQUFDO0FBVUYsTUFBTSxPQUFPLGdDQUFnQztJQWtCekM7O09BRUc7SUFDSDtRQUVJLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDO1FBQzVCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDbEgsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxVQUFVLENBQUM7UUFFdkIsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxFQUFFO1FBQ3pCLENBQUMsQ0FBQztRQUNGLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxFQUFFO1FBQzFCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsY0FBYztJQUNkLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0gsSUFDSSxLQUFLLENBQUMsS0FBSztRQUVYLElBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxLQUFLLEtBQUssRUFDcEQ7WUFDSSxPQUFPO1NBQ1Y7UUFFRCxnRUFBZ0U7UUFDaEUsTUFBTSxVQUFVLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUVwQyxvREFBb0Q7UUFDcEQsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztRQUVyRCx1QkFBdUI7UUFDdkIsVUFBVSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBRWpCLDBEQUEwRDtRQUMxRCxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFNUMsd0JBQXdCO1FBQ3hCLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRCxJQUFJLEtBQUs7UUFFTCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQztJQUVELHdHQUF3RztJQUN4RywwQ0FBMEM7SUFDMUMsd0dBQXdHO0lBRXhHOzs7O09BSUc7SUFDSCxnQkFBZ0IsQ0FBQyxFQUFPO1FBRXBCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQzNCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsaUJBQWlCLENBQUMsRUFBTztRQUVyQixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILFVBQVUsQ0FBQyxLQUFVO1FBRWpCLGlCQUFpQjtRQUNqQixJQUFLLENBQUMsS0FBSyxFQUNYO1lBQ0ksT0FBTztTQUNWO1FBRUQsZ0JBQWdCO1FBQ2hCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBRW5CLDRCQUE0QjtRQUM1QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7O09BS0c7SUFDSCxhQUFhLENBQUMsS0FBSyxFQUFFLE9BQU87UUFFeEIsbUJBQW1CO1FBQ25CLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV4QixvQkFBb0I7UUFDcEIsSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7UUFFbkIsOEJBQThCO1FBQzlCLElBQUksQ0FBQyxlQUFlLEdBQUcsT0FBTyxDQUFDO1FBRS9CLDRCQUE0QjtRQUM1QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxTQUFTLENBQUMsS0FBSyxFQUFFLEdBQUc7UUFFaEIsbUJBQW1CO1FBQ25CLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV4QiwyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7UUFFdkIsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsV0FBVyxDQUFDLEtBQUs7UUFFYixtQkFBbUI7UUFDbkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBRXhCLGdDQUFnQztRQUNoQyxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztRQUV2QixxQ0FBcUM7UUFDckMsSUFBSSxDQUFDLGVBQWUsR0FBRyxFQUFFLENBQUM7UUFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7UUFFdEIsNEJBQTRCO1FBQzVCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRDs7T0FFRztJQUNILG1CQUFtQjtRQUVmLElBQUssSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxXQUFXLEVBQzdIO1lBQ0ksT0FBTztTQUNWO1FBRUQsZ0NBQWdDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLEdBQUc7WUFDakIsT0FBTyxFQUFFLElBQUksQ0FBQyxlQUFlO1lBQzdCLEdBQUcsRUFBTSxJQUFJLENBQUMsV0FBVztZQUN6QixLQUFLLEVBQUksSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVc7WUFDdEQsRUFBRSxFQUFPLElBQUksQ0FBQyxlQUFlLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDdEcsRUFBRSxFQUFPLElBQUksQ0FBQyxlQUFlLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQ2xILENBQUM7UUFFRiwrQkFBK0I7UUFDL0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRTNDLDRCQUE0QjtRQUM1QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFN0MsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILGdCQUFnQixDQUFDLEtBQUs7UUFFbEIsbUJBQW1CO1FBQ25CLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztJQUMzQixDQUFDO0lBRUQ7O09BRUc7SUFDSCxVQUFVO1FBRU4sSUFBSyxJQUFJLENBQUMsZUFBZSxLQUFLLEVBQUUsRUFDaEM7WUFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLFVBQVUsQ0FBQztTQUMxQjthQUVEO1lBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7U0FDdEI7SUFDTCxDQUFDOzs7WUF2UEosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBTyw0QkFBNEI7Z0JBQzNDLHE3RUFBdUQ7Z0JBRXZELFVBQVUsRUFBSyxjQUFjO2dCQUM3QixhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsU0FBUyxFQUFNLENBQUMseUNBQXlDLENBQUM7O2FBQzdEOzs7OzsyQkFXSSxNQUFNO29CQXFDTixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIGZvcndhcmRSZWYsIElucHV0LCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBmdXNlQW5pbWF0aW9ucyB9IGZyb20gJy4uLy4uLy4uL0BmdXNlL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBNYXRDb2xvcnMgfSBmcm9tICcuLi8uLi8uLi9AZnVzZS9tYXQtY29sb3JzJztcclxuaW1wb3J0IHsgQ29udHJvbFZhbHVlQWNjZXNzb3IsIE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IEZVU0VfTUFURVJJQUxfQ09MT1JfUElDS0VSX1ZBTFVFX0FDQ0VTU09SOiBhbnkgPSB7XHJcbiAgICBwcm92aWRlICAgIDogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBGdXNlTWF0ZXJpYWxDb2xvclBpY2tlckNvbXBvbmVudCksXHJcbiAgICBtdWx0aSAgICAgIDogdHJ1ZVxyXG59O1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvciAgICAgOiAnZnVzZS1tYXRlcmlhbC1jb2xvci1waWNrZXInLFxyXG4gICAgdGVtcGxhdGVVcmwgIDogJy4vbWF0ZXJpYWwtY29sb3ItcGlja2VyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJscyAgICA6IFsnLi9tYXRlcmlhbC1jb2xvci1waWNrZXIuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGFuaW1hdGlvbnMgICA6IGZ1c2VBbmltYXRpb25zLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIHByb3ZpZGVycyAgICA6IFtGVVNFX01BVEVSSUFMX0NPTE9SX1BJQ0tFUl9WQUxVRV9BQ0NFU1NPUl1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VNYXRlcmlhbENvbG9yUGlja2VyQ29tcG9uZW50IGltcGxlbWVudHMgQ29udHJvbFZhbHVlQWNjZXNzb3Jcclxue1xyXG4gICAgY29sb3JzOiBhbnk7XHJcbiAgICBodWVzOiBzdHJpbmdbXTtcclxuICAgIHZpZXc6IHN0cmluZztcclxuICAgIHNlbGVjdGVkQ29sb3I6IGFueTtcclxuICAgIHNlbGVjdGVkUGFsZXR0ZTogc3RyaW5nO1xyXG4gICAgc2VsZWN0ZWRIdWU6IHN0cmluZztcclxuXHJcbiAgICAvLyBDb2xvciBjaGFuZ2VkXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNvbG9yQ2hhbmdlZDogRXZlbnRFbWl0dGVyPGFueT47XHJcblxyXG4gICAgLy8gUHJpdmF0ZVxyXG4gICAgcHJpdmF0ZSBfY29sb3I6IHN0cmluZztcclxuICAgIHByaXZhdGUgX21vZGVsQ2hhbmdlOiAodmFsdWU6IGFueSkgPT4gdm9pZDtcclxuICAgIHByaXZhdGUgX21vZGVsVG91Y2hlZDogKHZhbHVlOiBhbnkpID0+IHZvaWQ7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcigpXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgICAgIHRoaXMuY29sb3JDaGFuZ2VkID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG4gICAgICAgIHRoaXMuY29sb3JzID0gTWF0Q29sb3JzLmFsbDtcclxuICAgICAgICB0aGlzLmh1ZXMgPSBbJzUwJywgJzEwMCcsICcyMDAnLCAnMzAwJywgJzQwMCcsICc1MDAnLCAnNjAwJywgJzcwMCcsICc4MDAnLCAnOTAwJywgJ0ExMDAnLCAnQTIwMCcsICdBNDAwJywgJ0E3MDAnXTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkSHVlID0gJzUwMCc7XHJcbiAgICAgICAgdGhpcy52aWV3ID0gJ3BhbGV0dGVzJztcclxuXHJcbiAgICAgICAgLy8gU2V0IHRoZSBwcml2YXRlIGRlZmF1bHRzXHJcbiAgICAgICAgdGhpcy5fY29sb3IgPSAnJztcclxuICAgICAgICB0aGlzLl9tb2RlbENoYW5nZSA9ICgpID0+IHtcclxuICAgICAgICB9O1xyXG4gICAgICAgIHRoaXMuX21vZGVsVG91Y2hlZCA9ICgpID0+IHtcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIEFjY2Vzc29yc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlbGVjdGVkIGNsYXNzXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHZhbHVlXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzZXQgY29sb3IodmFsdWUpXHJcbiAgICB7XHJcbiAgICAgICAgaWYgKCAhdmFsdWUgfHwgdmFsdWUgPT09ICcnIHx8IHRoaXMuX2NvbG9yID09PSB2YWx1ZSApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTcGxpdCB0aGUgY29sb3IgdmFsdWUgKHJlZC00MDAsIGJsdWUtNTAwLCBmdXNlLW5hdnktNzAwIGV0Yy4pXHJcbiAgICAgICAgY29uc3QgY29sb3JQYXJ0cyA9IHZhbHVlLnNwbGl0KCctJyk7XHJcblxyXG4gICAgICAgIC8vIFRha2UgdGhlIHZlcnkgbGFzdCBwYXJ0IGFzIHRoZSBzZWxlY3RlZCBodWUgdmFsdWVcclxuICAgICAgICB0aGlzLnNlbGVjdGVkSHVlID0gY29sb3JQYXJ0c1tjb2xvclBhcnRzLmxlbmd0aCAtIDFdO1xyXG5cclxuICAgICAgICAvLyBSZW1vdmUgdGhlIGxhc3QgcGFydFxyXG4gICAgICAgIGNvbG9yUGFydHMucG9wKCk7XHJcblxyXG4gICAgICAgIC8vIFJlam9pbiB0aGUgcmVtYWluaW5nIHBhcnRzIGFzIHRoZSBzZWxlY3RlZCBwYWxldHRlIG5hbWVcclxuICAgICAgICB0aGlzLnNlbGVjdGVkUGFsZXR0ZSA9IGNvbG9yUGFydHMuam9pbignLScpO1xyXG5cclxuICAgICAgICAvLyBTdG9yZSB0aGUgY29sb3IgdmFsdWVcclxuICAgICAgICB0aGlzLl9jb2xvciA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb2xvcigpOiBzdHJpbmdcclxuICAgIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29sb3I7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgQ29udHJvbCBWYWx1ZSBBY2Nlc3NvciBpbXBsZW1lbnRhdGlvblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlZ2lzdGVyIG9uIGNoYW5nZSBmdW5jdGlvblxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBmblxyXG4gICAgICovXHJcbiAgICByZWdpc3Rlck9uQ2hhbmdlKGZuOiBhbnkpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgdGhpcy5fbW9kZWxDaGFuZ2UgPSBmbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlZ2lzdGVyIG9uIHRvdWNoZWQgZnVuY3Rpb25cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZm5cclxuICAgICAqL1xyXG4gICAgcmVnaXN0ZXJPblRvdWNoZWQoZm46IGFueSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLl9tb2RlbFRvdWNoZWQgPSBmbjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFdyaXRlIHZhbHVlIHRvIHRoZSB2aWV3IGZyb20gbW9kZWxcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gY29sb3JcclxuICAgICAqL1xyXG4gICAgd3JpdGVWYWx1ZShjb2xvcjogYW55KTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFJldHVybiBpZiBudWxsXHJcbiAgICAgICAgaWYgKCAhY29sb3IgKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gU2V0IHRoZSBjb2xvclxyXG4gICAgICAgIHRoaXMuY29sb3IgPSBjb2xvcjtcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBzZWxlY3RlZCBjb2xvclxyXG4gICAgICAgIHRoaXMudXBkYXRlU2VsZWN0ZWRDb2xvcigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VsZWN0IHBhbGV0dGVcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqIEBwYXJhbSBwYWxldHRlXHJcbiAgICAgKi9cclxuICAgIHNlbGVjdFBhbGV0dGUoZXZlbnQsIHBhbGV0dGUpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU3RvcCBwcm9wYWdhdGlvblxyXG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG5cclxuICAgICAgICAvLyBHbyB0byAnaHVlcycgdmlld1xyXG4gICAgICAgIHRoaXMudmlldyA9ICdodWVzJztcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBzZWxlY3RlZCBwYWxldHRlXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFBhbGV0dGUgPSBwYWxldHRlO1xyXG5cclxuICAgICAgICAvLyBVcGRhdGUgdGhlIHNlbGVjdGVkIGNvbG9yXHJcbiAgICAgICAgdGhpcy51cGRhdGVTZWxlY3RlZENvbG9yKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWxlY3QgaHVlXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcGFyYW0gaHVlXHJcbiAgICAgKi9cclxuICAgIHNlbGVjdEh1ZShldmVudCwgaHVlKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFN0b3AgcHJvcGFnYXRpb25cclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBzZWxlY3RlZCBodXNlXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZEh1ZSA9IGh1ZTtcclxuXHJcbiAgICAgICAgLy8gVXBkYXRlIHRoZSBzZWxlY3RlZCBjb2xvclxyXG4gICAgICAgIHRoaXMudXBkYXRlU2VsZWN0ZWRDb2xvcigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlIGNvbG9yXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKi9cclxuICAgIHJlbW92ZUNvbG9yKGV2ZW50KTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFN0b3AgcHJvcGFnYXRpb25cclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgLy8gUmV0dXJuIHRvIHRoZSAncGFsZXR0ZXMnIHZpZXdcclxuICAgICAgICB0aGlzLnZpZXcgPSAncGFsZXR0ZXMnO1xyXG5cclxuICAgICAgICAvLyBDbGVhciB0aGUgc2VsZWN0ZWQgcGFsZXR0ZSBhbmQgaHVlXHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFBhbGV0dGUgPSAnJztcclxuICAgICAgICB0aGlzLnNlbGVjdGVkSHVlID0gJyc7XHJcblxyXG4gICAgICAgIC8vIFVwZGF0ZSB0aGUgc2VsZWN0ZWQgY29sb3JcclxuICAgICAgICB0aGlzLnVwZGF0ZVNlbGVjdGVkQ29sb3IoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFVwZGF0ZSBzZWxlY3RlZCBjb2xvclxyXG4gICAgICovXHJcbiAgICB1cGRhdGVTZWxlY3RlZENvbG9yKCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICBpZiAoIHRoaXMuc2VsZWN0ZWRDb2xvciAmJiB0aGlzLnNlbGVjdGVkQ29sb3IucGFsZXR0ZSA9PT0gdGhpcy5zZWxlY3RlZFBhbGV0dGUgJiYgdGhpcy5zZWxlY3RlZENvbG9yLmh1ZSA9PT0gdGhpcy5zZWxlY3RlZEh1ZSApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBTZXQgdGhlIHNlbGVjdGVkIGNvbG9yIG9iamVjdFxyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRDb2xvciA9IHtcclxuICAgICAgICAgICAgcGFsZXR0ZTogdGhpcy5zZWxlY3RlZFBhbGV0dGUsXHJcbiAgICAgICAgICAgIGh1ZSAgICA6IHRoaXMuc2VsZWN0ZWRIdWUsXHJcbiAgICAgICAgICAgIGNsYXNzICA6IHRoaXMuc2VsZWN0ZWRQYWxldHRlICsgJy0nICsgdGhpcy5zZWxlY3RlZEh1ZSxcclxuICAgICAgICAgICAgYmcgICAgIDogdGhpcy5zZWxlY3RlZFBhbGV0dGUgPT09ICcnID8gJycgOiBNYXRDb2xvcnMuZ2V0Q29sb3IodGhpcy5zZWxlY3RlZFBhbGV0dGUpW3RoaXMuc2VsZWN0ZWRIdWVdLFxyXG4gICAgICAgICAgICBmZyAgICAgOiB0aGlzLnNlbGVjdGVkUGFsZXR0ZSA9PT0gJycgPyAnJyA6IE1hdENvbG9ycy5nZXRDb2xvcih0aGlzLnNlbGVjdGVkUGFsZXR0ZSkuY29udHJhc3RbdGhpcy5zZWxlY3RlZEh1ZV1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAvLyBFbWl0IHRoZSBjb2xvciBjaGFuZ2VkIGV2ZW50XHJcbiAgICAgICAgdGhpcy5jb2xvckNoYW5nZWQuZW1pdCh0aGlzLnNlbGVjdGVkQ29sb3IpO1xyXG5cclxuICAgICAgICAvLyBNYXJrIHRoZSBtb2RlbCBhcyB0b3VjaGVkXHJcbiAgICAgICAgdGhpcy5fbW9kZWxUb3VjaGVkKHRoaXMuc2VsZWN0ZWRDb2xvci5jbGFzcyk7XHJcblxyXG4gICAgICAgIC8vIFVwZGF0ZSB0aGUgbW9kZWxcclxuICAgICAgICB0aGlzLl9tb2RlbENoYW5nZSh0aGlzLnNlbGVjdGVkQ29sb3IuY2xhc3MpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR28gdG8gcGFsZXR0ZXMgdmlld1xyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICovXHJcbiAgICBnb1RvUGFsZXR0ZXNWaWV3KGV2ZW50KTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFN0b3AgcHJvcGFnYXRpb25cclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuXHJcbiAgICAgICAgdGhpcy52aWV3ID0gJ3BhbGV0dGVzJztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIG1lbnUgb3BlblxyXG4gICAgICovXHJcbiAgICBvbk1lbnVPcGVuKCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICBpZiAoIHRoaXMuc2VsZWN0ZWRQYWxldHRlID09PSAnJyApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLnZpZXcgPSAncGFsZXR0ZXMnO1xyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLnZpZXcgPSAnaHVlcyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==