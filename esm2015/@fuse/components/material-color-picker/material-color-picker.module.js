import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FusePipesModule } from '../../../@fuse/pipes/pipes.module';
import { FuseMaterialColorPickerComponent } from '../../../@fuse/components/material-color-picker/material-color-picker.component';
import { MaterialModule } from '../../../material.module';
export class FuseMaterialColorPickerModule {
}
FuseMaterialColorPickerModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    FuseMaterialColorPickerComponent
                ],
                imports: [
                    CommonModule,
                    FlexLayoutModule,
                    MaterialModule,
                    FusePipesModule
                ],
                exports: [
                    FuseMaterialColorPickerComponent
                ],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWF0ZXJpYWwtY29sb3ItcGlja2VyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL21hdGVyaWFsLWNvbG9yLXBpY2tlci9tYXRlcmlhbC1jb2xvci1waWNrZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUVwRSxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsTUFBTSxpRkFBaUYsQ0FBQztBQUNuSSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFrQjFELE1BQU0sT0FBTyw2QkFBNkI7OztZQWhCekMsUUFBUSxTQUFDO2dCQUNOLFlBQVksRUFBRTtvQkFDVixnQ0FBZ0M7aUJBQ25DO2dCQUNELE9BQU8sRUFBRTtvQkFDTCxZQUFZO29CQUVaLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFFZCxlQUFlO2lCQUNsQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsZ0NBQWdDO2lCQUNuQzthQUNKIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgRmxleExheW91dE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2ZsZXgtbGF5b3V0JztcclxuXHJcbmltcG9ydCB7IEZ1c2VQaXBlc01vZHVsZSB9IGZyb20gJy4uLy4uLy4uL0BmdXNlL3BpcGVzL3BpcGVzLm1vZHVsZSc7XHJcblxyXG5pbXBvcnQgeyBGdXNlTWF0ZXJpYWxDb2xvclBpY2tlckNvbXBvbmVudCB9IGZyb20gJy4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvbWF0ZXJpYWwtY29sb3ItcGlja2VyL21hdGVyaWFsLWNvbG9yLXBpY2tlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uLy4uLy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgRnVzZU1hdGVyaWFsQ29sb3JQaWNrZXJDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG5cclxuICAgICAgICBGbGV4TGF5b3V0TW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG5cclxuICAgICAgICBGdXNlUGlwZXNNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgRnVzZU1hdGVyaWFsQ29sb3JQaWNrZXJDb21wb25lbnRcclxuICAgIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlTWF0ZXJpYWxDb2xvclBpY2tlck1vZHVsZVxyXG57XHJcbn1cclxuIl19