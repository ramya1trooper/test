import { Component, EventEmitter, Output } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseConfigService } from '../../../@fuse/services/config.service';
export class FuseSearchBarComponent {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(_fuseConfigService) {
        this._fuseConfigService = _fuseConfigService;
        // Set the defaults
        this.input = new EventEmitter();
        this.collapsed = true;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
            this.fuseConfig = config;
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Collapse
     */
    collapse() {
        this.collapsed = true;
    }
    /**
     * Expand
     */
    expand() {
        this.collapsed = false;
    }
    /**
     * Search
     *
     * @param event
     */
    search(event) {
        this.input.emit(event.target.value);
    }
}
FuseSearchBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'fuse-search-bar',
                template: "<div class=\"fuse-search-bar\" [ngClass]=\"{'expanded':!collapsed}\">\r\n\r\n    <div class=\"fuse-search-bar-content\">\r\n\r\n        <label for=\"fuse-search-bar-input\">\r\n            <button mat-icon-button class=\"fuse-search-bar-expander\" aria-label=\"Expand Search Bar\" (click)=\"expand()\"\r\n                    *ngIf=\"collapsed\">\r\n                <mat-icon class=\"s-24 secondary-text\">search</mat-icon>\r\n            </button>\r\n        </label>\r\n\r\n        <input id=\"fuse-search-bar-input\" class=\"ml-24\" type=\"text\" placeholder=\"Search\" (input)=\"search($event)\"\r\n               fxFlex>\r\n\r\n        <button mat-icon-button class=\"fuse-search-bar-collapser\" (click)=\"collapse()\"\r\n                aria-label=\"Collapse Search Bar\">\r\n            <mat-icon class=\"s-24 secondary-text\">close</mat-icon>\r\n        </button>\r\n\r\n    </div>\r\n\r\n</div>",
                styles: [":host .fuse-search-bar{display:-webkit-box;display:flex;-webkit-box-flex:0;flex:0 1 auto;min-width:64px;height:64px;font-size:13px}:host .fuse-search-bar .fuse-search-bar-content{display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 1 auto;-webkit-box-align:center;align-items:center;-webkit-box-pack:start;justify-content:flex-start}:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-collapser,:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-expander{cursor:pointer;padding:0 20px;margin:0;width:64px!important;height:64px!important;line-height:64px!important}@media screen and (max-width:599px){:host .fuse-search-bar{height:56px}:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-collapser,:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-expander{height:56px!important;line-height:56px!important}}:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-loader{width:64px!important;height:64px!important;line-height:64px!important}@media screen and (max-width:599px){:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-loader{height:56px!important;line-height:56px!important}}:host .fuse-search-bar .fuse-search-bar-content .fuse-search-bar-collapser{display:none}:host .fuse-search-bar .fuse-search-bar-content #fuse-search-bar-input{display:none;-webkit-box-flex:1;flex:1 0 auto;min-height:64px;font-size:16px;background-color:transparent;color:currentColor}:host .fuse-search-bar.expanded{position:absolute;top:0;right:0;bottom:0;left:0;z-index:10}:host .fuse-search-bar.expanded .fuse-search-bar-content #fuse-search-bar-input{display:-webkit-box;display:flex}:host .fuse-search-bar.expanded .fuse-search-bar-content .fuse-search-bar-collapser{display:-webkit-box;display:flex}:host body.fuse-search-bar-expanded #toolbar{z-index:999!important}"]
            }] }
];
/** @nocollapse */
FuseSearchBarComponent.ctorParameters = () => [
    { type: FuseConfigService }
];
FuseSearchBarComponent.propDecorators = {
    input: [{ type: Output }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9zZWFyY2gtYmFyL3NlYXJjaC1iYXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFxQixNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFM0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFPM0UsTUFBTSxPQUFPLHNCQUFzQjtJQVcvQjs7OztPQUlHO0lBQ0gsWUFDWSxrQkFBcUM7UUFBckMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUc3QyxtQkFBbUI7UUFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBRXRCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsUUFBUTtRQUVKLDhCQUE4QjtRQUM5QixJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTTthQUN6QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNyQyxTQUFTLENBQ04sQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUNQLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO1FBQzdCLENBQUMsQ0FDSixDQUFDO0lBQ1YsQ0FBQztJQUVEOztPQUVHO0lBQ0gsV0FBVztRQUVQLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsUUFBUTtRQUVKLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQzFCLENBQUM7SUFFRDs7T0FFRztJQUNILE1BQU07UUFFRixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztJQUMzQixDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILE1BQU0sQ0FBQyxLQUFLO1FBRVIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7WUExRkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBSyxpQkFBaUI7Z0JBQzlCLGs1QkFBMEM7O2FBRTdDOzs7O1lBTlEsaUJBQWlCOzs7b0JBWXJCLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgT25EZXN0cm95LCBPbkluaXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IEZ1c2VDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vQGZ1c2Uvc2VydmljZXMvY29uZmlnLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvciAgIDogJ2Z1c2Utc2VhcmNoLWJhcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2VhcmNoLWJhci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHMgIDogWycuL3NlYXJjaC1iYXIuY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZVNlYXJjaEJhckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95XHJcbntcclxuICAgIGNvbGxhcHNlZDogYm9vbGVhbjtcclxuICAgIGZ1c2VDb25maWc6IGFueTtcclxuXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGlucHV0OiBFdmVudEVtaXR0ZXI8YW55PjtcclxuXHJcbiAgICAvLyBQcml2YXRlXHJcbiAgICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge0Z1c2VDb25maWdTZXJ2aWNlfSBfZnVzZUNvbmZpZ1NlcnZpY2VcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfZnVzZUNvbmZpZ1NlcnZpY2U6IEZ1c2VDb25maWdTZXJ2aWNlXHJcbiAgICApXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgICAgIHRoaXMuaW5wdXQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcbiAgICAgICAgdGhpcy5jb2xsYXBzZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gaW5pdFxyXG4gICAgICovXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU3Vic2NyaWJlIHRvIGNvbmZpZyBjaGFuZ2VzXHJcbiAgICAgICAgdGhpcy5fZnVzZUNvbmZpZ1NlcnZpY2UuY29uZmlnXHJcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoY29uZmlnKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mdXNlQ29uZmlnID0gY29uZmlnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gZGVzdHJveVxyXG4gICAgICovXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gVW5zdWJzY3JpYmUgZnJvbSBhbGwgc3Vic2NyaXB0aW9uc1xyXG4gICAgICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIFB1YmxpYyBtZXRob2RzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29sbGFwc2VcclxuICAgICAqL1xyXG4gICAgY29sbGFwc2UoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHRoaXMuY29sbGFwc2VkID0gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEV4cGFuZFxyXG4gICAgICovXHJcbiAgICBleHBhbmQoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHRoaXMuY29sbGFwc2VkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWFyY2hcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgc2VhcmNoKGV2ZW50KTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHRoaXMuaW5wdXQuZW1pdChldmVudC50YXJnZXQudmFsdWUpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=