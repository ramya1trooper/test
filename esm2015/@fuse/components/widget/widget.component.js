import { Component, ContentChildren, ElementRef, HostBinding, QueryList, Renderer2, ViewEncapsulation } from '@angular/core';
import { FuseWidgetToggleDirective } from './widget-toggle.directive';
export class FuseWidgetComponent {
    /**
     * Constructor
     *
     * @param {ElementRef} _elementRef
     * @param {Renderer2} _renderer
     */
    constructor(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        this.flipped = false;
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * After content init
     */
    ngAfterContentInit() {
        // Listen for the flip button click
        setTimeout(() => {
            this.toggleButtons.forEach(flipButton => {
                this._renderer.listen(flipButton.elementRef.nativeElement, 'click', (event) => {
                    event.preventDefault();
                    event.stopPropagation();
                    this.toggle();
                });
            });
        });
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle the flipped status
     */
    toggle() {
        this.flipped = !this.flipped;
    }
}
FuseWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'fuse-widget',
                template: "<ng-content></ng-content>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: ["fuse-widget{display:block;position:relative;-webkit-perspective:3000px;perspective:3000px;padding:12px}fuse-widget>div{position:relative;-webkit-transform-style:preserve-3d;transform-style:preserve-3d;-webkit-transition:-webkit-transform 1s;transition:transform 1s;transition:transform 1s,-webkit-transform 1s}fuse-widget>.fuse-widget-front{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;position:relative;overflow:hidden;visibility:visible;width:100%;opacity:1;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(0);transform:rotateY(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}fuse-widget>.fuse-widget-back{display:block;position:absolute;top:12px;right:12px;bottom:12px;left:12px;overflow:hidden;visibility:hidden;opacity:0;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(180deg);transform:rotateY(180deg);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}fuse-widget>.fuse-widget-back [fuseWidgetToggle]{position:absolute;top:0;right:0}fuse-widget.flipped>.fuse-widget-front{visibility:hidden;opacity:0;-webkit-transform:rotateY(180deg);transform:rotateY(180deg)}fuse-widget.flipped>.fuse-widget-back{display:block;visibility:visible;opacity:1;-webkit-transform:rotateY(360deg);transform:rotateY(360deg)}fuse-widget .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper{padding:16px 0}fuse-widget .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper .mat-form-field-infix{border:none;padding:0}fuse-widget .mat-form-field.mat-form-field-type-mat-select .mat-form-field-underline{display:none}.fusewidgetClass{display:block;position:relative;-webkit-perspective:3000px;perspective:3000px;padding:0}.fusewidgetClass>div{position:relative;-webkit-transform-style:preserve-3d;transform-style:preserve-3d;-webkit-transition:-webkit-transform 1s;transition:transform 1s;transition:transform 1s,-webkit-transform 1s}.fusewidgetClass>.fuse-widget-front{border-color:#dbdbdb!important;background:#fff;display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;position:relative;overflow:hidden;visibility:visible;width:100%;opacity:1;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(0);transform:rotateY(0);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}.fusewidgetClass>.fuse-widget-back{display:block;position:absolute;top:12px;right:12px;bottom:12px;left:12px;overflow:hidden;visibility:hidden;opacity:0;z-index:10;border-radius:8px;-webkit-transition:visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;transition:transform .5s ease-out,visibility ease-in .2s,opacity ease-in .2s,-webkit-transform .5s ease-out;-webkit-transform:rotateY(180deg);transform:rotateY(180deg);-webkit-backface-visibility:hidden;backface-visibility:hidden;border:1px solid}.fusewidgetClass>.fuse-widget-back [fuseWidgetToggle]{position:absolute;top:0;right:0}.fusewidgetClass.flipped>.fuse-widget-front{visibility:hidden;opacity:0;-webkit-transform:rotateY(180deg);transform:rotateY(180deg)}.fusewidgetClass.flipped>.fuse-widget-back{display:block;visibility:visible;opacity:1;-webkit-transform:rotateY(360deg);transform:rotateY(360deg)}.fusewidgetClass .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper{padding:16px 0}.fusewidgetClass .mat-form-field.mat-form-field-type-mat-select .mat-form-field-wrapper .mat-form-field-infix{border:none;padding:0}.fusewidgetClass .mat-form-field.mat-form-field-type-mat-select .mat-form-field-underline{display:none}"]
            }] }
];
/** @nocollapse */
FuseWidgetComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
FuseWidgetComponent.propDecorators = {
    flipped: [{ type: HostBinding, args: ['class.flipped',] }],
    toggleButtons: [{ type: ContentChildren, args: [FuseWidgetToggleDirective, { descendants: true },] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3dpZGdldC93aWRnZXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBb0IsU0FBUyxFQUFFLGVBQWUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0ksT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFTdEUsTUFBTSxPQUFPLG1CQUFtQjtJQVE1Qjs7Ozs7T0FLRztJQUNILFlBQ1ksV0FBdUIsRUFDdkIsU0FBb0I7UUFEcEIsZ0JBQVcsR0FBWCxXQUFXLENBQVk7UUFDdkIsY0FBUyxHQUFULFNBQVMsQ0FBVztRQWJoQyxZQUFPLEdBQUcsS0FBSyxDQUFDO0lBZ0JoQixDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxrQkFBa0I7UUFFZCxtQ0FBbUM7UUFDbkMsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxFQUFFO2dCQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxPQUFPLEVBQUUsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDMUUsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDbEIsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsTUFBTTtRQUVGLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO0lBQ2pDLENBQUM7OztZQTNESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFPLGFBQWE7Z0JBQzVCLHlDQUF3QztnQkFFeEMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBUnNELFVBQVU7WUFBMEIsU0FBUzs7O3NCQVkvRixXQUFXLFNBQUMsZUFBZTs0QkFHM0IsZUFBZSxTQUFDLHlCQUF5QixFQUFFLEVBQUMsV0FBVyxFQUFFLElBQUksRUFBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFmdGVyQ29udGVudEluaXQsIENvbXBvbmVudCwgQ29udGVudENoaWxkcmVuLCBFbGVtZW50UmVmLCBIb3N0QmluZGluZywgUXVlcnlMaXN0LCBSZW5kZXJlcjIsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmUgfSBmcm9tICcuL3dpZGdldC10b2dnbGUuZGlyZWN0aXZlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3IgICAgIDogJ2Z1c2Utd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsICA6ICcuL3dpZGdldC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHMgICAgOiBbJy4vd2lkZ2V0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgRnVzZVdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyQ29udGVudEluaXRcclxue1xyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5mbGlwcGVkJylcclxuICAgIGZsaXBwZWQgPSBmYWxzZTtcclxuXHJcbiAgICBAQ29udGVudENoaWxkcmVuKEZ1c2VXaWRnZXRUb2dnbGVEaXJlY3RpdmUsIHtkZXNjZW5kYW50czogdHJ1ZX0pXHJcbiAgICB0b2dnbGVCdXR0b25zOiBRdWVyeUxpc3Q8RnVzZVdpZGdldFRvZ2dsZURpcmVjdGl2ZT47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7RWxlbWVudFJlZn0gX2VsZW1lbnRSZWZcclxuICAgICAqIEBwYXJhbSB7UmVuZGVyZXIyfSBfcmVuZGVyZXJcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfZWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgICAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyXHJcbiAgICApXHJcbiAgICB7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWZ0ZXIgY29udGVudCBpbml0XHJcbiAgICAgKi9cclxuICAgIG5nQWZ0ZXJDb250ZW50SW5pdCgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gTGlzdGVuIGZvciB0aGUgZmxpcCBidXR0b24gY2xpY2tcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy50b2dnbGVCdXR0b25zLmZvckVhY2goZmxpcEJ1dHRvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLl9yZW5kZXJlci5saXN0ZW4oZmxpcEJ1dHRvbi5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdjbGljaycsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50b2dnbGUoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gICAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRvZ2dsZSB0aGUgZmxpcHBlZCBzdGF0dXNcclxuICAgICAqL1xyXG4gICAgdG9nZ2xlKCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLmZsaXBwZWQgPSAhdGhpcy5mbGlwcGVkO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=