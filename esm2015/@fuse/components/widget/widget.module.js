import { NgModule } from '@angular/core';
import { FuseWidgetComponent } from './widget.component';
import { FuseWidgetToggleDirective } from './widget-toggle.directive';
export class FuseWidgetModule {
}
FuseWidgetModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    FuseWidgetComponent,
                    FuseWidgetToggleDirective
                ],
                exports: [
                    FuseWidgetComponent,
                    FuseWidgetToggleDirective
                ],
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3dpZGdldC93aWRnZXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFekMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDekQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFZdEUsTUFBTSxPQUFPLGdCQUFnQjs7O1lBVjVCLFFBQVEsU0FBQztnQkFDTixZQUFZLEVBQUU7b0JBQ1YsbUJBQW1CO29CQUNuQix5QkFBeUI7aUJBQzVCO2dCQUNELE9BQU8sRUFBTztvQkFDVixtQkFBbUI7b0JBQ25CLHlCQUF5QjtpQkFDNUI7YUFDSiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBGdXNlV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi93aWRnZXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRnVzZVdpZGdldFRvZ2dsZURpcmVjdGl2ZSB9IGZyb20gJy4vd2lkZ2V0LXRvZ2dsZS5kaXJlY3RpdmUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEZ1c2VXaWRnZXRDb21wb25lbnQsXHJcbiAgICAgICAgRnVzZVdpZGdldFRvZ2dsZURpcmVjdGl2ZVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHMgICAgIDogW1xyXG4gICAgICAgIEZ1c2VXaWRnZXRDb21wb25lbnQsXHJcbiAgICAgICAgRnVzZVdpZGdldFRvZ2dsZURpcmVjdGl2ZVxyXG4gICAgXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VXaWRnZXRNb2R1bGVcclxue1xyXG59XHJcbiJdfQ==