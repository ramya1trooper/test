import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class FuseSidebarService {
    /**
     * Constructor
     */
    constructor() {
        // Private
        this._registry = {};
    }
    /**
     * Add the sidebar to the registry
     *
     * @param key
     * @param sidebar
     */
    register(key, sidebar) {
        // Check if the key already being used
        if (this._registry[key]) {
            console.error(`The sidebar with the key '${key}' already exists. Either unregister it first or use a unique key.`);
            return;
        }
        // Add to the registry
        this._registry[key] = sidebar;
    }
    /**
     * Remove the sidebar from the registry
     *
     * @param key
     */
    unregister(key) {
        // Check if the sidebar exists
        if (!this._registry[key]) {
            console.warn(`The sidebar with the key '${key}' doesn't exist in the registry.`);
        }
        // Unregister the sidebar
        delete this._registry[key];
    }
    /**
     * Return the sidebar with the given key
     *
     * @param key
     * @returns {FuseSidebarComponent}
     */
    getSidebar(key) {
        // Check if the sidebar exists
        if (!this._registry[key]) {
            console.warn(`The sidebar with the key '${key}' doesn't exist in the registry.`);
            return;
        }
        // Return the sidebar
        return this._registry[key];
    }
}
FuseSidebarService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
FuseSidebarService.ctorParameters = () => [];
FuseSidebarService.ngInjectableDef = i0.defineInjectable({ factory: function FuseSidebarService_Factory() { return new FuseSidebarService(); }, token: FuseSidebarService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvc2lkZWJhci9zaWRlYmFyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFPM0MsTUFBTSxPQUFPLGtCQUFrQjtJQUszQjs7T0FFRztJQUNIO1FBTkEsVUFBVTtRQUNGLGNBQVMsR0FBNEMsRUFBRSxDQUFDO0lBUWhFLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILFFBQVEsQ0FBQyxHQUFHLEVBQUUsT0FBTztRQUVqQixzQ0FBc0M7UUFDdEMsSUFBSyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUN4QjtZQUNJLE9BQU8sQ0FBQyxLQUFLLENBQUMsNkJBQTZCLEdBQUcsbUVBQW1FLENBQUMsQ0FBQztZQUVuSCxPQUFPO1NBQ1Y7UUFFRCxzQkFBc0I7UUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxPQUFPLENBQUM7SUFDbEMsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxVQUFVLENBQUMsR0FBRztRQUVWLDhCQUE4QjtRQUM5QixJQUFLLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFDekI7WUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLDZCQUE2QixHQUFHLGtDQUFrQyxDQUFDLENBQUM7U0FDcEY7UUFFRCx5QkFBeUI7UUFDekIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILFVBQVUsQ0FBQyxHQUFHO1FBRVYsOEJBQThCO1FBQzlCLElBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUN6QjtZQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsNkJBQTZCLEdBQUcsa0NBQWtDLENBQUMsQ0FBQztZQUVqRixPQUFPO1NBQ1Y7UUFFRCxxQkFBcUI7UUFDckIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7OztZQXZFSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2lkZWJhckNvbXBvbmVudCB9IGZyb20gJy4vc2lkZWJhci5jb21wb25lbnQnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlU2lkZWJhclNlcnZpY2Vcclxue1xyXG4gICAgLy8gUHJpdmF0ZVxyXG4gICAgcHJpdmF0ZSBfcmVnaXN0cnk6IHsgW2tleTogc3RyaW5nXTogRnVzZVNpZGViYXJDb21wb25lbnQgfSA9IHt9O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoKVxyXG4gICAge1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZCB0aGUgc2lkZWJhciB0byB0aGUgcmVnaXN0cnlcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ga2V5XHJcbiAgICAgKiBAcGFyYW0gc2lkZWJhclxyXG4gICAgICovXHJcbiAgICByZWdpc3RlcihrZXksIHNpZGViYXIpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gQ2hlY2sgaWYgdGhlIGtleSBhbHJlYWR5IGJlaW5nIHVzZWRcclxuICAgICAgICBpZiAoIHRoaXMuX3JlZ2lzdHJ5W2tleV0gKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgY29uc29sZS5lcnJvcihgVGhlIHNpZGViYXIgd2l0aCB0aGUga2V5ICcke2tleX0nIGFscmVhZHkgZXhpc3RzLiBFaXRoZXIgdW5yZWdpc3RlciBpdCBmaXJzdCBvciB1c2UgYSB1bmlxdWUga2V5LmApO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQWRkIHRvIHRoZSByZWdpc3RyeVxyXG4gICAgICAgIHRoaXMuX3JlZ2lzdHJ5W2tleV0gPSBzaWRlYmFyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlIHRoZSBzaWRlYmFyIGZyb20gdGhlIHJlZ2lzdHJ5XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGtleVxyXG4gICAgICovXHJcbiAgICB1bnJlZ2lzdGVyKGtleSk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICAvLyBDaGVjayBpZiB0aGUgc2lkZWJhciBleGlzdHNcclxuICAgICAgICBpZiAoICF0aGlzLl9yZWdpc3RyeVtrZXldIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihgVGhlIHNpZGViYXIgd2l0aCB0aGUga2V5ICcke2tleX0nIGRvZXNuJ3QgZXhpc3QgaW4gdGhlIHJlZ2lzdHJ5LmApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gVW5yZWdpc3RlciB0aGUgc2lkZWJhclxyXG4gICAgICAgIGRlbGV0ZSB0aGlzLl9yZWdpc3RyeVtrZXldO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJuIHRoZSBzaWRlYmFyIHdpdGggdGhlIGdpdmVuIGtleVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBrZXlcclxuICAgICAqIEByZXR1cm5zIHtGdXNlU2lkZWJhckNvbXBvbmVudH1cclxuICAgICAqL1xyXG4gICAgZ2V0U2lkZWJhcihrZXkpOiBGdXNlU2lkZWJhckNvbXBvbmVudFxyXG4gICAge1xyXG4gICAgICAgIC8vIENoZWNrIGlmIHRoZSBzaWRlYmFyIGV4aXN0c1xyXG4gICAgICAgIGlmICggIXRoaXMuX3JlZ2lzdHJ5W2tleV0gKVxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgY29uc29sZS53YXJuKGBUaGUgc2lkZWJhciB3aXRoIHRoZSBrZXkgJyR7a2V5fScgZG9lc24ndCBleGlzdCBpbiB0aGUgcmVnaXN0cnkuYCk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBSZXR1cm4gdGhlIHNpZGViYXJcclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVnaXN0cnlba2V5XTtcclxuICAgIH1cclxufVxyXG4iXX0=