import { Component, ElementRef, Input, Renderer2, ViewChild } from '@angular/core';
import { ObservableMedia } from '@angular/flex-layout';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseMatchMediaService } from '../../../@fuse/services/match-media.service';
import { FuseNavigationService } from '../../../@fuse/components/navigation/navigation.service';
export class FuseShortcutsComponent {
    /**
     * Constructor
     *
     * @param {Renderer2} _renderer
     * @param {CookieService} _cookieService
     * @param {FuseMatchMediaService} _fuseMatchMediaService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {ObservableMedia} _observableMedia
     */
    constructor(_cookieService, _fuseMatchMediaService, _fuseNavigationService, _observableMedia, _renderer) {
        this._cookieService = _cookieService;
        this._fuseMatchMediaService = _fuseMatchMediaService;
        this._fuseNavigationService = _fuseNavigationService;
        this._observableMedia = _observableMedia;
        this._renderer = _renderer;
        // Set the defaults
        this.shortcutItems = [];
        this.searching = false;
        this.mobileShortcutsPanelActive = false;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        // Get the navigation items and flatten them
        this.filteredNavigationItems = this.navigationItems = this._fuseNavigationService.getFlatNavigation(this.navigation);
        if (this._cookieService.check('FUSE2.shortcuts')) {
            this.shortcutItems = JSON.parse(this._cookieService.get('FUSE2.shortcuts'));
        }
        else {
            // User's shortcut items
            this.shortcutItems = [];
        }
        // Subscribe to media changes
        this._fuseMatchMediaService.onMediaChange
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            if (this._observableMedia.isActive('gt-sm')) {
                this.hideMobileShortcutsPanel();
            }
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Search
     *
     * @param event
     */
    search(event) {
        const value = event.target.value.toLowerCase();
        if (value === '') {
            this.searching = false;
            this.filteredNavigationItems = this.navigationItems;
            return;
        }
        this.searching = true;
        this.filteredNavigationItems = this.navigationItems.filter((navigationItem) => {
            return navigationItem.title.toLowerCase().includes(value);
        });
    }
    /**
     * Toggle shortcut
     *
     * @param event
     * @param itemToToggle
     */
    toggleShortcut(event, itemToToggle) {
        event.stopPropagation();
        for (let i = 0; i < this.shortcutItems.length; i++) {
            if (this.shortcutItems[i].url === itemToToggle.url) {
                this.shortcutItems.splice(i, 1);
                // Save to the cookies
                this._cookieService.set('FUSE2.shortcuts', JSON.stringify(this.shortcutItems));
                return;
            }
        }
        this.shortcutItems.push(itemToToggle);
        // Save to the cookies
        this._cookieService.set('FUSE2.shortcuts', JSON.stringify(this.shortcutItems));
    }
    /**
     * Is in shortcuts?
     *
     * @param navigationItem
     * @returns {any}
     */
    isInShortcuts(navigationItem) {
        return this.shortcutItems.find(item => {
            return item.url === navigationItem.url;
        });
    }
    /**
     * On menu open
     */
    onMenuOpen() {
        setTimeout(() => {
            this.searchInputField.nativeElement.focus();
        });
    }
    /**
     * Show mobile shortcuts
     */
    showMobileShortcutsPanel() {
        this.mobileShortcutsPanelActive = true;
        this._renderer.addClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
    }
    /**
     * Hide mobile shortcuts
     */
    hideMobileShortcutsPanel() {
        this.mobileShortcutsPanelActive = false;
        this._renderer.removeClass(this.shortcutsEl.nativeElement, 'show-mobile-panel');
    }
}
FuseShortcutsComponent.decorators = [
    { type: Component, args: [{
                selector: 'fuse-shortcuts',
                template: "<div id=\"fuse-shortcuts\" #shortcuts>\r\n\r\n    <div class=\"shortcuts-mobile-toggle\" *ngIf=\"!mobileShortcutsPanelActive\" fxLayout=\"row\" fxLayoutAlign=\"start center\"\r\n         fxHide fxShow.lt-md>\r\n        <button mat-icon-button (click)=\"showMobileShortcutsPanel()\">\r\n            <mat-icon class=\"amber-600-fg\">star</mat-icon>\r\n        </button>\r\n    </div>\r\n\r\n    <div class=\"shortcuts\" fxLayout=\"row\" fxHide fxShow.gt-sm>\r\n\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"space-between center\" fxFlex=\"0 1 auto\">\r\n\r\n            <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                <div class=\"w-40 h-40 p-4\" fxLayout=\"row\" fxLayoutAlign=\"center center\"\r\n                     *ngFor=\"let shortcutItem of shortcutItems\">\r\n\r\n                    <a mat-icon-button matTooltip=\"{{shortcutItem.title}}\" [routerLink]=\"shortcutItem.url\">\r\n                        <mat-icon class=\"secondary-text\" *ngIf=\"shortcutItem.icon\">{{shortcutItem.icon}}</mat-icon>\r\n                        <span *ngIf=\"!shortcutItem.icon\" class=\"h2 secondary-text text-bold\">\r\n                            {{shortcutItem.title.substr(0, 1).toUpperCase()}}\r\n                        </span>\r\n                    </a>\r\n\r\n                </div>\r\n\r\n                <button mat-icon-button [matMenuTriggerFor]=\"addMenu\" matTooltip=\"Click to add/remove shortcut\"\r\n                        (menuOpened)=\"onMenuOpen()\">\r\n                    <mat-icon class=\"amber-600-fg\">star</mat-icon>\r\n                </button>\r\n\r\n            </div>\r\n\r\n            <div class=\"shortcuts-mobile-close\" fxLayout=\"row\" fxLayoutAlign=\"start center\" fxHide fxShow.lt-md>\r\n                <button mat-icon-button (click)=\"hideMobileShortcutsPanel()\">\r\n                    <mat-icon>close</mat-icon>\r\n                </button>\r\n            </div>\r\n\r\n        </div>\r\n\r\n        <mat-menu #addMenu=\"matMenu\" class=\"w-240\">\r\n\r\n            <mat-form-field class=\"px-16 w-100-p\" (click)=\"$event.stopPropagation()\" floatLabel=\"never\">\r\n                <input #searchInput matInput placeholder=\"Search for an app or a page\" (input)=\"search($event)\">\r\n            </mat-form-field>\r\n\r\n            <mat-divider></mat-divider>\r\n\r\n            <mat-nav-list *ngIf=\"!searching\" style=\"max-height: 312px; overflow: auto\" fusePerfectScrollbar>\r\n\r\n                <mat-list-item *ngFor=\"let shortcutItem of shortcutItems\"\r\n                               (click)=\"toggleShortcut($event, shortcutItem)\">\r\n\r\n                    <div class=\"w-100-p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                        <mat-icon mat-list-icon class=\"mr-8 secondary-text\" *ngIf=\"shortcutItem.icon\">\r\n                            {{shortcutItem.icon}}\r\n                        </mat-icon>\r\n\r\n                        <span class=\"h2 w-32 h-32 p-4 mr-8 secondary-text text-bold\" fxLayout=\"row\"\r\n                              fxLayoutAlign=\"center center\" *ngIf=\"!shortcutItem.icon\">\r\n                            {{shortcutItem.title.substr(0, 1).toUpperCase()}}\r\n                        </span>\r\n\r\n                        <p matLine fxFlex>{{shortcutItem.title}}</p>\r\n\r\n                        <mat-icon class=\"ml-8 amber-fg\">star</mat-icon>\r\n\r\n                    </div>\r\n\r\n                </mat-list-item>\r\n\r\n                <mat-list-item *ngIf=\"shortcutItems.length === 0\">\r\n                    <p>\r\n                        <small>No shortcuts yet!</small>\r\n                    </p>\r\n                </mat-list-item>\r\n\r\n            </mat-nav-list>\r\n\r\n            <mat-nav-list *ngIf=\"searching\" style=\"max-height: 312px; overflow: auto\" fusePerfectScrollbar>\r\n\r\n                <mat-list-item *ngFor=\"let navigationItem of filteredNavigationItems\"\r\n                               (click)=\"toggleShortcut($event, navigationItem)\">\r\n\r\n                    <div class=\"w-100-p\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n                        <mat-icon mat-list-icon class=\"mr-8 secondary-text\" *ngIf=\"navigationItem.icon\">\r\n                            {{navigationItem.icon}}\r\n                        </mat-icon>\r\n\r\n                        <span class=\"h2 w-32 h-32 p-4 mr-8 secondary-text text-bold\" fxLayout=\"row\"\r\n                              fxLayoutAlign=\"center center\" *ngIf=\"!navigationItem.icon\">\r\n                            {{navigationItem.title.substr(0, 1).toUpperCase()}}\r\n                        </span>\r\n\r\n                        <p matLine fxFlex>{{navigationItem.title}}</p>\r\n\r\n                        <mat-icon class=\"ml-8 amber-fg\" *ngIf=\"isInShortcuts(navigationItem)\">star</mat-icon>\r\n\r\n                    </div>\r\n\r\n                </mat-list-item>\r\n\r\n            </mat-nav-list>\r\n\r\n        </mat-menu>\r\n\r\n    </div>\r\n\r\n</div>\r\n",
                styles: ["@media screen and (max-width:959px){:host #fuse-shortcuts.show-mobile-panel{position:absolute;top:0;right:0;bottom:0;left:0;z-index:99;padding:0 8px}:host #fuse-shortcuts.show-mobile-panel .shortcuts{display:-webkit-box!important;display:flex!important;-webkit-box-flex:1;flex:1;height:100%}:host #fuse-shortcuts.show-mobile-panel .shortcuts>div{-webkit-box-flex:1!important;flex:1 1 auto!important}}"]
            }] }
];
/** @nocollapse */
FuseShortcutsComponent.ctorParameters = () => [
    { type: CookieService },
    { type: FuseMatchMediaService },
    { type: FuseNavigationService },
    { type: ObservableMedia },
    { type: Renderer2 }
];
FuseShortcutsComponent.propDecorators = {
    navigation: [{ type: Input }],
    searchInputField: [{ type: ViewChild, args: ['searchInput',] }],
    shortcutsEl: [{ type: ViewChild, args: ['shortcuts',] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hvcnRjdXRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJAZnVzZS9jb21wb25lbnRzL3Nob3J0Y3V0cy9zaG9ydGN1dHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN0RyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3BGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBT2hHLE1BQU0sT0FBTyxzQkFBc0I7SUFvQi9COzs7Ozs7OztPQVFHO0lBQ0gsWUFDWSxjQUE2QixFQUM3QixzQkFBNkMsRUFDN0Msc0JBQTZDLEVBQzdDLGdCQUFpQyxFQUNqQyxTQUFvQjtRQUpwQixtQkFBYyxHQUFkLGNBQWMsQ0FBZTtRQUM3QiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXVCO1FBQzdDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBdUI7UUFDN0MscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFpQjtRQUNqQyxjQUFTLEdBQVQsU0FBUyxDQUFXO1FBRzVCLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsMEJBQTBCLEdBQUcsS0FBSyxDQUFDO1FBRXhDLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsUUFBUTtRQUVKLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXJILElBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsRUFDakQ7WUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDO1NBQy9FO2FBRUQ7WUFDSSx3QkFBd0I7WUFDeEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7U0FDM0I7UUFFRCw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGFBQWE7YUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUssSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsRUFDNUM7Z0JBQ0ksSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7YUFDbkM7UUFDTCxDQUFDLENBQUMsQ0FBQztJQUNYLENBQUM7SUFFRDs7T0FFRztJQUNILFdBQVc7UUFFUCxxQ0FBcUM7UUFDckMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BDLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsbUJBQW1CO0lBQ25CLHdHQUF3RztJQUV4Rzs7OztPQUlHO0lBQ0gsTUFBTSxDQUFDLEtBQUs7UUFFUixNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUUvQyxJQUFLLEtBQUssS0FBSyxFQUFFLEVBQ2pCO1lBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUM7WUFFcEQsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFFdEIsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUMsY0FBYyxFQUFFLEVBQUU7WUFDMUUsT0FBTyxjQUFjLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILGNBQWMsQ0FBQyxLQUFLLEVBQUUsWUFBWTtRQUU5QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7UUFFeEIsS0FBTSxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUNuRDtZQUNJLElBQUssSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssWUFBWSxDQUFDLEdBQUcsRUFDbkQ7Z0JBQ0ksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUVoQyxzQkFBc0I7Z0JBQ3RCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7Z0JBRS9FLE9BQU87YUFDVjtTQUNKO1FBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFFdEMsc0JBQXNCO1FBQ3RCLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7SUFDbkYsQ0FBQztJQUVEOzs7OztPQUtHO0lBQ0gsYUFBYSxDQUFDLGNBQWM7UUFFeEIsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUNsQyxPQUFPLElBQUksQ0FBQyxHQUFHLEtBQUssY0FBYyxDQUFDLEdBQUcsQ0FBQztRQUMzQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7T0FFRztJQUNILFVBQVU7UUFFTixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxDQUFDLGdCQUFnQixDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7T0FFRztJQUNILHdCQUF3QjtRQUVwQixJQUFJLENBQUMsMEJBQTBCLEdBQUcsSUFBSSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLG1CQUFtQixDQUFDLENBQUM7SUFDakYsQ0FBQztJQUVEOztPQUVHO0lBQ0gsd0JBQXdCO1FBRXBCLElBQUksQ0FBQywwQkFBMEIsR0FBRyxLQUFLLENBQUM7UUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztJQUNwRixDQUFDOzs7WUE5TEosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBSyxnQkFBZ0I7Z0JBQzdCLG04SkFBeUM7O2FBRTVDOzs7O1lBWFEsYUFBYTtZQUliLHFCQUFxQjtZQUNyQixxQkFBcUI7WUFOckIsZUFBZTtZQURrQyxTQUFTOzs7eUJBc0I5RCxLQUFLOytCQUdMLFNBQVMsU0FBQyxhQUFhOzBCQUd2QixTQUFTLFNBQUMsV0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0LCBSZW5kZXJlcjIsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlTWVkaWEgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IENvb2tpZVNlcnZpY2UgfSBmcm9tICduZ3gtY29va2llLXNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IEZ1c2VNYXRjaE1lZGlhU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL0BmdXNlL3NlcnZpY2VzL21hdGNoLW1lZGlhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGdXNlTmF2aWdhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3IgICA6ICdmdXNlLXNob3J0Y3V0cycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2hvcnRjdXRzLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJscyAgOiBbJy4vc2hvcnRjdXRzLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VTaG9ydGN1dHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveVxyXG57XHJcbiAgICBzaG9ydGN1dEl0ZW1zOiBhbnlbXTtcclxuICAgIG5hdmlnYXRpb25JdGVtczogYW55W107XHJcbiAgICBmaWx0ZXJlZE5hdmlnYXRpb25JdGVtczogYW55W107XHJcbiAgICBzZWFyY2hpbmc6IGJvb2xlYW47XHJcbiAgICBtb2JpbGVTaG9ydGN1dHNQYW5lbEFjdGl2ZTogYm9vbGVhbjtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbmF2aWdhdGlvbjogYW55O1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3NlYXJjaElucHV0JylcclxuICAgIHNlYXJjaElucHV0RmllbGQ7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnc2hvcnRjdXRzJylcclxuICAgIHNob3J0Y3V0c0VsOiBFbGVtZW50UmVmO1xyXG5cclxuICAgIC8vIFByaXZhdGVcclxuICAgIHByaXZhdGUgX3Vuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7UmVuZGVyZXIyfSBfcmVuZGVyZXJcclxuICAgICAqIEBwYXJhbSB7Q29va2llU2VydmljZX0gX2Nvb2tpZVNlcnZpY2VcclxuICAgICAqIEBwYXJhbSB7RnVzZU1hdGNoTWVkaWFTZXJ2aWNlfSBfZnVzZU1hdGNoTWVkaWFTZXJ2aWNlXHJcbiAgICAgKiBAcGFyYW0ge0Z1c2VOYXZpZ2F0aW9uU2VydmljZX0gX2Z1c2VOYXZpZ2F0aW9uU2VydmljZVxyXG4gICAgICogQHBhcmFtIHtPYnNlcnZhYmxlTWVkaWF9IF9vYnNlcnZhYmxlTWVkaWFcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfY29va2llU2VydmljZTogQ29va2llU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9mdXNlTWF0Y2hNZWRpYVNlcnZpY2U6IEZ1c2VNYXRjaE1lZGlhU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9mdXNlTmF2aWdhdGlvblNlcnZpY2U6IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIF9vYnNlcnZhYmxlTWVkaWE6IE9ic2VydmFibGVNZWRpYSxcclxuICAgICAgICBwcml2YXRlIF9yZW5kZXJlcjogUmVuZGVyZXIyXHJcbiAgICApXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgICAgIHRoaXMuc2hvcnRjdXRJdGVtcyA9IFtdO1xyXG4gICAgICAgIHRoaXMuc2VhcmNoaW5nID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5tb2JpbGVTaG9ydGN1dHNQYW5lbEFjdGl2ZSA9IGZhbHNlO1xyXG5cclxuICAgICAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gaW5pdFxyXG4gICAgICovXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gR2V0IHRoZSBuYXZpZ2F0aW9uIGl0ZW1zIGFuZCBmbGF0dGVuIHRoZW1cclxuICAgICAgICB0aGlzLmZpbHRlcmVkTmF2aWdhdGlvbkl0ZW1zID0gdGhpcy5uYXZpZ2F0aW9uSXRlbXMgPSB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2UuZ2V0RmxhdE5hdmlnYXRpb24odGhpcy5uYXZpZ2F0aW9uKTtcclxuXHJcbiAgICAgICAgaWYgKCB0aGlzLl9jb29raWVTZXJ2aWNlLmNoZWNrKCdGVVNFMi5zaG9ydGN1dHMnKSApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aGlzLnNob3J0Y3V0SXRlbXMgPSBKU09OLnBhcnNlKHRoaXMuX2Nvb2tpZVNlcnZpY2UuZ2V0KCdGVVNFMi5zaG9ydGN1dHMnKSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGVsc2VcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIC8vIFVzZXIncyBzaG9ydGN1dCBpdGVtc1xyXG4gICAgICAgICAgICB0aGlzLnNob3J0Y3V0SXRlbXMgPSBbXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFN1YnNjcmliZSB0byBtZWRpYSBjaGFuZ2VzXHJcbiAgICAgICAgdGhpcy5fZnVzZU1hdGNoTWVkaWFTZXJ2aWNlLm9uTWVkaWFDaGFuZ2VcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIHRoaXMuX29ic2VydmFibGVNZWRpYS5pc0FjdGl2ZSgnZ3Qtc20nKSApXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5oaWRlTW9iaWxlU2hvcnRjdXRzUGFuZWwoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPbiBkZXN0cm95XHJcbiAgICAgKi9cclxuICAgIG5nT25EZXN0cm95KCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICAvLyBVbnN1YnNjcmliZSBmcm9tIGFsbCBzdWJzY3JpcHRpb25zXHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwubmV4dCgpO1xyXG4gICAgICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWFyY2hcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgc2VhcmNoKGV2ZW50KTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gZXZlbnQudGFyZ2V0LnZhbHVlLnRvTG93ZXJDYXNlKCk7XHJcblxyXG4gICAgICAgIGlmICggdmFsdWUgPT09ICcnIClcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoaW5nID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsdGVyZWROYXZpZ2F0aW9uSXRlbXMgPSB0aGlzLm5hdmlnYXRpb25JdGVtcztcclxuXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2VhcmNoaW5nID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5maWx0ZXJlZE5hdmlnYXRpb25JdGVtcyA9IHRoaXMubmF2aWdhdGlvbkl0ZW1zLmZpbHRlcigobmF2aWdhdGlvbkl0ZW0pID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIG5hdmlnYXRpb25JdGVtLnRpdGxlLnRvTG93ZXJDYXNlKCkuaW5jbHVkZXModmFsdWUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVG9nZ2xlIHNob3J0Y3V0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKiBAcGFyYW0gaXRlbVRvVG9nZ2xlXHJcbiAgICAgKi9cclxuICAgIHRvZ2dsZVNob3J0Y3V0KGV2ZW50LCBpdGVtVG9Ub2dnbGUpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgIGZvciAoIGxldCBpID0gMDsgaSA8IHRoaXMuc2hvcnRjdXRJdGVtcy5sZW5ndGg7IGkrKyApXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZiAoIHRoaXMuc2hvcnRjdXRJdGVtc1tpXS51cmwgPT09IGl0ZW1Ub1RvZ2dsZS51cmwgKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNob3J0Y3V0SXRlbXMuc3BsaWNlKGksIDEpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFNhdmUgdG8gdGhlIGNvb2tpZXNcclxuICAgICAgICAgICAgICAgIHRoaXMuX2Nvb2tpZVNlcnZpY2Uuc2V0KCdGVVNFMi5zaG9ydGN1dHMnLCBKU09OLnN0cmluZ2lmeSh0aGlzLnNob3J0Y3V0SXRlbXMpKTtcclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc2hvcnRjdXRJdGVtcy5wdXNoKGl0ZW1Ub1RvZ2dsZSk7XHJcblxyXG4gICAgICAgIC8vIFNhdmUgdG8gdGhlIGNvb2tpZXNcclxuICAgICAgICB0aGlzLl9jb29raWVTZXJ2aWNlLnNldCgnRlVTRTIuc2hvcnRjdXRzJywgSlNPTi5zdHJpbmdpZnkodGhpcy5zaG9ydGN1dEl0ZW1zKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJcyBpbiBzaG9ydGN1dHM/XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIG5hdmlnYXRpb25JdGVtXHJcbiAgICAgKiBAcmV0dXJucyB7YW55fVxyXG4gICAgICovXHJcbiAgICBpc0luU2hvcnRjdXRzKG5hdmlnYXRpb25JdGVtKTogYW55XHJcbiAgICB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hvcnRjdXRJdGVtcy5maW5kKGl0ZW0gPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gaXRlbS51cmwgPT09IG5hdmlnYXRpb25JdGVtLnVybDtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIG1lbnUgb3BlblxyXG4gICAgICovXHJcbiAgICBvbk1lbnVPcGVuKCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zZWFyY2hJbnB1dEZpZWxkLm5hdGl2ZUVsZW1lbnQuZm9jdXMoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNob3cgbW9iaWxlIHNob3J0Y3V0c1xyXG4gICAgICovXHJcbiAgICBzaG93TW9iaWxlU2hvcnRjdXRzUGFuZWwoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIHRoaXMubW9iaWxlU2hvcnRjdXRzUGFuZWxBY3RpdmUgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuc2hvcnRjdXRzRWwubmF0aXZlRWxlbWVudCwgJ3Nob3ctbW9iaWxlLXBhbmVsJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBIaWRlIG1vYmlsZSBzaG9ydGN1dHNcclxuICAgICAqL1xyXG4gICAgaGlkZU1vYmlsZVNob3J0Y3V0c1BhbmVsKCk6IHZvaWRcclxuICAgIHtcclxuICAgICAgICB0aGlzLm1vYmlsZVNob3J0Y3V0c1BhbmVsQWN0aXZlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5zaG9ydGN1dHNFbC5uYXRpdmVFbGVtZW50LCAnc2hvdy1tb2JpbGUtcGFuZWwnKTtcclxuICAgIH1cclxufVxyXG4iXX0=