import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";
import * as _ from "lodash";
import * as i0 from "@angular/core";
export class FuseNavigationService {
    /**
     * Constructor
     */
    constructor() {
        this._registry = {};
        // Set the defaults
        this.onItemCollapsed = new Subject();
        this.onItemCollapseToggled = new Subject();
        // Set the private defaults
        this._currentNavigationKey = null;
        this._onNavigationChanged = new BehaviorSubject(null);
        this._onNavigationRegistered = new BehaviorSubject(null);
        this._onNavigationUnregistered = new BehaviorSubject(null);
        this._onNavigationItemAdded = new BehaviorSubject(null);
        this._onNavigationItemUpdated = new BehaviorSubject(null);
        this._onNavigationItemRemoved = new BehaviorSubject(null);
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Get onNavigationChanged
     *
     * @returns {Observable<any>}
     */
    get onNavigationChanged() {
        return this._onNavigationChanged.asObservable();
    }
    /**
     * Get onNavigationRegistered
     *
     * @returns {Observable<any>}
     */
    get onNavigationRegistered() {
        return this._onNavigationRegistered.asObservable();
    }
    /**
     * Get onNavigationUnregistered
     *
     * @returns {Observable<any>}
     */
    get onNavigationUnregistered() {
        return this._onNavigationUnregistered.asObservable();
    }
    /**
     * Get onNavigationItemAdded
     *
     * @returns {Observable<any>}
     */
    get onNavigationItemAdded() {
        return this._onNavigationItemAdded.asObservable();
    }
    /**
     * Get onNavigationItemUpdated
     *
     * @returns {Observable<any>}
     */
    get onNavigationItemUpdated() {
        return this._onNavigationItemUpdated.asObservable();
    }
    /**
     * Get onNavigationItemRemoved
     *
     * @returns {Observable<any>}
     */
    get onNavigationItemRemoved() {
        return this._onNavigationItemRemoved.asObservable();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Register the given navigation
     * with the given key
     *
     * @param key
     * @param navigation
     */
    register(key, navigation) {
        // Check if the key already being used
        if (this._registry[key]) {
            console.error(`The navigation with the key '${key}' already exists. Either unregister it first or use a unique key.`);
            return;
        }
        // Add to the registry
        this._registry[key] = navigation;
        // Notify the subject
        this._onNavigationRegistered.next([key, navigation]);
    }
    /**
     * Unregister the navigation from the registry
     * @param key
     */
    unregister(key) {
        // Check if the navigation exists
        if (!this._registry[key]) {
            console.warn(`The navigation with the key '${key}' doesn't exist in the registry.`);
        }
        // Unregister the sidebar
        delete this._registry[key];
        // Notify the subject
        this._onNavigationUnregistered.next(key);
    }
    /**
     * Get navigation from registry by key
     *
     * @param key
     * @returns {any}
     */
    getNavigation(key) {
        // Check if the navigation exists
        if (!this._registry[key]) {
            console.warn(`The navigation with the key '${key}' doesn't exist in the registry.`);
            return;
        }
        // Return the sidebar
        return this._registry[key];
    }
    /**
     * Get flattened navigation array
     *
     * @param navigation
     * @param flatNavigation
     * @returns {any[]}
     */
    getFlatNavigation(navigation, flatNavigation = []) {
        for (const item of navigation) {
            if (item.type === "item") {
                flatNavigation.push(item);
                continue;
            }
            if (item.type === "collapsable" || item.type === "group") {
                if (item.children) {
                    this.getFlatNavigation(item.children, flatNavigation);
                }
            }
        }
        return flatNavigation;
    }
    /**
     * Get the current navigation
     *
     * @returns {any}
     */
    getCurrentNavigation() {
        if (!this._currentNavigationKey) {
            console.warn(`The current navigation is not set.`);
            return;
        }
        return this.getNavigation(this._currentNavigationKey);
    }
    /**
     * Set the navigation with the key
     * as the current navigation
     *
     * @param key
     */
    setCurrentNavigation(key) {
        // Check if the sidebar exists
        if (!this._registry[key]) {
            console.warn(`The navigation with the key '${key}' doesn't exist in the registry.`);
            return;
        }
        // Set the current navigation key
        this._currentNavigationKey = key;
        // Notify the subject
        this._onNavigationChanged.next(key);
    }
    /**
     * Get navigation item by id from the
     * current navigation
     *
     * @param id
     * @param {any} navigation
     * @returns {any | boolean}
     */
    getNavigationItem(id, navigation = null) {
        if (!navigation) {
            navigation = this.getCurrentNavigation();
        }
        for (const item of navigation) {
            if (item.id === id) {
                return item;
            }
            if (item.children) {
                const childItem = this.getNavigationItem(id, item.children);
                if (childItem) {
                    return childItem;
                }
            }
        }
        return false;
    }
    /**
     * Get the parent of the navigation item
     * with the id
     *
     * @param id
     * @param {any} navigation
     * @param parent
     */
    getNavigationItemParent(id, navigation = null, parent = null) {
        if (!navigation) {
            navigation = this.getCurrentNavigation();
            parent = navigation;
        }
        for (const item of navigation) {
            if (item.id === id) {
                return parent;
            }
            if (item.children) {
                const childItem = this.getNavigationItemParent(id, item.children, item);
                if (childItem) {
                    return childItem;
                }
            }
        }
        return false;
    }
    /**
     * Add a navigation item to the specified location
     *
     * @param item
     * @param id
     */
    addNavigationItem(item, id) {
        // Get the current navigation
        const navigation = this.getCurrentNavigation();
        // Add to the end of the navigation
        if (id === "end") {
            navigation.push(item);
            // Trigger the observable
            this._onNavigationItemAdded.next(true);
            return;
        }
        // Add to the start of the navigation
        if (id === "start") {
            navigation.unshift(item);
            // Trigger the observable
            this._onNavigationItemAdded.next(true);
            return;
        }
        // Add it to a specific location
        const parent = this.getNavigationItem(id);
        if (parent) {
            // Check if parent has a children entry,
            // and add it if it doesn't
            if (!parent.children) {
                parent.children = [];
            }
            // Add the item
            parent.children.push(item);
        }
        // Trigger the observable
        this._onNavigationItemAdded.next(true);
    }
    /**
     * Update navigation item with the given id
     *
     * @param id
     * @param properties
     */
    updateNavigationItem(id, properties) {
        // Get the navigation item
        const navigationItem = this.getNavigationItem(id);
        // If there is no navigation with the give id, return
        if (!navigationItem) {
            return;
        }
        // Merge the navigation properties
        _.merge(navigationItem, properties);
        // Trigger the observable
        this._onNavigationItemUpdated.next(true);
    }
    /**
     * Remove navigation item with the given id
     *
     * @param id
     */
    removeNavigationItem(id) {
        const item = this.getNavigationItem(id);
        // Return, if there is not such an item
        if (!item) {
            return;
        }
        // Get the parent of the item
        let parent = this.getNavigationItemParent(id);
        // This check is required because of the first level
        // of the navigation, since the first level is not
        // inside the 'children' array
        parent = parent.children || parent;
        // Remove the item
        parent.splice(parent.indexOf(item), 1);
        // Trigger the observable
        this._onNavigationItemRemoved.next(true);
    }
}
FuseNavigationService.decorators = [
    { type: Injectable, args: [{
                providedIn: "root"
            },] }
];
/** @nocollapse */
FuseNavigationService.ctorParameters = () => [];
FuseNavigationService.ngInjectableDef = i0.defineInjectable({ factory: function FuseNavigationService_Factory() { return new FuseNavigationService(); }, token: FuseNavigationService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZUFBZSxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQzs7QUFPNUIsTUFBTSxPQUFPLHFCQUFxQjtJQWVoQzs7T0FFRztJQUNIO1FBTFEsY0FBUyxHQUEyQixFQUFFLENBQUM7UUFNN0MsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNyQyxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUUzQywyQkFBMkI7UUFDM0IsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyx5QkFBeUIsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzFELElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1RCxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLGNBQWM7SUFDZCx3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILElBQUksbUJBQW1CO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsSUFBSSxzQkFBc0I7UUFDeEIsT0FBTyxJQUFJLENBQUMsdUJBQXVCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDckQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxJQUFJLHdCQUF3QjtRQUMxQixPQUFPLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILElBQUkscUJBQXFCO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BELENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsSUFBSSx1QkFBdUI7UUFDekIsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDdEQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxJQUFJLHVCQUF1QjtRQUN6QixPQUFPLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN0RCxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7OztPQU1HO0lBQ0gsUUFBUSxDQUFDLEdBQUcsRUFBRSxVQUFVO1FBQ3RCLHNDQUFzQztRQUN0QyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDdkIsT0FBTyxDQUFDLEtBQUssQ0FDWCxnQ0FBZ0MsR0FBRyxtRUFBbUUsQ0FDdkcsQ0FBQztZQUVGLE9BQU87U0FDUjtRQUVELHNCQUFzQjtRQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUVqQyxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7O09BR0c7SUFDSCxVQUFVLENBQUMsR0FBRztRQUNaLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUN4QixPQUFPLENBQUMsSUFBSSxDQUNWLGdDQUFnQyxHQUFHLGtDQUFrQyxDQUN0RSxDQUFDO1NBQ0g7UUFFRCx5QkFBeUI7UUFDekIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBRTNCLHFCQUFxQjtRQUNyQixJQUFJLENBQUMseUJBQXlCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILGFBQWEsQ0FBQyxHQUFHO1FBQ2YsaUNBQWlDO1FBQ2pDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQ1YsZ0NBQWdDLEdBQUcsa0NBQWtDLENBQ3RFLENBQUM7WUFFRixPQUFPO1NBQ1I7UUFFRCxxQkFBcUI7UUFDckIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFFRDs7Ozs7O09BTUc7SUFDSCxpQkFBaUIsQ0FDZixVQUFVLEVBQ1YsaUJBQXVDLEVBQUU7UUFFekMsS0FBSyxNQUFNLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDN0IsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtnQkFDeEIsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFMUIsU0FBUzthQUNWO1lBRUQsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGFBQWEsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLE9BQU8sRUFBRTtnQkFDeEQsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNqQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQztpQkFDdkQ7YUFDRjtTQUNGO1FBRUQsT0FBTyxjQUFjLENBQUM7SUFDeEIsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxvQkFBb0I7UUFDbEIsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUMvQixPQUFPLENBQUMsSUFBSSxDQUFDLG9DQUFvQyxDQUFDLENBQUM7WUFFbkQsT0FBTztTQUNSO1FBRUQsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRDs7Ozs7T0FLRztJQUNILG9CQUFvQixDQUFDLEdBQUc7UUFDdEIsOEJBQThCO1FBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3hCLE9BQU8sQ0FBQyxJQUFJLENBQ1YsZ0NBQWdDLEdBQUcsa0NBQWtDLENBQ3RFLENBQUM7WUFFRixPQUFPO1NBQ1I7UUFFRCxpQ0FBaUM7UUFDakMsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEdBQUcsQ0FBQztRQUVqQyxxQkFBcUI7UUFDckIsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQ7Ozs7Ozs7T0FPRztJQUNILGlCQUFpQixDQUFDLEVBQUUsRUFBRSxVQUFVLEdBQUcsSUFBSTtRQUNyQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2YsVUFBVSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBQzFDO1FBRUQsS0FBSyxNQUFNLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDN0IsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDbEIsT0FBTyxJQUFJLENBQUM7YUFDYjtZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBRTVELElBQUksU0FBUyxFQUFFO29CQUNiLE9BQU8sU0FBUyxDQUFDO2lCQUNsQjthQUNGO1NBQ0Y7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNmLENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ0gsdUJBQXVCLENBQUMsRUFBRSxFQUFFLFVBQVUsR0FBRyxJQUFJLEVBQUUsTUFBTSxHQUFHLElBQUk7UUFDMUQsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNmLFVBQVUsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUN6QyxNQUFNLEdBQUcsVUFBVSxDQUFDO1NBQ3JCO1FBRUQsS0FBSyxNQUFNLElBQUksSUFBSSxVQUFVLEVBQUU7WUFDN0IsSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsRUFBRTtnQkFDbEIsT0FBTyxNQUFNLENBQUM7YUFDZjtZQUVELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakIsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUV4RSxJQUFJLFNBQVMsRUFBRTtvQkFDYixPQUFPLFNBQVMsQ0FBQztpQkFDbEI7YUFDRjtTQUNGO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsRUFBRTtRQUN4Qiw2QkFBNkI7UUFDN0IsTUFBTSxVQUFVLEdBQVUsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFFdEQsbUNBQW1DO1FBQ25DLElBQUksRUFBRSxLQUFLLEtBQUssRUFBRTtZQUNoQixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXRCLHlCQUF5QjtZQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXZDLE9BQU87U0FDUjtRQUVELHFDQUFxQztRQUNyQyxJQUFJLEVBQUUsS0FBSyxPQUFPLEVBQUU7WUFDbEIsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV6Qix5QkFBeUI7WUFDekIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUV2QyxPQUFPO1NBQ1I7UUFFRCxnQ0FBZ0M7UUFDaEMsTUFBTSxNQUFNLEdBQVEsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRS9DLElBQUksTUFBTSxFQUFFO1lBQ1Ysd0NBQXdDO1lBQ3hDLDJCQUEyQjtZQUMzQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFDcEIsTUFBTSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7YUFDdEI7WUFFRCxlQUFlO1lBQ2YsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDNUI7UUFFRCx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCxvQkFBb0IsQ0FBQyxFQUFFLEVBQUUsVUFBVTtRQUNqQywwQkFBMEI7UUFDMUIsTUFBTSxjQUFjLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRWxELHFEQUFxRDtRQUNyRCxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ25CLE9BQU87U0FDUjtRQUVELGtDQUFrQztRQUNsQyxDQUFDLENBQUMsS0FBSyxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUVwQyx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBRUQ7Ozs7T0FJRztJQUNILG9CQUFvQixDQUFDLEVBQUU7UUFDckIsTUFBTSxJQUFJLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBRXhDLHVDQUF1QztRQUN2QyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1QsT0FBTztTQUNSO1FBRUQsNkJBQTZCO1FBQzdCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUU5QyxvREFBb0Q7UUFDcEQsa0RBQWtEO1FBQ2xELDhCQUE4QjtRQUM5QixNQUFNLEdBQUcsTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUM7UUFFbkMsa0JBQWtCO1FBQ2xCLE1BQU0sQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUV2Qyx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMzQyxDQUFDOzs7WUFoWUYsVUFBVSxTQUFDO2dCQUNWLFVBQVUsRUFBRSxNQUFNO2FBQ25CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gXCJyeGpzXCI7XHJcbmltcG9ydCAqIGFzIF8gZnJvbSBcImxvZGFzaFwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25JdGVtIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL3R5cGVzXCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogXCJyb290XCJcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB7XHJcbiAgb25JdGVtQ29sbGFwc2VkOiBTdWJqZWN0PGFueT47XHJcbiAgb25JdGVtQ29sbGFwc2VUb2dnbGVkOiBTdWJqZWN0PGFueT47XHJcblxyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF9vbk5hdmlnYXRpb25DaGFuZ2VkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICBwcml2YXRlIF9vbk5hdmlnYXRpb25SZWdpc3RlcmVkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICBwcml2YXRlIF9vbk5hdmlnYXRpb25VbnJlZ2lzdGVyZWQ6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gIHByaXZhdGUgX29uTmF2aWdhdGlvbkl0ZW1BZGRlZDogQmVoYXZpb3JTdWJqZWN0PGFueT47XHJcbiAgcHJpdmF0ZSBfb25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQ6IEJlaGF2aW9yU3ViamVjdDxhbnk+O1xyXG4gIHByaXZhdGUgX29uTmF2aWdhdGlvbkl0ZW1SZW1vdmVkOiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuXHJcbiAgcHJpdmF0ZSBfY3VycmVudE5hdmlnYXRpb25LZXk6IHN0cmluZztcclxuICBwcml2YXRlIF9yZWdpc3RyeTogeyBba2V5OiBzdHJpbmddOiBhbnkgfSA9IHt9O1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgLy8gU2V0IHRoZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5vbkl0ZW1Db2xsYXBzZWQgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgdGhpcy5vbkl0ZW1Db2xsYXBzZVRvZ2dsZWQgPSBuZXcgU3ViamVjdCgpO1xyXG5cclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fY3VycmVudE5hdmlnYXRpb25LZXkgPSBudWxsO1xyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uQ2hhbmdlZCA9IG5ldyBCZWhhdmlvclN1YmplY3QobnVsbCk7XHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25SZWdpc3RlcmVkID0gbmV3IEJlaGF2aW9yU3ViamVjdChudWxsKTtcclxuICAgIHRoaXMuX29uTmF2aWdhdGlvblVucmVnaXN0ZXJlZCA9IG5ldyBCZWhhdmlvclN1YmplY3QobnVsbCk7XHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtQWRkZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbVJlbW92ZWQgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KG51bGwpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIEFjY2Vzc29yc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25DaGFuZ2VkXHJcbiAgICpcclxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAqL1xyXG4gIGdldCBvbk5hdmlnYXRpb25DaGFuZ2VkKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fb25OYXZpZ2F0aW9uQ2hhbmdlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25SZWdpc3RlcmVkXHJcbiAgICpcclxuICAgKiBAcmV0dXJucyB7T2JzZXJ2YWJsZTxhbnk+fVxyXG4gICAqL1xyXG4gIGdldCBvbk5hdmlnYXRpb25SZWdpc3RlcmVkKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5fb25OYXZpZ2F0aW9uUmVnaXN0ZXJlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25VbnJlZ2lzdGVyZWRcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICovXHJcbiAgZ2V0IG9uTmF2aWdhdGlvblVucmVnaXN0ZXJlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX29uTmF2aWdhdGlvblVucmVnaXN0ZXJlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25JdGVtQWRkZWRcclxuICAgKlxyXG4gICAqIEByZXR1cm5zIHtPYnNlcnZhYmxlPGFueT59XHJcbiAgICovXHJcbiAgZ2V0IG9uTmF2aWdhdGlvbkl0ZW1BZGRlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX29uTmF2aWdhdGlvbkl0ZW1BZGRlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25JdGVtVXBkYXRlZFxyXG4gICAqXHJcbiAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgKi9cclxuICBnZXQgb25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiB0aGlzLl9vbk5hdmlnYXRpb25JdGVtVXBkYXRlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCBvbk5hdmlnYXRpb25JdGVtUmVtb3ZlZFxyXG4gICAqXHJcbiAgICogQHJldHVybnMge09ic2VydmFibGU8YW55Pn1cclxuICAgKi9cclxuICBnZXQgb25OYXZpZ2F0aW9uSXRlbVJlbW92ZWQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiB0aGlzLl9vbk5hdmlnYXRpb25JdGVtUmVtb3ZlZC5hc09ic2VydmFibGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIFJlZ2lzdGVyIHRoZSBnaXZlbiBuYXZpZ2F0aW9uXHJcbiAgICogd2l0aCB0aGUgZ2l2ZW4ga2V5XHJcbiAgICpcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICogQHBhcmFtIG5hdmlnYXRpb25cclxuICAgKi9cclxuICByZWdpc3RlcihrZXksIG5hdmlnYXRpb24pOiB2b2lkIHtcclxuICAgIC8vIENoZWNrIGlmIHRoZSBrZXkgYWxyZWFkeSBiZWluZyB1c2VkXHJcbiAgICBpZiAodGhpcy5fcmVnaXN0cnlba2V5XSkge1xyXG4gICAgICBjb25zb2xlLmVycm9yKFxyXG4gICAgICAgIGBUaGUgbmF2aWdhdGlvbiB3aXRoIHRoZSBrZXkgJyR7a2V5fScgYWxyZWFkeSBleGlzdHMuIEVpdGhlciB1bnJlZ2lzdGVyIGl0IGZpcnN0IG9yIHVzZSBhIHVuaXF1ZSBrZXkuYFxyXG4gICAgICApO1xyXG5cclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFkZCB0byB0aGUgcmVnaXN0cnlcclxuICAgIHRoaXMuX3JlZ2lzdHJ5W2tleV0gPSBuYXZpZ2F0aW9uO1xyXG5cclxuICAgIC8vIE5vdGlmeSB0aGUgc3ViamVjdFxyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uUmVnaXN0ZXJlZC5uZXh0KFtrZXksIG5hdmlnYXRpb25dKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFVucmVnaXN0ZXIgdGhlIG5hdmlnYXRpb24gZnJvbSB0aGUgcmVnaXN0cnlcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICovXHJcbiAgdW5yZWdpc3RlcihrZXkpOiB2b2lkIHtcclxuICAgIC8vIENoZWNrIGlmIHRoZSBuYXZpZ2F0aW9uIGV4aXN0c1xyXG4gICAgaWYgKCF0aGlzLl9yZWdpc3RyeVtrZXldKSB7XHJcbiAgICAgIGNvbnNvbGUud2FybihcclxuICAgICAgICBgVGhlIG5hdmlnYXRpb24gd2l0aCB0aGUga2V5ICcke2tleX0nIGRvZXNuJ3QgZXhpc3QgaW4gdGhlIHJlZ2lzdHJ5LmBcclxuICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBVbnJlZ2lzdGVyIHRoZSBzaWRlYmFyXHJcbiAgICBkZWxldGUgdGhpcy5fcmVnaXN0cnlba2V5XTtcclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHN1YmplY3RcclxuICAgIHRoaXMuX29uTmF2aWdhdGlvblVucmVnaXN0ZXJlZC5uZXh0KGtleSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgbmF2aWdhdGlvbiBmcm9tIHJlZ2lzdHJ5IGJ5IGtleVxyXG4gICAqXHJcbiAgICogQHBhcmFtIGtleVxyXG4gICAqIEByZXR1cm5zIHthbnl9XHJcbiAgICovXHJcbiAgZ2V0TmF2aWdhdGlvbihrZXkpOiBhbnkge1xyXG4gICAgLy8gQ2hlY2sgaWYgdGhlIG5hdmlnYXRpb24gZXhpc3RzXHJcbiAgICBpZiAoIXRoaXMuX3JlZ2lzdHJ5W2tleV0pIHtcclxuICAgICAgY29uc29sZS53YXJuKFxyXG4gICAgICAgIGBUaGUgbmF2aWdhdGlvbiB3aXRoIHRoZSBrZXkgJyR7a2V5fScgZG9lc24ndCBleGlzdCBpbiB0aGUgcmVnaXN0cnkuYFxyXG4gICAgICApO1xyXG5cclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFJldHVybiB0aGUgc2lkZWJhclxyXG4gICAgcmV0dXJuIHRoaXMuX3JlZ2lzdHJ5W2tleV07XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgZmxhdHRlbmVkIG5hdmlnYXRpb24gYXJyYXlcclxuICAgKlxyXG4gICAqIEBwYXJhbSBuYXZpZ2F0aW9uXHJcbiAgICogQHBhcmFtIGZsYXROYXZpZ2F0aW9uXHJcbiAgICogQHJldHVybnMge2FueVtdfVxyXG4gICAqL1xyXG4gIGdldEZsYXROYXZpZ2F0aW9uKFxyXG4gICAgbmF2aWdhdGlvbixcclxuICAgIGZsYXROYXZpZ2F0aW9uOiBGdXNlTmF2aWdhdGlvbkl0ZW1bXSA9IFtdXHJcbiAgKTogYW55IHtcclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBuYXZpZ2F0aW9uKSB7XHJcbiAgICAgIGlmIChpdGVtLnR5cGUgPT09IFwiaXRlbVwiKSB7XHJcbiAgICAgICAgZmxhdE5hdmlnYXRpb24ucHVzaChpdGVtKTtcclxuXHJcbiAgICAgICAgY29udGludWU7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLnR5cGUgPT09IFwiY29sbGFwc2FibGVcIiB8fCBpdGVtLnR5cGUgPT09IFwiZ3JvdXBcIikge1xyXG4gICAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XHJcbiAgICAgICAgICB0aGlzLmdldEZsYXROYXZpZ2F0aW9uKGl0ZW0uY2hpbGRyZW4sIGZsYXROYXZpZ2F0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gZmxhdE5hdmlnYXRpb247XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBHZXQgdGhlIGN1cnJlbnQgbmF2aWdhdGlvblxyXG4gICAqXHJcbiAgICogQHJldHVybnMge2FueX1cclxuICAgKi9cclxuICBnZXRDdXJyZW50TmF2aWdhdGlvbigpOiBhbnkge1xyXG4gICAgaWYgKCF0aGlzLl9jdXJyZW50TmF2aWdhdGlvbktleSkge1xyXG4gICAgICBjb25zb2xlLndhcm4oYFRoZSBjdXJyZW50IG5hdmlnYXRpb24gaXMgbm90IHNldC5gKTtcclxuXHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICByZXR1cm4gdGhpcy5nZXROYXZpZ2F0aW9uKHRoaXMuX2N1cnJlbnROYXZpZ2F0aW9uS2V5KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNldCB0aGUgbmF2aWdhdGlvbiB3aXRoIHRoZSBrZXlcclxuICAgKiBhcyB0aGUgY3VycmVudCBuYXZpZ2F0aW9uXHJcbiAgICpcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICovXHJcbiAgc2V0Q3VycmVudE5hdmlnYXRpb24oa2V5KTogdm9pZCB7XHJcbiAgICAvLyBDaGVjayBpZiB0aGUgc2lkZWJhciBleGlzdHNcclxuICAgIGlmICghdGhpcy5fcmVnaXN0cnlba2V5XSkge1xyXG4gICAgICBjb25zb2xlLndhcm4oXHJcbiAgICAgICAgYFRoZSBuYXZpZ2F0aW9uIHdpdGggdGhlIGtleSAnJHtrZXl9JyBkb2Vzbid0IGV4aXN0IGluIHRoZSByZWdpc3RyeS5gXHJcbiAgICAgICk7XHJcblxyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gU2V0IHRoZSBjdXJyZW50IG5hdmlnYXRpb24ga2V5XHJcbiAgICB0aGlzLl9jdXJyZW50TmF2aWdhdGlvbktleSA9IGtleTtcclxuXHJcbiAgICAvLyBOb3RpZnkgdGhlIHN1YmplY3RcclxuICAgIHRoaXMuX29uTmF2aWdhdGlvbkNoYW5nZWQubmV4dChrZXkpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogR2V0IG5hdmlnYXRpb24gaXRlbSBieSBpZCBmcm9tIHRoZVxyXG4gICAqIGN1cnJlbnQgbmF2aWdhdGlvblxyXG4gICAqXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICogQHBhcmFtIHthbnl9IG5hdmlnYXRpb25cclxuICAgKiBAcmV0dXJucyB7YW55IHwgYm9vbGVhbn1cclxuICAgKi9cclxuICBnZXROYXZpZ2F0aW9uSXRlbShpZCwgbmF2aWdhdGlvbiA9IG51bGwpOiBhbnkgfCBib29sZWFuIHtcclxuICAgIGlmICghbmF2aWdhdGlvbikge1xyXG4gICAgICBuYXZpZ2F0aW9uID0gdGhpcy5nZXRDdXJyZW50TmF2aWdhdGlvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBuYXZpZ2F0aW9uKSB7XHJcbiAgICAgIGlmIChpdGVtLmlkID09PSBpZCkge1xyXG4gICAgICAgIHJldHVybiBpdGVtO1xyXG4gICAgICB9XHJcblxyXG4gICAgICBpZiAoaXRlbS5jaGlsZHJlbikge1xyXG4gICAgICAgIGNvbnN0IGNoaWxkSXRlbSA9IHRoaXMuZ2V0TmF2aWdhdGlvbkl0ZW0oaWQsIGl0ZW0uY2hpbGRyZW4pO1xyXG5cclxuICAgICAgICBpZiAoY2hpbGRJdGVtKSB7XHJcbiAgICAgICAgICByZXR1cm4gY2hpbGRJdGVtO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIEdldCB0aGUgcGFyZW50IG9mIHRoZSBuYXZpZ2F0aW9uIGl0ZW1cclxuICAgKiB3aXRoIHRoZSBpZFxyXG4gICAqXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICogQHBhcmFtIHthbnl9IG5hdmlnYXRpb25cclxuICAgKiBAcGFyYW0gcGFyZW50XHJcbiAgICovXHJcbiAgZ2V0TmF2aWdhdGlvbkl0ZW1QYXJlbnQoaWQsIG5hdmlnYXRpb24gPSBudWxsLCBwYXJlbnQgPSBudWxsKTogYW55IHtcclxuICAgIGlmICghbmF2aWdhdGlvbikge1xyXG4gICAgICBuYXZpZ2F0aW9uID0gdGhpcy5nZXRDdXJyZW50TmF2aWdhdGlvbigpO1xyXG4gICAgICBwYXJlbnQgPSBuYXZpZ2F0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIGZvciAoY29uc3QgaXRlbSBvZiBuYXZpZ2F0aW9uKSB7XHJcbiAgICAgIGlmIChpdGVtLmlkID09PSBpZCkge1xyXG4gICAgICAgIHJldHVybiBwYXJlbnQ7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGlmIChpdGVtLmNoaWxkcmVuKSB7XHJcbiAgICAgICAgY29uc3QgY2hpbGRJdGVtID0gdGhpcy5nZXROYXZpZ2F0aW9uSXRlbVBhcmVudChpZCwgaXRlbS5jaGlsZHJlbiwgaXRlbSk7XHJcblxyXG4gICAgICAgIGlmIChjaGlsZEl0ZW0pIHtcclxuICAgICAgICAgIHJldHVybiBjaGlsZEl0ZW07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQWRkIGEgbmF2aWdhdGlvbiBpdGVtIHRvIHRoZSBzcGVjaWZpZWQgbG9jYXRpb25cclxuICAgKlxyXG4gICAqIEBwYXJhbSBpdGVtXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICovXHJcbiAgYWRkTmF2aWdhdGlvbkl0ZW0oaXRlbSwgaWQpOiB2b2lkIHtcclxuICAgIC8vIEdldCB0aGUgY3VycmVudCBuYXZpZ2F0aW9uXHJcbiAgICBjb25zdCBuYXZpZ2F0aW9uOiBhbnlbXSA9IHRoaXMuZ2V0Q3VycmVudE5hdmlnYXRpb24oKTtcclxuXHJcbiAgICAvLyBBZGQgdG8gdGhlIGVuZCBvZiB0aGUgbmF2aWdhdGlvblxyXG4gICAgaWYgKGlkID09PSBcImVuZFwiKSB7XHJcbiAgICAgIG5hdmlnYXRpb24ucHVzaChpdGVtKTtcclxuXHJcbiAgICAgIC8vIFRyaWdnZXIgdGhlIG9ic2VydmFibGVcclxuICAgICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbUFkZGVkLm5leHQodHJ1ZSk7XHJcblxyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQWRkIHRvIHRoZSBzdGFydCBvZiB0aGUgbmF2aWdhdGlvblxyXG4gICAgaWYgKGlkID09PSBcInN0YXJ0XCIpIHtcclxuICAgICAgbmF2aWdhdGlvbi51bnNoaWZ0KGl0ZW0pO1xyXG5cclxuICAgICAgLy8gVHJpZ2dlciB0aGUgb2JzZXJ2YWJsZVxyXG4gICAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtQWRkZWQubmV4dCh0cnVlKTtcclxuXHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBBZGQgaXQgdG8gYSBzcGVjaWZpYyBsb2NhdGlvblxyXG4gICAgY29uc3QgcGFyZW50OiBhbnkgPSB0aGlzLmdldE5hdmlnYXRpb25JdGVtKGlkKTtcclxuXHJcbiAgICBpZiAocGFyZW50KSB7XHJcbiAgICAgIC8vIENoZWNrIGlmIHBhcmVudCBoYXMgYSBjaGlsZHJlbiBlbnRyeSxcclxuICAgICAgLy8gYW5kIGFkZCBpdCBpZiBpdCBkb2Vzbid0XHJcbiAgICAgIGlmICghcGFyZW50LmNoaWxkcmVuKSB7XHJcbiAgICAgICAgcGFyZW50LmNoaWxkcmVuID0gW107XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIC8vIEFkZCB0aGUgaXRlbVxyXG4gICAgICBwYXJlbnQuY2hpbGRyZW4ucHVzaChpdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUcmlnZ2VyIHRoZSBvYnNlcnZhYmxlXHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtQWRkZWQubmV4dCh0cnVlKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFVwZGF0ZSBuYXZpZ2F0aW9uIGl0ZW0gd2l0aCB0aGUgZ2l2ZW4gaWRcclxuICAgKlxyXG4gICAqIEBwYXJhbSBpZFxyXG4gICAqIEBwYXJhbSBwcm9wZXJ0aWVzXHJcbiAgICovXHJcbiAgdXBkYXRlTmF2aWdhdGlvbkl0ZW0oaWQsIHByb3BlcnRpZXMpOiB2b2lkIHtcclxuICAgIC8vIEdldCB0aGUgbmF2aWdhdGlvbiBpdGVtXHJcbiAgICBjb25zdCBuYXZpZ2F0aW9uSXRlbSA9IHRoaXMuZ2V0TmF2aWdhdGlvbkl0ZW0oaWQpO1xyXG5cclxuICAgIC8vIElmIHRoZXJlIGlzIG5vIG5hdmlnYXRpb24gd2l0aCB0aGUgZ2l2ZSBpZCwgcmV0dXJuXHJcbiAgICBpZiAoIW5hdmlnYXRpb25JdGVtKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICAvLyBNZXJnZSB0aGUgbmF2aWdhdGlvbiBwcm9wZXJ0aWVzXHJcbiAgICBfLm1lcmdlKG5hdmlnYXRpb25JdGVtLCBwcm9wZXJ0aWVzKTtcclxuXHJcbiAgICAvLyBUcmlnZ2VyIHRoZSBvYnNlcnZhYmxlXHJcbiAgICB0aGlzLl9vbk5hdmlnYXRpb25JdGVtVXBkYXRlZC5uZXh0KHRydWUpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogUmVtb3ZlIG5hdmlnYXRpb24gaXRlbSB3aXRoIHRoZSBnaXZlbiBpZFxyXG4gICAqXHJcbiAgICogQHBhcmFtIGlkXHJcbiAgICovXHJcbiAgcmVtb3ZlTmF2aWdhdGlvbkl0ZW0oaWQpOiB2b2lkIHtcclxuICAgIGNvbnN0IGl0ZW0gPSB0aGlzLmdldE5hdmlnYXRpb25JdGVtKGlkKTtcclxuXHJcbiAgICAvLyBSZXR1cm4sIGlmIHRoZXJlIGlzIG5vdCBzdWNoIGFuIGl0ZW1cclxuICAgIGlmICghaXRlbSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgLy8gR2V0IHRoZSBwYXJlbnQgb2YgdGhlIGl0ZW1cclxuICAgIGxldCBwYXJlbnQgPSB0aGlzLmdldE5hdmlnYXRpb25JdGVtUGFyZW50KGlkKTtcclxuXHJcbiAgICAvLyBUaGlzIGNoZWNrIGlzIHJlcXVpcmVkIGJlY2F1c2Ugb2YgdGhlIGZpcnN0IGxldmVsXHJcbiAgICAvLyBvZiB0aGUgbmF2aWdhdGlvbiwgc2luY2UgdGhlIGZpcnN0IGxldmVsIGlzIG5vdFxyXG4gICAgLy8gaW5zaWRlIHRoZSAnY2hpbGRyZW4nIGFycmF5XHJcbiAgICBwYXJlbnQgPSBwYXJlbnQuY2hpbGRyZW4gfHwgcGFyZW50O1xyXG5cclxuICAgIC8vIFJlbW92ZSB0aGUgaXRlbVxyXG4gICAgcGFyZW50LnNwbGljZShwYXJlbnQuaW5kZXhPZihpdGVtKSwgMSk7XHJcblxyXG4gICAgLy8gVHJpZ2dlciB0aGUgb2JzZXJ2YWJsZVxyXG4gICAgdGhpcy5fb25OYXZpZ2F0aW9uSXRlbVJlbW92ZWQubmV4dCh0cnVlKTtcclxuICB9XHJcbn1cclxuIl19