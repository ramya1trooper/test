import { ChangeDetectorRef, Component, HostBinding, Input } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseNavigationService } from '../../../../../@fuse/components/navigation/navigation.service';
export class FuseNavVerticalGroupComponent {
    /**
     * Constructor
     */
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    constructor(_changeDetectorRef, _fuseNavigationService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this.classes = 'nav-group nav-item';
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved).pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
FuseNavVerticalGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'fuse-nav-vertical-group',
                template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n    <div class=\"group-title\" [ngClass]=\"item.classes\">\r\n        <span class=\"hint-text\" [translate]=\"item.translate\">{{ item.title }}</span>\r\n    </div>\r\n\r\n    <div class=\"group-items\">\r\n        <ng-container *ngFor=\"let item of item.children\">\r\n            <fuse-nav-vertical-group *ngIf=\"item.type=='group'\" [item]=\"item\"></fuse-nav-vertical-group>\r\n            <fuse-nav-vertical-collapsable *ngIf=\"item.type=='collapsable'\"\r\n                                           [item]=\"item\"></fuse-nav-vertical-collapsable>\r\n            <fuse-nav-vertical-item *ngIf=\"item.type=='item'\" [item]=\"item\"></fuse-nav-vertical-item>\r\n        </ng-container>\r\n    </div>\r\n\r\n</ng-container>",
                styles: [".folded:not(.unfolded) :host>.group-title{-webkit-box-align:center;align-items:center}.folded:not(.unfolded) :host>.group-title>span{opacity:0;-webkit-transition:opacity .2s;transition:opacity .2s}.folded:not(.unfolded) :host>.group-title:before{content:'';display:block;position:absolute;min-width:1.6rem;border-top:2px solid;opacity:.2}"]
            }] }
];
/** @nocollapse */
FuseNavVerticalGroupComponent.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: FuseNavigationService }
];
FuseNavVerticalGroupComponent.propDecorators = {
    classes: [{ type: HostBinding, args: ['class',] }],
    item: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi92ZXJ0aWNhbC9ncm91cC9ncm91cC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNwRyxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHM0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFPdEcsTUFBTSxPQUFPLDZCQUE2QjtJQVd0Qzs7T0FFRztJQUVIOzs7O09BSUc7SUFDSCxZQUNZLGtCQUFxQyxFQUNyQyxzQkFBNkM7UUFEN0MsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXVCO1FBbkJ6RCxZQUFPLEdBQUcsb0JBQW9CLENBQUM7UUFzQjNCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDekMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsUUFBUTtRQUVKLCtCQUErQjtRQUMvQixLQUFLLENBQ0QsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixFQUNqRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLEVBQ25ELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsQ0FDdEQsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNyQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBRVosaUJBQWlCO1lBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUMzQyxDQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7SUFFRDs7T0FFRztJQUNILFdBQVc7UUFFUCxxQ0FBcUM7UUFDckMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BDLENBQUM7OztZQWhFSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFLLHlCQUF5QjtnQkFDdEMsZ3hCQUFxQzs7YUFFeEM7Ozs7WUFYUSxpQkFBaUI7WUFLakIscUJBQXFCOzs7c0JBU3pCLFdBQVcsU0FBQyxPQUFPO21CQUduQixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0b3JSZWYsIENvbXBvbmVudCwgSG9zdEJpbmRpbmcsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBtZXJnZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBGdXNlTmF2aWdhdGlvbkl0ZW0gfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9AZnVzZS90eXBlcyc7XHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvciAgIDogJ2Z1c2UtbmF2LXZlcnRpY2FsLWdyb3VwJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9ncm91cC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHMgIDogWycuL2dyb3VwLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1c2VOYXZWZXJ0aWNhbEdyb3VwQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3lcclxue1xyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcycpXHJcbiAgICBjbGFzc2VzID0gJ25hdi1ncm91cCBuYXYtaXRlbSc7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGl0ZW06IEZ1c2VOYXZpZ2F0aW9uSXRlbTtcclxuXHJcbiAgICAvLyBQcml2YXRlXHJcbiAgICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqL1xyXG5cclxuICAgIC8qKlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB7Q2hhbmdlRGV0ZWN0b3JSZWZ9IF9jaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAgICogQHBhcmFtIHtGdXNlTmF2aWdhdGlvblNlcnZpY2V9IF9mdXNlTmF2aWdhdGlvblNlcnZpY2VcclxuICAgICAqL1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBfY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgICAgIHByaXZhdGUgX2Z1c2VOYXZpZ2F0aW9uU2VydmljZTogRnVzZU5hdmlnYXRpb25TZXJ2aWNlXHJcbiAgICApXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU2V0IHRoZSBwcml2YXRlIGRlZmF1bHRzXHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIGluaXRcclxuICAgICAqL1xyXG4gICAgbmdPbkluaXQoKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFN1YnNjcmliZSB0byBuYXZpZ2F0aW9uIGl0ZW1cclxuICAgICAgICBtZXJnZShcclxuICAgICAgICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1BZGRlZCxcclxuICAgICAgICAgICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkl0ZW1VcGRhdGVkLFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbVJlbW92ZWRcclxuICAgICAgICApLnBpcGUodGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcblxyXG4gICAgICAgICAgICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgICAgICAgICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE9uIGRlc3Ryb3lcclxuICAgICAqL1xyXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZFxyXG4gICAge1xyXG4gICAgICAgIC8vIFVuc3Vic2NyaWJlIGZyb20gYWxsIHN1YnNjcmlwdGlvbnNcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5uZXh0KCk7XHJcbiAgICAgICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19