import { ChangeDetectorRef, Component, HostBinding, Input } from '@angular/core';
import { merge, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseNavigationService } from '../../../../../@fuse/components/navigation/navigation.service';
import { Compiler } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from '../../../../../_services/index';
export class FuseNavVerticalItemComponent {
    /**
     * Constructor
     */
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    constructor(_changeDetectorRef, _fuseNavigationService, compiler, router, messageService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this.compiler = compiler;
        this.router = router;
        this.messageService = messageService;
        this.classes = 'nav-item';
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved).pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    clickToGo(selectedItem) {
        this.messageService.sendRouting(selectedItem);
    }
}
FuseNavVerticalItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'fuse-nav-vertical-item',
                template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n    <!-- item.url -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"(item.url || item.id === 'dashboard') && !item.externalUrl && !item.function\"\r\n       [routerLink]=\"[item.url]\"  (click)=\"clickToGo(item)\" [routerLinkActive]=\"['active', 'accent']\"\r\n       [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\"\r\n       [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.externalUrl -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && !item.function\"\r\n       [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.function -->\r\n    <span class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && item.function\"\r\n          (click)=\"item.function()\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </span>\r\n\r\n    <!-- item.url && item.function -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && item.function\"\r\n       (click)=\"item.function()\"\r\n       [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n       [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\"\r\n       [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <!-- item.externalUrl && item.function -->\r\n    <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && item.function\"\r\n       (click)=\"item.function()\"\r\n       [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n        <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n    </a>\r\n\r\n    <ng-template #itemContent>\r\n        <mat-icon class=\"nav-link-icon\" *ngIf=\"item.icon\">{{item.icon}}</mat-icon>\r\n        <span class=\"nav-link-title\" [translate]=\"item.translate\">{{item.title}}</span>\r\n        <span class=\"nav-link-badge\" *ngIf=\"item.badge\" [translate]=\"item.badge.translate\"\r\n              [ngStyle]=\"{'background-color': item.badge.bg,'color': item.badge.fg}\">\r\n            {{item.badge.title}}\r\n        </span>\r\n    </ng-template>\r\n    \r\n</ng-container>",
                styles: [".folded:not(.unfolded) :host .nav-link>.nav-link-badge,.folded:not(.unfolded) :host .nav-link>.nav-link-title{opacity:0;-webkit-transition:opacity .2s;transition:opacity .2s}"]
            }] }
];
/** @nocollapse */
FuseNavVerticalItemComponent.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: FuseNavigationService },
    { type: Compiler },
    { type: Router },
    { type: MessageService }
];
FuseNavVerticalItemComponent.propDecorators = {
    classes: [{ type: HostBinding, args: ['class',] }],
    item: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL3ZlcnRpY2FsL2l0ZW0vaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFxQixNQUFNLGVBQWUsQ0FBQztBQUNwRyxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHM0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFDdEcsT0FBTyxFQUFhLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQWdCLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRXZELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQVFoRSxNQUFNLE9BQU8sNEJBQTRCO0lBV3JDOztPQUVHO0lBRUg7Ozs7T0FJRztJQUNILFlBQ1ksa0JBQXFDLEVBQ3JDLHNCQUE2QyxFQUM3QyxRQUFrQixFQUNsQixNQUFjLEVBQ2QsY0FBOEI7UUFKOUIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXVCO1FBQzdDLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQXRCMUMsWUFBTyxHQUFHLFVBQVUsQ0FBQztRQTBCakIsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUN6QyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxRQUFRO1FBRUosK0JBQStCO1FBQy9CLEtBQUssQ0FDRCxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLEVBQ2pELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyx1QkFBdUIsRUFDbkQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixDQUN0RCxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQyxHQUFHLEVBQUU7WUFFWixpQkFBaUI7WUFDakIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzNDLENBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQztJQUVEOztPQUVHO0lBQ0gsV0FBVztRQUVQLHFDQUFxQztRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVELFNBQVMsQ0FBQyxZQUFZO1FBQ2xCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2xELENBQUM7OztZQXhFSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFLLHdCQUF3QjtnQkFDckMscStFQUFvQzs7YUFFdkM7Ozs7WUFoQlEsaUJBQWlCO1lBS2pCLHFCQUFxQjtZQUNWLFFBQVE7WUFDTCxNQUFNO1lBRXBCLGNBQWM7OztzQkFVbEIsV0FBVyxTQUFDLE9BQU87bUJBR25CLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDaGFuZ2VEZXRlY3RvclJlZiwgQ29tcG9uZW50LCBIb3N0QmluZGluZywgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IG1lcmdlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uSXRlbSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uL0BmdXNlL3R5cGVzJztcclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7ICBOZ01vZHVsZSwgQ29tcGlsZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9fc2VydmljZXMvaW5kZXgnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3IgICA6ICdmdXNlLW5hdi12ZXJ0aWNhbC1pdGVtJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9pdGVtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJscyAgOiBbJy4vaXRlbS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGdXNlTmF2VmVydGljYWxJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3lcclxue1xyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcycpXHJcbiAgICBjbGFzc2VzID0gJ25hdi1pdGVtJztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgaXRlbTogRnVzZU5hdmlnYXRpb25JdGVtO1xyXG5cclxuICAgIC8vIFByaXZhdGVcclxuICAgIHByaXZhdGUgX3Vuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICovXHJcblxyXG4gICAgLyoqXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHtDaGFuZ2VEZXRlY3RvclJlZn0gX2NoYW5nZURldGVjdG9yUmVmXHJcbiAgICAgKiBAcGFyYW0ge0Z1c2VOYXZpZ2F0aW9uU2VydmljZX0gX2Z1c2VOYXZpZ2F0aW9uU2VydmljZVxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9jaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICAgICAgcHJpdmF0ZSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlOiBGdXNlTmF2aWdhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBjb21waWxlcjogQ29tcGlsZXIsXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcml2YXRlIG1lc3NhZ2VTZXJ2aWNlOiBNZXNzYWdlU2VydmljZVxyXG5cclxuICAgIClcclxuICAgIHtcclxuICAgICAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gaW5pdFxyXG4gICAgICovXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gU3Vic2NyaWJlIHRvIG5hdmlnYXRpb24gaXRlbVxyXG4gICAgICAgIG1lcmdlKFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbUFkZGVkLFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQsXHJcbiAgICAgICAgICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbk5hdmlnYXRpb25JdGVtUmVtb3ZlZFxyXG4gICAgICAgICkucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuXHJcbiAgICAgICAgICAgICAvLyBNYXJrIGZvciBjaGVja1xyXG4gICAgICAgICAgICAgdGhpcy5fY2hhbmdlRGV0ZWN0b3JSZWYubWFya0ZvckNoZWNrKCk7XHJcbiAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT24gZGVzdHJveVxyXG4gICAgICovXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkXHJcbiAgICB7XHJcbiAgICAgICAgLy8gVW5zdWJzY3JpYmUgZnJvbSBhbGwgc3Vic2NyaXB0aW9uc1xyXG4gICAgICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgICAgICB0aGlzLl91bnN1YnNjcmliZUFsbC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNsaWNrVG9HbyhzZWxlY3RlZEl0ZW0pIHtcclxuICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKHNlbGVjdGVkSXRlbSk7XHJcbiAgICB9XHJcbn1cclxuIl19