import { ChangeDetectorRef, Component, HostBinding, Input } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { merge, Subject } from "rxjs";
import { filter, takeUntil } from "rxjs/operators";
import { fuseAnimations } from "../../../../../@fuse/animations";
import { FuseNavigationService } from "../../../../../@fuse/components/navigation/navigation.service";
export class FuseNavVerticalCollapsableComponent {
    /**
     * Constructor
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {Router} _router
     */
    constructor(_changeDetectorRef, _fuseNavigationService, _router) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this._router = _router;
        this.classes = "nav-collapsable nav-item";
        this.isOpen = false;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        // Listen for router events
        this._router.events
            .pipe(filter(event => event instanceof NavigationEnd), takeUntil(this._unsubscribeAll))
            .subscribe((event) => {
            // Check if the url can be found in
            // one of the children of this item
            if (this.isUrlInChildren(this.item, event.urlAfterRedirects)) {
                this.expand();
            }
            else {
                this.collapse();
            }
        });
        // Listen for collapsing of any navigation item
        this._fuseNavigationService.onItemCollapsed
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(clickedItem => {
            if (clickedItem && clickedItem.children) {
                // Check if the clicked item is one
                // of the children of this item
                if (this.isChildrenOf(this.item, clickedItem)) {
                    return;
                }
                // Check if the url can be found in
                // one of the children of this item
                if (this.isUrlInChildren(this.item, this._router.url)) {
                    return;
                }
                // If the clicked item is not this item, collapse...
                if (this.item !== clickedItem) {
                    this.collapse();
                }
            }
        });
        // Check if the url can be found in
        // one of the children of this item
        if (this.isUrlInChildren(this.item, this._router.url)) {
            this.expand();
        }
        else {
            this.collapse();
        }
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle collapse
     *
     * @param ev
     */
    toggleOpen(ev) {
        ev.preventDefault();
        this.isOpen = !this.isOpen;
        // Navigation collapse toggled...
        this._fuseNavigationService.onItemCollapsed.next(this.item);
        this._fuseNavigationService.onItemCollapseToggled.next();
    }
    /**
     * Expand the collapsable navigation
     */
    expand() {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;
        // Mark for check
        this._changeDetectorRef.markForCheck();
        this._fuseNavigationService.onItemCollapseToggled.next();
    }
    /**
     * Collapse the collapsable navigation
     */
    collapse() {
        if (!this.isOpen) {
            return;
        }
        this.isOpen = false;
        // Mark for check
        this._changeDetectorRef.markForCheck();
        this._fuseNavigationService.onItemCollapseToggled.next();
    }
    /**
     * Check if the given parent has the
     * given item in one of its children
     *
     * @param parent
     * @param item
     * @returns {boolean}
     */
    isChildrenOf(parent, item) {
        if (!parent.children) {
            return false;
        }
        if (parent.children.indexOf(item) !== -1) {
            return true;
        }
        for (const children of parent.children) {
            if (children.children) {
                return this.isChildrenOf(children, item);
            }
        }
    }
    /**
     * Check if the given url can be found
     * in one of the given parent's children
     *
     * @param parent
     * @param url
     * @returns {boolean}
     */
    isUrlInChildren(parent, url) {
        if (!parent.children) {
            return false;
        }
        for (let i = 0; i < parent.children.length; i++) {
            if (parent.children[i].children) {
                if (this.isUrlInChildren(parent.children[i], url)) {
                    return true;
                }
            }
            if (parent.children[i].url === url ||
                url.includes(parent.children[i].url)) {
                return true;
            }
        }
        return false;
    }
}
FuseNavVerticalCollapsableComponent.decorators = [
    { type: Component, args: [{
                selector: "fuse-nav-vertical-collapsable",
                template: "<ng-container *ngIf=\"!item.hidden\">\r\n\r\n  <!-- normal collapsable -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && !item.function\" (click)=\"toggleOpen($event)\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.url -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && !item.function\"\r\n    (click)=\"toggleOpen($event)\" [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n    [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.externalUrl -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && !item.function\"\r\n    (click)=\"toggleOpen($event)\" [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.function -->\r\n  <span class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"!item.url && item.function\" (click)=\"toggleOpen($event);\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </span>\r\n\r\n  <!-- item.url && item.function -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && !item.externalUrl && item.function\"\r\n    (click)=\"toggleOpen($event);\" [routerLink]=\"[item.url]\" [routerLinkActive]=\"['active', 'accent']\"\r\n    [routerLinkActiveOptions]=\"{exact: item.exactMatch || false}\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <!-- item.externalUrl && item.function -->\r\n  <a class=\"nav-link\" [ngClass]=\"item.classes\" *ngIf=\"item.url && item.externalUrl && item.function\"\r\n    (click)=\"toggleOpen($event);\" [href]=\"item.url\" [target]=\"item.openInNewTab ? '_blank' : '_self'\">\r\n    <ng-container *ngTemplateOutlet=\"itemContent\"></ng-container>\r\n  </a>\r\n\r\n  <ng-template #itemContent>\r\n    <mat-icon class=\"nav-link-icon\" *ngIf=\"item.icon\">{{item.icon}}</mat-icon>\r\n    <span class=\"nav-link-title\" [translate]=\"item.translate\">{{item.title}}</span>\r\n    <span class=\"nav-link-badge\" *ngIf=\"item.badge\" [translate]=\"item.badge.translate\"\r\n      [ngStyle]=\"{'background-color': item.badge.bg,'color': item.badge.fg}\">\r\n      {{item.badge.title}}\r\n    </span>\r\n    <mat-icon class=\"collapsable-arrow\">keyboard_arrow_right</mat-icon>\r\n  </ng-template>\r\n\r\n  <div class=\"children\" [@slideInOut]=\"isOpen\">\r\n    <ng-container *ngFor=\"let item of item.children\">\r\n      <fuse-nav-vertical-item *ngIf=\"item.type=='item'\" [item]=\"item\"></fuse-nav-vertical-item>\r\n      <fuse-nav-vertical-collapsable *ngIf=\"item.type=='collapsable'\" [item]=\"item\"></fuse-nav-vertical-collapsable>\r\n      <fuse-nav-vertical-group *ngIf=\"item.type=='group'\" [item]=\"item\"></fuse-nav-vertical-group>\r\n    </ng-container>\r\n  </div>\r\n\r\n</ng-container>\r\n",
                animations: fuseAnimations,
                styles: [".folded:not(.unfolded) :host .nav-link>span{opacity:0;-webkit-transition:opacity .2s;transition:opacity .2s}.folded:not(.unfolded) :host.open .children{display:none!important}:host .nav-link .collapsable-arrow{-webkit-transition:opacity .25s ease-in-out .1s,-webkit-transform .3s ease-in-out;transition:transform .3s ease-in-out,opacity .25s ease-in-out .1s,-webkit-transform .3s ease-in-out;-webkit-transform:rotate(0);transform:rotate(0)}:host>.children{overflow:hidden}:host.open>.nav-link .collapsable-arrow{-webkit-transform:rotate(90deg);transform:rotate(90deg)}"]
            }] }
];
/** @nocollapse */
FuseNavVerticalCollapsableComponent.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: FuseNavigationService },
    { type: Router }
];
FuseNavVerticalCollapsableComponent.propDecorators = {
    item: [{ type: Input }],
    classes: [{ type: HostBinding, args: ["class",] }],
    isOpen: [{ type: HostBinding, args: ["class.open",] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29sbGFwc2FibGUuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIkBmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi92ZXJ0aWNhbC9jb2xsYXBzYWJsZS9jb2xsYXBzYWJsZS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLGlCQUFpQixFQUNqQixTQUFTLEVBQ1QsV0FBVyxFQUNYLEtBQUssRUFHTixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxNQUFNLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFHbkQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLCtEQUErRCxDQUFDO0FBUXRHLE1BQU0sT0FBTyxtQ0FBbUM7SUFhOUM7Ozs7OztPQU1HO0lBQ0gsWUFDVSxrQkFBcUMsRUFDckMsc0JBQTZDLEVBQzdDLE9BQWU7UUFGZix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW1CO1FBQ3JDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBdUI7UUFDN0MsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQWxCekIsWUFBTyxHQUFHLDBCQUEwQixDQUFDO1FBRzlCLFdBQU0sR0FBRyxLQUFLLENBQUM7UUFpQnBCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxvQkFBb0I7SUFDcEIsd0dBQXdHO0lBRXhHOztPQUVHO0lBQ0gsUUFBUTtRQUNOLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07YUFDaEIsSUFBSSxDQUNILE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEtBQUssWUFBWSxhQUFhLENBQUMsRUFDL0MsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FDaEM7YUFDQSxTQUFTLENBQUMsQ0FBQyxLQUFvQixFQUFFLEVBQUU7WUFDbEMsbUNBQW1DO1lBQ25DLG1DQUFtQztZQUNuQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsRUFBRTtnQkFDNUQsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2FBQ2Y7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFTCwrQ0FBK0M7UUFDL0MsSUFBSSxDQUFDLHNCQUFzQixDQUFDLGVBQWU7YUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxFQUFFO1lBQ3ZCLElBQUksV0FBVyxJQUFJLFdBQVcsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3ZDLG1DQUFtQztnQkFDbkMsK0JBQStCO2dCQUMvQixJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxXQUFXLENBQUMsRUFBRTtvQkFDN0MsT0FBTztpQkFDUjtnQkFFRCxtQ0FBbUM7Z0JBQ25DLG1DQUFtQztnQkFDbkMsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtvQkFDckQsT0FBTztpQkFDUjtnQkFFRCxvREFBb0Q7Z0JBQ3BELElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxXQUFXLEVBQUU7b0JBQzdCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDakI7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsbUNBQW1DO1FBQ25DLG1DQUFtQztRQUNuQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ3JELElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNmO2FBQU07WUFDTCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7UUFFRCwrQkFBK0I7UUFDL0IsS0FBSyxDQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsRUFDakQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixFQUNuRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQ3BEO2FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNkLGlCQUFpQjtZQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxXQUFXO1FBQ1QscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7Ozs7T0FJRztJQUNILFVBQVUsQ0FBQyxFQUFFO1FBQ1gsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBRXBCLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBRTNCLGlDQUFpQztRQUNqQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQzNELENBQUM7SUFFRDs7T0FFRztJQUNILE1BQU07UUFDSixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDZixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUVuQixpQkFBaUI7UUFDakIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXZDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxRQUFRO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDaEIsT0FBTztTQUNSO1FBRUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFFcEIsaUJBQWlCO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUV2QyxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0QsQ0FBQztJQUVEOzs7Ozs7O09BT0c7SUFDSCxZQUFZLENBQUMsTUFBTSxFQUFFLElBQUk7UUFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDcEIsT0FBTyxLQUFLLENBQUM7U0FDZDtRQUVELElBQUksTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDeEMsT0FBTyxJQUFJLENBQUM7U0FDYjtRQUVELEtBQUssTUFBTSxRQUFRLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUN0QyxJQUFJLFFBQVEsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3JCLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDMUM7U0FDRjtJQUNILENBQUM7SUFFRDs7Ozs7OztPQU9HO0lBQ0gsZUFBZSxDQUFDLE1BQU0sRUFBRSxHQUFHO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ3BCLE9BQU8sS0FBSyxDQUFDO1NBQ2Q7UUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDL0MsSUFBSSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRTtnQkFDL0IsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLEVBQUU7b0JBQ2pELE9BQU8sSUFBSSxDQUFDO2lCQUNiO2FBQ0Y7WUFFRCxJQUNFLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLEdBQUc7Z0JBQzlCLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsRUFDcEM7Z0JBQ0EsT0FBTyxJQUFJLENBQUM7YUFDYjtTQUNGO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDZixDQUFDOzs7WUF6TkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSwrQkFBK0I7Z0JBQ3pDLDJrR0FBMkM7Z0JBRTNDLFVBQVUsRUFBRSxjQUFjOzthQUMzQjs7OztZQXBCQyxpQkFBaUI7WUFhVixxQkFBcUI7WUFOTixNQUFNOzs7bUJBZTNCLEtBQUs7c0JBR0wsV0FBVyxTQUFDLE9BQU87cUJBR25CLFdBQVcsU0FBQyxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDaGFuZ2VEZXRlY3RvclJlZixcclxuICBDb21wb25lbnQsXHJcbiAgSG9zdEJpbmRpbmcsXHJcbiAgSW5wdXQsXHJcbiAgT25EZXN0cm95LFxyXG4gIE9uSW5pdFxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IE5hdmlnYXRpb25FbmQsIFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgbWVyZ2UsIFN1YmplY3QgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBmaWx0ZXIsIHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5cclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25JdGVtIH0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL0BmdXNlL3R5cGVzXCI7XHJcbmltcG9ydCB7IGZ1c2VBbmltYXRpb25zIH0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL0BmdXNlL2FuaW1hdGlvbnNcIjtcclxuaW1wb3J0IHsgRnVzZU5hdmlnYXRpb25TZXJ2aWNlIH0gZnJvbSBcIi4uLy4uLy4uLy4uLy4uL0BmdXNlL2NvbXBvbmVudHMvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLnNlcnZpY2VcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImZ1c2UtbmF2LXZlcnRpY2FsLWNvbGxhcHNhYmxlXCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9jb2xsYXBzYWJsZS5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9jb2xsYXBzYWJsZS5jb21wb25lbnQuc2Nzc1wiXSxcclxuICBhbmltYXRpb25zOiBmdXNlQW5pbWF0aW9uc1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU5hdlZlcnRpY2FsQ29sbGFwc2FibGVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgQElucHV0KClcclxuICBpdGVtOiBGdXNlTmF2aWdhdGlvbkl0ZW07XHJcblxyXG4gIEBIb3N0QmluZGluZyhcImNsYXNzXCIpXHJcbiAgY2xhc3NlcyA9IFwibmF2LWNvbGxhcHNhYmxlIG5hdi1pdGVtXCI7XHJcblxyXG4gIEBIb3N0QmluZGluZyhcImNsYXNzLm9wZW5cIilcclxuICBwdWJsaWMgaXNPcGVuID0gZmFsc2U7XHJcblxyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG5cclxuICAvKipcclxuICAgKiBDb25zdHJ1Y3RvclxyXG4gICAqXHJcbiAgICogQHBhcmFtIHtDaGFuZ2VEZXRlY3RvclJlZn0gX2NoYW5nZURldGVjdG9yUmVmXHJcbiAgICogQHBhcmFtIHtGdXNlTmF2aWdhdGlvblNlcnZpY2V9IF9mdXNlTmF2aWdhdGlvblNlcnZpY2VcclxuICAgKiBAcGFyYW0ge1JvdXRlcn0gX3JvdXRlclxyXG4gICAqL1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfY2hhbmdlRGV0ZWN0b3JSZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgcHJpdmF0ZSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlOiBGdXNlTmF2aWdhdGlvblNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlclxyXG4gICkge1xyXG4gICAgLy8gU2V0IHRoZSBwcml2YXRlIGRlZmF1bHRzXHJcbiAgICB0aGlzLl91bnN1YnNjcmliZUFsbCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgTGlmZWN5Y2xlIGhvb2tzXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuXHJcbiAgLyoqXHJcbiAgICogT24gaW5pdFxyXG4gICAqL1xyXG4gIG5nT25Jbml0KCk6IHZvaWQge1xyXG4gICAgLy8gTGlzdGVuIGZvciByb3V0ZXIgZXZlbnRzXHJcbiAgICB0aGlzLl9yb3V0ZXIuZXZlbnRzXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpLFxyXG4gICAgICAgIHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbClcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKChldmVudDogTmF2aWdhdGlvbkVuZCkgPT4ge1xyXG4gICAgICAgIC8vIENoZWNrIGlmIHRoZSB1cmwgY2FuIGJlIGZvdW5kIGluXHJcbiAgICAgICAgLy8gb25lIG9mIHRoZSBjaGlsZHJlbiBvZiB0aGlzIGl0ZW1cclxuICAgICAgICBpZiAodGhpcy5pc1VybEluQ2hpbGRyZW4odGhpcy5pdGVtLCBldmVudC51cmxBZnRlclJlZGlyZWN0cykpIHtcclxuICAgICAgICAgIHRoaXMuZXhwYW5kKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuY29sbGFwc2UoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIExpc3RlbiBmb3IgY29sbGFwc2luZyBvZiBhbnkgbmF2aWdhdGlvbiBpdGVtXHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25JdGVtQ29sbGFwc2VkXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoY2xpY2tlZEl0ZW0gPT4ge1xyXG4gICAgICAgIGlmIChjbGlja2VkSXRlbSAmJiBjbGlja2VkSXRlbS5jaGlsZHJlbikge1xyXG4gICAgICAgICAgLy8gQ2hlY2sgaWYgdGhlIGNsaWNrZWQgaXRlbSBpcyBvbmVcclxuICAgICAgICAgIC8vIG9mIHRoZSBjaGlsZHJlbiBvZiB0aGlzIGl0ZW1cclxuICAgICAgICAgIGlmICh0aGlzLmlzQ2hpbGRyZW5PZih0aGlzLml0ZW0sIGNsaWNrZWRJdGVtKSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLy8gQ2hlY2sgaWYgdGhlIHVybCBjYW4gYmUgZm91bmQgaW5cclxuICAgICAgICAgIC8vIG9uZSBvZiB0aGUgY2hpbGRyZW4gb2YgdGhpcyBpdGVtXHJcbiAgICAgICAgICBpZiAodGhpcy5pc1VybEluQ2hpbGRyZW4odGhpcy5pdGVtLCB0aGlzLl9yb3V0ZXIudXJsKSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgLy8gSWYgdGhlIGNsaWNrZWQgaXRlbSBpcyBub3QgdGhpcyBpdGVtLCBjb2xsYXBzZS4uLlxyXG4gICAgICAgICAgaWYgKHRoaXMuaXRlbSAhPT0gY2xpY2tlZEl0ZW0pIHtcclxuICAgICAgICAgICAgdGhpcy5jb2xsYXBzZSgpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gQ2hlY2sgaWYgdGhlIHVybCBjYW4gYmUgZm91bmQgaW5cclxuICAgIC8vIG9uZSBvZiB0aGUgY2hpbGRyZW4gb2YgdGhpcyBpdGVtXHJcbiAgICBpZiAodGhpcy5pc1VybEluQ2hpbGRyZW4odGhpcy5pdGVtLCB0aGlzLl9yb3V0ZXIudXJsKSkge1xyXG4gICAgICB0aGlzLmV4cGFuZCgpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jb2xsYXBzZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFN1YnNjcmliZSB0byBuYXZpZ2F0aW9uIGl0ZW1cclxuICAgIG1lcmdlKFxyXG4gICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbUFkZGVkLFxyXG4gICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQsXHJcbiAgICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbk5hdmlnYXRpb25JdGVtUmVtb3ZlZFxyXG4gICAgKVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAvLyBNYXJrIGZvciBjaGVja1xyXG4gICAgICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIE9uIGRlc3Ryb3lcclxuICAgKi9cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIC8vIFVuc3Vic2NyaWJlIGZyb20gYWxsIHN1YnNjcmlwdGlvbnNcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBUb2dnbGUgY29sbGFwc2VcclxuICAgKlxyXG4gICAqIEBwYXJhbSBldlxyXG4gICAqL1xyXG4gIHRvZ2dsZU9wZW4oZXYpOiB2b2lkIHtcclxuICAgIGV2LnByZXZlbnREZWZhdWx0KCk7XHJcblxyXG4gICAgdGhpcy5pc09wZW4gPSAhdGhpcy5pc09wZW47XHJcblxyXG4gICAgLy8gTmF2aWdhdGlvbiBjb2xsYXBzZSB0b2dnbGVkLi4uXHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25JdGVtQ29sbGFwc2VkLm5leHQodGhpcy5pdGVtKTtcclxuICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbkl0ZW1Db2xsYXBzZVRvZ2dsZWQubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogRXhwYW5kIHRoZSBjb2xsYXBzYWJsZSBuYXZpZ2F0aW9uXHJcbiAgICovXHJcbiAgZXhwYW5kKCk6IHZvaWQge1xyXG4gICAgaWYgKHRoaXMuaXNPcGVuKSB7XHJcbiAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLmlzT3BlbiA9IHRydWU7XHJcblxyXG4gICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG5cclxuICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbkl0ZW1Db2xsYXBzZVRvZ2dsZWQubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ29sbGFwc2UgdGhlIGNvbGxhcHNhYmxlIG5hdmlnYXRpb25cclxuICAgKi9cclxuICBjb2xsYXBzZSgpOiB2b2lkIHtcclxuICAgIGlmICghdGhpcy5pc09wZW4pIHtcclxuICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuaXNPcGVuID0gZmFsc2U7XHJcblxyXG4gICAgLy8gTWFyayBmb3IgY2hlY2tcclxuICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG5cclxuICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbkl0ZW1Db2xsYXBzZVRvZ2dsZWQubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogQ2hlY2sgaWYgdGhlIGdpdmVuIHBhcmVudCBoYXMgdGhlXHJcbiAgICogZ2l2ZW4gaXRlbSBpbiBvbmUgb2YgaXRzIGNoaWxkcmVuXHJcbiAgICpcclxuICAgKiBAcGFyYW0gcGFyZW50XHJcbiAgICogQHBhcmFtIGl0ZW1cclxuICAgKiBAcmV0dXJucyB7Ym9vbGVhbn1cclxuICAgKi9cclxuICBpc0NoaWxkcmVuT2YocGFyZW50LCBpdGVtKTogYm9vbGVhbiB7XHJcbiAgICBpZiAoIXBhcmVudC5jaGlsZHJlbikge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHBhcmVudC5jaGlsZHJlbi5pbmRleE9mKGl0ZW0pICE9PSAtMSkge1xyXG4gICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICBmb3IgKGNvbnN0IGNoaWxkcmVuIG9mIHBhcmVudC5jaGlsZHJlbikge1xyXG4gICAgICBpZiAoY2hpbGRyZW4uY2hpbGRyZW4pIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc0NoaWxkcmVuT2YoY2hpbGRyZW4sIGl0ZW0pO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBDaGVjayBpZiB0aGUgZ2l2ZW4gdXJsIGNhbiBiZSBmb3VuZFxyXG4gICAqIGluIG9uZSBvZiB0aGUgZ2l2ZW4gcGFyZW50J3MgY2hpbGRyZW5cclxuICAgKlxyXG4gICAqIEBwYXJhbSBwYXJlbnRcclxuICAgKiBAcGFyYW0gdXJsXHJcbiAgICogQHJldHVybnMge2Jvb2xlYW59XHJcbiAgICovXHJcbiAgaXNVcmxJbkNoaWxkcmVuKHBhcmVudCwgdXJsKTogYm9vbGVhbiB7XHJcbiAgICBpZiAoIXBhcmVudC5jaGlsZHJlbikge1xyXG4gICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBwYXJlbnQuY2hpbGRyZW4ubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKHBhcmVudC5jaGlsZHJlbltpXS5jaGlsZHJlbikge1xyXG4gICAgICAgIGlmICh0aGlzLmlzVXJsSW5DaGlsZHJlbihwYXJlbnQuY2hpbGRyZW5baV0sIHVybCkpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG5cclxuICAgICAgaWYgKFxyXG4gICAgICAgIHBhcmVudC5jaGlsZHJlbltpXS51cmwgPT09IHVybCB8fFxyXG4gICAgICAgIHVybC5pbmNsdWRlcyhwYXJlbnQuY2hpbGRyZW5baV0udXJsKVxyXG4gICAgICApIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiBmYWxzZTtcclxuICB9XHJcbn1cclxuIl19