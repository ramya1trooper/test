import { AccountService } from "./../../../shared/auth/account.service";
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, ViewEncapsulation } from "@angular/core";
import { merge, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { FuseNavigationService } from "../../../@fuse/components/navigation/navigation.service";
import * as _ from "lodash";
export class FuseNavigationComponent {
    /**
     *
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseNavigationService} _fuseNavigationService
     */
    constructor(_changeDetectorRef, _fuseNavigationService, accountService) {
        this._changeDetectorRef = _changeDetectorRef;
        this._fuseNavigationService = _fuseNavigationService;
        this.accountService = accountService;
        this.layout = "vertical";
        this.currentUserRole = {};
        this.filteredMenus = [];
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        this.accountService.get().subscribe(user => {
            localStorage.setItem("currentLoginUser", JSON.stringify(user.response));
            localStorage.setItem("userId", user.response._id);
            this.currentUserRole = user.response;
            this.accountService.emitSwitchEvent(user.response);
            this.loadAll();
        });
    }
    loadAll() {
        console.log("///////");
        // Load the navigation either from the input or from the service
        this.navigation =
            this.navigation || this._fuseNavigationService.getCurrentNavigation();
        if (this.currentUserRole && this.currentUserRole.role.length && this.currentUserRole.role[0].access.includes('*')) {
            this.filteredMenus = this.navigation;
        }
        else {
            let active_access = this.currentUserRole.role[0].access; // stgging
            // let userRole=["accessGroup","accessControl","manageUsers","manageUsers"];
            let userRole = active_access;
            userRole = _.uniq(userRole);
            var self = this;
            _.forEach(this.navigation, function (item, index) {
                var childrenList = [];
                if (item.children && item.children.length) {
                    _.forEach(item.children, function (menuItem) {
                        _.forEach(userRole, function (viewItem) {
                            if (viewItem == menuItem.id || (menuItem.menuId && menuItem.menuId == viewItem)) {
                                childrenList.push(menuItem);
                            }
                        });
                    });
                }
                if ((childrenList.length > 0) || index == 0) {
                    let t1 = item;
                    t1.children = childrenList;
                    self.filteredMenus.push(t1);
                }
            });
        }
        this._fuseNavigationService.onNavigationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            // Load the navigation
            this.navigation = this._fuseNavigationService.getCurrentNavigation();
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
        // Subscribe to navigation item
        merge(this._fuseNavigationService.onNavigationItemAdded, this._fuseNavigationService.onNavigationItemUpdated, this._fuseNavigationService.onNavigationItemRemoved)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            // Mark for check
            this._changeDetectorRef.markForCheck();
        });
    }
}
FuseNavigationComponent.decorators = [
    { type: Component, args: [{
                selector: "fuse-navigation",
                template: "<div class=\"nav\" [ngClass]=\"{'horizontal':layout === 'horizontal', 'vertical':layout === 'vertical'}\">\r\n\r\n    <!-- Vertical Navigation Layout -->\r\n    <ng-container *ngIf=\"layout === 'vertical'\">\r\n\r\n        <ng-container *ngFor=\"let item of filteredMenus\">\r\n\r\n            <fuse-nav-vertical-group *ngIf=\"item.type=='group'\" [item]=\"item\"></fuse-nav-vertical-group>\r\n            <fuse-nav-vertical-collapsable *ngIf=\"item.type=='collapsable'\"\r\n                                           [item]=\"item\"></fuse-nav-vertical-collapsable>\r\n            <fuse-nav-vertical-item *ngIf=\"item.type=='item'\" [item]=\"item\"></fuse-nav-vertical-item>\r\n\r\n        </ng-container>\r\n\r\n    </ng-container>\r\n    <!-- / Vertical Navigation Layout -->\r\n\r\n    <!-- Horizontal Navigation Layout -->\r\n    <ng-container *ngIf=\"layout === 'horizontal'\">\r\n\r\n        <ng-container *ngFor=\"let item of filteredMenus\">\r\n\r\n            <fuse-nav-horizontal-collapsable *ngIf=\"item.type=='group'\" [item]=\"item\"></fuse-nav-horizontal-collapsable>\r\n            <fuse-nav-horizontal-collapsable *ngIf=\"item.type=='collapsable'\"\r\n                                             [item]=\"item\"></fuse-nav-horizontal-collapsable>\r\n            <fuse-nav-horizontal-item *ngIf=\"item.type=='item'\" [item]=\"item\"></fuse-nav-horizontal-item>\r\n\r\n        </ng-container>\r\n\r\n    </ng-container>\r\n    <!-- / Horizontal Navigation Layout -->\r\n\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                changeDetection: ChangeDetectionStrategy.OnPush,
                styles: ["fuse-navigation{display:-webkit-box;display:flex;-webkit-box-flex:1;flex:1 0 auto}fuse-navigation>.nav{margin:0;padding:0;width:100%;display:block!important}"]
            }] }
];
/** @nocollapse */
FuseNavigationComponent.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: FuseNavigationService },
    { type: AccountService }
];
FuseNavigationComponent.propDecorators = {
    layout: [{ type: Input }],
    navigation: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUN4RSxPQUFPLEVBQ0wsdUJBQXVCLEVBQ3ZCLGlCQUFpQixFQUNqQixTQUFTLEVBQ1QsS0FBSyxFQUVMLGlCQUFpQixFQUNsQixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFM0MsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seURBQXlELENBQUM7QUFDaEcsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFRNUIsTUFBTSxPQUFPLHVCQUF1QjtJQVdsQzs7OztPQUlHO0lBQ0gsWUFDVSxrQkFBcUMsRUFDckMsc0JBQTZDLEVBQzdDLGNBQThCO1FBRjlCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBbUI7UUFDckMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF1QjtRQUM3QyxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFqQnhDLFdBQU0sR0FBRyxVQUFVLENBQUM7UUFDcEIsb0JBQWUsR0FBUSxFQUFFLENBQUM7UUFJMUIsa0JBQWEsR0FBUSxFQUFFLENBQUM7UUFjdEIsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxRQUFRO1FBQ04sSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDekMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLFlBQVksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3RDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNuRCxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0MsT0FBTztRQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdkIsZ0VBQWdFO1FBQ2hFLElBQUksQ0FBQyxVQUFVO1lBQ2YsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsc0JBQXNCLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztRQUNwRSxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDakgsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ3RDO2FBQU07WUFDTCxJQUFJLGFBQWEsR0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBRSxVQUFVO1lBQ25FLDRFQUE0RTtZQUM1RSxJQUFJLFFBQVEsR0FBQyxhQUFhLENBQUM7WUFDMUIsUUFBUSxHQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDMUIsSUFBSSxJQUFJLEdBQUMsSUFBSSxDQUFDO1lBQ2QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFDLFVBQVMsSUFBUSxFQUFDLEtBQVk7Z0JBRXRELElBQUksWUFBWSxHQUFHLEVBQUUsQ0FBQztnQkFDckIsSUFBRyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUNyQztvQkFDSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBUyxRQUFhO3dCQUMxQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBQyxVQUFTLFFBQWE7NEJBRTlCLElBQUcsUUFBUSxJQUFFLFFBQVEsQ0FBQyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQyxNQUFNLElBQUUsUUFBUSxDQUFDLEVBQzFFO2dDQUNHLFlBQVksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7NkJBQzlCO3dCQUNILENBQUMsQ0FBQyxDQUFBO29CQUNmLENBQUMsQ0FBQyxDQUFBO2lCQUVSO2dCQUNELElBQUcsQ0FBQyxZQUFZLENBQUMsTUFBTSxHQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBRSxDQUFDLEVBQ3RDO29CQUNBLElBQUksRUFBRSxHQUFDLElBQUksQ0FBQztvQkFDWixFQUFFLENBQUMsUUFBUSxHQUFDLFlBQVksQ0FBQztvQkFDeEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7aUJBQzVCO1lBQ0wsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxtQkFBbUI7YUFDNUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNkLHNCQUFzQjtZQUN0QixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBRXJFLGlCQUFpQjtZQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7UUFFTCwrQkFBK0I7UUFDL0IsS0FBSyxDQUNILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxxQkFBcUIsRUFDakQsSUFBSSxDQUFDLHNCQUFzQixDQUFDLHVCQUF1QixFQUNuRCxJQUFJLENBQUMsc0JBQXNCLENBQUMsdUJBQXVCLENBQ3BEO2FBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7YUFDckMsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNkLGlCQUFpQjtZQUNqQixJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDekMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDOzs7WUExR0osU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLHcrQ0FBMEM7Z0JBRTFDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTs7YUFDaEQ7Ozs7WUFqQkMsaUJBQWlCO1lBU1YscUJBQXFCO1lBWnJCLGNBQWM7OztxQkFzQnBCLEtBQUs7eUJBSUwsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSBcIi4vLi4vLi4vLi4vc2hhcmVkL2F1dGgvYWNjb3VudC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7XHJcbiAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksXHJcbiAgQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgQ29tcG9uZW50LFxyXG4gIElucHV0LFxyXG4gIE9uSW5pdCxcclxuICBWaWV3RW5jYXBzdWxhdGlvblxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IG1lcmdlLCBTdWJqZWN0IH0gZnJvbSBcInJ4anNcIjtcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlTmF2aWdhdGlvblNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24uc2VydmljZVwiO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gXCJsb2Rhc2hcIjtcclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiZnVzZS1uYXZpZ2F0aW9uXCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL25hdmlnYXRpb24uY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaFxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU5hdmlnYXRpb25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpXHJcbiAgbGF5b3V0ID0gXCJ2ZXJ0aWNhbFwiO1xyXG4gIGN1cnJlbnRVc2VyUm9sZTogYW55ID0ge307XHJcblxyXG4gIEBJbnB1dCgpXHJcbiAgbmF2aWdhdGlvbjogYW55O1xyXG4gIGZpbHRlcmVkTWVudXMgOiBhbnkgPVtdO1xyXG4gIC8vIFByaXZhdGVcclxuICBwcml2YXRlIF91bnN1YnNjcmliZUFsbDogU3ViamVjdDxhbnk+O1xyXG5cclxuICAvKipcclxuICAgKlxyXG4gICAqIEBwYXJhbSB7Q2hhbmdlRGV0ZWN0b3JSZWZ9IF9jaGFuZ2VEZXRlY3RvclJlZlxyXG4gICAqIEBwYXJhbSB7RnVzZU5hdmlnYXRpb25TZXJ2aWNlfSBfZnVzZU5hdmlnYXRpb25TZXJ2aWNlXHJcbiAgICovXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9jaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBwcml2YXRlIF9mdXNlTmF2aWdhdGlvblNlcnZpY2U6IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSxcclxuICAgIHByaXZhdGUgYWNjb3VudFNlcnZpY2U6IEFjY291bnRTZXJ2aWNlXHJcbiAgKSB7XHJcbiAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBPbiBpbml0XHJcbiAgICovXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLmFjY291bnRTZXJ2aWNlLmdldCgpLnN1YnNjcmliZSh1c2VyID0+IHtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50TG9naW5Vc2VyXCIsIEpTT04uc3RyaW5naWZ5KHVzZXIucmVzcG9uc2UpKTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJ1c2VySWRcIiwgdXNlci5yZXNwb25zZS5faWQpO1xyXG4gICAgICAgdGhpcy5jdXJyZW50VXNlclJvbGUgPSB1c2VyLnJlc3BvbnNlO1xyXG4gICAgICB0aGlzLmFjY291bnRTZXJ2aWNlLmVtaXRTd2l0Y2hFdmVudCh1c2VyLnJlc3BvbnNlKTtcclxuICAgICAgdGhpcy5sb2FkQWxsKCk7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgICBsb2FkQWxsKCkge1xyXG4gICAgY29uc29sZS5sb2coXCIvLy8vLy8vXCIpO1xyXG4gICAgLy8gTG9hZCB0aGUgbmF2aWdhdGlvbiBlaXRoZXIgZnJvbSB0aGUgaW5wdXQgb3IgZnJvbSB0aGUgc2VydmljZVxyXG4gICAgdGhpcy5uYXZpZ2F0aW9uID1cclxuICAgIHRoaXMubmF2aWdhdGlvbiB8fCB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2UuZ2V0Q3VycmVudE5hdmlnYXRpb24oKTtcclxuICAgICAgaWYgKHRoaXMuY3VycmVudFVzZXJSb2xlICYmIHRoaXMuY3VycmVudFVzZXJSb2xlLnJvbGUubGVuZ3RoICYmIHRoaXMuY3VycmVudFVzZXJSb2xlLnJvbGVbMF0uYWNjZXNzLmluY2x1ZGVzKCcqJykpIHtcclxuICAgICAgICB0aGlzLmZpbHRlcmVkTWVudXMgPSB0aGlzLm5hdmlnYXRpb247XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGV0IGFjdGl2ZV9hY2Nlc3M9dGhpcy5jdXJyZW50VXNlclJvbGUucm9sZVswXS5hY2Nlc3M7ICAvLyBzdGdnaW5nXHJcbiAgICAgICAvLyBsZXQgdXNlclJvbGU9W1wiYWNjZXNzR3JvdXBcIixcImFjY2Vzc0NvbnRyb2xcIixcIm1hbmFnZVVzZXJzXCIsXCJtYW5hZ2VVc2Vyc1wiXTtcclxuICAgICAgIGxldCB1c2VyUm9sZT1hY3RpdmVfYWNjZXNzO1xyXG4gICAgICAgIHVzZXJSb2xlPV8udW5pcSh1c2VyUm9sZSk7XHJcbiAgICAgICAgdmFyIHNlbGY9dGhpcztcclxuICAgICAgICBfLmZvckVhY2godGhpcy5uYXZpZ2F0aW9uLGZ1bmN0aW9uKGl0ZW06YW55LGluZGV4Ok51bWJlcilcclxuICAgICAgICB7ICBcclxuICAgICAgICAgIHZhciBjaGlsZHJlbkxpc3QgPSBbXTtcclxuICAgICAgICAgICBpZihpdGVtLmNoaWxkcmVuICYmIGl0ZW0uY2hpbGRyZW4ubGVuZ3RoKVxyXG4gICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgXy5mb3JFYWNoKGl0ZW0uY2hpbGRyZW4sIGZ1bmN0aW9uKG1lbnVJdGVtOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICBfLmZvckVhY2godXNlclJvbGUsZnVuY3Rpb24odmlld0l0ZW06IGFueSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZih2aWV3SXRlbT09bWVudUl0ZW0uaWQgfHwgKG1lbnVJdGVtLm1lbnVJZCAmJiBtZW51SXRlbS5tZW51SWQ9PXZpZXdJdGVtKSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoaWxkcmVuTGlzdC5wdXNoKG1lbnVJdGVtKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmKChjaGlsZHJlbkxpc3QubGVuZ3RoPjApIHx8IGluZGV4PT0wKSBcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICBsZXQgdDE9aXRlbTtcclxuICAgICAgICAgICAgdDEuY2hpbGRyZW49Y2hpbGRyZW5MaXN0O1xyXG4gICAgICAgICAgICAgc2VsZi5maWx0ZXJlZE1lbnVzLnB1c2godDEpO1xyXG4gICAgICAgICAgICB9IFxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9XHJcbiAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uQ2hhbmdlZFxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAvLyBMb2FkIHRoZSBuYXZpZ2F0aW9uXHJcbiAgICAgICAgdGhpcy5uYXZpZ2F0aW9uID0gdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLmdldEN1cnJlbnROYXZpZ2F0aW9uKCk7XHJcblxyXG4gICAgICAgIC8vIE1hcmsgZm9yIGNoZWNrXHJcbiAgICAgICAgdGhpcy5fY2hhbmdlRGV0ZWN0b3JSZWYubWFya0ZvckNoZWNrKCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIFN1YnNjcmliZSB0byBuYXZpZ2F0aW9uIGl0ZW1cclxuICAgIG1lcmdlKFxyXG4gICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbUFkZGVkLFxyXG4gICAgICB0aGlzLl9mdXNlTmF2aWdhdGlvblNlcnZpY2Uub25OYXZpZ2F0aW9uSXRlbVVwZGF0ZWQsXHJcbiAgICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbk5hdmlnYXRpb25JdGVtUmVtb3ZlZFxyXG4gICAgKVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAvLyBNYXJrIGZvciBjaGVja1xyXG4gICAgICAgIHRoaXMuX2NoYW5nZURldGVjdG9yUmVmLm1hcmtGb3JDaGVjaygpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICBcclxufVxyXG4iXX0=