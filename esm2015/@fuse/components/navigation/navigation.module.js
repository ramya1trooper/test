import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { TranslateModule } from "@ngx-translate/core";
import { FuseNavigationComponent } from "./navigation.component";
import { FuseNavVerticalItemComponent } from "./vertical/item/item.component";
import { FuseNavVerticalCollapsableComponent } from "./vertical/collapsable/collapsable.component";
import { FuseNavVerticalGroupComponent } from "./vertical/group/group.component";
import { FuseNavHorizontalItemComponent } from "./horizontal/item/item.component";
import { FuseNavHorizontalCollapsableComponent } from "./horizontal/collapsable/collapsable.component";
import { MaterialModule } from "../../../material.module";
// import {ManageAvComponent} from "../../../avm/manage-av/manage-av.component";
// import {REPORT_MANAGEMENT_ROUTE} from '../../../avm/report-management-routing.module';
const ROUTES = [];
export class FuseNavigationModule {
}
FuseNavigationModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    RouterModule.forRoot(ROUTES),
                    MaterialModule,
                    TranslateModule.forChild()
                ],
                exports: [FuseNavigationComponent],
                declarations: [
                    FuseNavigationComponent,
                    FuseNavVerticalGroupComponent,
                    FuseNavVerticalItemComponent,
                    FuseNavVerticalCollapsableComponent,
                    FuseNavHorizontalItemComponent,
                    FuseNavHorizontalCollapsableComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2aWdhdGlvbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiQGZ1c2UvY29tcG9uZW50cy9uYXZpZ2F0aW9uL25hdmlnYXRpb24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDakUsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1DQUFtQyxFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDbkcsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDakYsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDbEYsT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzFELGdGQUFnRjtBQUNoRix5RkFBeUY7QUFFekYsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO0FBb0JsQixNQUFNLE9BQU8sb0JBQW9COzs7WUFuQmhDLFFBQVEsU0FBQztnQkFDUixPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixZQUFZLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQztvQkFFNUIsY0FBYztvQkFFZCxlQUFlLENBQUMsUUFBUSxFQUFFO2lCQUMzQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztnQkFDbEMsWUFBWSxFQUFFO29CQUNaLHVCQUF1QjtvQkFDdkIsNkJBQTZCO29CQUM3Qiw0QkFBNEI7b0JBQzVCLG1DQUFtQztvQkFDbkMsOEJBQThCO29CQUM5QixxQ0FBcUM7aUJBQ3RDO2FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uQ29tcG9uZW50IH0gZnJvbSBcIi4vbmF2aWdhdGlvbi5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgRnVzZU5hdlZlcnRpY2FsSXRlbUNvbXBvbmVudCB9IGZyb20gXCIuL3ZlcnRpY2FsL2l0ZW0vaXRlbS5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgRnVzZU5hdlZlcnRpY2FsQ29sbGFwc2FibGVDb21wb25lbnQgfSBmcm9tIFwiLi92ZXJ0aWNhbC9jb2xsYXBzYWJsZS9jb2xsYXBzYWJsZS5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgRnVzZU5hdlZlcnRpY2FsR3JvdXBDb21wb25lbnQgfSBmcm9tIFwiLi92ZXJ0aWNhbC9ncm91cC9ncm91cC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgRnVzZU5hdkhvcml6b250YWxJdGVtQ29tcG9uZW50IH0gZnJvbSBcIi4vaG9yaXpvbnRhbC9pdGVtL2l0ZW0uY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IEZ1c2VOYXZIb3Jpem9udGFsQ29sbGFwc2FibGVDb21wb25lbnQgfSBmcm9tIFwiLi9ob3Jpem9udGFsL2NvbGxhcHNhYmxlL2NvbGxhcHNhYmxlLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gXCIuLi8uLi8uLi9tYXRlcmlhbC5tb2R1bGVcIjtcclxuLy8gaW1wb3J0IHtNYW5hZ2VBdkNvbXBvbmVudH0gZnJvbSBcIi4uLy4uLy4uL2F2bS9tYW5hZ2UtYXYvbWFuYWdlLWF2LmNvbXBvbmVudFwiO1xyXG4vLyBpbXBvcnQge1JFUE9SVF9NQU5BR0VNRU5UX1JPVVRFfSBmcm9tICcuLi8uLi8uLi9hdm0vcmVwb3J0LW1hbmFnZW1lbnQtcm91dGluZy5tb2R1bGUnO1xyXG5cclxuY29uc3QgUk9VVEVTID0gW107XHJcbkBOZ01vZHVsZSh7XHJcbiAgaW1wb3J0czogW1xyXG4gICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgUm91dGVyTW9kdWxlLmZvclJvb3QoUk9VVEVTKSxcclxuXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW0Z1c2VOYXZpZ2F0aW9uQ29tcG9uZW50XSxcclxuICBkZWNsYXJhdGlvbnM6IFtcclxuICAgIEZ1c2VOYXZpZ2F0aW9uQ29tcG9uZW50LFxyXG4gICAgRnVzZU5hdlZlcnRpY2FsR3JvdXBDb21wb25lbnQsXHJcbiAgICBGdXNlTmF2VmVydGljYWxJdGVtQ29tcG9uZW50LFxyXG4gICAgRnVzZU5hdlZlcnRpY2FsQ29sbGFwc2FibGVDb21wb25lbnQsXHJcbiAgICBGdXNlTmF2SG9yaXpvbnRhbEl0ZW1Db21wb25lbnQsXHJcbiAgICBGdXNlTmF2SG9yaXpvbnRhbENvbGxhcHNhYmxlQ29tcG9uZW50XHJcbiAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRnVzZU5hdmlnYXRpb25Nb2R1bGUge31cclxuIl19