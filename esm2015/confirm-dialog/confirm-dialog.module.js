import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
// import { FormLayoutModule } from "../form-layout/form-layout.module";
import { ButtonLayoutModule } from "../button-layout/button-layout.module";
import { ConfirmDialogComponent } from './confirm-dialog.component';
export class ConfirmDialogModule {
}
ConfirmDialogModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ConfirmDialogComponent],
                imports: [
                    RouterModule,
                    // FormLayoutModule,
                    ButtonLayoutModule,
                    // Material
                    MaterialModule,
                    TranslateModule,
                    FuseSharedModule
                ],
                exports: [ConfirmDialogComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29uZmlybS1kaWFsb2cubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNvbmZpcm0tZGlhbG9nL2NvbmZpcm0tZGlhbG9nLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBR3BELHdFQUF3RTtBQUN4RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUMzRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQWVwRSxNQUFNLE9BQU8sbUJBQW1COzs7WUFkL0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHNCQUFzQixDQUFDO2dCQUN0QyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixvQkFBb0I7b0JBQ3BCLGtCQUFrQjtvQkFDbEIsV0FBVztvQkFDWCxjQUFjO29CQUNkLGVBQWU7b0JBRWYsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUNsQyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9zaGFyZWQubW9kdWxlXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSBcIi4uL21hdGVyaWFsLm1vZHVsZVwiO1xyXG5cclxuaW1wb3J0IHsgTW9kZWxMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kZWwtbGF5b3V0L21vZGVsLWxheW91dC5jb21wb25lbnRcIjtcclxuLy8gaW1wb3J0IHsgRm9ybUxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9mb3JtLWxheW91dC9mb3JtLWxheW91dC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQnV0dG9uTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2J1dHRvbi1sYXlvdXQvYnV0dG9uLWxheW91dC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQ29uZmlybURpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4vY29uZmlybS1kaWFsb2cuY29tcG9uZW50JztcclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtDb25maXJtRGlhbG9nQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAvLyBGb3JtTGF5b3V0TW9kdWxlLFxyXG4gICAgQnV0dG9uTGF5b3V0TW9kdWxlLFxyXG4gICAgLy8gTWF0ZXJpYWxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG5cclxuICAgIEZ1c2VTaGFyZWRNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtDb25maXJtRGlhbG9nQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29uZmlybURpYWxvZ01vZHVsZSB7XHJcblxyXG59XHJcbiJdfQ==