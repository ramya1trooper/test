import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { VerticalBarChartComponent } from "./vertical-bar-chart.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { ChartsModule } from "ng2-charts";
import { FuseProgressBarModule } from '../@fuse/components/progress-bar/progress-bar.module';
import { FuseThemeOptionsModule } from '../@fuse/components/theme-options/theme-options.module';
import { FuseWidgetModule } from "../@fuse/components/widget/widget.module";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
export class VerticalBarChartlayoutModule {
}
VerticalBarChartlayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [VerticalBarChartComponent],
                imports: [
                    RouterModule,
                    NgxChartsModule,
                    ChartsModule,
                    FuseWidgetModule,
                    FuseSharedModule,
                    TranslateModule,
                    FuseProgressBarModule,
                    FuseThemeOptionsModule
                ],
                exports: [VerticalBarChartComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmVydGljYWwtYmFyLWNoYXJ0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ2ZXJ0aWNhbC1iYXItY2hhcnQvdmVydGljYWwtYmFyLWNoYXJ0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMzRSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDdkQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFlBQVksQ0FBQztBQUMxQyxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxzREFBc0QsQ0FBQztBQUMzRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSx3REFBd0QsQ0FBQTtBQUM3RixPQUFPLEVBQ0wsZ0JBQWdCLEVBQ2pCLE1BQU0sMENBQTBDLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBZ0J0RCxNQUFNLE9BQU8sNEJBQTRCOzs7WUFkeEMsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHlCQUF5QixDQUFDO2dCQUN6QyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixlQUFlO29CQUNmLFlBQVk7b0JBQ1osZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixzQkFBc0I7aUJBQ3ZCO2dCQUNELE9BQU8sRUFBRSxDQUFDLHlCQUF5QixDQUFDO2FBQ3JDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBWZXJ0aWNhbEJhckNoYXJ0Q29tcG9uZW50IH0gZnJvbSBcIi4vdmVydGljYWwtYmFyLWNoYXJ0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBOZ3hDaGFydHNNb2R1bGUgfSBmcm9tIFwiQHN3aW1sYW5lL25neC1jaGFydHNcIjtcclxuaW1wb3J0IHsgQ2hhcnRzTW9kdWxlIH0gZnJvbSBcIm5nMi1jaGFydHNcIjtcclxuaW1wb3J0IHtGdXNlUHJvZ3Jlc3NCYXJNb2R1bGV9IGZyb20gJy4uL0BmdXNlL2NvbXBvbmVudHMvcHJvZ3Jlc3MtYmFyL3Byb2dyZXNzLWJhci5tb2R1bGUnO1xyXG5pbXBvcnQge0Z1c2VUaGVtZU9wdGlvbnNNb2R1bGV9IGZyb20gJy4uL0BmdXNlL2NvbXBvbmVudHMvdGhlbWUtb3B0aW9ucy90aGVtZS1vcHRpb25zLm1vZHVsZSdcclxuaW1wb3J0IHtcclxuICBGdXNlV2lkZ2V0TW9kdWxlXHJcbn0gZnJvbSBcIi4uL0BmdXNlL2NvbXBvbmVudHMvd2lkZ2V0L3dpZGdldC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9zaGFyZWQubW9kdWxlXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW1ZlcnRpY2FsQmFyQ2hhcnRDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIE5neENoYXJ0c01vZHVsZSxcclxuICAgIENoYXJ0c01vZHVsZSxcclxuICAgIEZ1c2VXaWRnZXRNb2R1bGUsXHJcbiAgICBGdXNlU2hhcmVkTW9kdWxlLFxyXG4gICAgVHJhbnNsYXRlTW9kdWxlLFxyXG4gICAgRnVzZVByb2dyZXNzQmFyTW9kdWxlLFxyXG4gICAgRnVzZVRoZW1lT3B0aW9uc01vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW1ZlcnRpY2FsQmFyQ2hhcnRDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWZXJ0aWNhbEJhckNoYXJ0bGF5b3V0TW9kdWxlIHt9XHJcbiJdfQ==