import { Component, ViewEncapsulation, Input, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
export class VerticalBarChartComponent {
    constructor(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.showLegend = true;
        this.explodeSlices = false;
        this.doughnut = false;
        this.showXAxis = true;
        this.showYAxis = true;
        this.showXAxisLabel = true;
        this.xAxisLabel = 'Country';
        this.showYAxisLabel = true;
        this.yAxisLabel = 'Population';
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    ngOnInit() {
        this.chartData = this.result;
    }
    onLegendLabelClick(event) { }
    select(event) { }
}
VerticalBarChartComponent.decorators = [
    { type: Component, args: [{
                selector: "vertical-bar-chart",
                template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\" style=\"align-self: center;\" class=\"h-400 p-10  mat-body-2\">\r\n    <div class=\"inner\" *ngIf=\"chartData.length > 0\">\r\n\r\n      <ngx-charts-bar-vertical class=\"chart-container\" [view]=\"view\" [scheme]=\"scheme\" [results]=\"chartData\"\r\n        [gradient]=\"gradient\"\r\n        [xAxis]=\"showXAxis\"\r\n        [yAxis]=\"showYAxis\"\r\n        [legend]=\"showLegend\"\r\n        [showXAxisLabel]=\"showXAxisLabel\"\r\n        [showYAxisLabel]=\"showYAxisLabel\"\r\n        [xAxisLabel]=\"xAxisLabel\"\r\n        [yAxisLabel]=\"yAxisLabel\"\r\n        (select)=\"select($event)\"> \r\n      </ngx-charts-bar-vertical>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"chartData.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{label.nodata | translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                encapsulation: ViewEncapsulation.None,
                animations: fuseAnimations,
                styles: [".piechartdiv{display:block!important}.piechartdiv .legend-label-text{text-align:left}.inner{display:block;width:100%;text-align:center;margin-left:0!important}.inner img{width:auto;max-width:100%}.piechartdiv .chart-legend{padding-top:10px}.piechartdiv .legend-label{padding-bottom:7px}.piechartdiv .graphdiv{width:auto!important;padding-top:4px}fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}"]
            }] }
];
/** @nocollapse */
VerticalBarChartComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
VerticalBarChartComponent.propDecorators = {
    result: [{ type: Input }],
    scheme: [{ type: Input }],
    view: [{ type: Input }],
    label: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmVydGljYWwtYmFyLWNoYXJ0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ2ZXJ0aWNhbC1iYXItY2hhcnQvdmVydGljYWwtYmFyLWNoYXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULGlCQUFpQixFQUNqQixLQUFLLEVBQ0wsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVyRCxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUM1RixrREFBa0Q7QUFTbEQsTUFBTSxPQUFPLHlCQUF5QjtJQXlCcEMsWUFDVSw2QkFBMkQsRUFDeEMsT0FBTztRQUQxQixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQ3hDLFlBQU8sR0FBUCxPQUFPLENBQUE7UUExQnBDLGVBQVUsR0FBRyxJQUFJLENBQUM7UUFDbEIsa0JBQWEsR0FBRyxLQUFLLENBQUM7UUFDdEIsYUFBUSxHQUFHLEtBQUssQ0FBQztRQWVqQixjQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLGNBQVMsR0FBRyxJQUFJLENBQUM7UUFDakIsbUJBQWMsR0FBRyxJQUFJLENBQUM7UUFDdEIsZUFBVSxHQUFHLFNBQVMsQ0FBQztRQUN2QixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixlQUFVLEdBQUcsWUFBWSxDQUFDO1FBTXhCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsUUFBUTtRQUNOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUMvQixDQUFDO0lBQ0Qsa0JBQWtCLENBQUMsS0FBSyxJQUFHLENBQUM7SUFDNUIsTUFBTSxDQUFDLEtBQUssSUFBRyxDQUFDOzs7WUEzQ2pCLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixnMENBQWtEO2dCQUVsRCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsVUFBVSxFQUFFLGNBQWM7O2FBQzNCOzs7O1lBVFEsNEJBQTRCOzRDQXFDaEMsTUFBTSxTQUFDLFNBQVM7OztxQkF2QmxCLEtBQUs7cUJBQ0wsS0FBSzttQkFDTCxLQUFLO29CQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgSW5wdXQsXHJcbiAgSW5qZWN0XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgZnVzZUFuaW1hdGlvbnMgfSBmcm9tIFwiLi4vQGZ1c2UvYW5pbWF0aW9uc1wiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJ2ZXJ0aWNhbC1iYXItY2hhcnRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL3ZlcnRpY2FsLWJhci1jaGFydC5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi92ZXJ0aWNhbC1iYXItY2hhcnQuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBhbmltYXRpb25zOiBmdXNlQW5pbWF0aW9uc1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVmVydGljYWxCYXJDaGFydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgc2hvd0xlZ2VuZCA9IHRydWU7XHJcbiAgZXhwbG9kZVNsaWNlcyA9IGZhbHNlO1xyXG4gIGRvdWdobnV0ID0gZmFsc2U7XHJcbiAgQElucHV0KCkgcmVzdWx0OiBhbnk7XHJcbiAgQElucHV0KCkgc2NoZW1lOiBhbnk7XHJcbiAgQElucHV0KCkgdmlldzogYW55O1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBhbnk7XHJcblxyXG4gIGNoYXJ0RGF0YTogYW55O1xyXG4gIHRyYW5zbGF0ZTogYW55O1xyXG4gIGFuaW1hdGlvbnM7XHJcbiAgbGVnZW5kVGl0bGU7XHJcbiAgYXJjV2lkdGg7XHJcbiAgZ3JhZGllbnQ7XHJcbiAgbGVnZW5kUG9zaXRpb247XHJcbiAgcGllVG9vbHRpcFRleHQ7XHJcblxyXG4gIHNob3dYQXhpcyA9IHRydWU7XHJcbiAgc2hvd1lBeGlzID0gdHJ1ZTtcclxuICBzaG93WEF4aXNMYWJlbCA9IHRydWU7XHJcbiAgeEF4aXNMYWJlbCA9ICdDb3VudHJ5JztcclxuICBzaG93WUF4aXNMYWJlbCA9IHRydWU7XHJcbiAgeUF4aXNMYWJlbCA9ICdQb3B1bGF0aW9uJztcclxuXHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlOiBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHJpdmF0ZSBlbmdsaXNoXHJcbiAgKSB7XHJcbiAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmxvYWRUcmFuc2xhdGlvbnMoZW5nbGlzaCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuY2hhcnREYXRhID0gdGhpcy5yZXN1bHQ7XHJcbiAgfVxyXG4gIG9uTGVnZW5kTGFiZWxDbGljayhldmVudCkge31cclxuICBzZWxlY3QoZXZlbnQpIHt9XHJcbn1cclxuIl19