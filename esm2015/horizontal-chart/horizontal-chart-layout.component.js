import { Component, Input, ViewEncapsulation, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
export class HorizontalChartLayoutComponent {
    constructor(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this.gradient = false;
        this.showXAxis = true;
        this.showXAxisLabel = true;
        this.showYAxisLabel = true;
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    ngOnInit() { }
}
HorizontalChartLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "horizontal-chart-layout",
                template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\">\r\n\r\n    <div fxLayout=\"row wrap\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" class=\"p-10  mat-body-2\">\r\n      <div class=\"inner\" *ngIf=\"result && result.length > 0\">\r\n        <ngx-charts-bar-horizontal [view]=\"view\" [scheme]=\"color\" [results]=\"result\" [gradient]=\"gradient\"\r\n          [legend]=\"true\" [legendPosition]=\"'below'\" [showXAxisLabel]=\"showXAxisLabel\" [showYAxisLabel]=\"showYAxisLabel\"\r\n          [xAxisLabel]=\"Users\" [yAxisLabel]=\"Controls\" [xAxis]=\"showXAxis\" [barPadding]=\"12\" [showDataLabel]=\"true\">\r\n        </ngx-charts-bar-horizontal>\r\n      </div>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"result.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{ label.nodata | translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                animations: fuseAnimations,
                encapsulation: ViewEncapsulation.None,
                styles: ["fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important}.inner{display:block;width:100%;text-align:center;margin-top:19px;margin-left:0!important}.inner img{width:auto;max-width:100%}ngx-charts-legend.chart-legend{width:auto;clear:both;display:block;margin:0 auto}ngx-charts-legend.chart-legend>div{display:block;width:500px!important}ngx-charts-legend.chart-legend .legend-wrap{min-width:200px;text-align:left;margin:0 auto;float:none;padding-right:33px}ngx-charts-legend.chart-legend .legend-title{display:none}ngx-charts-legend.chart-legend .legend-labels{display:inline;text-align:left;margin:0 auto;background:0 0;float:none;padding:0;white-space:inherit}.h-400{min-height:434px!important}"]
            }] }
];
/** @nocollapse */
HorizontalChartLayoutComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
HorizontalChartLayoutComponent.propDecorators = {
    result: [{ type: Input }],
    color: [{ type: Input }],
    view: [{ type: Input }],
    label: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImhvcml6b250YWwtY2hhcnQvaG9yaXpvbnRhbC1jaGFydC1sYXlvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFDTCxTQUFTLEVBRVQsS0FBSyxFQUNMLGlCQUFpQixFQUNqQixNQUFNLEVBQ1AsTUFBTSxlQUFlLENBQUM7QUFDdkIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBRXJELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLGtEQUFrRDtBQVNsRCxNQUFNLE9BQU8sOEJBQThCO0lBWXpDLFlBQ1UsNkJBQTJELEVBQ3hDLE9BQU87UUFEMUIsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUE4QjtRQUN4QyxZQUFPLEdBQVAsT0FBTyxDQUFBO1FBVHBDLGFBQVEsR0FBRyxLQUFLLENBQUM7UUFDakIsY0FBUyxHQUFHLElBQUksQ0FBQztRQUNqQixtQkFBYyxHQUFHLElBQUksQ0FBQztRQUN0QixtQkFBYyxHQUFHLElBQUksQ0FBQztRQVFwQixJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELFFBQVEsS0FBSSxDQUFDOzs7WUExQmQsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSx5QkFBeUI7Z0JBQ25DLDgwQ0FBdUQ7Z0JBRXZELFVBQVUsRUFBRSxjQUFjO2dCQUMxQixhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDdEM7Ozs7WUFUUSw0QkFBNEI7NENBd0JoQyxNQUFNLFNBQUMsU0FBUzs7O3FCQWJsQixLQUFLO29CQUNMLEtBQUs7bUJBQ0wsS0FBSztvQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcclxuICBDb21wb25lbnQsXHJcbiAgT25Jbml0LFxyXG4gIElucHV0LFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gIEluamVjdFxyXG59IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IGZ1c2VBbmltYXRpb25zIH0gZnJvbSBcIi4uL0BmdXNlL2FuaW1hdGlvbnNcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcbmltcG9ydCB7IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UgfSBmcm9tIFwiLi4vQGZ1c2Uvc2VydmljZXMvdHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2VcIjtcclxuLy8gaW1wb3J0IHsgbG9jYWxlIGFzIGVuZ2xpc2ggfSBmcm9tIFwiLi4vaTE4bi9lblwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwiaG9yaXpvbnRhbC1jaGFydC1sYXlvdXRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL2hvcml6b250YWwtY2hhcnQtbGF5b3V0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2hvcml6b250YWwtY2hhcnQtbGF5b3V0LmNvbXBvbmVudC5zY3NzXCJdLFxyXG4gIGFuaW1hdGlvbnM6IGZ1c2VBbmltYXRpb25zLFxyXG4gIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEhvcml6b250YWxDaGFydExheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgcmVzdWx0OiBhbnk7XHJcbiAgQElucHV0KCkgY29sb3I6IGFueTtcclxuICBASW5wdXQoKSB2aWV3OiBhbnk7XHJcbiAgQElucHV0KCkgbGFiZWw6IGFueTtcclxuICBncmFkaWVudCA9IGZhbHNlO1xyXG4gIHNob3dYQXhpcyA9IHRydWU7XHJcbiAgc2hvd1hBeGlzTGFiZWwgPSB0cnVlO1xyXG4gIHNob3dZQXhpc0xhYmVsID0gdHJ1ZTtcclxuICB0cmFuc2xhdGU6IGFueTtcclxuICBVc2VycztcclxuICBDb250cm9scztcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2hcclxuICApIHtcclxuICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UubG9hZFRyYW5zbGF0aW9ucyhlbmdsaXNoKTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge31cclxufVxyXG4iXX0=