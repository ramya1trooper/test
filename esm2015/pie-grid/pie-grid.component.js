import { Component, ViewEncapsulation, Input, Inject } from "@angular/core";
import { fuseAnimations } from "../@fuse/animations";
// import { locale as english } from "../i18n/en";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
export class PieGridComponent {
    constructor(_fuseTranslationLoaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.english = english;
        this._fuseTranslationLoaderService.loadTranslations(english);
    }
    ngOnInit() {
        this.ChartData = this.result;
    }
}
PieGridComponent.decorators = [
    { type: Component, args: [{
                selector: "pie-grid",
                template: "<!-- <fuse-widget [@animate]=\"{value:'*',params:{y:'100%'}}\" class=\"widget\" fxLayout=\"column\" fxFlex=\"100\"\r\n  fxFlex.gt-sm=\"100\"> -->\r\n<div class=\"fuse-widget-front\">\r\n  <div class=\"px-16 border-bottom\" fxLayout=\"row wrap\" fxLayoutAlign=\"space-between center\">\r\n    <div></div>\r\n    <div fxFlex class=\"py-16 h3\">{{label.title | translate}}</div>\r\n  </div>\r\n  <div class=\"h-400\" fxFlex=\"100\" fxLayout=\"row wrap\" fxFlex.gt-sm=\"100\">\r\n    <div class=\"inner\" *ngIf=\"ChartData.length > 0\">\r\n      <ngx-charts-pie-grid [view]=\"view\" [scheme]=\"scheme\" [results]=\"ChartData\">\r\n      </ngx-charts-pie-grid>\r\n    </div>\r\n    <div class=\"inner\" *ngIf=\"ChartData.length == 0\">\r\n      <p>\r\n        <span class=\"mat-caption\">{{label.nodata| translate}}</span>\r\n      </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- </fuse-widget> -->\r\n",
                encapsulation: ViewEncapsulation.None,
                animations: fuseAnimations,
                styles: ["fuse-widget{position:relative!important;-webkit-perspective:3000px!important;perspective:3000px!important;padding:4px!important;height:-webkit-fit-content!important;height:-moz-fit-content!important;height:fit-content!important;width:100%!important}.inner{display:block;width:100%;text-align:center;margin-left:0!important}.inner img{width:auto;max-width:100%}ngx-charts-pie-grid{width:100%!important}"]
            }] }
];
/** @nocollapse */
PieGridComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
PieGridComponent.propDecorators = {
    result: [{ type: Input }],
    scheme: [{ type: Input }],
    view: [{ type: Input }],
    label: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGllLWdyaWQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInBpZS1ncmlkL3BpZS1ncmlkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUVULGlCQUFpQixFQUNqQixLQUFLLEVBQ0wsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVyRCxrREFBa0Q7QUFDbEQsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFTNUYsTUFBTSxPQUFPLGdCQUFnQjtJQU8zQixZQUNVLDZCQUEyRCxFQUN4QyxPQUFPO1FBRDFCLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBOEI7UUFDeEMsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUVsQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDL0IsQ0FBQzs7O1lBdkJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsODRCQUF3QztnQkFFeEMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLFVBQVUsRUFBRSxjQUFjOzthQUMzQjs7OztZQVJRLDRCQUE0Qjs0Q0FrQmhDLE1BQU0sU0FBQyxTQUFTOzs7cUJBUmxCLEtBQUs7cUJBQ0wsS0FBSzttQkFDTCxLQUFLO29CQUNMLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkluaXQsXHJcbiAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgSW5wdXQsXHJcbiAgSW5qZWN0XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgZnVzZUFuaW1hdGlvbnMgfSBmcm9tIFwiLi4vQGZ1c2UvYW5pbWF0aW9uc1wiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuLy8gaW1wb3J0IHsgbG9jYWxlIGFzIGVuZ2xpc2ggfSBmcm9tIFwiLi4vaTE4bi9lblwiO1xyXG5pbXBvcnQgeyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJwaWUtZ3JpZFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vcGllLWdyaWQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vcGllLWdyaWQuY29tcG9uZW50LnNjc3NcIl0sXHJcbiAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICBhbmltYXRpb25zOiBmdXNlQW5pbWF0aW9uc1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUGllR3JpZENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgcmVzdWx0OiBhbnk7XHJcbiAgQElucHV0KCkgc2NoZW1lOiBhbnk7XHJcbiAgQElucHV0KCkgdmlldzogYW55O1xyXG4gIEBJbnB1dCgpIGxhYmVsOiBhbnk7XHJcbiAgdHJhbnNsYXRlOiBhbnk7XHJcbiAgQ2hhcnREYXRhOiBhbnk7XHJcbiAgY29uc3RydWN0b3IoXHJcbiAgICBwcml2YXRlIF9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlOiBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHJpdmF0ZSBlbmdsaXNoXHJcbiAgKSB7XHJcbiAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmxvYWRUcmFuc2xhdGlvbnMoZW5nbGlzaCk7XHJcbiAgfVxyXG5cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuQ2hhcnREYXRhID0gdGhpcy5yZXN1bHQ7XHJcbiAgfVxyXG59XHJcbiJdfQ==