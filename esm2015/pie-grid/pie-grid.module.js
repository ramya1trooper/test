import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { PieGridComponent } from "./pie-grid.component";
import { NgxChartsModule } from "@swimlane/ngx-charts";
import { ChartsModule } from "ng2-charts";
import { FuseProgressBarModule } from '../@fuse/components/progress-bar/progress-bar.module';
import { FuseThemeOptionsModule } from '../@fuse/components/theme-options/theme-options.module';
import { FuseWidgetModule } from "../@fuse/components/widget/widget.module";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
export class PieGridlayoutModule {
}
PieGridlayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [PieGridComponent],
                imports: [
                    RouterModule,
                    NgxChartsModule,
                    ChartsModule,
                    FuseWidgetModule,
                    FuseSharedModule,
                    TranslateModule,
                    FuseProgressBarModule,
                    FuseThemeOptionsModule
                ],
                exports: [PieGridComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGllLWdyaWQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInBpZS1ncmlkL3BpZS1ncmlkLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUUvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDdkQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLFlBQVksQ0FBQztBQUMxQyxPQUFPLEVBQUMscUJBQXFCLEVBQUMsTUFBTSxzREFBc0QsQ0FBQztBQUMzRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSx3REFBd0QsQ0FBQTtBQUM3RixPQUFPLEVBQ0wsZ0JBQWdCLEVBQ2pCLE1BQU0sMENBQTBDLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBZ0J0RCxNQUFNLE9BQU8sbUJBQW1COzs7WUFkL0IsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGdCQUFnQixDQUFDO2dCQUNoQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixlQUFlO29CQUNmLFlBQVk7b0JBQ1osZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YscUJBQXFCO29CQUNyQixzQkFBc0I7aUJBQ3ZCO2dCQUNELE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO2FBQzVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcblxyXG5pbXBvcnQgeyBQaWVHcmlkQ29tcG9uZW50IH0gZnJvbSBcIi4vcGllLWdyaWQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IE5neENoYXJ0c01vZHVsZSB9IGZyb20gXCJAc3dpbWxhbmUvbmd4LWNoYXJ0c1wiO1xyXG5pbXBvcnQgeyBDaGFydHNNb2R1bGUgfSBmcm9tIFwibmcyLWNoYXJ0c1wiO1xyXG5pbXBvcnQge0Z1c2VQcm9ncmVzc0Jhck1vZHVsZX0gZnJvbSAnLi4vQGZ1c2UvY29tcG9uZW50cy9wcm9ncmVzcy1iYXIvcHJvZ3Jlc3MtYmFyLm1vZHVsZSc7XHJcbmltcG9ydCB7RnVzZVRoZW1lT3B0aW9uc01vZHVsZX0gZnJvbSAnLi4vQGZ1c2UvY29tcG9uZW50cy90aGVtZS1vcHRpb25zL3RoZW1lLW9wdGlvbnMubW9kdWxlJ1xyXG5pbXBvcnQge1xyXG4gIEZ1c2VXaWRnZXRNb2R1bGVcclxufSBmcm9tIFwiLi4vQGZ1c2UvY29tcG9uZW50cy93aWRnZXQvd2lkZ2V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbUGllR3JpZENvbXBvbmVudF0sXHJcbiAgaW1wb3J0czogW1xyXG4gICAgUm91dGVyTW9kdWxlLFxyXG4gICAgTmd4Q2hhcnRzTW9kdWxlLFxyXG4gICAgQ2hhcnRzTW9kdWxlLFxyXG4gICAgRnVzZVdpZGdldE1vZHVsZSxcclxuICAgIEZ1c2VTaGFyZWRNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUsXHJcbiAgICBGdXNlUHJvZ3Jlc3NCYXJNb2R1bGUsXHJcbiAgICBGdXNlVGhlbWVPcHRpb25zTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbUGllR3JpZENvbXBvbmVudF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBpZUdyaWRsYXlvdXRNb2R1bGUge31cclxuIl19