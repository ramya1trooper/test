import { AccountService } from "./../shared/auth/account.service";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSearchBarModule } from "../@fuse/components/search-bar/search-bar.module";
import { FuseShortcutsModule } from "../@fuse/components/shortcuts/shortcuts.module";
import { FuseSharedModule } from "../@fuse/shared.module";
import { MaterialModule } from "../material.module";
import { ToolbarComponent } from "./toolbar.component";
import { SessionModule } from "../session-expiration/session-expiration.module";
import { AuthModelDialog } from '../session-expiration/session-expiration-alert.component';
export class ToolbarModule {
    static forRoot(metaData, english) {
        return {
            ngModule: ToolbarModule,
            providers: [
                AccountService,
                {
                    provide: "metaData",
                    useValue: metaData
                },
                {
                    provide: "english",
                    useValue: english
                }
            ]
        };
    }
}
ToolbarModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ToolbarComponent],
                imports: [
                    RouterModule,
                    MaterialModule,
                    FuseSharedModule,
                    FuseSearchBarModule,
                    FuseShortcutsModule,
                    SessionModule
                ],
                exports: [ToolbarComponent],
                entryComponents: [AuthModelDialog],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbGJhci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsidG9vbGJhci90b29sYmFyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDbEUsT0FBTyxFQUFFLFFBQVEsRUFBdUIsc0JBQXNCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdEYsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRS9DLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdEQUFnRCxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzFELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN2RCxPQUFPLEVBQUMsYUFBYSxFQUFFLE1BQU0saURBQWlELENBQUE7QUFFOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDBEQUEwRCxDQUFDO0FBaUIzRixNQUFNLE9BQU8sYUFBYTtJQUN4QixNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsRUFBRSxPQUFPO1FBQzlCLE9BQU87WUFDTCxRQUFRLEVBQUUsYUFBYTtZQUN2QixTQUFTLEVBQUU7Z0JBQ1QsY0FBYztnQkFDZDtvQkFDRSxPQUFPLEVBQUUsVUFBVTtvQkFDbkIsUUFBUSxFQUFFLFFBQVE7aUJBQ25CO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxTQUFTO29CQUNsQixRQUFRLEVBQUUsT0FBTztpQkFDbEI7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOzs7WUEvQkYsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGdCQUFnQixDQUFDO2dCQUNoQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixjQUFjO29CQUVkLGdCQUFnQjtvQkFDaEIsbUJBQW1CO29CQUNuQixtQkFBbUI7b0JBQ25CLGFBQWE7aUJBQ2Q7Z0JBQ0QsT0FBTyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7Z0JBQzNCLGVBQWUsRUFBQyxDQUFFLGVBQWUsQ0FBQztnQkFDbEMsT0FBTyxFQUFFLENBQUMsc0JBQXNCLENBQUM7YUFDbEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9hdXRoL2FjY291bnQuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSwgTW9kdWxlV2l0aFByb3ZpZGVycywgQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VTZWFyY2hCYXJNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2UvY29tcG9uZW50cy9zZWFyY2gtYmFyL3NlYXJjaC1iYXIubW9kdWxlXCI7XHJcbmltcG9ydCB7IEZ1c2VTaG9ydGN1dHNNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2UvY29tcG9uZW50cy9zaG9ydGN1dHMvc2hvcnRjdXRzLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBUb29sYmFyQ29tcG9uZW50IH0gZnJvbSBcIi4vdG9vbGJhci5jb21wb25lbnRcIjtcclxuaW1wb3J0IHtTZXNzaW9uTW9kdWxlIH0gZnJvbSBcIi4uL3Nlc3Npb24tZXhwaXJhdGlvbi9zZXNzaW9uLWV4cGlyYXRpb24ubW9kdWxlXCJcclxuaW1wb3J0IHsgU2Vzc2lvbkNvbXBvbmVudCB9IGZyb20gJy4uL3Nlc3Npb24tZXhwaXJhdGlvbi9zZXNzaW9uLWV4cGlyYXRpb24uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQXV0aE1vZGVsRGlhbG9nIH0gZnJvbSAnLi4vc2Vzc2lvbi1leHBpcmF0aW9uL3Nlc3Npb24tZXhwaXJhdGlvbi1hbGVydC5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtUb29sYmFyQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICBNYXRlcmlhbE1vZHVsZSxcclxuXHJcbiAgICBGdXNlU2hhcmVkTW9kdWxlLFxyXG4gICAgRnVzZVNlYXJjaEJhck1vZHVsZSxcclxuICAgIEZ1c2VTaG9ydGN1dHNNb2R1bGUsXHJcbiAgICBTZXNzaW9uTW9kdWxlXHJcbiAgXSxcclxuICBleHBvcnRzOiBbVG9vbGJhckNvbXBvbmVudF0sXHJcbiAgZW50cnlDb21wb25lbnRzOlsgQXV0aE1vZGVsRGlhbG9nXSxcclxuICBzY2hlbWFzOiBbQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFRvb2xiYXJNb2R1bGUge1xyXG4gIHN0YXRpYyBmb3JSb290KG1ldGFEYXRhLCBlbmdsaXNoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBuZ01vZHVsZTogVG9vbGJhck1vZHVsZSxcclxuICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgQWNjb3VudFNlcnZpY2UsXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvdmlkZTogXCJtZXRhRGF0YVwiLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IG1ldGFEYXRhXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBcImVuZ2xpc2hcIixcclxuICAgICAgICAgIHVzZVZhbHVlOiBlbmdsaXNoXHJcbiAgICAgICAgfVxyXG4gICAgICBdXHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=