import { MessageService } from "./../_services/message.service";
import { Component, ViewEncapsulation, Inject } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { TranslateService } from "@ngx-translate/core";
import * as _ from "lodash";
import { FuseConfigService } from "../@fuse/services/config.service";
import { FuseSidebarService } from "../@fuse/components/sidebar/sidebar.service";
import { ContentService } from "../content/content.service";
import { AccountService } from "../shared/auth/account.service";
import { OAuthService } from "angular-oauth2-oidc";
import { CookieService } from 'ngx-cookie-service';
import { LoaderService } from '../loader.service';
import { FuseTranslationLoaderService } from '../@fuse/services/translation-loader.service';
// import * as moment from 'moment';
import * as moment_ from 'moment';
const moment = moment_;
import * as FileSaver from "file-saver";
export class ToolbarComponent {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TranslateService} _translateService
     */
    constructor(_fuseTranslationLoaderService, _fuseConfigService, _fuseSidebarService, _translateService, contentService, accountService, oauthService, messageService, _cookieService, loaderService, _metaData, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this._fuseConfigService = _fuseConfigService;
        this._fuseSidebarService = _fuseSidebarService;
        this._translateService = _translateService;
        this.contentService = contentService;
        this.accountService = accountService;
        this.oauthService = oauthService;
        this.messageService = messageService;
        this._cookieService = _cookieService;
        this.loaderService = loaderService;
        this._metaData = _metaData;
        this.english = english;
        this._fuseTranslationLoaderService.loadTranslations(english);
        // Set the defaults
        this.userStatusOptions = [
            {
                title: "Online",
                icon: "icon-checkbox-marked-circle",
                color: "#4CAF50"
            },
            {
                title: "Away",
                icon: "icon-clock",
                color: "#FFC107"
            },
            {
                title: "Do not Disturb",
                icon: "icon-minus-circle",
                color: "#F44336"
            },
            {
                title: "Invisible",
                icon: "icon-checkbox-blank-circle-outline",
                color: "#BDBDBD"
            },
            {
                title: "Offline",
                icon: "icon-checkbox-blank-circle-outline",
                color: "#616161"
            }
        ];
        this.languages = [
            {
                id: "en",
                title: "English",
                flag: "us"
            },
            {
                id: "tr",
                title: "Turkish",
                flag: "tr"
            }
        ];
        this.messageService.getDatasource().subscribe(response => {
            if (this.realm_data) {
                this.getAllDatasource();
            }
            console.log(" setDatasouce ", response);
            if (response && response.text && response.text != 'null') {
                this.defaultDatasource = response.text;
                this.sendMessage(response.text, null);
            }
        });
        this.messageService.getTriggerNotification()
            .subscribe(data => {
            console.log(data, ">>>>>>>>>>> DATA");
            if (this.realm_data) {
                this.getAllNotification();
            }
        });
        this.navigation = this.getMetaData();
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(settings => {
            this.horizontalNavbar = settings.layout.navbar.position === "top";
            this.rightNavbar = settings.layout.navbar.position === "right";
            this.hiddenNavbar = settings.layout.navbar.hidden === true;
        });
        // Set the selected language from default languages
        this.selectedLanguage = _.find(this.languages, {
            id: this._translateService.currentLang
        });
        let realmData = this._cookieService.get("realm");
        this.accountService.switchEvent.subscribe(data => {
            //get current username
            this.user = data.username;
            this.data = data.realm;
            // this.realm_data = data.realm[0]._id;
            console.log(this.user);
            if (realmData) {
                this.defaultRealm = this._cookieService.get("realmName");
                this.realm_data = realmData;
            }
            else {
                this.defaultRealm = this.data[0].name;
                this.realm_data = this.data[0]._id;
            }
            localStorage.setItem("realm", this.realm_data);
            localStorage.setItem("realmName", this.defaultRealm);
            console.log(">>>> realm_data toolbar ngOnInit ", this.realm_data);
            this.getAllDatasource();
            this.getAllNotification();
        });
        // this.getAllDatasource();
        // this.getAllNotification();
    }
    getId(event) {
        console.log("@@@@", event);
        this.accountService.changeRealm(event).subscribe((data) => {
            console.log("data", data);
            localStorage.removeItem("access_token");
            localStorage.setItem("access_token", data.body.response.access_token);
            localStorage.setItem("realm", event._id);
            localStorage.setItem("realmName", event.name);
            this._cookieService.set('realm', event._id); // new one
            this._cookieService.set('realmName', event.name); // new one
            localStorage.removeItem("datasource");
            window.location.reload();
        }, error => {
            console.log("err", error);
        });
    }
    getAllDatasource() {
        this.loaderService.startLoader();
        let query = {
            realm: this.realm_data
        };
        let apiUrl = "/datasources/all";
        console.log(">>> realm dtaa ", this.realm_data);
        console.log(">>> query ", query);
        this.contentService.getAllReponse(query, apiUrl).subscribe(res => {
            this.loaderService.stopLoader();
            this.dataSource = res.response.datasources;
            var dataSource = localStorage.getItem("datasource"); // check on Local Storage
            this.cookieDS = this._cookieService.get("datasource"); // check on cookie
            // if(dataSource && dataSource != 'null') 
            // {
            //   this.selectedDatasourceId = dataSource;
            //   this.defaultDatasourceName = localStorage.getItem("datasourceName");
            //   this.defaultDatasource = dataSource;
            // } else 
            if (this.cookieDS && this.cookieDS != 'null') {
                this.selectedDatasourceId = this.cookieDS;
                this.defaultDatasourceName = this._cookieService.get("datasourceName");
                this.defaultDatasource = this.cookieDS;
                localStorage.setItem("datasource", this.selectedDatasourceId);
                localStorage.setItem("datasourceName", this.defaultDatasourceName);
            }
            else {
                var defaultDatasourceFilter = [];
                defaultDatasourceFilter = _.filter(res.response.datasources, { defaultDatasource: true });
                if (defaultDatasourceFilter && defaultDatasourceFilter.length) {
                    this.selectedDatasourceId = defaultDatasourceFilter[0]._id;
                    this.defaultDatasourceName = defaultDatasourceFilter[0].name;
                    this.defaultDatasource = defaultDatasourceFilter[0]._id;
                    localStorage.setItem("datasource", this.selectedDatasourceId);
                    localStorage.setItem("datasourceName", this.defaultDatasourceName);
                }
                else {
                    this.selectedDatasourceId = res.response.datasources[0]._id;
                    this.defaultDatasourceName = res.response.datasources[0].name;
                    this.defaultDatasource = res.response.datasources[0]._id;
                    localStorage.setItem("datasource", this.selectedDatasourceId);
                    localStorage.setItem("datasourceName", this.defaultDatasourceName);
                }
            }
        });
    }
    clearallNotification(notificationList) {
        let apiUrl = "/notification/updateAll/";
        let queryObj = {
            isread: true,
            feature: "isread",
            realm: this.realm_data
        };
        console.log(queryObj, ",,.,.,..,.");
        this.contentService.updateRequest(queryObj, apiUrl, true)
            .subscribe(res => {
            this.enableNotification = false;
            this.ngOnInit();
        });
    }
    getAllNotification() {
        let query = {
            unread: true,
            toolbar: true,
            realm: this.realm_data
        };
        let apiUrl = "/notification";
        this.notificationData = [];
        this.contentService.getAllReponse(query, apiUrl).subscribe(res => {
            this.notificationList = res.response.notificationList;
            var self = this;
            _.forEach(this.notificationList, function (listItem) {
                let lastIndex = listItem.data ? listItem.data.length - 1 : 0;
                let item = listItem.data[lastIndex];
                console.log(item, ">>>>>item");
                item.feature = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.FEATURE." + item.messageObj.feature);
                let operationStatus = item.messageObj.action + "_" + item.messageObj.status;
                console.log("Operation Data", operationStatus);
                item.status_action = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.ACTION_STATUS." + operationStatus);
                item.msg = item.feature + "   " + item.status_action;
                console.log(item, ">>>>> item");
                item.StatusNew = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.STATUS." + item.messageObj.status);
                item.dateTimeAgo = moment(item.created_at).fromNow();
                item.newstring = item.userName.charAt(0);
                console.log("Convert hours >>>", item.dateTimeAgo);
                item.percentage = self._fuseTranslationLoaderService.instant("NOTIFICATION_FORMATS.PROGRESS." + item.messageObj.status);
                item.operationStatus = operationStatus;
                listItem = Object.assign({}, listItem, item);
                self.notificationData.push(listItem);
                // $(".progress-bar-striped").width(item.percentage);
            });
            this.count = res.response.total;
        });
    }
    navigateToDownloads(msg) {
        console.log("Message value >>>>>>", msg);
        // this.closeSideNav();
        // this.router.navigate(['/pages/download-report']);
        // tslint:disable-next-line:no-conditional-assignment
        // if (msg.messageObj.feature = 'MANAGE_RULESET') {
        //   let mainMenu = this._metaData.filter(obj => obj["menuName"] == "controlManagement");
        //   console.log(">>> Meta data ", this._metaData);
        //   if(mainMenu.length > 0)
        //    {
        //     let submenus = mainMenu[0].subMenu;
        //     console.log("submenu >>>>>", submenus);
        //     let subMenu = submenus.filter(obj => obj["menuName"] == "ruleset");
        //     let menuItem = subMenu[0];
        //     let tempItem = {
        //       id: menuItem.menuName,
        //       title: menuItem.title,
        //       type: 
        //         menuItem.subMenu
        //           ? "collapsable"
        //           : "item",
        //       url: menuItem.routingUI,
        //       configFilePath: menuItem.configFilePath
        //     };
        //     console.log(">>> menuItem ", tempItem);
        //     this.closeSideNav();
        //     this.messageService.sendRouting(tempItem);
        //   }
        // }
        if (msg.additionalData && msg.additionalData.length) {
            var downloadId = msg.additionalData[0].instanceId;
        }
        else {
            var downloadId = null;
        }
        console.log("Report id >>>>>", downloadId);
        let query = {
            reportId: downloadId
        };
        let apiUrl = '/reports/downloadReport';
        if (downloadId == undefined || downloadId == null || downloadId == '') {
            console.log("Report Id Null");
        }
        else {
            this.contentService
                .getAllReponse(query, apiUrl)
                .subscribe(data => {
                const ab = new ArrayBuffer(data.data.length);
                const view = new Uint8Array(ab);
                for (let i = 0; i < data.data.length; i++) {
                    view[i] = data.data[i];
                }
                let downloadType = 'application/zip';
                const file = new Blob([ab], { type: downloadType });
                FileSaver.saveAs(file, msg.additionalData[0].name + '.zip');
            });
        }
    }
    getMetaData() {
        console.log(this._metaData);
        let navigationList = [];
        _.forEach(this._metaData, function (item) {
            var childrenList = [];
            if (item.subMenu && item.subMenu.length) {
                _.forEach(item.subMenu, function (menuItem) {
                    let tempItem = {
                        id: menuItem.menuName,
                        title: menuItem.title,
                        type: menuItem.subMenu && menuItem.subMenu.length
                            ? "collapsable"
                            : "item",
                        url: menuItem.routingUI,
                        configFilePath: menuItem.configFilePath
                    };
                    childrenList.push(tempItem);
                });
            }
            let temp = {
                id: item.menuName,
                title: item.title,
                type: item.subMenu && item.subMenu.length ? "collapsable" : "item",
                translate: item.translate,
                icon: item.icon,
                children: childrenList,
                configFilePath: item.configFilePath
            };
            navigationList.push(temp);
        });
        return navigationList;
    }
    sendMessage(id, triggerFrom) {
        for (var i = 0; i < this.dataSource.length; i++) {
            if (id == this.dataSource[i]._id) {
                localStorage.setItem("datasourceName", this.dataSource[i].name);
            }
        }
        localStorage.setItem("datasource", id);
        if (triggerFrom) {
            this.updateDatasource(id);
        }
        else {
            this.messageService.sendMessage(id);
        }
    }
    updateDatasource(id) {
        let queryObj = {
            defaultDatasource: true
        };
        this.contentService
            .updateRequest(queryObj, '/datasources/setDefault/', id)
            .subscribe(res => {
            this.messageService.sendMessage(id);
        }, error => {
        });
        console.log('actionRedirect >>>>>>>>>>>>>>> queryObj ', queryObj);
    }
    redirectToNotification() {
        console.log(">>> redirectTo Notification link");
        let mainMenu = this._metaData.filter(obj => obj["menuName"] == "manageAccount");
        console.log(">>> mainMenu ", mainMenu);
        if (mainMenu.length > 0) {
            let submenus = mainMenu[0].subMenu;
            let subMenu = submenus.filter(obj => obj["menuId"] == "notification");
            let menuItem = subMenu[0];
            let tempItem = {
                id: menuItem.menuName,
                title: menuItem.title,
                type: menuItem.subMenu && menuItem.subMenu.length
                    ? "collapsable"
                    : "item",
                url: menuItem.routingUI,
                configFilePath: menuItem.configFilePath
            };
            console.log(">>> menuItem ", tempItem);
            this.messageService.sendRouting(tempItem);
        }
        this.enableNotification = false;
    }
    ousideClick(e) {
        this.enableNotification = false;
    }
    Read(input) {
        let apiUrl = "/notification/update";
        let queryObj = {
            isread: true,
            feature: "isread",
            idList: input._id
        };
        this.contentService.createRequest(queryObj, apiUrl)
            .subscribe(res => {
            this.getAllNotification();
        });
        // this.ngOnInit();
    }
    Remove(input) {
        let apiUrl = "/notification/update";
        let queryObj = {
            feature: "delete",
            idList: input._id
        };
        this.contentService.createRequest(queryObj, apiUrl)
            .subscribe(res => {
            this.ngOnInit();
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key) {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }
    /**
     * Search
     *
     * @param value
     */
    search(value) {
        // Do your search here...
        console.log(value);
    }
    /**
     * Set the language
     *
     * @param lang
     */
    setLanguage(lang) {
        // Set the selected language for the toolbar
        this.selectedLanguage = lang;
        // Use the selected language for translations
        this._translateService.use(lang.id);
    }
    logout() {
        console.log("Logout...");
        var dataSource = localStorage.getItem("datasource");
        var datasourceName = localStorage.getItem("datasourceName");
        this._cookieService.set('datasource', dataSource);
        this._cookieService.set('datasourceName', datasourceName);
        localStorage.clear();
        sessionStorage.clear();
        this.oauthService.logOut(false);
    }
    changePwd() {
        // window.location.href = 'http://192.168.2.192:9014/change-password';
        console.log("user:::" + localStorage.getItem("access_token"));
        var access_token = localStorage.getItem("access_token");
        this.accountService.changePassword(access_token);
    }
    // sideNodifi
    openSideNodifiNav(e) {
        console.log(e, "check");
        this.enableNotification = !this.enableNotification;
    }
    closeSideNav() {
        this.enableNotification = false;
    }
    close() {
        this.enableNotification = false;
    }
}
ToolbarComponent.decorators = [
    { type: Component, args: [{
                selector: "toolbar",
                template: "<mat-toolbar class=\"p-0 mat-elevation-z1\">\r\n\r\n  <div fxFlex fxFill fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n    <div fxFlex=\"1 0 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n      <button mat-icon-button class=\"navbar-toggle-button\" *ngIf=\"!hiddenNavbar && !rightNavbar\"\r\n        (click)=\"toggleSidebarOpen('navbar')\" fxHide.gt-md>\r\n        <mat-icon class=\"secondary-text\">menu</mat-icon>\r\n      </button>\r\n\r\n      <div class=\"toolbar-separator\" *ngIf=\"!hiddenNavbar && !rightNavbar\" fxHide.gt-md></div>\r\n\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start center\" *ngIf=\"horizontalNavbar\">\r\n        <div class=\"logo ml-16\">\r\n          <img class=\"logo-icon\" src=\"assets/images/logos/sentri.png\">\r\n        </div>\r\n      </div>\r\n\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n        <!-- <button mat-button [matMenuTriggerFor]=\"realmItems\" class=\"user-button\">\r\n          <div fxLayout=\"row\">\r\n            <span class=\"username mr-12\" fxHide fxShow.gt-sm>{{realm_data}}</span>\r\n            <mat-icon class=\"s-16\" fxHide.xs>keyboard_arrow_down</mat-icon>\r\n          </div>\r\n          <mat-menu #realmItems=\"matMenu\" [overlapTrigger]=\"false\">\r\n            <button mat-menu-item *ngFor=\"let data_obj of data\">\r\n              <span value=\"data_obj._id\">{{data_obj.name}}</span>\r\n            </button>\r\n          </mat-menu>\r\n        </button> -->\r\n        <div class=\"h2 mb-12\" style=\"width: 180px;padding: 10px;padding-top: 45px;\">\r\n          <mat-form-field appearance=\"outline\" fxFlex=\"100\" fxlayout=\"row\" fxlayout.xs=\"column\" style=\"width:100%\">\r\n            <mat-label>Select Tenant</mat-label>\r\n            <mat-select required placeholder=\"Select Tenant\" [(ngModel)]=\"realm_data\">\r\n              <mat-option *ngFor=\"let data_obj of data\" [value]=\"data_obj._id\" (click)=\"getId(data_obj)\">\r\n                {{data_obj.name}}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n        </div>\r\n      </div>\r\n      <div class=\"px-8 px-md-16\">\r\n        <fuse-shortcuts [navigation]=\"navigation\"></fuse-shortcuts>\r\n      </div>\r\n      <div class=\"px-8 px-md-16\">\r\n        <session-expiration></session-expiration>\r\n      </div>\r\n    </div>\r\n    <img [src]=\"english.logoUrl\" style=\"height: 58%;\" alt=\"\">\r\n\r\n    <div class=\"\" fxFlex=\"0 1 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n      <div class=\"h2 mb-12\" style=\"width: 180px;padding: 10px;padding-top: 45px;\">\r\n        <mat-form-field appearance=\"outline\" fxFlex=\"100\" fxlayout=\"row\" fxlayout.xs=\"column\" style=\"width:100%\">\r\n          <mat-label>Select Datasource</mat-label>\r\n          <mat-select [(ngModel)]=\"defaultDatasource\" required placeholder=\"Select Datasource\"\r\n            (selectionChange)=\"sendMessage($event.value, 'toolbar')\">\r\n            <mat-option *ngFor=\"let ds of dataSource\" value=\"{{ds._id}}\">\r\n              {{ds.name}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n     <div>\r\n      <span class=\"sen-lib-notcount\" *ngIf=\"count > 0\"> {{count}}</span>\r\n      <button mat-button (click)=\"openSideNodifiNav($event)\" class=\"sen-lib-outline\">\r\n        <mat-icon matBadgeColor=\"accent\">\r\n          notifications\r\n        </mat-icon>\r\n      </button>\r\n      <mat-menu #notification=\"matMenu\" class=\"sen-lib-mat\">\r\n        <div class=\"sen-lib-ntytopheader\">\r\n          <p class=\"text-center\" style=\"font-size: 16px;\r\n          font-weight: 500;margin-bottom:0\">Notifications</p>\r\n        </div>\r\n        <mat-list role=\"list\">\r\n          <!-- <ng-container *ngFor=\"let notify of notificationList | slice:0:5; let i=index\"> -->\r\n            <ng-container *ngFor=\"let notify of notificationList; let i=index\">\r\n            <mat-list-item role=\"listitem\" class=\"sen-lib-mtlist\">\r\n              <span class=\"sen-lib-mbtm\" style=\"font-weight: 700;\">{{notify.msg}} ,\r\n\r\n                {{notify.created_at | date: 'dd/MM/yyyy hh:mm a'}}</span>\r\n              <button mat-icon-button style=\"float:right\">\r\n                <mat-icon style=\"color:red;\">close</mat-icon>\r\n              </button>\r\n            </mat-list-item>\r\n\r\n          </ng-container>\r\n          <p class=\"text-center\" style=\"margin-top:12px;\">\r\n            <button (click)=\"redirectToNotification()\" class=\"sen-lib-ntybtn btn-warning\">\r\n              <span>View All Notification</span>\r\n            </button>\r\n          </p>\r\n        </mat-list>\r\n      </mat-menu>\r\n\r\n    </div>\r\n    <div class=\"\" fxFlex=\"0 1 auto\" fxLayout=\"row\" fxLayoutAlign=\"start center\">\r\n\r\n      <button mat-button [matMenuTriggerFor]=\"userMenu\" class=\"user-button\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"center center\">\r\n          <img class=\"avatar mr-0 mr-sm-16\" src=\"assets/images/avatars/profile.jpg\">\r\n          <span class=\"username mr-12\" fxHide fxShow.gt-sm>{{user}}</span>\r\n          <mat-icon class=\"s-16\" fxHide.xs>keyboard_arrow_down</mat-icon>\r\n        </div>\r\n      </button>\r\n\r\n      <mat-menu #userMenu=\"matMenu\" [overlapTrigger]=\"false\">\r\n\r\n        <button mat-menu-item (click)=\"changePwd()\" class=\"sen-lib-outline\">\r\n          <mat-icon>https</mat-icon>\r\n          <span>Change Password</span>\r\n        </button>\r\n\r\n        <button mat-menu-item class=\"sen-lib-outline\">\r\n          <mat-icon>help_outline</mat-icon>\r\n          <span>Help / Support</span>\r\n        </button>\r\n\r\n        <button mat-menu-item class=\"\" (click)=\"logout()\" class=\"sen-lib-outline\">\r\n          <mat-icon>exit_to_app</mat-icon>\r\n          <span>Logout</span>\r\n        </button>\r\n\r\n      </mat-menu>\r\n\r\n    </div>\r\n\r\n  </div>\r\n\r\n</mat-toolbar>\r\n\r\n<!-- New UI Raj  -->\r\n<!-- aside nodification side bar -->\r\n<ng-container *ngIf=\"enableNotification \">\r\n  <div class=\"sen-lib-notifiright\">\r\n    <div class=\"sen-lib-simple-wrapper\">\r\n      <div class=\"sen-lib-right-topheader p-3\">\r\n        <div class=\"card-title sen-card-n-title\">Notifications</div>\r\n        <div class=\"card-options ml-auto\">\r\n          <a class=\"sen-lib-sidetop-close sen-lib-anchorclose\" (click)=\"closeSideNav()\">\r\n            <span class=\"material-icons sen-lib-f-20 \">\r\n              highlight_off\r\n            </span>\r\n          </a>\r\n        </div>\r\n      </div>\r\n      <ng-container *ngIf=\"notificationList && notificationList.length==0\">\r\n        <div class=\"panel-body\">\r\n          <!-- <div class=\"list-group list-group-flush\"> -->\r\n          <!-- <div class=\"list-group-item d-flex align-items-center\"> -->\r\n          <div class=\"text-center\">\r\n            <img src=\"/assets/no.PNG\" class=\"sen-lib-no-image\">\r\n          </div>\r\n          <div class=\"text-center\">\r\n            <h6 class=\"sen-lib-notifi-text\">No Notifications Found</h6>\r\n            <p class=\"sen-lib-notifi-message-content\">You have currently no notifications. We'll notify\r\n              you when something new arrives</p>\r\n            <button class=\"btn btn-close-nfbtm\" (click)=\"close()\">Close</button>\r\n          </div>\r\n          <!-- </div> -->\r\n          <!-- </div> -->\r\n        </div>\r\n      </ng-container>\r\n      <div class=\"panel-body\" *ngIf=\"notificationData && notificationData.length > 0\" style=\"overflow-y:auto;\"\r\n        [class.sen-lib-scrollbar]=\"notificationData.length > 5\">\r\n        <div class=\"text-right\">\r\n          <a class=\"sen-lib-clear\" (click)=\"clearallNotification(notificationList)\">Clear All</a>\r\n        </div>\r\n        <ng-container *ngFor=\"let notify of notificationData;let i= index;\">\r\n          <div class=\"list-group list-group-flush\">\r\n            <div class=\"list-group-item d-flex  align-items-center\" style=\"border-bottom:1px solid lightgray;\">\r\n              <div class=\"mr-3\">\r\n                <span class=\"avatar avatar-lg bround  cover-image\">\r\n                  {{notify.newstring}}\r\n                  <span class=\"avatar-status bg-success\"></span>\r\n                </span>\r\n              </div>\r\n              <div class=\"sen-lib-wordbreak\">\r\n                <strong></strong>\r\n                <span *ngIf=\"notify.additionalData && notify.additionalData.length\">\r\n                  {{notify.additionalData[0].name}}\r\n                </span>\r\n                {{notify.msg}}\r\n                <!-- <span> By - {{notify.userName}}</span> -->\r\n                <div class=\"\">\r\n                  <span class=\"material-icons sen-book-markcolor\">\r\n                    bookmark\r\n                  </span>\r\n                  <span class=\"sen-lib-feature\"> {{notify.feature}}</span>\r\n                  <!-- <span class=\"badge badge-success sen-lib-badgedone\">\r\n                    {{notify.StatusNew}}\r\n                  </span> -->\r\n                </div>\r\n                <div class=\"small text-muted\"> {{notify.dateTimeAgo}} ago </div>\r\n                <!-- <span style=\"font-size: 10px;\" *ngIf=\"notify.percentage\"> {{notify.percentage}}</span> -->\r\n                <span class=\"badge badge-success sen-lib-badgedone\">\r\n                  {{notify.StatusNew}}\r\n                </span>\r\n                <span *ngIf=\"notify.percentage == '100 %' && notify.messageObj && (notify.messageObj.feature == 'DOWNLOAD_REPORT_EXPORT' || notify.messageObj.feature == 'RUN_REPORT')\" class=\"material-icons sen-download\" (click)=\"navigateToDownloads(notify)\" matTooltip=\"Download\">\r\n                  get_app\r\n                </span>\r\n                <!-- <ng-container>\r\n                  <div style=\"cursor: pointer;\" class=\"sen-lib-wordbreak\">\r\n\r\n                </div>\r\n                </ng-container> -->\r\n                <ng-container *ngIf=\"notify.messageObj && notify.messageObj.type == 'auditing'\">\r\n                  <span style=\"padding-left: 14px;\r\n                  font-size: 10px;\">{{notify.percentage}}</span>\r\n                <!-- Dynamic Progress Bar style css working Neeed to Verify minor style-->\r\n                  <!-- <div class=\"progress\" role=\"progressbar\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\" [ngClass]=\"{'bg-warning': notify.percentage =='25 %','bg-failed':notify.percentage =='0 %','bg-success': notify.percentage =='100 %','bg-intiated': notify.percentage =='10 %',\r\n                  'bg-reimport': notify.percentage =='50 %'}\">\r\n                    <div  role=\"progressbar\"  \r\n                    class=\"progress-bar progress-bar-striped\">\r\n                    </div> \r\n                  </div> -->\r\n                <!-- Dynamic Progress bar end -->\r\n                <!-- <div class=\"progress\">\r\n                  <div [ngStyle]=\"\" class=\"progress-bar progress-bar-striped bg-success\" role=\"progressbar\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div> -->\r\n                \r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '50 %'\">\r\n                <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 50%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '100 %' && (notify.operationStatus !== 'Create_Success' || notify.operationStatus !== 'Update_Success' || notify.operationStatus !== 'Default_Success')\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 95%; background: green;\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '10 %'\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 20%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '0 %'\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 99%; background: red;\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                <div class=\"progress\" *ngIf=\"notify.percentage == '25 %'\">\r\n                  <div class=\"progress-bar progress-bar-striped\" role=\"progressbar\" style=\"width: 35%; background: rgb(255, 230, 0);\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\r\n                </div>\r\n                </ng-container>\r\n              </div>\r\n              <!-- <div class=\"sen-lib-markusread\" *ngIf=\"notify.isread == false\">\r\n                <button class=\"sen-lib-btn-tooltip\" matTooltip=\"Mark us read\" (click)=\"Read(notify)\"></button>\r\n              </div> -->\r\n              <div>\r\n                <span class=\"material-icons sen-lib-notify-remove\" (click)=\"Read(notify)\"> \r\n                  close\r\n                </span>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </ng-container>\r\n      </div>\r\n      <div class=\"sen-lib-seeAll\" *ngIf=\"notificationData && notificationData.length > 0\">\r\n        <div class=\"text-center\">\r\n          <button class=\"btn btn-warning btn-all-nodifi\" (click)=\"redirectToNotification()\">See All\r\n            Notification</button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div (click)=\"ousideClick($event)\" [class.ui-lib-overlay]=\"enableNotification\"></div>\r\n  <!-- <div class=\"sen-lib-rightbar-overlay\"></div> -->\r\n</ng-container>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: ["toolbar{position:relative;display:-webkit-box;display:flex;-webkit-box-flex:0;flex:0 0 auto;z-index:4}toolbar.below{z-index:2}toolbar .mat-toolbar{position:relative;background:inherit!important;color:inherit!important}toolbar .logo{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}toolbar .logo .logo-icon{width:38px}toolbar .chat-panel-toggle-button,toolbar .language-button,toolbar .quick-panel-toggle-button,toolbar .user-button,toolbar fuse-search-bar{min-width:64px;height:64px}@media screen and (max-width:599px){toolbar .chat-panel-toggle-button,toolbar .language-button,toolbar .quick-panel-toggle-button,toolbar .user-button,toolbar fuse-search-bar{height:56px}}toolbar .navbar-toggle-button{min-width:56px;height:56px}toolbar .toolbar-separator{height:64px;width:1px}@media screen and (max-width:599px){toolbar .toolbar-separator{height:56px}}.sen-lib-outline,.user-button{outline:0!important}.sen-lib-mbtm{margin-bottom:5px!important}.sen-lib-mtlist{border-bottom:1px solid #ccc!important;height:initial!important;font-size:14px!important}.sen-lib-ntybtn{text-align:center;padding:12px;text-decoration:none!important;font-size:14px;font-weight:500;width:68%;border-radius:25px;border:0;color:#fff!important;outline:0!important;background:#2c69f5}.sen-lib-notcount{border-radius:20px;background:#ec1707;margin-right:-22px;margin-top:3px;width:17px;height:17px;background-color:#ff8f00!important;position:relative;top:-9px;z-index:999;left:25px;font-size:9px;line-height:12px;font-family:Roboto,sans-serif;color:#fff;font-weight:700;padding:2px}.sen-lib-ntytopheader{background:linear-gradient(135deg,#fad961 0,#f76b1c 100%);margin-top:-9px;padding-top:10px;padding-bottom:10px;color:#fff}.sen-lib-right-topheader{top:0;width:100%;z-index:1;display:-webkit-box;display:flex;margin-bottom:12px;background:#ecf0fa}.card-title{font-weight:700;color:#242f48;font-size:14px;text-transform:uppercase}.sen-lib-anchorclose{cursor:pointer;text-decoration:none!important;position:relative;top:6px}.sen-lib-f-20{font-size:20px}.cover-image{text-transform:capitalize;width:50px!important;height:50px!important;background:#4161ef;color:#fff!important;font-size:13px;padding:5px 12px;border-radius:50%}.sen-lib-wordbreak{width:100%;word-break:break-all!important;font-size:13px;font-weight:500}.sen-book-markcolor{color:red}.sen-lib-feature{color:#0e6377!important}.sen-lib-btn-tooltip{padding:0;-webkit-transition:none;transition:none;width:18px!important;height:18px;font-size:0;background:#0052cc;border:4px solid #ebecf0;border-radius:100%;margin-right:5px;visibility:visible}.sen-lib-badgedone{background:#e3fcef!important;color:#064!important;position:relative;left:12px}.sen-lib-seeAll{margin-top:12px}button.btn.btn-warning.btn-all-nodifi{background:#4161ef;color:#fff;font-size:14px;border-color:#4161ef;width:66%}.sen-card-n-title{top:4px;position:relative}.sen-lib-notify-remove{cursor:pointer;color:red}.sen-lib-scrollbar{height:600px!important;margin-right:15px}.sen-lib-scrollbar::-webkit-scrollbar{width:8px!important}.sen-lib-scrollbar::-webkit-scrollbar-track{background:#f1f1f1!important}.sen-lib-scrollbar::-webkit-scrollbar-thumb{background:#dedee0!important}.sen-lib-notifi-text{font-size:20px;font-weight:700}.sen-lib-notifi-message-content{font-size:14px;font-style:italic;color:gray}.sen-lib-no-image{width:200px;margin-top:70px}button.btn.btn-close-nfbtm{font-size:14px;font-weight:600;background:red;color:#fff;width:35%}.sen-download{color:red;font-size:20px;margin-left:6px;cursor:pointer}.progress-bar.progress-bar-striped.bg-warning{width:35%;background:#ffe600}.progress-bar.progress-bar-striped.bg-success{width:95%;background:green}.progress-bar.progress-bar-striped.bg-failed{width:99%;background:red}.progress-bar.progress-bar-striped.bg-danger{width:50%;background:#00f}.sen-lib-clear{color:#072bbf;font-size:14px;font-weight:500;margin-right:12px;position:relative;bottom:5px;cursor:pointer}.progress-bar-striped{background-image:linear-gradient(45deg,rgba(255,255,255,.15) 25%,transparent 25%,transparent 50%,rgba(255,255,255,.15) 50%,rgba(255,255,255,.15) 75%,transparent 75%,transparent)!important;background-size:1rem 1rem!important}.ui-lib-overlay{background-color:rgba(52,58,64,.55);position:fixed;left:0;right:0;top:0;bottom:0;z-index:9998;-webkit-transition:.2s ease-out;transition:.2s ease-out;display:block;height:100vh}"]
            }] }
];
/** @nocollapse */
ToolbarComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: FuseConfigService },
    { type: FuseSidebarService },
    { type: TranslateService },
    { type: ContentService },
    { type: AccountService },
    { type: OAuthService },
    { type: MessageService },
    { type: CookieService },
    { type: LoaderService },
    { type: undefined, decorators: [{ type: Inject, args: ["metaData",] }] },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9vbGJhci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsidG9vbGJhci90b29sYmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUNMLFNBQVMsRUFHVCxpQkFBaUIsRUFDakIsTUFBTSxFQUNQLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDdkMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3ZELE9BQU8sS0FBSyxDQUFDLE1BQU0sUUFBUSxDQUFDO0FBRTVCLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUc1RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDNUYsb0NBQW9DO0FBQ3BDLE9BQU8sS0FBSyxPQUFPLE1BQU0sUUFBUSxDQUFDO0FBQ2xDLE1BQU0sTUFBTSxHQUFHLE9BQU8sQ0FBQztBQUN2QixPQUFPLEtBQUssU0FBUyxNQUFNLFlBQVksQ0FBQztBQVN4QyxNQUFNLE9BQU8sZ0JBQWdCO0lBd0IzQjs7Ozs7O09BTUc7SUFDSCxZQUNVLDZCQUEyRCxFQUMzRCxrQkFBcUMsRUFDckMsbUJBQXVDLEVBQ3ZDLGlCQUFtQyxFQUNuQyxjQUE4QixFQUM5QixjQUE4QixFQUM5QixZQUEwQixFQUMxQixjQUE4QixFQUM5QixjQUE2QixFQUM3QixhQUE0QixFQUNSLFNBQVMsRUFDWCxPQUFPO1FBWHpCLGtDQUE2QixHQUE3Qiw2QkFBNkIsQ0FBOEI7UUFDM0QsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQyx3QkFBbUIsR0FBbkIsbUJBQW1CLENBQW9CO1FBQ3ZDLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBa0I7UUFDbkMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWU7UUFDN0Isa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDUixjQUFTLEdBQVQsU0FBUyxDQUFBO1FBQ1gsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUVqQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDN0QsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxpQkFBaUIsR0FBRztZQUN2QjtnQkFDRSxLQUFLLEVBQUUsUUFBUTtnQkFDZixJQUFJLEVBQUUsNkJBQTZCO2dCQUNuQyxLQUFLLEVBQUUsU0FBUzthQUNqQjtZQUNEO2dCQUNFLEtBQUssRUFBRSxNQUFNO2dCQUNiLElBQUksRUFBRSxZQUFZO2dCQUNsQixLQUFLLEVBQUUsU0FBUzthQUNqQjtZQUNEO2dCQUNFLEtBQUssRUFBRSxnQkFBZ0I7Z0JBQ3ZCLElBQUksRUFBRSxtQkFBbUI7Z0JBQ3pCLEtBQUssRUFBRSxTQUFTO2FBQ2pCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFdBQVc7Z0JBQ2xCLElBQUksRUFBRSxvQ0FBb0M7Z0JBQzFDLEtBQUssRUFBRSxTQUFTO2FBQ2pCO1lBQ0Q7Z0JBQ0UsS0FBSyxFQUFFLFNBQVM7Z0JBQ2hCLElBQUksRUFBRSxvQ0FBb0M7Z0JBQzFDLEtBQUssRUFBRSxTQUFTO2FBQ2pCO1NBQ0YsQ0FBQztRQUVGLElBQUksQ0FBQyxTQUFTLEdBQUc7WUFDZjtnQkFDRSxFQUFFLEVBQUUsSUFBSTtnQkFDUixLQUFLLEVBQUUsU0FBUztnQkFDaEIsSUFBSSxFQUFFLElBQUk7YUFDWDtZQUNEO2dCQUNFLEVBQUUsRUFBRSxJQUFJO2dCQUNSLEtBQUssRUFBRSxTQUFTO2dCQUNoQixJQUFJLEVBQUUsSUFBSTthQUNYO1NBQ0YsQ0FBQztRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO1lBQ3ZELElBQUcsSUFBSSxDQUFDLFVBQVUsRUFBQztnQkFDakIsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUE7YUFDeEI7WUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3hDLElBQUcsUUFBUSxJQUFJLFFBQVEsQ0FBQyxJQUFJLElBQUksUUFBUSxDQUFDLElBQUksSUFBSSxNQUFNLEVBQUM7Z0JBQ3RELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDO2dCQUN2QyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDdkM7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLEVBQUU7YUFDM0MsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLGtCQUFrQixDQUFDLENBQUM7WUFDckMsSUFBRyxJQUFJLENBQUMsVUFBVSxFQUFDO2dCQUNqQixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUMzQjtRQUNILENBQUMsQ0FBQyxDQUFBO1FBQ0YsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7UUFFckMsMkJBQTJCO1FBQzNCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxRQUFRO1FBQ04sa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNO2FBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQztZQUNsRSxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsS0FBSyxPQUFPLENBQUM7WUFDL0QsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDO1FBQzdELENBQUMsQ0FBQyxDQUFDO1FBRUwsbURBQW1EO1FBQ25ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDN0MsRUFBRSxFQUFFLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXO1NBQ3ZDLENBQUMsQ0FBQztRQUNILElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUMvQyxzQkFBc0I7WUFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUN2Qix1Q0FBdUM7WUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDdkIsSUFBSSxTQUFTLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekQsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztnQkFDdEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQzthQUNwQztZQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUMvQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDckQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQ0FBbUMsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDakUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7WUFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFDSCwyQkFBMkI7UUFDM0IsNkJBQTZCO0lBQy9CLENBQUM7SUFDRCxLQUFLLENBQUMsS0FBSztRQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FDOUMsQ0FBQyxJQUFTLEVBQUUsRUFBRTtZQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQzFCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDeEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3pDLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUN2RCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsVUFBVTtZQUM1RCxZQUFZLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ3RDLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDM0IsQ0FBQyxFQUNELEtBQUssQ0FBQyxFQUFFO1lBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUNGLENBQUM7SUFDSixDQUFDO0lBRUQsZ0JBQWdCO1FBRWQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNqQyxJQUFJLEtBQUssR0FBRztZQUNWLEtBQUssRUFBRyxJQUFJLENBQUMsVUFBVTtTQUN4QixDQUFDO1FBQ0YsSUFBSSxNQUFNLEdBQUcsa0JBQWtCLENBQUM7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDL0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUMsS0FBSyxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUM7WUFDM0MsSUFBSSxVQUFVLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFJLHlCQUF5QjtZQUNqRixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUksa0JBQWtCO1lBQzVFLDBDQUEwQztZQUMxQyxJQUFJO1lBQ0osNENBQTRDO1lBQzVDLHlFQUF5RTtZQUN6RSx5Q0FBeUM7WUFDekMsVUFBVTtZQUNWLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLE1BQU0sRUFBRTtnQkFDNUMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7Z0JBQzFDLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO2dCQUN2RSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDdkMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7Z0JBQzlELFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7YUFDcEU7aUJBQU07Z0JBQ0wsSUFBSSx1QkFBdUIsR0FBRyxFQUFFLENBQUM7Z0JBQ2pDLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsRUFBQyxpQkFBaUIsRUFBRSxJQUFJLEVBQUMsQ0FBQyxDQUFDO2dCQUN4RixJQUFHLHVCQUF1QixJQUFJLHVCQUF1QixDQUFDLE1BQU0sRUFBQztvQkFDM0QsSUFBSSxDQUFDLG9CQUFvQixHQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztvQkFDM0QsSUFBSSxDQUFDLHFCQUFxQixHQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztvQkFDN0QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLHVCQUF1QixDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztvQkFDeEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7b0JBQzlELFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUM7aUJBQ3BFO3FCQUFJO29CQUNILElBQUksQ0FBQyxvQkFBb0IsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7b0JBQzVELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7b0JBQzlELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7b0JBQ3pELFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO29CQUM5RCxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2lCQUNwRTthQUVGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0Qsb0JBQW9CLENBQUMsZ0JBQWdCO1FBQ25DLElBQUksTUFBTSxHQUFHLDBCQUEwQixDQUFDO1FBQ3hDLElBQUksUUFBUSxHQUFHO1lBQ2IsTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsUUFBUTtZQUNqQixLQUFLLEVBQUcsSUFBSSxDQUFDLFVBQVU7U0FDeEIsQ0FBQztRQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFBO1FBQ25DLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDO2FBQ3RELFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNmLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7WUFDaEMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELGtCQUFrQjtRQUNoQixJQUFJLEtBQUssR0FBRztZQUNWLE1BQU0sRUFBRSxJQUFJO1lBQ1osT0FBTyxFQUFFLElBQUk7WUFDYixLQUFLLEVBQUcsSUFBSSxDQUFDLFVBQVU7U0FDeEIsQ0FBQztRQUNGLElBQUksTUFBTSxHQUFFLGVBQWUsQ0FBQztRQUM1QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsRUFBRSxDQUFDO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDL0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUM7WUFDdEQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBRWhCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLFVBQVMsUUFBUTtnQkFDaEQsSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzdELElBQUksSUFBSSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQ3BDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFdBQVcsQ0FBQyxDQUFBO2dCQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3ZELCtCQUErQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzdELElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQztnQkFDNUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxlQUFlLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLHFDQUFxQyxHQUFHLGVBQWUsQ0FBQyxDQUFDO2dCQUN6SCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7Z0JBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUMvQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3pELDhCQUE4QixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzNELElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDekMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ25ELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyxnQ0FBZ0MsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN4SCxJQUFJLENBQUMsZUFBZSxHQUFHLGVBQWUsQ0FBQztnQkFDdkMsUUFBUSxxQkFBTyxRQUFRLEVBQUssSUFBSSxDQUFDLENBQUE7Z0JBQ2pDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ3JDLHFEQUFxRDtZQUV2RCxDQUFDLENBQ0YsQ0FBQztZQUNGLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7UUFDbEMsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBQ0QsbUJBQW1CLENBQUMsR0FBRztRQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3pDLHVCQUF1QjtRQUN2QixvREFBb0Q7UUFDcEQscURBQXFEO1FBQ3JELG1EQUFtRDtRQUNuRCx5RkFBeUY7UUFDekYsbURBQW1EO1FBQ25ELDRCQUE0QjtRQUM1QixPQUFPO1FBQ1AsMENBQTBDO1FBQzFDLDhDQUE4QztRQUU5QywwRUFBMEU7UUFDMUUsaUNBQWlDO1FBQ2pDLHVCQUF1QjtRQUN2QiwrQkFBK0I7UUFDL0IsK0JBQStCO1FBQy9CLGVBQWU7UUFDZiwyQkFBMkI7UUFDM0IsNEJBQTRCO1FBQzVCLHNCQUFzQjtRQUN0QixpQ0FBaUM7UUFDakMsZ0RBQWdEO1FBQ2hELFNBQVM7UUFDVCw4Q0FBOEM7UUFDOUMsMkJBQTJCO1FBQzNCLGlEQUFpRDtRQUNqRCxNQUFNO1FBQ04sSUFBSTtRQUNKLElBQUksR0FBRyxDQUFDLGNBQWMsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRTtZQUNuRCxJQUFJLFVBQVUsR0FBRyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztTQUNuRDthQUFNO1lBQ0wsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLENBQUMsQ0FBQztRQUMzQyxJQUFJLEtBQUssR0FBRztZQUNWLFFBQVEsRUFBRSxVQUFVO1NBQ3JCLENBQUE7UUFDRCxJQUFJLE1BQU0sR0FBRyx5QkFBeUIsQ0FBQztRQUN2QyxJQUFJLFVBQVUsSUFBSSxTQUFTLElBQUksVUFBVSxJQUFJLElBQUksSUFBSSxVQUFVLElBQUksRUFBRSxFQUFFO1lBQ3JFLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUMvQjthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWM7aUJBQ2xCLGFBQWEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDO2lCQUM1QixTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2hCLE1BQU0sRUFBRSxHQUFHLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzdDLE1BQU0sSUFBSSxHQUFHLElBQUksVUFBVSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUNoQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ3pDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUN4QjtnQkFDRCxJQUFJLFlBQVksR0FBRyxpQkFBaUIsQ0FBQztnQkFDckMsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO2dCQUNwRCxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksR0FBRyxNQUFNLENBQUMsQ0FBQztZQUNoRSxDQUFDLENBQUMsQ0FBQztTQUNGO0lBRUQsQ0FBQztJQUtILFdBQVc7UUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1QixJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUM7UUFFeEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFVBQVUsSUFBSTtZQUN0QyxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7WUFDdEIsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFO2dCQUN2QyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxRQUFRO29CQUN4QyxJQUFJLFFBQVEsR0FBRzt3QkFDYixFQUFFLEVBQUUsUUFBUSxDQUFDLFFBQVE7d0JBQ3JCLEtBQUssRUFBRSxRQUFRLENBQUMsS0FBSzt3QkFDckIsSUFBSSxFQUNGLFFBQVEsQ0FBQyxPQUFPLElBQUksUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzRCQUN6QyxDQUFDLENBQUMsYUFBYTs0QkFDZixDQUFDLENBQUMsTUFBTTt3QkFDWixHQUFHLEVBQUUsUUFBUSxDQUFDLFNBQVM7d0JBRXZCLGNBQWMsRUFBRSxRQUFRLENBQUMsY0FBYztxQkFDeEMsQ0FBQztvQkFDRixZQUFZLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQzthQUNKO1lBRUQsSUFBSSxJQUFJLEdBQUc7Z0JBQ1QsRUFBRSxFQUFFLElBQUksQ0FBQyxRQUFRO2dCQUNqQixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7Z0JBQ2pCLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLE1BQU07Z0JBQ2xFLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUztnQkFDekIsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO2dCQUNmLFFBQVEsRUFBRSxZQUFZO2dCQUN0QixjQUFjLEVBQUUsSUFBSSxDQUFDLGNBQWM7YUFDcEMsQ0FBQztZQUNGLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDNUIsQ0FBQyxDQUFDLENBQUM7UUFFSCxPQUFPLGNBQWMsQ0FBQztJQUN4QixDQUFDO0lBQ0QsV0FBVyxDQUFDLEVBQUUsRUFBRSxXQUFXO1FBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUMvQyxJQUFJLEVBQUUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRTtnQkFDaEMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2pFO1NBQ0Y7UUFDRCxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsQ0FBQztRQUN2QyxJQUFHLFdBQVcsRUFBQztZQUNiLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUMzQjthQUFJO1lBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDckM7SUFDSCxDQUFDO0lBQ0QsZ0JBQWdCLENBQUUsRUFBRTtRQUNsQixJQUFJLFFBQVEsR0FBRztZQUNYLGlCQUFpQixFQUFHLElBQUk7U0FDekIsQ0FBQztRQUNKLElBQUksQ0FBQyxjQUFjO2FBQ2xCLGFBQWEsQ0FBQyxRQUFRLEVBQUMsMEJBQTBCLEVBQUUsRUFBRSxDQUFDO2FBQ3RELFNBQVMsQ0FDUixHQUFHLENBQUMsRUFBRTtZQUNKLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtRQUNSLENBQUMsQ0FDRixDQUFDO1FBQ0YsT0FBTyxDQUFDLEdBQUcsQ0FBQywwQ0FBMEMsRUFBRSxRQUFRLENBQUMsQ0FBQTtJQUNuRSxDQUFDO0lBQ0Qsc0JBQXNCO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0NBQWtDLENBQUMsQ0FBQztRQUNoRCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxlQUFlLENBQUMsQ0FBQztRQUNoRixPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN2QyxJQUFHLFFBQVEsQ0FBQyxNQUFNLEdBQUMsQ0FBQyxFQUNuQjtZQUNDLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUM7WUFDbkMsSUFBSSxPQUFPLEdBQUcsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxjQUFjLENBQUMsQ0FBQztZQUN0RSxJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDMUIsSUFBSSxRQUFRLEdBQUc7Z0JBQ2IsRUFBRSxFQUFFLFFBQVEsQ0FBQyxRQUFRO2dCQUNyQixLQUFLLEVBQUUsUUFBUSxDQUFDLEtBQUs7Z0JBQ3JCLElBQUksRUFDRixRQUFRLENBQUMsT0FBTyxJQUFJLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTTtvQkFDekMsQ0FBQyxDQUFDLGFBQWE7b0JBQ2YsQ0FBQyxDQUFDLE1BQU07Z0JBQ1osR0FBRyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2dCQUN2QixjQUFjLEVBQUUsUUFBUSxDQUFDLGNBQWM7YUFDeEMsQ0FBQztZQUNGLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztJQUNsQyxDQUFDO0lBQ0QsV0FBVyxDQUFDLENBQUM7UUFDWCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxJQUFJLENBQUMsS0FBSztRQUNSLElBQUksTUFBTSxHQUFHLHNCQUFzQixDQUFDO1FBQ3BDLElBQUksUUFBUSxHQUFHO1lBQ2IsTUFBTSxFQUFFLElBQUk7WUFDWixPQUFPLEVBQUUsUUFBUTtZQUNqQixNQUFNLEVBQUUsS0FBSyxDQUFDLEdBQUc7U0FDbEIsQ0FBQztRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7YUFDaEQsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2YsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7UUFDOUIsQ0FBQyxDQUFDLENBQUM7UUFDSCxtQkFBbUI7SUFFckIsQ0FBQztJQUdELE1BQU0sQ0FBQyxLQUFLO1FBQ1YsSUFBSSxNQUFNLEdBQUcsc0JBQXNCLENBQUM7UUFDcEMsSUFBSSxRQUFRLEdBQUc7WUFDYixPQUFPLEVBQUUsUUFBUTtZQUNqQixNQUFNLEVBQUUsS0FBSyxDQUFDLEdBQUc7U0FDbEIsQ0FBQztRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUM7YUFDaEQsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2YsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUtEOztPQUVHO0lBQ0gsV0FBVztRQUNULHFDQUFxQztRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDbEMsQ0FBQztJQUVELHdHQUF3RztJQUN4RyxtQkFBbUI7SUFDbkIsd0dBQXdHO0lBRXhHOzs7O09BSUc7SUFDSCxpQkFBaUIsQ0FBQyxHQUFHO1FBQ25CLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDeEQsQ0FBQztJQUVEOzs7O09BSUc7SUFDSCxNQUFNLENBQUMsS0FBSztRQUNWLHlCQUF5QjtRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3JCLENBQUM7SUFFRDs7OztPQUlHO0lBQ0gsV0FBVyxDQUFDLElBQUk7UUFDZCw0Q0FBNEM7UUFDNUMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUU3Qiw2Q0FBNkM7UUFDN0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDdEMsQ0FBQztJQUNELE1BQU07UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3pCLElBQUksVUFBVSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDcEQsSUFBSSxjQUFjLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxVQUFVLENBQUMsQ0FBQztRQUNsRCxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUMxRCxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDckIsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxTQUFTO1FBQ1Asc0VBQXNFO1FBQ3RFLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUM5RCxJQUFJLFlBQVksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFDRCxhQUFhO0lBQ2IsaUJBQWlCLENBQUMsQ0FBQztRQUNqQixPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQTtRQUN2QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDckQsQ0FBQztJQUNELFlBQVk7UUFDVixJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO0lBQ2xDLENBQUM7SUFDRCxLQUFLO1FBQ0gsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztJQUNsQyxDQUFDOzs7WUF2aEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsU0FBUztnQkFDbkIscXpiQUF1QztnQkFFdkMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3RDOzs7O1lBWlEsNEJBQTRCO1lBVDVCLGlCQUFpQjtZQUNqQixrQkFBa0I7WUFKbEIsZ0JBQWdCO1lBS2hCLGNBQWM7WUFHZCxjQUFjO1lBQ2QsWUFBWTtZQW5CWixjQUFjO1lBb0JkLGFBQWE7WUFDYixhQUFhOzRDQXdEakIsTUFBTSxTQUFDLFVBQVU7NENBQ2pCLE1BQU0sU0FBQyxTQUFTIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi8uLi9fc2VydmljZXMvbWVzc2FnZS5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uRGVzdHJveSxcclxuICBPbkluaXQsXHJcbiAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgSW5qZWN0XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5pbXBvcnQgKiBhcyBfIGZyb20gXCJsb2Rhc2hcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VDb25maWdTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEZ1c2VTaWRlYmFyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9jb21wb25lbnRzL3NpZGViYXIvc2lkZWJhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSBcIi4uL2NvbnRlbnQvY29udGVudC5zZXJ2aWNlXCI7XHJcblxyXG5pbXBvcnQgeyBuYXZpZ2F0aW9uIH0gZnJvbSBcIi4uL25hdmlnYXRpb24vbmF2aWdhdGlvblwiO1xyXG5pbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gXCIuLi9zaGFyZWQvYXV0aC9hY2NvdW50LnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgT0F1dGhTZXJ2aWNlIH0gZnJvbSBcImFuZ3VsYXItb2F1dGgyLW9pZGNcIjtcclxuaW1wb3J0IHsgQ29va2llU2VydmljZSB9IGZyb20gJ25neC1jb29raWUtc2VydmljZSc7XHJcbmltcG9ydCB7IExvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UgfSBmcm9tICcuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZSc7XHJcbi8vIGltcG9ydCAqIGFzIG1vbWVudCBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgKiBhcyBtb21lbnRfIGZyb20gJ21vbWVudCc7XHJcbmNvbnN0IG1vbWVudCA9IG1vbWVudF87XHJcbmltcG9ydCAqIGFzIEZpbGVTYXZlciBmcm9tIFwiZmlsZS1zYXZlclwiO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInRvb2xiYXJcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL3Rvb2xiYXIuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vdG9vbGJhci5jb21wb25lbnQuc2Nzc1wiXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUb29sYmFyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gIGhvcml6b250YWxOYXZiYXI6IGJvb2xlYW47XHJcbiAgcmlnaHROYXZiYXI6IGJvb2xlYW47XHJcbiAgaGlkZGVuTmF2YmFyOiBib29sZWFuO1xyXG4gIGxhbmd1YWdlczogYW55O1xyXG4gIG5hdmlnYXRpb246IGFueTtcclxuICBzZWxlY3RlZExhbmd1YWdlOiBhbnk7XHJcbiAgdXNlclN0YXR1c09wdGlvbnM6IGFueVtdO1xyXG4gIGRhdGFTb3VyY2U6IGFueTtcclxuICBub3RpZmljYXRpb25MaXN0OiBhbnlbXTtcclxuICBzZWxlY3RlZERhdGFzb3VyY2VJZDogYW55O1xyXG4gIGRlZmF1bHREYXRhc291cmNlTmFtZTogYW55O1xyXG4gIGRlZmF1bHREYXRhc291cmNlOiBhbnk7XHJcbiAgZGVmYXVsdFJlYWxtOiBzdHJpbmc7XHJcbiAgbm90aWZpY2F0aW9uRGF0YSA6IGFueVtdO1xyXG4gIHVzZXI6IGFueTtcclxuICBkYXRhOiBhbnk7XHJcbiAgcmVhbG1fZGF0YTogYW55O1xyXG4gIGNvb2tpZURTOiBhbnk7XHJcbiAgY291bnQ6IG51bWJlcjtcclxuICAvLyBQcml2YXRlXHJcbiAgcHJpdmF0ZSBfdW5zdWJzY3JpYmVBbGw6IFN1YmplY3Q8YW55PjtcclxuICBlbmFibGVOb3RpZmljYXRpb246IGJvb2xlYW47XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge0Z1c2VDb25maWdTZXJ2aWNlfSBfZnVzZUNvbmZpZ1NlcnZpY2VcclxuICAgKiBAcGFyYW0ge0Z1c2VTaWRlYmFyU2VydmljZX0gX2Z1c2VTaWRlYmFyU2VydmljZVxyXG4gICAqIEBwYXJhbSB7VHJhbnNsYXRlU2VydmljZX0gX3RyYW5zbGF0ZVNlcnZpY2VcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9mdXNlQ29uZmlnU2VydmljZTogRnVzZUNvbmZpZ1NlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9mdXNlU2lkZWJhclNlcnZpY2U6IEZ1c2VTaWRlYmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgX3RyYW5zbGF0ZVNlcnZpY2U6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgYWNjb3VudFNlcnZpY2U6IEFjY291bnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBvYXV0aFNlcnZpY2U6IE9BdXRoU2VydmljZSxcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfY29va2llU2VydmljZTogQ29va2llU2VydmljZSxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIEBJbmplY3QoXCJtZXRhRGF0YVwiKSBwcml2YXRlIF9tZXRhRGF0YSxcclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHB1YmxpYyBlbmdsaXNoXHJcbiAgKSB7XHJcbiAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmxvYWRUcmFuc2xhdGlvbnMoZW5nbGlzaCk7XHJcbiAgICAvLyBTZXQgdGhlIGRlZmF1bHRzXHJcbiAgICB0aGlzLnVzZXJTdGF0dXNPcHRpb25zID0gW1xyXG4gICAgICB7XHJcbiAgICAgICAgdGl0bGU6IFwiT25saW5lXCIsXHJcbiAgICAgICAgaWNvbjogXCJpY29uLWNoZWNrYm94LW1hcmtlZC1jaXJjbGVcIixcclxuICAgICAgICBjb2xvcjogXCIjNENBRjUwXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIHRpdGxlOiBcIkF3YXlcIixcclxuICAgICAgICBpY29uOiBcImljb24tY2xvY2tcIixcclxuICAgICAgICBjb2xvcjogXCIjRkZDMTA3XCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIHRpdGxlOiBcIkRvIG5vdCBEaXN0dXJiXCIsXHJcbiAgICAgICAgaWNvbjogXCJpY29uLW1pbnVzLWNpcmNsZVwiLFxyXG4gICAgICAgIGNvbG9yOiBcIiNGNDQzMzZcIlxyXG4gICAgICB9LFxyXG4gICAgICB7XHJcbiAgICAgICAgdGl0bGU6IFwiSW52aXNpYmxlXCIsXHJcbiAgICAgICAgaWNvbjogXCJpY29uLWNoZWNrYm94LWJsYW5rLWNpcmNsZS1vdXRsaW5lXCIsXHJcbiAgICAgICAgY29sb3I6IFwiI0JEQkRCRFwiXHJcbiAgICAgIH0sXHJcbiAgICAgIHtcclxuICAgICAgICB0aXRsZTogXCJPZmZsaW5lXCIsXHJcbiAgICAgICAgaWNvbjogXCJpY29uLWNoZWNrYm94LWJsYW5rLWNpcmNsZS1vdXRsaW5lXCIsXHJcbiAgICAgICAgY29sb3I6IFwiIzYxNjE2MVwiXHJcbiAgICAgIH1cclxuICAgIF07XHJcblxyXG4gICAgdGhpcy5sYW5ndWFnZXMgPSBbXHJcbiAgICAgIHtcclxuICAgICAgICBpZDogXCJlblwiLFxyXG4gICAgICAgIHRpdGxlOiBcIkVuZ2xpc2hcIixcclxuICAgICAgICBmbGFnOiBcInVzXCJcclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGlkOiBcInRyXCIsXHJcbiAgICAgICAgdGl0bGU6IFwiVHVya2lzaFwiLFxyXG4gICAgICAgIGZsYWc6IFwidHJcIlxyXG4gICAgICB9XHJcbiAgICBdO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5nZXREYXRhc291cmNlKCkuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgaWYodGhpcy5yZWFsbV9kYXRhKXtcclxuICAgICAgICB0aGlzLmdldEFsbERhdGFzb3VyY2UoKVxyXG4gICAgICB9XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiIHNldERhdGFzb3VjZSBcIiwgcmVzcG9uc2UpO1xyXG4gICAgICBpZihyZXNwb25zZSAmJiByZXNwb25zZS50ZXh0ICYmIHJlc3BvbnNlLnRleHQgIT0gJ251bGwnKXtcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gcmVzcG9uc2UudGV4dDtcclxuICAgICAgICB0aGlzLnNlbmRNZXNzYWdlKHJlc3BvbnNlLnRleHQsIG51bGwpO1xyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2UuZ2V0VHJpZ2dlck5vdGlmaWNhdGlvbigpXHJcbiAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhkYXRhLFwiPj4+Pj4+Pj4+Pj4gREFUQVwiKTtcclxuICAgICAgaWYodGhpcy5yZWFsbV9kYXRhKXtcclxuICAgICAgICB0aGlzLmdldEFsbE5vdGlmaWNhdGlvbigpO1xyXG4gICAgICB9XHJcbiAgICB9KVxyXG4gICAgdGhpcy5uYXZpZ2F0aW9uID0gdGhpcy5nZXRNZXRhRGF0YSgpO1xyXG5cclxuICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwgPSBuZXcgU3ViamVjdCgpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIExpZmVjeWNsZSBob29rc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIE9uIGluaXRcclxuICAgKi9cclxuICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgIC8vIFN1YnNjcmliZSB0byB0aGUgY29uZmlnIGNoYW5nZXNcclxuICAgIHRoaXMuX2Z1c2VDb25maWdTZXJ2aWNlLmNvbmZpZ1xyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpKVxyXG4gICAgICAuc3Vic2NyaWJlKHNldHRpbmdzID0+IHtcclxuICAgICAgICB0aGlzLmhvcml6b250YWxOYXZiYXIgPSBzZXR0aW5ncy5sYXlvdXQubmF2YmFyLnBvc2l0aW9uID09PSBcInRvcFwiO1xyXG4gICAgICAgIHRoaXMucmlnaHROYXZiYXIgPSBzZXR0aW5ncy5sYXlvdXQubmF2YmFyLnBvc2l0aW9uID09PSBcInJpZ2h0XCI7XHJcbiAgICAgICAgdGhpcy5oaWRkZW5OYXZiYXIgPSBzZXR0aW5ncy5sYXlvdXQubmF2YmFyLmhpZGRlbiA9PT0gdHJ1ZTtcclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gU2V0IHRoZSBzZWxlY3RlZCBsYW5ndWFnZSBmcm9tIGRlZmF1bHQgbGFuZ3VhZ2VzXHJcbiAgICB0aGlzLnNlbGVjdGVkTGFuZ3VhZ2UgPSBfLmZpbmQodGhpcy5sYW5ndWFnZXMsIHtcclxuICAgICAgaWQ6IHRoaXMuX3RyYW5zbGF0ZVNlcnZpY2UuY3VycmVudExhbmdcclxuICAgIH0pO1xyXG4gICAgbGV0IHJlYWxtRGF0YSA9IHRoaXMuX2Nvb2tpZVNlcnZpY2UuZ2V0KFwicmVhbG1cIik7XHJcbiAgICB0aGlzLmFjY291bnRTZXJ2aWNlLnN3aXRjaEV2ZW50LnN1YnNjcmliZShkYXRhID0+IHtcclxuICAgICAgLy9nZXQgY3VycmVudCB1c2VybmFtZVxyXG4gICAgICB0aGlzLnVzZXIgPSBkYXRhLnVzZXJuYW1lO1xyXG4gICAgICB0aGlzLmRhdGEgPSBkYXRhLnJlYWxtO1xyXG4gICAgICAvLyB0aGlzLnJlYWxtX2RhdGEgPSBkYXRhLnJlYWxtWzBdLl9pZDtcclxuICAgICAgY29uc29sZS5sb2codGhpcy51c2VyKTtcclxuICAgICAgaWYgKHJlYWxtRGF0YSkge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdFJlYWxtID0gdGhpcy5fY29va2llU2VydmljZS5nZXQoXCJyZWFsbU5hbWVcIik7XHJcbiAgICAgICAgdGhpcy5yZWFsbV9kYXRhID0gcmVhbG1EYXRhO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdFJlYWxtID0gdGhpcy5kYXRhWzBdLm5hbWU7XHJcbiAgICAgICAgdGhpcy5yZWFsbV9kYXRhID0gdGhpcy5kYXRhWzBdLl9pZDtcclxuICAgICAgfVxyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJlYWxtXCIsIHRoaXMucmVhbG1fZGF0YSk7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwicmVhbG1OYW1lXCIsIHRoaXMuZGVmYXVsdFJlYWxtKTtcclxuICAgICAgY29uc29sZS5sb2coXCI+Pj4+IHJlYWxtX2RhdGEgdG9vbGJhciBuZ09uSW5pdCBcIix0aGlzLnJlYWxtX2RhdGEpO1xyXG4gICAgICB0aGlzLmdldEFsbERhdGFzb3VyY2UoKTtcclxuICAgICAgdGhpcy5nZXRBbGxOb3RpZmljYXRpb24oKTtcclxuICAgIH0pO1xyXG4gICAgLy8gdGhpcy5nZXRBbGxEYXRhc291cmNlKCk7XHJcbiAgICAvLyB0aGlzLmdldEFsbE5vdGlmaWNhdGlvbigpO1xyXG4gIH1cclxuICBnZXRJZChldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coXCJAQEBAXCIsIGV2ZW50KTtcclxuICAgIHRoaXMuYWNjb3VudFNlcnZpY2UuY2hhbmdlUmVhbG0oZXZlbnQpLnN1YnNjcmliZShcclxuICAgICAgKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiZGF0YVwiLCBkYXRhKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImFjY2Vzc190b2tlblwiKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImFjY2Vzc190b2tlblwiLCBkYXRhLmJvZHkucmVzcG9uc2UuYWNjZXNzX3Rva2VuKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcInJlYWxtXCIsIGV2ZW50Ll9pZCk7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJyZWFsbU5hbWVcIiwgZXZlbnQubmFtZSk7XHJcbiAgICAgICAgdGhpcy5fY29va2llU2VydmljZS5zZXQoJ3JlYWxtJywgZXZlbnQuX2lkKTsgLy8gbmV3IG9uZVxyXG4gICAgICAgIHRoaXMuX2Nvb2tpZVNlcnZpY2Uuc2V0KCdyZWFsbU5hbWUnLCBldmVudC5uYW1lKTsgLy8gbmV3IG9uZVxyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiZGF0YXNvdXJjZVwiKTtcclxuICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XHJcbiAgICAgIH0sXHJcbiAgICAgIGVycm9yID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcImVyclwiLCBlcnJvcik7XHJcbiAgICAgIH1cclxuICAgICk7XHJcbiAgfVxyXG5cclxuICBnZXRBbGxEYXRhc291cmNlKClcclxuICB7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgcmVhbG0gOiB0aGlzLnJlYWxtX2RhdGFcclxuICAgIH07XHJcbiAgICBsZXQgYXBpVXJsID0gXCIvZGF0YXNvdXJjZXMvYWxsXCI7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiByZWFsbSBkdGFhIFwiLHRoaXMucmVhbG1fZGF0YSk7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiBxdWVyeSBcIixxdWVyeSk7XHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IHJlcy5yZXNwb25zZS5kYXRhc291cmNlcztcclxuICAgICAgdmFyIGRhdGFTb3VyY2UgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VcIik7ICAgIC8vIGNoZWNrIG9uIExvY2FsIFN0b3JhZ2VcclxuICAgICAgdGhpcy5jb29raWVEUyA9IHRoaXMuX2Nvb2tpZVNlcnZpY2UuZ2V0KFwiZGF0YXNvdXJjZVwiKTsgICAgLy8gY2hlY2sgb24gY29va2llXHJcbiAgICAgIC8vIGlmKGRhdGFTb3VyY2UgJiYgZGF0YVNvdXJjZSAhPSAnbnVsbCcpIFxyXG4gICAgICAvLyB7XHJcbiAgICAgIC8vICAgdGhpcy5zZWxlY3RlZERhdGFzb3VyY2VJZCA9IGRhdGFTb3VyY2U7XHJcbiAgICAgIC8vICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VOYW1lXCIpO1xyXG4gICAgICAvLyAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSBkYXRhU291cmNlO1xyXG4gICAgICAvLyB9IGVsc2UgXHJcbiAgICAgIGlmICh0aGlzLmNvb2tpZURTICYmIHRoaXMuY29va2llRFMgIT0gJ251bGwnKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZERhdGFzb3VyY2VJZCA9IHRoaXMuY29va2llRFM7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUgPSB0aGlzLl9jb29raWVTZXJ2aWNlLmdldChcImRhdGFzb3VyY2VOYW1lXCIpO1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSB0aGlzLmNvb2tpZURTO1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZGF0YXNvdXJjZVwiLCB0aGlzLnNlbGVjdGVkRGF0YXNvdXJjZUlkKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImRhdGFzb3VyY2VOYW1lXCIsIHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICB2YXIgZGVmYXVsdERhdGFzb3VyY2VGaWx0ZXIgPSBbXTtcclxuICAgICAgICBkZWZhdWx0RGF0YXNvdXJjZUZpbHRlciA9IF8uZmlsdGVyKHJlcy5yZXNwb25zZS5kYXRhc291cmNlcywge2RlZmF1bHREYXRhc291cmNlOiB0cnVlfSk7XHJcbiAgICAgICAgaWYoZGVmYXVsdERhdGFzb3VyY2VGaWx0ZXIgJiYgZGVmYXVsdERhdGFzb3VyY2VGaWx0ZXIubGVuZ3RoKXtcclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhc291cmNlSWQgPSBkZWZhdWx0RGF0YXNvdXJjZUZpbHRlclswXS5faWQ7XHJcbiAgICAgICAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSA9IGRlZmF1bHREYXRhc291cmNlRmlsdGVyWzBdLm5hbWU7XHJcbiAgICAgICAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gZGVmYXVsdERhdGFzb3VyY2VGaWx0ZXJbMF0uX2lkO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlXCIsIHRoaXMuc2VsZWN0ZWREYXRhc291cmNlSWQpO1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlTmFtZVwiLCB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSk7XHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRGF0YXNvdXJjZUlkID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLl9pZDtcclxuICAgICAgICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLm5hbWU7XHJcbiAgICAgICAgICB0aGlzLmRlZmF1bHREYXRhc291cmNlID0gcmVzLnJlc3BvbnNlLmRhdGFzb3VyY2VzWzBdLl9pZDtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZGF0YXNvdXJjZVwiLCB0aGlzLnNlbGVjdGVkRGF0YXNvdXJjZUlkKTtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZGF0YXNvdXJjZU5hbWVcIiwgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIGNsZWFyYWxsTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbkxpc3QpIHtcclxuICAgIGxldCBhcGlVcmwgPSBcIi9ub3RpZmljYXRpb24vdXBkYXRlQWxsL1wiO1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICBpc3JlYWQ6IHRydWUsXHJcbiAgICAgIGZlYXR1cmU6IFwiaXNyZWFkXCIsXHJcbiAgICAgIHJlYWxtIDogdGhpcy5yZWFsbV9kYXRhXHJcbiAgICB9O1xyXG4gICAgY29uc29sZS5sb2cocXVlcnlPYmosIFwiLCwuLC4sLi4sLlwiKVxyXG4gICAgdGhpcy5jb250ZW50U2VydmljZS51cGRhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCBhcGlVcmwsIHRydWUpXHJcbiAgICAgIC5zdWJzY3JpYmUocmVzID0+IHtcclxuICAgICAgICB0aGlzLmVuYWJsZU5vdGlmaWNhdGlvbiA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBnZXRBbGxOb3RpZmljYXRpb24oKSB7XHJcbiAgICBsZXQgcXVlcnkgPSB7XHJcbiAgICAgIHVucmVhZDogdHJ1ZSxcclxuICAgICAgdG9vbGJhcjogdHJ1ZSxcclxuICAgICAgcmVhbG0gOiB0aGlzLnJlYWxtX2RhdGFcclxuICAgIH07XHJcbiAgICBsZXQgYXBpVXJsID1cIi9ub3RpZmljYXRpb25cIjtcclxuICAgIHRoaXMubm90aWZpY2F0aW9uRGF0YSA9IFtdO1xyXG4gICAgdGhpcy5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICB0aGlzLm5vdGlmaWNhdGlvbkxpc3QgPSByZXMucmVzcG9uc2Uubm90aWZpY2F0aW9uTGlzdDtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgXy5mb3JFYWNoKHRoaXMubm90aWZpY2F0aW9uTGlzdCwgZnVuY3Rpb24obGlzdEl0ZW0pIHtcclxuICAgICAgICBsZXQgbGFzdEluZGV4ID0gbGlzdEl0ZW0uZGF0YSA/IGxpc3RJdGVtLmRhdGEubGVuZ3RoIC0gMSA6IDA7XHJcbiAgICAgICAgbGV0IGl0ZW0gPSBsaXN0SXRlbS5kYXRhW2xhc3RJbmRleF07XHJcbiAgICAgICAgY29uc29sZS5sb2coaXRlbSxcIj4+Pj4+aXRlbVwiKVxyXG4gICAgICAgICAgaXRlbS5mZWF0dXJlID0gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICBcIk5PVElGSUNBVElPTl9GT1JNQVRTLkZFQVRVUkUuXCIgKyBpdGVtLm1lc3NhZ2VPYmouZmVhdHVyZSk7XHJcbiAgICAgICAgICBsZXQgb3BlcmF0aW9uU3RhdHVzID0gaXRlbS5tZXNzYWdlT2JqLmFjdGlvbiArIFwiX1wiICsgaXRlbS5tZXNzYWdlT2JqLnN0YXR1cztcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiT3BlcmF0aW9uIERhdGFcIiwgb3BlcmF0aW9uU3RhdHVzKTtcclxuICAgICAgICAgIGl0ZW0uc3RhdHVzX2FjdGlvbiA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcIk5PVElGSUNBVElPTl9GT1JNQVRTLkFDVElPTl9TVEFUVVMuXCIgKyBvcGVyYXRpb25TdGF0dXMpO1xyXG4gICAgICAgICAgaXRlbS5tc2cgPSBpdGVtLmZlYXR1cmUgKyBcIiAgIFwiICsgaXRlbS5zdGF0dXNfYWN0aW9uO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coaXRlbSxcIj4+Pj4+IGl0ZW1cIik7XHJcbiAgICAgICAgICBpdGVtLlN0YXR1c05ldyA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgXCJOT1RJRklDQVRJT05fRk9STUFUUy5TVEFUVVMuXCIgKyBpdGVtLm1lc3NhZ2VPYmouc3RhdHVzKTtcclxuICAgICAgICAgIGl0ZW0uZGF0ZVRpbWVBZ28gPSBtb21lbnQoaXRlbS5jcmVhdGVkX2F0KS5mcm9tTm93KCk7XHJcbiAgICAgICAgICBpdGVtLm5ld3N0cmluZyA9IGl0ZW0udXNlck5hbWUuY2hhckF0KDApO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCJDb252ZXJ0IGhvdXJzID4+PlwiLCBpdGVtLmRhdGVUaW1lQWdvKTtcclxuICAgICAgICAgIGl0ZW0ucGVyY2VudGFnZSA9IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcIk5PVElGSUNBVElPTl9GT1JNQVRTLlBST0dSRVNTLlwiICsgaXRlbS5tZXNzYWdlT2JqLnN0YXR1cyk7XHJcbiAgICAgICAgICBpdGVtLm9wZXJhdGlvblN0YXR1cyA9IG9wZXJhdGlvblN0YXR1cztcclxuICAgICAgICAgIGxpc3RJdGVtID0gey4uLmxpc3RJdGVtLCAuLi5pdGVtfVxyXG4gICAgICAgICAgc2VsZi5ub3RpZmljYXRpb25EYXRhLnB1c2gobGlzdEl0ZW0pO1xyXG4gICAgICAgICAgLy8gJChcIi5wcm9ncmVzcy1iYXItc3RyaXBlZFwiKS53aWR0aChpdGVtLnBlcmNlbnRhZ2UpO1xyXG5cclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgICAgIHRoaXMuY291bnQgPSByZXMucmVzcG9uc2UudG90YWw7XHJcbiAgICB9KTtcclxuICB9XHJcbiAgbmF2aWdhdGVUb0Rvd25sb2Fkcyhtc2cpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiTWVzc2FnZSB2YWx1ZSA+Pj4+Pj5cIiwgbXNnKTtcclxuICAgIC8vIHRoaXMuY2xvc2VTaWRlTmF2KCk7XHJcbiAgICAvLyB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy9wYWdlcy9kb3dubG9hZC1yZXBvcnQnXSk7XHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8tY29uZGl0aW9uYWwtYXNzaWdubWVudFxyXG4gICAgLy8gaWYgKG1zZy5tZXNzYWdlT2JqLmZlYXR1cmUgPSAnTUFOQUdFX1JVTEVTRVQnKSB7XHJcbiAgICAvLyAgIGxldCBtYWluTWVudSA9IHRoaXMuX21ldGFEYXRhLmZpbHRlcihvYmogPT4gb2JqW1wibWVudU5hbWVcIl0gPT0gXCJjb250cm9sTWFuYWdlbWVudFwiKTtcclxuICAgIC8vICAgY29uc29sZS5sb2coXCI+Pj4gTWV0YSBkYXRhIFwiLCB0aGlzLl9tZXRhRGF0YSk7XHJcbiAgICAvLyAgIGlmKG1haW5NZW51Lmxlbmd0aCA+IDApXHJcbiAgICAvLyAgICB7XHJcbiAgICAvLyAgICAgbGV0IHN1Ym1lbnVzID0gbWFpbk1lbnVbMF0uc3ViTWVudTtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhcInN1Ym1lbnUgPj4+Pj5cIiwgc3VibWVudXMpO1xyXG4gICAgICAgIFxyXG4gICAgLy8gICAgIGxldCBzdWJNZW51ID0gc3VibWVudXMuZmlsdGVyKG9iaiA9PiBvYmpbXCJtZW51TmFtZVwiXSA9PSBcInJ1bGVzZXRcIik7XHJcbiAgICAvLyAgICAgbGV0IG1lbnVJdGVtID0gc3ViTWVudVswXTtcclxuICAgIC8vICAgICBsZXQgdGVtcEl0ZW0gPSB7XHJcbiAgICAvLyAgICAgICBpZDogbWVudUl0ZW0ubWVudU5hbWUsXHJcbiAgICAvLyAgICAgICB0aXRsZTogbWVudUl0ZW0udGl0bGUsXHJcbiAgICAvLyAgICAgICB0eXBlOiBcclxuICAgIC8vICAgICAgICAgbWVudUl0ZW0uc3ViTWVudVxyXG4gICAgLy8gICAgICAgICAgID8gXCJjb2xsYXBzYWJsZVwiXHJcbiAgICAvLyAgICAgICAgICAgOiBcIml0ZW1cIixcclxuICAgIC8vICAgICAgIHVybDogbWVudUl0ZW0ucm91dGluZ1VJLFxyXG4gICAgLy8gICAgICAgY29uZmlnRmlsZVBhdGg6IG1lbnVJdGVtLmNvbmZpZ0ZpbGVQYXRoXHJcbiAgICAvLyAgICAgfTtcclxuICAgIC8vICAgICBjb25zb2xlLmxvZyhcIj4+PiBtZW51SXRlbSBcIiwgdGVtcEl0ZW0pO1xyXG4gICAgLy8gICAgIHRoaXMuY2xvc2VTaWRlTmF2KCk7XHJcbiAgICAvLyAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kUm91dGluZyh0ZW1wSXRlbSk7XHJcbiAgICAvLyAgIH1cclxuICAgIC8vIH1cclxuICAgIGlmIChtc2cuYWRkaXRpb25hbERhdGEgJiYgbXNnLmFkZGl0aW9uYWxEYXRhLmxlbmd0aCkge1xyXG4gICAgICB2YXIgZG93bmxvYWRJZCA9IG1zZy5hZGRpdGlvbmFsRGF0YVswXS5pbnN0YW5jZUlkO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdmFyIGRvd25sb2FkSWQgPSBudWxsO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coXCJSZXBvcnQgaWQgPj4+Pj5cIiwgZG93bmxvYWRJZCk7XHJcbiAgICBsZXQgcXVlcnkgPSB7XHJcbiAgICAgIHJlcG9ydElkOiBkb3dubG9hZElkXHJcbiAgICB9XHJcbiAgICBsZXQgYXBpVXJsID0gJy9yZXBvcnRzL2Rvd25sb2FkUmVwb3J0JztcclxuICAgIGlmIChkb3dubG9hZElkID09IHVuZGVmaW5lZCB8fCBkb3dubG9hZElkID09IG51bGwgfHwgZG93bmxvYWRJZCA9PSAnJykge1xyXG4gICAgICBjb25zb2xlLmxvZyhcIlJlcG9ydCBJZCBOdWxsXCIpO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIGNvbnN0IGFiID0gbmV3IEFycmF5QnVmZmVyKGRhdGEuZGF0YS5sZW5ndGgpO1xyXG4gICAgICAgIGNvbnN0IHZpZXcgPSBuZXcgVWludDhBcnJheShhYik7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBkYXRhLmRhdGEubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgIHZpZXdbaV0gPSBkYXRhLmRhdGFbaV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGxldCBkb3dubG9hZFR5cGUgPSAnYXBwbGljYXRpb24vemlwJztcclxuICAgICAgICBjb25zdCBmaWxlID0gbmV3IEJsb2IoW2FiXSwgeyB0eXBlOiBkb3dubG9hZFR5cGUgfSk7XHJcbiAgICAgICAgRmlsZVNhdmVyLnNhdmVBcyhmaWxlLCBtc2cuYWRkaXRpb25hbERhdGFbMF0ubmFtZSArICcuemlwJyk7XHJcbiAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG5cclxuXHJcblxyXG4gIGdldE1ldGFEYXRhKCkge1xyXG4gICAgY29uc29sZS5sb2codGhpcy5fbWV0YURhdGEpO1xyXG4gICAgbGV0IG5hdmlnYXRpb25MaXN0ID0gW107XHJcblxyXG4gICAgXy5mb3JFYWNoKHRoaXMuX21ldGFEYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICB2YXIgY2hpbGRyZW5MaXN0ID0gW107XHJcbiAgICAgIGlmIChpdGVtLnN1Yk1lbnUgJiYgaXRlbS5zdWJNZW51Lmxlbmd0aCkge1xyXG4gICAgICAgIF8uZm9yRWFjaChpdGVtLnN1Yk1lbnUsIGZ1bmN0aW9uIChtZW51SXRlbSkge1xyXG4gICAgICAgICAgbGV0IHRlbXBJdGVtID0ge1xyXG4gICAgICAgICAgICBpZDogbWVudUl0ZW0ubWVudU5hbWUsXHJcbiAgICAgICAgICAgIHRpdGxlOiBtZW51SXRlbS50aXRsZSxcclxuICAgICAgICAgICAgdHlwZTpcclxuICAgICAgICAgICAgICBtZW51SXRlbS5zdWJNZW51ICYmIG1lbnVJdGVtLnN1Yk1lbnUubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICA/IFwiY29sbGFwc2FibGVcIlxyXG4gICAgICAgICAgICAgICAgOiBcIml0ZW1cIixcclxuICAgICAgICAgICAgdXJsOiBtZW51SXRlbS5yb3V0aW5nVUksXHJcblxyXG4gICAgICAgICAgICBjb25maWdGaWxlUGF0aDogbWVudUl0ZW0uY29uZmlnRmlsZVBhdGhcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgICBjaGlsZHJlbkxpc3QucHVzaCh0ZW1wSXRlbSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH1cclxuXHJcbiAgICAgIGxldCB0ZW1wID0ge1xyXG4gICAgICAgIGlkOiBpdGVtLm1lbnVOYW1lLFxyXG4gICAgICAgIHRpdGxlOiBpdGVtLnRpdGxlLFxyXG4gICAgICAgIHR5cGU6IGl0ZW0uc3ViTWVudSAmJiBpdGVtLnN1Yk1lbnUubGVuZ3RoID8gXCJjb2xsYXBzYWJsZVwiIDogXCJpdGVtXCIsXHJcbiAgICAgICAgdHJhbnNsYXRlOiBpdGVtLnRyYW5zbGF0ZSxcclxuICAgICAgICBpY29uOiBpdGVtLmljb24sXHJcbiAgICAgICAgY2hpbGRyZW46IGNoaWxkcmVuTGlzdCxcclxuICAgICAgICBjb25maWdGaWxlUGF0aDogaXRlbS5jb25maWdGaWxlUGF0aFxyXG4gICAgICB9O1xyXG4gICAgICBuYXZpZ2F0aW9uTGlzdC5wdXNoKHRlbXApO1xyXG4gICAgfSk7XHJcblxyXG4gICAgcmV0dXJuIG5hdmlnYXRpb25MaXN0O1xyXG4gIH1cclxuICBzZW5kTWVzc2FnZShpZCwgdHJpZ2dlckZyb20pIHtcclxuICAgIGZvciAodmFyIGkgPSAwOyBpIDwgdGhpcy5kYXRhU291cmNlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGlmIChpZCA9PSB0aGlzLmRhdGFTb3VyY2VbaV0uX2lkKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJkYXRhc291cmNlTmFtZVwiLCB0aGlzLmRhdGFTb3VyY2VbaV0ubmFtZSk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiZGF0YXNvdXJjZVwiLCBpZCk7XHJcbiAgICBpZih0cmlnZ2VyRnJvbSl7XHJcbiAgICAgIHRoaXMudXBkYXRlRGF0YXNvdXJjZShpZCk7XHJcbiAgICB9ZWxzZXtcclxuICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTWVzc2FnZShpZCk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHVwZGF0ZURhdGFzb3VyY2UgKGlkKXtcclxuICAgIGxldCBxdWVyeU9iaiA9IHtcclxuICAgICAgICBkZWZhdWx0RGF0YXNvdXJjZSA6IHRydWVcclxuICAgICAgfTtcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgIC51cGRhdGVSZXF1ZXN0KHF1ZXJ5T2JqLCcvZGF0YXNvdXJjZXMvc2V0RGVmYXVsdC8nLCBpZClcclxuICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgIHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTWVzc2FnZShpZCk7XHJcbiAgICAgIH0sXHJcbiAgICAgIGVycm9yID0+IHtcclxuICAgICAgfVxyXG4gICAgKTtcclxuICAgIGNvbnNvbGUubG9nKCdhY3Rpb25SZWRpcmVjdCA+Pj4+Pj4+Pj4+Pj4+Pj4gcXVlcnlPYmogJywgcXVlcnlPYmopXHJcbiAgfVxyXG4gIHJlZGlyZWN0VG9Ob3RpZmljYXRpb24oKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiByZWRpcmVjdFRvIE5vdGlmaWNhdGlvbiBsaW5rXCIpO1xyXG4gICAgbGV0IG1haW5NZW51ID0gdGhpcy5fbWV0YURhdGEuZmlsdGVyKG9iaiA9PiBvYmpbXCJtZW51TmFtZVwiXSA9PSBcIm1hbmFnZUFjY291bnRcIik7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiBtYWluTWVudSBcIiwgbWFpbk1lbnUpO1xyXG4gICAgaWYobWFpbk1lbnUubGVuZ3RoPjApXHJcbiAgICAge1xyXG4gICAgICBsZXQgc3VibWVudXMgPSBtYWluTWVudVswXS5zdWJNZW51O1xyXG4gICAgICBsZXQgc3ViTWVudSA9IHN1Ym1lbnVzLmZpbHRlcihvYmogPT4gb2JqW1wibWVudUlkXCJdID09IFwibm90aWZpY2F0aW9uXCIpO1xyXG4gICAgICBsZXQgbWVudUl0ZW0gPSBzdWJNZW51WzBdO1xyXG4gICAgICBsZXQgdGVtcEl0ZW0gPSB7XHJcbiAgICAgICAgaWQ6IG1lbnVJdGVtLm1lbnVOYW1lLFxyXG4gICAgICAgIHRpdGxlOiBtZW51SXRlbS50aXRsZSxcclxuICAgICAgICB0eXBlOlxyXG4gICAgICAgICAgbWVudUl0ZW0uc3ViTWVudSAmJiBtZW51SXRlbS5zdWJNZW51Lmxlbmd0aFxyXG4gICAgICAgICAgICA/IFwiY29sbGFwc2FibGVcIlxyXG4gICAgICAgICAgICA6IFwiaXRlbVwiLFxyXG4gICAgICAgIHVybDogbWVudUl0ZW0ucm91dGluZ1VJLFxyXG4gICAgICAgIGNvbmZpZ0ZpbGVQYXRoOiBtZW51SXRlbS5jb25maWdGaWxlUGF0aFxyXG4gICAgICB9O1xyXG4gICAgICBjb25zb2xlLmxvZyhcIj4+PiBtZW51SXRlbSBcIiwgdGVtcEl0ZW0pO1xyXG4gICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRSb3V0aW5nKHRlbXBJdGVtKTtcclxuICAgIH1cclxuICAgIHRoaXMuZW5hYmxlTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgfVxyXG4gIG91c2lkZUNsaWNrKGUpIHtcclxuICAgIHRoaXMuZW5hYmxlTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgfVxyXG4gIFJlYWQoaW5wdXQpIHtcclxuICAgIGxldCBhcGlVcmwgPSBcIi9ub3RpZmljYXRpb24vdXBkYXRlXCI7XHJcbiAgICBsZXQgcXVlcnlPYmogPSB7XHJcbiAgICAgIGlzcmVhZDogdHJ1ZSxcclxuICAgICAgZmVhdHVyZTogXCJpc3JlYWRcIixcclxuICAgICAgaWRMaXN0OiBpbnB1dC5faWRcclxuICAgIH07XHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVJlcXVlc3QocXVlcnlPYmosIGFwaVVybClcclxuICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgIHRoaXMuZ2V0QWxsTm90aWZpY2F0aW9uKCk7XHJcbiAgICB9KTtcclxuICAgIC8vIHRoaXMubmdPbkluaXQoKTtcclxuXHJcbiAgfVxyXG5cclxuXHJcbiAgUmVtb3ZlKGlucHV0KSB7XHJcbiAgICBsZXQgYXBpVXJsID0gXCIvbm90aWZpY2F0aW9uL3VwZGF0ZVwiO1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge1xyXG4gICAgICBmZWF0dXJlOiBcImRlbGV0ZVwiLFxyXG4gICAgICBpZExpc3Q6IGlucHV0Ll9pZFxyXG4gICAgfTtcclxuICAgIHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChxdWVyeU9iaiwgYXBpVXJsKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlcyA9PiB7XHJcbiAgICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG5cclxuXHJcblxyXG4gIC8qKlxyXG4gICAqIE9uIGRlc3Ryb3lcclxuICAgKi9cclxuICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgIC8vIFVuc3Vic2NyaWJlIGZyb20gYWxsIHN1YnNjcmlwdGlvbnNcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLm5leHQoKTtcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsLmNvbXBsZXRlKCk7XHJcbiAgfVxyXG5cclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG4gIC8vIEAgUHVibGljIG1ldGhvZHNcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBUb2dnbGUgc2lkZWJhciBvcGVuXHJcbiAgICpcclxuICAgKiBAcGFyYW0ga2V5XHJcbiAgICovXHJcbiAgdG9nZ2xlU2lkZWJhck9wZW4oa2V5KTogdm9pZCB7XHJcbiAgICB0aGlzLl9mdXNlU2lkZWJhclNlcnZpY2UuZ2V0U2lkZWJhcihrZXkpLnRvZ2dsZU9wZW4oKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFNlYXJjaFxyXG4gICAqXHJcbiAgICogQHBhcmFtIHZhbHVlXHJcbiAgICovXHJcbiAgc2VhcmNoKHZhbHVlKTogdm9pZCB7XHJcbiAgICAvLyBEbyB5b3VyIHNlYXJjaCBoZXJlLi4uXHJcbiAgICBjb25zb2xlLmxvZyh2YWx1ZSk7XHJcbiAgfVxyXG5cclxuICAvKipcclxuICAgKiBTZXQgdGhlIGxhbmd1YWdlXHJcbiAgICpcclxuICAgKiBAcGFyYW0gbGFuZ1xyXG4gICAqL1xyXG4gIHNldExhbmd1YWdlKGxhbmcpOiB2b2lkIHtcclxuICAgIC8vIFNldCB0aGUgc2VsZWN0ZWQgbGFuZ3VhZ2UgZm9yIHRoZSB0b29sYmFyXHJcbiAgICB0aGlzLnNlbGVjdGVkTGFuZ3VhZ2UgPSBsYW5nO1xyXG5cclxuICAgIC8vIFVzZSB0aGUgc2VsZWN0ZWQgbGFuZ3VhZ2UgZm9yIHRyYW5zbGF0aW9uc1xyXG4gICAgdGhpcy5fdHJhbnNsYXRlU2VydmljZS51c2UobGFuZy5pZCk7XHJcbiAgfVxyXG4gIGxvZ291dCgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiTG9nb3V0Li4uXCIpO1xyXG4gICAgdmFyIGRhdGFTb3VyY2UgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VcIik7XHJcbiAgICB2YXIgZGF0YXNvdXJjZU5hbWUgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VOYW1lXCIpO1xyXG4gICAgdGhpcy5fY29va2llU2VydmljZS5zZXQoJ2RhdGFzb3VyY2UnLCBkYXRhU291cmNlKTtcclxuICAgIHRoaXMuX2Nvb2tpZVNlcnZpY2Uuc2V0KCdkYXRhc291cmNlTmFtZScsIGRhdGFzb3VyY2VOYW1lKTtcclxuICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgc2Vzc2lvblN0b3JhZ2UuY2xlYXIoKTtcclxuICAgIHRoaXMub2F1dGhTZXJ2aWNlLmxvZ091dChmYWxzZSk7XHJcbiAgfVxyXG4gIGNoYW5nZVB3ZCgpIHtcclxuICAgIC8vIHdpbmRvdy5sb2NhdGlvbi5ocmVmID0gJ2h0dHA6Ly8xOTIuMTY4LjIuMTkyOjkwMTQvY2hhbmdlLXBhc3N3b3JkJztcclxuICAgIGNvbnNvbGUubG9nKFwidXNlcjo6OlwiICsgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJhY2Nlc3NfdG9rZW5cIikpO1xyXG4gICAgdmFyIGFjY2Vzc190b2tlbiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiYWNjZXNzX3Rva2VuXCIpO1xyXG4gICAgdGhpcy5hY2NvdW50U2VydmljZS5jaGFuZ2VQYXNzd29yZChhY2Nlc3NfdG9rZW4pO1xyXG4gIH1cclxuICAvLyBzaWRlTm9kaWZpXHJcbiAgb3BlblNpZGVOb2RpZmlOYXYoZSkge1xyXG4gICAgY29uc29sZS5sb2coZSwgXCJjaGVja1wiKVxyXG4gICAgdGhpcy5lbmFibGVOb3RpZmljYXRpb24gPSAhdGhpcy5lbmFibGVOb3RpZmljYXRpb247XHJcbiAgfVxyXG4gIGNsb3NlU2lkZU5hdigpIHtcclxuICAgIHRoaXMuZW5hYmxlTm90aWZpY2F0aW9uID0gZmFsc2U7XHJcbiAgfVxyXG4gIGNsb3NlKCkge1xyXG4gICAgdGhpcy5lbmFibGVOb3RpZmljYXRpb24gPSBmYWxzZTtcclxuICB9XHJcbn1cclxuXHJcblxyXG4iXX0=