import { NgModule } from "@angular/core";
import { FuseSharedModule } from "../@fuse/shared.module";
import { NavbarComponent } from "../navbar/navbar.component";
import { NavbarVerticalStyle1Module } from "./vertical/style-1/style-1.module";
export class NavbarModule {
}
NavbarModule.decorators = [
    { type: NgModule, args: [{
                declarations: [NavbarComponent],
                imports: [FuseSharedModule, NavbarVerticalStyle1Module],
                exports: [NavbarComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJuYXZiYXIvbmF2YmFyLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXpDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRTFELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQU8vRSxNQUFNLE9BQU8sWUFBWTs7O1lBTHhCLFFBQVEsU0FBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxlQUFlLENBQUM7Z0JBQy9CLE9BQU8sRUFBRSxDQUFDLGdCQUFnQixFQUFFLDBCQUEwQixDQUFDO2dCQUN2RCxPQUFPLEVBQUUsQ0FBQyxlQUFlLENBQUM7YUFDM0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IE5hdmJhckNvbXBvbmVudCB9IGZyb20gXCIuLi9uYXZiYXIvbmF2YmFyLmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBOYXZiYXJWZXJ0aWNhbFN0eWxlMU1vZHVsZSB9IGZyb20gXCIuL3ZlcnRpY2FsL3N0eWxlLTEvc3R5bGUtMS5tb2R1bGVcIjtcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbTmF2YmFyQ29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbRnVzZVNoYXJlZE1vZHVsZSwgTmF2YmFyVmVydGljYWxTdHlsZTFNb2R1bGVdLFxyXG4gIGV4cG9ydHM6IFtOYXZiYXJDb21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOYXZiYXJNb2R1bGUge31cclxuIl19