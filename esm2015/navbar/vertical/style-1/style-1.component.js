import { Component, ViewChild, ViewEncapsulation, Inject } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { Subject } from "rxjs";
import { delay, filter, take, takeUntil } from "rxjs/operators";
import { FuseConfigService } from "../../../@fuse/services/config.service";
import { FuseNavigationService } from "../../../@fuse/components/navigation/navigation.service";
import { FusePerfectScrollbarDirective } from "../../../@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive";
import { FuseSidebarService } from "../../../@fuse/components/sidebar/sidebar.service";
export class NavbarVerticalStyle1Component {
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     */
    constructor(_fuseConfigService, _fuseNavigationService, _fuseSidebarService, _router, english) {
        this._fuseConfigService = _fuseConfigService;
        this._fuseNavigationService = _fuseNavigationService;
        this._fuseSidebarService = _fuseSidebarService;
        this._router = _router;
        this.english = english;
        this.toggleOpen = true;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        // console.log(english, "::::::::::english>>>>>>>");
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    // Directive
    set directive(theDirective) {
        if (!theDirective) {
            return;
        }
        this._fusePerfectScrollbar = theDirective;
        // Update the scrollbar on collapsable item toggle
        this._fuseNavigationService.onItemCollapseToggled
            .pipe(delay(500), takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            this._fusePerfectScrollbar.update();
        });
        // Scroll to the active item position
        this._router.events
            .pipe(filter(event => event instanceof NavigationEnd), take(1))
            .subscribe(() => {
            setTimeout(() => {
                const activeNavItem = document.querySelector("navbar .nav-link.active");
                if (activeNavItem) {
                    const activeItemOffsetTop = activeNavItem.offsetTop, activeItemOffsetParentTop = activeNavItem.offsetParent.offsetTop, scrollDistance = activeItemOffsetTop - activeItemOffsetParentTop - 48 * 3 - 168;
                    this._fusePerfectScrollbar.scrollToTop(scrollDistance);
                }
            });
        });
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    /**
     * On init
     */
    ngOnInit() {
        this._router.events
            .pipe(filter(event => event instanceof NavigationEnd), takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            if (this._fuseSidebarService.getSidebar("navbar")) {
                this._fuseSidebarService.getSidebar("navbar").close();
            }
        });
        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(config => {
            this.fuseConfig = config;
        });
        // Get current navigation
        this._fuseNavigationService.onNavigationChanged
            .pipe(filter(value => value !== null), takeUntil(this._unsubscribeAll))
            .subscribe(() => {
            this.navigation = this._fuseNavigationService.getCurrentNavigation();
        });
    }
    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Toggle sidebar opened status
     */
    toggleSidebarOpened() {
        this.toggleOpen = true;
        this._fuseSidebarService.getSidebar("navbar").toggleOpen();
    }
    /**
     * Toggle sidebar folded status
     */
    toggleSidebarFolded() {
        this.toggleOpen = false;
        this._fuseSidebarService.getSidebar("navbar").toggleFold();
    }
}
NavbarVerticalStyle1Component.decorators = [
    { type: Component, args: [{
                selector: "navbar-vertical-style-1",
                template: "<div class=\"navbar-top\" [ngClass]=\"fuseConfig.layout.navbar.secondaryBackground\">\r\n\r\n  <div class=\"logo\">\r\n    <img *ngIf=\"!toggleOpen\" class=\"logo-icon\" src=\"assets/images/logos/trooper.png\">\r\n    <img *ngIf=\"toggleOpen\" src=\"assets/images/logos/trooper.png\">\r\n  </div>\r\n\r\n  <div class=\"buttons\">\r\n\r\n    <button mat-icon-button class=\"toggle-sidebar-folded\" (click)=\"toggleSidebarFolded()\" fxHide.lt-lg>\r\n      <mat-icon class=\"secondary-text\" style=\"color:#8c51ff !important\">menu</mat-icon>\r\n    </button>\r\n\r\n    <button mat-icon-button class=\"toggle-sidebar-opened\" (click)=\"toggleSidebarOpened()\" fxHide.gt-md>\r\n      <mat-icon class=\"secondary-text\" style=\"color:#8c51ff !important\">arrow_back</mat-icon>\r\n    </button>\r\n\r\n  </div>\r\n\r\n</div>\r\n\r\n\r\n<div class=\"navbar-scroll-container\" [ngClass]=\"fuseConfig.layout.navbar.primaryBackground\" fusePerfectScrollbar\r\n  [fusePerfectScrollbarOptions]=\"{suppressScrollX: true}\">\r\n\r\n  <div class=\"navbar-content\">\r\n    <fuse-navigation class=\"material2\" layout=\"vertical\"></fuse-navigation>\r\n  </div>\r\n\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: ["fuse-sidebar.navbar-fuse-sidebar{overflow:hidden}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top{padding:12px 0;-webkit-box-pack:center;justify-content:center;background-color:#fff!important}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top .buttons{display:none}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top .logo .logo-icon{width:100%}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-top .logo .logo-text{display:none}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user{padding:12px 0;height:64px;min-height:64px;max-height:64px}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container{position:relative;top:auto;padding:0;-webkit-transform:translateX(0);transform:translateX(0);left:auto}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container .avatar{width:40px;height:40px}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .email,fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .user .username{display:none}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .navbar-content{padding-top:0}fuse-sidebar.navbar-fuse-sidebar.folded:not(.unfolded) navbar navbar-vertical-style-1 .navbar-scroll-container .navbar-content .material2 .nav-item .nav-link{border-radius:20px;margin:0 12px;padding:0 12px}navbar.vertical-style-1{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;width:100%;height:100%}navbar.vertical-style-1.right-navbar .toggle-sidebar-opened mat-icon{-webkit-transform:rotate(180deg);transform:rotate(180deg)}navbar navbar-vertical-style-1{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;width:100%;height:100%}navbar navbar-vertical-style-1 .navbar-top{display:-webkit-box;display:flex;-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;-webkit-box-flex:1;flex:1 0 auto;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between;min-height:64px;max-height:64px;height:64px;padding:12px 12px 12px 20px;background-color:#fff!important;border-bottom:.5px solid grey}@media screen and (max-width:599px){navbar navbar-vertical-style-1 .navbar-top{min-height:56px;max-height:56px;height:56px}}navbar navbar-vertical-style-1 .navbar-top .logo{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}navbar navbar-vertical-style-1 .navbar-top .logo .logo-icon{width:100%}navbar navbar-vertical-style-1 .navbar-top .logo .logo-text{margin-left:12px;font-size:16px;font-weight:300;letter-spacing:.4px;line-height:normal}navbar navbar-vertical-style-1 .navbar-top .buttons{display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center}navbar navbar-vertical-style-1 .navbar-scroll-container{display:-webkit-box;display:flex;-webkit-box-orient:vertical;-webkit-box-direction:normal;flex-direction:column;-webkit-box-flex:1;flex:1 1 auto;overflow-y:auto;-webkit-overflow-scrolling:touch;background:url(assets/images/logos/1.png)!important}navbar navbar-vertical-style-1 .navbar-scroll-container .user{position:relative;display:-webkit-box;display:flex;-webkit-box-align:center;align-items:center;-webkit-box-pack:start;justify-content:flex-start;width:100%;height:136px;min-height:136px;max-height:136px;padding:24px 0 64px}navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container{position:absolute;top:92px;border-radius:50%;padding:8px;-webkit-transform:translateX(-50%);transform:translateX(-50%);left:50%}navbar navbar-vertical-style-1 .navbar-scroll-container .user .avatar-container .avatar{width:72px;height:72px;margin:0}navbar navbar-vertical-style-1 .navbar-scroll-container .navbar-content{-webkit-box-flex:1;flex:1 1 auto;padding-top:32px}"]
            }] }
];
/** @nocollapse */
NavbarVerticalStyle1Component.ctorParameters = () => [
    { type: FuseConfigService },
    { type: FuseNavigationService },
    { type: FuseSidebarService },
    { type: Router },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
NavbarVerticalStyle1Component.propDecorators = {
    directive: [{ type: ViewChild, args: [FusePerfectScrollbarDirective,] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3R5bGUtMS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsibmF2YmFyL3ZlcnRpY2FsL3N0eWxlLTEvc3R5bGUtMS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUNMLFNBQVMsRUFHVCxTQUFTLEVBQ1QsaUJBQWlCLEVBQ2pCLE1BQU0sRUFDUCxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWhFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlEQUF5RCxDQUFDO0FBQ2hHLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG1GQUFtRixDQUFDO0FBQ2xJLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1EQUFtRCxDQUFDO0FBUXZGLE1BQU0sT0FBTyw2QkFBNkI7SUFTeEM7Ozs7Ozs7T0FPRztJQUNILFlBQ1Usa0JBQXFDLEVBQ3JDLHNCQUE2QyxFQUM3QyxtQkFBdUMsRUFDdkMsT0FBZSxFQUNHLE9BQU87UUFKekIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFtQjtRQUNyQywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXVCO1FBQzdDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBb0I7UUFDdkMsWUFBTyxHQUFQLE9BQU8sQ0FBUTtRQUNHLFlBQU8sR0FBUCxPQUFPLENBQUE7UUFuQm5DLGVBQVUsR0FBWSxJQUFJLENBQUM7UUFxQnpCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDckMsb0RBQW9EO0lBQ3RELENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsY0FBYztJQUNkLHdHQUF3RztJQUV4RyxZQUFZO0lBQ1osSUFDSSxTQUFTLENBQUMsWUFBMkM7UUFDdkQsSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNqQixPQUFPO1NBQ1I7UUFFRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDO1FBRTFDLGtEQUFrRDtRQUNsRCxJQUFJLENBQUMsc0JBQXNCLENBQUMscUJBQXFCO2FBQzlDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQzthQUNqRCxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO1FBRUwscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTthQUNoQixJQUFJLENBQ0gsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsS0FBSyxZQUFZLGFBQWEsQ0FBQyxFQUMvQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQ1I7YUFDQSxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2QsVUFBVSxDQUFDLEdBQUcsRUFBRTtnQkFDZCxNQUFNLGFBQWEsR0FBUSxRQUFRLENBQUMsYUFBYSxDQUMvQyx5QkFBeUIsQ0FDMUIsQ0FBQztnQkFFRixJQUFJLGFBQWEsRUFBRTtvQkFDakIsTUFBTSxtQkFBbUIsR0FBRyxhQUFhLENBQUMsU0FBUyxFQUNqRCx5QkFBeUIsR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFDaEUsY0FBYyxHQUNaLG1CQUFtQixHQUFHLHlCQUF5QixHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO29CQUVuRSxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFDO2lCQUN4RDtZQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG9CQUFvQjtJQUNwQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxRQUFRO1FBQ04sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNO2FBQ2hCLElBQUksQ0FDSCxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLFlBQVksYUFBYSxDQUFDLEVBQy9DLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQ2hDO2FBQ0EsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtnQkFDakQsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN2RDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBRUwsa0NBQWtDO1FBQ2xDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNO2FBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2FBQ3JDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztRQUMzQixDQUFDLENBQUMsQ0FBQztRQUVMLHlCQUF5QjtRQUN6QixJQUFJLENBQUMsc0JBQXNCLENBQUMsbUJBQW1CO2FBQzVDLElBQUksQ0FDSCxNQUFNLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLEVBQy9CLFNBQVMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQ2hDO2FBQ0EsU0FBUyxDQUFDLEdBQUcsRUFBRTtZQUNkLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLG9CQUFvQixFQUFFLENBQUM7UUFDdkUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxXQUFXO1FBQ1QscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDNUIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsQyxDQUFDO0lBRUQsd0dBQXdHO0lBQ3hHLG1CQUFtQjtJQUNuQix3R0FBd0c7SUFFeEc7O09BRUc7SUFDSCxtQkFBbUI7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDdkIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUM3RCxDQUFDO0lBRUQ7O09BRUc7SUFDSCxtQkFBbUI7UUFDakIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUM3RCxDQUFDOzs7WUEvSUYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSx5QkFBeUI7Z0JBQ25DLHdwQ0FBdUM7Z0JBRXZDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN0Qzs7OztZQVZRLGlCQUFpQjtZQUNqQixxQkFBcUI7WUFFckIsa0JBQWtCO1lBUEgsTUFBTTs0Q0FxQ3pCLE1BQU0sU0FBQyxTQUFTOzs7d0JBWWxCLFNBQVMsU0FBQyw2QkFBNkIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBPbkRlc3Ryb3ksXHJcbiAgT25Jbml0LFxyXG4gIFZpZXdDaGlsZCxcclxuICBWaWV3RW5jYXBzdWxhdGlvbixcclxuICBJbmplY3RcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBOYXZpZ2F0aW9uRW5kLCBSb3V0ZXIgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqc1wiO1xyXG5pbXBvcnQgeyBkZWxheSwgZmlsdGVyLCB0YWtlLCB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VDb25maWdTZXJ2aWNlIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL3NlcnZpY2VzL2NvbmZpZy5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEZ1c2VOYXZpZ2F0aW9uU2VydmljZSB9IGZyb20gXCIuLi8uLi8uLi9AZnVzZS9jb21wb25lbnRzL25hdmlnYXRpb24vbmF2aWdhdGlvbi5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IEZ1c2VQZXJmZWN0U2Nyb2xsYmFyRGlyZWN0aXZlIH0gZnJvbSBcIi4uLy4uLy4uL0BmdXNlL2RpcmVjdGl2ZXMvZnVzZS1wZXJmZWN0LXNjcm9sbGJhci9mdXNlLXBlcmZlY3Qtc2Nyb2xsYmFyLmRpcmVjdGl2ZVwiO1xyXG5pbXBvcnQgeyBGdXNlU2lkZWJhclNlcnZpY2UgfSBmcm9tIFwiLi4vLi4vLi4vQGZ1c2UvY29tcG9uZW50cy9zaWRlYmFyL3NpZGViYXIuc2VydmljZVwiO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgc2VsZWN0b3I6IFwibmF2YmFyLXZlcnRpY2FsLXN0eWxlLTFcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL3N0eWxlLTEuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vc3R5bGUtMS5jb21wb25lbnQuc2Nzc1wiXSxcclxuICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOYXZiYXJWZXJ0aWNhbFN0eWxlMUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICBmdXNlQ29uZmlnOiBhbnk7XHJcbiAgbmF2aWdhdGlvbjogYW55O1xyXG4gIHRvZ2dsZU9wZW46IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAvLyBQcml2YXRlXHJcbiAgcHJpdmF0ZSBfZnVzZVBlcmZlY3RTY3JvbGxiYXI6IEZ1c2VQZXJmZWN0U2Nyb2xsYmFyRGlyZWN0aXZlO1xyXG4gIHByaXZhdGUgX3Vuc3Vic2NyaWJlQWxsOiBTdWJqZWN0PGFueT47XHJcblxyXG4gIC8qKlxyXG4gICAqIENvbnN0cnVjdG9yXHJcbiAgICpcclxuICAgKiBAcGFyYW0ge0Z1c2VDb25maWdTZXJ2aWNlfSBfZnVzZUNvbmZpZ1NlcnZpY2VcclxuICAgKiBAcGFyYW0ge0Z1c2VOYXZpZ2F0aW9uU2VydmljZX0gX2Z1c2VOYXZpZ2F0aW9uU2VydmljZVxyXG4gICAqIEBwYXJhbSB7RnVzZVNpZGViYXJTZXJ2aWNlfSBfZnVzZVNpZGViYXJTZXJ2aWNlXHJcbiAgICogQHBhcmFtIHtSb3V0ZXJ9IF9yb3V0ZXJcclxuICAgKi9cclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VDb25maWdTZXJ2aWNlOiBGdXNlQ29uZmlnU2VydmljZSxcclxuICAgIHByaXZhdGUgX2Z1c2VOYXZpZ2F0aW9uU2VydmljZTogRnVzZU5hdmlnYXRpb25TZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfZnVzZVNpZGViYXJTZXJ2aWNlOiBGdXNlU2lkZWJhclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIF9yb3V0ZXI6IFJvdXRlcixcclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHB1YmxpYyBlbmdsaXNoXHJcbiAgKSB7XHJcbiAgICAvLyBTZXQgdGhlIHByaXZhdGUgZGVmYXVsdHNcclxuICAgIHRoaXMuX3Vuc3Vic2NyaWJlQWxsID0gbmV3IFN1YmplY3QoKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKGVuZ2xpc2gsIFwiOjo6Ojo6Ojo6OmVuZ2xpc2g+Pj4+Pj4+XCIpO1xyXG4gIH1cclxuXHJcbiAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAvLyBAIEFjY2Vzc29yc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8vIERpcmVjdGl2ZVxyXG4gIEBWaWV3Q2hpbGQoRnVzZVBlcmZlY3RTY3JvbGxiYXJEaXJlY3RpdmUpXHJcbiAgc2V0IGRpcmVjdGl2ZSh0aGVEaXJlY3RpdmU6IEZ1c2VQZXJmZWN0U2Nyb2xsYmFyRGlyZWN0aXZlKSB7XHJcbiAgICBpZiAoIXRoZURpcmVjdGl2ZSkge1xyXG4gICAgICByZXR1cm47XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5fZnVzZVBlcmZlY3RTY3JvbGxiYXIgPSB0aGVEaXJlY3RpdmU7XHJcblxyXG4gICAgLy8gVXBkYXRlIHRoZSBzY3JvbGxiYXIgb24gY29sbGFwc2FibGUgaXRlbSB0b2dnbGVcclxuICAgIHRoaXMuX2Z1c2VOYXZpZ2F0aW9uU2VydmljZS5vbkl0ZW1Db2xsYXBzZVRvZ2dsZWRcclxuICAgICAgLnBpcGUoZGVsYXkoNTAwKSwgdGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5fZnVzZVBlcmZlY3RTY3JvbGxiYXIudXBkYXRlKCk7XHJcbiAgICAgIH0pO1xyXG5cclxuICAgIC8vIFNjcm9sbCB0byB0aGUgYWN0aXZlIGl0ZW0gcG9zaXRpb25cclxuICAgIHRoaXMuX3JvdXRlci5ldmVudHNcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKGV2ZW50ID0+IGV2ZW50IGluc3RhbmNlb2YgTmF2aWdhdGlvbkVuZCksXHJcbiAgICAgICAgdGFrZSgxKVxyXG4gICAgICApXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgY29uc3QgYWN0aXZlTmF2SXRlbTogYW55ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcclxuICAgICAgICAgICAgXCJuYXZiYXIgLm5hdi1saW5rLmFjdGl2ZVwiXHJcbiAgICAgICAgICApO1xyXG5cclxuICAgICAgICAgIGlmIChhY3RpdmVOYXZJdGVtKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjdGl2ZUl0ZW1PZmZzZXRUb3AgPSBhY3RpdmVOYXZJdGVtLm9mZnNldFRvcCxcclxuICAgICAgICAgICAgICBhY3RpdmVJdGVtT2Zmc2V0UGFyZW50VG9wID0gYWN0aXZlTmF2SXRlbS5vZmZzZXRQYXJlbnQub2Zmc2V0VG9wLFxyXG4gICAgICAgICAgICAgIHNjcm9sbERpc3RhbmNlID1cclxuICAgICAgICAgICAgICAgIGFjdGl2ZUl0ZW1PZmZzZXRUb3AgLSBhY3RpdmVJdGVtT2Zmc2V0UGFyZW50VG9wIC0gNDggKiAzIC0gMTY4O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5fZnVzZVBlcmZlY3RTY3JvbGxiYXIuc2Nyb2xsVG9Ub3Aoc2Nyb2xsRGlzdGFuY2UpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBMaWZlY3ljbGUgaG9va3NcclxuICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAvKipcclxuICAgKiBPbiBpbml0XHJcbiAgICovXHJcbiAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICB0aGlzLl9yb3V0ZXIuZXZlbnRzXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIGZpbHRlcihldmVudCA9PiBldmVudCBpbnN0YW5jZW9mIE5hdmlnYXRpb25FbmQpLFxyXG4gICAgICAgIHRha2VVbnRpbCh0aGlzLl91bnN1YnNjcmliZUFsbClcclxuICAgICAgKVxyXG4gICAgICAuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICBpZiAodGhpcy5fZnVzZVNpZGViYXJTZXJ2aWNlLmdldFNpZGViYXIoXCJuYXZiYXJcIikpIHtcclxuICAgICAgICAgIHRoaXMuX2Z1c2VTaWRlYmFyU2VydmljZS5nZXRTaWRlYmFyKFwibmF2YmFyXCIpLmNsb3NlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAvLyBTdWJzY3JpYmUgdG8gdGhlIGNvbmZpZyBjaGFuZ2VzXHJcbiAgICB0aGlzLl9mdXNlQ29uZmlnU2VydmljZS5jb25maWdcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMuX3Vuc3Vic2NyaWJlQWxsKSlcclxuICAgICAgLnN1YnNjcmliZShjb25maWcgPT4ge1xyXG4gICAgICAgIHRoaXMuZnVzZUNvbmZpZyA9IGNvbmZpZztcclxuICAgICAgfSk7XHJcblxyXG4gICAgLy8gR2V0IGN1cnJlbnQgbmF2aWdhdGlvblxyXG4gICAgdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLm9uTmF2aWdhdGlvbkNoYW5nZWRcclxuICAgICAgLnBpcGUoXHJcbiAgICAgICAgZmlsdGVyKHZhbHVlID0+IHZhbHVlICE9PSBudWxsKSxcclxuICAgICAgICB0YWtlVW50aWwodGhpcy5fdW5zdWJzY3JpYmVBbGwpXHJcbiAgICAgIClcclxuICAgICAgLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5uYXZpZ2F0aW9uID0gdGhpcy5fZnVzZU5hdmlnYXRpb25TZXJ2aWNlLmdldEN1cnJlbnROYXZpZ2F0aW9uKCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgLyoqXHJcbiAgICogT24gZGVzdHJveVxyXG4gICAqL1xyXG4gIG5nT25EZXN0cm95KCk6IHZvaWQge1xyXG4gICAgLy8gVW5zdWJzY3JpYmUgZnJvbSBhbGwgc3Vic2NyaXB0aW9uc1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwubmV4dCgpO1xyXG4gICAgdGhpcy5fdW5zdWJzY3JpYmVBbGwuY29tcGxldGUoKTtcclxuICB9XHJcblxyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcbiAgLy8gQCBQdWJsaWMgbWV0aG9kc1xyXG4gIC8vIC0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tXHJcblxyXG4gIC8qKlxyXG4gICAqIFRvZ2dsZSBzaWRlYmFyIG9wZW5lZCBzdGF0dXNcclxuICAgKi9cclxuICB0b2dnbGVTaWRlYmFyT3BlbmVkKCk6IHZvaWQge1xyXG4gICAgdGhpcy50b2dnbGVPcGVuID0gdHJ1ZTtcclxuICAgIHRoaXMuX2Z1c2VTaWRlYmFyU2VydmljZS5nZXRTaWRlYmFyKFwibmF2YmFyXCIpLnRvZ2dsZU9wZW4oKTtcclxuICB9XHJcblxyXG4gIC8qKlxyXG4gICAqIFRvZ2dsZSBzaWRlYmFyIGZvbGRlZCBzdGF0dXNcclxuICAgKi9cclxuICB0b2dnbGVTaWRlYmFyRm9sZGVkKCk6IHZvaWQge1xyXG4gICAgdGhpcy50b2dnbGVPcGVuID0gZmFsc2U7XHJcbiAgICB0aGlzLl9mdXNlU2lkZWJhclNlcnZpY2UuZ2V0U2lkZWJhcihcIm5hdmJhclwiKS50b2dnbGVGb2xkKCk7XHJcbiAgfVxyXG59XHJcbiJdfQ==