import { Component, ElementRef, Input, Renderer2, ViewEncapsulation } from '@angular/core';
export class NavbarComponent {
    /**
     * Constructor
     *
     * @param {ElementRef} _elementRef
     * @param {Renderer2} _renderer
     */
    constructor(_elementRef, _renderer) {
        this._elementRef = _elementRef;
        this._renderer = _renderer;
        // Set the private defaults
        this._variant = 'vertical-style-1';
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------
    /**
     * Variant
     */
    get variant() {
        return this._variant;
    }
    set variant(value) {
        // Remove the old class name
        this._renderer.removeClass(this._elementRef.nativeElement, this.variant);
        // Store the variant value
        this._variant = value;
        // Add the new class name
        this._renderer.addClass(this._elementRef.nativeElement, value);
    }
}
NavbarComponent.decorators = [
    { type: Component, args: [{
                selector: 'navbar',
                template: "<ng-container *ngIf=\"variant === 'vertical-style-1'\">\r\n  <navbar-vertical-style-1></navbar-vertical-style-1>\r\n</ng-container>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
NavbarComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
NavbarComponent.propDecorators = {
    variant: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmF2YmFyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJuYXZiYXIvbmF2YmFyLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBUTNGLE1BQU0sT0FBTyxlQUFlO0lBS3hCOzs7OztPQUtHO0lBQ0gsWUFDWSxXQUF1QixFQUN2QixTQUFvQjtRQURwQixnQkFBVyxHQUFYLFdBQVcsQ0FBWTtRQUN2QixjQUFTLEdBQVQsU0FBUyxDQUFXO1FBRzVCLDJCQUEyQjtRQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLGtCQUFrQixDQUFDO0lBQ3ZDLENBQUM7SUFFRCx3R0FBd0c7SUFDeEcsY0FBYztJQUNkLHdHQUF3RztJQUV4Rzs7T0FFRztJQUNILElBQUksT0FBTztRQUVQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDO0lBRUQsSUFDSSxPQUFPLENBQUMsS0FBYTtRQUVyQiw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRXpFLDBCQUEwQjtRQUMxQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUV0Qix5QkFBeUI7UUFDekIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDbkUsQ0FBQzs7O1lBakRKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQU8sUUFBUTtnQkFDdkIsbUpBQXdDO2dCQUV4QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFQbUIsVUFBVTtZQUFTLFNBQVM7OztzQkF3QzNDLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIElucHV0LCBSZW5kZXJlcjIsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yICAgICA6ICduYXZiYXInLFxyXG4gICAgdGVtcGxhdGVVcmwgIDogJy4vbmF2YmFyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJscyAgICA6IFsnLi9uYXZiYXIuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIE5hdmJhckNvbXBvbmVudFxyXG57XHJcbiAgICAvLyBQcml2YXRlXHJcbiAgICBfdmFyaWFudDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0ge0VsZW1lbnRSZWZ9IF9lbGVtZW50UmVmXHJcbiAgICAgKiBAcGFyYW0ge1JlbmRlcmVyMn0gX3JlbmRlcmVyXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX2VsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgcHJpdmF0ZSBfcmVuZGVyZXI6IFJlbmRlcmVyMlxyXG4gICAgKVxyXG4gICAge1xyXG4gICAgICAgIC8vIFNldCB0aGUgcHJpdmF0ZSBkZWZhdWx0c1xyXG4gICAgICAgIHRoaXMuX3ZhcmlhbnQgPSAndmVydGljYWwtc3R5bGUtMSc7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cclxuICAgIC8vIEAgQWNjZXNzb3JzXHJcbiAgICAvLyAtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVmFyaWFudFxyXG4gICAgICovXHJcbiAgICBnZXQgdmFyaWFudCgpOiBzdHJpbmdcclxuICAgIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdmFyaWFudDtcclxuICAgIH1cclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2V0IHZhcmlhbnQodmFsdWU6IHN0cmluZylcclxuICAgIHtcclxuICAgICAgICAvLyBSZW1vdmUgdGhlIG9sZCBjbGFzcyBuYW1lXHJcbiAgICAgICAgdGhpcy5fcmVuZGVyZXIucmVtb3ZlQ2xhc3ModGhpcy5fZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCB0aGlzLnZhcmlhbnQpO1xyXG5cclxuICAgICAgICAvLyBTdG9yZSB0aGUgdmFyaWFudCB2YWx1ZVxyXG4gICAgICAgIHRoaXMuX3ZhcmlhbnQgPSB2YWx1ZTtcclxuXHJcbiAgICAgICAgLy8gQWRkIHRoZSBuZXcgY2xhc3MgbmFtZVxyXG4gICAgICAgIHRoaXMuX3JlbmRlcmVyLmFkZENsYXNzKHRoaXMuX2VsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgdmFsdWUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==