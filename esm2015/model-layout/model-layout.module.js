import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { FormLayoutModule } from "../form-layout/form-layout.module";
import { ButtonLayoutModule } from "../button-layout/button-layout.module";
export class ModelLayoutModule {
}
ModelLayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ModelLayoutComponent],
                imports: [
                    RouterModule,
                    FormLayoutModule,
                    ButtonLayoutModule,
                    // Material
                    MaterialModule,
                    TranslateModule,
                    FuseSharedModule
                ],
                exports: [ModelLayoutComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWwtbGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJtb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBZTNFLE1BQU0sT0FBTyxpQkFBaUI7OztZQWQ3QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsb0JBQW9CLENBQUM7Z0JBQ3BDLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGdCQUFnQjtvQkFDaEIsa0JBQWtCO29CQUNsQixXQUFXO29CQUNYLGNBQWM7b0JBQ2QsZUFBZTtvQkFFZixnQkFBZ0I7aUJBQ2pCO2dCQUNELE9BQU8sRUFBRSxDQUFDLG9CQUFvQixDQUFDO2FBQ2hDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvcm91dGVyXCI7XHJcbmltcG9ydCB7IEZ1c2VTaGFyZWRNb2R1bGUgfSBmcm9tIFwiLi4vQGZ1c2Uvc2hhcmVkLm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tIFwiQG5neC10cmFuc2xhdGUvY29yZVwiO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gXCIuLi9tYXRlcmlhbC5tb2R1bGVcIjtcclxuXHJcbmltcG9ydCB7IE1vZGVsTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL21vZGVsLWxheW91dC9tb2RlbC1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IEZvcm1MYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vZm9ybS1sYXlvdXQvZm9ybS1sYXlvdXQubW9kdWxlXCI7XHJcbmltcG9ydCB7IEJ1dHRvbkxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9idXR0b24tbGF5b3V0L2J1dHRvbi1sYXlvdXQubW9kdWxlXCI7XHJcbkBOZ01vZHVsZSh7XHJcbiAgZGVjbGFyYXRpb25zOiBbTW9kZWxMYXlvdXRDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIEZvcm1MYXlvdXRNb2R1bGUsXHJcbiAgICBCdXR0b25MYXlvdXRNb2R1bGUsXHJcbiAgICAvLyBNYXRlcmlhbFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUsXHJcblxyXG4gICAgRnVzZVNoYXJlZE1vZHVsZVxyXG4gIF0sXHJcbiAgZXhwb3J0czogW01vZGVsTGF5b3V0Q29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTW9kZWxMYXlvdXRNb2R1bGUge31cclxuIl19