import { Injectable } from "@angular/core";
import { Subject } from "rxjs/Subject";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import * as i0 from "@angular/core";
export class MessageService {
    constructor() {
        this.subject = new Subject();
        this.routing = new Subject();
        this.clickEventSource = new BehaviorSubject(0);
        this.clickEventMessage = this.clickEventSource.asObservable();
        this.modelCloseSource = new BehaviorSubject(0);
        this.modelCloseMessage = this.modelCloseSource.asObservable();
        this.enableMessage = new Subject();
        this.setDatasouce = new Subject();
        this.pageRoutingMsg = new Subject();
        this.triggerNotification = new Subject();
        this.tableHeaderUpdate = new Subject();
    }
    sendButtonEnableMessage(message) {
        this.enableMessage.next({ data: message });
    }
    getButtonEnableMessage() {
        return this.enableMessage.asObservable();
    }
    sendRoutingMessage(data) {
        this.pageRoutingMsg.next({ pageData: data });
    }
    getRoutingMessage() {
        return this.pageRoutingMsg.asObservable();
    }
    sendTriggerNotification(data) {
        this.triggerNotification.next({ trigger: data });
    }
    getTriggerNotification() {
        return this.triggerNotification.asObservable();
    }
    sendTableHeaderUpdate(data) {
        this.tableHeaderUpdate.next({ data: data });
    }
    getTableHeaderUpdate() {
        return this.tableHeaderUpdate.asObservable();
    }
    sendMessage(message) {
        this.subject.next({ text: message });
    }
    clearMessages() {
        this.subject.next();
    }
    getMessage() {
        return this.subject.asObservable();
    }
    sendRouting(data) {
        this.routing.next({ data: data });
    }
    getRouting() {
        return this.routing.asObservable();
    }
    sendClickEvent(message) {
        this.clickEventSource.next(message);
    }
    sendModelCloseEvent(message) {
        this.modelCloseSource.next(message);
    }
    isObject(source) {
        return !!source && source.constructor === Object;
    }
    isArray(source) {
        return !!source && source.constructor === Array;
    }
    sendDatasource(message) {
        this.setDatasouce.next({ text: message });
    }
    getDatasource() {
        return this.setDatasouce.asObservable();
    }
}
MessageService.decorators = [
    { type: Injectable, args: [{ providedIn: "root" },] }
];
MessageService.ngInjectableDef = i0.defineInjectable({ factory: function MessageService_Factory() { return new MessageService(); }, token: MessageService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVzc2FnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbIl9zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBQ3ZDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQzs7QUFHdkQsTUFBTSxPQUFPLGNBQWM7SUFEM0I7UUFFVSxZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQU8sQ0FBQztRQUM3QixZQUFPLEdBQUcsSUFBSSxPQUFPLEVBQU8sQ0FBQztRQUM3QixxQkFBZ0IsR0FBRyxJQUFJLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNsRCxzQkFBaUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFakQscUJBQWdCLEdBQUcsSUFBSSxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFFbEQsc0JBQWlCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxDQUFDO1FBRWpELGtCQUFhLEdBQUcsSUFBSSxPQUFPLEVBQU8sQ0FBQztRQUNuQyxpQkFBWSxHQUFFLElBQUksT0FBTyxFQUFPLENBQUM7UUFDakMsbUJBQWMsR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO1FBQ3BDLHdCQUFtQixHQUFHLElBQUksT0FBTyxFQUFPLENBQUM7UUF5QnpDLHNCQUFpQixHQUFHLElBQUksT0FBTyxFQUFPLENBQUM7S0E4Q2hEO0lBckVDLHVCQUF1QixDQUFFLE9BQWdCO1FBQ3ZDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUMsSUFBSSxFQUFHLE9BQU8sRUFBQyxDQUFDLENBQUE7SUFDM0MsQ0FBQztJQUVELHNCQUFzQjtRQUNwQixPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVELGtCQUFrQixDQUFDLElBQVU7UUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBQyxRQUFRLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtJQUM1QyxDQUFDO0lBRUQsaUJBQWlCO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFFRCx1QkFBdUIsQ0FBQyxJQUFVO1FBQ2hDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsRUFBQyxPQUFPLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQTtJQUNoRCxDQUFDO0lBQ0Qsc0JBQXNCO1FBQ3BCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ2pELENBQUM7SUFHRCxxQkFBcUIsQ0FBRSxJQUFZO1FBQ2pDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBQyxJQUFJLEVBQUUsSUFBSSxFQUFDLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBQ0Qsb0JBQW9CO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQy9DLENBQUM7SUFDRCxXQUFXLENBQUMsT0FBZTtRQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRCxhQUFhO1FBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN0QixDQUFDO0lBRUQsVUFBVTtRQUNSLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsV0FBVyxDQUFDLElBQVM7UUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBQ0QsVUFBVTtRQUNSLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsY0FBYyxDQUFDLE9BQVk7UUFDekIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsbUJBQW1CLENBQUMsT0FBWTtRQUM5QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3RDLENBQUM7SUFDRCxRQUFRLENBQUMsTUFBTTtRQUNiLE9BQU8sQ0FBQyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsV0FBVyxLQUFLLE1BQU0sQ0FBQztJQUNuRCxDQUFDO0lBRUQsT0FBTyxDQUFDLE1BQU07UUFDWixPQUFPLENBQUMsQ0FBQyxNQUFNLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxLQUFLLENBQUM7SUFDbEQsQ0FBQztJQUNELGNBQWMsQ0FBQyxPQUFlO1FBQzVCLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQUNELGFBQWE7UUFDWCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDMUMsQ0FBQzs7O1lBcEZGLFVBQVUsU0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gXCJyeGpzL09ic2VydmFibGVcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSBcInJ4anMvQmVoYXZpb3JTdWJqZWN0XCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7IHByb3ZpZGVkSW46IFwicm9vdFwiIH0pXHJcbmV4cG9ydCBjbGFzcyBNZXNzYWdlU2VydmljZSB7XHJcbiAgcHJpdmF0ZSBzdWJqZWN0ID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHByaXZhdGUgcm91dGluZyA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuICBwcml2YXRlIGNsaWNrRXZlbnRTb3VyY2UgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KDApO1xyXG4gIGNsaWNrRXZlbnRNZXNzYWdlID0gdGhpcy5jbGlja0V2ZW50U291cmNlLmFzT2JzZXJ2YWJsZSgpO1xyXG5cclxuICBwcml2YXRlIG1vZGVsQ2xvc2VTb3VyY2UgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KDApO1xyXG5cclxuICBtb2RlbENsb3NlTWVzc2FnZSA9IHRoaXMubW9kZWxDbG9zZVNvdXJjZS5hc09ic2VydmFibGUoKTtcclxuXHJcbiAgcHJpdmF0ZSBlbmFibGVNZXNzYWdlID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHByaXZhdGUgc2V0RGF0YXNvdWNlPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcbiAgcHJpdmF0ZSBwYWdlUm91dGluZ01zZyA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuICBwcml2YXRlIHRyaWdnZXJOb3RpZmljYXRpb24gPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcblxyXG4gIHNlbmRCdXR0b25FbmFibGVNZXNzYWdlIChtZXNzYWdlIDogc3RyaW5nKSB7XHJcbiAgICB0aGlzLmVuYWJsZU1lc3NhZ2UubmV4dCh7ZGF0YSA6IG1lc3NhZ2V9KVxyXG4gIH1cclxuXHJcbiAgZ2V0QnV0dG9uRW5hYmxlTWVzc2FnZSAoKSA6IE9ic2VydmFibGU8YW55PntcclxuICAgIHJldHVybiB0aGlzLmVuYWJsZU1lc3NhZ2UuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZW5kUm91dGluZ01lc3NhZ2UoZGF0YSA6IGFueSl7XHJcbiAgICB0aGlzLnBhZ2VSb3V0aW5nTXNnLm5leHQoe3BhZ2VEYXRhOiBkYXRhfSlcclxuICB9XHJcblxyXG4gIGdldFJvdXRpbmdNZXNzYWdlICgpIDogT2JzZXJ2YWJsZTxhbnk+e1xyXG4gICAgcmV0dXJuIHRoaXMucGFnZVJvdXRpbmdNc2cuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZW5kVHJpZ2dlck5vdGlmaWNhdGlvbihkYXRhIDogYW55KXtcclxuICAgIHRoaXMudHJpZ2dlck5vdGlmaWNhdGlvbi5uZXh0KHt0cmlnZ2VyOiBkYXRhfSlcclxuICB9XHJcbiAgZ2V0VHJpZ2dlck5vdGlmaWNhdGlvbiAoKSA6IE9ic2VydmFibGU8YW55PntcclxuICAgIHJldHVybiB0aGlzLnRyaWdnZXJOb3RpZmljYXRpb24uYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBwcml2YXRlIHRhYmxlSGVhZGVyVXBkYXRlID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gIHNlbmRUYWJsZUhlYWRlclVwZGF0ZSAoZGF0YTogc3RyaW5nKXtcclxuICAgIHRoaXMudGFibGVIZWFkZXJVcGRhdGUubmV4dCh7ZGF0YTogZGF0YX0pO1xyXG4gIH1cclxuICBnZXRUYWJsZUhlYWRlclVwZGF0ZSAoKSA6IE9ic2VydmFibGU8YW55PntcclxuICAgIHJldHVybiB0aGlzLnRhYmxlSGVhZGVyVXBkYXRlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuICBzZW5kTWVzc2FnZShtZXNzYWdlOiBzdHJpbmcpIHtcclxuICAgIHRoaXMuc3ViamVjdC5uZXh0KHsgdGV4dDogbWVzc2FnZSB9KTtcclxuICB9XHJcbiAgXHJcbiAgY2xlYXJNZXNzYWdlcygpIHtcclxuICAgIHRoaXMuc3ViamVjdC5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICBnZXRNZXNzYWdlKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICByZXR1cm4gdGhpcy5zdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuXHJcbiAgc2VuZFJvdXRpbmcoZGF0YTogYW55KSB7XHJcbiAgICB0aGlzLnJvdXRpbmcubmV4dCh7IGRhdGE6IGRhdGEgfSk7XHJcbiAgfVxyXG4gIGdldFJvdXRpbmcoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIHJldHVybiB0aGlzLnJvdXRpbmcuYXNPYnNlcnZhYmxlKCk7XHJcbiAgfVxyXG5cclxuICBzZW5kQ2xpY2tFdmVudChtZXNzYWdlOiBhbnkpIHtcclxuICAgIHRoaXMuY2xpY2tFdmVudFNvdXJjZS5uZXh0KG1lc3NhZ2UpO1xyXG4gIH1cclxuXHJcbiAgc2VuZE1vZGVsQ2xvc2VFdmVudChtZXNzYWdlOiBhbnkpIHtcclxuICAgIHRoaXMubW9kZWxDbG9zZVNvdXJjZS5uZXh0KG1lc3NhZ2UpO1xyXG4gIH1cclxuICBpc09iamVjdChzb3VyY2UpIHtcclxuICAgIHJldHVybiAhIXNvdXJjZSAmJiBzb3VyY2UuY29uc3RydWN0b3IgPT09IE9iamVjdDtcclxuICB9XHJcblxyXG4gIGlzQXJyYXkoc291cmNlKSB7XHJcbiAgICByZXR1cm4gISFzb3VyY2UgJiYgc291cmNlLmNvbnN0cnVjdG9yID09PSBBcnJheTtcclxuICB9XHJcbiAgc2VuZERhdGFzb3VyY2UobWVzc2FnZTogc3RyaW5nKSB7XHJcbiAgICB0aGlzLnNldERhdGFzb3VjZS5uZXh0KHsgdGV4dDogbWVzc2FnZSB9KTtcclxuICB9XHJcbiAgZ2V0RGF0YXNvdXJjZSgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuc2V0RGF0YXNvdWNlLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxufVxyXG4iXX0=