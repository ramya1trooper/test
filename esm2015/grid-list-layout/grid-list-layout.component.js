import { ModelLayoutComponent } from "./../model-layout/model-layout.component";
import { Component, Input, Inject } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as FileSaver from "file-saver";
import { MatDialogRef } from "@angular/material/dialog";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as en } from "../i18n/en";
import * as _ from "lodash";
import { MatDialog } from "@angular/material";
import { SnackBarService } from "./../shared/snackbar.service";
import { LoaderService } from "../loader.service";
import { ContentService } from "../content/content.service";
import { MessageService } from "../_services/index";
import { takeUntil } from "rxjs/operators";
import { Subject } from "rxjs/Subject";
export class GridListLayoutComponent {
    constructor(formBuilder, _matDialog, _fuseTranslationLoaderService, snackBarService, loaderService, contentService, messageService, matDialogRef, en) {
        this.formBuilder = formBuilder;
        this._matDialog = _matDialog;
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.snackBarService = snackBarService;
        this.loaderService = loaderService;
        this.contentService = contentService;
        this.messageService = messageService;
        this.matDialogRef = matDialogRef;
        this.en = en;
        this.buttonsConfigData = {};
        this.responsive = true;
        this.show = false;
        this.selectedValue = {};
        this.unsubscribe = new Subject();
        this.selectedChip = {};
        this.enableFormLayout = false;
        this.tableHeaderConfig = [];
        this.selectedTableHeader = [];
        this.recipient = [];
        this.defaultDatasourceName = localStorage.getItem("datasourceName");
        this.defaultDatasource = localStorage.getItem("datasource");
        this._fuseTranslationLoaderService.loadTranslations(en);
        this.CreateForm = new FormGroup({
            selectReport: new FormControl()
        });
        this.messageService
            .getMessage()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((message) => {
            console.log(message, "....gridMESSAGE");
            this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            console.log(message, "....gridcurrentConfigData");
            // this.enableTableLayout = false;
            // this.enableNewTableLayout = false;
            this.ngOnInit();
        });
    }
    ngOnInit() {
        let initialObj = {
            datasourceId: this.defaultDatasource,
            datasourceName: this.defaultDatasourceName,
            limit: 10,
            offset: 0,
        };
        this.userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        if (this.userData) {
            this.recipient.push(this.userData.email);
        }
        this.buttonsConfigData = localStorage.getItem("buttonConfig")
            ? JSON.parse(localStorage.getItem("buttonConfig"))
            : {};
        this.formData = this.data;
        let tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(tempData)
            ? JSON.parse(tempData)
            : {
                datasourceId: this.defaultDatasource,
                datasourceName: this.defaultDatasourceName,
                limit: 10,
                offset: 0,
            };
        // this.inputData = { ...this.inputData, ...initialObj };
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        // console.log(this.formValues, ".....grid this.inputData");
        // this.inputData["datasourceId"] = this.defaultDatasource;
        this.buttonsConfigData = localStorage.getItem("buttonConfig")
            ? JSON.parse(localStorage.getItem("buttonConfig"))
            : {};
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.currentData = this.currentConfigData["listView"];
        this.noOfColumns = this.currentData.numberOfColumnsInRow;
        this.gridListData = this.currentData.gridData;
        this.selectedReportType = this.inputData["selectedReportType"];
        var self = this;
        setTimeout(function () {
            if (self.selectedReportType) {
                self.enableFormLayout = true;
            }
        }, 100);
    }
    onButtonClick(buttonId, itemData) {
        let modelData;
        let modelWidth;
        if (buttonId.id == "run") {
            modelData = _.find(itemData.modelData, "isRun");
        }
        else {
            modelData = _.find(itemData.modelData, "isSchedule");
        }
        modelWidth = modelData.size;
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: "exportView",
                modelData: modelData,
            },
        })
            .afterClosed()
            .subscribe((response) => {
            // localStorage.removeItem("currentInput");
        });
    }
    onResize(event) {
        var width;
        if (event === 0) {
            width = window.innerWidth;
        }
        else {
            width = event.target.innerWidth;
        }
        if (width <= 624) {
            this.noOfColumns = 1;
        }
        else if (width <= 900 && width > 750) {
            this.noOfColumns = 2;
        }
        else if (width <= 1500 && width > 1000) {
            this.noOfColumns = 3;
        }
        else if (width >= 1500) {
            this.noOfColumns = 4;
        }
    }
    buttonListClick(data, formValues) {
        console.log(data, ".....DATA");
        this.enableFormLayout = false;
        console.log(formValues, "....formValues");
        if (data.action == "export") {
            this.onExportClick(data, formValues);
        }
        else {
            this.inputData["selectedChips"] = {};
            this.inputData["selectedValue"] = this.selectedValue;
            this.inputData["selectedReportType"] = this.selectedReportType;
            this.inputData["selectedChipKeyList"] = {};
            let self = this;
            self.tableList = [];
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            this.ngOnInit();
        }
    }
    onExportClick(buttonData, request) {
        console.log(">>>>>>>>>>>>onExportClick ", request);
        let tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(tempData)
            ? JSON.parse(tempData)
            : {
                limit: 10,
                offset: 0,
            };
        // this.inputData=request
        console.log("requestDetails>>>", this.inputData);
        let exportRequest, requestDetails, apiUrl;
        let requestData = {}, reportTemp;
        let dynamicReport = request.isdyamicData ? request.isdyamicData : false;
        if (dynamicReport) {
            this.currentData = this.currentConfigData.exportData;
            exportRequest = this.currentData.exportRequest;
            let ReportData = JSON.parse(localStorage.getItem("CurrentReportData"));
            apiUrl = exportRequest.apiUrl;
            reportTemp = Object.assign({}, ReportData, buttonData);
            requestDetails = exportRequest.RequestData;
        }
        else {
            let formRequest = request.exportRequest;
            exportRequest = formRequest
                ? formRequest
                : this.formValues.exportData.exportRequest;
            this.inputData["exportType"] = buttonData ? buttonData.exportType : "";
            requestDetails = exportRequest.requestData;
            apiUrl = exportRequest.apiUrl;
        }
        console.log(requestDetails, ">>>gridkkkrequestDetails");
        var self = this;
        _.forEach(requestDetails, function (item) {
            let tempData;
            if (item.subKey &&
                self.inputData[item.value] &&
                self.inputData[item.value][item.subKey]) {
                if (item.convertString) {
                    let temp = [];
                    _.forEach(self.inputData[item.value][item.subKey], function (dataItem) {
                        temp.push(dataItem);
                    });
                    tempData = JSON.stringify(temp);
                }
                else {
                    tempData = self.inputData[item.value][item.subKey];
                }
            }
            else if (!item.subKey) {
                if (item.reportCheck) {
                    var t1 = item.fromTranslation
                        ? self._fuseTranslationLoaderService.instant(item.value)
                        : item.value;
                    tempData = item.conditionCheck ? reportTemp[item.value] : t1;
                }
                else if (item.fromLoginData) {
                    tempData = self.userData ? self.userData[item.value] : "";
                }
                else {
                    tempData = item.isDefault ? item.value : self.inputData[item.value];
                }
            }
            // console.log(self.formValues);
            // if (self.formValues.getValueFromConfig && self.formValues[item.value]) {
            //   tempData = self.formValues[item.value];
            // }
            console.log("checkrequestData ", tempData);
            if (tempData)
                requestData[item.name] = item.convertToString
                    ? JSON.stringify(tempData)
                    : tempData;
        });
        if (exportRequest.isReportCreate) {
            // form based report create
            // Export report
            var self = this;
            this.submitted = true;
            console.log(">>Form report requestData ", requestData);
            localStorage.setItem('currentInput', JSON.stringify(this.inputData));
            this.validationCheck(function (result) {
                console.log(">>>>>>>> result ", result);
                let toastMessageDetails = exportRequest.toastMessage;
                self.contentService.createRequest(requestData, apiUrl).subscribe((res) => {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                    if (res.status == 200) {
                        localStorage.removeItem("CurrentReportData");
                        // self.matDialogRef.close();
                        self.messageService.sendModelCloseEvent("listView");
                    }
                }, (error) => {
                    self.submitted = false;
                    self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                });
            });
        }
        else {
            console.log("second");
            this.contentService
                .getExportResponse(requestData, apiUrl)
                .subscribe((data) => {
                if (this.formValues.exportData &&
                    this.formValues.exportData.s3Download) {
                    let toastMessageDetails = exportRequest.toastMessage;
                    this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                    // this.matDialogRef.close();
                }
                else {
                    let fileName = this._fuseTranslationLoaderService.instant(exportRequest.downloadFileName);
                    this.downloadExportFile(data, fileName + buttonData.fileType, buttonData.selectionType);
                }
            });
        }
    }
    downloadExportFile(reportValue, fileName, selectionType) {
        const blob = new Blob([reportValue], { type: selectionType });
        FileSaver.saveAs(blob, fileName);
    }
    validationCheck(callback) {
        let tempData = JSON.parse(localStorage.getItem("currentInput"));
        if (this.inputGroup && this.inputGroup.valid) {
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            callback(true);
        }
        else {
            callback(false);
        }
    }
    onSelectreport(e) {
        console.log(e.value, "testst herher>>>>>>> ");
        this.selectedValue = _.find(this.gridListData, { gridId: e.value });
        this.data = this.selectedValue.modelData;
        // this.gridListData = this.currentData.gridData;
        this.currentReportType = this.selectedValue;
        this.currentReportType.formData.forEach((datas) => {
            this.getDatas = datas;
        });
        this.enableFormLayout = true;
        // start table headerData
        this.tableHeaderConfig = this.selectedValue.tableData[0].tableHeader;
        let tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
        this.selectedTableHeader = _.map(tempArray, "value");
        this.inputData["selectedChips"] = {};
        this.inputData["selectedValue"] = this.selectedValue;
        this.inputData["selectedReportType"] = this.selectedReportType;
        this.inputData["selectedChipKeyList"] = {};
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        let self = this;
        self.tableList = [];
        console.log("SELF>>>>>>", self);
        // }
        // end table header 
        // setTimeout(function(){
        // }, 500)
        // console.log(this.gridListData, "check here");
    }
    updateTableSettings(event) {
        console.log(">>> updateTableSettings event ", event);
        let selectedHeader = this.selectedTableHeader;
        this.tableHeaderConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isActive = true;
            }
            else {
                item.isActive = false;
            }
        });
        localStorage.removeItem("selectedTableHeaders");
        localStorage.setItem("selectedTableHeaders", JSON.stringify(this.tableHeaderConfig));
        this.messageService.sendTableHeaderUpdate("update");
    }
    submitView(e) {
        this.tableShow = false;
        console.log(this, "Checkkkk>>>>");
        let tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(JSON.parse(tempData))
            ? JSON.parse(tempData)
            : {};
        var self = this;
        let item = this.selectedValue;
        console.log(item, "selfchiplimit");
        let apiUrl = item.sumbitRequest.apiUrl;
        let responseName = item.sumbitRequest.response;
        let requestData = item.sumbitRequest.requestData;
        let query = {};
        //debugger;
        console.log("query>>>>>>", query);
        _.forEach(requestData, function (requestItem) {
            let tempData = requestItem.subKey && self.inputData[requestItem.value]
                ? self.inputData[requestItem.value][requestItem.subKey]
                : self.inputData[requestItem.value];
            if (requestItem.isDefault) {
                query[requestItem.name] = requestItem.convertToString
                    ? JSON.stringify(requestItem.value)
                    : requestItem.value;
            }
            else if (tempData) {
                query[requestItem.name] = requestItem.convertToString
                    ? JSON.stringify(tempData)
                    : tempData;
            }
            // console.log("SASAAsa",requestItem.name)
            // if (requestItem.isDefault) {
            //   query[requestItem.name] = requestItem.value;
            // } 
            // else {
            //   query[requestItem.name] = requestItem.convertToString
            //   ? JSON.stringify(self.inputData[requestItem.value])
            //   : self.inputData[requestItem.value];
            // }
        });
        self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
            console.log("SSSSSS>>>>>>", data);
            let responseVal = data.response[responseName];
            let validateData = responseVal[0];
            let isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
            if (!isArrayOfJSON) {
                const tempList = [];
                _.forEach(item.data, function (item) {
                    tempList.push({ name: item, _id: item });
                });
                item.data = tempList;
            }
            self.tableList = self.selectedValue.tableData;
            self.currentTableLoad = self.selectedValue;
            self.currentTableLoad["response"] = responseVal;
            self.currentTableLoad["total"] = data.response.total;
            console.log(self, ">>> this");
            // debugger;
            self.tableShow = true;
        });
        // });
    }
}
GridListLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "grid-list-layout",
                template: "<div class=\"content p-24\">\r\n  <!-- new card -->\r\n  \r\n  <!-- end card -->\r\n\r\n  <!-- <mat-grid-list class=\"row\" [cols]=\"noOfColumns\" rowHeight=\"310\" (window:resize)=\"onResize($event)\"\r\n    [responsive]=\"responsive\">\r\n    <mat-grid-tile class=\"col-sm-4\" *ngFor=\"let gridData of gridListData\">\r\n      <div class=\"card card-1\">\r\n        <div class=\"card-body\">\r\n          <div class=\"px-16 border-bottom runcard_title\" fxla yout=\"row wrap\" fxlayoutalign=\"space-between center\"\r\n            ng-reflect-layout=\"row wrap\" ng-reflect-align=\"space-between center\"\r\n            style=\"flex-flow: row wrap; box-sizing: border-box; display: flex; max-height: 100%; place-content: center space-between; align-items: center;border-radius: 14px 14px 0px 0px;\">\r\n            <div class=\"ng-tns-c30-8\">\r\n            </div>\r\n            <div class=\"py-16 h3 run-title\" fxflex=\"\" ng-reflect-flex=\"\" style=\"flex: 1 1 0%; box-sizing: border-box;\">\r\n              {{gridData.header | translate}}\r\n            </div>\r\n          </div>\r\n          <p class=\"card-text border-bottom bb\">{{gridData.content | translate}}</p>\r\n          <div class=\"ui-common-button-row button-cent\">\r\n            <button *ngFor=\"let buttonId of gridData.buttonList\" class=\"common-btn btnformat\"\r\n              (click)=\"onButtonClick(buttonsConfigData[buttonId], gridData)\"\r\n              [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n              mat-flat-button>\r\n              <mat-icon class=\"icon-size\" *ngIf=\"buttonsConfigData[buttonId]\">{{buttonsConfigData[buttonId].icon}}\r\n              </mat-icon>{{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n            </button>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </mat-grid-tile>\r\n  </mat-grid-list> -->\r\n\r\n\r\n\r\n  <!-- 17112020 new Report Ui Design -->\r\n  <div class=\"row\">\r\n    <div class=\"col-12\">\r\n      <div class=\"card-hover-shadow-2x mb-3 card card-new-shadow\">\r\n        <div class=\"card-header-tab card-header\">\r\n          <div class=\"card-header-title font-size-lg text-capitalize font-weight Normal\">\r\n            <span class=\"material-icons header-icon\">\r\n              report\r\n            </span>\r\n            Reports Details\r\n          </div>\r\n        </div>\r\n        <div class=\"sen-lib-scroll-area-lg\">\r\n          <div class=\"sen-lib-scrollbar-container\">\r\n            <div class=\"p-2\">\r\n              <ul class=\"sen-lib-list-wrapper list-group list-group-flush\">\r\n                <!-- new design -->\r\n                <div class=\"row\">\r\n                  <div class=\"col-6\">\r\n                    <form [formGroup]=\"CreateForm\">\r\n                      <!-- <mat-label>Select Report</mat-label> -->\r\n                      <mat-form-field appearance=\"outline\" style=\"width: 100%;margin-top:14px;padding-left: 19px;\">\r\n                        <mat-label>Select Report</mat-label>\r\n                        <mat-select placeholder=\"Select report\" (selectionChange)=\"onSelectreport($event)\"\r\n                          [(ngModel)]=\"selectedReportType\" formControlName=\"selectReport\">\r\n                          <mat-option *ngFor=\"let gridData of gridListData\" [value]=\"gridData.gridId\">\r\n                            {{gridData.header | translate}}\r\n                          </mat-option>\r\n                        </mat-select>\r\n                      </mat-form-field>\r\n\r\n                    </form>\r\n                  </div>\r\n                  <div class=\"col-6\">\r\n                    <div class=\"float-right\" style=\"margin-top:15px;margin-right:15px;\">\r\n                      <div class=\"btn-group sen-lib-btn-bg-group ng-star-inserted\" role=\"group\">\r\n\r\n                        <button  class=\"tableSettingsBtn btn btn-border-right sen-lib-grp-btn-font\" type=\"button\"\r\n                        (click)=\"select.open()\">\r\n                          <span class=\"material-icons sen-lib-delete sen-lib-cursor\"\r\n                            >settings</span>Settings\r\n                          <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n                            (selectionChange)=\"updateTableSettings($event)\">\r\n                            <mat-option *ngFor=\"let columnSetting of tableHeaderConfig\" [checked]=\"columnSetting.isActive\"\r\n                              [value]=\"columnSetting.value\">{{ columnSetting.name | translate}}</mat-option>\r\n                          </mat-select> \r\n                        </button>\r\n\r\n                        <ng-container *ngFor=\"let formBtn of getDatas?.buttonsList\">\r\n                          <button class=\"btn btn-border-left sen-lib-grp-btn-font\" type=\"button\"\r\n                            (click)=\"buttonListClick(buttonsConfigData[formBtn],getDatas)\">\r\n                            <span class=\"material-icons sen-lib-delete sen-lib-cursor\">\r\n                              <span *ngIf=\"formBtn=='reset'\">refresh</span>\r\n                              <span *ngIf=\"formBtn=='exportCSV'\">cloud_download</span>\r\n                              <span *ngIf=\"formBtn=='exportPDF'\">cloud_download</span>\r\n                              <span *ngIf=\"formBtn=='exportXLSX'\">cloud_download</span>\r\n                            </span>\r\n                            <!-- {{formBtn | translate}} -->\r\n                            {{formBtn=='reset'?'Reset':''}}\r\n                            {{formBtn=='exportCSV'?'Export CSV':''}}\r\n                            {{formBtn=='exportXLSX'?'Export XLSX':''}}\r\n                            {{formBtn=='exportPDF'?'Export PDF':''}}\r\n                          </button>\r\n                        </ng-container>\r\n                        <!-- <button class=\"btn btn-border-right\" type=\"button\">\r\n                          <span class=\"material-icons sen-lib-delete sen-lib-cursor\"> refresh </span>\r\n                          <span class=\"sen-lib-grp-btn-font\">Reset</span>\r\n                        </button>\r\n                        <button class=\"btn btn-border-right\" type=\"button\">\r\n                          <span class=\"material-icons sen-lib-delete sen-lib-cursor\"> cloud_download </span>\r\n                          <span class=\"sen-lib-grp-btn-font\">Expot CSV</span>\r\n                        </button>\r\n                        <button class=\"btn \" type=\"button\">\r\n                          <span class=\"material-icons sen-lib-delete sen-lib-cursor\"> cloud_download </span>\r\n                          <span class=\"sen-lib-grp-btn-font\">Export XLSX</span>\r\n                        </button> -->\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div mat-dialog-content class=\"p-24 pb-0 m-0 col-12\"\r\n                    *ngIf=\"currentReportType && currentReportType.formData && enableFormLayout\" fusePerfectScrollbar>\r\n                    <ng-container *ngFor=\"let formField of currentReportType.formData\">\r\n                      <form-layout [onLoadData]=\"data\" [importData]=\"formField.importData\" [formValues]=\"formField\">\r\n                      </form-layout>\r\n                      <div class=\"text-right\" style=\"margin-bottom: 5px;\">\r\n                        <button class=\"btn btn-success sen-lib-btn-size\"\r\n                          (click)=\"submitView($event)\">{{formField.submitButton}}</button>\r\n                      </div>\r\n                      <div class=\"col-12\" style=\"margin-top: 15px;padding: 0;margin-bottom: 10px;\" *ngIf=\"tableShow\">\r\n                        <!-- <new-table-layout  [onLoad]=\"data\" [tableId]=\"tableItem.tableId\"\r\n                        [viewFrom]=\"'list'\"></new-table-layout> -->\r\n                        <ng-container *ngFor=\"let tableItem of tableList\">\r\n                          <new-table-layout [tableId]=\"tableItem.tableId\" [viewFrom]=\"'view'\" [onLoad]=\"currentTableLoad\">\r\n                          </new-table-layout>\r\n                        </ng-container>\r\n                       \r\n                        <!-- <table class=\"table table-bordered\">\r\n                          <thead style=\"background-color: #243b55;\">\r\n                            <tr>\r\n                              <th scope=\"col\">#</th>\r\n                              <th scope=\"col\">First</th>\r\n                              <th scope=\"col\">Last</th>\r\n                              <th scope=\"col\">Handle</th>\r\n                            </tr>\r\n                          </thead>\r\n                          <tbody>\r\n                            <tr>\r\n                              <th scope=\"row\">1</th>\r\n                              <td>Mark</td>\r\n                              <td>Otto</td>\r\n                              <td>@mdo</td>\r\n                            </tr>\r\n                            <tr>\r\n                              <th scope=\"row\">2</th>\r\n                              <td>Jacob</td>\r\n                              <td>Thornton</td>\r\n                              <td>@fat</td>\r\n                            </tr>\r\n                            <tr>\r\n                              <th scope=\"row\">3</th>\r\n                              <td colspan=\"2\">Larry the Bird</td>\r\n                              <td>@twitter</td>\r\n                            </tr>\r\n                          </tbody>\r\n                        </table> -->\r\n                      </div>\r\n                    </ng-container>\r\n                  </div>\r\n                </div>\r\n              </ul>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <!-- 17112020 -->\r\n</div>\r\n",
                styles: [".card{background:#fff!important;display:inline-block!important;margin:1rem!important;position:relative!important;border-radius:4px;width:100%;max-width:100%}.btn-border-left{border-left:2px solid #fff}.btn-common-view{border-radius:25px!important;margin:8px}.run-title{font-size:15px!important;text-align:center!important;color:#000!important}.card-text{font-size:13px;min-height:155px;padding:10px!important;min-width:300px!important}.card-title{font-size:18px!important}.card-1{box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-webkit-transition:.3s cubic-bezier(.25,.8,.25,1);transition:.3s cubic-bezier(.25,.8,.25,1)}.card-1:hover{box-shadow:0 2px 14px rgba(0,0,0,.25),0 0 10px rgba(0,0,0,.22)}.runcard_title{background-color:#f5f5f5!important;border-radius:14px 14px 0 0!important}.ui-common-button-row a,.ui-common-button-row button{margin-right:8px}.button-cent{text-align:center!important}.btnformat{font-size:12px!important;border-radius:25px;font-weight:700;box-sizing:border-box;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;outline:0;border:none;-webkit-tap-highlight-color:transparent;display:inline-block;white-space:nowrap;text-decoration:none;vertical-align:baseline;text-align:center;margin:0;min-width:64px;line-height:36px;padding:0 16px;overflow:visible}.task{position:relative;overflow:hidden;cursor:pointer;-webkit-perspective:800px;perspective:800px;-webkit-transform-style:preserve-3d;transform-style:preserve-3d;margin-bottom:12px}.abstract,.details{width:100%;padding:15px 30px;position:relative;background:#fff}.abstract{-webkit-transition:.3s;transition:.3s}.details{max-height:0;padding:0;overflow:hidden;visibility:hidden;-webkit-transform:rotateX(-180deg);transform:rotateX(-180deg);-webkit-transform-origin:top center;transform-origin:top center;-webkit-backface-visibility:hidden;backface-visibility:hidden;-webkit-transition:transform .3s;transition:transform .3s}.task:hover .details{max-height:none;overflow:visible;visibility:visible;-webkit-transform:rotateX(0);transform:rotateX(0)}.task:hover .abstract,.task:hover .details{background:#fafafa}.box7{position:relative;box-shadow:0 0 9px rgba(0,0,0,.3);overflow:hidden}.box7::before,.box7:after{content:''}.box7::after,.box7::before{width:100%;height:100%;background:rgba(11,33,47,.9);position:absolute;top:0;left:0;opacity:0;-webkit-transition:.5s;transition:.5s}.icon{padding-left:0}.icon li{list-style:none;text-align:center}.box7 .icon li a{text-align:center}.box7 .box-content{width:100%;position:absolute;bottom:-100%;left:0;-webkit-transition:.5s;transition:.5s}.box-7 .title{display:block;font-size:22px;font-weight:700;color:#fff!important;margin:0 0 10px;text-transform:uppercase;letter-spacing:1px;text-align:center}.box-7 .post{display:block;font-size:15px;font-weight:600;color:#fff;margin-bottom:10px;font-style:italic}.box-7 .icon{margin:0;padding:0;list-style:none}.box-7 .icon li{display:inline-block}.box-7 .icon li a{display:block;width:35px;height:35px;line-height:35px;border-radius:50%;background:#0dab76;font-size:18px;color:#fff;margin-right:10px;-webkit-transition:.5s;transition:.5s}.box7:after{background:rgba(255,255,255,.3);border:2px solid #0dab76;top:0;left:170%;opacity:1;z-index:1;-webkit-transform:skewX(45deg);transform:skewX(45deg);-webkit-transition:1s;transition:1s}.box7:hover:before{opacity:1}.box7:hover:after{left:-170%!important}.box7:hover .box-content{bottom:30%}.box7-description{text-align:justify!important}.box7-header{padding:10px;background:-webkit-gradient(linear,right top,left top,from(#141e30),to(#243b55))!important;background:linear-gradient(to left,#141e30,#243b55)!important;color:#fff;text-align:center;font-weight:600}.card .box7-header ul.creative-dots li.big-dot{left:-32px;top:0;width:50px;height:50px;border-radius:80%!important}.card .box7-header ul.creative-dots li{position:absolute;border-radius:100%}.senbg-primary{background-color:#7e37d8!important;color:#fff}.semi-big-dot{width:30px;height:30px;left:-14px;top:42px}ul.creative-dots li{position:absolute;border-radius:100%}.senbg-secondary{background-color:#fe80b2!important;color:#fff}ul.creative-dots li.medium-dot{width:20px;height:20px;left:18px;top:-5px}.ul.creative-dots li{position:absolute;border-radius:100%}.senbg-warning{background-color:#ffc717!important;color:#fff}.senbg-info{background-color:#06b5dd!important;color:#fff}.semi-small-dot{width:8px;height:8px;left:32px;top:37px}.title{text-align:center;color:#fff;font-weight:600}.sen-less-height{line-height:16px;overflow:hidden}.show{overflow:visible;height:auto}.sen-lib-btn-bg-group{border-radius:29px;margin-left:10px;box-shadow:3px 3px 5px 0 #b3b3b3;background:linear-gradient(135deg,#a2b6df 0,#33569b 100%)}.btn-border-right{border-right:2px solid #fff!important}.sen-lib-delete{font-size:22px;position:relative;color:#fff;top:4px}.sen-lib-grp-btn-font{position:relative;bottom:4px;font-size:13px;color:#fff;text-transform:capitalize}.btn-border-right:nth-child(3){border-right:0!important}.sen-lib-btn-size{border-radius:50px;margin-top:15px;font-size:13px}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}"]
            }] }
];
/** @nocollapse */
GridListLayoutComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: MatDialog },
    { type: FuseTranslationLoaderService },
    { type: SnackBarService },
    { type: LoaderService },
    { type: ContentService },
    { type: MessageService },
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
GridListLayoutComponent.propDecorators = {
    formValues: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JpZC1saXN0LWxheW91dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiZ3JpZC1saXN0LWxheW91dC9ncmlkLWxpc3QtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUNoRixPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDakUsT0FBTyxFQUNMLFdBQVcsRUFDWCxTQUFTLEVBRVQsV0FBVyxFQUNaLE1BQU0sZ0JBQWdCLENBQUM7QUFDeEIsT0FBTyxLQUFLLFNBQVMsTUFBTSxZQUFZLENBQUM7QUFDeEMsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRXhELE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLDZDQUE2QztBQUM3QyxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sY0FBYyxDQUFDO0FBT3ZDLE1BQU0sT0FBTyx1QkFBdUI7SUFxQ2xDLFlBQ1UsV0FBd0IsRUFDeEIsVUFBcUIsRUFDckIsNkJBQTJELEVBQzNELGVBQWdDLEVBQ2hDLGFBQTRCLEVBQzVCLGNBQThCLEVBQzlCLGNBQThCLEVBQy9CLFlBQWdELEVBQzVCLEVBQUU7UUFSckIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQUNyQixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQzNELG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUM1QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQy9CLGlCQUFZLEdBQVosWUFBWSxDQUFvQztRQUM1QixPQUFFLEdBQUYsRUFBRSxDQUFBO1FBdkMvQixzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFJNUIsZUFBVSxHQUFHLElBQUksQ0FBQztRQUNsQixTQUFJLEdBQUcsS0FBSyxDQUFDO1FBQ2Isa0JBQWEsR0FBUSxFQUFFLENBQUM7UUFZaEIsZ0JBQVcsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBSzFDLGlCQUFZLEdBQVEsRUFBRSxDQUFDO1FBR3ZCLHFCQUFnQixHQUFhLEtBQUssQ0FBQztRQUVuQyxzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFDNUIsd0JBQW1CLEdBQVEsRUFBRSxDQUFDO1FBb0M5QixjQUFTLEdBQVEsRUFBRSxDQUFDO1FBeEJsQixJQUFJLENBQUMscUJBQXFCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1FBQ3BFLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBRTVELElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksU0FBUyxDQUFDO1lBQzlCLFlBQVksRUFBRSxJQUFJLFdBQVcsRUFBRTtTQUNoQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsY0FBYzthQUNsQixVQUFVLEVBQUU7YUFDWixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNqQyxTQUFTLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQzFDLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBRWxELGtDQUFrQztZQUNsQyxxQ0FBcUM7WUFDckMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2xCLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUtELFFBQVE7UUFDTixJQUFJLFVBQVUsR0FBRztZQUNmLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCO1lBQ3BDLGNBQWMsRUFBRSxJQUFJLENBQUMscUJBQXFCO1lBQzFDLEtBQUssRUFBRSxFQUFFO1lBQ1QsTUFBTSxFQUFFLENBQUM7U0FDVixDQUFDO1FBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBQ3JFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzFDO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDO1lBQzNELENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDbEQsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNQLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUMxQixJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUNuQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDdEIsQ0FBQyxDQUFDO2dCQUNFLFlBQVksRUFBRSxJQUFJLENBQUMsaUJBQWlCO2dCQUNwQyxjQUFjLEVBQUUsSUFBSSxDQUFDLHFCQUFxQjtnQkFDMUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ1QsTUFBTSxFQUFFLENBQUM7YUFDVixDQUFDO1FBQ04seURBQXlEO1FBQ3pELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsNERBQTREO1FBRTVELDJEQUEyRDtRQUMzRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUM7WUFDM0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUNsRCxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1AsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztRQUNGLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxvQkFBb0IsQ0FBQztRQUN6RCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDO1FBQzlDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDL0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNULElBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFDO2dCQUN6QixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO2FBQzlCO1FBQ0gsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFBO0lBRVQsQ0FBQztJQUNELGFBQWEsQ0FBQyxRQUFRLEVBQUUsUUFBUTtRQUU5QixJQUFJLFNBQVMsQ0FBQztRQUNkLElBQUksVUFBVSxDQUFDO1FBQ2YsSUFBSSxRQUFRLENBQUMsRUFBRSxJQUFJLEtBQUssRUFBRTtZQUN4QixTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2pEO2FBQU07WUFDTCxTQUFTLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxFQUFFLFlBQVksQ0FBQyxDQUFDO1NBQ3REO1FBQ0QsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTthQUM3QixJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDMUIsWUFBWSxFQUFFLElBQUk7WUFDbEIsS0FBSyxFQUFFLFVBQVU7WUFDakIsVUFBVSxFQUFFLHFCQUFxQjtZQUNqQyxJQUFJLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLFlBQVk7Z0JBQ3BCLFNBQVMsRUFBRSxTQUFTO2FBQ3JCO1NBQ0YsQ0FBQzthQUNELFdBQVcsRUFBRTthQUNiLFNBQVMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ3RCLDJDQUEyQztRQUM3QyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxRQUFRLENBQUMsS0FBSztRQUNaLElBQUksS0FBSyxDQUFDO1FBQ1YsSUFBSSxLQUFLLEtBQUssQ0FBQyxFQUFFO1lBQ2YsS0FBSyxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUM7U0FDM0I7YUFBTTtZQUNMLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQztTQUNqQztRQUNELElBQUksS0FBSyxJQUFJLEdBQUcsRUFBRTtZQUNoQixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztTQUN0QjthQUFNLElBQUksS0FBSyxJQUFJLEdBQUcsSUFBSSxLQUFLLEdBQUcsR0FBRyxFQUFFO1lBQ3RDLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1NBQ3RCO2FBQU0sSUFBSSxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssR0FBRyxJQUFJLEVBQUU7WUFDeEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7U0FDdEI7YUFBTSxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBRUQsZUFBZSxDQUFDLElBQUksRUFBRSxVQUFVO1FBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFLLENBQUM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUMxQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksUUFBUSxFQUFFO1lBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3RDO2FBQ0c7WUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUNyQyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7WUFDckQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztZQUMvRCxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzNDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUNwQixZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjtJQUNILENBQUM7SUFDRCxhQUFhLENBQUMsVUFBVSxFQUFFLE9BQU87UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNuRCxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztZQUNuQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDdEIsQ0FBQyxDQUFDO2dCQUNFLEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxDQUFDO2FBQ1YsQ0FBQztRQUNOLHlCQUF5QjtRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLG1CQUFtQixFQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQTtRQUMvQyxJQUFJLGFBQWEsRUFBRSxjQUFjLEVBQUUsTUFBTSxDQUFDO1FBQzFDLElBQUksV0FBVyxHQUFHLEVBQUUsRUFDbEIsVUFBVSxDQUFDO1FBQ2IsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3hFLElBQUksYUFBYSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztZQUNyRCxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7WUFDL0MsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUN2RSxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixVQUFVLHFCQUFRLFVBQVUsRUFBSyxVQUFVLENBQUUsQ0FBQztZQUM5QyxjQUFjLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztTQUM1QzthQUFNO1lBQ0wsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQztZQUN4QyxhQUFhLEdBQUcsV0FBVztnQkFDekIsQ0FBQyxDQUFDLFdBQVc7Z0JBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztZQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ3ZFLGNBQWMsR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO1lBQzNDLE1BQU0sR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQy9CO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUMsMEJBQTBCLENBQUMsQ0FBQTtRQUV0RCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxJQUFJO1lBQ3RDLElBQUksUUFBUSxDQUFDO1lBQ2IsSUFDRSxJQUFJLENBQUMsTUFBTTtnQkFDWCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDdkM7Z0JBQ0EsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN0QixJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7b0JBQ2QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFDakQsUUFBUTt3QkFFUixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN0QixDQUFDLENBQUMsQ0FBQztvQkFDSCxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDakM7cUJBQU07b0JBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDcEQ7YUFDRjtpQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDdkIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNwQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZTt3QkFDM0IsQ0FBQyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzt3QkFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7b0JBQ2YsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDOUQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUM3QixRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDM0Q7cUJBQU07b0JBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNyRTthQUNGO1lBQ0QsZ0NBQWdDO1lBQ2hDLDJFQUEyRTtZQUMzRSw0Q0FBNEM7WUFDNUMsSUFBSTtZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsUUFBUSxDQUFDLENBQUM7WUFFM0MsSUFBSSxRQUFRO2dCQUNWLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7b0JBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztvQkFDMUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztRQUVILElBQUksYUFBYSxDQUFDLGNBQWMsRUFBRTtZQUNoQywyQkFBMkI7WUFDM0IsZ0JBQWdCO1lBQ2hCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztZQUN0QixPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixFQUFFLFdBQVcsQ0FBQyxDQUFDO1lBQ3ZELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUE7WUFDcEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLE1BQU07Z0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLElBQUksbUJBQW1CLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQztnQkFDckQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDOUQsQ0FBQyxHQUFHLEVBQUUsRUFBRTtvQkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7b0JBQ0YsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTt3QkFDckIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO3dCQUM3Qyw2QkFBNkI7d0JBQzdCLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQ3JEO2dCQUNILENBQUMsRUFDRCxDQUFDLEtBQUssRUFBRSxFQUFFO29CQUNSLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7Z0JBQ0osQ0FBQyxDQUNGLENBQUM7WUFDSixDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07WUFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFBO1lBQ3JCLElBQUksQ0FBQyxjQUFjO2lCQUNoQixpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN0QyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDbEIsSUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVU7b0JBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFDckM7b0JBQ0EsSUFBSSxtQkFBbUIsR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDO29CQUNyRCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7b0JBQ0YsNkJBQTZCO2lCQUM5QjtxQkFBTTtvQkFDTCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN2RCxhQUFhLENBQUMsZ0JBQWdCLENBQy9CLENBQUM7b0JBQ0YsSUFBSSxDQUFDLGtCQUFrQixDQUNyQixJQUFJLEVBQ0osUUFBUSxHQUFHLFVBQVUsQ0FBQyxRQUFRLEVBQzlCLFVBQVUsQ0FBQyxhQUFhLENBQ3pCLENBQUM7aUJBQ0g7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQztJQUNELGtCQUFrQixDQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUUsYUFBYTtRQUNyRCxNQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7UUFDOUQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELGVBQWUsQ0FBQyxRQUFRO1FBQ3RCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBRTlELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRTtZQUM1QyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1lBQ3JFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNoQjthQUFJO1lBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ2hCO0lBRUwsQ0FBQztJQUNELGNBQWMsQ0FBQyxDQUFDO1FBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLHVCQUF1QixDQUFDLENBQUM7UUFFOUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLEtBQUssRUFBQyxDQUFDLENBQUE7UUFDakUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQztRQUN6QyxpREFBaUQ7UUFDakQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7UUFDNUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQTtRQUNGLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFHN0IseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7UUFDckUsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1FBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsb0JBQW9CLENBQUMsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDL0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUMzQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1FBQ25FLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBQyxJQUFJLENBQUMsQ0FBQTtRQUVoQyxJQUFJO1FBQ04sb0JBQW9CO1FBQ3BCLHlCQUF5QjtRQUV6QixVQUFVO1FBQ1YsZ0RBQWdEO0lBQ2xELENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxLQUFLO1FBQ3ZCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0NBQWdDLEVBQUMsS0FBSyxDQUFDLENBQUM7UUFDcEQsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBQzlDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxJQUFJO1lBQzNDLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMzQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQzthQUN0QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQzthQUN2QjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsWUFBWSxDQUFDLFVBQVUsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBQ2hELFlBQVksQ0FBQyxPQUFPLENBQ2xCLHNCQUFzQixFQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUN2QyxDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBR0QsVUFBVSxDQUFDLENBQUM7UUFDVixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztRQUNsQyxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDL0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDUCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxJQUFJLEdBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQTtRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztRQUNqQyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztRQUN2QyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztRQUMvQyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQztRQUNqRCxJQUFJLEtBQUssR0FBRyxFQUFFLENBQUM7UUFDZixXQUFXO1FBQ1gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUMsS0FBSyxDQUFDLENBQUE7UUFDaEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO1lBQzFDLElBQUksUUFBUSxHQUFHLFdBQVcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUNoRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQztnQkFDdkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTtnQkFDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTtvQkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztvQkFDbkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7YUFDdkI7aUJBQU0sSUFBSSxRQUFRLEVBQUU7Z0JBQ25CLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGVBQWU7b0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztvQkFDMUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQzthQUNkO1lBQ0wsMENBQTBDO1lBQzFDLCtCQUErQjtZQUMvQixpREFBaUQ7WUFFakQsS0FBSztZQUNMLFNBQVM7WUFDVCwwREFBMEQ7WUFDMUQsd0RBQXdEO1lBQ3hELHlDQUF5QztZQUN6QyxJQUFJO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDbEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUMsSUFBSSxDQUFDLENBQUE7WUFDaEMsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQWEsQ0FBQztZQUMxRCxJQUFJLFlBQVksR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDakUsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQkFDbEIsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO2dCQUNsQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxJQUFJO29CQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztnQkFDM0MsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUM7YUFDdEI7WUFDRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDO1lBQzlDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQzNDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxXQUFXLENBQUM7WUFDaEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO1lBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFBO1lBQzdCLFlBQVk7WUFDWixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztRQUNQLE1BQU07SUFDTixDQUFDOzs7WUF2Y0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLGc1VEFBZ0Q7O2FBRWpEOzs7O1lBdkJDLFdBQVc7WUFXSixTQUFTO1lBSFQsNEJBQTRCO1lBSTVCLGVBQWU7WUFDZixhQUFhO1lBQ2IsY0FBYztZQUNkLGNBQWM7WUFUZCxZQUFZOzRDQWdFaEIsTUFBTSxTQUFDLFNBQVM7Ozt5QkE3Q2xCLEtBQUsiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBNb2RlbExheW91dENvbXBvbmVudCB9IGZyb20gXCIuLy4uL21vZGVsLWxheW91dC9tb2RlbC1sYXlvdXQuY29tcG9uZW50XCI7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCwgSW5qZWN0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtcclxuICBGb3JtQnVpbGRlcixcclxuICBGb3JtR3JvdXAsXHJcbiAgVmFsaWRhdG9ycyxcclxuICBGb3JtQ29udHJvbFxyXG59IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2dcIjtcclxuXHJcbmltcG9ydCB7IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UgfSBmcm9tIFwiLi4vQGZ1c2Uvc2VydmljZXMvdHJhbnNsYXRpb24tbG9hZGVyLnNlcnZpY2VcIjtcclxuLy8gaW1wb3J0IHsgbG9jYWxlIGFzIGVuIH0gZnJvbSBcIi4uL2kxOG4vZW5cIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbFwiO1xyXG5pbXBvcnQgeyBTbmFja0JhclNlcnZpY2UgfSBmcm9tIFwiLi8uLi9zaGFyZWQvc25hY2tiYXIuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL2xvYWRlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSBcIi4uL2NvbnRlbnQvY29udGVudC5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL19zZXJ2aWNlcy9pbmRleFwiO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tIFwicnhqcy9vcGVyYXRvcnNcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImdyaWQtbGlzdC1sYXlvdXRcIixcclxuICB0ZW1wbGF0ZVVybDogXCIuL2dyaWQtbGlzdC1sYXlvdXQuY29tcG9uZW50Lmh0bWxcIixcclxuICBzdHlsZVVybHM6IFtcIi4vZ3JpZC1saXN0LWxheW91dC5jb21wb25lbnQuc2Nzc1wiXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEdyaWRMaXN0TGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICBASW5wdXQoKSBmb3JtVmFsdWVzOiBhbnk7XHJcbiAgY3VycmVudENvbmZpZ0RhdGE6IGFueTtcclxuICBub09mQ29sdW1uczogbnVtYmVyO1xyXG4gIGN1cnJlbnREYXRhOiBhbnk7XHJcbiAgZ3JpZExpc3REYXRhOiBhbnk7XHJcbiAgZ2V0RGF0YXM6IGFueTtcclxuICBidXR0b25zQ29uZmlnRGF0YTogYW55ID0ge307XHJcbiAgZGlhbG9nUmVmOiBhbnk7XHJcbiAgaGVhZGVyVGV4dDogYW55O1xyXG4gIG1vZGFsSW5kZXg6IGFueTtcclxuICByZXNwb25zaXZlID0gdHJ1ZTtcclxuICBzaG93ID0gZmFsc2U7XHJcbiAgc2VsZWN0ZWRWYWx1ZTogYW55ID0ge307XHJcbiAgQ3JlYXRlRm9ybTogRm9ybUdyb3VwO1xyXG4gIGN1cnJlbnRSZXBvcnRUeXBlOiBhbnk7XHJcbiAgZGF0YTogYW55XHJcbiAgZm9ybURhdGFnZXQ6IGFueTtcclxuICB0YWJsZVNob3c6IGJvb2xlYW47XHJcbiAgZ2V0dGFibGV2YWw6YW55O1xyXG4gIHN1Ym1pdHRlZDogYm9vbGVhbjtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIGlucHV0RGF0YTogYW55O1xyXG4gIHRhYmxlTGlzdCA6IGFueTtcclxuICBjdXJyZW50VGFibGVMb2FkIDogYW55O1xyXG4gIHByaXZhdGUgdW5zdWJzY3JpYmUgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xyXG4gIHVzZXJEYXRhOiBhbnk7XHJcbiAgY2hpcFZhbGlkaXRhaW9uOiBib29sZWFuO1xyXG4gIENoaXBMaW1pdDogYW55O1xyXG4gIENoaXBPcGVyYXRvcjogYW55O1xyXG4gIHNlbGVjdGVkQ2hpcDogYW55ID0ge307XHJcbiAgaW5wdXRHcm91cDogRm9ybUdyb3VwO1xyXG4gIGRlZmF1bHREYXRhc291cmNlTmFtZTogYW55O1xyXG4gIGVuYWJsZUZvcm1MYXlvdXQgOiBib29sZWFuID0gZmFsc2U7XHJcbiAgc2VsZWN0ZWRSZXBvcnRUeXBlIDogYW55O1xyXG4gIHRhYmxlSGVhZGVyQ29uZmlnOiBhbnkgPSBbXTtcclxuICBzZWxlY3RlZFRhYmxlSGVhZGVyOiBhbnkgPSBbXTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBfbWF0RGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIF9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlOiBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgY29udGVudFNlcnZpY2U6IENvbnRlbnRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBwdWJsaWMgbWF0RGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8TW9kZWxMYXlvdXRDb21wb25lbnQ+LFxyXG4gICAgQEluamVjdChcImVuZ2xpc2hcIikgcHJpdmF0ZSBlblxyXG4gICkge1xyXG4gICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VOYW1lXCIpO1xyXG4gICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZVwiKTtcclxuXHJcbiAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmxvYWRUcmFuc2xhdGlvbnMoZW4pO1xyXG4gICAgdGhpcy5DcmVhdGVGb3JtID0gbmV3IEZvcm1Hcm91cCh7XHJcbiAgICAgIHNlbGVjdFJlcG9ydDogbmV3IEZvcm1Db250cm9sKClcclxuICAgIH0pO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZVxyXG4gICAgLmdldE1lc3NhZ2UoKVxyXG4gICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmUpKVxyXG4gICAgLnN1YnNjcmliZSgobWVzc2FnZSkgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhtZXNzYWdlLCBcIi4uLi5ncmlkTUVTU0FHRVwiKTtcclxuICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgICApO1xyXG4gICAgICBjb25zb2xlLmxvZyhtZXNzYWdlLCBcIi4uLi5ncmlkY3VycmVudENvbmZpZ0RhdGFcIik7XHJcblxyXG4gICAgICAvLyB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICAgIC8vIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSBmYWxzZTtcclxuICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgfSk7XHJcbiAgfVxyXG5cclxuICBmb3JtRGF0YTogYW55O1xyXG4gIHJlY2lwaWVudDogYW55ID0gW107XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgbGV0IGluaXRpYWxPYmogPSB7XHJcbiAgICAgIGRhdGFzb3VyY2VJZDogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSxcclxuICAgICAgZGF0YXNvdXJjZU5hbWU6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lLFxyXG4gICAgICBsaW1pdDogMTAsXHJcbiAgICAgIG9mZnNldDogMCxcclxuICAgIH07XHJcbiAgICB0aGlzLnVzZXJEYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRMb2dpblVzZXJcIikpO1xyXG4gICAgaWYgKHRoaXMudXNlckRhdGEpIHtcclxuICAgICAgdGhpcy5yZWNpcGllbnQucHVzaCh0aGlzLnVzZXJEYXRhLmVtYWlsKTtcclxuICAgIH1cclxuICAgIHRoaXMuYnV0dG9uc0NvbmZpZ0RhdGEgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImJ1dHRvbkNvbmZpZ1wiKVxyXG4gICAgICA/IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJidXR0b25Db25maWdcIikpXHJcbiAgICAgIDoge307XHJcbiAgICB0aGlzLmZvcm1EYXRhID0gdGhpcy5kYXRhO1xyXG4gICAgbGV0IHRlbXBEYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcERhdGEpXHJcbiAgICAgID8gSlNPTi5wYXJzZSh0ZW1wRGF0YSlcclxuICAgICAgOiB7XHJcbiAgICAgICAgICBkYXRhc291cmNlSWQ6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2UsXHJcbiAgICAgICAgICBkYXRhc291cmNlTmFtZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZU5hbWUsXHJcbiAgICAgICAgICBsaW1pdDogMTAsXHJcbiAgICAgICAgICBvZmZzZXQ6IDAsXHJcbiAgICAgICAgfTtcclxuICAgIC8vIHRoaXMuaW5wdXREYXRhID0geyAuLi50aGlzLmlucHV0RGF0YSwgLi4uaW5pdGlhbE9iaiB9O1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuZm9ybVZhbHVlcywgXCIuLi4uLmdyaWQgdGhpcy5pbnB1dERhdGFcIik7XHJcblxyXG4gICAgLy8gdGhpcy5pbnB1dERhdGFbXCJkYXRhc291cmNlSWRcIl0gPSB0aGlzLmRlZmF1bHREYXRhc291cmNlO1xyXG4gICAgdGhpcy5idXR0b25zQ29uZmlnRGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiYnV0dG9uQ29uZmlnXCIpXHJcbiAgICAgID8gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImJ1dHRvbkNvbmZpZ1wiKSlcclxuICAgICAgOiB7fTtcclxuICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRDb25maWdEYXRhXCIpXHJcbiAgICApO1xyXG4gICAgdGhpcy5jdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXTtcclxuICAgIHRoaXMubm9PZkNvbHVtbnMgPSB0aGlzLmN1cnJlbnREYXRhLm51bWJlck9mQ29sdW1uc0luUm93O1xyXG4gICAgdGhpcy5ncmlkTGlzdERhdGEgPSB0aGlzLmN1cnJlbnREYXRhLmdyaWREYXRhO1xyXG4gICAgdGhpcy5zZWxlY3RlZFJlcG9ydFR5cGUgPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkUmVwb3J0VHlwZVwiXTtcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKXtcclxuICAgICAgaWYoc2VsZi5zZWxlY3RlZFJlcG9ydFR5cGUpe1xyXG4gICAgICAgIHNlbGYuZW5hYmxlRm9ybUxheW91dCA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH0sIDEwMClcclxuXHJcbiAgfVxyXG4gIG9uQnV0dG9uQ2xpY2soYnV0dG9uSWQsIGl0ZW1EYXRhKSB7XHJcblxyXG4gICAgbGV0IG1vZGVsRGF0YTtcclxuICAgIGxldCBtb2RlbFdpZHRoO1xyXG4gICAgaWYgKGJ1dHRvbklkLmlkID09IFwicnVuXCIpIHtcclxuICAgICAgbW9kZWxEYXRhID0gXy5maW5kKGl0ZW1EYXRhLm1vZGVsRGF0YSwgXCJpc1J1blwiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIG1vZGVsRGF0YSA9IF8uZmluZChpdGVtRGF0YS5tb2RlbERhdGEsIFwiaXNTY2hlZHVsZVwiKTtcclxuICAgIH1cclxuICAgIG1vZGVsV2lkdGggPSBtb2RlbERhdGEuc2l6ZTtcclxuICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nXHJcbiAgICAgIC5vcGVuKE1vZGVsTGF5b3V0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIGFjdGlvbjogXCJleHBvcnRWaWV3XCIsXHJcbiAgICAgICAgICBtb2RlbERhdGE6IG1vZGVsRGF0YSxcclxuICAgICAgICB9LFxyXG4gICAgICB9KVxyXG4gICAgICAuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIC8vIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICB9KTtcclxuICB9XHJcbiAgb25SZXNpemUoZXZlbnQpIHtcclxuICAgIHZhciB3aWR0aDtcclxuICAgIGlmIChldmVudCA9PT0gMCkge1xyXG4gICAgICB3aWR0aCA9IHdpbmRvdy5pbm5lcldpZHRoO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgd2lkdGggPSBldmVudC50YXJnZXQuaW5uZXJXaWR0aDtcclxuICAgIH1cclxuICAgIGlmICh3aWR0aCA8PSA2MjQpIHtcclxuICAgICAgdGhpcy5ub09mQ29sdW1ucyA9IDE7XHJcbiAgICB9IGVsc2UgaWYgKHdpZHRoIDw9IDkwMCAmJiB3aWR0aCA+IDc1MCkge1xyXG4gICAgICB0aGlzLm5vT2ZDb2x1bW5zID0gMjtcclxuICAgIH0gZWxzZSBpZiAod2lkdGggPD0gMTUwMCAmJiB3aWR0aCA+IDEwMDApIHtcclxuICAgICAgdGhpcy5ub09mQ29sdW1ucyA9IDM7XHJcbiAgICB9IGVsc2UgaWYgKHdpZHRoID49IDE1MDApIHtcclxuICAgICAgdGhpcy5ub09mQ29sdW1ucyA9IDQ7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBidXR0b25MaXN0Q2xpY2soZGF0YSwgZm9ybVZhbHVlcykge1xyXG4gICAgY29uc29sZS5sb2coZGF0YSwgXCIuLi4uLkRBVEFcIik7XHJcbiAgICB0aGlzLmVuYWJsZUZvcm1MYXlvdXQgPSBmYWxzZTtcclxuICAgIGNvbnNvbGUubG9nKGZvcm1WYWx1ZXMsIFwiLi4uLmZvcm1WYWx1ZXNcIik7XHJcbiAgICBpZiAoZGF0YS5hY3Rpb24gPT0gXCJleHBvcnRcIikge1xyXG4gICAgICB0aGlzLm9uRXhwb3J0Q2xpY2soZGF0YSwgZm9ybVZhbHVlcyk7XHJcbiAgICB9IFxyXG4gICAgZWxzZXtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdID0ge307XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRWYWx1ZVwiXSA9IHRoaXMuc2VsZWN0ZWRWYWx1ZTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZFJlcG9ydFR5cGVcIl0gPSB0aGlzLnNlbGVjdGVkUmVwb3J0VHlwZTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdID0ge307XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgc2VsZi50YWJsZUxpc3QgPSBbXTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gICAgfVxyXG4gIH1cclxuICBvbkV4cG9ydENsaWNrKGJ1dHRvbkRhdGEsIHJlcXVlc3QpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+Pj4+b25FeHBvcnRDbGljayBcIiwgcmVxdWVzdCk7XHJcbiAgICBsZXQgdGVtcERhdGEgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wRGF0YSlcclxuICAgICAgPyBKU09OLnBhcnNlKHRlbXBEYXRhKVxyXG4gICAgICA6IHtcclxuICAgICAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgICAgIG9mZnNldDogMCxcclxuICAgICAgICB9O1xyXG4gICAgLy8gdGhpcy5pbnB1dERhdGE9cmVxdWVzdFxyXG4gICAgY29uc29sZS5sb2coXCJyZXF1ZXN0RGV0YWlscz4+PlwiLHRoaXMuaW5wdXREYXRhKVxyXG4gICAgbGV0IGV4cG9ydFJlcXVlc3QsIHJlcXVlc3REZXRhaWxzLCBhcGlVcmw7XHJcbiAgICBsZXQgcmVxdWVzdERhdGEgPSB7fSxcclxuICAgICAgcmVwb3J0VGVtcDtcclxuICAgIGxldCBkeW5hbWljUmVwb3J0ID0gcmVxdWVzdC5pc2R5YW1pY0RhdGEgPyByZXF1ZXN0LmlzZHlhbWljRGF0YSA6IGZhbHNlO1xyXG4gICAgaWYgKGR5bmFtaWNSZXBvcnQpIHtcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGEuZXhwb3J0RGF0YTtcclxuICAgICAgZXhwb3J0UmVxdWVzdCA9IHRoaXMuY3VycmVudERhdGEuZXhwb3J0UmVxdWVzdDtcclxuICAgICAgbGV0IFJlcG9ydERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiQ3VycmVudFJlcG9ydERhdGFcIikpO1xyXG4gICAgICBhcGlVcmwgPSBleHBvcnRSZXF1ZXN0LmFwaVVybDtcclxuICAgICAgcmVwb3J0VGVtcCA9IHsgLi4uUmVwb3J0RGF0YSwgLi4uYnV0dG9uRGF0YSB9O1xyXG4gICAgICByZXF1ZXN0RGV0YWlscyA9IGV4cG9ydFJlcXVlc3QuUmVxdWVzdERhdGE7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBsZXQgZm9ybVJlcXVlc3QgPSByZXF1ZXN0LmV4cG9ydFJlcXVlc3Q7XHJcbiAgICAgIGV4cG9ydFJlcXVlc3QgPSBmb3JtUmVxdWVzdFxyXG4gICAgICAgID8gZm9ybVJlcXVlc3RcclxuICAgICAgICA6IHRoaXMuZm9ybVZhbHVlcy5leHBvcnREYXRhLmV4cG9ydFJlcXVlc3Q7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wiZXhwb3J0VHlwZVwiXSA9IGJ1dHRvbkRhdGEgPyBidXR0b25EYXRhLmV4cG9ydFR5cGUgOiBcIlwiO1xyXG4gICAgICByZXF1ZXN0RGV0YWlscyA9IGV4cG9ydFJlcXVlc3QucmVxdWVzdERhdGE7XHJcbiAgICAgIGFwaVVybCA9IGV4cG9ydFJlcXVlc3QuYXBpVXJsO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2cocmVxdWVzdERldGFpbHMsXCI+Pj5ncmlka2trcmVxdWVzdERldGFpbHNcIilcclxuXHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGxldCB0ZW1wRGF0YTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIGl0ZW0uc3ViS2V5ICYmXHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV0gJiZcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgKSB7XHJcbiAgICAgICAgaWYgKGl0ZW0uY29udmVydFN0cmluZykge1xyXG4gICAgICAgICAgbGV0IHRlbXAgPSBbXTtcclxuICAgICAgICAgIF8uZm9yRWFjaChzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV0sIGZ1bmN0aW9uIChcclxuICAgICAgICAgICAgZGF0YUl0ZW1cclxuICAgICAgICAgICkge1xyXG4gICAgICAgICAgICB0ZW1wLnB1c2goZGF0YUl0ZW0pO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IEpTT04uc3RyaW5naWZ5KHRlbXApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSBpZiAoIWl0ZW0uc3ViS2V5KSB7XHJcbiAgICAgICAgaWYgKGl0ZW0ucmVwb3J0Q2hlY2spIHtcclxuICAgICAgICAgIHZhciB0MSA9IGl0ZW0uZnJvbVRyYW5zbGF0aW9uXHJcbiAgICAgICAgICAgID8gc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KGl0ZW0udmFsdWUpXHJcbiAgICAgICAgICAgIDogaXRlbS52YWx1ZTtcclxuICAgICAgICAgIHRlbXBEYXRhID0gaXRlbS5jb25kaXRpb25DaGVjayA/IHJlcG9ydFRlbXBbaXRlbS52YWx1ZV0gOiB0MTtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uZnJvbUxvZ2luRGF0YSkge1xyXG4gICAgICAgICAgdGVtcERhdGEgPSBzZWxmLnVzZXJEYXRhID8gc2VsZi51c2VyRGF0YVtpdGVtLnZhbHVlXSA6IFwiXCI7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRlbXBEYXRhID0gaXRlbS5pc0RlZmF1bHQgPyBpdGVtLnZhbHVlIDogc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKHNlbGYuZm9ybVZhbHVlcyk7XHJcbiAgICAgIC8vIGlmIChzZWxmLmZvcm1WYWx1ZXMuZ2V0VmFsdWVGcm9tQ29uZmlnICYmIHNlbGYuZm9ybVZhbHVlc1tpdGVtLnZhbHVlXSkge1xyXG4gICAgICAvLyAgIHRlbXBEYXRhID0gc2VsZi5mb3JtVmFsdWVzW2l0ZW0udmFsdWVdO1xyXG4gICAgICAvLyB9XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiY2hlY2tyZXF1ZXN0RGF0YSBcIiwgdGVtcERhdGEpO1xyXG5cclxuICAgICAgaWYgKHRlbXBEYXRhKVxyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSlcclxuICAgICAgICAgIDogdGVtcERhdGE7XHJcbiAgICB9KTtcclxuXHJcbiAgICBpZiAoZXhwb3J0UmVxdWVzdC5pc1JlcG9ydENyZWF0ZSkge1xyXG4gICAgICAvLyBmb3JtIGJhc2VkIHJlcG9ydCBjcmVhdGVcclxuICAgICAgLy8gRXhwb3J0IHJlcG9ydFxyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgICAgY29uc29sZS5sb2coXCI+PkZvcm0gcmVwb3J0IHJlcXVlc3REYXRhIFwiLCByZXF1ZXN0RGF0YSk7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjdXJyZW50SW5wdXQnLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpXHJcbiAgICAgIHRoaXMudmFsaWRhdGlvbkNoZWNrKGZ1bmN0aW9uIChyZXN1bHQpIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+IHJlc3VsdCBcIiwgcmVzdWx0KTtcclxuICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGV4cG9ydFJlcXVlc3QudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChyZXF1ZXN0RGF0YSwgYXBpVXJsKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzID09IDIwMCkge1xyXG4gICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiQ3VycmVudFJlcG9ydERhdGFcIik7XHJcbiAgICAgICAgICAgICAgLy8gc2VsZi5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICBzZWxmLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zb2xlLmxvZyhcInNlY29uZFwiKVxyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEV4cG9ydFJlc3BvbnNlKHJlcXVlc3REYXRhLCBhcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuZXhwb3J0RGF0YSAmJlxyXG4gICAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuZXhwb3J0RGF0YS5zM0Rvd25sb2FkXHJcbiAgICAgICAgICApIHtcclxuICAgICAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBleHBvcnRSZXF1ZXN0LnRvYXN0TWVzc2FnZTtcclxuICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgLy8gdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxldCBmaWxlTmFtZSA9IHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICBleHBvcnRSZXF1ZXN0LmRvd25sb2FkRmlsZU5hbWVcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgdGhpcy5kb3dubG9hZEV4cG9ydEZpbGUoXHJcbiAgICAgICAgICAgICAgZGF0YSxcclxuICAgICAgICAgICAgICBmaWxlTmFtZSArIGJ1dHRvbkRhdGEuZmlsZVR5cGUsXHJcbiAgICAgICAgICAgICAgYnV0dG9uRGF0YS5zZWxlY3Rpb25UeXBlXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGRvd25sb2FkRXhwb3J0RmlsZShyZXBvcnRWYWx1ZSwgZmlsZU5hbWUsIHNlbGVjdGlvblR5cGUpIHtcclxuICAgIGNvbnN0IGJsb2IgPSBuZXcgQmxvYihbcmVwb3J0VmFsdWVdLCB7IHR5cGU6IHNlbGVjdGlvblR5cGUgfSk7XHJcbiAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIGZpbGVOYW1lKTtcclxuICB9XHJcblxyXG4gIHZhbGlkYXRpb25DaGVjayhjYWxsYmFjaykge1xyXG4gICAgbGV0IHRlbXBEYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcblxyXG4gICAgICBpZiAodGhpcy5pbnB1dEdyb3VwICYmIHRoaXMuaW5wdXRHcm91cC52YWxpZCkge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGNhbGxiYWNrKGZhbHNlKVxyXG4gICAgICB9XHJcblxyXG4gIH1cclxuICBvblNlbGVjdHJlcG9ydChlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlLnZhbHVlLCBcInRlc3RzdCBoZXJoZXI+Pj4+Pj4+IFwiKTtcclxuICAgIFxyXG4gICAgdGhpcy5zZWxlY3RlZFZhbHVlID0gXy5maW5kKHRoaXMuZ3JpZExpc3REYXRhLCB7Z3JpZElkOiBlLnZhbHVlfSlcclxuICAgIHRoaXMuZGF0YSA9IHRoaXMuc2VsZWN0ZWRWYWx1ZS5tb2RlbERhdGE7XHJcbiAgICAvLyB0aGlzLmdyaWRMaXN0RGF0YSA9IHRoaXMuY3VycmVudERhdGEuZ3JpZERhdGE7XHJcbiAgICB0aGlzLmN1cnJlbnRSZXBvcnRUeXBlID0gdGhpcy5zZWxlY3RlZFZhbHVlO1xyXG4gICAgdGhpcy5jdXJyZW50UmVwb3J0VHlwZS5mb3JtRGF0YS5mb3JFYWNoKChkYXRhcykgPT4ge1xyXG4gICAgICB0aGlzLmdldERhdGFzID0gZGF0YXM7XHJcbiAgICB9KVxyXG4gICAgdGhpcy5lbmFibGVGb3JtTGF5b3V0ID0gdHJ1ZTtcclxuXHJcblxyXG4gICAgLy8gc3RhcnQgdGFibGUgaGVhZGVyRGF0YVxyXG4gICAgdGhpcy50YWJsZUhlYWRlckNvbmZpZyA9IHRoaXMuc2VsZWN0ZWRWYWx1ZS50YWJsZURhdGFbMF0udGFibGVIZWFkZXI7XHJcbiAgICBsZXQgdGVtcEFycmF5ID0gXy5maWx0ZXIodGhpcy50YWJsZUhlYWRlckNvbmZpZywgeyBpc0FjdGl2ZTogdHJ1ZSB9KTtcclxuICAgIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlciA9IF8ubWFwKHRlbXBBcnJheSwgXCJ2YWx1ZVwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwc1wiXSA9IHt9O1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkVmFsdWVcIl0gPSB0aGlzLnNlbGVjdGVkVmFsdWU7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRSZXBvcnRUeXBlXCJdID0gdGhpcy5zZWxlY3RlZFJlcG9ydFR5cGU7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwS2V5TGlzdFwiXSA9IHt9O1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICBzZWxmLnRhYmxlTGlzdCA9IFtdO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiU0VMRj4+Pj4+PlwiLHNlbGYpXHJcblxyXG4gICAgICAvLyB9XHJcbiAgICAvLyBlbmQgdGFibGUgaGVhZGVyIFxyXG4gICAgLy8gc2V0VGltZW91dChmdW5jdGlvbigpe1xyXG4gICAgICBcclxuICAgIC8vIH0sIDUwMClcclxuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuZ3JpZExpc3REYXRhLCBcImNoZWNrIGhlcmVcIik7XHJcbiAgfVxyXG4gIFxyXG4gIHVwZGF0ZVRhYmxlU2V0dGluZ3MoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+IHVwZGF0ZVRhYmxlU2V0dGluZ3MgZXZlbnQgXCIsZXZlbnQpO1xyXG4gICAgbGV0IHNlbGVjdGVkSGVhZGVyID0gdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyO1xyXG4gICAgdGhpcy50YWJsZUhlYWRlckNvbmZpZy5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmIChzZWxlY3RlZEhlYWRlci5pbmRleE9mKGl0ZW0udmFsdWUpID49IDApIHtcclxuICAgICAgICBpdGVtLmlzQWN0aXZlID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpdGVtLmlzQWN0aXZlID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKTtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFxyXG4gICAgICBcInNlbGVjdGVkVGFibGVIZWFkZXJzXCIsXHJcbiAgICAgIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVIZWFkZXJDb25maWcpXHJcbiAgICApO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kVGFibGVIZWFkZXJVcGRhdGUoXCJ1cGRhdGVcIik7XHJcbiAgfVxyXG5cclxuIFxyXG4gIHN1Ym1pdFZpZXcoZSkge1xyXG4gICAgdGhpcy50YWJsZVNob3cgPSBmYWxzZTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiQ2hlY2tra2s+Pj4+XCIpO1xyXG4gICAgbGV0IHRlbXBEYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkoSlNPTi5wYXJzZSh0ZW1wRGF0YSkpXHJcbiAgICAgID8gSlNPTi5wYXJzZSh0ZW1wRGF0YSlcclxuICAgICAgOiB7fTsgXHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICBsZXQgaXRlbT10aGlzLnNlbGVjdGVkVmFsdWVcclxuICAgIGNvbnNvbGUubG9nKGl0ZW0sIFwic2VsZmNoaXBsaW1pdFwiKTtcclxuICAgICAgbGV0IGFwaVVybCA9IGl0ZW0uc3VtYml0UmVxdWVzdC5hcGlVcmw7XHJcbiAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLnN1bWJpdFJlcXVlc3QucmVzcG9uc2U7XHJcbiAgICAgIGxldCByZXF1ZXN0RGF0YSA9IGl0ZW0uc3VtYml0UmVxdWVzdC5yZXF1ZXN0RGF0YTtcclxuICAgICAgbGV0IHF1ZXJ5ID0ge307XHJcbiAgICAgIC8vZGVidWdnZXI7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwicXVlcnk+Pj4+Pj5cIixxdWVyeSlcclxuICAgICAgXy5mb3JFYWNoKHJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICBsZXQgdGVtcERhdGEgPSByZXF1ZXN0SXRlbS5zdWJLZXkgJiYgc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkocmVxdWVzdEl0ZW0udmFsdWUpXHJcbiAgICAgICAgICAgICAgICA6IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRlbXBEYXRhKSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkodGVtcERhdGEpXHJcbiAgICAgICAgICAgICAgICA6IHRlbXBEYXRhO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coXCJTQVNBQXNhXCIscmVxdWVzdEl0ZW0ubmFtZSlcclxuICAgICAgICAvLyBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgLy8gICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICBcclxuICAgICAgICAvLyB9IFxyXG4gICAgICAgIC8vIGVsc2Uge1xyXG4gICAgICAgIC8vICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAvLyAgID8gSlNPTi5zdHJpbmdpZnkoc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdKVxyXG4gICAgICAgIC8vICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgLy8gfVxyXG4gICAgICB9KTtcclxuICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiU1NTU1NTPj4+Pj4+XCIsZGF0YSlcclxuICAgICAgICBsZXQgcmVzcG9uc2VWYWwgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgbGV0IHZhbGlkYXRlRGF0YSA9IHJlc3BvbnNlVmFsWzBdO1xyXG4gICAgICAgIGxldCBpc0FycmF5T2ZKU09OID0gXy5pc1BsYWluT2JqZWN0KHZhbGlkYXRlRGF0YSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgaWYgKCFpc0FycmF5T2ZKU09OKSB7XHJcbiAgICAgICAgICBjb25zdCB0ZW1wTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICBfLmZvckVhY2goaXRlbS5kYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goeyBuYW1lOiBpdGVtLCBfaWQ6IGl0ZW0gfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgaXRlbS5kYXRhID0gdGVtcExpc3Q7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNlbGYudGFibGVMaXN0ID0gc2VsZi5zZWxlY3RlZFZhbHVlLnRhYmxlRGF0YTtcclxuICAgICAgICBzZWxmLmN1cnJlbnRUYWJsZUxvYWQgPSBzZWxmLnNlbGVjdGVkVmFsdWU7XHJcbiAgICAgICAgc2VsZi5jdXJyZW50VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSByZXNwb25zZVZhbDtcclxuICAgICAgICBzZWxmLmN1cnJlbnRUYWJsZUxvYWRbXCJ0b3RhbFwiXSA9IGRhdGEucmVzcG9uc2UudG90YWw7XHJcbiAgICAgICAgY29uc29sZS5sb2coc2VsZiwgXCI+Pj4gdGhpc1wiKVxyXG4gICAgICAgIC8vIGRlYnVnZ2VyO1xyXG4gICAgICAgIHNlbGYudGFibGVTaG93ID0gdHJ1ZTtcclxuICAgICAgfSk7XHJcbiAgLy8gfSk7XHJcbiAgfVxyXG4gIC8vIHN1Ym1pdFZpZXcoZSl7XHJcblxyXG4gIC8vICAgY29uc29sZS5sb2codGhpcyxcIlRISVM+Pj4+Pj4+Pj5cIilcclxuICAvLyAgIHRoaXMudGFibGVMaXN0ID0gdGhpcy5zZWxlY3RlZFZhbHVlLnRhYmxlRGF0YTtcclxuICAvLyAgIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IHRoaXMuc2VsZWN0ZWRWYWx1ZTtcclxuICAvLyAgIHNlbGYuY3VycmVudFRhYmxlTG9hZCA9IHNlbGYuZm9ybVZhbHVlcztcclxuICAvLyAgICAgICAgICAgICAgIHNlbGYuY3VycmVudFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gc2VsZi50YWJsZUxpc3Q7XHJcbiAgLy8gICAgICAgICAgICAgICBzZWxmLmN1cnJlbnRUYWJsZUxvYWRbXCJ0b3RhbFwiXSA9IHJlc3VsdC5yZXNwb25zZS50b3RhbDtcclxuICAvLyAgIHRoaXMudGFibGVTaG93ID0gdHJ1ZTtcclxuICAvLyB9XHJcbn1cclxuIl19