import { SnackBarService } from "./../shared/snackbar.service";
import { AwsS3UploadService } from "./../shared/aws-s3-upload.service";
import { Component, Inject, ViewChild, ElementRef, Input, ChangeDetectorRef, } from "@angular/core";
import { MatAutocomplete, } from "@angular/material/autocomplete";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import { FormBuilder, FormControl, FormGroup, Validators, } from "@angular/forms";
import { ContentService } from "../content/content.service";
import * as _ from "lodash";
import { MessageService } from "../_services/index";
import * as FileSaver from "file-saver";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { MatDialogRef } from "@angular/material/dialog";
import * as moment_ from "moment";
import { LoaderService } from "../loader.service";
const moment = moment_;
export class FormLayoutComponent {
    constructor(_fuseTranslationLoaderService, contentService, formBuilder, messageService, changeDetector, matDialogRef, AwsS3UploadService, ref, snackBarService, loaderService, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.contentService = contentService;
        this.formBuilder = formBuilder;
        this.messageService = messageService;
        this.changeDetector = changeDetector;
        this.matDialogRef = matDialogRef;
        this.AwsS3UploadService = AwsS3UploadService;
        this.ref = ref;
        this.snackBarService = snackBarService;
        this.loaderService = loaderService;
        this.english = english;
        this.enableSelectOption = true;
        this.enableNewTableLayout = false;
        this.unsubscribe = new Subject();
        this.unsubscribeClick = new Subject();
        this.buttonsConfigData = {};
        this.enableSchedule = false;
        this.removable = true;
        this.selectedChip = {};
        this.panelOpenState = true;
        this.enableImportData = false;
        this.errorShow = {};
        this.enableSelectValueView = false;
        this.enableOptionFields = false;
        this.selectedChips = {};
        this.selectedChipKeyList = {};
        this.enableSelectable = true;
        this.enableRemovable = true;
        this.image = {};
        this.scheduleField = {};
        this.enableRepeatData = false;
        this.recipient = [];
        this.isNameAvailable = true;
        this.charlimitcheck = true;
        this.chipLength = 0;
        this.selectedchipLength = 0;
        this.dateFields = {};
        this.minDate = new Date();
        this.minEndDate = new Date();
        this.enabledateFields = false;
        this.dateFormatcheck = true;
        //for cron schedule expression by Raj
        this.secondsList = [];
        this.secondsLastDigits = [];
        this.hourListView = [];
        this.hourstwothree = [];
        this.days = [];
        this.month = [];
        this.monthsCount = [];
        this.dateCount = [];
        this.numOfdays = [];
        this.numOfmonths = [];
        this.cropExpressionSelection = {};
        this._fuseTranslationLoaderService.loadTranslations(english);
        this.defaultDatasource = localStorage.getItem("datasource");
        this.defaultDatasourceName = localStorage.getItem("datasourceName");
        this.realm = localStorage.getItem("realm");
        this.queryParams = {
            offset: 0,
            limit: 10,
            datasource: this.defaultDatasource,
            realm: this.realm
        };
        this.messageService.clickEventMessage
            .pipe(takeUntil(this.unsubscribeClick))
            .subscribe((data) => {
            console.log(this.formValues, ">>>>>>FORM VALUESSS");
            let tempData = localStorage.getItem("currentInput");
            console.log(tempData, ">>>>tempData");
            this.inputData = !_.isEmpty(JSON.parse(tempData))
                ? JSON.parse(tempData)
                : {};
            this.showData(this.inputData);
        });
        this.enableSelectValueView = false;
        this.messageService
            .getMessage()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((message) => {
            console.log(message, "....MESSAGE");
            this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            // if(this.currentConfigData && this.currentConfigData.listView &&  this.currentConfigData.listView.enableTableLayout){
            this.enableTableLayout = false;
            this.enableNewTableLayout = false;
            this.ngOnInit();
            // }
        });
        this.importControl = new ImportValue({});
        this.messageService.modelCloseMessage // whatif - new
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((data) => {
            console.log(data, ">>>>data Refresh from fromdata to tabledata");
            this.data = data;
            if (this.data) {
                this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
                this.ngOnInit();
            }
        });
    }
    ngOnInit() {
        this.getRulesWidth = null;
        this.hiddenFixedLayout = null;
        console.log(this.formValues, ".....formValues");
        console.log(this.importData, "importData????");
        this.selectedChips = {};
        this.selectedChipKeyList = {};
        //get values for cron expression dropdown
        this.viewOfseconds();
        let initialObj = {
            datasourceId: this.defaultDatasource,
            datasourceName: this.defaultDatasourceName,
            limit: 10,
            offset: 0,
        };
        this.userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        if (this.userData) {
            this.recipient.push(this.userData.email);
        }
        this.buttonsConfigData = localStorage.getItem("buttonConfig")
            ? JSON.parse(localStorage.getItem("buttonConfig"))
            : {};
        this.formData = this.data;
        let tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(tempData)
            ? JSON.parse(tempData)
            : {
                datasourceId: this.defaultDatasource,
                datasourceName: this.defaultDatasourceName,
                limit: 10,
                offset: 0,
            };
        this.inputData = Object.assign({}, this.inputData, initialObj);
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.currentData = this.fromRouting ? this.currentConfigData['pageRoutingView'] : this.currentConfigData["listView"];
        this.stepperData = this.currentData.createModelData;
        this.classDiv =
            this.currentData && this.currentData.beforeFromDivElementstyle
                ? this.currentData.beforeFromDivElementstyle
                : null;
        // debugger;
        this.initialHeight =
            this.currentData && this.currentData.height
                ? this.currentData.height
                : null;
        this.getRulesWidth =
            this.formValues && this.formValues.widthReduce
                ? this.formValues.widthReduce
                : null;
        this.hiddenFixedLayout =
            this.formValues && this.formValues.hiddenFixedLayout
                ? this.formValues.hiddenFixedLayout
                : null;
        this.dynamicFormHeight =
            this.formValues && this.formValues.dynamicFormHeight
                ? this.formValues.dynamicFormHeight
                : null;
        // this.hiddenFixedLayout = this.currentData.hiddenFixedLayout;
        console.log(this.classDiv);
        if (this.formValues) {
            this.enableTextFields = this.formValues.textFields ? true : false;
            this.enableSelectFields = this.formValues.selectFields ? true : false;
            this.enableOptionFields = this.formValues.chooseOneFields ? true : false;
            // this.enableChipFields = (this.chipListFields && this.chipListFields.length) ? true : false;
            console.log(this.formValues.chipFields, "this.formValues.chipFields");
            this.enableChipFields = this.formValues.chipFields ? true : false;
            this.enableTabs = this.formValues.tabData ? true : false;
            this.enableMultiselectFields = this.formValues.multiSelectFields
                ? true
                : false;
            this.enabledateFields = this.formValues.dateFields ? true : false;
        }
        this.enableTableLayout = false;
        this.enableNewTableLayout = false;
        this.submitted = false;
        this.viewData = {};
        let tempObj = {};
        if (this.importData) {
            this.enableImportData = true;
        }
        if (this.enableTextFields) {
            _.forEach(this.formValues.textFields, function (item) {
                if (item.inputType == "email") {
                    tempObj[item.name] = new FormControl("", Validators.email);
                }
                else {
                    tempObj[item.name] = new FormControl("");
                }
            });
        }
        if (this.enabledateFields) {
            var self = this;
            _.forEach(this.formValues.dateFields, function (item) {
                tempObj[item.name] = new FormControl(new Date());
            });
            // this.dateFields = this.formValues.dateFields;
        }
        if (this.formValues && this.formValues.autoCompleteFields) {
            _.forEach(this.formValues.autoCompleteFields, function (item) {
                tempObj[item.name] = new FormControl({
                    value: "",
                    disabled: item.disable,
                });
            });
        }
        if (this.enableSelectFields) {
            _.forEach(this.formValues.selectFields, function (item) {
                console.log(item);
                tempObj[item.name] = new FormControl("");
            });
        }
        if (this.enableMultiselectFields) {
            _.forEach(this.formValues.multiSelectFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        if (this.enableOptionFields) {
            // this.formValues.chooseOneFields.forEach(item => {
            //   tempObj[item.name] = new FormControl("");
            //   this.inputData[item.name] = item.fields[0].value;
            //   console.log(this.inputData, "///////");
            //   console.log(item, "///////item");
            // });
            var self = this;
            _.forEach(this.formValues.chooseOneFields, function (item) {
                tempObj[item.name] = new FormControl("");
                self.inputData[item.name] = item.fields[0].value;
                console.log(self.ChipLimit, "chipLimit>>>");
                console.log(self.inputData, "///////");
                console.log(item, "///////item");
            });
            if (this.formValues.isSetDefaultValue) {
                this.inputData[this.formValues.chooseOneFields[0].defaultKey] = this.formValues.chooseOneFields[0].defaultValue;
            }
        }
        console.log(this.enableChipFields, "enableChipFields");
        if (this.enableChipFields) {
            console.log(this.enableChipFields, "enableChipFields");
            var self = this;
            _.forEach(this.formValues.chipFields, function (item) {
                console.log(item, "selfchiplimit");
                console.log(item.chipCondition, "data>>>>>>>>>>>");
                tempObj[item.name] = new FormControl([""]);
                if (item.chipCondition) {
                    self.selectedChips[item.name] = [];
                    let temp = item.chipCondition.variable;
                    if (item.chipCondition.data &&
                        self.inputData &&
                        self.inputData[temp] &&
                        item.chipCondition.data[self.inputData[temp]]) {
                        self.ChipLimit =
                            item.chipCondition.data[self.inputData[temp]]["limit"];
                        self.ChipOperator =
                            item.chipCondition.data[self.inputData[temp]]["operator"];
                        self.ChipLimit =
                            item.chipCondition.data[self.inputData[temp]]["limit"];
                        self.ChipOperator =
                            item.chipCondition.data[self.inputData[temp]]["operator"];
                    }
                }
                if (item.onLoadFunction) {
                    // chipfields OnLoad function added when implement run reports
                    let apiUrl = item.onLoadFunction.apiUrl;
                    let responseName = item.onLoadFunction.response;
                    let requestData = item.onLoadFunction.requestData;
                    let query = { realm: self.realm };
                    //console.log(self.inputData, ">>>>>>>")
                    //debugger;
                    _.forEach(requestData, function (requestItem) {
                        if (requestItem.isDefault) {
                            query[requestItem.name] = requestItem.value;
                        }
                        else {
                            query[requestItem.name] = self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                        item.data = data.response[responseName];
                        let validateData = item.data[0];
                        let isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                        if (!isArrayOfJSON) {
                            const tempList = [];
                            if (item.onLoadFunction.subResponse) {
                                item.data = item.data[item.onLoadFunction.subResponse];
                                let keys = _.keys(item.data);
                                _.forEach(keys, function (itemKey) {
                                    tempList.push({
                                        name: item.data[itemKey][requestData["key"]],
                                        _id: itemKey,
                                    });
                                });
                            }
                            else {
                                _.forEach(item.data, function (item) {
                                    tempList.push({ name: item, _id: item });
                                });
                            }
                            item.data = tempList;
                        }
                    });
                }
                // self.ChipLimit[item.name] =
                //   item.chipCondition.data[
                //     self.inputData[]
                //   ];
            });
        }
        console.log(this.selectedChips, ">>>>>>>>>> selectedchips");
        // this.selectedChips = this.selectedChips;
        if (this.formValues && this.formValues.scheduleFields) {
            let item = this.formValues.scheduleFields;
            Object.keys(item).forEach(function (value) {
                tempObj[value] = new FormControl([""]);
            });
            // _.forEach(this.formValues.scheduleFields, function(item) {
            // tempObj[item.name] = new FormControl(['']);
            // tempObj[item.time] = new FormControl(['']);
            // tempObj[item.meridian] = new FormControl(['']);
            // tempObj[item.repeatType] = new FormControl(['']);
            // tempObj[item.endDate] = new FormControl(['']);
            // tempObj[item.endDateTime] = new FormControl([''])
            // })
            this.scheduleField = this.formValues.scheduleFields;
        }
        if (this.formValues && this.formValues.formArrayValues) {
            tempObj[this.formValues.formFieldName] = this.formBuilder.array([
                this.createCondition(),
            ]);
        }
        this.inputGroup = new FormGroup(tempObj);
        this.onLoad();
        if (this.enableTabs) {
            this.changeTab(0);
        }
        if (this.formValues && this.formValues.tableData) {
            this.tableList = this.inputData[this.formValues.tableKey];
            if (this.tableList && this.tableList.length) {
                this.currentTableLoad = this.formValues;
                this.currentTableLoad["response"] = this.tableList;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            else if (this.formValues.isToggleBased) {
                let tempObj = localStorage.getItem("selectedToggleDetail");
                let selectedToggleData = JSON.parse(tempObj);
                console.log(selectedToggleData, "....selectedToggleData");
                this.tableList = this.currentData.formData[0].tableData;
                let tempTable = _.filter(this.tableList, {
                    tableId: selectedToggleData.toggleId,
                });
                console.log(tempTable, "........................tempTable");
                this.formValues.tableData = tempTable;
                this.currentTableLoad = this.formValues;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            else if (this.formValues.isdyamicData) {
                // added additional report implementation
                // this.currentConfigData = JSON.parse(
                //   localStorage.getItem("currentConfigData")
                // );
                // this.currentData = this.currentConfigData["view"];
                let tempObj = localStorage.getItem("CurrentReportData");
                let Reportitem = JSON.parse(tempObj);
                console.log(Reportitem, "....CurrentReportData");
                var selectedReport = eval(this.formValues.condition);
                this.tableList = this.formValues.tableData;
                let tempTable = _.filter(this.tableList, {
                    tableId: selectedReport,
                });
                this.formValues.tableData = tempTable;
                // this.selectedmodelId = this.inputData._id;
                this.currentTableLoad = this.formValues;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            else if (this.formValues.enableTableLayout) {
                this.tableList = this.formValues.tableData;
                this.currentTableLoad = this.formValues;
                this.enableTableLayout = true;
                this.enableNewTableLayout = this.formValues.enableNewTableLayout
                    ? true
                    : false;
            }
            // let currentTabTable = this.formValues;
            // this.currentTableLoad = currentTabTable;
            // console.log(this.tableList, "<<<<<<<tableList");
        }
    }
    createCondition() {
        let formArrayValue = this.formValues.formArrayValues;
        let tempObj = {};
        if (formArrayValue.selectFields) {
            _.forEach(formArrayValue.selectFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        if (formArrayValue.autoCompleteFields) {
            _.forEach(formArrayValue.autoCompleteFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        return this.formBuilder.group(tempObj);
    }
    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribeClick.next();
    }
    addNewFormArray(formFieldName) {
        // this.inputData[formFieldName] = this.inputGroup.get(formFieldName) as FormArray;
        // if (this.inputData[formFieldName].controls.length == 2) {
        // 	this.isDisabled = true;
        // }
        // this.inputData[formFieldName].push(this.createCondition());
        let control = this.inputGroup.controls[formFieldName];
        control.push(this.createCondition());
    }
    onLoad() {
        var self = this;
        console.log(".>.... self.inputData ", this.inputData);
        if (this.enableSelectFields) {
            _.forEach(this.formValues.selectFields, function (item) {
                if (item.onLoadFunction) {
                    let apiUrl = item.onLoadFunction.apiUrl;
                    let responseName = item.onLoadFunction.response;
                    let requestData = item.onLoadFunction.requestData;
                    let query = { realm: self.realm };
                    _.forEach(requestData, function (requestItem) {
                        if (requestItem.isDefault) {
                            query[requestItem.name] = requestItem.value;
                        }
                        else {
                            query[requestItem.name] = self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                        // if(item.isFilter)
                        // {
                        //   let reponseData= data.response[responseName] as object[];
                        //   var tempData=reponseData.filter(function(val)
                        //   {
                        //     return val[item.filterKey]==item.filtervalue;
                        //   })
                        //   item.data=tempData;
                        // }
                        item.data = data.response[responseName];
                        // temp.selectedDataField = item.data[0].name;
                        // temp.dataId = item.data[0]._id;
                        // temp.getChipList(temp.dataId)
                        //console.log(">>>>>>>>>>>>>>>>> onLoad seletfield  ",item.name);
                        console.log(">>>>>> OnLoad data", data);
                        if (item.setDefaultDs) {
                            self.inputData[item.name] = self.inputData.datasourceName;
                            self.inputGroup.controls[item.name].setValue(self.inputData.datasourceId);
                        }
                        if (item.setDefaultRuleset && ((self.onLoadData && !self.onLoadData.savedData) || !self.onLoadData)) {
                            _.forEach(item.data, function (subitem) {
                                if (subitem.defaultRuleset) {
                                    let id = subitem._id;
                                    self.inputGroup.controls[item.name].setValue(id);
                                    self.inputData[item.name + "Id"] = subitem._id;
                                    self.inputData[item.name + "Name"] = subitem.name;
                                }
                            });
                        }
                        console.log(">>>>>>>>>>>> self.inputData OnLoad ", self.inputData);
                        localStorage.setItem("currentInput", JSON.stringify(self.inputData));
                    });
                }
            });
        }
        if (this.enableMultiselectFields) {
            _.forEach(this.formValues.multiSelectFields, function (item) {
                if (item.onLoadFunction) {
                    let apiUrl = item.onLoadFunction.apiUrl;
                    let responseName = item.onLoadFunction.response;
                    let query = {
                        realm: self.realm
                    };
                    self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                        item.data = data.response[responseName];
                    });
                }
            });
        }
        if (this.enableChipFields) {
            console.log(this.enableChipFields, "enableChipFields");
            var self = this;
            _.forEach(this.formValues.chipFields, function (item) {
                if (item.onLoadFunction) {
                    // chipfields OnLoad function added when implement run reports
                    let apiUrl = item.onLoadFunction.apiUrl;
                    let responseName = item.onLoadFunction.response;
                    let requestData = item.onLoadFunction.requestData;
                    let query = { realm: self.realm };
                    console.log(self.inputData, ">>>>>>>");
                    setTimeout(function () {
                        _.forEach(requestData, function (requestItem) {
                            if (requestItem.isDefault) {
                                query[requestItem.name] = requestItem.value;
                            }
                            else if (requestItem.subKey) {
                                if (requestItem.isMap && self.inputData[requestItem.value]) {
                                    // query[requestItem.name] = self.inputData[requestItem.value] ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey) : []
                                    // query[requestItem.name] = requestItem.isConvertToString ? JSON.stringify(query[requestItem.name]) : query[requestItem.name];
                                    let t1 = self.inputData[requestItem.value]
                                        ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey)
                                        : [];
                                    if (t1.length)
                                        query[requestItem.name] =
                                            requestItem.isConvertToString && typeof t1 !== "string"
                                                ? JSON.stringify(t1)
                                                : t1;
                                }
                                else if (self.inputData[requestItem.value]) {
                                    query[requestItem.name] = self.inputData[requestItem.value]
                                        ? self.inputData[requestItem.value][requestItem.subKey]
                                        : [];
                                    query[requestItem.name] = requestItem.isConvertToString
                                        ? JSON.stringify(query[requestItem.name])
                                        : query[requestItem.name];
                                }
                            }
                            else {
                                query[requestItem.name] = self.inputData[requestItem.value]
                                    ? self.inputData[requestItem.value]
                                    : [];
                            }
                        });
                        self.contentService
                            .getAllReponse(query, apiUrl)
                            .subscribe((data) => {
                            item.data = data.response[responseName];
                            let validateData = item.data[0];
                            let isArrayOfJSON = _.isPlainObject(validateData)
                                ? true
                                : false;
                            if (!isArrayOfJSON) {
                                const tempList = [];
                                if (item.onLoadFunction.subResponse) {
                                    item.data = item.data[item.onLoadFunction.subResponse];
                                    let keys = _.keys(item.data);
                                    _.forEach(keys, function (itemKey) {
                                        tempList.push({
                                            name: item.data[itemKey][requestData["key"]],
                                            _id: itemKey,
                                        });
                                    });
                                }
                                else {
                                    _.forEach(item.data, function (item) {
                                        tempList.push({ name: item, _id: item });
                                    });
                                }
                                item.data = tempList;
                            }
                        });
                    }, 2000);
                }
                // self.ChipLimit[item.name] =
                //   item.chipCondition.data[
                //     self.inputData[]
                //   ];
            });
        }
        if (this.onLoadData && this.onLoadData.savedData) {
            this.loadSavedData();
        }
    }
    // getOptionText(option) {
    //   // if (condition) {
    //     console.log(option,)
    //   // }
    //   return option.name;
    // }
    addChipItem(event) { }
    validateDate(type, event, dateField) {
        this.dateFormatcheck = true;
        if (event.value) {
            let disbalePreviousDate = dateField.validatewithnextdate ? true : false;
            if (disbalePreviousDate)
                this.minEndDate = event.value.add(1, "days")._d;
        }
        else {
            this.dateFormatcheck = false;
        }
    }
    onChipSearch(searchValue, selectedChip) {
        console.log(searchValue, "searchValue>>>>>>");
        var charLength = searchValue.length;
        let selectedIndex = this.formValues.chipFields.indexOf(selectedChip);
        this.inputData["searchValue"] = searchValue;
        if (selectedChip.onSearchFunction) {
            let apiUrl = selectedChip.onSearchFunction.apiUrl;
            let responseName = selectedChip.onSearchFunction.response;
            let query = { realm: this.realm };
            var self = this;
            var temp = {};
            _.forEach(selectedChip.onSearchFunction.requestData, function (requestItem) {
                if (requestItem.subKey) {
                    temp =
                        self.inputData[requestItem.value] &&
                            self.inputData[requestItem.value][requestItem.subKey]
                            ? self.inputData[requestItem.value][requestItem.subKey]
                            : "";
                    query[requestItem.name] = temp;
                }
                else if (requestItem.isDefault) {
                    query[requestItem.name] = requestItem.value;
                }
                else if (requestItem.alternativeKeyCheck) {
                    query[requestItem.name] = (self.inputData[requestItem.value] === undefined) ? self.inputData[requestItem.alternativeKey] : self.inputData[requestItem.value];
                }
                else {
                    query[requestItem.name] = self.inputData[requestItem.value];
                }
            });
            var limit = selectedChip.charlimit ? selectedChip.charlimit : 3;
            // console.log("Limit ", limit);
            // if (charLength >= limit || !searchValue) {
            this.charlimitcheck = true;
            this.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                this.formValues.chipFields[selectedIndex].data = data.response[responseName];
                let item = this.formValues.chipFields[selectedIndex];
                let validateData = item.data[0];
                let isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                if (!isArrayOfJSON) {
                    const tempList = [];
                    _.forEach(item.data, function (item) {
                        tempList.push({ name: item, _id: item });
                    });
                    item.data = tempList;
                }
            });
            // } else {
            //   console.log("Stop API ");
            //   this.charlimitcheck = false;
            // }
        }
    }
    onLoadChipChange(item) {
        if (item.onLoadFunction) {
            // chipfields OnLoad function added when implement run reports
            let apiUrl = item.onLoadFunction.apiUrl;
            let responseName = item.onLoadFunction.response;
            let requestData = item.onLoadFunction.requestData;
            let query = { realm: this.realm };
            let self = this;
            _.forEach(requestData, function (requestItem) {
                console.log(requestItem, ">>requestItem");
                if (requestItem.isDefault) {
                    query[requestItem.name] = requestItem.value;
                }
                else if (requestItem.subKey) {
                    if (requestItem.isMap && self.inputData[requestItem.value]) {
                        // query[requestItem.name] = self.inputData[requestItem.value] ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey) : []
                        // query[requestItem.name] = requestItem.isConvertToString ? JSON.stringify(query[requestItem.name]) : query[requestItem.name];
                        let t1 = self.inputData[requestItem.value]
                            ? _.map(self.inputData[requestItem.value][requestItem.subKey], requestItem.mapKey)
                            : [];
                        if (t1.length) {
                            query[requestItem.name] =
                                requestItem.isConvertToString && typeof t1 !== "string"
                                    ? JSON.stringify(t1)
                                    : t1;
                        }
                    }
                    else if (self.inputData[requestItem.value]) {
                        query[requestItem.name] = self.inputData[requestItem.value]
                            ? self.inputData[requestItem.value][requestItem.subKey]
                            : [];
                        query[requestItem.name] = requestItem.isConvertToString
                            ? JSON.stringify(query[requestItem.name])
                            : query[requestItem.name];
                    }
                }
                else {
                    query[requestItem.name] = self.inputData[requestItem.value];
                }
            });
            console.log(query, ">>>query");
            console.log(this, ">>> THIS");
            self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                item.data = data.response[responseName];
                self.inputData["selectedChips"][item.name] = item.data;
                self.inputData["selectedChipKeyList"][item.name] = _.map(item.data, item.onLoadFunction.keyToSave);
                console.log(self.inputData, ">>> INPUT");
                localStorage.setItem("currentInput", JSON.stringify(self.inputData));
            });
        }
    }
    onChipSelect(event, selectedChipId, keyToSave, chipError) {
        this.chipListOpenView = true;
        console.log(event, "Event>>>>>>>", this.ChipLimit, this.chipLength);
        console.log(selectedChipId, ">>selectedChipId");
        // console.log(chipError, "Event>>>>>>>");
        if (this.ChipLimit == this.chipLength) {
            this.chipInput.nativeElement.value = "";
            // this.snackBarService.warning(
            //   this._fuseTranslationLoaderService.instant(chipError)
            // );
        }
        else {
            this.chipValiditaion = false;
            this.selectedChips[selectedChipId] = this.selectedChips[selectedChipId]
                ? this.selectedChips[selectedChipId]
                : [];
            // if (this.selectedChips[selectedChipId].indexOf(event.option.value) < 0) {
            //   this.selectedChips[selectedChipId].push(event.option.value);
            // }
            let exitIndex = _.findIndex(this.selectedChips[selectedChipId], event.option.value);
            console.log('>> exitIndex ', exitIndex);
            if (exitIndex < 0) {
                this.selectedChips[selectedChipId].push(event.option.value);
            }
            console.log(this.formValues.chipFields, "<<<<<>>>>>");
            let selectedFormChip = _.find(this.formValues.chipFields, {
                name: selectedChipId,
            });
            console.log(selectedFormChip, ">>>selectedFormChip");
            console.log(this.inputData, "<<<<<inputData>>>>>");
            event["value"] = null;
            this.chipInput.nativeElement.value = "";
            this.selectedChipKeyList[selectedChipId] = _.map(this.selectedChips[selectedChipId], keyToSave);
            // this.onLoadChipChange();
            this.inputData["selectedChipKeyList"] = this.selectedChipKeyList;
            this.inputData["selectedChips"] = this.selectedChips;
            if (selectedFormChip && selectedFormChip["onChangeLoad"]) {
                let onLoadChipData = _.find(this.formValues.chipFields, {
                    name: selectedFormChip["loadchildKey"],
                });
                this.onLoadChipChange(onLoadChipData);
            }
            this.chipLength = this.inputData["selectedChips"][selectedChipId].length;
            this.selectedchipLength = this.selectedChips[selectedChipId].length;
            this.inputGroup.get(selectedChipId).setValue("");
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    }
    removeSelectedChip(value, selectedChipId, keyToSave) {
        console.log("removeSelectedChip ", this.inputData);
        let selectedIndex = this.selectedChips[selectedChipId].indexOf(value);
        if (selectedIndex >= 0) {
            this.selectedChips[selectedChipId].splice(selectedIndex, 1);
        }
        this.selectedChipKeyList[selectedChipId] = _.map(this.selectedChips[selectedChipId], keyToSave);
        this.chipLength = this.inputData["selectedChips"][selectedChipId].length;
        console.log(this.chipLength, ">>>>> SELECTED CHIP VALUE LENGTH");
        let selectedFormChip = _.find(this.formValues.chipFields, {
            name: selectedChipId,
        });
        console.log(selectedFormChip);
        if (this.chipLength) {
            console.log(selectedFormChip, ">>>selectedFormChip");
            if (selectedFormChip && selectedFormChip["onChangeLoad"]) {
                let onLoadChipData = _.find(this.formValues.chipFields, {
                    name: selectedFormChip["loadchildKey"],
                });
                this.onLoadChipChange(onLoadChipData);
            }
        }
        else {
            this.selectedChips[selectedFormChip["loadchildKey"]] = [];
        }
        this.inputData["selectedChipKeyList"] = this.selectedChipKeyList;
        this.inputData["selectedChips"] = this.selectedChips;
    }
    loadSavedData() {
        let savedData = this.onLoadData.savedData;
        let viewSavedData = this.currentConfigData[this.onLoadData.action]
            .savedDataDetails;
        var self = this;
        this.inputData["_id"] = savedData._id;
        _.forEach(viewSavedData, function (item) {
            if (item.subKey) {
                self.inputData[item.keyToShow] = self.inputData[item.keyToShow]
                    ? self.inputData[item.keyToShow]
                    : {};
                if (item.assignFirstIndexval) {
                    self.inputData[item.keyToShow] = (savedData[item.name] && savedData[item.name].length) ? savedData[item.name][0][item.subKey] : "";
                }
                else {
                    // self.inputData[item.keyToShow] = savedData[item.name][item.subKey];
                    self.inputData[item.keyToShow] =
                        savedData[item.name] && savedData[item.name][item.subKey]
                            ? savedData[item.name][item.subKey]
                            : "";
                }
            }
            else if (item.isCondition) {
                self.inputData[item.keyToShow] = eval(item.condition);
            }
            else {
                self.inputData[item.keyToShow] = savedData[item.name];
            }
            if (item.takeKeyList) {
                if (item.keyListName) {
                    self.inputData[item.keyListName] = self.inputData[item.keyListName]
                        ? self.inputData[item.keyListName]
                        : {};
                    self.inputData[item.keyListName][item.chipId] = [];
                    self.inputData[item.keyListName][item.chipId] = _.map(savedData[item.name], item.keyToSave);
                }
                self.inputData[item.keyToShow] = self.inputData[item.keyToShow] ? self.inputData[item.keyToShow] : {};
                self.inputData[item.keyToShow][item.chipId] = savedData[item.name];
                if (item.keyToShow == "selectedChips") {
                    self.selectedChips[item.chipId] = savedData[item.name];
                    self.inputData["selectedChips"] = self.selectedChips;
                }
                if (item.putKeyList) {
                    self.inputData[item.name] =
                        self.inputData[item.keyListName][item.chipId];
                }
            }
        });
        if (this.formValues && this.formValues.selectedDataKey) {
            let selectedDataKey = this.formValues.selectedDataKey;
            if (this.inputData[selectedDataKey] &&
                this.inputData[selectedDataKey].length) {
                this.selectedTableView =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues.tableData
                        : [];
                this.selectTableLoad =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues
                        : {};
                this.selectTableLoad["response"] = this.inputData[selectedDataKey];
                this.enableSelectValueView = true;
            }
        }
        else {
            if (this.inputData.selectedData && this.inputData.selectedData.length) {
                this.selectedTableView =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues.tableData
                        : [];
                this.selectTableLoad =
                    this.formValues && this.formValues.viewSelectedValues
                        ? this.formValues.viewSelectedValues
                        : {};
                this.selectTableLoad["response"] = this.inputData.selectedData;
                this.enableSelectValueView = true;
            }
        }
        if (this.formValues && this.formValues.showSelectedChips) {
            if (this.formValues && this.formValues.chipCondition) {
                let temp = this.formValues.chipCondition.variable;
                if (this.formValues.chipCondition.data &&
                    this.inputData &&
                    this.inputData[temp] &&
                    this.formValues.chipCondition.data[this.inputData[temp]]) {
                    this.ChipLimit = this.formValues.chipCondition.data[this.inputData[temp]]["limit"];
                    this.ChipOperator = this.formValues.chipCondition.data[this.inputData[temp]]["operator"];
                    this.inputData["chipOperator"] = this.ChipOperator;
                }
            }
        }
        this.viewData = this.inputData;
        // this.inputGroup.controls['ruleset'].setValue(this.inputData.ruleset);
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    }
    getChipList() {
        console.log(">>>>>>> entered chip fields ");
        if (this.enableChipFields) {
            var self = this;
            _.forEach(this.formValues.chipFields, function (item) {
                console.log("item ", item);
                if (item.onLoadFunction) {
                    let apiUrl = item.onLoadFunction.apiUrl;
                    let responseName = item.onLoadFunction.response;
                    let requestData = item.onLoadFunction.requestData;
                    let query = { realm: self.realm };
                    _.forEach(requestData, function (requestItem) {
                        if (requestItem.isDefault) {
                            query[requestItem.name] = requestItem.value;
                        }
                        else {
                            query[requestItem.name] = requestItem.isConvertToString
                                ? JSON.stringify(self.inputData[requestItem.value])
                                : self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                        item.data = data.response[responseName];
                        let validateData = item.data[0];
                        let isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                        if (!isArrayOfJSON) {
                            const tempList = [];
                            if (item.onLoadFunction.subResponse) {
                                item.data = item.data[item.onLoadFunction.subResponse];
                                let keys = _.keys(item.data);
                                _.forEach(keys, function (itemKey) {
                                    tempList.push({
                                        name: item.data[itemKey][requestData["key"]],
                                        _id: itemKey,
                                    });
                                });
                            }
                            else {
                                _.forEach(item.data, function (item) {
                                    tempList.push({ name: item, _id: item });
                                });
                            }
                            item.data = tempList;
                        }
                    });
                }
            });
        }
        else {
        }
    }
    removeTag(chip, id) {
        this.selectedChip[id] =
            this.selectedChip[id] && this.selectedChip[id].length
                ? this.selectedChip[id]
                : [];
        const index = this.selectedChip[id].indexOf(chip);
        if (index >= 0) {
            this.selectedChip[id].splice(index, 1);
        }
    }
    onButtonClick(data, formValues) {
        console.log(data, ".....DATA");
        console.log(formValues, "....formValues");
        if (data.action == "export") {
            this.onExportClick(data, formValues);
        }
        else if (data.action == "scheduleView") {
            this.inputData["reportName"] = this._fuseTranslationLoaderService.instant(formValues.name);
            this.inputData["reportType"] = formValues.reportType;
            if (!this.inputData["repeatType"])
                this.inputData["repeatType"] = "run_once";
            //  this.inputData["scheduleType"] = formValues.isSingle ? 3 : 2;
            let requestData = formValues.scheduleRequest
                ? formValues.scheduleRequest.requestData
                : [];
            var self = this;
            this.submitted = true;
            let toastMessageDetails = formValues.scheduleRequest.toastMessage;
            requestData["realm"] = this.realm;
            this.validateSchduleData(function (result) {
                if (result) {
                    self.constructScheduleData(requestData, function (returnData) {
                        console.log(">>>>>>>>>>>>.. returnData ", returnData);
                        if (returnData) {
                            self.contentService
                                .createRequest(returnData, formValues.scheduleRequest.apiUrl)
                                .subscribe((res) => {
                                console.log(" cretae Request success ", res);
                                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                                if (res.status == 201) {
                                    localStorage.removeItem("currentInput");
                                    self.matDialogRef.close();
                                    self.messageService.sendModelCloseEvent("listView");
                                }
                            }, (error) => {
                                console.log(" cretae Request Error ", error);
                                self.submitted = false;
                                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                            });
                        }
                    });
                }
            });
        }
        else if (data.action == "resetView") {
            this.ngOnInit();
        }
        else if (data.action == "cancel") {
            this.matDialogRef.close();
        }
        else if (data.action == 'import') {
            this.onImport();
        }
        else if (data.action == "submitView" || data.action == "checkConflict" || data.action == "update") {
            console.log(this.inputGroup, ">>>>>>>>>>>ss");
            if (this.formValues.getFormControlValue) {
                this.inputData[this.formValues.valueToSave] = this.inputGroup.value[this.formValues.formFieldName];
                if (this.formValues.isMergeObject) {
                    let obj = this.inputGroup.value[this.formValues.mapObjectName];
                    this.inputData = Object.assign({}, this.inputData, obj[0]);
                }
                console.log(this.inputData, "......INPUT DATA");
            }
            else if (this.formValues.getFromcurrentInput) {
                let tempData = JSON.parse(localStorage.getItem("currentInput"));
                this.inputData = Object.assign({}, this.inputData, tempData);
            }
            this.viewData = this.inputData;
            this.onSubmit();
        }
        else {
            this.enableTableLayout = false;
            this.enableNewTableLayout = false;
            console.log(data, "....DATA");
            console.log(formValues, "......FormValues");
            this.submitted = true;
            if (formValues.validateForm) {
                let self = this;
                this.validationCheck(function (result) {
                    console.log("result ><><><><><", result);
                    if (result) {
                        let conflictRequest = formValues.requestDetails;
                        let requestDetails = conflictRequest.requestData;
                        let apiUrl = conflictRequest.apiUrl;
                        let toastMessageDetails = conflictRequest.toastMessage;
                        let currentInputData = JSON.parse(localStorage.getItem("currentInput"));
                        let requestData = { realm: self.realm };
                        _.forEach(requestDetails, function (item) {
                            let tempData = item.subKey
                                ? currentInputData[item.value][item.subKey]
                                : currentInputData[item.value];
                            if (tempData) {
                                requestData[item.name] = item.convertToString
                                    ? JSON.stringify(tempData)
                                    : tempData;
                            }
                            else if (item.directAssign) {
                                requestData[item.name] = item.convertToString
                                    ? JSON.stringify(item.value)
                                    : item.value;
                            }
                        });
                        console.log(requestData, "....requestData");
                        self.loaderService.startLoader();
                        self.contentService.getAllReponse(requestData, apiUrl).subscribe((result) => {
                            self.loaderService.stopLoader();
                            console.log(result.response, "....data.response");
                            console.log(conflictRequest.responseName, ".....RESPONSE NAME");
                            let tempData = result.response[conflictRequest.responseName];
                            console.log(tempData, "....tempData");
                            self.tableList = tempData;
                            // let currentTabTable = this.formValues;
                            // this.currentTableLoad = currentTabTable;
                            self.currentTableLoad = self.formValues;
                            self.currentTableLoad["response"] = self.tableList;
                            self.currentTableLoad["total"] = result.response.total;
                            self.enableTableLayout = self.formValues.enableNewTableLayout
                                ? false
                                : true;
                            self.enableNewTableLayout = self.formValues.enableNewTableLayout
                                ? true
                                : false;
                            // self.enable = true;
                            self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        }, (err) => {
                            self.loaderService.stopLoader();
                            console.log("Err ", err);
                        });
                    }
                    else {
                    }
                });
            }
            else {
                let conflictRequest = formValues.requestDetails;
                let requestDetails = conflictRequest.requestData;
                let apiUrl = conflictRequest.apiUrl;
                let toastMessageDetails = conflictRequest.toastMessage;
                let currentInputData = JSON.parse(localStorage.getItem("currentInput"));
                let requestData = { realm: this.realm };
                _.forEach(requestDetails, function (item) {
                    let tempData = item.subKey
                        ? currentInputData[item.value][item.subKey]
                        : currentInputData[item.value];
                    if (tempData) {
                        requestData[item.name] = item.convertToString
                            ? JSON.stringify(tempData)
                            : tempData;
                    }
                    else if (item.directAssign) {
                        requestData[item.name] = item.convertToString
                            ? JSON.stringify(item.value)
                            : item.value;
                    }
                });
                console.log(requestData, "....requestData");
                this.loaderService.startLoader();
                this.contentService.getAllReponse(requestData, apiUrl).subscribe((result) => {
                    this.loaderService.stopLoader();
                    console.log(result.response, "....data.response");
                    console.log(conflictRequest.responseName, ".....RESPONSE NAME");
                    let tempData = result.response[conflictRequest.responseName];
                    console.log(tempData, "....tempData");
                    this.tableList = tempData;
                    // let currentTabTable = this.formValues;
                    // this.currentTableLoad = currentTabTable;
                    this.currentTableLoad = this.formValues;
                    this.currentTableLoad["response"] = this.tableList;
                    this.currentTableLoad["total"] = result.response.total;
                    this.enableTableLayout = true;
                    this.enableNewTableLayout = this.formValues.enableNewTableLayout
                        ? true
                        : false;
                    this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                }, (err) => {
                    this.loaderService.stopLoader();
                    console.log("Err ", err);
                });
            }
        }
    }
    validateSchduleData(callback) {
        this.submitted = true;
        let tempData = JSON.parse(localStorage.getItem("currentInput"));
        if (tempData && tempData.selectedData && tempData.selectedData.length) {
            this.inputData = Object.assign({}, this.inputData, tempData);
        }
        if (this.formValues.formusedForCheck) {
        }
        this.inputData["chipLimit"] = this.ChipLimit;
        this.inputData["chipOperator"] = this.ChipOperator;
        console.log(this.inputData, " Schedule inputDatainputData");
        console.log(this.inputGroup, "....... this.inputGroup ");
        // console.log(this.inputGroup.get("scheduleField") , "-- scheduleField");
        let dateField = this.inputGroup.get("startDate").value;
        // console.log(">>>>>>>>>>> dateField ",dateField);
        //  if(dateField==undefined){
        //   this.dateFormatcheck = false;
        //   this.inputGroup.get("meridian").invalid;
        //  }
        if (this.formValues.validateSelectedVal) {
            if (this.inputData &&
                this.inputData["selectedData"] &&
                this.inputData["selectedData"].length) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
            else {
                let item = this.formValues.scheduleFields;
                Object.keys(item).forEach(function (value) {
                    // tempObj[value] = new FormControl([""]);
                    // let val=this.inputGroup.value[value];
                    //  if(!val)
                    //  {
                    //   this.inputGroup.controls(item).setErrors({'incorrect': true});
                    //  }
                });
                let toastMessageDetails = this.formValues.validations.toastMessage;
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                callback(false);
            }
        }
        else {
            if (this.inputGroup && this.inputGroup.valid) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
        }
    }
    constructScheduleData(requestDetails, callback) {
        let date = moment(this.inputData.startDate).format("DD-MM-YYYY");
        let dateConcat = moment(date + " " + this.inputData.time + " " + this.inputData.meridian, "DD-MM-YYYY HH:mm a").format();
        this.inputData["dateTime"] = dateConcat;
        this.inputData["time"] = dateConcat;
        this.inputData["userRecipients"] = this.recipient;
        this.inputData["selectedDays"] = [];
        this.inputData["date"] = moment().format("DD-MMM-YYYY");
        let currentInputData = JSON.parse(localStorage.getItem("currentInput"));
        this.inputData = Object.assign({}, this.inputData, currentInputData);
        this.inputData = Object.assign({}, this.inputData, this.inputGroup.value);
        var self = this;
        let requestData = {};
        _.forEach(requestDetails, function (item) {
            let tempData;
            if (item.subKey &&
                self.inputData[item.value] &&
                self.inputData[item.value][item.subKey]) {
                tempData = self.inputData[item.value][item.subKey];
            }
            else if (!item.subKey) {
                if (item.isDefault) {
                    tempData = item.value;
                }
                else if (item.AssignFirstIndexval) {
                    tempData = self.inputData[item.value][0];
                }
                // else if(item.fromFormControl){
                //   tempData = self.inputGroup.value
                // }
                else {
                    tempData = self.inputData[item.value];
                }
            }
            if (tempData) {
                requestData[item.name] = item.convertToString
                    ? JSON.stringify(tempData)
                    : tempData;
            }
        });
        callback(requestData);
    }
    showRepeatData(val) {
        if (val.checked) {
            this.enableRepeatData = true;
            this.inputGroup.get("repeatType").setValue("daily");
            this.inputData["daily"] = true;
        }
        else {
            this.enableRepeatData = false;
            this.inputGroup.get("repeatType").setValue("");
            this.inputData["daily"] = false;
        }
    }
    getSelecteddays($event, element) {
        var index = this.inputData["selectedDays"].indexOf(element);
        if (index === -1) {
            this.inputData["selectedDays"].push(element);
        }
        else {
            this.inputData["selectedDays"].splice(index, 1);
        }
    }
    onRepeatSelectChange(value, data) {
        var self = this;
        _.forEach(data, function (item) {
            if (item.value == value) {
                self.inputData[value] = true;
            }
            else {
                self.inputData[item.value] = false;
            }
        });
    }
    onExportClick(buttonData, request) {
        console.log(">>>>>>>>>>>>onExportClick ", request);
        let exportRequest, requestDetails, apiUrl;
        let requestData = {}, reportTemp;
        let dynamicReport = request.isdyamicData ? request.isdyamicData : false;
        if (dynamicReport) {
            this.currentData = this.currentConfigData.exportData;
            exportRequest = this.currentData.exportRequest;
            let ReportData = JSON.parse(localStorage.getItem("CurrentReportData"));
            apiUrl = exportRequest.apiUrl;
            reportTemp = Object.assign({}, ReportData, buttonData);
            requestDetails = exportRequest.RequestData;
        }
        else {
            let formRequest = request.exportRequest;
            exportRequest = formRequest
                ? formRequest
                : this.formValues.exportData.exportRequest;
            this.inputData["exportType"] = buttonData.exportType;
            requestDetails = exportRequest.requestData;
            apiUrl = exportRequest.apiUrl;
        }
        var self = this;
        _.forEach(requestDetails, function (item) {
            let tempData;
            if (item.subKey &&
                self.inputData[item.value] &&
                self.inputData[item.value][item.subKey]) {
                if (item.convertString) {
                    let temp = [];
                    _.forEach(self.inputData[item.value][item.subKey], function (dataItem) {
                        temp.push(dataItem);
                    });
                    tempData = JSON.stringify(temp);
                }
                else {
                    tempData = self.inputData[item.value][item.subKey];
                }
            }
            else if (!item.subKey) {
                if (item.reportCheck) {
                    var t1 = item.fromTranslation
                        ? self._fuseTranslationLoaderService.instant(item.value)
                        : item.value;
                    tempData = item.conditionCheck ? reportTemp[item.value] : t1;
                }
                else if (item.fromLoginData) {
                    tempData = self.userData ? self.userData[item.value] : "";
                }
                else {
                    tempData = item.isDefault ? item.value : self.inputData[item.value];
                }
            }
            if (self.formValues.getValueFromConfig && self.formValues[item.value]) {
                tempData = self.formValues[item.value];
            }
            if (tempData)
                requestData[item.name] = item.convertToString
                    ? JSON.stringify(tempData)
                    : tempData;
        });
        if (exportRequest.isReportCreate) {
            // form based report create
            // Export report
            var self = this;
            this.submitted = true;
            console.log(">>Form report requestData ", requestData);
            this.validationCheck(function (result) {
                console.log(">>>>>>>> result ", result);
                let toastMessageDetails = exportRequest.toastMessage;
                self.contentService.createRequest(requestData, apiUrl).subscribe((res) => {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                    if (res.status == 200) {
                        localStorage.removeItem("CurrentReportData");
                        self.matDialogRef.close();
                        self.messageService.sendModelCloseEvent("listView");
                    }
                }, (error) => {
                    self.submitted = false;
                    self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                });
            });
        }
        else {
            this.contentService
                .getExportResponse(requestData, apiUrl)
                .subscribe((data) => {
                if (this.formValues.exportData &&
                    this.formValues.exportData.s3Download) {
                    let toastMessageDetails = exportRequest.toastMessage;
                    this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                    this.matDialogRef.close();
                }
                else {
                    let fileName = this._fuseTranslationLoaderService.instant(exportRequest.downloadFileName);
                    this.downloadExportFile(data, fileName + buttonData.fileType, buttonData.selectionType);
                }
            });
        }
    }
    downloadExportFile(reportValue, fileName, selectionType) {
        const blob = new Blob([reportValue], { type: selectionType });
        FileSaver.saveAs(blob, fileName);
    }
    onSelectionChanged(event, id) {
        this.selectedChip[id] =
            this.selectedChip[id] && this.selectedChip[id].length
                ? this.selectedChip[id]
                : [];
        let index = this.selectedChip[id].indexOf(event.option.value);
        if (index < 0) {
            this.selectedChip[id].push(event.option.value);
        }
        this.accessInput.nativeElement.value = "";
    }
    onSearchAccessControl(event) {
        const value = event.value;
        if ((value || "").trim()) {
            this.chipListFields.forEach((item) => {
                item.data.push(value.trim());
            });
        }
        this.accessInput.nativeElement.value = "";
    }
    ExportButtons(value) {
        let self = this;
        let apiUrl;
        let inputFieldObj = {};
        let queryObj = {};
        let requestObj = {};
        if (self.formData.buttons) {
            _.forEach(self.formData.buttons, function (item) {
                apiUrl = item.onLoadFunction.apiUrl;
                Object.keys(self.selectedChip).forEach((keys) => {
                    inputFieldObj[keys] = JSON.stringify(self.selectedChip[keys]);
                    queryObj = {
                        datasource: self.dataId,
                        report_type: value,
                        type: "SOD",
                    };
                });
            });
            requestObj = Object.assign({}, inputFieldObj, queryObj);
            self.contentService.getExportResponse(apiUrl, requestObj).subscribe((data) => {
                this.downloadFile(data.body, value);
            }, (error) => {
                console.log(error);
            });
        }
    }
    downloadFile(response, value) {
        this.runReport = this._fuseTranslationLoaderService.instant(this.formData.header);
        if (value === "CSV") {
            const blob = new Blob([response], { type: "text/csv" });
            FileSaver.saveAs(blob, this.runReport + ".csv");
        }
        else {
            const blob = new Blob([response], { type: "text/xlsx" });
            FileSaver.saveAs(blob, this.runReport + ".xlsx");
        }
    }
    receiveClick(event) {
        this.enableSelectValueView = false;
        let tempData = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(tempData) ? JSON.parse(tempData) : {};
        this.selectedTableView = this.formValues.viewSelectedValues
            ? this.formValues.viewSelectedValues.tableData
            : [];
        this.selectTableLoad = this.formValues.viewSelectedValues || {};
        if (this.formValues &&
            this.formValues.viewSelectedValues &&
            this.formValues.viewSelectedValues.inputKey) {
            this.selectTableLoad["response"] = this.inputData[this.formValues.viewSelectedValues.inputKey];
        }
        else {
            this.selectTableLoad["response"] = this.inputData.selectedData;
        }
        if (this.selectedTableView && this.selectedTableView.length) {
            setTimeout(() => {
                this.enableSelectValueView = true;
            }, 100);
        }
        this.messageService.sendButtonEnableMessage("data");
    }
    deleteClick(event) {
        this.enableTableLayout = false;
        console.log(event, ">>>>>EVENT");
        console.log(this, ">>> THIS");
        setTimeout(() => {
            this.enableTableLayout = true;
        }, 200);
    }
    changeTab(event) {
        event = event && event.index ? event.index : 0;
        let currentTabTable = this.formValues.tabData[event].loadTable;
        this.tableList = currentTabTable.tableData ? currentTabTable.tableData : [];
        this.currentTableLoad = currentTabTable;
        console.log(">>>  this.currentTableLoad ", this.currentTableLoad);
        if (this.formValues.fromLocalStorage && this.formValues.assignFirstTable) {
            // added for ruleset error log
            let responseKey = this.tableList[0].responseKey;
            let tableId = this.tableList[0].tableId;
            let subKey = this.tableList[0].subResponseKey;
            if (this.inputData[responseKey] &&
                this.inputData[responseKey][tableId] &&
                this.inputData[responseKey][tableId][subKey]) {
                this.currentTableLoad["response"] = this.inputData[responseKey][tableId][subKey]
                    ? this.inputData[responseKey][tableId][subKey]
                    : [];
            }
            if (this.tableList[0].enableErrorCount && this.tableList[0].countKey) {
                // errorShow key and value
                this.currentTableLoad[this.tableList[0].countKey] = this.inputData[responseKey][tableId];
            }
        }
        else {
        }
        this.enableTableLayout = true;
        this.enableNewTableLayout = this.formValues.enableNewTableLayout
            ? true
            : false;
    }
    navigateNext() {
        console.log(this.ChipLimit, "::::");
        let temp = this.isNameAvailable;
        this.submitted = true;
        var self = this;
        if (this.formValues.getFormControlValue) {
            this.inputData[this.formValues.valueToSave] = this.inputGroup.value[this.formValues.formFieldName];
        }
        console.log(this.inputData, "......INPUT DATA");
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.validationCheck(function (result) {
            if (result && temp) {
                self.stepperVal.next();
            }
        });
    }
    validationCheck(callback) {
        let tempData = JSON.parse(localStorage.getItem("currentInput"));
        console.log(">>> valaidationcheck tempData ", tempData);
        if (tempData && tempData.selectedData && tempData.selectedData.length) {
            this.inputData = Object.assign({}, this.inputData, tempData);
        }
        if (tempData &&
            tempData[this.formValues.validateSelectKey] &&
            tempData[this.formValues.validateSelectKey].length) {
            this.inputData = Object.assign({}, this.inputData, tempData);
        }
        if (this.formValues.formusedForCheck) {
        }
        this.inputData["chipLimit"] = this.ChipLimit;
        this.inputData["chipOperator"] = this.ChipOperator;
        console.log(this.inputData, "inputDatainputData");
        console.log(this.inputData, "TESTTT");
        console.log("this.formValues >>>>", this.formValues);
        if (this.formValues.validateSelectedVal) {
            if (this.inputData &&
                this.inputData[this.formValues.validateSelectKey] &&
                this.inputData[this.formValues.validateSelectKey].length) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
            else if (this.inputData &&
                this.inputData["selectedData"] &&
                this.inputData["selectedData"].length &&
                !this.formValues.validateSelectKey) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
            else {
                let toastMessageDetails = this.formValues.validations.toastMessage;
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                callback(false);
            }
        }
        else if (this.formValues.validateValueStatus &&
            this.formValues.validateSubKey) {
            // added When Manage role - access check
            var checkVal = _.find(this.inputData[this.formValues.validateSubKey], function (item) {
                return item.status == "ACTIVE";
            });
            if (checkVal) {
                callback(true);
            }
            else {
                let toastMessageDetails = this.formValues.validations.toastMessage;
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                this.chipValiditaion = true;
                callback(false);
            }
        }
        else if (this.formValues.validateSelectedChip) {
            if (this.inputData &&
                this.inputData["selectedChipKeyList"] &&
                this.inputData["selectedChipKeyList"][this.formValues.validationChipId] &&
                this.inputData["selectedChipKeyList"][this.formValues.validationChipId]
                    .length) {
                if (this.inputData.controlType == "SOD" &&
                    this.inputData["selectedChipKeyList"][this.formValues.validationChipId].length >= 2) {
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    callback(true);
                }
                else if (this.inputData.controlType == "Sensitive" &&
                    this.inputData["selectedChipKeyList"][this.formValues.validationChipId].length >= 1) {
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    callback(true);
                }
                else if (!this.inputData.controlType) {
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    callback(true);
                }
                else {
                    let toastMessageDetails = this.formValues.validations.toastMessage;
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    this.chipValiditaion = true;
                    callback(false);
                }
            }
            else {
                // console.log("this.selectedChips.length ",this.selectedChips)
                // console.log("this.formValues.chipFields ",this.formValues.chipFields)
                if (_.isEmpty(this.selectedChip)) {
                    let toastMessageDetails = this.formValues.validations.toastMessage;
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    this.chipValiditaion = true;
                    callback(false);
                }
                else if (!_.isEmpty(this.selectedChip)) {
                    let self = this;
                    _.forEach(this.formValues.requiredChips, function (item) {
                        // console.log("item >>>>>>>>",item)
                        if (self.selectedChip[item] &&
                            self.selectedChip[item].length <= 0) {
                            self.chipValiditaion = true;
                            callback(false);
                        }
                        else if (self.selectedChip[item] &&
                            self.selectedChip[item].length > 0) {
                            self.chipValiditaion = false;
                            callback(true);
                        }
                    });
                }
                else {
                    let toastMessageDetails = this.formValues.validations.toastMessage;
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    callback(false);
                }
            }
        }
        else {
            if (this.inputGroup && this.inputGroup.valid) {
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                callback(true);
            }
        }
    }
    navigatePrevious() {
        this.stepperVal.previous();
    }
    showData(tempData) {
        console.log(tempData, "DDDD");
        console.log(this.formValues, ">>>>>>formValues");
        this.selectedChips = this.inputData.selectedChips;
        this.selectedChipKeyList = this.inputData.selectedChipKeyList;
        // debugger;
        if (this.formValues && this.formValues.chipFields) {
            let self = this;
            _.forEach(this.formValues.chipFields, function (x) {
                if (x && x.chipCondition) {
                    let temp = x.chipCondition.variable;
                    if (x.chipCondition.data &&
                        self.inputData &&
                        self.inputData[temp] &&
                        x.chipCondition.data[self.inputData[temp]]) {
                        self.ChipLimit =
                            x.chipCondition.data[self.inputData[temp]]["limit"];
                        self.ChipOperator =
                            x.chipCondition.data[self.inputData[temp]]["operator"];
                    }
                }
                if (x.onLoadFunction) {
                    // chipfields OnLoad function added when implement run reports
                    let apiUrl = x.onLoadFunction.apiUrl;
                    let responseName = x.onLoadFunction.response;
                    let requestData = x.onLoadFunction.requestData;
                    let query = { realm: self.realm };
                    _.forEach(requestData, function (requestItem) {
                        if (requestItem.isDefault) {
                            query[requestItem.name] = requestItem.value;
                        }
                        else {
                            query[requestItem.name] = self.inputData[requestItem.value];
                        }
                    });
                    self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                        x.data = data.response[responseName];
                    });
                }
            });
        }
        this.viewData = tempData;
        console.log(this.viewData, "......VIEW DATA");
        if (this.formValues &&
            (this.formValues.confirmData || this.formValues.showSelectedValues)) {
            this.enableSelectValueView = false;
            if (this.formValues && this.formValues.viewSelectedValues) {
                this.selectedTableView = this.formValues.viewSelectedValues.tableData
                    ? this.formValues.viewSelectedValues.tableData
                    : [];
                this.selectTableLoad = this.formValues.viewSelectedValues;
                if (this.formValues &&
                    this.formValues.viewSelectedValues &&
                    this.formValues.viewSelectedValues.inputKey) {
                    this.selectTableLoad["response"] = this.inputData[this.formValues.viewSelectedValues.inputKey];
                }
                else {
                    this.selectTableLoad["response"] = this.inputData.selectedData;
                }
                setTimeout(() => {
                    this.enableSelectValueView = true;
                }, 100);
            }
        }
        if (this.formValues && this.formValues.autoCompleteFields) {
            _.forEach(this.formValues.autoCompleteFields, function (autoCompleteItem) {
                console.log(autoCompleteItem, ">>>>>>autoCompleteItem");
                if (autoCompleteItem.valueFromLocal) {
                    console.log(autoCompleteItem.dataKeyToSave, ">>>autoCompleteItem.dataKeyToSave");
                    autoCompleteItem[autoCompleteItem.dataKeyToSave] =
                        tempData[autoCompleteItem.inputKey];
                    //       // autoCompleteItem[autoCompleteItem.dataKeyToSave] = _.map(self.inputData[autoCompleteItem.inputKey], autoCompleteItem.keyToShow);
                }
            });
            //   console.log(this.formValues.autoCompleteFields, ">>>>>this.formValues.autoCompleteFields")
        }
        // if(this.formValues && this.formValues.showSelectedChips) {
        // }
    }
    validateObjectFields(inputData, mandatoryFields, callback) {
        let valid = true;
        _.forEach(mandatoryFields, function (item) {
            if (!inputData[item]) {
                valid = false;
            }
        });
        callback(valid);
    }
    addNewValue() {
        console.log(this, "......");
        if (this.formValues && this.formValues.enableAddButton) {
            let tempData = localStorage.getItem("currentInput");
            this.inputData = !_.isEmpty(JSON.parse(tempData))
                ? JSON.parse(tempData)
                : {};
            let tempArray = this.inputData[this.formValues.dataToSave]
                ? this.inputData[this.formValues.dataToSave]
                : [];
            let tempObj = {};
            let inputValues = this.inputGroup.value;
            console.log(this.formValues.mandatoryFields, ">>>>>this.formValues.mandatoryFields");
            if (this.formValues && this.formValues.mandatoryFields) {
                var self = this;
                this.validateObjectFields(inputValues, this.formValues.mandatoryFields, function (valid) {
                    if (valid) {
                        _.forEach(self.formValues.objectFormat, function (objItem) {
                            tempObj[objItem.name] = inputValues[objItem.value];
                        });
                        let exist = tempArray && tempArray.length
                            ? _.findIndex(tempArray, tempObj)
                            : -1;
                        if (exist < 0) {
                            tempArray.push(tempObj);
                        }
                        self.inputData[self.formValues.dataToSave] = tempArray;
                        localStorage.setItem("currentInput", JSON.stringify(self.inputData));
                        self.receiveClick(null);
                    }
                    else {
                        self.snackBarService.warning("Enter Mandatory Fields!");
                    }
                });
                this.inputGroup.reset();
            }
            // if(_.isEqual(inputValueKeys, this.formValues.mandatoryFields)){
            //   _.forEach(this.formValues.objectFormat, function(objItem){
            //     tempObj[objItem.name] = inputValues[objItem.value]
            //   })
            // }else{
            //   this.snackBarService.warning("Enter Mandatory Fields!");
            // }
        }
    }
    onChange(data, selectedField) {
        console.log(" data ", data);
        console.log(" selectedField ", selectedField);
        let tempData = JSON.parse(localStorage.getItem("currentInput"));
        let selectId = selectedField.name + "Id";
        let existCheck = _.has(this.inputData, selectId);
        let idKey = existCheck ? selectId : selectedField.name + "Id";
        let nameKey = selectedField.name + "Name";
        this.inputData[idKey] = data[selectedField.keyToSave];
        if (selectedField.additionalKeyToSave) {
            this.inputData[selectedField.name + "Name"] =
                data[selectedField.additionalKeyToSave];
            nameKey = selectedField.name + "Name";
        }
        tempData = Object.assign({}, tempData, this.inputData);
        if (selectedField.refreshChipField && this.selectedChips) {
            let self = this;
            _.forEach(selectedField.refreshChipFieldKey, function (refreshItem) {
                self.selectedChips[refreshItem] = [];
                self.selectedChipKeyList[refreshItem] = [];
            });
        }
        tempData['selectedChips'] = this.selectedChips;
        tempData['selectedChipKeyList'] = this.selectedChipKeyList;
        this.inputData = tempData;
        if (_.has(this.inputData, selectedField.name) &&
            tempData[selectedField.name] === "") {
            tempData[selectedField.name] = this.inputData[selectedField.name];
            tempData[idKey] = this.inputData[idKey];
            tempData[nameKey] = this.inputData[nameKey];
            localStorage.setItem("currentInput", JSON.stringify(tempData));
        }
        else {
            localStorage.setItem("currentInput", JSON.stringify(tempData));
        }
        if (selectedField.getAutocompleteData) {
            let selectedRequestData = _.find(this.formValues.requestList, {
                requestId: data.value,
            });
            console.log(selectedRequestData, ">>>>>>selectedRequestData");
            this.getAutoCompleteValues(selectedRequestData, data, selectedField);
        }
        else if (selectedField.onLoadValueKey) {
            if (selectedField.onChipLoad) {
                _.forEach(this.formValues.chipFields, function (selectFieldItem) {
                    if (selectFieldItem.name == selectedField.onLoadValueKey) {
                        selectFieldItem.data = data[selectedField.onChipLoad];
                    }
                });
            }
            else {
                _.forEach(this.formValues.selectFields, function (selectFieldItem) {
                    if (selectFieldItem.name == selectedField.onLoadValueKey) {
                        selectFieldItem.data = data[selectedField.onLoadValueKey];
                    }
                });
            }
        }
        else {
            this.getChipList();
        }
    }
    getAutoCompleteValues(selectedRequestData, selectedData, selectedField) {
        console.log("** ", ">>>selectedField", selectedField);
        console.log("** selectedRequestData ", selectedRequestData);
        // console.log(" ** selectedRequestData.textFieldName ",selectedRequestData.textFieldName);
        if (selectedRequestData && selectedRequestData.keyToSaveOnInput) {
            this.inputData[selectedRequestData.keyToSaveOnInput] =
                selectedData[selectedRequestData.keyToSaveOnInput];
        }
        var self = this;
        let index;
        let currentObj;
        if (this.formValues && this.formValues.formArrayValues) {
            index = _.findIndex(this.formValues.formArrayValues.autoCompleteFields, {
                name: selectedRequestData.textFieldName,
            });
            currentObj = this.formValues.formArrayValues.autoCompleteFields[index];
        }
        else {
            index = _.findIndex(this.formValues.autoCompleteFields, selectedRequestData);
            currentObj = this.formValues.autoCompleteFields[index];
        }
        console.log(index, ">>>>>>index");
        // debugger;
        // let autoCompleteField = this.formValues.formArrayValues.autoCompleteFields;
        // _.forEach(this.formValues.formArrayValues.autoCompleteFields, function(
        //   item
        // ) {
        // let item = selectedRequestData;
        if (selectedRequestData && selectedRequestData.data) {
            console.log(" entered default data ");
            currentObj[selectedField.onLoadDataKey] = selectedRequestData.data;
        }
        else {
            if (selectedRequestData.onLoadFunction) {
                let apiUrl = selectedRequestData.onLoadFunction.apiUrl;
                let responseName = selectedRequestData.onLoadFunction.response;
                let query = { realm: self.realm };
                _.forEach(selectedRequestData.onLoadFunction.requestData, function (requestItem) {
                    if (requestItem.isDefault) {
                        query[requestItem.name] = requestItem.value;
                    }
                    else {
                        if (!requestItem.isSearch) {
                            query[requestItem.name] = self.inputData[requestItem.value];
                        }
                        if (requestItem.isEventValue) {
                            query[requestItem.name] = selectedData;
                        }
                    }
                });
                console.log(" query ", query, " apiurl ", apiUrl);
                //this.loaderService.startLoader();
                self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                    // this.loaderService.stopLoader();
                    currentObj[selectedField.onLoadDataKey] = data.response[responseName];
                    let validateData = currentObj[selectedField.onLoadDataKey][0];
                    let isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                    if (!isArrayOfJSON) {
                        const tempList = [];
                        _.forEach(currentObj[selectedField.onLoadDataKey], function (val) {
                            tempList.push({ obj_name: val, obj_desc: val });
                        });
                        currentObj[selectedField.onLoadDataKey] = tempList;
                    }
                    this.viewData[selectedField.onLoadDataKey] =
                        currentObj[selectedField.onLoadDataKey];
                    if (index == selectedField.enableConditionIndex) {
                        console.log(this, ">>>>>");
                        this.inputGroup.controls[selectedRequestData.name].enable();
                    }
                }, (err) => {
                    // this.loaderService.stopLoader();
                    console.log(" Error ", err);
                });
            }
        }
        // console.log(it/em, ".....>>>ITEMMM");
        // tempArray.push(item);
        // });
        currentObj = Object.assign({}, currentObj, selectedRequestData);
        console.log(currentObj, ".....currentObj");
        // this.formValues.formArrayValues.autoCompleteFields[index] = {
        //   ...currentObj,
        //   ...item
        // };
        if (this.formValues && this.formValues.formArrayValues) {
            this.formValues.formArrayValues.autoCompleteFields[index] = currentObj;
        }
        else {
            this.formValues.autoCompleteFields[index] = currentObj;
        }
        // console.log(
        //   this.formValues.formArrayValues.autoCompleteFields,
        //   ">>>>>>>>>>>>>>>>>"
        // );
    }
    onAutoCompleteSearch(searchValue, inputField) {
        console.log(searchValue, ">>>>>searchValue");
        console.log(inputField, ">>>>> INPUT FIELD");
        if (inputField.valueFromLocal || inputField.enableFilter) {
            if (searchValue) {
                let searchObj = {};
                searchObj[inputField.keyToShow] = searchValue;
                inputField[inputField.dataKeyToSave] = _.filter(inputField[inputField.dataKeyToSave], function (item) {
                    const filterValue = searchValue.toLowerCase();
                    if (item[inputField.keyToShow].toLowerCase().indexOf(filterValue) == 0) {
                        return item;
                    }
                });
            }
            else {
                inputField[inputField.dataKeyToSave] =
                    this.viewData && this.viewData[inputField.dataKeyToSave]
                        ? this.viewData[inputField.dataKeyToSave]
                        : this.viewData[inputField.inputKey];
            }
        }
    }
    onSelectAutoComplete(event, index, selectedField) {
        console.log(event, ">>>>>EVENT");
        console.log(index, ">>>>>INDEX");
        console.log(selectedField, ">>>>> SELECTED FIELD");
        let selectedRequestData = _.find(this.formValues.autoCompleteFields, {
            functionCallIndex: index + 1,
        });
        console.log(selectedRequestData, ">>>>>>selectedRequestData");
        this.getAutoCompleteValues(selectedRequestData, event, selectedField);
    }
    onSearchChange(searchValue, inputField) {
        console.log(inputField, "..inputField");
        console.log(searchValue, ".........searchvalue");
        var charLength = searchValue.length;
        var limit = this.formValues.formArrayValues.autoCompleteFields.charlimit
            ? this.formValues.formArrayValues.autoCompleteFields.charlimit
            : 3;
        console.log("Limit ", limit);
        // if (charLength >= limit || !searchValue) {
        console.log("call API ");
        this.charlimitcheck = true;
        var self = this;
        let index = _.findIndex(this.formValues.formArrayValues.autoCompleteFields, { name: inputField.textFieldName });
        let currentObj = this.formValues.formArrayValues.autoCompleteFields[index];
        if (inputField.onLoadFunction) {
            let apiUrl = inputField.onLoadFunction.apiUrl;
            let responseName = inputField.onLoadFunction.response;
            let query = { realm: self.realm };
            _.forEach(inputField.onLoadFunction.requestData, function (requestItem) {
                if (requestItem.isSearch) {
                    query[requestItem.name] = searchValue;
                }
                else if (requestItem.isDefault) {
                    query[requestItem.name] = requestItem.value;
                }
                else {
                    query[requestItem.name] = self.inputData[requestItem.value];
                }
            });
            //this.loaderService.startLoader();
            self.contentService.getAllReponse(query, apiUrl).subscribe((data) => {
                console.log(">> onsearch Change ");
                // this.loaderService.stopLoader();
                currentObj[inputField.onLoadDataKey] = data.response[responseName];
                let validateData = currentObj[inputField.onLoadDataKey][0];
                let isArrayOfJSON = _.isPlainObject(validateData) ? true : false;
                if (!isArrayOfJSON) {
                    const tempList = [];
                    _.forEach(currentObj[inputField.onLoadDataKey], function (val) {
                        tempList.push({ obj_name: val, obj_desc: val });
                    });
                    currentObj[inputField.onLoadDataKey] = tempList;
                }
                this.viewData[inputField.onLoadDataKey] =
                    currentObj[inputField.onLoadDataKey];
                console.log(currentObj, ">>>>>currentObj");
            }, (err) => {
                // this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
        this.formValues.formArrayValues.autoCompleteFields[index] = currentObj;
        // } else {
        //   console.log("Stop API ");
        //   this.charlimitcheck = false;
        // }
    }
    onOptionChange(event, selectedOption) {
        console.log(this.inputData, "inputData>>>>>>>>");
        this.inputData[selectedOption.name] = event.value;
        console.log(this.formValues);
        // if (this.enableChipFields) {
        //   console.log(this.enableChipFields, "enableChipFields");
        //   var self = this;
        //   _.forEach(this.formValues.chipFields, function(x) {
        //     console.log("x");
        //     if (x.chipCondition && x.chipCondition.variable) {
        //       var self = this;
        //       let temp = x.chipCondition.variable;
        //       self.ChipLimit = x.chipCondition.data[self.inputData[temp]]["limit"];
        //       self.ChipOperator =
        //         x.chipCondition.data[self.inputData[temp]]["operator"];
        //       console.log(self.ChipLimit, "ChipLimit>>>");
        //     }
        //   });
        //   console.log(this.ChipLimit, "1");
        //   console.log(this.ChipOperator, "2");
        //   self.inputData["chipLimit"] = this.ChipLimit;
        //   self.inputData["chipOperator"] = this.ChipOperator;
        //   // localStorage.setItem('chipData',event.value)
        // }
        console.log(event, "....event.value");
        console.log(selectedOption, "....selectedOption");
        this.inputData[selectedOption.name] = event.value;
        var self = this;
        if (selectedOption && selectedOption.isImportType) {
            this.selectImportView(event.value);
        }
        else if (selectedOption && selectedOption.name == 'chechImport') {
            if (event.value == 'NO') {
                this.enableImportData = false;
            }
            else {
                this.enableImportData = true;
            }
        }
        else {
            _.forEach(selectedOption.fields, function (optionItem) {
                if (optionItem.checkedType) {
                    self.inputData[optionItem.checkedType] =
                        optionItem.value === event.value ? true : false;
                }
            });
            console.log(self.inputData, "self");
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    }
    toggleChanged(event) { }
    onSearchName(event, fieldData) {
        console.log(" on Search Name ************************** ");
        let charLength = event.length;
        console.log(" On search Name event ", event, " fielddata ", fieldData, " Length: ", charLength);
        if (fieldData.enableOnTypeSearch && event) {
            var limit = fieldData.charlimit ? fieldData.charlimit : 3;
            if (limit <= charLength) {
                this.charlimitcheck = true;
                console.log("proceed API call");
                let requestDetails = fieldData.onTypeSearch;
                let query = { realm: this.realm };
                var self = this;
                console.log(self.currentData, ">>>> CURRENT");
                _.forEach(requestDetails.requestData, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        query[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else if (requestItem.isDefault) {
                        query[requestItem.name] = requestItem.value;
                    }
                    else {
                        query[requestItem.name] = self.inputData[requestItem.value];
                    }
                });
                console.log("Query ", query);
                // this.loaderService.startLoader();
                this.contentService
                    .getAllReponse(query, requestDetails.apiUrl)
                    .subscribe((data) => {
                    //  this.loaderService.stopLoader();
                    console.log(data, "data:::::");
                    // let responseLength =
                    //   data.response &&
                    //   data.response[requestDetails.responseName].length > 0
                    //     ? data.response[requestDetails.responseName].length
                    //     : 0;
                    if (data.response && data.response[requestDetails.responseName]) {
                        this.isNameAvailable =
                            data.response[requestDetails.responseName].length == 0
                                ? true
                                : false;
                        console.log(this.isNameAvailable, "isNameAvilable1");
                    }
                    else if (data.status == 409) {
                        this.isNameAvailable = data.status == 409 ? false : true;
                        console.log(this.isNameAvailable, "isNameAvilable2");
                    }
                    // this.isNameAvailable =
                    //   responseLength === 0 || data.status != 409 ? true : false;
                    // console.log(
                    //   data.response[requestDetails.responseName].length,
                    //   "IIIIII",
                    //   this.isNameAvailable
                    // );
                }, (err) => {
                    // this.loaderService.stopLoader();
                    if (err.status == 409) {
                        this.isNameAvailable = err.status == 409 ? false : true;
                        console.log(this.isNameAvailable, "isNameAvilable2");
                    }
                });
            }
            else {
                console.log("STOP APi call");
                // this.isNameAvailable=false;
                this.charlimitcheck = false;
            }
        }
    }
    onSubmit() {
        var self = this;
        console.log(this, ">>>>>>>>>>>>&&&&&THIS");
        this.submitted = true;
        this.validationCheck(function (result) {
            if (result) {
                let requestDetails = (self.onLoadData && self.onLoadData.action &&
                    self.currentConfigData[self.onLoadData.action] &&
                    self.currentConfigData[self.onLoadData.action].requestDetails)
                    ? self.currentConfigData[self.onLoadData.action].requestDetails
                    : {};
                if (!self.onLoadData && self.formValues.analysisRequest) {
                    requestDetails = self.formValues.analysisRequest;
                }
                else if (self.formValues.requestDetails) {
                    requestDetails = self.formValues.requestDetails;
                }
                let details = self.viewData;
                // details["status"] = "ACTIVE";
                // details["operators"] = ["AND"];
                console.log(">>> details ", details);
                let apiUrl = requestDetails.apiUrl;
                let requestData = {
                    realm: self.realm
                };
                _.forEach(requestDetails.requestData, function (item) {
                    if (item.isDefault) {
                        requestData[item.name] = item.value;
                    }
                    else if (item.controlTypeCheck) {
                        //  for storing Operators
                        //  requestData[item.name] = eval(item.condition);
                        requestData[item.name] =
                            details["controlType"] && details["controlType"] == "SOD"
                                ? "AND"
                                : "OR";
                    }
                    else if (item.fromFormGroup) {
                        let formGroupvalue = self.inputGroup.value;
                        requestData[item.name] = formGroupvalue[item.value];
                    }
                    else {
                        let existCheck = _.has(details, item.value);
                        console.log(">>> existCheck ", existCheck);
                        if (existCheck) {
                            requestData[item.name] = item.subKey
                                ? details[item.value][item.subKey]
                                : details[item.value];
                        }
                        if (requestData[item.name] === undefined &&
                            item.alternativeKeyCheck) {
                            // addded for storing  default selecteld field objectId . (Edit)
                            requestData[item.name] = details[item.alternativeKey];
                        }
                    }
                });
                let toastMessageDetails = requestDetails.toastMessage;
                if (self.onLoadData && (self.onLoadData.action === "edit" || self.onLoadData.action === "update")) {
                    let idVal = requestDetails.multipleId ? true : self.inputData._id;
                    self.contentService
                        .updateRequest(requestData, apiUrl, idVal)
                        .subscribe((res) => {
                        self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        if (res.status == 201 || res.status == 200) {
                            localStorage.removeItem("currentInput");
                            self.matDialogRef.close();
                            self.messageService.sendModelCloseEvent("listView");
                        }
                    }, (error) => {
                        self.submitted = false;
                        self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    });
                }
                else {
                    self.contentService.createRequest(requestData, apiUrl).subscribe((res) => {
                        self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        if (res.status == 201) {
                            localStorage.removeItem("currentInput");
                            self.matDialogRef.close();
                            self.messageService.sendModelCloseEvent("listView");
                        }
                    }, (error) => {
                        self.submitted = false;
                        self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    });
                }
            }
        });
    }
    onFileChange(event, fileType, importData) {
        console.log(">>> onFileChange event ", event);
        let reader = new FileReader();
        if (event.length) {
            let file = event[0];
            let fileName = file.name;
            console.log(">>> file ", file);
            console.log(">>> fileName ", fileName);
            this.fileType = file.type;
            let fileExt = fileName.split(".").pop();
            let fileValidateFlag = false;
            if (importData && importData.multiFileSupport) {
                let validFileTypes = importData.supportedFiles.split(",");
                fileValidateFlag = (validFileTypes.indexOf(fileExt) >= 0) ? true : false;
            }
            else {
                fileValidateFlag = (fileExt == fileType) ? true : false;
            }
            if (fileValidateFlag) {
                console.log(" Proceed ");
                reader.readAsDataURL(file);
                reader.onload = () => {
                    this.image = file;
                    this.inputGroup.patchValue({
                        file: reader.result,
                    });
                    this.selectedFileName = file.name;
                    this.fileUpload = file;
                    this.changeDetector.markForCheck();
                };
            }
            else {
                console.log(" File Extension Error ");
                this.importData.uploadFile = {};
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant("File Extension Error. File extension should be " +
                    fileType +
                    " format"));
            }
        }
        else {
            console.log(">>> file required ");
        }
    }
    onFileChange_backup(event, fileType, importData) {
        let reader = new FileReader();
        if (importData && importData.multiFileSupport) {
            let file = event.target.files[0];
            this.fileType = file.type;
            let fileName = file.name;
            let fileExt = fileName.split(".").pop();
            let validFileTypes = importData.supportedFiles.split(",");
            console.log(fileExt, "importData.supportedFiles >>>>>", validFileTypes);
            if (validFileTypes.indexOf(fileExt) >= 0) {
                console.log(" Proceed ");
                reader.readAsDataURL(file);
                reader.onload = () => {
                    this.image = file;
                    this.inputGroup.patchValue({
                        file: reader.result,
                    });
                    this.selectedFileName = file.name;
                    this.fileUpload = file;
                    this.changeDetector.markForCheck();
                };
            }
            else {
                console.log(" File Extension Error ");
                this.importData.uploadFile = {};
                this.snackBarService.warning(this._fuseTranslationLoaderService.instant("File Extension Error. File extension should be " +
                    importData.supportedFiles +
                    " format"));
            }
        }
        else {
            if (event.target.files && event.target.files.length) {
                let file = event.target.files[0];
                this.fileType = file.type;
                let fileName = file.name;
                console.log(fileName, " File Name");
                let fileExt = fileName.split(".").pop();
                // console.log(fileType," File Type ");
                // console.log(fileExt," fileExt ");
                if (fileExt == fileType) {
                    console.log(" Proceed ");
                    reader.readAsDataURL(file);
                    reader.onload = () => {
                        this.image = file;
                        this.inputGroup.patchValue({
                            file: reader.result,
                        });
                        this.selectedFileName = file.name;
                        this.fileUpload = file;
                        this.changeDetector.markForCheck();
                    };
                }
                else {
                    console.log(" File Extension Error ");
                    this.importData.uploadFile = {};
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant("File Extension Error. File extension should be " +
                        fileType +
                        " format"));
                }
            }
        }
    }
    closeModel() {
        this.matDialogRef.close();
        this.messageService.sendModelCloseEvent("listView");
    }
    onImport() {
        this.submitted = true;
        let requestData = {};
        var self = this;
        this.inputData.cronExpression = this.cronExpression ? this.cronExpression : "runOnce";
        let importDetails = this.importData.onLoadFunction;
        _.forEach(importDetails.requestData, function (item) {
            if (item.isDefault) {
                requestData[item.name] = item.value;
            }
            else {
                requestData[item.name] = item.subKey
                    ? self.inputData[item.value][item.subKey]
                    : self.inputData[item.value];
            }
        });
        if (this.importData.uploadFile) {
            var formData = new FormData();
            Object.keys(requestData).map((key) => {
                formData.append(key, requestData[key]);
            });
            formData.append("file", this.fileUpload);
            requestData = formData;
        }
        let toastMessageDetails = importDetails.toastMessage;
        console.log(this, ">>>>>> THIS");
        requestData["realm"] = this.realm;
        if (this.inputGroup && this.inputGroup.valid) {
            this.loaderService.startLoader();
            //this.matDialogRef.close();
            console.log("This.input .....  ", this.inputData);
            // debugger;
            if (this.importData.function && this.importData.function == "edit") { // ruleset, datasource - reimport
                if (this.fileUpload !== undefined) {
                    this.contentService
                        .updateRequest(requestData, importDetails.apiUrl, this.inputData["_id"]
                    // this.inputData["datasourceId"]
                    )
                        .subscribe((res) => {
                        console.log(res, ".....res");
                        this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        if (res.status == 201 || res.status == 200) {
                            if (this.importData.s3upload) {
                                let response = res.body.response;
                                if (response && response.job) {
                                    this.matDialogRef.close();
                                    this.imageUpload(response.job.datasource, this.importData);
                                }
                                else if (response && response.datasource) {
                                    this.matDialogRef.close();
                                    console.log("<<<<DATA IMPORT>>>", response.datasource);
                                    this.imageUpload(response.datasource, this.importData);
                                }
                                else if (response && response.ruleset) {
                                    this.matDialogRef.close();
                                    console.log("<<<<Ruleset IMPORT>>>", response.ruleset);
                                    this.imageUpload(response.ruleset, this.importData);
                                }
                            }
                            else {
                                this.matDialogRef.close();
                            }
                        }
                        this.loaderService.stopLoader();
                    }, (error) => {
                        // this.matDialogRef.close();
                        this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                        this.errorTableList = this.importData.tableData;
                        this.errorLogTable = this.importData;
                        this.errorLogTable["response"] = error.error.response;
                        this.errorLogTable["responseKey"] = "logsArr";
                        this.errorShow = error.error.response;
                        console.log(this.errorLogTable, "errorLog>>>>>>>");
                    });
                }
                else {
                    this.loaderService.stopLoader();
                    this.snackBarService.warning(this._fuseTranslationLoaderService.instant(this.importData.validations.message));
                }
            }
            else {
                if (this.inputData && this.inputData.importType == "avm") {
                    this.contentService.avmImport(requestData, this.fileUpload, importDetails.avmApi).subscribe((res) => {
                        this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        this.loaderService.stopLoader();
                        this.matDialogRef.close();
                    }, (error) => {
                        this.loaderService.stopLoader();
                        this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                    });
                }
                else if (this.inputData && this.inputData.importType == "sodAgent") {
                    if (importDetails.sodAgentRequest) {
                        _.forEach(importDetails.sodAgentRequest, function (item) {
                            if (item.isDefault) {
                                requestData[item.name] = item.value;
                            }
                            else if (item.fromUserData) {
                                if (item.subKey && item.assigneFirstValue) {
                                    requestData[item.name] = (self.userData && self.userData[item.value] && self.userData[item.value][0]) ? self.userData[item.value][0][item.subKey] : "";
                                }
                                else if (item.subKey) {
                                    requestData[item.name] = (self.userData && self.userData[item.value]) ? self.userData[item.value] : "";
                                }
                                else {
                                    requestData[item.name] = (self.userData && self.userData[item.value]) ? self.userData[item.value] : "";
                                }
                            }
                            else {
                                requestData[item.name] = item.subKey
                                    ? self.inputData[item.value][item.subKey]
                                    : self.inputData[item.value];
                            }
                        });
                    }
                    console.log(this, ">>>>>> THIS");
                    console.log(">>>> requestData ", requestData);
                    let produtName = requestData && requestData["productName"] ? requestData["productName"] : "Ebs";
                    console.log(">>> produtName ", produtName);
                    this.contentService
                        .createRequest(requestData, importDetails.sodAgentCreate + this.inputData[importDetails.sodAgentParameterKey])
                        .subscribe((res) => {
                        console.log(res, ".....res");
                        if (res && res.body && res.body.data) {
                            this.contentService
                                .getS3Response(requestData, importDetails.sodAgentDownloadApi
                                + this.inputData[importDetails.sodAgentParameterKey]
                                + '/' + this.userData.customer + '/' + produtName)
                                .subscribe((data) => {
                                console.log(data, ">>>>data");
                                var localfilename = "Sod_Agent_" + produtName;
                                //var localfilename = "Sod_Agent_Ebs";
                                localfilename = localfilename.replace(/\s/gi, "_");
                                this.downloadExportFile(data.body, localfilename + '.zip', 'application/zip');
                                this.matDialogRef.close();
                                this.loaderService.stopLoader();
                            });
                        }
                        else {
                            this.snackBarService.warning("Unable To Download agent!");
                        }
                    }, (error) => {
                    });
                }
                else {
                    let apiurl = importDetails.apiUrl;
                    if (this.inputData.importType == 'webservice') {
                        // apiurl=importDetails.webserviceApi;
                        this.importData.s3upload = false;
                        requestData["webservice"] = true;
                    }
                    this.contentService
                        .createRequest(requestData, apiurl)
                        .subscribe((res) => {
                        console.log(res, ".....res");
                        this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                        if (res.status == 201 || res.status == 200) {
                            // this.matDialogRef.close();
                            if (this.importData.s3upload) {
                                let response = (res.body && res.body.response && res.body.response.datasource) ? res.body.response.datasource : res.body.response;
                                if (response && response.job) {
                                    this.matDialogRef.close();
                                    this.imageUpload(response.job.datasource, this.importData);
                                }
                                else {
                                    this.matDialogRef.close();
                                    this.imageUpload(response, this.importData);
                                }
                            }
                            else {
                                this.matDialogRef.close();
                            }
                        }
                        else if (res.status == 206) {
                            this.errorTableList = this.importData.tableData;
                            this.errorLogTable = this.importData;
                            this.errorLogTable["response"] = res.body.response;
                            this.errorLogTable["responseKey"] = "logsArr";
                            this.errorShow = res.body.response;
                            console.log(this.errorLogTable, "errorLog on different status code >>>>>>>");
                        }
                    }, (error) => {
                        // this.matDialogRef.close();
                        this.snackBarService.warning(this._fuseTranslationLoaderService.instant(toastMessageDetails.error));
                        this.errorTableList = this.importData.tableData;
                        this.errorLogTable = this.importData;
                        this.errorLogTable["response"] = error.error.response;
                        this.errorLogTable["responseKey"] = "logsArr";
                        this.errorShow = error.error.response;
                        console.log(this.errorLogTable, "errorLog>>>>>>>");
                    });
                }
            }
        }
    }
    selectImportView(event) {
        console.log(event, ".....event");
        this.enableTextFields = false;
        this.enableOptionFields = false;
        let toggleFields = this.formValues.enableToggleFields;
        if (event != "csv" && event != "avm") {
            this.enableImportData = false;
            if (toggleFields && toggleFields.mergeTextFields) {
                this.formValues.textFields = _.concat(this.formValues.textFields, toggleFields.textFields);
            }
            if (toggleFields && toggleFields.additionalOption) {
                this.formValues.additionalOptionFields = toggleFields.additionalOptionFields;
            }
            if (toggleFields && toggleFields.enableSchedule) {
                this.enableSchedule = true;
            }
            else {
                this.enableSchedule = false;
            }
            if (toggleFields && toggleFields.enableAgentNote) {
                this.formValues.enableAgentNote = toggleFields.enableAgentNote;
            }
            var self = this;
            if (toggleFields.isSetDefaultValue) {
                _.forEach(toggleFields.additionalOptionFields, function (optionItem) {
                    self.inputData[optionItem.defaultKey] = optionItem.defaultValue;
                });
            }
            this.enableTextFields = true;
            this.enableOptionFields = true;
        }
        else {
            this.formValues.enableAgentNote = false;
            this.enableSchedule = false;
            this.formValues.textFields = this.formValues.textFields.filter(function (item) {
                return !toggleFields.textFields.includes(item);
            });
            if (toggleFields.additionalOptionFields) {
                this.formValues.additionalOptionFields = [];
            }
            this.enableImportData = true;
            // this.formValues.textFields =
            this.enableTextFields = true;
            this.enableOptionFields = true;
        }
        let tempObj = {};
        if (this.enableTextFields) {
            _.forEach(this.formValues.textFields, function (item) {
                tempObj[item.name] = new FormControl("");
            });
        }
        if (toggleFields.additionalOption) {
            tempObj["everyMinute"] = new FormControl("");
            tempObj["mainType"] = new FormControl("");
            tempObj["hourListvalue"] = new FormControl("");
            tempObj["everyDays"] = new FormControl("");
            tempObj["dayStartingOn"] = new FormControl("");
            tempObj["monthStartingOn"] = new FormControl("");
        }
        this.inputGroup = new FormGroup(tempObj);
    }
    imageUpload(data, importData) {
        console.log(data, ".....data");
        this.AwsS3UploadService.find({
            mimeType: this.fileType,
            type: importData.s3RequestType ? importData.s3RequestType : "datasource",
        }).subscribe((s3Credentials) => {
            if (s3Credentials) {
                this.AwsS3UploadService.uploadS3(this.image, s3Credentials.response, importData)
                    .then((result) => {
                    console.log(result, "...result");
                    let url = decodeURIComponent(result.PostResponse.Location);
                    console.log(url, "...url");
                    this.inputData["_id"] = data._id;
                    this.inputData["datasourcePath"] = url;
                    this.inputData["status"] = "Uploaded";
                    if (this.importData.function &&
                        this.importData.function == "edit") {
                        this.inputData["reimport"] = true;
                    }
                    else {
                        this.inputData["reimport"] = false;
                    }
                    let requestData = {};
                    console.log(this, "....THSIIISSS");
                    let updateDetails = this.importData.updateRequest;
                    console.log(updateDetails, ".....updateDetails");
                    var self = this;
                    _.forEach(updateDetails.requestData, function (item) {
                        requestData[item.name] = self.inputData[item.value];
                    });
                    console.log(requestData, "....REQUEST DATA");
                    this.contentService
                        .updateRequest(requestData, updateDetails.apiUrl, this.inputData._id)
                        .subscribe((res) => {
                        this.matDialogRef.close();
                        this.messageService.sendModelCloseEvent("listView");
                    });
                    // this.formData['erp_instance'] = this.csvForm.value.erp_instance;
                    // this.formData['name'] = this.csvForm.value.name;
                    // this.formData['status'] = "Uploaded";
                    // this.formData['ds_path'] = url;
                    // this.SetupAdministrationService.putImportCSV(this.formData, this.id).subscribe((dataNew: any) => {
                    // 	this.matDialogRef.close(dataNew.status);
                    // });
                })
                    .catch((error) => { });
            }
        });
    }
    viewOfseconds() {
        for (let i = 1; i <= 60; i++) {
            // console.log(i);
            this.secondsList.push(i);
        }
        this.secondsLastDigits = [];
        this.hourListView = [];
        this.hourstwothree = [];
        for (let j = 0; j <= 59; j++) {
            // console.log(i);
            this.secondsLastDigits.push(j);
        }
        for (let k = 1; k <= 24; k++) {
            this.hourListView.push(k);
        }
        for (let hours = 1; hours <= 23; hours++) {
            this.hourstwothree.push(hours);
        }
        this.days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        this.month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
            "November", "December "];
        this.monthsCount = [];
        this.dateCount = [];
        for (let dateCounts = 1; dateCounts <= 7; dateCounts++) {
            this.dateCount.push(dateCounts);
        }
        for (let monthCount = 1; monthCount <= 12; monthCount++) {
            this.monthsCount.push(monthCount);
        }
        this.numOfdays = [];
        this.days.forEach(element => {
            console.log(element);
            this.numOfdays.push(element);
        });
        console.log(this.numOfdays);
        this.numOfmonths = [];
        this.month.forEach(element => {
            this.numOfmonths.push(element);
        });
    }
    secondChange() {
        this.secondsViewShow = true;
        this.MinutesViewShow = false;
        this.hourViewShow = false;
        this.dayviewShow = false;
        this.monthviewShow = false;
    }
    minutesChange() {
        this.secondsViewShow = false;
        this.hourViewShow = false;
        this.MinutesViewShow = true;
        this.dayviewShow = false;
        this.monthviewShow = false;
    }
    hourChange() {
        this.hourViewShow = true;
        this.MinutesViewShow = false;
        this.secondsViewShow = false;
        this.dayviewShow = false;
        this.monthviewShow = false;
    }
    dayChange() {
        this.dayviewShow = true;
        this.hourViewShow = false;
        this.MinutesViewShow = false;
        this.secondsViewShow = false;
        this.monthviewShow = false;
    }
    monthChange() {
        this.monthviewShow = true;
        this.hourViewShow = false;
        this.MinutesViewShow = false;
        this.secondsViewShow = false;
        this.dayviewShow = false;
    }
    everyMinutes(event) {
        console.log(event, "rrr");
        this.getmin = true;
        let everyMinute = event.value;
        console.log(everyMinute, ">>>everyMinute");
        this.cropExpressionSelection.everyminuteMain = everyMinute;
        // console.log(this.onPermSelectionOption.everyminuteMain);
    }
    everyMinuteSelect(e) {
        console.log(e, "defe");
        this.getmin = true;
        this.hourExp = false;
        this.dateCExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysExp = false;
        this.cronExpression = '*/' + e.value + ' * * * *';
        console.log(this.cronExpression);
    }
    everySecondOfMinutes(e) {
        console.log(e, "dddd");
        // this.getmin = false;
        // this.hourExp = true;
        this.everySecondOfMinutesExp = e.value;
        console.log(this.everySecondOfMinutesExp);
    }
    selectHour(e) {
        this.getmin = false;
        this.hourExp = true;
        this.dateCExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysExp = false;
        this.everHourExp = e.value;
        this.cronExpression = "0 0/" + e.value + ' * * *';
        console.log(this.cronExpression);
    }
    dateCounts(e) {
        this.dateCExp = true;
        this.getmin = false;
        this.hourExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysExp = false;
        this.dateCountDetailsExp = e.value;
        console.log(this.dateCountDetailsExp);
        let dayIdx = this.days.indexOf(this.daysCountsExp);
        this.cronExpression = "0 0 */" + e.value + " * " + dayIdx + '-6';
    }
    daysCounts(e) {
        this.daysExp = true;
        this.dateCExp = false;
        this.getmin = false;
        this.hourExp = false;
        this.monthViewfull = false;
        this.fullMonth = false;
        this.daysCountsExp = e.value;
        let dayIdx = this.days.indexOf(this.daysCountsExp);
        this.cronExpression = "0 0 */" + this.dateCountDetailsExp + " * " + dayIdx + '-6';
    }
    monthExp(e) {
        this.dateCExp = false;
        this.getmin = false;
        this.hourExp = false;
        this.daysExp = false;
        this.monthViewfull = true;
        this.fullMonth = true;
        this.monthexp = e.value;
        let mnthIdx = this.month.indexOf(this.monthexp);
        this.cronExpression = "0 0 1 " + mnthIdx + '/' + this.monthCountExp + " *";
    }
    monthCount(e) {
        this.dateCExp = false;
        this.daysExp = false;
        this.getmin = false;
        this.hourExp = false;
        this.monthViewfull = false;
        this.fullMonth = true;
        this.monthCountExp = e.value;
        let mnthIdx = this.month.indexOf(this.monthexp);
        this.cronExpression = "0 0 1 " + mnthIdx + '/' + e.value + " *";
    }
    onSelectionStartDay(event) { }
    SelectedDayCount(event) { }
}
FormLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "form-layout",
                template: "<div [ngClass]=\"classDiv\">\r\n  <!--WORK BY RAMYA-->\r\n  <form [formGroup]=\"inputGroup\" [ngClass]=\"dynamicFormHeight?dynamicFormHeight:initialHeight\"\r\n    class=\"sentri-form-height\" fxflex=\"1 1 auto\">\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"enableOptionFields\">\r\n      <div *ngFor=\"let optionField of formValues.chooseOneFields\"\r\n        style=\"display: flex;margin-top:12px;margin-bottom:20px;\">\r\n        <div *ngIf=\"optionField.label\" class=\"mr-8\" [ngClass]=\"optionField.widthBase\" style=\"width: 20%;\">\r\n          <mat-label class=\"mr-8 mr-res-5btm\"\r\n            style=\"font-size : 14px;padding: 0px 0px;margin-top:12px;margin-bottom:10px\">\r\n            {{optionField.label | translate}}:\r\n          </mat-label>\r\n        </div>\r\n        <mat-radio-group (change)=\"onOptionChange($event, optionField)\" [value]=\"inputData[optionField.name]\" required>\r\n          <mat-radio-button [ngClass]=\"fieldData.resWidth\"\r\n            [ngStyle]=\"{'width': fieldData.width, 'padding' : fieldData.padding}\"\r\n            style=\"width: 40%; padding: 0 0 10px 60px;\" *ngFor=\"let fieldData of optionField.fields\"\r\n            value=\"{{fieldData[optionField.keyToSave] | translate}}\"><span\r\n              class=\"p-4\">{{fieldData[optionField.keyToShow] | translate}}</span>&nbsp;&nbsp;\r\n          </mat-radio-button>\r\n        </mat-radio-group>\r\n\r\n        <p *ngIf=\"optionField.validations && submitted && inputGroup.get(optionField.name) && inputGroup.get(optionField.name).invalid\"\r\n          style=\"padding-top: 14px;\">\r\n          <mat-error class=\"error_margin\">\r\n            {{optionField.validations.message | translate}}\r\n          </mat-error>\r\n        </p>\r\n      </div>\r\n    </div>\r\n    <!-- <br> -->\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formHeader\">\r\n      <div class=\"h2 mb-6 ui-common-lib-f17\">{{formValues.formHeader | translate}}</div>\r\n    </div>\r\n    <!-- <div class=\"row\" md-content layout-padding *ngIf=\"enableTextFields\"> -->\r\n\r\n    <!--form new layout  -->\r\n    <div [ngClass]=\"formValues.listDetail ? 'sen-lib-list-view' : 'row'\" >\r\n      <!-- formtext -->\r\n      <ng-container *ngIf=\"enableTextFields\">\r\n        <ng-container *ngFor=\"let textField of formValues.textFields\">\r\n          <div class=\"col-md-6\">\r\n            <mat-form-field class=\"full-width\" appearance=\"outline\">\r\n              <mat-label>{{textField.label | translate}}</mat-label>\r\n              <input matInput [(ngModel)]=\"inputData[textField.name]\" [maxlength]=\"textField.maxlength\"\r\n                (input)=\"onSearchName($event.target.value, textField)\" [formControlName]=\"textField.name\"\r\n                [type]=\"textField.inputType\" [required]=\"textField.validations\" [readonly]=\"textField.isReadonly\">\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"!isNameAvailable\">\r\n                {{textField.existNameMessage | translate}}\r\n              </mat-error>\r\n\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.validations && submitted && inputGroup.get(textField.name).invalid\">\r\n                {{textField.validations.message | translate}}\r\n              </mat-error>\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.inputType=='email' && inputGroup.get(textField.name).invalid && (inputGroup.get(textField.name).dirty || inputGroup.get(textField.name).touched)\">\r\n                {{textField.emailValidate | translate}}\r\n              </mat-error>\r\n            </p>\r\n\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- end text -->\r\n      <ng-container *ngIf=\"enableSelectFields\">\r\n        <ng-container *ngFor=\"let selectField of formValues.selectFields\">\r\n          <div class=\"col-md-6\">\r\n            <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n              {{selectField.sideLabel| translate}}</mat-label>\r\n            <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n              {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n            <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{selectField.label |translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                [required]=\"selectField.validations\" [disabled]=\"selectField.disable\">\r\n                <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                  value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                  {{item[selectField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                {{selectField.validations.message | translate}}\r\n              </mat-error>\r\n            </p>\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"formValues.autoCompleteFields\">\r\n        <ng-container *ngFor=\"let inputField of formValues.autoCompleteFields; let i = index;\">\r\n          <div class=\"col\" [ngStyle]=\"{'width': inputField.width}\">\r\n            <mat-form-field appearance=\"outline\" class=\"full-width\" style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{inputField.label | translate}}</mat-label>\r\n              <input matInput [formControlName]=\"inputField.name\"\r\n                (input)=\"onAutoCompleteSearch($event.target.value,inputField)\" [disabled]=\"inputField.disable\"\r\n                [matAutocomplete]=\"auto\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)='onSelectAutoComplete($event.option.value, i, inputField)'>\r\n                <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.dataKeyToSave]\"\r\n                  [value]=\"fieldValue[inputField.keyToSave]\">\r\n                  {{fieldValue[inputField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <!-- <ng-container *ngIf=\"formValues.enableAddButton\">\r\n              <button mat-mini-fab (click)=\"addNewValue()\">\r\n                <mat-icon class=\"white-icon\">add</mat-icon>\r\n              </button>\r\n            </ng-container> -->\r\n          </div>\r\n        </ng-container>\r\n      </ng-container>\r\n      <ng-container *ngIf=\"formValues.enableAddButton\">\r\n        <button mat-mini-fab [ngClass]=\"formValues.UIaddButtonClass\" (click)=\"addNewValue()\">\r\n          <mat-icon class=\"white-icon\">add</mat-icon>\r\n        </button>\r\n      </ng-container>\r\n      <div layout=\"row\" class=\"col-12\" md-content layout-padding\r\n        *ngIf=\"formValues.additionalOptionFields && formValues.additionalOptionFields.length\">\r\n        <div *ngFor=\"let optionField of formValues.additionalOptionFields\"\r\n          style=\"display: flex;margin-top:12px;margin-bottom:12px;\">\r\n          <div *ngIf=\"optionField.label\" class=\"mr-8\" [ngClass]=\"optionField.widthBase\" style=\"width: 20%;\">\r\n            <mat-label class=\"mr-8\" style=\"font-size : 14px;padding: 0px 0px;margin-top:12px;margin-bottom:10px\">\r\n              {{optionField.label | translate}}:\r\n            </mat-label>\r\n          </div>\r\n          <mat-radio-group (change)=\"onOptionChange($event, optionField)\" [value]=\"inputData[optionField.name]\"\r\n            required>\r\n            <mat-radio-button *ngFor=\"let fieldData of optionField.fields\"\r\n              value=\"{{fieldData[optionField.keyToSave] | translate}}\"><span\r\n                class=\"p-4\">{{fieldData[optionField.keyToShow] | translate}}</span>&nbsp;&nbsp;\r\n            </mat-radio-button>\r\n          </mat-radio-group>\r\n\r\n          <p *ngIf=\"optionField.validations && submitted && inputGroup.get(optionField.name) && inputGroup.get(optionField.name).invalid\"\r\n            style=\"padding-top: 14px;\">\r\n            <mat-error class=\"error_margin\">\r\n              {{optionField.validations.message | translate}}\r\n            </mat-error>\r\n          </p>\r\n        </div>\r\n      </div>\r\n      <!-- enable select fields -->\r\n      \r\n      <!-- multi -->\r\n      <ng-container *ngIf=\"enableMultiselectFields\">\r\n        <ng-container *ngFor=\"let selectField of formValues.multiSelectFields\">\r\n          <ng-container>\r\n            <!-- <td class=\"column\" > -->\r\n            <div class=\"col-6\">\r\n              <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n                {{selectField.sideLabel| translate}}</mat-label>\r\n              <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n                {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n              <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n                <mat-label>{{selectField.label |translate}}</mat-label>\r\n                <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                  [required]=\"selectField.validations\" multiple>\r\n                  <mat-option *ngFor=\"let item of selectField.data\" value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                    {{item[selectField.keyToShow] | translate}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>\r\n              <p class=\"errorP\">\r\n                <mat-error class=\"error_margin\"\r\n                  *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                  {{selectField.validations.message | translate}}\r\n                </mat-error>\r\n              </p>\r\n            </div>\r\n            <!-- </td> -->\r\n          </ng-container>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- chiplist -->\r\n      <ng-container *ngIf=\"formValues.chipFields && formValues.chipFields.length > 0 \">\r\n        <ng-container *ngFor=\"let chipField of formValues.chipFields\">\r\n          <div class=\"col-md-6\" [ngClass]=\"chipField.addFull\">\r\n            <mat-form-field appearance=\"outline\" [ngClass]=\"chipField.addFull ? 'half-width' : 'full-width'\"\r\n              style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{chipField.label | translate}}</mat-label>\r\n              <!-- <p>{{selectedChips[chipField.name] | json}}</p> -->\r\n              <input matInput #chipInput [formControlName]=\"chipField.name\"\r\n                *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable\"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <input matInput #chipInput [formControlName]=\"chipField.name\" *ngIf=\"!chipField.chipCondition \"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)=\"onChipSelect($event, chipField.name, chipField.keyToSave,chipField.chipCondition)\">\r\n                <mat-option *ngFor=\"let item of chipField.data \" [value]=\"item\">\r\n                  {{item[chipField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"chipField.validations && submitted && chipValiditaion\">\r\n                {{chipField.validations.message | translate}}\r\n              </mat-error>\r\n            </p>\r\n            <mat-chip-list #chipList>\r\n              <ng-container\r\n                *ngIf=\"!chipField.accordion && inputData.selectedChips && inputData.selectedChips[chipField.name] && inputData.selectedChips[chipField.name].length\">\r\n                <div *ngFor=\"let chip of inputData.selectedChips[chipField.name]; let i = index;\">\r\n                  <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n                    (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n                    {{chip[chipField.keyToShow]}}\r\n                    <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                  </mat-chip>\r\n                  <span\r\n                    *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n                  <span\r\n                    *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n                </div>\r\n              </ng-container>\r\n            </mat-chip-list>\r\n\r\n            <mat-accordion\r\n              *ngIf=\"chipField.accordion && inputData.selectedChips && inputData.selectedChips[chipField.name] && inputData.selectedChips[chipField.name].length\">\r\n              <mat-expansion-panel [expanded]=\"panelOpenState === true\" (opened)=\"panelOpenState = true\"\r\n                (closed)=\"panelOpenState = false\" *ngIf=\"chipListOpenView\" [class.sentri-active]=\"chipField.accordion\">\r\n                <mat-expansion-panel-header>\r\n                  <mat-panel-title>\r\n                    Selected Roles\r\n                  </mat-panel-title>\r\n                  <mat-panel-description>\r\n\r\n                  </mat-panel-description>\r\n                </mat-expansion-panel-header>\r\n                <mat-chip-list #chipList>\r\n                  <div *ngFor=\"let chip of inputData.selectedChips[chipField.name]; let i = index;\">\r\n                    <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n                      (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n                      {{chip[chipField.keyToShow]}}\r\n                      <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                    </mat-chip>\r\n                    <span\r\n                      *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n                    <span\r\n                      *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== inputData.selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n                  </div>\r\n                </mat-chip-list>\r\n              </mat-expansion-panel>\r\n            </mat-accordion>\r\n          </div>\r\n\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- end -->\r\n\r\n      <!-- end  -->\r\n      <!-- confirmation section -->\r\n      <div class=\"col-md-12\">\r\n        <ng-container *ngIf=\"formValues.confirmData\">\r\n          <ng-container *ngIf=\"formValues.confirmDetailsHeading\">\r\n            <h6 class=\"mb-6 ui-common-lib-confirmation-header\">{{formValues.confirmDetailsHeading | translate}}</h6>\r\n          </ng-container>\r\n          <div class=\"row\">\r\n            <ng-container *ngFor=\"let confirmField of formValues.confirmData\">\r\n              <div class=\"col-md-6\">\r\n                <p style=\"padding-left: 12px;padding-top: 12px;\">\r\n                  <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}:</b> </mat-label>\r\n                  <!-- <span *ngIf=\"confirmField.type == 'date'\"\r\n                    style=\"padding-left: 5px;\">{{viewData[confirmField.value] | date:'short'}}</span> -->\r\n                    <span *ngIf=\"confirmField.type == 'date' && confirmField.dateFormat\"\r\n                    style=\"padding-left: 5px;\">\r\n                    {{viewData[confirmField.value] | date: confirmField.dateFormat}}\r\n                   </span>\r\n                   <span *ngIf=\"confirmField.type == 'date' && !confirmField.dateFormat\"\r\n                    style=\"padding-left: 5px;\">\r\n                    {{viewData[confirmField.value] | date:'short'}}\r\n                   </span>\r\n                  <span *ngIf=\"confirmField.type != 'date'\"\r\n                    style=\"padding-left: 5px;\">{{viewData[confirmField.value]}}</span>\r\n                </p>\r\n              </div>\r\n            </ng-container>\r\n          </div>\r\n          <!-- <td class=\"column\" style=\"padding: 10px;\" *ngFor=\"let confirmField of formValues.confirmData\">\r\n            <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}</b> </mat-label>\r\n            <span *ngIf=\"confirmField.type == 'date'\"\r\n              style=\"padding: 10%;\">{{viewData[confirmField.value] | date:'short'}}</span>\r\n            <span *ngIf=\"confirmField.type != 'date'\" style=\"padding: 10%;\">{{viewData[confirmField.value]}}</span>\r\n            <br>\r\n          </td> -->\r\n        </ng-container>\r\n      </div>\r\n      <!-- end conf -->\r\n      <!-- schedule form view design -->\r\n      <ng-container *ngIf=\"formValues.scheduleFields\">\r\n        <div class=\"col-md-6\">\r\n          <mat-form-field class=\"full-width\" appearance=\"outline\" floatLabel=\"always\" style=\"padding-right: 1.7%;\">\r\n            <!-- <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\"> -->\r\n            <mat-label>{{scheduleField.startDateLabel | translate}}</mat-label>\r\n            <input matInput [matDatepicker]=\"startDatePicker\" [(ngModel)]=\"inputData[scheduleField.startDate]\"\r\n              (dateChange)=\"validateDate('change', $event,scheduleField)\" required\r\n              [formControlName]=\"scheduleField.startDate\" name=\"startdatetime\" [min]=\"minDate\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"startDatePicker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #startDatePicker></mat-datepicker>\r\n          </mat-form-field>\r\n          <p class=\"errorP\">\r\n            <mat-error class=\"error_margin\" *ngIf=\"formValues.scheduleFields.validations && !dateFormatcheck\">\r\n              {{formValues.scheduleFields.validations.datemessage | translate}}\r\n            </mat-error>\r\n          </p>\r\n        </div>\r\n        <div class=\"col-md-3\">\r\n          <mat-form-field class=\"full-width\" appearance=\"outline\" class=\"pr-4\" floatLabel=\"always\" ngDefaultControl>\r\n            <!-- <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\"> -->\r\n            <mat-label>{{scheduleField.timeLabel | translate}}</mat-label>\r\n            <input matInput type=\"time\" [(ngModel)]=\"inputData[scheduleField.time]\"\r\n              [formControlName]=\"scheduleField.time\" min=\"01:00\" max=\"12:00\">\r\n          </mat-form-field>\r\n        </div>\r\n        <div class=\"col-md-3 sentri-responsive-col-3\">\r\n          <mat-checkbox style=\"position: relative;top:25px;\" (change)=\"showRepeatData($event)\">\r\n            {{scheduleField.repeatLabel | translate}}</mat-checkbox>\r\n        </div>\r\n        <ng-container *ngIf=\"enableRepeatData\">\r\n          <div class=\"col-md-9\">\r\n            <mat-form-field class=\"full-width\" appearance=\"outline\" style=\"padding-top: 3%;\">\r\n              <mat-label>{{scheduleField.selectRepeatLabel | translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData.repeatType\" [formControlName]=\"scheduleField.repeatType\">\r\n                <mat-option *ngFor=\"let itemData of scheduleField.repeatData\" value=\"{{itemData.value}}\"\r\n                  (click)=\"onRepeatSelectChange(itemData.value, scheduleField.repeatData)\">\r\n                  {{itemData.name | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>Repeats is required!</mat-error>\r\n            </mat-form-field>\r\n            <ng-container *ngIf=\"inputData.weekly\">\r\n              <mat-checkbox *ngFor=\"let days of scheduleField.weekDaysData; let i = index\" class=\"example-margin\"\r\n                value=\"{{days.value}}\" [formControlName]=\"scheduleField.weeklyDays\"\r\n                (change)=\"getSelecteddays($event,days.value)\" id=\"checkbox_{{i}}\"\r\n                style=\"padding-right: 0px;padding-left: 10px;\">\r\n                {{days.name}}\r\n              </mat-checkbox>\r\n            </ng-container>\r\n          </div>\r\n          <div class=\"col-md-6\" *ngIf=\"inputData.daily\">\r\n            <ng-container>\r\n              <mat-radio-group floatLabel=\"always\" [formControlName]=\"scheduleField.repeatByDay\">\r\n                <mat-radio-button value=\"day\" (click)=\"onSelectionStartDay('day')\">\r\n                  <span style=\"margin-top: 8%;\">{{scheduleField.everyLabel | translate}}</span>&nbsp;&nbsp;\r\n                  <mat-form-field class=\"full-width\">\r\n                    <mat-label>{{scheduleField.daysLabel | translate}}</mat-label>\r\n                    <input matInput [formControlName]=\"scheduleField.repeatDaysCount\"\r\n                      (change)=\"SelectedDayCount($event)\">\r\n                  </mat-form-field>&nbsp;&nbsp;<span\r\n                    style=\"margin-top: 8%;\">{{scheduleField.daysLabel | translate}}</span>\r\n                </mat-radio-button>\r\n                <mat-radio-button value=\"week\" (click)=\"onSelectionStartDay('week')\">\r\n                  {{scheduleField.weekDayLabel | translate}}&nbsp;&nbsp;\r\n                </mat-radio-button>\r\n              </mat-radio-group>\r\n            </ng-container>\r\n          </div>\r\n          <div class=\"col-md-12\" *ngIf=\"inputData.monthly\">\r\n            <ng-container>\r\n              <span style=\"margin-top: 4%;\">{{scheduleField.dayLabel | translate}}23</span>&nbsp;&nbsp;<mat-form-field\r\n                appearance=\"outline\">\r\n                <mat-label></mat-label>\r\n                <input matInput [formControlName]=\"scheduleField.monthlyDate\">\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.everyofLabel | translate}}</span>&nbsp;&nbsp;\r\n              <mat-form-field appearance=\"outline\" style=\"width:20%;\">\r\n                <mat-label>Select</mat-label>\r\n                <mat-select placeholder=\"Select\" [formControlName]=\"scheduleField.selectMonth\">\r\n                  <mat-option *ngFor=\"let month of scheduleField.monthListData\" value=\"{{month.value}}\">\r\n                    {{month.name}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.monthsLabel | translate}}</span>\r\n            </ng-container>\r\n          </div>\r\n          <div class=\"col-md-6 sen-lib-responsive-enddate\">\r\n            <!-- <p class=\"mr-8\">{{scheduleField.endsLabel | translate}}</p> -->\r\n            <mat-radio-group floatLabel=\"always\" style=\"position: relative;top:13px;\"\r\n              [formControlName]=\"scheduleField.endDate\">\r\n              <mat-radio-button value=\"no_end_date\">\r\n                {{scheduleField.noEndDate | translate}}&nbsp;&nbsp;\r\n              </mat-radio-button>\r\n              <br>\r\n              <mat-radio-button value=\"until\" style=\"margin-top: 3%;\" (click)=\"showEndDate = true\">\r\n                <span style=\"margin-top: 8%;\">{{scheduleField.untillLabel | translate}}</span>&nbsp;&nbsp;\r\n                <mat-form-field appearance=\"outline\" floatLabel=\"always\" class=\"mr-sm-12\" fxFlex=\"45\" ngDefaultControl\r\n                  *ngIf=\"showEndDate\">\r\n                  <mat-label>{{scheduleField.endDateLabel | translate}}</mat-label>\r\n                  <input matInput [matDatepicker]=\"endDatePicker\" [formControlName]=\"scheduleField.endDateTime\"\r\n                    [min]=\"minEndDate\">\r\n                  <mat-datepicker-toggle matSuffix [for]=\"endDatePicker\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #endDatePicker></mat-datepicker>\r\n                </mat-form-field>\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div>\r\n        </ng-container>\r\n\r\n      </ng-container>\r\n      <ng-container *ngIf=\"formValues.listDetail\">\r\n        <ng-container *ngFor=\"let item of inputData[formValues.keyToShow]\">\r\n          <ul style=\"padding-left: 15px;\">\r\n            <li>\r\n              {{item[formValues.keyToDisplay]}}\r\n            </li>\r\n          </ul>\r\n        </ng-container>\r\n      </ng-container>\r\n      <!-- end schedule view -->\r\n    </div>\r\n    <!-- end new layout -->\r\n\r\n\r\n    <table class=\"full-width\" cellspacing=\"0\">\r\n      <tr class=\"row\">\r\n        <!-- <ng-container *ngIf=\"formValues.autoCompleteFields\">\r\n          <td class=\"column\" [ngStyle]=\"{'width': inputField.width}\"\r\n            *ngFor=\"let inputField of formValues.autoCompleteFields; let i = index;\">\r\n            <mat-form-field appearance=\"outline\" class=\"full-width\" style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{inputField.label | translate}}</mat-label>\r\n              <input matInput [formControlName]=\"inputField.name\"\r\n                (input)=\"onAutoCompleteSearch($event.target.value,inputField)\" [disabled]=\"inputField.disable\"\r\n                [matAutocomplete]=\"auto\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)='onSelectAutoComplete($event.option.value, i, inputField)'>\r\n                <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.dataKeyToSave]\"\r\n                  [value]=\"fieldValue[inputField.keyToSave]\">\r\n                  {{fieldValue[inputField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n          </td>\r\n          <div fxlayout=\"row\" fxlayoutalign=\"start center\" *ngIf=\"formValues.enableAddButton\">\r\n            <button mat-mini-fab (click)=\"addNewValue()\">\r\n              <mat-icon class=\"white-icon\">add</mat-icon>\r\n            </button>\r\n          </div>\r\n        </ng-container> -->\r\n        <ng-container *ngIf=\"enableTextFields\">\r\n          <td class=\"column\" *ngFor=\"let textField of formValues.textFields\">\r\n            <!-- <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{textField.label | translate}}</mat-label>\r\n              <input matInput [(ngModel)]=\"inputData[textField.name]\" [maxlength]=\"textField.maxlength\"\r\n                (input)=\"onSearchName($event.target.value, textField)\" [formControlName]=\"textField.name\"\r\n                [type]=\"textField.inputType\" [required]=\"textField.validations\" [readonly]=\"textField.isReadonly\">\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"!isNameAvailable\">\r\n                {{textField.existNameMessage | translate}}\r\n              </mat-error>\r\n              \r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.validations && submitted && inputGroup.get(textField.name).invalid\">\r\n                {{textField.validations.message | translate}}\r\n              </mat-error>\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"textField.inputType=='email' && inputGroup.get(textField.name).invalid && (inputGroup.get(textField.name).dirty || inputGroup.get(textField.name).touched)\">\r\n                {{textField.emailValidate | translate}}\r\n              </mat-error>\r\n            </p> -->\r\n\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"formValues.enableSlideToggle\">\r\n          <div layout=\"row\" class=\"slide-toggle\" md-content layout-padding>\r\n            {{formValues.toggleLabel | translate}}\r\n            <mat-slide-toggle (change)=\"toggleChanged($event)\" class=\"slide-toggle-padding\">\r\n            </mat-slide-toggle>\r\n          </div>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"enableSelectFields\">\r\n          <td class=\"column\" *ngFor=\"let selectField of formValues.selectFields\">\r\n            <!-- <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n              {{selectField.sideLabel| translate}}</mat-label>\r\n            <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n              {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n            <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{selectField.label |translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                [required]=\"selectField.validations\">\r\n                <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                  value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                  {{item[selectField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                {{selectField.validations.message | translate}}\r\n              </mat-error>\r\n            </p> -->\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"enableMultiselectFields\">\r\n          <td class=\"column\" *ngFor=\"let selectField of formValues.multiSelectFields\">\r\n            <!-- <mat-label *ngIf=\"selectField.showSideLabel && selectField.sideLabel\" class=\"mr-8\">\r\n              {{selectField.sideLabel| translate}}</mat-label>\r\n            <mat-label #sideLabel class=\"mr-8\" *ngIf=\"selectField.showSideLabel && !selectField.sideLabel\">\r\n              {{selectField.sideLabel?selectField.sideLabel: selectField.label | translate}}</mat-label>\r\n            <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;\" appearance=\"outline\">\r\n              <mat-label>{{selectField.label |translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n                [required]=\"selectField.validations\" multiple>\r\n                <mat-option *ngFor=\"let item of selectField.data\" value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                  {{item[selectField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\"\r\n                *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                {{selectField.validations.message | translate}}\r\n              </mat-error>\r\n            </p> -->\r\n          </td>\r\n        </ng-container>\r\n        <ng-container *ngIf=\"formValues.confirmData\">\r\n          <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.confirmDetailsHeading\">\r\n            <div class=\"h2 mb-6\">{{formValues.confirmDetailsHeading | translate}}</div>\r\n          </div>\r\n          <td class=\"column\" style=\"padding: 10px;\" *ngFor=\"let confirmField of formValues.confirmData\">\r\n            <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}</b> </mat-label>\r\n            <span *ngIf=\"confirmField.type == 'date'\"\r\n              style=\"padding: 10%;\">{{viewData[confirmField.value] | date:'short'}}</span>\r\n            <span *ngIf=\"confirmField.type != 'date'\" style=\"padding: 10%;\">{{viewData[confirmField.value]}}</span>\r\n            <br>\r\n          </td> -->\r\n        </ng-container>\r\n        <ng-container *ngIf=\"formValues.chipFields\">\r\n          <td class=\"column\" *ngFor=\"let chipField of formValues.chipFields\">\r\n            <!-- <mat-form-field appearance=\"outline\" class=\"full-width\" style=\"padding: 0 5px 0 0;\">\r\n              <mat-label>{{chipField.label | translate}}</mat-label>\r\n            \r\n              <input matInput #chipInput [formControlName]=\"chipField.name\"\r\n                *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable\"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <input matInput #chipInput [formControlName]=\"chipField.name\" *ngIf=\"!chipField.chipCondition \"\r\n                (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\"\r\n                [matChipInputFor]=\"chipList\" (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n              <mat-autocomplete #auto=\"matAutocomplete\"\r\n                (optionSelected)=\"onChipSelect($event, chipField.name, chipField.keyToSave,chipField.chipCondition)\">\r\n                <mat-option *ngFor=\"let item of chipField.data \" [value]=\"item\">\r\n                  {{item[chipField.keyToShow] | translate}}\r\n                </mat-option>\r\n              </mat-autocomplete>\r\n            </mat-form-field>\r\n            <p class=\"errorP\">\r\n              <mat-error class=\"error_margin\" *ngIf=\"chipField.validations && submitted && chipValiditaion\">\r\n                {{chipField.validations.message | translate}}\r\n              </mat-error>\r\n            </p>\r\n            <mat-chip-list #chipList>\r\n              <div *ngFor=\"let chip of selectedChips[chipField.name]; let i = index;\">\r\n                <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n                  (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n                  {{chip[chipField.keyToShow]}}\r\n                  <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n                </mat-chip>\r\n                <span\r\n                  *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n                <span\r\n                  *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n              </div>\r\n            </mat-chip-list> -->\r\n          </td>\r\n        </ng-container>\r\n      </tr>\r\n    </table>\r\n    <!-- </div> -->\r\n    <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"enableSelectFields\">\r\n    <div *ngFor=\"let selectField of formValues.selectFields\" fxFlex.gt-sm=\"50\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n      <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"30\" *ngIf=\"selectField.showSideLabel\" fxLayoutAlign=\"center\"\r\n        class=\"mr-8\">\r\n        <div layout=\"row\">\r\n          <mat-label class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n        </div>\r\n      </div>\r\n      <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" style=\"display: inline;\">\r\n        <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n          <mat-label>{{selectField.label |translate}}</mat-label>\r\n          <mat-select [(ngModel)]=\"inputData[selectField.name]\" [formControlName]=\"selectField.name\"\r\n            [required]=\"selectField.validations\">\r\n            <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(selectField, item, selectField)\"\r\n              value=\"{{item[selectField.keyToShow] | translate}}\">\r\n              {{item[selectField.keyToShow] | translate}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n        <p *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n          <mat-error class=\"error_margin\">\r\n            {{selectField.validations.message | translate}}\r\n          </mat-error>\r\n        </p>\r\n      </div>\r\n    </div>\r\n  </div> -->\r\n    <!-- <div class=\"row\" md-content layout-padding *ngIf=\"enableChipFields\">\r\n    <div class=\"column\" *ngFor=\"let chipField of formValues.chipFields\">\r\n      <mat-form-field appearance=\"outline\" class=\"custom-mat-form\">\r\n        <mat-label>{{chipField.label | translate}}</mat-label>\r\n        <input matInput #chipInput [formControlName]=\"chipField.name\"\r\n          *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable\"\r\n          (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\" [matChipInputFor]=\"chipList\"\r\n          (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n        <input matInput #chipInput [formControlName]=\"chipField.name\" *ngIf=\"!chipField.chipCondition \"\r\n          (input)=\"onChipSearch($event.target.value, chipField)\" [matAutocomplete]=\"auto\" [matChipInputFor]=\"chipList\"\r\n          (matChipInputTokenEnd)=\"addChipItem($event)\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\"\r\n          (optionSelected)=\"onChipSelect($event, chipField.name, chipField.keyToSave,chipField.chipCondition)\">\r\n          <mat-option *ngFor=\"let item of chipField.data \" [value]=\"item\">\r\n            {{item[chipField.keyToShow] | translate}}\r\n          </mat-option>\r\n        </mat-autocomplete>\r\n      </mat-form-field>\r\n      <p class=\"errorP\">\r\n        <mat-error class=\"error_margin\"\r\n          *ngIf=\"chipField.validations && submitted && chipValiditaion\">\r\n          {{chipField.validations.message | translate}}\r\n        </mat-error>\r\n      </p>\r\n      <mat-chip-list #chipList>\r\n        <div *ngFor=\"let chip of selectedChips[chipField.name]; let i = index;\">\r\n          <mat-chip role=\"option\" [selectable]=\"enableSelectable\" [removable]=\"enableRemovable\"\r\n            (removed)=\"removeSelectedChip(chip, chipField.name, chipField.keyToSave)\" id=\"matchipID\">\r\n            {{chip[chipField.keyToShow]}}\r\n            <mat-icon matChipRemove *ngIf=\"removable\">cancel</mat-icon>\r\n          </mat-chip>\r\n          <span\r\n            *ngIf=\"chipField.chipCondition && chipField.chipCondition.variable && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{ChipOperator | translate}}</span>\r\n          <span\r\n            *ngIf=\"!chipField.chipCondition && (chipField.operator && i !== selectedChips[chipField.name].length-1)\">{{chipField.operator | translate}}</span>\r\n        </div>\r\n      </mat-chip-list>\r\n    </div>\r\n  </div> -->\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.scheduleFields\">\r\n      <div>\r\n        <div>\r\n          <!-- <mat-form-field appearance=\"outline\" floatLabel=\"always\" fxFlex=\"60\" style=\"padding-right: 1.7%;\">\r\n            <mat-label>{{scheduleField.startDateLabel | translate}}</mat-label>\r\n            <input matInput [matDatepicker]=\"startDatePicker\" [(ngModel)]=\"inputData[scheduleField.startDate]\"\r\n              (dateChange)=\"validateDate('change', $event,scheduleField)\" required\r\n              [formControlName]=\"scheduleField.startDate\" name=\"startdatetime\" [min]=\"minDate\">\r\n            <mat-datepicker-toggle matSuffix [for]=\"startDatePicker\"></mat-datepicker-toggle>\r\n            <mat-datepicker #startDatePicker></mat-datepicker>\r\n          </mat-form-field>\r\n\r\n          <mat-form-field appearance=\"outline\" fxFlex=23 class=\"pr-4\" floatLabel=\"always\" ngDefaultControl>\r\n            <mat-label>{{scheduleField.timeLabel | translate}}</mat-label>\r\n            <input matInput type=\"time\" [(ngModel)]=\"inputData[scheduleField.time]\"\r\n              [formControlName]=\"scheduleField.time\" min=\"01:00\" max=\"12:00\">\r\n          </mat-form-field> -->\r\n        </div>\r\n\r\n        <!-- <p class=\"errorP\">\r\n          <mat-error class=\"error_margin\" *ngIf=\"formValues.scheduleFields.validations && !dateFormatcheck\">\r\n            {{formValues.scheduleFields.validations.datemessage | translate}}\r\n          </mat-error>\r\n        </p>\r\n\r\n        <div>\r\n          <mat-checkbox (change)=\"showRepeatData($event)\">{{scheduleField.repeatLabel | translate}}</mat-checkbox>\r\n        </div> -->\r\n\r\n        <div layout=\"row\" md-content layout-padding *ngIf=\"enableRepeatData\">\r\n          <!-- <div>\r\n            <mat-form-field appearance=\"outline\" fxFlex=\"85\" style=\"padding-top: 3%;\">\r\n              <mat-label>{{scheduleField.selectRepeatLabel | translate}}</mat-label>\r\n              <mat-select [(ngModel)]=\"inputData.repeatType\" [formControlName]=\"scheduleField.repeatType\">\r\n                <mat-option *ngFor=\"let itemData of scheduleField.repeatData\" value=\"{{itemData.value}}\"\r\n                  (click)=\"onRepeatSelectChange(itemData.value, scheduleField.repeatData)\">\r\n                  {{itemData.name | translate}}\r\n                </mat-option>\r\n              </mat-select>\r\n              <mat-error>Repeats is required!</mat-error>\r\n            </mat-form-field>\r\n          </div>\r\n          <div>\r\n            <mat-radio-group floatLabel=\"always\" *ngIf=\"inputData.daily\" [formControlName]=\"scheduleField.repeatByDay\">\r\n              <mat-radio-button value=\"day\" (click)=\"onSelectionStartDay('day')\">\r\n                <span style=\"margin-top: 8%;\">{{scheduleField.everyLabel | translate}}</span>&nbsp;&nbsp;<mat-form-field\r\n                  appearance=\"outline\" fxFlex=\"20\">\r\n                  <mat-label>{{scheduleField.daysLabel | translate}}</mat-label>\r\n                  <input matInput [formControlName]=\"scheduleField.repeatDaysCount\" (change)=\"SelectedDayCount($event)\">\r\n                </mat-form-field>&nbsp;&nbsp;<span\r\n                  style=\"margin-top: 8%;\">{{scheduleField.daysLabel | translate}}</span>\r\n              </mat-radio-button>\r\n              <br>\r\n              <mat-radio-button value=\"week\" (click)=\"onSelectionStartDay('week')\">\r\n                {{scheduleField.weekDayLabel | translate}}&nbsp;&nbsp;\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n            <div *ngIf=\"inputData.weekly\">\r\n              <mat-checkbox *ngFor=\"let days of scheduleField.weekDaysData; let i = index\" class=\"example-margin\"\r\n                value=\"{{days.value}}\" [formControlName]=\"scheduleField.weeklyDays\"\r\n                (change)=\"getSelecteddays($event,days.value)\" id=\"checkbox_{{i}}\"\r\n                style=\"padding-right: 0px;padding-left: 10px;\">\r\n                {{days.name}}\r\n              </mat-checkbox>\r\n            </div>\r\n            <div *ngIf=\"inputData.monthly\">\r\n              <span style=\"margin-top: 4%;\">{{scheduleField.dayLabel | translate}}</span>&nbsp;&nbsp;<mat-form-field\r\n                appearance=\"outline\" fxFlex=\"28\">\r\n                <mat-label></mat-label>\r\n                <input matInput [formControlName]=\"scheduleField.monthlyDate\">\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.everyofLabel | translate}}</span>&nbsp;&nbsp;\r\n              <mat-form-field appearance=\"outline\" fxFlex=\"28\">\r\n                <mat-label>Select</mat-label>\r\n                <mat-select placeholder=\"Select\" [formControlName]=\"scheduleField.selectMonth\">\r\n                  <mat-option *ngFor=\"let month of scheduleField.monthListData\" value=\"{{month.value}}\">\r\n                    {{month.name}}\r\n                  </mat-option>\r\n                </mat-select>\r\n              </mat-form-field>&nbsp;&nbsp;<span\r\n                style=\"margin-top: 4%;\">{{scheduleField.monthsLabel | translate}}</span>\r\n            </div>\r\n          </div>\r\n          <br>\r\n          <div>\r\n            <mat-label class=\"mr-8\">{{scheduleField.endsLabel | translate}}</mat-label>&nbsp;\r\n            <br>\r\n            <mat-radio-group floatLabel=\"always\" [formControlName]=\"scheduleField.endDate\">\r\n              <mat-radio-button value=\"no_end_date\">\r\n                {{scheduleField.noEndDate | translate}}&nbsp;&nbsp;\r\n              </mat-radio-button>\r\n              <br>\r\n              <mat-radio-button value=\"until\" style=\"margin-top: 3%;\" (click)=\"showEndDate = true\">\r\n                <span style=\"margin-top: 8%;\">{{scheduleField.untillLabel | translate}}</span>&nbsp;&nbsp;\r\n                <mat-form-field appearance=\"outline\" floatLabel=\"always\" class=\"mr-sm-12\" fxFlex=\"45\" ngDefaultControl\r\n                  *ngIf=\"showEndDate\">\r\n                  <mat-label>{{scheduleField.endDateLabel | translate}}</mat-label>\r\n                  <input matInput [matDatepicker]=\"endDatePicker\" [formControlName]=\"scheduleField.endDateTime\"\r\n                    [min]=\"minEndDate\">\r\n                  <mat-datepicker-toggle matSuffix [for]=\"endDatePicker\"></mat-datepicker-toggle>\r\n                  <mat-datepicker #endDatePicker></mat-datepicker>\r\n                </mat-form-field>\r\n              </mat-radio-button>\r\n            </mat-radio-group>\r\n          </div> -->\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.dateFields\">\r\n      <div fxLayout=\"column\" fxFlex=\"100\" *ngFor=\"let dateField of formValues.dateFields; let i = index;\"\r\n        fxFlex.gt-sm=\"50\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n        <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n          <mat-label>{{dateField.dateLabel | translate}}</mat-label>\r\n          <input matInput [matDatepicker]=\"i\" [formControlName]=\"dateField.dateName\" [min]=\"minDate\"\r\n            [(ngModel)]=\"inputData[dateField.dateName]\" *ngIf=\"dateField.dateName=='startDate' && !inputData['_id']\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n          <input matInput [matDatepicker]=\"i\" [formControlName]=\"dateField.dateName\"\r\n            [(ngModel)]=\"inputData[dateField.dateName]\" *ngIf=\"dateField.dateName=='startDate' && inputData['_id']\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n\r\n          <input *ngIf=\"dateField.dateName=='endDate'\" matInput [matDatepicker]=\"i\"\r\n            [formControlName]=\"dateField.dateName\" [(ngModel)]=\"inputData[dateField.dateName]\" [min]=\"minEndDate\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n          <input *ngIf=\"dateField.dateName!='endDate' && dateField.dateName!='startDate' \" matInput [matDatepicker]=\"i\"\r\n            [(ngModel)]=\"inputData[dateField.dateName]\" [formControlName]=\"dateField.dateName\"\r\n            (dateChange)=\"validateDate('change', $event,dateField)\" required>\r\n          <mat-datepicker-toggle matSuffix [for]=\"i\"></mat-datepicker-toggle>\r\n          <mat-datepicker #i></mat-datepicker>\r\n        </mat-form-field>\r\n        <p class=\"errorP\">\r\n          <!-- <mat-error class=\"error_margin\" *ngIf=\"!dateFormatcheck\">\r\n          {{dateField.validateDateFormat | translate}}\r\n        </mat-error> -->\r\n          <mat-error class=\"error_margin\"\r\n            *ngIf=\"dateField.validations && submitted && inputGroup.get(dateField.name).invalid\">\r\n            {{dateField.validations.message | translate}}\r\n          </mat-error>\r\n        </p>\r\n      </div>\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formArrayValues\">\r\n      <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.fieldSetHeader\">\r\n        <div class=\"h2 mb-6  ui-common-lib-f17\">{{formValues.fieldSetHeader | translate}}</div>\r\n      </div>\r\n      <div style=\"float: right; margin-bottom: -16px;margin-top: -25px;\" fxlayout=\"row\" fxlayoutalign=\"start center\"\r\n        *ngIf=\"!formValues.singleField\">\r\n        <button mat-mini-fab class=\"ui-common-lib-outline\" (click)=\"addNewFormArray(formValues.formFieldName)\">\r\n          <mat-icon class=\"white-icon\">add</mat-icon>\r\n        </button>\r\n      </div>\r\n      <div [formArrayName]=\"formValues.formFieldName\">\r\n        <div *ngFor=\"let comp of inputGroup.get(formValues.formFieldName)['controls']; let i=index\">\r\n\r\n          <fieldset>\r\n            <!-- <div *ngIf=\"formValues.singleField\">\r\n            <legend>  <h3>Condition : </h3></legend>\r\n           </div>\r\n           <div *ngIf=\"!formValues.singleField\">\r\n            <legend>  <h3>Condition {{i+1}}: </h3></legend>\r\n           </div> -->\r\n            <legend>\r\n              <span *ngIf=\"!formValues.singleField\">\r\n                <h3>Condition {{i+1}}:</h3>\r\n              </span>\r\n              <span *ngIf=\"formValues.singleField\">\r\n                <h3>Condition:</h3>\r\n              </span>\r\n\r\n            </legend>\r\n            <div [formGroupName]=\"i\">\r\n              <!-- condition form -->\r\n              <div class=\"row\">\r\n                <ng-container *ngIf=\"formValues.formArrayValues.selectFields\">\r\n                  <ng-container *ngFor=\"let selectField of formValues.formArrayValues.selectFields\">\r\n                    <div [ngClass]=\"(selectField && selectField.className) ? selectField.className : 'col-md-6'\">\r\n                      <ng-container *ngIf=\"selectField.showSideLabel\">\r\n                        <div>\r\n                          <mat-label class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n                        </div>\r\n                      </ng-container>\r\n                      <mat-form-field class=\"full-width\" appearance=\"outline\" class=\"custom-mat-form\">\r\n                        <mat-label>{{selectField.label |translate}}</mat-label>\r\n                        <mat-select [formControlName]=\"selectField.name\" [required]=\"selectField.validations\">\r\n                          <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                            value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                            {{item[selectField.keyToShow] | translate}}\r\n                          </mat-option>\r\n                        </mat-select>\r\n                      </mat-form-field>\r\n                    </div>\r\n\r\n                  </ng-container>\r\n                </ng-container>\r\n                <ng-container *ngIf=\"formValues.formArrayValues.autoCompleteFields\">\r\n                  <ng-container *ngFor=\"let inputField of formValues.formArrayValues.autoCompleteFields\">\r\n                    <div [ngClass]=\"(inputField && inputField.className) ? inputField.className : 'col-md-6'\">\r\n                      <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                        <mat-label>Enter Value</mat-label>\r\n                        <input matInput #accessInput [formControlName]=\"inputField.name\"\r\n                          (input)=\"onSearchChange($event.target.value,inputField)\" [matAutocomplete]=\"auto\">\r\n                        <mat-autocomplete #auto=\"matAutocomplete\">\r\n                          <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.name]\"\r\n                            [value]=\"fieldValue[inputField.keyToSave]\">\r\n                            {{fieldValue[inputField.keyToShow] | translate}}\r\n                          </mat-option>\r\n                        </mat-autocomplete>\r\n                      </mat-form-field>\r\n                    </div>\r\n                  </ng-container>\r\n                </ng-container>\r\n              </div>\r\n              <!-- end condition form -->\r\n              <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formArrayValues.selectFields\">\r\n                <div *ngFor=\"let selectField of formValues.formArrayValues.selectFields\" fxFlex.gt-sm=\"50\"\r\n                  fxLayoutAlign=\"center\" class=\"mr-8\">\r\n                  <!-- <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"30\" *ngIf=\"selectField.showSideLabel\"\r\n                    fxLayoutAlign=\"center\" class=\"mr-8\">\r\n                    <div layout=\"row\">\r\n                      <mat-label class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n                    </div>\r\n                  </div> -->\r\n                  <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\"\r\n                    style=\"display: inline;\">\r\n                    <!-- <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                      <mat-label>{{selectField.label |translate}}</mat-label>\r\n                      <mat-select [formControlName]=\"selectField.name\" [required]=\"selectField.validations\">\r\n                        <mat-option *ngFor=\"let item of selectField.data\" (click)=\"onChange(item, selectField)\"\r\n                          value=\"{{item[selectField.keyToSave] | translate}}\">\r\n                          {{item[selectField.keyToShow] | translate}}\r\n                        </mat-option>\r\n                      </mat-select>\r\n                    </mat-form-field> -->\r\n                    <!-- <p *ngIf=\"selectField.validations && submitted && inputGroup.get(selectField.name).invalid\">\r\n                  <mat-error class=\"error_margin\">\r\n                    {{selectField.validations.message | translate}}\r\n                  </mat-error>\r\n                </p> -->\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.formArrayValues.autoCompleteFields\">\r\n                <div *ngFor=\"let inputField of formValues.formArrayValues.autoCompleteFields\" fxFlex.gt-sm=\"50\"\r\n                  fxLayoutAlign=\"center\" class=\"mr-8\">\r\n                  <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\"\r\n                    style=\"display: inline;\">\r\n                    <!-- <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                      <mat-label>Enter Value</mat-label>\r\n                      <input matInput #accessInput [formControlName]=\"inputField.name\"\r\n                        (input)=\"onSearchChange($event.target.value,inputField)\" [matAutocomplete]=\"auto\">\r\n                      <mat-autocomplete #auto=\"matAutocomplete\">\r\n                        <mat-option class=\"custom-options\" *ngFor=\"let fieldValue of inputField[inputField.name]\"\r\n                          [value]=\"fieldValue[inputField.keyToSave]\">\r\n                          {{fieldValue[inputField.keyToShow] | translate}}\r\n                        </mat-option>\r\n                      </mat-autocomplete>\r\n                    </mat-form-field> -->\r\n                    <!-- <p *ngIf=\"inputField.validations && submitted && inputGroup.get(inputField.name).invalid\">\r\n                  <mat-error class=\"error_margin\">\r\n                    {{inputField.validations.message | translate}}\r\n                  </mat-error>\r\n                </p> -->\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </fieldset>\r\n        </div>\r\n      </div><br>\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=enableTabs>\r\n      <mat-tab-group (selectedTabChange)=\"changeTab($event)\">\r\n        <mat-tab *ngFor=\"let tab of formValues.tabData;let index = index\" [label]=\"tab.label | translate\">\r\n          <div layout=\"row\" md-content layout-padding>\r\n            <div fxLayout=\"column\" fxFlex=\"100\" *ngFor=\"let textField of tab.textFields\" fxFlex.gt-sm=\"50\"\r\n              fxLayoutAlign=\"center\" class=\"mr-8\">\r\n              <mat-form-field appearance=\"outline\" fxFlex=\"100\" class=\"custom-mat-form\">\r\n                <mat-label>{{textField.label | translate}}</mat-label>\r\n                <input matInput>\r\n              </mat-form-field>\r\n            </div>\r\n          </div>\r\n          <div *ngIf=\"enableTableLayout\">\r\n            <div *ngIf=\"currentTableLoad.errorShow\" style=\"padding-top: 15px;padding-inline-start: 26px;\">\r\n              <mat-panel-title>\r\n                Total:&nbsp;{{currentTableLoad.errorShow.total}}&nbsp;&nbsp;&nbsp;SaveCount:&nbsp;{{currentTableLoad.errorShow.saveCount}}&nbsp;&nbsp;&nbsp;FailedCount:&nbsp;{{currentTableLoad.errorShow.failedCount}}&nbsp;&nbsp;&nbsp;DuplicateCount:&nbsp;{{currentTableLoad.errorShow.duplicateCount}}\r\n              </mat-panel-title>\r\n            </div>\r\n            <ng-container *ngFor=\"let tableItem of tableList\">\r\n              <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n                [viewFrom]=\"'create'\" [onLoad]=\"currentTableLoad\"></table-layout>\r\n            </ng-container>\r\n          </div>\r\n        </mat-tab>\r\n      </mat-tab-group>\r\n    </div>\r\n    <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.confirmData\">\r\n    <div fxLayout=\"column\" fxFlex=\"50\" fxLayoutAlign=\"center\" class=\"mr-8\"\r\n      *ngFor=\"let confirmField of formValues.confirmData\" fxFlex.gt-sm=\"100\">\r\n      <mat-label class=\"font-size-14\"> <b>{{confirmField.label | translate}}</b> </mat-label>\r\n      <span style=\"padding: 10%;\">{{viewData[confirmField.value]}}</span>\r\n    </div>\r\n  </div> -->\r\n    <div layout=\"row\" md-content layout-padding\r\n      *ngIf=\"formValues.showSelectedChips && formValues.viewSelectedChips && inputData.selectedChips\">\r\n      <div class=\"h3 mb-24 ui-common-lib-confirmation-header\">{{formValues.viewSelectedChips.heading | translate}}\r\n      </div>\r\n      <mat-chip-list #chipList>\r\n        <ng-container\r\n          *ngFor=\"let chip of inputData.selectedChips[formValues.viewSelectedChips.chipId]; let ind = index\">\r\n          <!-- <p>{{inputData | json}}</p> -->\r\n          <mat-chip role=\"option\" id=\"matchipID\">\r\n            {{chip[formValues.viewSelectedChips.keyToShow]}}\r\n          </mat-chip>\r\n          <span\r\n            *ngIf=\"inputData.operators && inputData.operators.length && (ind !== inputData.selectedChips[formValues.viewSelectedChips.chipId].length-1)\">{{inputData[\"chipOperator\"] | translate}}</span>\r\n          <span\r\n            *ngIf=\"!inputData.operators && !inputData['chipOperator'] && (ind !== inputData.selectedChips[formValues.viewSelectedChips.chipId].length-1)\">{{formValues.viewSelectedChips.operator | translate}}</span>\r\n          <span\r\n            *ngIf=\"!inputData.operators && inputData['chipOperator']  && (ind !== inputData.selectedChips[formValues.viewSelectedChips.chipId].length-1)\">{{inputData[\"chipOperator\"] | translate}}</span>\r\n\r\n        </ng-container>\r\n      </mat-chip-list>\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding\r\n      *ngIf=\"formValues.showSelectedChips && formValues.viewSelectedChipsArray &&  inputData.selectedChips\">\r\n      <div *ngFor=\"let viewChip of formValues.viewSelectedChipsArray\">\r\n        <div class=\"h3 mb-24 ui-common-lib-confirmation-header\">{{viewChip.heading | translate}}\r\n        </div>\r\n        <mat-chip-list #chipList>\r\n          <ng-container *ngFor=\"let chip of inputData.selectedChips[viewChip.chipId]; let ind = index\">\r\n            <mat-chip role=\"option\" id=\"matchipID\">\r\n              {{viewChip.keyToShow ? chip[viewChip.keyToShow] : chip}}\r\n            </mat-chip>\r\n            <span\r\n            *ngIf=\"viewChip.operatorsCheck && inputData.chipOperator && (ind !== inputData.selectedChips[viewChip.chipId].length-1)\">{{inputData[\"chipOperator\"] | translate}}</span>\r\n          </ng-container>\r\n        </mat-chip-list>\r\n      </div>\r\n\r\n    </div>\r\n    <div layout=\"row\" md-content layout-padding *ngIf=\"enableSelectValueView\">\r\n      <div layout=\"row\" md-content layout-padding style=\"padding-top: 25px;\">\r\n        <ng-container *ngIf=\"formValues.selected_view_heading \">\r\n          <div class=\"h2 mb-6 ui-common-lib-confirmation-header\">{{formValues.selected_view_heading | translate}}</div>\r\n        </ng-container>\r\n      </div>\r\n      <ng-container *ngFor=\"let tableItem of selectedTableView\">\r\n        <div style=\"margin:-16px;z-index: 1;\">\r\n          <table-layout [tableId]=\"tableItem.tableId\" (actionClickEvent)=\"deleteClick($event)\" [viewFrom]=\"'view'\"\r\n            [onLoad]=\"selectTableLoad\"></table-layout>\r\n        </div>\r\n      </ng-container>\r\n    </div>\r\n    <div layout=\"row\" *ngIf=\"errorShow.logsArr?.length > 0\" class=\"p-12\" md-content layout-padding>\r\n      <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\">\r\n        <mat-accordion>\r\n          <mat-expansion-panel (opened)=\"panelOpenState = true\" (closed)=\"panelOpenState = false\">\r\n            <mat-expansion-panel-header>\r\n              <mat-panel-title>\r\n                Total:&nbsp;{{errorShow.total}}&nbsp;&nbsp;&nbsp;SaveCount:&nbsp;{{errorShow.saveCount}}&nbsp;&nbsp;&nbsp;FailedCount:&nbsp;{{errorShow.failedCount}}&nbsp;&nbsp;&nbsp;DuplicateCount:&nbsp;{{errorShow.duplicateCount}}\r\n              </mat-panel-title>\r\n            </mat-expansion-panel-header>\r\n            <div class=\"import-error-view\">\r\n              <ng-container *ngFor=\"let tableItem of errorTableList\">\r\n                <table-layout [tableId]=\"tableItem.tableId\" [disablePagination]=\"true\" [viewFrom]=\"'import'\"\r\n                  [onLoad]=\"errorLogTable\"></table-layout>\r\n              </ng-container>\r\n            </div>\r\n          </mat-expansion-panel>\r\n        </mat-accordion>\r\n      </div>\r\n    </div>\r\n    <div *ngIf=\"enableImportData\" style=\"padding : 13px 0 0 0;margin-top:-23px;\">\r\n      <div layout=\"row\" md-content layout-padding class=\"import_border\">\r\n        <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" class=\"mr-8\">\r\n          <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\" fxLayoutAlign=\"center\" style=\"text-align: center;\"\r\n            class=\"mr-8\">\r\n            <div class=\"board-list-item add-new-board ng-trigger ng-trigger-animate md-headline\" fxlayout=\"column\"\r\n              layout-align=\"center center\" fileDragDrop (onFileDropped)=\"onFileChange($event, importData.fileType,importData)\">\r\n              <mat-icon class=\"s-56 mat-icon material-icons import-icon\" role=\"img\" aria-hidden=\"true\">backup\r\n              </mat-icon>\r\n              <!-- <mat-icon class=\"s-56 mat-icon material-icons import-icon\" role=\"img\" aria-hidden=\"true\">unarchive\r\n              </mat-icon> -->\r\n              <div class=\"board-name\">{{'FORMS.DRAG_&_DROP_LBL' |translate}}</div>\r\n              <div class=\"board-name\" style=\"font-size:normal\">{{'FORMS.SELECT_BELOW_LBL' | translate}}</div>\r\n              <div style=\"padding: 10px 0 5px 0;\">\r\n                <button mat-button color=\"accent\" type=\"button\"\r\n                  class=\"ui-common-lib-browse-btn ui-common-lib-outline accent\" (click)=\"fileInput.click()\"\r\n                  style=\"border:2px solid\">{{'BUTTONS.BROWSE_BTN_LBL' | translate}}</button>\r\n                <input hidden type=\"file\" accept=\"'.'+importData.fileType\" required #fileInput\r\n                  (change)=\"onFileChange($event.target.files, importData.fileType,importData)\">\r\n\r\n                  <p class=\"sentri-filename\" *ngIf=\"selectedFileName\">{{selectedFileName}}</p>\r\n              </div>\r\n            </div>\r\n            <div *ngIf=\"importData.validations && submitted && !selectedFileName\">\r\n              <mat-error>\r\n                {{importData.validations.message | translate}}\r\n              </mat-error>\r\n            </div>\r\n          </div>\r\n        </div>\r\n\r\n      </div>\r\n      <div *ngIf=\"importData.uploadInfo\" class=\"import-file-info\">\r\n        <p style=\"color:#d2584f;margin-bottom:0px !important;\">\r\n          {{importData.uploadInfo | translate}}\r\n        </p>\r\n      </div>\r\n      <div *ngIf=\"importData.tagLine\">\r\n        <p style=\"color:#d2584f;margin-bottom:0px !important;\">\r\n          {{importData.tagLine | translate}}\r\n          <a target=\"_self\" [attr.href]=\"importData.sampleXlsx\" [attr.download]=\"importData.downloadFileNameXlsx\">\r\n            <span>\r\n              {{'XLSX'}}\r\n            </span>\r\n          </a> /\r\n          <a target=\"_self\" [attr.href]=\"importData.sampleFileZip\" [attr.download]=\"importData.downloadFileNameZip\">\r\n            <span>\r\n              {{'ZIP'}}\r\n            </span>\r\n          </a>\r\n        </p>\r\n      </div>\r\n      <div *ngIf=\"!importData.s3upload || importData.showSampleFormat\" layout=\"row\" md-content layout-padding>\r\n        <div fxLayout=\"column\" fxFlex=\"100\" fxFlex.gt-sm=\"100\">\r\n          <p style=\"color:#2979ff;text-decoration: underline;\">\r\n            <a target=\"_self\" [attr.href]=\"importData.sampleFile\" [attr.download]=\"importData.downloadFileName\">\r\n              <span>\r\n                {{'FORMS.SAMPLE_FORMAT_LBL' | translate}}\r\n              </span>\r\n            </a>\r\n          </p>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <ng-container *ngIf=\"formValues.buttonsList && formValues.beforeTableView\">\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"end end\" [class.bottom-layout]=\"formValues.buttonsList\"\r\n        *ngIf=\"formValues.buttonsList\">\r\n        <button *ngFor=\"let buttonId of formValues.buttonsList\"\r\n          (click)=\"onButtonClick(buttonsConfigData[buttonId],formValues)\"\r\n          [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n          mat-raised-button class=\"btn-common-view\">\r\n          {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n        </button>\r\n      </div>\r\n    </ng-container>\r\n    <div layout=\"row\" md-content layout-padding\r\n      *ngIf=\"formValues.tableData && enableTableLayout && !formValues.enableNewTableLayout\">\r\n      <ng-container *ngFor=\"let tableItem of formValues.tableData\">\r\n        <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\" [viewFrom]=\"'view'\"\r\n          [onLoad]=\"currentTableLoad\"></table-layout>\r\n      </ng-container>\r\n    </div>\r\n    <div *ngIf=\"formValues.enableNewTableLayout && enableNewTableLayout\">\r\n      <ng-container *ngFor=\"let tableItem of formValues.tableData\">\r\n        <new-table-layout (checkClickEventMessage)=\"receiveClick($event)\"  [tableId]=\"tableItem.tableId\" [viewFrom]=\"'view'\" [onLoad]=\"currentTableLoad\">\r\n        </new-table-layout>\r\n      </ng-container>\r\n    </div>\r\n    <ng-container *ngIf=\"!formValues.buttonsList\">\r\n      <!-- button last day fixed next button set starting -->\r\n    </ng-container>\r\n    <ng-container *ngIf=\"formValues.buttonsList && formValues.enableFormButton\">\r\n      <div fxLayout=\"row\" fxLayoutAlign=\"end end\" [class.bottom-layout]=\"formValues.buttonsList\"\r\n        *ngIf=\"formValues.buttonsList\">\r\n        <button *ngFor=\"let buttonId of formValues.buttonsList\"\r\n          (click)=\"onButtonClick(buttonsConfigData[buttonId],formValues)\"\r\n          [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n          mat-raised-button class=\"btn-common-view\" style=\"margin: 0 3px;\">\r\n          {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n        </button>\r\n      </div>\r\n    </ng-container>\r\n    <ng-container *ngIf=\"enableSchedule\">\r\n      <div class=\"col-md-12\">\r\n        <div class=\"sen-header-id\">\r\n          <h6>Schedule Information</h6>\r\n        </div>\r\n        <div class=\"col-md-12\" style=\"margin-top:15px;\">\r\n          <mat-radio-group formControlName=\"mainType\" [(ngModel)]=\"cropExpressionSelection.selectionOption\"\r\n            aria-label=\"Select an option\" class=\"fullwidth\">\r\n            <mat-radio-button value=\"Hours\" (change)=\"hourChange()\" style=\"margin-right:15px;\">Hours\r\n            </mat-radio-button>\r\n            <mat-radio-button value=\"Day\" (change)=\"dayChange()\" style=\"margin-right:15px;z-index: 999;\">Day\r\n            </mat-radio-button>\r\n            <mat-radio-button value=\"Month\" (change)=\"monthChange()\" style=\"margin-right:15px\">Month\r\n            </mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n        <div class=\"col-md-12\">\r\n          <ng-container *ngIf=\"MinutesViewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\">\r\n                  <mat-select formControlName=\"everyMinute\" (selectionChange)=\"everyMinuteSelect($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.everyMinute\" single>\r\n                    <mat-option *ngFor=\"let topping of secondsList \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"hourViewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <br>\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\" appearance=\"outline\">\r\n                  <mat-select formControlName=\"hourListvalue\" (selectionChange)=\"selectHour($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.hourListvalue\" single>\r\n                    <mat-option *ngFor=\"let topping of hourListView \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n          <ng-container *ngIf=\"dayviewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <br>\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\">\r\n                  <mat-select formControlName=\"everyDays\" (selectionChange)=\"dateCounts($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.everyDays\" single>\r\n                    <mat-option *ngFor=\"let topping of dateCount \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <span style=\"margin-right: 12px;;\">\r\n                  <mat-label>day(s) starting on</mat-label>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\" style=\"margin-left:7px;\">\r\n                  <mat-select formControlName=\"dayStartingOn\" (selectionChange)=\"daysCounts($event)\"\r\n                    [(ngModel)]=\"cropExpressionSelection.dayStartingOn\" single>\r\n                    <mat-option *ngFor=\"let topping of days \" [value]=\"topping\">{{topping}}\r\n                    </mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n\r\n          <ng-container *ngIf=\"monthviewShow\">\r\n            <div class=\"row\" style=\"margin-top:15px;\">\r\n              <div class=\"col-12\">\r\n                <span style=\"margin-right:12px;\">\r\n                  <mat-radio-button value=\"Every\" style=\"margin-right:15px\">Every\r\n                  </mat-radio-button>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\">\r\n                  <mat-select (selectionChange)=\"monthCount($event)\" formControlName=\"monthStartingOn\"\r\n                    [(ngModel)]=\"cropExpressionSelection.monthStartingOn\" single>\r\n                    <mat-option *ngFor=\"let topping of monthsCount \" [value]=\"topping\">{{topping}}</mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <span style=\"margin-right: 12px;;\">\r\n                  <mat-label>months(s) starting in</mat-label>\r\n                </span>\r\n                <mat-form-field class=\"sen-lib-width-18\"  appearance=\"outline\" style=\"margin-left:7px;\">\r\n                  <mat-select (selectionChange)=\"monthExp($event)\" formControlName=\"monthStartingOn\"\r\n                    [(ngModel)]=\"cropExpressionSelection.monthDetailsShow\" single>\r\n                    <mat-option *ngFor=\"let topping of numOfmonths \" [value]=\"topping\">{{topping}}\r\n                    </mat-option>\r\n                  </mat-select>\r\n                </mat-form-field>\r\n                <br>\r\n              </div>\r\n            </div>\r\n          </ng-container>\r\n          <!-- end exp -->\r\n          <!-- end -->\r\n        </div>\r\n      </div>\r\n\r\n\r\n    </ng-container>\r\n    <ng-container *ngIf=\"formValues.enableAgentNote\">\r\n      <div>\r\n        <p>Note: Once Download the agent the following steps to be followed to run the agent</p>\r\n        <ul>\r\n          <li>Extract the Downloaded package into a folder</li>\r\n          <li>Execute the commad to provide root permission to run the script file (run chmod 777 sodagent.sh)</li>\r\n          <li>Execute the commad sh sodagent.sh to run the script file</li>\r\n        </ul>\r\n        <p>Please Refer the Readme file for further clarificaion on installation</p>\r\n        <p>Note: To check the test connection Plese make sure that you have access to the database system</p>\r\n      </div>\r\n    </ng-container>\r\n    <!-- <div layout=\"row\" md-content layout-padding *ngIf=\"formValues.tableData && enableTableLayout\">\r\n    <ng-container *ngFor=\"let tableItem of formValues.tableData\">\r\n      <table-layout (checkClickEventMessage)=\"receiveClick($event)\" [tableId]=\"tableItem.tableId\"\r\n        [viewFrom]=\"'create'\" [onLoad]=\"currentTableLoad\"></table-layout>\r\n    </ng-container>\r\n  </div> -->\r\n  </form>\r\n  <ng-container *ngIf=\"!formValues.buttonsList\">\r\n    <div>\r\n      <div class=\"col-12 sentri-btn-footer\" [ngClass]=\"getRulesWidth ? getRulesWidth:hiddenFixedLayout\">\r\n        <!-- fxLayout=\"row\" fxLayoutAlign=\"end end\" -->\r\n        <div class=\"ui-common-lib-whitebg\">\r\n          <div fxLayout=\"row\" fxLayoutAlign=\"end end\" style=\"padding: 10px 0px 0px 0px;padding-bottom:10px !important;\"\r\n            [class.bottom-layout]=\"formValues.buttonsList\" *ngIf=\"!formValues.buttonsList\">\r\n            <!-- <button *ngIf=\"formValues.enableTestConnection\" mat-raised-button class=\"btn-common-view\" type=\"button\" color=\"warn\">Test Connection</button> -->\r\n            <button type=\"button\" *ngIf=\"stepperVal\"\r\n              class=\"mat-raised-button ui-common-lib-outline cancel-btn btn-common-view sen-cancel\" ng-reflect-color=\"accent\"\r\n              ng-reflect-type=\"button\" (click)=\"closeModel()\">\r\n              <span class=\"mat-button-wrapper\">{{\"BUTTONS.CANCEL_BTN_LBL\" | translate}}</span>\r\n            </button>\r\n            <button class=\"btn-common-view mrStyle ui-common-lib-outline\" mat-raised-button\r\n              *ngIf=\"stepperVal && stepperVal.selectedIndex > 0\" type=\"button\" color=\"accent\"\r\n              (click)=\"navigatePrevious()\">\r\n              <mat-icon>navigate_before</mat-icon>{{\"BUTTONS.PREVIOUS_BTN_LBL\" | translate}}\r\n            </button>\r\n            <button class=\"btn-common-view ui-common-lib-outline sen-next-btn\" mat-raised-button type=\"button\" color=\"accent\"\r\n              (click)=\"navigateNext()\"\r\n              *ngIf=\"stepperVal && stepperVal['_steps'] && (stepperVal.selectedIndex !== stepperVal._steps.length-1) \">\r\n              <mat-icon>navigate_next</mat-icon>{{\"BUTTONS.NEXT_BTN_LBL\" | translate}}\r\n            </button>\r\n            <button mat-raised-button type=\"button\"\r\n              *ngIf=\"stepperVal && stepperVal['_steps'] && (stepperVal.selectedIndex === stepperVal._steps.length-1)\"\r\n              class=\"btn-common-view ui-common-lib-outline sen-next-btn\" color=\"warn\" (click)=\"onSubmit()\">\r\n              {{\"BUTTONS.SUBMIT_BTN_LBL\" | translate}}\r\n            </button>\r\n          <!--\r\n            <button *ngIf=\"importData && importData.function !=='edit'\" mat-raised-button\r\n              class=\"btn-common-view ui-common-lib-outline\" type=\"button\" color=\"warn\" (click)=\"onImport()\">\r\n              {{\"BUTTONS.IMPORT_BTN_LBL\" | translate}}\r\n            </button>\r\n            <button *ngIf=\"importData && importData.function =='edit'\" mat-raised-button\r\n              class=\"btn-common-view ui-common-lib-outline\" type=\"button\" color=\"warn\" (click)=\"onImport()\">\r\n              {{\"BUTTONS.RE_IMPORT_BTN_LBL\" | translate}}\r\n            </button> -->\r\n             <span *ngIf=\"importData && importData.buttonsList\">\r\n              <button *ngFor=\"let buttonId of importData.buttonsList\"\r\n              (click)=\"onButtonClick(buttonsConfigData[buttonId],importData)\"\r\n              [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n              mat-raised-button class=\"btn-common-view\" style=\"margin: 0 3px;\">\r\n              {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n            </button>\r\n             </span>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n  <ng-container *ngIf=\"formValues.buttonsList && !formValues.beforeTableView\">\r\n    <div class=\"col-12 sentri-btn-footer\" [ngClass]=\"getRulesWidth ? getRulesWidth:hiddenFixedLayout\">\r\n      <!-- fxLayout=\"row\" fxLayoutAlign=\"end end\" -->\r\n      <div class=\"ui-common-lib-whitebg\">\r\n        <div fxLayout=\"row\" fxLayoutAlign=\"end end\" [class.bottom-layout]=\"formValues.buttonsList\"\r\n          *ngIf=\"formValues.buttonsList\">\r\n          <button *ngFor=\"let buttonId of formValues.buttonsList\"\r\n            (click)=\"onButtonClick(buttonsConfigData[buttonId],formValues)\"\r\n            [ngClass]=\"(buttonsConfigData[buttonId] && buttonsConfigData[buttonId].style )? buttonsConfigData[buttonId].style : ''\"\r\n            mat-raised-button class=\"btn-common-view\" style=\"margin: 0 3px;\">\r\n            {{buttonsConfigData[buttonId] ? (buttonsConfigData[buttonId].label | translate) : \"\" }}\r\n          </button>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </ng-container>\r\n\r\n</div>\r\n",
                styles: [".cancel-btn{border:1px solid #4527a0!important;background-color:#fff!important;color:#000!important;margin-right:8px!important}.btn-common-view{border-radius:25px!important}.error_margin{margin-top:-30px}p.errorP{margin-top:10px}.ui-common-lib-outline{outline:0!important}.bottom-layout{margin:1%}.import-error-view{max-height:250px;overflow:scroll}.btnformat{border-radius:25px;position:relative;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-webkit-tap-highlight-color:transparent;margin-right:5px}.chip-label,.select-label{margin-top:2%;width:50%;float:left}.slide-toggle{font-weight:700;font-size:initial}.slide-toggle-padding{padding:0 0 0 10px}.import-icon{background:-webkit-gradient(linear,left top,left bottom,from(#8c51ff),to(#5198fe));-webkit-background-clip:text;-webkit-text-fill-color:transparent}.column{float:left;width:50%}.row:after{content:\"\";display:table;clear:both}.full-width{width:100%}.half-width{width:48.5%}.confirm-data-view{margin:0 0 0 10px;border-radius:4px;font-size:16px;background-color:#fff;border:2px solid}.mrStyle{margin-right:8px}div.column{padding-right:35px!important}.sen-lib-form-layout{margin-left:28px;margin-right:28px;background:#fff;box-shadow:0 5px 5px -3px rgba(0,0,0,.2),0 8px 10px 1px rgba(0,0,0,.14),0 3px 14px 2px rgba(0,0,0,.12);padding:28px 27px}.sen-m-0{margin:0!important}input.mat-chip-input{width:150px;margin:0!important;-webkit-box-flex:1;flex:1 0 150px}.ui-common-lib-confirmation-header{background:#394e6661;font-size:16px!important;padding:4px 4px 6px;margin-top:8px;font-weight:600;color:#000;box-shadow:2px 2px 1px 1px #dadada;border-bottom:1px solid #c4c4c4;position:relative;z-index:22}.ui-common-lib-padding-10{padding:12px}.ui-common-lib-browse-btn{background:linear-gradient(45deg,#8c51ff,#5198fe) i!important;color:#fff!important;border-radius:20px!important}.ui-common-lib-f17{font-size:17px!important}.paddingAdd{padding-bottom:10px!important}.widthBase{width:25%!important}.sentri-active{border-bottom:2px solid currentColor}.sentri-form-height{height:303px;padding-left:12px;overflow-y:auto;overflow-x:hidden;margin-bottom:60px}.sen-lib-height{height:auto!important;overflow:unset!important}.sentri-btn-footer{right:0;position:absolute;bottom:0;width:100%;background:#f7f7f7;border-top:2px solid #d8d7d7;z-index:99999}.sen-lib-width{width:100%!important;right:0!important}.sen-lib-width-report{width:50%!important;right:25%!important;bottom:20%!important}.sen-lib-hidden{display:none!important}.sen-lib-height-conflict{height:303px;overflow-y:auto;overflow-x:hidden;margin-bottom:20px}.sentri-ruleset-height{height:355px!important}.sentri-margin-set{margin-bottom:38px!important}.sentri-form-height.sen-lib-height-btm{height:auto!important;margin-bottom:0;overflow:hidden}.sentri-margin-height{height:auto!important;margin-bottom:0!important}.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:154px}.sentri-height-auto{height:100%;margin:0!important;overflow:hidden!important;padding:12px!important}.sen-lib-footer-fixedfordatasource{position:fixed!important;width:50%!important;left:25%!important}.sen-lib-footer-fixedforruleset{bottom:15%;position:fixed;width:50%;left:25%}.sen-next-btn{color:#fff!important;background-color:#e91e63!important}.sen-cancel{border:1px solid #4527a0!important;background-color:#fff!important;color:#000!important;margin-right:8px!important}@media only screen and (max-width:1300px){.sentri-responsive-col-3{width:100%!important;max-width:100%!important;-webkit-box-flex:0;flex:0 0 26%;top:-37px}.sen-lib-responsive-enddate{max-width:100%;margin-left:1px}.sen-res-width100{width:100%!important}.mr-res-5btm{margin-bottom:8px}}@media only screen and (max-width:12){.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:144px}}@media only screen and (max-width:1440px){.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:144px}}@media only screen and (max-width:2560px){.sen-lib-footer-accessgroup{width:80%!important;position:fixed;left:10%}}.sen-header-id{background:#bab8b8;box-shadow:4px 3px 1px 0 #dcdcdc}.sen-header-id h6{padding:5px;font-size:14px}.sen-lib-width-18{width:18%}.sen-lib-list-view{display:block;flex-wrap:wrap;margin-right:-15px;margin-left:-15px}.import-file-info{color:#d2584f;margin-bottom:-1rem!important}.btn-submit{background-color:#2b14bd!important}.ui-lib-accessgroup-add{position:relative;right:10px;top:5px}"]
            }] }
];
/** @nocollapse */
FormLayoutComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: ContentService },
    { type: FormBuilder },
    { type: MessageService },
    { type: ChangeDetectorRef },
    { type: MatDialogRef },
    { type: AwsS3UploadService },
    { type: ChangeDetectorRef },
    { type: SnackBarService },
    { type: LoaderService },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
FormLayoutComponent.propDecorators = {
    formValues: [{ type: Input }],
    stepperVal: [{ type: Input }],
    importData: [{ type: Input }],
    onLoadData: [{ type: Input }],
    chipListFields: [{ type: Input }],
    title: [{ type: Input }],
    data: [{ type: Input }],
    fromRouting: [{ type: Input }],
    accessInput: [{ type: ViewChild, args: ["accessInput",] }],
    chipInput: [{ type: ViewChild, args: ["chipInput",] }],
    matAutocomplete: [{ type: ViewChild, args: ["auto",] }]
};
export class ImportValue {
    constructor(control) {
        this.data_source = control.data_source || "";
        this.file = control.file || "";
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImZvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDdkUsT0FBTyxFQUNMLFNBQVMsRUFDVCxNQUFNLEVBQ04sU0FBUyxFQUNULFVBQVUsRUFDVixLQUFLLEVBQ0wsaUJBQWlCLEdBR2xCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFFTCxlQUFlLEdBQ2hCLE1BQU0sZ0NBQWdDLENBQUM7QUFHeEMsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDNUYsa0RBQWtEO0FBRWxELE9BQU8sRUFDTCxXQUFXLEVBQ1gsV0FBVyxFQUNYLFNBQVMsRUFDVCxVQUFVLEdBRVgsTUFBTSxnQkFBZ0IsQ0FBQztBQUV4QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFNUQsT0FBTyxLQUFLLENBQUMsTUFBTSxRQUFRLENBQUM7QUFDNUIsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDdkMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUN4RCxPQUFPLEtBQUssT0FBTyxNQUFNLFFBQVEsQ0FBQztBQUNsQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFbEQsTUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDO0FBT3ZCLE1BQU0sT0FBTyxtQkFBbUI7SUEwRjlCLFlBQ1UsNkJBQTJELEVBQzNELGNBQThCLEVBQzlCLFdBQXdCLEVBQ3hCLGNBQThCLEVBQzlCLGNBQWlDLEVBQ2xDLFlBQWdELEVBQy9DLGtCQUFzQyxFQUN0QyxHQUFzQixFQUN0QixlQUFnQyxFQUNoQyxhQUE0QixFQUNULE9BQU87UUFWMUIsa0NBQTZCLEdBQTdCLDZCQUE2QixDQUE4QjtRQUMzRCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtRQUNsQyxpQkFBWSxHQUFaLFlBQVksQ0FBb0M7UUFDL0MsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxRQUFHLEdBQUgsR0FBRyxDQUFtQjtRQUN0QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsa0JBQWEsR0FBYixhQUFhLENBQWU7UUFDVCxZQUFPLEdBQVAsT0FBTyxDQUFBO1FBckZwQyx1QkFBa0IsR0FBRyxJQUFJLENBQUM7UUFnQjFCLHlCQUFvQixHQUFZLEtBQUssQ0FBQztRQUM5QixnQkFBVyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFDbEMscUJBQWdCLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUMvQyxzQkFBaUIsR0FBUSxFQUFFLENBQUM7UUFDNUIsbUJBQWMsR0FBYSxLQUFLLENBQUM7UUFDakMsY0FBUyxHQUFZLElBQUksQ0FBQztRQUMxQixpQkFBWSxHQUFRLEVBQUUsQ0FBQztRQUt2QixtQkFBYyxHQUFXLElBQUksQ0FBQztRQUM5QixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFVbEMsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUdwQiwwQkFBcUIsR0FBWSxLQUFLLENBQUM7UUFHdkMsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBRXBDLGtCQUFhLEdBQVEsRUFBRSxDQUFDO1FBQ3hCLHdCQUFtQixHQUFRLEVBQUUsQ0FBQztRQUM5QixxQkFBZ0IsR0FBRyxJQUFJLENBQUM7UUFDeEIsb0JBQWUsR0FBRyxJQUFJLENBQUM7UUFFdkIsVUFBSyxHQUFRLEVBQUUsQ0FBQztRQUNoQixrQkFBYSxHQUFRLEVBQUUsQ0FBQztRQUN4QixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFFbEMsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUNiLG9CQUFlLEdBQVksSUFBSSxDQUFDO1FBQ2hDLG1CQUFjLEdBQVksSUFBSSxDQUFDO1FBRXRDLGVBQVUsR0FBRyxDQUFDLENBQUM7UUFDZix1QkFBa0IsR0FBRyxDQUFDLENBQUM7UUFHdkIsZUFBVSxHQUFRLEVBQUUsQ0FBQztRQUNyQixZQUFPLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUNyQixlQUFVLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN4QixxQkFBZ0IsR0FBWSxLQUFLLENBQUM7UUFDM0Isb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFneUZ2QyxxQ0FBcUM7UUFDckMsZ0JBQVcsR0FBUyxFQUFFLENBQUM7UUFDdkIsc0JBQWlCLEdBQVEsRUFBRSxDQUFDO1FBQzVCLGlCQUFZLEdBQVMsRUFBRSxDQUFDO1FBQ3hCLGtCQUFhLEdBQVMsRUFBRSxDQUFDO1FBQ3pCLFNBQUksR0FBUyxFQUFFLENBQUM7UUFDaEIsVUFBSyxHQUFTLEVBQUUsQ0FBQztRQUNqQixnQkFBVyxHQUFTLEVBQUUsQ0FBQztRQUN2QixjQUFTLEdBQVMsRUFBRSxDQUFDO1FBQ3JCLGNBQVMsR0FBUyxFQUFFLENBQUM7UUFDckIsZ0JBQVcsR0FBUyxFQUFFLENBQUM7UUFPdkIsNEJBQXVCLEdBQVMsRUFBRSxDQUFDO1FBN3hGakMsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDcEUsSUFBSSxDQUFDLEtBQUssR0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUc7WUFDakIsTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsRUFBRTtZQUNULFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO1lBQ2xDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztTQUNsQixDQUFDO1FBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUI7YUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQzthQUN0QyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUNsQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUNwRCxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3BELE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQy9DLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztnQkFDdEIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztZQUNQLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ2hDLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLENBQUMsY0FBYzthQUNoQixVQUFVLEVBQUU7YUFDWixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNqQyxTQUFTLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNyQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQztZQUNwQyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1lBQ0YsdUhBQXVIO1lBQ3ZILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztZQUNsQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDaEIsSUFBSTtRQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUV6QyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLGVBQWU7YUFDbEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDakMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsNkNBQTZDLENBQUMsQ0FBQztZQUNqRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUNqQixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7Z0JBQ2IsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FDMUMsQ0FBQztnQkFDRixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDakI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxRQUFRO1FBQ04sSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztRQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsRUFBRSxDQUFDO1FBQzlCLHlDQUF5QztRQUN6QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFFckIsSUFBSSxVQUFVLEdBQUc7WUFDZixZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtZQUNwQyxjQUFjLEVBQUUsSUFBSSxDQUFDLHFCQUFxQjtZQUMxQyxLQUFLLEVBQUUsRUFBRTtZQUNULE1BQU0sRUFBRSxDQUFDO1NBQ1YsQ0FBQztRQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQztRQUNELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQztZQUMzRCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ2xELENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDUCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDMUIsSUFBSSxRQUFRLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNwRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7WUFDbkMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQ3RCLENBQUMsQ0FBQztnQkFDRSxZQUFZLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtnQkFDcEMsY0FBYyxFQUFFLElBQUksQ0FBQyxxQkFBcUI7Z0JBQzFDLEtBQUssRUFBRSxFQUFFO2dCQUNULE1BQU0sRUFBRSxDQUFDO2FBQ1YsQ0FBQztRQUNOLElBQUksQ0FBQyxTQUFTLHFCQUFRLElBQUksQ0FBQyxTQUFTLEVBQUssVUFBVSxDQUFFLENBQUM7UUFDdEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1FBQ0YsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxlQUFlLENBQUM7UUFDcEQsSUFBSSxDQUFDLFFBQVE7WUFDWCxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMseUJBQXlCO2dCQUM1RCxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx5QkFBeUI7Z0JBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDUCxZQUFZO1FBQ2hCLElBQUksQ0FBQyxhQUFhO1lBQ2hCLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNO2dCQUN6QyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNO2dCQUN6QixDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ1gsSUFBSSxDQUFDLGFBQWE7WUFDaEIsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVc7Z0JBQzVDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVc7Z0JBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDWCxJQUFJLENBQUMsaUJBQWlCO1lBQ3BCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUI7Z0JBQ2xELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQjtnQkFDbkMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUVYLElBQUksQ0FBQyxpQkFBaUI7WUFDbEIsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQjtnQkFDbEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCO2dCQUNuQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQ2IsK0RBQStEO1FBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzNCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNuQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ2xFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDdEUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN6RSw4RkFBOEY7WUFDOUYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSw0QkFBNEIsQ0FBQyxDQUFDO1lBQ3RFLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDbEUsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDekQsSUFBSSxDQUFDLHVCQUF1QixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCO2dCQUM5RCxDQUFDLENBQUMsSUFBSTtnQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ1YsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUNuRTtRQUVELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7UUFDL0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNuQixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7UUFDakIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ25CLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUM7U0FDOUI7UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLE9BQU8sRUFBRTtvQkFDN0IsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLEVBQUUsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM1RDtxQkFBTTtvQkFDTCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUMxQztZQUNILENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUN6QixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxVQUFVLElBQUk7Z0JBQ2xELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ25ELENBQUMsQ0FBQyxDQUFDO1lBQ0gsZ0RBQWdEO1NBQ2pEO1FBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUU7WUFDekQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLFVBQVUsSUFBSTtnQkFDMUQsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQztvQkFDbkMsS0FBSyxFQUFFLEVBQUU7b0JBQ1QsUUFBUSxFQUFFLElBQUksQ0FBQyxPQUFPO2lCQUN2QixDQUFDLENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDM0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxVQUFVLElBQUk7Z0JBQ3BELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2xCLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLElBQUk7Z0JBQ3pELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLG9EQUFvRDtZQUNwRCw4Q0FBOEM7WUFDOUMsc0RBQXNEO1lBQ3RELDRDQUE0QztZQUM1QyxzQ0FBc0M7WUFDdEMsTUFBTTtZQUNOLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFLFVBQVUsSUFBSTtnQkFDdkQsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDekMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBRWpELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxjQUFjLENBQUMsQ0FBQztnQkFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2dCQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztZQUNuQyxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRTtnQkFDckMsSUFBSSxDQUFDLFNBQVMsQ0FDWixJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQzlDLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDO2FBQ3JEO1NBQ0Y7UUFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1FBQ3ZELElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGtCQUFrQixDQUFDLENBQUM7WUFDdkQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxJQUFJO2dCQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxlQUFlLENBQUMsQ0FBQztnQkFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQ25ELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUMzQyxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztvQkFDbkMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7b0JBQ3ZDLElBQ0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJO3dCQUN2QixJQUFJLENBQUMsU0FBUzt3QkFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQzt3QkFDcEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUM3Qzt3QkFDQSxJQUFJLENBQUMsU0FBUzs0QkFDWixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ3pELElBQUksQ0FBQyxZQUFZOzRCQUNmLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQzt3QkFDNUQsSUFBSSxDQUFDLFNBQVM7NEJBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3dCQUN6RCxJQUFJLENBQUMsWUFBWTs0QkFDZixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzdEO2lCQUNGO2dCQUVELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsOERBQThEO29CQUM5RCxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztvQkFDeEMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7b0JBQ2hELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO29CQUNsRCxJQUFJLEtBQUssR0FBRyxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7b0JBQ2hDLHdDQUF3QztvQkFDeEMsV0FBVztvQkFDWCxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxVQUFVLFdBQVc7d0JBQzFDLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTs0QkFDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO3lCQUM3Qzs2QkFBTTs0QkFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUM3RDtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7d0JBQ2xFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQWEsQ0FBQzt3QkFDcEQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7d0JBQ2pFLElBQUksQ0FBQyxhQUFhLEVBQUU7NEJBQ2xCLE1BQU0sUUFBUSxHQUFHLEVBQUUsQ0FBQzs0QkFDcEIsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRTtnQ0FDbkMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLENBQUM7Z0NBQ3ZELElBQUksSUFBSSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dDQUM3QixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxVQUFVLE9BQU87b0NBQy9CLFFBQVEsQ0FBQyxJQUFJLENBQUM7d0NBQ1osSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dDQUM1QyxHQUFHLEVBQUUsT0FBTztxQ0FDYixDQUFDLENBQUM7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NkJBQ0o7aUNBQU07Z0NBQ0wsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLFVBQVUsSUFBSTtvQ0FDakMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7Z0NBQzNDLENBQUMsQ0FBQyxDQUFDOzZCQUNKOzRCQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsUUFBUSxDQUFDO3lCQUN0QjtvQkFDSCxDQUFDLENBQUMsQ0FBQztpQkFDSjtnQkFDRCw4QkFBOEI7Z0JBQzlCLDZCQUE2QjtnQkFDN0IsdUJBQXVCO2dCQUN2QixPQUFPO1lBQ1QsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSwwQkFBMEIsQ0FBQyxDQUFDO1FBQzVELDJDQUEyQztRQUMzQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDckQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUM7WUFDMUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxLQUFLO2dCQUN2QyxPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3pDLENBQUMsQ0FBQyxDQUFDO1lBQ0gsNkRBQTZEO1lBQzdELDhDQUE4QztZQUM5Qyw4Q0FBOEM7WUFDOUMsa0RBQWtEO1lBQ2xELG9EQUFvRDtZQUNwRCxpREFBaUQ7WUFDakQsb0RBQW9EO1lBQ3BELEtBQUs7WUFDTCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDO1NBQ3JEO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO2dCQUM5RCxJQUFJLENBQUMsZUFBZSxFQUFFO2FBQ3ZCLENBQUMsQ0FBQztTQUNKO1FBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7UUFFZCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNuQjtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMxRCxJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN4QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztnQkFDbkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO29CQUM5RCxDQUFDLENBQUMsSUFBSTtvQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ1g7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRTtnQkFDeEMsSUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2dCQUMzRCxJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQzdDLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsd0JBQXdCLENBQUMsQ0FBQztnQkFDMUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ3hELElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDdkMsT0FBTyxFQUFFLGtCQUFrQixDQUFDLFFBQVE7aUJBQ3JDLENBQUMsQ0FBQztnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxtQ0FBbUMsQ0FBQyxDQUFDO2dCQUM1RCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO2dCQUN4QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO2dCQUM5QixJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0I7b0JBQzlELENBQUMsQ0FBQyxJQUFJO29CQUNOLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDWDtpQkFBTSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFO2dCQUN2Qyx5Q0FBeUM7Z0JBQ3pDLHVDQUF1QztnQkFDdkMsOENBQThDO2dCQUM5QyxLQUFLO2dCQUNMLHFEQUFxRDtnQkFFckQsSUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNyQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO2dCQUNqRCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztnQkFFM0MsSUFBSSxTQUFTLEdBQUcsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO29CQUN2QyxPQUFPLEVBQUUsY0FBYztpQkFDeEIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztnQkFDdEMsNkNBQTZDO2dCQUM3QyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO29CQUM5RCxDQUFDLENBQUMsSUFBSTtvQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ1g7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixFQUFFO2dCQUM1QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO2dCQUMzQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztnQkFDeEMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztnQkFDOUIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO29CQUM5RCxDQUFDLENBQUMsSUFBSTtvQkFDTixDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ1g7WUFDRCx5Q0FBeUM7WUFDekMsMkNBQTJDO1lBRTNDLG1EQUFtRDtTQUNwRDtJQUNILENBQUM7SUFFRCxlQUFlO1FBQ2IsSUFBSSxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7UUFDckQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQUksY0FBYyxDQUFDLFlBQVksRUFBRTtZQUMvQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJO2dCQUNuRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRTtZQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRSxVQUFVLElBQUk7Z0JBQ3pELE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0MsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUNELFdBQVc7UUFDVCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBQ0QsZUFBZSxDQUFDLGFBQWE7UUFDM0IsbUZBQW1GO1FBQ25GLDREQUE0RDtRQUM1RCwyQkFBMkI7UUFDM0IsSUFBSTtRQUNKLDhEQUE4RDtRQUM5RCxJQUFJLE9BQU8sR0FBYyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNqRSxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFDRCxNQUFNO1FBQ0osSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3RELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzNCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxJQUFJO2dCQUNwRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3ZCLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO29CQUN4QyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztvQkFDaEQsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUM7b0JBQ2xELElBQUksS0FBSyxHQUFHLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO3dCQUMxQyxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUU7NEJBQ3pCLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQzt5QkFDN0M7NkJBQU07NEJBQ0wsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDN0Q7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO3dCQUNsRSxvQkFBb0I7d0JBQ3BCLElBQUk7d0JBQ0osOERBQThEO3dCQUM5RCxrREFBa0Q7d0JBQ2xELE1BQU07d0JBQ04sb0RBQW9EO3dCQUNwRCxPQUFPO3dCQUNQLHdCQUF3Qjt3QkFDeEIsSUFBSTt3QkFDSixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFhLENBQUM7d0JBRXBELDhDQUE4Qzt3QkFDOUMsa0NBQWtDO3dCQUNsQyxnQ0FBZ0M7d0JBQ2hDLGlFQUFpRTt3QkFFakUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsQ0FBQzt3QkFDeEMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFOzRCQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQzs0QkFDMUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQzVCLENBQUM7eUJBQ0g7d0JBRUQsSUFBSSxJQUFJLENBQUMsaUJBQWlCLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFOzRCQUNuRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxPQUFPO2dDQUNwQyxJQUFJLE9BQU8sQ0FBQyxjQUFjLEVBQUU7b0NBQzFCLElBQUksRUFBRSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUM7b0NBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7b0NBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDO29DQUMvQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztpQ0FDbkQ7NEJBQ0gsQ0FBQyxDQUFDLENBQUM7eUJBQ0o7d0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQ0FBcUMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7d0JBQ25FLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBRXZFLENBQUMsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLHVCQUF1QixFQUFFO1lBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLElBQUk7Z0JBQ3pELElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtvQkFDdkIsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ3hDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO29CQUNoRCxJQUFJLEtBQUssR0FBRzt3QkFDVixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7cUJBQ2xCLENBQUM7b0JBQ0YsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO3dCQUNsRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFhLENBQUM7b0JBQ3RELENBQUMsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGtCQUFrQixDQUFDLENBQUM7WUFDdkQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxJQUFJO2dCQUNsRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7b0JBQ3ZCLDhEQUE4RDtvQkFDOUQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ3hDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO29CQUNoRCxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztvQkFDbEQsSUFBSSxLQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBQ3ZDLFVBQVUsQ0FBQzt3QkFDVCxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxVQUFVLFdBQVc7NEJBQzFDLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTtnQ0FDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDOzZCQUM3QztpQ0FBTSxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUU7Z0NBQzdCLElBQUksV0FBVyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQ0FDMUQsc0pBQXNKO29DQUN0SiwrSEFBK0g7b0NBQy9ILElBQUksRUFBRSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzt3Q0FDeEMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUNyRCxXQUFXLENBQUMsTUFBTSxDQUNuQjt3Q0FDSCxDQUFDLENBQUMsRUFBRSxDQUFDO29DQUNQLElBQUksRUFBRSxDQUFDLE1BQU07d0NBQ1gsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUM7NENBQ3JCLFdBQVcsQ0FBQyxpQkFBaUIsSUFBSSxPQUFPLEVBQUUsS0FBSyxRQUFRO2dEQUNyRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUM7Z0RBQ3BCLENBQUMsQ0FBQyxFQUFFLENBQUM7aUNBQ1o7cUNBQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQ0FDNUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7d0NBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDO3dDQUN2RCxDQUFDLENBQUMsRUFBRSxDQUFDO29DQUNQLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGlCQUFpQjt3Q0FDckQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt3Q0FDekMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7aUNBQzdCOzZCQUNGO2lDQUFNO2dDQUNMLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO29DQUN6RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO29DQUNuQyxDQUFDLENBQUMsRUFBRSxDQUFDOzZCQUNSO3dCQUNILENBQUMsQ0FBQyxDQUFDO3dCQUNILElBQUksQ0FBQyxjQUFjOzZCQUNoQixhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQzs2QkFDNUIsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7NEJBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQWEsQ0FBQzs0QkFDcEQsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDaEMsSUFBSSxhQUFhLEdBQUcsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7Z0NBQy9DLENBQUMsQ0FBQyxJQUFJO2dDQUNOLENBQUMsQ0FBQyxLQUFLLENBQUM7NEJBQ1YsSUFBSSxDQUFDLGFBQWEsRUFBRTtnQ0FDbEIsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO2dDQUNwQixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFO29DQUNuQyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQ0FDdkQsSUFBSSxJQUFJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0NBQzdCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLFVBQVUsT0FBTzt3Q0FDL0IsUUFBUSxDQUFDLElBQUksQ0FBQzs0Q0FDWixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7NENBQzVDLEdBQUcsRUFBRSxPQUFPO3lDQUNiLENBQUMsQ0FBQztvQ0FDTCxDQUFDLENBQUMsQ0FBQztpQ0FDSjtxQ0FBTTtvQ0FDTCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxJQUFJO3dDQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztvQ0FDM0MsQ0FBQyxDQUFDLENBQUM7aUNBQ0o7Z0NBQ0QsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUM7NkJBQ3RCO3dCQUNILENBQUMsQ0FBQyxDQUFDO29CQUNQLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztpQkFDVjtnQkFDRCw4QkFBOEI7Z0JBQzlCLDZCQUE2QjtnQkFDN0IsdUJBQXVCO2dCQUN2QixPQUFPO1lBQ1QsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUNoRCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDdEI7SUFDSCxDQUFDO0lBQ0QsMEJBQTBCO0lBQzFCLHdCQUF3QjtJQUN4QiwyQkFBMkI7SUFDM0IsU0FBUztJQUNULHdCQUF3QjtJQUN4QixJQUFJO0lBQ0osV0FBVyxDQUFDLEtBQXdCLElBQVMsQ0FBQztJQUM5QyxZQUFZLENBQUMsSUFBWSxFQUFFLEtBQVUsRUFBRSxTQUFjO1FBQ25ELElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksS0FBSyxDQUFDLEtBQUssRUFBRTtZQUNmLElBQUksbUJBQW1CLEdBQUcsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUN4RSxJQUFJLG1CQUFtQjtnQkFBRSxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxNQUFNLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDMUU7YUFBTTtZQUNMLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1NBQzlCO0lBQ0gsQ0FBQztJQUNELFlBQVksQ0FBQyxXQUFtQixFQUFFLFlBQVk7UUFDNUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUM5QyxJQUFJLFVBQVUsR0FBRyxXQUFXLENBQUMsTUFBTSxDQUFDO1FBQ3BDLElBQUksYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxHQUFHLFdBQVcsQ0FBQztRQUM1QyxJQUFJLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTtZQUNqQyxJQUFJLE1BQU0sR0FBRyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDO1lBQ2xELElBQUksWUFBWSxHQUFHLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFDMUQsSUFBSSxLQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO1lBQ2hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7WUFDZCxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsVUFDbkQsV0FBVztnQkFFWCxJQUFJLFdBQVcsQ0FBQyxNQUFNLEVBQUU7b0JBQ3RCLElBQUk7d0JBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDOzRCQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDOzRCQUNuRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzs0QkFDdkQsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDVCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQztpQkFDaEM7cUJBQU0sSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO29CQUNoQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7aUJBQzdDO3FCQUFNLElBQUcsV0FBVyxDQUFDLG1CQUFtQixFQUFDO29CQUN4QyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUcsU0FBUyxDQUFFLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDeko7cUJBQU07b0JBQ0wsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDN0Q7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNoRSxnQ0FBZ0M7WUFDaEMsNkNBQTZDO1lBQzNDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO1lBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDbEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQzVELFlBQVksQ0FDRCxDQUFDO2dCQUNkLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNoQyxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDakUsSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDbEIsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNwQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsVUFBVSxJQUFJO3dCQUNqQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztvQkFDM0MsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLElBQUksR0FBRyxRQUFRLENBQUM7aUJBQ3RCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDTCxXQUFXO1lBQ1gsOEJBQThCO1lBQzlCLGlDQUFpQztZQUNqQyxJQUFJO1NBQ0w7SUFDSCxDQUFDO0lBQ0QsZ0JBQWdCLENBQUMsSUFBSTtRQUNuQixJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDdkIsOERBQThEO1lBQzlELElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1lBQ3hDLElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1lBQ2hELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO1lBQ2xELElBQUksS0FBSyxHQUFHLEVBQUMsS0FBSyxFQUFDLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztZQUMvQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsVUFBVSxXQUFXO2dCQUMxQyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxlQUFlLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO29CQUN6QixLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7aUJBQzdDO3FCQUFNLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTtvQkFDN0IsSUFBSSxXQUFXLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUMxRCxzSkFBc0o7d0JBQ3RKLCtIQUErSDt3QkFDL0gsSUFBSSxFQUFFLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDOzRCQUN4QyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FDSCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQ3JELFdBQVcsQ0FBQyxNQUFNLENBQ25COzRCQUNILENBQUMsQ0FBQyxFQUFFLENBQUM7d0JBQ1AsSUFBSSxFQUFFLENBQUMsTUFBTSxFQUFFOzRCQUNiLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDO2dDQUNyQixXQUFXLENBQUMsaUJBQWlCLElBQUksT0FBTyxFQUFFLEtBQUssUUFBUTtvQ0FDckQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDO29DQUNwQixDQUFDLENBQUMsRUFBRSxDQUFDO3lCQUNWO3FCQUNGO3lCQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUU7d0JBQzVDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDOzRCQUN6RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQzs0QkFDdkQsQ0FBQyxDQUFDLEVBQUUsQ0FBQzt3QkFDUCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxpQkFBaUI7NEJBQ3JELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ3pDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO3FCQUM3QjtpQkFDRjtxQkFBTTtvQkFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM3RDtZQUNILENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsVUFBVSxDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNsRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFhLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3ZELElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FDdEQsSUFBSSxDQUFDLElBQUksRUFDVCxJQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FDOUIsQ0FBQztnQkFDRixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ3pDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDdkUsQ0FBQyxDQUFDLENBQUM7U0FDSjtJQUNILENBQUM7SUFDRCxZQUFZLENBQ1YsS0FBbUMsRUFDbkMsY0FBYyxFQUNkLFNBQVMsRUFDVCxTQUFTO1FBRVQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztRQUM3QixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUNoRCwwQ0FBMEM7UUFDMUMsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUN4QyxnQ0FBZ0M7WUFDaEMsMERBQTBEO1lBQzFELEtBQUs7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDN0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQztnQkFDckUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDO2dCQUNwQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ1AsNEVBQTRFO1lBQzVFLGlFQUFpRTtZQUNqRSxJQUFJO1lBQ0osSUFBSSxTQUFTLEdBQUUsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxFQUFFLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7WUFDbEYsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUMsU0FBUyxDQUFDLENBQUM7WUFDdkMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxFQUFFO2dCQUNqQixJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQzdEO1lBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRSxZQUFZLENBQUMsQ0FBQztZQUN0RCxJQUFJLGdCQUFnQixHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7Z0JBQ3hELElBQUksRUFBRSxjQUFjO2FBQ3JCLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUscUJBQXFCLENBQUMsQ0FBQztZQUNuRCxLQUFLLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7WUFDeEMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLENBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLEVBQ2xDLFNBQVMsQ0FDVixDQUFDO1lBQ0YsMkJBQTJCO1lBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7WUFDakUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ3JELElBQUksZ0JBQWdCLElBQUksZ0JBQWdCLENBQUMsY0FBYyxDQUFDLEVBQUU7Z0JBQ3hELElBQUksY0FBYyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7b0JBQ3RELElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUM7aUJBQ3ZDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7YUFDdkM7WUFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxDQUFDO1lBQ3pFLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUNwRSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztTQUN0RTtJQUNILENBQUM7SUFDRCxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsY0FBYyxFQUFFLFNBQVM7UUFDakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDbkQsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDdEUsSUFBSSxhQUFhLElBQUksQ0FBQyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxDQUFDLGFBQWEsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUM3RDtRQUNELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUM5QyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxFQUNsQyxTQUFTLENBQ1YsQ0FBQztRQUVGLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDekUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFDLGtDQUFrQyxDQUFDLENBQUE7UUFDL0QsSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFO1lBQ3hELElBQUksRUFBRSxjQUFjO1NBQ3JCLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQTtRQUM3QixJQUFHLElBQUksQ0FBQyxVQUFVLEVBQUM7WUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLHFCQUFxQixDQUFDLENBQUM7WUFDckQsSUFBSSxnQkFBZ0IsSUFBSSxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDeEQsSUFBSSxjQUFjLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsRUFBRTtvQkFDdEQsSUFBSSxFQUFFLGdCQUFnQixDQUFDLGNBQWMsQ0FBQztpQkFDdkMsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUN2QztTQUNKO2FBQUk7WUFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUFDLGNBQWMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQzNEO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUNqRSxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDdkQsQ0FBQztJQUNELGFBQWE7UUFDWCxJQUFJLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQztRQUMxQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7YUFDL0QsZ0JBQWdCLENBQUM7UUFDcEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsU0FBUyxDQUFDLEdBQUcsQ0FBQztRQUN0QyxDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsRUFBRSxVQUFVLElBQUk7WUFDckMsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNmLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDN0QsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDaEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDUCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDNUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUJBQ3BJO3FCQUFNO29CQUNMLHNFQUFzRTtvQkFDdEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO3dCQUM1QixTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzs0QkFDdkQsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzs0QkFDbkMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDVjthQUNGO2lCQUFLLElBQUcsSUFBSSxDQUFDLFdBQVcsRUFBQztnQkFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMzRDtpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3ZEO1lBQ0QsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNwQixJQUFHLElBQUksQ0FBQyxXQUFXLEVBQUM7b0JBQ2xCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQzt3QkFDbkUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQzt3QkFDbEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUNuRCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FDbkQsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FDZixDQUFDO2lCQUNIO2dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUEsRUFBRSxDQUFDO2dCQUNyRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkUsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLGVBQWUsRUFBRTtvQkFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsR0FBSSxJQUFJLENBQUMsYUFBYSxDQUFDO2lCQUN2RDtnQkFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7b0JBQ25CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDdkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2lCQUNqRDthQUNGO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUU7WUFDdEQsSUFBSSxlQUFlLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7WUFDdEQsSUFDRSxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQztnQkFDL0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsQ0FBQyxNQUFNLEVBQ3RDO2dCQUNBLElBQUksQ0FBQyxpQkFBaUI7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7d0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7d0JBQzlDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1QsSUFBSSxDQUFDLGVBQWU7b0JBQ2xCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7d0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQjt3QkFDcEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDVCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7YUFDbkM7U0FDRjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3JFLElBQUksQ0FBQyxpQkFBaUI7b0JBQ3BCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7d0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7d0JBQzlDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1QsSUFBSSxDQUFDLGVBQWU7b0JBQ2xCLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0I7d0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQjt3QkFDcEMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDVCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO2dCQUMvRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO2FBQ25DO1NBQ0Y7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRTtZQUN4RCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUU7Z0JBQ3BELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztnQkFDbEQsSUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJO29CQUNsQyxJQUFJLENBQUMsU0FBUztvQkFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsRUFDeEQ7b0JBQ0EsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQ3JCLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ1gsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ3BELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQ3JCLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBRWQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO2lCQUNwRDthQUNGO1NBQ0Y7UUFDRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDL0Isd0VBQXdFO1FBQ3hFLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDdkUsQ0FBQztJQUVELFdBQVc7UUFDVCxPQUFPLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7UUFFNUMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDekIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxJQUFJO2dCQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDM0IsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO29CQUN2QixJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztvQkFDeEMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7b0JBQ2hELElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDO29CQUNsRCxJQUFJLEtBQUssR0FBRyxFQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7b0JBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLFVBQVUsV0FBVzt3QkFDMUMsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFOzRCQUN6QixLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7eUJBQzdDOzZCQUFNOzRCQUNMLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLGlCQUFpQjtnQ0FDckQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7Z0NBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdkM7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO3dCQUNsRSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFhLENBQUM7d0JBQ3BELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2hDLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO3dCQUNqRSxJQUFJLENBQUMsYUFBYSxFQUFFOzRCQUNsQixNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7NEJBQ3BCLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUU7Z0NBQ25DLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dDQUN2RCxJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQ0FDN0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsVUFBVSxPQUFPO29DQUMvQixRQUFRLENBQUMsSUFBSSxDQUFDO3dDQUNaLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3Q0FDNUMsR0FBRyxFQUFFLE9BQU87cUNBQ2IsQ0FBQyxDQUFDO2dDQUNMLENBQUMsQ0FBQyxDQUFDOzZCQUNKO2lDQUFNO2dDQUNMLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLElBQUk7b0NBQ2pDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO2dDQUMzQyxDQUFDLENBQUMsQ0FBQzs2QkFDSjs0QkFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLFFBQVEsQ0FBQzt5QkFDdEI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO2FBQU07U0FDTjtJQUNILENBQUM7SUFDRCxTQUFTLENBQUMsSUFBSSxFQUFFLEVBQUU7UUFDaEIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLE1BQU07Z0JBQ25ELENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQztnQkFDdkIsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNULE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ2xELElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN4QztJQUNILENBQUM7SUFDRCxhQUFhLENBQUMsSUFBSSxFQUFFLFVBQVU7UUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUMxQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksUUFBUSxFQUFFO1lBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3RDO2FBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLGNBQWMsRUFBRTtZQUN4QyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3ZFLFVBQVUsQ0FBQyxJQUFJLENBQ2hCLENBQUM7WUFDRixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO2dCQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQztZQUU1QyxpRUFBaUU7WUFDakUsSUFBSSxXQUFXLEdBQUcsVUFBVSxDQUFDLGVBQWU7Z0JBQzFDLENBQUMsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLFdBQVc7Z0JBQ3hDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDUCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxtQkFBbUIsR0FBRyxVQUFVLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQztZQUNsRSxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUNsQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsVUFBVSxNQUFNO2dCQUN2QyxJQUFJLE1BQU0sRUFBRTtvQkFDVixJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxFQUFFLFVBQVUsVUFBVTt3QkFDMUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDdEQsSUFBSSxVQUFVLEVBQUU7NEJBQ2QsSUFBSSxDQUFDLGNBQWM7aUNBQ2hCLGFBQWEsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7aUNBQzVELFNBQVMsQ0FDUixDQUFDLEdBQUcsRUFBRSxFQUFFO2dDQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0NBQzdDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztnQ0FDRixJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFO29DQUNyQixZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO29DQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO29DQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO2lDQUNyRDs0QkFDSCxDQUFDLEVBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQ0FDUixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLEtBQUssQ0FBQyxDQUFDO2dDQUM3QyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQ0FDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDOzRCQUNKLENBQUMsQ0FDRixDQUFDO3lCQUNMO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxXQUFXLEVBQUU7WUFDckMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1NBQ2pCO2FBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLFFBQVEsRUFBRTtZQUNsQyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQzNCO2FBQU0sSUFBRyxJQUFJLENBQUMsTUFBTSxJQUFFLFFBQVEsRUFBQztZQUM5QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7YUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksWUFBWSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksZUFBZSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksUUFBUSxFQUFFO1lBQ2pHLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxlQUFlLENBQUMsQ0FBQztZQUM5QyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLEVBQUU7Z0JBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FDakUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzlCLENBQUM7Z0JBQ0YsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRTtvQkFDakMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFDL0QsSUFBSSxDQUFDLFNBQVMscUJBQVEsSUFBSSxDQUFDLFNBQVMsRUFBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUUsQ0FBQztpQkFDbkQ7Z0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGtCQUFrQixDQUFDLENBQUM7YUFDakQ7aUJBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQixFQUFFO2dCQUM5QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztnQkFDaEUsSUFBSSxDQUFDLFNBQVMscUJBQVEsSUFBSSxDQUFDLFNBQVMsRUFBSyxRQUFRLENBQUUsQ0FBQzthQUNyRDtZQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUMvQixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDakI7YUFBTTtZQUNMLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7WUFDL0IsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEtBQUssQ0FBQztZQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxVQUFVLENBQUMsQ0FBQztZQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksVUFBVSxDQUFDLFlBQVksRUFBRTtnQkFDM0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsTUFBTTtvQkFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUMsQ0FBQztvQkFDekMsSUFBSSxNQUFNLEVBQUU7d0JBQ1YsSUFBSSxlQUFlLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQzt3QkFDaEQsSUFBSSxjQUFjLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQzt3QkFDakQsSUFBSSxNQUFNLEdBQUcsZUFBZSxDQUFDLE1BQU0sQ0FBQzt3QkFDcEMsSUFBSSxtQkFBbUIsR0FBRyxlQUFlLENBQUMsWUFBWSxDQUFDO3dCQUN2RCxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQy9CLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQ3JDLENBQUM7d0JBQ0YsSUFBSSxXQUFXLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO3dCQUN0QyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7NEJBQ3RDLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxNQUFNO2dDQUN4QixDQUFDLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0NBQzNDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ2pDLElBQUksUUFBUSxFQUFFO2dDQUNaLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztvQ0FDMUIsQ0FBQyxDQUFDLFFBQVEsQ0FBQzs2QkFDZDtpQ0FBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0NBQzVCLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWU7b0NBQzNDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7b0NBQzVCLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDOzZCQUNoQjt3QkFDSCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO3dCQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUNqQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUM5RCxDQUFDLE1BQU0sRUFBRSxFQUFFOzRCQUNULElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxFQUFFLENBQUM7NEJBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDOzRCQUNsRCxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxZQUFZLEVBQUUsb0JBQW9CLENBQUMsQ0FBQzs0QkFDaEUsSUFBSSxRQUFRLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FDNUIsZUFBZSxDQUFDLFlBQVksQ0FDakIsQ0FBQzs0QkFDZCxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsQ0FBQzs0QkFDdEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7NEJBQzFCLHlDQUF5Qzs0QkFDekMsMkNBQTJDOzRCQUMzQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzs0QkFDeEMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7NEJBQ25ELElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzs0QkFDdkQsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsb0JBQW9CO2dDQUMzRCxDQUFDLENBQUMsS0FBSztnQ0FDUCxDQUFDLENBQUMsSUFBSSxDQUFDOzRCQUNULElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQjtnQ0FDOUQsQ0FBQyxDQUFDLElBQUk7Z0NBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQzs0QkFDVixzQkFBc0I7NEJBQ3RCLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQzt3QkFDSixDQUFDLEVBQ0QsQ0FBQyxHQUFHLEVBQUUsRUFBRTs0QkFDTixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQzt3QkFDM0IsQ0FBQyxDQUNGLENBQUM7cUJBQ0g7eUJBQU07cUJBQ047Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjtpQkFBTTtnQkFDTCxJQUFJLGVBQWUsR0FBRyxVQUFVLENBQUMsY0FBYyxDQUFDO2dCQUNoRCxJQUFJLGNBQWMsR0FBRyxlQUFlLENBQUMsV0FBVyxDQUFDO2dCQUNqRCxJQUFJLE1BQU0sR0FBRyxlQUFlLENBQUMsTUFBTSxDQUFDO2dCQUNwQyxJQUFJLG1CQUFtQixHQUFHLGVBQWUsQ0FBQyxZQUFZLENBQUM7Z0JBQ3ZELElBQUksZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLElBQUksV0FBVyxHQUFHLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztnQkFDdEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxJQUFJO29CQUN0QyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTTt3QkFDeEIsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUMzQyxDQUFDLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNqQyxJQUFJLFFBQVEsRUFBRTt3QkFDWixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlOzRCQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7NEJBQzFCLENBQUMsQ0FBQyxRQUFRLENBQUM7cUJBQ2Q7eUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO3dCQUM1QixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlOzRCQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDOzRCQUM1QixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztxQkFDaEI7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDNUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDakMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDOUQsQ0FBQyxNQUFNLEVBQUUsRUFBRTtvQkFDVCxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztvQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLG9CQUFvQixDQUFDLENBQUM7b0JBQ2hFLElBQUksUUFBUSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQzVCLGVBQWUsQ0FBQyxZQUFZLENBQ2pCLENBQUM7b0JBQ2QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsY0FBYyxDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO29CQUMxQix5Q0FBeUM7b0JBQ3pDLDJDQUEyQztvQkFDM0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7b0JBQ3hDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO29CQUNuRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEdBQUcsTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUM7b0JBQzlCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQjt3QkFDOUQsQ0FBQyxDQUFDLElBQUk7d0JBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDVixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7Z0JBQ0osQ0FBQyxFQUNELENBQUMsR0FBRyxFQUFFLEVBQUU7b0JBQ04sSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztvQkFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQzNCLENBQUMsQ0FDRixDQUFDO2FBQ0g7U0FDRjtJQUNILENBQUM7SUFDRCxtQkFBbUIsQ0FBQyxRQUFRO1FBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ2hFLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDckUsSUFBSSxDQUFDLFNBQVMscUJBQVEsSUFBSSxDQUFDLFNBQVMsRUFBSyxRQUFRLENBQUUsQ0FBQztTQUNyRDtRQUNELElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRTtTQUNyQztRQUNELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDbkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLDhCQUE4QixDQUFDLENBQUM7UUFDNUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLDBCQUEwQixDQUFDLENBQUM7UUFDekQsMEVBQTBFO1FBQzFFLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN2RCxtREFBbUQ7UUFDbkQsNkJBQTZCO1FBQzdCLGtDQUFrQztRQUNsQyw2Q0FBNkM7UUFDN0MsS0FBSztRQUNMLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsRUFBRTtZQUN2QyxJQUNFLElBQUksQ0FBQyxTQUFTO2dCQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDO2dCQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sRUFDckM7Z0JBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDckUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hCO2lCQUFNO2dCQUNMLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDO2dCQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEtBQUs7b0JBQ3ZDLDBDQUEwQztvQkFDMUMsd0NBQXdDO29CQUN4QyxZQUFZO29CQUNaLEtBQUs7b0JBQ0wsbUVBQW1FO29CQUNuRSxLQUFLO2dCQUNQLENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDO2dCQUNuRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FDdEUsQ0FBQztnQkFDRixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDakI7U0FDRjthQUFNO1lBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFO2dCQUM1QyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEI7U0FDRjtJQUNILENBQUM7SUFDRCxxQkFBcUIsQ0FBQyxjQUFjLEVBQUUsUUFBUTtRQUM1QyxJQUFJLElBQUksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDakUsSUFBSSxVQUFVLEdBQUcsTUFBTSxDQUNyQixJQUFJLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFDaEUsb0JBQW9CLENBQ3JCLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDWCxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUN4QyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNsRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxHQUFHLE1BQU0sRUFBRSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN4RCxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksQ0FBQyxTQUFTLHFCQUFRLElBQUksQ0FBQyxTQUFTLEVBQUssZ0JBQWdCLENBQUUsQ0FBQztRQUM1RCxJQUFJLENBQUMsU0FBUyxxQkFBUSxJQUFJLENBQUMsU0FBUyxFQUFLLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFFLENBQUM7UUFDakUsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztRQUNyQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7WUFDdEMsSUFBSSxRQUFRLENBQUM7WUFDYixJQUNFLElBQUksQ0FBQyxNQUFNO2dCQUNYLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztnQkFDMUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUN2QztnQkFDQSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3BEO2lCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUN2QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7b0JBQ2xCLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUN2QjtxQkFBTSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtvQkFDbkMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQztnQkFDRCxpQ0FBaUM7Z0JBQ2pDLHFDQUFxQztnQkFDckMsSUFBSTtxQkFDQztvQkFDSCxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0Y7WUFDRCxJQUFJLFFBQVEsRUFBRTtnQkFDWixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlO29CQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7b0JBQzFCLENBQUMsQ0FBQyxRQUFRLENBQUM7YUFDZDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxDQUFDLFdBQVcsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFDRCxjQUFjLENBQUMsR0FBRztRQUNoQixJQUFJLEdBQUcsQ0FBQyxPQUFPLEVBQUU7WUFDZixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNwRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEtBQUssQ0FBQztZQUM5QixJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDL0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsR0FBRyxLQUFLLENBQUM7U0FDakM7SUFDSCxDQUFDO0lBRUQsZUFBZSxDQUFDLE1BQU0sRUFBRSxPQUFPO1FBQzdCLElBQUksS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzVELElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzlDO2FBQU07WUFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDakQ7SUFDSCxDQUFDO0lBQ0Qsb0JBQW9CLENBQUMsS0FBSyxFQUFFLElBQUk7UUFDOUIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLFVBQVUsSUFBSTtZQUM1QixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksS0FBSyxFQUFFO2dCQUN2QixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQzthQUM5QjtpQkFBTTtnQkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxLQUFLLENBQUM7YUFDcEM7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxhQUFhLENBQUMsVUFBVSxFQUFFLE9BQU87UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyw0QkFBNEIsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUNuRCxJQUFJLGFBQWEsRUFBRSxjQUFjLEVBQUUsTUFBTSxDQUFDO1FBQzFDLElBQUksV0FBVyxHQUFHLEVBQUUsRUFDbEIsVUFBVSxDQUFDO1FBQ2IsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3hFLElBQUksYUFBYSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQztZQUNyRCxhQUFhLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUM7WUFDL0MsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQztZQUN2RSxNQUFNLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQztZQUM5QixVQUFVLHFCQUFRLFVBQVUsRUFBSyxVQUFVLENBQUUsQ0FBQztZQUM5QyxjQUFjLEdBQUcsYUFBYSxDQUFDLFdBQVcsQ0FBQztTQUM1QzthQUFNO1lBQ0wsSUFBSSxXQUFXLEdBQUcsT0FBTyxDQUFDLGFBQWEsQ0FBQztZQUN4QyxhQUFhLEdBQUcsV0FBVztnQkFDekIsQ0FBQyxDQUFDLFdBQVc7Z0JBQ2IsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztZQUM3QyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUM7WUFDckQsY0FBYyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7WUFDM0MsTUFBTSxHQUFHLGFBQWEsQ0FBQyxNQUFNLENBQUM7U0FDL0I7UUFDRCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxJQUFJO1lBQ3RDLElBQUksUUFBUSxDQUFDO1lBQ2IsSUFDRSxJQUFJLENBQUMsTUFBTTtnQkFDWCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDdkM7Z0JBQ0EsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUN0QixJQUFJLElBQUksR0FBRyxFQUFFLENBQUM7b0JBQ2QsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsVUFDakQsUUFBUTt3QkFFUixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUN0QixDQUFDLENBQUMsQ0FBQztvQkFDSCxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDakM7cUJBQU07b0JBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDcEQ7YUFDRjtpQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDdkIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUNwQixJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMsZUFBZTt3QkFDM0IsQ0FBQyxDQUFDLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQzt3QkFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7b0JBQ2YsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDOUQ7cUJBQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUM3QixRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDM0Q7cUJBQU07b0JBQ0wsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNyRTthQUNGO1lBQ0QsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUNyRSxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDeEM7WUFDRCxJQUFJLFFBQVE7Z0JBQ1YsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZTtvQkFDM0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO29CQUMxQixDQUFDLENBQUMsUUFBUSxDQUFDO1FBQ2pCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxhQUFhLENBQUMsY0FBYyxFQUFFO1lBQ2hDLDJCQUEyQjtZQUMzQixnQkFBZ0I7WUFDaEIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDdkQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLE1BQU07Z0JBQ25DLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUUsTUFBTSxDQUFDLENBQUM7Z0JBQ3hDLElBQUksbUJBQW1CLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQztnQkFDckQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDOUQsQ0FBQyxHQUFHLEVBQUUsRUFBRTtvQkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7b0JBQ0YsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTt3QkFDckIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO3dCQUM3QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUNyRDtnQkFDSCxDQUFDLEVBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDUixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO2dCQUNKLENBQUMsQ0FDRixDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7U0FDSjthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWM7aUJBQ2hCLGlCQUFpQixDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUM7aUJBQ3RDLFNBQVMsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNsQixJQUNFLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVTtvQkFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUNyQztvQkFDQSxJQUFJLG1CQUFtQixHQUFHLGFBQWEsQ0FBQyxZQUFZLENBQUM7b0JBQ3JELElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQztvQkFDRixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO2lCQUMzQjtxQkFBTTtvQkFDTCxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN2RCxhQUFhLENBQUMsZ0JBQWdCLENBQy9CLENBQUM7b0JBQ0YsSUFBSSxDQUFDLGtCQUFrQixDQUNyQixJQUFJLEVBQ0osUUFBUSxHQUFHLFVBQVUsQ0FBQyxRQUFRLEVBQzlCLFVBQVUsQ0FBQyxhQUFhLENBQ3pCLENBQUM7aUJBQ0g7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNOO0lBQ0gsQ0FBQztJQUVELGtCQUFrQixDQUFDLFdBQVcsRUFBRSxRQUFRLEVBQUUsYUFBYTtRQUNyRCxNQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7UUFDOUQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELGtCQUFrQixDQUFDLEtBQW1DLEVBQUUsRUFBRTtRQUN4RCxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTTtnQkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRSxDQUFDO2dCQUN2QixDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1QsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5RCxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7WUFDYixJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ2hEO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztJQUM1QyxDQUFDO0lBRUQscUJBQXFCLENBQUMsS0FBd0I7UUFDNUMsTUFBTSxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO1lBQ3hCLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ25DLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQy9CLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO0lBQzVDLENBQUM7SUFDRCxhQUFhLENBQUMsS0FBSztRQUNqQixJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7UUFDaEIsSUFBSSxNQUFNLENBQUM7UUFDWCxJQUFJLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFDdkIsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksVUFBVSxHQUFHLEVBQUUsQ0FBQztRQUNwQixJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFO1lBQ3pCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsVUFBVSxJQUFJO2dCQUM3QyxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7Z0JBQ3BDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksRUFBRSxFQUFFO29CQUM5QyxhQUFhLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7b0JBQzlELFFBQVEsR0FBRzt3QkFDVCxVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU07d0JBQ3ZCLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixJQUFJLEVBQUUsS0FBSztxQkFDWixDQUFDO2dCQUNKLENBQUMsQ0FBQyxDQUFDO1lBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDSCxVQUFVLHFCQUFRLGFBQWEsRUFBSyxRQUFRLENBQUUsQ0FBQztZQUMvQyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQ2pFLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ1AsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1lBQ3RDLENBQUMsRUFDRCxDQUFDLEtBQUssRUFBRSxFQUFFO2dCQUNSLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsQ0FBQyxDQUNGLENBQUM7U0FDSDtJQUNILENBQUM7SUFDRCxZQUFZLENBQUMsUUFBUSxFQUFFLEtBQUs7UUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN6RCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FDckIsQ0FBQztRQUNGLElBQUksS0FBSyxLQUFLLEtBQUssRUFBRTtZQUNuQixNQUFNLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUM7WUFDeEQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsR0FBRyxNQUFNLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0wsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDO1lBQ3pELFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLENBQUM7U0FDbEQ7SUFDSCxDQUFDO0lBQ0QsWUFBWSxDQUFDLEtBQUs7UUFDaEIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLEtBQUssQ0FBQztRQUNuQyxJQUFJLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ3BELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO1lBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7WUFDOUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNQLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsSUFBSSxFQUFFLENBQUM7UUFDaEUsSUFDRSxJQUFJLENBQUMsVUFBVTtZQUNmLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO1lBQ2xDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsUUFBUSxFQUMzQztZQUNBLElBQUksQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FDL0MsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQzVDLENBQUM7U0FDSDthQUFNO1lBQ0wsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQztTQUNoRTtRQUNELElBQUksSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7WUFDM0QsVUFBVSxDQUFDLEdBQUcsRUFBRTtnQkFDZCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDO1lBQ3BDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUNUO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsV0FBVyxDQUFDLEtBQUs7UUFDZixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDO1FBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzdCLFVBQVUsQ0FBQyxHQUFFLEVBQUU7WUFDYixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQ2hDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQTtJQUNULENBQUM7SUFFRCxTQUFTLENBQUMsS0FBSztRQUNiLEtBQUssR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksZUFBZSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUMvRCxJQUFJLENBQUMsU0FBUyxHQUFHLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM1RSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZUFBZSxDQUFDO1FBQ3hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsNkJBQTZCLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDbEUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUU7WUFDeEUsOEJBQThCO1lBQzlCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQ2hELElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDO1lBQ3hDLElBQUksTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsY0FBYyxDQUFDO1lBQzlDLElBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLENBQUMsT0FBTyxDQUFDO2dCQUNwQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxFQUM1QztnQkFDQSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FDN0QsT0FBTyxDQUNSLENBQUMsTUFBTSxDQUFDO29CQUNQLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQztvQkFDOUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzthQUNSO1lBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFO2dCQUNwRSwwQkFBMEI7Z0JBQzFCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQ2hFLFdBQVcsQ0FDWixDQUFDLE9BQU8sQ0FBQyxDQUFDO2FBQ1o7U0FDRjthQUFNO1NBQ047UUFDRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO1FBQzlCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQjtZQUM5RCxDQUFDLENBQUMsSUFBSTtZQUNOLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDWixDQUFDO0lBRUQsWUFBWTtRQUNWLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUNwQyxJQUFJLElBQUksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUNqRSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDOUIsQ0FBQztTQUNIO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDaEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQTtRQUNwRSxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsTUFBTTtZQUNuQyxJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDeEI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFDRCxlQUFlLENBQUMsUUFBUTtRQUN0QixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNoRSxPQUFPLENBQUMsR0FBRyxDQUFDLGdDQUFnQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBRXhELElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUU7WUFDckUsSUFBSSxDQUFDLFNBQVMscUJBQVEsSUFBSSxDQUFDLFNBQVMsRUFBSyxRQUFRLENBQUUsQ0FBQztTQUNyRDtRQUNELElBQ0UsUUFBUTtZQUNSLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDO1lBQzNDLFFBQVEsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsTUFBTSxFQUNsRDtZQUNBLElBQUksQ0FBQyxTQUFTLHFCQUFRLElBQUksQ0FBQyxTQUFTLEVBQUssUUFBUSxDQUFFLENBQUM7U0FDckQ7UUFDRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLEVBQUU7U0FDckM7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLHNCQUFzQixFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLEVBQUU7WUFDdkMsSUFDRSxJQUFJLENBQUMsU0FBUztnQkFDZCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUM7Z0JBQ2pELElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE1BQU0sRUFDeEQ7Z0JBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDckUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hCO2lCQUFNLElBQ0wsSUFBSSxDQUFDLFNBQVM7Z0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTTtnQkFDckMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixFQUNsQztnQkFDQSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDaEI7aUJBQU07Z0JBQ0wsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7Z0JBQ25FLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUN0RSxDQUFDO2dCQUNGLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNqQjtTQUNGO2FBQU0sSUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLG1CQUFtQjtZQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFDOUI7WUFDQSx3Q0FBd0M7WUFDeEMsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxFQUM5QyxVQUFVLElBQUk7Z0JBQ1osT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLFFBQVEsQ0FBQztZQUNqQyxDQUFDLENBQ0YsQ0FBQztZQUNGLElBQUksUUFBUSxFQUFFO2dCQUNaLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNoQjtpQkFBTTtnQkFDTCxJQUFJLG1CQUFtQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQztnQkFDbkUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQ3RFLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7Z0JBQzVCLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNqQjtTQUNGO2FBQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLG9CQUFvQixFQUFFO1lBQy9DLElBQ0UsSUFBSSxDQUFDLFNBQVM7Z0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUNqQztnQkFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FBQztxQkFDcEUsTUFBTSxFQUNUO2dCQUNBLElBQ0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLElBQUksS0FBSztvQkFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxxQkFBcUIsQ0FBQyxDQUNuQyxJQUFJLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUNqQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQ2I7b0JBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztvQkFDckUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNoQjtxQkFBTSxJQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxJQUFJLFdBQVc7b0JBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLENBQUMsQ0FDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxnQkFBZ0IsQ0FDakMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUNiO29CQUNBLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3JFLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDaEI7cUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFO29CQUN0QyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNyRSxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7aUJBQ2hCO3FCQUFNO29CQUNMLElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDO29CQUNuRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7b0JBQ0YsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7b0JBQzVCLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDakI7YUFDRjtpQkFBTTtnQkFDTCwrREFBK0Q7Z0JBQy9ELHdFQUF3RTtnQkFDeEUsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsRUFBRTtvQkFDaEMsSUFBSSxtQkFBbUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxZQUFZLENBQUM7b0JBQ25FLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztvQkFDRixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztvQkFDNUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQjtxQkFBTSxJQUFJLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUU7b0JBQ3hDLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDaEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxVQUFVLElBQUk7d0JBQ3JELG9DQUFvQzt3QkFDcEMsSUFDRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQzs0QkFDdkIsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUNuQzs0QkFDQSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQzs0QkFDNUIsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUNqQjs2QkFBTSxJQUNMLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDOzRCQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQ2xDOzRCQUNBLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDOzRCQUM3QixRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7eUJBQ2hCO29CQUNILENBQUMsQ0FBQyxDQUFDO2lCQUNKO3FCQUFNO29CQUNMLElBQUksbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDO29CQUNuRSxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7b0JBQ0YsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQjthQUNGO1NBQ0Y7YUFBTTtZQUNMLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRTtnQkFDNUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztnQkFDckUsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ2hCO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsUUFBUSxDQUFDLFFBQVE7UUFDZixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM5QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDO1FBQ2xELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDO1FBQzlELFlBQVk7UUFDWixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUU7WUFDakQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxDQUFDO2dCQUMvQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsYUFBYSxFQUFFO29CQUN4QixJQUFJLElBQUksR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztvQkFDcEMsSUFDRSxDQUFDLENBQUMsYUFBYSxDQUFDLElBQUk7d0JBQ3BCLElBQUksQ0FBQyxTQUFTO3dCQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO3dCQUNwQixDQUFDLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQzFDO3dCQUNBLElBQUksQ0FBQyxTQUFTOzRCQUNaLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQzt3QkFDdEQsSUFBSSxDQUFDLFlBQVk7NEJBQ2YsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDO3FCQUMxRDtpQkFDRjtnQkFDRCxJQUFJLENBQUMsQ0FBQyxjQUFjLEVBQUU7b0JBQ3BCLDhEQUE4RDtvQkFDOUQsSUFBSSxNQUFNLEdBQUcsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7b0JBQ3JDLElBQUksWUFBWSxHQUFHLENBQUMsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO29CQUM3QyxJQUFJLFdBQVcsR0FBRyxDQUFDLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztvQkFDL0MsSUFBSSxLQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO29CQUNoQyxDQUFDLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxVQUFVLFdBQVc7d0JBQzFDLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTs0QkFDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO3lCQUM3Qzs2QkFBTTs0QkFDTCxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUM3RDtvQkFDSCxDQUFDLENBQUMsQ0FBQztvQkFDSCxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7d0JBQ2xFLENBQUMsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQWEsQ0FBQztvQkFDbkQsQ0FBQyxDQUFDLENBQUM7aUJBQ0o7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBRUQsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7UUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDOUMsSUFDRSxJQUFJLENBQUMsVUFBVTtZQUNmLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxFQUNuRTtZQUNBLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxLQUFLLENBQUM7WUFDbkMsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3pELElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7b0JBQ25FLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFNBQVM7b0JBQzlDLENBQUMsQ0FBQyxFQUFFLENBQUM7Z0JBQ1AsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDO2dCQUMxRCxJQUNFLElBQUksQ0FBQyxVQUFVO29CQUNmLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCO29CQUNsQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFDM0M7b0JBQ0EsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FDNUMsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO2lCQUNoRTtnQkFDRCxVQUFVLENBQUMsR0FBRyxFQUFFO29CQUNkLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUM7Z0JBQ3BDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQzthQUNUO1NBQ0Y7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRTtZQUN6RCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUUsVUFDNUMsZ0JBQWdCO2dCQUVoQixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLHdCQUF3QixDQUFDLENBQUM7Z0JBQ3hELElBQUksZ0JBQWdCLENBQUMsY0FBYyxFQUFFO29CQUNuQyxPQUFPLENBQUMsR0FBRyxDQUNULGdCQUFnQixDQUFDLGFBQWEsRUFDOUIsbUNBQW1DLENBQ3BDLENBQUM7b0JBQ0YsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDO3dCQUM5QyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3RDLDRJQUE0STtpQkFDN0k7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILCtGQUErRjtTQUNoRztRQUNELDZEQUE2RDtRQUU3RCxJQUFJO0lBQ04sQ0FBQztJQUVELG9CQUFvQixDQUFDLFNBQVMsRUFBRSxlQUFlLEVBQUUsUUFBUTtRQUN2RCxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUM7UUFDakIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxlQUFlLEVBQUUsVUFBVSxJQUFJO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3BCLEtBQUssR0FBRyxLQUFLLENBQUM7YUFDZjtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xCLENBQUM7SUFDRCxXQUFXO1FBQ1QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDNUIsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELElBQUksUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDcEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDL0MsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO2dCQUN0QixDQUFDLENBQUMsRUFBRSxDQUFDO1lBQ1AsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQztnQkFDeEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7Z0JBQzVDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDUCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDakIsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUM7WUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FDVCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsRUFDL0Isc0NBQXNDLENBQ3ZDLENBQUM7WUFDRixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDaEIsSUFBSSxDQUFDLG9CQUFvQixDQUN2QixXQUFXLEVBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQy9CLFVBQVUsS0FBSztvQkFDYixJQUFJLEtBQUssRUFBRTt3QkFDVCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLFVBQVUsT0FBTzs0QkFDdkQsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO3dCQUNyRCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLEtBQUssR0FDUCxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU07NEJBQzNCLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUM7NEJBQ2pDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDVCxJQUFJLEtBQUssR0FBRyxDQUFDLEVBQUU7NEJBQ2IsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQzt5QkFDekI7d0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxHQUFHLFNBQVMsQ0FBQzt3QkFDdkQsWUFBWSxDQUFDLE9BQU8sQ0FDbEIsY0FBYyxFQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUMvQixDQUFDO3dCQUNGLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7cUJBQ3pCO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLHlCQUF5QixDQUFDLENBQUM7cUJBQ3pEO2dCQUNILENBQUMsQ0FDRixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDekI7WUFDRCxrRUFBa0U7WUFDbEUsK0RBQStEO1lBQy9ELHlEQUF5RDtZQUN6RCxPQUFPO1lBQ1AsU0FBUztZQUNULDZEQUE2RDtZQUM3RCxJQUFJO1NBQ0w7SUFDSCxDQUFDO0lBQ0QsUUFBUSxDQUFDLElBQUksRUFBRSxhQUFhO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDOUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7UUFDaEUsSUFBSSxRQUFRLEdBQUcsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDekMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2pELElBQUksS0FBSyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUM5RCxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEQsSUFBSSxhQUFhLENBQUMsbUJBQW1CLEVBQUU7WUFDckMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztnQkFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQzFDLE9BQU8sR0FBRyxhQUFhLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztTQUN2QztRQUNELFFBQVEscUJBQU8sUUFBUSxFQUFLLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUM1QyxJQUFHLGFBQWEsQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsRUFBRSxVQUFTLFdBQVc7Z0JBQy9ELElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUNyQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFBO1NBQ0g7UUFFRCxRQUFRLENBQUMsZUFBZSxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztRQUMvQyxRQUFRLENBQUMscUJBQXFCLENBQUMsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUM7UUFDM0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7UUFDMUIsSUFDRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsYUFBYSxDQUFDLElBQUksQ0FBQztZQUN6QyxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFDbkM7WUFDQSxRQUFRLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2xFLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3hDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzVDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztTQUNoRTthQUFJO1lBQ0gsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1NBQ2hFO1FBRUQsSUFBSSxhQUFhLENBQUMsbUJBQW1CLEVBQUU7WUFDckMsSUFBSSxtQkFBbUIsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFO2dCQUM1RCxTQUFTLEVBQUUsSUFBSSxDQUFDLEtBQUs7YUFDdEIsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsRUFBRSxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7U0FDdEU7YUFBSyxJQUFHLGFBQWEsQ0FBQyxjQUFjLEVBQUM7WUFDbEMsSUFBRyxhQUFhLENBQUMsVUFBVSxFQUMzQjtnQkFDRSxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLFVBQVMsZUFBZTtvQkFDNUQsSUFBRyxlQUFlLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxjQUFjLEVBQUM7d0JBQ3RELGVBQWUsQ0FBQyxJQUFJLEdBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztxQkFDeEQ7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7YUFDRDtpQkFBSTtnQkFDSCxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLFVBQVMsZUFBZTtvQkFDOUQsSUFBRyxlQUFlLENBQUMsSUFBSSxJQUFJLGFBQWEsQ0FBQyxjQUFjLEVBQUM7d0JBQ3RELGVBQWUsQ0FBQyxJQUFJLEdBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQTtxQkFDM0Q7Z0JBQ0wsQ0FBQyxDQUFDLENBQUE7YUFDRDtTQUNKO2FBQU07WUFDTCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDcEI7SUFDSCxDQUFDO0lBRUQscUJBQXFCLENBQUMsbUJBQW1CLEVBQUUsWUFBWSxFQUFFLGFBQWE7UUFDcEUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsa0JBQWtCLEVBQUUsYUFBYSxDQUFDLENBQUM7UUFDdEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO1FBRTVELDJGQUEyRjtRQUMzRixJQUFJLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLGdCQUFnQixFQUFFO1lBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsZ0JBQWdCLENBQUM7Z0JBQ2xELFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3REO1FBQ0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksS0FBSyxDQUFDO1FBQ1YsSUFBSSxVQUFVLENBQUM7UUFDZixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLEVBQUU7WUFDdEQsS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3RFLElBQUksRUFBRSxtQkFBbUIsQ0FBQyxhQUFhO2FBQ3hDLENBQUMsQ0FBQztZQUNILFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN4RTthQUFNO1lBQ0wsS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQ2pCLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQ2xDLG1CQUFtQixDQUNwQixDQUFDO1lBQ0YsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDeEQ7UUFFRCxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsQ0FBQztRQUNsQyxZQUFZO1FBQ1osOEVBQThFO1FBQzlFLDBFQUEwRTtRQUMxRSxTQUFTO1FBQ1QsTUFBTTtRQUNOLGtDQUFrQztRQUNsQyxJQUFJLG1CQUFtQixJQUFJLG1CQUFtQixDQUFDLElBQUksRUFBRTtZQUNuRCxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7WUFDdEMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxtQkFBbUIsQ0FBQyxJQUFJLENBQUM7U0FDcEU7YUFBTTtZQUNMLElBQUksbUJBQW1CLENBQUMsY0FBYyxFQUFFO2dCQUN0QyxJQUFJLE1BQU0sR0FBRyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO2dCQUN2RCxJQUFJLFlBQVksR0FBRyxtQkFBbUIsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO2dCQUMvRCxJQUFJLEtBQUssR0FBRyxFQUFDLEtBQUssRUFBRyxJQUFJLENBQUMsS0FBSyxFQUFDLENBQUM7Z0JBQ2pDLENBQUMsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxVQUN4RCxXQUFXO29CQUVYLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTt3QkFDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDO3FCQUM3Qzt5QkFBTTt3QkFDTCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRTs0QkFDekIsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDN0Q7d0JBQ0QsSUFBSSxXQUFXLENBQUMsWUFBWSxFQUFFOzRCQUM1QixLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFlBQVksQ0FBQzt5QkFDeEM7cUJBQ0Y7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztnQkFDbEQsbUNBQW1DO2dCQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUN4RCxDQUFDLElBQUksRUFBRSxFQUFFO29CQUNQLG1DQUFtQztvQkFDbkMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUNyRCxZQUFZLENBQ0QsQ0FBQztvQkFDZCxJQUFJLFlBQVksR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO29CQUM5RCxJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztvQkFDakUsSUFBSSxDQUFDLGFBQWEsRUFBRTt3QkFDbEIsTUFBTSxRQUFRLEdBQUcsRUFBRSxDQUFDO3dCQUNwQixDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEVBQUUsVUFDakQsR0FBRzs0QkFFSCxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDbEQsQ0FBQyxDQUFDLENBQUM7d0JBQ0gsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxRQUFRLENBQUM7cUJBQ3BEO29CQUNELElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQzt3QkFDeEMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFDMUMsSUFBSSxLQUFLLElBQUksYUFBYSxDQUFDLG9CQUFvQixFQUFFO3dCQUMvQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUM7cUJBQzdEO2dCQUNILENBQUMsRUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNOLG1DQUFtQztvQkFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQzlCLENBQUMsQ0FDRixDQUFDO2FBQ0g7U0FDRjtRQUNELHdDQUF3QztRQUN4Qyx3QkFBd0I7UUFDeEIsTUFBTTtRQUNOLFVBQVUscUJBQVEsVUFBVSxFQUFLLG1CQUFtQixDQUFFLENBQUM7UUFDdkQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUMzQyxnRUFBZ0U7UUFDaEUsbUJBQW1CO1FBQ25CLFlBQVk7UUFDWixLQUFLO1FBQ0wsSUFBSSxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFFO1lBQ3RELElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLFVBQVUsQ0FBQztTQUN4RTthQUFNO1lBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxVQUFVLENBQUM7U0FDeEQ7UUFDRCxlQUFlO1FBQ2Ysd0RBQXdEO1FBQ3hELHdCQUF3QjtRQUN4QixLQUFLO0lBQ1AsQ0FBQztJQUVELG9CQUFvQixDQUFDLFdBQVcsRUFBRSxVQUFVO1FBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDN0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUM3QyxJQUFJLFVBQVUsQ0FBQyxjQUFjLElBQUksVUFBVSxDQUFDLFlBQVksRUFBRTtZQUN4RCxJQUFJLFdBQVcsRUFBRTtnQkFDZixJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7Z0JBQ25CLFNBQVMsQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEdBQUcsV0FBVyxDQUFDO2dCQUM5QyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQzdDLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEVBQ3BDLFVBQVUsSUFBSTtvQkFDWixNQUFNLFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQzlDLElBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUNsRTt3QkFDQSxPQUFPLElBQUksQ0FBQztxQkFDYjtnQkFDSCxDQUFDLENBQ0YsQ0FBQzthQUNIO2lCQUFNO2dCQUNMLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO29CQUNsQyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQzt3QkFDdEQsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQzt3QkFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzFDO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsb0JBQW9CLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxhQUFhO1FBQzlDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQ2pDLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLHNCQUFzQixDQUFDLENBQUM7UUFDbkQsSUFBSSxtQkFBbUIsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLEVBQUU7WUFDbkUsaUJBQWlCLEVBQUUsS0FBSyxHQUFHLENBQUM7U0FDN0IsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSwyQkFBMkIsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDeEUsQ0FBQztJQUNELGNBQWMsQ0FBQyxXQUFXLEVBQUUsVUFBVTtRQUNwQyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxjQUFjLENBQUMsQ0FBQztRQUN4QyxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1FBQ2pELElBQUksVUFBVSxHQUFHLFdBQVcsQ0FBQyxNQUFNLENBQUM7UUFDcEMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsU0FBUztZQUN0RSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsa0JBQWtCLENBQUMsU0FBUztZQUM5RCxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ04sT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDN0IsNkNBQTZDO1FBQzNDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7UUFDM0IsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixFQUNsRCxFQUFFLElBQUksRUFBRSxVQUFVLENBQUMsYUFBYSxFQUFFLENBQ25DLENBQUM7UUFDRixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FDakUsS0FBSyxDQUNOLENBQUM7UUFDRixJQUFJLFVBQVUsQ0FBQyxjQUFjLEVBQUU7WUFDN0IsSUFBSSxNQUFNLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDOUMsSUFBSSxZQUFZLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7WUFDdEQsSUFBSSxLQUFLLEdBQUcsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBQyxDQUFDO1lBQ2hDLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxXQUFXLEVBQUUsVUFDL0MsV0FBVztnQkFFWCxJQUFJLFdBQVcsQ0FBQyxRQUFRLEVBQUU7b0JBQ3hCLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDO2lCQUN2QztxQkFBTSxJQUFJLFdBQVcsQ0FBQyxTQUFTLEVBQUU7b0JBQ2hDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQztpQkFDN0M7cUJBQU07b0JBQ0wsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDN0Q7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILG1DQUFtQztZQUNuQyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUN4RCxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDbkMsbUNBQW1DO2dCQUNuQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQ2xELFlBQVksQ0FDRCxDQUFDO2dCQUNkLElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQzNELElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUNqRSxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUNsQixNQUFNLFFBQVEsR0FBRyxFQUFFLENBQUM7b0JBQ3BCLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsRUFBRSxVQUFVLEdBQUc7d0JBQzNELFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxRQUFRLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO29CQUNsRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLFFBQVEsQ0FBQztpQkFDakQ7Z0JBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO29CQUNyQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUN2QyxPQUFPLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQzdDLENBQUMsRUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNOLG1DQUFtQztnQkFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDOUIsQ0FBQyxDQUNGLENBQUM7U0FDSDtRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxHQUFHLFVBQVUsQ0FBQztRQUN6RSxXQUFXO1FBQ1gsOEJBQThCO1FBQzlCLGlDQUFpQztRQUNqQyxJQUFJO0lBQ04sQ0FBQztJQUNELGNBQWMsQ0FBQyxLQUFLLEVBQUUsY0FBYztRQUNsQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsbUJBQW1CLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ2xELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzdCLCtCQUErQjtRQUMvQiw0REFBNEQ7UUFDNUQscUJBQXFCO1FBQ3JCLHdEQUF3RDtRQUN4RCx3QkFBd0I7UUFDeEIseURBQXlEO1FBQ3pELHlCQUF5QjtRQUN6Qiw2Q0FBNkM7UUFDN0MsOEVBQThFO1FBQzlFLDRCQUE0QjtRQUM1QixrRUFBa0U7UUFDbEUscURBQXFEO1FBQ3JELFFBQVE7UUFDUixRQUFRO1FBQ1Isc0NBQXNDO1FBQ3RDLHlDQUF5QztRQUN6QyxrREFBa0Q7UUFDbEQsd0RBQXdEO1FBQ3hELG9EQUFvRDtRQUNwRCxJQUFJO1FBQ0osT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO1FBQ2xELElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDbEQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLElBQUksY0FBYyxJQUFJLGNBQWMsQ0FBQyxZQUFZLEVBQUU7WUFDakQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQzthQUFNLElBQUcsY0FBYyxJQUFJLGNBQWMsQ0FBQyxJQUFJLElBQUUsYUFBYSxFQUFDO1lBQzdELElBQUcsS0FBSyxDQUFDLEtBQUssSUFBRSxJQUFJLEVBQUM7Z0JBQ25CLElBQUksQ0FBQyxnQkFBZ0IsR0FBQyxLQUFLLENBQUM7YUFDN0I7aUJBQUk7Z0JBQ0gsSUFBSSxDQUFDLGdCQUFnQixHQUFDLElBQUksQ0FBQzthQUM1QjtTQUNGO2FBQ0s7WUFDSixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUsVUFBVSxVQUFVO2dCQUNuRCxJQUFJLFVBQVUsQ0FBQyxXQUFXLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQzt3QkFDcEMsVUFBVSxDQUFDLEtBQUssS0FBSyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztpQkFDbkQ7WUFDSCxDQUFDLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsQ0FBQztZQUNwQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1NBQ3RFO0lBQ0gsQ0FBQztJQUNELGFBQWEsQ0FBQyxLQUFLLElBQUcsQ0FBQztJQUN2QixZQUFZLENBQUMsS0FBSyxFQUFFLFNBQVM7UUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyw2Q0FBNkMsQ0FBQyxDQUFDO1FBQzNELElBQUksVUFBVSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FDVCx3QkFBd0IsRUFDeEIsS0FBSyxFQUNMLGFBQWEsRUFDYixTQUFTLEVBQ1QsV0FBVyxFQUNYLFVBQVUsQ0FDWCxDQUFDO1FBQ0YsSUFBSSxTQUFTLENBQUMsa0JBQWtCLElBQUksS0FBSyxFQUFFO1lBQ3pDLElBQUksS0FBSyxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUMxRCxJQUFJLEtBQUssSUFBSSxVQUFVLEVBQUU7Z0JBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7Z0JBQ2hDLElBQUksY0FBYyxHQUFHLFNBQVMsQ0FBQyxZQUFZLENBQUM7Z0JBQzVDLElBQUksS0FBSyxHQUFHLEVBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLLEVBQUMsQ0FBQztnQkFDaEMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsY0FBYyxDQUFDLENBQUM7Z0JBQzlDLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxVQUFVLFdBQVc7b0JBQ3pELElBQUksV0FBVyxDQUFDLGVBQWUsRUFBRTt3QkFDL0IsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDL0Q7eUJBQU0sSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO3dCQUNoQyxLQUFLLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7cUJBQzdDO3lCQUFNO3dCQUNMLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQzdEO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixvQ0FBb0M7Z0JBQ3BDLElBQUksQ0FBQyxjQUFjO3FCQUNoQixhQUFhLENBQUMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7cUJBQzNDLFNBQVMsQ0FDUixDQUFDLElBQUksRUFBRSxFQUFFO29CQUNQLG9DQUFvQztvQkFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBQy9CLHVCQUF1QjtvQkFDdkIscUJBQXFCO29CQUNyQiwwREFBMEQ7b0JBQzFELDBEQUEwRDtvQkFDMUQsV0FBVztvQkFDWCxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLEVBQUU7d0JBQy9ELElBQUksQ0FBQyxlQUFlOzRCQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQztnQ0FDcEQsQ0FBQyxDQUFDLElBQUk7Z0NBQ04sQ0FBQyxDQUFDLEtBQUssQ0FBQzt3QkFDWixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztxQkFDdEQ7eUJBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTt3QkFDN0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQ3pELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO3FCQUN0RDtvQkFDRCx5QkFBeUI7b0JBQ3pCLCtEQUErRDtvQkFDL0QsZUFBZTtvQkFDZix1REFBdUQ7b0JBQ3ZELGNBQWM7b0JBQ2QseUJBQXlCO29CQUN6QixLQUFLO2dCQUNQLENBQUMsRUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNOLG1DQUFtQztvQkFDbkMsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTt3QkFDckIsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7d0JBQ3hELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO3FCQUN0RDtnQkFDSCxDQUFDLENBQ0YsQ0FBQzthQUNMO2lCQUFNO2dCQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLENBQUM7Z0JBQzdCLDhCQUE4QjtnQkFDOUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7YUFDN0I7U0FDRjtJQUNILENBQUM7SUFDRCxRQUFRO1FBQ04sSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLHVCQUF1QixDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLE1BQU07WUFDbkMsSUFBSSxNQUFNLEVBQUU7Z0JBQ1YsSUFBSSxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTTtvQkFDN0QsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDO29CQUM5QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxjQUFjLENBQUM7b0JBQzVELENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQyxjQUFjO29CQUMvRCxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNULElBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxFQUFDO29CQUNyRCxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUM7aUJBQ2xEO3FCQUFLLElBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUM7b0JBQ3RDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQTtpQkFDaEQ7Z0JBQ0QsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztnQkFDNUIsZ0NBQWdDO2dCQUNoQyxrQ0FBa0M7Z0JBQ2xDLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNyQyxJQUFJLE1BQU0sR0FBRyxjQUFjLENBQUMsTUFBTSxDQUFDO2dCQUNuQyxJQUFJLFdBQVcsR0FBRztvQkFDaEIsS0FBSyxFQUFHLElBQUksQ0FBQyxLQUFLO2lCQUNuQixDQUFDO2dCQUNGLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUk7b0JBQ2xELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTt3QkFDbEIsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO3FCQUNyQzt5QkFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTt3QkFDaEMseUJBQXlCO3dCQUN6QixrREFBa0Q7d0JBQ2xELFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDOzRCQUNwQixPQUFPLENBQUMsYUFBYSxDQUFDLElBQUksT0FBTyxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUs7Z0NBQ3ZELENBQUMsQ0FBQyxLQUFLO2dDQUNQLENBQUMsQ0FBQyxJQUFJLENBQUM7cUJBQ1o7eUJBQU0sSUFBRyxJQUFJLENBQUMsYUFBYSxFQUFDO3dCQUMzQixJQUFJLGNBQWMsR0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQzt3QkFDekMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRSxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUNwRDt5QkFBSzt3QkFDSixJQUFJLFVBQVUsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzVDLE9BQU8sQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUMsVUFBVSxDQUFDLENBQUM7d0JBQzFDLElBQUcsVUFBVSxFQUFDOzRCQUNiLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU07Z0NBQ3BDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7Z0NBQ2xDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUN0Qjt3QkFFRCxJQUNFLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssU0FBUzs0QkFDcEMsSUFBSSxDQUFDLG1CQUFtQixFQUN4Qjs0QkFDQSxnRUFBZ0U7NEJBQ2hFLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQzt5QkFDdkQ7cUJBQ0Y7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO2dCQUN0RCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sS0FBSyxNQUFNLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLEVBQUU7b0JBQ2pHLElBQUksS0FBSyxHQUFHLGNBQWMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUM7b0JBQ2xFLElBQUksQ0FBQyxjQUFjO3lCQUNoQixhQUFhLENBQUMsV0FBVyxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUM7eUJBQ3pDLFNBQVMsQ0FDUixDQUFDLEdBQUcsRUFBRSxFQUFFO3dCQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUN0QixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxPQUFPLENBQzVCLENBQ0YsQ0FBQzt3QkFDRixJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFOzRCQUMxQyxZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDOzRCQUN4QyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDOzRCQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO3lCQUNyRDtvQkFDSCxDQUFDLEVBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTt3QkFDUixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQzt3QkFDdkIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO29CQUNKLENBQUMsQ0FDRixDQUFDO2lCQUNMO3FCQUFNO29CQUNMLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQzlELENBQUMsR0FBRyxFQUFFLEVBQUU7d0JBQ04sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO3dCQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7NEJBQ3JCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7NEJBQ3hDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7NEJBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7eUJBQ3JEO29CQUNILENBQUMsRUFDRCxDQUFDLEtBQUssRUFBRSxFQUFFO3dCQUNSLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO3dCQUN2QixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7b0JBQ0osQ0FBQyxDQUNGLENBQUM7aUJBQ0g7YUFDRjtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELFlBQVksQ0FBQyxLQUFLLEVBQUUsUUFBUSxFQUFFLFVBQVU7UUFDdEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUM5QyxJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQzlCLElBQUcsS0FBSyxDQUFDLE1BQU0sRUFBQztZQUNkLElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNwQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3hDLElBQUksZ0JBQWdCLEdBQUMsS0FBSyxDQUFDO1lBQzNCLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDOUMsSUFBSSxjQUFjLEdBQUcsVUFBVSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3hELGdCQUFnQixHQUFDLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQSxDQUFDLENBQUEsSUFBSSxDQUFBLENBQUMsQ0FBQSxLQUFLLENBQUM7YUFDckU7aUJBQUk7Z0JBQ0gsZ0JBQWdCLEdBQUcsQ0FBQyxPQUFPLElBQUksUUFBUSxDQUFDLENBQUEsQ0FBQyxDQUFDLElBQUksQ0FBQSxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ3ZEO1lBQ0QsSUFBRyxnQkFBZ0IsRUFBQztnQkFDbEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekIsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDM0IsTUFBTSxDQUFDLE1BQU0sR0FBRyxHQUFHLEVBQUU7b0JBQ25CLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQzt3QkFDekIsSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNO3FCQUNwQixDQUFDLENBQUM7b0JBQ0gsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksRUFBRSxDQUFDO2dCQUNyQyxDQUFDLENBQUM7YUFDSDtpQkFBSTtnQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7Z0JBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztnQkFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLGlEQUFpRDtvQkFDL0MsUUFBUTtvQkFDUixTQUFTLENBQ1osQ0FDRixDQUFDO2FBQ0g7U0FDRjthQUFJO1lBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO1NBQ25DO0lBRUgsQ0FBQztJQUNELG1CQUFtQixDQUFDLEtBQUssRUFBRSxRQUFRLEVBQUUsVUFBVTtRQUM3QyxJQUFJLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO1FBQzlCLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxnQkFBZ0IsRUFBRTtZQUM3QyxJQUFJLElBQUksR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN6QixJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO1lBQ3hDLElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzFELE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFFLGlDQUFpQyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ3hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3pCLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQzNCLE1BQU0sQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFFO29CQUNuQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztvQkFDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUM7d0JBQ3pCLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTTtxQkFDcEIsQ0FBQyxDQUFDO29CQUNILElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO29CQUNsQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztvQkFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDckMsQ0FBQyxDQUFDO2FBQ0g7aUJBQU07Z0JBQ0wsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsR0FBRyxFQUFFLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxpREFBaUQ7b0JBQy9DLFVBQVUsQ0FBQyxjQUFjO29CQUN6QixTQUFTLENBQ1osQ0FDRixDQUFDO2FBQ0g7U0FDRjthQUFNO1lBQ0wsSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ25ELElBQUksSUFBSSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQzFCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ3pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQyxDQUFDO2dCQUNwQyxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN4Qyx1Q0FBdUM7Z0JBQ3ZDLG9DQUFvQztnQkFDcEMsSUFBSSxPQUFPLElBQUksUUFBUSxFQUFFO29CQUN2QixPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUN6QixNQUFNLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUMzQixNQUFNLENBQUMsTUFBTSxHQUFHLEdBQUcsRUFBRTt3QkFDbkIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7d0JBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDOzRCQUN6QixJQUFJLEVBQUUsTUFBTSxDQUFDLE1BQU07eUJBQ3BCLENBQUMsQ0FBQzt3QkFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQzt3QkFDbEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7d0JBQ3ZCLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ3JDLENBQUMsQ0FBQztpQkFDSDtxQkFBTTtvQkFDTCxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztvQkFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLGlEQUFpRDt3QkFDL0MsUUFBUTt3QkFDUixTQUFTLENBQ1osQ0FDRixDQUFDO2lCQUNIO2FBQ0Y7U0FDRjtJQUNILENBQUM7SUFDRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFDRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDdkYsSUFBSSxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUM7UUFDbkQsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtZQUNqRCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2xCLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNyQztpQkFBTTtnQkFDTCxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNO29CQUNsQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDekMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ2hDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFO1lBQzlCLElBQUksUUFBUSxHQUFHLElBQUksUUFBUSxFQUFFLENBQUM7WUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtnQkFDbkMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDekMsQ0FBQyxDQUFDLENBQUM7WUFDSCxRQUFRLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDeEMsV0FBVyxHQUFHLFFBQVEsQ0FBQztTQUN6QjtRQUNELElBQUksbUJBQW1CLEdBQUcsYUFBYSxDQUFDLFlBQVksQ0FBQztRQUNyRCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxhQUFhLENBQUMsQ0FBQTtRQUMvQixXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQztRQUNoQyxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUc7WUFDN0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNqQyw0QkFBNEI7WUFDNUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDbEQsWUFBWTtZQUNaLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLElBQUksTUFBTSxFQUFFLEVBQUUsaUNBQWlDO2dCQUNyRyxJQUFHLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUNoQztvQkFDQSxJQUFJLENBQUMsY0FBYzt5QkFDaEIsYUFBYSxDQUNaLFdBQVcsRUFDWCxhQUFhLENBQUMsTUFBTSxFQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQztvQkFDckIsaUNBQWlDO3FCQUNsQzt5QkFDQSxTQUFTLENBQ1IsQ0FBQyxHQUFHLEVBQUUsRUFBRTt3QkFDTixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxVQUFVLENBQUMsQ0FBQzt3QkFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO3dCQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7NEJBQzFDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUU7Z0NBQzVCLElBQUksUUFBUSxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO2dDQUNqQyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsR0FBRyxFQUFFO29DQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO29DQUMxQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQ0FDNUQ7cUNBQU0sSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFVBQVUsRUFBRTtvQ0FDMUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQ0FDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBRSxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7b0NBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7aUNBQ3hEO3FDQUFLLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxPQUFPLEVBQUU7b0NBQ3RDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0NBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLEVBQUUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29DQUN2RCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lDQUNyRDs2QkFDRjtpQ0FBTTtnQ0FDTCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDOzZCQUMzQjt5QkFDRjt3QkFDRCxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNsQyxDQUFDLEVBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTt3QkFDUiw2QkFBNkI7d0JBQzdCLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQzt3QkFDRixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO3dCQUNoRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7d0JBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7d0JBQ3RELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEdBQUcsU0FBUyxDQUFDO3dCQUM5QyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO3dCQUN0QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztvQkFDckQsQ0FBQyxDQUNGLENBQUM7aUJBQ0o7cUJBQUk7b0JBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztvQkFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FDcEMsQ0FDRixDQUFDO2lCQUNGO2FBQ0Q7aUJBQU07Z0JBQ0wsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxJQUFJLEtBQUssRUFBRTtvQkFDeEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FDekYsQ0FBQyxHQUFHLEVBQUUsRUFBRTt3QkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7d0JBQ0YsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDNUIsQ0FBQyxFQUNELENBQUMsS0FBSyxFQUFFLEVBQUU7d0JBQ1IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDaEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO29CQUNKLENBQUMsQ0FDRixDQUFDO2lCQUNIO3FCQUFNLElBQUcsSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsSUFBSSxVQUFVLEVBQUM7b0JBQ2xFLElBQUcsYUFBYSxDQUFDLGVBQWUsRUFBRTt3QkFDaEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsZUFBZSxFQUFFLFVBQVUsSUFBSTs0QkFDckQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dDQUNsQixXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7NkJBQ3JDO2lDQUFNLElBQUcsSUFBSSxDQUFDLFlBQVksRUFBQztnQ0FDMUIsSUFBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBQztvQ0FDdkMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7aUNBQ3hKO3FDQUFLLElBQUcsSUFBSSxDQUFDLE1BQU0sRUFBQztvQ0FDbkIsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDeEc7cUNBQUk7b0NBQ0gsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDeEc7NkJBQ0Y7aUNBQUs7Z0NBQ0osV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTTtvQ0FDbEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7b0NBQ3pDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDaEM7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7b0JBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUMsYUFBYSxDQUFDLENBQUM7b0JBQ2hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzdDLElBQUksVUFBVSxHQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQyxDQUFBLFdBQVcsQ0FBQyxhQUFhLENBQUMsQ0FBQSxDQUFDLENBQUEsS0FBSyxDQUFDO29CQUMxRixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUMxQyxJQUFJLENBQUMsY0FBYzt5QkFDaEIsYUFBYSxDQUFDLFdBQVcsRUFBRSxhQUFhLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLENBQUM7eUJBQzdHLFNBQVMsQ0FDUixDQUFDLEdBQUcsRUFBRSxFQUFFO3dCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDO3dCQUM3QixJQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFDOzRCQUNsQyxJQUFJLENBQUMsY0FBYztpQ0FDZCxhQUFhLENBQUMsV0FBVyxFQUFFLGFBQWEsQ0FBQyxtQkFBbUI7a0NBQ3pELElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDO2tDQUNsRCxHQUFHLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUUsR0FBRyxHQUFDLFVBQVUsQ0FDN0M7aUNBQ0YsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0NBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFDLFVBQVUsQ0FBQyxDQUFBO2dDQUN6QixJQUFJLGFBQWEsR0FBRyxZQUFZLEdBQUMsVUFBVSxDQUFDO2dDQUM3QyxzQ0FBc0M7Z0NBQ3pDLGFBQWEsR0FBRyxhQUFhLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxHQUFHLENBQUMsQ0FBQztnQ0FDaEQsSUFBSSxDQUFDLGtCQUFrQixDQUNyQixJQUFJLENBQUMsSUFBSSxFQUNULGFBQWEsR0FBRyxNQUFNLEVBQUUsaUJBQWlCLENBQzFDLENBQUM7Z0NBQ0YsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQ0FDMUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQzs0QkFDcEMsQ0FBQyxDQUFDLENBQUM7eUJBQ1I7NkJBQUk7NEJBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsMkJBQTJCLENBQUMsQ0FBQzt5QkFDM0Q7b0JBQ0gsQ0FBQyxFQUNELENBQUMsS0FBSyxFQUFFLEVBQUU7b0JBQ1YsQ0FBQyxDQUNGLENBQUM7aUJBRUw7cUJBQUs7b0JBQ0osSUFBSSxNQUFNLEdBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQztvQkFDaEMsSUFBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsSUFBRSxZQUFZLEVBQUM7d0JBQzFDLHNDQUFzQzt3QkFDckMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO3dCQUNqQyxXQUFXLENBQUMsWUFBWSxDQUFDLEdBQUcsSUFBSSxDQUFDO3FCQUNsQztvQkFDRCxJQUFJLENBQUMsY0FBYzt5QkFDaEIsYUFBYSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUM7eUJBQ2xDLFNBQVMsQ0FDUixDQUFDLEdBQUcsRUFBRSxFQUFFO3dCQUNOLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLFVBQVUsQ0FBQyxDQUFDO3dCQUM3QixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7d0JBQ0YsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsTUFBTSxJQUFJLEdBQUcsRUFBRTs0QkFDMUMsNkJBQTZCOzRCQUM3QixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFO2dDQUM1QixJQUFJLFFBQVEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLElBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7Z0NBQ2pJLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxHQUFHLEVBQUU7b0NBQzVCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7b0NBQzFCLElBQUksQ0FBQyxXQUFXLENBQ2QsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQ3ZCLElBQUksQ0FBQyxVQUFVLENBQ2hCLENBQUM7aUNBQ0g7cUNBQU07b0NBQ0wsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQ0FDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lDQUM3Qzs2QkFDRjtpQ0FBTTtnQ0FDTCxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDOzZCQUMzQjt5QkFDRjs2QkFBTSxJQUFJLEdBQUcsQ0FBQyxNQUFNLElBQUksR0FBRyxFQUFFOzRCQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDOzRCQUNoRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7NEJBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7NEJBQ25ELElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEdBQUcsU0FBUyxDQUFDOzRCQUM5QyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDOzRCQUNuQyxPQUFPLENBQUMsR0FBRyxDQUNULElBQUksQ0FBQyxhQUFhLEVBQ2xCLDJDQUEyQyxDQUM1QyxDQUFDO3lCQUNIO29CQUNILENBQUMsRUFDRCxDQUFDLEtBQUssRUFBRSxFQUFFO3dCQUNSLDZCQUE2Qjt3QkFDN0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQzFCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLEtBQUssQ0FDMUIsQ0FDRixDQUFDO3dCQUNGLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUM7d0JBQ2hELElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQzt3QkFDckMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQzt3QkFDdEQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxTQUFTLENBQUM7d0JBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7d0JBQ3RDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO29CQUNyRCxDQUFDLENBQ0YsQ0FBQztpQkFDTDthQUNGO1NBQ0Y7SUFDSCxDQUFDO0lBQ0QsZ0JBQWdCLENBQUMsS0FBSztRQUNwQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQztRQUNqQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1FBQzlCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxLQUFLLENBQUM7UUFDaEMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQztRQUN0RCxJQUFJLEtBQUssSUFBSSxLQUFLLElBQUksS0FBSyxJQUFJLEtBQUssRUFBRTtZQUNwQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxlQUFlLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUMxQixZQUFZLENBQUMsVUFBVSxDQUN4QixDQUFDO2FBQ0g7WUFDRCxJQUFHLFlBQVksSUFBSSxZQUFZLENBQUMsZ0JBQWdCLEVBQUM7Z0JBQy9DLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEdBQUcsWUFBWSxDQUFDLHNCQUFzQixDQUFDO2FBQzlFO1lBQ0QsSUFBRyxZQUFZLElBQUksWUFBWSxDQUFDLGNBQWMsRUFBQztnQkFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7YUFDNUI7aUJBQUk7Z0JBQ0gsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7YUFDN0I7WUFDRCxJQUFHLFlBQVksSUFBSSxZQUFZLENBQUMsZUFBZSxFQUFFO2dCQUMvQyxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsR0FBRyxZQUFZLENBQUMsZUFBZSxDQUFDO2FBQ2hFO1lBQ0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksWUFBWSxDQUFDLGlCQUFpQixFQUFFO2dCQUNsQyxDQUFDLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxzQkFBc0IsRUFBRSxVQUFTLFVBQVU7b0JBQ2hFLElBQUksQ0FBQyxTQUFTLENBQ1osVUFBVSxDQUFDLFVBQVUsQ0FDdEIsR0FBRyxVQUFVLENBQUMsWUFBWSxDQUFDO2dCQUM5QixDQUFDLENBQUMsQ0FBQTthQUNIO1lBQ0QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1NBRWhDO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7WUFDeEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxLQUFLLENBQUM7WUFDNUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLFVBQzdELElBQUk7Z0JBRUosT0FBTyxDQUFDLFlBQVksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2pELENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBRyxZQUFZLENBQUMsc0JBQXNCLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLEdBQUcsRUFBRSxDQUFBO2FBQzVDO1lBRUQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM3QiwrQkFBK0I7WUFDL0IsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1NBQ2hDO1FBQ0QsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQ3pCLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsVUFBVSxJQUFJO2dCQUNsRCxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzNDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxJQUFHLFlBQVksQ0FBQyxnQkFBZ0IsRUFBRTtZQUNoQyxPQUFPLENBQUMsYUFBYSxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDN0MsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQzFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMvQyxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDM0MsT0FBTyxDQUFDLGVBQWUsQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQyxHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2xEO1FBRUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMzQyxDQUFDO0lBQ0QsV0FBVyxDQUFDLElBQUksRUFBRSxVQUFVO1FBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQy9CLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7WUFDM0IsUUFBUSxFQUFFLElBQUksQ0FBQyxRQUFRO1lBQ3ZCLElBQUksRUFBRSxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxZQUFZO1NBQ3pFLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxhQUFrQixFQUFFLEVBQUU7WUFDbEMsSUFBSSxhQUFhLEVBQUU7Z0JBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQzlCLElBQUksQ0FBQyxLQUFLLEVBQ1YsYUFBYSxDQUFDLFFBQVEsRUFDdEIsVUFBVSxDQUNYO3FCQUNFLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO29CQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUNqQyxJQUFJLEdBQUcsR0FBRyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO29CQUNqQyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsR0FBRyxDQUFDO29CQUN2QyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFVBQVUsQ0FBQztvQkFDdEMsSUFDRSxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVE7d0JBQ3hCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxJQUFJLE1BQU0sRUFDbEM7d0JBQ0EsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ25DO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEdBQUcsS0FBSyxDQUFDO3FCQUNwQztvQkFDRCxJQUFJLFdBQVcsR0FBRyxFQUFFLENBQUM7b0JBQ3JCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGVBQWUsQ0FBQyxDQUFDO29CQUNuQyxJQUFJLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQztvQkFDbEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsb0JBQW9CLENBQUMsQ0FBQztvQkFDakQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO29CQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsVUFBVSxJQUFJO3dCQUNqRCxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUN0RCxDQUFDLENBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO29CQUM3QyxJQUFJLENBQUMsY0FBYzt5QkFDaEIsYUFBYSxDQUNaLFdBQVcsRUFDWCxhQUFhLENBQUMsTUFBTSxFQUNwQixJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FDbkI7eUJBQ0EsU0FBUyxDQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7d0JBQ2pCLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQ3RELENBQUMsQ0FBQyxDQUFDO29CQUNMLG1FQUFtRTtvQkFDbkUsbURBQW1EO29CQUNuRCx3Q0FBd0M7b0JBQ3hDLGtDQUFrQztvQkFDbEMscUdBQXFHO29CQUNyRyw0Q0FBNEM7b0JBQzVDLE1BQU07Z0JBQ1IsQ0FBQyxDQUFDO3FCQUNELEtBQUssQ0FBQyxDQUFDLEtBQUssRUFBRSxFQUFFLEdBQUUsQ0FBQyxDQUFDLENBQUM7YUFDekI7UUFDSCxDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFvQkQsYUFBYTtRQUNYLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUU7WUFDNUIsa0JBQWtCO1lBQ2xCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQzFCO1FBQ0QsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsWUFBWSxHQUFHLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUN4QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQzVCLGtCQUFrQjtZQUNsQixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO1NBQ2hDO1FBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUMzQjtRQUNELEtBQUssSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFLEtBQUssSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUU7WUFDeEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEM7UUFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsUUFBUSxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDM0YsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLFNBQVM7WUFDNUcsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ3BCLEtBQUssSUFBSSxVQUFVLEdBQUcsQ0FBQyxFQUFFLFVBQVUsSUFBSSxDQUFDLEVBQUUsVUFBVSxFQUFFLEVBQUU7WUFDdEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDakM7UUFDRCxLQUFLLElBQUksVUFBVSxHQUFHLENBQUMsRUFBRSxVQUFVLElBQUksRUFBRSxFQUFFLFVBQVUsRUFBRSxFQUFFO1lBQ3ZELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1NBQ25DO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7UUFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDMUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNyQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUMvQixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1FBQzNCLElBQUksQ0FBQyxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFBO0lBQ0osQ0FBQztJQUNELFlBQVk7UUFDVixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN6QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBQ0QsYUFBYTtRQUNYLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3pCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO0lBQzdCLENBQUM7SUFDRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7UUFDekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7SUFDN0IsQ0FBQztJQUNELFNBQVM7UUFDUCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUMxQixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztJQUM3QixDQUFDO0lBQ0QsV0FBVztRQUNULElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDO1FBQzFCLElBQUksQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1FBQzFCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO0lBQzNCLENBQUM7SUFDRCxZQUFZLENBQUMsS0FBSztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFDLGdCQUFnQixDQUFDLENBQUE7UUFDekMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLGVBQWUsR0FBRyxXQUFXLENBQUM7UUFDM0QsMkRBQTJEO0lBQzdELENBQUM7SUFVRCxpQkFBaUIsQ0FBQyxDQUFDO1FBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFBO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxHQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUUsVUFBVSxDQUFDO1FBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFDRCxvQkFBb0IsQ0FBQyxDQUFDO1FBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1FBQ3ZCLHVCQUF1QjtRQUN2Qix1QkFBdUI7UUFDdkIsSUFBSSxDQUFDLHVCQUF1QixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDdkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBQ0QsVUFBVSxDQUFDLENBQUM7UUFDVixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtRQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLGNBQWMsR0FBRyxNQUFNLEdBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRSxRQUFRLENBQUM7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDbkMsQ0FBQztJQUVELFVBQVUsQ0FBQyxDQUFDO1FBQ1YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUE7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQztRQUN0QyxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUE7UUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRSxRQUFRLEdBQUUsQ0FBQyxDQUFDLEtBQUssR0FBSSxLQUFLLEdBQUMsTUFBTSxHQUFDLElBQUksQ0FBQztJQUM5RCxDQUFDO0lBRUQsVUFBVSxDQUFDLENBQUM7UUFDVixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztRQUNwQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztRQUMzQixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQTtRQUN0QixJQUFJLENBQUMsYUFBYSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDN0IsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFBO1FBQ2xELElBQUksQ0FBQyxjQUFjLEdBQUUsUUFBUSxHQUFFLElBQUksQ0FBQyxtQkFBbUIsR0FBSSxLQUFLLEdBQUMsTUFBTSxHQUFDLElBQUksQ0FBQztJQUUvRSxDQUFDO0lBRUQsUUFBUSxDQUFDLENBQUM7UUFDUixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztRQUNwQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztRQUNyQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQztRQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQTtRQUNyQixJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDeEIsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFBO1FBQy9DLElBQUksQ0FBQyxjQUFjLEdBQUUsUUFBUSxHQUFFLE9BQU8sR0FBRSxHQUFHLEdBQUUsSUFBSSxDQUFDLGFBQWEsR0FBSSxJQUFJLENBQUM7SUFDMUUsQ0FBQztJQUVELFVBQVUsQ0FBQyxDQUFDO1FBQ1YsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7UUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzdCLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQTtRQUMvQyxJQUFJLENBQUMsY0FBYyxHQUFFLFFBQVEsR0FBRSxPQUFPLEdBQUUsR0FBRyxHQUFFLENBQUMsQ0FBQyxLQUFLLEdBQUksSUFBSSxDQUFDO0lBQy9ELENBQUM7SUFFRCxtQkFBbUIsQ0FBQyxLQUFLLElBQUcsQ0FBQztJQUM3QixnQkFBZ0IsQ0FBQyxLQUFLLElBQUcsQ0FBQzs7O1lBdmpHM0IsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2dCQUN2Qix5NGdGQUEyQzs7YUFFNUM7Ozs7WUE3QlEsNEJBQTRCO1lBVzVCLGNBQWM7WUFQckIsV0FBVztZQVVKLGNBQWM7WUF4QnJCLGlCQUFpQjtZQTZCVixZQUFZO1lBcENaLGtCQUFrQjtZQU96QixpQkFBaUI7WUFSVixlQUFlO1lBdUNmLGFBQWE7NENBOEdqQixNQUFNLFNBQUMsU0FBUzs7O3lCQXBHbEIsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7eUJBQ0wsS0FBSzs2QkFDTCxLQUFLO29CQUNMLEtBQUs7bUJBQ0wsS0FBSzswQkFDTCxLQUFLOzBCQXFDTCxTQUFTLFNBQUMsYUFBYTt3QkFDdkIsU0FBUyxTQUFDLFdBQVc7OEJBQ3JCLFNBQVMsU0FBQyxNQUFNOztBQXNnR25CLE1BQU0sT0FBTyxXQUFXO0lBSXRCLFlBQVksT0FBTztRQUNqQixJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQyxXQUFXLElBQUksRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQztDQUNGIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSBcIi4vLi4vc2hhcmVkL3NuYWNrYmFyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQXdzUzNVcGxvYWRTZXJ2aWNlIH0gZnJvbSBcIi4vLi4vc2hhcmVkL2F3cy1zMy11cGxvYWQuc2VydmljZVwiO1xyXG5pbXBvcnQge1xyXG4gIENvbXBvbmVudCxcclxuICBJbmplY3QsXHJcbiAgVmlld0NoaWxkLFxyXG4gIEVsZW1lbnRSZWYsXHJcbiAgSW5wdXQsXHJcbiAgQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgQWZ0ZXJDb250ZW50SW5pdCxcclxuICBPbkluaXQsXHJcbn0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHtcclxuICBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50LFxyXG4gIE1hdEF1dG9jb21wbGV0ZSxcclxufSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvYXV0b2NvbXBsZXRlXCI7XHJcbmltcG9ydCB7IE1hdENoaXBJbnB1dEV2ZW50IH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2NoaXBzXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlXCI7XHJcbi8vIGltcG9ydCB7IGxvY2FsZSBhcyBlbmdsaXNoIH0gZnJvbSBcIi4uL2kxOG4vZW5cIjtcclxuXHJcbmltcG9ydCB7XHJcbiAgRm9ybUJ1aWxkZXIsXHJcbiAgRm9ybUNvbnRyb2wsXHJcbiAgRm9ybUdyb3VwLFxyXG4gIFZhbGlkYXRvcnMsXHJcbiAgRm9ybUFycmF5LFxyXG59IGZyb20gXCJAYW5ndWxhci9mb3Jtc1wiO1xyXG5cclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tIFwiLi4vY29udGVudC9jb250ZW50LnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgTW9kZWxMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kZWwtbGF5b3V0L21vZGVsLWxheW91dC5jb21wb25lbnRcIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL19zZXJ2aWNlcy9pbmRleFwiO1xyXG5cclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tIFwicnhqcy9TdWJqZWN0XCI7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYgfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvZGlhbG9nXCI7XHJcbmltcG9ydCAqIGFzIG1vbWVudF8gZnJvbSBcIm1vbWVudFwiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL2xvYWRlci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IE1hdERhdGVwaWNrZXJJbnB1dEV2ZW50IH0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsL2RhdGVwaWNrZXJcIjtcclxuY29uc3QgbW9tZW50ID0gbW9tZW50XztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcImZvcm0tbGF5b3V0XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9mb3JtLWxheW91dC5jb21wb25lbnQuaHRtbFwiLFxyXG4gIHN0eWxlVXJsczogW1wiLi9mb3JtLWxheW91dC5jb21wb25lbnQuc2Nzc1wiXSxcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1MYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gIEBJbnB1dCgpIGZvcm1WYWx1ZXM6IGFueTtcclxuICBASW5wdXQoKSBzdGVwcGVyVmFsOiBhbnk7XHJcbiAgQElucHV0KCkgaW1wb3J0RGF0YTogYW55O1xyXG4gIEBJbnB1dCgpIG9uTG9hZERhdGE6IGFueTtcclxuICBASW5wdXQoKSBjaGlwTGlzdEZpZWxkczogYW55O1xyXG4gIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XHJcbiAgQElucHV0KCkgZGF0YTogYW55O1xyXG4gIEBJbnB1dCgpIGZyb21Sb3V0aW5nOiBhbnk7XHJcbiAgaW1wb3J0Q29udHJvbDogSW1wb3J0VmFsdWU7XHJcbiAgY3VycmVudFRhYmxlTG9hZDogYW55O1xyXG4gIGVuYWJsZVRleHRGaWVsZHM6IGJvb2xlYW47XHJcbiAgZW5hYmxlU2VsZWN0RmllbGRzOiBib29sZWFuO1xyXG4gIGVuYWJsZUNoaXBGaWVsZHM6IGJvb2xlYW47XHJcbiAgZW5hYmxlTXVsdGlzZWxlY3RGaWVsZHM6IGJvb2xlYW47XHJcbiAgZW5hYmxlVGFiczogYm9vbGVhbjtcclxuICBlbmFibGVTZWxlY3RPcHRpb24gPSB0cnVlO1xyXG4gIHF1ZXJ5UGFyYW1zOiBhbnk7XHJcbiAgY29udGVudERhdGE6IGFueTtcclxuICBjb2x1bW5zOiBhbnk7XHJcbiAgZGlzcGxheWVkQ29sdW1uczogYW55O1xyXG4gIHRvdGFsOiBhbnk7XHJcbiAgZW5hYmxlVGFibGVMYXlvdXQ6IGJvb2xlYW47XHJcbiAgdmlld0RhdGE6IGFueTtcclxuICBzdWJtaXR0ZWQ6IGJvb2xlYW47XHJcbiAgaW5wdXRHcm91cDogRm9ybUdyb3VwO1xyXG4gIGlucHV0RGF0YTogYW55O1xyXG4gIGN1cnJlbnRDb25maWdEYXRhOiBhbnk7XHJcbiAgc2VsZWN0ZWRGaWxlTmFtZTogYW55O1xyXG4gIGZpbGVVcGxvYWQ6IGFueTtcclxuICB0YWJsZUxpc3Q6IGFueTtcclxuICBjaGlwVmFsaWRpdGFpb246IGJvb2xlYW47XHJcbiAgZW5hYmxlTmV3VGFibGVMYXlvdXQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlQ2xpY2sgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xyXG4gIGJ1dHRvbnNDb25maWdEYXRhOiBhbnkgPSB7fTtcclxuICBlbmFibGVTY2hlZHVsZSA6IGJvb2xlYW4gPSBmYWxzZTtcclxuICByZW1vdmFibGU6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIHNlbGVjdGVkQ2hpcDogYW55ID0ge307XHJcbiAgc3RlcHBlckRhdGE6IGFueTtcclxuICBmb3JtRGF0YTogYW55O1xyXG4gIHJ1blJlcG9ydDogYW55O1xyXG4gIHNob3dFbmREYXRlO1xyXG4gIHBhbmVsT3BlblN0YXRlOmJvb2xlYW4gPSB0cnVlO1xyXG4gIGVuYWJsZUltcG9ydERhdGE6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBAVmlld0NoaWxkKFwiYWNjZXNzSW5wdXRcIikgYWNjZXNzSW5wdXQ6IEVsZW1lbnRSZWY7XHJcbiAgQFZpZXdDaGlsZChcImNoaXBJbnB1dFwiKSBjaGlwSW5wdXQ6IEVsZW1lbnRSZWY8SFRNTElucHV0RWxlbWVudD47XHJcbiAgQFZpZXdDaGlsZChcImF1dG9cIikgbWF0QXV0b2NvbXBsZXRlOiBNYXRBdXRvY29tcGxldGU7XHJcblxyXG4gIHNlbGVjdGVkRGF0YUZpZWxkOiBhbnk7XHJcbiAgZGF0YUlkOiBzdHJpbmc7XHJcbiAgYWNjZXNzQ29udHJvbElkOiBhbnk7XHJcbiAgZXJyb3JUYWJsZUxpc3Q6IGFueTtcclxuICBlcnJvckxvZ1RhYmxlOiBhbnk7XHJcbiAgZXJyb3JTaG93OiBhbnkgPSB7fTtcclxuICBzZWxlY3RlZFRhYmxlVmlldzogYW55O1xyXG4gIHNlbGVjdFRhYmxlTG9hZDogYW55O1xyXG4gIGVuYWJsZVNlbGVjdFZhbHVlVmlldzogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGRlZmF1bHREYXRhc291cmNlOiBhbnk7XHJcbiAgcmVhbG06IGFueTtcclxuICBlbmFibGVPcHRpb25GaWVsZHM6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZU5hbWU6IGFueTtcclxuICBzZWxlY3RlZENoaXBzOiBhbnkgPSB7fTtcclxuICBzZWxlY3RlZENoaXBLZXlMaXN0OiBhbnkgPSB7fTtcclxuICBlbmFibGVTZWxlY3RhYmxlID0gdHJ1ZTtcclxuICBlbmFibGVSZW1vdmFibGUgPSB0cnVlO1xyXG4gIGZpbGVUeXBlOiBhbnk7XHJcbiAgaW1hZ2U6IGFueSA9IHt9O1xyXG4gIHNjaGVkdWxlRmllbGQ6IGFueSA9IHt9O1xyXG4gIGVuYWJsZVJlcGVhdERhdGE6IGJvb2xlYW4gPSBmYWxzZTtcclxuICB1c2VyRGF0YTogYW55O1xyXG4gIHJlY2lwaWVudDogYW55ID0gW107XHJcbiAgcHVibGljIGlzTmFtZUF2YWlsYWJsZTogYm9vbGVhbiA9IHRydWU7XHJcbiAgcHVibGljIGNoYXJsaW1pdGNoZWNrOiBib29sZWFuID0gdHJ1ZTtcclxuICBjdXJyZW50RGF0YTogYW55O1xyXG4gIGNoaXBMZW5ndGggPSAwO1xyXG4gIHNlbGVjdGVkY2hpcExlbmd0aCA9IDA7XHJcbiAgQ2hpcExpbWl0OiBhbnk7XHJcbiAgQ2hpcE9wZXJhdG9yOiBhbnk7XHJcbiAgZGF0ZUZpZWxkczogYW55ID0ge307XHJcbiAgbWluRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgbWluRW5kRGF0ZSA9IG5ldyBEYXRlKCk7XHJcbiAgZW5hYmxlZGF0ZUZpZWxkczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIHB1YmxpYyBkYXRlRm9ybWF0Y2hlY2s6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIGNsYXNzRGl2OiBhbnk7XHJcbiAgY2hpcExpc3RPcGVuVmlldzogYm9vbGVhbjtcclxuICBpbml0aWFsSGVpZ2h0OiBhbnk7XHJcbiAgZ2V0UnVsZXNXaWR0aDogYW55O1xyXG4gIGhpZGRlbkZpeGVkTGF5b3V0OiBhbnk7XHJcbiAgZHluYW1pY0Zvcm1IZWlnaHQ6IGFueTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgZm9ybUJ1aWxkZXI6IEZvcm1CdWlsZGVyLFxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNoYW5nZURldGVjdG9yOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuICAgIHB1YmxpYyBtYXREaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxNb2RlbExheW91dENvbXBvbmVudD4sXHJcbiAgICBwcml2YXRlIEF3c1MzVXBsb2FkU2VydmljZTogQXdzUzNVcGxvYWRTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSByZWY6IENoYW5nZURldGVjdG9yUmVmLFxyXG4gICAgcHJpdmF0ZSBzbmFja0JhclNlcnZpY2U6IFNuYWNrQmFyU2VydmljZSxcclxuICAgIHByaXZhdGUgbG9hZGVyU2VydmljZTogTG9hZGVyU2VydmljZSxcclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHByaXZhdGUgZW5nbGlzaFxyXG4gICkge1xyXG4gICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gICAgdGhpcy5kZWZhdWx0RGF0YXNvdXJjZSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiZGF0YXNvdXJjZVwiKTtcclxuICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJkYXRhc291cmNlTmFtZVwiKTtcclxuICAgIHRoaXMucmVhbG09bG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJyZWFsbVwiKTtcclxuICAgIHRoaXMucXVlcnlQYXJhbXMgPSB7XHJcbiAgICAgIG9mZnNldDogMCxcclxuICAgICAgbGltaXQ6IDEwLFxyXG4gICAgICBkYXRhc291cmNlOiB0aGlzLmRlZmF1bHREYXRhc291cmNlLFxyXG4gICAgICByZWFsbTogdGhpcy5yZWFsbVxyXG4gICAgfTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2UuY2xpY2tFdmVudE1lc3NhZ2VcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmVDbGljaykpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmZvcm1WYWx1ZXMsIFwiPj4+Pj4+Rk9STSBWQUxVRVNTU1wiKTtcclxuICAgICAgICBsZXQgdGVtcERhdGEgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyh0ZW1wRGF0YSwgXCI+Pj4+dGVtcERhdGFcIik7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KEpTT04ucGFyc2UodGVtcERhdGEpKVxyXG4gICAgICAgICAgPyBKU09OLnBhcnNlKHRlbXBEYXRhKVxyXG4gICAgICAgICAgOiB7fTsgXHJcbiAgICAgICAgdGhpcy5zaG93RGF0YSh0aGlzLmlucHV0RGF0YSk7XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5lbmFibGVTZWxlY3RWYWx1ZVZpZXcgPSBmYWxzZTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2VcclxuICAgICAgLmdldE1lc3NhZ2UoKVxyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZSkpXHJcbiAgICAgIC5zdWJzY3JpYmUoKG1lc3NhZ2UpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhtZXNzYWdlLCBcIi4uLi5NRVNTQUdFXCIpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgLy8gaWYodGhpcy5jdXJyZW50Q29uZmlnRGF0YSAmJiB0aGlzLmN1cnJlbnRDb25maWdEYXRhLmxpc3RWaWV3ICYmICB0aGlzLmN1cnJlbnRDb25maWdEYXRhLmxpc3RWaWV3LmVuYWJsZVRhYmxlTGF5b3V0KXtcclxuICAgICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5lbmFibGVOZXdUYWJsZUxheW91dCA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICAvLyB9XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5pbXBvcnRDb250cm9sID0gbmV3IEltcG9ydFZhbHVlKHt9KTtcclxuXHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLm1vZGVsQ2xvc2VNZXNzYWdlIC8vIHdoYXRpZiAtIG5ld1xyXG4gICAgICAucGlwZSh0YWtlVW50aWwodGhpcy51bnN1YnNjcmliZSkpXHJcbiAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICBjb25zb2xlLmxvZyhkYXRhLCBcIj4+Pj5kYXRhIFJlZnJlc2ggZnJvbSBmcm9tZGF0YSB0byB0YWJsZWRhdGFcIik7XHJcbiAgICAgICAgdGhpcy5kYXRhID0gZGF0YTtcclxuICAgICAgICBpZiAodGhpcy5kYXRhKSB7XHJcbiAgICAgICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuICBuZ09uSW5pdCgpIHtcclxuICAgIHRoaXMuZ2V0UnVsZXNXaWR0aCA9IG51bGw7XHJcbiAgICB0aGlzLmhpZGRlbkZpeGVkTGF5b3V0ID0gbnVsbDtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZm9ybVZhbHVlcywgXCIuLi4uLmZvcm1WYWx1ZXNcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmltcG9ydERhdGEsIFwiaW1wb3J0RGF0YT8/Pz9cIik7XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2hpcHMgPSB7fTtcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGlwS2V5TGlzdCA9IHt9O1xyXG4gICAgLy9nZXQgdmFsdWVzIGZvciBjcm9uIGV4cHJlc3Npb24gZHJvcGRvd25cclxuICAgIHRoaXMudmlld09mc2Vjb25kcygpO1xyXG5cclxuICAgIGxldCBpbml0aWFsT2JqID0ge1xyXG4gICAgICBkYXRhc291cmNlSWQ6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2UsXHJcbiAgICAgIGRhdGFzb3VyY2VOYW1lOiB0aGlzLmRlZmF1bHREYXRhc291cmNlTmFtZSxcclxuICAgICAgbGltaXQ6IDEwLFxyXG4gICAgICBvZmZzZXQ6IDAsXHJcbiAgICB9O1xyXG4gICAgdGhpcy51c2VyRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50TG9naW5Vc2VyXCIpKTtcclxuICAgIGlmICh0aGlzLnVzZXJEYXRhKSB7XHJcbiAgICAgIHRoaXMucmVjaXBpZW50LnB1c2godGhpcy51c2VyRGF0YS5lbWFpbCk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmJ1dHRvbnNDb25maWdEYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJidXR0b25Db25maWdcIilcclxuICAgICAgPyBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiYnV0dG9uQ29uZmlnXCIpKVxyXG4gICAgICA6IHt9O1xyXG4gICAgdGhpcy5mb3JtRGF0YSA9IHRoaXMuZGF0YTtcclxuICAgIGxldCB0ZW1wRGF0YSA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXBEYXRhKVxyXG4gICAgICA/IEpTT04ucGFyc2UodGVtcERhdGEpXHJcbiAgICAgIDoge1xyXG4gICAgICAgICAgZGF0YXNvdXJjZUlkOiB0aGlzLmRlZmF1bHREYXRhc291cmNlLFxyXG4gICAgICAgICAgZGF0YXNvdXJjZU5hbWU6IHRoaXMuZGVmYXVsdERhdGFzb3VyY2VOYW1lLFxyXG4gICAgICAgICAgbGltaXQ6IDEwLFxyXG4gICAgICAgICAgb2Zmc2V0OiAwLFxyXG4gICAgICAgIH07XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLmluaXRpYWxPYmogfTtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgKTtcclxuICAgIHRoaXMuY3VycmVudERhdGEgPSB0aGlzLmZyb21Sb3V0aW5nID8gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVsncGFnZVJvdXRpbmdWaWV3J10gOiB0aGlzLmN1cnJlbnRDb25maWdEYXRhW1wibGlzdFZpZXdcIl07XHJcbiAgICB0aGlzLnN0ZXBwZXJEYXRhID0gdGhpcy5jdXJyZW50RGF0YS5jcmVhdGVNb2RlbERhdGE7XHJcbiAgICB0aGlzLmNsYXNzRGl2ID1cclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSAmJiB0aGlzLmN1cnJlbnREYXRhLmJlZm9yZUZyb21EaXZFbGVtZW50c3R5bGVcclxuICAgICAgICA/IHRoaXMuY3VycmVudERhdGEuYmVmb3JlRnJvbURpdkVsZW1lbnRzdHlsZVxyXG4gICAgICAgIDogbnVsbDtcclxuICAgICAgICAvLyBkZWJ1Z2dlcjtcclxuICAgIHRoaXMuaW5pdGlhbEhlaWdodCA9XHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEgJiYgdGhpcy5jdXJyZW50RGF0YS5oZWlnaHRcclxuICAgICAgICA/IHRoaXMuY3VycmVudERhdGEuaGVpZ2h0XHJcbiAgICAgICAgOiBudWxsO1xyXG4gICAgdGhpcy5nZXRSdWxlc1dpZHRoID1cclxuICAgICAgdGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy53aWR0aFJlZHVjZVxyXG4gICAgICAgID8gdGhpcy5mb3JtVmFsdWVzLndpZHRoUmVkdWNlXHJcbiAgICAgICAgOiBudWxsO1xyXG4gICAgdGhpcy5oaWRkZW5GaXhlZExheW91dCA9XHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuaGlkZGVuRml4ZWRMYXlvdXRcclxuICAgICAgICA/IHRoaXMuZm9ybVZhbHVlcy5oaWRkZW5GaXhlZExheW91dFxyXG4gICAgICAgIDogbnVsbDtcclxuICAgICAgICBcclxuICAgIHRoaXMuZHluYW1pY0Zvcm1IZWlnaHQgPVxyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuZHluYW1pY0Zvcm1IZWlnaHRcclxuICAgICAgICAgID8gdGhpcy5mb3JtVmFsdWVzLmR5bmFtaWNGb3JtSGVpZ2h0XHJcbiAgICAgICAgICA6IG51bGw7XHJcbiAgICAvLyB0aGlzLmhpZGRlbkZpeGVkTGF5b3V0ID0gdGhpcy5jdXJyZW50RGF0YS5oaWRkZW5GaXhlZExheW91dDtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY2xhc3NEaXYpO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcykge1xyXG4gICAgICB0aGlzLmVuYWJsZVRleHRGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMudGV4dEZpZWxkcyA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgdGhpcy5lbmFibGVTZWxlY3RGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMuc2VsZWN0RmllbGRzID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICB0aGlzLmVuYWJsZU9wdGlvbkZpZWxkcyA9IHRoaXMuZm9ybVZhbHVlcy5jaG9vc2VPbmVGaWVsZHMgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgIC8vIHRoaXMuZW5hYmxlQ2hpcEZpZWxkcyA9ICh0aGlzLmNoaXBMaXN0RmllbGRzICYmIHRoaXMuY2hpcExpc3RGaWVsZHMubGVuZ3RoKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIFwidGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHNcIik7XHJcbiAgICAgIHRoaXMuZW5hYmxlQ2hpcEZpZWxkcyA9IHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICB0aGlzLmVuYWJsZVRhYnMgPSB0aGlzLmZvcm1WYWx1ZXMudGFiRGF0YSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgdGhpcy5lbmFibGVNdWx0aXNlbGVjdEZpZWxkcyA9IHRoaXMuZm9ybVZhbHVlcy5tdWx0aVNlbGVjdEZpZWxkc1xyXG4gICAgICAgID8gdHJ1ZVxyXG4gICAgICAgIDogZmFsc2U7XHJcbiAgICAgIHRoaXMuZW5hYmxlZGF0ZUZpZWxkcyA9IHRoaXMuZm9ybVZhbHVlcy5kYXRlRmllbGRzID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSBmYWxzZTtcclxuICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSBmYWxzZTtcclxuICAgIHRoaXMuc3VibWl0dGVkID0gZmFsc2U7XHJcbiAgICB0aGlzLnZpZXdEYXRhID0ge307XHJcbiAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgaWYgKHRoaXMuaW1wb3J0RGF0YSkge1xyXG4gICAgICB0aGlzLmVuYWJsZUltcG9ydERhdGEgPSB0cnVlO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZW5hYmxlVGV4dEZpZWxkcykge1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgaWYgKGl0ZW0uaW5wdXRUeXBlID09IFwiZW1haWxcIikge1xyXG4gICAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIsIFZhbGlkYXRvcnMuZW1haWwpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0ZW1wT2JqW2l0ZW0ubmFtZV0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmVuYWJsZWRhdGVGaWVsZHMpIHtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmRhdGVGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKG5ldyBEYXRlKCkpO1xyXG4gICAgICB9KTtcclxuICAgICAgLy8gdGhpcy5kYXRlRmllbGRzID0gdGhpcy5mb3JtVmFsdWVzLmRhdGVGaWVsZHM7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKHtcclxuICAgICAgICAgIHZhbHVlOiBcIlwiLFxyXG4gICAgICAgICAgZGlzYWJsZWQ6IGl0ZW0uZGlzYWJsZSxcclxuICAgICAgICB9KTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5lbmFibGVTZWxlY3RGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5zZWxlY3RGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coaXRlbSk7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmVuYWJsZU11bHRpc2VsZWN0RmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMubXVsdGlTZWxlY3RGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmVuYWJsZU9wdGlvbkZpZWxkcykge1xyXG4gICAgICAvLyB0aGlzLmZvcm1WYWx1ZXMuY2hvb3NlT25lRmllbGRzLmZvckVhY2goaXRlbSA9PiB7XHJcbiAgICAgIC8vICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICAvLyAgIHRoaXMuaW5wdXREYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmZpZWxkc1swXS52YWx1ZTtcclxuICAgICAgLy8gICBjb25zb2xlLmxvZyh0aGlzLmlucHV0RGF0YSwgXCIvLy8vLy8vXCIpO1xyXG4gICAgICAvLyAgIGNvbnNvbGUubG9nKGl0ZW0sIFwiLy8vLy8vL2l0ZW1cIik7XHJcbiAgICAgIC8vIH0pO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuY2hvb3NlT25lRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5maWVsZHNbMF0udmFsdWU7XHJcblxyXG4gICAgICAgIGNvbnNvbGUubG9nKHNlbGYuQ2hpcExpbWl0LCBcImNoaXBMaW1pdD4+PlwiKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxmLmlucHV0RGF0YSwgXCIvLy8vLy8vXCIpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGl0ZW0sIFwiLy8vLy8vL2l0ZW1cIik7XHJcbiAgICAgIH0pO1xyXG4gICAgICBpZiAodGhpcy5mb3JtVmFsdWVzLmlzU2V0RGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuY2hvb3NlT25lRmllbGRzWzBdLmRlZmF1bHRLZXlcclxuICAgICAgICBdID0gdGhpcy5mb3JtVmFsdWVzLmNob29zZU9uZUZpZWxkc1swXS5kZWZhdWx0VmFsdWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZW5hYmxlQ2hpcEZpZWxkcywgXCJlbmFibGVDaGlwRmllbGRzXCIpO1xyXG4gICAgaWYgKHRoaXMuZW5hYmxlQ2hpcEZpZWxkcykge1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmVuYWJsZUNoaXBGaWVsZHMsIFwiZW5hYmxlQ2hpcEZpZWxkc1wiKTtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coaXRlbSwgXCJzZWxmY2hpcGxpbWl0XCIpO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGl0ZW0uY2hpcENvbmRpdGlvbiwgXCJkYXRhPj4+Pj4+Pj4+Pj5cIik7XHJcbiAgICAgICAgdGVtcE9ialtpdGVtLm5hbWVdID0gbmV3IEZvcm1Db250cm9sKFtcIlwiXSk7XHJcbiAgICAgICAgaWYgKGl0ZW0uY2hpcENvbmRpdGlvbikge1xyXG4gICAgICAgICAgc2VsZi5zZWxlY3RlZENoaXBzW2l0ZW0ubmFtZV0gPSBbXTtcclxuICAgICAgICAgIGxldCB0ZW1wID0gaXRlbS5jaGlwQ29uZGl0aW9uLnZhcmlhYmxlO1xyXG4gICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICBpdGVtLmNoaXBDb25kaXRpb24uZGF0YSAmJlxyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YSAmJlxyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YVt0ZW1wXSAmJlxyXG4gICAgICAgICAgICBpdGVtLmNoaXBDb25kaXRpb24uZGF0YVtzZWxmLmlucHV0RGF0YVt0ZW1wXV1cclxuICAgICAgICAgICkge1xyXG4gICAgICAgICAgICBzZWxmLkNoaXBMaW1pdCA9XHJcbiAgICAgICAgICAgICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dW1wibGltaXRcIl07XHJcbiAgICAgICAgICAgIHNlbGYuQ2hpcE9wZXJhdG9yID1cclxuICAgICAgICAgICAgICBpdGVtLmNoaXBDb25kaXRpb24uZGF0YVtzZWxmLmlucHV0RGF0YVt0ZW1wXV1bXCJvcGVyYXRvclwiXTtcclxuICAgICAgICAgICAgc2VsZi5DaGlwTGltaXQgPVxyXG4gICAgICAgICAgICAgIGl0ZW0uY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcImxpbWl0XCJdO1xyXG4gICAgICAgICAgICBzZWxmLkNoaXBPcGVyYXRvciA9XHJcbiAgICAgICAgICAgICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dW1wib3BlcmF0b3JcIl07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgICAgLy8gY2hpcGZpZWxkcyBPbkxvYWQgZnVuY3Rpb24gYWRkZWQgd2hlbiBpbXBsZW1lbnQgcnVuIHJlcG9ydHNcclxuICAgICAgICAgIGxldCBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICAgIGxldCByZXNwb25zZU5hbWUgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICAgICAgbGV0IHJlcXVlc3REYXRhID0gaXRlbS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YTtcclxuICAgICAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTogc2VsZi5yZWFsbX07XHJcbiAgICAgICAgICAvL2NvbnNvbGUubG9nKHNlbGYuaW5wdXREYXRhLCBcIj4+Pj4+Pj5cIilcclxuICAgICAgICAgIC8vZGVidWdnZXI7XHJcbiAgICAgICAgICBfLmZvckVhY2gocmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGl0ZW0uZGF0YSA9IGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgICAgbGV0IHZhbGlkYXRlRGF0YSA9IGl0ZW0uZGF0YVswXTtcclxuICAgICAgICAgICAgbGV0IGlzQXJyYXlPZkpTT04gPSBfLmlzUGxhaW5PYmplY3QodmFsaWRhdGVEYXRhKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKCFpc0FycmF5T2ZKU09OKSB7XHJcbiAgICAgICAgICAgICAgY29uc3QgdGVtcExpc3QgPSBbXTtcclxuICAgICAgICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbi5zdWJSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgaXRlbS5kYXRhID0gaXRlbS5kYXRhW2l0ZW0ub25Mb2FkRnVuY3Rpb24uc3ViUmVzcG9uc2VdO1xyXG4gICAgICAgICAgICAgICAgbGV0IGtleXMgPSBfLmtleXMoaXRlbS5kYXRhKTtcclxuICAgICAgICAgICAgICAgIF8uZm9yRWFjaChrZXlzLCBmdW5jdGlvbiAoaXRlbUtleSkge1xyXG4gICAgICAgICAgICAgICAgICB0ZW1wTGlzdC5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBpdGVtLmRhdGFbaXRlbUtleV1bcmVxdWVzdERhdGFbXCJrZXlcIl1dLFxyXG4gICAgICAgICAgICAgICAgICAgIF9pZDogaXRlbUtleSxcclxuICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgXy5mb3JFYWNoKGl0ZW0uZGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7IG5hbWU6IGl0ZW0sIF9pZDogaXRlbSB9KTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBpdGVtLmRhdGEgPSB0ZW1wTGlzdDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHNlbGYuQ2hpcExpbWl0W2l0ZW0ubmFtZV0gPVxyXG4gICAgICAgIC8vICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGFbXHJcbiAgICAgICAgLy8gICAgIHNlbGYuaW5wdXREYXRhW11cclxuICAgICAgICAvLyAgIF07XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2codGhpcy5zZWxlY3RlZENoaXBzLCBcIj4+Pj4+Pj4+Pj4gc2VsZWN0ZWRjaGlwc1wiKTtcclxuICAgIC8vIHRoaXMuc2VsZWN0ZWRDaGlwcyA9IHRoaXMuc2VsZWN0ZWRDaGlwcztcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLnNjaGVkdWxlRmllbGRzKSB7XHJcbiAgICAgIGxldCBpdGVtID0gdGhpcy5mb3JtVmFsdWVzLnNjaGVkdWxlRmllbGRzO1xyXG4gICAgICBPYmplY3Qua2V5cyhpdGVtKS5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgIHRlbXBPYmpbdmFsdWVdID0gbmV3IEZvcm1Db250cm9sKFtcIlwiXSk7XHJcbiAgICAgIH0pO1xyXG4gICAgICAvLyBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLnNjaGVkdWxlRmllbGRzLCBmdW5jdGlvbihpdGVtKSB7XHJcbiAgICAgIC8vIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChbJyddKTtcclxuICAgICAgLy8gdGVtcE9ialtpdGVtLnRpbWVdID0gbmV3IEZvcm1Db250cm9sKFsnJ10pO1xyXG4gICAgICAvLyB0ZW1wT2JqW2l0ZW0ubWVyaWRpYW5dID0gbmV3IEZvcm1Db250cm9sKFsnJ10pO1xyXG4gICAgICAvLyB0ZW1wT2JqW2l0ZW0ucmVwZWF0VHlwZV0gPSBuZXcgRm9ybUNvbnRyb2woWycnXSk7XHJcbiAgICAgIC8vIHRlbXBPYmpbaXRlbS5lbmREYXRlXSA9IG5ldyBGb3JtQ29udHJvbChbJyddKTtcclxuICAgICAgLy8gdGVtcE9ialtpdGVtLmVuZERhdGVUaW1lXSA9IG5ldyBGb3JtQ29udHJvbChbJyddKVxyXG4gICAgICAvLyB9KVxyXG4gICAgICB0aGlzLnNjaGVkdWxlRmllbGQgPSB0aGlzLmZvcm1WYWx1ZXMuc2NoZWR1bGVGaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzKSB7XHJcbiAgICAgIHRlbXBPYmpbdGhpcy5mb3JtVmFsdWVzLmZvcm1GaWVsZE5hbWVdID0gdGhpcy5mb3JtQnVpbGRlci5hcnJheShbXHJcbiAgICAgICAgdGhpcy5jcmVhdGVDb25kaXRpb24oKSxcclxuICAgICAgXSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLmlucHV0R3JvdXAgPSBuZXcgRm9ybUdyb3VwKHRlbXBPYmopO1xyXG4gICAgdGhpcy5vbkxvYWQoKTtcclxuXHJcbiAgICBpZiAodGhpcy5lbmFibGVUYWJzKSB7XHJcbiAgICAgIHRoaXMuY2hhbmdlVGFiKDApO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMudGFibGVEYXRhKSB7XHJcbiAgICAgIHRoaXMudGFibGVMaXN0ID0gdGhpcy5pbnB1dERhdGFbdGhpcy5mb3JtVmFsdWVzLnRhYmxlS2V5XTtcclxuICAgICAgaWYgKHRoaXMudGFibGVMaXN0ICYmIHRoaXMudGFibGVMaXN0Lmxlbmd0aCkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWRbXCJyZXNwb25zZVwiXSA9IHRoaXMudGFibGVMaXN0O1xyXG4gICAgICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLmZvcm1WYWx1ZXMuaXNUb2dnbGVCYXNlZCkge1xyXG4gICAgICAgIGxldCB0ZW1wT2JqID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJzZWxlY3RlZFRvZ2dsZURldGFpbFwiKTtcclxuICAgICAgICBsZXQgc2VsZWN0ZWRUb2dnbGVEYXRhID0gSlNPTi5wYXJzZSh0ZW1wT2JqKTtcclxuICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZFRvZ2dsZURhdGEsIFwiLi4uLnNlbGVjdGVkVG9nZ2xlRGF0YVwiKTtcclxuICAgICAgICB0aGlzLnRhYmxlTGlzdCA9IHRoaXMuY3VycmVudERhdGEuZm9ybURhdGFbMF0udGFibGVEYXRhO1xyXG4gICAgICAgIGxldCB0ZW1wVGFibGUgPSBfLmZpbHRlcih0aGlzLnRhYmxlTGlzdCwge1xyXG4gICAgICAgICAgdGFibGVJZDogc2VsZWN0ZWRUb2dnbGVEYXRhLnRvZ2dsZUlkLFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHRlbXBUYWJsZSwgXCIuLi4uLi4uLi4uLi4uLi4uLi4uLi4uLi50ZW1wVGFibGVcIik7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnRhYmxlRGF0YSA9IHRlbXBUYWJsZTtcclxuICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWQgPSB0aGlzLmZvcm1WYWx1ZXM7XHJcbiAgICAgICAgdGhpcy5lbmFibGVUYWJsZUxheW91dCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5lbmFibGVOZXdUYWJsZUxheW91dCA9IHRoaXMuZm9ybVZhbHVlcy5lbmFibGVOZXdUYWJsZUxheW91dFxyXG4gICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICA6IGZhbHNlO1xyXG4gICAgICB9IGVsc2UgaWYgKHRoaXMuZm9ybVZhbHVlcy5pc2R5YW1pY0RhdGEpIHtcclxuICAgICAgICAvLyBhZGRlZCBhZGRpdGlvbmFsIHJlcG9ydCBpbXBsZW1lbnRhdGlvblxyXG4gICAgICAgIC8vIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgIC8vICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgICAgIC8vICk7XHJcbiAgICAgICAgLy8gdGhpcy5jdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJ2aWV3XCJdO1xyXG5cclxuICAgICAgICBsZXQgdGVtcE9iaiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiQ3VycmVudFJlcG9ydERhdGFcIik7XHJcbiAgICAgICAgbGV0IFJlcG9ydGl0ZW0gPSBKU09OLnBhcnNlKHRlbXBPYmopO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFJlcG9ydGl0ZW0sIFwiLi4uLkN1cnJlbnRSZXBvcnREYXRhXCIpO1xyXG4gICAgICAgIHZhciBzZWxlY3RlZFJlcG9ydCA9IGV2YWwodGhpcy5mb3JtVmFsdWVzLmNvbmRpdGlvbik7XHJcbiAgICAgICAgdGhpcy50YWJsZUxpc3QgPSB0aGlzLmZvcm1WYWx1ZXMudGFibGVEYXRhO1xyXG5cclxuICAgICAgICBsZXQgdGVtcFRhYmxlID0gXy5maWx0ZXIodGhpcy50YWJsZUxpc3QsIHtcclxuICAgICAgICAgIHRhYmxlSWQ6IHNlbGVjdGVkUmVwb3J0LFxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy50YWJsZURhdGEgPSB0ZW1wVGFibGU7XHJcbiAgICAgICAgLy8gdGhpcy5zZWxlY3RlZG1vZGVsSWQgPSB0aGlzLmlucHV0RGF0YS5faWQ7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgfSBlbHNlIGlmICh0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlVGFibGVMYXlvdXQpIHtcclxuICAgICAgICB0aGlzLnRhYmxlTGlzdCA9IHRoaXMuZm9ybVZhbHVlcy50YWJsZURhdGE7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgICAvLyBsZXQgY3VycmVudFRhYlRhYmxlID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgICAvLyB0aGlzLmN1cnJlbnRUYWJsZUxvYWQgPSBjdXJyZW50VGFiVGFibGU7XHJcblxyXG4gICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLnRhYmxlTGlzdCwgXCI8PDw8PDw8dGFibGVMaXN0XCIpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgY3JlYXRlQ29uZGl0aW9uKCk6IEZvcm1Hcm91cCB7XHJcbiAgICBsZXQgZm9ybUFycmF5VmFsdWUgPSB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzO1xyXG4gICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgIGlmIChmb3JtQXJyYXlWYWx1ZS5zZWxlY3RGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKGZvcm1BcnJheVZhbHVlLnNlbGVjdEZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICB0ZW1wT2JqW2l0ZW0ubmFtZV0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKGZvcm1BcnJheVZhbHVlLmF1dG9Db21wbGV0ZUZpZWxkcykge1xyXG4gICAgICBfLmZvckVhY2goZm9ybUFycmF5VmFsdWUuYXV0b0NvbXBsZXRlRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHRlbXBPYmpbaXRlbS5uYW1lXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gdGhpcy5mb3JtQnVpbGRlci5ncm91cCh0ZW1wT2JqKTtcclxuICB9XHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlLm5leHQoKTtcclxuICAgIHRoaXMudW5zdWJzY3JpYmVDbGljay5uZXh0KCk7XHJcbiAgfVxyXG4gIGFkZE5ld0Zvcm1BcnJheShmb3JtRmllbGROYW1lKSB7XHJcbiAgICAvLyB0aGlzLmlucHV0RGF0YVtmb3JtRmllbGROYW1lXSA9IHRoaXMuaW5wdXRHcm91cC5nZXQoZm9ybUZpZWxkTmFtZSkgYXMgRm9ybUFycmF5O1xyXG4gICAgLy8gaWYgKHRoaXMuaW5wdXREYXRhW2Zvcm1GaWVsZE5hbWVdLmNvbnRyb2xzLmxlbmd0aCA9PSAyKSB7XHJcbiAgICAvLyBcdHRoaXMuaXNEaXNhYmxlZCA9IHRydWU7XHJcbiAgICAvLyB9XHJcbiAgICAvLyB0aGlzLmlucHV0RGF0YVtmb3JtRmllbGROYW1lXS5wdXNoKHRoaXMuY3JlYXRlQ29uZGl0aW9uKCkpO1xyXG4gICAgbGV0IGNvbnRyb2wgPSA8Rm9ybUFycmF5PnRoaXMuaW5wdXRHcm91cC5jb250cm9sc1tmb3JtRmllbGROYW1lXTtcclxuICAgIGNvbnRyb2wucHVzaCh0aGlzLmNyZWF0ZUNvbmRpdGlvbigpKTtcclxuICB9XHJcbiAgb25Mb2FkKCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgY29uc29sZS5sb2coXCIuPi4uLi4gc2VsZi5pbnB1dERhdGEgXCIsIHRoaXMuaW5wdXREYXRhKTtcclxuICAgIGlmICh0aGlzLmVuYWJsZVNlbGVjdEZpZWxkcykge1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLnNlbGVjdEZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgLy8gaWYoaXRlbS5pc0ZpbHRlcilcclxuICAgICAgICAgICAgLy8ge1xyXG4gICAgICAgICAgICAvLyAgIGxldCByZXBvbnNlRGF0YT0gZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgICAvLyAgIHZhciB0ZW1wRGF0YT1yZXBvbnNlRGF0YS5maWx0ZXIoZnVuY3Rpb24odmFsKVxyXG4gICAgICAgICAgICAvLyAgIHtcclxuICAgICAgICAgICAgLy8gICAgIHJldHVybiB2YWxbaXRlbS5maWx0ZXJLZXldPT1pdGVtLmZpbHRlcnZhbHVlO1xyXG4gICAgICAgICAgICAvLyAgIH0pXHJcbiAgICAgICAgICAgIC8vICAgaXRlbS5kYXRhPXRlbXBEYXRhO1xyXG4gICAgICAgICAgICAvLyB9XHJcbiAgICAgICAgICAgIGl0ZW0uZGF0YSA9IGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSBhcyBvYmplY3RbXTtcclxuXHJcbiAgICAgICAgICAgIC8vIHRlbXAuc2VsZWN0ZWREYXRhRmllbGQgPSBpdGVtLmRhdGFbMF0ubmFtZTtcclxuICAgICAgICAgICAgLy8gdGVtcC5kYXRhSWQgPSBpdGVtLmRhdGFbMF0uX2lkO1xyXG4gICAgICAgICAgICAvLyB0ZW1wLmdldENoaXBMaXN0KHRlbXAuZGF0YUlkKVxyXG4gICAgICAgICAgICAvL2NvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+Pj4+Pj4+Pj4gb25Mb2FkIHNlbGV0ZmllbGQgIFwiLGl0ZW0ubmFtZSk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+PiBPbkxvYWQgZGF0YVwiLCBkYXRhKTtcclxuICAgICAgICAgICAgaWYgKGl0ZW0uc2V0RGVmYXVsdERzKSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhLmRhdGFzb3VyY2VOYW1lO1xyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXRHcm91cC5jb250cm9sc1tpdGVtLm5hbWVdLnNldFZhbHVlKFxyXG4gICAgICAgICAgICAgICAgc2VsZi5pbnB1dERhdGEuZGF0YXNvdXJjZUlkXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGl0ZW0uc2V0RGVmYXVsdFJ1bGVzZXQgJiYgKChzZWxmLm9uTG9hZERhdGEgJiYgIXNlbGYub25Mb2FkRGF0YS5zYXZlZERhdGEpIHx8ICFzZWxmLm9uTG9hZERhdGEpKSB7XHJcbiAgICAgICAgICAgICAgXy5mb3JFYWNoKGl0ZW0uZGF0YSwgZnVuY3Rpb24gKHN1Yml0ZW0pIHtcclxuICAgICAgICAgICAgICAgIGlmIChzdWJpdGVtLmRlZmF1bHRSdWxlc2V0KSB7XHJcbiAgICAgICAgICAgICAgICAgIGxldCBpZCA9IHN1Yml0ZW0uX2lkO1xyXG4gICAgICAgICAgICAgICAgICBzZWxmLmlucHV0R3JvdXAuY29udHJvbHNbaXRlbS5uYW1lXS5zZXRWYWx1ZShpZCk7XHJcbiAgICAgICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZSArIFwiSWRcIl0gPSBzdWJpdGVtLl9pZDtcclxuICAgICAgICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5uYW1lICsgXCJOYW1lXCJdID0gc3ViaXRlbS5uYW1lO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+Pj4+IHNlbGYuaW5wdXREYXRhIE9uTG9hZCBcIiwgc2VsZi5pbnB1dERhdGEpO1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeShzZWxmLmlucHV0RGF0YSkpO1xyXG5cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZW5hYmxlTXVsdGlzZWxlY3RGaWVsZHMpIHtcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5tdWx0aVNlbGVjdEZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgcXVlcnkgPSB7XHJcbiAgICAgICAgICAgIHJlYWxtOiBzZWxmLnJlYWxtXHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBpdGVtLmRhdGEgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmICh0aGlzLmVuYWJsZUNoaXBGaWVsZHMpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5lbmFibGVDaGlwRmllbGRzLCBcImVuYWJsZUNoaXBGaWVsZHNcIik7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIGlmIChpdGVtLm9uTG9hZEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgICAvLyBjaGlwZmllbGRzIE9uTG9hZCBmdW5jdGlvbiBhZGRlZCB3aGVuIGltcGxlbWVudCBydW4gcmVwb3J0c1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKHNlbGYuaW5wdXREYXRhLCBcIj4+Pj4+Pj5cIik7XHJcbiAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVxdWVzdEl0ZW0uc3ViS2V5KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNNYXAgJiYgc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdKSB7XHJcbiAgICAgICAgICAgICAgICAgIC8vIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdID8gXy5tYXAoc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdW3JlcXVlc3RJdGVtLnN1YktleV0sIHJlcXVlc3RJdGVtLm1hcEtleSkgOiBbXVxyXG4gICAgICAgICAgICAgICAgICAvLyBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmlzQ29udmVydFRvU3RyaW5nID8gSlNPTi5zdHJpbmdpZnkocXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0pIDogcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV07XHJcbiAgICAgICAgICAgICAgICAgIGxldCB0MSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICAgID8gXy5tYXAoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0SXRlbS5tYXBLZXlcclxuICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgICAgICAgICBpZiAodDEubGVuZ3RoKVxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID1cclxuICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3RJdGVtLmlzQ29udmVydFRvU3RyaW5nICYmIHR5cGVvZiB0MSAhPT0gXCJzdHJpbmdcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHQxKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICA6IHQxO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0pIHtcclxuICAgICAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5pc0NvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkocXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0pXHJcbiAgICAgICAgICAgICAgICAgICAgOiBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgPyBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybClcclxuICAgICAgICAgICAgICAuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpdGVtLmRhdGEgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgICAgICAgICBsZXQgdmFsaWRhdGVEYXRhID0gaXRlbS5kYXRhWzBdO1xyXG4gICAgICAgICAgICAgICAgbGV0IGlzQXJyYXlPZkpTT04gPSBfLmlzUGxhaW5PYmplY3QodmFsaWRhdGVEYXRhKVxyXG4gICAgICAgICAgICAgICAgICA/IHRydWVcclxuICAgICAgICAgICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIGlmICghaXNBcnJheU9mSlNPTikge1xyXG4gICAgICAgICAgICAgICAgICBjb25zdCB0ZW1wTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbi5zdWJSZXNwb25zZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0uZGF0YSA9IGl0ZW0uZGF0YVtpdGVtLm9uTG9hZEZ1bmN0aW9uLnN1YlJlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgICAgICBsZXQga2V5cyA9IF8ua2V5cyhpdGVtLmRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaChrZXlzLCBmdW5jdGlvbiAoaXRlbUtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IGl0ZW0uZGF0YVtpdGVtS2V5XVtyZXF1ZXN0RGF0YVtcImtleVwiXV0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF9pZDogaXRlbUtleSxcclxuICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIF8uZm9yRWFjaChpdGVtLmRhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0ZW1wTGlzdC5wdXNoKHsgbmFtZTogaXRlbSwgX2lkOiBpdGVtIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIGl0ZW0uZGF0YSA9IHRlbXBMaXN0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfSwgMjAwMCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIHNlbGYuQ2hpcExpbWl0W2l0ZW0ubmFtZV0gPVxyXG4gICAgICAgIC8vICAgaXRlbS5jaGlwQ29uZGl0aW9uLmRhdGFbXHJcbiAgICAgICAgLy8gICAgIHNlbGYuaW5wdXREYXRhW11cclxuICAgICAgICAvLyAgIF07XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMub25Mb2FkRGF0YSAmJiB0aGlzLm9uTG9hZERhdGEuc2F2ZWREYXRhKSB7XHJcbiAgICAgIHRoaXMubG9hZFNhdmVkRGF0YSgpO1xyXG4gICAgfVxyXG4gIH1cclxuICAvLyBnZXRPcHRpb25UZXh0KG9wdGlvbikge1xyXG4gIC8vICAgLy8gaWYgKGNvbmRpdGlvbikge1xyXG4gIC8vICAgICBjb25zb2xlLmxvZyhvcHRpb24sKVxyXG4gIC8vICAgLy8gfVxyXG4gIC8vICAgcmV0dXJuIG9wdGlvbi5uYW1lO1xyXG4gIC8vIH1cclxuICBhZGRDaGlwSXRlbShldmVudDogTWF0Q2hpcElucHV0RXZlbnQpOiB2b2lkIHt9XHJcbiAgdmFsaWRhdGVEYXRlKHR5cGU6IFN0cmluZywgZXZlbnQ6IGFueSwgZGF0ZUZpZWxkOiBhbnkpIHtcclxuICAgIHRoaXMuZGF0ZUZvcm1hdGNoZWNrID0gdHJ1ZTtcclxuICAgIGlmIChldmVudC52YWx1ZSkge1xyXG4gICAgICBsZXQgZGlzYmFsZVByZXZpb3VzRGF0ZSA9IGRhdGVGaWVsZC52YWxpZGF0ZXdpdGhuZXh0ZGF0ZSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgaWYgKGRpc2JhbGVQcmV2aW91c0RhdGUpIHRoaXMubWluRW5kRGF0ZSA9IGV2ZW50LnZhbHVlLmFkZCgxLCBcImRheXNcIikuX2Q7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRhdGVGb3JtYXRjaGVjayA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuICBvbkNoaXBTZWFyY2goc2VhcmNoVmFsdWU6IHN0cmluZywgc2VsZWN0ZWRDaGlwKSB7XHJcbiAgICBjb25zb2xlLmxvZyhzZWFyY2hWYWx1ZSwgXCJzZWFyY2hWYWx1ZT4+Pj4+PlwiKTtcclxuICAgIHZhciBjaGFyTGVuZ3RoID0gc2VhcmNoVmFsdWUubGVuZ3RoO1xyXG4gICAgbGV0IHNlbGVjdGVkSW5kZXggPSB0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkcy5pbmRleE9mKHNlbGVjdGVkQ2hpcCk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlYXJjaFZhbHVlXCJdID0gc2VhcmNoVmFsdWU7XHJcbiAgICBpZiAoc2VsZWN0ZWRDaGlwLm9uU2VhcmNoRnVuY3Rpb24pIHtcclxuICAgICAgbGV0IGFwaVVybCA9IHNlbGVjdGVkQ2hpcC5vblNlYXJjaEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IHNlbGVjdGVkQ2hpcC5vblNlYXJjaEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICBsZXQgcXVlcnkgPSB7cmVhbG06IHRoaXMucmVhbG19O1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHZhciB0ZW1wID0ge307XHJcbiAgICAgIF8uZm9yRWFjaChzZWxlY3RlZENoaXAub25TZWFyY2hGdW5jdGlvbi5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKFxyXG4gICAgICAgIHJlcXVlc3RJdGVtXHJcbiAgICAgICkge1xyXG4gICAgICAgIGlmIChyZXF1ZXN0SXRlbS5zdWJLZXkpIHtcclxuICAgICAgICAgIHRlbXAgPVxyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV0gJiZcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdW3JlcXVlc3RJdGVtLnN1YktleV1cclxuICAgICAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSB0ZW1wO1xyXG4gICAgICAgIH0gZWxzZSBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSBpZihyZXF1ZXN0SXRlbS5hbHRlcm5hdGl2ZUtleUNoZWNrKXtcclxuICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gKHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXT09PXVuZGVmaW5lZCApP3NlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLmFsdGVybmF0aXZlS2V5XTpzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIHZhciBsaW1pdCA9IHNlbGVjdGVkQ2hpcC5jaGFybGltaXQgPyBzZWxlY3RlZENoaXAuY2hhcmxpbWl0IDogMztcclxuICAgICAgLy8gY29uc29sZS5sb2coXCJMaW1pdCBcIiwgbGltaXQpO1xyXG4gICAgICAvLyBpZiAoY2hhckxlbmd0aCA+PSBsaW1pdCB8fCAhc2VhcmNoVmFsdWUpIHtcclxuICAgICAgICB0aGlzLmNoYXJsaW1pdGNoZWNrID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkc1tzZWxlY3RlZEluZGV4XS5kYXRhID0gZGF0YS5yZXNwb25zZVtcclxuICAgICAgICAgICAgcmVzcG9uc2VOYW1lXHJcbiAgICAgICAgICBdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgbGV0IGl0ZW0gPSB0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkc1tzZWxlY3RlZEluZGV4XTtcclxuICAgICAgICAgIGxldCB2YWxpZGF0ZURhdGEgPSBpdGVtLmRhdGFbMF07XHJcbiAgICAgICAgICBsZXQgaXNBcnJheU9mSlNPTiA9IF8uaXNQbGFpbk9iamVjdCh2YWxpZGF0ZURhdGEpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgaWYgKCFpc0FycmF5T2ZKU09OKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHRlbXBMaXN0ID0gW107XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChpdGVtLmRhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7IG5hbWU6IGl0ZW0sIF9pZDogaXRlbSB9KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGl0ZW0uZGF0YSA9IHRlbXBMaXN0O1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAvLyB9IGVsc2Uge1xyXG4gICAgICAvLyAgIGNvbnNvbGUubG9nKFwiU3RvcCBBUEkgXCIpO1xyXG4gICAgICAvLyAgIHRoaXMuY2hhcmxpbWl0Y2hlY2sgPSBmYWxzZTtcclxuICAgICAgLy8gfVxyXG4gICAgfVxyXG4gIH1cclxuICBvbkxvYWRDaGlwQ2hhbmdlKGl0ZW0pIHtcclxuICAgIGlmIChpdGVtLm9uTG9hZEZ1bmN0aW9uKSB7XHJcbiAgICAgIC8vIGNoaXBmaWVsZHMgT25Mb2FkIGZ1bmN0aW9uIGFkZGVkIHdoZW4gaW1wbGVtZW50IHJ1biByZXBvcnRzXHJcbiAgICAgIGxldCBhcGlVcmwgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgIGxldCByZXF1ZXN0RGF0YSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVxdWVzdERhdGE7XHJcbiAgICAgIGxldCBxdWVyeSA9IHtyZWFsbTp0aGlzLnJlYWxtfTtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2gocmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHJlcXVlc3RJdGVtLCBcIj4+cmVxdWVzdEl0ZW1cIik7XHJcbiAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLnN1YktleSkge1xyXG4gICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzTWFwICYmIHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSkge1xyXG4gICAgICAgICAgICAvLyBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSA/IF8ubWFwKHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVtyZXF1ZXN0SXRlbS5zdWJLZXldLCByZXF1ZXN0SXRlbS5tYXBLZXkpIDogW11cclxuICAgICAgICAgICAgLy8gcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5pc0NvbnZlcnRUb1N0cmluZyA/IEpTT04uc3RyaW5naWZ5KHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdKSA6IHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdO1xyXG4gICAgICAgICAgICBsZXQgdDEgPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgICAgICA/IF8ubWFwKFxyXG4gICAgICAgICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1bcmVxdWVzdEl0ZW0uc3ViS2V5XSxcclxuICAgICAgICAgICAgICAgICAgcmVxdWVzdEl0ZW0ubWFwS2V5XHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgOiBbXTtcclxuICAgICAgICAgICAgaWYgKHQxLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID1cclxuICAgICAgICAgICAgICAgIHJlcXVlc3RJdGVtLmlzQ29udmVydFRvU3RyaW5nICYmIHR5cGVvZiB0MSAhPT0gXCJzdHJpbmdcIlxyXG4gICAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHQxKVxyXG4gICAgICAgICAgICAgICAgICA6IHQxO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSkge1xyXG4gICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgID8gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdW3JlcXVlc3RJdGVtLnN1YktleV1cclxuICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmlzQ29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSlcclxuICAgICAgICAgICAgICA6IHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgICBjb25zb2xlLmxvZyhxdWVyeSwgXCI+Pj5xdWVyeVwiKTtcclxuICAgICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4gVEhJU1wiKTtcclxuICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgIGl0ZW0uZGF0YSA9IGRhdGEucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSBhcyBvYmplY3RbXTtcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcHNcIl1baXRlbS5uYW1lXSA9IGl0ZW0uZGF0YTtcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcEtleUxpc3RcIl1baXRlbS5uYW1lXSA9IF8ubWFwKFxyXG4gICAgICAgICAgaXRlbS5kYXRhLFxyXG4gICAgICAgICAgaXRlbS5vbkxvYWRGdW5jdGlvbi5rZXlUb1NhdmVcclxuICAgICAgICApO1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHNlbGYuaW5wdXREYXRhLCBcIj4+PiBJTlBVVFwiKTtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeShzZWxmLmlucHV0RGF0YSkpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcbiAgb25DaGlwU2VsZWN0KFxyXG4gICAgZXZlbnQ6IE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnQsXHJcbiAgICBzZWxlY3RlZENoaXBJZCxcclxuICAgIGtleVRvU2F2ZSxcclxuICAgIGNoaXBFcnJvclxyXG4gICk6IHZvaWQge1xyXG4gICAgdGhpcy5jaGlwTGlzdE9wZW5WaWV3ID0gdHJ1ZTtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcIkV2ZW50Pj4+Pj4+PlwiLCB0aGlzLkNoaXBMaW1pdCwgdGhpcy5jaGlwTGVuZ3RoKTtcclxuICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkQ2hpcElkLCBcIj4+c2VsZWN0ZWRDaGlwSWRcIik7XHJcbiAgICAvLyBjb25zb2xlLmxvZyhjaGlwRXJyb3IsIFwiRXZlbnQ+Pj4+Pj4+XCIpO1xyXG4gICAgaWYgKHRoaXMuQ2hpcExpbWl0ID09IHRoaXMuY2hpcExlbmd0aCkge1xyXG4gICAgICB0aGlzLmNoaXBJbnB1dC5uYXRpdmVFbGVtZW50LnZhbHVlID0gXCJcIjtcclxuICAgICAgLy8gdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgLy8gICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoY2hpcEVycm9yKVxyXG4gICAgICAvLyApO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jaGlwVmFsaWRpdGFpb24gPSBmYWxzZTtcclxuICAgICAgdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXSA9IHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF1cclxuICAgICAgICA/IHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF1cclxuICAgICAgICA6IFtdO1xyXG4gICAgICAvLyBpZiAodGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXS5pbmRleE9mKGV2ZW50Lm9wdGlvbi52YWx1ZSkgPCAwKSB7XHJcbiAgICAgIC8vICAgdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXS5wdXNoKGV2ZW50Lm9wdGlvbi52YWx1ZSk7XHJcbiAgICAgIC8vIH1cclxuICAgICAgbGV0IGV4aXRJbmRleD0gXy5maW5kSW5kZXgodGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXSwgZXZlbnQub3B0aW9uLnZhbHVlKVxyXG4gICAgICBjb25zb2xlLmxvZygnPj4gZXhpdEluZGV4ICcsZXhpdEluZGV4KTtcclxuICAgICAgaWYgKGV4aXRJbmRleCA8IDApIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkQ2hpcHNbc2VsZWN0ZWRDaGlwSWRdLnB1c2goZXZlbnQub3B0aW9uLnZhbHVlKTtcclxuICAgICAgfVxyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkcywgXCI8PDw8PD4+Pj4+XCIpO1xyXG4gICAgICBsZXQgc2VsZWN0ZWRGb3JtQ2hpcCA9IF8uZmluZCh0aGlzLmZvcm1WYWx1ZXMuY2hpcEZpZWxkcywge1xyXG4gICAgICAgIG5hbWU6IHNlbGVjdGVkQ2hpcElkLFxyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2coc2VsZWN0ZWRGb3JtQ2hpcCwgXCI+Pj5zZWxlY3RlZEZvcm1DaGlwXCIpO1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLmlucHV0RGF0YSwgXCI8PDw8PGlucHV0RGF0YT4+Pj4+XCIpO1xyXG4gICAgICBldmVudFtcInZhbHVlXCJdID0gbnVsbDtcclxuICAgICAgdGhpcy5jaGlwSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9IFwiXCI7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwS2V5TGlzdFtzZWxlY3RlZENoaXBJZF0gPSBfLm1hcChcclxuICAgICAgICB0aGlzLnNlbGVjdGVkQ2hpcHNbc2VsZWN0ZWRDaGlwSWRdLFxyXG4gICAgICAgIGtleVRvU2F2ZVxyXG4gICAgICApO1xyXG4gICAgICAvLyB0aGlzLm9uTG9hZENoaXBDaGFuZ2UoKTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdID0gdGhpcy5zZWxlY3RlZENoaXBLZXlMaXN0O1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcHNcIl0gPSB0aGlzLnNlbGVjdGVkQ2hpcHM7XHJcbiAgICAgIGlmIChzZWxlY3RlZEZvcm1DaGlwICYmIHNlbGVjdGVkRm9ybUNoaXBbXCJvbkNoYW5nZUxvYWRcIl0pIHtcclxuICAgICAgICBsZXQgb25Mb2FkQ2hpcERhdGEgPSBfLmZpbmQodGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIHtcclxuICAgICAgICAgIG5hbWU6IHNlbGVjdGVkRm9ybUNoaXBbXCJsb2FkY2hpbGRLZXlcIl0sXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5vbkxvYWRDaGlwQ2hhbmdlKG9uTG9hZENoaXBEYXRhKTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmNoaXBMZW5ndGggPSB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcHNcIl1bc2VsZWN0ZWRDaGlwSWRdLmxlbmd0aDtcclxuICAgICAgdGhpcy5zZWxlY3RlZGNoaXBMZW5ndGggPSB0aGlzLnNlbGVjdGVkQ2hpcHNbc2VsZWN0ZWRDaGlwSWRdLmxlbmd0aDtcclxuICAgICAgdGhpcy5pbnB1dEdyb3VwLmdldChzZWxlY3RlZENoaXBJZCkuc2V0VmFsdWUoXCJcIik7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHJlbW92ZVNlbGVjdGVkQ2hpcCh2YWx1ZSwgc2VsZWN0ZWRDaGlwSWQsIGtleVRvU2F2ZSkge1xyXG4gICAgY29uc29sZS5sb2coXCJyZW1vdmVTZWxlY3RlZENoaXAgXCIsIHRoaXMuaW5wdXREYXRhKTtcclxuICAgIGxldCBzZWxlY3RlZEluZGV4ID0gdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXS5pbmRleE9mKHZhbHVlKTtcclxuICAgIGlmIChzZWxlY3RlZEluZGV4ID49IDApIHtcclxuICAgICAgdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkQ2hpcElkXS5zcGxpY2Uoc2VsZWN0ZWRJbmRleCwgMSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnNlbGVjdGVkQ2hpcEtleUxpc3Rbc2VsZWN0ZWRDaGlwSWRdID0gXy5tYXAoXHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwc1tzZWxlY3RlZENoaXBJZF0sXHJcbiAgICAgIGtleVRvU2F2ZVxyXG4gICAgKTtcclxuICAgIFxyXG4gICAgdGhpcy5jaGlwTGVuZ3RoID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdW3NlbGVjdGVkQ2hpcElkXS5sZW5ndGg7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmNoaXBMZW5ndGgsXCI+Pj4+PiBTRUxFQ1RFRCBDSElQIFZBTFVFIExFTkdUSFwiKVxyXG4gICAgbGV0IHNlbGVjdGVkRm9ybUNoaXAgPSBfLmZpbmQodGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIHtcclxuICAgICAgbmFtZTogc2VsZWN0ZWRDaGlwSWQsXHJcbiAgICB9KTtcclxuICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkRm9ybUNoaXApXHJcbiAgICBpZih0aGlzLmNoaXBMZW5ndGgpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkRm9ybUNoaXAsIFwiPj4+c2VsZWN0ZWRGb3JtQ2hpcFwiKTtcclxuICAgICAgICBpZiAoc2VsZWN0ZWRGb3JtQ2hpcCAmJiBzZWxlY3RlZEZvcm1DaGlwW1wib25DaGFuZ2VMb2FkXCJdKSB7XHJcbiAgICAgICAgICBsZXQgb25Mb2FkQ2hpcERhdGEgPSBfLmZpbmQodGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIHtcclxuICAgICAgICAgICAgbmFtZTogc2VsZWN0ZWRGb3JtQ2hpcFtcImxvYWRjaGlsZEtleVwiXSxcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5vbkxvYWRDaGlwQ2hhbmdlKG9uTG9hZENoaXBEYXRhKTtcclxuICAgICAgICB9XHJcbiAgICB9ZWxzZXtcclxuICAgICAgdGhpcy5zZWxlY3RlZENoaXBzW3NlbGVjdGVkRm9ybUNoaXBbXCJsb2FkY2hpbGRLZXlcIl1dID0gW107XHJcbiAgICB9XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcEtleUxpc3RcIl0gPSB0aGlzLnNlbGVjdGVkQ2hpcEtleUxpc3Q7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcHNcIl0gPSB0aGlzLnNlbGVjdGVkQ2hpcHM7XHJcbiAgfVxyXG4gIGxvYWRTYXZlZERhdGEoKSB7XHJcbiAgICBsZXQgc2F2ZWREYXRhID0gdGhpcy5vbkxvYWREYXRhLnNhdmVkRGF0YTtcclxuICAgIGxldCB2aWV3U2F2ZWREYXRhID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVt0aGlzLm9uTG9hZERhdGEuYWN0aW9uXVxyXG4gICAgICAuc2F2ZWREYXRhRGV0YWlscztcclxuICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIHRoaXMuaW5wdXREYXRhW1wiX2lkXCJdID0gc2F2ZWREYXRhLl9pZDtcclxuICAgIF8uZm9yRWFjaCh2aWV3U2F2ZWREYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBpZiAoaXRlbS5zdWJLZXkpIHtcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd10gPSBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd11cclxuICAgICAgICAgID8gc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddXHJcbiAgICAgICAgICA6IHt9O1xyXG4gICAgICAgIGlmIChpdGVtLmFzc2lnbkZpcnN0SW5kZXh2YWwpIHtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA9IChzYXZlZERhdGFbaXRlbS5uYW1lXSAmJiBzYXZlZERhdGFbaXRlbS5uYW1lXS5sZW5ndGgpID8gc2F2ZWREYXRhW2l0ZW0ubmFtZV1bMF1baXRlbS5zdWJLZXldIDogXCJcIjtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgLy8gc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddID0gc2F2ZWREYXRhW2l0ZW0ubmFtZV1baXRlbS5zdWJLZXldO1xyXG4gICAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddID1cclxuICAgICAgICAgICAgc2F2ZWREYXRhW2l0ZW0ubmFtZV0gJiYgc2F2ZWREYXRhW2l0ZW0ubmFtZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgPyBzYXZlZERhdGFbaXRlbS5uYW1lXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgfVxyXG4gICAgICB9ZWxzZSBpZihpdGVtLmlzQ29uZGl0aW9uKXtcclxuICAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleVRvU2hvd10gPSAgZXZhbChpdGVtLmNvbmRpdGlvbik7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddID0gc2F2ZWREYXRhW2l0ZW0ubmFtZV07XHJcbiAgICAgIH1cclxuICAgICAgaWYgKGl0ZW0udGFrZUtleUxpc3QpIHtcclxuICAgICAgICBpZihpdGVtLmtleUxpc3ROYW1lKXtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5TGlzdE5hbWVdID0gc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlMaXN0TmFtZV1cclxuICAgICAgICAgID8gc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlMaXN0TmFtZV1cclxuICAgICAgICAgIDoge307XHJcbiAgICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLmtleUxpc3ROYW1lXVtpdGVtLmNoaXBJZF0gPSBbXTtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5TGlzdE5hbWVdW2l0ZW0uY2hpcElkXSA9IF8ubWFwKFxyXG4gICAgICAgICAgICBzYXZlZERhdGFbaXRlbS5uYW1lXSxcclxuICAgICAgICAgICAgaXRlbS5rZXlUb1NhdmVcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA/IHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5VG9TaG93XSA6e307XHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5rZXlUb1Nob3ddW2l0ZW0uY2hpcElkXSA9IHNhdmVkRGF0YVtpdGVtLm5hbWVdO1xyXG4gICAgICAgIGlmIChpdGVtLmtleVRvU2hvdyA9PSBcInNlbGVjdGVkQ2hpcHNcIikge1xyXG4gICAgICAgICAgc2VsZi5zZWxlY3RlZENoaXBzW2l0ZW0uY2hpcElkXSA9IHNhdmVkRGF0YVtpdGVtLm5hbWVdO1xyXG4gICAgICAgICAgc2VsZi5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBzXCJdID0gIHNlbGYuc2VsZWN0ZWRDaGlwcztcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKGl0ZW0ucHV0S2V5TGlzdCkge1xyXG4gICAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS5uYW1lXSA9XHJcbiAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0ua2V5TGlzdE5hbWVdW2l0ZW0uY2hpcElkXTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuc2VsZWN0ZWREYXRhS2V5KSB7XHJcbiAgICAgIGxldCBzZWxlY3RlZERhdGFLZXkgPSB0aGlzLmZvcm1WYWx1ZXMuc2VsZWN0ZWREYXRhS2V5O1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbc2VsZWN0ZWREYXRhS2V5XSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW3NlbGVjdGVkRGF0YUtleV0ubGVuZ3RoXHJcbiAgICAgICkge1xyXG4gICAgICAgIHRoaXMuc2VsZWN0ZWRUYWJsZVZpZXcgPVxyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXNcclxuICAgICAgICAgICAgPyB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzLnRhYmxlRGF0YVxyXG4gICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkID1cclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzXHJcbiAgICAgICAgICAgID8gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlc1xyXG4gICAgICAgICAgICA6IHt9O1xyXG4gICAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLmlucHV0RGF0YVtzZWxlY3RlZERhdGFLZXldO1xyXG4gICAgICAgIHRoaXMuZW5hYmxlU2VsZWN0VmFsdWVWaWV3ID0gdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YSAmJiB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEubGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZFRhYmxlVmlldyA9XHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlc1xyXG4gICAgICAgICAgICA/IHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMudGFibGVEYXRhXHJcbiAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgdGhpcy5zZWxlY3RUYWJsZUxvYWQgPVxyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXNcclxuICAgICAgICAgICAgPyB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzXHJcbiAgICAgICAgICAgIDoge307XHJcbiAgICAgICAgdGhpcy5zZWxlY3RUYWJsZUxvYWRbXCJyZXNwb25zZVwiXSA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YTtcclxuICAgICAgICB0aGlzLmVuYWJsZVNlbGVjdFZhbHVlVmlldyA9IHRydWU7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLnNob3dTZWxlY3RlZENoaXBzKSB7XHJcbiAgICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLmNoaXBDb25kaXRpb24pIHtcclxuICAgICAgICBsZXQgdGVtcCA9IHRoaXMuZm9ybVZhbHVlcy5jaGlwQ29uZGl0aW9uLnZhcmlhYmxlO1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy5jaGlwQ29uZGl0aW9uLmRhdGEgJiZcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhICYmXHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVt0ZW1wXSAmJlxyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmNoaXBDb25kaXRpb24uZGF0YVt0aGlzLmlucHV0RGF0YVt0ZW1wXV1cclxuICAgICAgICApIHtcclxuICAgICAgICAgIHRoaXMuQ2hpcExpbWl0ID0gdGhpcy5mb3JtVmFsdWVzLmNoaXBDb25kaXRpb24uZGF0YVtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbdGVtcF1cclxuICAgICAgICAgIF1bXCJsaW1pdFwiXTtcclxuICAgICAgICAgIHRoaXMuQ2hpcE9wZXJhdG9yID0gdGhpcy5mb3JtVmFsdWVzLmNoaXBDb25kaXRpb24uZGF0YVtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbdGVtcF1cclxuICAgICAgICAgIF1bXCJvcGVyYXRvclwiXTtcclxuICAgICAgICAgIFxyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJjaGlwT3BlcmF0b3JcIl0gPSB0aGlzLkNoaXBPcGVyYXRvcjtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHRoaXMudmlld0RhdGEgPSB0aGlzLmlucHV0RGF0YTtcclxuICAgIC8vIHRoaXMuaW5wdXRHcm91cC5jb250cm9sc1sncnVsZXNldCddLnNldFZhbHVlKHRoaXMuaW5wdXREYXRhLnJ1bGVzZXQpO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICB9XHJcblxyXG4gIGdldENoaXBMaXN0KCkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+IGVudGVyZWQgY2hpcCBmaWVsZHMgXCIpO1xyXG5cclxuICAgIGlmICh0aGlzLmVuYWJsZUNoaXBGaWVsZHMpIHtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJpdGVtIFwiLCBpdGVtKTtcclxuICAgICAgICBpZiAoaXRlbS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSBpdGVtLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0uaXNDb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkoc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdKVxyXG4gICAgICAgICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCBhcGlVcmwpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBpdGVtLmRhdGEgPSBkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W107XHJcbiAgICAgICAgICAgIGxldCB2YWxpZGF0ZURhdGEgPSBpdGVtLmRhdGFbMF07XHJcbiAgICAgICAgICAgIGxldCBpc0FycmF5T2ZKU09OID0gXy5pc1BsYWluT2JqZWN0KHZhbGlkYXRlRGF0YSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIGlmICghaXNBcnJheU9mSlNPTikge1xyXG4gICAgICAgICAgICAgIGNvbnN0IHRlbXBMaXN0ID0gW107XHJcbiAgICAgICAgICAgICAgaWYgKGl0ZW0ub25Mb2FkRnVuY3Rpb24uc3ViUmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgIGl0ZW0uZGF0YSA9IGl0ZW0uZGF0YVtpdGVtLm9uTG9hZEZ1bmN0aW9uLnN1YlJlc3BvbnNlXTtcclxuICAgICAgICAgICAgICAgIGxldCBrZXlzID0gXy5rZXlzKGl0ZW0uZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBfLmZvckVhY2goa2V5cywgZnVuY3Rpb24gKGl0ZW1LZXkpIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogaXRlbS5kYXRhW2l0ZW1LZXldW3JlcXVlc3REYXRhW1wia2V5XCJdXSxcclxuICAgICAgICAgICAgICAgICAgICBfaWQ6IGl0ZW1LZXksXHJcbiAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIF8uZm9yRWFjaChpdGVtLmRhdGEsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goeyBuYW1lOiBpdGVtLCBfaWQ6IGl0ZW0gfSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgaXRlbS5kYXRhID0gdGVtcExpc3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgfVxyXG4gIH1cclxuICByZW1vdmVUYWcoY2hpcCwgaWQpIHtcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGlwW2lkXSA9XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwW2lkXSAmJiB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0ubGVuZ3RoXHJcbiAgICAgICAgPyB0aGlzLnNlbGVjdGVkQ2hpcFtpZF1cclxuICAgICAgICA6IFtdO1xyXG4gICAgY29uc3QgaW5kZXggPSB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0uaW5kZXhPZihjaGlwKTtcclxuICAgIGlmIChpbmRleCA+PSAwKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRDaGlwW2lkXS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gIH1cclxuICBvbkJ1dHRvbkNsaWNrKGRhdGEsIGZvcm1WYWx1ZXMpIHtcclxuICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiLi4uLi5EQVRBXCIpO1xyXG4gICAgY29uc29sZS5sb2coZm9ybVZhbHVlcywgXCIuLi4uZm9ybVZhbHVlc1wiKTtcclxuICAgIGlmIChkYXRhLmFjdGlvbiA9PSBcImV4cG9ydFwiKSB7XHJcbiAgICAgIHRoaXMub25FeHBvcnRDbGljayhkYXRhLCBmb3JtVmFsdWVzKTtcclxuICAgIH0gZWxzZSBpZiAoZGF0YS5hY3Rpb24gPT0gXCJzY2hlZHVsZVZpZXdcIikge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcInJlcG9ydE5hbWVcIl0gPSB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgZm9ybVZhbHVlcy5uYW1lXHJcbiAgICAgICk7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wicmVwb3J0VHlwZVwiXSA9IGZvcm1WYWx1ZXMucmVwb3J0VHlwZTtcclxuICAgICAgaWYgKCF0aGlzLmlucHV0RGF0YVtcInJlcGVhdFR5cGVcIl0pXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJyZXBlYXRUeXBlXCJdID0gXCJydW5fb25jZVwiO1xyXG5cclxuICAgICAgLy8gIHRoaXMuaW5wdXREYXRhW1wic2NoZWR1bGVUeXBlXCJdID0gZm9ybVZhbHVlcy5pc1NpbmdsZSA/IDMgOiAyO1xyXG4gICAgICBsZXQgcmVxdWVzdERhdGEgPSBmb3JtVmFsdWVzLnNjaGVkdWxlUmVxdWVzdFxyXG4gICAgICAgID8gZm9ybVZhbHVlcy5zY2hlZHVsZVJlcXVlc3QucmVxdWVzdERhdGFcclxuICAgICAgICA6IFtdO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBmb3JtVmFsdWVzLnNjaGVkdWxlUmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIHJlcXVlc3REYXRhW1wicmVhbG1cIl0gPSB0aGlzLnJlYWxtO1xyXG4gICAgICB0aGlzLnZhbGlkYXRlU2NoZHVsZURhdGEoZnVuY3Rpb24gKHJlc3VsdCkge1xyXG4gICAgICAgIGlmIChyZXN1bHQpIHtcclxuICAgICAgICAgIHNlbGYuY29uc3RydWN0U2NoZWR1bGVEYXRhKHJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmV0dXJuRGF0YSkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4+Pi4uIHJldHVybkRhdGEgXCIsIHJldHVybkRhdGEpO1xyXG4gICAgICAgICAgICBpZiAocmV0dXJuRGF0YSkge1xyXG4gICAgICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5jcmVhdGVSZXF1ZXN0KHJldHVybkRhdGEsIGZvcm1WYWx1ZXMuc2NoZWR1bGVSZXF1ZXN0LmFwaVVybClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIiBjcmV0YWUgUmVxdWVzdCBzdWNjZXNzIFwiLCByZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgc2VsZi5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICAgIHNlbGYubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCIgY3JldGFlIFJlcXVlc3QgRXJyb3IgXCIsIGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICBzZWxmLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIGlmIChkYXRhLmFjdGlvbiA9PSBcInJlc2V0Vmlld1wiKSB7XHJcbiAgICAgIHRoaXMubmdPbkluaXQoKTtcclxuICAgIH0gZWxzZSBpZiAoZGF0YS5hY3Rpb24gPT0gXCJjYW5jZWxcIikge1xyXG4gICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgfSBlbHNlIGlmKGRhdGEuYWN0aW9uPT0naW1wb3J0Jyl7XHJcbiAgICAgIHRoaXMub25JbXBvcnQoKTtcclxuICAgIH1cclxuICAgIGVsc2UgaWYgKGRhdGEuYWN0aW9uID09IFwic3VibWl0Vmlld1wiIHx8IGRhdGEuYWN0aW9uID09IFwiY2hlY2tDb25mbGljdFwiIHx8IGRhdGEuYWN0aW9uID09IFwidXBkYXRlXCIpIHtcclxuICAgICAgY29uc29sZS5sb2codGhpcy5pbnB1dEdyb3VwLCBcIj4+Pj4+Pj4+Pj4+c3NcIik7XHJcbiAgICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMuZ2V0Rm9ybUNvbnRyb2xWYWx1ZSkge1xyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy52YWx1ZVRvU2F2ZV0gPSB0aGlzLmlucHV0R3JvdXAudmFsdWVbXHJcbiAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMuZm9ybUZpZWxkTmFtZVxyXG4gICAgICAgIF07XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybVZhbHVlcy5pc01lcmdlT2JqZWN0KSB7XHJcbiAgICAgICAgICBsZXQgb2JqID0gdGhpcy5pbnB1dEdyb3VwLnZhbHVlW3RoaXMuZm9ybVZhbHVlcy5tYXBPYmplY3ROYW1lXTtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhID0geyAuLi50aGlzLmlucHV0RGF0YSwgLi4ub2JqWzBdIH07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIi4uLi4uLklOUFVUIERBVEFcIik7XHJcbiAgICAgIH0gZWxzZSBpZiAodGhpcy5mb3JtVmFsdWVzLmdldEZyb21jdXJyZW50SW5wdXQpIHtcclxuICAgICAgICBsZXQgdGVtcERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpKTtcclxuICAgICAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLnRlbXBEYXRhIH07XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy52aWV3RGF0YSA9IHRoaXMuaW5wdXREYXRhO1xyXG4gICAgICB0aGlzLm9uU3VibWl0KCk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSBmYWxzZTtcclxuICAgICAgY29uc29sZS5sb2coZGF0YSwgXCIuLi4uREFUQVwiKTtcclxuICAgICAgY29uc29sZS5sb2coZm9ybVZhbHVlcywgXCIuLi4uLi5Gb3JtVmFsdWVzXCIpO1xyXG4gICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgIGlmIChmb3JtVmFsdWVzLnZhbGlkYXRlRm9ybSkge1xyXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICB0aGlzLnZhbGlkYXRpb25DaGVjayhmdW5jdGlvbiAocmVzdWx0KSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcInJlc3VsdCA+PD48Pjw+PD48XCIsIHJlc3VsdCk7XHJcbiAgICAgICAgICBpZiAocmVzdWx0KSB7XHJcbiAgICAgICAgICAgIGxldCBjb25mbGljdFJlcXVlc3QgPSBmb3JtVmFsdWVzLnJlcXVlc3REZXRhaWxzO1xyXG4gICAgICAgICAgICBsZXQgcmVxdWVzdERldGFpbHMgPSBjb25mbGljdFJlcXVlc3QucmVxdWVzdERhdGE7XHJcbiAgICAgICAgICAgIGxldCBhcGlVcmwgPSBjb25mbGljdFJlcXVlc3QuYXBpVXJsO1xyXG4gICAgICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGNvbmZsaWN0UmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgICAgIGxldCBjdXJyZW50SW5wdXREYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSB7cmVhbG06IHNlbGYucmVhbG19O1xyXG4gICAgICAgICAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgbGV0IHRlbXBEYXRhID0gaXRlbS5zdWJLZXlcclxuICAgICAgICAgICAgICAgID8gY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgICAgICAgIDogY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICBpZiAodGVtcERhdGEpIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHRlbXBEYXRhKVxyXG4gICAgICAgICAgICAgICAgICA6IHRlbXBEYXRhO1xyXG4gICAgICAgICAgICAgIH0gZWxzZSBpZiAoaXRlbS5kaXJlY3RBc3NpZ24pIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KGl0ZW0udmFsdWUpXHJcbiAgICAgICAgICAgICAgICAgIDogaXRlbS52YWx1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhyZXF1ZXN0RGF0YSwgXCIuLi4ucmVxdWVzdERhdGFcIik7XHJcbiAgICAgICAgICAgIHNlbGYubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocmVxdWVzdERhdGEsIGFwaVVybCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgIChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgIHNlbGYubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXN1bHQucmVzcG9uc2UsIFwiLi4uLmRhdGEucmVzcG9uc2VcIik7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhjb25mbGljdFJlcXVlc3QucmVzcG9uc2VOYW1lLCBcIi4uLi4uUkVTUE9OU0UgTkFNRVwiKTtcclxuICAgICAgICAgICAgICAgIGxldCB0ZW1wRGF0YSA9IHJlc3VsdC5yZXNwb25zZVtcclxuICAgICAgICAgICAgICAgICAgY29uZmxpY3RSZXF1ZXN0LnJlc3BvbnNlTmFtZVxyXG4gICAgICAgICAgICAgICAgXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRlbXBEYXRhLCBcIi4uLi50ZW1wRGF0YVwiKTtcclxuICAgICAgICAgICAgICAgIHNlbGYudGFibGVMaXN0ID0gdGVtcERhdGE7XHJcbiAgICAgICAgICAgICAgICAvLyBsZXQgY3VycmVudFRhYlRhYmxlID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgICAgICAgICAgICAgLy8gdGhpcy5jdXJyZW50VGFibGVMb2FkID0gY3VycmVudFRhYlRhYmxlO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5jdXJyZW50VGFibGVMb2FkID0gc2VsZi5mb3JtVmFsdWVzO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5jdXJyZW50VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSBzZWxmLnRhYmxlTGlzdDtcclxuICAgICAgICAgICAgICAgIHNlbGYuY3VycmVudFRhYmxlTG9hZFtcInRvdGFsXCJdID0gcmVzdWx0LnJlc3BvbnNlLnRvdGFsO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5lbmFibGVUYWJsZUxheW91dCA9IHNlbGYuZm9ybVZhbHVlcy5lbmFibGVOZXdUYWJsZUxheW91dFxyXG4gICAgICAgICAgICAgICAgICA/IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgIDogdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHNlbGYuZW5hYmxlTmV3VGFibGVMYXlvdXQgPSBzZWxmLmZvcm1WYWx1ZXMuZW5hYmxlTmV3VGFibGVMYXlvdXRcclxuICAgICAgICAgICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAvLyBzZWxmLmVuYWJsZSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgIHNlbGYubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkVyciBcIiwgZXJyKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGxldCBjb25mbGljdFJlcXVlc3QgPSBmb3JtVmFsdWVzLnJlcXVlc3REZXRhaWxzO1xyXG4gICAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IGNvbmZsaWN0UmVxdWVzdC5yZXF1ZXN0RGF0YTtcclxuICAgICAgICBsZXQgYXBpVXJsID0gY29uZmxpY3RSZXF1ZXN0LmFwaVVybDtcclxuICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGNvbmZsaWN0UmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgbGV0IGN1cnJlbnRJbnB1dERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpKTtcclxuICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSB7cmVhbG06IHRoaXMucmVhbG19O1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgIGxldCB0ZW1wRGF0YSA9IGl0ZW0uc3ViS2V5XHJcbiAgICAgICAgICAgID8gY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV1cclxuICAgICAgICAgICAgOiBjdXJyZW50SW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgaWYgKHRlbXBEYXRhKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkodGVtcERhdGEpXHJcbiAgICAgICAgICAgICAgOiB0ZW1wRGF0YTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoaXRlbS5kaXJlY3RBc3NpZ24pIHtcclxuICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShpdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICAgIDogaXRlbS52YWx1ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhyZXF1ZXN0RGF0YSwgXCIuLi4ucmVxdWVzdERhdGFcIik7XHJcbiAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0YXJ0TG9hZGVyKCk7XHJcbiAgICAgICAgdGhpcy5jb250ZW50U2VydmljZS5nZXRBbGxSZXBvbnNlKHJlcXVlc3REYXRhLCBhcGlVcmwpLnN1YnNjcmliZShcclxuICAgICAgICAgIChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LnJlc3BvbnNlLCBcIi4uLi5kYXRhLnJlc3BvbnNlXCIpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhjb25mbGljdFJlcXVlc3QucmVzcG9uc2VOYW1lLCBcIi4uLi4uUkVTUE9OU0UgTkFNRVwiKTtcclxuICAgICAgICAgICAgbGV0IHRlbXBEYXRhID0gcmVzdWx0LnJlc3BvbnNlW1xyXG4gICAgICAgICAgICAgIGNvbmZsaWN0UmVxdWVzdC5yZXNwb25zZU5hbWVcclxuICAgICAgICAgICAgXSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codGVtcERhdGEsIFwiLi4uLnRlbXBEYXRhXCIpO1xyXG4gICAgICAgICAgICB0aGlzLnRhYmxlTGlzdCA9IHRlbXBEYXRhO1xyXG4gICAgICAgICAgICAvLyBsZXQgY3VycmVudFRhYlRhYmxlID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmN1cnJlbnRUYWJsZUxvYWQgPSBjdXJyZW50VGFiVGFibGU7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZCA9IHRoaXMuZm9ybVZhbHVlcztcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLnRhYmxlTGlzdDtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50VGFibGVMb2FkW1widG90YWxcIl0gPSByZXN1bHQucmVzcG9uc2UudG90YWw7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmVuYWJsZU5ld1RhYmxlTGF5b3V0ID0gdGhpcy5mb3JtVmFsdWVzLmVuYWJsZU5ld1RhYmxlTGF5b3V0XHJcbiAgICAgICAgICAgICAgPyB0cnVlXHJcbiAgICAgICAgICAgICAgOiBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiRXJyIFwiLCBlcnIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgdmFsaWRhdGVTY2hkdWxlRGF0YShjYWxsYmFjaykge1xyXG4gICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgbGV0IHRlbXBEYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcbiAgICBpZiAodGVtcERhdGEgJiYgdGVtcERhdGEuc2VsZWN0ZWREYXRhICYmIHRlbXBEYXRhLnNlbGVjdGVkRGF0YS5sZW5ndGgpIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGEgPSB7IC4uLnRoaXMuaW5wdXREYXRhLCAuLi50ZW1wRGF0YSB9O1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcy5mb3JtdXNlZEZvckNoZWNrKSB7XHJcbiAgICB9XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImNoaXBMaW1pdFwiXSA9IHRoaXMuQ2hpcExpbWl0O1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJjaGlwT3BlcmF0b3JcIl0gPSB0aGlzLkNoaXBPcGVyYXRvcjtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIiBTY2hlZHVsZSBpbnB1dERhdGFpbnB1dERhdGFcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmlucHV0R3JvdXAsIFwiLi4uLi4uLiB0aGlzLmlucHV0R3JvdXAgXCIpO1xyXG4gICAgLy8gY29uc29sZS5sb2codGhpcy5pbnB1dEdyb3VwLmdldChcInNjaGVkdWxlRmllbGRcIikgLCBcIi0tIHNjaGVkdWxlRmllbGRcIik7XHJcbiAgICBsZXQgZGF0ZUZpZWxkID0gdGhpcy5pbnB1dEdyb3VwLmdldChcInN0YXJ0RGF0ZVwiKS52YWx1ZTtcclxuICAgIC8vIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+Pj4gZGF0ZUZpZWxkIFwiLGRhdGVGaWVsZCk7XHJcbiAgICAvLyAgaWYoZGF0ZUZpZWxkPT11bmRlZmluZWQpe1xyXG4gICAgLy8gICB0aGlzLmRhdGVGb3JtYXRjaGVjayA9IGZhbHNlO1xyXG4gICAgLy8gICB0aGlzLmlucHV0R3JvdXAuZ2V0KFwibWVyaWRpYW5cIikuaW52YWxpZDtcclxuICAgIC8vICB9XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU2VsZWN0ZWRWYWwpIHtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXS5sZW5ndGhcclxuICAgICAgKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBsZXQgaXRlbSA9IHRoaXMuZm9ybVZhbHVlcy5zY2hlZHVsZUZpZWxkcztcclxuICAgICAgICBPYmplY3Qua2V5cyhpdGVtKS5mb3JFYWNoKGZ1bmN0aW9uICh2YWx1ZSkge1xyXG4gICAgICAgICAgLy8gdGVtcE9ialt2YWx1ZV0gPSBuZXcgRm9ybUNvbnRyb2woW1wiXCJdKTtcclxuICAgICAgICAgIC8vIGxldCB2YWw9dGhpcy5pbnB1dEdyb3VwLnZhbHVlW3ZhbHVlXTtcclxuICAgICAgICAgIC8vICBpZighdmFsKVxyXG4gICAgICAgICAgLy8gIHtcclxuICAgICAgICAgIC8vICAgdGhpcy5pbnB1dEdyb3VwLmNvbnRyb2xzKGl0ZW0pLnNldEVycm9ycyh7J2luY29ycmVjdCc6IHRydWV9KTtcclxuICAgICAgICAgIC8vICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSB0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbnMudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQodG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvcilcclxuICAgICAgICApO1xyXG4gICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgICAgaWYgKHRoaXMuaW5wdXRHcm91cCAmJiB0aGlzLmlucHV0R3JvdXAudmFsaWQpIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNvbnN0cnVjdFNjaGVkdWxlRGF0YShyZXF1ZXN0RGV0YWlscywgY2FsbGJhY2spIHtcclxuICAgIGxldCBkYXRlID0gbW9tZW50KHRoaXMuaW5wdXREYXRhLnN0YXJ0RGF0ZSkuZm9ybWF0KFwiREQtTU0tWVlZWVwiKTtcclxuICAgIGxldCBkYXRlQ29uY2F0ID0gbW9tZW50KFxyXG4gICAgICBkYXRlICsgXCIgXCIgKyB0aGlzLmlucHV0RGF0YS50aW1lICsgXCIgXCIgKyB0aGlzLmlucHV0RGF0YS5tZXJpZGlhbixcclxuICAgICAgXCJERC1NTS1ZWVlZIEhIOm1tIGFcIlxyXG4gICAgKS5mb3JtYXQoKTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wiZGF0ZVRpbWVcIl0gPSBkYXRlQ29uY2F0O1xyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJ0aW1lXCJdID0gZGF0ZUNvbmNhdDtcclxuICAgIHRoaXMuaW5wdXREYXRhW1widXNlclJlY2lwaWVudHNcIl0gPSB0aGlzLnJlY2lwaWVudDtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXlzXCJdID0gW107XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImRhdGVcIl0gPSBtb21lbnQoKS5mb3JtYXQoXCJERC1NTU0tWVlZWVwiKTtcclxuICAgIGxldCBjdXJyZW50SW5wdXREYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKSk7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLmN1cnJlbnRJbnB1dERhdGEgfTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0geyAuLi50aGlzLmlucHV0RGF0YSwgLi4udGhpcy5pbnB1dEdyb3VwLnZhbHVlIH07XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICBsZXQgcmVxdWVzdERhdGEgPSB7fTtcclxuICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgbGV0IHRlbXBEYXRhO1xyXG4gICAgICBpZiAoXHJcbiAgICAgICAgaXRlbS5zdWJLZXkgJiZcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXSAmJlxyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICApIHtcclxuICAgICAgICB0ZW1wRGF0YSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XTtcclxuICAgICAgfSBlbHNlIGlmICghaXRlbS5zdWJLZXkpIHtcclxuICAgICAgICBpZiAoaXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgIHRlbXBEYXRhID0gaXRlbS52YWx1ZTtcclxuICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uQXNzaWduRmlyc3RJbmRleHZhbCkge1xyXG4gICAgICAgICAgdGVtcERhdGEgPSBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVswXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gZWxzZSBpZihpdGVtLmZyb21Gb3JtQ29udHJvbCl7XHJcbiAgICAgICAgLy8gICB0ZW1wRGF0YSA9IHNlbGYuaW5wdXRHcm91cC52YWx1ZVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgIHRlbXBEYXRhID0gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIGlmICh0ZW1wRGF0YSkge1xyXG4gICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgPyBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSlcclxuICAgICAgICAgIDogdGVtcERhdGE7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgY2FsbGJhY2socmVxdWVzdERhdGEpO1xyXG4gIH1cclxuICBzaG93UmVwZWF0RGF0YSh2YWwpIHtcclxuICAgIGlmICh2YWwuY2hlY2tlZCkge1xyXG4gICAgICB0aGlzLmVuYWJsZVJlcGVhdERhdGEgPSB0cnVlO1xyXG4gICAgICB0aGlzLmlucHV0R3JvdXAuZ2V0KFwicmVwZWF0VHlwZVwiKS5zZXRWYWx1ZShcImRhaWx5XCIpO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtcImRhaWx5XCJdID0gdHJ1ZTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZW5hYmxlUmVwZWF0RGF0YSA9IGZhbHNlO1xyXG4gICAgICB0aGlzLmlucHV0R3JvdXAuZ2V0KFwicmVwZWF0VHlwZVwiKS5zZXRWYWx1ZShcIlwiKTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJkYWlseVwiXSA9IGZhbHNlO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZ2V0U2VsZWN0ZWRkYXlzKCRldmVudCwgZWxlbWVudCkge1xyXG4gICAgdmFyIGluZGV4ID0gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERheXNcIl0uaW5kZXhPZihlbGVtZW50KTtcclxuICAgIGlmIChpbmRleCA9PT0gLTEpIHtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERheXNcIl0ucHVzaChlbGVtZW50KTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXlzXCJdLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9uUmVwZWF0U2VsZWN0Q2hhbmdlKHZhbHVlLCBkYXRhKSB7XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICBfLmZvckVhY2goZGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKGl0ZW0udmFsdWUgPT0gdmFsdWUpIHtcclxuICAgICAgICBzZWxmLmlucHV0RGF0YVt2YWx1ZV0gPSB0cnVlO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBvbkV4cG9ydENsaWNrKGJ1dHRvbkRhdGEsIHJlcXVlc3QpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+Pj4+b25FeHBvcnRDbGljayBcIiwgcmVxdWVzdCk7XHJcbiAgICBsZXQgZXhwb3J0UmVxdWVzdCwgcmVxdWVzdERldGFpbHMsIGFwaVVybDtcclxuICAgIGxldCByZXF1ZXN0RGF0YSA9IHt9LFxyXG4gICAgICByZXBvcnRUZW1wO1xyXG4gICAgbGV0IGR5bmFtaWNSZXBvcnQgPSByZXF1ZXN0LmlzZHlhbWljRGF0YSA/IHJlcXVlc3QuaXNkeWFtaWNEYXRhIDogZmFsc2U7XHJcbiAgICBpZiAoZHluYW1pY1JlcG9ydCkge1xyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5leHBvcnREYXRhO1xyXG4gICAgICBleHBvcnRSZXF1ZXN0ID0gdGhpcy5jdXJyZW50RGF0YS5leHBvcnRSZXF1ZXN0O1xyXG4gICAgICBsZXQgUmVwb3J0RGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJDdXJyZW50UmVwb3J0RGF0YVwiKSk7XHJcbiAgICAgIGFwaVVybCA9IGV4cG9ydFJlcXVlc3QuYXBpVXJsO1xyXG4gICAgICByZXBvcnRUZW1wID0geyAuLi5SZXBvcnREYXRhLCAuLi5idXR0b25EYXRhIH07XHJcbiAgICAgIHJlcXVlc3REZXRhaWxzID0gZXhwb3J0UmVxdWVzdC5SZXF1ZXN0RGF0YTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGxldCBmb3JtUmVxdWVzdCA9IHJlcXVlc3QuZXhwb3J0UmVxdWVzdDtcclxuICAgICAgZXhwb3J0UmVxdWVzdCA9IGZvcm1SZXF1ZXN0XHJcbiAgICAgICAgPyBmb3JtUmVxdWVzdFxyXG4gICAgICAgIDogdGhpcy5mb3JtVmFsdWVzLmV4cG9ydERhdGEuZXhwb3J0UmVxdWVzdDtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJleHBvcnRUeXBlXCJdID0gYnV0dG9uRGF0YS5leHBvcnRUeXBlO1xyXG4gICAgICByZXF1ZXN0RGV0YWlscyA9IGV4cG9ydFJlcXVlc3QucmVxdWVzdERhdGE7XHJcbiAgICAgIGFwaVVybCA9IGV4cG9ydFJlcXVlc3QuYXBpVXJsO1xyXG4gICAgfVxyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICBsZXQgdGVtcERhdGE7XHJcbiAgICAgIGlmIChcclxuICAgICAgICBpdGVtLnN1YktleSAmJlxyXG4gICAgICAgIHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdICYmXHJcbiAgICAgICAgc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICkge1xyXG4gICAgICAgIGlmIChpdGVtLmNvbnZlcnRTdHJpbmcpIHtcclxuICAgICAgICAgIGxldCB0ZW1wID0gW107XHJcbiAgICAgICAgICBfLmZvckVhY2goc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldLCBmdW5jdGlvbiAoXHJcbiAgICAgICAgICAgIGRhdGFJdGVtXHJcbiAgICAgICAgICApIHtcclxuICAgICAgICAgICAgdGVtcC5wdXNoKGRhdGFJdGVtKTtcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGVtcERhdGEgPSBKU09OLnN0cmluZ2lmeSh0ZW1wKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGVtcERhdGEgPSBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXVtpdGVtLnN1YktleV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKCFpdGVtLnN1YktleSkge1xyXG4gICAgICAgIGlmIChpdGVtLnJlcG9ydENoZWNrKSB7XHJcbiAgICAgICAgICB2YXIgdDEgPSBpdGVtLmZyb21UcmFuc2xhdGlvblxyXG4gICAgICAgICAgICA/IHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChpdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICA6IGl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IGl0ZW0uY29uZGl0aW9uQ2hlY2sgPyByZXBvcnRUZW1wW2l0ZW0udmFsdWVdIDogdDE7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmZyb21Mb2dpbkRhdGEpIHtcclxuICAgICAgICAgIHRlbXBEYXRhID0gc2VsZi51c2VyRGF0YSA/IHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV0gOiBcIlwiO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0ZW1wRGF0YSA9IGl0ZW0uaXNEZWZhdWx0ID8gaXRlbS52YWx1ZSA6IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICBpZiAoc2VsZi5mb3JtVmFsdWVzLmdldFZhbHVlRnJvbUNvbmZpZyAmJiBzZWxmLmZvcm1WYWx1ZXNbaXRlbS52YWx1ZV0pIHtcclxuICAgICAgICB0ZW1wRGF0YSA9IHNlbGYuZm9ybVZhbHVlc1tpdGVtLnZhbHVlXTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGVtcERhdGEpXHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0uY29udmVydFRvU3RyaW5nXHJcbiAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHRlbXBEYXRhKVxyXG4gICAgICAgICAgOiB0ZW1wRGF0YTtcclxuICAgIH0pO1xyXG4gICAgaWYgKGV4cG9ydFJlcXVlc3QuaXNSZXBvcnRDcmVhdGUpIHtcclxuICAgICAgLy8gZm9ybSBiYXNlZCByZXBvcnQgY3JlYXRlXHJcbiAgICAgIC8vIEV4cG9ydCByZXBvcnRcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB0aGlzLnN1Ym1pdHRlZCA9IHRydWU7XHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj5Gb3JtIHJlcG9ydCByZXF1ZXN0RGF0YSBcIiwgcmVxdWVzdERhdGEpO1xyXG4gICAgICB0aGlzLnZhbGlkYXRpb25DaGVjayhmdW5jdGlvbiAocmVzdWx0KSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+PiByZXN1bHQgXCIsIHJlc3VsdCk7XHJcbiAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBleHBvcnRSZXF1ZXN0LnRvYXN0TWVzc2FnZTtcclxuICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVJlcXVlc3QocmVxdWVzdERhdGEsIGFwaVVybCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDApIHtcclxuICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcIkN1cnJlbnRSZXBvcnREYXRhXCIpO1xyXG4gICAgICAgICAgICAgIHNlbGYubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgc2VsZi5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zdWJtaXR0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgIC5nZXRFeHBvcnRSZXNwb25zZShyZXF1ZXN0RGF0YSwgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmV4cG9ydERhdGEgJiZcclxuICAgICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmV4cG9ydERhdGEuczNEb3dubG9hZFxyXG4gICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gZXhwb3J0UmVxdWVzdC50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgZmlsZU5hbWUgPSB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgZXhwb3J0UmVxdWVzdC5kb3dubG9hZEZpbGVOYW1lXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIHRoaXMuZG93bmxvYWRFeHBvcnRGaWxlKFxyXG4gICAgICAgICAgICAgIGRhdGEsXHJcbiAgICAgICAgICAgICAgZmlsZU5hbWUgKyBidXR0b25EYXRhLmZpbGVUeXBlLFxyXG4gICAgICAgICAgICAgIGJ1dHRvbkRhdGEuc2VsZWN0aW9uVHlwZVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgZG93bmxvYWRFeHBvcnRGaWxlKHJlcG9ydFZhbHVlLCBmaWxlTmFtZSwgc2VsZWN0aW9uVHlwZSkge1xyXG4gICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtyZXBvcnRWYWx1ZV0sIHsgdHlwZTogc2VsZWN0aW9uVHlwZSB9KTtcclxuICAgIEZpbGVTYXZlci5zYXZlQXMoYmxvYiwgZmlsZU5hbWUpO1xyXG4gIH1cclxuXHJcbiAgb25TZWxlY3Rpb25DaGFuZ2VkKGV2ZW50OiBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50LCBpZCkge1xyXG4gICAgdGhpcy5zZWxlY3RlZENoaXBbaWRdID1cclxuICAgICAgdGhpcy5zZWxlY3RlZENoaXBbaWRdICYmIHRoaXMuc2VsZWN0ZWRDaGlwW2lkXS5sZW5ndGhcclxuICAgICAgICA/IHRoaXMuc2VsZWN0ZWRDaGlwW2lkXVxyXG4gICAgICAgIDogW107XHJcbiAgICBsZXQgaW5kZXggPSB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0uaW5kZXhPZihldmVudC5vcHRpb24udmFsdWUpO1xyXG4gICAgaWYgKGluZGV4IDwgMCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkQ2hpcFtpZF0ucHVzaChldmVudC5vcHRpb24udmFsdWUpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5hY2Nlc3NJbnB1dC5uYXRpdmVFbGVtZW50LnZhbHVlID0gXCJcIjtcclxuICB9XHJcblxyXG4gIG9uU2VhcmNoQWNjZXNzQ29udHJvbChldmVudDogTWF0Q2hpcElucHV0RXZlbnQpIHtcclxuICAgIGNvbnN0IHZhbHVlID0gZXZlbnQudmFsdWU7XHJcbiAgICBpZiAoKHZhbHVlIHx8IFwiXCIpLnRyaW0oKSkge1xyXG4gICAgICB0aGlzLmNoaXBMaXN0RmllbGRzLmZvckVhY2goKGl0ZW0pID0+IHtcclxuICAgICAgICBpdGVtLmRhdGEucHVzaCh2YWx1ZS50cmltKCkpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICAgIHRoaXMuYWNjZXNzSW5wdXQubmF0aXZlRWxlbWVudC52YWx1ZSA9IFwiXCI7XHJcbiAgfVxyXG4gIEV4cG9ydEJ1dHRvbnModmFsdWUpIHtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIGxldCBhcGlVcmw7XHJcbiAgICBsZXQgaW5wdXRGaWVsZE9iaiA9IHt9O1xyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge307XHJcbiAgICBsZXQgcmVxdWVzdE9iaiA9IHt9O1xyXG4gICAgaWYgKHNlbGYuZm9ybURhdGEuYnV0dG9ucykge1xyXG4gICAgICBfLmZvckVhY2goc2VsZi5mb3JtRGF0YS5idXR0b25zLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIGFwaVVybCA9IGl0ZW0ub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICAgIE9iamVjdC5rZXlzKHNlbGYuc2VsZWN0ZWRDaGlwKS5mb3JFYWNoKChrZXlzKSA9PiB7XHJcbiAgICAgICAgICBpbnB1dEZpZWxkT2JqW2tleXNdID0gSlNPTi5zdHJpbmdpZnkoc2VsZi5zZWxlY3RlZENoaXBba2V5c10pO1xyXG4gICAgICAgICAgcXVlcnlPYmogPSB7XHJcbiAgICAgICAgICAgIGRhdGFzb3VyY2U6IHNlbGYuZGF0YUlkLFxyXG4gICAgICAgICAgICByZXBvcnRfdHlwZTogdmFsdWUsXHJcbiAgICAgICAgICAgIHR5cGU6IFwiU09EXCIsXHJcbiAgICAgICAgICB9O1xyXG4gICAgICAgIH0pO1xyXG4gICAgICB9KTtcclxuICAgICAgcmVxdWVzdE9iaiA9IHsgLi4uaW5wdXRGaWVsZE9iaiwgLi4ucXVlcnlPYmogfTtcclxuICAgICAgc2VsZi5jb250ZW50U2VydmljZS5nZXRFeHBvcnRSZXNwb25zZShhcGlVcmwsIHJlcXVlc3RPYmopLnN1YnNjcmliZShcclxuICAgICAgICAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5kb3dubG9hZEZpbGUoZGF0YS5ib2R5LCB2YWx1ZSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKGVycm9yKTtcclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGRvd25sb2FkRmlsZShyZXNwb25zZSwgdmFsdWUpIHtcclxuICAgIHRoaXMucnVuUmVwb3J0ID0gdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICB0aGlzLmZvcm1EYXRhLmhlYWRlclxyXG4gICAgKTtcclxuICAgIGlmICh2YWx1ZSA9PT0gXCJDU1ZcIikge1xyXG4gICAgICBjb25zdCBibG9iID0gbmV3IEJsb2IoW3Jlc3BvbnNlXSwgeyB0eXBlOiBcInRleHQvY3N2XCIgfSk7XHJcbiAgICAgIEZpbGVTYXZlci5zYXZlQXMoYmxvYiwgdGhpcy5ydW5SZXBvcnQgKyBcIi5jc3ZcIik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBjb25zdCBibG9iID0gbmV3IEJsb2IoW3Jlc3BvbnNlXSwgeyB0eXBlOiBcInRleHQveGxzeFwiIH0pO1xyXG4gICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIHRoaXMucnVuUmVwb3J0ICsgXCIueGxzeFwiKTtcclxuICAgIH1cclxuICB9XHJcbiAgcmVjZWl2ZUNsaWNrKGV2ZW50KSB7XHJcbiAgICB0aGlzLmVuYWJsZVNlbGVjdFZhbHVlVmlldyA9IGZhbHNlO1xyXG4gICAgbGV0IHRlbXBEYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcERhdGEpID8gSlNPTi5wYXJzZSh0ZW1wRGF0YSkgOiB7fTtcclxuICAgIHRoaXMuc2VsZWN0ZWRUYWJsZVZpZXcgPSB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzXHJcbiAgICAgID8gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy50YWJsZURhdGFcclxuICAgICAgOiBbXTtcclxuICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkID0gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcyB8fCB7fTtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzICYmXHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMgJiZcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy5pbnB1dEtleVxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMuc2VsZWN0VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSB0aGlzLmlucHV0RGF0YVtcclxuICAgICAgICB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzLmlucHV0S2V5XHJcbiAgICAgIF07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnNlbGVjdFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhO1xyXG4gICAgfVxyXG4gICAgaWYgKHRoaXMuc2VsZWN0ZWRUYWJsZVZpZXcgJiYgdGhpcy5zZWxlY3RlZFRhYmxlVmlldy5sZW5ndGgpIHtcclxuICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgdGhpcy5lbmFibGVTZWxlY3RWYWx1ZVZpZXcgPSB0cnVlO1xyXG4gICAgICB9LCAxMDApO1xyXG4gICAgfVxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kQnV0dG9uRW5hYmxlTWVzc2FnZShcImRhdGFcIik7XHJcbiAgfVxyXG5cclxuICBkZWxldGVDbGljayhldmVudCl7XHJcbiAgICB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gZmFsc2U7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCxcIj4+Pj4+RVZFTlRcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLFwiPj4+IFRISVNcIik7XHJcbiAgICBzZXRUaW1lb3V0KCgpPT57XHJcbiAgICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgfSwgMjAwKVxyXG4gIH1cclxuXHJcbiAgY2hhbmdlVGFiKGV2ZW50KSB7XHJcbiAgICBldmVudCA9IGV2ZW50ICYmIGV2ZW50LmluZGV4ID8gZXZlbnQuaW5kZXggOiAwO1xyXG4gICAgbGV0IGN1cnJlbnRUYWJUYWJsZSA9IHRoaXMuZm9ybVZhbHVlcy50YWJEYXRhW2V2ZW50XS5sb2FkVGFibGU7XHJcbiAgICB0aGlzLnRhYmxlTGlzdCA9IGN1cnJlbnRUYWJUYWJsZS50YWJsZURhdGEgPyBjdXJyZW50VGFiVGFibGUudGFibGVEYXRhIDogW107XHJcbiAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWQgPSBjdXJyZW50VGFiVGFibGU7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiAgdGhpcy5jdXJyZW50VGFibGVMb2FkIFwiLCB0aGlzLmN1cnJlbnRUYWJsZUxvYWQpO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcy5mcm9tTG9jYWxTdG9yYWdlICYmIHRoaXMuZm9ybVZhbHVlcy5hc3NpZ25GaXJzdFRhYmxlKSB7XHJcbiAgICAgIC8vIGFkZGVkIGZvciBydWxlc2V0IGVycm9yIGxvZ1xyXG4gICAgICBsZXQgcmVzcG9uc2VLZXkgPSB0aGlzLnRhYmxlTGlzdFswXS5yZXNwb25zZUtleTtcclxuICAgICAgbGV0IHRhYmxlSWQgPSB0aGlzLnRhYmxlTGlzdFswXS50YWJsZUlkO1xyXG4gICAgICBsZXQgc3ViS2V5ID0gdGhpcy50YWJsZUxpc3RbMF0uc3ViUmVzcG9uc2VLZXk7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtyZXNwb25zZUtleV0gJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtyZXNwb25zZUtleV1bdGFibGVJZF0gJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtyZXNwb25zZUtleV1bdGFibGVJZF1bc3ViS2V5XVxyXG4gICAgICApIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZUxvYWRbXCJyZXNwb25zZVwiXSA9IHRoaXMuaW5wdXREYXRhW3Jlc3BvbnNlS2V5XVtcclxuICAgICAgICAgIHRhYmxlSWRcclxuICAgICAgICBdW3N1YktleV1cclxuICAgICAgICAgID8gdGhpcy5pbnB1dERhdGFbcmVzcG9uc2VLZXldW3RhYmxlSWRdW3N1YktleV1cclxuICAgICAgICAgIDogW107XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMudGFibGVMaXN0WzBdLmVuYWJsZUVycm9yQ291bnQgJiYgdGhpcy50YWJsZUxpc3RbMF0uY291bnRLZXkpIHtcclxuICAgICAgICAvLyBlcnJvclNob3cga2V5IGFuZCB2YWx1ZVxyXG4gICAgICAgIHRoaXMuY3VycmVudFRhYmxlTG9hZFt0aGlzLnRhYmxlTGlzdFswXS5jb3VudEtleV0gPSB0aGlzLmlucHV0RGF0YVtcclxuICAgICAgICAgIHJlc3BvbnNlS2V5XHJcbiAgICAgICAgXVt0YWJsZUlkXTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIHtcclxuICAgIH1cclxuICAgIHRoaXMuZW5hYmxlVGFibGVMYXlvdXQgPSB0cnVlO1xyXG4gICAgdGhpcy5lbmFibGVOZXdUYWJsZUxheW91dCA9IHRoaXMuZm9ybVZhbHVlcy5lbmFibGVOZXdUYWJsZUxheW91dFxyXG4gICAgICA/IHRydWVcclxuICAgICAgOiBmYWxzZTtcclxuICB9XHJcblxyXG4gIG5hdmlnYXRlTmV4dCgpIHtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuQ2hpcExpbWl0LCBcIjo6OjpcIik7XHJcbiAgICBsZXQgdGVtcCA9IHRoaXMuaXNOYW1lQXZhaWxhYmxlO1xyXG4gICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcy5nZXRGb3JtQ29udHJvbFZhbHVlKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy52YWx1ZVRvU2F2ZV0gPSB0aGlzLmlucHV0R3JvdXAudmFsdWVbXHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmZvcm1GaWVsZE5hbWVcclxuICAgICAgXTtcclxuICAgIH1cclxuICAgIGNvbnNvbGUubG9nKHRoaXMuaW5wdXREYXRhLCBcIi4uLi4uLklOUFVUIERBVEFcIik7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpXHJcbiAgICB0aGlzLnZhbGlkYXRpb25DaGVjayhmdW5jdGlvbiAocmVzdWx0KSB7XHJcbiAgICAgIGlmIChyZXN1bHQgJiYgdGVtcCkge1xyXG4gICAgICAgIHNlbGYuc3RlcHBlclZhbC5uZXh0KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICB2YWxpZGF0aW9uQ2hlY2soY2FsbGJhY2spIHtcclxuICAgIGxldCB0ZW1wRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIikpO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gdmFsYWlkYXRpb25jaGVjayB0ZW1wRGF0YSBcIiwgdGVtcERhdGEpO1xyXG5cclxuICAgIGlmICh0ZW1wRGF0YSAmJiB0ZW1wRGF0YS5zZWxlY3RlZERhdGEgJiYgdGVtcERhdGEuc2VsZWN0ZWREYXRhLmxlbmd0aCkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLnRlbXBEYXRhIH07XHJcbiAgICB9XHJcbiAgICBpZiAoXHJcbiAgICAgIHRlbXBEYXRhICYmXHJcbiAgICAgIHRlbXBEYXRhW3RoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVNlbGVjdEtleV0gJiZcclxuICAgICAgdGVtcERhdGFbdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU2VsZWN0S2V5XS5sZW5ndGhcclxuICAgICkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YSA9IHsgLi4udGhpcy5pbnB1dERhdGEsIC4uLnRlbXBEYXRhIH07XHJcbiAgICB9XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzLmZvcm11c2VkRm9yQ2hlY2spIHtcclxuICAgIH1cclxuICAgIHRoaXMuaW5wdXREYXRhW1wiY2hpcExpbWl0XCJdID0gdGhpcy5DaGlwTGltaXQ7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcImNoaXBPcGVyYXRvclwiXSA9IHRoaXMuQ2hpcE9wZXJhdG9yO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiaW5wdXREYXRhaW5wdXREYXRhXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiVEVTVFRUXCIpO1xyXG4gICAgY29uc29sZS5sb2coXCJ0aGlzLmZvcm1WYWx1ZXMgPj4+PlwiLCB0aGlzLmZvcm1WYWx1ZXMpO1xyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVNlbGVjdGVkVmFsKSB7XHJcbiAgICAgIGlmIChcclxuICAgICAgICB0aGlzLmlucHV0RGF0YSAmJlxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy52YWxpZGF0ZVNlbGVjdEtleV0gJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVt0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGVTZWxlY3RLZXldLmxlbmd0aFxyXG4gICAgICApIHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gJiZcclxuICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXS5sZW5ndGggJiZcclxuICAgICAgICAhdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU2VsZWN0S2V5XHJcbiAgICAgICkge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSB0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbnMudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQodG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvcilcclxuICAgICAgICApO1xyXG4gICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIGlmIChcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlVmFsdWVTdGF0dXMgJiZcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU3ViS2V5XHJcbiAgICApIHtcclxuICAgICAgLy8gYWRkZWQgV2hlbiBNYW5hZ2Ugcm9sZSAtIGFjY2VzcyBjaGVja1xyXG4gICAgICB2YXIgY2hlY2tWYWwgPSBfLmZpbmQoXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRlU3ViS2V5XSxcclxuICAgICAgICBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgcmV0dXJuIGl0ZW0uc3RhdHVzID09IFwiQUNUSVZFXCI7XHJcbiAgICAgICAgfVxyXG4gICAgICApO1xyXG4gICAgICBpZiAoY2hlY2tWYWwpIHtcclxuICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9ucy50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudCh0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yKVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdGhpcy5jaGlwVmFsaWRpdGFpb24gPSB0cnVlO1xyXG4gICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgfVxyXG4gICAgfSBlbHNlIGlmICh0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGVTZWxlY3RlZENoaXApIHtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMuaW5wdXREYXRhICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdW1xyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRpb25DaGlwSWRcclxuICAgICAgICBdICYmXHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZENoaXBLZXlMaXN0XCJdW3RoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9uQ2hpcElkXVxyXG4gICAgICAgICAgLmxlbmd0aFxyXG4gICAgICApIHtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YS5jb250cm9sVHlwZSA9PSBcIlNPRFwiICYmXHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkQ2hpcEtleUxpc3RcIl1bXHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9uQ2hpcElkXHJcbiAgICAgICAgICBdLmxlbmd0aCA+PSAyXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgY2FsbGJhY2sodHJ1ZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhLmNvbnRyb2xUeXBlID09IFwiU2Vuc2l0aXZlXCIgJiZcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRDaGlwS2V5TGlzdFwiXVtcclxuICAgICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRpb25DaGlwSWRcclxuICAgICAgICAgIF0ubGVuZ3RoID49IDFcclxuICAgICAgICApIHtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgICB9IGVsc2UgaWYgKCF0aGlzLmlucHV0RGF0YS5jb250cm9sVHlwZSkge1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IHRoaXMuZm9ybVZhbHVlcy52YWxpZGF0aW9ucy50b2FzdE1lc3NhZ2U7XHJcbiAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5jaGlwVmFsaWRpdGFpb24gPSB0cnVlO1xyXG4gICAgICAgICAgY2FsbGJhY2soZmFsc2UpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICAvLyBjb25zb2xlLmxvZyhcInRoaXMuc2VsZWN0ZWRDaGlwcy5sZW5ndGggXCIsdGhpcy5zZWxlY3RlZENoaXBzKVxyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKFwidGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMgXCIsdGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMpXHJcbiAgICAgICAgaWYgKF8uaXNFbXB0eSh0aGlzLnNlbGVjdGVkQ2hpcCkpIHtcclxuICAgICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gdGhpcy5mb3JtVmFsdWVzLnZhbGlkYXRpb25zLnRvYXN0TWVzc2FnZTtcclxuICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLmNoaXBWYWxpZGl0YWlvbiA9IHRydWU7XHJcbiAgICAgICAgICBjYWxsYmFjayhmYWxzZSk7XHJcbiAgICAgICAgfSBlbHNlIGlmICghXy5pc0VtcHR5KHRoaXMuc2VsZWN0ZWRDaGlwKSkge1xyXG4gICAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICAgICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5yZXF1aXJlZENoaXBzLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAvLyBjb25zb2xlLmxvZyhcIml0ZW0gPj4+Pj4+Pj5cIixpdGVtKVxyXG4gICAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICAgc2VsZi5zZWxlY3RlZENoaXBbaXRlbV0gJiZcclxuICAgICAgICAgICAgICBzZWxmLnNlbGVjdGVkQ2hpcFtpdGVtXS5sZW5ndGggPD0gMFxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICBzZWxmLmNoaXBWYWxpZGl0YWlvbiA9IHRydWU7XHJcbiAgICAgICAgICAgICAgY2FsbGJhY2soZmFsc2UpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwW2l0ZW1dICYmXHJcbiAgICAgICAgICAgICAgc2VsZi5zZWxlY3RlZENoaXBbaXRlbV0ubGVuZ3RoID4gMFxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICBzZWxmLmNoaXBWYWxpZGl0YWlvbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIGNhbGxiYWNrKHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSB0aGlzLmZvcm1WYWx1ZXMudmFsaWRhdGlvbnMudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIGNhbGxiYWNrKGZhbHNlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLmlucHV0R3JvdXAgJiYgdGhpcy5pbnB1dEdyb3VwLnZhbGlkKSB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICBjYWxsYmFjayh0cnVlKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuICBuYXZpZ2F0ZVByZXZpb3VzKCkge1xyXG4gICAgdGhpcy5zdGVwcGVyVmFsLnByZXZpb3VzKCk7XHJcbiAgfVxyXG5cclxuICBzaG93RGF0YSh0ZW1wRGF0YSkge1xyXG4gICAgY29uc29sZS5sb2codGVtcERhdGEsIFwiRERERFwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZm9ybVZhbHVlcywgXCI+Pj4+Pj5mb3JtVmFsdWVzXCIpO1xyXG4gICAgdGhpcy5zZWxlY3RlZENoaXBzID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWRDaGlwcztcclxuICAgIHRoaXMuc2VsZWN0ZWRDaGlwS2V5TGlzdCA9IHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkQ2hpcEtleUxpc3Q7XHJcbiAgICAvLyBkZWJ1Z2dlcjtcclxuICAgIGlmICh0aGlzLmZvcm1WYWx1ZXMgJiYgdGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIGZ1bmN0aW9uICh4KSB7XHJcbiAgICAgICAgaWYgKHggJiYgeC5jaGlwQ29uZGl0aW9uKSB7XHJcbiAgICAgICAgICBsZXQgdGVtcCA9IHguY2hpcENvbmRpdGlvbi52YXJpYWJsZTtcclxuICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgeC5jaGlwQ29uZGl0aW9uLmRhdGEgJiZcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGEgJiZcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbdGVtcF0gJiZcclxuICAgICAgICAgICAgeC5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dXHJcbiAgICAgICAgICApIHtcclxuICAgICAgICAgICAgc2VsZi5DaGlwTGltaXQgPVxyXG4gICAgICAgICAgICAgIHguY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcImxpbWl0XCJdO1xyXG4gICAgICAgICAgICBzZWxmLkNoaXBPcGVyYXRvciA9XHJcbiAgICAgICAgICAgICAgeC5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dW1wib3BlcmF0b3JcIl07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh4Lm9uTG9hZEZ1bmN0aW9uKSB7XHJcbiAgICAgICAgICAvLyBjaGlwZmllbGRzIE9uTG9hZCBmdW5jdGlvbiBhZGRlZCB3aGVuIGltcGxlbWVudCBydW4gcmVwb3J0c1xyXG4gICAgICAgICAgbGV0IGFwaVVybCA9IHgub25Mb2FkRnVuY3Rpb24uYXBpVXJsO1xyXG4gICAgICAgICAgbGV0IHJlc3BvbnNlTmFtZSA9IHgub25Mb2FkRnVuY3Rpb24ucmVzcG9uc2U7XHJcbiAgICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSB4Lm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5pbnB1dERhdGFbcmVxdWVzdEl0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUoKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgeC5kYXRhID0gZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0aGlzLnZpZXdEYXRhID0gdGVtcERhdGE7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnZpZXdEYXRhLCBcIi4uLi4uLlZJRVcgREFUQVwiKTtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzICYmXHJcbiAgICAgICh0aGlzLmZvcm1WYWx1ZXMuY29uZmlybURhdGEgfHwgdGhpcy5mb3JtVmFsdWVzLnNob3dTZWxlY3RlZFZhbHVlcylcclxuICAgICkge1xyXG4gICAgICB0aGlzLmVuYWJsZVNlbGVjdFZhbHVlVmlldyA9IGZhbHNlO1xyXG4gICAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy52aWV3U2VsZWN0ZWRWYWx1ZXMpIHtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkVGFibGVWaWV3ID0gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy50YWJsZURhdGFcclxuICAgICAgICAgID8gdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy50YWJsZURhdGFcclxuICAgICAgICAgIDogW107XHJcbiAgICAgICAgdGhpcy5zZWxlY3RUYWJsZUxvYWQgPSB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzO1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcyAmJlxyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcyAmJlxyXG4gICAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnZpZXdTZWxlY3RlZFZhbHVlcy5pbnB1dEtleVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RUYWJsZUxvYWRbXCJyZXNwb25zZVwiXSA9IHRoaXMuaW5wdXREYXRhW1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1WYWx1ZXMudmlld1NlbGVjdGVkVmFsdWVzLmlucHV0S2V5XHJcbiAgICAgICAgICBdO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdFRhYmxlTG9hZFtcInJlc3BvbnNlXCJdID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgIH1cclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgIHRoaXMuZW5hYmxlU2VsZWN0VmFsdWVWaWV3ID0gdHJ1ZTtcclxuICAgICAgICB9LCAxMDApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLCBmdW5jdGlvbiAoXHJcbiAgICAgICAgYXV0b0NvbXBsZXRlSXRlbVxyXG4gICAgICApIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhhdXRvQ29tcGxldGVJdGVtLCBcIj4+Pj4+PmF1dG9Db21wbGV0ZUl0ZW1cIik7XHJcbiAgICAgICAgaWYgKGF1dG9Db21wbGV0ZUl0ZW0udmFsdWVGcm9tTG9jYWwpIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFxyXG4gICAgICAgICAgICBhdXRvQ29tcGxldGVJdGVtLmRhdGFLZXlUb1NhdmUsXHJcbiAgICAgICAgICAgIFwiPj4+YXV0b0NvbXBsZXRlSXRlbS5kYXRhS2V5VG9TYXZlXCJcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICBhdXRvQ29tcGxldGVJdGVtW2F1dG9Db21wbGV0ZUl0ZW0uZGF0YUtleVRvU2F2ZV0gPVxyXG4gICAgICAgICAgICB0ZW1wRGF0YVthdXRvQ29tcGxldGVJdGVtLmlucHV0S2V5XTtcclxuICAgICAgICAgIC8vICAgICAgIC8vIGF1dG9Db21wbGV0ZUl0ZW1bYXV0b0NvbXBsZXRlSXRlbS5kYXRhS2V5VG9TYXZlXSA9IF8ubWFwKHNlbGYuaW5wdXREYXRhW2F1dG9Db21wbGV0ZUl0ZW0uaW5wdXRLZXldLCBhdXRvQ29tcGxldGVJdGVtLmtleVRvU2hvdyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgLy8gICBjb25zb2xlLmxvZyh0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLCBcIj4+Pj4+dGhpcy5mb3JtVmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkc1wiKVxyXG4gICAgfVxyXG4gICAgLy8gaWYodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5zaG93U2VsZWN0ZWRDaGlwcykge1xyXG5cclxuICAgIC8vIH1cclxuICB9XHJcblxyXG4gIHZhbGlkYXRlT2JqZWN0RmllbGRzKGlucHV0RGF0YSwgbWFuZGF0b3J5RmllbGRzLCBjYWxsYmFjaykge1xyXG4gICAgbGV0IHZhbGlkID0gdHJ1ZTtcclxuICAgIF8uZm9yRWFjaChtYW5kYXRvcnlGaWVsZHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmICghaW5wdXREYXRhW2l0ZW1dKSB7XHJcbiAgICAgICAgdmFsaWQgPSBmYWxzZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBjYWxsYmFjayh2YWxpZCk7XHJcbiAgfVxyXG4gIGFkZE5ld1ZhbHVlKCkge1xyXG4gICAgY29uc29sZS5sb2codGhpcywgXCIuLi4uLi5cIik7XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5lbmFibGVBZGRCdXR0b24pIHtcclxuICAgICAgbGV0IHRlbXBEYXRhID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eShKU09OLnBhcnNlKHRlbXBEYXRhKSlcclxuICAgICAgICA/IEpTT04ucGFyc2UodGVtcERhdGEpXHJcbiAgICAgICAgOiB7fTtcclxuICAgICAgbGV0IHRlbXBBcnJheSA9IHRoaXMuaW5wdXREYXRhW3RoaXMuZm9ybVZhbHVlcy5kYXRhVG9TYXZlXVxyXG4gICAgICAgID8gdGhpcy5pbnB1dERhdGFbdGhpcy5mb3JtVmFsdWVzLmRhdGFUb1NhdmVdXHJcbiAgICAgICAgOiBbXTtcclxuICAgICAgbGV0IHRlbXBPYmogPSB7fTtcclxuICAgICAgbGV0IGlucHV0VmFsdWVzID0gdGhpcy5pbnB1dEdyb3VwLnZhbHVlO1xyXG4gICAgICBjb25zb2xlLmxvZyhcclxuICAgICAgICB0aGlzLmZvcm1WYWx1ZXMubWFuZGF0b3J5RmllbGRzLFxyXG4gICAgICAgIFwiPj4+Pj50aGlzLmZvcm1WYWx1ZXMubWFuZGF0b3J5RmllbGRzXCJcclxuICAgICAgKTtcclxuICAgICAgaWYgKHRoaXMuZm9ybVZhbHVlcyAmJiB0aGlzLmZvcm1WYWx1ZXMubWFuZGF0b3J5RmllbGRzKSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIHRoaXMudmFsaWRhdGVPYmplY3RGaWVsZHMoXHJcbiAgICAgICAgICBpbnB1dFZhbHVlcyxcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy5tYW5kYXRvcnlGaWVsZHMsXHJcbiAgICAgICAgICBmdW5jdGlvbiAodmFsaWQpIHtcclxuICAgICAgICAgICAgaWYgKHZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgXy5mb3JFYWNoKHNlbGYuZm9ybVZhbHVlcy5vYmplY3RGb3JtYXQsIGZ1bmN0aW9uIChvYmpJdGVtKSB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wT2JqW29iakl0ZW0ubmFtZV0gPSBpbnB1dFZhbHVlc1tvYmpJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICBsZXQgZXhpc3QgPVxyXG4gICAgICAgICAgICAgICAgdGVtcEFycmF5ICYmIHRlbXBBcnJheS5sZW5ndGhcclxuICAgICAgICAgICAgICAgICAgPyBfLmZpbmRJbmRleCh0ZW1wQXJyYXksIHRlbXBPYmopXHJcbiAgICAgICAgICAgICAgICAgIDogLTE7XHJcbiAgICAgICAgICAgICAgaWYgKGV4aXN0IDwgMCkge1xyXG4gICAgICAgICAgICAgICAgdGVtcEFycmF5LnB1c2godGVtcE9iaik7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3NlbGYuZm9ybVZhbHVlcy5kYXRhVG9TYXZlXSA9IHRlbXBBcnJheTtcclxuICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcclxuICAgICAgICAgICAgICAgIFwiY3VycmVudElucHV0XCIsXHJcbiAgICAgICAgICAgICAgICBKU09OLnN0cmluZ2lmeShzZWxmLmlucHV0RGF0YSlcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgIHNlbGYucmVjZWl2ZUNsaWNrKG51bGwpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXCJFbnRlciBNYW5kYXRvcnkgRmllbGRzIVwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdGhpcy5pbnB1dEdyb3VwLnJlc2V0KCk7XHJcbiAgICAgIH1cclxuICAgICAgLy8gaWYoXy5pc0VxdWFsKGlucHV0VmFsdWVLZXlzLCB0aGlzLmZvcm1WYWx1ZXMubWFuZGF0b3J5RmllbGRzKSl7XHJcbiAgICAgIC8vICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5vYmplY3RGb3JtYXQsIGZ1bmN0aW9uKG9iakl0ZW0pe1xyXG4gICAgICAvLyAgICAgdGVtcE9ialtvYmpJdGVtLm5hbWVdID0gaW5wdXRWYWx1ZXNbb2JqSXRlbS52YWx1ZV1cclxuICAgICAgLy8gICB9KVxyXG4gICAgICAvLyB9ZWxzZXtcclxuICAgICAgLy8gICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFwiRW50ZXIgTWFuZGF0b3J5IEZpZWxkcyFcIik7XHJcbiAgICAgIC8vIH1cclxuICAgIH1cclxuICB9XHJcbiAgb25DaGFuZ2UoZGF0YSwgc2VsZWN0ZWRGaWVsZCkge1xyXG4gICAgY29uc29sZS5sb2coXCIgZGF0YSBcIiwgZGF0YSk7XHJcbiAgICBjb25zb2xlLmxvZyhcIiBzZWxlY3RlZEZpZWxkIFwiLCBzZWxlY3RlZEZpZWxkKTtcclxuICAgIGxldCB0ZW1wRGF0YSA9IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIikpO1xyXG4gICAgbGV0IHNlbGVjdElkID0gc2VsZWN0ZWRGaWVsZC5uYW1lICsgXCJJZFwiO1xyXG4gICAgbGV0IGV4aXN0Q2hlY2sgPSBfLmhhcyh0aGlzLmlucHV0RGF0YSwgc2VsZWN0SWQpO1xyXG4gICAgbGV0IGlkS2V5ID0gZXhpc3RDaGVjayA/IHNlbGVjdElkIDogc2VsZWN0ZWRGaWVsZC5uYW1lICsgXCJJZFwiO1xyXG4gICAgbGV0IG5hbWVLZXkgPSBzZWxlY3RlZEZpZWxkLm5hbWUgKyBcIk5hbWVcIjtcclxuICAgIHRoaXMuaW5wdXREYXRhW2lkS2V5XSA9IGRhdGFbc2VsZWN0ZWRGaWVsZC5rZXlUb1NhdmVdO1xyXG4gICAgaWYgKHNlbGVjdGVkRmllbGQuYWRkaXRpb25hbEtleVRvU2F2ZSkge1xyXG4gICAgICB0aGlzLmlucHV0RGF0YVtzZWxlY3RlZEZpZWxkLm5hbWUgKyBcIk5hbWVcIl0gPVxyXG4gICAgICAgIGRhdGFbc2VsZWN0ZWRGaWVsZC5hZGRpdGlvbmFsS2V5VG9TYXZlXTtcclxuICAgICAgbmFtZUtleSA9IHNlbGVjdGVkRmllbGQubmFtZSArIFwiTmFtZVwiO1xyXG4gICAgfVxyXG4gICAgdGVtcERhdGEgPSB7Li4udGVtcERhdGEsIC4uLnRoaXMuaW5wdXREYXRhfTtcclxuICAgIGlmKHNlbGVjdGVkRmllbGQucmVmcmVzaENoaXBGaWVsZCAmJiB0aGlzLnNlbGVjdGVkQ2hpcHMpIHtcclxuICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG4gICAgICBfLmZvckVhY2goc2VsZWN0ZWRGaWVsZC5yZWZyZXNoQ2hpcEZpZWxkS2V5LCBmdW5jdGlvbihyZWZyZXNoSXRlbSl7XHJcbiAgICAgICAgc2VsZi5zZWxlY3RlZENoaXBzW3JlZnJlc2hJdGVtXSA9IFtdO1xyXG4gICAgICAgIHNlbGYuc2VsZWN0ZWRDaGlwS2V5TGlzdFtyZWZyZXNoSXRlbV0gPSBbXTtcclxuICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICB0ZW1wRGF0YVsnc2VsZWN0ZWRDaGlwcyddID0gdGhpcy5zZWxlY3RlZENoaXBzO1xyXG4gICAgdGVtcERhdGFbJ3NlbGVjdGVkQ2hpcEtleUxpc3QnXSA9IHRoaXMuc2VsZWN0ZWRDaGlwS2V5TGlzdDtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gdGVtcERhdGE7XHJcbiAgICBpZiAoXHJcbiAgICAgIF8uaGFzKHRoaXMuaW5wdXREYXRhLCBzZWxlY3RlZEZpZWxkLm5hbWUpICYmXHJcbiAgICAgIHRlbXBEYXRhW3NlbGVjdGVkRmllbGQubmFtZV0gPT09IFwiXCJcclxuICAgICkge1xyXG4gICAgICB0ZW1wRGF0YVtzZWxlY3RlZEZpZWxkLm5hbWVdID0gdGhpcy5pbnB1dERhdGFbc2VsZWN0ZWRGaWVsZC5uYW1lXTtcclxuICAgICAgdGVtcERhdGFbaWRLZXldID0gdGhpcy5pbnB1dERhdGFbaWRLZXldO1xyXG4gICAgICB0ZW1wRGF0YVtuYW1lS2V5XSA9IHRoaXMuaW5wdXREYXRhW25hbWVLZXldO1xyXG4gICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0ZW1wRGF0YSkpO1xyXG4gICAgfWVsc2V7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRlbXBEYXRhKSk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYgKHNlbGVjdGVkRmllbGQuZ2V0QXV0b2NvbXBsZXRlRGF0YSkge1xyXG4gICAgICBsZXQgc2VsZWN0ZWRSZXF1ZXN0RGF0YSA9IF8uZmluZCh0aGlzLmZvcm1WYWx1ZXMucmVxdWVzdExpc3QsIHtcclxuICAgICAgICByZXF1ZXN0SWQ6IGRhdGEudmFsdWUsXHJcbiAgICAgIH0pO1xyXG4gICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZFJlcXVlc3REYXRhLCBcIj4+Pj4+PnNlbGVjdGVkUmVxdWVzdERhdGFcIik7XHJcbiAgICAgIHRoaXMuZ2V0QXV0b0NvbXBsZXRlVmFsdWVzKHNlbGVjdGVkUmVxdWVzdERhdGEsIGRhdGEsIHNlbGVjdGVkRmllbGQpO1xyXG4gICAgfWVsc2UgaWYoc2VsZWN0ZWRGaWVsZC5vbkxvYWRWYWx1ZUtleSl7XHJcbiAgICAgICAgaWYoc2VsZWN0ZWRGaWVsZC5vbkNoaXBMb2FkKVxyXG4gICAgICAgIHsgXHJcbiAgICAgICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLmNoaXBGaWVsZHMsIGZ1bmN0aW9uKHNlbGVjdEZpZWxkSXRlbSl7XHJcbiAgICAgICAgICAgIGlmKHNlbGVjdEZpZWxkSXRlbS5uYW1lID09IHNlbGVjdGVkRmllbGQub25Mb2FkVmFsdWVLZXkpe1xyXG4gICAgICAgICAgICAgIHNlbGVjdEZpZWxkSXRlbS5kYXRhID0gIGRhdGFbc2VsZWN0ZWRGaWVsZC5vbkNoaXBMb2FkXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICBfLmZvckVhY2godGhpcy5mb3JtVmFsdWVzLnNlbGVjdEZpZWxkcywgZnVuY3Rpb24oc2VsZWN0RmllbGRJdGVtKXtcclxuICAgICAgICAgICAgaWYoc2VsZWN0RmllbGRJdGVtLm5hbWUgPT0gc2VsZWN0ZWRGaWVsZC5vbkxvYWRWYWx1ZUtleSl7XHJcbiAgICAgICAgICAgICAgc2VsZWN0RmllbGRJdGVtLmRhdGEgPSAgZGF0YVtzZWxlY3RlZEZpZWxkLm9uTG9hZFZhbHVlS2V5XVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmdldENoaXBMaXN0KCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBnZXRBdXRvQ29tcGxldGVWYWx1ZXMoc2VsZWN0ZWRSZXF1ZXN0RGF0YSwgc2VsZWN0ZWREYXRhLCBzZWxlY3RlZEZpZWxkKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIioqIFwiLCBcIj4+PnNlbGVjdGVkRmllbGRcIiwgc2VsZWN0ZWRGaWVsZCk7XHJcbiAgICBjb25zb2xlLmxvZyhcIioqIHNlbGVjdGVkUmVxdWVzdERhdGEgXCIsIHNlbGVjdGVkUmVxdWVzdERhdGEpO1xyXG5cclxuICAgIC8vIGNvbnNvbGUubG9nKFwiICoqIHNlbGVjdGVkUmVxdWVzdERhdGEudGV4dEZpZWxkTmFtZSBcIixzZWxlY3RlZFJlcXVlc3REYXRhLnRleHRGaWVsZE5hbWUpO1xyXG4gICAgaWYgKHNlbGVjdGVkUmVxdWVzdERhdGEgJiYgc2VsZWN0ZWRSZXF1ZXN0RGF0YS5rZXlUb1NhdmVPbklucHV0KSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW3NlbGVjdGVkUmVxdWVzdERhdGEua2V5VG9TYXZlT25JbnB1dF0gPVxyXG4gICAgICAgIHNlbGVjdGVkRGF0YVtzZWxlY3RlZFJlcXVlc3REYXRhLmtleVRvU2F2ZU9uSW5wdXRdO1xyXG4gICAgfVxyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgbGV0IGluZGV4O1xyXG4gICAgbGV0IGN1cnJlbnRPYmo7XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMpIHtcclxuICAgICAgaW5kZXggPSBfLmZpbmRJbmRleCh0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcywge1xyXG4gICAgICAgIG5hbWU6IHNlbGVjdGVkUmVxdWVzdERhdGEudGV4dEZpZWxkTmFtZSxcclxuICAgICAgfSk7XHJcbiAgICAgIGN1cnJlbnRPYmogPSB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkc1tpbmRleF07XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpbmRleCA9IF8uZmluZEluZGV4KFxyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHMsXHJcbiAgICAgICAgc2VsZWN0ZWRSZXF1ZXN0RGF0YVxyXG4gICAgICApO1xyXG4gICAgICBjdXJyZW50T2JqID0gdGhpcy5mb3JtVmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkc1tpbmRleF07XHJcbiAgICB9XHJcblxyXG4gICAgY29uc29sZS5sb2coaW5kZXgsIFwiPj4+Pj4+aW5kZXhcIik7XHJcbiAgICAvLyBkZWJ1Z2dlcjtcclxuICAgIC8vIGxldCBhdXRvQ29tcGxldGVGaWVsZCA9IHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzO1xyXG4gICAgLy8gXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLCBmdW5jdGlvbihcclxuICAgIC8vICAgaXRlbVxyXG4gICAgLy8gKSB7XHJcbiAgICAvLyBsZXQgaXRlbSA9IHNlbGVjdGVkUmVxdWVzdERhdGE7XHJcbiAgICBpZiAoc2VsZWN0ZWRSZXF1ZXN0RGF0YSAmJiBzZWxlY3RlZFJlcXVlc3REYXRhLmRhdGEpIHtcclxuICAgICAgY29uc29sZS5sb2coXCIgZW50ZXJlZCBkZWZhdWx0IGRhdGEgXCIpO1xyXG4gICAgICBjdXJyZW50T2JqW3NlbGVjdGVkRmllbGQub25Mb2FkRGF0YUtleV0gPSBzZWxlY3RlZFJlcXVlc3REYXRhLmRhdGE7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoc2VsZWN0ZWRSZXF1ZXN0RGF0YS5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgIGxldCBhcGlVcmwgPSBzZWxlY3RlZFJlcXVlc3REYXRhLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICBsZXQgcmVzcG9uc2VOYW1lID0gc2VsZWN0ZWRSZXF1ZXN0RGF0YS5vbkxvYWRGdW5jdGlvbi5yZXNwb25zZTtcclxuICAgICAgICBsZXQgcXVlcnkgPSB7cmVhbG0gOiBzZWxmLnJlYWxtfTtcclxuICAgICAgICBfLmZvckVhY2goc2VsZWN0ZWRSZXF1ZXN0RGF0YS5vbkxvYWRGdW5jdGlvbi5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKFxyXG4gICAgICAgICAgcmVxdWVzdEl0ZW1cclxuICAgICAgICApIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICghcmVxdWVzdEl0ZW0uaXNTZWFyY2gpIHtcclxuICAgICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uaXNFdmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxlY3RlZERhdGE7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIiBxdWVyeSBcIiwgcXVlcnksIFwiIGFwaXVybCBcIiwgYXBpVXJsKTtcclxuICAgICAgICAvL3RoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuZ2V0QWxsUmVwb25zZShxdWVyeSwgYXBpVXJsKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAvLyB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjdXJyZW50T2JqW3NlbGVjdGVkRmllbGQub25Mb2FkRGF0YUtleV0gPSBkYXRhLnJlc3BvbnNlW1xyXG4gICAgICAgICAgICAgIHJlc3BvbnNlTmFtZVxyXG4gICAgICAgICAgICBdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgICBsZXQgdmFsaWRhdGVEYXRhID0gY3VycmVudE9ialtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldWzBdO1xyXG4gICAgICAgICAgICBsZXQgaXNBcnJheU9mSlNPTiA9IF8uaXNQbGFpbk9iamVjdCh2YWxpZGF0ZURhdGEpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAoIWlzQXJyYXlPZkpTT04pIHtcclxuICAgICAgICAgICAgICBjb25zdCB0ZW1wTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50T2JqW3NlbGVjdGVkRmllbGQub25Mb2FkRGF0YUtleV0sIGZ1bmN0aW9uIChcclxuICAgICAgICAgICAgICAgIHZhbFxyXG4gICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgdGVtcExpc3QucHVzaCh7IG9ial9uYW1lOiB2YWwsIG9ial9kZXNjOiB2YWwgfSk7XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgY3VycmVudE9ialtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldID0gdGVtcExpc3Q7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgdGhpcy52aWV3RGF0YVtzZWxlY3RlZEZpZWxkLm9uTG9hZERhdGFLZXldID1cclxuICAgICAgICAgICAgICBjdXJyZW50T2JqW3NlbGVjdGVkRmllbGQub25Mb2FkRGF0YUtleV07XHJcbiAgICAgICAgICAgIGlmIChpbmRleCA9PSBzZWxlY3RlZEZpZWxkLmVuYWJsZUNvbmRpdGlvbkluZGV4KSB7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcywgXCI+Pj4+PlwiKTtcclxuICAgICAgICAgICAgICB0aGlzLmlucHV0R3JvdXAuY29udHJvbHNbc2VsZWN0ZWRSZXF1ZXN0RGF0YS5uYW1lXS5lbmFibGUoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgLy8gdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCIgRXJyb3IgXCIsIGVycik7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLy8gY29uc29sZS5sb2coaXQvZW0sIFwiLi4uLi4+Pj5JVEVNTU1cIik7XHJcbiAgICAvLyB0ZW1wQXJyYXkucHVzaChpdGVtKTtcclxuICAgIC8vIH0pO1xyXG4gICAgY3VycmVudE9iaiA9IHsgLi4uY3VycmVudE9iaiwgLi4uc2VsZWN0ZWRSZXF1ZXN0RGF0YSB9O1xyXG4gICAgY29uc29sZS5sb2coY3VycmVudE9iaiwgXCIuLi4uLmN1cnJlbnRPYmpcIik7XHJcbiAgICAvLyB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkc1tpbmRleF0gPSB7XHJcbiAgICAvLyAgIC4uLmN1cnJlbnRPYmosXHJcbiAgICAvLyAgIC4uLml0ZW1cclxuICAgIC8vIH07XHJcbiAgICBpZiAodGhpcy5mb3JtVmFsdWVzICYmIHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMpIHtcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbaW5kZXhdID0gY3VycmVudE9iajtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIHRoaXMuZm9ybVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbaW5kZXhdID0gY3VycmVudE9iajtcclxuICAgIH1cclxuICAgIC8vIGNvbnNvbGUubG9nKFxyXG4gICAgLy8gICB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkcyxcclxuICAgIC8vICAgXCI+Pj4+Pj4+Pj4+Pj4+Pj4+PlwiXHJcbiAgICAvLyApO1xyXG4gIH1cclxuXHJcbiAgb25BdXRvQ29tcGxldGVTZWFyY2goc2VhcmNoVmFsdWUsIGlucHV0RmllbGQpIHtcclxuICAgIGNvbnNvbGUubG9nKHNlYXJjaFZhbHVlLCBcIj4+Pj4+c2VhcmNoVmFsdWVcIik7XHJcbiAgICBjb25zb2xlLmxvZyhpbnB1dEZpZWxkLCBcIj4+Pj4+IElOUFVUIEZJRUxEXCIpO1xyXG4gICAgaWYgKGlucHV0RmllbGQudmFsdWVGcm9tTG9jYWwgfHwgaW5wdXRGaWVsZC5lbmFibGVGaWx0ZXIpIHtcclxuICAgICAgaWYgKHNlYXJjaFZhbHVlKSB7XHJcbiAgICAgICAgbGV0IHNlYXJjaE9iaiA9IHt9O1xyXG4gICAgICAgIHNlYXJjaE9ialtpbnB1dEZpZWxkLmtleVRvU2hvd10gPSBzZWFyY2hWYWx1ZTtcclxuICAgICAgICBpbnB1dEZpZWxkW2lucHV0RmllbGQuZGF0YUtleVRvU2F2ZV0gPSBfLmZpbHRlcihcclxuICAgICAgICAgIGlucHV0RmllbGRbaW5wdXRGaWVsZC5kYXRhS2V5VG9TYXZlXSxcclxuICAgICAgICAgIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbHRlclZhbHVlID0gc2VhcmNoVmFsdWUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgIGl0ZW1baW5wdXRGaWVsZC5rZXlUb1Nob3ddLnRvTG93ZXJDYXNlKCkuaW5kZXhPZihmaWx0ZXJWYWx1ZSkgPT0gMFxyXG4gICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICByZXR1cm4gaXRlbTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaW5wdXRGaWVsZFtpbnB1dEZpZWxkLmRhdGFLZXlUb1NhdmVdID1cclxuICAgICAgICAgIHRoaXMudmlld0RhdGEgJiYgdGhpcy52aWV3RGF0YVtpbnB1dEZpZWxkLmRhdGFLZXlUb1NhdmVdXHJcbiAgICAgICAgICAgID8gdGhpcy52aWV3RGF0YVtpbnB1dEZpZWxkLmRhdGFLZXlUb1NhdmVdXHJcbiAgICAgICAgICAgIDogdGhpcy52aWV3RGF0YVtpbnB1dEZpZWxkLmlucHV0S2V5XTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgb25TZWxlY3RBdXRvQ29tcGxldGUoZXZlbnQsIGluZGV4LCBzZWxlY3RlZEZpZWxkKSB7XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCI+Pj4+PkVWRU5UXCIpO1xyXG4gICAgY29uc29sZS5sb2coaW5kZXgsIFwiPj4+Pj5JTkRFWFwiKTtcclxuICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkRmllbGQsIFwiPj4+Pj4gU0VMRUNURUQgRklFTERcIik7XHJcbiAgICBsZXQgc2VsZWN0ZWRSZXF1ZXN0RGF0YSA9IF8uZmluZCh0aGlzLmZvcm1WYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLCB7XHJcbiAgICAgIGZ1bmN0aW9uQ2FsbEluZGV4OiBpbmRleCArIDEsXHJcbiAgICB9KTtcclxuICAgIGNvbnNvbGUubG9nKHNlbGVjdGVkUmVxdWVzdERhdGEsIFwiPj4+Pj4+c2VsZWN0ZWRSZXF1ZXN0RGF0YVwiKTtcclxuICAgIHRoaXMuZ2V0QXV0b0NvbXBsZXRlVmFsdWVzKHNlbGVjdGVkUmVxdWVzdERhdGEsIGV2ZW50LCBzZWxlY3RlZEZpZWxkKTtcclxuICB9XHJcbiAgb25TZWFyY2hDaGFuZ2Uoc2VhcmNoVmFsdWUsIGlucHV0RmllbGQpIHtcclxuICAgIGNvbnNvbGUubG9nKGlucHV0RmllbGQsIFwiLi5pbnB1dEZpZWxkXCIpO1xyXG4gICAgY29uc29sZS5sb2coc2VhcmNoVmFsdWUsIFwiLi4uLi4uLi4uc2VhcmNodmFsdWVcIik7XHJcbiAgICB2YXIgY2hhckxlbmd0aCA9IHNlYXJjaFZhbHVlLmxlbmd0aDtcclxuICAgIHZhciBsaW1pdCA9IHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLmNoYXJsaW1pdFxyXG4gICAgICA/IHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLmNoYXJsaW1pdFxyXG4gICAgICA6IDM7XHJcbiAgICBjb25zb2xlLmxvZyhcIkxpbWl0IFwiLCBsaW1pdCk7XHJcbiAgICAvLyBpZiAoY2hhckxlbmd0aCA+PSBsaW1pdCB8fCAhc2VhcmNoVmFsdWUpIHtcclxuICAgICAgY29uc29sZS5sb2coXCJjYWxsIEFQSSBcIik7XHJcbiAgICAgIHRoaXMuY2hhcmxpbWl0Y2hlY2sgPSB0cnVlO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIGxldCBpbmRleCA9IF8uZmluZEluZGV4KFxyXG4gICAgICAgIHRoaXMuZm9ybVZhbHVlcy5mb3JtQXJyYXlWYWx1ZXMuYXV0b0NvbXBsZXRlRmllbGRzLFxyXG4gICAgICAgIHsgbmFtZTogaW5wdXRGaWVsZC50ZXh0RmllbGROYW1lIH1cclxuICAgICAgKTtcclxuICAgICAgbGV0IGN1cnJlbnRPYmogPSB0aGlzLmZvcm1WYWx1ZXMuZm9ybUFycmF5VmFsdWVzLmF1dG9Db21wbGV0ZUZpZWxkc1tcclxuICAgICAgICBpbmRleFxyXG4gICAgICBdO1xyXG4gICAgICBpZiAoaW5wdXRGaWVsZC5vbkxvYWRGdW5jdGlvbikge1xyXG4gICAgICAgIGxldCBhcGlVcmwgPSBpbnB1dEZpZWxkLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgICBsZXQgcmVzcG9uc2VOYW1lID0gaW5wdXRGaWVsZC5vbkxvYWRGdW5jdGlvbi5yZXNwb25zZTtcclxuICAgICAgICBsZXQgcXVlcnkgPSB7cmVhbG06IHNlbGYucmVhbG19O1xyXG4gICAgICAgIF8uZm9yRWFjaChpbnB1dEZpZWxkLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAoXHJcbiAgICAgICAgICByZXF1ZXN0SXRlbVxyXG4gICAgICAgICkge1xyXG4gICAgICAgICAgaWYgKHJlcXVlc3RJdGVtLmlzU2VhcmNoKSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKHJlcXVlc3RJdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgLy90aGlzLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgICAgICBzZWxmLmNvbnRlbnRTZXJ2aWNlLmdldEFsbFJlcG9uc2UocXVlcnksIGFwaVVybCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2coXCI+PiBvbnNlYXJjaCBDaGFuZ2UgXCIpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgICAgICBjdXJyZW50T2JqW2lucHV0RmllbGQub25Mb2FkRGF0YUtleV0gPSBkYXRhLnJlc3BvbnNlW1xyXG4gICAgICAgICAgICAgIHJlc3BvbnNlTmFtZVxyXG4gICAgICAgICAgICBdIGFzIG9iamVjdFtdO1xyXG4gICAgICAgICAgICBsZXQgdmFsaWRhdGVEYXRhID0gY3VycmVudE9ialtpbnB1dEZpZWxkLm9uTG9hZERhdGFLZXldWzBdO1xyXG4gICAgICAgICAgICBsZXQgaXNBcnJheU9mSlNPTiA9IF8uaXNQbGFpbk9iamVjdCh2YWxpZGF0ZURhdGEpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAoIWlzQXJyYXlPZkpTT04pIHtcclxuICAgICAgICAgICAgICBjb25zdCB0ZW1wTGlzdCA9IFtdO1xyXG4gICAgICAgICAgICAgIF8uZm9yRWFjaChjdXJyZW50T2JqW2lucHV0RmllbGQub25Mb2FkRGF0YUtleV0sIGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICAgICAgICAgIHRlbXBMaXN0LnB1c2goeyBvYmpfbmFtZTogdmFsLCBvYmpfZGVzYzogdmFsIH0pO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIGN1cnJlbnRPYmpbaW5wdXRGaWVsZC5vbkxvYWREYXRhS2V5XSA9IHRlbXBMaXN0O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMudmlld0RhdGFbaW5wdXRGaWVsZC5vbkxvYWREYXRhS2V5XSA9XHJcbiAgICAgICAgICAgICAgY3VycmVudE9ialtpbnB1dEZpZWxkLm9uTG9hZERhdGFLZXldO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhjdXJyZW50T2JqLCBcIj4+Pj4+Y3VycmVudE9ialwiKTtcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIC8vIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIEVycm9yIFwiLCBlcnIpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLmZvcm1BcnJheVZhbHVlcy5hdXRvQ29tcGxldGVGaWVsZHNbaW5kZXhdID0gY3VycmVudE9iajtcclxuICAgIC8vIH0gZWxzZSB7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKFwiU3RvcCBBUEkgXCIpO1xyXG4gICAgLy8gICB0aGlzLmNoYXJsaW1pdGNoZWNrID0gZmFsc2U7XHJcbiAgICAvLyB9XHJcbiAgfVxyXG4gIG9uT3B0aW9uQ2hhbmdlKGV2ZW50LCBzZWxlY3RlZE9wdGlvbikge1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiaW5wdXREYXRhPj4+Pj4+Pj5cIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtzZWxlY3RlZE9wdGlvbi5uYW1lXSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5mb3JtVmFsdWVzKTtcclxuICAgIC8vIGlmICh0aGlzLmVuYWJsZUNoaXBGaWVsZHMpIHtcclxuICAgIC8vICAgY29uc29sZS5sb2codGhpcy5lbmFibGVDaGlwRmllbGRzLCBcImVuYWJsZUNoaXBGaWVsZHNcIik7XHJcbiAgICAvLyAgIHZhciBzZWxmID0gdGhpcztcclxuICAgIC8vICAgXy5mb3JFYWNoKHRoaXMuZm9ybVZhbHVlcy5jaGlwRmllbGRzLCBmdW5jdGlvbih4KSB7XHJcbiAgICAvLyAgICAgY29uc29sZS5sb2coXCJ4XCIpO1xyXG4gICAgLy8gICAgIGlmICh4LmNoaXBDb25kaXRpb24gJiYgeC5jaGlwQ29uZGl0aW9uLnZhcmlhYmxlKSB7XHJcbiAgICAvLyAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAvLyAgICAgICBsZXQgdGVtcCA9IHguY2hpcENvbmRpdGlvbi52YXJpYWJsZTtcclxuICAgIC8vICAgICAgIHNlbGYuQ2hpcExpbWl0ID0geC5jaGlwQ29uZGl0aW9uLmRhdGFbc2VsZi5pbnB1dERhdGFbdGVtcF1dW1wibGltaXRcIl07XHJcbiAgICAvLyAgICAgICBzZWxmLkNoaXBPcGVyYXRvciA9XHJcbiAgICAvLyAgICAgICAgIHguY2hpcENvbmRpdGlvbi5kYXRhW3NlbGYuaW5wdXREYXRhW3RlbXBdXVtcIm9wZXJhdG9yXCJdO1xyXG4gICAgLy8gICAgICAgY29uc29sZS5sb2coc2VsZi5DaGlwTGltaXQsIFwiQ2hpcExpbWl0Pj4+XCIpO1xyXG4gICAgLy8gICAgIH1cclxuICAgIC8vICAgfSk7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKHRoaXMuQ2hpcExpbWl0LCBcIjFcIik7XHJcbiAgICAvLyAgIGNvbnNvbGUubG9nKHRoaXMuQ2hpcE9wZXJhdG9yLCBcIjJcIik7XHJcbiAgICAvLyAgIHNlbGYuaW5wdXREYXRhW1wiY2hpcExpbWl0XCJdID0gdGhpcy5DaGlwTGltaXQ7XHJcbiAgICAvLyAgIHNlbGYuaW5wdXREYXRhW1wiY2hpcE9wZXJhdG9yXCJdID0gdGhpcy5DaGlwT3BlcmF0b3I7XHJcbiAgICAvLyAgIC8vIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdjaGlwRGF0YScsZXZlbnQudmFsdWUpXHJcbiAgICAvLyB9XHJcbiAgICBjb25zb2xlLmxvZyhldmVudCwgXCIuLi4uZXZlbnQudmFsdWVcIik7XHJcbiAgICBjb25zb2xlLmxvZyhzZWxlY3RlZE9wdGlvbiwgXCIuLi4uc2VsZWN0ZWRPcHRpb25cIik7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtzZWxlY3RlZE9wdGlvbi5uYW1lXSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgaWYgKHNlbGVjdGVkT3B0aW9uICYmIHNlbGVjdGVkT3B0aW9uLmlzSW1wb3J0VHlwZSkge1xyXG4gICAgICB0aGlzLnNlbGVjdEltcG9ydFZpZXcoZXZlbnQudmFsdWUpO1xyXG4gICAgfSBlbHNlIGlmKHNlbGVjdGVkT3B0aW9uICYmIHNlbGVjdGVkT3B0aW9uLm5hbWU9PSdjaGVjaEltcG9ydCcpe1xyXG4gICAgICBpZihldmVudC52YWx1ZT09J05PJyl7XHJcbiAgICAgICAgdGhpcy5lbmFibGVJbXBvcnREYXRhPWZhbHNlO1xyXG4gICAgICB9ZWxzZXtcclxuICAgICAgICB0aGlzLmVuYWJsZUltcG9ydERhdGE9dHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgIGVsc2Uge1xyXG4gICAgICBfLmZvckVhY2goc2VsZWN0ZWRPcHRpb24uZmllbGRzLCBmdW5jdGlvbiAob3B0aW9uSXRlbSkge1xyXG4gICAgICAgIGlmIChvcHRpb25JdGVtLmNoZWNrZWRUeXBlKSB7XHJcbiAgICAgICAgICBzZWxmLmlucHV0RGF0YVtvcHRpb25JdGVtLmNoZWNrZWRUeXBlXSA9XHJcbiAgICAgICAgICAgIG9wdGlvbkl0ZW0udmFsdWUgPT09IGV2ZW50LnZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSk7XHJcbiAgICAgIGNvbnNvbGUubG9nKHNlbGYuaW5wdXREYXRhLCBcInNlbGZcIik7XHJcbiAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIHRvZ2dsZUNoYW5nZWQoZXZlbnQpIHt9XHJcbiAgb25TZWFyY2hOYW1lKGV2ZW50LCBmaWVsZERhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiIG9uIFNlYXJjaCBOYW1lICoqKioqKioqKioqKioqKioqKioqKioqKioqIFwiKTtcclxuICAgIGxldCBjaGFyTGVuZ3RoID0gZXZlbnQubGVuZ3RoO1xyXG4gICAgY29uc29sZS5sb2coXHJcbiAgICAgIFwiIE9uIHNlYXJjaCBOYW1lIGV2ZW50IFwiLFxyXG4gICAgICBldmVudCxcclxuICAgICAgXCIgZmllbGRkYXRhIFwiLFxyXG4gICAgICBmaWVsZERhdGEsXHJcbiAgICAgIFwiIExlbmd0aDogXCIsXHJcbiAgICAgIGNoYXJMZW5ndGhcclxuICAgICk7XHJcbiAgICBpZiAoZmllbGREYXRhLmVuYWJsZU9uVHlwZVNlYXJjaCAmJiBldmVudCkge1xyXG4gICAgICB2YXIgbGltaXQgPSBmaWVsZERhdGEuY2hhcmxpbWl0ID8gZmllbGREYXRhLmNoYXJsaW1pdCA6IDM7XHJcbiAgICAgIGlmIChsaW1pdCA8PSBjaGFyTGVuZ3RoKSB7XHJcbiAgICAgICAgdGhpcy5jaGFybGltaXRjaGVjayA9IHRydWU7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCJwcm9jZWVkIEFQSSBjYWxsXCIpO1xyXG4gICAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IGZpZWxkRGF0YS5vblR5cGVTZWFyY2g7XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0ge3JlYWxtOiB0aGlzLnJlYWxtfTtcclxuICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgY29uc29sZS5sb2coc2VsZi5jdXJyZW50RGF0YSwgXCI+Pj4+IENVUlJFTlRcIik7XHJcbiAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3REZXRhaWxzLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5mcm9tQ3VycmVudERhdGEpIHtcclxuICAgICAgICAgICAgcXVlcnlbcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmN1cnJlbnREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAocmVxdWVzdEl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5W3JlcXVlc3RJdGVtLm5hbWVdID0gcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBxdWVyeVtyZXF1ZXN0SXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlF1ZXJ5IFwiLCBxdWVyeSk7XHJcbiAgICAgICAgLy8gdGhpcy5sb2FkZXJTZXJ2aWNlLnN0YXJ0TG9hZGVyKCk7XHJcbiAgICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnksIHJlcXVlc3REZXRhaWxzLmFwaVVybClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgLy8gIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coZGF0YSwgXCJkYXRhOjo6OjpcIik7XHJcbiAgICAgICAgICAgICAgLy8gbGV0IHJlc3BvbnNlTGVuZ3RoID1cclxuICAgICAgICAgICAgICAvLyAgIGRhdGEucmVzcG9uc2UgJiZcclxuICAgICAgICAgICAgICAvLyAgIGRhdGEucmVzcG9uc2VbcmVxdWVzdERldGFpbHMucmVzcG9uc2VOYW1lXS5sZW5ndGggPiAwXHJcbiAgICAgICAgICAgICAgLy8gICAgID8gZGF0YS5yZXNwb25zZVtyZXF1ZXN0RGV0YWlscy5yZXNwb25zZU5hbWVdLmxlbmd0aFxyXG4gICAgICAgICAgICAgIC8vICAgICA6IDA7XHJcbiAgICAgICAgICAgICAgaWYgKGRhdGEucmVzcG9uc2UgJiYgZGF0YS5yZXNwb25zZVtyZXF1ZXN0RGV0YWlscy5yZXNwb25zZU5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzTmFtZUF2YWlsYWJsZSA9XHJcbiAgICAgICAgICAgICAgICAgIGRhdGEucmVzcG9uc2VbcmVxdWVzdERldGFpbHMucmVzcG9uc2VOYW1lXS5sZW5ndGggPT0gMFxyXG4gICAgICAgICAgICAgICAgICAgID8gdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmlzTmFtZUF2YWlsYWJsZSwgXCJpc05hbWVBdmlsYWJsZTFcIik7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmIChkYXRhLnN0YXR1cyA9PSA0MDkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNOYW1lQXZhaWxhYmxlID0gZGF0YS5zdGF0dXMgPT0gNDA5ID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5pc05hbWVBdmFpbGFibGUsIFwiaXNOYW1lQXZpbGFibGUyXCIpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAvLyB0aGlzLmlzTmFtZUF2YWlsYWJsZSA9XHJcbiAgICAgICAgICAgICAgLy8gICByZXNwb25zZUxlbmd0aCA9PT0gMCB8fCBkYXRhLnN0YXR1cyAhPSA0MDkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXHJcbiAgICAgICAgICAgICAgLy8gICBkYXRhLnJlc3BvbnNlW3JlcXVlc3REZXRhaWxzLnJlc3BvbnNlTmFtZV0ubGVuZ3RoLFxyXG4gICAgICAgICAgICAgIC8vICAgXCJJSUlJSUlcIixcclxuICAgICAgICAgICAgICAvLyAgIHRoaXMuaXNOYW1lQXZhaWxhYmxlXHJcbiAgICAgICAgICAgICAgLy8gKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgIC8vIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgaWYgKGVyci5zdGF0dXMgPT0gNDA5KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzTmFtZUF2YWlsYWJsZSA9IGVyci5zdGF0dXMgPT0gNDA5ID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2codGhpcy5pc05hbWVBdmFpbGFibGUsIFwiaXNOYW1lQXZpbGFibGUyXCIpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zb2xlLmxvZyhcIlNUT1AgQVBpIGNhbGxcIik7XHJcbiAgICAgICAgLy8gdGhpcy5pc05hbWVBdmFpbGFibGU9ZmFsc2U7XHJcbiAgICAgICAgdGhpcy5jaGFybGltaXRjaGVjayA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIG9uU3VibWl0KCkge1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgY29uc29sZS5sb2codGhpcyxcIj4+Pj4+Pj4+Pj4+PiYmJiYmVEhJU1wiKTtcclxuICAgIHRoaXMuc3VibWl0dGVkID0gdHJ1ZTtcclxuICAgIHRoaXMudmFsaWRhdGlvbkNoZWNrKGZ1bmN0aW9uIChyZXN1bHQpIHtcclxuICAgICAgaWYgKHJlc3VsdCkge1xyXG4gICAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IChzZWxmLm9uTG9hZERhdGEgJiYgc2VsZi5vbkxvYWREYXRhLmFjdGlvbiAmJlxyXG4gICAgICAgICAgc2VsZi5jdXJyZW50Q29uZmlnRGF0YVtzZWxmLm9uTG9hZERhdGEuYWN0aW9uXSAmJlxyXG4gICAgICAgICAgc2VsZi5jdXJyZW50Q29uZmlnRGF0YVtzZWxmLm9uTG9hZERhdGEuYWN0aW9uXS5yZXF1ZXN0RGV0YWlscylcclxuICAgICAgICAgICAgPyBzZWxmLmN1cnJlbnRDb25maWdEYXRhW3NlbGYub25Mb2FkRGF0YS5hY3Rpb25dLnJlcXVlc3REZXRhaWxzXHJcbiAgICAgICAgICAgIDoge307XHJcbiAgICAgICAgaWYoIXNlbGYub25Mb2FkRGF0YSAmJiBzZWxmLmZvcm1WYWx1ZXMuYW5hbHlzaXNSZXF1ZXN0KXtcclxuICAgICAgICAgIHJlcXVlc3REZXRhaWxzID0gc2VsZi5mb3JtVmFsdWVzLmFuYWx5c2lzUmVxdWVzdDtcclxuICAgICAgICB9ZWxzZSBpZihzZWxmLmZvcm1WYWx1ZXMucmVxdWVzdERldGFpbHMpe1xyXG4gICAgICAgICAgcmVxdWVzdERldGFpbHMgPSBzZWxmLmZvcm1WYWx1ZXMucmVxdWVzdERldGFpbHNcclxuICAgICAgICB9XHJcbiAgICAgICAgbGV0IGRldGFpbHMgPSBzZWxmLnZpZXdEYXRhO1xyXG4gICAgICAgIC8vIGRldGFpbHNbXCJzdGF0dXNcIl0gPSBcIkFDVElWRVwiO1xyXG4gICAgICAgIC8vIGRldGFpbHNbXCJvcGVyYXRvcnNcIl0gPSBbXCJBTkRcIl07XHJcbiAgICAgICAgY29uc29sZS5sb2coXCI+Pj4gZGV0YWlscyBcIiwgZGV0YWlscyk7XHJcbiAgICAgICAgbGV0IGFwaVVybCA9IHJlcXVlc3REZXRhaWxzLmFwaVVybDtcclxuICAgICAgICBsZXQgcmVxdWVzdERhdGEgPSB7XHJcbiAgICAgICAgICByZWFsbSA6IHNlbGYucmVhbG1cclxuICAgICAgICB9O1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICAgIGlmIChpdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS52YWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSBpZiAoaXRlbS5jb250cm9sVHlwZUNoZWNrKSB7XHJcbiAgICAgICAgICAgIC8vICBmb3Igc3RvcmluZyBPcGVyYXRvcnNcclxuICAgICAgICAgICAgLy8gIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBldmFsKGl0ZW0uY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9XHJcbiAgICAgICAgICAgICAgZGV0YWlsc1tcImNvbnRyb2xUeXBlXCJdICYmIGRldGFpbHNbXCJjb250cm9sVHlwZVwiXSA9PSBcIlNPRFwiXHJcbiAgICAgICAgICAgICAgICA/IFwiQU5EXCJcclxuICAgICAgICAgICAgICAgIDogXCJPUlwiO1xyXG4gICAgICAgICAgfSBlbHNlIGlmKGl0ZW0uZnJvbUZvcm1Hcm91cCl7XHJcbiAgICAgICAgICAgIGxldCBmb3JtR3JvdXB2YWx1ZT1zZWxmLmlucHV0R3JvdXAudmFsdWU7XHJcbiAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPWZvcm1Hcm91cHZhbHVlW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICBsZXQgZXhpc3RDaGVjayA9IF8uaGFzKGRldGFpbHMsIGl0ZW0udmFsdWUpO1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+PiBleGlzdENoZWNrIFwiLGV4aXN0Q2hlY2spO1xyXG4gICAgICAgICAgICBpZihleGlzdENoZWNrKXtcclxuICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnN1YktleVxyXG4gICAgICAgICAgICAgPyBkZXRhaWxzW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgICAgOiBkZXRhaWxzW2l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9PT0gdW5kZWZpbmVkICYmXHJcbiAgICAgICAgICAgICAgaXRlbS5hbHRlcm5hdGl2ZUtleUNoZWNrXHJcbiAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgIC8vIGFkZGRlZCBmb3Igc3RvcmluZyAgZGVmYXVsdCBzZWxlY3RlbGQgZmllbGQgb2JqZWN0SWQgLiAoRWRpdClcclxuICAgICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gZGV0YWlsc1tpdGVtLmFsdGVybmF0aXZlS2V5XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgICAgIGlmIChzZWxmLm9uTG9hZERhdGEgJiYgKHNlbGYub25Mb2FkRGF0YS5hY3Rpb24gPT09IFwiZWRpdFwiIHx8IHNlbGYub25Mb2FkRGF0YS5hY3Rpb24gPT09IFwidXBkYXRlXCIpKSB7XHJcbiAgICAgICAgICBsZXQgaWRWYWwgPSByZXF1ZXN0RGV0YWlscy5tdWx0aXBsZUlkID8gdHJ1ZSA6IHNlbGYuaW5wdXREYXRhLl9pZDtcclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgICAgLnVwZGF0ZVJlcXVlc3QocmVxdWVzdERhdGEsIGFwaVVybCwgaWRWYWwpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDEgfHwgcmVzLnN0YXR1cyA9PSAyMDApIHtcclxuICAgICAgICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgICAgICAgICAgICAgIHNlbGYubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgIHNlbGYubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZWxmLnN1Ym1pdHRlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHNlbGYuY29udGVudFNlcnZpY2UuY3JlYXRlUmVxdWVzdChyZXF1ZXN0RGF0YSwgYXBpVXJsKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgaWYgKHJlcy5zdGF0dXMgPT0gMjAxKSB7XHJcbiAgICAgICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICAgICAgICAgIHNlbGYubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICBzZWxmLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgIHNlbGYuc3VibWl0dGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9KTtcclxuICB9XHJcbiAgb25GaWxlQ2hhbmdlKGV2ZW50LCBmaWxlVHlwZSwgaW1wb3J0RGF0YSkge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gb25GaWxlQ2hhbmdlIGV2ZW50IFwiLCBldmVudCk7XHJcbiAgICBsZXQgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgIGlmKGV2ZW50Lmxlbmd0aCl7XHJcbiAgICAgIGxldCBmaWxlID0gZXZlbnRbMF07XHJcbiAgICAgIGxldCBmaWxlTmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgY29uc29sZS5sb2coXCI+Pj4gZmlsZSBcIixmaWxlKTtcclxuICAgICAgY29uc29sZS5sb2coXCI+Pj4gZmlsZU5hbWUgXCIsZmlsZU5hbWUpO1xyXG4gICAgICB0aGlzLmZpbGVUeXBlID0gZmlsZS50eXBlO1xyXG4gICAgICBsZXQgZmlsZUV4dCA9IGZpbGVOYW1lLnNwbGl0KFwiLlwiKS5wb3AoKTtcclxuICAgICAgbGV0IGZpbGVWYWxpZGF0ZUZsYWc9ZmFsc2U7XHJcbiAgICAgIGlmIChpbXBvcnREYXRhICYmIGltcG9ydERhdGEubXVsdGlGaWxlU3VwcG9ydCkge1xyXG4gICAgICAgbGV0IHZhbGlkRmlsZVR5cGVzID0gaW1wb3J0RGF0YS5zdXBwb3J0ZWRGaWxlcy5zcGxpdChcIixcIik7XHJcbiAgICAgICAgIGZpbGVWYWxpZGF0ZUZsYWc9KHZhbGlkRmlsZVR5cGVzLmluZGV4T2YoZmlsZUV4dCkgPj0gMCk/dHJ1ZTpmYWxzZTsgXHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgIGZpbGVWYWxpZGF0ZUZsYWcgPSAoZmlsZUV4dCA9PSBmaWxlVHlwZSk/IHRydWU6IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIGlmKGZpbGVWYWxpZGF0ZUZsYWcpe1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiIFByb2NlZWQgXCIpO1xyXG4gICAgICAgIHJlYWRlci5yZWFkQXNEYXRhVVJMKGZpbGUpO1xyXG4gICAgICAgIHJlYWRlci5vbmxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICB0aGlzLmltYWdlID0gZmlsZTtcclxuICAgICAgICAgIHRoaXMuaW5wdXRHcm91cC5wYXRjaFZhbHVlKHtcclxuICAgICAgICAgICAgZmlsZTogcmVhZGVyLnJlc3VsdCxcclxuICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgdGhpcy5zZWxlY3RlZEZpbGVOYW1lID0gZmlsZS5uYW1lO1xyXG4gICAgICAgICAgdGhpcy5maWxlVXBsb2FkID0gZmlsZTtcclxuICAgICAgICAgIHRoaXMuY2hhbmdlRGV0ZWN0b3IubWFya0ZvckNoZWNrKCk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCIgRmlsZSBFeHRlbnNpb24gRXJyb3IgXCIpO1xyXG4gICAgICAgIHRoaXMuaW1wb3J0RGF0YS51cGxvYWRGaWxlID0ge307XHJcbiAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgXCJGaWxlIEV4dGVuc2lvbiBFcnJvci4gRmlsZSBleHRlbnNpb24gc2hvdWxkIGJlIFwiICtcclxuICAgICAgICAgICAgICBmaWxlVHlwZSArXHJcbiAgICAgICAgICAgICAgXCIgZm9ybWF0XCJcclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9ZWxzZXtcclxuICAgICAgY29uc29sZS5sb2coXCI+Pj4gZmlsZSByZXF1aXJlZCBcIik7XHJcbiAgICB9XHJcbiAgIFxyXG4gIH1cclxuICBvbkZpbGVDaGFuZ2VfYmFja3VwKGV2ZW50LCBmaWxlVHlwZSwgaW1wb3J0RGF0YSkge1xyXG4gICAgbGV0IHJlYWRlciA9IG5ldyBGaWxlUmVhZGVyKCk7XHJcbiAgICBpZiAoaW1wb3J0RGF0YSAmJiBpbXBvcnREYXRhLm11bHRpRmlsZVN1cHBvcnQpIHtcclxuICAgICAgbGV0IGZpbGUgPSBldmVudC50YXJnZXQuZmlsZXNbMF07XHJcbiAgICAgIHRoaXMuZmlsZVR5cGUgPSBmaWxlLnR5cGU7XHJcbiAgICAgIGxldCBmaWxlTmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgbGV0IGZpbGVFeHQgPSBmaWxlTmFtZS5zcGxpdChcIi5cIikucG9wKCk7XHJcbiAgICAgIGxldCB2YWxpZEZpbGVUeXBlcyA9IGltcG9ydERhdGEuc3VwcG9ydGVkRmlsZXMuc3BsaXQoXCIsXCIpO1xyXG4gICAgICBjb25zb2xlLmxvZyhmaWxlRXh0LCBcImltcG9ydERhdGEuc3VwcG9ydGVkRmlsZXMgPj4+Pj5cIiwgdmFsaWRGaWxlVHlwZXMpO1xyXG4gICAgICBpZiAodmFsaWRGaWxlVHlwZXMuaW5kZXhPZihmaWxlRXh0KSA+PSAwKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coXCIgUHJvY2VlZCBcIik7XHJcbiAgICAgICAgcmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XHJcbiAgICAgICAgcmVhZGVyLm9ubG9hZCA9ICgpID0+IHtcclxuICAgICAgICAgIHRoaXMuaW1hZ2UgPSBmaWxlO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dEdyb3VwLnBhdGNoVmFsdWUoe1xyXG4gICAgICAgICAgICBmaWxlOiByZWFkZXIucmVzdWx0LFxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0aGlzLnNlbGVjdGVkRmlsZU5hbWUgPSBmaWxlLm5hbWU7XHJcbiAgICAgICAgICB0aGlzLmZpbGVVcGxvYWQgPSBmaWxlO1xyXG4gICAgICAgICAgdGhpcy5jaGFuZ2VEZXRlY3Rvci5tYXJrRm9yQ2hlY2soKTtcclxuICAgICAgICB9O1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKFwiIEZpbGUgRXh0ZW5zaW9uIEVycm9yIFwiKTtcclxuICAgICAgICB0aGlzLmltcG9ydERhdGEudXBsb2FkRmlsZSA9IHt9O1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgIFwiRmlsZSBFeHRlbnNpb24gRXJyb3IuIEZpbGUgZXh0ZW5zaW9uIHNob3VsZCBiZSBcIiArXHJcbiAgICAgICAgICAgICAgaW1wb3J0RGF0YS5zdXBwb3J0ZWRGaWxlcyArXHJcbiAgICAgICAgICAgICAgXCIgZm9ybWF0XCJcclxuICAgICAgICAgIClcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICBpZiAoZXZlbnQudGFyZ2V0LmZpbGVzICYmIGV2ZW50LnRhcmdldC5maWxlcy5sZW5ndGgpIHtcclxuICAgICAgICBsZXQgZmlsZSA9IGV2ZW50LnRhcmdldC5maWxlc1swXTtcclxuICAgICAgICB0aGlzLmZpbGVUeXBlID0gZmlsZS50eXBlO1xyXG4gICAgICAgIGxldCBmaWxlTmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgICBjb25zb2xlLmxvZyhmaWxlTmFtZSwgXCIgRmlsZSBOYW1lXCIpO1xyXG4gICAgICAgIGxldCBmaWxlRXh0ID0gZmlsZU5hbWUuc3BsaXQoXCIuXCIpLnBvcCgpO1xyXG4gICAgICAgIC8vIGNvbnNvbGUubG9nKGZpbGVUeXBlLFwiIEZpbGUgVHlwZSBcIik7XHJcbiAgICAgICAgLy8gY29uc29sZS5sb2coZmlsZUV4dCxcIiBmaWxlRXh0IFwiKTtcclxuICAgICAgICBpZiAoZmlsZUV4dCA9PSBmaWxlVHlwZSkge1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCIgUHJvY2VlZCBcIik7XHJcbiAgICAgICAgICByZWFkZXIucmVhZEFzRGF0YVVSTChmaWxlKTtcclxuICAgICAgICAgIHJlYWRlci5vbmxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaW1hZ2UgPSBmaWxlO1xyXG4gICAgICAgICAgICB0aGlzLmlucHV0R3JvdXAucGF0Y2hWYWx1ZSh7XHJcbiAgICAgICAgICAgICAgZmlsZTogcmVhZGVyLnJlc3VsdCxcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRGaWxlTmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkID0gZmlsZTtcclxuICAgICAgICAgICAgdGhpcy5jaGFuZ2VEZXRlY3Rvci5tYXJrRm9yQ2hlY2soKTtcclxuICAgICAgICAgIH07XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiIEZpbGUgRXh0ZW5zaW9uIEVycm9yIFwiKTtcclxuICAgICAgICAgIHRoaXMuaW1wb3J0RGF0YS51cGxvYWRGaWxlID0ge307XHJcbiAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgXCJGaWxlIEV4dGVuc2lvbiBFcnJvci4gRmlsZSBleHRlbnNpb24gc2hvdWxkIGJlIFwiICtcclxuICAgICAgICAgICAgICAgIGZpbGVUeXBlICtcclxuICAgICAgICAgICAgICAgIFwiIGZvcm1hdFwiXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIGNsb3NlTW9kZWwoKSB7XHJcbiAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgfVxyXG4gIG9uSW1wb3J0KCkge1xyXG4gICAgdGhpcy5zdWJtaXR0ZWQgPSB0cnVlO1xyXG4gICAgbGV0IHJlcXVlc3REYXRhID0ge307XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICB0aGlzLmlucHV0RGF0YS5jcm9uRXhwcmVzc2lvbiA9IHRoaXMuY3JvbkV4cHJlc3Npb24gPyAgdGhpcy5jcm9uRXhwcmVzc2lvbiA6IFwicnVuT25jZVwiO1xyXG4gICAgbGV0IGltcG9ydERldGFpbHMgPSB0aGlzLmltcG9ydERhdGEub25Mb2FkRnVuY3Rpb247XHJcbiAgICBfLmZvckVhY2goaW1wb3J0RGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgaWYgKGl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0udmFsdWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0uc3ViS2V5XHJcbiAgICAgICAgICA/IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdW2l0ZW0uc3ViS2V5XVxyXG4gICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBpZiAodGhpcy5pbXBvcnREYXRhLnVwbG9hZEZpbGUpIHtcclxuICAgICAgdmFyIGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XHJcbiAgICAgIE9iamVjdC5rZXlzKHJlcXVlc3REYXRhKS5tYXAoKGtleSkgPT4ge1xyXG4gICAgICAgIGZvcm1EYXRhLmFwcGVuZChrZXksIHJlcXVlc3REYXRhW2tleV0pO1xyXG4gICAgICB9KTtcclxuICAgICAgZm9ybURhdGEuYXBwZW5kKFwiZmlsZVwiLCB0aGlzLmZpbGVVcGxvYWQpO1xyXG4gICAgICAgcmVxdWVzdERhdGEgPSBmb3JtRGF0YTsgXHJcbiAgICB9XHJcbiAgICBsZXQgdG9hc3RNZXNzYWdlRGV0YWlscyA9IGltcG9ydERldGFpbHMudG9hc3RNZXNzYWdlO1xyXG4gICAgY29uc29sZS5sb2codGhpcyxcIj4+Pj4+PiBUSElTXCIpXHJcbiAgICByZXF1ZXN0RGF0YVtcInJlYWxtXCJdPXRoaXMucmVhbG07XHJcbiAgICBpZiAodGhpcy5pbnB1dEdyb3VwICYmIHRoaXMuaW5wdXRHcm91cC52YWxpZCApIHtcclxuICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0YXJ0TG9hZGVyKCk7XHJcbiAgICAgIC8vdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgY29uc29sZS5sb2coXCJUaGlzLmlucHV0IC4uLi4uICBcIiwgdGhpcy5pbnB1dERhdGEpO1xyXG4gICAgICAvLyBkZWJ1Z2dlcjtcclxuICAgICAgaWYgKHRoaXMuaW1wb3J0RGF0YS5mdW5jdGlvbiAmJiB0aGlzLmltcG9ydERhdGEuZnVuY3Rpb24gPT0gXCJlZGl0XCIpIHsgLy8gcnVsZXNldCwgZGF0YXNvdXJjZSAtIHJlaW1wb3J0XHJcbiAgICAgICAgaWYodGhpcy5maWxlVXBsb2FkICE9PSB1bmRlZmluZWQgKVxyXG4gICAgICAgIHsgXHJcbiAgICAgICAgdGhpcy5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgLnVwZGF0ZVJlcXVlc3QoXHJcbiAgICAgICAgICAgIHJlcXVlc3REYXRhLFxyXG4gICAgICAgICAgICBpbXBvcnREZXRhaWxzLmFwaVVybCxcclxuICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJfaWRcIl1cclxuICAgICAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJkYXRhc291cmNlSWRcIl1cclxuICAgICAgICAgIClcclxuICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXMsIFwiLi4uLi5yZXNcIik7XHJcbiAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzID09IDIwMSB8fCByZXMuc3RhdHVzID09IDIwMCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuaW1wb3J0RGF0YS5zM3VwbG9hZCkge1xyXG4gICAgICAgICAgICAgICAgICBsZXQgcmVzcG9uc2UgPSByZXMuYm9keS5yZXNwb25zZTtcclxuICAgICAgICAgICAgICAgICAgaWYgKHJlc3BvbnNlICYmIHJlc3BvbnNlLmpvYikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVVwbG9hZChyZXNwb25zZS5qb2IuZGF0YXNvdXJjZSwgdGhpcy5pbXBvcnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5kYXRhc291cmNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIjw8PDxEQVRBIElNUE9SVD4+PlwiLCByZXNwb25zZS5kYXRhc291cmNlKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlVXBsb2FkKHJlc3BvbnNlLmRhdGFzb3VyY2UsIHRoaXMuaW1wb3J0RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgIH1lbHNlIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5ydWxlc2V0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIjw8PDxSdWxlc2V0IElNUE9SVD4+PlwiLCByZXNwb25zZS5ydWxlc2V0KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlVXBsb2FkKHJlc3BvbnNlLnJ1bGVzZXQsIHRoaXMuaW1wb3J0RGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgIC8vIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICAgIHRoaXMuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgdGhpcy5lcnJvclRhYmxlTGlzdCA9IHRoaXMuaW1wb3J0RGF0YS50YWJsZURhdGE7XHJcbiAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlID0gdGhpcy5pbXBvcnREYXRhO1xyXG4gICAgICAgICAgICAgIHRoaXMuZXJyb3JMb2dUYWJsZVtcInJlc3BvbnNlXCJdID0gZXJyb3IuZXJyb3IucmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlW1wicmVzcG9uc2VLZXlcIl0gPSBcImxvZ3NBcnJcIjtcclxuICAgICAgICAgICAgICB0aGlzLmVycm9yU2hvdyA9IGVycm9yLmVycm9yLnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMuZXJyb3JMb2dUYWJsZSwgXCJlcnJvckxvZz4+Pj4+Pj5cIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICk7XHJcbiAgICAgICB9ZWxzZXtcclxuICAgICAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RvcExvYWRlcigpO1xyXG4gICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLndhcm5pbmcoXHJcbiAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgIHRoaXMuaW1wb3J0RGF0YS52YWxpZGF0aW9ucy5tZXNzYWdlXHJcbiAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgICAgIH1cclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpZiAodGhpcy5pbnB1dERhdGEgJiYgdGhpcy5pbnB1dERhdGEuaW1wb3J0VHlwZSA9PSBcImF2bVwiKSB7XHJcbiAgICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlLmF2bUltcG9ydChyZXF1ZXN0RGF0YSwgdGhpcy5maWxlVXBsb2FkLCBpbXBvcnREZXRhaWxzLmF2bUFwaSkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2UgaWYodGhpcy5pbnB1dERhdGEgJiYgdGhpcy5pbnB1dERhdGEuaW1wb3J0VHlwZSA9PSBcInNvZEFnZW50XCIpe1xyXG4gICAgICAgICAgaWYoaW1wb3J0RGV0YWlscy5zb2RBZ2VudFJlcXVlc3QpIHtcclxuICAgICAgICAgICAgXy5mb3JFYWNoKGltcG9ydERldGFpbHMuc29kQWdlbnRSZXF1ZXN0LCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgIGlmIChpdGVtLmlzRGVmYXVsdCkge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0udmFsdWU7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIGlmKGl0ZW0uZnJvbVVzZXJEYXRhKXtcclxuICAgICAgICAgICAgICAgIGlmKGl0ZW0uc3ViS2V5ICYmIGl0ZW0uYXNzaWduZUZpcnN0VmFsdWUpe1xyXG4gICAgICAgICAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gKHNlbGYudXNlckRhdGEgJiYgc2VsZi51c2VyRGF0YVtpdGVtLnZhbHVlXSAmJiBzZWxmLnVzZXJEYXRhW2l0ZW0udmFsdWVdWzBdKSA/IHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV1bMF1baXRlbS5zdWJLZXldIDogXCJcIjtcclxuICAgICAgICAgICAgICAgIH1lbHNlIGlmKGl0ZW0uc3ViS2V5KXtcclxuICAgICAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IChzZWxmLnVzZXJEYXRhICYmIHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV0pID8gc2VsZi51c2VyRGF0YVtpdGVtLnZhbHVlXSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IChzZWxmLnVzZXJEYXRhICYmIHNlbGYudXNlckRhdGFbaXRlbS52YWx1ZV0pID8gc2VsZi51c2VyRGF0YVtpdGVtLnZhbHVlXSA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfWVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0uc3ViS2V5XHJcbiAgICAgICAgICAgICAgICAgID8gc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV1baXRlbS5zdWJLZXldXHJcbiAgICAgICAgICAgICAgICAgIDogc2VsZi5pbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMsXCI+Pj4+Pj4gVEhJU1wiKTtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+PiByZXF1ZXN0RGF0YSBcIixyZXF1ZXN0RGF0YSk7XHJcbiAgICAgICAgICBsZXQgcHJvZHV0TmFtZT1yZXF1ZXN0RGF0YSAmJiByZXF1ZXN0RGF0YVtcInByb2R1Y3ROYW1lXCJdP3JlcXVlc3REYXRhW1wicHJvZHVjdE5hbWVcIl06XCJFYnNcIjtcclxuICAgICAgICAgIGNvbnNvbGUubG9nKFwiPj4+IHByb2R1dE5hbWUgXCIscHJvZHV0TmFtZSk7XHJcbiAgICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5jcmVhdGVSZXF1ZXN0KHJlcXVlc3REYXRhLCBpbXBvcnREZXRhaWxzLnNvZEFnZW50Q3JlYXRlICsgdGhpcy5pbnB1dERhdGFbaW1wb3J0RGV0YWlscy5zb2RBZ2VudFBhcmFtZXRlcktleV0pXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzLCBcIi4uLi4ucmVzXCIpO1xyXG4gICAgICAgICAgICAgICAgaWYocmVzICYmIHJlcy5ib2R5ICYmIHJlcy5ib2R5LmRhdGEpe1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAuZ2V0UzNSZXNwb25zZShyZXF1ZXN0RGF0YSwgaW1wb3J0RGV0YWlscy5zb2RBZ2VudERvd25sb2FkQXBpIFxyXG4gICAgICAgICAgICAgICAgICAgICAgICArIHRoaXMuaW5wdXREYXRhW2ltcG9ydERldGFpbHMuc29kQWdlbnRQYXJhbWV0ZXJLZXldXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICsgJy8nICsgdGhpcy51c2VyRGF0YS5jdXN0b21lciArJy8nK3Byb2R1dE5hbWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhkYXRhLFwiPj4+PmRhdGFcIilcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIGxvY2FsZmlsZW5hbWUgPSBcIlNvZF9BZ2VudF9cIitwcm9kdXROYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIC8vdmFyIGxvY2FsZmlsZW5hbWUgPSBcIlNvZF9BZ2VudF9FYnNcIjtcclxuXHRcdFx0ICAgICAgICAgICAgICAgICAgICBsb2NhbGZpbGVuYW1lID0gbG9jYWxmaWxlbmFtZS5yZXBsYWNlKC9cXHMvZ2ksIFwiX1wiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkRXhwb3J0RmlsZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGEuYm9keSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxvY2FsZmlsZW5hbWUgKyAnLnppcCcsICdhcHBsaWNhdGlvbi96aXAnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFwiVW5hYmxlIFRvIERvd25sb2FkIGFnZW50IVwiKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICAgIFxyXG4gICAgICAgIH1lbHNlIHtcclxuICAgICAgICAgIGxldCBhcGl1cmw9aW1wb3J0RGV0YWlscy5hcGlVcmw7XHJcbiAgICAgICAgICBpZih0aGlzLmlucHV0RGF0YS5pbXBvcnRUeXBlPT0nd2Vic2VydmljZScpe1xyXG4gICAgICAgICAgIC8vIGFwaXVybD1pbXBvcnREZXRhaWxzLndlYnNlcnZpY2VBcGk7XHJcbiAgICAgICAgICAgIHRoaXMuaW1wb3J0RGF0YS5zM3VwbG9hZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICByZXF1ZXN0RGF0YVtcIndlYnNlcnZpY2VcIl0gPSB0cnVlO1xyXG4gICAgICAgICAgfSAgXHJcbiAgICAgICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5jcmVhdGVSZXF1ZXN0KHJlcXVlc3REYXRhLCBhcGl1cmwpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocmVzLCBcIi4uLi4ucmVzXCIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVzLnN0YXR1cyA9PSAyMDEgfHwgcmVzLnN0YXR1cyA9PSAyMDApIHtcclxuICAgICAgICAgICAgICAgICAgLy8gdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaW1wb3J0RGF0YS5zM3VwbG9hZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCByZXNwb25zZSA9IChyZXMuYm9keSAmJiByZXMuYm9keS5yZXNwb25zZSAmJnJlcy5ib2R5LnJlc3BvbnNlLmRhdGFzb3VyY2UpID8gcmVzLmJvZHkucmVzcG9uc2UuZGF0YXNvdXJjZSA6IHJlcy5ib2R5LnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXNwb25zZSAmJiByZXNwb25zZS5qb2IpIHtcclxuICAgICAgICAgICAgICAgICAgICAgIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLmltYWdlVXBsb2FkKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5qb2IuZGF0YXNvdXJjZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbXBvcnREYXRhXHJcbiAgICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgdGhpcy5pbWFnZVVwbG9hZChyZXNwb25zZSwgdGhpcy5pbXBvcnREYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZXMuc3RhdHVzID09IDIwNikge1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmVycm9yVGFibGVMaXN0ID0gdGhpcy5pbXBvcnREYXRhLnRhYmxlRGF0YTtcclxuICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlID0gdGhpcy5pbXBvcnREYXRhO1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmVycm9yTG9nVGFibGVbXCJyZXNwb25zZVwiXSA9IHJlcy5ib2R5LnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmVycm9yTG9nVGFibGVbXCJyZXNwb25zZUtleVwiXSA9IFwibG9nc0FyclwiO1xyXG4gICAgICAgICAgICAgICAgICB0aGlzLmVycm9yU2hvdyA9IHJlcy5ib2R5LnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVycm9yTG9nVGFibGUsXHJcbiAgICAgICAgICAgICAgICAgICAgXCJlcnJvckxvZyBvbiBkaWZmZXJlbnQgc3RhdHVzIGNvZGUgPj4+Pj4+PlwiXHJcbiAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIHRoaXMubWF0RGlhbG9nUmVmLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lcnJvclxyXG4gICAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvclRhYmxlTGlzdCA9IHRoaXMuaW1wb3J0RGF0YS50YWJsZURhdGE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yTG9nVGFibGUgPSB0aGlzLmltcG9ydERhdGE7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yTG9nVGFibGVbXCJyZXNwb25zZVwiXSA9IGVycm9yLmVycm9yLnJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lcnJvckxvZ1RhYmxlW1wicmVzcG9uc2VLZXlcIl0gPSBcImxvZ3NBcnJcIjtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JTaG93ID0gZXJyb3IuZXJyb3IucmVzcG9uc2U7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmVycm9yTG9nVGFibGUsIFwiZXJyb3JMb2c+Pj4+Pj4+XCIpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgc2VsZWN0SW1wb3J0VmlldyhldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQsIFwiLi4uLi5ldmVudFwiKTtcclxuICAgIHRoaXMuZW5hYmxlVGV4dEZpZWxkcyA9IGZhbHNlO1xyXG4gICAgdGhpcy5lbmFibGVPcHRpb25GaWVsZHMgPSBmYWxzZTtcclxuICAgIGxldCB0b2dnbGVGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlVG9nZ2xlRmllbGRzO1xyXG4gICAgaWYgKGV2ZW50ICE9IFwiY3N2XCIgJiYgZXZlbnQgIT0gXCJhdm1cIikge1xyXG4gICAgICB0aGlzLmVuYWJsZUltcG9ydERhdGEgPSBmYWxzZTtcclxuICAgICAgaWYgKHRvZ2dsZUZpZWxkcyAmJiB0b2dnbGVGaWVsZHMubWVyZ2VUZXh0RmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMgPSBfLmNvbmNhdChcclxuICAgICAgICAgIHRoaXMuZm9ybVZhbHVlcy50ZXh0RmllbGRzLFxyXG4gICAgICAgICAgdG9nZ2xlRmllbGRzLnRleHRGaWVsZHNcclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICAgIGlmKHRvZ2dsZUZpZWxkcyAmJiB0b2dnbGVGaWVsZHMuYWRkaXRpb25hbE9wdGlvbil7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmFkZGl0aW9uYWxPcHRpb25GaWVsZHMgPSB0b2dnbGVGaWVsZHMuYWRkaXRpb25hbE9wdGlvbkZpZWxkcztcclxuICAgICAgfVxyXG4gICAgICBpZih0b2dnbGVGaWVsZHMgJiYgdG9nZ2xlRmllbGRzLmVuYWJsZVNjaGVkdWxlKXtcclxuICAgICAgICB0aGlzLmVuYWJsZVNjaGVkdWxlID0gdHJ1ZTtcclxuICAgICAgfWVsc2V7XHJcbiAgICAgICAgdGhpcy5lbmFibGVTY2hlZHVsZSA9IGZhbHNlO1xyXG4gICAgICB9XHJcbiAgICAgIGlmKHRvZ2dsZUZpZWxkcyAmJiB0b2dnbGVGaWVsZHMuZW5hYmxlQWdlbnROb3RlKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmVuYWJsZUFnZW50Tm90ZSA9IHRvZ2dsZUZpZWxkcy5lbmFibGVBZ2VudE5vdGU7XHJcbiAgICAgIH1cclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICBpZiAodG9nZ2xlRmllbGRzLmlzU2V0RGVmYXVsdFZhbHVlKSB7XHJcbiAgICAgICAgXy5mb3JFYWNoKHRvZ2dsZUZpZWxkcy5hZGRpdGlvbmFsT3B0aW9uRmllbGRzLCBmdW5jdGlvbihvcHRpb25JdGVtKXtcclxuICAgICAgICAgIHNlbGYuaW5wdXREYXRhW1xyXG4gICAgICAgICAgICBvcHRpb25JdGVtLmRlZmF1bHRLZXlcclxuICAgICAgICAgIF0gPSBvcHRpb25JdGVtLmRlZmF1bHRWYWx1ZTtcclxuICAgICAgICB9KVxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuZW5hYmxlVGV4dEZpZWxkcyA9IHRydWU7XHJcbiAgICAgIHRoaXMuZW5hYmxlT3B0aW9uRmllbGRzID0gdHJ1ZTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmZvcm1WYWx1ZXMuZW5hYmxlQWdlbnROb3RlID0gZmFsc2U7XHJcbiAgICAgIHRoaXMuZW5hYmxlU2NoZWR1bGUgPSBmYWxzZTtcclxuICAgICAgdGhpcy5mb3JtVmFsdWVzLnRleHRGaWVsZHMgPSB0aGlzLmZvcm1WYWx1ZXMudGV4dEZpZWxkcy5maWx0ZXIoZnVuY3Rpb24gKFxyXG4gICAgICAgIGl0ZW1cclxuICAgICAgKSB7XHJcbiAgICAgICAgcmV0dXJuICF0b2dnbGVGaWVsZHMudGV4dEZpZWxkcy5pbmNsdWRlcyhpdGVtKTtcclxuICAgICAgfSk7XHJcbiAgICAgIGlmKHRvZ2dsZUZpZWxkcy5hZGRpdGlvbmFsT3B0aW9uRmllbGRzKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtVmFsdWVzLmFkZGl0aW9uYWxPcHRpb25GaWVsZHMgPSBbXVxyXG4gICAgICB9XHJcblxyXG4gICAgICB0aGlzLmVuYWJsZUltcG9ydERhdGEgPSB0cnVlO1xyXG4gICAgICAvLyB0aGlzLmZvcm1WYWx1ZXMudGV4dEZpZWxkcyA9XHJcbiAgICAgIHRoaXMuZW5hYmxlVGV4dEZpZWxkcyA9IHRydWU7XHJcbiAgICAgIHRoaXMuZW5hYmxlT3B0aW9uRmllbGRzID0gdHJ1ZTtcclxuICAgIH1cclxuICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICBpZiAodGhpcy5lbmFibGVUZXh0RmllbGRzKSB7XHJcbiAgICAgIF8uZm9yRWFjaCh0aGlzLmZvcm1WYWx1ZXMudGV4dEZpZWxkcywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICB0ZW1wT2JqW2l0ZW0ubmFtZV0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlmKHRvZ2dsZUZpZWxkcy5hZGRpdGlvbmFsT3B0aW9uKSB7XHJcbiAgICAgIHRlbXBPYmpbXCJldmVyeU1pbnV0ZVwiXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgdGVtcE9ialtcIm1haW5UeXBlXCJdID0gbmV3IEZvcm1Db250cm9sKFwiXCIpO1xyXG4gICAgICB0ZW1wT2JqW1wiaG91ckxpc3R2YWx1ZVwiXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgdGVtcE9ialtcImV2ZXJ5RGF5c1wiXSA9IG5ldyBGb3JtQ29udHJvbChcIlwiKTtcclxuICAgICAgdGVtcE9ialtcImRheVN0YXJ0aW5nT25cIl0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICAgIHRlbXBPYmpbXCJtb250aFN0YXJ0aW5nT25cIl0gPSBuZXcgRm9ybUNvbnRyb2woXCJcIik7XHJcbiAgICB9XHJcblxyXG4gICAgdGhpcy5pbnB1dEdyb3VwID0gbmV3IEZvcm1Hcm91cCh0ZW1wT2JqKTtcclxuICB9XHJcbiAgaW1hZ2VVcGxvYWQoZGF0YSwgaW1wb3J0RGF0YSkge1xyXG4gICAgY29uc29sZS5sb2coZGF0YSwgXCIuLi4uLmRhdGFcIik7XHJcbiAgICB0aGlzLkF3c1MzVXBsb2FkU2VydmljZS5maW5kKHtcclxuICAgICAgbWltZVR5cGU6IHRoaXMuZmlsZVR5cGUsXHJcbiAgICAgIHR5cGU6IGltcG9ydERhdGEuczNSZXF1ZXN0VHlwZSA/IGltcG9ydERhdGEuczNSZXF1ZXN0VHlwZSA6IFwiZGF0YXNvdXJjZVwiLFxyXG4gICAgfSkuc3Vic2NyaWJlKChzM0NyZWRlbnRpYWxzOiBhbnkpID0+IHtcclxuICAgICAgaWYgKHMzQ3JlZGVudGlhbHMpIHtcclxuICAgICAgICB0aGlzLkF3c1MzVXBsb2FkU2VydmljZS51cGxvYWRTMyhcclxuICAgICAgICAgIHRoaXMuaW1hZ2UsXHJcbiAgICAgICAgICBzM0NyZWRlbnRpYWxzLnJlc3BvbnNlLFxyXG4gICAgICAgICAgaW1wb3J0RGF0YVxyXG4gICAgICAgIClcclxuICAgICAgICAgIC50aGVuKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgY29uc29sZS5sb2cocmVzdWx0LCBcIi4uLnJlc3VsdFwiKTtcclxuICAgICAgICAgICAgbGV0IHVybCA9IGRlY29kZVVSSUNvbXBvbmVudChyZXN1bHQuUG9zdFJlc3BvbnNlLkxvY2F0aW9uKTtcclxuICAgICAgICAgICAgY29uc29sZS5sb2codXJsLCBcIi4uLnVybFwiKTtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJfaWRcIl0gPSBkYXRhLl9pZDtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJkYXRhc291cmNlUGF0aFwiXSA9IHVybDtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzdGF0dXNcIl0gPSBcIlVwbG9hZGVkXCI7XHJcbiAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICB0aGlzLmltcG9ydERhdGEuZnVuY3Rpb24gJiZcclxuICAgICAgICAgICAgICB0aGlzLmltcG9ydERhdGEuZnVuY3Rpb24gPT0gXCJlZGl0XCJcclxuICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJyZWltcG9ydFwiXSA9IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJyZWltcG9ydFwiXSA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGxldCByZXF1ZXN0RGF0YSA9IHt9O1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLCBcIi4uLi5USFNJSUlTU1NcIik7XHJcbiAgICAgICAgICAgIGxldCB1cGRhdGVEZXRhaWxzID0gdGhpcy5pbXBvcnREYXRhLnVwZGF0ZVJlcXVlc3Q7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHVwZGF0ZURldGFpbHMsIFwiLi4uLi51cGRhdGVEZXRhaWxzXCIpO1xyXG4gICAgICAgICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaCh1cGRhdGVEZXRhaWxzLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBzZWxmLmlucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcXVlc3REYXRhLCBcIi4uLi5SRVFVRVNUIERBVEFcIik7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAgICAgICAudXBkYXRlUmVxdWVzdChcclxuICAgICAgICAgICAgICAgIHJlcXVlc3REYXRhLFxyXG4gICAgICAgICAgICAgICAgdXBkYXRlRGV0YWlscy5hcGlVcmwsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmlucHV0RGF0YS5faWRcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgLnN1YnNjcmliZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTW9kZWxDbG9zZUV2ZW50KFwibGlzdFZpZXdcIik7XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZm9ybURhdGFbJ2VycF9pbnN0YW5jZSddID0gdGhpcy5jc3ZGb3JtLnZhbHVlLmVycF9pbnN0YW5jZTtcclxuICAgICAgICAgICAgLy8gdGhpcy5mb3JtRGF0YVsnbmFtZSddID0gdGhpcy5jc3ZGb3JtLnZhbHVlLm5hbWU7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuZm9ybURhdGFbJ3N0YXR1cyddID0gXCJVcGxvYWRlZFwiO1xyXG4gICAgICAgICAgICAvLyB0aGlzLmZvcm1EYXRhWydkc19wYXRoJ10gPSB1cmw7XHJcbiAgICAgICAgICAgIC8vIHRoaXMuU2V0dXBBZG1pbmlzdHJhdGlvblNlcnZpY2UucHV0SW1wb3J0Q1NWKHRoaXMuZm9ybURhdGEsIHRoaXMuaWQpLnN1YnNjcmliZSgoZGF0YU5ldzogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIC8vIFx0dGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoZGF0YU5ldy5zdGF0dXMpO1xyXG4gICAgICAgICAgICAvLyB9KTtcclxuICAgICAgICAgIH0pXHJcbiAgICAgICAgICAuY2F0Y2goKGVycm9yKSA9PiB7fSk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gIH1cclxuICBcclxuICAvL2ZvciBjcm9uIHNjaGVkdWxlIGV4cHJlc3Npb24gYnkgUmFqXHJcbiAgc2Vjb25kc0xpc3QgOiBhbnkgPSBbXTtcclxuICBzZWNvbmRzTGFzdERpZ2l0czogYW55ID0gW107XHJcbiAgaG91ckxpc3RWaWV3IDogYW55ID0gW107XHJcbiAgaG91cnN0d290aHJlZSA6IGFueSA9IFtdO1xyXG4gIGRheXMgOiBhbnkgPSBbXTtcclxuICBtb250aCA6IGFueSA9IFtdO1xyXG4gIG1vbnRoc0NvdW50IDogYW55ID0gW107XHJcbiAgZGF0ZUNvdW50IDogYW55ID0gW107XHJcbiAgbnVtT2ZkYXlzIDogYW55ID0gW107XHJcbiAgbnVtT2Ztb250aHMgOiBhbnkgPSBbXTtcclxuICBzZWNvbmRzVmlld1Nob3cgOiBib29sZWFuO1xyXG4gIGhvdXJWaWV3U2hvdyA6IGJvb2xlYW47XHJcbiAgTWludXRlc1ZpZXdTaG93IDogYm9vbGVhbjtcclxuICBkYXl2aWV3U2hvdyA6IGJvb2xlYW47XHJcbiAgbW9udGh2aWV3U2hvdyA6IGJvb2xlYW47XHJcbiAgZ2V0bWluIDogYm9vbGVhbjtcclxuICBjcm9wRXhwcmVzc2lvblNlbGVjdGlvbiA6IGFueSA9IHt9O1xyXG4gIHZpZXdPZnNlY29uZHMoKSB7XHJcbiAgICBmb3IgKGxldCBpID0gMTsgaSA8PSA2MDsgaSsrKSB7XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKGkpO1xyXG4gICAgICB0aGlzLnNlY29uZHNMaXN0LnB1c2goaSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnNlY29uZHNMYXN0RGlnaXRzID0gW107XHJcbiAgICB0aGlzLmhvdXJMaXN0VmlldyA9IFtdO1xyXG4gICAgdGhpcy5ob3Vyc3R3b3RocmVlID0gW107XHJcbiAgICBmb3IgKGxldCBqID0gMDsgaiA8PSA1OTsgaisrKSB7XHJcbiAgICAgIC8vIGNvbnNvbGUubG9nKGkpO1xyXG4gICAgICB0aGlzLnNlY29uZHNMYXN0RGlnaXRzLnB1c2goaik7XHJcbiAgICB9XHJcbiAgICBmb3IgKGxldCBrID0gMTsgayA8PSAyNDsgaysrKSB7XHJcbiAgICAgIHRoaXMuaG91ckxpc3RWaWV3LnB1c2goayk7XHJcbiAgICB9XHJcbiAgICBmb3IgKGxldCBob3VycyA9IDE7IGhvdXJzIDw9IDIzOyBob3VycysrKSB7XHJcbiAgICAgIHRoaXMuaG91cnN0d290aHJlZS5wdXNoKGhvdXJzKTtcclxuICAgIH1cclxuICAgIHRoaXMuZGF5cyA9IFtcIlN1bmRheVwiLCBcIk1vbmRheVwiLCBcIlR1ZXNkYXlcIiwgXCJXZWRuZXNkYXlcIiwgXCJUaHVyc2RheVwiLCBcIkZyaWRheVwiLCBcIlNhdHVyZGF5XCJdO1xyXG4gICAgdGhpcy5tb250aCA9IFtcIkphbnVhcnlcIiwgXCJGZWJydWFyeVwiLCBcIk1hcmNoXCIsIFwiQXByaWxcIiwgXCJNYXlcIiwgXCJKdW5lXCIsIFwiSnVseVwiLCBcIkF1Z3VzdFwiLCBcIlNlcHRlbWJlclwiLCBcIk9jdG9iZXJcIixcclxuICAgICAgXCJOb3ZlbWJlclwiLCBcIkRlY2VtYmVyIFwiXTtcclxuICAgIHRoaXMubW9udGhzQ291bnQgPSBbXTtcclxuICAgIHRoaXMuZGF0ZUNvdW50ID0gW107XHJcbiAgICBmb3IgKGxldCBkYXRlQ291bnRzID0gMTsgZGF0ZUNvdW50cyA8PSA3OyBkYXRlQ291bnRzKyspIHtcclxuICAgICAgdGhpcy5kYXRlQ291bnQucHVzaChkYXRlQ291bnRzKTtcclxuICAgIH1cclxuICAgIGZvciAobGV0IG1vbnRoQ291bnQgPSAxOyBtb250aENvdW50IDw9IDEyOyBtb250aENvdW50KyspIHtcclxuICAgICAgdGhpcy5tb250aHNDb3VudC5wdXNoKG1vbnRoQ291bnQpO1xyXG4gICAgfVxyXG4gICAgdGhpcy5udW1PZmRheXMgPSBbXTtcclxuICAgIHRoaXMuZGF5cy5mb3JFYWNoKGVsZW1lbnQgPT4ge1xyXG4gICAgICBjb25zb2xlLmxvZyhlbGVtZW50KTtcclxuICAgICAgdGhpcy5udW1PZmRheXMucHVzaChlbGVtZW50KTtcclxuICAgIH0pO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5udW1PZmRheXMpXHJcbiAgICB0aGlzLm51bU9mbW9udGhzID0gW107XHJcbiAgICB0aGlzLm1vbnRoLmZvckVhY2goZWxlbWVudCA9PiB7XHJcbiAgICAgIHRoaXMubnVtT2Ztb250aHMucHVzaChlbGVtZW50KTtcclxuICAgIH0pXHJcbiAgfVxyXG4gIHNlY29uZENoYW5nZSgpIHtcclxuICAgIHRoaXMuc2Vjb25kc1ZpZXdTaG93ID0gdHJ1ZTtcclxuICAgIHRoaXMuTWludXRlc1ZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLmhvdXJWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYXl2aWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5tb250aHZpZXdTaG93ID0gZmFsc2U7XHJcbiAgfVxyXG4gIG1pbnV0ZXNDaGFuZ2UoKSB7XHJcbiAgICB0aGlzLnNlY29uZHNWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5ob3VyVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuTWludXRlc1ZpZXdTaG93ID0gdHJ1ZTtcclxuICAgIHRoaXMuZGF5dmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGh2aWV3U2hvdyA9IGZhbHNlO1xyXG4gIH1cclxuICBob3VyQ2hhbmdlKCkge1xyXG4gICAgdGhpcy5ob3VyVmlld1Nob3cgPSB0cnVlO1xyXG4gICAgdGhpcy5NaW51dGVzVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuc2Vjb25kc1ZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLmRheXZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRodmlld1Nob3cgPSBmYWxzZTtcclxuICB9XHJcbiAgZGF5Q2hhbmdlKCkge1xyXG4gICAgdGhpcy5kYXl2aWV3U2hvdyA9IHRydWU7XHJcbiAgICB0aGlzLmhvdXJWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5NaW51dGVzVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuc2Vjb25kc1ZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRodmlld1Nob3cgPSBmYWxzZTtcclxuICB9XHJcbiAgbW9udGhDaGFuZ2UoKSB7XHJcbiAgICB0aGlzLm1vbnRodmlld1Nob3cgPSB0cnVlO1xyXG4gICAgdGhpcy5ob3VyVmlld1Nob3cgPSBmYWxzZTtcclxuICAgIHRoaXMuTWludXRlc1ZpZXdTaG93ID0gZmFsc2U7XHJcbiAgICB0aGlzLnNlY29uZHNWaWV3U2hvdyA9IGZhbHNlO1xyXG4gICAgdGhpcy5kYXl2aWV3U2hvdyA9IGZhbHNlO1xyXG4gIH1cclxuICBldmVyeU1pbnV0ZXMoZXZlbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKGV2ZW50LCBcInJyclwiKTtcclxuICAgIHRoaXMuZ2V0bWluID0gdHJ1ZTtcclxuICAgIGxldCBldmVyeU1pbnV0ZSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgY29uc29sZS5sb2coZXZlcnlNaW51dGUsXCI+Pj5ldmVyeU1pbnV0ZVwiKVxyXG4gICAgdGhpcy5jcm9wRXhwcmVzc2lvblNlbGVjdGlvbi5ldmVyeW1pbnV0ZU1haW4gPSBldmVyeU1pbnV0ZTtcclxuICAgIC8vIGNvbnNvbGUubG9nKHRoaXMub25QZXJtU2VsZWN0aW9uT3B0aW9uLmV2ZXJ5bWludXRlTWFpbik7XHJcbiAgfVxyXG4gIGhvdXJFeHAgOiBib29sZWFuO1xyXG4gIGRhdGVDRXhwIDogYm9vbGVhbjtcclxuICBtb250aFZpZXdmdWxsIDogYm9vbGVhbjtcclxuICBmdWxsTW9udGggOiBib29sZWFuO1xyXG4gIGRheXNFeHAgOiBib29sZWFuO1xyXG4gIGV2ZXJ5TWludXRlc0V4cCA6IGFueTtcclxuICBjcm9uRXhwcmVzc2lvbiA6IGFueTtcclxuICBldmVyeVNlY29uZE9mTWludXRlc0V4cCA6IGFueTtcclxuICBldmVySG91ckV4cCA6IGFueTtcclxuICBldmVyeU1pbnV0ZVNlbGVjdChlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlLCBcImRlZmVcIik7XHJcbiAgICB0aGlzLmdldG1pbiA9IHRydWU7XHJcbiAgICB0aGlzLmhvdXJFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMuZGF0ZUNFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGhWaWV3ZnVsbCA9IGZhbHNlO1xyXG4gICAgdGhpcy5mdWxsTW9udGggPSBmYWxzZVxyXG4gICAgdGhpcy5kYXlzRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmNyb25FeHByZXNzaW9uID0gJyovJysgZS52YWx1ZSArJyAqICogKiAqJztcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY3JvbkV4cHJlc3Npb24pO1xyXG4gIH1cclxuICBldmVyeVNlY29uZE9mTWludXRlcyhlKSB7XHJcbiAgICBjb25zb2xlLmxvZyhlLCBcImRkZGRcIik7XHJcbiAgICAvLyB0aGlzLmdldG1pbiA9IGZhbHNlO1xyXG4gICAgLy8gdGhpcy5ob3VyRXhwID0gdHJ1ZTtcclxuICAgIHRoaXMuZXZlcnlTZWNvbmRPZk1pbnV0ZXNFeHAgPSBlLnZhbHVlO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5ldmVyeVNlY29uZE9mTWludXRlc0V4cCk7XHJcbiAgfVxyXG4gIHNlbGVjdEhvdXIoZSkge1xyXG4gICAgdGhpcy5nZXRtaW4gPSBmYWxzZTtcclxuICAgIHRoaXMuaG91ckV4cCA9IHRydWU7XHJcbiAgICB0aGlzLmRhdGVDRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLm1vbnRoVmlld2Z1bGwgPSBmYWxzZTtcclxuICAgIHRoaXMuZnVsbE1vbnRoID0gZmFsc2VcclxuICAgIHRoaXMuZGF5c0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5ldmVySG91ckV4cCA9IGUudmFsdWU7XHJcbiAgICB0aGlzLmNyb25FeHByZXNzaW9uID0gXCIwIDAvXCIgK2UudmFsdWUgKycgKiAqIConO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5jcm9uRXhwcmVzc2lvbik7XHJcbiAgfVxyXG4gIGRhdGVDb3VudERldGFpbHNFeHAgOiBhbnk7XHJcbiAgZGF0ZUNvdW50cyhlKSB7XHJcbiAgICB0aGlzLmRhdGVDRXhwID0gdHJ1ZTtcclxuICAgIHRoaXMuZ2V0bWluID0gZmFsc2U7XHJcbiAgICB0aGlzLmhvdXJFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGhWaWV3ZnVsbCA9IGZhbHNlO1xyXG4gICAgdGhpcy5mdWxsTW9udGggPSBmYWxzZVxyXG4gICAgdGhpcy5kYXlzRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmRhdGVDb3VudERldGFpbHNFeHAgPSBlLnZhbHVlO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5kYXRlQ291bnREZXRhaWxzRXhwKTtcclxuICAgIGxldCBkYXlJZHggPSB0aGlzLmRheXMuaW5kZXhPZih0aGlzLmRheXNDb3VudHNFeHApXHJcbiAgICB0aGlzLmNyb25FeHByZXNzaW9uID1cIjAgMCAqL1wiKyBlLnZhbHVlICsgIFwiICogXCIrZGF5SWR4KyctNic7XHJcbiAgfVxyXG4gIGRheXNDb3VudHNFeHAgOiBhbnk7XHJcbiAgZGF5c0NvdW50cyhlKSB7XHJcbiAgICB0aGlzLmRheXNFeHAgPSB0cnVlO1xyXG4gICAgdGhpcy5kYXRlQ0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5nZXRtaW4gPSBmYWxzZTtcclxuICAgIHRoaXMuaG91ckV4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5tb250aFZpZXdmdWxsID0gZmFsc2U7XHJcbiAgICB0aGlzLmZ1bGxNb250aCA9IGZhbHNlXHJcbiAgICB0aGlzLmRheXNDb3VudHNFeHAgPSBlLnZhbHVlO1xyXG4gICAgbGV0IGRheUlkeCA9IHRoaXMuZGF5cy5pbmRleE9mKHRoaXMuZGF5c0NvdW50c0V4cClcclxuICAgIHRoaXMuY3JvbkV4cHJlc3Npb24gPVwiMCAwICovXCIrIHRoaXMuZGF0ZUNvdW50RGV0YWlsc0V4cCArICBcIiAqIFwiK2RheUlkeCsnLTYnO1xyXG4gICAgXHJcbiAgfVxyXG4gIG1vbnRoZXhwIDogYW55O1xyXG4gIG1vbnRoRXhwKGUpIHtcclxuICAgIHRoaXMuZGF0ZUNFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMuZ2V0bWluID0gZmFsc2U7XHJcbiAgICB0aGlzLmhvdXJFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMuZGF5c0V4cCA9IGZhbHNlO1xyXG4gICAgdGhpcy5tb250aFZpZXdmdWxsID0gdHJ1ZTtcclxuICAgIHRoaXMuZnVsbE1vbnRoID0gdHJ1ZVxyXG4gICAgdGhpcy5tb250aGV4cCA9IGUudmFsdWU7XHJcbiAgICBsZXQgbW50aElkeCA9IHRoaXMubW9udGguaW5kZXhPZih0aGlzLm1vbnRoZXhwKVxyXG4gICAgdGhpcy5jcm9uRXhwcmVzc2lvbiA9XCIwIDAgMSBcIisgbW50aElkeCArJy8nKyB0aGlzLm1vbnRoQ291bnRFeHAgKyAgXCIgKlwiO1xyXG4gIH1cclxuICBtb250aENvdW50RXhwIDogYW55XHJcbiAgbW9udGhDb3VudChlKSB7XHJcbiAgICB0aGlzLmRhdGVDRXhwID0gZmFsc2U7XHJcbiAgICB0aGlzLmRheXNFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMuZ2V0bWluID0gZmFsc2U7XHJcbiAgICB0aGlzLmhvdXJFeHAgPSBmYWxzZTtcclxuICAgIHRoaXMubW9udGhWaWV3ZnVsbCA9IGZhbHNlO1xyXG4gICAgdGhpcy5mdWxsTW9udGggPSB0cnVlO1xyXG4gICAgdGhpcy5tb250aENvdW50RXhwID0gZS52YWx1ZTtcclxuICAgIGxldCBtbnRoSWR4ID0gdGhpcy5tb250aC5pbmRleE9mKHRoaXMubW9udGhleHApXHJcbiAgICB0aGlzLmNyb25FeHByZXNzaW9uID1cIjAgMCAxIFwiKyBtbnRoSWR4ICsnLycrIGUudmFsdWUgKyAgXCIgKlwiO1xyXG4gIH1cclxuXHJcbiAgb25TZWxlY3Rpb25TdGFydERheShldmVudCkge31cclxuICBTZWxlY3RlZERheUNvdW50KGV2ZW50KSB7fVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgSW1wb3J0VmFsdWUge1xyXG4gIGRhdGFfc291cmNlOiBzdHJpbmc7XHJcbiAgZmlsZTogc3RyaW5nO1xyXG5cclxuICBjb25zdHJ1Y3Rvcihjb250cm9sKSB7XHJcbiAgICB0aGlzLmRhdGFfc291cmNlID0gY29udHJvbC5kYXRhX3NvdXJjZSB8fCBcIlwiO1xyXG4gICAgdGhpcy5maWxlID0gY29udHJvbC5maWxlIHx8IFwiXCI7XHJcbiAgfVxyXG59XHJcbiJdfQ==