import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { TranslateModule } from "@ngx-translate/core";
import { MaterialModule } from "../material.module";
import { FormLayoutComponent } from "../form-layout/form-layout.component";
import { TableLayoutModule } from "../table-layout/table-layout.module";
import { NewTableLayoutModule } from "../new-table-layout/new-table-layout.module";
export class FormLayoutModule {
}
FormLayoutModule.decorators = [
    { type: NgModule, args: [{
                declarations: [FormLayoutComponent],
                imports: [
                    RouterModule,
                    TableLayoutModule,
                    MaterialModule,
                    TranslateModule,
                    NewTableLayoutModule,
                    FuseSharedModule
                ],
                exports: [FormLayoutComponent]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1sYXlvdXQubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImZvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUMxRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzNFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBY25GLE1BQU0sT0FBTyxnQkFBZ0I7OztZQVo1QixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFLENBQUMsbUJBQW1CLENBQUM7Z0JBQ25DLE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCxlQUFlO29CQUNmLG9CQUFvQjtvQkFDcEIsZ0JBQWdCO2lCQUNqQjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxtQkFBbUIsQ0FBQzthQUMvQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSBcIi4uL0BmdXNlL3NoYXJlZC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSBcIkBuZ3gtdHJhbnNsYXRlL2NvcmVcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBGb3JtTGF5b3V0Q29tcG9uZW50IH0gZnJvbSBcIi4uL2Zvcm0tbGF5b3V0L2Zvcm0tbGF5b3V0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBUYWJsZUxheW91dE1vZHVsZSB9IGZyb20gXCIuLi90YWJsZS1sYXlvdXQvdGFibGUtbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBOZXdUYWJsZUxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9uZXctdGFibGUtbGF5b3V0L25ldy10YWJsZS1sYXlvdXQubW9kdWxlXCI7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gIGRlY2xhcmF0aW9uczogW0Zvcm1MYXlvdXRDb21wb25lbnRdLFxyXG4gIGltcG9ydHM6IFtcclxuICAgIFJvdXRlck1vZHVsZSxcclxuICAgIFRhYmxlTGF5b3V0TW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBUcmFuc2xhdGVNb2R1bGUsXHJcbiAgICBOZXdUYWJsZUxheW91dE1vZHVsZSxcclxuICAgIEZ1c2VTaGFyZWRNb2R1bGVcclxuICBdLFxyXG4gIGV4cG9ydHM6IFtGb3JtTGF5b3V0Q29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybUxheW91dE1vZHVsZSB7fVxyXG4iXX0=