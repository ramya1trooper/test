import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import "rxjs/Rx";
import { map } from "rxjs/operators";
import { LoaderService } from '../loader.service';
import { MessageService } from '../_services';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../loader.service";
import * as i3 from "../_services/message.service";
// import { MessageService } from "../_services/index";
export class ContentService {
    constructor(httpClient, loaderService, messageService, environment) {
        this.httpClient = httpClient;
        this.loaderService = loaderService;
        this.messageService = messageService;
        this.environment = environment;
        this.baseURL = this.environment.baseUrl;
        console.log(">>>>>>>>>>>>environment", this.environment);
    }
    getAllReponse(query, apiUrl) {
        let url = this.baseURL + apiUrl;
        // this.messageService.sendTriggerNotification({data : "trigger"})
        return this.httpClient.get(url, { params: query });
    }
    getResponse(query, apiUrl) {
        let url = this.baseURL + apiUrl;
        // this.messageService.sendTriggerNotification({data : "trigger"})
        return this.httpClient.get(url + query);
    }
    getExportResponse(query, apiUrl) {
        let url = this.baseURL + apiUrl;
        return this.httpClient.get(url, {
            params: query,
            responseType: "text"
        });
    }
    getS3Response(query, apiUrl) {
        let url = this.baseURL + apiUrl;
        // return this.httpClient.get<any>(url, {
        //   params: query,
        //   responseType: "text" as "json"
        // });
        return this.httpClient.get(url, { observe: 'response', responseType: 'blob' });
    }
    createRequest(data, apiUrl) {
        let url = this.baseURL + apiUrl;
        this.loaderService.startLoader();
        return this.httpClient
            .post(url, data, { observe: "response" })
            .pipe(map(resp => {
            this.loaderService.stopLoader();
            this.messageService.sendTriggerNotification({ data: "trigger" });
            return resp;
        }));
    }
    updateRequest(data, apiUrl, id) {
        let url = this.baseURL + apiUrl + id;
        this.loaderService.startLoader();
        return this.httpClient
            .put(url, data, { observe: "response" })
            .pipe(map(resp => {
            this.loaderService.stopLoader();
            this.messageService.sendTriggerNotification({ data: "trigger" });
            return resp;
        }));
    }
    avmImport(data, file, apiUrl) {
        var formData = new FormData();
        Object.keys(data).map((key) => {
            formData.append(key, data[key]);
        });
        formData.append('file', file);
        console.log('file >>', file);
        return this.httpClient.post(`${this.baseURL}` + apiUrl, formData, { observe: 'response' })
            .pipe(map(resp => {
            this.messageService.sendTriggerNotification({ data: "trigger" });
            return resp;
        }));
    }
}
ContentService.decorators = [
    { type: Injectable, args: [{
                providedIn: "root"
            },] }
];
/** @nocollapse */
ContentService.ctorParameters = () => [
    { type: HttpClient },
    { type: LoaderService },
    { type: MessageService },
    { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] }
];
ContentService.ngInjectableDef = i0.defineInjectable({ factory: function ContentService_Factory() { return new ContentService(i0.inject(i1.HttpClient), i0.inject(i2.LoaderService), i0.inject(i3.MessageService), i0.inject("environment")); }, token: ContentService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbImNvbnRlbnQvY29udGVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLFNBQVMsQ0FBQztBQUNqQixPQUFPLEVBQUUsR0FBRyxFQUFjLE1BQU0sZ0JBQWdCLENBQUM7QUFDakQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxjQUFjLENBQUE7Ozs7O0FBQzdDLHVEQUF1RDtBQUt2RCxNQUFNLE9BQU8sY0FBYztJQUd6QixZQUNVLFVBQXNCLEVBQ3RCLGFBQTJCLEVBQzNCLGNBQStCLEVBQ1IsV0FBVztRQUhsQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGtCQUFhLEdBQWIsYUFBYSxDQUFjO1FBQzNCLG1CQUFjLEdBQWQsY0FBYyxDQUFpQjtRQUNSLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBRTFDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUM7UUFDeEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVELGFBQWEsQ0FBQyxLQUFVLEVBQUUsTUFBVztRQUNuQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUNoQyxrRUFBa0U7UUFDbEUsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBTSxHQUFHLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUMsQ0FBQztJQUMxRCxDQUFDO0lBRUQsV0FBVyxDQUFDLEtBQVUsRUFBRSxNQUFXO1FBQ2pDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ2hDLGtFQUFrRTtRQUNsRSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFNLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBQ0QsaUJBQWlCLENBQUMsS0FBVSxFQUFFLE1BQVc7UUFDdkMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFDaEMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBTSxHQUFHLEVBQUU7WUFDbkMsTUFBTSxFQUFFLEtBQUs7WUFDYixZQUFZLEVBQUUsTUFBZ0I7U0FDL0IsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELGFBQWEsQ0FBQyxLQUFVLEVBQUUsTUFBVztRQUNuQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztRQUNoQyx5Q0FBeUM7UUFDekMsbUJBQW1CO1FBQ25CLG1DQUFtQztRQUNuQyxNQUFNO1FBQ04sT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBRSxNQUFnQixFQUFFLENBQUMsQ0FBQztJQUUzRixDQUFDO0lBRUQsYUFBYSxDQUFDLElBQVMsRUFBRSxNQUFXO1FBQ2xDLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBQ2hDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsVUFBVTthQUNuQixJQUFJLENBQU0sR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQzthQUM3QyxJQUFJLENBQ0gsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLEVBQUMsSUFBSSxFQUFHLFNBQVMsRUFBQyxDQUFDLENBQUE7WUFDL0QsT0FBTyxJQUFJLENBQUM7UUFFZCxDQUFDLENBQUMsQ0FDSCxDQUFDO0lBQ04sQ0FBQztJQUVELGFBQWEsQ0FBQyxJQUFTLEVBQUUsTUFBVyxFQUFFLEVBQUU7UUFDdEMsSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ3JDLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLENBQUM7UUFDakMsT0FBTyxJQUFJLENBQUMsVUFBVTthQUNuQixHQUFHLENBQU0sR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQzthQUM1QyxJQUFJLENBQ0gsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ1QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsQ0FBQztZQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLEVBQUMsSUFBSSxFQUFHLFNBQVMsRUFBQyxDQUFDLENBQUE7WUFDL0QsT0FBTyxJQUFJLENBQUM7UUFDZCxDQUFDLENBQUMsQ0FDSCxDQUFBO0lBQ0wsQ0FBQztJQUNELFNBQVMsQ0FBQyxJQUFTLEVBQUUsSUFBUyxFQUFFLE1BQVk7UUFDNUMsSUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztRQUM5QixNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO1lBQzdCLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsUUFBUSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDOUIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUMsSUFBSSxDQUFDLENBQUE7UUFDM0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBTSxHQUFHLElBQUksQ0FBQyxPQUFPLEVBQUUsR0FBRyxNQUFNLEVBQUUsUUFBUSxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDO2FBQzdGLElBQUksQ0FDSixHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDTCxJQUFJLENBQUMsY0FBYyxDQUFDLHVCQUF1QixDQUFDLEVBQUMsSUFBSSxFQUFHLFNBQVMsRUFBQyxDQUFDLENBQUE7WUFDcEUsT0FBTyxJQUFJLENBQUM7UUFDYixDQUFDLENBQUMsQ0FDRixDQUFBO0lBQ0gsQ0FBQzs7O1lBdEZELFVBQVUsU0FBQztnQkFDVixVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQVRRLFVBQVU7WUFHVixhQUFhO1lBQ2IsY0FBYzs0Q0FhbEIsTUFBTSxTQUFDLGFBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPYnNlcnZhYmxlLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tIFwicnhqcy9cIjtcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gXCJAYW5ndWxhci9jb21tb24vaHR0cFwiO1xyXG5pbXBvcnQgXCJyeGpzL1J4XCI7XHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciB9IGZyb20gXCJyeGpzL29wZXJhdG9yc1wiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gJy4uL19zZXJ2aWNlcydcclxuLy8gaW1wb3J0IHsgTWVzc2FnZVNlcnZpY2UgfSBmcm9tIFwiLi4vX3NlcnZpY2VzL2luZGV4XCI7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgcHJvdmlkZWRJbjogXCJyb290XCJcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRlbnRTZXJ2aWNlIHtcclxuICBiYXNlVVJMOiBzdHJpbmc7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxyXG4gICAgcHJpdmF0ZSBsb2FkZXJTZXJ2aWNlOkxvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIG1lc3NhZ2VTZXJ2aWNlIDogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBASW5qZWN0KFwiZW52aXJvbm1lbnRcIikgcHJpdmF0ZSBlbnZpcm9ubWVudFxyXG4gICkge1xyXG4gICAgdGhpcy5iYXNlVVJMID0gdGhpcy5lbnZpcm9ubWVudC5iYXNlVXJsO1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+Pj4+Pj5lbnZpcm9ubWVudFwiLCB0aGlzLmVudmlyb25tZW50KTtcclxuICB9XHJcblxyXG4gIGdldEFsbFJlcG9uc2UocXVlcnk6IGFueSwgYXBpVXJsOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMuYmFzZVVSTCArIGFwaVVybDtcclxuICAgIC8vIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRyaWdnZXJOb3RpZmljYXRpb24oe2RhdGEgOiBcInRyaWdnZXJcIn0pXHJcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxhbnk+KHVybCwgeyBwYXJhbXM6IHF1ZXJ5IH0pO1xyXG4gIH1cclxuXHJcbiAgZ2V0UmVzcG9uc2UocXVlcnk6IGFueSwgYXBpVXJsOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMuYmFzZVVSTCArIGFwaVVybDtcclxuICAgIC8vIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFRyaWdnZXJOb3RpZmljYXRpb24oe2RhdGEgOiBcInRyaWdnZXJcIn0pXHJcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxhbnk+KHVybCArIHF1ZXJ5KTtcclxuICB9XHJcbiAgZ2V0RXhwb3J0UmVzcG9uc2UocXVlcnk6IGFueSwgYXBpVXJsOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMuYmFzZVVSTCArIGFwaVVybDtcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0PGFueT4odXJsLCB7XHJcbiAgICAgIHBhcmFtczogcXVlcnksXHJcbiAgICAgIHJlc3BvbnNlVHlwZTogXCJ0ZXh0XCIgYXMgXCJqc29uXCJcclxuICAgIH0pO1xyXG4gIH1cclxuICBnZXRTM1Jlc3BvbnNlKHF1ZXJ5OiBhbnksIGFwaVVybDogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIGxldCB1cmwgPSB0aGlzLmJhc2VVUkwgKyBhcGlVcmw7XHJcbiAgICAvLyByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxhbnk+KHVybCwge1xyXG4gICAgLy8gICBwYXJhbXM6IHF1ZXJ5LFxyXG4gICAgLy8gICByZXNwb25zZVR5cGU6IFwidGV4dFwiIGFzIFwianNvblwiXHJcbiAgICAvLyB9KTtcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQuZ2V0KHVybCwgeyBvYnNlcnZlOiAncmVzcG9uc2UnLCByZXNwb25zZVR5cGU6ICdibG9iJyBhcyAnanNvbicgfSk7XHJcblxyXG4gIH1cclxuXHJcbiAgY3JlYXRlUmVxdWVzdChkYXRhOiBhbnksIGFwaVVybDogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgIGxldCB1cmwgPSB0aGlzLmJhc2VVUkwgKyBhcGlVcmw7XHJcbiAgICB0aGlzLmxvYWRlclNlcnZpY2Uuc3RhcnRMb2FkZXIoKTtcclxuICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnRcclxuICAgICAgLnBvc3Q8YW55Pih1cmwsIGRhdGEsIHsgb2JzZXJ2ZTogXCJyZXNwb25zZVwiIH0pXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIG1hcChyZXNwID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7ICAgXHJcbiAgICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRUcmlnZ2VyTm90aWZpY2F0aW9uKHtkYXRhIDogXCJ0cmlnZ2VyXCJ9KVxyXG4gICAgICAgICAgcmV0dXJuIHJlc3A7XHJcbiAgICAgICAgICAgXHJcbiAgICAgICAgfSlcclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIHVwZGF0ZVJlcXVlc3QoZGF0YTogYW55LCBhcGlVcmw6IGFueSwgaWQpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgbGV0IHVybCA9IHRoaXMuYmFzZVVSTCArIGFwaVVybCArIGlkO1xyXG4gICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0YXJ0TG9hZGVyKCk7XHJcbiAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50XHJcbiAgICAgIC5wdXQ8YW55Pih1cmwsIGRhdGEsIHsgb2JzZXJ2ZTogXCJyZXNwb25zZVwiIH0pXHJcbiAgICAgIC5waXBlKFxyXG4gICAgICAgIG1hcChyZXNwID0+IHtcclxuICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7ICAgXHJcbiAgICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRUcmlnZ2VyTm90aWZpY2F0aW9uKHtkYXRhIDogXCJ0cmlnZ2VyXCJ9KVxyXG4gICAgICAgICAgcmV0dXJuIHJlc3A7XHJcbiAgICAgICAgfSlcclxuICAgICAgKVxyXG4gIH1cclxuICBhdm1JbXBvcnQoZGF0YTogYW55LCBmaWxlOiBhbnksIGFwaVVybCA6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcblx0XHR2YXIgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuXHRcdE9iamVjdC5rZXlzKGRhdGEpLm1hcCgoa2V5KSA9PiB7XHJcblx0XHRcdGZvcm1EYXRhLmFwcGVuZChrZXksIGRhdGFba2V5XSk7XHJcblx0XHR9KTtcclxuXHRcdGZvcm1EYXRhLmFwcGVuZCgnZmlsZScsIGZpbGUpO1xyXG5cdFx0Y29uc29sZS5sb2coJ2ZpbGUgPj4nLGZpbGUpXHJcblx0XHRyZXR1cm4gdGhpcy5odHRwQ2xpZW50LnBvc3Q8YW55PihgJHt0aGlzLmJhc2VVUkx9YCArIGFwaVVybCwgZm9ybURhdGEsIHsgb2JzZXJ2ZTogJ3Jlc3BvbnNlJyB9KVxyXG5cdFx0XHQucGlwZShcclxuXHRcdFx0XHRtYXAocmVzcCA9PiB7XHJcbiAgICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRUcmlnZ2VyTm90aWZpY2F0aW9uKHtkYXRhIDogXCJ0cmlnZ2VyXCJ9KVxyXG5cdFx0XHRcdFx0cmV0dXJuIHJlc3A7XHJcblx0XHRcdFx0fSlcclxuXHRcdFx0KVxyXG5cdH1cclxufVxyXG4iXX0=