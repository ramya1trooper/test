import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { FuseSharedModule } from "../@fuse/shared.module";
import { ContentComponent } from "./content.component";
import { TableLayoutModule } from "../table-layout/table-layout.module";
import { ButtonLayoutModule } from "../button-layout/button-layout.module";
import { MaterialModule } from "../material.module";
import { FormLayoutModule } from "../form-layout/form-layout.module";
import { NewTableLayoutModule } from "../new-table-layout/new-table-layout.module";
//from
import { CardLayoutModule } from "../card-layout/card-layout.module";
import { TranslateModule } from "@ngx-translate/core";
// import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HttpClientModule } from "@angular/common/http";
import { ChartLayoutModule } from "../chart-layout/chart-layout.module";
import { GridListLayoutModule } from "../grid-list-layout/grid-list-layout.module";
import { ReportManagementModule } from '../avm/report-management.module';
//to this
export class ContentModule {
    static forRoot(metaData, environment, english) {
        return {
            ngModule: ContentModule,
            providers: [
                {
                    provide: "metaData",
                    useValue: metaData
                },
                {
                    provide: "environment",
                    useValue: environment
                },
                {
                    provide: "english",
                    useValue: english
                }
            ]
        };
    }
}
ContentModule.decorators = [
    { type: NgModule, args: [{
                declarations: [ContentComponent],
                imports: [
                    RouterModule,
                    FuseSharedModule,
                    TableLayoutModule,
                    ButtonLayoutModule,
                    FormLayoutModule,
                    MaterialModule,
                    ReportManagementModule,
                    NewTableLayoutModule,
                    //from
                    CardLayoutModule,
                    GridListLayoutModule,
                    ChartLayoutModule,
                    HttpClientModule,
                    TranslateModule.forRoot()
                    //to this
                ],
                exports: [
                    ContentComponent,
                    MaterialModule,
                    //from
                    TranslateModule
                    //to this
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiY29udGVudC9jb250ZW50Lm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUF1QixNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFMUQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDeEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDM0UsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBRXJFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ25GLE1BQU07QUFDTixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsZUFBZSxFQUFtQixNQUFNLHFCQUFxQixDQUFDO0FBQ3ZFLGtFQUFrRTtBQUNsRSxPQUFPLEVBQWMsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUdwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNuRixPQUFPLEVBQUMsc0JBQXNCLEVBQUMsTUFBTSxpQ0FBaUMsQ0FBQztBQUN2RSxTQUFTO0FBNkJULE1BQU0sT0FBTyxhQUFhO0lBQ3hCLE1BQU0sQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxPQUFPO1FBQzNDLE9BQU87WUFDTCxRQUFRLEVBQUUsYUFBYTtZQUN2QixTQUFTLEVBQUU7Z0JBQ1Q7b0JBQ0UsT0FBTyxFQUFFLFVBQVU7b0JBQ25CLFFBQVEsRUFBRSxRQUFRO2lCQUNuQjtnQkFDRDtvQkFDRSxPQUFPLEVBQUUsYUFBYTtvQkFDdEIsUUFBUSxFQUFFLFdBQVc7aUJBQ3RCO2dCQUNEO29CQUNFLE9BQU8sRUFBRSxTQUFTO29CQUNsQixRQUFRLEVBQUUsT0FBTztpQkFDbEI7YUFDRjtTQUNGLENBQUM7SUFDSixDQUFDOzs7WUE5Q0YsUUFBUSxTQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLGdCQUFnQixDQUFDO2dCQUNoQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtvQkFDWixnQkFBZ0I7b0JBQ2hCLGlCQUFpQjtvQkFDakIsa0JBQWtCO29CQUNsQixnQkFBZ0I7b0JBQ2hCLGNBQWM7b0JBQ2Qsc0JBQXNCO29CQUN0QixvQkFBb0I7b0JBQ3BCLE1BQU07b0JBQ04sZ0JBQWdCO29CQUNoQixvQkFBb0I7b0JBQ3BCLGlCQUFpQjtvQkFDakIsZ0JBQWdCO29CQUNoQixlQUFlLENBQUMsT0FBTyxFQUFFO29CQUN6QixTQUFTO2lCQUNWO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxnQkFBZ0I7b0JBQ2hCLGNBQWM7b0JBQ2QsTUFBTTtvQkFDTixlQUFlO29CQUNmLFNBQVM7aUJBQ1Y7YUFDRiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBNb2R1bGVXaXRoUHJvdmlkZXJzIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcclxuaW1wb3J0IHsgUm91dGVyTW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL3JvdXRlclwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVNoYXJlZE1vZHVsZSB9IGZyb20gXCIuLi9AZnVzZS9zaGFyZWQubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBDb250ZW50Q29tcG9uZW50IH0gZnJvbSBcIi4vY29udGVudC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgVGFibGVMYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vdGFibGUtbGF5b3V0L3RhYmxlLWxheW91dC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgQnV0dG9uTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2J1dHRvbi1sYXlvdXQvYnV0dG9uLWxheW91dC5tb2R1bGVcIjtcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tIFwiLi4vbWF0ZXJpYWwubW9kdWxlXCI7XHJcbmltcG9ydCB7IEZvcm1MYXlvdXRNb2R1bGUgfSBmcm9tIFwiLi4vZm9ybS1sYXlvdXQvZm9ybS1sYXlvdXQubW9kdWxlXCI7XHJcblxyXG5pbXBvcnQgeyBOZXdUYWJsZUxheW91dE1vZHVsZSB9IGZyb20gXCIuLi9uZXctdGFibGUtbGF5b3V0L25ldy10YWJsZS1sYXlvdXQubW9kdWxlXCI7XHJcbi8vZnJvbVxyXG5pbXBvcnQgeyBDYXJkTGF5b3V0TW9kdWxlIH0gZnJvbSBcIi4uL2NhcmQtbGF5b3V0L2NhcmQtbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUsIFRyYW5zbGF0ZUxvYWRlciB9IGZyb20gXCJAbmd4LXRyYW5zbGF0ZS9jb3JlXCI7XHJcbi8vIGltcG9ydCB7VHJhbnNsYXRlSHR0cExvYWRlcn0gZnJvbSAnQG5neC10cmFuc2xhdGUvaHR0cC1sb2FkZXInO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcbmltcG9ydCB7IE5neENoYXJ0c01vZHVsZSB9IGZyb20gXCJAc3dpbWxhbmUvbmd4LWNoYXJ0c1wiO1xyXG5pbXBvcnQgeyBDaGFydHNNb2R1bGUgfSBmcm9tIFwibmcyLWNoYXJ0c1wiO1xyXG5pbXBvcnQgeyBDaGFydExheW91dE1vZHVsZSB9IGZyb20gXCIuLi9jaGFydC1sYXlvdXQvY2hhcnQtbGF5b3V0Lm1vZHVsZVwiO1xyXG5pbXBvcnQgeyBHcmlkTGlzdExheW91dE1vZHVsZSB9IGZyb20gXCIuLi9ncmlkLWxpc3QtbGF5b3V0L2dyaWQtbGlzdC1sYXlvdXQubW9kdWxlXCI7XHJcbmltcG9ydCB7UmVwb3J0TWFuYWdlbWVudE1vZHVsZX0gZnJvbSAnLi4vYXZtL3JlcG9ydC1tYW5hZ2VtZW50Lm1vZHVsZSc7XHJcbi8vdG8gdGhpc1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICBkZWNsYXJhdGlvbnM6IFtDb250ZW50Q29tcG9uZW50XSxcclxuICBpbXBvcnRzOiBbXHJcbiAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICBGdXNlU2hhcmVkTW9kdWxlLFxyXG4gICAgVGFibGVMYXlvdXRNb2R1bGUsXHJcbiAgICBCdXR0b25MYXlvdXRNb2R1bGUsXHJcbiAgICBGb3JtTGF5b3V0TW9kdWxlLFxyXG4gICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICBSZXBvcnRNYW5hZ2VtZW50TW9kdWxlLFxyXG4gICAgTmV3VGFibGVMYXlvdXRNb2R1bGUsXHJcbiAgICAvL2Zyb21cclxuICAgIENhcmRMYXlvdXRNb2R1bGUsXHJcbiAgICBHcmlkTGlzdExheW91dE1vZHVsZSxcclxuICAgIENoYXJ0TGF5b3V0TW9kdWxlLFxyXG4gICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JSb290KClcclxuICAgIC8vdG8gdGhpc1xyXG4gIF0sXHJcbiAgZXhwb3J0czogW1xyXG4gICAgQ29udGVudENvbXBvbmVudCxcclxuICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgLy9mcm9tXHJcbiAgICBUcmFuc2xhdGVNb2R1bGVcclxuICAgIC8vdG8gdGhpc1xyXG4gIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRlbnRNb2R1bGUge1xyXG4gIHN0YXRpYyBmb3JSb290KG1ldGFEYXRhLCBlbnZpcm9ubWVudCwgZW5nbGlzaCk6IE1vZHVsZVdpdGhQcm92aWRlcnMge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgbmdNb2R1bGU6IENvbnRlbnRNb2R1bGUsXHJcbiAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgIHByb3ZpZGU6IFwibWV0YURhdGFcIixcclxuICAgICAgICAgIHVzZVZhbHVlOiBtZXRhRGF0YVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgcHJvdmlkZTogXCJlbnZpcm9ubWVudFwiLFxyXG4gICAgICAgICAgdXNlVmFsdWU6IGVudmlyb25tZW50XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBwcm92aWRlOiBcImVuZ2xpc2hcIixcclxuICAgICAgICAgIHVzZVZhbHVlOiBlbmdsaXNoXHJcbiAgICAgICAgfVxyXG4gICAgICBdXHJcbiAgICB9O1xyXG4gIH1cclxufVxyXG4iXX0=