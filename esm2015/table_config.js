export const manageAvTableConfig = [
    {
        value: 'controlname',
        name: 'Control Name',
        isactive: true
    },
    {
        value: 'description',
        name: 'Description',
        isactive: true
    },
    {
        value: 'usercount',
        name: 'User Count',
        isactive: true,
        onSelect: true
    },
    {
        value: 'datasource',
        name: 'Datasource',
        isactive: true
    },
    {
        value: 'controltype',
        name: 'Control Type',
        isactive: true
    }
];
export const manageAvDetailTableConfig = [
    {
        value: 'sodname',
        name: 'Control Name',
        isactive: true
    },
    {
        value: 'username',
        name: 'User Name',
        isactive: true,
        onSelect: true
    },
    {
        value: 'firstname',
        name: 'First Name',
        isactive: true
    },
    {
        value: 'lastname',
        name: 'Last Name',
        isactive: true
    }
    // {
    // 	value: 'entitlement',
    // 	name: "Entitlement",
    // 	isactive: false
    // }
];
export const manageAvbyLedgerUserTableConfig = [
    {
        value: 'ledger',
        name: 'Ledger',
        isactive: true
    },
    {
        value: 'journalname',
        name: 'Journal Name',
        isactive: true
    },
    {
        value: 'journaldescription',
        name: 'Journal Description',
        isactive: true
    },
    {
        value: 'journalsource',
        name: 'Journal Source',
        isactive: true
    },
    {
        value: 'journalcategory',
        name: 'Journal Category',
        isactive: true
    },
    {
        value: 'journalperiod',
        name: 'Journal Period',
        isactive: true
    },
    {
        value: 'currencycode',
        name: 'Currency Code',
        isactive: false
    },
    {
        value: 'createdby',
        name: 'Created By',
        isactive: true
    },
    {
        value: 'postedby',
        name: 'Posted By',
        isactive: false
    },
    {
        value: 'amount',
        name: 'Amount',
        isactive: true
    },
    {
        value: 'posteddate',
        name: 'Posted Date',
        isactive: false
    }
];
export const manageAvbyVendorUserTableConfig = [
    {
        value: 'vendorname',
        name: 'Vendor Name',
        isactive: true
    },
    {
        value: 'vendornum',
        name: 'Vendor Number',
        isactive: true
    },
    {
        value: 'invoiceid',
        name: 'Invoice ID',
        isactive: true
    },
    {
        value: 'invoicenum',
        name: 'Invoice Number',
        isactive: true
    },
    {
        value: 'invoiceamount',
        name: 'Invoice Amount',
        isactive: true
    },
    {
        value: 'amountpaid',
        name: 'Amount Paid',
        isactive: true
    },
    {
        value: 'source',
        name: 'Source',
        isactive: false
    },
    {
        value: 'paymentstatus',
        name: 'Payment Status',
        isactive: false
    },
    {
        value: 'createdby',
        name: 'Created By',
        isactive: true
    },
    {
        value: 'checkamount',
        name: 'Check Amount',
        isactive: false
    },
    {
        value: 'checkdate',
        name: 'Check Date',
        isactive: false
    }
];
export const manageAvbyInvoiceUserTableConfig = [
    {
        value: 'invoice_id',
        name: 'Invoice Id',
        isactive: true
    },
    {
        value: 'invoice_num',
        name: 'Invoice Number',
        isactive: true
    },
    {
        value: 'invoice_currency',
        name: 'Invoice Currency',
        isactive: false
    },
    {
        value: 'invoice_amount',
        name: 'Invoice Amount',
        isactive: true
    },
    {
        value: 'invoice_date',
        name: 'Invoice Date',
        isactive: true
    },
    {
        value: 'source',
        name: 'Source',
        isactive: true
    },
    {
        value: 'invoice_type',
        name: 'Invoice Type',
        isactive: true
    },
    {
        value: 'base_amount',
        name: 'Base Amount',
        isactive: false
    },
    {
        value: 'site_updated_by',
        name: 'Site Updated By',
        isactive: false
    },
    {
        value: 'site_created_by',
        name: 'Site Created By',
        isactive: false
    },
    {
        value: 'invoice_created_by',
        name: 'Invoice Created By',
        isactive: true
    },
    {
        value: 'invoice_updated_by',
        name: 'Invoice Updated By',
        isactive: false
    }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGVfY29uZmlnLmpzIiwic291cmNlUm9vdCI6Im5nOi8vdWktY29tbW9uLWxpYi8iLCJzb3VyY2VzIjpbInRhYmxlX2NvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQSxNQUFNLENBQUMsTUFBTSxtQkFBbUIsR0FBRztJQUNqQztRQUNFLEtBQUssRUFBRSxhQUFhO1FBQ3BCLElBQUksRUFBRSxjQUFjO1FBQ3BCLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxhQUFhO1FBQ3BCLElBQUksRUFBRSxhQUFhO1FBQ25CLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxXQUFXO1FBQ2xCLElBQUksRUFBRSxZQUFZO1FBQ2xCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFlBQVk7UUFDbkIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGFBQWE7UUFDcEIsSUFBSSxFQUFFLGNBQWM7UUFDcEIsUUFBUSxFQUFFLElBQUk7S0FDZjtDQUNGLENBQUM7QUFDRixNQUFNLENBQUMsTUFBTSx5QkFBeUIsR0FBRztJQUN2QztRQUNFLEtBQUssRUFBRSxTQUFTO1FBQ2hCLElBQUksRUFBRSxjQUFjO1FBQ3BCLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxVQUFVO1FBQ2pCLElBQUksRUFBRSxXQUFXO1FBQ2pCLFFBQVEsRUFBRSxJQUFJO1FBQ2QsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFdBQVc7UUFDbEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFVBQVU7UUFDakIsSUFBSSxFQUFFLFdBQVc7UUFDakIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNELElBQUk7SUFDSix5QkFBeUI7SUFDekIsd0JBQXdCO0lBQ3hCLG1CQUFtQjtJQUNuQixJQUFJO0NBQ0wsQ0FBQztBQUNGLE1BQU0sQ0FBQyxNQUFNLCtCQUErQixHQUFHO0lBQzdDO1FBQ0UsS0FBSyxFQUFFLFFBQVE7UUFDZixJQUFJLEVBQUUsUUFBUTtRQUNkLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxhQUFhO1FBQ3BCLElBQUksRUFBRSxjQUFjO1FBQ3BCLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxvQkFBb0I7UUFDM0IsSUFBSSxFQUFFLHFCQUFxQjtRQUMzQixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsZUFBZTtRQUN0QixJQUFJLEVBQUUsZ0JBQWdCO1FBQ3RCLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxpQkFBaUI7UUFDeEIsSUFBSSxFQUFFLGtCQUFrQjtRQUN4QixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsZUFBZTtRQUN0QixJQUFJLEVBQUUsZ0JBQWdCO1FBQ3RCLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxjQUFjO1FBQ3JCLElBQUksRUFBRSxlQUFlO1FBQ3JCLFFBQVEsRUFBRSxLQUFLO0tBQ2hCO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsV0FBVztRQUNsQixJQUFJLEVBQUUsWUFBWTtRQUNsQixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsVUFBVTtRQUNqQixJQUFJLEVBQUUsV0FBVztRQUNqQixRQUFRLEVBQUUsS0FBSztLQUNoQjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFFBQVE7UUFDZixJQUFJLEVBQUUsUUFBUTtRQUNkLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxZQUFZO1FBQ25CLElBQUksRUFBRSxhQUFhO1FBQ25CLFFBQVEsRUFBRSxLQUFLO0tBQ2hCO0NBQ0YsQ0FBQztBQUNGLE1BQU0sQ0FBQyxNQUFNLCtCQUErQixHQUFHO0lBQzdDO1FBQ0UsS0FBSyxFQUFFLFlBQVk7UUFDbkIsSUFBSSxFQUFFLGFBQWE7UUFDbkIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFdBQVc7UUFDbEIsSUFBSSxFQUFFLGVBQWU7UUFDckIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFdBQVc7UUFDbEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFlBQVk7UUFDbkIsSUFBSSxFQUFFLGdCQUFnQjtRQUN0QixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsZUFBZTtRQUN0QixJQUFJLEVBQUUsZ0JBQWdCO1FBQ3RCLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxZQUFZO1FBQ25CLElBQUksRUFBRSxhQUFhO1FBQ25CLFFBQVEsRUFBRSxJQUFJO0tBQ2Y7SUFDRDtRQUNFLEtBQUssRUFBRSxRQUFRO1FBQ2YsSUFBSSxFQUFFLFFBQVE7UUFDZCxRQUFRLEVBQUUsS0FBSztLQUNoQjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGVBQWU7UUFDdEIsSUFBSSxFQUFFLGdCQUFnQjtRQUN0QixRQUFRLEVBQUUsS0FBSztLQUNoQjtJQUNEO1FBQ0UsS0FBSyxFQUFFLFdBQVc7UUFDbEIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGFBQWE7UUFDcEIsSUFBSSxFQUFFLGNBQWM7UUFDcEIsUUFBUSxFQUFFLEtBQUs7S0FDaEI7SUFDRDtRQUNFLEtBQUssRUFBRSxXQUFXO1FBQ2xCLElBQUksRUFBRSxZQUFZO1FBQ2xCLFFBQVEsRUFBRSxLQUFLO0tBQ2hCO0NBQ0YsQ0FBQztBQUNGLE1BQU0sQ0FBQyxNQUFNLGdDQUFnQyxHQUFHO0lBQzlDO1FBQ0UsS0FBSyxFQUFFLFlBQVk7UUFDbkIsSUFBSSxFQUFFLFlBQVk7UUFDbEIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGFBQWE7UUFDcEIsSUFBSSxFQUFFLGdCQUFnQjtRQUN0QixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsa0JBQWtCO1FBQ3pCLElBQUksRUFBRSxrQkFBa0I7UUFDeEIsUUFBUSxFQUFFLEtBQUs7S0FDaEI7SUFDRDtRQUNFLEtBQUssRUFBRSxnQkFBZ0I7UUFDdkIsSUFBSSxFQUFFLGdCQUFnQjtRQUN0QixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsY0FBYztRQUNyQixJQUFJLEVBQUUsY0FBYztRQUNwQixRQUFRLEVBQUUsSUFBSTtLQUNmO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsUUFBUTtRQUNmLElBQUksRUFBRSxRQUFRO1FBQ2QsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGNBQWM7UUFDckIsSUFBSSxFQUFFLGNBQWM7UUFDcEIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGFBQWE7UUFDcEIsSUFBSSxFQUFFLGFBQWE7UUFDbkIsUUFBUSxFQUFFLEtBQUs7S0FDaEI7SUFDRDtRQUNFLEtBQUssRUFBRSxpQkFBaUI7UUFDeEIsSUFBSSxFQUFFLGlCQUFpQjtRQUN2QixRQUFRLEVBQUUsS0FBSztLQUNoQjtJQUNEO1FBQ0UsS0FBSyxFQUFFLGlCQUFpQjtRQUN4QixJQUFJLEVBQUUsaUJBQWlCO1FBQ3ZCLFFBQVEsRUFBRSxLQUFLO0tBQ2hCO0lBQ0Q7UUFDRSxLQUFLLEVBQUUsb0JBQW9CO1FBQzNCLElBQUksRUFBRSxvQkFBb0I7UUFDMUIsUUFBUSxFQUFFLElBQUk7S0FDZjtJQUNEO1FBQ0UsS0FBSyxFQUFFLG9CQUFvQjtRQUMzQixJQUFJLEVBQUUsb0JBQW9CO1FBQzFCLFFBQVEsRUFBRSxLQUFLO0tBQ2hCO0NBQ0YsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5leHBvcnQgY29uc3QgbWFuYWdlQXZUYWJsZUNvbmZpZyA9IFtcclxuICB7XHJcbiAgICB2YWx1ZTogJ2NvbnRyb2xuYW1lJyxcclxuICAgIG5hbWU6ICdDb250cm9sIE5hbWUnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnZGVzY3JpcHRpb24nLFxyXG4gICAgbmFtZTogJ0Rlc2NyaXB0aW9uJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ3VzZXJjb3VudCcsXHJcbiAgICBuYW1lOiAnVXNlciBDb3VudCcsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZSxcclxuICAgIG9uU2VsZWN0OiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2RhdGFzb3VyY2UnLFxyXG4gICAgbmFtZTogJ0RhdGFzb3VyY2UnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnY29udHJvbHR5cGUnLFxyXG4gICAgbmFtZTogJ0NvbnRyb2wgVHlwZScsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH1cclxuXTtcclxuZXhwb3J0IGNvbnN0IG1hbmFnZUF2RGV0YWlsVGFibGVDb25maWcgPSBbXHJcbiAge1xyXG4gICAgdmFsdWU6ICdzb2RuYW1lJyxcclxuICAgIG5hbWU6ICdDb250cm9sIE5hbWUnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAndXNlcm5hbWUnLFxyXG4gICAgbmFtZTogJ1VzZXIgTmFtZScsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZSxcclxuICAgIG9uU2VsZWN0OiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2ZpcnN0bmFtZScsXHJcbiAgICBuYW1lOiAnRmlyc3QgTmFtZScsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdsYXN0bmFtZScsXHJcbiAgICBuYW1lOiAnTGFzdCBOYW1lJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfVxyXG4gIC8vIHtcclxuICAvLyBcdHZhbHVlOiAnZW50aXRsZW1lbnQnLFxyXG4gIC8vIFx0bmFtZTogXCJFbnRpdGxlbWVudFwiLFxyXG4gIC8vIFx0aXNhY3RpdmU6IGZhbHNlXHJcbiAgLy8gfVxyXG5dO1xyXG5leHBvcnQgY29uc3QgbWFuYWdlQXZieUxlZGdlclVzZXJUYWJsZUNvbmZpZyA9IFtcclxuICB7XHJcbiAgICB2YWx1ZTogJ2xlZGdlcicsXHJcbiAgICBuYW1lOiAnTGVkZ2VyJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2pvdXJuYWxuYW1lJyxcclxuICAgIG5hbWU6ICdKb3VybmFsIE5hbWUnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnam91cm5hbGRlc2NyaXB0aW9uJyxcclxuICAgIG5hbWU6ICdKb3VybmFsIERlc2NyaXB0aW9uJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2pvdXJuYWxzb3VyY2UnLFxyXG4gICAgbmFtZTogJ0pvdXJuYWwgU291cmNlJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2pvdXJuYWxjYXRlZ29yeScsXHJcbiAgICBuYW1lOiAnSm91cm5hbCBDYXRlZ29yeScsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdqb3VybmFscGVyaW9kJyxcclxuICAgIG5hbWU6ICdKb3VybmFsIFBlcmlvZCcsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdjdXJyZW5jeWNvZGUnLFxyXG4gICAgbmFtZTogJ0N1cnJlbmN5IENvZGUnLFxyXG4gICAgaXNhY3RpdmU6IGZhbHNlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2NyZWF0ZWRieScsXHJcbiAgICBuYW1lOiAnQ3JlYXRlZCBCeScsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdwb3N0ZWRieScsXHJcbiAgICBuYW1lOiAnUG9zdGVkIEJ5JyxcclxuICAgIGlzYWN0aXZlOiBmYWxzZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdhbW91bnQnLFxyXG4gICAgbmFtZTogJ0Ftb3VudCcsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdwb3N0ZWRkYXRlJyxcclxuICAgIG5hbWU6ICdQb3N0ZWQgRGF0ZScsXHJcbiAgICBpc2FjdGl2ZTogZmFsc2VcclxuICB9XHJcbl07XHJcbmV4cG9ydCBjb25zdCBtYW5hZ2VBdmJ5VmVuZG9yVXNlclRhYmxlQ29uZmlnID0gW1xyXG4gIHtcclxuICAgIHZhbHVlOiAndmVuZG9ybmFtZScsXHJcbiAgICBuYW1lOiAnVmVuZG9yIE5hbWUnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAndmVuZG9ybnVtJyxcclxuICAgIG5hbWU6ICdWZW5kb3IgTnVtYmVyJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2ludm9pY2VpZCcsXHJcbiAgICBuYW1lOiAnSW52b2ljZSBJRCcsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdpbnZvaWNlbnVtJyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIE51bWJlcicsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdpbnZvaWNlYW1vdW50JyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIEFtb3VudCcsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdhbW91bnRwYWlkJyxcclxuICAgIG5hbWU6ICdBbW91bnQgUGFpZCcsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdzb3VyY2UnLFxyXG4gICAgbmFtZTogJ1NvdXJjZScsXHJcbiAgICBpc2FjdGl2ZTogZmFsc2VcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAncGF5bWVudHN0YXR1cycsXHJcbiAgICBuYW1lOiAnUGF5bWVudCBTdGF0dXMnLFxyXG4gICAgaXNhY3RpdmU6IGZhbHNlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2NyZWF0ZWRieScsXHJcbiAgICBuYW1lOiAnQ3JlYXRlZCBCeScsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdjaGVja2Ftb3VudCcsXHJcbiAgICBuYW1lOiAnQ2hlY2sgQW1vdW50JyxcclxuICAgIGlzYWN0aXZlOiBmYWxzZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdjaGVja2RhdGUnLFxyXG4gICAgbmFtZTogJ0NoZWNrIERhdGUnLFxyXG4gICAgaXNhY3RpdmU6IGZhbHNlXHJcbiAgfVxyXG5dO1xyXG5leHBvcnQgY29uc3QgbWFuYWdlQXZieUludm9pY2VVc2VyVGFibGVDb25maWcgPSBbXHJcbiAge1xyXG4gICAgdmFsdWU6ICdpbnZvaWNlX2lkJyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIElkJyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2ludm9pY2VfbnVtJyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIE51bWJlcicsXHJcbiAgICBpc2FjdGl2ZTogdHJ1ZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdpbnZvaWNlX2N1cnJlbmN5JyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIEN1cnJlbmN5JyxcclxuICAgIGlzYWN0aXZlOiBmYWxzZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdpbnZvaWNlX2Ftb3VudCcsXHJcbiAgICBuYW1lOiAnSW52b2ljZSBBbW91bnQnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnaW52b2ljZV9kYXRlJyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIERhdGUnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnc291cmNlJyxcclxuICAgIG5hbWU6ICdTb3VyY2UnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnaW52b2ljZV90eXBlJyxcclxuICAgIG5hbWU6ICdJbnZvaWNlIFR5cGUnLFxyXG4gICAgaXNhY3RpdmU6IHRydWVcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnYmFzZV9hbW91bnQnLFxyXG4gICAgbmFtZTogJ0Jhc2UgQW1vdW50JyxcclxuICAgIGlzYWN0aXZlOiBmYWxzZVxyXG4gIH0sXHJcbiAge1xyXG4gICAgdmFsdWU6ICdzaXRlX3VwZGF0ZWRfYnknLFxyXG4gICAgbmFtZTogJ1NpdGUgVXBkYXRlZCBCeScsXHJcbiAgICBpc2FjdGl2ZTogZmFsc2VcclxuICB9LFxyXG4gIHtcclxuICAgIHZhbHVlOiAnc2l0ZV9jcmVhdGVkX2J5JyxcclxuICAgIG5hbWU6ICdTaXRlIENyZWF0ZWQgQnknLFxyXG4gICAgaXNhY3RpdmU6IGZhbHNlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2ludm9pY2VfY3JlYXRlZF9ieScsXHJcbiAgICBuYW1lOiAnSW52b2ljZSBDcmVhdGVkIEJ5JyxcclxuICAgIGlzYWN0aXZlOiB0cnVlXHJcbiAgfSxcclxuICB7XHJcbiAgICB2YWx1ZTogJ2ludm9pY2VfdXBkYXRlZF9ieScsXHJcbiAgICBuYW1lOiAnSW52b2ljZSBVcGRhdGVkIEJ5JyxcclxuICAgIGlzYWN0aXZlOiBmYWxzZVxyXG4gIH1cclxuXTtcclxuIl19