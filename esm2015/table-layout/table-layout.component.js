import { Component, ViewChild, Input, Output, EventEmitter, Inject, ChangeDetectorRef } from "@angular/core";
import { MatPaginator, MatTableDataSource, MatDialog, MatCheckbox, MatTable } from "@angular/material";
import { SnackBarService } from "./../shared/snackbar.service";
import { animate, state, style, transition, trigger } from '@angular/animations';
import * as FileSaver from "file-saver";
import { FormControl } from "@angular/forms";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import * as _ from "lodash";
import { ContentService } from "../content/content.service";
import { MessageService } from "../_services/index";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
import { LoaderService } from '../loader.service';
export class TableLayoutComponent {
    constructor(_fuseTranslationLoaderService, contentService, messageService, _matDialog, snackBarService, changeDetectorRef, environment, english, loaderService) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this.contentService = contentService;
        this.messageService = messageService;
        this._matDialog = _matDialog;
        this.snackBarService = snackBarService;
        this.changeDetectorRef = changeDetectorRef;
        this.environment = environment;
        this.english = english;
        this.loaderService = loaderService;
        this.disablePagination = false;
        this.checkClickEventMessage = new EventEmitter();
        this.actionClickEvent = new EventEmitter();
        this.enableSelectOption = false;
        this.enableSearch = false;
        this.tableData = {};
        this.enableAction = false;
        this.limit = 10;
        this.offset = 0;
        this.selectedData = [];
        this.inputData = {};
        this.unsubscribe = new Subject();
        this.unsubscribeModel = new Subject();
        this.currentFilteredValue = {};
        this.enableOptionFilter = false;
        this.enableUIfilter = false;
        this.disablePaginator = false;
        this.redirectUri = this.environment.redirectUri;
        this._fuseTranslationLoaderService.loadTranslations(english);
        /* // temporarily commented
        this.messageService.modelCloseMessage
          .pipe(takeUntil(this.unsubscribeModel))
          .subscribe(data => {
            console.log(data, ">>>>data")
            this.selectedData = [];
            if (data != 0) {
              this.inputData["selectedData"] = [];
              this.inputData["selectedIdList"] = [];
              this.inputData["selectAll"] = false;
            }
            console.log(this.tableData);
            console.log(this.dataSource);
            // _.forEach(this.tableData, function(item) {
            //   item.checked = false;
            // });
    
            // this.dataSource.data.map(obj => {
            //   obj.checked = false;
            // });
            // this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
            // // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
            // this.checkClickEventMessage.emit("clicked");
            this.data = data;
            // if(this.data === 'listView'){
            if (this.data) {
              this.currentConfigData = JSON.parse(
                localStorage.getItem("currentConfigData")
              );
              this.ngOnInit();
            }
            // }
          });
          */
        this.messageService.getMessage().subscribe(message => {
            this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
            if (this.currentConfigData &&
                this.currentConfigData.listView &&
                this.currentConfigData.listView.enableTableLayout) {
                this.ngOnInit();
            }
        });
        this.messageService
            .getTableHeaderUpdate()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
            this.updateTableView();
        });
    }
    ngOnInit() {
        // this.changeDetectorRef.detectChanges();
        this.inputData["selectAll"] = false;
        console.log(this, "......table this");
        this.defaultDatasource = localStorage.getItem("datasource");
        this.queryParams = {
            offset: 0,
            limit: 10,
            datasource: this.defaultDatasource
        };
        //  this.paginator["pageIndex"] = 0;
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        // if (this.data == 'listView' || this.data == 'refreshPage') {
        //   this.inputData["selectedData"] = [];
        //   this.inputData["selectedIdList"] = [];
        // }
        this.inputData["datasourceId"] = this.defaultDatasource;
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        // this.changeDetectorRef.detectChanges();
        this.fromFieldValue = new FormControl([""]);
        this.toFieldValue = new FormControl([""]);
        this.onLoadData(null);
    }
    ngAfterViewInit() {
        console.log("ngAfterViewInit ");
        console.log(this.selectedPaginator, "selectedPag>>>>");
        if (this.dataSource && this.selectedPaginator) {
            this.dataSource.paginator = this.selectedPaginator;
        }
    }
    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribeModel.next();
    }
    updateTableView() {
        let currentTableHeader = JSON.parse(localStorage.getItem("selectedTableHeaders"));
        this.columns = currentTableHeader;
        this.displayedColumns = this.columns
            .filter(function (val) {
            return val.isActive;
        })
            .map(function (val) {
            return val.value;
        });
    }
    onLoadData(enableNext) {
        this.currentFilteredValue = !_.isEmpty(this.currentFilteredValue) ? this.currentFilteredValue : {};
        this.columns = [];
        this.displayedColumns = [];
        let tableArray = this.onLoad
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        let responseKey = this.onLoad && this.onLoad.responseKey ? this.onLoad.responseKey : null;
        let index = _.findIndex(tableArray, { tableId: this.tableId });
        //if(index>-1)
        this.currentTableData = tableArray[index];
        console.log(tableArray, ".....tableArray");
        console.log(this.currentTableData, ".....currentTableData");
        let checkOptionFilter = (this.currentTableData && this.currentTableData.enableOptionsFilter) ? true : false;
        if (checkOptionFilter) {
            var self = this;
            this.enableOptionFilter = true;
            let Optiondata = this.currentTableData.filterOptionLoadFunction;
            this.optionFilterFields = this.currentTableData.optionFields;
            var apiUrl = Optiondata.apiUrl;
            var responseName = Optiondata.response;
            var self = this;
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(res => {
                let tempArray = (res.response[responseName]) ? res.response[responseName] : [];
                self.optionFilterdata = tempArray;
            });
        }
        if (this.currentTableData && this.currentTableData.enableUifilter) {
            let filterKey = this.currentTableData.uiFilterKey;
            this.enableUIfilter = true;
            // this.dataSource.filterPredicate = (data: Element, filter: string) => {
            //   return data[filterKey] == filter;
            //  };
        }
        // if(this.onLoad && this.onLoad.getFromcurrentInput){
        if (this.onLoad && this.currentTableData.checkFromInputData) {
            let temp = localStorage.getItem("currentInput");
            this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
            let data = (this.currentTableData.keyTosave && this.inputData[this.currentTableData.keyTosave]) ? this.inputData[this.currentTableData.keyTosave] : [];
            if (data.length > 0) {
                this.onLoad.response = data;
                this.onLoad.total = data.length;
            }
        }
        console.log(">>>> this.onLoad ", this.onLoad);
        if (this.onLoad && this.onLoad.response) {
            this.getLoadData(this.currentTableData, this.onLoad.response, this.onLoad.total, responseKey);
        }
        else {
            if (this.currentTableData)
                this.getData(this.currentTableData);
        }
        if (!enableNext) {
            // this.paginator.firstPage();
        }
    }
    getData(currentTableValue) {
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        this.columns = currentTableValue.tableHeader;
        if (currentTableValue.onLoadFunction) {
            var apiUrl = currentTableValue.onLoadFunction.apiUrl;
            var responseName = currentTableValue.onLoadFunction.response;
            let keyToSet = currentTableValue.onLoadFunction.keyForStringResponse;
            this.currentData = this.currentConfigData["listView"];
            var includeRequest = currentTableValue.onLoadFunction.includerequestData ? true : false;
            //if ( currentTableValue.onLoadFunction.enableLoader) {
            this.loaderService.startLoader();
            //}
            let dynamicLoad = currentTableValue.onLoadFunction.dynamicReportLoad; // condition checked when implement additional report view
            let requestedQuery = currentTableValue.onLoadFunction.requestData;
            var self = this;
            var requestQueryParam = {};
            if (dynamicLoad) {
                let tempObj = localStorage.getItem("CurrentReportData");
                let reportITem = JSON.parse(tempObj);
                _.forEach(requestedQuery, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else {
                        requestQueryParam[requestItem.name] = (reportITem[requestItem.value]) ? reportITem[requestItem.value] : requestItem.value;
                    }
                });
            }
            else {
                _.forEach(requestedQuery, function (requestItem) {
                    if (requestItem.fromCurrentData) {
                        requestQueryParam[requestItem.name] = self.currentData[requestItem.value];
                    }
                    else if (requestItem.directAssign) {
                        self.queryParams[requestItem.name] = requestItem.convertToString
                            ? JSON.stringify(requestItem.value)
                            : requestItem.value;
                    }
                    else {
                        self.queryParams[requestItem.name] = requestItem.convertToString
                            ? JSON.stringify(self.inputData[requestItem.value])
                            : self.inputData[requestItem.value];
                    }
                });
            }
            this.queryParams = Object.assign({}, this.queryParams, requestQueryParam);
            this.contentService
                .getAllReponse(this.queryParams, apiUrl)
                .subscribe(data => {
                // if ( currentTableValue.onLoadFunction.enableLoader) {
                this.loaderService.stopLoader();
                //}
                this.enableSearch = this.currentData.enableGlobalSearch;
                this.displayedColumns = this.columns
                    .filter(function (val) {
                    return val.isActive;
                })
                    .map(function (val) {
                    return val.value;
                });
                let tempArray = [];
                var self = this;
                console.log(self, ">>>>>>>>>>THISSSS");
                _.forEach(data.response[responseName], function (item, index) {
                    if (typeof item !== "object") {
                        let tempObj = {};
                        tempObj[keyToSet] = item;
                        let filterObj = {};
                        filterObj[currentTableValue.filterKey] = tempObj[currentTableValue.dataFilterKey];
                        let selectedValue = _.find(self.inputData[currentTableValue.selectedValuesKey], filterObj);
                        if (selectedValue) {
                            tempObj['checked'] = true;
                        }
                        tempArray.push(tempObj);
                    }
                    else {
                        let tempObj = {};
                        _.forEach(currentTableValue.dataViewFormat, function (viewItem) {
                            if (viewItem.subkey) {
                                if (viewItem.assignFirstIndexval) {
                                    let resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                    tempObj[viewItem.name] = item[viewItem.value]
                                        ? resultValue
                                        : "";
                                }
                                else {
                                    tempObj[viewItem.name] = item[viewItem.value]
                                        ? item[viewItem.value][viewItem.subkey]
                                        : "";
                                }
                            }
                            else if (viewItem.isCondition) {
                                tempObj[viewItem.name] = eval(viewItem.condition);
                            }
                            else if (viewItem.isAddDefaultValue) {
                                tempObj[viewItem.name] = viewItem.value;
                            }
                            else {
                                tempObj[viewItem.name] = item[viewItem.value];
                            }
                            if (currentTableValue.loadLabelFromConfig) {
                                tempObj[viewItem.name] =
                                    currentTableValue.headerList[item[viewItem.value]];
                            }
                        });
                        let filterObj = {};
                        filterObj[currentTableValue.filterKey] = item[currentTableValue.dataFilterKey];
                        let selectedValue = _.find(self.inputData[currentTableValue.selectedValuesKey], filterObj);
                        if (selectedValue) {
                            item.checked = true;
                        }
                        tempObj = Object.assign({}, item, tempObj);
                        tempArray.push(tempObj);
                    }
                });
                if (tempArray && tempArray.length) {
                    this.tableData = tempArray;
                }
                else {
                    this.tableData =
                        data && data.response && data.response[responseName]
                            ? data.response[responseName]
                            : [];
                }
                if (data && data.response) {
                    this.length = data.response.total;
                }
                this.dataSource = new MatTableDataSource(this.tableData);
                // this.dataSource.paginator = this.selectedPaginator; // newly added 
                localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                this.inputData["selectAll"] = false;
                var filteredKey = (this.currentFilteredValue.searchKey) ? this.currentFilteredValue.searchKey : '';
                _.forEach(self.columns, function (x) {
                    if (filteredKey == x.value) {
                        self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                    }
                    else {
                        self.inputData[x.keyToSave] = null;
                    }
                });
                // this.selectAllCheck({ checked: false });
                // this.selectAll = false;
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.inputData["selectedData"] = [];
                // this.inputData["selectedIdList"] = [];
                // localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                // console.log(this.selectAll, "SelectAll>>>>>>>");
                // this.changeDetectorRef.detectChanges();
                this.checkClickEventMessage.emit("clicked");
            }, err => {
                this.loaderService.stopLoader();
                console.log(" Error ", err);
            });
        }
    }
    getLoadData(currentValue, responseValue, totalLength, responseKey) {
        console.log(currentValue, "....currentValue");
        let tempArray = [];
        // if (responseValue && responseValue[responseKey]) {
        responseValue = responseKey ? responseValue[responseKey] : responseValue;
        if (responseValue && responseValue.length) {
            _.forEach(responseValue, function (item, index) {
                if (typeof item !== "object") {
                    let tempObj = {};
                    tempObj[currentValue.keyToSet] = item;
                    tempArray.push(tempObj);
                }
                else {
                    let tempObj = {};
                    _.forEach(currentValue.dataViewFormat, function (viewItem) {
                        if (viewItem.subkey) {
                            if (viewItem.assignFirstIndexval) {
                                let resultValue = (!_.isEmpty(item[viewItem.value]) && item[viewItem.value][0][viewItem.subkey]) ? (item[viewItem.value][0][viewItem.subkey]) : "";
                                tempObj[viewItem.name] = item[viewItem.value]
                                    ? resultValue
                                    : "";
                            }
                            else {
                                tempObj[viewItem.name] = item[viewItem.value]
                                    ? item[viewItem.value][viewItem.subkey]
                                    : "";
                            }
                        }
                        else {
                            tempObj[viewItem.name] = item[viewItem.value];
                        }
                    });
                    tempObj = Object.assign({}, item, tempObj);
                    tempArray.push(tempObj);
                }
            });
        }
        this.columns = currentValue.tableHeader;
        responseValue = tempArray.length ? tempArray : responseValue;
        console.log(this.inputData, "...INPUTDATA");
        if (currentValue && currentValue.mapDataFunction) {
            let self = this;
            _.forEach(currentValue.mapDataFunction, function (mapItem) {
                _.forEach(mapItem.requestData, function (requestItem) {
                    self.queryParams[requestItem.name] = requestItem.convertToString
                        ? JSON.stringify(self.inputData[requestItem.value])
                        : self.inputData[requestItem.value];
                });
                self.contentService
                    .getAllReponse(self.queryParams, mapItem.apiUrl)
                    .subscribe(data => {
                    console.log(mapItem, ">>>>>>> MAP ITEM");
                    console.log(data, ".....DATAAAA");
                    let tempObj = _.keyBy(data.response[mapItem.response], "_id");
                    console.log(tempObj, "....tempObj");
                    _.forEach(responseValue, function (responseItem) {
                        if (data.response &&
                            data.response.keyForCheck == mapItem.keyForCheck &&
                            responseItem[mapItem.keyForCheck]) {
                            responseItem[mapItem.showKey] =
                                tempObj && responseItem[mapItem.keyForCheck]
                                    ? tempObj[responseItem[mapItem.keyForCheck]].objectTypes
                                    : [];
                            responseItem[mapItem.modelKey] = responseItem[mapItem.showKey];
                        }
                        else
                            console.log(responseItem, ".....responseItem");
                    });
                    console.log(responseValue, ">>>>>responseValue");
                });
            });
        }
        else {
            this.dataSource = new MatTableDataSource(responseValue);
            this.length = totalLength ? totalLength : responseValue.length;
        }
        this.tableData = tempArray;
        this.dataSource = new MatTableDataSource(responseValue);
        this.length = totalLength ? totalLength : responseValue.length;
        console.log(this.columns, ">>>>>COLUMNSS");
        // this.displayedColumns = _.map(this.columns, "value");
        this.displayedColumns = this.columns
            .filter(function (val) {
            return val.isActive;
        })
            .map(function (val) {
            return val.value;
        });
        this.dataSource.paginator = this.selectedPaginator;
        console.log(this.selectedPaginator, ".................... selectedPaginator");
        console.log(this.dataSource, ".........DATASOURCEEEE");
        this.disablePaginator = currentValue.disablePagination ? currentValue.disablePagination : false;
        //paginatorEnable
        // this.dataSource = responseKey
        //   ? new MatTableDataSource(responseValue[responseKey])
        //   : new MatTableDataSource(responseValue);
    }
    updateCheckClick(selected, event) {
        // selected.type = this.currentTableData.tableType
        //   ? this.currentTableData.tableType
        //   : "";
        if (this.currentTableData && this.currentTableData.tableType) {
            selected.type = this.currentTableData.tableType;
        }
        // selected.typeId = this.currentTableData.typeId
        //   ? this.currentTableData.typeId
        //   : "";
        let securityTypeLength = this.currentTableData.securityTypeLength ? Number(this.currentTableData.securityTypeLength) : 3;
        console.log("this.currentTableData.securityTypeLength????", this.currentTableData.securityTypeLength);
        console.log(">>> securityTypeLengthsss ", securityTypeLength);
        // if (selected && selected.typeId) {
        //   if (selected.typeId == "2") {
        //     selected["security_type"] = [4];
        //     selected["level"] = [4];
        //   } else {
        //     selected["security_type"] = [1, 2, 3];
        //     selected["level"] = [1, 2, 3];
        //    }
        let tempArray = [];
        let predefinedArray = this.currentTableData.securityArrayValue ? JSON.parse("[" + this.currentTableData.securityArrayValue + "]") : tempArray;
        console.log(">>> predefinedArray ", predefinedArray);
        if (securityTypeLength && securityTypeLength > 0) {
            console.log("inside");
            if (predefinedArray && predefinedArray.length) {
                tempArray = predefinedArray;
            }
            else {
                for (let i = 1; i <= securityTypeLength; i++) {
                    tempArray.push(i);
                }
            }
            selected["security_type"] = tempArray;
        }
        else {
            console.log("outside");
            if (securityTypeLength == 0) {
                selected["security_type"] = tempArray;
            }
            else {
                selected["security_type"] = [1, 2, 3];
            }
        }
        //}
        console.log(">>>  selected security_type ", selected);
        if (this.currentTableData && this.currentTableData.dataFormatToSave) {
            _.forEach(this.currentTableData.dataFormatToSave, function (item) {
                selected[item.name] = selected[item.value];
            });
        }
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        let savedSelectedData = this.inputData.selectedData && this.inputData.selectedData.length
            ? this.inputData.selectedData
            : [];
        this.selectedData = _.merge(savedSelectedData, this.selectedData);
        if (event.checked) {
            this.selectedData.push(selected);
        }
        else {
            // let index = this.selectedData.findIndex(
            //   obj => obj.object_name === selected.object_name
            // );
            let objectKey = this.currentTableData.objectKeyToCheck ? this.currentTableData.objectKeyToCheck : "obj_name";
            let index = this.selectedData.findIndex(obj => obj[objectKey] === selected[objectKey]);
            this.selectedData.splice(index, 1);
        }
        console.log(">>>> this.selectedData ", this.selectedData);
        this.inputData["selectedData"] = this.selectedData;
        if (this.currentTableData && this.currentTableData.saveInArray) {
            let arrayObj = this.currentTableData.saveInArray;
            if (this.currentTableData.tableId === arrayObj.key) {
                let tempArray = this.selectedData || this.selectedData.length
                    ? _.map(this.selectedData, arrayObj.valueToMap)
                    : [];
                tempArray = tempArray.filter(function (element) {
                    return element != null;
                });
                this.inputData[arrayObj.key] = tempArray;
            }
        }
        // check box remain search column value start
        var filteredKey = (this.currentFilteredValue.searchKey) ? this.currentFilteredValue.searchKey : '';
        if (filteredKey) {
            var self = this;
            _.forEach(self.columns, function (x) {
                if (filteredKey == x.value) {
                    self.inputData[x.keyToSave] = self.currentFilteredValue.searchValue;
                }
                else {
                    self.inputData[x.keyToSave] = null;
                }
            });
        }
        // check box remain search column value start
        this.inputData["selectedIdList"] = _.map(this.selectedData, "_id");
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    }
    selectAllCheck(event) {
        if (event.checked === true) {
            this.dataSource.data.map(obj => {
                obj.checked = true;
            });
            this.inputData["selectedData"] = this.dataSource.data;
            this.inputData["selectedIdList"] = _.map(this.dataSource.data, "_id");
        }
        else {
            this.dataSource.data.map(obj => {
                obj.checked = false;
            });
            this.inputData["selectedData"] = [];
            this.inputData["selectedIdList"] = [];
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        this.checkClickEventMessage.emit("clicked");
    }
    addFieldClick(item, objectList) {
        console.log(item, "..........ITEM");
        console.log(this.inputData, ".......INPUT DAta");
        console.log(objectList, "......ObjectList");
    }
    selectAllData() {
        let requestDetails = this.currentData.selectAllRequest;
        let query = {};
        let self = this;
        _.forEach(requestDetails.requestData, function (item) {
            query[item.name] = self.inputData[item.value];
        });
        this.contentService
            .getAllReponse(query, requestDetails.apiUrl)
            .subscribe(data => {
            this.inputData["selectedIdList"] =
                data.response[requestDetails.responseName];
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        });
    }
    clearSelectAllData() {
        this.ngOnInit();
    }
    applyFilter(event, key) {
        console.log(">>>>>>>>>> event ", event, " key ", key);
        this.currentFilteredValue = {
            searchKey: key,
            searchValue: event
        };
        console.log(">>> this.currentFilteredValue  ", this.currentFilteredValue);
        this.inputData[key] = event; // searched value set
        let tableArray = this.onLoad && this.onLoad.tableData
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        let index = _.findIndex(tableArray, { tableId: this.tableId });
        let searchRequest = tableArray[index].onTableSearch;
        let request = searchRequest.requestData;
        let isUiserach = (searchRequest.uiSearch) ? true : false;
        if (isUiserach) {
            this.dataSource.filter = event;
        }
        else {
            this.currentTableData = tableArray[index];
            let query = {
                offset: 0,
                limit: 10,
                datasource: this.defaultDatasource
            };
            if (searchRequest && searchRequest.isSinglerequest) {
                query[searchRequest.singleRequestKey] = event;
            }
            else {
                var self = this;
                _.forEach(request, function (item) {
                    if (self.inputData[item.name])
                        query[item.value] = self.inputData[item.name]; // onTable Search Value passed instead of name 
                });
            }
            this.queryParams = query;
            this.getData(this.currentTableData);
        }
    }
    applyFilterSearch(eventItem, key) {
        console.log(">>> applyFilterSearch");
        eventItem.preventDefault();
        let event = eventItem.target.value;
        this.currentFilteredValue = {};
        let temp = localStorage.getItem("currentInput");
        console.log(">>>>>>>>>> event ", event, " key ", key);
        this.currentFilteredValue = {
            searchKey: key,
            searchValue: event
        };
        this.inputData[key] = event; // searched value set
        let tableArray = this.onLoad && this.onLoad.tableData
            ? this.onLoad.tableData
            : this.currentConfigData["listView"].tableData;
        let index = _.findIndex(tableArray, { tableId: this.tableId });
        let searchRequest = tableArray[index].onTableSearch;
        let request = searchRequest.requestData;
        let isUiserach = (searchRequest.uiSearch) ? true : false;
        if (isUiserach) {
            this.dataSource.filter = event;
        }
        else {
            this.currentTableData = tableArray[index];
            let query = {
                offset: 0,
                limit: 10,
                datasource: this.defaultDatasource
            };
            console.log(">>>> searchRequest ", searchRequest);
            if (searchRequest && searchRequest.isSinglerequest) {
                query[searchRequest.singleRequestKey] = event;
            }
            else {
                var self = this;
                _.forEach(request, function (item) {
                    if (self.inputData[item.name])
                        query[item.value] = self.inputData[item.name]; // onTable Search Value passed instead of name 
                });
            }
            this.queryParams = query;
            this.getData(this.currentTableData);
        }
    }
    filterReset(field) {
        delete this.queryParams[field.requestKey];
        this.ngOnInit();
    }
    onMultiSelect(event, rowVal) {
        let temp = localStorage.getItem("currentInput");
        this.inputData = !_.isEmpty(temp) ? JSON.parse(temp) : {};
        let selectedData = this.inputData.selectedData;
        let index = _.findIndex(selectedData, { obj_name: rowVal.obj_name });
        if (index > -1 && selectedData && selectedData.length) {
            this.inputData.selectedData[index].security_type = event.value;
            this.inputData.selectedData[index].level = event.value;
            this.inputData.selectedData[index].objectLevel = event.value;
        }
        localStorage.setItem("currentInput", JSON.stringify(this.inputData));
    }
    changePage(event) {
        if (event.pageSize != this.limit) {
            this.queryParams["limit"] = event.pageSize;
            this.queryParams["offset"] = 0;
        }
        else {
            this.queryParams["offset"] = event.pageIndex * this.limit;
            this.queryParams["limit"] = this.limit;
        }
        this.onLoadData("next");
    }
    viewData(element, col) {
        console.log(col, "viewData >>>>>>>>> isRedirect ", col.isRedirect, this.environment);
        if (col.isRedirect) {
            let testStr = "/control-management/access-control";
            window.location.href = `${this.redirectUri}` + testStr;
            console.log("Redirected ********************");
            // this.route.navigateByUrl("/control-management/access-control");
        }
        else {
            this.actionClick(element, "view");
        }
    }
    addButtonClick(element, action) {
        console.log("addButtonClick ");
        console.log(" element ", element);
        console.log(" action ", action);
    }
    actionClick(element, action) {
        console.log(element, ">>>>element");
        if (action == "edit" || action == "view" || action == "info") {
            this.openDialog(element, action);
        }
        else if (action == "delete") {
            console.log(this, ">>>>>>>this");
            if (this.onLoad && this.onLoad.inputKey) {
                let inputKey = this.onLoad.inputKey;
                if (this.inputData && this.inputData[inputKey]) {
                    let selectedData = this.inputData[inputKey];
                    // let index = selectedData.findIndex(
                    //   obj => obj == element
                    // );
                    let index = _.findIndex(selectedData, element);
                    console.log(">>>> delete index ", index);
                    if (index > -1) {
                        selectedData.splice(index, 1);
                    }
                    this.inputData[inputKey] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    this.actionClickEvent.emit("clicked");
                }
            }
            else {
                if (this.inputData && this.inputData.selectedData) {
                    let objectKey = this.currentTableData.objectKeyToCheck ? this.currentTableData.objectKeyToCheck : "obj_name";
                    console.log(">>>> objectKey ", objectKey);
                    let selectedData = this.inputData.selectedData;
                    // let index = selectedData.findIndex(
                    //   obj => obj.object_name == element.object_name
                    // );
                    let index = selectedData.findIndex(obj => obj[objectKey] == element[objectKey]);
                    selectedData.splice(index, 1);
                    this.inputData["selectedData"] = selectedData;
                    this.inputData["selectedIdList"] = _.map(selectedData, "_id");
                    this.dataSource = new MatTableDataSource(selectedData);
                    this.length = selectedData.length;
                    localStorage.setItem("currentInput", JSON.stringify(this.inputData));
                    this.actionClickEvent.emit("clicked");
                }
            }
        }
        else if (action == "remove_red_eye") {
            this.openDialog(element, "view");
        }
        else if (action == "restore_page") {
            this.openDialog(element, action);
        }
        else if (action == 'askdefaultbutton') {
            let dsid = element._id;
            let dsname = element.name;
            console.log(" dsid ", dsid, " dsname ", dsname);
            this.messageService.sendDatasource(dsid);
            this.snackBarService.add(this._fuseTranslationLoaderService.instant("Datasource Default Updated Successfully!"));
        }
        else if (action == "save_alt") {
            let requestDetails = this.currentData.downloadRequest;
            let query = {
                reportId: element._id
            };
            this.contentService
                .getAllReponse(query, requestDetails.apiUrl)
                .subscribe(data => {
                const ab = new ArrayBuffer(data.data.length);
                const view = new Uint8Array(ab);
                for (let i = 0; i < data.data.length; i++) {
                    view[i] = data.data[i];
                }
                let downloadType = (element.fileType == "CSV") ? "text/csv" : "text/xlsx";
                const file = new Blob([ab], { type: downloadType });
                FileSaver.saveAs(file, element.fileName + element.fileExtension);
            });
        }
    }
    actionRedirect(element, columnData) {
        console.log("actionRedirect >>>>>> ", element);
        let requestDetails = this.currentTableData;
        let queryObj = {};
        var self = this;
        console.log("requestDetails >>>>>> ", requestDetails);
        if (columnData.rulesetCheck) {
            let toastMessageDetails = requestDetails.onTableUpdate.toastMessage;
            _.forEach(requestDetails.onTableUpdate.requestData, function (item) {
                queryObj[item.name] = item.subkey
                    ? element[item.value][item.subkey]
                    : element[item.value];
            });
            self.contentService
                .updateRequest(queryObj, requestDetails.onTableUpdate.apiUrl, element._id)
                .subscribe(res => {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                this.ngOnInit();
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
            console.log('actionRedirect >>>>>>>>>>>>>>> queryObj ', queryObj);
        }
        else {
        }
    }
    onToggleChange(event, element) {
        console.log(">>>>>>>>>>>> event ", event, " :element ", element);
        element.status = event.checked ? "ACTIVE" : "INACTIVE";
        let requestDetails = this.currentTableData
            ? this.currentTableData.onToggleChange
            : "";
        if (requestDetails) {
            let toastMessageDetails = requestDetails.toastMessage;
            var self = this;
            let queryObj = {};
            _.forEach(requestDetails.requestData, function (item) {
                queryObj[item.name] = item.subkey
                    ? element[item.value][item.subkey]
                    : element[item.value];
            });
            self.contentService
                .updateRequest(queryObj, requestDetails.apiUrl, element._id)
                .subscribe(res => {
                if (element.status == 'ACTIVE' && toastMessageDetails.enable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.enable));
                }
                else if (element.status == 'INACTIVE' && toastMessageDetails.disable) {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.disable));
                }
                else {
                    self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                }
            }, error => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
        }
        else if (this.currentTableData && this.currentTableData.saveselectedToggle) {
            this.inputData[this.currentTableData["keyTosave"]] = this.dataSource.data;
            localStorage.setItem("currentInput", JSON.stringify(this.inputData));
        }
    }
    openDialog(element, action) {
        console.log(" Open Dialog ********* element: ", element);
        let modelWidth = this.currentConfigData[action].modelData.size;
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                savedData: element
            }
        })
            .afterClosed()
            .subscribe(response => {
            localStorage.removeItem("currentInput");
            this.messageService.sendModelCloseEvent("listView");
        });
    }
}
TableLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "table-layout",
                template: "<ng-container *ngIf=\"inputData.selectAll && length > 10\">\r\n\r\n  <div *ngIf=\"inputData.selectAll && length > 10 && (inputData.selectedIdList.length != length)\" class=\"header-top header selectall-export\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n    fxlayoutalign=\"space-between center\">\r\n    <!-- <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\"> -->\r\n    <span class=\"m-0\">{{currentData.selectAllText | translate}}\r\n      <button mat-button color=\"accent\" (click)=\"selectAllData()\">Select All {{length}}\r\n        {{currentData.selectAccesGroup}}</button>\r\n      </span>\r\n    <!-- </div> -->\r\n  </div>\r\n  <div *ngIf=\"inputData.selectAll && length > 10 && (inputData.selectedIdList.length == length)\" class=\"header-top header selectall-export\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n    fxlayoutalign=\"space-between center\">\r\n    <span class=\"m-0\">All {{length}} {{currentData.afterSelectAllText}}\r\n      <button mat-button color=\"accent\" (click)=\"clearSelectAllData()\">Clear Selection</button></span>\r\n  </div>\r\n\r\n</ng-container>\r\n\r\n<!--  dropdown filter start-->\r\n\r\n<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\" *ngIf=\"enableOptionFilter\">\r\n\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n  </div>\r\n  <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"margin-top: -39px;margin-bottom: -26px;\">\r\n    <div *ngFor=\"let optionField of optionFilterFields\">\r\n    <!-- <button mat-raised-button class=\"warn ml-4 mt-8\" style=\"float:right\" (click)=\"filterReset(optionField)\">\r\n      Reset\r\n    </button> -->\r\n    <button mat-raised-button class=\"mat-raised-button ml-4 mt-8\" style=\"float:right\" (click)=\"filterReset(optionField)\">\r\n     <mat-icon>autorenew</mat-icon>\r\n    </button>\r\n    <mat-form-field appearance=\"outline\" class=\"right\" style=\"float:right;\">\r\n      <mat-label>{{optionField.label| translate}}</mat-label>\r\n      <mat-select name=\"data_source\" (selectionChange)=\"applyFilter($event.value,optionField.keyToSave)\" placeholder=\"'Select Data Source'\">\r\n        <mat-option *ngFor=\"let item of optionFilterdata\" value=\"{{item[optionField.keyToSave] | translate}}\">{{item[optionField.keyToShow] | translate}}\r\n        </mat-option>\r\n      </mat-select>\r\n    </mat-form-field>\r\n    </div>\r\n  </div>\r\n \r\n</div> <!--  dropdown filter end -->\r\n<!-- input search filter start -->\r\n<div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\" fxlayoutalign=\"space-between center\" *ngIf=\"enableUIfilter\">\r\n  <div class=\"example-header\">\r\n    <mat-form-field style=\"padding-left: 10px;\">\r\n      <input matInput (keyup)=\"applyFilter($event.target.value,'')\" placeholder=\"Filter\">\r\n    </mat-form-field>\r\n  </div>\r\n</div>\r\n\r\n<!-- input search filter end -->\r\n\r\n<!-- <p>this.data Before : {{this.data}} - condition :{{(this.data=='listView'||this.data=='refreshPage'||this.data==0)}}</p>  -->  \r\n<div class=\"content p-12\" fusePerfectScrollbar >\r\n  <div class=\"res-table\">\r\n    <table mat-table [dataSource]=\"dataSource\" multiTemplateDataRows>\r\n      <ng-container *ngFor=\"let column of columns; let i = index\" [matColumnDef]=\"column.value\">\r\n        <th mat-header-cell *matHeaderCellDef class=\"tbl-header-style heading-label\">\r\n          <span [ngClass]=\"{'selectTH' : column.enableSelectAll}\">{{ column.name | translate}}</span>\r\n          <div class=\"tabheade\"  *ngIf=\"column.enableSelectAll\">\r\n            <mat-checkbox [(ngModel)]=\"inputData.selectAll\" (change)=\"selectAllCheck($event)\">\r\n            </mat-checkbox>\r\n          </div>\r\n          <div class=\"tabheade\" *ngIf=\"column.enableSearch\">\r\n            <mat-form-field appearance=\"outline\" fxFlex=\"80\" class=\"custom-mat-form\">\r\n              <mat-label>{{'SEARCH.TABLE_PLACE_HOLDER_LBL' | translate}}</mat-label>\r\n              <input matInput [(ngModel)]=\"inputData[column.keyToSave]\"\r\n                (keydown.enter)=\"applyFilterSearch($event, column.value)\"  placeholder=\"Search\"\r\n                [disabled]=\"column.disableSearch\" />\r\n            </mat-form-field>\r\n          </div>\r\n        </th>\r\n      \r\n      \r\n        <td mat-cell *matCellDef=\"let row\">\r\n         <!-- <p> row value {{row | json}} </p>  -->\r\n          <!-- <span>{{ row[column.defaultVal] }}- ROW</span>\r\n          <span>{{row[column.value] }}- COLUMN</span> \r\n          <span>{{column.defaultVal &&  row[column.defaultVal] && row[column.value]=='Imported'}} - INPUTDATA</span> -->\r\n          <span *ngIf=\"column.value=='issuetype'\" [ngClass]=\"{'bg-warning':row[column.value]=='warning','bg-failed':row[column.value]=='danger','bg-success':row[column.value]=='success'}\" class=\"ng-star-inserted \">\r\n            {{row[column.value]=='warning'?'MEDIUM':''}}\r\n            {{row[column.value]=='danger'?'HIGH':''}}\r\n            {{row[column.value]=='success'?'SUCCESS':''}}\r\n           </span> \r\n           <span *ngIf=\"column.isNotArray\">\r\n            {{column.data[row[column.value]] | translate}} \r\n          </span>\r\n\r\n          <span *ngIf=\"column.value=='errorDescription'  && row.total\">\r\n            <mat-panel-title>\r\n              Total:&nbsp;{{row.total}}&nbsp;&nbsp;&nbsp;SaveCount:&nbsp;{{row.saveCount}}&nbsp;&nbsp;&nbsp;FailedCount:&nbsp;{{row.failedCount}}&nbsp;&nbsp;&nbsp;DuplicateCount:&nbsp;{{row.duplicateCount}}\r\n            </mat-panel-title>\r\n          </span>\r\n          <span *ngIf=\"column.value=='errorDescription' && !row.total\">\r\n            {{row[column.value]}}\r\n          </span>\r\n\r\n          <span *ngIf=\"column.value == 'select'\">\r\n            <mat-checkbox (click)=\"$event.stopPropagation()\" (change)=\"updateCheckClick(row, $event)\"\r\n              [checked]=\"row.checked\">\r\n            </mat-checkbox>\r\n          </span>\r\n          <span *ngIf=\"column.isStopDialog\" class=\"view-mode\">{{row[column.value]}}</span>\r\n        \r\n          <span *ngIf=\"column.isViewMode\" class=\"view-mode\" (click)=\"viewData(row,column)\">{{row[column.value]}}</span>\r\n          <span *ngIf=\"(column.value !== 'security_type' && column.value!== 'object_type' && column.value !== 'level')  \r\n            && column.type !== 'date' && !column.isViewMode && !column.showStatusToggle && !column.isStopDialog\r\n            && !column.mapFromConfig && column.value!=='issuetype' && column.value!=='securityType' && !column.expandCollapse && column.value!=='errorDescription'\" \r\n            [ngClass]=\"(row.statusClass && row.statusClass[row[column.value]] )? row.statusClass[row[column.value]] : ''\">\r\n            {{row[column.value]}}\r\n          </span>\r\n          <span *ngIf=\"column.expandCollapse\" [ngClass]=\"(row.expandCollapse) ? 'view-mode' : ''\" >{{row[column.value]}}</span>\r\n          <span *ngIf=\"column.type == 'date'\">{{row[column.value] | date:'short'}}</span>\r\n          <span *ngIf=\"column.showStatusToggle\">\r\n            <mat-slide-toggle fxFlex class=\"mat-accent\" aria-label=\"Cloud\" (change)=\"onToggleChange($event, row)\"\r\n              [checked]=\"(row[column.value] == 'ACTIVE' || row[column.value] === true)? true : false\">\r\n            </mat-slide-toggle>\r\n          </span>\r\n          <span *ngIf=\"column.value == 'action'\">\r\n            <!-- <span> Condition1 - {{column.condition | translate}}</span>\r\n            <span>R1 -{{column.condition}} </span> -->\r\n            <button *ngFor=\"let button of column.buttons\" mat-icon-button color=\"accent\"\r\n              (click)=\"actionClick(row, button)\">\r\n              <mat-icon *ngIf=\"column.isCondition && button=='save_alt' && (row.status=='Completed')\">{{button}}</mat-icon>\r\n              <mat-icon *ngIf=\"!column.isCondition\">{{button}}</mat-icon>\r\n            </button>\r\n          </span>\r\n          <span *ngIf=\"column.value == 'object_type'\">\r\n            <mat-form-field class=\"multi-select-dropdown\">\r\n              <mat-select [(ngModel)]=\"column.keyToShow ? row[column.keyToShow] : row[column.value]\"\r\n                (selectionChange)=\"onMultiSelect($event, row)\" multiple>\r\n                <mat-option *ngFor=\"let item of row.objectTypes\" [value]=\"item\">{{item}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </span>\r\n          <ng-container *ngIf=\"column.value == 'status' && column.conditioncheck && !column.rulesetCheck && row[column.value]==column.conditioncheck && inputData.datasourceId!=row._id\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-intiated ng-star-inserted reimport \"\r\n            (click)=\"actionClick(row,'askdefaultbutton' )\" >\r\n                 Set as Default\r\n               <!-- {{row[column.value]}} -->\r\n          </span> \r\n          </ng-container>\r\n          <ng-container *ngIf=\"column.value == 'status' && column.conditioncheck && column.rulesetCheck && row[column.value]==column.conditioncheck && !(column.defaultVal &&  row[column.defaultVal])\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-intiated ng-star-inserted reimport \"\r\n            (click)=\"actionRedirect(row,column)\" >\r\n                 Set as Default\r\n               <!-- {{row[column.value]}} -->\r\n          </span> \r\n          </ng-container>\r\n            <ng-container *ngIf=\"(column.value == 'status' || (column.defaultVal &&  row[column.defaultVal]))&& column.conditioncheck && row[column.value]==column.conditioncheck && (inputData.datasourceId==row._id || (column.defaultVal &&  row[column.defaultVal]))\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-success ng-star-inserted reimport \" \r\n            >\r\n                Default\r\n          </span> \r\n          </ng-container> \r\n          <!-- column.defaultVal &&  row[column.defaultVal] && row[column.value]=='Imported' -->\r\n          <!-- <ng-container *ngIf=\"column.value == 'status' && column.defaultVal =='defaultRuleset' && column.conditioncheck && row[column.value]==column.conditioncheck && inputData.rulesetId==row.defaultRuleset\"> \r\n            &nbsp;&nbsp; <span  class=\"label bg-success ng-star-inserted reimport \"\r\n            >\r\n                Default\r\n          </span> \r\n          </ng-container>  -->\r\n\r\n          <span *ngIf=\"column.value == 'reimport'\" class=\"label bg-success ng-star-inserted reimport\"\r\n            (click)=\"actionClick(row,'edit' )\">\r\n            Re-import\r\n          </span>\r\n          <span *ngIf=\"(column.value == 'security_type'|| column.value == 'level') && row['type'] != 'Role' && !column.mapFromConfig\" >\r\n            <mat-form-field class=\"multi-select-dropdown\" *ngIf=\"column.showOnlyselectedObj\">\r\n              <mat-select [(ngModel)]=\"row[column.value]\" (selectionChange)=\"onMultiSelect($event, row)\" multiple>\r\n                <mat-option *ngFor=\"let item of column[row[column.mapbyObject]]\" [value]=\"item.value\">{{item.name | translate}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n            <mat-form-field class=\"multi-select-dropdown\" *ngIf=\"!column.showOnlyselectedObj\">\r\n              <mat-select [(ngModel)]=\"row[column.value]\" (selectionChange)=\"onMultiSelect($event, row)\" multiple>\r\n                <mat-option *ngFor=\"let item of column.data\" [value]=\"item.value\">{{item.name | translate}}</mat-option>\r\n              </mat-select>\r\n            </mat-form-field>\r\n          </span>\r\n          <span *ngIf=\"column.isArray && !column.fromElement\">\r\n            <ng-container *ngFor=\"let data of row[column.keyToShow]; let i=index\">\r\n              {{column.data[data] | translate}}<span *ngIf=\"row[column.keyToShow].length-1>i\">, </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"column.isArray && column.fromElement\">\r\n            <ng-container *ngFor=\"let data of row[column.keyToShow]; let i=index\">\r\n              {{data}}<span *ngIf=\"row[column.keyToShow].length-1>i\">, </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"column.isArryObj\">\r\n            <ng-container *ngFor=\"let data of row[column.keyToShow]; let i=index\">\r\n              {{data[column.subKey] | translate }}<span *ngIf=\"row[column.keyToShow].length-1>i\">, </span>\r\n            </ng-container>\r\n          </span>\r\n          <span *ngIf=\"column.mapFromConfig\">\r\n            {{column.mapData[row[column.value]] | translate}}\r\n          </span>\r\n          <!-- <span *ngIf=\"column.value == 'role_type' || column.value=='role_desc'\">{{row[column.value]}}</span> -->\r\n          <!-- <span *ngIf=\"column.type == 'date'\">{{row[column.value] | date}}</span> -->\r\n        </td>\r\n      </ng-container>\r\n      <ng-container matColumnDef=\"expandedDetail\">\r\n        <td mat-cell *matCellDef=\"let element\" [attr.colspan]=\"displayedColumns.length\">\r\n           <div\r\n              class=\"example-element-detail\"\r\n              [@detailExpand]=\"element == expandedElement ? 'expanded' : 'collapsed'\"\r\n              *ngFor=\"let elementItem of element[element.expandElementKey]\"\r\n           >\r\n              <div class=\"example-element-description\" *ngIf=\"element.expandCollapse\">\r\n                 <ul>\r\n                    <li>\r\n                       {{ elementItem[element.keyToShowExpand] }}\r\n                    </li>\r\n                 </ul>\r\n              </div>\r\n           </div>\r\n        </td>\r\n     </ng-container>\r\n      <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n      <tr\r\n               mat-row\r\n               *matRowDef=\"let element; columns: displayedColumns\"\r\n               class=\"example-element-row\"\r\n               [class.example-expanded-row]=\"expandedElement === element\"\r\n               (click)=\"expandedElement = element\"\r\n            ></tr>\r\n            <tr mat-row *matRowDef=\"let row; columns: ['expandedDetail']\"  class=\"example-detail-row\"></tr>\r\n    </table>\r\n    <div [hidden]=\"tableData.length==0\">    \r\n     <mat-paginator #paginator *ngIf=\"!disablePagination&& !disablePaginator && viewFrom!='view';else select_Paginator\" [pageSizeOptions]=\"[10,25,50,100]\" [length]=\"length\"\r\n        [pageSize]=\"limit\" showFirstLastButtons (page)=\"changePage($event)\">\r\n      </mat-paginator>\r\n      <ng-template #select_Paginator>\r\n        <mat-paginator  *ngIf=\"!disablePaginator\" #selectedPaginator [pageSizeOptions]=\"[10,25,50,100]\" [length]=\"length\"\r\n        [pageSize]=\"limit\" showFirstLastButtons (page)=\"changePage($event)\">\r\n        </mat-paginator>\r\n     </ng-template> \r\n    </div>\r\n    \r\n    <div class=\"p-10\" style=\"    text-align: center;\r\n    \" *ngIf=\"tableData.length==0\">\r\n      <h2 class=\"m-0 font-weight-900 noRecords\">No Records Found</h2>\r\n\r\n    </div>\r\n  </div>\r\n  <!-- <div *ngIf=\"!length\">\r\n    <div class=\"toolbar p-12 mat-elevation-z8\" style=\"background-color: white;text-align: center;\r\n    font-size: large;\">\r\n        No Reports Found!\r\n    </div>\r\n  </div> -->\r\n</div>\r\n\r\n<!-- How to include this directive -->\r\n\r\n<!-- <table-layout [enableSearch]= \"true\" [tableData] = \"contentData\" [displayedColumns] = \"displayedColumns\"></table-layout> -->\r\n",
                animations: [
                    trigger('detailExpand', [
                        state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
                        state('expanded', style({ height: '*' })),
                        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
                    ]),
                ],
                styles: [".custom-mat-form{display:inline!important}.tabheade{padding-top:3px}.heading-label{font-size:14px;padding-top:10px}tr.example-detail-row{height:0}tr.example-element-row:not(.example-expanded-row):hover{background:#f5f5f5}tr.example-element-row:not(.example-expanded-row):active{background:#efefef}.res-table{height:100%;overflow-x:auto}.mat-table-sticky{background:#59abfd;opacity:1}.mat-cell,.mat-footer-cell,.mat-header-cell{min-width:80px;box-sizing:border-box}.mat-footer-row,.mat-header-row,.mat-row{min-width:1920px}table{width:100%;padding-top:10px}.mat-form-field-wrapper{padding-bottom:10px}.mat-header-cell{font-weight:700!important;color:#000!important}.selectall-export{text-align:center;margin:0 1%;padding:4px;font-size:14px;border-radius:4px;background:#ececeaab}.multi-select-dropdown{min-width:0;width:180px!important}.selectBoxStyle{font-size:50px!important}.view-mode{color:#039be5;font-size:14px;font-weight:600;cursor:pointer}.selectTH{position:absolute;top:31px}.noRecords{padding:25px 0;color:gray}.reimport{cursor:pointer}"]
            }] }
];
/** @nocollapse */
TableLayoutComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: ContentService },
    { type: MessageService },
    { type: MatDialog },
    { type: SnackBarService },
    { type: ChangeDetectorRef },
    { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] },
    { type: LoaderService }
];
TableLayoutComponent.propDecorators = {
    viewFrom: [{ type: Input }],
    onLoad: [{ type: Input }],
    tableId: [{ type: Input }],
    disablePagination: [{ type: Input }],
    selectAllBox: [{ type: ViewChild, args: ["selectAllBox",] }],
    table: [{ type: ViewChild, args: ["MatTable",] }],
    checkClickEventMessage: [{ type: Output }],
    actionClickEvent: [{ type: Output }],
    paginator: [{ type: ViewChild, args: ["paginator",] }],
    selectedPaginator: [{ type: ViewChild, args: ["selectedPaginator",] }]
};
export function base64ToArrayBuffer(base64) {
    const binaryString = window.atob(base64); // Comment this if not using base64
    const bytes = new Uint8Array(binaryString.length);
    return bytes.map((byte, i) => binaryString.charCodeAt(i));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFibGUtbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJ0YWJsZS1sYXlvdXQvdGFibGUtbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQUNULFNBQVMsRUFDVCxLQUFLLEVBQ0wsTUFBTSxFQUNOLFlBQVksRUFDWixNQUFNLEVBRU4saUJBQWlCLEVBQ2xCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFDTCxZQUFZLEVBQ1osa0JBQWtCLEVBQ2xCLFNBQVMsRUFDVCxXQUFXLEVBQ1gsUUFBUSxFQUNULE1BQU0sbUJBQW1CLENBQUM7QUFDM0IsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDakYsT0FBTyxLQUFLLFNBQVMsTUFBTSxZQUFZLENBQUM7QUFFeEMsT0FBTyxFQUVMLFdBQVcsRUFJWixNQUFNLGdCQUFnQixDQUFDO0FBRXhCLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLGtEQUFrRDtBQUNsRCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUU1QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxjQUFjLENBQUM7QUFDdkMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQWVsRCxNQUFNLE9BQU8sb0JBQW9CO0lBMkMvQixZQUNVLDZCQUEyRCxFQUMzRCxjQUE4QixFQUM5QixjQUE4QixFQUM5QixVQUFxQixFQUNyQixlQUFnQyxFQUNoQyxpQkFBb0MsRUFDYixXQUFXLEVBRWYsT0FBTyxFQUMxQixhQUE0QjtRQVQ1QixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQzNELG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBVztRQUNyQixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtRQUNiLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBRWYsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQUMxQixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQWpEN0Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBR2xDLDJCQUFzQixHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7UUFDcEQscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVUsQ0FBQztRQUN4RCx1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFDcEMsaUJBQVksR0FBWSxLQUFLLENBQUM7UUFDOUIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQUNwQixpQkFBWSxHQUFZLEtBQUssQ0FBQztRQVM5QixVQUFLLEdBQVcsRUFBRSxDQUFDO1FBQ25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFFbkIsaUJBQVksR0FBUSxFQUFFLENBQUM7UUFDdkIsY0FBUyxHQUFRLEVBQUUsQ0FBQztRQU1aLGdCQUFXLEdBQUcsSUFBSSxPQUFPLEVBQVEsQ0FBQztRQUNsQyxxQkFBZ0IsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBSS9DLHlCQUFvQixHQUFRLEVBQUUsQ0FBQztRQUUvQix1QkFBa0IsR0FBWSxLQUFLLENBQUM7UUFFcEMsbUJBQWMsR0FBWSxLQUFLLENBQUM7UUFFaEMscUJBQWdCLEdBQVksS0FBSyxDQUFDO1FBYWhDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7UUFDaEQsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzdEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUFpQ0k7UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsRUFBRSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNuRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1lBQ0YsSUFDRSxJQUFJLENBQUMsaUJBQWlCO2dCQUN0QixJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUTtnQkFDL0IsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxpQkFBaUIsRUFDakQ7Z0JBQ0EsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2FBQ2pCO1FBQ0gsQ0FBQyxDQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsY0FBYzthQUNoQixvQkFBb0IsRUFBRTthQUN0QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNqQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUdELFFBQVE7UUFDTiwwQ0FBMEM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsR0FBRyxLQUFLLENBQUM7UUFFcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztRQUN0QyxJQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsV0FBVyxHQUFHO1lBQ2pCLE1BQU0sRUFBRSxDQUFDO1lBQ1QsS0FBSyxFQUFFLEVBQUU7WUFDVCxVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjtTQUNuQyxDQUFDO1FBQ0Ysb0NBQW9DO1FBQ3BDLElBQUksSUFBSSxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FDakMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUMxQyxDQUFDO1FBQ0YsK0RBQStEO1FBQy9ELHlDQUF5QztRQUN6QywyQ0FBMkM7UUFDM0MsSUFBSTtRQUNKLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ3hELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsMENBQTBDO1FBQzFDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzVDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxXQUFXLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7SUFFeEIsQ0FBQztJQUNELGVBQWU7UUFDYixPQUFPLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDaEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUN2RCxJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztTQUNwRDtJQUVILENBQUM7SUFDRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELGVBQWU7UUFDYixJQUFJLGtCQUFrQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQ2pDLFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FDN0MsQ0FBQztRQUNGLElBQUksQ0FBQyxPQUFPLEdBQUcsa0JBQWtCLENBQUM7UUFDbEMsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO2FBQ2pDLE1BQU0sQ0FBQyxVQUFVLEdBQUc7WUFDbkIsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ3RCLENBQUMsQ0FBQzthQUNELEdBQUcsQ0FBQyxVQUFVLEdBQUc7WUFDaEIsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUNELFVBQVUsQ0FBQyxVQUFVO1FBQ25CLElBQUksQ0FBQyxvQkFBb0IsR0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ2pHLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFDM0IsSUFBSSxVQUFVLEdBQUcsSUFBSSxDQUFDLE1BQU07WUFDMUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztZQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUNqRCxJQUFJLFdBQVcsR0FDYixJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBQzFFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxDQUFDO1FBQy9ELGNBQWM7UUFDZCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7UUFDM0MsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztRQUM1RCxJQUFJLGlCQUFpQixHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUM1RyxJQUFJLGlCQUFpQixFQUFFO1lBQ3JCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO1lBQy9CLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx3QkFBd0IsQ0FBQztZQUNoRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQztZQUM3RCxJQUFJLE1BQU0sR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDO1lBQy9CLElBQUksWUFBWSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDdkMsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUM7aUJBQ3ZDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDZixJQUFJLFNBQVMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUUvRSxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1lBQ3BDLENBQUMsQ0FBQyxDQUFDO1NBQ047UUFDRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFO1lBQ2pFLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7WUFDbEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUM7WUFDM0IseUVBQXlFO1lBQ3pFLHNDQUFzQztZQUN0QyxNQUFNO1NBQ1A7UUFDRixzREFBc0Q7UUFDckQsSUFBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsRUFBQztZQUN6RCxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7WUFDMUQsSUFBSSxJQUFJLEdBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFBLENBQUMsQ0FBQSxFQUFFLENBQUM7WUFDakosSUFBRyxJQUFJLENBQUMsTUFBTSxHQUFDLENBQUMsRUFBQztnQkFDZixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsR0FBQyxJQUFJLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7YUFDL0I7U0FDRjtRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRTdDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUN2QyxJQUFJLENBQUMsV0FBVyxDQUNkLElBQUksQ0FBQyxnQkFBZ0IsRUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUNqQixXQUFXLENBQ1osQ0FBQztTQUNIO2FBQU07WUFDTCxJQUFJLElBQUksQ0FBQyxnQkFBZ0I7Z0JBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUNoRTtRQUNELElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDZiw4QkFBOEI7U0FDL0I7SUFDSCxDQUFDO0lBQ0QsT0FBTyxDQUFDLGlCQUFpQjtRQUN2QixJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQyxXQUFXLENBQUM7UUFDN0MsSUFBSSxpQkFBaUIsQ0FBQyxjQUFjLEVBQUU7WUFDcEMsSUFBSSxNQUFNLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztZQUNyRCxJQUFJLFlBQVksR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1lBQzdELElBQUksUUFBUSxHQUFHLGlCQUFpQixDQUFDLGNBQWMsQ0FBQyxvQkFBb0IsQ0FBQztZQUNyRSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztZQUN0RCxJQUFJLGNBQWMsR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3hGLHVEQUF1RDtZQUN2RCxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ2pDLEdBQUc7WUFDSCxJQUFJLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsQ0FBRSwwREFBMEQ7WUFDakksSUFBSSxjQUFjLEdBQUcsaUJBQWlCLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQztZQUNsRSxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxpQkFBaUIsR0FBRyxFQUFFLENBQUM7WUFDM0IsSUFBSSxXQUFXLEVBQUU7Z0JBQ2YsSUFBSSxPQUFPLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN4RCxJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUNyQyxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLFdBQVc7b0JBQzdDLElBQUksV0FBVyxDQUFDLGVBQWUsRUFBRTt3QkFDL0IsaUJBQWlCLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO3FCQUMxRTt5QkFBTTt3QkFDTCxpQkFBaUIsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7cUJBQzNIO2dCQUNILENBQUMsQ0FBQyxDQUFDO2FBQ0o7aUJBQU07Z0JBQ0wsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsVUFBVSxXQUFXO29CQUM3QyxJQUFJLFdBQVcsQ0FBQyxlQUFlLEVBQUU7d0JBQy9CLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQTtxQkFDMUU7eUJBQU0sSUFBSSxXQUFXLENBQUMsWUFBWSxFQUFFO3dCQUNuQyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsZUFBZTs0QkFDOUQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQzs0QkFDbkMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUM7cUJBQ3ZCO3lCQUFNO3dCQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxlQUFlOzRCQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUN2QztnQkFDSCxDQUFDLENBQUMsQ0FBQzthQUNKO1lBQ0QsSUFBSSxDQUFDLFdBQVcscUJBQVEsSUFBSSxDQUFDLFdBQVcsRUFBSyxpQkFBaUIsQ0FBRSxDQUFDO1lBQ2pFLElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUM7aUJBQ3ZDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEIsd0RBQXdEO2dCQUN4RCxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxHQUFHO2dCQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQztnQkFDeEQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO3FCQUNqQyxNQUFNLENBQUMsVUFBVSxHQUFHO29CQUNuQixPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUM7Z0JBQ3RCLENBQUMsQ0FBQztxQkFDRCxHQUFHLENBQUMsVUFBVSxHQUFHO29CQUNoQixPQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUM7Z0JBQ25CLENBQUMsQ0FBQyxDQUFDO2dCQUNMLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztnQkFDbkIsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBQyxtQkFBbUIsQ0FBQyxDQUFDO2dCQUN0QyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLEVBQUUsVUFBVSxJQUFJLEVBQUUsS0FBSztvQkFDMUQsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7d0JBQzVCLElBQUksT0FBTyxHQUFHLEVBQUUsQ0FBQzt3QkFDakIsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLElBQUksQ0FBQzt3QkFDekIsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO3dCQUNuQixTQUFTLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLEdBQUcsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDO3dCQUNsRixJQUFJLGFBQWEsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsRUFBRSxTQUFTLENBQUMsQ0FBQzt3QkFDM0YsSUFBRyxhQUFhLEVBQUM7NEJBQ2YsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQzt5QkFDM0I7d0JBQ0QsU0FBUyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDekI7eUJBQU07d0JBQ0wsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO3dCQUNqQixDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLGNBQWMsRUFBRSxVQUFVLFFBQVE7NEJBQzVELElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTtnQ0FDbkIsSUFBSSxRQUFRLENBQUMsbUJBQW1CLEVBQUU7b0NBQ2hDLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztvQ0FDbkosT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzt3Q0FDM0MsQ0FBQyxDQUFDLFdBQVc7d0NBQ2IsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQ0FDUjtxQ0FBTTtvQ0FDTCxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO3dDQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO3dDQUN2QyxDQUFDLENBQUMsRUFBRSxDQUFDO2lDQUNSOzZCQUNGO2lDQUFNLElBQUksUUFBUSxDQUFDLFdBQVcsRUFBRTtnQ0FDL0IsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDOzZCQUNuRDtpQ0FBTSxJQUFJLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRTtnQ0FDckMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDOzZCQUN6QztpQ0FBTTtnQ0FDTCxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7NkJBQy9DOzRCQUVELElBQUksaUJBQWlCLENBQUMsbUJBQW1CLEVBQUU7Z0NBQ3pDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO29DQUNwQixpQkFBaUIsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDOzZCQUN0RDt3QkFDSCxDQUFDLENBQUMsQ0FBQzt3QkFDSCxJQUFJLFNBQVMsR0FBRyxFQUFFLENBQUM7d0JBQ25CLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsYUFBYSxDQUFDLENBQUM7d0JBQy9FLElBQUksYUFBYSxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLFNBQVMsQ0FBQyxDQUFDO3dCQUMzRixJQUFHLGFBQWEsRUFBQzs0QkFDZixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzt5QkFDckI7d0JBQ0QsT0FBTyxxQkFBUSxJQUFJLEVBQUssT0FBTyxDQUFFLENBQUM7d0JBQ2xDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7cUJBQ3pCO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUNILElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7b0JBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBcUIsQ0FBQztpQkFDeEM7cUJBQU07b0JBQ0wsSUFBSSxDQUFDLFNBQVM7d0JBQ1osSUFBSSxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7NEJBQ2xELENBQUMsQ0FBRSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBYzs0QkFDM0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztpQkFDVjtnQkFDRCxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO2lCQUNuQztnQkFDRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUN6RCxzRUFBc0U7Z0JBQ3RFLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7Z0JBQ3JFLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUNwQyxJQUFJLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2dCQUNuRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsVUFBVSxDQUFDO29CQUNqQyxJQUFJLFdBQVcsSUFBSSxDQUFDLENBQUMsS0FBSyxFQUFFO3dCQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxDQUFDO3FCQUNyRTt5QkFBTTt3QkFDTCxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsR0FBRyxJQUFJLENBQUM7cUJBQ3BDO2dCQUNILENBQUMsQ0FBQyxDQUFDO2dCQUVILDJDQUEyQztnQkFDM0MsMEJBQTBCO2dCQUMxQixtREFBbUQ7Z0JBQ25ELHVDQUF1QztnQkFDdkMseUNBQXlDO2dCQUN6Qyx3RUFBd0U7Z0JBQ3hFLG1EQUFtRDtnQkFDbkQsMENBQTBDO2dCQUUxQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzlDLENBQUMsRUFDQyxHQUFHLENBQUMsRUFBRTtnQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM5QixDQUFDLENBQUMsQ0FBQztTQUNSO0lBQ0gsQ0FBQztJQUNELFdBQVcsQ0FBQyxZQUFZLEVBQUUsYUFBYSxFQUFFLFdBQVcsRUFBRSxXQUFXO1FBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxFQUFFLGtCQUFrQixDQUFDLENBQUM7UUFDOUMsSUFBSSxTQUFTLEdBQUcsRUFBRSxDQUFDO1FBQ25CLHFEQUFxRDtRQUNyRCxhQUFhLEdBQUcsV0FBVyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUN6RSxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQ3pDLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsSUFBSSxFQUFFLEtBQUs7Z0JBQzVDLElBQUksT0FBTyxJQUFJLEtBQUssUUFBUSxFQUFFO29CQUM1QixJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ2pCLE9BQU8sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUN0QyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN6QjtxQkFBTTtvQkFDTCxJQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7b0JBQ2pCLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGNBQWMsRUFBRSxVQUFVLFFBQVE7d0JBQ3ZELElBQUksUUFBUSxDQUFDLE1BQU0sRUFBRTs0QkFDbkIsSUFBSSxRQUFRLENBQUMsbUJBQW1CLEVBQUU7Z0NBQ2hDLElBQUksV0FBVyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQ0FDbkosT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztvQ0FDM0MsQ0FBQyxDQUFDLFdBQVc7b0NBQ2IsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs2QkFDUjtpQ0FBTTtnQ0FDTCxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO29DQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDO29DQUN2QyxDQUFDLENBQUMsRUFBRSxDQUFDOzZCQUNSO3lCQUNGOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDL0M7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsT0FBTyxxQkFBUSxJQUFJLEVBQUssT0FBTyxDQUFFLENBQUM7b0JBQ2xDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3pCO1lBQ0gsQ0FBQyxDQUFDLENBQUM7U0FDSjtRQUNELElBQUksQ0FBQyxPQUFPLEdBQUcsWUFBWSxDQUFDLFdBQVcsQ0FBQztRQUN4QyxhQUFhLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFDN0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQzVDLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxlQUFlLEVBQUU7WUFDaEQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLGVBQWUsRUFBRSxVQUFVLE9BQU87Z0JBQ3ZELENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxVQUFVLFdBQVc7b0JBQ2xELElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLFdBQVcsQ0FBQyxlQUFlO3dCQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDbkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN4QyxDQUFDLENBQUMsQ0FBQztnQkFDSCxJQUFJLENBQUMsY0FBYztxQkFDaEIsYUFBYSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQztxQkFDL0MsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO29CQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxjQUFjLENBQUMsQ0FBQztvQkFDbEMsSUFBSSxPQUFPLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDOUQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsYUFBYSxDQUFDLENBQUM7b0JBQ3BDLENBQUMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLFVBQVUsWUFBWTt3QkFDN0MsSUFDRSxJQUFJLENBQUMsUUFBUTs0QkFDYixJQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsSUFBSSxPQUFPLENBQUMsV0FBVzs0QkFDaEQsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFDakM7NEJBQ0EsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUM7Z0NBQzNCLE9BQU8sSUFBSSxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQztvQ0FDMUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVztvQ0FDeEQsQ0FBQyxDQUFDLEVBQUUsQ0FBQzs0QkFDVCxZQUFZLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7eUJBQ2hFOzs0QkFDQyxPQUFPLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO29CQUNuRCxDQUFDLENBQUMsQ0FBQztvQkFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxvQkFBb0IsQ0FBQyxDQUFDO2dCQUNuRCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUMsQ0FBQyxDQUFDO1NBQ0o7YUFBTTtZQUNMLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztZQUN4RCxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1NBQ2hFO1FBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFxQixDQUFDO1FBQ3ZDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUN4RCxJQUFJLENBQUMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQy9ELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxlQUFlLENBQUMsQ0FBQztRQUMzQyx3REFBd0Q7UUFDeEQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPO2FBQ2pDLE1BQU0sQ0FBQyxVQUFVLEdBQUc7WUFDbkIsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ3RCLENBQUMsQ0FBQzthQUNELEdBQUcsQ0FBQyxVQUFVLEdBQUc7WUFDaEIsT0FBTyxHQUFHLENBQUMsS0FBSyxDQUFDO1FBQ25CLENBQUMsQ0FBQyxDQUFDO1FBQ0wsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDO1FBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLHdDQUF3QyxDQUFDLENBQUM7UUFDOUUsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLHdCQUF3QixDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLGdCQUFnQixHQUFDLFlBQVksQ0FBQyxpQkFBaUIsQ0FBQSxDQUFDLENBQUEsWUFBWSxDQUFDLGlCQUFpQixDQUFBLENBQUMsQ0FBQSxLQUFLLENBQUM7UUFDMUYsaUJBQWlCO1FBQ2pCLGdDQUFnQztRQUNoQyx5REFBeUQ7UUFDekQsNkNBQTZDO0lBQy9DLENBQUM7SUFDRCxnQkFBZ0IsQ0FBQyxRQUFRLEVBQUUsS0FBSztRQUM5QixrREFBa0Q7UUFDbEQsc0NBQXNDO1FBQ3RDLFVBQVU7UUFDVixJQUFHLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUMzRDtZQUNFLFFBQVEsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztTQUNqRDtRQUVELGlEQUFpRDtRQUNqRCxtQ0FBbUM7UUFDbkMsVUFBVTtRQUNSLElBQUksa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFBLENBQUMsQ0FBQSxNQUFNLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGtCQUFrQixDQUFDLENBQUEsQ0FBQyxDQUFBLENBQUMsQ0FBQztRQUNySCxPQUFPLENBQUMsR0FBRyxDQUFDLDhDQUE4QyxFQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsQ0FBQyxDQUFBO1FBQ3BHLE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTRCLEVBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUMvRCxxQ0FBcUM7UUFDckMsa0NBQWtDO1FBQ2xDLHVDQUF1QztRQUN2QywrQkFBK0I7UUFDL0IsYUFBYTtRQUNiLDZDQUE2QztRQUM3QyxxQ0FBcUM7UUFDckMsT0FBTztRQUNGLElBQUksU0FBUyxHQUFDLEVBQUUsQ0FBQztRQUNqQixJQUFJLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLENBQUEsQ0FBQyxDQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxrQkFBa0IsR0FBRyxHQUFHLENBQUMsQ0FBQSxDQUFDLENBQUEsU0FBUyxDQUFDO1FBQzFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLEVBQUMsZUFBZSxDQUFDLENBQUM7UUFDcEQsSUFBRyxrQkFBa0IsSUFBSSxrQkFBa0IsR0FBQyxDQUFDLEVBQzdDO1lBQ0UsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQTtZQUNwQixJQUFHLGVBQWUsSUFBSSxlQUFlLENBQUMsTUFBTSxFQUM1QztnQkFDRSxTQUFTLEdBQUMsZUFBZSxDQUFDO2FBQzNCO2lCQUNEO2dCQUNFLEtBQUksSUFBSSxDQUFDLEdBQUMsQ0FBQyxFQUFDLENBQUMsSUFBRSxrQkFBa0IsRUFBQyxDQUFDLEVBQUUsRUFDckM7b0JBQ0MsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDbEI7YUFDRjtZQUNILFFBQVEsQ0FBQyxlQUFlLENBQUMsR0FBRSxTQUFTLENBQUM7U0FDckM7YUFBSztZQUNMLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUE7WUFDbEIsSUFBRyxrQkFBa0IsSUFBRSxDQUFDLEVBQ3hCO2dCQUNBLFFBQVEsQ0FBQyxlQUFlLENBQUMsR0FBRSxTQUFTLENBQUM7YUFDcEM7aUJBQ0Q7Z0JBQ0EsUUFBUSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNyQztTQUNMO1FBQ04sR0FBRztRQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDdEQsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFO1lBQ25FLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixFQUFFLFVBQVUsSUFBSTtnQkFDOUQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdDLENBQUMsQ0FBQyxDQUFDO1NBQ0o7UUFFRCxJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDMUQsSUFBSSxpQkFBaUIsR0FDbkIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTTtZQUMvRCxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZO1lBQzdCLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDVCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ2xFLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBRUwsMkNBQTJDO1lBQzNDLG9EQUFvRDtZQUNwRCxLQUFLO1lBQ0wsSUFBSSxTQUFTLEdBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUEsQ0FBQyxDQUFBLFVBQVUsQ0FBQztZQUN2RyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FDcEMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLEtBQUssUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUM5QyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3BDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyx5QkFBeUIsRUFBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDekQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQ3BELElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7WUFDN0QsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFdBQVcsQ0FBQztZQUNqRCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLEtBQUssUUFBUSxDQUFDLEdBQUcsRUFBRTtnQkFDbEQsSUFBSSxTQUFTLEdBQ1gsSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU07b0JBQzNDLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLFVBQVUsQ0FBQztvQkFDL0MsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDVCxTQUFTLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxVQUFVLE9BQU87b0JBQzVDLE9BQU8sT0FBTyxJQUFJLElBQUksQ0FBQztnQkFDekIsQ0FBQyxDQUFDLENBQUM7Z0JBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsU0FBUyxDQUFDO2FBQzFDO1NBQ0Y7UUFFRyw2Q0FBNkM7UUFDOUMsSUFBSSxXQUFXLEdBQUcsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUNsRyxJQUFHLFdBQVcsRUFBQztZQUNmLElBQUksSUFBSSxHQUFDLElBQUksQ0FBQztZQUNkLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxVQUFVLENBQUM7Z0JBQ2pDLElBQUksV0FBVyxJQUFJLENBQUMsQ0FBQyxLQUFLLEVBQUU7b0JBQzFCLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxXQUFXLENBQUM7aUJBQ3JFO3FCQUFNO29CQUNMLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxHQUFHLElBQUksQ0FBQztpQkFDcEM7WUFDSCxDQUFDLENBQUMsQ0FBQztTQUNKO1FBQ0UsNkNBQTZDO1FBQ2xELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFDbkUsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUNyRSxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFFRCxjQUFjLENBQUMsS0FBSztRQUNsQixJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssSUFBSSxFQUFFO1lBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDN0IsR0FBRyxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDckIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDO1lBQ3RELElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQ3ZFO2FBQU07WUFDTCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLEdBQUcsQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO1lBQ3RCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxFQUFFLENBQUM7WUFDcEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN2QztRQUNELFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQ0QsYUFBYSxDQUFDLElBQUksRUFBRSxVQUFVO1FBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLENBQUM7UUFDcEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLG1CQUFtQixDQUFDLENBQUE7UUFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBRUQsYUFBYTtRQUNYLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUM7UUFDdkQsSUFBSSxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2YsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLFdBQVcsRUFBRSxVQUFVLElBQUk7WUFDbEQsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxjQUFjO2FBQ2hCLGFBQWEsQ0FBQyxLQUFLLEVBQUUsY0FBYyxDQUFDLE1BQU0sQ0FBQzthQUMzQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDN0MsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRCxrQkFBa0I7UUFDaEIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFDRCxXQUFXLENBQUMsS0FBSyxFQUFFLEdBQUc7UUFDcEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxvQkFBb0IsR0FBRztZQUMxQixTQUFTLEVBQUUsR0FBRztZQUNkLFdBQVcsRUFBRSxLQUFLO1NBQ25CLENBQUM7UUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLGlDQUFpQyxFQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBRSxDQUFDO1FBRTFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUMsS0FBSyxDQUFDLENBQUMscUJBQXFCO1FBQ2hELElBQUksVUFBVSxHQUNaLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTO1lBQ2xDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDdkIsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUM7UUFDbkQsSUFBSSxLQUFLLEdBQUcsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUM7UUFDL0QsSUFBSSxhQUFhLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUNwRCxJQUFJLE9BQU8sR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO1FBQ3hDLElBQUksVUFBVSxHQUFHLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUN6RCxJQUFJLFVBQVUsRUFBRTtZQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNoQzthQUFNO1lBQ0wsSUFBSSxDQUFDLGdCQUFnQixHQUFHLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQyxJQUFJLEtBQUssR0FBRztnQkFDVixNQUFNLEVBQUUsQ0FBQztnQkFDVCxLQUFLLEVBQUUsRUFBRTtnQkFDVCxVQUFVLEVBQUUsSUFBSSxDQUFDLGlCQUFpQjthQUNuQyxDQUFDO1lBRUYsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLGVBQWUsRUFBRTtnQkFDbEQsS0FBSyxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLEtBQUssQ0FBQzthQUMvQztpQkFBTTtnQkFDTCxJQUFJLElBQUksR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLENBQUMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLFVBQVUsSUFBSTtvQkFDL0IsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQzNCLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQywrQ0FBK0M7Z0JBQ2xHLENBQUMsQ0FBQyxDQUFDO2FBQ0o7WUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztZQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO1NBQ3JDO0lBRUgsQ0FBQztJQUVELGlCQUFpQixDQUFDLFNBQVMsRUFBRSxHQUFHO1FBQzlCLE9BQU8sQ0FBQyxHQUFHLENBQUMsdUJBQXVCLENBQUMsQ0FBQztRQUNyQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDM0IsSUFBSSxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUM7UUFDbkMsSUFBSSxDQUFDLG9CQUFvQixHQUFHLEVBQUUsQ0FBQztRQUMvQixJQUFJLElBQUksR0FBRyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1FBQ2hELE9BQU8sQ0FBQyxHQUFHLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxHQUFHLENBQUMsQ0FBQztRQUN0RCxJQUFJLENBQUMsb0JBQW9CLEdBQUc7WUFDeEIsU0FBUyxFQUFFLEdBQUc7WUFDZCxXQUFXLEVBQUUsS0FBSztTQUNyQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLENBQUMsQ0FBQyxxQkFBcUI7UUFDbEQsSUFBSSxVQUFVLEdBQ1YsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVM7WUFDaEMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUztZQUN2QixDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVMsQ0FBQztRQUN2RCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQztRQUMvRCxJQUFJLGFBQWEsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsYUFBYSxDQUFDO1FBQ3BELElBQUksT0FBTyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7UUFDeEMsSUFBSSxVQUFVLEdBQUcsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3pELElBQUksVUFBVSxFQUFFO1lBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO1NBQ2xDO2FBQU07WUFDSCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzFDLElBQUksS0FBSyxHQUFHO2dCQUNSLE1BQU0sRUFBRSxDQUFDO2dCQUNULEtBQUssRUFBRSxFQUFFO2dCQUNULFVBQVUsRUFBRSxJQUFJLENBQUMsaUJBQWlCO2FBQ3JDLENBQUM7WUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQ2xELElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxlQUFlLEVBQUU7Z0JBQ2hELEtBQUssQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxLQUFLLENBQUM7YUFDakQ7aUJBQ0k7Z0JBQ0QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUNoQixDQUFDLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRSxVQUFVLElBQUk7b0JBQzdCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO3dCQUN6QixLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsK0NBQStDO2dCQUN0RyxDQUFDLENBQUMsQ0FBQzthQUNOO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7SUFDQyxXQUFXLENBQUMsS0FBSztRQUNmLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xCLENBQUM7SUFDRCxhQUFhLENBQUMsS0FBSyxFQUFFLE1BQU07UUFDekIsSUFBSSxJQUFJLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzFELElBQUksWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDO1FBQy9DLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxTQUFTLENBQUMsWUFBWSxFQUFFLEVBQUUsUUFBUSxFQUFFLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQ3JFLElBQUksS0FBSyxHQUFHLENBQUMsQ0FBQyxJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQ3JELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQy9ELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ3ZELElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1NBQzlEO1FBQ0QsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztJQUN2RSxDQUFDO0lBRUQsVUFBVSxDQUFDLEtBQUs7UUFDZCxJQUFJLEtBQUssQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNoQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDM0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDaEM7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO1lBQzFELElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztTQUN4QztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUIsQ0FBQztJQUNELFFBQVEsQ0FBQyxPQUFPLEVBQUUsR0FBRztRQUNuQixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxnQ0FBZ0MsRUFBRSxHQUFHLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNyRixJQUFJLEdBQUcsQ0FBQyxVQUFVLEVBQUU7WUFDbEIsSUFBSSxPQUFPLEdBQUcsb0NBQW9DLENBQUE7WUFDbEQsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxFQUFFLEdBQUcsT0FBTyxDQUFDO1lBRXZELE9BQU8sQ0FBQyxHQUFHLENBQUMsaUNBQWlDLENBQUMsQ0FBQTtZQUM5QyxrRUFBa0U7U0FFbkU7YUFBTTtZQUNMLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ25DO0lBQ0gsQ0FBQztJQUNELGNBQWMsQ0FBQyxPQUFPLEVBQUUsTUFBTTtRQUM1QixPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDbEMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQztJQUNELFdBQVcsQ0FBQyxPQUFPLEVBQUUsTUFBTTtRQUN6QixPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxhQUFhLENBQUMsQ0FBQTtRQUNuQyxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUksTUFBTSxJQUFJLE1BQU0sSUFBSSxNQUFNLElBQUcsTUFBTSxFQUFFO1lBQzNELElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxNQUFNLElBQUksUUFBUSxFQUFFO1lBQzdCLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1lBQ2pDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFDdkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUM7Z0JBQ3BDLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxFQUFFO29CQUM5QyxJQUFJLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUM1QyxzQ0FBc0M7b0JBQ3RDLDBCQUEwQjtvQkFDMUIsS0FBSztvQkFDTCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBQyxPQUFPLENBQUMsQ0FBQztvQkFDOUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsRUFBQyxLQUFLLENBQUMsQ0FBQztvQkFDeEMsSUFBRyxLQUFLLEdBQUMsQ0FBQyxDQUFDLEVBQUM7d0JBQ1YsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7cUJBQy9CO29CQUNELElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsWUFBWSxDQUFDO29CQUN4QyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBQzlELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDdkQsSUFBSSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsTUFBTSxDQUFDO29CQUNsQyxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO29CQUNyRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2lCQUN2QzthQUNGO2lCQUFNO2dCQUNMLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksRUFBRTtvQkFDakQsSUFBSSxTQUFTLEdBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGdCQUFnQixDQUFBLENBQUMsQ0FBQSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZ0JBQWdCLENBQUEsQ0FBQyxDQUFBLFVBQVUsQ0FBQztvQkFDdkcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBQyxTQUFTLENBQUMsQ0FBQztvQkFDekMsSUFBSSxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUM7b0JBQy9DLHNDQUFzQztvQkFDdEMsa0RBQWtEO29CQUNsRCxLQUFLO29CQUNMLElBQUksS0FBSyxHQUFHLFlBQVksQ0FBQyxTQUFTLENBQ2hDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FDNUMsQ0FBQztvQkFDRixZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsR0FBRyxZQUFZLENBQUM7b0JBQzlDLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFlBQVksRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFDOUQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUN2RCxJQUFJLENBQUMsTUFBTSxHQUFHLFlBQVksQ0FBQyxNQUFNLENBQUM7b0JBQ2xDLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7b0JBQ3JFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQ3ZDO2FBQ0Y7U0FDRjthQUFNLElBQUksTUFBTSxJQUFJLGdCQUFnQixFQUFFO1lBQ3JDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxNQUFNLElBQUksY0FBYyxFQUFFO1lBQ25DLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQ2xDO2FBQU0sSUFBSSxNQUFNLElBQUksa0JBQWtCLEVBQUU7WUFDdkMsSUFBSSxJQUFJLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQztZQUN2QixJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQzFCLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDaEQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLDBDQUEwQyxDQUMzQyxDQUNGLENBQUM7U0FDSDthQUFNLElBQUksTUFBTSxJQUFJLFVBQVUsRUFBRTtZQUMvQixJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLGVBQWUsQ0FBQztZQUN0RCxJQUFJLEtBQUssR0FBRztnQkFDVixRQUFRLEVBQUUsT0FBTyxDQUFDLEdBQUc7YUFDdEIsQ0FBQTtZQUNELElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsS0FBSyxFQUFFLGNBQWMsQ0FBQyxNQUFNLENBQUM7aUJBQzNDLFNBQVMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEIsTUFBTSxFQUFFLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDN0MsTUFBTSxJQUFJLEdBQUcsSUFBSSxVQUFVLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ2hDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtvQkFDekMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7aUJBQ3hCO2dCQUNELElBQUksWUFBWSxHQUFHLENBQUMsT0FBTyxDQUFDLFFBQVEsSUFBSSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUM7Z0JBQzFFLE1BQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFLENBQUMsQ0FBQztnQkFDcEQsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDbkUsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFDRCxjQUFjLENBQUMsT0FBTyxFQUFDLFVBQVU7UUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUMvQyxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUE7UUFDMUMsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2xCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixFQUFFLGNBQWMsQ0FBQyxDQUFDO1FBQ3RELElBQUksVUFBVSxDQUFDLFlBQVksRUFBRTtZQUMzQixJQUFJLG1CQUFtQixHQUFHLGNBQWMsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1lBQ3BFLENBQUMsQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsVUFBVSxJQUFJO2dCQUNoRSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNO29CQUMvQixDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDO29CQUNsQyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxjQUFjO2lCQUNoQixhQUFhLENBQUMsUUFBUSxFQUFFLGNBQWMsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUM7aUJBQ3pFLFNBQVMsQ0FDUixHQUFHLENBQUMsRUFBRTtnQkFDSixJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xCLENBQUMsRUFDRCxLQUFLLENBQUMsRUFBRTtnQkFDTixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztZQUNKLE9BQU8sQ0FBQyxHQUFHLENBQUMsMENBQTBDLEVBQUUsUUFBUSxDQUFDLENBQUE7U0FDbEU7YUFBTTtTQUVOO0lBRUgsQ0FBQztJQUNELGNBQWMsQ0FBQyxLQUFLLEVBQUUsT0FBTztRQUMzQixPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEtBQUssRUFBRSxZQUFZLEVBQUUsT0FBTyxDQUFDLENBQUM7UUFDakUsT0FBTyxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLGNBQWMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCO1lBQ3hDLENBQUMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYztZQUN0QyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQ1AsSUFBSSxjQUFjLEVBQUU7WUFDbEIsSUFBSSxtQkFBbUIsR0FBRyxjQUFjLENBQUMsWUFBWSxDQUFDO1lBQ3RELElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztZQUNoQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7WUFDbEIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsV0FBVyxFQUFFLFVBQVUsSUFBSTtnQkFDbEQsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTTtvQkFDL0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQztvQkFDbEMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7WUFDSCxJQUFJLENBQUMsY0FBYztpQkFDaEIsYUFBYSxDQUFDLFFBQVEsRUFBRSxjQUFjLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUM7aUJBQzNELFNBQVMsQ0FDUixHQUFHLENBQUMsRUFBRTtnQkFDSCxJQUFHLE9BQU8sQ0FBQyxNQUFNLElBQUUsUUFBUSxJQUFLLG1CQUFtQixDQUFDLE1BQU0sRUFDMUQ7b0JBQ0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE1BQU0sQ0FDM0IsQ0FDRixDQUFDO2lCQUNGO3FCQUFLLElBQUcsT0FBTyxDQUFDLE1BQU0sSUFBRSxVQUFVLElBQUssbUJBQW1CLENBQUMsT0FBTyxFQUNuRTtvQkFDQyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FDdEIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsT0FBTyxDQUM1QixDQUNGLENBQUM7aUJBQ0Y7cUJBQ0Q7b0JBQ0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2lCQUNGO1lBRUosQ0FBQyxFQUNELEtBQUssQ0FBQyxFQUFFO2dCQUNOLElBQUksQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUMxQixJQUFJLENBQUMsNkJBQTZCLENBQUMsT0FBTyxDQUN4QyxtQkFBbUIsQ0FBQyxLQUFLLENBQzFCLENBQ0YsQ0FBQztZQUNKLENBQUMsQ0FDRixDQUFDO1NBQ0w7YUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsa0JBQWtCLEVBQUU7WUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztZQUMxRSxZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1NBQ3RFO0lBQ0gsQ0FBQztJQUNELFVBQVUsQ0FBQyxPQUFPLEVBQUUsTUFBTTtRQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLGtDQUFrQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1FBQ3pELElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQy9ELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVU7YUFDN0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzFCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLEtBQUssRUFBRSxVQUFVO1lBQ2pCLFVBQVUsRUFBRSxxQkFBcUI7WUFDakMsSUFBSSxFQUFFO2dCQUNKLE1BQU0sRUFBRSxNQUFNO2dCQUNkLFNBQVMsRUFBRSxPQUFPO2FBQ25CO1NBQ0YsQ0FBQzthQUNELFdBQVcsRUFBRTthQUNiLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUNwQixZQUFZLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDOzs7WUFuOEJGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsY0FBYztnQkFDeEIsNDFlQUE0QztnQkFFNUMsVUFBVSxFQUFFO29CQUNWLE9BQU8sQ0FBQyxjQUFjLEVBQUU7d0JBQ3RCLEtBQUssQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDO3dCQUM3RSxLQUFLLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUN6QyxVQUFVLENBQUMsd0JBQXdCLEVBQUUsT0FBTyxDQUFDLHNDQUFzQyxDQUFDLENBQUM7cUJBQ3RGLENBQUM7aUJBQ0g7O2FBQ0Y7Ozs7WUF4QlEsNEJBQTRCO1lBSTVCLGNBQWM7WUFDZCxjQUFjO1lBckJyQixTQUFTO1lBSUYsZUFBZTtZQVR0QixpQkFBaUI7NENBZ0dkLE1BQU0sU0FBQyxhQUFhOzRDQUVwQixNQUFNLFNBQUMsU0FBUztZQW5FWixhQUFhOzs7dUJBZ0JuQixLQUFLO3FCQUNMLEtBQUs7c0JBQ0wsS0FBSztnQ0FDTCxLQUFLOzJCQUNMLFNBQVMsU0FBQyxjQUFjO29CQUN4QixTQUFTLFNBQUMsVUFBVTtxQ0FDcEIsTUFBTTsrQkFDTixNQUFNO3dCQXVHTixTQUFTLFNBQUMsV0FBVztnQ0FDckIsU0FBUyxTQUFDLG1CQUFtQjs7QUEwMEJoQyxNQUFNLFVBQVUsbUJBQW1CLENBQUMsTUFBYztJQUNoRCxNQUFNLFlBQVksR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsbUNBQW1DO0lBQzdFLE1BQU0sS0FBSyxHQUFHLElBQUksVUFBVSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNsRCxPQUFPLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDNUQsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIFZpZXdDaGlsZCxcclxuICBJbnB1dCxcclxuICBPdXRwdXQsXHJcbiAgRXZlbnRFbWl0dGVyLFxyXG4gIEluamVjdCxcclxuICBPbkluaXQsXHJcbiAgQ2hhbmdlRGV0ZWN0b3JSZWZcclxufSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5pbXBvcnQge1xyXG4gIE1hdFBhZ2luYXRvcixcclxuICBNYXRUYWJsZURhdGFTb3VyY2UsXHJcbiAgTWF0RGlhbG9nLFxyXG4gIE1hdENoZWNrYm94LFxyXG4gIE1hdFRhYmxlXHJcbn0gZnJvbSBcIkBhbmd1bGFyL21hdGVyaWFsXCI7XHJcbmltcG9ydCB7IFNuYWNrQmFyU2VydmljZSB9IGZyb20gXCIuLy4uL3NoYXJlZC9zbmFja2Jhci5zZXJ2aWNlXCI7XHJcbmltcG9ydCB7IGFuaW1hdGUsIHN0YXRlLCBzdHlsZSwgdHJhbnNpdGlvbiwgdHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgKiBhcyBGaWxlU2F2ZXIgZnJvbSBcImZpbGUtc2F2ZXJcIjtcclxuXHJcbmltcG9ydCB7XHJcbiAgRm9ybUJ1aWxkZXIsXHJcbiAgRm9ybUNvbnRyb2wsXHJcbiAgRm9ybUdyb3VwLFxyXG4gIFZhbGlkYXRvcnMsXHJcbiAgRm9ybUFycmF5XHJcbn0gZnJvbSBcIkBhbmd1bGFyL2Zvcm1zXCI7XHJcblxyXG5pbXBvcnQgeyBGdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlIH0gZnJvbSBcIi4uL0BmdXNlL3NlcnZpY2VzL3RyYW5zbGF0aW9uLWxvYWRlci5zZXJ2aWNlXCI7XHJcbi8vIGltcG9ydCB7IGxvY2FsZSBhcyBlbmdsaXNoIH0gZnJvbSBcIi4uL2kxOG4vZW5cIjtcclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcblxyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gXCIuLi9jb250ZW50L2NvbnRlbnQuc2VydmljZVwiO1xyXG5pbXBvcnQgeyBNZXNzYWdlU2VydmljZSB9IGZyb20gXCIuLi9fc2VydmljZXMvaW5kZXhcIjtcclxuaW1wb3J0IHsgTW9kZWxMYXlvdXRDb21wb25lbnQgfSBmcm9tIFwiLi4vbW9kZWwtbGF5b3V0L21vZGVsLWxheW91dC5jb21wb25lbnRcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcbmltcG9ydCB7IFNlbGVjdGlvbk1vZGVsIH0gZnJvbSBcIkBhbmd1bGFyL2Nkay9jb2xsZWN0aW9uc1wiO1xyXG5pbXBvcnQgeyBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi4vbG9hZGVyLnNlcnZpY2UnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gIHNlbGVjdG9yOiBcInRhYmxlLWxheW91dFwiLFxyXG4gIHRlbXBsYXRlVXJsOiBcIi4vdGFibGUtbGF5b3V0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL3RhYmxlLWxheW91dC5jb21wb25lbnQuc2Nzc1wiXSxcclxuICBhbmltYXRpb25zOiBbXHJcbiAgICB0cmlnZ2VyKCdkZXRhaWxFeHBhbmQnLCBbXHJcbiAgICAgIHN0YXRlKCdjb2xsYXBzZWQnLCBzdHlsZSh7IGhlaWdodDogJzBweCcsIG1pbkhlaWdodDogJzAnLCBkaXNwbGF5OiAnbm9uZScgfSkpLFxyXG4gICAgICBzdGF0ZSgnZXhwYW5kZWQnLCBzdHlsZSh7IGhlaWdodDogJyonIH0pKSxcclxuICAgICAgdHJhbnNpdGlvbignZXhwYW5kZWQgPD0+IGNvbGxhcHNlZCcsIGFuaW1hdGUoJzIyNW1zIGN1YmljLWJlemllcigwLjQsIDAuMCwgMC4yLCAxKScpKSxcclxuICAgIF0pLFxyXG4gIF0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUYWJsZUxheW91dENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgQElucHV0KCkgdmlld0Zyb206IGFueTtcclxuICBASW5wdXQoKSBvbkxvYWQ6IGFueTtcclxuICBASW5wdXQoKSB0YWJsZUlkOiBhbnk7XHJcbiAgQElucHV0KCkgZGlzYWJsZVBhZ2luYXRpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBAVmlld0NoaWxkKFwic2VsZWN0QWxsQm94XCIpIHNlbGVjdEFsbEJveDogTWF0Q2hlY2tib3g7XHJcbiAgQFZpZXdDaGlsZChcIk1hdFRhYmxlXCIpIHRhYmxlOiBNYXRUYWJsZTxhbnk+O1xyXG4gIEBPdXRwdXQoKSBjaGVja0NsaWNrRXZlbnRNZXNzYWdlID0gbmV3IEV2ZW50RW1pdHRlcjxzdHJpbmc+KCk7XHJcbiAgQE91dHB1dCgpIGFjdGlvbkNsaWNrRXZlbnQgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuICBlbmFibGVTZWxlY3RPcHRpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICBlbmFibGVTZWFyY2g6IGJvb2xlYW4gPSBmYWxzZTtcclxuICB0YWJsZURhdGE6IGFueSA9IHt9O1xyXG4gIGVuYWJsZUFjdGlvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gIGRpc3BsYXllZENvbHVtbnM6IGFueTtcclxuICBjb2x1bW5zOiBhbnk7XHJcbiAgdGFibGVMaXN0OiBhbnk7XHJcbiAgY3VycmVudENvbmZpZ0RhdGE6IGFueTtcclxuICBjdXJyZW50RGF0YTogYW55O1xyXG4gIGV4cGFuZGVkRWxlbWVudDogYW55O1xyXG4gIC8vIEBWaWV3Q2hpbGQoJ3ZpZXdNZScsIHsgc3RhdGljOiBmYWxzZSB9KVxyXG4gIGRhdGFTb3VyY2U6IGFueTtcclxuICBsaW1pdDogbnVtYmVyID0gMTA7XHJcbiAgb2Zmc2V0OiBudW1iZXIgPSAwO1xyXG4gIGxlbmd0aDogbnVtYmVyO1xyXG4gIHNlbGVjdGVkRGF0YTogYW55ID0gW107XHJcbiAgaW5wdXREYXRhOiBhbnkgPSB7fTtcclxuICBxdWVyeVBhcmFtczogYW55O1xyXG4gIGRlZmF1bHREYXRhc291cmNlOiBhbnk7XHJcbiAgZGlhbG9nUmVmOiBhbnk7XHJcbiAgY3VycmVudFRhYmxlRGF0YTogYW55O1xyXG4gIGRhdGE6IGFueTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlTW9kZWwgPSBuZXcgU3ViamVjdDx2b2lkPigpO1xyXG4gIGlucHV0VmFsdWU6IGFueTtcclxuICBmcm9tRmllbGRWYWx1ZTogYW55O1xyXG4gIHRvRmllbGRWYWx1ZTogYW55O1xyXG4gIGN1cnJlbnRGaWx0ZXJlZFZhbHVlOiBhbnkgPSB7fTtcclxuICBvcHRpb25GaWx0ZXJkYXRhOiBhbnk7XHJcbiAgZW5hYmxlT3B0aW9uRmlsdGVyOiBib29sZWFuID0gZmFsc2U7XHJcbiAgb3B0aW9uRmlsdGVyRmllbGRzOiBhbnk7XHJcbiAgZW5hYmxlVUlmaWx0ZXI6IGJvb2xlYW4gPSBmYWxzZTtcclxuICByZWRpcmVjdFVyaTogc3RyaW5nO1xyXG4gIGRpc2FibGVQYWdpbmF0b3I6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2U6IEZ1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UsXHJcbiAgICBwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSxcclxuICAgIHByaXZhdGUgbWVzc2FnZVNlcnZpY2U6IE1lc3NhZ2VTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBfbWF0RGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICBwcml2YXRlIHNuYWNrQmFyU2VydmljZTogU25hY2tCYXJTZXJ2aWNlLFxyXG4gICAgcHJpdmF0ZSBjaGFuZ2VEZXRlY3RvclJlZjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICBASW5qZWN0KFwiZW52aXJvbm1lbnRcIikgcHJpdmF0ZSBlbnZpcm9ubWVudCxcclxuXHJcbiAgICBASW5qZWN0KFwiZW5nbGlzaFwiKSBwcml2YXRlIGVuZ2xpc2gsXHJcbiAgICBwcml2YXRlIGxvYWRlclNlcnZpY2U6IExvYWRlclNlcnZpY2VcclxuICApIHtcclxuICAgIHRoaXMucmVkaXJlY3RVcmkgPSB0aGlzLmVudmlyb25tZW50LnJlZGlyZWN0VXJpO1xyXG4gICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gICAgLyogLy8gdGVtcG9yYXJpbHkgY29tbWVudGVkXHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLm1vZGVsQ2xvc2VNZXNzYWdlXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlTW9kZWwpKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiPj4+PmRhdGFcIilcclxuICAgICAgICB0aGlzLnNlbGVjdGVkRGF0YSA9IFtdO1xyXG4gICAgICAgIGlmIChkYXRhICE9IDApIHtcclxuICAgICAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gW107XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gW107XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnRhYmxlRGF0YSk7XHJcbiAgICAgICAgY29uc29sZS5sb2codGhpcy5kYXRhU291cmNlKTtcclxuICAgICAgICAvLyBfLmZvckVhY2godGhpcy50YWJsZURhdGEsIGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAvLyAgIGl0ZW0uY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgIC8vIH0pO1xyXG5cclxuICAgICAgICAvLyB0aGlzLmRhdGFTb3VyY2UuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICAvLyAgIG9iai5jaGVja2VkID0gZmFsc2U7XHJcbiAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHRoaXMuZGF0YVNvdXJjZS5kYXRhLCBcIl9pZFwiKTtcclxuICAgICAgICAvLyAvLyBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgIC8vIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICAgICAgICB0aGlzLmRhdGEgPSBkYXRhO1xyXG4gICAgICAgIC8vIGlmKHRoaXMuZGF0YSA9PT0gJ2xpc3RWaWV3Jyl7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YSkge1xyXG4gICAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIC8vIH1cclxuICAgICAgfSk7XHJcbiAgICAgICovXHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLmdldE1lc3NhZ2UoKS5zdWJzY3JpYmUobWVzc2FnZSA9PiB7XHJcbiAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgPSBKU09OLnBhcnNlKFxyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICAgKTtcclxuICAgICAgaWYgKFxyXG4gICAgICAgIHRoaXMuY3VycmVudENvbmZpZ0RhdGEgJiZcclxuICAgICAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhLmxpc3RWaWV3ICYmXHJcbiAgICAgICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YS5saXN0Vmlldy5lbmFibGVUYWJsZUxheW91dFxyXG4gICAgICApIHtcclxuICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2VcclxuICAgICAgLmdldFRhYmxlSGVhZGVyVXBkYXRlKClcclxuICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMudW5zdWJzY3JpYmUpKVxyXG4gICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgIHRoaXMudXBkYXRlVGFibGVWaWV3KCk7XHJcbiAgICAgIH0pO1xyXG4gIH1cclxuICBAVmlld0NoaWxkKFwicGFnaW5hdG9yXCIpIHBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIEBWaWV3Q2hpbGQoXCJzZWxlY3RlZFBhZ2luYXRvclwiKSBzZWxlY3RlZFBhZ2luYXRvcjogTWF0UGFnaW5hdG9yO1xyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgLy8gdGhpcy5jaGFuZ2VEZXRlY3RvclJlZi5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiLi4uLi4udGFibGUgdGhpc1wiKTtcclxuICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VcIik7XHJcbiAgICB0aGlzLnF1ZXJ5UGFyYW1zID0ge1xyXG4gICAgICBvZmZzZXQ6IDAsXHJcbiAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZVxyXG4gICAgfTtcclxuICAgIC8vICB0aGlzLnBhZ2luYXRvcltcInBhZ2VJbmRleFwiXSA9IDA7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgdGhpcy5jdXJyZW50Q29uZmlnRGF0YSA9IEpTT04ucGFyc2UoXHJcbiAgICAgIGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudENvbmZpZ0RhdGFcIilcclxuICAgICk7XHJcbiAgICAvLyBpZiAodGhpcy5kYXRhID09ICdsaXN0VmlldycgfHwgdGhpcy5kYXRhID09ICdyZWZyZXNoUGFnZScpIHtcclxuICAgIC8vICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgIC8vICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgLy8gfVxyXG4gICAgdGhpcy5pbnB1dERhdGFbXCJkYXRhc291cmNlSWRcIl0gPSB0aGlzLmRlZmF1bHREYXRhc291cmNlO1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIC8vIHRoaXMuY2hhbmdlRGV0ZWN0b3JSZWYuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgdGhpcy5mcm9tRmllbGRWYWx1ZSA9IG5ldyBGb3JtQ29udHJvbChbXCJcIl0pO1xyXG4gICAgdGhpcy50b0ZpZWxkVmFsdWUgPSBuZXcgRm9ybUNvbnRyb2woW1wiXCJdKTtcclxuICAgIHRoaXMub25Mb2FkRGF0YShudWxsKTtcclxuXHJcbiAgfVxyXG4gIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgIGNvbnNvbGUubG9nKFwibmdBZnRlclZpZXdJbml0IFwiKTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0ZWRQYWdpbmF0b3IsIFwic2VsZWN0ZWRQYWc+Pj4+XCIpO1xyXG4gICAgaWYgKHRoaXMuZGF0YVNvdXJjZSAmJiB0aGlzLnNlbGVjdGVkUGFnaW5hdG9yKSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnNlbGVjdGVkUGFnaW5hdG9yO1xyXG4gICAgfVxyXG5cclxuICB9XHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlLm5leHQoKTtcclxuICAgIHRoaXMudW5zdWJzY3JpYmVNb2RlbC5uZXh0KCk7XHJcbiAgfVxyXG5cclxuICB1cGRhdGVUYWJsZVZpZXcoKSB7XHJcbiAgICBsZXQgY3VycmVudFRhYmxlSGVhZGVyID0gSlNPTi5wYXJzZShcclxuICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKVxyXG4gICAgKTtcclxuICAgIHRoaXMuY29sdW1ucyA9IGN1cnJlbnRUYWJsZUhlYWRlcjtcclxuICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IHRoaXMuY29sdW1uc1xyXG4gICAgICAuZmlsdGVyKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsLmlzQWN0aXZlO1xyXG4gICAgICB9KVxyXG4gICAgICAubWFwKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICByZXR1cm4gdmFsLnZhbHVlO1xyXG4gICAgICB9KTtcclxuICB9XHJcbiAgb25Mb2FkRGF0YShlbmFibGVOZXh0KSB7XHJcbiAgICB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlPSFfLmlzRW1wdHkodGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSkgPyB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlIDoge307XHJcbiAgICB0aGlzLmNvbHVtbnMgPSBbXTtcclxuICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IFtdO1xyXG4gICAgbGV0IHRhYmxlQXJyYXkgPSB0aGlzLm9uTG9hZFxyXG4gICAgICA/IHRoaXMub25Mb2FkLnRhYmxlRGF0YVxyXG4gICAgICA6IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXS50YWJsZURhdGE7XHJcbiAgICBsZXQgcmVzcG9uc2VLZXkgPVxyXG4gICAgICB0aGlzLm9uTG9hZCAmJiB0aGlzLm9uTG9hZC5yZXNwb25zZUtleSA/IHRoaXMub25Mb2FkLnJlc3BvbnNlS2V5IDogbnVsbDtcclxuICAgIGxldCBpbmRleCA9IF8uZmluZEluZGV4KHRhYmxlQXJyYXksIHsgdGFibGVJZDogdGhpcy50YWJsZUlkIH0pO1xyXG4gICAgLy9pZihpbmRleD4tMSlcclxuICAgIHRoaXMuY3VycmVudFRhYmxlRGF0YSA9IHRhYmxlQXJyYXlbaW5kZXhdO1xyXG4gICAgY29uc29sZS5sb2codGFibGVBcnJheSwgXCIuLi4uLnRhYmxlQXJyYXlcIik7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmN1cnJlbnRUYWJsZURhdGEsIFwiLi4uLi5jdXJyZW50VGFibGVEYXRhXCIpO1xyXG4gICAgbGV0IGNoZWNrT3B0aW9uRmlsdGVyID0gKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEuZW5hYmxlT3B0aW9uc0ZpbHRlcikgPyB0cnVlIDogZmFsc2U7XHJcbiAgICBpZiAoY2hlY2tPcHRpb25GaWx0ZXIpIHtcclxuICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICB0aGlzLmVuYWJsZU9wdGlvbkZpbHRlciA9IHRydWU7XHJcbiAgICAgIGxldCBPcHRpb25kYXRhID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLmZpbHRlck9wdGlvbkxvYWRGdW5jdGlvbjtcclxuICAgICAgdGhpcy5vcHRpb25GaWx0ZXJGaWVsZHMgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEub3B0aW9uRmllbGRzO1xyXG4gICAgICB2YXIgYXBpVXJsID0gT3B0aW9uZGF0YS5hcGlVcmw7XHJcbiAgICAgIHZhciByZXNwb25zZU5hbWUgPSBPcHRpb25kYXRhLnJlc3BvbnNlO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHRoaXMuY29udGVudFNlcnZpY2VcclxuICAgICAgICAuZ2V0QWxsUmVwb25zZSh0aGlzLnF1ZXJ5UGFyYW1zLCBhcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZShyZXMgPT4ge1xyXG4gICAgICAgICAgbGV0IHRlbXBBcnJheSA9IChyZXMucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSkgPyByZXMucmVzcG9uc2VbcmVzcG9uc2VOYW1lXSA6IFtdO1xyXG5cclxuICAgICAgICAgIHNlbGYub3B0aW9uRmlsdGVyZGF0YSA9IHRlbXBBcnJheTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLmVuYWJsZVVpZmlsdGVyKSB7XHJcbiAgICAgIGxldCBmaWx0ZXJLZXkgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudWlGaWx0ZXJLZXk7XHJcbiAgICAgIHRoaXMuZW5hYmxlVUlmaWx0ZXIgPSB0cnVlO1xyXG4gICAgICAvLyB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyUHJlZGljYXRlID0gKGRhdGE6IEVsZW1lbnQsIGZpbHRlcjogc3RyaW5nKSA9PiB7XHJcbiAgICAgIC8vICAgcmV0dXJuIGRhdGFbZmlsdGVyS2V5XSA9PSBmaWx0ZXI7XHJcbiAgICAgIC8vICB9O1xyXG4gICAgfVxyXG4gICAvLyBpZih0aGlzLm9uTG9hZCAmJiB0aGlzLm9uTG9hZC5nZXRGcm9tY3VycmVudElucHV0KXtcclxuICAgIGlmKHRoaXMub25Mb2FkICYmIHRoaXMuY3VycmVudFRhYmxlRGF0YS5jaGVja0Zyb21JbnB1dERhdGEpe1xyXG4gICAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YSA9ICFfLmlzRW1wdHkodGVtcCkgPyBKU09OLnBhcnNlKHRlbXApIDoge307XHJcbiAgICAgIGxldCBkYXRhPSh0aGlzLmN1cnJlbnRUYWJsZURhdGEua2V5VG9zYXZlICYmIHRoaXMuaW5wdXREYXRhW3RoaXMuY3VycmVudFRhYmxlRGF0YS5rZXlUb3NhdmVdKT90aGlzLmlucHV0RGF0YVt0aGlzLmN1cnJlbnRUYWJsZURhdGEua2V5VG9zYXZlXTpbXTtcclxuICAgICAgaWYoZGF0YS5sZW5ndGg+MCl7XHJcbiAgICAgICAgdGhpcy5vbkxvYWQucmVzcG9uc2U9ZGF0YTtcclxuICAgICAgICB0aGlzLm9uTG9hZC50b3RhbD1kYXRhLmxlbmd0aDtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+IHRoaXMub25Mb2FkIFwiLHRoaXMub25Mb2FkKTtcclxuXHJcbiAgICBpZiAodGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQucmVzcG9uc2UpIHtcclxuICAgICAgdGhpcy5nZXRMb2FkRGF0YShcclxuICAgICAgICB0aGlzLmN1cnJlbnRUYWJsZURhdGEsXHJcbiAgICAgICAgdGhpcy5vbkxvYWQucmVzcG9uc2UsXHJcbiAgICAgICAgdGhpcy5vbkxvYWQudG90YWwsXHJcbiAgICAgICAgcmVzcG9uc2VLZXlcclxuICAgICAgKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEpIHRoaXMuZ2V0RGF0YSh0aGlzLmN1cnJlbnRUYWJsZURhdGEpO1xyXG4gICAgfVxyXG4gICAgaWYgKCFlbmFibGVOZXh0KSB7XHJcbiAgICAgIC8vIHRoaXMucGFnaW5hdG9yLmZpcnN0UGFnZSgpO1xyXG4gICAgfVxyXG4gIH1cclxuICBnZXREYXRhKGN1cnJlbnRUYWJsZVZhbHVlKSB7XHJcbiAgICBsZXQgdGVtcCA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgdGhpcy5pbnB1dERhdGEgPSAhXy5pc0VtcHR5KHRlbXApID8gSlNPTi5wYXJzZSh0ZW1wKSA6IHt9O1xyXG4gICAgdGhpcy5jb2x1bW5zID0gY3VycmVudFRhYmxlVmFsdWUudGFibGVIZWFkZXI7XHJcbiAgICBpZiAoY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24pIHtcclxuICAgICAgdmFyIGFwaVVybCA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmFwaVVybDtcclxuICAgICAgdmFyIHJlc3BvbnNlTmFtZSA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLnJlc3BvbnNlO1xyXG4gICAgICBsZXQga2V5VG9TZXQgPSBjdXJyZW50VGFibGVWYWx1ZS5vbkxvYWRGdW5jdGlvbi5rZXlGb3JTdHJpbmdSZXNwb25zZTtcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXTtcclxuICAgICAgdmFyIGluY2x1ZGVSZXF1ZXN0ID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uaW5jbHVkZXJlcXVlc3REYXRhID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAvL2lmICggY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uZW5hYmxlTG9hZGVyKSB7XHJcbiAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdGFydExvYWRlcigpO1xyXG4gICAgICAvL31cclxuICAgICAgbGV0IGR5bmFtaWNMb2FkID0gY3VycmVudFRhYmxlVmFsdWUub25Mb2FkRnVuY3Rpb24uZHluYW1pY1JlcG9ydExvYWQ7ICAvLyBjb25kaXRpb24gY2hlY2tlZCB3aGVuIGltcGxlbWVudCBhZGRpdGlvbmFsIHJlcG9ydCB2aWV3XHJcbiAgICAgIGxldCByZXF1ZXN0ZWRRdWVyeSA9IGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLnJlcXVlc3REYXRhO1xyXG4gICAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICAgIHZhciByZXF1ZXN0UXVlcnlQYXJhbSA9IHt9O1xyXG4gICAgICBpZiAoZHluYW1pY0xvYWQpIHtcclxuICAgICAgICBsZXQgdGVtcE9iaiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiQ3VycmVudFJlcG9ydERhdGFcIik7XHJcbiAgICAgICAgbGV0IHJlcG9ydElUZW0gPSBKU09OLnBhcnNlKHRlbXBPYmopO1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0ZWRRdWVyeSwgZnVuY3Rpb24gKHJlcXVlc3RJdGVtKSB7XHJcbiAgICAgICAgICBpZiAocmVxdWVzdEl0ZW0uZnJvbUN1cnJlbnREYXRhKSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RRdWVyeVBhcmFtW3JlcXVlc3RJdGVtLm5hbWVdID0gc2VsZi5jdXJyZW50RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV1cclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJlcXVlc3RRdWVyeVBhcmFtW3JlcXVlc3RJdGVtLm5hbWVdID0gKHJlcG9ydElUZW1bcmVxdWVzdEl0ZW0udmFsdWVdKSA/IHJlcG9ydElUZW1bcmVxdWVzdEl0ZW0udmFsdWVdIDogcmVxdWVzdEl0ZW0udmFsdWU7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3RlZFF1ZXJ5LCBmdW5jdGlvbiAocmVxdWVzdEl0ZW0pIHtcclxuICAgICAgICAgIGlmIChyZXF1ZXN0SXRlbS5mcm9tQ3VycmVudERhdGEpIHtcclxuICAgICAgICAgICAgcmVxdWVzdFF1ZXJ5UGFyYW1bcmVxdWVzdEl0ZW0ubmFtZV0gPSBzZWxmLmN1cnJlbnREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgfSBlbHNlIGlmIChyZXF1ZXN0SXRlbS5kaXJlY3RBc3NpZ24pIHtcclxuICAgICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICAgID8gSlNPTi5zdHJpbmdpZnkocmVxdWVzdEl0ZW0udmFsdWUpXHJcbiAgICAgICAgICAgICAgOiByZXF1ZXN0SXRlbS52YWx1ZTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHNlbGYucXVlcnlQYXJhbXNbcmVxdWVzdEl0ZW0ubmFtZV0gPSByZXF1ZXN0SXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSlcclxuICAgICAgICAgICAgICA6IHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zID0geyAuLi50aGlzLnF1ZXJ5UGFyYW1zLCAuLi5yZXF1ZXN0UXVlcnlQYXJhbSB9O1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UodGhpcy5xdWVyeVBhcmFtcywgYXBpVXJsKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgICAvLyBpZiAoIGN1cnJlbnRUYWJsZVZhbHVlLm9uTG9hZEZ1bmN0aW9uLmVuYWJsZUxvYWRlcikge1xyXG4gICAgICAgICAgdGhpcy5sb2FkZXJTZXJ2aWNlLnN0b3BMb2FkZXIoKTtcclxuICAgICAgICAgIC8vfVxyXG4gICAgICAgICAgdGhpcy5lbmFibGVTZWFyY2ggPSB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZUdsb2JhbFNlYXJjaDtcclxuICAgICAgICAgIHRoaXMuZGlzcGxheWVkQ29sdW1ucyA9IHRoaXMuY29sdW1uc1xyXG4gICAgICAgICAgICAuZmlsdGVyKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdmFsLmlzQWN0aXZlO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAubWFwKGZ1bmN0aW9uICh2YWwpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gdmFsLnZhbHVlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgIGxldCB0ZW1wQXJyYXkgPSBbXTtcclxuICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICAgIGNvbnNvbGUubG9nKHNlbGYsXCI+Pj4+Pj4+Pj4+VEhJU1NTU1wiKTtcclxuICAgICAgICAgIF8uZm9yRWFjaChkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0sIGZ1bmN0aW9uIChpdGVtLCBpbmRleCkge1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGl0ZW0gIT09IFwib2JqZWN0XCIpIHtcclxuICAgICAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgICAgIHRlbXBPYmpba2V5VG9TZXRdID0gaXRlbTtcclxuICAgICAgICAgICAgICBsZXQgZmlsdGVyT2JqID0ge307XHJcbiAgICAgICAgICAgICAgZmlsdGVyT2JqW2N1cnJlbnRUYWJsZVZhbHVlLmZpbHRlcktleV0gPSB0ZW1wT2JqW2N1cnJlbnRUYWJsZVZhbHVlLmRhdGFGaWx0ZXJLZXldO1xyXG4gICAgICAgICAgICAgIGxldCBzZWxlY3RlZFZhbHVlID0gXy5maW5kKHNlbGYuaW5wdXREYXRhW2N1cnJlbnRUYWJsZVZhbHVlLnNlbGVjdGVkVmFsdWVzS2V5XSwgZmlsdGVyT2JqKTtcclxuICAgICAgICAgICAgICBpZihzZWxlY3RlZFZhbHVlKXtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbJ2NoZWNrZWQnXSA9IHRydWU7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIGxldCB0ZW1wT2JqID0ge307XHJcbiAgICAgICAgICAgICAgXy5mb3JFYWNoKGN1cnJlbnRUYWJsZVZhbHVlLmRhdGFWaWV3Rm9ybWF0LCBmdW5jdGlvbiAodmlld0l0ZW0pIHtcclxuICAgICAgICAgICAgICAgIGlmICh2aWV3SXRlbS5zdWJrZXkpIHtcclxuICAgICAgICAgICAgICAgICAgaWYgKHZpZXdJdGVtLmFzc2lnbkZpcnN0SW5kZXh2YWwpIHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzdWx0VmFsdWUgPSAoIV8uaXNFbXB0eShpdGVtW3ZpZXdJdGVtLnZhbHVlXSkgJiYgaXRlbVt2aWV3SXRlbS52YWx1ZV1bMF1bdmlld0l0ZW0uc3Via2V5XSkgPyAoaXRlbVt2aWV3SXRlbS52YWx1ZV1bMF1bdmlld0l0ZW0uc3Via2V5XSkgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICAgICAgPyByZXN1bHRWYWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgICAgOiBcIlwiO1xyXG4gICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICAgICAgPyBpdGVtW3ZpZXdJdGVtLnZhbHVlXVt2aWV3SXRlbS5zdWJrZXldXHJcbiAgICAgICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodmlld0l0ZW0uaXNDb25kaXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGV2YWwodmlld0l0ZW0uY29uZGl0aW9uKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodmlld0l0ZW0uaXNBZGREZWZhdWx0VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IHZpZXdJdGVtLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChjdXJyZW50VGFibGVWYWx1ZS5sb2FkTGFiZWxGcm9tQ29uZmlnKSB7XHJcbiAgICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPVxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRUYWJsZVZhbHVlLmhlYWRlckxpc3RbaXRlbVt2aWV3SXRlbS52YWx1ZV1dO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgIGxldCBmaWx0ZXJPYmogPSB7fTtcclxuICAgICAgICAgICAgICBmaWx0ZXJPYmpbY3VycmVudFRhYmxlVmFsdWUuZmlsdGVyS2V5XSA9IGl0ZW1bY3VycmVudFRhYmxlVmFsdWUuZGF0YUZpbHRlcktleV07XHJcbiAgICAgICAgICAgICAgbGV0IHNlbGVjdGVkVmFsdWUgPSBfLmZpbmQoc2VsZi5pbnB1dERhdGFbY3VycmVudFRhYmxlVmFsdWUuc2VsZWN0ZWRWYWx1ZXNLZXldLCBmaWx0ZXJPYmopO1xyXG4gICAgICAgICAgICAgIGlmKHNlbGVjdGVkVmFsdWUpe1xyXG4gICAgICAgICAgICAgICAgaXRlbS5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgdGVtcE9iaiA9IHsgLi4uaXRlbSwgLi4udGVtcE9iaiB9O1xyXG4gICAgICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICAgIGlmICh0ZW1wQXJyYXkgJiYgdGVtcEFycmF5Lmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhYmxlRGF0YSA9IHRlbXBBcnJheSBhcyBvYmplY3RbXTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFibGVEYXRhID1cclxuICAgICAgICAgICAgICBkYXRhICYmIGRhdGEucmVzcG9uc2UgJiYgZGF0YS5yZXNwb25zZVtyZXNwb25zZU5hbWVdXHJcbiAgICAgICAgICAgICAgICA/IChkYXRhLnJlc3BvbnNlW3Jlc3BvbnNlTmFtZV0gYXMgb2JqZWN0W10pXHJcbiAgICAgICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5yZXNwb25zZSkge1xyXG4gICAgICAgICAgICB0aGlzLmxlbmd0aCA9IGRhdGEucmVzcG9uc2UudG90YWw7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMudGFibGVEYXRhKTtcclxuICAgICAgICAgIC8vIHRoaXMuZGF0YVNvdXJjZS5wYWdpbmF0b3IgPSB0aGlzLnNlbGVjdGVkUGFnaW5hdG9yOyAvLyBuZXdseSBhZGRlZCBcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdEFsbFwiXSA9IGZhbHNlO1xyXG4gICAgICAgICAgdmFyIGZpbHRlcmVkS2V5ID0gKHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoS2V5KSA/IHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoS2V5IDogJyc7XHJcbiAgICAgICAgICBfLmZvckVhY2goc2VsZi5jb2x1bW5zLCBmdW5jdGlvbiAoeCkge1xyXG4gICAgICAgICAgICBpZiAoZmlsdGVyZWRLZXkgPT0geC52YWx1ZSkge1xyXG4gICAgICAgICAgICAgIHNlbGYuaW5wdXREYXRhW3gua2V5VG9TYXZlXSA9IHNlbGYuY3VycmVudEZpbHRlcmVkVmFsdWUuc2VhcmNoVmFsdWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbeC5rZXlUb1NhdmVdID0gbnVsbDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgLy8gdGhpcy5zZWxlY3RBbGxDaGVjayh7IGNoZWNrZWQ6IGZhbHNlIH0pO1xyXG4gICAgICAgICAgLy8gdGhpcy5zZWxlY3RBbGwgPSBmYWxzZTtcclxuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0QWxsLCBcIlNlbGVjdEFsbD4+Pj4+Pj5cIik7XHJcbiAgICAgICAgICAvLyB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkRGF0YVwiXSA9IFtdO1xyXG4gICAgICAgICAgLy8gdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgICAgICAgLy8gbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuc2VsZWN0QWxsLCBcIlNlbGVjdEFsbD4+Pj4+Pj5cIik7XHJcbiAgICAgICAgICAvLyB0aGlzLmNoYW5nZURldGVjdG9yUmVmLmRldGVjdENoYW5nZXMoKTtcclxuXHJcbiAgICAgICAgICB0aGlzLmNoZWNrQ2xpY2tFdmVudE1lc3NhZ2UuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgICAgICAgfSxcclxuICAgICAgICAgIGVyciA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGVyU2VydmljZS5zdG9wTG9hZGVyKCk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiIEVycm9yIFwiLCBlcnIpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICB9XHJcbiAgfVxyXG4gIGdldExvYWREYXRhKGN1cnJlbnRWYWx1ZSwgcmVzcG9uc2VWYWx1ZSwgdG90YWxMZW5ndGgsIHJlc3BvbnNlS2V5KSB7XHJcbiAgICBjb25zb2xlLmxvZyhjdXJyZW50VmFsdWUsIFwiLi4uLmN1cnJlbnRWYWx1ZVwiKTtcclxuICAgIGxldCB0ZW1wQXJyYXkgPSBbXTtcclxuICAgIC8vIGlmIChyZXNwb25zZVZhbHVlICYmIHJlc3BvbnNlVmFsdWVbcmVzcG9uc2VLZXldKSB7XHJcbiAgICByZXNwb25zZVZhbHVlID0gcmVzcG9uc2VLZXkgPyByZXNwb25zZVZhbHVlW3Jlc3BvbnNlS2V5XSA6IHJlc3BvbnNlVmFsdWU7XHJcbiAgICBpZiAocmVzcG9uc2VWYWx1ZSAmJiByZXNwb25zZVZhbHVlLmxlbmd0aCkge1xyXG4gICAgICBfLmZvckVhY2gocmVzcG9uc2VWYWx1ZSwgZnVuY3Rpb24gKGl0ZW0sIGluZGV4KSB7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBpdGVtICE9PSBcIm9iamVjdFwiKSB7XHJcbiAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgdGVtcE9ialtjdXJyZW50VmFsdWUua2V5VG9TZXRdID0gaXRlbTtcclxuICAgICAgICAgIHRlbXBBcnJheS5wdXNoKHRlbXBPYmopO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBsZXQgdGVtcE9iaiA9IHt9O1xyXG4gICAgICAgICAgXy5mb3JFYWNoKGN1cnJlbnRWYWx1ZS5kYXRhVmlld0Zvcm1hdCwgZnVuY3Rpb24gKHZpZXdJdGVtKSB7XHJcbiAgICAgICAgICAgIGlmICh2aWV3SXRlbS5zdWJrZXkpIHtcclxuICAgICAgICAgICAgICBpZiAodmlld0l0ZW0uYXNzaWduRmlyc3RJbmRleHZhbCkge1xyXG4gICAgICAgICAgICAgICAgbGV0IHJlc3VsdFZhbHVlID0gKCFfLmlzRW1wdHkoaXRlbVt2aWV3SXRlbS52YWx1ZV0pICYmIGl0ZW1bdmlld0l0ZW0udmFsdWVdWzBdW3ZpZXdJdGVtLnN1YmtleV0pID8gKGl0ZW1bdmlld0l0ZW0udmFsdWVdWzBdW3ZpZXdJdGVtLnN1YmtleV0pIDogXCJcIjtcclxuICAgICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXVxyXG4gICAgICAgICAgICAgICAgICA/IHJlc3VsdFZhbHVlXHJcbiAgICAgICAgICAgICAgICAgIDogXCJcIjtcclxuICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGVtcE9ialt2aWV3SXRlbS5uYW1lXSA9IGl0ZW1bdmlld0l0ZW0udmFsdWVdXHJcbiAgICAgICAgICAgICAgICAgID8gaXRlbVt2aWV3SXRlbS52YWx1ZV1bdmlld0l0ZW0uc3Via2V5XVxyXG4gICAgICAgICAgICAgICAgICA6IFwiXCI7XHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIHRlbXBPYmpbdmlld0l0ZW0ubmFtZV0gPSBpdGVtW3ZpZXdJdGVtLnZhbHVlXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgICB0ZW1wT2JqID0geyAuLi5pdGVtLCAuLi50ZW1wT2JqIH07XHJcbiAgICAgICAgICB0ZW1wQXJyYXkucHVzaCh0ZW1wT2JqKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgdGhpcy5jb2x1bW5zID0gY3VycmVudFZhbHVlLnRhYmxlSGVhZGVyO1xyXG4gICAgcmVzcG9uc2VWYWx1ZSA9IHRlbXBBcnJheS5sZW5ndGggPyB0ZW1wQXJyYXkgOiByZXNwb25zZVZhbHVlO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiLi4uSU5QVVREQVRBXCIpO1xyXG4gICAgaWYgKGN1cnJlbnRWYWx1ZSAmJiBjdXJyZW50VmFsdWUubWFwRGF0YUZ1bmN0aW9uKSB7XHJcbiAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgXy5mb3JFYWNoKGN1cnJlbnRWYWx1ZS5tYXBEYXRhRnVuY3Rpb24sIGZ1bmN0aW9uIChtYXBJdGVtKSB7XHJcbiAgICAgICAgXy5mb3JFYWNoKG1hcEl0ZW0ucmVxdWVzdERhdGEsIGZ1bmN0aW9uIChyZXF1ZXN0SXRlbSkge1xyXG4gICAgICAgICAgc2VsZi5xdWVyeVBhcmFtc1tyZXF1ZXN0SXRlbS5uYW1lXSA9IHJlcXVlc3RJdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAgICAgICA/IEpTT04uc3RyaW5naWZ5KHNlbGYuaW5wdXREYXRhW3JlcXVlc3RJdGVtLnZhbHVlXSlcclxuICAgICAgICAgICAgOiBzZWxmLmlucHV0RGF0YVtyZXF1ZXN0SXRlbS52YWx1ZV07XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgc2VsZi5jb250ZW50U2VydmljZVxyXG4gICAgICAgICAgLmdldEFsbFJlcG9uc2Uoc2VsZi5xdWVyeVBhcmFtcywgbWFwSXRlbS5hcGlVcmwpXHJcbiAgICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhtYXBJdGVtLCBcIj4+Pj4+Pj4gTUFQIElURU1cIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKGRhdGEsIFwiLi4uLi5EQVRBQUFBXCIpO1xyXG4gICAgICAgICAgICBsZXQgdGVtcE9iaiA9IF8ua2V5QnkoZGF0YS5yZXNwb25zZVttYXBJdGVtLnJlc3BvbnNlXSwgXCJfaWRcIik7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHRlbXBPYmosIFwiLi4uLnRlbXBPYmpcIik7XHJcbiAgICAgICAgICAgIF8uZm9yRWFjaChyZXNwb25zZVZhbHVlLCBmdW5jdGlvbiAocmVzcG9uc2VJdGVtKSB7XHJcbiAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgZGF0YS5yZXNwb25zZSAmJlxyXG4gICAgICAgICAgICAgICAgZGF0YS5yZXNwb25zZS5rZXlGb3JDaGVjayA9PSBtYXBJdGVtLmtleUZvckNoZWNrICYmXHJcbiAgICAgICAgICAgICAgICByZXNwb25zZUl0ZW1bbWFwSXRlbS5rZXlGb3JDaGVja11cclxuICAgICAgICAgICAgICApIHtcclxuICAgICAgICAgICAgICAgIHJlc3BvbnNlSXRlbVttYXBJdGVtLnNob3dLZXldID1cclxuICAgICAgICAgICAgICAgICAgdGVtcE9iaiAmJiByZXNwb25zZUl0ZW1bbWFwSXRlbS5rZXlGb3JDaGVja11cclxuICAgICAgICAgICAgICAgICAgICA/IHRlbXBPYmpbcmVzcG9uc2VJdGVtW21hcEl0ZW0ua2V5Rm9yQ2hlY2tdXS5vYmplY3RUeXBlc1xyXG4gICAgICAgICAgICAgICAgICAgIDogW107XHJcbiAgICAgICAgICAgICAgICByZXNwb25zZUl0ZW1bbWFwSXRlbS5tb2RlbEtleV0gPSByZXNwb25zZUl0ZW1bbWFwSXRlbS5zaG93S2V5XTtcclxuICAgICAgICAgICAgICB9IGVsc2VcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlSXRlbSwgXCIuLi4uLnJlc3BvbnNlSXRlbVwiKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlVmFsdWUsIFwiPj4+Pj5yZXNwb25zZVZhbHVlXCIpO1xyXG4gICAgICAgICAgfSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgdGhpcy5kYXRhU291cmNlID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShyZXNwb25zZVZhbHVlKTtcclxuICAgICAgdGhpcy5sZW5ndGggPSB0b3RhbExlbmd0aCA/IHRvdGFsTGVuZ3RoIDogcmVzcG9uc2VWYWx1ZS5sZW5ndGg7XHJcbiAgICB9XHJcbiAgICB0aGlzLnRhYmxlRGF0YSA9IHRlbXBBcnJheSBhcyBvYmplY3RbXTtcclxuICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZSk7XHJcbiAgICB0aGlzLmxlbmd0aCA9IHRvdGFsTGVuZ3RoID8gdG90YWxMZW5ndGggOiByZXNwb25zZVZhbHVlLmxlbmd0aDtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuY29sdW1ucywgXCI+Pj4+PkNPTFVNTlNTXCIpO1xyXG4gICAgLy8gdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gXy5tYXAodGhpcy5jb2x1bW5zLCBcInZhbHVlXCIpO1xyXG4gICAgdGhpcy5kaXNwbGF5ZWRDb2x1bW5zID0gdGhpcy5jb2x1bW5zXHJcbiAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKHZhbCkge1xyXG4gICAgICAgIHJldHVybiB2YWwuaXNBY3RpdmU7XHJcbiAgICAgIH0pXHJcbiAgICAgIC5tYXAoZnVuY3Rpb24gKHZhbCkge1xyXG4gICAgICAgIHJldHVybiB2YWwudmFsdWU7XHJcbiAgICAgIH0pO1xyXG4gICAgdGhpcy5kYXRhU291cmNlLnBhZ2luYXRvciA9IHRoaXMuc2VsZWN0ZWRQYWdpbmF0b3I7XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLnNlbGVjdGVkUGFnaW5hdG9yLCBcIi4uLi4uLi4uLi4uLi4uLi4uLi4uIHNlbGVjdGVkUGFnaW5hdG9yXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5kYXRhU291cmNlLCBcIi4uLi4uLi4uLkRBVEFTT1VSQ0VFRUVcIik7XHJcbiAgICB0aGlzLmRpc2FibGVQYWdpbmF0b3I9Y3VycmVudFZhbHVlLmRpc2FibGVQYWdpbmF0aW9uP2N1cnJlbnRWYWx1ZS5kaXNhYmxlUGFnaW5hdGlvbjpmYWxzZTtcclxuICAgIC8vcGFnaW5hdG9yRW5hYmxlXHJcbiAgICAvLyB0aGlzLmRhdGFTb3VyY2UgPSByZXNwb25zZUtleVxyXG4gICAgLy8gICA/IG5ldyBNYXRUYWJsZURhdGFTb3VyY2UocmVzcG9uc2VWYWx1ZVtyZXNwb25zZUtleV0pXHJcbiAgICAvLyAgIDogbmV3IE1hdFRhYmxlRGF0YVNvdXJjZShyZXNwb25zZVZhbHVlKTtcclxuICB9XHJcbiAgdXBkYXRlQ2hlY2tDbGljayhzZWxlY3RlZCwgZXZlbnQpIHtcclxuICAgIC8vIHNlbGVjdGVkLnR5cGUgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEudGFibGVUeXBlXHJcbiAgICAvLyAgID8gdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlVHlwZVxyXG4gICAgLy8gICA6IFwiXCI7XHJcbiAgICBpZih0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnRhYmxlVHlwZSlcclxuICAgIHtcclxuICAgICAgc2VsZWN0ZWQudHlwZSA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZVR5cGU7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gc2VsZWN0ZWQudHlwZUlkID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnR5cGVJZFxyXG4gICAgLy8gICA/IHRoaXMuY3VycmVudFRhYmxlRGF0YS50eXBlSWRcclxuICAgIC8vICAgOiBcIlwiO1xyXG4gICAgICBsZXQgc2VjdXJpdHlUeXBlTGVuZ3RoID0gdGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5VHlwZUxlbmd0aD9OdW1iZXIodGhpcy5jdXJyZW50VGFibGVEYXRhLnNlY3VyaXR5VHlwZUxlbmd0aCk6MztcclxuICAgICAgY29uc29sZS5sb2coXCJ0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlUeXBlTGVuZ3RoPz8/P1wiLHRoaXMuY3VycmVudFRhYmxlRGF0YS5zZWN1cml0eVR5cGVMZW5ndGgpXHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj4+IHNlY3VyaXR5VHlwZUxlbmd0aHNzcyBcIixzZWN1cml0eVR5cGVMZW5ndGgpO1xyXG4gICAgLy8gaWYgKHNlbGVjdGVkICYmIHNlbGVjdGVkLnR5cGVJZCkge1xyXG4gICAgLy8gICBpZiAoc2VsZWN0ZWQudHlwZUlkID09IFwiMlwiKSB7XHJcbiAgICAvLyAgICAgc2VsZWN0ZWRbXCJzZWN1cml0eV90eXBlXCJdID0gWzRdO1xyXG4gICAgLy8gICAgIHNlbGVjdGVkW1wibGV2ZWxcIl0gPSBbNF07XHJcbiAgICAvLyAgIH0gZWxzZSB7XHJcbiAgICAvLyAgICAgc2VsZWN0ZWRbXCJzZWN1cml0eV90eXBlXCJdID0gWzEsIDIsIDNdO1xyXG4gICAgLy8gICAgIHNlbGVjdGVkW1wibGV2ZWxcIl0gPSBbMSwgMiwgM107XHJcbiAgICAvLyAgICB9XHJcbiAgICAgICAgIGxldCB0ZW1wQXJyYXk9W107XHJcbiAgICAgICAgIGxldCBwcmVkZWZpbmVkQXJyYXkgPSB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlBcnJheVZhbHVlP0pTT04ucGFyc2UoXCJbXCIgKyB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2VjdXJpdHlBcnJheVZhbHVlICsgXCJdXCIpOnRlbXBBcnJheTtcclxuICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4gcHJlZGVmaW5lZEFycmF5IFwiLHByZWRlZmluZWRBcnJheSk7XHJcbiAgICAgICAgIGlmKHNlY3VyaXR5VHlwZUxlbmd0aCAmJiBzZWN1cml0eVR5cGVMZW5ndGg+MClcclxuICAgICAgICAge1xyXG4gICAgICAgICAgIGNvbnNvbGUubG9nKFwiaW5zaWRlXCIpXHJcbiAgICAgICAgICAgIGlmKHByZWRlZmluZWRBcnJheSAmJiBwcmVkZWZpbmVkQXJyYXkubGVuZ3RoKVxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgdGVtcEFycmF5PXByZWRlZmluZWRBcnJheTtcclxuICAgICAgICAgICAgfWVsc2VcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIGZvcihsZXQgaT0xO2k8PXNlY3VyaXR5VHlwZUxlbmd0aDtpKyspXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICB0ZW1wQXJyYXkucHVzaChpKTtcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9dGVtcEFycmF5O1xyXG4gICAgICAgICB9ZWxzZSB7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIm91dHNpZGVcIilcclxuICAgICAgICAgICAgICBpZihzZWN1cml0eVR5cGVMZW5ndGg9PTApXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXT0gdGVtcEFycmF5O1xyXG4gICAgICAgICAgICAgIH1lbHNlXHJcbiAgICAgICAgICAgICAgeyAgIFxyXG4gICAgICAgICAgICAgIHNlbGVjdGVkW1wic2VjdXJpdHlfdHlwZVwiXSA9IFsxLCAyLCAzXTsgICBcclxuICAgICAgICAgICAgICB9XHJcbiAgICAgICAgIH1cclxuICAgIC8vfVxyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gIHNlbGVjdGVkIHNlY3VyaXR5X3R5cGUgXCIsIHNlbGVjdGVkKTtcclxuICAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLmRhdGFGb3JtYXRUb1NhdmUpIHtcclxuICAgICAgXy5mb3JFYWNoKHRoaXMuY3VycmVudFRhYmxlRGF0YS5kYXRhRm9ybWF0VG9TYXZlLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHNlbGVjdGVkW2l0ZW0ubmFtZV0gPSBzZWxlY3RlZFtpdGVtLnZhbHVlXTtcclxuICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wKSA/IEpTT04ucGFyc2UodGVtcCkgOiB7fTtcclxuICAgIGxldCBzYXZlZFNlbGVjdGVkRGF0YSA9XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YSAmJiB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGEubGVuZ3RoXHJcbiAgICAgICAgPyB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFcclxuICAgICAgICA6IFtdO1xyXG4gICAgdGhpcy5zZWxlY3RlZERhdGEgPSBfLm1lcmdlKHNhdmVkU2VsZWN0ZWREYXRhLCB0aGlzLnNlbGVjdGVkRGF0YSk7XHJcbiAgICBpZiAoZXZlbnQuY2hlY2tlZCkge1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0YS5wdXNoKHNlbGVjdGVkKTtcclxuICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAvLyBsZXQgaW5kZXggPSB0aGlzLnNlbGVjdGVkRGF0YS5maW5kSW5kZXgoXHJcbiAgICAgIC8vICAgb2JqID0+IG9iai5vYmplY3RfbmFtZSA9PT0gc2VsZWN0ZWQub2JqZWN0X25hbWVcclxuICAgICAgLy8gKTtcclxuICAgICAgbGV0IG9iamVjdEtleT10aGlzLmN1cnJlbnRUYWJsZURhdGEub2JqZWN0S2V5VG9DaGVjaz90aGlzLmN1cnJlbnRUYWJsZURhdGEub2JqZWN0S2V5VG9DaGVjazpcIm9ial9uYW1lXCI7XHJcbiAgICAgIGxldCBpbmRleCA9IHRoaXMuc2VsZWN0ZWREYXRhLmZpbmRJbmRleChcclxuICAgICAgICAgb2JqID0+IG9ialtvYmplY3RLZXldID09PSBzZWxlY3RlZFtvYmplY3RLZXldXHJcbiAgICAgICApO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkRGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgfVxyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+IHRoaXMuc2VsZWN0ZWREYXRhIFwiLHRoaXMuc2VsZWN0ZWREYXRhKTtcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gdGhpcy5zZWxlY3RlZERhdGE7XHJcbiAgIGlmICh0aGlzLmN1cnJlbnRUYWJsZURhdGEgJiYgdGhpcy5jdXJyZW50VGFibGVEYXRhLnNhdmVJbkFycmF5KSB7XHJcbiAgICAgIGxldCBhcnJheU9iaiA9IHRoaXMuY3VycmVudFRhYmxlRGF0YS5zYXZlSW5BcnJheTtcclxuICAgICAgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YS50YWJsZUlkID09PSBhcnJheU9iai5rZXkpIHtcclxuICAgICAgICBsZXQgdGVtcEFycmF5ID1cclxuICAgICAgICAgIHRoaXMuc2VsZWN0ZWREYXRhIHx8IHRoaXMuc2VsZWN0ZWREYXRhLmxlbmd0aFxyXG4gICAgICAgICAgICA/IF8ubWFwKHRoaXMuc2VsZWN0ZWREYXRhLCBhcnJheU9iai52YWx1ZVRvTWFwKVxyXG4gICAgICAgICAgICA6IFtdO1xyXG4gICAgICAgIHRlbXBBcnJheSA9IHRlbXBBcnJheS5maWx0ZXIoZnVuY3Rpb24gKGVsZW1lbnQpIHtcclxuICAgICAgICAgIHJldHVybiBlbGVtZW50ICE9IG51bGw7XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbYXJyYXlPYmoua2V5XSA9IHRlbXBBcnJheTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgICAgICAvLyBjaGVjayBib3ggcmVtYWluIHNlYXJjaCBjb2x1bW4gdmFsdWUgc3RhcnRcclxuICAgICAgIHZhciBmaWx0ZXJlZEtleSA9ICh0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSkgPyB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaEtleSA6ICcnO1xyXG4gICAgICAgIGlmKGZpbHRlcmVkS2V5KXtcclxuICAgICAgICB2YXIgc2VsZj10aGlzO1xyXG4gICAgICAgIF8uZm9yRWFjaChzZWxmLmNvbHVtbnMsIGZ1bmN0aW9uICh4KSB7XHJcbiAgICAgICAgICBpZiAoZmlsdGVyZWRLZXkgPT0geC52YWx1ZSkge1xyXG4gICAgICAgICAgICBzZWxmLmlucHV0RGF0YVt4LmtleVRvU2F2ZV0gPSBzZWxmLmN1cnJlbnRGaWx0ZXJlZFZhbHVlLnNlYXJjaFZhbHVlO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgc2VsZi5pbnB1dERhdGFbeC5rZXlUb1NhdmVdID0gbnVsbDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICAgICAvLyBjaGVjayBib3ggcmVtYWluIHNlYXJjaCBjb2x1bW4gdmFsdWUgc3RhcnRcclxuICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLnNlbGVjdGVkRGF0YSwgXCJfaWRcIik7XHJcbiAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgdGhpcy5jaGVja0NsaWNrRXZlbnRNZXNzYWdlLmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gIH1cclxuXHJcbiAgc2VsZWN0QWxsQ2hlY2soZXZlbnQpIHtcclxuICAgIGlmIChldmVudC5jaGVja2VkID09PSB0cnVlKSB7XHJcbiAgICAgIHRoaXMuZGF0YVNvdXJjZS5kYXRhLm1hcChvYmogPT4ge1xyXG4gICAgICAgIG9iai5jaGVja2VkID0gdHJ1ZTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWREYXRhXCJdID0gdGhpcy5kYXRhU291cmNlLmRhdGE7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW1wic2VsZWN0ZWRJZExpc3RcIl0gPSBfLm1hcCh0aGlzLmRhdGFTb3VyY2UuZGF0YSwgXCJfaWRcIik7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UuZGF0YS5tYXAob2JqID0+IHtcclxuICAgICAgICBvYmouY2hlY2tlZCA9IGZhbHNlO1xyXG4gICAgICB9KTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBbXTtcclxuICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IFtdO1xyXG4gICAgfVxyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIHRoaXMuY2hlY2tDbGlja0V2ZW50TWVzc2FnZS5lbWl0KFwiY2xpY2tlZFwiKTtcclxuICB9XHJcbiAgYWRkRmllbGRDbGljayhpdGVtLCBvYmplY3RMaXN0KSB7XHJcbiAgICBjb25zb2xlLmxvZyhpdGVtLCBcIi4uLi4uLi4uLi5JVEVNXCIpO1xyXG4gICAgY29uc29sZS5sb2codGhpcy5pbnB1dERhdGEsIFwiLi4uLi4uLklOUFVUIERBdGFcIilcclxuICAgIGNvbnNvbGUubG9nKG9iamVjdExpc3QsIFwiLi4uLi4uT2JqZWN0TGlzdFwiKTtcclxuICB9XHJcblxyXG4gIHNlbGVjdEFsbERhdGEoKSB7XHJcbiAgICBsZXQgcmVxdWVzdERldGFpbHMgPSB0aGlzLmN1cnJlbnREYXRhLnNlbGVjdEFsbFJlcXVlc3Q7XHJcbiAgICBsZXQgcXVlcnkgPSB7fTtcclxuICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgcXVlcnlbaXRlbS5uYW1lXSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0udmFsdWVdO1xyXG4gICAgfSk7XHJcbiAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgIC5nZXRBbGxSZXBvbnNlKHF1ZXJ5LCByZXF1ZXN0RGV0YWlscy5hcGlVcmwpXHJcbiAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB7XHJcbiAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9XHJcbiAgICAgICAgICBkYXRhLnJlc3BvbnNlW3JlcXVlc3REZXRhaWxzLnJlc3BvbnNlTmFtZV07XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG5cclxuICBjbGVhclNlbGVjdEFsbERhdGEgKCkge1xyXG4gICAgdGhpcy5uZ09uSW5pdCgpO1xyXG4gIH1cclxuICBhcHBseUZpbHRlcihldmVudCwga2V5KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4gZXZlbnQgXCIsIGV2ZW50LCBcIiBrZXkgXCIsIGtleSk7XHJcbiAgICB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlID0ge1xyXG4gICAgICBzZWFyY2hLZXk6IGtleSxcclxuICAgICAgc2VhcmNoVmFsdWU6IGV2ZW50XHJcbiAgICB9O1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4gdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSAgXCIsdGhpcy5jdXJyZW50RmlsdGVyZWRWYWx1ZSApO1xyXG5cclxuICAgIHRoaXMuaW5wdXREYXRhW2tleV09ZXZlbnQ7IC8vIHNlYXJjaGVkIHZhbHVlIHNldFxyXG4gICAgbGV0IHRhYmxlQXJyYXkgPVxyXG4gICAgICB0aGlzLm9uTG9hZCAmJiB0aGlzLm9uTG9hZC50YWJsZURhdGFcclxuICAgICAgICA/IHRoaXMub25Mb2FkLnRhYmxlRGF0YVxyXG4gICAgICAgIDogdGhpcy5jdXJyZW50Q29uZmlnRGF0YVtcImxpc3RWaWV3XCJdLnRhYmxlRGF0YTtcclxuICAgIGxldCBpbmRleCA9IF8uZmluZEluZGV4KHRhYmxlQXJyYXksIHsgdGFibGVJZDogdGhpcy50YWJsZUlkIH0pO1xyXG4gICAgbGV0IHNlYXJjaFJlcXVlc3QgPSB0YWJsZUFycmF5W2luZGV4XS5vblRhYmxlU2VhcmNoO1xyXG4gICAgbGV0IHJlcXVlc3QgPSBzZWFyY2hSZXF1ZXN0LnJlcXVlc3REYXRhO1xyXG4gICAgbGV0IGlzVWlzZXJhY2ggPSAoc2VhcmNoUmVxdWVzdC51aVNlYXJjaCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICBpZiAoaXNVaXNlcmFjaCkge1xyXG4gICAgICB0aGlzLmRhdGFTb3VyY2UuZmlsdGVyID0gZXZlbnQ7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmN1cnJlbnRUYWJsZURhdGEgPSB0YWJsZUFycmF5W2luZGV4XTtcclxuICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgIG9mZnNldDogMCxcclxuICAgICAgICBsaW1pdDogMTAsXHJcbiAgICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZVxyXG4gICAgICB9O1xyXG5cclxuICAgICAgaWYgKHNlYXJjaFJlcXVlc3QgJiYgc2VhcmNoUmVxdWVzdC5pc1NpbmdsZXJlcXVlc3QpIHtcclxuICAgICAgICBxdWVyeVtzZWFyY2hSZXF1ZXN0LnNpbmdsZVJlcXVlc3RLZXldID0gZXZlbnQ7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgICAgIF8uZm9yRWFjaChyZXF1ZXN0LCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgICAgaWYgKHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZV0pXHJcbiAgICAgICAgICAgIHF1ZXJ5W2l0ZW0udmFsdWVdID0gc2VsZi5pbnB1dERhdGFbaXRlbS5uYW1lXTsgLy8gb25UYWJsZSBTZWFyY2ggVmFsdWUgcGFzc2VkIGluc3RlYWQgb2YgbmFtZSBcclxuICAgICAgICB9KTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zID0gcXVlcnk7XHJcbiAgICAgIHRoaXMuZ2V0RGF0YSh0aGlzLmN1cnJlbnRUYWJsZURhdGEpO1xyXG4gICAgfVxyXG5cclxuICB9XHJcblxyXG4gIGFwcGx5RmlsdGVyU2VhcmNoKGV2ZW50SXRlbSwga2V5KSB7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+PiBhcHBseUZpbHRlclNlYXJjaFwiKTtcclxuICAgIGV2ZW50SXRlbS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgbGV0IGV2ZW50ID0gZXZlbnRJdGVtLnRhcmdldC52YWx1ZTtcclxuICAgIHRoaXMuY3VycmVudEZpbHRlcmVkVmFsdWUgPSB7fTtcclxuICAgIGxldCB0ZW1wID0gbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4gZXZlbnQgXCIsIGV2ZW50LCBcIiBrZXkgXCIsIGtleSk7XHJcbiAgICB0aGlzLmN1cnJlbnRGaWx0ZXJlZFZhbHVlID0ge1xyXG4gICAgICAgIHNlYXJjaEtleToga2V5LFxyXG4gICAgICAgIHNlYXJjaFZhbHVlOiBldmVudFxyXG4gICAgfTtcclxuICAgIHRoaXMuaW5wdXREYXRhW2tleV0gPSBldmVudDsgLy8gc2VhcmNoZWQgdmFsdWUgc2V0XHJcbiAgICBsZXQgdGFibGVBcnJheSA9XHJcbiAgICAgICAgdGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQudGFibGVEYXRhXHJcbiAgICAgICAgICAgID8gdGhpcy5vbkxvYWQudGFibGVEYXRhXHJcbiAgICAgICAgICAgIDogdGhpcy5jdXJyZW50Q29uZmlnRGF0YVtcImxpc3RWaWV3XCJdLnRhYmxlRGF0YTtcclxuICAgIGxldCBpbmRleCA9IF8uZmluZEluZGV4KHRhYmxlQXJyYXksIHsgdGFibGVJZDogdGhpcy50YWJsZUlkIH0pO1xyXG4gICAgbGV0IHNlYXJjaFJlcXVlc3QgPSB0YWJsZUFycmF5W2luZGV4XS5vblRhYmxlU2VhcmNoO1xyXG4gICAgbGV0IHJlcXVlc3QgPSBzZWFyY2hSZXF1ZXN0LnJlcXVlc3REYXRhO1xyXG4gICAgbGV0IGlzVWlzZXJhY2ggPSAoc2VhcmNoUmVxdWVzdC51aVNlYXJjaCkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICBpZiAoaXNVaXNlcmFjaCkge1xyXG4gICAgICAgIHRoaXMuZGF0YVNvdXJjZS5maWx0ZXIgPSBldmVudDtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50VGFibGVEYXRhID0gdGFibGVBcnJheVtpbmRleF07XHJcbiAgICAgICAgbGV0IHF1ZXJ5ID0ge1xyXG4gICAgICAgICAgICBvZmZzZXQ6IDAsXHJcbiAgICAgICAgICAgIGxpbWl0OiAxMCxcclxuICAgICAgICAgICAgZGF0YXNvdXJjZTogdGhpcy5kZWZhdWx0RGF0YXNvdXJjZVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc29sZS5sb2coXCI+Pj4+IHNlYXJjaFJlcXVlc3QgXCIsIHNlYXJjaFJlcXVlc3QpO1xyXG4gICAgICAgIGlmIChzZWFyY2hSZXF1ZXN0ICYmIHNlYXJjaFJlcXVlc3QuaXNTaW5nbGVyZXF1ZXN0KSB7XHJcbiAgICAgICAgICAgIHF1ZXJ5W3NlYXJjaFJlcXVlc3Quc2luZ2xlUmVxdWVzdEtleV0gPSBldmVudDtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgICAgICAgXy5mb3JFYWNoKHJlcXVlc3QsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoc2VsZi5pbnB1dERhdGFbaXRlbS5uYW1lXSlcclxuICAgICAgICAgICAgICAgICAgICBxdWVyeVtpdGVtLnZhbHVlXSA9IHNlbGYuaW5wdXREYXRhW2l0ZW0ubmFtZV07IC8vIG9uVGFibGUgU2VhcmNoIFZhbHVlIHBhc3NlZCBpbnN0ZWFkIG9mIG5hbWUgXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnF1ZXJ5UGFyYW1zID0gcXVlcnk7XHJcbiAgICAgICAgdGhpcy5nZXREYXRhKHRoaXMuY3VycmVudFRhYmxlRGF0YSk7XHJcbiAgICB9XHJcbn1cclxuICBmaWx0ZXJSZXNldChmaWVsZCkge1xyXG4gICAgZGVsZXRlIHRoaXMucXVlcnlQYXJhbXNbZmllbGQucmVxdWVzdEtleV07XHJcbiAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgfVxyXG4gIG9uTXVsdGlTZWxlY3QoZXZlbnQsIHJvd1ZhbCkge1xyXG4gICAgbGV0IHRlbXAgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMuaW5wdXREYXRhID0gIV8uaXNFbXB0eSh0ZW1wKSA/IEpTT04ucGFyc2UodGVtcCkgOiB7fTtcclxuICAgIGxldCBzZWxlY3RlZERhdGEgPSB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGE7XHJcbiAgICBsZXQgaW5kZXggPSBfLmZpbmRJbmRleChzZWxlY3RlZERhdGEsIHsgb2JqX25hbWU6IHJvd1ZhbC5vYmpfbmFtZSB9KTtcclxuICAgIGlmIChpbmRleCA+IC0xICYmIHNlbGVjdGVkRGF0YSAmJiBzZWxlY3RlZERhdGEubGVuZ3RoKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YVtpbmRleF0uc2VjdXJpdHlfdHlwZSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgICB0aGlzLmlucHV0RGF0YS5zZWxlY3RlZERhdGFbaW5kZXhdLmxldmVsID0gZXZlbnQudmFsdWU7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YVtpbmRleF0ub2JqZWN0TGV2ZWwgPSBldmVudC52YWx1ZTtcclxuICAgIH1cclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgfVxyXG5cclxuICBjaGFuZ2VQYWdlKGV2ZW50KSB7XHJcbiAgICBpZiAoZXZlbnQucGFnZVNpemUgIT0gdGhpcy5saW1pdCkge1xyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wibGltaXRcIl0gPSBldmVudC5wYWdlU2l6ZTtcclxuICAgICAgdGhpcy5xdWVyeVBhcmFtc1tcIm9mZnNldFwiXSA9IDA7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLnF1ZXJ5UGFyYW1zW1wib2Zmc2V0XCJdID0gZXZlbnQucGFnZUluZGV4ICogdGhpcy5saW1pdDtcclxuICAgICAgdGhpcy5xdWVyeVBhcmFtc1tcImxpbWl0XCJdID0gdGhpcy5saW1pdDtcclxuICAgIH1cclxuICAgIHRoaXMub25Mb2FkRGF0YShcIm5leHRcIik7XHJcbiAgfVxyXG4gIHZpZXdEYXRhKGVsZW1lbnQsIGNvbCkge1xyXG4gICAgY29uc29sZS5sb2coY29sLCBcInZpZXdEYXRhID4+Pj4+Pj4+PiBpc1JlZGlyZWN0IFwiLCBjb2wuaXNSZWRpcmVjdCwgdGhpcy5lbnZpcm9ubWVudCk7XHJcbiAgICBpZiAoY29sLmlzUmVkaXJlY3QpIHtcclxuICAgICAgbGV0IHRlc3RTdHIgPSBcIi9jb250cm9sLW1hbmFnZW1lbnQvYWNjZXNzLWNvbnRyb2xcIlxyXG4gICAgICB3aW5kb3cubG9jYXRpb24uaHJlZiA9IGAke3RoaXMucmVkaXJlY3RVcml9YCArIHRlc3RTdHI7XHJcblxyXG4gICAgICBjb25zb2xlLmxvZyhcIlJlZGlyZWN0ZWQgKioqKioqKioqKioqKioqKioqKipcIilcclxuICAgICAgLy8gdGhpcy5yb3V0ZS5uYXZpZ2F0ZUJ5VXJsKFwiL2NvbnRyb2wtbWFuYWdlbWVudC9hY2Nlc3MtY29udHJvbFwiKTtcclxuXHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmFjdGlvbkNsaWNrKGVsZW1lbnQsIFwidmlld1wiKTtcclxuICAgIH1cclxuICB9XHJcbiAgYWRkQnV0dG9uQ2xpY2soZWxlbWVudCwgYWN0aW9uKSB7XHJcbiAgICBjb25zb2xlLmxvZyhcImFkZEJ1dHRvbkNsaWNrIFwiKTtcclxuICAgIGNvbnNvbGUubG9nKFwiIGVsZW1lbnQgXCIsIGVsZW1lbnQpO1xyXG4gICAgY29uc29sZS5sb2coXCIgYWN0aW9uIFwiLCBhY3Rpb24pO1xyXG4gIH1cclxuICBhY3Rpb25DbGljayhlbGVtZW50LCBhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKGVsZW1lbnQsIFwiPj4+PmVsZW1lbnRcIilcclxuICAgIGlmIChhY3Rpb24gPT0gXCJlZGl0XCIgfHwgYWN0aW9uID09IFwidmlld1wiIHx8IGFjdGlvbiA9PVwiaW5mb1wiKSB7XHJcbiAgICAgIHRoaXMub3BlbkRpYWxvZyhlbGVtZW50LCBhY3Rpb24pO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT0gXCJkZWxldGVcIikge1xyXG4gICAgICBjb25zb2xlLmxvZyh0aGlzLCBcIj4+Pj4+Pj50aGlzXCIpO1xyXG4gICAgICBpZiAodGhpcy5vbkxvYWQgJiYgdGhpcy5vbkxvYWQuaW5wdXRLZXkpIHtcclxuICAgICAgICBsZXQgaW5wdXRLZXkgPSB0aGlzLm9uTG9hZC5pbnB1dEtleTtcclxuICAgICAgICBpZiAodGhpcy5pbnB1dERhdGEgJiYgdGhpcy5pbnB1dERhdGFbaW5wdXRLZXldKSB7XHJcbiAgICAgICAgICBsZXQgc2VsZWN0ZWREYXRhID0gdGhpcy5pbnB1dERhdGFbaW5wdXRLZXldO1xyXG4gICAgICAgICAgLy8gbGV0IGluZGV4ID0gc2VsZWN0ZWREYXRhLmZpbmRJbmRleChcclxuICAgICAgICAgIC8vICAgb2JqID0+IG9iaiA9PSBlbGVtZW50XHJcbiAgICAgICAgICAvLyApO1xyXG4gICAgICAgICAgbGV0IGluZGV4ID0gXy5maW5kSW5kZXgoc2VsZWN0ZWREYXRhLGVsZW1lbnQpO1xyXG4gICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4+IGRlbGV0ZSBpbmRleCBcIixpbmRleCk7XHJcbiAgICAgICAgICBpZihpbmRleD4tMSl7XHJcbiAgICAgICAgICAgIHNlbGVjdGVkRGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbaW5wdXRLZXldID0gc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZElkTGlzdFwiXSA9IF8ubWFwKHNlbGVjdGVkRGF0YSwgXCJfaWRcIik7XHJcbiAgICAgICAgICB0aGlzLmRhdGFTb3VyY2UgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHNlbGVjdGVkRGF0YSk7XHJcbiAgICAgICAgICB0aGlzLmxlbmd0aCA9IHNlbGVjdGVkRGF0YS5sZW5ndGg7XHJcbiAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbShcImN1cnJlbnRJbnB1dFwiLCBKU09OLnN0cmluZ2lmeSh0aGlzLmlucHV0RGF0YSkpO1xyXG4gICAgICAgICAgdGhpcy5hY3Rpb25DbGlja0V2ZW50LmVtaXQoXCJjbGlja2VkXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIHsgICAgXHJcbiAgICAgICAgaWYgKHRoaXMuaW5wdXREYXRhICYmIHRoaXMuaW5wdXREYXRhLnNlbGVjdGVkRGF0YSkge1xyXG4gICAgICAgICAgbGV0IG9iamVjdEtleT10aGlzLmN1cnJlbnRUYWJsZURhdGEub2JqZWN0S2V5VG9DaGVjaz90aGlzLmN1cnJlbnRUYWJsZURhdGEub2JqZWN0S2V5VG9DaGVjazpcIm9ial9uYW1lXCI7XHJcbiAgICAgICAgICBjb25zb2xlLmxvZyhcIj4+Pj4gb2JqZWN0S2V5IFwiLG9iamVjdEtleSk7XHJcbiAgICAgICAgICBsZXQgc2VsZWN0ZWREYXRhID0gdGhpcy5pbnB1dERhdGEuc2VsZWN0ZWREYXRhO1xyXG4gICAgICAgICAgLy8gbGV0IGluZGV4ID0gc2VsZWN0ZWREYXRhLmZpbmRJbmRleChcclxuICAgICAgICAgIC8vICAgb2JqID0+IG9iai5vYmplY3RfbmFtZSA9PSBlbGVtZW50Lm9iamVjdF9uYW1lXHJcbiAgICAgICAgICAvLyApO1xyXG4gICAgICAgICAgbGV0IGluZGV4ID0gc2VsZWN0ZWREYXRhLmZpbmRJbmRleChcclxuICAgICAgICAgICAgb2JqID0+IG9ialtvYmplY3RLZXldID09IGVsZW1lbnRbb2JqZWN0S2V5XVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHNlbGVjdGVkRGF0YS5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgICAgdGhpcy5pbnB1dERhdGFbXCJzZWxlY3RlZERhdGFcIl0gPSBzZWxlY3RlZERhdGE7XHJcbiAgICAgICAgICB0aGlzLmlucHV0RGF0YVtcInNlbGVjdGVkSWRMaXN0XCJdID0gXy5tYXAoc2VsZWN0ZWREYXRhLCBcIl9pZFwiKTtcclxuICAgICAgICAgIHRoaXMuZGF0YVNvdXJjZSA9IG5ldyBNYXRUYWJsZURhdGFTb3VyY2Uoc2VsZWN0ZWREYXRhKTtcclxuICAgICAgICAgIHRoaXMubGVuZ3RoID0gc2VsZWN0ZWREYXRhLmxlbmd0aDtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFwiY3VycmVudElucHV0XCIsIEpTT04uc3RyaW5naWZ5KHRoaXMuaW5wdXREYXRhKSk7XHJcbiAgICAgICAgICB0aGlzLmFjdGlvbkNsaWNrRXZlbnQuZW1pdChcImNsaWNrZWRcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcInJlbW92ZV9yZWRfZXllXCIpIHtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGVsZW1lbnQsIFwidmlld1wiKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwicmVzdG9yZV9wYWdlXCIpIHtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGVsZW1lbnQsIGFjdGlvbik7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSAnYXNrZGVmYXVsdGJ1dHRvbicpIHtcclxuICAgICAgbGV0IGRzaWQgPSBlbGVtZW50Ll9pZDtcclxuICAgICAgbGV0IGRzbmFtZSA9IGVsZW1lbnQubmFtZTtcclxuICAgICAgY29uc29sZS5sb2coXCIgZHNpZCBcIiwgZHNpZCwgXCIgZHNuYW1lIFwiLCBkc25hbWUpO1xyXG4gICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmREYXRhc291cmNlKGRzaWQpO1xyXG4gICAgICB0aGlzLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgXCJEYXRhc291cmNlIERlZmF1bHQgVXBkYXRlZCBTdWNjZXNzZnVsbHkhXCJcclxuICAgICAgICApXHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PSBcInNhdmVfYWx0XCIpIHtcclxuICAgICAgbGV0IHJlcXVlc3REZXRhaWxzID0gdGhpcy5jdXJyZW50RGF0YS5kb3dubG9hZFJlcXVlc3Q7XHJcbiAgICAgIGxldCBxdWVyeSA9IHtcclxuICAgICAgICByZXBvcnRJZDogZWxlbWVudC5faWRcclxuICAgICAgfVxyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEFsbFJlcG9uc2UocXVlcnksIHJlcXVlc3REZXRhaWxzLmFwaVVybClcclxuICAgICAgICAuc3Vic2NyaWJlKGRhdGEgPT4ge1xyXG4gICAgICAgICAgY29uc3QgYWIgPSBuZXcgQXJyYXlCdWZmZXIoZGF0YS5kYXRhLmxlbmd0aCk7XHJcbiAgICAgICAgICBjb25zdCB2aWV3ID0gbmV3IFVpbnQ4QXJyYXkoYWIpO1xyXG4gICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBkYXRhLmRhdGEubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgdmlld1tpXSA9IGRhdGEuZGF0YVtpXTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGxldCBkb3dubG9hZFR5cGUgPSAoZWxlbWVudC5maWxlVHlwZSA9PSBcIkNTVlwiKSA/IFwidGV4dC9jc3ZcIiA6IFwidGV4dC94bHN4XCI7XHJcbiAgICAgICAgICBjb25zdCBmaWxlID0gbmV3IEJsb2IoW2FiXSwgeyB0eXBlOiBkb3dubG9hZFR5cGUgfSk7XHJcbiAgICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGZpbGUsIGVsZW1lbnQuZmlsZU5hbWUgKyBlbGVtZW50LmZpbGVFeHRlbnNpb24pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH1cclxuICBhY3Rpb25SZWRpcmVjdChlbGVtZW50LGNvbHVtbkRhdGEpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiYWN0aW9uUmVkaXJlY3QgPj4+Pj4+IFwiLCBlbGVtZW50KTtcclxuICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YVxyXG4gICAgbGV0IHF1ZXJ5T2JqID0ge307XHJcbiAgICB2YXIgc2VsZiA9IHRoaXM7XHJcbiAgICBjb25zb2xlLmxvZyhcInJlcXVlc3REZXRhaWxzID4+Pj4+PiBcIiwgcmVxdWVzdERldGFpbHMpO1xyXG4gICAgaWYgKGNvbHVtbkRhdGEucnVsZXNldENoZWNrKSB7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gcmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscy5vblRhYmxlVXBkYXRlLnJlcXVlc3REYXRhLCBmdW5jdGlvbiAoaXRlbSkge1xyXG4gICAgICAgIHF1ZXJ5T2JqW2l0ZW0ubmFtZV0gPSBpdGVtLnN1YmtleVxyXG4gICAgICAgICAgPyBlbGVtZW50W2l0ZW0udmFsdWVdW2l0ZW0uc3Via2V5XVxyXG4gICAgICAgICAgOiBlbGVtZW50W2l0ZW0udmFsdWVdO1xyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAudXBkYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMub25UYWJsZVVwZGF0ZS5hcGlVcmwsIGVsZW1lbnQuX2lkKVxyXG4gICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICByZXMgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB0aGlzLm5nT25Jbml0KCk7XHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS53YXJuaW5nKFxyXG4gICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgICAgY29uc29sZS5sb2coJ2FjdGlvblJlZGlyZWN0ID4+Pj4+Pj4+Pj4+Pj4+PiBxdWVyeU9iaiAnLCBxdWVyeU9iailcclxuICAgIH0gZWxzZSB7XHJcblxyXG4gICAgfVxyXG5cclxuICB9XHJcbiAgb25Ub2dnbGVDaGFuZ2UoZXZlbnQsIGVsZW1lbnQpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+Pj4+IGV2ZW50IFwiLCBldmVudCwgXCIgOmVsZW1lbnQgXCIsIGVsZW1lbnQpO1xyXG4gICAgZWxlbWVudC5zdGF0dXMgPSBldmVudC5jaGVja2VkID8gXCJBQ1RJVkVcIiA6IFwiSU5BQ1RJVkVcIjtcclxuICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHRoaXMuY3VycmVudFRhYmxlRGF0YVxyXG4gICAgICA/IHRoaXMuY3VycmVudFRhYmxlRGF0YS5vblRvZ2dsZUNoYW5nZVxyXG4gICAgICA6IFwiXCI7XHJcbiAgICBpZiAocmVxdWVzdERldGFpbHMpIHtcclxuICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSByZXF1ZXN0RGV0YWlscy50b2FzdE1lc3NhZ2U7XHJcbiAgICAgIHZhciBzZWxmID0gdGhpcztcclxuICAgICAgbGV0IHF1ZXJ5T2JqID0ge307XHJcbiAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscy5yZXF1ZXN0RGF0YSwgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBxdWVyeU9ialtpdGVtLm5hbWVdID0gaXRlbS5zdWJrZXlcclxuICAgICAgICAgID8gZWxlbWVudFtpdGVtLnZhbHVlXVtpdGVtLnN1YmtleV1cclxuICAgICAgICAgIDogZWxlbWVudFtpdGVtLnZhbHVlXTtcclxuICAgICAgfSk7XHJcbiAgICAgIHNlbGYuY29udGVudFNlcnZpY2VcclxuICAgICAgICAudXBkYXRlUmVxdWVzdChxdWVyeU9iaiwgcmVxdWVzdERldGFpbHMuYXBpVXJsLCBlbGVtZW50Ll9pZClcclxuICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgcmVzID0+IHtcclxuICAgICAgICAgICAgIGlmKGVsZW1lbnQuc3RhdHVzPT0nQUNUSVZFJyAmJiAgdG9hc3RNZXNzYWdlRGV0YWlscy5lbmFibGUgKVxyXG4gICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5lbmFibGVcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgfWVsc2UgaWYoZWxlbWVudC5zdGF0dXM9PSdJTkFDVElWRScgJiYgIHRvYXN0TWVzc2FnZURldGFpbHMuZGlzYWJsZSlcclxuICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZGlzYWJsZVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICB9ZWxzZVxyXG4gICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgIHNlbGYuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgICAgICApXHJcbiAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgIH1cclxuICAgICAgICAgIFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGVycm9yID0+IHtcclxuICAgICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgICBzZWxmLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLmVycm9yXHJcbiAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICB9IGVsc2UgaWYgKHRoaXMuY3VycmVudFRhYmxlRGF0YSAmJiB0aGlzLmN1cnJlbnRUYWJsZURhdGEuc2F2ZXNlbGVjdGVkVG9nZ2xlKSB7XHJcbiAgICAgIHRoaXMuaW5wdXREYXRhW3RoaXMuY3VycmVudFRhYmxlRGF0YVtcImtleVRvc2F2ZVwiXV0gPSB0aGlzLmRhdGFTb3VyY2UuZGF0YTtcclxuICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJjdXJyZW50SW5wdXRcIiwgSlNPTi5zdHJpbmdpZnkodGhpcy5pbnB1dERhdGEpKTtcclxuICAgIH1cclxuICB9XHJcbiAgb3BlbkRpYWxvZyhlbGVtZW50LCBhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKFwiIE9wZW4gRGlhbG9nICoqKioqKioqKiBlbGVtZW50OiBcIiwgZWxlbWVudCk7XHJcbiAgICBsZXQgbW9kZWxXaWR0aCA9IHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXS5tb2RlbERhdGEuc2l6ZTtcclxuICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nXHJcbiAgICAgIC5vcGVuKE1vZGVsTGF5b3V0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIGFjdGlvbjogYWN0aW9uLFxyXG4gICAgICAgICAgc2F2ZWREYXRhOiBlbGVtZW50XHJcbiAgICAgICAgfVxyXG4gICAgICB9KVxyXG4gICAgICAuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAuc3Vic2NyaWJlKHJlc3BvbnNlID0+IHtcclxuICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgfSk7XHJcbiAgfVxyXG59XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gYmFzZTY0VG9BcnJheUJ1ZmZlcihiYXNlNjQ6IHN0cmluZykge1xyXG4gIGNvbnN0IGJpbmFyeVN0cmluZyA9IHdpbmRvdy5hdG9iKGJhc2U2NCk7IC8vIENvbW1lbnQgdGhpcyBpZiBub3QgdXNpbmcgYmFzZTY0XHJcbiAgY29uc3QgYnl0ZXMgPSBuZXcgVWludDhBcnJheShiaW5hcnlTdHJpbmcubGVuZ3RoKTtcclxuICByZXR1cm4gYnl0ZXMubWFwKChieXRlLCBpKSA9PiBiaW5hcnlTdHJpbmcuY2hhckNvZGVBdChpKSk7XHJcbn1cclxuXHJcbiJdfQ==