import { Component, Inject, Input } from '@angular/core';
import { SessionService } from './session-expiration.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
export class AuthModelDialog {
    constructor(authService, dialog, matDialogRef, data, oauthService) {
        this.authService = authService;
        this.dialog = dialog;
        this.matDialogRef = matDialogRef;
        this.data = data;
        this.oauthService = oauthService;
        this.unsubscribe$ = new Subject();
    }
    ngOnInit() {
        this.minutesDisplay = this.data.minutesDisplay;
        this.secondsDisplay = this.data.secondsDisplay;
        const interval = 1000; //1000
        const duration = 2 * 60;
        this.timerSubscription = timer(0, interval).pipe(take(duration)).subscribe(value => this.render((duration - +value) * interval), err => { }, () => {
            this.authService.logOutUser();
        });
    }
    logout() {
        this.oauthService.logOut(false);
        this.matDialogRef.close();
    }
    onSubmit() {
        this.authService.notifyUserAction();
        this.matDialogRef.close();
        this.oauthService.silentRefresh();
    }
    render(count) {
        this.secondsDisplay = this.getSeconds(count);
        this.minutesDisplay = this.getMinutes(count);
    }
    getSeconds(ticks) {
        const seconds = ((ticks % 60000) / 1000).toFixed(0);
        return this.pad(seconds);
    }
    getMinutes(ticks) {
        const minutes = Math.floor(ticks / 60000);
        return this.pad(minutes);
    }
    pad(digit) {
        return digit <= 9 ? '0' + digit : digit;
    }
}
AuthModelDialog.decorators = [
    { type: Component, args: [{
                selector: 'auth.model',
                template: "<scrumboard-board-card-dialog>\r\n\t<div class=\"dialog-content-wrapper\">\r\n\t\t<div class=\"header-top accent ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n\t\t\tfxlayoutalign=\"space-between center\">\r\n\t\t\t<div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<h2 class=\"m-0 font-weight-900\">Session Timeout</h2>\r\n\t\t\t</div>\r\n\t\t\t<div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n\t\t\t\t<button mat-icon-button (click)=\"matDialogRef.close()\" aria-label=\"Close Dialog\" style=\"float:right\">\r\n\t\t\t\t\t<mat-icon>close</mat-icon>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n        </div>\r\n        <div mat-dialog-content class=\"pb-0 m-0 msg_content\" fusePerfectScrollbar style=\"padding: 20px;\">\r\n            <p>You're being timed out due to inactivity.Please choose to stay signed in or to logout.</p>\r\n            <p>Otherwise,you will logged off automatically in <strong>{{ (minutesDisplay) }}:{{ (secondsDisplay) && (secondsDisplay <=59) ? secondsDisplay : '00' }}</strong></p>\r\n        </div>\r\n        <div class=\"header-top ctrl-create header p-12\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n        fxlayoutalign=\"space-between center\" style=\"border-top: 1px solid grey;\">\r\n        <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n            <h2 class=\"m-0\"></h2>\r\n        </div>\r\n        <div class=\"toolbar\"  fxlayout=\"row\" fxlayoutalign=\"start center\">\r\n            <button mat-raised-button class=\"mr-4\" color=\"warn\" style=\"float:right;border-radius: 25px !important;\" (click)=\"onSubmit()\">Stay Logged In\t\t\t\r\n            </button>\r\n            <button type=\"button\" class=\"mat-raised-button mr-8\" ng-reflect-color=\"accent\" ng-reflect-type=\"button\"\r\n                style=\"background-color: grey !important;color: white !important;border-radius: 25px !important;margin-right: 8px !important;\"\r\n                (click)=\"logout()\">\r\n                <span class=\"mat-button-wrapper\">Log Off</span>\r\n                <div class=\"mat-button-ripple mat-ripple\" matripple=\"\" ng-reflect-centered=\"false\"\r\n                    ng-reflect-disabled=\"false\"></div>\r\n                <div class=\"mat-button-focus-overlay\"></div>\r\n            </button>\r\n        </div>\r\n    </div>\r\n    </div>\r\n</scrumboard-board-card-dialog>        ",
                providers: [SessionService]
            }] }
];
/** @nocollapse */
AuthModelDialog.ctorParameters = () => [
    { type: SessionService },
    { type: MatDialog },
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: OAuthService }
];
AuthModelDialog.propDecorators = {
    appChildMessage: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLWFsZXJ0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLWFsZXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFxQixNQUFNLEVBQThDLEtBQUssRUFBRyxNQUFNLGVBQWUsQ0FBQztBQUt6SCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFlBQVksRUFBcUMsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RixPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBZ0IsTUFBTSxNQUFNLENBQUM7QUFDcEQsT0FBTyxFQUFhLElBQUksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWpELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsWUFBWSxFQUFzQyxlQUFlLEVBQWdDLE1BQU0sbUJBQW1CLENBQUM7QUFVcEksTUFBTSxPQUFPLGVBQWU7SUFReEIsWUFBb0IsV0FBMkIsRUFDcEMsTUFBaUIsRUFDakIsWUFBMkMsRUFDakIsSUFBUyxFQUNsQyxZQUEwQjtRQUpsQixnQkFBVyxHQUFYLFdBQVcsQ0FBZ0I7UUFDcEMsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQixpQkFBWSxHQUFaLFlBQVksQ0FBK0I7UUFDakIsU0FBSSxHQUFKLElBQUksQ0FBSztRQUNsQyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQVB0QyxpQkFBWSxHQUFrQixJQUFJLE9BQU8sRUFBRSxDQUFDO0lBVTVDLENBQUM7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUMvQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO1FBQy9DLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxDQUFDLE1BQU07UUFFN0IsTUFBTSxRQUFRLEdBQUcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsaUJBQWlCLEdBQUcsS0FBSyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQyxJQUFJLENBQ2hELElBQUksQ0FBQyxRQUFRLENBQUMsQ0FDYixDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUNwQixJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsUUFBUSxDQUFDLEVBQzNDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUNWLEdBQUcsRUFBRTtZQUNELElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDbEMsQ0FBQyxDQUNBLENBQUE7SUFDTCxDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFLENBQUM7UUFDcEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFFTyxNQUFNLENBQUMsS0FBSztRQUNoQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFTyxVQUFVLENBQUMsS0FBYTtRQUNoQyxNQUFNLE9BQU8sR0FBRyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUNwRCxPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVPLFVBQVUsQ0FBQyxLQUFhO1FBQ2hDLE1BQU0sT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDO1FBQzFDLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRU8sR0FBRyxDQUFDLEtBQVU7UUFDdEIsT0FBTyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDeEMsQ0FBQzs7O1lBckVKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsWUFBWTtnQkFDdEIsaTdFQUFrRDtnQkFDbEQsU0FBUyxFQUFFLENBQUMsY0FBYyxDQUFDO2FBQzlCOzs7O1lBYlEsY0FBYztZQUtkLFNBQVM7WUFDVCxZQUFZOzRDQXFCWixNQUFNLFNBQUMsZUFBZTtZQTFCdEIsWUFBWTs7OzhCQW1CaEIsS0FBSyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95LCBPbkluaXQsIEluamVjdCwgVmlld0VuY2Fwc3VsYXRpb24sIFZpZXdDaGlsZCwgQWZ0ZXJWaWV3SW5pdCxJbnB1dCAgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUJ1aWxkZXIsIEZvcm1Hcm91cCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcblxyXG5pbXBvcnQgeyBTZXNzaW9uU2VydmljZSB9IGZyb20gJy4vc2Vzc2lvbi1leHBpcmF0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPQXV0aFNlcnZpY2UsIEF1dGhDb25maWcsIE51bGxWYWxpZGF0aW9uSGFuZGxlciB9IGZyb20gJ2FuZ3VsYXItb2F1dGgyLW9pZGMnO1xyXG5pbXBvcnQgeyBTdWJqZWN0LCB0aW1lciwgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCwgdGFrZSB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2RpYWxvZyc7XHJcbmltcG9ydCB7IE1hdERpYWxvZ1JlZiwgTWF0Q2hpcElucHV0RXZlbnQsIE1hdEF1dG9jb21wbGV0ZSwgTUFUX0RJQUxPR19EQVRBLCBNYXRBdXRvY29tcGxldGVTZWxlY3RlZEV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhdXRoLm1vZGVsJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zZXNzaW9uLWV4cGlyYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgcHJvdmlkZXJzOiBbU2Vzc2lvblNlcnZpY2VdXHJcbn0pXHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIEF1dGhNb2RlbERpYWxvZyB7XHJcblxyXG4gICAgbWludXRlc0Rpc3BsYXk7XHJcbiAgICBzZWNvbmRzRGlzcGxheTtcclxuICAgIEBJbnB1dCgpIGFwcENoaWxkTWVzc2FnZTogc3RyaW5nO1xyXG4gICAgdW5zdWJzY3JpYmUkOiBTdWJqZWN0PHZvaWQ+ID0gbmV3IFN1YmplY3QoKTtcclxuICAgIHRpbWVyU3Vic2NyaXB0aW9uOiBTdWJzY3JpcHRpb247XHJcbiAgICBcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXV0aFNlcnZpY2U6IFNlc3Npb25TZXJ2aWNlLFxyXG4gICAgICAgIHB1YmxpYyBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwdWJsaWMgbWF0RGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8QXV0aE1vZGVsRGlhbG9nPixcclxuICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSkgcHJpdmF0ZSBkYXRhOiBhbnksXHJcbiAgICAgICAgcHJpdmF0ZSBvYXV0aFNlcnZpY2U6IE9BdXRoU2VydmljZVxyXG4gICAgKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMubWludXRlc0Rpc3BsYXkgPSB0aGlzLmRhdGEubWludXRlc0Rpc3BsYXk7XHJcbiAgICAgICAgdGhpcy5zZWNvbmRzRGlzcGxheSA9IHRoaXMuZGF0YS5zZWNvbmRzRGlzcGxheTtcclxuICAgICAgICBjb25zdCBpbnRlcnZhbCA9IDEwMDA7IC8vMTAwMFxyXG5cclxuICAgICAgICBjb25zdCBkdXJhdGlvbiA9IDIgKiA2MDtcclxuICAgICAgICB0aGlzLnRpbWVyU3Vic2NyaXB0aW9uID0gdGltZXIoMCwgaW50ZXJ2YWwpLnBpcGUoXHJcbiAgICAgICAgdGFrZShkdXJhdGlvbilcclxuICAgICAgICApLnN1YnNjcmliZSh2YWx1ZSA9PlxyXG4gICAgICAgIHRoaXMucmVuZGVyKChkdXJhdGlvbiAtICt2YWx1ZSkgKiBpbnRlcnZhbCksXHJcbiAgICAgICAgZXJyID0+IHsgfSxcclxuICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2UubG9nT3V0VXNlcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgICApXHJcbiAgICB9XHJcblxyXG4gICAgbG9nb3V0KCkge1xyXG4gICAgICAgIHRoaXMub2F1dGhTZXJ2aWNlLmxvZ091dChmYWxzZSk7XHJcbiAgICAgICAgdGhpcy5tYXREaWFsb2dSZWYuY2xvc2UoKTtcclxuICAgIH1cclxuXHJcbiAgICBvblN1Ym1pdCgpIHtcclxuICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLm5vdGlmeVVzZXJBY3Rpb24oKTtcclxuICAgICAgICB0aGlzLm1hdERpYWxvZ1JlZi5jbG9zZSgpO1xyXG4gICAgICAgIHRoaXMub2F1dGhTZXJ2aWNlLnNpbGVudFJlZnJlc2goKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlbmRlcihjb3VudCkge1xyXG4gICAgICAgIHRoaXMuc2Vjb25kc0Rpc3BsYXkgPSB0aGlzLmdldFNlY29uZHMoY291bnQpO1xyXG4gICAgICAgIHRoaXMubWludXRlc0Rpc3BsYXkgPSB0aGlzLmdldE1pbnV0ZXMoY291bnQpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBwcml2YXRlIGdldFNlY29uZHModGlja3M6IG51bWJlcikge1xyXG4gICAgY29uc3Qgc2Vjb25kcyA9ICgodGlja3MgJSA2MDAwMCkgLyAxMDAwKS50b0ZpeGVkKDApO1xyXG4gICAgcmV0dXJuIHRoaXMucGFkKHNlY29uZHMpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0TWludXRlcyh0aWNrczogbnVtYmVyKSB7XHJcbiAgICBjb25zdCBtaW51dGVzID0gTWF0aC5mbG9vcih0aWNrcyAvIDYwMDAwKTtcclxuICAgIHJldHVybiB0aGlzLnBhZChtaW51dGVzKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHBhZChkaWdpdDogYW55KSB7XHJcbiAgICByZXR1cm4gZGlnaXQgPD0gOSA/ICcwJyArIGRpZ2l0IDogZGlnaXQ7XHJcbiAgICB9XHJcbn0iXX0=