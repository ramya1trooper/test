import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatIconModule, MatInputModule } from '@angular/material';
import { SessionComponent } from './session-expiration.component';
import { SessionService } from './session-expiration.service';
import { AuthModelDialog } from './session-expiration-alert.component';
import { FuseSharedModule } from '../@fuse/shared.module';
export class SessionModule {
}
SessionModule.decorators = [
    { type: NgModule, args: [{
                declarations: [SessionComponent, AuthModelDialog
                ],
                imports: [
                    MatButtonModule,
                    MatCheckboxModule,
                    MatFormFieldModule,
                    MatIconModule,
                    MatInputModule, FuseSharedModule
                ],
                exports: [SessionComponent, AuthModelDialog],
                providers: [SessionService],
                schemas: [CUSTOM_ELEMENTS_SCHEMA]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLHNCQUFzQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpFLE9BQU8sRUFBRSxlQUFlLEVBQUUsaUJBQWlCLEVBQUUsa0JBQWtCLEVBQUUsYUFBYSxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBR3pILE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ25FLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDdkUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFrQjFELE1BQU0sT0FBTyxhQUFhOzs7WUFmekIsUUFBUSxTQUFDO2dCQUNULFlBQVksRUFBRSxDQUFDLGdCQUFnQixFQUFDLGVBQWU7aUJBRTlDO2dCQUNELE9BQU8sRUFBRTtvQkFDUixlQUFlO29CQUNmLGlCQUFpQjtvQkFDakIsa0JBQWtCO29CQUNsQixhQUFhO29CQUNiLGNBQWMsRUFBQyxnQkFBZ0I7aUJBQy9CO2dCQUNELE9BQU8sRUFBRSxDQUFDLGdCQUFnQixFQUFDLGVBQWUsQ0FBQztnQkFDM0MsU0FBUyxFQUFFLENBQUMsY0FBYyxDQUFDO2dCQUMzQixPQUFPLEVBQUUsQ0FBQyxzQkFBc0IsQ0FBQzthQUNqQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlLCBDVVNUT01fRUxFTUVOVFNfU0NIRU1BIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSwgTWF0Q2hlY2tib3hNb2R1bGUsIE1hdEZvcm1GaWVsZE1vZHVsZSwgTWF0SWNvbk1vZHVsZSwgTWF0SW5wdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5cclxuIGltcG9ydCB7IFNlc3Npb25Db21wb25lbnQgfSBmcm9tICcuL3Nlc3Npb24tZXhwaXJhdGlvbi5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTZXNzaW9uU2VydmljZSB9IGZyb20gJy4vc2Vzc2lvbi1leHBpcmF0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBdXRoTW9kZWxEaWFsb2cgfSBmcm9tICcuL3Nlc3Npb24tZXhwaXJhdGlvbi1hbGVydC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGdXNlU2hhcmVkTW9kdWxlIH0gZnJvbSAnLi4vQGZ1c2Uvc2hhcmVkLm1vZHVsZSc7XHJcblxyXG5cclxuQE5nTW9kdWxlKHtcclxuXHRkZWNsYXJhdGlvbnM6IFtTZXNzaW9uQ29tcG9uZW50LEF1dGhNb2RlbERpYWxvZ1xyXG5cdFx0XHJcblx0XSxcclxuXHRpbXBvcnRzOiBbXHJcblx0XHRNYXRCdXR0b25Nb2R1bGUsXHJcblx0XHRNYXRDaGVja2JveE1vZHVsZSxcclxuXHRcdE1hdEZvcm1GaWVsZE1vZHVsZSxcclxuXHRcdE1hdEljb25Nb2R1bGUsXHJcblx0XHRNYXRJbnB1dE1vZHVsZSxGdXNlU2hhcmVkTW9kdWxlXHJcblx0XSxcclxuXHRleHBvcnRzOiBbU2Vzc2lvbkNvbXBvbmVudCxBdXRoTW9kZWxEaWFsb2ddLFxyXG5cdHByb3ZpZGVyczogW1Nlc3Npb25TZXJ2aWNlXSxcclxuXHRzY2hlbWFzOiBbQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIFNlc3Npb25Nb2R1bGUge1xyXG59XHJcbiJdfQ==