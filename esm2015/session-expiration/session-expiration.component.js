import { Component, HostListener } from '@angular/core';
import { SessionService } from './session-expiration.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject, timer } from 'rxjs';
import { takeUntil, take } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { AuthModelDialog } from './session-expiration-alert.component';
export class SessionComponent {
    constructor(authService, dialog, oauthService) {
        this.authService = authService;
        this.dialog = dialog;
        this.oauthService = oauthService;
        this.minutesDisplay = 0;
        this.secondsDisplay = 0;
        this.endTime = 20; // 20 - 20Mins;
        this.unsubscribe$ = new Subject();
        this.appParentMessage = 'This message is from parent';
        this.subscription = this.authService
            .userActionOccured()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(() => {
            if (this.timerSubscription) {
                this.timerSubscription.unsubscribe();
            }
            this.resetTimer();
        });
    }
    resetTimerfn() {
        this.authService.notifyUserAction();
    }
    ngOnInit() {
        this.resetTimer();
    }
    openDialog() {
        const dialogRef = this.dialog.open(AuthModelDialog, {
            width: '40%',
            data: {
                minutesDisplay: this.minutesDisplay,
                secondsDisplay: this.secondsDisplay
            }
        });
        dialogRef.afterClosed().subscribe(result => {
            console.log(`Dialog result:`);
            this.authService.notifyUserAction();
        });
    }
    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    resetTimer(endTime = this.endTime) {
        // console.log('######');
        const interval = 1000; //1000
        const duration = endTime * 60;
        this.timerSubscription = timer(0, interval)
            .pipe(take(duration))
            .subscribe(value => this.render((duration - +value) * interval), err => { }, () => {
            this.authService.logOutUser();
        });
    }
    render(count) {
        this.secondsDisplay = this.getSeconds(count);
        this.minutesDisplay = this.getMinutes(count);
        if (this.minutesDisplay == 1 && this.secondsDisplay == 59) {
            this.openDialog();
        }
    }
    getSeconds(ticks) {
        const seconds = ((ticks % 60000) / 1000).toFixed(0);
        return this.pad(seconds);
    }
    getMinutes(ticks) {
        const minutes = Math.floor(ticks / 60000);
        return this.pad(minutes);
    }
    pad(digit) {
        return digit <= 9 ? '0' + digit : digit;
    }
}
SessionComponent.decorators = [
    { type: Component, args: [{
                selector: 'session-expiration',
                template: `
    <h1></h1>
  `,
                providers: [SessionService]
            }] }
];
/** @nocollapse */
SessionComponent.ctorParameters = () => [
    { type: SessionService },
    { type: MatDialog },
    { type: OAuthService }
];
SessionComponent.propDecorators = {
    resetTimerfn: [{ type: HostListener, args: ['document:keyup', [],] }, { type: HostListener, args: ['document:click', [],] }, { type: HostListener, args: ['document:wheel', [],] }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3VpLWNvbW1vbi1saWIvIiwic291cmNlcyI6WyJzZXNzaW9uLWV4cGlyYXRpb24vc2Vzc2lvbi1leHBpcmF0aW9uLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQ0wsU0FBUyxFQU9ULFlBQVksRUFDYixNQUFNLGVBQWUsQ0FBQztBQUl2QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUNMLFlBQVksRUFHYixNQUFNLHFCQUFxQixDQUFDO0FBQzdCLE9BQU8sRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFnQixNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRWpELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVFyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFXdkUsTUFBTSxPQUFPLGdCQUFnQjtJQVMzQixZQUNVLFdBQTJCLEVBQzVCLE1BQWlCLEVBQ2hCLFlBQTBCO1FBRjFCLGdCQUFXLEdBQVgsV0FBVyxDQUFnQjtRQUM1QixXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2hCLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBWHBDLG1CQUFjLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLG1CQUFjLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLFlBQU8sR0FBRyxFQUFFLENBQUMsQ0FBQyxlQUFlO1FBRTdCLGlCQUFZLEdBQWtCLElBQUksT0FBTyxFQUFFLENBQUM7UUFFNUMscUJBQWdCLEdBQUcsNkJBQTZCLENBQUM7UUFPL0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsV0FBVzthQUNqQyxpQkFBaUIsRUFBRTthQUNuQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNsQyxTQUFTLENBQUMsR0FBRyxFQUFFO1lBQ2QsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUN0QztZQUNELElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNwQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFJRCxZQUFZO1FBQ1YsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFDRCxRQUFRO1FBQ04sSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCxVQUFVO1FBQ1IsTUFBTSxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ2xELEtBQUssRUFBRSxLQUFLO1lBQ1osSUFBSSxFQUFFO2dCQUNKLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYztnQkFDbkMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjO2FBQ3BDO1NBQ0YsQ0FBQyxDQUFDO1FBRUgsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDOUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3RDLENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUVELFdBQVc7UUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3pCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQztJQUVELFVBQVUsQ0FBQyxVQUFrQixJQUFJLENBQUMsT0FBTztRQUN2Qyx5QkFBeUI7UUFDekIsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLENBQUMsTUFBTTtRQUU3QixNQUFNLFFBQVEsR0FBRyxPQUFPLEdBQUcsRUFBRSxDQUFDO1FBQzlCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQzthQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3BCLFNBQVMsQ0FDUixLQUFLLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxRQUFRLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxRQUFRLENBQUMsRUFDcEQsR0FBRyxDQUFDLEVBQUUsR0FBRSxDQUFDLEVBQ1QsR0FBRyxFQUFFO1lBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUNoQyxDQUFDLENBQ0YsQ0FBQztJQUNOLENBQUM7SUFFTyxNQUFNLENBQUMsS0FBYztRQUMzQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdDLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxFQUFFLEVBQUU7WUFDekQsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQ25CO0lBQ0gsQ0FBQztJQUVPLFVBQVUsQ0FBQyxLQUFhO1FBQzlCLE1BQU0sT0FBTyxHQUFHLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ3BELE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMzQixDQUFDO0lBRU8sVUFBVSxDQUFDLEtBQWE7UUFDOUIsTUFBTSxPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDMUMsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzNCLENBQUM7SUFFTyxHQUFHLENBQUMsS0FBVTtRQUNwQixPQUFPLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMxQyxDQUFDOzs7WUFuR0YsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLFFBQVEsRUFBRTs7R0FFVDtnQkFFRCxTQUFTLEVBQUUsQ0FBQyxjQUFjLENBQUM7YUFDNUI7Ozs7WUExQlEsY0FBYztZQVNkLFNBQVM7WUFQaEIsWUFBWTs7OzJCQWtEWCxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxjQUNqQyxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSxjQUNqQyxZQUFZLFNBQUMsZ0JBQWdCLEVBQUUsRUFBRSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7XHJcbiAgQ29tcG9uZW50LFxyXG4gIE9uRGVzdHJveSxcclxuICBPbkluaXQsXHJcbiAgSW5qZWN0LFxyXG4gIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gIFZpZXdDaGlsZCxcclxuICBBZnRlclZpZXdJbml0LFxyXG4gIEhvc3RMaXN0ZW5lclxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtQnVpbGRlciwgRm9ybUdyb3VwLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuaW1wb3J0IHsgU2Vzc2lvblNlcnZpY2UgfSBmcm9tICcuL3Nlc3Npb24tZXhwaXJhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHtcclxuICBPQXV0aFNlcnZpY2UsXHJcbiAgQXV0aENvbmZpZyxcclxuICBOdWxsVmFsaWRhdGlvbkhhbmRsZXJcclxufSBmcm9tICdhbmd1bGFyLW9hdXRoMi1vaWRjJztcclxuaW1wb3J0IHsgU3ViamVjdCwgdGltZXIsIFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwsIHRha2UgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbC9kaWFsb2cnO1xyXG5pbXBvcnQge1xyXG4gIE1hdERpYWxvZ1JlZixcclxuICBNYXRDaGlwSW5wdXRFdmVudCxcclxuICBNYXRBdXRvY29tcGxldGUsXHJcbiAgTUFUX0RJQUxPR19EQVRBLFxyXG4gIE1hdEF1dG9jb21wbGV0ZVNlbGVjdGVkRXZlbnRcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IEF1dGhNb2RlbERpYWxvZyB9IGZyb20gJy4vc2Vzc2lvbi1leHBpcmF0aW9uLWFsZXJ0LmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogJ3Nlc3Npb24tZXhwaXJhdGlvbicsXHJcbiAgdGVtcGxhdGU6IGBcclxuICAgIDxoMT48L2gxPlxyXG4gIGAsXHJcbiAgc3R5bGVzOiBbXSxcclxuICBwcm92aWRlcnM6IFtTZXNzaW9uU2VydmljZV1cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBTZXNzaW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25EZXN0cm95LCBPbkluaXQge1xyXG4gIG1pbnV0ZXNEaXNwbGF5ID0gMDtcclxuICBzZWNvbmRzRGlzcGxheSA9IDA7XHJcbiAgZW5kVGltZSA9IDIwOyAvLyAyMCAtIDIwTWlucztcclxuXHJcbiAgdW5zdWJzY3JpYmUkOiBTdWJqZWN0PHZvaWQ+ID0gbmV3IFN1YmplY3QoKTtcclxuICB0aW1lclN1YnNjcmlwdGlvbjogU3Vic2NyaXB0aW9uO1xyXG4gIGFwcFBhcmVudE1lc3NhZ2UgPSAnVGhpcyBtZXNzYWdlIGlzIGZyb20gcGFyZW50JztcclxuICBzdWJzY3JpcHRpb246IFN1YnNjcmlwdGlvbjtcclxuICBjb25zdHJ1Y3RvcihcclxuICAgIHByaXZhdGUgYXV0aFNlcnZpY2U6IFNlc3Npb25TZXJ2aWNlLFxyXG4gICAgcHVibGljIGRpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHJpdmF0ZSBvYXV0aFNlcnZpY2U6IE9BdXRoU2VydmljZVxyXG4gICkge1xyXG4gICAgdGhpcy5zdWJzY3JpcHRpb24gPSB0aGlzLmF1dGhTZXJ2aWNlXHJcbiAgICAgIC51c2VyQWN0aW9uT2NjdXJlZCgpXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlJCkpXHJcbiAgICAgIC5zdWJzY3JpYmUoKCkgPT4ge1xyXG4gICAgICAgIGlmICh0aGlzLnRpbWVyU3Vic2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICB0aGlzLnRpbWVyU3Vic2NyaXB0aW9uLnVuc3Vic2NyaWJlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmVzZXRUaW1lcigpO1xyXG4gICAgICB9KTtcclxuICB9XHJcbiAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5dXAnLCBbXSlcclxuICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDpjbGljaycsIFtdKVxyXG4gIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OndoZWVsJywgW10pXHJcbiAgcmVzZXRUaW1lcmZuKCkge1xyXG4gICAgdGhpcy5hdXRoU2VydmljZS5ub3RpZnlVc2VyQWN0aW9uKCk7XHJcbiAgfVxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5yZXNldFRpbWVyKCk7XHJcbiAgfVxyXG5cclxuICBvcGVuRGlhbG9nKCkge1xyXG4gICAgY29uc3QgZGlhbG9nUmVmID0gdGhpcy5kaWFsb2cub3BlbihBdXRoTW9kZWxEaWFsb2csIHtcclxuICAgICAgd2lkdGg6ICc0MCUnLFxyXG4gICAgICBkYXRhOiB7XHJcbiAgICAgICAgbWludXRlc0Rpc3BsYXk6IHRoaXMubWludXRlc0Rpc3BsYXksXHJcbiAgICAgICAgc2Vjb25kc0Rpc3BsYXk6IHRoaXMuc2Vjb25kc0Rpc3BsYXlcclxuICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgZGlhbG9nUmVmLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKHJlc3VsdCA9PiB7XHJcbiAgICAgIGNvbnNvbGUubG9nKGBEaWFsb2cgcmVzdWx0OmApO1xyXG4gICAgICB0aGlzLmF1dGhTZXJ2aWNlLm5vdGlmeVVzZXJBY3Rpb24oKTtcclxuICAgIH0pO1xyXG4gIH1cclxuXHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlJC5uZXh0KCk7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlJC5jb21wbGV0ZSgpO1xyXG4gIH1cclxuXHJcbiAgcmVzZXRUaW1lcihlbmRUaW1lOiBudW1iZXIgPSB0aGlzLmVuZFRpbWUpIHtcclxuICAgIC8vIGNvbnNvbGUubG9nKCcjIyMjIyMnKTtcclxuICAgIGNvbnN0IGludGVydmFsID0gMTAwMDsgLy8xMDAwXHJcblxyXG4gICAgY29uc3QgZHVyYXRpb24gPSBlbmRUaW1lICogNjA7XHJcbiAgICB0aGlzLnRpbWVyU3Vic2NyaXB0aW9uID0gdGltZXIoMCwgaW50ZXJ2YWwpXHJcbiAgICAgIC5waXBlKHRha2UoZHVyYXRpb24pKVxyXG4gICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgIHZhbHVlID0+IHRoaXMucmVuZGVyKChkdXJhdGlvbiAtICt2YWx1ZSkgKiBpbnRlcnZhbCksXHJcbiAgICAgICAgZXJyID0+IHt9LFxyXG4gICAgICAgICgpID0+IHtcclxuICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2UubG9nT3V0VXNlcigpO1xyXG4gICAgICAgIH1cclxuICAgICAgKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcmVuZGVyKGNvdW50IDogbnVtYmVyKSB7XHJcbiAgICB0aGlzLnNlY29uZHNEaXNwbGF5ID0gdGhpcy5nZXRTZWNvbmRzKGNvdW50KTtcclxuICAgIHRoaXMubWludXRlc0Rpc3BsYXkgPSB0aGlzLmdldE1pbnV0ZXMoY291bnQpO1xyXG4gICAgaWYgKHRoaXMubWludXRlc0Rpc3BsYXkgPT0gMSAmJiB0aGlzLnNlY29uZHNEaXNwbGF5ID09IDU5KSB7XHJcbiAgICAgIHRoaXMub3BlbkRpYWxvZygpO1xyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcHJpdmF0ZSBnZXRTZWNvbmRzKHRpY2tzOiBudW1iZXIpIHtcclxuICAgIGNvbnN0IHNlY29uZHMgPSAoKHRpY2tzICUgNjAwMDApIC8gMTAwMCkudG9GaXhlZCgwKTtcclxuICAgIHJldHVybiB0aGlzLnBhZChzZWNvbmRzKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgZ2V0TWludXRlcyh0aWNrczogbnVtYmVyKSB7XHJcbiAgICBjb25zdCBtaW51dGVzID0gTWF0aC5mbG9vcih0aWNrcyAvIDYwMDAwKTtcclxuICAgIHJldHVybiB0aGlzLnBhZChtaW51dGVzKTtcclxuICB9XHJcblxyXG4gIHByaXZhdGUgcGFkKGRpZ2l0OiBhbnkpIHtcclxuICAgIHJldHVybiBkaWdpdCA8PSA5ID8gJzAnICsgZGlnaXQgOiBkaWdpdDtcclxuICB9XHJcbn1cclxuIl19