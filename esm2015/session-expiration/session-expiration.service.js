import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import 'rxjs/Rx';
import { environment } from '../environments/environment';
import * as i0 from "@angular/core";
import * as i1 from "angular-oauth2-oidc";
import * as i2 from "@angular/common/http";
export class SessionService {
    constructor(oauthService, httpClient) {
        this.oauthService = oauthService;
        this.httpClient = httpClient;
        this.baseURL = environment.baseUrl;
        this._userActionOccured = new Subject();
    }
    userActionOccured() {
        return this._userActionOccured.asObservable();
    }
    notifyUserAction() {
        this._userActionOccured.next();
    }
    loginUser() {
        console.log('user login');
    }
    logOutUser() {
        console.log('logout...');
        localStorage.clear();
        sessionStorage.clear();
        this.oauthService.logOut(false);
    }
    continueSession() {
        console.log(`I issue an API request to server.`);
    }
    stopSession() {
        console.log(`I logout.`);
    }
}
SessionService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SessionService.ctorParameters = () => [
    { type: OAuthService },
    { type: HttpClient }
];
SessionService.ngInjectableDef = i0.defineInjectable({ factory: function SessionService_Factory() { return new SessionService(i0.inject(i1.OAuthService), i0.inject(i2.HttpClient)); }, token: SessionService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2Vzc2lvbi1leHBpcmF0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsic2Vzc2lvbi1leHBpcmF0aW9uL3Nlc3Npb24tZXhwaXJhdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxPQUFPLEVBQStCLE1BQU0sTUFBTSxDQUFDO0FBQzVELE9BQU8sRUFBRSxVQUFVLEVBQWdCLE1BQU0sc0JBQXNCLENBQUM7QUFFaEUsT0FBTyxTQUFTLENBQUM7QUFFakIsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDZCQUE2QixDQUFDOzs7O0FBTzFELE1BQU0sT0FBTyxjQUFjO0lBS3pCLFlBQ1UsWUFBMEIsRUFDMUIsVUFBc0I7UUFEdEIsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQU5oQyxZQUFPLEdBQVcsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUV0Qyx1QkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBTyxDQUFDO0lBS3JDLENBQUM7SUFDSixpQkFBaUI7UUFDZixPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNoRCxDQUFDO0lBQ0QsZ0JBQWdCO1FBQ2QsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFRCxTQUFTO1FBQ1AsT0FBTyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsVUFBVTtRQUNSLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDekIsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsQyxDQUFDO0lBRUQsZUFBZTtRQUNiLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBQ0QsV0FBVztRQUNULE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDM0IsQ0FBQzs7O1lBckNGLFVBQVUsU0FDVDtnQkFDQSxVQUFVLEVBQUUsTUFBTTthQUNuQjs7OztZQVhRLFlBQVk7WUFFWixVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPQXV0aFNlcnZpY2UgfSBmcm9tICdhbmd1bGFyLW9hdXRoMi1vaWRjJztcclxuaW1wb3J0IHsgU3ViamVjdCwgT2JzZXJ2YWJsZSwgQmVoYXZpb3JTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQsIEh0dHBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgJ3J4anMvUngnO1xyXG5cclxuaW1wb3J0IHsgZW52aXJvbm1lbnQgfSBmcm9tICcuLi9lbnZpcm9ubWVudHMvZW52aXJvbm1lbnQnO1xyXG5cclxuQEluamVjdGFibGUoXHJcbiAge1xyXG4gIHByb3ZpZGVkSW46ICdyb290J1xyXG59XHJcbilcclxuZXhwb3J0IGNsYXNzIFNlc3Npb25TZXJ2aWNlIHtcclxuICBiYXNlVVJMOiBzdHJpbmcgPSBlbnZpcm9ubWVudC5iYXNlVXJsO1xyXG5cclxuICBfdXNlckFjdGlvbk9jY3VyZWQgPSBuZXcgU3ViamVjdDxhbnk+KCk7XHJcblxyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBvYXV0aFNlcnZpY2U6IE9BdXRoU2VydmljZSxcclxuICAgIHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudFxyXG4gICkge31cclxuICB1c2VyQWN0aW9uT2NjdXJlZCgpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgcmV0dXJuIHRoaXMuX3VzZXJBY3Rpb25PY2N1cmVkLmFzT2JzZXJ2YWJsZSgpO1xyXG4gIH1cclxuICBub3RpZnlVc2VyQWN0aW9uKCkge1xyXG4gICAgdGhpcy5fdXNlckFjdGlvbk9jY3VyZWQubmV4dCgpO1xyXG4gIH1cclxuXHJcbiAgbG9naW5Vc2VyKCkge1xyXG4gICAgY29uc29sZS5sb2coJ3VzZXIgbG9naW4nKTtcclxuICB9XHJcblxyXG4gIGxvZ091dFVzZXIoKSB7XHJcbiAgICBjb25zb2xlLmxvZygnbG9nb3V0Li4uJyk7XHJcbiAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcclxuICAgIHNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XHJcbiAgICB0aGlzLm9hdXRoU2VydmljZS5sb2dPdXQoZmFsc2UpO1xyXG4gIH1cclxuXHJcbiAgY29udGludWVTZXNzaW9uKCkge1xyXG4gICAgY29uc29sZS5sb2coYEkgaXNzdWUgYW4gQVBJIHJlcXVlc3QgdG8gc2VydmVyLmApO1xyXG4gIH1cclxuICBzdG9wU2Vzc2lvbigpIHtcclxuICAgIGNvbnNvbGUubG9nKGBJIGxvZ291dC5gKTtcclxuICB9XHJcbn1cclxuIl19