import { SnackBarService } from "./../shared/snackbar.service";
import { Component, Input, Inject } from "@angular/core";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
// import { locale as english } from "../i18n/en";
import { ModelLayoutComponent } from "../model-layout/model-layout.component";
import { HttpClient } from "@angular/common/http";
import { MatDialog } from "@angular/material";
import * as FileSaver from "file-saver";
import { ContentService } from "../content/content.service";
import * as _ from "lodash";
import { MessageService } from "../_services/message.service";
import { Subject } from "rxjs/Subject";
import { takeUntil } from "rxjs/operators";
export class ButtonLayoutComponent {
    constructor(_fuseTranslationLoaderService, _matDialog, httpClient, contentService, snackBarService, messageService, environment, english) {
        this._fuseTranslationLoaderService = _fuseTranslationLoaderService;
        this._matDialog = _matDialog;
        this.httpClient = httpClient;
        this.contentService = contentService;
        this.snackBarService = snackBarService;
        this.messageService = messageService;
        this.environment = environment;
        this.english = english;
        this.enableHeader = true;
        this.enableButtonLayout = false;
        this.buttonsConfigData = {};
        this.unsubscribe = new Subject();
        this.tableHeaderConfig = [];
        this.selectedTableHeader = [];
        this._fuseTranslationLoaderService.loadTranslations(english);
        this.messageService
            .getButtonEnableMessage()
            .pipe(takeUntil(this.unsubscribe))
            .subscribe((message) => {
            this.reinitializeButtons();
        });
    }
    ngOnInit() {
        this.httpClient
            .get("assets/configFiles/buttons_configFile.json")
            .subscribe((fileResponse) => {
            localStorage.setItem("buttonConfig", JSON.stringify(fileResponse));
            this.buttonsConfigData = fileResponse;
        });
        this.currentConfigData = JSON.parse(localStorage.getItem("currentConfigData"));
        this.defaultDatasource = localStorage.getItem("datasource");
        this.userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        this.onLoad();
    }
    ngOnDestroy() {
        this.unsubscribe.next();
    }
    reinitializeButtons() {
        // is alow datasouce check
        let userData = JSON.parse(localStorage.getItem("currentLoginUser"));
        let tempObj = localStorage.getItem("currentInput");
        tempObj = tempObj ? JSON.parse(tempObj) : {};
        _.forEach(this.currentData.enabledButtons, function (item) {
            if (item.keyToCheck) {
                item.disable =
                    tempObj && tempObj[item.keyToCheck] && tempObj[item.keyToCheck].length
                        ? false
                        : true;
            }
            if (item.checkimportAccess) {
                let currentRealm = localStorage.getItem("realm");
                let matchedRealm = _.filter(userData.realm, { _id: currentRealm });
                console.log(">>> matchedrealm", matchedRealm);
                //  console.log(">>> current Realm access ",matchedRealm[0]['allow_datasource']);
                //  item.disable =(matchedRealm.length>0 && matchedRealm[0]['allow_datasource'] == "YES") ? false : true;
                item.disable = userData && userData.allow_datasource == "YES" ? false : true;
            }
        });
    }
    onLoad() {
        this.currentData = this.fromRouting ? this.currentConfigData['pageRoutingView'] : this.currentConfigData["listView"];
        if (this.currentData &&
            this.currentData.enableTableSettings &&
            this.currentData.tableData &&
            this.currentData.tableData[0]) {
            this.tableHeaderConfig = this.currentData.tableData[0].tableHeader;
            let tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
            this.selectedTableHeader = _.map(tempArray, "value");
            _.remove(this.tableHeaderConfig, { value: "select" });
            _.remove(this.tableHeaderConfig, { value: "action" });
        }
        else if (this.currentData &&
            this.currentData.enableButtonToggle &&
            this.currentData.formData &&
            this.currentData.formData[0] &&
            this.currentData.formData[0].tableData) {
            this.tableHeaderConfig = this.currentData.formData[0].tableData[0].tableHeader;
            let tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
            this.selectedTableHeader = _.map(tempArray, "value");
        }
        else if (this.currentData &&
            this.currentData.enableTableSettings &&
            this.currentData.enableFormLayout) {
            this.tableHeaderConfig = this.currentData.formData[0].tableData[0].tableHeader;
            let tempArray = _.filter(this.tableHeaderConfig, { isActive: true });
            this.selectedTableHeader = _.map(tempArray, "value");
        }
        // if(this.currentData && this.currentData.enableTableSettings){
        //   this.tableHeaderConfig = this.currentData.tableData[0].tableHeader;
        //   let tempArray = _.filter(this.tableHeaderConfig, {isActive : true})
        //   this.selectedTableHeader = _.map(tempArray, 'value');
        // }
        console.log(this.currentData, ".......CURRENT");
        this.formValues = this.currentData.formData
            ? this.currentData.formData[0]
            : {};
        console.log(this.formValues, ".....formValues");
        this.headerData = {
            pageHeader: this.currentData.pageHeader,
            pageTagLine: this.currentData.pageTagLine,
            pageHeaderIcon: this.currentData.pageHeaderIcon,
        };
        this.enabledButtons = this.currentData.enabledButtons;
        this.enableButtonLayout =
            this.enabledButtons && this.enabledButtons.length ? true : false;
        if (this.currentData.enableButtonToggle) {
            this.enableButtonLayout = true;
            this.defaultToggleValue = this.currentData.buttonToggles[0];
            this.toggleChange(this.defaultToggleValue);
        }
        this.reinitializeButtons();
    }
    backClick() {
        console.log("BACK");
        localStorage.removeItem("currentInput");
        let obj = {
            id: this.currentData.backNavId,
            url: this.currentData.backNavUrl,
            configFilePath: this.currentData.backNavConfigFilePath
        };
        this.messageService.sendRouting(obj);
    }
    updateTableSettings(event) {
        let selectedHeader = this.selectedTableHeader;
        this.tableHeaderConfig.forEach(function (item) {
            if (selectedHeader.indexOf(item.value) >= 0) {
                item.isActive = true;
            }
            else {
                item.isActive = false;
            }
        });
        localStorage.removeItem("selectedTableHeaders");
        localStorage.setItem("selectedTableHeaders", JSON.stringify(this.tableHeaderConfig));
        this.messageService.sendTableHeaderUpdate("update");
    }
    enableFilter() {
        this.messageService.sendTableHeaderUpdate("filter");
    }
    onClick(action) {
        console.log(">> onClick Export action ", action);
        if (action === "exportData") {
            this.exportData(action);
        }
        else if (action === "refreshPage") {
            this.reloadData(action);
        }
        else if (action == "runAnalyze") {
            console.log(this.currentConfigData[action]);
            let runData = this.currentConfigData[action].runRequest;
            let requestDetails = runData.requestData;
            let apiUrl = runData.apiUrl;
            let toastMessageDetails = runData.toastMessage;
            let currentInputData = JSON.parse(localStorage.getItem("currentInput"));
            let requestData = {};
            _.forEach(requestDetails, function (item) {
                if (item.isDefault) {
                    requestData[item.name] = item.value;
                }
                else {
                    requestData[item.name] = item.convertToString
                        ? JSON.stringify(currentInputData[item.value])
                        : currentInputData[item.value];
                }
            });
            console.log(requestData, "....requestData");
            this.contentService
                .getExportResponse(requestData, apiUrl)
                .subscribe((data) => {
                this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                this.messageService.sendModelCloseEvent("listView");
            });
        }
        else if (action == "exportView") {
            console.log(this, "............THIS");
            let modelData = {};
            if (this.defaultToggleValue) {
                let toggleId = this.defaultToggleValue.toggleId;
                console.log(toggleId, ">>>>>toggleId>>>");
                let modelList = this.currentConfigData[action].modelData;
                console.log(modelList, ".....modelList>>>modelList");
                modelData = _.find(modelList, { modelId: toggleId });
            }
            else {
                modelData = this.currentConfigData[action].modelData;
            }
            let modelWidth = modelData["size"];
            this.dialogRef = this._matDialog
                .open(ModelLayoutComponent, {
                disableClose: true,
                width: modelWidth,
                panelClass: "contact-form-dialog",
                data: {
                    action: "exportView",
                    modelData: modelData,
                },
            })
                .afterClosed()
                .subscribe((response) => {
                localStorage.removeItem("currentInput");
            });
        }
        else if (action == "edit") {
            console.log(this, "............THIS");
            let modelData = {};
            if (this.defaultToggleValue) {
                let toggleId = this.defaultToggleValue.toggleId;
                console.log(toggleId, ">>>>>toggleId>>>");
                let modelList = this.currentConfigData[action].modelData;
                console.log(modelList, ".....modelList>>>modelList");
                modelData = _.find(modelList, { modelId: toggleId });
            }
            else {
                modelData = this.currentConfigData[action].modelData;
            }
            let modelWidth = modelData["size"];
            this.dialogRef = this._matDialog
                .open(ModelLayoutComponent, {
                disableClose: true,
                width: modelWidth,
                panelClass: "contact-form-dialog",
                data: {
                    action: "update",
                    modelData: modelData,
                },
            })
                .afterClosed()
                .subscribe((response) => {
                localStorage.removeItem("currentInput");
            });
        }
        else {
            this.messageService.sendModelCloseEvent(action);
            this.openDialog(action);
        }
    }
    exportData(action) {
        console.log(">>>>>>>> ExportData ", action);
        let currentInputData = JSON.parse(localStorage.getItem("currentInput"));
        let exportData = this.currentConfigData[action];
        currentInputData["exportType"] = exportData.exportType;
        let requestDetails = exportData.exportRequest.requestData;
        let apiUrl = exportData.exportRequest.apiUrl;
        let toastMessageDetails = exportData.exportRequest.toastMessage;
        let requestData = {};
        var self = this;
        let reportTemp;
        console.log(">>>>>>>>>> currentInputData ", currentInputData);
        _.forEach(requestDetails, function (item) {
            // requestData[item.name] = item.convertToString
            //   ? JSON.stringify(currentInputData[item.value])
            //   : currentInputData[item.value];
            let tempData;
            if (item.convertToString) {
                requestData[item.name] = JSON.stringify(currentInputData[item.value]);
            }
            else if (item.isDefault) {
                requestData[item.name] = item.value;
            }
            else if (item.fromLoginData) {
                requestData[item.name] = self.userData ? self.userData[item.value] : "";
            }
            else {
                requestData[item.name] = currentInputData[item.value];
            }
        });
        if (exportData.exportRequest.isReportCreate) {
            // button based report create
            console.log(">>>>>>>>> requestDetails ", requestData);
            let toastMessageDetails = exportData.exportRequest.toastMessage;
            self.contentService.createRequest(requestData, apiUrl).subscribe((res) => {
                self.snackBarService.add(self._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                if (res.status == 200) {
                    localStorage.removeItem("CurrentReportData");
                    self.messageService.sendModelCloseEvent("listView");
                }
            }, (error) => {
                self.snackBarService.warning(self._fuseTranslationLoaderService.instant(toastMessageDetails.error));
            });
        }
        else {
            this.contentService
                .getExportResponse(requestData, apiUrl)
                .subscribe((data) => {
                let fileName = this._fuseTranslationLoaderService.instant(exportData.exportRequest.downloadFileName);
                this.snackBarService.add(this._fuseTranslationLoaderService.instant(toastMessageDetails.success));
                this.reloadData(action);
                this.downloadFile(data, fileName + exportData.exportRequest.downloadFiletype);
            });
        }
    }
    reloadData(action) {
        localStorage.removeItem("currentInput");
        this.messageService.sendModelCloseEvent(action);
        console.log(action, "....action");
        this.reinitializeButtons();
    }
    toggleChange(event) {
        console.log(event);
        let valueToSave = event && event.value ? event.value : event;
        localStorage.setItem("selectedToggleDetail", JSON.stringify(valueToSave));
        // this.currentTableLoad = this.formValues;
        // this.currentTableLoad["response"] = "";
        // this.currentTableLoad["total"] = ""
        // this.enableTableLayout = true
        this.messageService.sendMessage("toggle");
    }
    openDialog(action) {
        let modelWidth = this.currentConfigData[action]
            ? this.currentConfigData[action].modelData.size
            : "80%";
        this.dialogRef = this._matDialog
            .open(ModelLayoutComponent, {
            disableClose: true,
            width: modelWidth,
            panelClass: "contact-form-dialog",
            data: {
                action: action,
                title: this.headerText,
                modalIndex: this.modalIndex,
            },
        })
            .afterClosed()
            .subscribe((response) => {
            localStorage.removeItem("currentInput");
            this.messageService.sendModelCloseEvent("listView");
        });
    }
    downloadFile(reportValue, fileName) {
        const blob = new Blob([reportValue], { type: "text/csv" });
        FileSaver.saveAs(blob, fileName);
    }
    getEnabledbuttons(buttonsList) {
        let temp = {};
        this.enableButtonLayout = buttonsList && buttonsList.length ? true : false;
        this.enabledButtons = buttonsList;
    }
}
ButtonLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: "button-layout",
                template: "<div class=\"header-top ctrl-create header p-24 senlib-fixed-header\" fxlayout=\"row\" fxlayout.xs=\"column\"\r\n  fxlayoutalign=\"space-between center\">\r\n  <div class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"width:40%;\">\r\n    <h2 class=\"m-0 senlib-top-header\">\r\n      <span class=\"material-icons\" *ngIf=\"fromRouting\" (click)=\"backClick()\"\r\n        style=\"margin-right: 7px;font-size: 25px;position: relative;top:2px; color: #59588b;cursor: pointer;\">arrow_back</span>\r\n      <span class=\"material-icons\"\r\n        style=\"margin-right: 7px;font-size: 21px;position: relative;top:2px\">{{headerData.pageHeaderIcon}}</span>\r\n      {{headerData.pageHeader | translate}}\r\n    </h2>\r\n    <span class=\"senlib-top-italic\"> {{headerData.pageTagLine | translate}} </span>\r\n  </div>\r\n  <!-- <div *ngIf=\"fromRouting\" class=\"logo mb-16 mb-sm-0\" fxlayout=\"row\" fxlayoutalign=\"start center\" style=\"width:40%;\">\r\n    <button class=\"btn btn-primary\">BACK</button>\r\n  </div> -->\r\n  <!-- <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\"> -->\r\n\r\n  <!-- </div> -->\r\n  <div class=\"toolbar\" fxlayout=\"row\" fxlayoutalign=\"start center\" *ngIf=\"enableButtonLayout && buttonsConfigData\">\r\n    <!-- new button -->\r\n    <button class=\"btn btn-light-primary mr-2 margin-top-5\" (click)=\"enableFilter()\">\r\n      <span class=\"k-icon k-i-filter\"\r\n        style=\"font-size: 18px;position:relative;bottom:1px;margin-right:5px\"></span>Filter\r\n    </button>\r\n    <button *ngIf=\"currentData && currentData.enableTableSettings\" class=\"tableSettingsBtn btn btn-light-primary mr-2\"\r\n      (click)=\"select.open()\">\r\n      <span class=\"material-icons\"\r\n        style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">settings</span>Settings\r\n      <!-- <mat-icon style=\"font-size: 26px;\">settings</mat-icon> -->\r\n      <!-- <mat-form-field style=\"margin: 0px 10px 0 0;width:0\"> -->\r\n      <!-- <mat-label>Table Settings</mat-label> -->\r\n      <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n        (selectionChange)=\"updateTableSettings($event)\">\r\n        <mat-option *ngFor=\"let columnSetting of tableHeaderConfig\" [checked]=\"columnSetting.isActive\"\r\n          [value]=\"columnSetting.value\">{{ columnSetting.name | translate}}</mat-option>\r\n      </mat-select>\r\n      <!-- </mat-form-field> -->\r\n    </button>\r\n    <button class=\"btn btn-light-primary mr-2 tableSettingsBtn\" *ngFor=\"let buttonData of enabledButtons\"\r\n      [disabled]=\"buttonData.disable\" (click)=\"onClick(buttonsConfigData[buttonData.buttonId].action)\"\r\n      [ngClass]=\"(buttonsConfigData[buttonData.buttonId] && buttonsConfigData[buttonData.buttonId].style )? buttonsConfigData[buttonData.buttonId].style : ''\">\r\n      <span class=\"material-icons\" *ngIf=\"buttonsConfigData[buttonData.buttonId]\"\r\n        style=\"font-size: 18px;position:relative;top:3px;margin-right:5px\">\r\n        {{buttonsConfigData[buttonData.buttonId].icon}}</span>\r\n      <span>\r\n        {{buttonsConfigData[buttonData.buttonId] ? (buttonsConfigData[buttonData.buttonId].label | translate) : \"\" }}</span>\r\n    </button>\r\n\r\n    <!-- end  -->\r\n    <!-- SELECT COMPONENT IN HEADER FUNCTIONALITY NOT IMPLEMENTED -->\r\n    <ng-container *ngIf=\"currentData.selectFields && currentData.selectFields.length>0\">\r\n      <ng-container *ngFor=\"let selectField of currentData.selectFields\">\r\n        <mat-label *ngIf=\"selectField.showSideLabel\" class=\"mr-8\">{{selectField.label | translate}}</mat-label>\r\n        <mat-form-field class=\"full-width\" style=\"padding: 0 5px 0 0;width: 100px;\" appearance=\"outline\">\r\n          <mat-label>{{selectField.label |translate}}</mat-label>\r\n          <mat-select [formControlName]=\"selectField.name\" [required]=\"selectField.validations\">\r\n            <mat-option *ngFor=\"let item of selectField.data\" value=\"{{item[selectField.keyToShow] | translate}}\">\r\n              {{item[selectField.keyToShow] | translate}}\r\n            </mat-option>\r\n          </mat-select>\r\n        </mat-form-field>\r\n\r\n      </ng-container>\r\n    </ng-container>\r\n    <!-- <button mat-icon-button class=\"mat-warn\" (click)=\"enableFilter()\">\r\n      <span class=\"k-icon k-i-filter\" style=\"font-size: 26px;\"></span>\r\n    </button>\r\n    <button *ngIf=\"currentData && currentData.enableTableSettings\" style=\"width:75px\" mat-icon-button class=\"mat-warn\"\r\n      matTooltip=\"Table Settings\" (click)=\"select.open()\">\r\n      <mat-icon style=\"font-size: 26px;\">settings</mat-icon>\r\n      <mat-form-field style=\"margin: 0px 10px 0 0;width:0\">\r\n        <mat-label>Table Settings</mat-label>\r\n        <mat-select #select multiple style=\"width: 0px\" [(ngModel)]=\"selectedTableHeader\"\r\n          (selectionChange)=\"updateTableSettings($event)\">\r\n          <mat-option *ngFor=\"let columnSetting of tableHeaderConfig\" [checked]=\"columnSetting.isActive\"\r\n            [value]=\"columnSetting.value\">{{ columnSetting.name | translate}}</mat-option>\r\n        </mat-select>\r\n      </mat-form-field>\r\n    </button>\r\n    <button *ngFor=\"let buttonData of enabledButtons\" (click)=\"onClick(buttonsConfigData[buttonData.buttonId].action)\"\r\n      [ngClass]=\"(buttonsConfigData[buttonData.buttonId] && buttonsConfigData[buttonData.buttonId].style )? buttonsConfigData[buttonData.buttonId].style : ''\"\r\n      mat-raised-button class=\"mat-raised-button mr-4\" [disabled]=\"buttonData.disable\">\r\n      <mat-icon class=\"icon-size\" *ngIf=\"buttonsConfigData[buttonData.buttonId]\">\r\n        {{buttonsConfigData[buttonData.buttonId].icon}}</mat-icon>\r\n      {{buttonsConfigData[buttonData.buttonId] ? (buttonsConfigData[buttonData.buttonId].label | translate) : \"\" }}\r\n    </button>-->\r\n    <mat-button-toggle-group class=\"sentri-group-toggle\" [(value)]=\"defaultToggleValue\"\r\n      *ngIf=\"currentData && currentData.enableButtonToggle\">\r\n      <mat-button-toggle class=\"ui-common-lib-btn-toggle\" *ngFor=\"let toggleItem of currentData.buttonToggles;\"\r\n        (change)=\"toggleChange($event)\" [value]=\"toggleItem\">{{toggleItem.labelName}}</mat-button-toggle>\r\n    </mat-button-toggle-group>\r\n  </div>\r\n  <div fxlayout=\"row\" fxlayoutalign=\"center stretch\" style=\"margin: -19px;\" *ngIf=\"enableModelButtonLayout\">\r\n    <button class=\"modal-view-buttons\" mat-raised-button type=\"button\" color=\"accent\" (click)=\"openDialog('new')\">\r\n      <mat-icon>play_arrow</mat-icon>{{'BUTTONS.RUN_BTN_LBL' | translate}}\r\n    </button>\r\n    <button class=\"modal-view-buttons\" mat-raised-button type=\"button\" color=\"warn\" (click)=\"openDialog('new')\">\r\n      <mat-icon>schedule</mat-icon>{{'BUTTONS.SCHEDULE_BTN_LBL' | translate}}\r\n    </button>\r\n  </div>\r\n</div>\r\n",
                styles: [".ctrl-create{-webkit-box-orient:horizontal;-webkit-box-direction:normal;flex-direction:row;box-sizing:border-box;display:-webkit-box;display:flex;max-height:100%;align-content:center;-webkit-box-align:center;align-items:center;-webkit-box-pack:justify;justify-content:space-between}.mat-raised-button{border-radius:6px!important}.modal-view-buttons{margin-right:10px}.icon-size{font-size:21px}.senlib-fixed-header{background-color:#fff;margin:0 1px 30px;padding:12px 25px!important;box-shadow:0 1px 2px rgba(0,0,0,.1)}.senlib-top-header{font-weight:500}.senlib-top-italic{font-style:italic!important}.sen-card-lib{position:absolute!important;width:30%!important}.tableSettingsBtn .mat-select-arrow-wrapper{display:none!important}.btn-light-primary:disabled{color:currentColor!important}button.btn.btn-light-primary{color:#223664!important;border-color:transparent;padding:.55rem .75rem;font-size:14px;line-height:1.35;border-radius:.42rem;outline:0!important}.margin-top-5{margin-top:5px}.sentri-group-toggle{border:none!important;color:red;position:relative;top:3px}.mat-button-toggle-checked{background:#fff!important;color:gray!important;border-bottom:2px solid #4d4d88!important}.ui-common-lib-btn-toggle{border-left:none!important;outline:0}.ui-common-lib-btn-toggle:hover{background:#fff!important}.mat-button-toggle-button:focus{outline:0!important}"]
            }] }
];
/** @nocollapse */
ButtonLayoutComponent.ctorParameters = () => [
    { type: FuseTranslationLoaderService },
    { type: MatDialog },
    { type: HttpClient },
    { type: ContentService },
    { type: SnackBarService },
    { type: MessageService },
    { type: undefined, decorators: [{ type: Inject, args: ["environment",] }] },
    { type: undefined, decorators: [{ type: Inject, args: ["english",] }] }
];
ButtonLayoutComponent.propDecorators = {
    enableModelButtonLayout: [{ type: Input }],
    headerText: [{ type: Input }],
    modalIndex: [{ type: Input }],
    enableHeader: [{ type: Input }],
    fromRouting: [{ type: Input }]
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9uLWxheW91dC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiYnV0dG9uLWxheW91dC9idXR0b24tbGF5b3V0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLFNBQVMsRUFBYSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXBFLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQzVGLGtEQUFrRDtBQUVsRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUU5RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFbEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzlDLE9BQU8sS0FBSyxTQUFTLE1BQU0sWUFBWSxDQUFDO0FBRXhDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUU1RCxPQUFPLEtBQUssQ0FBQyxNQUFNLFFBQVEsQ0FBQztBQUM1QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN2QyxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFPM0MsTUFBTSxPQUFPLHFCQUFxQjtJQXNCaEMsWUFDVSw2QkFBMkQsRUFDM0QsVUFBcUIsRUFDckIsVUFBc0IsRUFDdEIsY0FBOEIsRUFDL0IsZUFBZ0MsRUFFL0IsY0FBOEIsRUFDUCxXQUFXLEVBQ2YsT0FBTztRQVIxQixrQ0FBNkIsR0FBN0IsNkJBQTZCLENBQThCO1FBQzNELGVBQVUsR0FBVixVQUFVLENBQVc7UUFDckIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDL0Isb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBRS9CLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUNQLGdCQUFXLEdBQVgsV0FBVyxDQUFBO1FBQ2YsWUFBTyxHQUFQLE9BQU8sQ0FBQTtRQTNCM0IsaUJBQVksR0FBWSxJQUFJLENBQUM7UUFHdEMsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBR3BDLHNCQUFpQixHQUFRLEVBQUUsQ0FBQztRQUNwQixnQkFBVyxHQUFHLElBQUksT0FBTyxFQUFRLENBQUM7UUFLMUMsc0JBQWlCLEdBQVEsRUFBRSxDQUFDO1FBQzVCLHdCQUFtQixHQUFRLEVBQUUsQ0FBQztRQWdCNUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzdELElBQUksQ0FBQyxjQUFjO2FBQ2hCLHNCQUFzQixFQUFFO2FBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ2pDLFNBQVMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFFBQVE7UUFDTixJQUFJLENBQUMsVUFBVTthQUNaLEdBQUcsQ0FBQyw0Q0FBNEMsQ0FBQzthQUNqRCxTQUFTLENBQUMsQ0FBQyxZQUFZLEVBQUUsRUFBRTtZQUMxQixZQUFZLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDbkUsSUFBSSxDQUFDLGlCQUFpQixHQUFHLFlBQVksQ0FBQztRQUN4QyxDQUFDLENBQUMsQ0FBQztRQUNMLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUNqQyxZQUFZLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLENBQzFDLENBQUM7UUFDRixJQUFJLENBQUMsaUJBQWlCLEdBQUcsWUFBWSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM1RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7UUFDckUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ2hCLENBQUM7SUFDRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQixDQUFDO0lBRUQsbUJBQW1CO1FBQ2pCLDBCQUEwQjtRQUMxQixJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO1FBQ3BFLElBQUksT0FBTyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDbkQsT0FBTyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzdDLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLEVBQUUsVUFBVSxJQUFJO1lBQ3ZELElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDbkIsSUFBSSxDQUFDLE9BQU87b0JBQ1YsT0FBTyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxNQUFNO3dCQUNwRSxDQUFDLENBQUMsS0FBSzt3QkFDUCxDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ1o7WUFDRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDekIsSUFBSSxZQUFZLEdBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDL0MsSUFBSSxZQUFZLEdBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFDLEVBQUMsR0FBRyxFQUFDLFlBQVksRUFBQyxDQUFDLENBQUM7Z0JBQzdELE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEVBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQy9DLGlGQUFpRjtnQkFDakYseUdBQXlHO2dCQUN2RyxJQUFJLENBQUMsT0FBTyxHQUFFLFFBQVEsSUFBSSxRQUFRLENBQUMsZ0JBQWdCLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzthQUM5RTtRQUNILENBQUMsQ0FBQyxDQUFDO0lBQ0wsQ0FBQztJQUNELE1BQU07UUFDSixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQSxJQUFJLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDcEgsSUFDRSxJQUFJLENBQUMsV0FBVztZQUNoQixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQjtZQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVM7WUFDMUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEVBQzdCO1lBQ0EsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQztZQUNuRSxJQUFJLFNBQVMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1lBQ3JFLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztZQUNyRCxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLEtBQUssRUFBRSxRQUFRLEVBQUUsQ0FBQyxDQUFDO1lBQ3RELENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7U0FDdkQ7YUFBTSxJQUNMLElBQUksQ0FBQyxXQUFXO1lBQ2hCLElBQUksQ0FBQyxXQUFXLENBQUMsa0JBQWtCO1lBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN6QixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUN0QztZQUNBLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQy9FLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ3REO2FBQU0sSUFDTCxJQUFJLENBQUMsV0FBVztZQUNoQixJQUFJLENBQUMsV0FBVyxDQUFDLG1CQUFtQjtZQUNwQyxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUNqQztZQUNBLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDO1lBQy9FLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7WUFDckUsSUFBSSxDQUFDLG1CQUFtQixHQUFHLENBQUMsQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ3REO1FBQ0QsZ0VBQWdFO1FBQ2hFLHdFQUF3RTtRQUN4RSx3RUFBd0U7UUFDeEUsMERBQTBEO1FBQzFELElBQUk7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUTtZQUN6QyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO1lBQzlCLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDUCxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztRQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHO1lBQ2hCLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVU7WUFDdkMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVztZQUN6QyxjQUFjLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1NBQ2hELENBQUM7UUFDRixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDO1FBQ3RELElBQUksQ0FBQyxrQkFBa0I7WUFDckIsSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDbkUsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixFQUFFO1lBQ3ZDLElBQUksQ0FBQyxrQkFBa0IsR0FBRSxJQUFJLENBQUE7WUFDN0IsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDNUM7UUFDRCxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBQ0QsU0FBUztRQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEIsWUFBWSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUN4QyxJQUFJLEdBQUcsR0FBRztZQUNSLEVBQUUsRUFBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVM7WUFDL0IsR0FBRyxFQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTtZQUNqQyxjQUFjLEVBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUI7U0FDeEQsQ0FBQTtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFDRCxtQkFBbUIsQ0FBQyxLQUFLO1FBQ3ZCLElBQUksY0FBYyxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUM5QyxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFVBQVUsSUFBSTtZQUMzQyxJQUFJLGNBQWMsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDdEI7aUJBQU07Z0JBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7YUFDdkI7UUFDSCxDQUFDLENBQUMsQ0FBQztRQUNILFlBQVksQ0FBQyxVQUFVLENBQUMsc0JBQXNCLENBQUMsQ0FBQztRQUNoRCxZQUFZLENBQUMsT0FBTyxDQUNsQixzQkFBc0IsRUFDdEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FDdkMsQ0FBQztRQUNGLElBQUksQ0FBQyxjQUFjLENBQUMscUJBQXFCLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVELFlBQVk7UUFDVixJQUFJLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFDRCxPQUFPLENBQUMsTUFBTTtRQUNaLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLEVBQUMsTUFBTSxDQUFDLENBQUE7UUFDL0MsSUFBSSxNQUFNLEtBQUssWUFBWSxFQUFFO1lBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDekI7YUFBTSxJQUFJLE1BQU0sS0FBSyxhQUFhLEVBQUU7WUFDbkMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN6QjthQUFNLElBQUksTUFBTSxJQUFJLFlBQVksRUFBRTtZQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQzVDLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxVQUFVLENBQUM7WUFDeEQsSUFBSSxjQUFjLEdBQUcsT0FBTyxDQUFDLFdBQVcsQ0FBQztZQUN6QyxJQUFJLE1BQU0sR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDO1lBQzVCLElBQUksbUJBQW1CLEdBQUcsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUMvQyxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1lBQ3hFLElBQUksV0FBVyxHQUFHLEVBQUUsQ0FBQztZQUNyQixDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7Z0JBQ3RDLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtvQkFDbEIsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNyQztxQkFBTTtvQkFDTCxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxlQUFlO3dCQUMzQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7d0JBQzlDLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2xDO1lBQ0gsQ0FBQyxDQUFDLENBQUM7WUFDSCxPQUFPLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxjQUFjO2lCQUNoQixpQkFBaUIsQ0FBQyxXQUFXLEVBQUUsTUFBTSxDQUFDO2lCQUN0QyxTQUFTLENBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDbEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDdEQsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNLElBQUksTUFBTSxJQUFJLFlBQVksRUFBRTtZQUNqQyxPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3RDLElBQUksU0FBUyxHQUFDLEVBQUUsQ0FBQztZQUNqQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDM0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQztnQkFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQztnQkFDekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztnQkFDcEQsU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7YUFDdkQ7aUJBQUk7Z0JBQ0YsU0FBUyxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUM7YUFDckQ7WUFDRCxJQUFJLFVBQVUsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTtpQkFDN0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUMxQixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsS0FBSyxFQUFFLFVBQVU7Z0JBQ2pCLFVBQVUsRUFBRSxxQkFBcUI7Z0JBQ2pDLElBQUksRUFBRTtvQkFDSixNQUFNLEVBQUUsWUFBWTtvQkFDcEIsU0FBUyxFQUFFLFNBQVM7aUJBQ3JCO2FBQ0YsQ0FBQztpQkFDRCxXQUFXLEVBQUU7aUJBQ2IsU0FBUyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ3RCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7U0FFTjthQUFLLElBQUcsTUFBTSxJQUFJLE1BQU0sRUFBQztZQUN4QixPQUFPLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxrQkFBa0IsQ0FBQyxDQUFDO1lBQ3RDLElBQUksU0FBUyxHQUFDLEVBQUUsQ0FBQztZQUNqQixJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtnQkFDM0IsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQztnQkFDaEQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsa0JBQWtCLENBQUMsQ0FBQztnQkFDMUMsSUFBSSxTQUFTLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsQ0FBQztnQkFDekQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztnQkFDcEQsU0FBUyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUM7YUFDdkQ7aUJBQUk7Z0JBQ0YsU0FBUyxHQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQUM7YUFDckQ7WUFDRCxJQUFJLFVBQVUsR0FBRyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbkMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVTtpQkFDN0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFO2dCQUMxQixZQUFZLEVBQUUsSUFBSTtnQkFDbEIsS0FBSyxFQUFFLFVBQVU7Z0JBQ2pCLFVBQVUsRUFBRSxxQkFBcUI7Z0JBQ2pDLElBQUksRUFBRTtvQkFDSixNQUFNLEVBQUUsUUFBUTtvQkFDaEIsU0FBUyxFQUFFLFNBQVM7aUJBQ3JCO2FBQ0YsQ0FBQztpQkFDRCxXQUFXLEVBQUU7aUJBQ2IsU0FBUyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ3RCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDMUMsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0wsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQ3pCO0lBQ0gsQ0FBQztJQUVELFVBQVUsQ0FBQyxNQUFNO1FBQ2YsT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUM1QyxJQUFJLGdCQUFnQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLElBQUksVUFBVSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRCxnQkFBZ0IsQ0FBQyxZQUFZLENBQUMsR0FBRyxVQUFVLENBQUMsVUFBVSxDQUFDO1FBQ3ZELElBQUksY0FBYyxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDO1FBQzFELElBQUksTUFBTSxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO1FBQzdDLElBQUksbUJBQW1CLEdBQUcsVUFBVSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUM7UUFDaEUsSUFBSSxXQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ3JCLElBQUksSUFBSSxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLFVBQVUsQ0FBQztRQUNmLE9BQU8sQ0FBQyxHQUFHLENBQUMsOEJBQThCLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUM5RCxDQUFDLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxVQUFVLElBQUk7WUFDdEMsZ0RBQWdEO1lBQ2hELG1EQUFtRDtZQUNuRCxvQ0FBb0M7WUFDcEMsSUFBSSxRQUFRLENBQUM7WUFDYixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3hCLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzthQUN2RTtpQkFBTSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ3pCLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNyQztpQkFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7Z0JBQzdCLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQzthQUN6RTtpQkFBTTtnQkFDTCxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLGdCQUFnQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN2RDtRQUNILENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxVQUFVLENBQUMsYUFBYSxDQUFDLGNBQWMsRUFBRTtZQUMzQyw2QkFBNkI7WUFDN0IsT0FBTyxDQUFDLEdBQUcsQ0FBQywyQkFBMkIsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUN0RCxJQUFJLG1CQUFtQixHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1lBQ2hFLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQyxTQUFTLENBQzlELENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLElBQUksR0FBRyxDQUFDLE1BQU0sSUFBSSxHQUFHLEVBQUU7b0JBQ3JCLFlBQVksQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsQ0FBQztvQkFDN0MsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDckQ7WUFDSCxDQUFDLEVBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDUixJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FDMUIsSUFBSSxDQUFDLDZCQUE2QixDQUFDLE9BQU8sQ0FDeEMsbUJBQW1CLENBQUMsS0FBSyxDQUMxQixDQUNGLENBQUM7WUFDSixDQUFDLENBQ0YsQ0FBQztTQUNIO2FBQU07WUFDTCxJQUFJLENBQUMsY0FBYztpQkFDaEIsaUJBQWlCLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQztpQkFDdEMsU0FBUyxDQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7Z0JBQ2xCLElBQUksUUFBUSxHQUFHLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3ZELFVBQVUsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQzFDLENBQUM7Z0JBQ0YsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQ3RCLElBQUksQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQ3hDLG1CQUFtQixDQUFDLE9BQU8sQ0FDNUIsQ0FDRixDQUFDO2dCQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQ2YsSUFBSSxFQUNKLFFBQVEsR0FBRyxVQUFVLENBQUMsYUFBYSxDQUFDLGdCQUFnQixDQUNyRCxDQUFDO1lBQ0osQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFFRCxVQUFVLENBQUMsTUFBTTtRQUNmLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztRQUNsQyxJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztJQUM3QixDQUFDO0lBRUQsWUFBWSxDQUFDLEtBQUs7UUFDaEIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNuQixJQUFJLFdBQVcsR0FBRyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQzdELFlBQVksQ0FBQyxPQUFPLENBQUMsc0JBQXNCLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1FBQzFFLDJDQUEyQztRQUMzQywwQ0FBMEM7UUFDMUMsc0NBQXNDO1FBQ3RDLGdDQUFnQztRQUNoQyxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBQ0QsVUFBVSxDQUFDLE1BQU07UUFDZixJQUFJLFVBQVUsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDO1lBQzdDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxDQUFDLElBQUk7WUFDL0MsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVU7YUFDN0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQzFCLFlBQVksRUFBRSxJQUFJO1lBQ2xCLEtBQUssRUFBRSxVQUFVO1lBQ2pCLFVBQVUsRUFBRSxxQkFBcUI7WUFDakMsSUFBSSxFQUFFO2dCQUNKLE1BQU0sRUFBRSxNQUFNO2dCQUNkLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDdEIsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVO2FBQzVCO1NBQ0YsQ0FBQzthQUNELFdBQVcsRUFBRTthQUNiLFNBQVMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ3RCLFlBQVksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFDRCxZQUFZLENBQUMsV0FBVyxFQUFFLFFBQVE7UUFDaEMsTUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxXQUFXLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQzNELFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxpQkFBaUIsQ0FBQyxXQUFXO1FBQzNCLElBQUksSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNkLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxXQUFXLElBQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7UUFDM0UsSUFBSSxDQUFDLGNBQWMsR0FBRyxXQUFXLENBQUM7SUFDcEMsQ0FBQzs7O1lBcFlGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsdzJOQUE2Qzs7YUFFOUM7Ozs7WUFyQlEsNEJBQTRCO1lBTzVCLFNBQVM7WUFGVCxVQUFVO1lBS1YsY0FBYztZQWJkLGVBQWU7WUFnQmYsY0FBYzs0Q0F1Q2xCLE1BQU0sU0FBQyxhQUFhOzRDQUNwQixNQUFNLFNBQUMsU0FBUzs7O3NDQTlCbEIsS0FBSzt5QkFDTCxLQUFLO3lCQUNMLEtBQUs7MkJBQ0wsS0FBSzswQkFDTCxLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgU25hY2tCYXJTZXJ2aWNlIH0gZnJvbSBcIi4vLi4vc2hhcmVkL3NuYWNrYmFyLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3Q2hpbGQsIElucHV0LCBJbmplY3QgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xyXG5cclxuaW1wb3J0IHsgRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSB9IGZyb20gXCIuLi9AZnVzZS9zZXJ2aWNlcy90cmFuc2xhdGlvbi1sb2FkZXIuc2VydmljZVwiO1xyXG4vLyBpbXBvcnQgeyBsb2NhbGUgYXMgZW5nbGlzaCB9IGZyb20gXCIuLi9pMThuL2VuXCI7XHJcblxyXG5pbXBvcnQgeyBNb2RlbExheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi9tb2RlbC1sYXlvdXQvbW9kZWwtbGF5b3V0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBUYWJsZUxheW91dENvbXBvbmVudCB9IGZyb20gXCIuLi90YWJsZS1sYXlvdXQvdGFibGUtbGF5b3V0LmNvbXBvbmVudFwiO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSBcIkBhbmd1bGFyL2NvbW1vbi9odHRwXCI7XHJcblxyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWxcIjtcclxuaW1wb3J0ICogYXMgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcblxyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gXCIuLi9jb250ZW50L2NvbnRlbnQuc2VydmljZVwiO1xyXG5cclxuaW1wb3J0ICogYXMgXyBmcm9tIFwibG9kYXNoXCI7XHJcbmltcG9ydCB7IE1lc3NhZ2VTZXJ2aWNlIH0gZnJvbSBcIi4uL19zZXJ2aWNlcy9tZXNzYWdlLnNlcnZpY2VcIjtcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gXCJyeGpzL1N1YmplY3RcIjtcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSBcInJ4anMvb3BlcmF0b3JzXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICBzZWxlY3RvcjogXCJidXR0b24tbGF5b3V0XCIsXHJcbiAgdGVtcGxhdGVVcmw6IFwiLi9idXR0b24tbGF5b3V0LmNvbXBvbmVudC5odG1sXCIsXHJcbiAgc3R5bGVVcmxzOiBbXCIuL2J1dHRvbi1sYXlvdXQuY29tcG9uZW50LnNjc3NcIl0sXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCdXR0b25MYXlvdXRDb21wb25lbnQge1xyXG4gIEBJbnB1dCgpIGVuYWJsZU1vZGVsQnV0dG9uTGF5b3V0OiBhbnk7XHJcbiAgQElucHV0KCkgaGVhZGVyVGV4dDogc3RyaW5nO1xyXG4gIEBJbnB1dCgpIG1vZGFsSW5kZXg6IGFueTtcclxuICBASW5wdXQoKSBlbmFibGVIZWFkZXI6IGJvb2xlYW4gPSB0cnVlO1xyXG4gIEBJbnB1dCgpIGZyb21Sb3V0aW5nIDogYW55O1xyXG4gIGhlYWRlckRhdGE6IGFueTtcclxuICBlbmFibGVCdXR0b25MYXlvdXQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICBlbmFibGVkQnV0dG9uczogYW55O1xyXG4gIGRpYWxvZ1JlZjogYW55O1xyXG4gIGJ1dHRvbnNDb25maWdEYXRhOiBhbnkgPSB7fTtcclxuICBwcml2YXRlIHVuc3Vic2NyaWJlID0gbmV3IFN1YmplY3Q8dm9pZD4oKTtcclxuICBkZWZhdWx0RGF0YXNvdXJjZTogYW55O1xyXG4gIGN1cnJlbnREYXRhOiBhbnk7XHJcbiAgY3VycmVudENvbmZpZ0RhdGE6IGFueTtcclxuICBkZWZhdWx0VG9nZ2xlVmFsdWU6IGFueTtcclxuICB0YWJsZUhlYWRlckNvbmZpZzogYW55ID0gW107XHJcbiAgc2VsZWN0ZWRUYWJsZUhlYWRlcjogYW55ID0gW107XHJcbiAgLy8gZW5hYmxlVGFibGVMYXlvdXQgOiBib29sZWFuID0gZmFsc2U7XHJcbiAgZm9ybVZhbHVlczogYW55O1xyXG4gIGN1cnJlbnRUYWJsZUxvYWQ6IGFueTtcclxuICB1c2VyRGF0YTogYW55O1xyXG4gIGNvbnN0cnVjdG9yKFxyXG4gICAgcHJpdmF0ZSBfZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZTogRnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZSxcclxuICAgIHByaXZhdGUgX21hdERpYWxvZzogTWF0RGlhbG9nLFxyXG4gICAgcHJpdmF0ZSBodHRwQ2xpZW50OiBIdHRwQ2xpZW50LFxyXG4gICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICBwdWJsaWMgc25hY2tCYXJTZXJ2aWNlOiBTbmFja0JhclNlcnZpY2UsXHJcblxyXG4gICAgcHJpdmF0ZSBtZXNzYWdlU2VydmljZTogTWVzc2FnZVNlcnZpY2UsXHJcbiAgICBASW5qZWN0KFwiZW52aXJvbm1lbnRcIikgcHJpdmF0ZSBlbnZpcm9ubWVudCxcclxuICAgIEBJbmplY3QoXCJlbmdsaXNoXCIpIHByaXZhdGUgZW5nbGlzaFxyXG4gICkge1xyXG4gICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5sb2FkVHJhbnNsYXRpb25zKGVuZ2xpc2gpO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZVxyXG4gICAgICAuZ2V0QnV0dG9uRW5hYmxlTWVzc2FnZSgpXHJcbiAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLnVuc3Vic2NyaWJlKSlcclxuICAgICAgLnN1YnNjcmliZSgobWVzc2FnZSkgPT4ge1xyXG4gICAgICAgIHRoaXMucmVpbml0aWFsaXplQnV0dG9ucygpO1xyXG4gICAgICB9KTtcclxuICB9XHJcblxyXG4gIG5nT25Jbml0KCkge1xyXG4gICAgdGhpcy5odHRwQ2xpZW50XHJcbiAgICAgIC5nZXQoXCJhc3NldHMvY29uZmlnRmlsZXMvYnV0dG9uc19jb25maWdGaWxlLmpzb25cIilcclxuICAgICAgLnN1YnNjcmliZSgoZmlsZVJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJidXR0b25Db25maWdcIiwgSlNPTi5zdHJpbmdpZnkoZmlsZVJlc3BvbnNlKSk7XHJcbiAgICAgICAgdGhpcy5idXR0b25zQ29uZmlnRGF0YSA9IGZpbGVSZXNwb25zZTtcclxuICAgICAgfSk7XHJcbiAgICB0aGlzLmN1cnJlbnRDb25maWdEYXRhID0gSlNPTi5wYXJzZShcclxuICAgICAgbG9jYWxTdG9yYWdlLmdldEl0ZW0oXCJjdXJyZW50Q29uZmlnRGF0YVwiKVxyXG4gICAgKTtcclxuICAgIHRoaXMuZGVmYXVsdERhdGFzb3VyY2UgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImRhdGFzb3VyY2VcIik7XHJcbiAgICB0aGlzLnVzZXJEYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRMb2dpblVzZXJcIikpO1xyXG4gICAgdGhpcy5vbkxvYWQoKTtcclxuICB9XHJcbiAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICB0aGlzLnVuc3Vic2NyaWJlLm5leHQoKTtcclxuICB9XHJcblxyXG4gIHJlaW5pdGlhbGl6ZUJ1dHRvbnMoKSB7XHJcbiAgICAvLyBpcyBhbG93IGRhdGFzb3VjZSBjaGVja1xyXG4gICAgbGV0IHVzZXJEYXRhID0gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRMb2dpblVzZXJcIikpO1xyXG4gICAgbGV0IHRlbXBPYmogPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRlbXBPYmogPSB0ZW1wT2JqID8gSlNPTi5wYXJzZSh0ZW1wT2JqKSA6IHt9O1xyXG4gICAgXy5mb3JFYWNoKHRoaXMuY3VycmVudERhdGEuZW5hYmxlZEJ1dHRvbnMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmIChpdGVtLmtleVRvQ2hlY2spIHtcclxuICAgICAgICBpdGVtLmRpc2FibGUgPVxyXG4gICAgICAgICAgdGVtcE9iaiAmJiB0ZW1wT2JqW2l0ZW0ua2V5VG9DaGVja10gJiYgdGVtcE9ialtpdGVtLmtleVRvQ2hlY2tdLmxlbmd0aFxyXG4gICAgICAgICAgICA/IGZhbHNlXHJcbiAgICAgICAgICAgIDogdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoaXRlbS5jaGVja2ltcG9ydEFjY2Vzcykge1xyXG4gICAgICAgICBsZXQgY3VycmVudFJlYWxtPWxvY2FsU3RvcmFnZS5nZXRJdGVtKFwicmVhbG1cIik7XHJcbiAgICAgICAgIGxldCBtYXRjaGVkUmVhbG09Xy5maWx0ZXIodXNlckRhdGEucmVhbG0se19pZDpjdXJyZW50UmVhbG19KTtcclxuICAgICAgICAgY29uc29sZS5sb2coXCI+Pj4gbWF0Y2hlZHJlYWxtXCIsbWF0Y2hlZFJlYWxtKTtcclxuICAgICAgIC8vICBjb25zb2xlLmxvZyhcIj4+PiBjdXJyZW50IFJlYWxtIGFjY2VzcyBcIixtYXRjaGVkUmVhbG1bMF1bJ2FsbG93X2RhdGFzb3VyY2UnXSk7XHJcbiAgICAgICAvLyAgaXRlbS5kaXNhYmxlID0obWF0Y2hlZFJlYWxtLmxlbmd0aD4wICYmIG1hdGNoZWRSZWFsbVswXVsnYWxsb3dfZGF0YXNvdXJjZSddID09IFwiWUVTXCIpID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICAgICBpdGVtLmRpc2FibGUgPXVzZXJEYXRhICYmIHVzZXJEYXRhLmFsbG93X2RhdGFzb3VyY2UgPT0gXCJZRVNcIiA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgfVxyXG4gIG9uTG9hZCgpIHtcclxuICAgIHRoaXMuY3VycmVudERhdGEgPSB0aGlzLmZyb21Sb3V0aW5nID8gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVsncGFnZVJvdXRpbmdWaWV3J10gOnRoaXMuY3VycmVudENvbmZpZ0RhdGFbXCJsaXN0Vmlld1wiXTtcclxuICAgIGlmIChcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YSAmJlxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZVRhYmxlU2V0dGluZ3MgJiZcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YS50YWJsZURhdGEgJiZcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YS50YWJsZURhdGFbMF1cclxuICAgICkge1xyXG4gICAgICB0aGlzLnRhYmxlSGVhZGVyQ29uZmlnID0gdGhpcy5jdXJyZW50RGF0YS50YWJsZURhdGFbMF0udGFibGVIZWFkZXI7XHJcbiAgICAgIGxldCB0ZW1wQXJyYXkgPSBfLmZpbHRlcih0aGlzLnRhYmxlSGVhZGVyQ29uZmlnLCB7IGlzQWN0aXZlOiB0cnVlIH0pO1xyXG4gICAgICB0aGlzLnNlbGVjdGVkVGFibGVIZWFkZXIgPSBfLm1hcCh0ZW1wQXJyYXksIFwidmFsdWVcIik7XHJcbiAgICAgIF8ucmVtb3ZlKHRoaXMudGFibGVIZWFkZXJDb25maWcsIHsgdmFsdWU6IFwic2VsZWN0XCIgfSk7XHJcbiAgICAgIF8ucmVtb3ZlKHRoaXMudGFibGVIZWFkZXJDb25maWcsIHsgdmFsdWU6IFwiYWN0aW9uXCIgfSk7XHJcbiAgICB9IGVsc2UgaWYgKFxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEuZW5hYmxlQnV0dG9uVG9nZ2xlICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEuZm9ybURhdGEgJiZcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YS5mb3JtRGF0YVswXSAmJlxyXG4gICAgICB0aGlzLmN1cnJlbnREYXRhLmZvcm1EYXRhWzBdLnRhYmxlRGF0YVxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMudGFibGVIZWFkZXJDb25maWcgPSB0aGlzLmN1cnJlbnREYXRhLmZvcm1EYXRhWzBdLnRhYmxlRGF0YVswXS50YWJsZUhlYWRlcjtcclxuICAgICAgbGV0IHRlbXBBcnJheSA9IF8uZmlsdGVyKHRoaXMudGFibGVIZWFkZXJDb25maWcsIHsgaXNBY3RpdmU6IHRydWUgfSk7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlciA9IF8ubWFwKHRlbXBBcnJheSwgXCJ2YWx1ZVwiKTtcclxuICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEgJiZcclxuICAgICAgdGhpcy5jdXJyZW50RGF0YS5lbmFibGVUYWJsZVNldHRpbmdzICYmXHJcbiAgICAgIHRoaXMuY3VycmVudERhdGEuZW5hYmxlRm9ybUxheW91dFxyXG4gICAgKSB7XHJcbiAgICAgIHRoaXMudGFibGVIZWFkZXJDb25maWcgPSB0aGlzLmN1cnJlbnREYXRhLmZvcm1EYXRhWzBdLnRhYmxlRGF0YVswXS50YWJsZUhlYWRlcjtcclxuICAgICAgbGV0IHRlbXBBcnJheSA9IF8uZmlsdGVyKHRoaXMudGFibGVIZWFkZXJDb25maWcsIHsgaXNBY3RpdmU6IHRydWUgfSk7XHJcbiAgICAgIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlciA9IF8ubWFwKHRlbXBBcnJheSwgXCJ2YWx1ZVwiKTtcclxuICAgIH1cclxuICAgIC8vIGlmKHRoaXMuY3VycmVudERhdGEgJiYgdGhpcy5jdXJyZW50RGF0YS5lbmFibGVUYWJsZVNldHRpbmdzKXtcclxuICAgIC8vICAgdGhpcy50YWJsZUhlYWRlckNvbmZpZyA9IHRoaXMuY3VycmVudERhdGEudGFibGVEYXRhWzBdLnRhYmxlSGVhZGVyO1xyXG4gICAgLy8gICBsZXQgdGVtcEFycmF5ID0gXy5maWx0ZXIodGhpcy50YWJsZUhlYWRlckNvbmZpZywge2lzQWN0aXZlIDogdHJ1ZX0pXHJcbiAgICAvLyAgIHRoaXMuc2VsZWN0ZWRUYWJsZUhlYWRlciA9IF8ubWFwKHRlbXBBcnJheSwgJ3ZhbHVlJyk7XHJcbiAgICAvLyB9XHJcbiAgICBjb25zb2xlLmxvZyh0aGlzLmN1cnJlbnREYXRhLCBcIi4uLi4uLi5DVVJSRU5UXCIpO1xyXG4gICAgdGhpcy5mb3JtVmFsdWVzID0gdGhpcy5jdXJyZW50RGF0YS5mb3JtRGF0YVxyXG4gICAgICA/IHRoaXMuY3VycmVudERhdGEuZm9ybURhdGFbMF1cclxuICAgICAgOiB7fTtcclxuICAgIGNvbnNvbGUubG9nKHRoaXMuZm9ybVZhbHVlcywgXCIuLi4uLmZvcm1WYWx1ZXNcIik7XHJcbiAgICB0aGlzLmhlYWRlckRhdGEgPSB7XHJcbiAgICAgIHBhZ2VIZWFkZXI6IHRoaXMuY3VycmVudERhdGEucGFnZUhlYWRlcixcclxuICAgICAgcGFnZVRhZ0xpbmU6IHRoaXMuY3VycmVudERhdGEucGFnZVRhZ0xpbmUsXHJcbiAgICAgIHBhZ2VIZWFkZXJJY29uOiB0aGlzLmN1cnJlbnREYXRhLnBhZ2VIZWFkZXJJY29uLFxyXG4gICAgfTtcclxuICAgIHRoaXMuZW5hYmxlZEJ1dHRvbnMgPSB0aGlzLmN1cnJlbnREYXRhLmVuYWJsZWRCdXR0b25zO1xyXG4gICAgdGhpcy5lbmFibGVCdXR0b25MYXlvdXQgPVxyXG4gICAgICB0aGlzLmVuYWJsZWRCdXR0b25zICYmIHRoaXMuZW5hYmxlZEJ1dHRvbnMubGVuZ3RoID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgaWYgKHRoaXMuY3VycmVudERhdGEuZW5hYmxlQnV0dG9uVG9nZ2xlKSB7XHJcbiAgICAgIHRoaXMuZW5hYmxlQnV0dG9uTGF5b3V0ID10cnVlXHJcbiAgICAgIHRoaXMuZGVmYXVsdFRvZ2dsZVZhbHVlID0gdGhpcy5jdXJyZW50RGF0YS5idXR0b25Ub2dnbGVzWzBdO1xyXG4gICAgICB0aGlzLnRvZ2dsZUNoYW5nZSh0aGlzLmRlZmF1bHRUb2dnbGVWYWx1ZSk7XHJcbiAgICB9XHJcbiAgICB0aGlzLnJlaW5pdGlhbGl6ZUJ1dHRvbnMoKTsgXHJcbiAgfVxyXG4gIGJhY2tDbGljaygpe1xyXG4gICAgY29uc29sZS5sb2coXCJCQUNLXCIpO1xyXG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICBsZXQgb2JqID0ge1xyXG4gICAgICBpZCA6IHRoaXMuY3VycmVudERhdGEuYmFja05hdklkLFxyXG4gICAgICB1cmwgOiB0aGlzLmN1cnJlbnREYXRhLmJhY2tOYXZVcmwsXHJcbiAgICAgIGNvbmZpZ0ZpbGVQYXRoIDogdGhpcy5jdXJyZW50RGF0YS5iYWNrTmF2Q29uZmlnRmlsZVBhdGhcclxuICAgIH1cclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZFJvdXRpbmcob2JqKTtcclxuICB9XHJcbiAgdXBkYXRlVGFibGVTZXR0aW5ncyhldmVudCkge1xyXG4gICAgbGV0IHNlbGVjdGVkSGVhZGVyID0gdGhpcy5zZWxlY3RlZFRhYmxlSGVhZGVyO1xyXG4gICAgdGhpcy50YWJsZUhlYWRlckNvbmZpZy5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIGlmIChzZWxlY3RlZEhlYWRlci5pbmRleE9mKGl0ZW0udmFsdWUpID49IDApIHtcclxuICAgICAgICBpdGVtLmlzQWN0aXZlID0gdHJ1ZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBpdGVtLmlzQWN0aXZlID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG4gICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJzZWxlY3RlZFRhYmxlSGVhZGVyc1wiKTtcclxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKFxyXG4gICAgICBcInNlbGVjdGVkVGFibGVIZWFkZXJzXCIsXHJcbiAgICAgIEpTT04uc3RyaW5naWZ5KHRoaXMudGFibGVIZWFkZXJDb25maWcpXHJcbiAgICApO1xyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kVGFibGVIZWFkZXJVcGRhdGUoXCJ1cGRhdGVcIik7XHJcbiAgfVxyXG5cclxuICBlbmFibGVGaWx0ZXIoKSB7XHJcbiAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRUYWJsZUhlYWRlclVwZGF0ZShcImZpbHRlclwiKTtcclxuICB9XHJcbiAgb25DbGljayhhY3Rpb24pIHtcclxuICAgIGNvbnNvbGUubG9nKFwiPj4gb25DbGljayBFeHBvcnQgYWN0aW9uIFwiLGFjdGlvbilcclxuICAgIGlmIChhY3Rpb24gPT09IFwiZXhwb3J0RGF0YVwiKSB7XHJcbiAgICAgIHRoaXMuZXhwb3J0RGF0YShhY3Rpb24pO1xyXG4gICAgfSBlbHNlIGlmIChhY3Rpb24gPT09IFwicmVmcmVzaFBhZ2VcIikge1xyXG4gICAgICB0aGlzLnJlbG9hZERhdGEoYWN0aW9uKTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwicnVuQW5hbHl6ZVwiKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMuY3VycmVudENvbmZpZ0RhdGFbYWN0aW9uXSk7XHJcbiAgICAgIGxldCBydW5EYXRhID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLnJ1blJlcXVlc3Q7XHJcbiAgICAgIGxldCByZXF1ZXN0RGV0YWlscyA9IHJ1bkRhdGEucmVxdWVzdERhdGE7XHJcbiAgICAgIGxldCBhcGlVcmwgPSBydW5EYXRhLmFwaVVybDtcclxuICAgICAgbGV0IHRvYXN0TWVzc2FnZURldGFpbHMgPSBydW5EYXRhLnRvYXN0TWVzc2FnZTtcclxuICAgICAgbGV0IGN1cnJlbnRJbnB1dERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpKTtcclxuICAgICAgbGV0IHJlcXVlc3REYXRhID0ge307XHJcbiAgICAgIF8uZm9yRWFjaChyZXF1ZXN0RGV0YWlscywgZnVuY3Rpb24gKGl0ZW0pIHtcclxuICAgICAgICBpZiAoaXRlbS5pc0RlZmF1bHQpIHtcclxuICAgICAgICAgIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLnZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gaXRlbS5jb252ZXJ0VG9TdHJpbmdcclxuICAgICAgICAgICAgPyBKU09OLnN0cmluZ2lmeShjdXJyZW50SW5wdXREYXRhW2l0ZW0udmFsdWVdKVxyXG4gICAgICAgICAgICA6IGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuICAgICAgY29uc29sZS5sb2cocmVxdWVzdERhdGEsIFwiLi4uLnJlcXVlc3REYXRhXCIpO1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEV4cG9ydFJlc3BvbnNlKHJlcXVlc3REYXRhLCBhcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgdGhpcy5zbmFja0JhclNlcnZpY2UuYWRkKFxyXG4gICAgICAgICAgICB0aGlzLl9mdXNlVHJhbnNsYXRpb25Mb2FkZXJTZXJ2aWNlLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgdG9hc3RNZXNzYWdlRGV0YWlscy5zdWNjZXNzXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICk7XHJcbiAgICAgICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgICB9KTtcclxuICAgIH0gZWxzZSBpZiAoYWN0aW9uID09IFwiZXhwb3J0Vmlld1wiKSB7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiLi4uLi4uLi4uLi4uVEhJU1wiKTtcclxuICAgICAgbGV0IG1vZGVsRGF0YT17fTtcclxuICAgICAgaWYgKHRoaXMuZGVmYXVsdFRvZ2dsZVZhbHVlKSB7XHJcbiAgICAgICAgbGV0IHRvZ2dsZUlkID0gdGhpcy5kZWZhdWx0VG9nZ2xlVmFsdWUudG9nZ2xlSWQ7XHJcbiAgICAgICAgY29uc29sZS5sb2codG9nZ2xlSWQsIFwiPj4+Pj50b2dnbGVJZD4+PlwiKTtcclxuICAgICAgICBsZXQgbW9kZWxMaXN0ID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLm1vZGVsRGF0YTtcclxuICAgICAgICBjb25zb2xlLmxvZyhtb2RlbExpc3QsIFwiLi4uLi5tb2RlbExpc3Q+Pj5tb2RlbExpc3RcIik7XHJcbiAgICAgICAgIG1vZGVsRGF0YSA9IF8uZmluZChtb2RlbExpc3QsIHsgbW9kZWxJZDogdG9nZ2xlSWQgfSk7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgICBtb2RlbERhdGE9dGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLm1vZGVsRGF0YTtcclxuICAgICAgfVxyXG4gICAgICBsZXQgbW9kZWxXaWR0aCA9IG1vZGVsRGF0YVtcInNpemVcIl07XHJcbiAgICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nXHJcbiAgICAgICAgLm9wZW4oTW9kZWxMYXlvdXRDb21wb25lbnQsIHtcclxuICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgICAgICAgcGFuZWxDbGFzczogXCJjb250YWN0LWZvcm0tZGlhbG9nXCIsXHJcbiAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgIGFjdGlvbjogXCJleHBvcnRWaWV3XCIsXHJcbiAgICAgICAgICAgIG1vZGVsRGF0YTogbW9kZWxEYXRhLFxyXG4gICAgICAgICAgfSxcclxuICAgICAgICB9KVxyXG4gICAgICAgIC5hZnRlckNsb3NlZCgpXHJcbiAgICAgICAgLnN1YnNjcmliZSgocmVzcG9uc2UpID0+IHtcclxuICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1lbHNlIGlmKGFjdGlvbiA9PSBcImVkaXRcIil7XHJcbiAgICAgIGNvbnNvbGUubG9nKHRoaXMsIFwiLi4uLi4uLi4uLi4uVEhJU1wiKTtcclxuICAgICAgbGV0IG1vZGVsRGF0YT17fTtcclxuICAgICAgaWYgKHRoaXMuZGVmYXVsdFRvZ2dsZVZhbHVlKSB7XHJcbiAgICAgICAgbGV0IHRvZ2dsZUlkID0gdGhpcy5kZWZhdWx0VG9nZ2xlVmFsdWUudG9nZ2xlSWQ7XHJcbiAgICAgICAgY29uc29sZS5sb2codG9nZ2xlSWQsIFwiPj4+Pj50b2dnbGVJZD4+PlwiKTtcclxuICAgICAgICBsZXQgbW9kZWxMaXN0ID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLm1vZGVsRGF0YTtcclxuICAgICAgICBjb25zb2xlLmxvZyhtb2RlbExpc3QsIFwiLi4uLi5tb2RlbExpc3Q+Pj5tb2RlbExpc3RcIik7XHJcbiAgICAgICAgIG1vZGVsRGF0YSA9IF8uZmluZChtb2RlbExpc3QsIHsgbW9kZWxJZDogdG9nZ2xlSWQgfSk7XHJcbiAgICAgIH1lbHNle1xyXG4gICAgICAgICBtb2RlbERhdGE9dGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLm1vZGVsRGF0YTtcclxuICAgICAgfVxyXG4gICAgICBsZXQgbW9kZWxXaWR0aCA9IG1vZGVsRGF0YVtcInNpemVcIl07XHJcbiAgICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nXHJcbiAgICAgICAgLm9wZW4oTW9kZWxMYXlvdXRDb21wb25lbnQsIHtcclxuICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgICAgICAgcGFuZWxDbGFzczogXCJjb250YWN0LWZvcm0tZGlhbG9nXCIsXHJcbiAgICAgICAgICBkYXRhOiB7XHJcbiAgICAgICAgICAgIGFjdGlvbjogXCJ1cGRhdGVcIixcclxuICAgICAgICAgICAgbW9kZWxEYXRhOiBtb2RlbERhdGEsXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLmFmdGVyQ2xvc2VkKClcclxuICAgICAgICAuc3Vic2NyaWJlKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgbG9jYWxTdG9yYWdlLnJlbW92ZUl0ZW0oXCJjdXJyZW50SW5wdXRcIik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoYWN0aW9uKTtcclxuICAgICAgdGhpcy5vcGVuRGlhbG9nKGFjdGlvbik7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICBleHBvcnREYXRhKGFjdGlvbikge1xyXG4gICAgY29uc29sZS5sb2coXCI+Pj4+Pj4+PiBFeHBvcnREYXRhIFwiLCBhY3Rpb24pO1xyXG4gICAgbGV0IGN1cnJlbnRJbnB1dERhdGEgPSBKU09OLnBhcnNlKGxvY2FsU3RvcmFnZS5nZXRJdGVtKFwiY3VycmVudElucHV0XCIpKTtcclxuICAgIGxldCBleHBvcnREYXRhID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dO1xyXG4gICAgY3VycmVudElucHV0RGF0YVtcImV4cG9ydFR5cGVcIl0gPSBleHBvcnREYXRhLmV4cG9ydFR5cGU7XHJcbiAgICBsZXQgcmVxdWVzdERldGFpbHMgPSBleHBvcnREYXRhLmV4cG9ydFJlcXVlc3QucmVxdWVzdERhdGE7XHJcbiAgICBsZXQgYXBpVXJsID0gZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LmFwaVVybDtcclxuICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LnRvYXN0TWVzc2FnZTtcclxuICAgIGxldCByZXF1ZXN0RGF0YSA9IHt9O1xyXG4gICAgdmFyIHNlbGYgPSB0aGlzO1xyXG4gICAgbGV0IHJlcG9ydFRlbXA7XHJcbiAgICBjb25zb2xlLmxvZyhcIj4+Pj4+Pj4+Pj4gY3VycmVudElucHV0RGF0YSBcIiwgY3VycmVudElucHV0RGF0YSk7XHJcbiAgICBfLmZvckVhY2gocmVxdWVzdERldGFpbHMsIGZ1bmN0aW9uIChpdGVtKSB7XHJcbiAgICAgIC8vIHJlcXVlc3REYXRhW2l0ZW0ubmFtZV0gPSBpdGVtLmNvbnZlcnRUb1N0cmluZ1xyXG4gICAgICAvLyAgID8gSlNPTi5zdHJpbmdpZnkoY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXSlcclxuICAgICAgLy8gICA6IGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV07XHJcbiAgICAgIGxldCB0ZW1wRGF0YTtcclxuICAgICAgaWYgKGl0ZW0uY29udmVydFRvU3RyaW5nKSB7XHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IEpTT04uc3RyaW5naWZ5KGN1cnJlbnRJbnB1dERhdGFbaXRlbS52YWx1ZV0pO1xyXG4gICAgICB9IGVsc2UgaWYgKGl0ZW0uaXNEZWZhdWx0KSB7XHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IGl0ZW0udmFsdWU7XHJcbiAgICAgIH0gZWxzZSBpZiAoaXRlbS5mcm9tTG9naW5EYXRhKSB7XHJcbiAgICAgICAgcmVxdWVzdERhdGFbaXRlbS5uYW1lXSA9IHNlbGYudXNlckRhdGEgPyBzZWxmLnVzZXJEYXRhW2l0ZW0udmFsdWVdIDogXCJcIjtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXF1ZXN0RGF0YVtpdGVtLm5hbWVdID0gY3VycmVudElucHV0RGF0YVtpdGVtLnZhbHVlXTtcclxuICAgICAgfVxyXG4gICAgfSk7XHJcbiAgICBpZiAoZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LmlzUmVwb3J0Q3JlYXRlKSB7XHJcbiAgICAgIC8vIGJ1dHRvbiBiYXNlZCByZXBvcnQgY3JlYXRlXHJcbiAgICAgIGNvbnNvbGUubG9nKFwiPj4+Pj4+Pj4+IHJlcXVlc3REZXRhaWxzIFwiLCByZXF1ZXN0RGF0YSk7XHJcbiAgICAgIGxldCB0b2FzdE1lc3NhZ2VEZXRhaWxzID0gZXhwb3J0RGF0YS5leHBvcnRSZXF1ZXN0LnRvYXN0TWVzc2FnZTtcclxuICAgICAgc2VsZi5jb250ZW50U2VydmljZS5jcmVhdGVSZXF1ZXN0KHJlcXVlc3REYXRhLCBhcGlVcmwpLnN1YnNjcmliZShcclxuICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICBzZWxmLnNuYWNrQmFyU2VydmljZS5hZGQoXHJcbiAgICAgICAgICAgIHNlbGYuX2Z1c2VUcmFuc2xhdGlvbkxvYWRlclNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgICB0b2FzdE1lc3NhZ2VEZXRhaWxzLnN1Y2Nlc3NcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIGlmIChyZXMuc3RhdHVzID09IDIwMCkge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcIkN1cnJlbnRSZXBvcnREYXRhXCIpO1xyXG4gICAgICAgICAgICBzZWxmLm1lc3NhZ2VTZXJ2aWNlLnNlbmRNb2RlbENsb3NlRXZlbnQoXCJsaXN0Vmlld1wiKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgc2VsZi5zbmFja0JhclNlcnZpY2Uud2FybmluZyhcclxuICAgICAgICAgICAgc2VsZi5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuZXJyb3JcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICAgICk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICB0aGlzLmNvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgLmdldEV4cG9ydFJlc3BvbnNlKHJlcXVlc3REYXRhLCBhcGlVcmwpXHJcbiAgICAgICAgLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgbGV0IGZpbGVOYW1lID0gdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICBleHBvcnREYXRhLmV4cG9ydFJlcXVlc3QuZG93bmxvYWRGaWxlTmFtZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICAgIHRoaXMuc25hY2tCYXJTZXJ2aWNlLmFkZChcclxuICAgICAgICAgICAgdGhpcy5fZnVzZVRyYW5zbGF0aW9uTG9hZGVyU2VydmljZS5pbnN0YW50KFxyXG4gICAgICAgICAgICAgIHRvYXN0TWVzc2FnZURldGFpbHMuc3VjY2Vzc1xyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICApO1xyXG4gICAgICAgICAgdGhpcy5yZWxvYWREYXRhKGFjdGlvbik7XHJcbiAgICAgICAgICB0aGlzLmRvd25sb2FkRmlsZShcclxuICAgICAgICAgICAgZGF0YSxcclxuICAgICAgICAgICAgZmlsZU5hbWUgKyBleHBvcnREYXRhLmV4cG9ydFJlcXVlc3QuZG93bmxvYWRGaWxldHlwZVxyXG4gICAgICAgICAgKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIHJlbG9hZERhdGEoYWN0aW9uKSB7XHJcbiAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbShcImN1cnJlbnRJbnB1dFwiKTtcclxuICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChhY3Rpb24pO1xyXG4gICAgY29uc29sZS5sb2coYWN0aW9uLCBcIi4uLi5hY3Rpb25cIik7XHJcbiAgICB0aGlzLnJlaW5pdGlhbGl6ZUJ1dHRvbnMoKTtcclxuICB9XHJcblxyXG4gIHRvZ2dsZUNoYW5nZShldmVudCkge1xyXG4gICAgY29uc29sZS5sb2coZXZlbnQpO1xyXG4gICAgbGV0IHZhbHVlVG9TYXZlID0gZXZlbnQgJiYgZXZlbnQudmFsdWUgPyBldmVudC52YWx1ZSA6IGV2ZW50O1xyXG4gICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0oXCJzZWxlY3RlZFRvZ2dsZURldGFpbFwiLCBKU09OLnN0cmluZ2lmeSh2YWx1ZVRvU2F2ZSkpO1xyXG4gICAgLy8gdGhpcy5jdXJyZW50VGFibGVMb2FkID0gdGhpcy5mb3JtVmFsdWVzO1xyXG4gICAgLy8gdGhpcy5jdXJyZW50VGFibGVMb2FkW1wicmVzcG9uc2VcIl0gPSBcIlwiO1xyXG4gICAgLy8gdGhpcy5jdXJyZW50VGFibGVMb2FkW1widG90YWxcIl0gPSBcIlwiXHJcbiAgICAvLyB0aGlzLmVuYWJsZVRhYmxlTGF5b3V0ID0gdHJ1ZVxyXG4gICAgdGhpcy5tZXNzYWdlU2VydmljZS5zZW5kTWVzc2FnZShcInRvZ2dsZVwiKTtcclxuICB9XHJcbiAgb3BlbkRpYWxvZyhhY3Rpb24pIHtcclxuICAgIGxldCBtb2RlbFdpZHRoID0gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dXHJcbiAgICAgID8gdGhpcy5jdXJyZW50Q29uZmlnRGF0YVthY3Rpb25dLm1vZGVsRGF0YS5zaXplXHJcbiAgICAgIDogXCI4MCVcIjtcclxuICAgIHRoaXMuZGlhbG9nUmVmID0gdGhpcy5fbWF0RGlhbG9nXHJcbiAgICAgIC5vcGVuKE1vZGVsTGF5b3V0Q29tcG9uZW50LCB7XHJcbiAgICAgICAgZGlzYWJsZUNsb3NlOiB0cnVlLFxyXG4gICAgICAgIHdpZHRoOiBtb2RlbFdpZHRoLFxyXG4gICAgICAgIHBhbmVsQ2xhc3M6IFwiY29udGFjdC1mb3JtLWRpYWxvZ1wiLFxyXG4gICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgIGFjdGlvbjogYWN0aW9uLFxyXG4gICAgICAgICAgdGl0bGU6IHRoaXMuaGVhZGVyVGV4dCxcclxuICAgICAgICAgIG1vZGFsSW5kZXg6IHRoaXMubW9kYWxJbmRleCxcclxuICAgICAgICB9LFxyXG4gICAgICB9KVxyXG4gICAgICAuYWZ0ZXJDbG9zZWQoKVxyXG4gICAgICAuc3Vic2NyaWJlKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKFwiY3VycmVudElucHV0XCIpO1xyXG4gICAgICAgIHRoaXMubWVzc2FnZVNlcnZpY2Uuc2VuZE1vZGVsQ2xvc2VFdmVudChcImxpc3RWaWV3XCIpO1xyXG4gICAgICB9KTtcclxuICB9XHJcbiAgZG93bmxvYWRGaWxlKHJlcG9ydFZhbHVlLCBmaWxlTmFtZSkge1xyXG4gICAgY29uc3QgYmxvYiA9IG5ldyBCbG9iKFtyZXBvcnRWYWx1ZV0sIHsgdHlwZTogXCJ0ZXh0L2NzdlwiIH0pO1xyXG4gICAgRmlsZVNhdmVyLnNhdmVBcyhibG9iLCBmaWxlTmFtZSk7XHJcbiAgfVxyXG5cclxuICBnZXRFbmFibGVkYnV0dG9ucyhidXR0b25zTGlzdCkge1xyXG4gICAgbGV0IHRlbXAgPSB7fTtcclxuICAgIHRoaXMuZW5hYmxlQnV0dG9uTGF5b3V0ID0gYnV0dG9uc0xpc3QgJiYgYnV0dG9uc0xpc3QubGVuZ3RoID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgdGhpcy5lbmFibGVkQnV0dG9ucyA9IGJ1dHRvbnNMaXN0O1xyXG4gIH1cclxufVxyXG4iXX0=