/**
 * Default Fuse Configuration
 *
 * You can edit these options to change the default options. All these options also can be
 * changed per component basis. See `app/main/pages/authentication/login/login.component.ts`
 * constructor method to learn more about changing these options per component basis.
 */
export const fuseConfig = {
    // Color themes can be defined in src/app/app.theme.scss
    colorTheme: "theme-default",
    customScrollbars: true,
    layout: {
        style: "vertical-layout-1",
        width: "fullwidth",
        navbar: {
            primaryBackground: "fuse-navy-700",
            secondaryBackground: "fuse-navy-900",
            folded: false,
            hidden: false,
            position: "left",
            variant: "vertical-style-1"
        },
        toolbar: {
            customBackgroundColor: false,
            background: "fuse-white-500",
            hidden: false,
            position: "below-static"
        },
        footer: {
            customBackgroundColor: true,
            background: "fuse-navy-900",
            hidden: false,
            position: "below-fixed"
        },
        sidepanel: {
            hidden: false,
            position: "right"
        }
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290Ijoibmc6Ly91aS1jb21tb24tbGliLyIsInNvdXJjZXMiOlsiZnVzZS1jb25maWcvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7Ozs7OztHQU1HO0FBRUgsTUFBTSxDQUFDLE1BQU0sVUFBVSxHQUFlO0lBQ3BDLHdEQUF3RDtJQUN4RCxVQUFVLEVBQUUsZUFBZTtJQUMzQixnQkFBZ0IsRUFBRSxJQUFJO0lBQ3RCLE1BQU0sRUFBRTtRQUNOLEtBQUssRUFBRSxtQkFBbUI7UUFDMUIsS0FBSyxFQUFFLFdBQVc7UUFDbEIsTUFBTSxFQUFFO1lBQ04saUJBQWlCLEVBQUUsZUFBZTtZQUNsQyxtQkFBbUIsRUFBRSxlQUFlO1lBQ3BDLE1BQU0sRUFBRSxLQUFLO1lBQ2IsTUFBTSxFQUFFLEtBQUs7WUFDYixRQUFRLEVBQUUsTUFBTTtZQUNoQixPQUFPLEVBQUUsa0JBQWtCO1NBQzVCO1FBQ0QsT0FBTyxFQUFFO1lBQ1AscUJBQXFCLEVBQUUsS0FBSztZQUM1QixVQUFVLEVBQUUsZ0JBQWdCO1lBQzVCLE1BQU0sRUFBRSxLQUFLO1lBQ2IsUUFBUSxFQUFFLGNBQWM7U0FDekI7UUFDRCxNQUFNLEVBQUU7WUFDTixxQkFBcUIsRUFBRSxJQUFJO1lBQzNCLFVBQVUsRUFBRSxlQUFlO1lBQzNCLE1BQU0sRUFBRSxLQUFLO1lBQ2IsUUFBUSxFQUFFLGFBQWE7U0FDeEI7UUFDRCxTQUFTLEVBQUU7WUFDVCxNQUFNLEVBQUUsS0FBSztZQUNiLFFBQVEsRUFBRSxPQUFPO1NBQ2xCO0tBQ0Y7Q0FDRixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgRnVzZUNvbmZpZyB9IGZyb20gXCIuLi9AZnVzZS90eXBlc1wiO1xyXG5cclxuLyoqXHJcbiAqIERlZmF1bHQgRnVzZSBDb25maWd1cmF0aW9uXHJcbiAqXHJcbiAqIFlvdSBjYW4gZWRpdCB0aGVzZSBvcHRpb25zIHRvIGNoYW5nZSB0aGUgZGVmYXVsdCBvcHRpb25zLiBBbGwgdGhlc2Ugb3B0aW9ucyBhbHNvIGNhbiBiZVxyXG4gKiBjaGFuZ2VkIHBlciBjb21wb25lbnQgYmFzaXMuIFNlZSBgYXBwL21haW4vcGFnZXMvYXV0aGVudGljYXRpb24vbG9naW4vbG9naW4uY29tcG9uZW50LnRzYFxyXG4gKiBjb25zdHJ1Y3RvciBtZXRob2QgdG8gbGVhcm4gbW9yZSBhYm91dCBjaGFuZ2luZyB0aGVzZSBvcHRpb25zIHBlciBjb21wb25lbnQgYmFzaXMuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGNvbnN0IGZ1c2VDb25maWc6IEZ1c2VDb25maWcgPSB7XHJcbiAgLy8gQ29sb3IgdGhlbWVzIGNhbiBiZSBkZWZpbmVkIGluIHNyYy9hcHAvYXBwLnRoZW1lLnNjc3NcclxuICBjb2xvclRoZW1lOiBcInRoZW1lLWRlZmF1bHRcIixcclxuICBjdXN0b21TY3JvbGxiYXJzOiB0cnVlLFxyXG4gIGxheW91dDoge1xyXG4gICAgc3R5bGU6IFwidmVydGljYWwtbGF5b3V0LTFcIixcclxuICAgIHdpZHRoOiBcImZ1bGx3aWR0aFwiLFxyXG4gICAgbmF2YmFyOiB7XHJcbiAgICAgIHByaW1hcnlCYWNrZ3JvdW5kOiBcImZ1c2UtbmF2eS03MDBcIixcclxuICAgICAgc2Vjb25kYXJ5QmFja2dyb3VuZDogXCJmdXNlLW5hdnktOTAwXCIsXHJcbiAgICAgIGZvbGRlZDogZmFsc2UsXHJcbiAgICAgIGhpZGRlbjogZmFsc2UsXHJcbiAgICAgIHBvc2l0aW9uOiBcImxlZnRcIixcclxuICAgICAgdmFyaWFudDogXCJ2ZXJ0aWNhbC1zdHlsZS0xXCJcclxuICAgIH0sXHJcbiAgICB0b29sYmFyOiB7XHJcbiAgICAgIGN1c3RvbUJhY2tncm91bmRDb2xvcjogZmFsc2UsXHJcbiAgICAgIGJhY2tncm91bmQ6IFwiZnVzZS13aGl0ZS01MDBcIixcclxuICAgICAgaGlkZGVuOiBmYWxzZSxcclxuICAgICAgcG9zaXRpb246IFwiYmVsb3ctc3RhdGljXCJcclxuICAgIH0sXHJcbiAgICBmb290ZXI6IHtcclxuICAgICAgY3VzdG9tQmFja2dyb3VuZENvbG9yOiB0cnVlLFxyXG4gICAgICBiYWNrZ3JvdW5kOiBcImZ1c2UtbmF2eS05MDBcIixcclxuICAgICAgaGlkZGVuOiBmYWxzZSxcclxuICAgICAgcG9zaXRpb246IFwiYmVsb3ctZml4ZWRcIlxyXG4gICAgfSxcclxuICAgIHNpZGVwYW5lbDoge1xyXG4gICAgICBoaWRkZW46IGZhbHNlLFxyXG4gICAgICBwb3NpdGlvbjogXCJyaWdodFwiXHJcbiAgICB9XHJcbiAgfVxyXG59O1xyXG4iXX0=