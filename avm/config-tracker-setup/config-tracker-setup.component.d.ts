import { OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SetupAdministrationService } from './../setup-administration.service';
import { SnackBarService } from './../../shared/snackbar.service';
import { ReportManagementService } from '../../avm/report-management.service';
import { Router } from '@angular/router';
export declare class ConfigTrackerSetupComponent implements OnInit {
    private formBuilder;
    private snackBarService;
    private SetupAdministrationService;
    private _reportManagementService;
    private router;
    CreateForm: FormGroup;
    control: Notification;
    dataSource: any;
    data_source: any;
    isActive: boolean;
    status: any;
    submitted: boolean;
    scheduleType: any;
    scheduleTypeValue: any;
    errorStatus: any;
    errorMsg: any;
    configDetailsID: any;
    module: [];
    object_name: [];
    frequentData: {
        "value": string;
        "name": string;
    }[];
    days: any[];
    constructor(formBuilder: FormBuilder, snackBarService: SnackBarService, SetupAdministrationService: SetupAdministrationService, _reportManagementService: ReportManagementService, router: Router);
    ngOnInit(): void;
    loadAll(): void;
    showDetail(e: any): void;
    getfrequentData($event: any): void;
    loadData(dataValue: any): void;
    onSubmit(): void;
}
export declare class ConfigSetup {
    userName: string;
    password: string;
    clientID: string;
    fusionUrl: string;
    scheduleType: string;
    retainDays: string;
    scheduleTypeValue: string;
    constructor(control: any);
}
