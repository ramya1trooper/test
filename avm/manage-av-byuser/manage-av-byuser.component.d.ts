import { SelectionModel } from '@angular/cdk/collections';
import { OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material';
import { MatSidenav } from '@angular/material';
import { Location } from '@angular/common';
import { ReportManagementService } from '../report-management.service';
import { SnackBarService } from './../../shared/snackbar.service';
import 'rxjs/add/observable/of';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from '../../_services/message.service';
import { State } from '@progress/kendo-data-query';
import { GridDataResult, DataStateChangeEvent } from '@progress/kendo-angular-grid';
export interface manageAvbyuser {
    ledger: string;
    journalname: string;
    journaldescription: string;
    journalsource: string;
    journalcategory: string;
    journalperiod: string;
    currencycode: string;
    status: string;
    createdby: string;
    postedby: string;
    amount: number;
    posteddate: Date;
}
export declare class ManageAvbyuserComponent implements OnInit {
    private _reportManagementService;
    private activeRoute;
    private snackBarService;
    private location;
    private messageService;
    private router;
    manageAvsbyuser: any;
    customdatasource: object[];
    defaultdatasourceId: string;
    selection: SelectionModel<manageAvbyuser>;
    dialogRef: any;
    nav_position: string;
    queryParams: any;
    limit: number;
    offset: number;
    length: number;
    tableConfig: any;
    avmExportObj: {};
    isLedgerTable: Boolean;
    isVendorTable: Boolean;
    isInvoiceTable: Boolean;
    columns: any[];
    settings: any;
    data: any;
    tempsettings: object;
    tempCols: object;
    dataSource: any;
    displayedColumns: any;
    selectedTableHeader: any[];
    kendoGridData: GridDataResult;
    enableFilter: boolean;
    _data: any[];
    info: boolean;
    type: "numeric" | "input";
    pageSizes: {
        text: number;
        value: number;
    }[];
    previousNext: boolean;
    state: State;
    getNotification(): void;
    viewData: {
        employees: any[];
    };
    paginator: MatPaginator;
    drawer: MatSidenav;
    constructor(_reportManagementService: ReportManagementService, activeRoute: ActivatedRoute, snackBarService: SnackBarService, location: Location, messageService: MessageService, router: Router);
    ngOnInit(): void;
    dataStateChange(state: DataStateChangeEvent): void;
    applyTableState(state: State): void;
    onClick(action: any): void;
    changePage(event: any): void;
    enableFilterOptions(): void;
    applyFilter(filterValue: string): void;
    backToAVDetail(): void;
    backToAV(): void;
    getdatasource(): void;
    getDatasourceId(id: any): void;
    updateTable(event: any): void;
    tableNamesearch(nameKey: any, myArray: any): number;
    panelOpenState: boolean;
    tableSetting(): void;
    loadTableColumn(): void;
    onLoadmanageAvs(id: any): void;
    exportAvmUserdetail(): void;
}
