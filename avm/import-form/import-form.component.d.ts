import { OnInit, ChangeDetectorRef, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ReportManagementService } from './../report-management.service';
import { SnackBarService } from './../../shared/snackbar.service';
export declare class AvmImportFormComponent implements OnInit {
    matDialogRef: MatDialogRef<AvmImportFormComponent>;
    private formBuilder;
    private reportManagementService;
    private cd;
    private snackBarService;
    dialogTitle: string;
    importAccessEntitlementForm: FormGroup;
    control: EntitlementImport;
    submitted: boolean;
    afterClose: EventEmitter<any>;
    dataSource: any;
    selectedDatasourceId: any;
    selectedDatasourceName: any;
    fileUpload: any;
    defaultDatasource: string;
    defaultDatasourceName: string;
    selectedFileName: string;
    btnSubmit: boolean;
    constructor(matDialogRef: MatDialogRef<AvmImportFormComponent>, formBuilder: FormBuilder, reportManagementService: ReportManagementService, cd: ChangeDetectorRef, snackBarService: SnackBarService);
    ngOnInit(): void;
    panelOpenState: boolean;
    logsArr: any;
    logsData: any;
    totalCount: number;
    saveCount: number;
    failedCount: number;
    duplicateCount: number;
    errorLogsHeader: string[];
    onSubmit(): void;
    getDatasource(): void;
    onChangeDatasourceId(dsID: any, dsIname: any): void;
    onFileChange(event: any): void;
}
export declare class EntitlementImport {
    data_source: string;
    file: string;
    constructor(control: any);
}
