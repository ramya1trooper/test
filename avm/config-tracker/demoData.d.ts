export declare const configMetaData: {
    "moduleData": {
        "name": string;
        "value": string;
    }[];
    "objectNames": {
        "value": string;
        "name": string;
    }[];
    "selectedTableHeader": {
        "value": string;
        "name": string;
    }[];
    "tableConfig": {
        "transactiontype": {
            "name": string;
            "value": string;
            "isActive": boolean;
        }[];
        "paymentterms": {
            "name": string;
            "value": string;
            "isActive": boolean;
        }[];
        "tolerances": {
            "name": string;
            "value": string;
            "isActive": boolean;
        }[];
        "autocashruleset": {
            "name": string;
            "value": string;
            "isActive": boolean;
        }[];
        "invoiceholdreleasename": {
            "name": string;
            "value": string;
            "isActive": boolean;
        }[];
    };
    "lastModifiedData": {
        "transactiontype": {
            "_id": string;
            "type": string;
            "configTrackerId": string;
            "Transaction Type Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Name": {
                "value": string;
                "isChanged": boolean;
            };
            "Type": {
                "value": string;
                "isChanged": boolean;
            };
            "Start Date": {
                "value": string;
                "isChanged": boolean;
            };
            "Open Receivable": {
                "value": string;
                "isChanged": boolean;
            };
            "Natural Application Only": {
                "value": string;
                "isChanged": boolean;
            };
            "Created By User Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Last Updated By User Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Last Update Date": {
                "value": string;
                "isChanged": boolean;
            };
        }[];
        "paymentterms": {
            "_id": string;
            "type": string;
            "configTrackerId": string;
            "Term Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Name": {
                "value": string;
                "isChanged": boolean;
            };
            "Description": {
                "value": string;
                "isChanged": boolean;
            };
            "Prepayment": {
                "value": string;
                "isChanged": boolean;
            };
            "Base Amount": {
                "value": string;
                "isChanged": boolean;
            };
            "Discount Basis": {
                "value": string;
                "isChanged": boolean;
            };
            "Created By User Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Last Updated By User Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Last Update Date": {
                "value": string;
                "isChanged": boolean;
            };
        }[];
        "tolerances": {
            "_id": string;
            "type": string;
            "configTrackerId": string;
            "Tolerance Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Tolerance Name": {
                "value": string;
                "isChanged": boolean;
            };
            "Description": {
                "value": string;
                "isChanged": boolean;
            };
            "Price Variance Tolerance": {
                "value": string;
                "isChanged": boolean;
            };
            "Total Amount Variance": {
                "value": string;
                "isChanged": boolean;
            };
            "Tolerance Type": {
                "value": string;
                "isChanged": boolean;
            };
            "Created By User Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Last Updated By User Id": {
                "value": string;
                "isChanged": boolean;
            };
            "Last Update Date": {
                "value": string;
                "isChanged": boolean;
            };
        }[];
    };
    "historyData": {
        "transactiontype": {
            "01": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Transaction Type Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Type": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Start Date": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Open Receivable": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Natural Application Only": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
            "02": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Transaction Type Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Type": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Start Date": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Open Receivable": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Natural Application Only": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
            "03": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Transaction Type Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Type": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Start Date": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Open Receivable": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Natural Application Only": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
            "04": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Transaction Type Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Type": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Start Date": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Open Receivable": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Natural Application Only": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
        };
        "paymentterms": {
            "11": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Term Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Description": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Prepayment": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Base Amount": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Discount Basis": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
            "12": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Term Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Description": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Prepayment": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Base Amount": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Discount Basis": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
            "13": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Term Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Description": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Prepayment": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Base Amount": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Discount Basis": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
        };
        "tolerances": {
            "21": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Tolerance Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Tolerance Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Description": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Price Variance Tolerance": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Total Amount Variance": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Tolerance Type": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
            "22": {
                "_id": string;
                "type": string;
                "configTrackerId": string;
                "Tolerance Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Tolerance Name": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Description": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Price Variance Tolerance": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Total Amount Variance": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Tolerance Type": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Created By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Updated By User Id": {
                    "value": string;
                    "isChanged": boolean;
                };
                "Last Update Date": {
                    "value": string;
                    "isChanged": boolean;
                };
            }[];
        };
    };
};
