import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { SnackBarService } from './../../../shared/snackbar.service';
export declare class ReportHistoryComponent implements OnInit {
    matDialogRef: MatDialogRef<ReportHistoryComponent>;
    private snackBarService;
    private data;
    dialogTitle: string;
    historyData: any;
    displayColumns: string[];
    parentLimit: 10;
    reportHistory: any;
    changedData: any;
    oldReportData: any;
    groupByShowHistory: any;
    dateColumns: any;
    selectedHeader: any;
    constructor(matDialogRef: MatDialogRef<ReportHistoryComponent>, snackBarService: SnackBarService, data: any);
    ngOnInit(): void;
    constructTable(): void;
}
