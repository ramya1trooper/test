import { MessageService } from "../_services/message.service";
import { HttpClient } from "@angular/common/http";
import { Compiler } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { ContentService } from "./content.service";
import { LoaderService } from '../loader.service';
export declare class ContentComponent {
    private compiler;
    private router;
    private messageService;
    private contentService;
    private httpClient;
    private route;
    private loaderService;
    private _metaData;
    currentRouteID: any;
    enableHeading: boolean;
    tableId: any;
    currentData: any;
    enableRunReportLayout: boolean;
    enableRunReport: boolean;
    enableAVM: boolean;
    enableAVMDetails: boolean;
    enableAVMUser: boolean;
    enableConfigTracker: boolean;
    enableConfigSetup: boolean;
    enableLibSetup: boolean;
    gridListData: any;
    currentConfigData: any;
    enableRouting: boolean;
    private unsubscribe;
    private unsubscribeRouting;
    /**
     * Constructor
     */
    data: any;
    constructor(compiler: Compiler, router: Router, messageService: MessageService, contentService: ContentService, httpClient: HttpClient, route: ActivatedRoute, loaderService: LoaderService, _metaData: any);
    ngOnInit(): void;
    getMetaData(): any[];
    ngOnDestroy(): void;
    receiveClick(event: any): void;
    loadData(data: any): void;
    loadRouting(data: any): void;
    loadPageRedirection(): void;
}
