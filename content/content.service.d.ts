import { Observable } from "rxjs/";
import { HttpClient } from "@angular/common/http";
import "rxjs/Rx";
import { LoaderService } from '../loader.service';
import { MessageService } from '../_services';
export declare class ContentService {
    private httpClient;
    private loaderService;
    private messageService;
    private environment;
    baseURL: string;
    constructor(httpClient: HttpClient, loaderService: LoaderService, messageService: MessageService, environment: any);
    getAllReponse(query: any, apiUrl: any): Observable<any>;
    getResponse(query: any, apiUrl: any): Observable<any>;
    getExportResponse(query: any, apiUrl: any): Observable<any>;
    getS3Response(query: any, apiUrl: any): Observable<any>;
    createRequest(data: any, apiUrl: any): Observable<any>;
    updateRequest(data: any, apiUrl: any, id: any): Observable<any>;
    avmImport(data: any, file: any, apiUrl: any): Observable<any>;
}
