import { ModuleWithProviders } from "@angular/core";
export declare class ContentModule {
    static forRoot(metaData: any, environment: any, english: any): ModuleWithProviders;
}
