import { NgxUiLoaderService } from 'ngx-ui-loader';
export declare class LoaderService {
    private ngxService;
    constructor(ngxService: NgxUiLoaderService);
    startLoader(): void;
    stopLoader(): void;
}
