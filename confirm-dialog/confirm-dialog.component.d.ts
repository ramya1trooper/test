import { MatDialogRef } from '@angular/material';
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
export declare class ConfirmDialogComponent {
    dialogRef: MatDialogRef<ConfirmDialogComponent>;
    private _fuseTranslationLoaderService;
    private english;
    data: any;
    title: any;
    message: any;
    note: any;
    modelData: any;
    includeNote: boolean;
    constructor(dialogRef: MatDialogRef<ConfirmDialogComponent>, _fuseTranslationLoaderService: FuseTranslationLoaderService, english: any, data: any);
    ngOnInit(): void;
    onConfirm(): void;
    onDismiss(): void;
}
