import { OnInit } from "@angular/core";
import { ContentService } from "../content/content.service";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
import { MessageService } from "../_services/message.service";
import { LoaderService } from "../loader.service";
export declare class CardLayoutComponent implements OnInit {
    private _fuseTranslationLoaderService;
    private contentService;
    private messageService;
    private loaderService;
    private english;
    dashboardCountData: any;
    dashboardLabel: any;
    currentConfigData: any;
    cardLayoutData: any[];
    defaultDatasource: any;
    inputData: any;
    closebanner: boolean;
    constructor(_fuseTranslationLoaderService: FuseTranslationLoaderService, contentService: ContentService, messageService: MessageService, loaderService: LoaderService, english: any);
    ngOnInit(): void;
    closeBanner(e: any): void;
}
