import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Router } from "@angular/router";
export declare class LoginService {
    private http;
    private router;
    private environment;
    baseURL: string;
    constructor(http: HttpClient, router: Router, environment: any);
    loginAuth(obj: any): Observable<any>;
    retriveJwt(resp: any): any;
    storeAuthToken(jwt: any): void;
    logout(): void;
}
