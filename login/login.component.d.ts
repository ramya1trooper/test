import { OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { FuseConfigService } from "../@fuse/services/config.service";
import { LoginService } from "./login.service";
import { OAuthService } from "angular-oauth2-oidc";
export declare class LoginComponent implements OnInit {
    private _fuseConfigService;
    private _formBuilder;
    private loginService;
    private router;
    private oauthService;
    loginForm: FormGroup;
    authenticationError: boolean;
    username: string;
    password: string;
    errorStatus: any;
    errorMsg: any;
    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(_fuseConfigService: FuseConfigService, _formBuilder: FormBuilder, loginService: LoginService, router: Router, oauthService: OAuthService);
    /**
     * On init
     */
    ngOnInit(): void;
    readonly f: {
        [key: string]: import("@angular/forms/src/model").AbstractControl;
    };
    login(): void;
}
