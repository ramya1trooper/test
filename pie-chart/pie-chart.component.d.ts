import { OnInit } from "@angular/core";
import { FuseTranslationLoaderService } from "../@fuse/services/translation-loader.service";
export declare class PieChartComponent implements OnInit {
    private _fuseTranslationLoaderService;
    private english;
    showLegend: boolean;
    explodeSlices: boolean;
    doughnut: boolean;
    result: any;
    scheme: any;
    view: any;
    label: any;
    chartData: any;
    translate: any;
    animations: any;
    legendTitle: any;
    arcWidth: any;
    gradient: any;
    legendPosition: any;
    pieTooltipText: any;
    constructor(_fuseTranslationLoaderService: FuseTranslationLoaderService, english: any);
    ngOnInit(): void;
    onLegendLabelClick(event: any): void;
    select(event: any): void;
}
