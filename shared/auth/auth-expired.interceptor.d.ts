import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { OAuthService } from "angular-oauth2-oidc";
import { LoaderService } from '../../loader.service';
export declare class AuthExpiredInterceptor implements HttpInterceptor {
    private router;
    private oauthService;
    private Loaderservice;
    constructor(router: Router, oauthService: OAuthService, Loaderservice: LoaderService);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
