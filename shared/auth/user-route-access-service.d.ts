import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { Principal } from "./principal.service";
export declare class UserRouteAccessService implements CanActivate {
    private router;
    private principal;
    constructor(router: Router, principal: Principal);
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Promise<boolean>;
    checkLogin(authorities: string[], url: string): Promise<boolean>;
}
