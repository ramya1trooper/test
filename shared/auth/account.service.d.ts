import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable, Subject } from "rxjs/Rx";
export declare class AccountService {
    private http;
    private environment;
    baseURL: string;
    issuesURL: string;
    idMgmtURL: string;
    constructor(http: HttpClient, environment: any);
    switchEvent: Subject<any>;
    get(): Observable<any>;
    changePassword(access_token: string): void;
    emitSwitchEvent(event: any): void;
    changeRealm(body: any): Observable<HttpResponse<any[]>>;
}
