import { TemplateRef, ViewContainerRef } from '@angular/core';
import { Principal } from './principal.service';
export declare class HasAnyAuthorityDirective {
    private principal;
    private templateRef;
    private viewContainerRef;
    authorities: string[];
    constructor(principal: Principal, templateRef: TemplateRef<any>, viewContainerRef: ViewContainerRef);
    hasAnyAuthority: string | string[];
    private updateView;
}
