import { ModuleWithProviders } from "@angular/core";
export declare class SharedModule {
    static forRoot(environment: any, english: any): ModuleWithProviders;
}
