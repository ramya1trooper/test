import { OnDestroy } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
export declare class SnackBarMessage {
    message: string;
    action: string;
    config: MatSnackBarConfig;
}
export declare class SnackBarService implements OnDestroy {
    snackBar: MatSnackBar;
    private messageQueue;
    private subscription;
    private snackBarRef;
    constructor(snackBar: MatSnackBar);
    ngOnDestroy(): void;
    /**
     * Add a message
     * @param message The message to show in the snackbar.
     * @param action The label for the snackbar action.
     * @param config Additional configuration options for the snackbar.
     */
    add(message: string, action?: string, config?: MatSnackBarConfig): void;
    warning(message: string, action?: string, config?: MatSnackBarConfig): void;
}
