import { Observable } from "rxjs/Rx";
import { NgxXml2jsonService } from "ngx-xml2json";
import { HttpClient } from "@angular/common/http";
import { Http } from "@angular/http";
export declare class AwsS3UploadService {
    private httpClient;
    private http;
    private xml2js;
    private environment;
    resourceUrl: string;
    allQuery: any;
    constructor(httpClient: HttpClient, http: Http, xml2js: NgxXml2jsonService, environment: any);
    find(req?: any): Observable<any>;
    uploadS3(file: any, s3credentials: any, data: any): Promise<any>;
}
